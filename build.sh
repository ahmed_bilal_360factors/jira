#! /bin/sh
cd "`dirname "$0"`"
SETTINGSFILE="settings.xml"
LOCALREPO="/root/.m2/repository/"
set -e
export PATH=.:$PATH
#mvn install:install-file -DgroupId=javax.activation -DartifactId=activation -Dversion=1.1.1 -Dpackaging=jar -Dfile=/opt/tmp/activation-1.1.1.jar
#mvn install:install-file -DgroupId=javax.jms -DartifactId=jms -Dversion=1.1 -Dpackaging=jar -Dfile=/opt/tmp/jms-1.1.jar
#mvn install:install-file -DgroupId=com.sun.jmx -DartifactId=jmxri -Dversion=1.2.1 -Dpackaging=jar -Dfile=/opt/tmp/jmxri.jar
#mvn install:install-file -DgroupId=com.sun.jdmk -DartifactId=jmxtools -Dversion=1.2.1 -Dpackaging=jar -Dfile=/opt/tmp/jmxtools.jar
#mvn install:install-file -DgroupId=jndi -DartifactId=jndi -Dversion=1.2.1 -Dpackaging=jar -Dfile=/opt/tmp/jndi.jar
#mvn install:install-file -DgroupId=jta -DartifactId=jta -Dversion=1.0.1 -Dpackaging=jar -Dfile=/opt/tmp/jta-1_0_1B-classes.jar
#mvn install:install-file -DgroupId=javax.mail -DartifactId=mail -Dversion=1.4 -Dpackaging=jar -Dfile=/opt/tmp/mail-1.4.jar
#mvn install:install-file -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0.4.0 -Dpackaging=jar -Dfile=/opt/tmp/ojdbc6.jar

mvn3.sh install -f jira-project/pom.xml -Dmaven.test.skip -Dmaven.test.skip -s $SETTINGSFILE -Dmaven.repo.local="$LOCALREPO" "$@"
mvn3.sh install -f jira-project/jira-components/jira-webapp/pom.xml -Dmaven.test.skip -Pbuild-from-source-dist -Dmaven.test.skip -s $SETTINGSFILE -Dmaven.repo.local="$LOCALREPO" "$@"
mvn3.sh clean package -Dmaven.test.skip -f jira-project/jira-distribution/jira-webapp-dist/pom.xml -Dmaven.test.skip -s $SETTINGSFILE -Dmaven.repo.local="$LOCALREPO" "$@"
