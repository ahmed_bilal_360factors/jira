package com.atlassian.jira.webtest.webdriver.tests.websudo;

/**
 * @since v5.0
 */

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.EnableWebSudo;
import com.atlassian.jira.pageobjects.config.ResetDataOnce;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.jira.pageobjects.pages.JiraLoginPage;
import com.atlassian.jira.pageobjects.pages.PluginsOverviewPage;
import com.atlassian.jira.pageobjects.pages.admin.ViewAttachmentsSettingsPage;
import com.atlassian.jira.pageobjects.pages.admin.ViewProjectsPage;
import com.atlassian.jira.pageobjects.project.DeleteProjectPage;
import com.atlassian.jira.pageobjects.websudo.JiraWebSudoBanner;
import com.atlassian.jira.pageobjects.websudo.JiraWebSudoPage;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@WebTest ({ Category.WEBDRIVER_TEST, Category.SECURITY })
@ResetDataOnce
@EnableWebSudo
public class TestWebSudo extends BaseJiraWebTest
{
    private static final String PASSWORD_FOR_WEBSUDO = JiraLoginPage.PASSWORD_ADMIN;

    @After
    public void dropWebSudo()
    {
        JiraWebSudoBanner webSudoBanner = pageBinder.bind(JiraWebSudoBanner.class);
        if(webSudoBanner.isShowing())
        {
            webSudoBanner.dropWebSudo(DashboardPage.class);
        }
    }

    private void triggerWebSudo()
    {
        jira.visitDelayed(ViewAttachmentsSettingsPage.class);
        final JiraWebSudoPage websudoPage = pageBinder.bind(JiraWebSudoPage.class);
        assertTrue(websudoPage.isAt().now());

        final ViewAttachmentsSettingsPage viewAttachmentSettings = websudoPage.confirm(PASSWORD_FOR_WEBSUDO, ViewAttachmentsSettingsPage.class);
        assertTrue(viewAttachmentSettings.isAt().now());
    }

    @Test
    public void testInvalidateSessionOnNewWebSudo()
    {
        final String before = jira.getTester().getDriver().manage().getCookieNamed("JSESSIONID").getValue();

        jira.visitDelayed(DeleteProjectPage.class, 10000L);
        final JiraWebSudoPage websudoPage = pageBinder.bind(JiraWebSudoPage.class);
        assertTrue(websudoPage.isAt().now());
        websudoPage.confirm(PASSWORD_FOR_WEBSUDO, DeleteProjectPage.class);

        final String after = jira.getTester().getDriver().manage().getCookieNamed("JSESSIONID").getValue();
        assertThat("Http Session was not invalidated when new web sudo session was started (core admin pages)", after, Matchers.not(before));
    }

    @Test
    public void testInvalidateSessionOnNewWebSudoOnPluginAdminPage()
    {
        final String before = jira.getTester().getDriver().manage().getCookieNamed("JSESSIONID").getValue();

        jira.visitDelayed(PluginsOverviewPage.class);
        final JiraWebSudoPage websudoPage = pageBinder.bind(JiraWebSudoPage.class);
        assertTrue(websudoPage.isAt().now());
        websudoPage.confirm(PASSWORD_FOR_WEBSUDO, PluginsOverviewPage.class);

        final String after = jira.getTester().getDriver().manage().getCookieNamed("JSESSIONID").getValue();
        assertThat("Http Session was not invalidated when new web sudo session was started (plugin admin pages)", after, Matchers.not(before));
    }

    @Test
    public void testWebSudoLoginPageAppearsOnlyOnceForProtectedPages()
    {
        triggerWebSudo();

        final ViewAttachmentsSettingsPage viewAttachmentSettings = jira.visit(ViewAttachmentsSettingsPage.class);
        assertTrue(viewAttachmentSettings.isAt().now());

        JiraWebSudoBanner webSudoBanner = pageBinder.bind(JiraWebSudoBanner.class);
        assertThat(webSudoBanner.isShowing(), is(true));
        assertThat(webSudoBanner.hasNormalDropLink(), is(false));
        assertThat(webSudoBanner.hasProtectedDropLink(), is(true));
    }

    @Test
    public void testWebSudoLoginPageSkippedForNormalPages()
    {
        final ViewProjectsPage viewProjectsPage = jira.visit(ViewProjectsPage.class);
        assertTrue(viewProjectsPage.isAt().now());

        JiraWebSudoBanner webSudoBanner = pageBinder.bind(JiraWebSudoBanner.class);
        assertThat(webSudoBanner.isShowing(), is(false));
    }

    @Test
    public void testWebSudoBannerDisappearsAfterDropOnNormalPages()
    {
        triggerWebSudo();
        jira.visit(ViewProjectsPage.class);

        JiraWebSudoBanner webSudoBanner = pageBinder.bind(JiraWebSudoBanner.class);
        assertThat(webSudoBanner.isShowing(), is(true));
        assertThat(webSudoBanner.hasNormalDropLink(), is(true));
        assertThat(webSudoBanner.hasProtectedDropLink(), is(false));

        final String oldLocation = jira.getTester().getDriver().getCurrentUrl();
        webSudoBanner.dropWebSudo(ViewProjectsPage.class);

        assertEquals("The old location and the current location should match.", oldLocation,
                jira.getTester().getDriver().getCurrentUrl());
        assertFalse(webSudoBanner.isShowing());

        jira.visit(DashboardPage.class);
        webSudoBanner = pageBinder.bind(JiraWebSudoBanner.class);
        assertFalse(webSudoBanner.isShowing());
    }

    @Test
    public void testWebSudoCancelRedirect()
    {
        jira.visit(DashboardPage.class);

        jira.visitDelayed(ViewAttachmentsSettingsPage.class);
        JiraWebSudoPage websudoPage = pageBinder.bind(JiraWebSudoPage.class);
        assertTrue(websudoPage.isAt().now());

        final ViewProjectsPage projectsPage = websudoPage.cancel(ViewProjectsPage.class);
        assertTrue(projectsPage.isAt().now());

        JiraWebSudoBanner webSudoBanner = pageBinder.bind(JiraWebSudoBanner.class);
        assertThat(webSudoBanner.isShowing(), is(false));
    }

    @Test
    public void testWebSudoBannerRedirectsAfterDropOnProtectedPages()
    {
        triggerWebSudo();

        jira.visit(ViewAttachmentsSettingsPage.class);
        JiraWebSudoBanner webSudoBanner = pageBinder.bind(JiraWebSudoBanner.class);
        assertTrue(webSudoBanner.isShowing());
        assertThat(webSudoBanner.hasNormalDropLink(), is(false));
        assertThat(webSudoBanner.hasProtectedDropLink(), is(true));

        final DashboardPage dashboard = webSudoBanner.dropWebSudo(DashboardPage.class);
        assertTrue(dashboard.isAt().now());

        webSudoBanner = pageBinder.bind(JiraWebSudoBanner.class);
        assertFalse(webSudoBanner.isShowing());
    }
}
