package com.atlassian.jira.webtest.webdriver.tests.setup;

import java.io.IOException;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.pages.setup.SetupAccountPage;
import com.atlassian.jira.pageobjects.pages.setup.SetupFinishingPage;
import com.atlassian.jira.pageobjects.pages.setup.SetupModePage;
import com.atlassian.jira.pageobjects.pages.setup.SetupProductBundlePage;
import com.atlassian.jira.webtest.webdriver.util.AUIFlags;
import com.atlassian.jira.webtests.LicenseKeys;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.atlassian.webdriver.utils.element.ElementLocated;
import com.atlassian.webdriver.utils.element.WebDriverPoller;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import org.eclipse.jetty.server.DispatcherType;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.eclipse.jetty.util.ajax.JSON;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * ************************************************************************<br>
 * Run this test with -Dtest.jira.setup.skip=true
 * JIRA needs to be run in dev mode for this test to succeed
 * ************************************************************************<br>
 */
// belongs to category SETUP_PRISTINE, but this category cannot be used until SETUP_PRISTINE is on master
@WebTest ({ Category.WEBDRIVER_TEST, Category.SETUP })
public class TestInstantSetup extends BaseJiraWebTest
{
    private static final int PORT = 44455;
    private static final String USER_EMAIL = "charlie@atlassian.com";
    private static final String USER_PASSWORD = "1234";
    private static final String USER_EMAIL_IN_URI_KEY = "userEmailInUri";
    private static final String USER_EMAIL_IN_AID_KEY = "userEmailInAid";
    private static final String XSRF_TOKEN = "WowMuchTokenVerySafetySuchProtection";
    private static final String XSRF_TOKEN_KEY = "xsrfToken";
    private Server mockServer;

    /* data is put here by the mock jetty server. Webdriver polling for elements does the
     * heavy lifting of time synchronisation.                                               */
    Map<String, String> asyncData = Collections.synchronizedMap(Maps.<String, String>newHashMap());

    @Inject
    private WebDriverPoller webDriverPoller;

    @Before
    public void setUp() throws Exception
    {
        if (isJiraSetup())
        {
            throw new IllegalStateException("JIRA must be in pristine state.");
        }

        mockServer = new Server(PORT);
        final HandlerList list = new HandlerList();

        list.addHandler(createContext(new AidUserExistsServlet(false), "/aid/profile/rest/user", "/*"));
        list.addHandler(createContext(new AidUserSignupServlet(), "/aid/profile/rest", "/signUp"));
        list.addHandler(createContext(new HamletEvaluationLicenseServlet(), "/hamlet/1.0/public/license", "/createEvaluation"));

        mockServer.setHandler(list);
        mockServer.start();
    }

    private ServletContextHandler createContext(final HttpServlet handler, final String contextPath, final String pathSpec)
    {
        final FilterHolder corsFilterHolder;
        corsFilterHolder = new FilterHolder(new CrossOriginFilter());
        corsFilterHolder.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,POST,HEAD,OPTIONS");
        corsFilterHolder.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        corsFilterHolder.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin,ATL-XSRF-Token");

        final ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath(contextPath);
        context.addServlet(new ServletHolder(handler), pathSpec);
        context.addFilter(corsFilterHolder, pathSpec, EnumSet.of(DispatcherType.REQUEST));
        return context;
    }

    @After
    public void tearDown() throws Exception
    {
        mockServer.stop();
    }

    @Test
    @LoginAs (anonymous = true)
    public void testInstantSetupSucceeds() throws Exception
    {
        final SetupModePage setupModePage = pageBinder.bind(SetupModePage.class);
        setupModePage.selectMode(SetupModePage.ModeSelection.INSTANT);

        final SetupProductBundlePage setupProductBundlePage = setupModePage.submitToInstantPath();
        setupProductBundlePage.selectBundle(SetupProductBundlePage.BundleSelection.TRACKING);

        final SetupAccountPage setupAccountPage = setupProductBundlePage.submitToInstantPath();
        final SetupAccountPage.CheckAccount checkAccount = setupAccountPage.checkAccountView();

        setTestUrlsInMacUtil();

        final SetupAccountPage.CreateAccount createAccount = checkAccount
                .setEmail(USER_EMAIL)
                .clickNextWithNonExistingUserEmail();
        assertEquals("User email should be escaped.", USER_EMAIL.replace("@", "%40"), pollAsyncData(USER_EMAIL_IN_URI_KEY));

        assertEquals("Email should be already prefilled.", USER_EMAIL, createAccount.getEmail());
        final SetupAccountPage.SetupAccountSummary setupAccountSummary = createAccount
                .setFullName("Charlie of Atlassian")
                .setPassword(USER_PASSWORD)
                .acceptLicenseAgreement(true)
                .clickCreate();
        assertEquals("The AID account name should be the user email provided.", USER_EMAIL, pollAsyncData(USER_EMAIL_IN_AID_KEY));
        assertEquals("The XSRF Token should be passed back to HAMLET", XSRF_TOKEN, pollAsyncData(XSRF_TOKEN_KEY));

        final SetupFinishingPage setupFinishingPage = setupAccountSummary.clickNext();

        setupFinishingPage.waitToFinish();
        backdoor.darkFeatures().loginAs(USER_EMAIL, USER_PASSWORD).enableForSite(FunctTestConstants.DARK_FEATURE_DISABLE_ONBOARDING_FLAG);
        setupFinishingPage.submit();
        assertJiraIsOnDashboard();

        // testing also if post setup announcements are properly integrated with instant setup
        // keeping method assertContainsPostSetupPopup in PostSetup class to make visible
        // where post setup steps are tested
        new AUIFlags(jira).closeAllFlags();
        assertContainsPostSetupPopup();
    }

    private String pollAsyncData(final String asyncDataKey)
    {
        Poller.waitUntilTrue(Conditions.forSupplier(new Supplier<Boolean>() {

            @Override
            public Boolean get()
            {
                return asyncData.containsKey(asyncDataKey);
            }
        }));
        return asyncData.get(asyncDataKey);
    }

    private void setTestUrlsInMacUtil()
    {
        final String scriptThatSetsTestLassoUrl = "(new (require(\"jira/setup/setup-mac-util\"))()).setDevModeLassoUrl(\"http://localhost:" + PORT + "/aid/\");";
        jira.getTester().getDriver().executeScript(scriptThatSetsTestLassoUrl);

        final String scriptThatSetsTestHamletUrl = "(new (require(\"jira/setup/setup-mac-util\"))()).setDevModeHamletUrl(\"http://localhost:" + PORT + "/hamlet/\");";
        jira.getTester().getDriver().executeScript(scriptThatSetsTestHamletUrl);
    }

    public void assertContainsPostSetupPopup()
    {
        final String POST_SETUP_DB = "inline-dialog-post-setup-announcement-database-setup";
        webDriverPoller.waitUntil(new ElementLocated(By.id(POST_SETUP_DB)), 1);
    }

    private boolean isJiraSetup()
    {
        final String baseUrl = jira.getProductInstance().getBaseUrl();
        final WebDriverTester tester = jira.getTester();
        tester.gotoUrl(baseUrl);
        final String currentURL = tester.getDriver().getCurrentUrl();
        return (baseUrl + "/secure/Dashboard.jspa").equals(currentURL) || (baseUrl + "/login.jsp").equals(currentURL);
    }

    private void assertJiraIsOnDashboard()
    {
        final String baseUrl = jira.getProductInstance().getBaseUrl();
        final String currentURL = jira.getTester().getDriver().getCurrentUrl();

        assertThat(currentURL, is(equalTo(baseUrl + "/secure/Dashboard.jspa")));
    }

    private class AidUserExistsServlet extends HttpServlet
    {
        private final boolean exists;

        private AidUserExistsServlet(final boolean exists)
        {
            this.exists = exists;
        }

        @Override
        protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
        {
            final String requestURI = request.getRequestURI();
            System.out.println(getClass().getSimpleName() + ": " + requestURI);
            asyncData.put(USER_EMAIL_IN_URI_KEY, last(requestURI.split("/")));

            final Map<String, Object> responseMap = Maps.newHashMap();
            responseMap.put("exists", exists);
            responseMap.put("contactDetails", null);
            responseMap.put("organisationDetails", null);

            respondWithData(response, responseMap);
        }

        private <T> T last(final T[] arr)
        {
            return arr[arr.length - 1];
        }
    }

    private class AidUserSignupServlet extends HttpServlet
    {
        @Override
        protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
        {
            final Map<String, String> data = (Map<String, String>) JSON.parse(request.getReader());
            final String requestEmail = data.get("email");
            asyncData.put(USER_EMAIL_IN_AID_KEY, requestEmail);

            final ImmutableMap<String, Object> responseMap = ImmutableMap.<String, Object>of(
                    "username", requestEmail,
                    "xsrfToken", XSRF_TOKEN
            );
            respondWithData(response, responseMap);
        }
    }

    private class HamletEvaluationLicenseServlet extends HttpServlet
    {
        @Override
        protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
        {
            asyncData.put(XSRF_TOKEN_KEY, request.getHeader("ATL-XSRF-Token"));

            // this comes with data: SEN and product_key: "jira". Not asserted on, no benefit.

            final ImmutableMap<String, Object> responseMap = ImmutableMap.<String, Object>of(
                    "id", 12345678,
                    "owner", USER_EMAIL,
                    "organisation", "Evaluation license",
                    "licenseKey", LicenseKeys.V2_AGED_EVALUATION_LICENSE.getLicenseString()
            );
            respondWithData(response, responseMap);
        }
    }

    private void respondWithData(final HttpServletResponse response, final Map<String, Object> responseMap)
            throws IOException
    {
        final String json = JSON.toString(responseMap);
        response.setContentType(MediaType.APPLICATION_JSON);
        response.getWriter().write(json);
        response.getWriter().close();
    }
}
