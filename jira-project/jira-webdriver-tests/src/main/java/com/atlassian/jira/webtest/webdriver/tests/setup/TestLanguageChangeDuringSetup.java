package com.atlassian.jira.webtest.webdriver.tests.setup;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.pages.setup.SetupLanguageDialog;
import com.atlassian.jira.pageobjects.pages.setup.SetupModePage;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

/**
 * ************************************************************************<br>
 * Run this test with -Dtest.jira.setup.skip=true
 * JIRA needs to be run in dev mode for this test to succeed
 * ************************************************************************<br>
 */
// belongs to category SETUP_PRISTINE, but this category cannot be used until SETUP_PRISTINE is on master
@WebTest ({ Category.WEBDRIVER_TEST, Category.SETUP })
public class TestLanguageChangeDuringSetup extends BaseJiraWebTest
{
    @Before
    public void setUp() throws Exception
    {
        if (isJiraSetup())
        {
            throw new IllegalStateException("JIRA must be in pristine state.");
        }
    }

    @Test
    @LoginAs (anonymous = true)
    public void testLanguageDialogChangesLanguage() throws Exception
    {
        final SetupModePage setupMode = pageBinder.bind(SetupModePage.class);
        final String initialTriggerText = setupMode.getLanguageChangeDialogTriggerText();
        final SetupLanguageDialog langDialog = setupMode.openLanguageDialog();

        langDialog.selectLanguage("Antarctica");
        final SetupModePage setupModeReloaded = langDialog.submitLanguage();

        assertThat(setupModeReloaded.getLanguageChangeDialogTriggerText(), not(initialTriggerText));
    }

    private boolean isJiraSetup()
    {
        final String baseUrl = jira.getProductInstance().getBaseUrl();
        final WebDriverTester tester = jira.getTester();
        tester.gotoUrl(baseUrl);
        final String currentURL = tester.getDriver().getCurrentUrl();
        return (baseUrl + "/secure/Dashboard.jspa").equals(currentURL) || (baseUrl + "/login.jsp").equals(currentURL);
    }
}
