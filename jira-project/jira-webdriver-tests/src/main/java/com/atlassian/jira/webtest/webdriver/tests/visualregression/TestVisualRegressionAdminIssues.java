package com.atlassian.jira.webtest.webdriver.tests.visualregression;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Webdriver test for visual regression for the issues section of JIRA admin.
 *
 * @since v6.4.2
 */
@WebTest({Category.WEBDRIVER_TEST, Category.VISUAL_REGRESSION})
public class TestVisualRegressionAdminIssues extends JiraVisualRegressionTest
{

    @BeforeClass
    public static void restoreInstance()
    {
        //we cannot restore instance via @Restore annotation as it doesn't support restoring license from backup file
        backdoor.dataImport().restoreDataFromResource("xml/TestVisualRegressionSmoke.zip", "");
    }

    @Test
    public void testAdminIssueTypes()
    {
        assertPageMatches("admin-issue-types", "/secure/admin/ViewIssueTypes.jspa");
    }

    @Test
    public void testAdminIssueTypeSchemes()
    {
        assertPageMatches("admin-issue-type-schemes", "/secure/admin/ManageIssueTypeSchemes!default.jspa");
    }

    @Test
    public void testAdminAddIssueTypeScheme()
    {
        assertPageMatches("admin-add-issue-type-scheme", "/secure/admin/ConfigureOptionSchemes!default.jspa");
    }

    @Test
    public void testAdminEditIssueTypeScheme()
    {
        assertPageMatches("admin-edit-issue-type-scheme", "/secure/admin/ConfigureOptionSchemes!default.jspa?fieldId=&schemeId=10000");
    }

    @Test
    public void testAdminConfigureScreen()
    {
        assertPageMatches("admin-configure-screen", "/secure/admin/ConfigureFieldScreen.jspa?id=1");
    }

    @Test
    public void testAdminCustomFields()
    {
        assertPageMatches("admin-custom-fields", "/secure/admin/ViewCustomFields.jspa");
    }

    @Test
    public void testAdminConfigureCustomField()
    {
        assertPageMatches("admin-configure-custom-field", "/secure/admin/ConfigureCustomField!default.jspa?customFieldId=10071");
    }

    @Test
    public void testAdminIssueStatuses()
    {
        assertPageMatches("admin-issue-statuses", "/secure/admin/ViewStatuses.jspa");
    }

    @Test
    public void testAdminTranslateIssueStatuses()
    {
        assertPageMatches("admin-translate-issue-statuses", "/secure/admin/ViewTranslations!default.jspa?issueConstantType=status");
    }

    @Test
    public void testAdminIssueResolutions()
    {
        assertPageMatches("admin-issue-resolutions", "/secure/admin/ViewResolutions.jspa");
    }

    @Test
    public void testAdminIssuePriorities()
    {
        assertPageMatches("admin-issue-priorities", "/secure/admin/ViewPriorities.jspa");
    }

}

