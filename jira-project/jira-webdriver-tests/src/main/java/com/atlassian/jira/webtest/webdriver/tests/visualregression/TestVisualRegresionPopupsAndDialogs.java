package com.atlassian.jira.webtest.webdriver.tests.visualregression;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.dialogs.LabelsDialog;
import com.atlassian.jira.pageobjects.dialogs.LogWorkDialog;
import com.atlassian.jira.pageobjects.model.DefaultIssueActions;
import com.atlassian.jira.pageobjects.pages.viewissue.AssignIssueDialog;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.AttachFileDialog;
import com.atlassian.jira.pageobjects.pages.viewissue.linkissue.LinkIssueDialog;
import org.junit.Before;
import org.junit.Test;

/**
 * Visual regression tests for in-page dialogs and own-page popups. These screenshots are generally smaller than a normal page.
 *
 * @since v6.0
 */
@WebTest ( { Category.WEBDRIVER_TEST, Category.VISUAL_REGRESSION })
@Restore ("xml/TestVisualRegressionSmoke.zip")
public class TestVisualRegresionPopupsAndDialogs extends JiraVisualRegressionTest
{

    @Before
    public void beforeTest() {
        resizeWindowToDefaultDimentions();
    }
    @Test
    public void testUserPickerPopup()
    {
        assertPageMatches("popup-user-picker", "/secure/popups/UserPickerBrowser.jspa?formName=jiraform&multiSelect=true&decorator=popup&element=customfield_10090");
    }

    @Test
    public void testFilterOrProjectPickerPopup()
    {
        assertPageMatches("popup-filter-project-picker", "/secure/FilterPickerPopup.jspa?showProjects=true&filterView=projects&field=filterId");
    }

    @Test
    public void testFilterPickerPopupSearch()
    {
        assertPageMatches("popup-filter-search", "/secure/FilterPickerPopup.jspa?filterView=search&field=filterId&showProjects=true&searchName=&searchOwnerUserName=&searchShareType=any&groupShare=jira-administrators&projectShare=10020&roleShare=&Search=Search");
    }

    @Test
    public void testAssignDialog()
    {
        goToIssue().getIssueMenu().invoke(DefaultIssueActions.ASSIGN_ISSUE);
        pageBinder.bind(AssignIssueDialog.class);
        assertUIMatches("issue-assign-dialog");
    }

    @Test
    public void testAttachDialog()
    {
        goToIssue().getIssueMenu().invoke(DefaultIssueActions.ATTACH_FILES);
        pageBinder.bind(AttachFileDialog.class);
        assertUIMatches("issue-attach-dialog");
    }

    @Test
    public void testEditDialog()
    {
        goToIssue().editIssue();
        assertUIMatches("issue-edit-dialog");
    }

    @Test
    public void testLabelsDialog()
    {
        goToIssue().getIssueMenu().invoke(DefaultIssueActions.EDIT_LABELS);
        pageBinder.bind(LabelsDialog.class);
        assertUIMatches("issue-labels-dialog");
    }

    @Test
    public void testLinkDialog()
    {
        goToIssue().getIssueMenu().invoke(DefaultIssueActions.LINK_ISSUE);
        pageBinder.bind(LinkIssueDialog.class);
        assertUIMatches("issue-link-dialog");
    }

    @Test
    public void testLogWorkDialog()
    {
        goToIssue().getIssueMenu().invoke(DefaultIssueActions.LOG_WORK);
        pageBinder.bind(LogWorkDialog.class);
        assertUIMatches("issue-log-work-dialog");
    }

    private ViewIssuePage goToIssue() {
        return jira.goToViewIssue("XSS-17");
    }

}
