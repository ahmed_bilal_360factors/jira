package com.atlassian.jira.webtest.webdriver.tests.bulk;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.navigator.AdvancedSearch;
import com.atlassian.jira.pageobjects.navigator.BulkEdit;
import com.atlassian.jira.pageobjects.navigator.ChooseOperation;
import com.atlassian.jira.pageobjects.navigator.ConfirmationPage;
import com.atlassian.jira.pageobjects.navigator.EditDetails;
import com.atlassian.jira.pageobjects.navigator.OperationProgress;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.testkit.client.restclient.Component;
import com.atlassian.jira.testkit.client.restclient.Version;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.containsString;

import static com.atlassian.jira.rest.api.issue.ResourceRef.withName;

import javax.annotation.Nullable;
import javax.inject.Inject;

/**
 * Tests for BulkEdit of multi select system fields
 *
 * @since v6.4
 */
@WebTest ({ Category.WEBDRIVER_TEST, Category.BULK_OPERATIONS })
public class TestBulkEditIssues extends BaseJiraWebTest
{
    @Inject
    PageElementFinder elementFinder;

    protected static final String PROJECT_NAME = "Test Project";
    protected static final String PROJECT_KEY = "TST";
    protected static final String OPTION_ADD = "add";
    protected static final String OPTION_REMOVE = "remove";
    protected static final String OPTION_REPLACE = "replace";
    protected static final String OPTION_REMOVEALL = "removeall";
    protected static final String OPTION_ADD_DESCRIPTION = "Add to existing";
    protected static final String OPTION_REMOVE_DESCRIPTION = "Find and remove these";
    protected static final String OPTION_REPLACE_DESCRIPTION = "Replace all with";
    protected static final String OPTION_REMOVEALL_DESCRIPTION = "Clear field";
    protected static final String LABELS_NAME = "Labels";
    protected static final String VERSIONS_NAME = "Fix Version/s";
    protected static final String COMPONENTS_NAME = "Component/s";
    protected static final String LABEL1 = "Label1";
    protected static final String LABEL2 = "Label2";
    protected static final String LABEL3 = "Label3";
    protected static final String LABEL4 = "Label4";
    protected static final String NON_EXISTING_VERISON_NAME = "NewVersion";
    Component COMPONENT1 = new Component();
    Component COMPONENT2 = new Component();
    Component COMPONENT3 = new Component();
    Version VERSION1 = new Version();
    Version VERSION2 = new Version();
    Version VERSION3 = new Version();

    @Before
    public void initializeTestData()
    {
        backdoor.restoreBlankInstance();
        backdoor.project().addProject(PROJECT_NAME, PROJECT_KEY, "admin");
        COMPONENT1.id(new Long(10001)).project(PROJECT_KEY).name("Component1");
        COMPONENT2.id(new Long(10002)).project(PROJECT_KEY).name("Component2");
        COMPONENT3.id(new Long(10003)).project(PROJECT_KEY).name("Component3");
        VERSION1.id(new Long(10001)).project(PROJECT_KEY).name("Version1");
        VERSION2.id(new Long(10001)).project(PROJECT_KEY).name("Version2");
        VERSION3.id(new Long(10001)).project(PROJECT_KEY).name("Version3");

        backdoor.components().create(COMPONENT1);
        backdoor.components().create(COMPONENT2);
        backdoor.components().create(COMPONENT3);
        backdoor.versions().create(VERSION1);
        backdoor.versions().create(VERSION2);
        backdoor.versions().create(VERSION3);
    }

    @Test
    public void testBulkEditAddLabels()
    {
        IssueCreateResponse issue1 = backdoor.issues().createIssue(PROJECT_KEY,"Bulk Edit Test Issue 1");
        IssueCreateResponse issue2 = backdoor.issues().createIssue(PROJECT_KEY,"Bulk Edit Test Issue 2");
        backdoor.issues().addLabel(issue1.key(),LABEL1);
        backdoor.issues().addLabel(issue2.key(),LABEL2);
        AdvancedSearch searchPage = jira.visit(AdvancedSearch.class);
        searchPage = searchPage.enterQuery("").submit();
        BulkEdit bulkEdit = searchPage.toolsMenu().open().bulkChange();
        ChooseOperation chooseOperation = bulkEdit.selectAllIssues().chooseOperation();
        EditDetails editDetails = chooseOperation.editIssues();
        // Had to add aditional actions to expandUnavailableActions and mark labels checkbox
        // as there was flakiness problem with operations done too fast:
        // https://jdog.jira-dev.com/browse/JDEV-31473")
        ConfirmationPage confirmationPage = editDetails.typeLabels(LABEL2).typeLabels(LABEL3)
                .expandUnavailableActions().markLabelsCheckbox().next();
        assertThat(confirmationPage.getUpdatedField(),containsString(LABELS_NAME));
        assertThat(confirmationPage.getUpdateOption(),containsString(OPTION_ADD_DESCRIPTION));
        OperationProgress operationProgress = confirmationPage.confirm();
        operationProgress.acknowledge();
        assertThat(backdoor.issues().getIssue(issue1.key()).fields.labels,hasItem(LABEL1));
        assertThat(backdoor.issues().getIssue(issue1.key()).fields.labels,hasItem(LABEL2));
        assertThat(backdoor.issues().getIssue(issue1.key()).fields.labels,hasItem(LABEL3));
        assertThat(backdoor.issues().getIssue(issue2.key()).fields.labels,not(hasItem(LABEL1)));
        assertThat(backdoor.issues().getIssue(issue2.key()).fields.labels,hasItem(LABEL2));
        assertThat(backdoor.issues().getIssue(issue2.key()).fields.labels,hasItem(LABEL3));
        assertThat(backdoor.issues().getIssue(issue2.key()).fields.labels.size(),equalTo(2));
    }

    @Test
    public void testBulkEditRemoveExistingLabel()
    {
        IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_KEY,"Bulk Edit Test Issue");
        backdoor.issues().addLabel(issue.key(),LABEL1);
        backdoor.issues().addLabel(issue.key(),LABEL2);
        backdoor.issues().addLabel(issue.key(),LABEL3);
        AdvancedSearch searchPage = jira.visit(AdvancedSearch.class);
        searchPage = searchPage.enterQuery("").submit();
        BulkEdit bulkEdit = searchPage.toolsMenu().open().bulkChange();
        ChooseOperation chooseOperation = bulkEdit.selectAllIssues().chooseOperation();
        EditDetails editDetails = chooseOperation.editIssues();
        // Had to add aditional actions to expandUnavailableActions and mark labels checkbox
        // as there was flakiness problem with operations done too fast:
        // https://jdog.jira-dev.com/browse/JDEV-31473")
        ConfirmationPage confirmationPage = editDetails.addLabels(OPTION_REMOVE,LABEL1).typeLabels(LABEL2)
                .expandUnavailableActions().markLabelsCheckbox().next();
        assertThat(confirmationPage.getUpdatedField(),containsString(LABELS_NAME));
        assertThat(confirmationPage.getUpdateOption(),containsString(OPTION_REMOVE_DESCRIPTION));
        OperationProgress operationProgress = confirmationPage.confirm();
        operationProgress.acknowledge();
        assertThat(backdoor.issues().getIssue(issue.key()).fields.labels,hasItem(LABEL3));
        assertThat(backdoor.issues().getIssue(issue.key()).fields.labels,not(hasItem(LABEL1)));
        assertThat(backdoor.issues().getIssue(issue.key()).fields.labels,not(hasItem(LABEL2)));
    }

    @Test
    public void testBulkEditReplaceExistingLabels()
    {
        IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_KEY,"Bulk Edit Test Issue");
        backdoor.issues().addLabel(issue.key(),LABEL1);
        backdoor.issues().addLabel(issue.key(),LABEL2);
        AdvancedSearch searchPage = jira.visit(AdvancedSearch.class);
        searchPage = searchPage.enterQuery("").submit();
        BulkEdit bulkEdit = searchPage.toolsMenu().open().bulkChange();
        ChooseOperation chooseOperation = bulkEdit.selectAllIssues().chooseOperation();
        EditDetails editDetails = chooseOperation.editIssues();
        // Had to add aditional actions to expandUnavailableActions and mark labels checkbox
        // as there was flakiness problem with operations done too fast:
        // https://jdog.jira-dev.com/browse/JDEV-31473")
        ConfirmationPage confirmationPage = editDetails.addLabels(OPTION_REPLACE, LABEL3).typeLabels(LABEL4)
                .expandUnavailableActions().markLabelsCheckbox().next();
        assertThat(confirmationPage.getUpdatedField(),containsString(LABELS_NAME));
        assertThat(confirmationPage.getUpdateOption(),containsString(OPTION_REPLACE_DESCRIPTION));
        OperationProgress operationProgress = confirmationPage.confirm();
        operationProgress.acknowledge();
        assertThat(backdoor.issues().getIssue(issue.key()).fields.labels, hasItem(LABEL3));
        assertThat(backdoor.issues().getIssue(issue.key()).fields.labels,hasItem(LABEL4));
        assertThat(backdoor.issues().getIssue(issue.key()).fields.labels,not(hasItem(LABEL1)));
        assertThat(backdoor.issues().getIssue(issue.key()).fields.labels,not(hasItem(LABEL2)));
    }

    @Test
    public void testBulkEditRemoveAllLabels()
    {
        IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_KEY,"Bulk Edit Test Issue");
        backdoor.issues().addLabel(issue.key(),LABEL1);
        backdoor.issues().addLabel(issue.key(),LABEL2);
        AdvancedSearch searchPage = jira.visit(AdvancedSearch.class);
        searchPage = searchPage.enterQuery("").submit();
        BulkEdit bulkEdit = searchPage.toolsMenu().open().bulkChange();
        ChooseOperation chooseOperation = bulkEdit.selectAllIssues().chooseOperation();
        EditDetails editDetails = chooseOperation.editIssues();
        ConfirmationPage confirmationPage = editDetails.addLabels(OPTION_REMOVEALL,"").next();
        assertThat(confirmationPage.getUpdatedField(),containsString(LABELS_NAME));
        assertThat(confirmationPage.getUpdateOption(),containsString(OPTION_REMOVEALL_DESCRIPTION));
        OperationProgress operationProgress = confirmationPage.confirm();
        operationProgress.acknowledge();
        assertThat(backdoor.issues().getIssue(issue.key()).fields.labels.isEmpty(),is(true));
    }

    @Test
    public void testBulkEditAddVersions()
    {
        IssueCreateResponse issue1 = backdoor.issues().createIssue(PROJECT_KEY,"Bulk Edit Test Issue");
        IssueCreateResponse issue2 = backdoor.issues().createIssue(PROJECT_KEY,"Bulk Edit Test Issue");
        IssueFields issueFields1 = new IssueFields();
        issueFields1.fixVersions(withName(VERSION1.name));
        IssueFields issueFields2 = new IssueFields();
        issueFields2.fixVersions(withName(VERSION2.name));
        backdoor.issues().setIssueFields(issue1.key(),issueFields1);
        backdoor.issues().setIssueFields(issue2.key(),issueFields2);
        AdvancedSearch searchPage = jira.visit(AdvancedSearch.class);
        searchPage = searchPage.enterQuery("").submit();
        BulkEdit bulkEdit = searchPage.toolsMenu().open().bulkChange();
        ChooseOperation chooseOperation = bulkEdit.selectFirstIssue().chooseOperation();
        EditDetails editDetails = chooseOperation.editIssues();
        ConfirmationPage confirmationPage = editDetails.addFixVersions(OPTION_ADD,  NON_EXISTING_VERISON_NAME).next();
        assertThat(confirmationPage.getUpdatedField(),containsString(VERSIONS_NAME));
        assertThat(confirmationPage.getUpdateOption(),containsString(OPTION_ADD_DESCRIPTION));
        OperationProgress operationProgress = confirmationPage.confirm();
        operationProgress.acknowledge();
        assertThat(transformVersions(backdoor.issues().getIssue(issue1.key()).fields.fixVersions),hasItem(VERSION1.name));
        assertThat(transformVersions(backdoor.issues().getIssue(issue1.key()).fields.fixVersions),not(hasItem(NON_EXISTING_VERISON_NAME)));
        assertThat(transformVersions(backdoor.issues().getIssue(issue2.key()).fields.fixVersions),hasItem(VERSION2.name));
        assertThat(transformVersions(backdoor.issues().getIssue(issue2.key()).fields.fixVersions),hasItem(NON_EXISTING_VERISON_NAME));
    }

    @Test
    public void testBulkEditRemoveExistingVersion()
    {
        IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_KEY,"Bulk Edit Test Issue");
        IssueFields issueFields = new IssueFields();
        issueFields.fixVersions(Lists.newArrayList(withName(VERSION1.name),withName(VERSION2.name)));
        backdoor.issues().setIssueFields(issue.key(), issueFields);
        AdvancedSearch searchPage = jira.visit(AdvancedSearch.class);
        searchPage = searchPage.enterQuery("").submit();
        BulkEdit bulkEdit = searchPage.toolsMenu().open().bulkChange();
        ChooseOperation chooseOperation = bulkEdit.selectFirstIssue().chooseOperation();
        EditDetails editDetails = chooseOperation.editIssues();
        ConfirmationPage confirmationPage = editDetails.addFixVersions(OPTION_REMOVE, VERSION1.name).next();
        assertThat(confirmationPage.getUpdatedField(),containsString(VERSIONS_NAME));
        assertThat(confirmationPage.getUpdateOption(),containsString(OPTION_REMOVE_DESCRIPTION));
        OperationProgress operationProgress = confirmationPage.confirm();
        operationProgress.acknowledge();
        ViewIssuePage viewIssuePage = jira.goToViewIssue(issue.key());
        assertThat(transformVersions(backdoor.issues().getIssue(issue.key()).fields.fixVersions),not(hasItem(VERSION1.name)));
        assertThat(transformVersions(backdoor.issues().getIssue(issue.key()).fields.fixVersions),hasItem(VERSION2.name));
    }

    @Test
    public void testBulkEditAddComponents()
    {
        initializeTestData();
        IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_KEY,"Bulk Edit Test Issue");
        IssueFields issueFields = new IssueFields();
        issueFields.components(Lists.newArrayList(withName(COMPONENT1.name), withName(COMPONENT2.name)));
        backdoor.issues().setIssueFields(issue.key(),issueFields);
        AdvancedSearch searchPage = jira.visit(AdvancedSearch.class);
        searchPage = searchPage.enterQuery("").submit();
        BulkEdit bulkEdit = searchPage.toolsMenu().open().bulkChange();
        ChooseOperation chooseOperation = bulkEdit.selectFirstIssue().chooseOperation();
        EditDetails editDetails = chooseOperation.editIssues();
        ConfirmationPage confirmationPage = editDetails.addComponents(OPTION_ADD, COMPONENT3.name).next();
        assertThat(confirmationPage.getUpdatedField(),containsString(COMPONENTS_NAME));
        assertThat(confirmationPage.getUpdateOption(),containsString(OPTION_ADD_DESCRIPTION));
        OperationProgress operationProgress = confirmationPage.confirm();
        operationProgress.acknowledge();
        assertThat(transformComponents(backdoor.issues().getIssue(issue.key()).fields.components),hasItem(COMPONENT1.name));
        assertThat(transformComponents(backdoor.issues().getIssue(issue.key()).fields.components),hasItem(COMPONENT2.name));
        assertThat(transformComponents(backdoor.issues().getIssue(issue.key()).fields.components),hasItem(COMPONENT3.name));
    }

    @Test
    public void testBulkEditRemoveAllComponents()
    {
        IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_KEY,"Bulk Edit Test Issue");
        IssueFields issueFields = new IssueFields();
        issueFields.components(Lists.newArrayList(withName(COMPONENT1.name),withName(COMPONENT2.name),withName(COMPONENT3.name)));
        backdoor.issues().setIssueFields(issue.key(),issueFields);
        AdvancedSearch searchPage = jira.visit(AdvancedSearch.class);
        searchPage = searchPage.enterQuery("").submit();
        BulkEdit bulkEdit = searchPage.toolsMenu().open().bulkChange();
        ChooseOperation chooseOperation = bulkEdit.selectFirstIssue().chooseOperation();
        EditDetails editDetails = chooseOperation.editIssues();
        ConfirmationPage confirmationPage = editDetails.addComponents(OPTION_REMOVEALL, "").next();
        assertThat(confirmationPage.getUpdatedField(),containsString(COMPONENTS_NAME));
        assertThat(confirmationPage.getUpdateOption(),containsString(OPTION_REMOVEALL_DESCRIPTION));
        OperationProgress operationProgress = confirmationPage.confirm();
        operationProgress.acknowledge();
        assertThat((backdoor.issues().getIssue(issue.key()).fields.components.isEmpty()), is(true));
    }

    @Test
    public void testBulkEditReplaceComponents()
    {
        IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_KEY,"Bulk Edit Test Issue");
        IssueFields issueFields = new IssueFields();
        issueFields.components(Lists.newArrayList(withName(COMPONENT1.name),withName(COMPONENT2.name)));
        backdoor.issues().setIssueFields(issue.key(),issueFields);
        AdvancedSearch searchPage = jira.visit(AdvancedSearch.class);
        searchPage = searchPage.enterQuery("").submit();
        BulkEdit bulkEdit = searchPage.toolsMenu().open().bulkChange();
        ChooseOperation chooseOperation = bulkEdit.selectFirstIssue().chooseOperation();
        EditDetails editDetails = chooseOperation.editIssues();
        ConfirmationPage confirmationPage = editDetails.addComponents(OPTION_REPLACE, COMPONENT3.name).next();
        assertThat(confirmationPage.getUpdatedField(),containsString(COMPONENTS_NAME));
        assertThat(confirmationPage.getUpdateOption(),containsString(OPTION_REPLACE_DESCRIPTION));
        OperationProgress operationProgress = confirmationPage.confirm();
        operationProgress.acknowledge();
        assertThat(transformComponents(backdoor.issues().getIssue(issue.key()).fields.components),not(hasItem(COMPONENT1.name)));
        assertThat(transformComponents(backdoor.issues().getIssue(issue.key()).fields.components),not(hasItem(COMPONENT2.name)));
        assertThat(transformComponents(backdoor.issues().getIssue(issue.key()).fields.components),hasItem(COMPONENT3.name));
    }

    @Test
    public void testValidationForEmptyLabelsFieldWhenAddSelected()
    {
        IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_KEY,"Bulk Edit Test Issue");
        AdvancedSearch searchPage = jira.visit(AdvancedSearch.class);
        searchPage = searchPage.enterQuery("").submit();
        BulkEdit bulkEdit = searchPage.toolsMenu().open().bulkChange();
        ChooseOperation chooseOperation = bulkEdit.selectFirstIssue().chooseOperation();
        EditDetails editDetails = chooseOperation.editIssues();
        editDetails.markLabelsCheckbox().selectLabelsOption(OPTION_ADD).nextWhenFailedValidation();
        assertThat(editDetails.isAt().byDefaultTimeout(),is(true));
        assertThat(editDetails.getValidationError(),containsString("Labels"));
        assertThat(editDetails.getValidationError(),containsString("Add to existing"));
        assertThat(editDetails.getValidationError(),containsString("At least one valid value must be provided for the multi select field:"));
    }

    @Test
    public void testValidationForEmptyComponentsFieldWhenRemoveSelected()
    {
        IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_KEY,"Bulk Edit Test Issue");
        AdvancedSearch searchPage = jira.visit(AdvancedSearch.class);
        searchPage = searchPage.enterQuery("").submit();
        BulkEdit bulkEdit = searchPage.toolsMenu().open().bulkChange();
        ChooseOperation chooseOperation = bulkEdit.selectFirstIssue().chooseOperation();
        EditDetails editDetails = chooseOperation.editIssues();
        editDetails.selectComponentsOption(OPTION_REMOVE).nextWhenFailedValidation();
        assertThat(editDetails.isAt().byDefaultTimeout(),is(true));
        assertThat(editDetails.getValidationError(),containsString("Component/s"));
        assertThat(editDetails.getValidationError(),containsString("Find and remove these"));
        assertThat(editDetails.getValidationError(),containsString("At least one valid value must be provided for the multi select field:"));
    }

    @Test
    public void testValidationForEmptyFixVersionFieldWhenReplaceSelected()
    {
        IssueCreateResponse issue = backdoor.issues().createIssue(PROJECT_KEY,"Bulk Edit Test Issue");
        AdvancedSearch searchPage = jira.visit(AdvancedSearch.class);
        searchPage = searchPage.enterQuery("").submit();
        BulkEdit bulkEdit = searchPage.toolsMenu().open().bulkChange();
        ChooseOperation chooseOperation = bulkEdit.selectFirstIssue().chooseOperation();
        EditDetails editDetails = chooseOperation.editIssues();
        editDetails.selectFixVersionsOption(OPTION_REPLACE).nextWhenFailedValidation();
        assertThat(editDetails.isAt().byDefaultTimeout(),is(true));
        assertThat(editDetails.getValidationError(),containsString("Fix Version/s"));
        assertThat(editDetails.getValidationError(),containsString("Replace all with"));
        assertThat(editDetails.getValidationError(),containsString("At least one valid value must be provided for the multi select field:"));
    }

    public Iterable<String> transformComponents(Iterable<Component> components)
    {
        return Iterables.transform(components, new Function<Component, String>()
        {
            @Override
            public String apply(@Nullable final Component component)
            {
                return component.name;
            }
        });
    }

    public Iterable<String> transformVersions(Iterable<Version> versions)
    {
        return Iterables.transform(versions, new Function<Version, String>()
        {
            @Override
            public String apply(@Nullable final Version version)
            {
                return version.name;
            }
        });
    }
}
