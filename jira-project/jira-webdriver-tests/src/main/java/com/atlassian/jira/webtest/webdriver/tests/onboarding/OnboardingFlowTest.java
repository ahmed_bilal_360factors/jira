package com.atlassian.jira.webtest.webdriver.tests.onboarding;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.CreateUser;
import com.atlassian.jira.pageobjects.config.LoginAs;
import com.atlassian.jira.pageobjects.config.ResetDataOnce;
import com.atlassian.jira.pageobjects.navigator.AgnosticIssueNavigator;
import com.atlassian.jira.pageobjects.navigator.IssueNavigatorResults;
import com.atlassian.jira.pageobjects.onboarding.AssignIssuesSequence;
import com.atlassian.jira.pageobjects.onboarding.AvatarSequence;
import com.atlassian.jira.pageobjects.onboarding.ChooseNextStepSequence;
import com.atlassian.jira.pageobjects.onboarding.CreateIssuesSequence;
import com.atlassian.jira.pageobjects.onboarding.CreateProjectSequence;
import com.atlassian.jira.pageobjects.onboarding.NutshellSequence;
import com.atlassian.jira.pageobjects.onboarding.WelcomeToJiraPage;
import com.atlassian.jira.pageobjects.pages.project.BrowseProjectsPage;
import com.atlassian.jira.webtests.Groups;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@WebTest({ Category.WEBDRIVER_TEST })
@ResetDataOnce()
public class OnboardingFlowTest extends BaseJiraWebTest
{
    private static final String DEVELOPER = "developer";
    private static final String PROJECT_NAME = "Test My Project";
    private static final String PROJECT_KEY = "TMP";
    private static final String SUMMARY_1 = "An issue 1";
    private static final String SUMMARY_2 = "An issue 2";
    private static final String USER_1 = "alice";
    private static final String USER_2 = "bob";

    @BeforeClass
    public static void setUpClass()
    {
        createDeveloper(USER_1);
        createDeveloper(USER_2);
    }

    @Test
    @LoginAs(admin = true, targetPage = WelcomeToJiraPage.class)
    public void testOnboardingAsAdmin()
    {
        goThroughAvatarPicker();
        goThroughNutshell();
        goThroughCreateProject();
        goThroughCreateIssues(SUMMARY_1, SUMMARY_2);
        goThroughAssignIssues(USER_1, USER_2);
        assertIssues(SUMMARY_1, USER_1, SUMMARY_2, USER_2);
    }

    @Test
    @CreateUser(username = DEVELOPER, password = DEVELOPER, developer = true)
    @LoginAs(user = DEVELOPER, targetPage = WelcomeToJiraPage.class)
    public void testOnboardingAsNonAdmin()
    {
        goThroughAvatarPicker();
        goThroughNutshell();
        goThroughChooseNextStep();
    }

    private void goThroughAvatarPicker()
    {
        AvatarSequence avatarSequence = pageBinder.bind(AvatarSequence.class);
        avatarSequence.done();
    }

    private void goThroughNutshell()
    {
        NutshellSequence nutshellSequence = pageBinder.bind(NutshellSequence.class);
        while (!nutshellSequence.isDone())
        {
            nutshellSequence.nextStep();
        }
    }

    private void goThroughCreateProject()
    {
        CreateProjectSequence createProjectSequence = pageBinder.bind(CreateProjectSequence.class);
        createProjectSequence.setProjectName(PROJECT_NAME);
        Poller.waitUntilEquals("Project key should be generated",
                PROJECT_KEY, createProjectSequence.getProjectKey()
        );
        createProjectSequence.submit();
    }

    private void goThroughCreateIssues(String summary1, String summary2)
    {
        CreateIssuesSequence createIssuesSequence = pageBinder.bind(CreateIssuesSequence.class);
        createIssuesSequence.createIssue(summary1);
        createIssuesSequence.createIssue(summary2);
        assertThat("Issues were created", createIssuesSequence.getNumberOfIssuesCreated(), is(2));
        createIssuesSequence.done();
    }

    private void goThroughAssignIssues(String assignee1, String assignee2)
    {
        AssignIssuesSequence assignIssuesSequence = pageBinder.bind(AssignIssuesSequence.class);
        assignIssuesSequence.assignIssue(0, assignee1);
        assignIssuesSequence.assignIssue(1, assignee2);
        assignIssuesSequence.done();
    }

    private void assertIssues(String summary1, String assignee1, String summary2, String assignee2)
    {
        // we are redirected to the embedded issues navigator, but broken page objects mean we can't test this page
        // just yet.  So lets click through to the full issue nav
        jira.goToIssueNavigator().enterQuery("project=" + PROJECT_KEY).submit();

        AgnosticIssueNavigator issueNavigator = pageBinder.bind(AgnosticIssueNavigator.class);
        IssueNavigatorResults results = issueNavigator.getResults();
        // Issues are displayed in undetermined order
        assertThat(results.getSelectedIssue().getSummary(), anyOf(is(summary2), is(summary1)));
        if (results.getSelectedIssue().getSummary().equals(summary2))
        {
            assertThat(results.getSelectedIssue().getAssignee(), is(assignee2));
            results.nextIssue();
            assertThat(results.getSelectedIssue().getSummary(), is(summary1));
            assertThat(results.getSelectedIssue().getAssignee(), is(assignee1));
        }
        else
        {
            assertThat(results.getSelectedIssue().getAssignee(), is(assignee1));
            results.nextIssue();
            assertThat(results.getSelectedIssue().getSummary(), is(summary2));
            assertThat(results.getSelectedIssue().getAssignee(), is(assignee2));
        }
    }

    private void goThroughChooseNextStep()
    {
        ChooseNextStepSequence chooseNextStepSequence = pageBinder.bind(ChooseNextStepSequence.class);
        chooseNextStepSequence.clickBrowseProjects();
        pageBinder.bind(BrowseProjectsPage.class);
    }

    private static void createDeveloper(String username)
    {
        backdoor.usersAndGroups().addUser(username).addUserToGroup(username, Groups.DEVELOPERS);
    }
}
