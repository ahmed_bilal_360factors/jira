package com.atlassian.jira.webtest.webdriver.tests.visualregression;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;

/**
 * Webdriver test for visual regression for the system section of JIRA admin.
 *
 * @since v6.4.2
 */
@WebTest({Category.WEBDRIVER_TEST, Category.VISUAL_REGRESSION})
public class TestVisualRegressionAdminSystem extends JiraVisualRegressionTest
{

    @BeforeClass
    public static void restoreInstance()
    {
        //we cannot restore instance via @Restore annotation as it doesn't support restoring license from backup file
        backdoor.dataImport().restoreDataFromResource("xml/TestVisualRegressionSmoke.zip", "");
    }

    @Test
    public void testTestForSchedulerAdmin()
    {
        assertPageMatches("test-for-scheduler-admin", "secure/admin/SchedulerAdmin.jspa");
    }

    @Test
    public void testAdminApplicationProperties()
    {
        assertPageMatches("admin-application-properties", "/secure/admin/ViewApplicationProperties.jspa");
    }

    @Test
    public void testAdminAdvancedApplicationProperties()
    {
        assertPageMatches("admin-advanced-application-properties", "/secure/admin/AdvancedApplicationProperties.jspa");
    }

    @Test
    public void testAdminViewProjectRoles()
    {
        assertPageMatches("admin-view-project-roles", "/secure/project/ViewProjectRoles.jspa");
    }

    @Test
    public void testAdminGlobalPermissions()
    {
        assertPageMatches("admin-global-permissions", "/secure/admin/GlobalPermissions!default.jspa");
    }

    @Test
    public void testAdminConnectionsWhitelist()
    {
        assertPageMatches("admin-connections-whitelist", "/plugins/servlet/whitelist");
    }

    @Test
    public void testAdminIssueLinking()
    {
        assertPageMatches("admin-issue-linking", "/secure/admin/ViewLinkTypes!default.jspa");
    }

    @Test
    public void testAdminLookAndFeel()
    {
        assertPageMatches("admin-look-and-feel", "/secure/admin/LookAndFeel!default.jspa");
    }

    @Test
    public void testAdminAnnouncmentBanner()
    {
        assertPageMatches("admin-announcements-banner", "/secure/admin/EditAnnouncementBanner!default.jspa");
    }

    @Test
    public void testAdminApplicationNavigator()
    {
        assertPageMatches("admin-application-navigator", "/plugins/servlet/customize-application-navigator");
    }

    @Test
    public void testAdminSearchSharedFilters()
    {
        assertPageMatches("admin-search-shared-filters", "/secure/admin/filters/ViewSharedFilters.jspa");
    }

    @Test
    public void testAdminSearchSharedDashboards()
    {
        assertPageMatches("admin-search-shared-dashboards", "/secure/admin/dashboards/ViewSharedDashboards.jspa");
    }

    @Test
    public void testAdminEventListeners()
    {
        assertPageMatches("admin-event-listeners", "/secure/admin/ViewListeners!default.jspa");
    }

}

