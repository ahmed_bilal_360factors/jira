package com.atlassian.jira.webtest.webdriver.tests.visualregression;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;

/**
 * Webdriver test for visual regression for the users section of JIRA admin.
 *
 * @since v6.4.2
 */
@WebTest({Category.WEBDRIVER_TEST, Category.VISUAL_REGRESSION})
public class TestVisualRegressionAdminUsers extends JiraVisualRegressionTest
{

    @BeforeClass
    public static void restoreInstance()
    {
        //we cannot restore instance via @Restore annotation as it doesn't support restoring license from backup file
        backdoor.dataImport().restoreDataFromResource("xml/TestVisualRegressionSmoke.zip", "");
    }

    @Test
    public void testAdminViewUserProfile() throws InterruptedException
    {
        goTo("/secure/admin/user/ViewUser.jspa?name=admin");

        // Ignore login times.
        addElementsToIgnore(By.id("lastLogin"));
        addElementsToIgnore(By.id("previousLogin"));
        addElementsToIgnore(By.id("lastFailedLogin"));

        //and login count as we do not restore JIRA at each test
        addElementsToIgnore(By.id("loginCount"));

        assertUIMatches("admin-user-profile");
    }

    @Test
    public void testAdminUsersList()
    {
        goTo("/secure/admin/user/UserBrowser.jspa");
        addElementsToIgnore(By.id("user_browser_table"));
        assertUIMatches("admin-users-list");
    }

    @Test
    public void testAdminGroups() {
        assertPageMatches("admin-groups-list", "/secure/admin/user/GroupBrowser.jspa");
    }

    @Test
    public void testUserDirectories() {
        assertPageMatches("admin-user-directories", "/plugins/servlet/embedded-crowd/directories/list");
    }
}

