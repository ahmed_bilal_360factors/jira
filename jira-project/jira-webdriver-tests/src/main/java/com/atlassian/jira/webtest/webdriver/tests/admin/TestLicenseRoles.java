package com.atlassian.jira.webtest.webdriver.tests.admin;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.matcher.LicenseRoleBeanMatcher;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.ResetDataOnce;
import com.atlassian.jira.pageobjects.pages.admin.licenserole.LicenseRolePage;
import com.atlassian.jira.webtests.LicenseKeys;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

@WebTest ({ Category.WEBDRIVER_TEST, Category.ADMINISTRATION })
@ResetDataOnce
public class TestLicenseRoles extends BaseJiraWebTest
{
    private static final String GROUP_USERS = "jira-users";
    private static final String GROUP_DEVELOPERS = "jira-developers";
    private static final String GROUP_ADMINS = "jira-administrators";
    public static final String ROLEID = "com.atlassian.jira.platform";

    /**
     * Happy integration test. Edge cases are tested in QUnit.
     */
    @Test
    public void sanityTest()
    {
        //Role needs to be licensed.
        backdoor.license().set(LicenseKeys.V2_PLATFORM_ROLE.getLicenseString());

        final LicenseRolePage page = jira.goTo(LicenseRolePage.class);

        LicenseRolePage.Role role = page.role(ROLEID);
        assertThat(role, RoleMatcher.forBusinessUser(null));
        assertThat(backdoor.licenseRoles().getBusinessUser(),
                LicenseRoleBeanMatcher.forBusinessUser());

        //Add First group should have primary set.
        assertThat(role.addGroup(GROUP_USERS), RoleMatcher.forBusinessUser(GROUP_USERS, GROUP_USERS));
        assertThat(backdoor.licenseRoles().getBusinessUser(),
                LicenseRoleBeanMatcher.forBusinessUser().setGroups(GROUP_USERS).setPrimaryGroup(GROUP_USERS));

        //Add second group should leave users as the primary.
        assertThat(role.addGroup(GROUP_DEVELOPERS), RoleMatcher.forBusinessUser(GROUP_USERS, GROUP_USERS, GROUP_DEVELOPERS));
        assertThat(backdoor.licenseRoles().getBusinessUser(),
                LicenseRoleBeanMatcher.forBusinessUser().setGroups(GROUP_USERS, GROUP_DEVELOPERS).setPrimaryGroup(GROUP_USERS));

        //Set the primary to the second group.
        role.getGroup(GROUP_DEVELOPERS).setPrimary();
        assertThat(role, RoleMatcher.forBusinessUser(GROUP_DEVELOPERS, GROUP_USERS, GROUP_DEVELOPERS));
        assertThat(backdoor.licenseRoles().getBusinessUser(),
                LicenseRoleBeanMatcher.forBusinessUser().setGroups(GROUP_USERS, GROUP_DEVELOPERS).setPrimaryGroup(GROUP_DEVELOPERS));

        //Now remove group.
        role.getGroup(GROUP_USERS).remove();
        assertThat(role, RoleMatcher.forBusinessUser(GROUP_DEVELOPERS, GROUP_DEVELOPERS));
        assertThat(backdoor.licenseRoles().getBusinessUser(),
                LicenseRoleBeanMatcher.forBusinessUser().setGroups(GROUP_DEVELOPERS).setPrimaryGroup(GROUP_DEVELOPERS));
    }

    private static class RoleMatcher extends TypeSafeDiagnosingMatcher<LicenseRolePage.Role>
    {
        private final String name;
        private List<Matcher<? super LicenseRolePage.RoleGroup>> groups;

        public static RoleMatcher forBusinessUser(String primary, String...others)
        {
            return new RoleMatcher("Business User", primary, others);
        }

        private RoleMatcher(String name, String primary, String...groups)
        {
            this.name = name;
            List<Matcher<? super LicenseRolePage.RoleGroup>> matchers = Lists.newArrayList();
            matchers.addAll(GroupMatcher.forGroups(primary, groups));
            this.groups = ImmutableList.copyOf(matchers);
        }

        @Override
        protected boolean matchesSafely(final LicenseRolePage.Role item, final Description mismatchDescription)
        {
            if (!Objects.equal(name, item.getName()))
            {
                describeTo(item, mismatchDescription);
                return false;
            }
            else
            {
                final List<LicenseRolePage.RoleGroup> itemGroups = item.getGroups();
                if (groups.isEmpty())
                {
                    if (!itemGroups.isEmpty())
                    {
                        describeTo(item, mismatchDescription);
                        return false;
                    }
                }
                else
                {
                    final Matcher<Iterable<LicenseRolePage.RoleGroup>> contains = Matchers.contains(groups);
                    if (!contains.matches(itemGroups))
                    {
                        describeTo(item, mismatchDescription);
                        return false;
                    }
                }
            }
            return true;
        }

        private static boolean describeTo(final LicenseRolePage.Role item, final Description mismatchDescription)
        {
            mismatchDescription.appendText(String.format("[name: %s, groups: [", item.getName()));
            boolean first = true;
            for (LicenseRolePage.RoleGroup roleGroup : item.getGroups())
            {
                if (!first)
                {
                    mismatchDescription.appendValue(", ");
                }

                mismatchDescription.appendText(GroupMatcher.toString(roleGroup));

                first = false;
            }
            mismatchDescription.appendText("]");
            return false;
        }

        @Override
        public void describeTo(final Description description)
        {
            description.appendText(String.format("[name: %s", name))
                    .appendList(", groups: [", ", ", "]", groups);
        }
    }

    private static class GroupMatcher extends TypeSafeDiagnosingMatcher<LicenseRolePage.RoleGroup>
    {
        private final String group;
        private final boolean primary;

        private static List<GroupMatcher> forGroups(String primary, String...groups)
        {
            final ImmutableList.Builder<GroupMatcher> builder = ImmutableList.builder();
            for (String group: Arrays.asList(groups))
            {
                builder.add(new GroupMatcher(group, primary != null && primary.equals(group)));
            }
            return builder.build();
        }

        private GroupMatcher(String group, boolean primary)
        {
            this.group = group;
            this.primary = primary;
        }

        @Override
        protected boolean matchesSafely(final LicenseRolePage.RoleGroup item, final Description mismatchDescription)
        {
            if (Objects.equal(group, item.getName()) && primary == item.isPrimary() && primary != item.canRemove())
            {
                return true;
            }
            else
            {
                mismatchDescription.appendValue(toString(item));
                return false;
            }
        }

        private static String toString(final LicenseRolePage.RoleGroup item)
        {
            final boolean primary = item.isPrimary();
            return String.format("[name: %s, primary: %s, canRemove: %s]", item.getName(), primary, !primary);
        }

        @Override
        public void describeTo(final Description description)
        {
            description.appendText(String.format("[name: %s, primary: %s, canRemove: %s]", group, primary, !primary));
        }
    }
}
