package com.atlassian.jira.webtest.webdriver.tests.visualregression;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.framework.util.TimedQueryFactory;
import com.atlassian.jira.pageobjects.pages.admin.user.UserBrowserPage;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;

import com.google.common.base.Supplier;
import com.google.inject.Inject;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.atlassian.jira.functest.framework.NavigationImpl.PROJECT_PLUGIN_PREFIX;
import static org.hamcrest.Matchers.is;

/**
 * Webdriver test for visual regression.
 *
 * @since v5.0
 */
@WebTest({Category.WEBDRIVER_TEST, Category.VISUAL_REGRESSION})
public class TestVisualRegressionSmoke extends JiraVisualRegressionTest
{
    @Inject
    TimedQueryFactory timedQueryFactory;

    @BeforeClass
    public static void restoreInstance()
    {
        //we cannot restore instance via @Restore annotation as it doesn't support restoring license from backup file
        backdoor.dataImport().restoreDataFromResource("xml/TestVisualRegressionSmoke.zip", "");
    }

    @Test
    public void testEmptyDashboard()
    {
        visualComparer.setWaitforJQueryTimeout(0);
        visualComparer.setRefreshAfterResize(false);
        resizeWindowToDefaultDimentions();
        jira.gotoHomePage().gadgets().switchDashboard("Empty");
        assertUIMatches("empty-dashboard");
    }

    @Test
    public void testMediumDashboard()
    {
        visualComparer.setWaitforJQueryTimeout(0);
        visualComparer.setRefreshAfterResize(false);
        resizeWindowToDefaultDimentions();
        jira.gotoHomePage().gadgets().switchDashboard("Medium");
        assertUIMatches("medium-dashboard");
    }

    @Test
    public void testViewIssue()
    {
        jira.goToViewIssue("BULK-5");
        assertUIMatches("long-issue");
    }

    @Test
    public void testUserBrowser()
    {
        jira.goTo(UserBrowserPage.class).setUserFilterTo10Users().gotoResultPage(2);

        /*
         * As we just logged in with 'admin', its login info will have been updated to the current time.
         * We should ignore that info in the screenshot.
         */
        addElementsToIgnore(By.xpath("//table[@id=\"user_browser_table\"]/tbody/tr[@data-user=\"admin\"]/td[@data-cell-type=\"login-details\"]"));

        // Take the screenshot and compare.
        assertUIMatches("user-browser");
    }

    @Test
    public void testManageDashboardsFavouriteTab()
    {
        assertPageMatches("manage-favourite-dashboards", "/secure/ConfigurePortalPages!default.jspa?view=favourites");
    }

    @Test
    public void cloneDashboardPage()
    {
        goTo("/secure/ConfigurePortalPages!default.jspa?view=favourites");
        clickOnElement("#pp_10032 > .cell-type-actions .cog-dd"); // To open the options dropdown
        clickOnElement("#clone_2"); // To select 'Copy' from the dropdown
        assertUIMatches("clone-medium-dashboard");
    }

    @Test
    public void testManageFiltersFavouriteTab()
    {
        assertPageMatches("manage-favourite-filters", "/secure/ManageFilters.jspa?filterView=favourites");
    }

    @Test
    public void testSearchFilters()
    {
        goTo("/secure/ManageFilters.jspa?filterView=search");
        clickOnElement("#filterSearchForm input[type=submit]");
        visualComparer.setWaitforJQueryTimeout(5000);
        visualComparer.setRefreshAfterResize(false);
        assertUIMatches("search-filters");
    }

    @Test
    @Ignore("Test can't seem to find the input element when run in bamboo. Don't know why.")
    public void subscribeToFilterMonthly()
    {
        goTo("/secure/ManageFilters.jspa?filterView=my");
        clickOnElement("#subscribe_Bugs", true);
        clickOnElement("input[value='daysOfMonth']"); // to change to a days in a month filter
        assertUIMatches("subscribe-to-bugs-filter-monthly");
    }

    @Test
    public void editFilterPage()
    {
        assertPageMatches("edit-bugs-filter", "/EditFilter!default.jspa?filterId=10016");
    }

    @Test
    public void testBrowseProjectSummary()
    {
        goTo("/browse/BULK");

        // wait for ActivityStream gadget to load up
        final WebElement activityStreamFrame = jira.getTester().getDriver().findElement(By.id("gadget-1"));
        jira.getTester().getDriver().switchTo().frame(activityStreamFrame);
        Poller.waitUntil("Activity stream failed to load up.", timedQueryFactory.forSupplier(new Supplier<Boolean>()
        {
            @Override
            public Boolean get()
            {
                return jira.getTester().getDriver().findElement(By.id("activity-stream-show-more")).isDisplayed();
            }
        }, TimeoutType.COMPONENT_LOAD), is(true));
        jira.getTester().getDriver().switchTo().defaultContent();
        // ignore the 30 day summary graph
        addElementsToIgnore(By.cssSelector("#fragcreatedvsresolved img"));
        // Wait for the activity stream to load in...
        visualComparer.setWaitforJQueryTimeout(7000);
        assertUIMatches("browse-project-summary");
    }

    @Test
    public void testBrowseProjectIssues()
    {
        assertPageMatches("browse-project-issues", "/projects/BULK/issues");
    }

    @Test
    public void testBrowseProjectReports()
    {
        assertPageMatches("browse-project-reports", getBrowseProjectPageUrl("report-page"));
    }

    @Test
    public void testBrowseProjectVersions()
    {
        assertPageMatches("browse-project-versions", getBrowseProjectPageUrl("versions-page"));
    }

    @Test
    public void testBrowseProjectComponents()
    {
        assertPageMatches("browse-project-components", getBrowseProjectPageUrl("components-page"));
    }

    @Test
    public void testComponentSummaryViaBrowseProject()
    {
        assertPageMatches("component-summary-for-project", "/browse/BULK/component/10003", 5000);
    }

    @Test
    public void testVersionSummaryViaBrowseProject()
    {
        assertPageMatches("version-summary-for-project", "/browse/XSS/fixforversion/10023", 5000);
    }

    @Test
    public void testAdminViewUserProfile()
    {
        goTo("/secure/admin/user/ViewUser.jspa?name=admin");

        // Ignore login times.
        addElementsToIgnore(By.id("lastLogin"));
        addElementsToIgnore(By.id("previousLogin"));
        addElementsToIgnore(By.id("lastFailedLogin"));

        //and login count as we do not restore JIRA at each test
        addElementsToIgnore(By.id("loginCount"));

        assertUIMatches("admin-user-profile");
    }

    @Test
    public void testUserRolePermissions()
    {
        goTo("/secure/admin/user/ViewUser.jspa?name=admin");
        clickOnElement("#viewprojectroles_link");
        assertUIMatches("admin-user-permissions");
    }

    @Test
    public void testProfilePage()
    {
        goTo("/secure/ViewProfile.jspa");

        // wait for ActivityStream gadget to load up
        final WebElement activityStreamFrame = jira.getTester().getDriver().findElement(By.id("gadget-1"));
        jira.getTester().getDriver().switchTo().frame(activityStreamFrame);
        Poller.waitUntil("Activity stream failed to load up.", timedQueryFactory.forSupplier(new Supplier<Boolean>()
        {
            @Override
            public Boolean get()
            {
                return jira.getTester().getDriver().findElement(By.id("activity-stream-show-more")).isDisplayed();
            }
        }, TimeoutType.COMPONENT_LOAD), is(true));
        jira.getTester().getDriver().switchTo().defaultContent();

        visualComparer.setWaitforJQueryTimeout(7000);
        visualComparer.setRefreshAfterResize(false); //otherwise activity stream fails to load on time
        assertUIMatches("view-profile");
    }

    @Test
    public void testAuiLayoutPage()
    {
        assertPageMatches("aui-examples-page", "aui-examples/default-layout.jsp");
    }

    @Test
    public void test404MessagePage()
    {
        assertPageMatches("404-message-page", "foo");
    }

    @Ignore("The value of the test is minimal -- there's a lot of dynamic data on the page, plus it's static anyway and unlikely to change.")
    public void test500MessagePage()
    {
        // Trying to hit the displayError.jsp causes a 500 error
        assertPageMatches("500-message-page", "displayError.jsp");
    }

    @Test
    public void testDisplayErrorMessagePage()
    {
        assertPageMatches("display-error-message-page", "display-error");
    }

    @Test
    public void testSignupMessagePage()
    {
        assertPageMatches("signup-message-page", "/secure/Signup!default.jspa");
    }

    @Test
    public void testSignupPage()
    {
        jira.logout();
        assertPageMatches("signup-page", "/secure/Signup!default.jspa");
    }

    private String getBrowseProjectPageUrl(final String pageKey)
    {
        return getBrowseProjectPageUrl("BULK", pageKey);
    }

    private String getBrowseProjectPageUrl(String projectKey, String pageKey)
    {
        goTo("/browse/" + projectKey);
        return jira.getTester().getDriver().findElement(By.cssSelector("a[data-link-id=\"" + PROJECT_PLUGIN_PREFIX + pageKey + "\"]")).getAttribute("href");
    }
}

