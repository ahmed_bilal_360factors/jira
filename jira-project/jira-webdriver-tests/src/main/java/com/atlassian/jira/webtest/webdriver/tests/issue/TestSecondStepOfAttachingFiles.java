package com.atlassian.jira.webtest.webdriver.tests.issue;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.Nullable;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.config.ResetDataOnce;
import com.atlassian.jira.pageobjects.dialogs.AcknowledgeDialog;
import com.atlassian.jira.pageobjects.elements.AuiMessage;
import com.atlassian.jira.pageobjects.elements.GlobalFlags;
import com.atlassian.jira.pageobjects.elements.LinkElement;
import com.atlassian.jira.pageobjects.model.DefaultIssueActions;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.AttachFileDialog;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.AttachFilePage;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.MultiFileInput;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.uri.Uri;

import com.google.common.base.Function;
import com.google.common.base.Joiner;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.atlassian.jira.pageobjects.config.BlankInstance.PROJECT_KEY;
import static com.atlassian.jira.pageobjects.elements.AuiMessages.containsAnyText;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static java.lang.String.format;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Tests the second step of a two-step attachment upload.
 * <p/>
 * The first step is uploading a temporary attachment. The second step is promoting the temporary attachments to
 * permanent ones.
 *
 * @since v6.4
 */
@WebTest ({ Category.WEBDRIVER_TEST, Category.ATTACHMENTS })
@ResetDataOnce
public final class TestSecondStepOfAttachingFiles extends BaseJiraWebTest
{
    private static final String BROKEN_ATTACHMENT_STORE = "/dev/null";
    private static final int FILES_TO_UPLOAD = 3;
    private static IssueCreateResponse ISSUE;

    @BeforeClass
    public static void setUpIssue()
    {
        backdoor.attachments().enable();
        final String summary = TestSecondStepOfAttachingFiles.class.toString();
        ISSUE = backdoor.issues().createIssue(PROJECT_KEY, summary);
    }

    @Test
    public final void viaDialogShouldFailWhenAttachmentStoreIsBroken()
    {
        final AttachFileDialog dialog = uploadTemporaryFileViaDialog();
        breakAttachmentStore();

        dialog.submit();

        final List<String> failedTemporaryFileNames = getFileNames(dialog.getFileInput().getFiles());
        final List<AuiMessage> allMessages = pageBinder.bind(GlobalFlags.class).getMessages();
        assertErrorMessages(allMessages, failedTemporaryFileNames);
    }

    private AttachFileDialog uploadTemporaryFileViaDialog()
    {
        jira.goToViewIssue(ISSUE.key())
                .getIssueMenu()
                .invoke(DefaultIssueActions.ATTACH_FILES);
        final AttachFileDialog dialog = pageBinder.bind(AttachFileDialog.class);
        final MultiFileInput fileInput = dialog.getFileInput();
        putTestFiles(fileInput);
        return dialog;
    }

    private void breakAttachmentStore()
    {
        backdoor.attachments().setAttachmentPath(BROKEN_ATTACHMENT_STORE);
    }

    private void assertErrorMessages(
            final Iterable<AuiMessage> allMessages,
            final List<String> failedTemporaryFileNames)
    {
        final List<AuiMessage> errorMessages = filterErrorMessages(allMessages, failedTemporaryFileNames);
        assertThat("each attachment should emit error", errorMessages, hasSize(failedTemporaryFileNames.size()));
        for (final AuiMessage errorMessage : errorMessages)
        {
            assertErrorMessage(errorMessage);
        }
    }

    private List<AuiMessage> filterErrorMessages(
            final Iterable<AuiMessage> allMessages,
            final Iterable<String> temporaryFileNames)
    {
        return copyOf(filter(allMessages, containsAnyText(temporaryFileNames)));
    }

    private void assertErrorMessage(final AuiMessage errorMessage)
    {
        final String targetDirectory = Joiner.on(File.separatorChar)
                .join(BROKEN_ATTACHMENT_STORE, PROJECT_KEY, ISSUE.key());
        final String expectedMessage =
                format("Could not save attachment to storage: Unable to create target directory %s", targetDirectory);
        assertThat("message type should match", errorMessage.getType(), is(AuiMessage.Type.ERROR));
        assertThat("message body should match", errorMessage.getMessage().now(), containsString(expectedMessage));
    }

    @Test
    public final void viaFormShouldFailWhenAttachmentStoreIsBroken()
    {
        final AttachFilePage attachFilePage = uploadTemporaryFileViaForm();
        breakAttachmentStore();

        final AcknowledgeDialog acknowledgeDialog = attachFilePage.submit(AcknowledgeDialog.class);

        final Iterable<AuiMessage> allMessages = acknowledgeDialog.getMessages().getAllMessages().now();
        final List<File> failedTemporaryFiles = attachFilePage.getFileInput().getFiles();
        assertErrorMessages(allMessages, getFileNames(failedTemporaryFiles));
        assertAcknowledgeLink(acknowledgeDialog);
    }

    private AttachFilePage uploadTemporaryFileViaForm()
    {
        final Integer issueId = Integer.valueOf(ISSUE.id());
        final AttachFilePage attachFilePage = jira.goTo(AttachFilePage.class, issueId);
        final MultiFileInput fileInput = attachFilePage.getFileInput();
        putTestFiles(fileInput);
        return attachFilePage;
    }

    private void putTestFiles(final MultiFileInput fileInput)
    {
        for (int i = 0; i < FILES_TO_UPLOAD; i++)
        {
            final File file = createTestFile();
            fileInput.putFile(file);
        }
    }

    private List<String> getFileNames(final List<File> files)
    {
        return copyOf(
                transform(
                        files, new Function<File, String>()
                        {
                            @Override
                            public String apply(@Nullable final File file)
                            {
                                checkNotNull(file, "file");
                                return file.getName();
                            }
                        }
                )
        );
    }

    private File createTestFile()
    {
        try
        {
            final File file = File.createTempFile("attach-file-page-", ".txt");
            FileUtils.writeStringToFile(file, "Lorem ipsum");
            return file;
        }
        catch (final IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    private void assertAcknowledgeLink(final AcknowledgeDialog acknowledgeDialog)
    {
        final LinkElement acknowledgeLink = acknowledgeDialog.getAcknowledgeLink();
        final Uri destination = acknowledgeLink.getDestination();
        assertThat("acknowledge link should have proper label", acknowledgeLink.getText(), is("Acknowledge"));
        assertThat(
                "link should pass certain parameters",
                destination.getQueryParameters().keySet(),
                containsInAnyOrder("id", "atl_token")
        );
        assertThat("id parameter should match", destination.getQueryParameters("id"), is(contains(ISSUE.id())));
    }
}
