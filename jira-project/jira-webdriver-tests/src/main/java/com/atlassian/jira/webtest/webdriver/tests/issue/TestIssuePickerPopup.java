package com.atlassian.jira.webtest.webdriver.tests.issue;

import com.atlassian.jira.functest.framework.matchers.IterableMatchers;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.issue.IssueKey;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.IssuePickerPopup;
import com.atlassian.jira.pageobjects.components.fields.IssuePickerRowMatchers;
import com.atlassian.jira.pageobjects.model.DefaultIssueActions;
import com.atlassian.jira.pageobjects.pages.viewissue.ConvertIssuePage;
import com.atlassian.jira.pageobjects.pages.viewissue.MoveSubtaskChooseOperation;
import com.atlassian.jira.pageobjects.pages.viewissue.MoveSubtaskParentPage;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.pageobjects.elements.query.Poller;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.functest.framework.matchers.IterableMatchers.hasItemThat;
import static com.atlassian.jira.pageobjects.components.fields.IssuePickerRowMatchers.containsIssueKeys;
import static com.atlassian.jira.pageobjects.components.fields.IssuePickerRowMatchers.hasIssueKey;
import static junit.framework.Assert.assertEquals;

/**
 * Webdriver test for the 'Issue Picker' popup
 *
 * @since v5.2
 */
@WebTest ({ Category.WEBDRIVER_TEST, Category.ISSUES })
public class TestIssuePickerPopup extends BaseJiraWebTest
{
    public static final boolean FAVOURITE = true;

    private static final String SEARCH_REQUEST_NAME = "SearchRequest";
    private static final String DEFAULT_FILTER_ID = "-1";

    private IssuePickerPopup currentPicker;
    private ConvertIssuePage currentConvertIssuePage;
    private MoveSubtaskParentPage currentMoveSubtaskParentPage;

    @Before
    public void setUp()
    {
        backdoor.restoreBlankInstance();
        backdoor.subtask().enable();

        backdoor.filters().createFilter("key > hsp-2 or project = MKY", SEARCH_REQUEST_NAME, FAVOURITE);

        Issue.createIssues();

        //visit issue to create user issue history
        backdoor.getTestkit().userHistory().addIssue("admin", Issue.HSP1.key());
        backdoor.getTestkit().userHistory().addIssue("admin", Issue.HSP2.key());
        backdoor.getTestkit().userHistory().addIssue("admin", Issue.HSP3.key());
        backdoor.getTestkit().userHistory().addIssue("admin", Issue.HSP4.key());
        backdoor.getTestkit().userHistory().addIssue("admin", Issue.HSP5.key());
        backdoor.getTestkit().userHistory().addIssue("admin", Issue.MKY1.key());
    }

    @Test
    public void testConvertToSubtask()
    {
        currentConvertIssuePage = goToConvertToSubtask(Issue.HSP1);
        shouldSelectHsp2AsHsp1Parent();
    }

    @Test
    public void testConvertToSubtask2()
    {
        currentConvertIssuePage = goToConvertToSubtask(Issue.HSP2);
        shouldSelectHsp1AsHsp2Parent();
    }

    @Test
    public void testConvertToSubtaskViaSearchRequestMode()
    {
        currentConvertIssuePage = goToConvertToSubtask(Issue.HSP3);
        shouldSelectViaSearchRequestMode();
    }

    @Test
    public void testConvertIssueParentFromSearchRequest()
    {
        currentConvertIssuePage = goToConvertToSubtask(Issue.HSP1);
        shouldHandleSwitchingToSearchRequestAndBack();
    }

    @Test
    public void testConvertToSubtaskWithSwitchingWhenSetToDefault()
    {
        currentConvertIssuePage = goToConvertToSubtask(Issue.HSP1);
        shouldSwitchToRecentIssuesGivenFilterSelectSetToDefaultValue();
    }

    @Test
    public void testMoveIssueParent()
    {
        currentMoveSubtaskParentPage = goToMoveSubtaskParent(Issue.HSP5);
        shouldPickMoveIssueFromPicker();
    }


    @Test
    public void testMoveIssueParent2()
    {
        currentMoveSubtaskParentPage = goToMoveSubtaskParent(Issue.HSP5);
        shouldPickMoveIssueFromPicker2();
    }

    @Test
    public void testMoveIssueParentFromSearchRequest()
    {
        currentMoveSubtaskParentPage = goToMoveSubtaskParent(Issue.HSP5);
        shouldPickMoveIssueFromPickerFromSearchRequest();
    }

    @Test
    public void testMoveIssueParentAfterSwitching()
    {
        currentMoveSubtaskParentPage = goToMoveSubtaskParent(Issue.HSP5);
        shouldPickMoveIssueFromPickerAfterSwitching();
    }

    @Test
    public void testMoveIssueParentAfterSwitchingWhenSetToDefault()
    {
        currentMoveSubtaskParentPage = goToMoveSubtaskParent(Issue.HSP5);
        shouldPickMoveIssueFromPickerWhenSetToDefault();
    }

    private void shouldPickMoveIssueFromPickerWhenSetToDefault()
    {
        currentPicker = currentMoveSubtaskParentPage.openIssuePickerPopup();
        Poller.waitUntilTrue("Issue picker popup did not load.", currentPicker.isOpen());
        assertInRecentIssues();
        Poller.waitUntil("Issue list should contain \"HSP2\", \"HSP3\", \"HSP1\".",
                currentPicker.getRecentIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP2.key(), Issue.HSP3.key(), Issue.HSP1.key()))));

        currentPicker.triggerSearchModeFilterByText(SEARCH_REQUEST_NAME);
        Poller.waitUntilTrue(currentPicker.isInSearchModeFilter());
        Poller.waitUntil("Issue list should contain \"HSP3\".",
                currentPicker.getFilterIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP3.key()))));

        currentPicker.triggerSearchModeFilter(DEFAULT_FILTER_ID);
        assertInRecentIssues();
        Poller.waitUntil("Issue list should contain \"HSP2\", \"HSP3\", \"HSP1\".",
                currentPicker.getRecentIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP2.key(), Issue.HSP3.key(), Issue.HSP1.key()))));

        currentPicker.getRecentIssuesSection().getIssueRow(Issue.HSP2.key()).selectRow();

        // switch back to the convert issue window
        currentPicker.switchBack();

        Poller.waitUntil("\"HSP2\" expected to be selected on \"HSP5\" convert issue page",
                currentMoveSubtaskParentPage.legacyPicker().getValue(),
                Matchers.startsWith(Issue.HSP2.key()));
    }

    private void shouldPickMoveIssueFromPickerAfterSwitching()
    {
        currentPicker = currentMoveSubtaskParentPage.openIssuePickerPopup();
        Poller.waitUntilTrue("Issue picker popup did not load.", currentPicker.isOpen());
        assertInRecentIssues();
        Poller.waitUntil("Issue list should contain \"HSP2\", \"HSP3\", \"HSP4\".",
                currentPicker.getRecentIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP2.key(), Issue.HSP3.key(), Issue.HSP1.key()))));

        currentPicker.triggerSearchModeFilterByText(SEARCH_REQUEST_NAME);
        Poller.waitUntilTrue(currentPicker.isInSearchModeFilter());
        Poller.waitUntil("Issue list should contain \"HSP3\".",
                currentPicker.getFilterIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP3.key()))));

        currentPicker.triggerSearchModeRecent();
        assertInRecentIssues();
        Poller.waitUntil("Issue list should contain \"HSP2\", \"HSP3\", \"HSP4\".",
                currentPicker.getRecentIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP2.key(), Issue.HSP3.key(), Issue.HSP1.key()))));

        currentPicker.getRecentIssuesSection().getIssueRow(Issue.HSP2.key()).selectRow();

        // switch back to the convert issue window
        currentPicker.switchBack();

        Poller.waitUntil("\"HSP2\" expected to be selected on \"HSP5\" convert issue page",
                currentMoveSubtaskParentPage.legacyPicker().getValue(),
                Matchers.startsWith(Issue.HSP2.key()));
    }

    private void shouldPickMoveIssueFromPickerFromSearchRequest()
    {
        currentPicker = currentMoveSubtaskParentPage.openIssuePickerPopup();
        Poller.waitUntilTrue("Issue picker popup did not load.", currentPicker.isOpen());
        assertInRecentIssues();
        Poller.waitUntil("Issue list should contain \"HSP2\", \"HSP1\", \"HSP3\".",
                currentPicker.getRecentIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP2.key(), Issue.HSP1.key(), Issue.HSP3.key()))));

        currentPicker.triggerSearchModeFilterByText(SEARCH_REQUEST_NAME);
        Poller.waitUntilTrue(currentPicker.isInSearchModeFilter());
        Poller.waitUntil("Issue list should contain \"HSP3\".",
                currentPicker.getFilterIssuesSection().getIssueRows(),
                hasItemThat(hasIssueKey(Issue.HSP3.key())));

        currentPicker.getFilterIssuesSection().getIssueRow(Issue.HSP3.key()).selectRow();

        // switch back to the convert issue window
        currentPicker.switchBack();

        Poller.waitUntil("\"HSP3\" expected to be selected on \"HSP5\" move issue page",
                currentMoveSubtaskParentPage.legacyPicker().getValue(),
                Matchers.startsWith(Issue.HSP3.key()));
    }


    private void shouldPickMoveIssueFromPicker()
    {
        currentPicker = currentMoveSubtaskParentPage.openIssuePickerPopup();
        Poller.waitUntilTrue("Issue picker popup did not load.", currentPicker.isOpen());
        assertInRecentIssues();
        Poller.waitUntil("Issue list should contain \"HSP2\", \"HSP3\", \"HSP1\".",
                currentPicker.getRecentIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP2.key(), Issue.HSP3.key(), Issue.HSP1.key()))));

        currentPicker.getRecentIssuesSection().getIssueRow(Issue.HSP1.key()).selectRow();

        // switch back to the convert issue window
        currentPicker.switchBack();

        Poller.waitUntil("\"HSP1\" expected to be selected on \"HSP5\" move issue page",
                currentMoveSubtaskParentPage.legacyPicker().getValue(),
                Matchers.startsWith(Issue.HSP1.key()));
    }

    private void shouldPickMoveIssueFromPicker2()
    {
        currentPicker = currentMoveSubtaskParentPage.openIssuePickerPopup();
        Poller.waitUntilTrue("Issue picker popup did not load.", currentPicker.isOpen());
        assertInRecentIssues();
        Poller.waitUntil("Issue list should contain \"HSP2\", \"HSP3\", \"HSP1\".",
                currentPicker.getRecentIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP2.key(), Issue.HSP3.key(), Issue.HSP1.key()))));

        currentPicker.getRecentIssuesSection().getIssueRow(Issue.HSP3.key()).selectRow();

        // switch back to the convert issue window
        currentPicker.switchBack();

        Poller.waitUntil("\"HSP3\" expected to be selected on \"HSP5\" move issue page",
                currentMoveSubtaskParentPage.legacyPicker().getValue(),
                Matchers.startsWith(Issue.HSP3.key()));
    }

    private void shouldSwitchToRecentIssuesGivenFilterSelectSetToDefaultValue()
    {
        currentPicker = currentConvertIssuePage.openIssuePickerPopup();
        Poller.waitUntilTrue("Issue picker popup did not load.", currentPicker.isOpen());
        assertInRecentIssues();
        Poller.waitUntil("Issue list should contain \"HSP2\", \"HSP3\", \"HSP4\".",
                currentPicker.getRecentIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP2.key(), Issue.HSP3.key(), Issue.HSP4.key()))));

        currentPicker.triggerSearchModeFilterByText(SEARCH_REQUEST_NAME);
        Poller.waitUntilTrue(currentPicker.isInSearchModeFilter());
        Poller.waitUntil("Issue list should contain \"HSP3\", \"HSP4\".",
                currentPicker.getFilterIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP3.key(), Issue.HSP4.key()))));

        currentPicker.triggerSearchModeFilter(DEFAULT_FILTER_ID);
        assertInRecentIssues();
        Poller.waitUntil("Issue list should contain \"HSP2\", \"HSP3\", \"HSP4\".",
                currentPicker.getRecentIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP2.key(), Issue.HSP3.key(), Issue.HSP4.key()))));

        currentPicker.getRecentIssuesSection().getIssueRow(Issue.HSP3.key()).selectRow();

        // switch back to the convert issue window
        currentPicker.switchBack();

        Poller.waitUntil("\"HSP3\" expected to be selected on \"HSP1\" convert issue page",
                currentConvertIssuePage.legacyPicker().getValue(),
                Matchers.startsWith(Issue.HSP3.key()));
    }

    private void shouldHandleSwitchingToSearchRequestAndBack()
    {
        currentPicker = currentConvertIssuePage.openIssuePickerPopup();
        Poller.waitUntilTrue("Issue picker popup did not load.", currentPicker.isOpen());
        assertInRecentIssues();
        Poller.waitUntil("Issue list should contain \"HSP2\", \"HSP3\", \"HSP4\".",
                currentPicker.getRecentIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP2.key(), Issue.HSP3.key(), Issue.HSP4.key()))));

        currentPicker.triggerSearchModeFilterByText(SEARCH_REQUEST_NAME);
        Poller.waitUntilTrue(currentPicker.isInSearchModeFilter());
        Poller.waitUntil("Issue list should contain \"HSP3\", \"HSP4\".",
                currentPicker.getFilterIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP3.key(), Issue.HSP4.key()))));

        currentPicker.triggerSearchModeRecent();
        assertInRecentIssues();
        Poller.waitUntil("Issue list should contain \"HSP2\", \"HSP3\", \"HSP4\".",
                currentPicker.getRecentIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP2.key(), Issue.HSP3.key(), Issue.HSP4.key()))));

        currentPicker.getRecentIssuesSection().getIssueRow(Issue.HSP3.key()).selectRow();

        // switch back to the convert issue window
        currentPicker.switchBack();

        Poller.waitUntil("\"HSP3\" expected to be selected on \"HSP1\" convert issue page",
                currentConvertIssuePage.legacyPicker().getValue(),
                Matchers.startsWith(Issue.HSP3.key()));
    }

    private void shouldSelectHsp2AsHsp1Parent()
    {
        currentPicker = currentConvertIssuePage.openIssuePickerPopup();
        Poller.waitUntilTrue("Issue picker popup did not load.", currentPicker.isOpen());
        assertInRecentIssues();
        Poller.waitUntil("Issue list should contain \"HSP2\", \"HSP3\", \"HSP4\".",
                currentPicker.getRecentIssuesSection().getIssueRows(),
                hasItemThat(IssuePickerRowMatchers.containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP2.key(), Issue.HSP3.key(), Issue.HSP4.key()))));

        currentPicker.getRecentIssuesSection().getIssueRow(Issue.HSP2.key()).selectRow();

        // switch back to the convert issue window
        currentPicker.switchBack();

        Poller.waitUntil("\"HSP2\" expected to be selected on \"HSP1\" convert issue page",
                currentConvertIssuePage.legacyPicker().getValue(),
                Matchers.startsWith(Issue.HSP2.key()));
    }

    private void shouldSelectViaSearchRequestMode()
    {
        currentPicker = currentConvertIssuePage.openIssuePickerPopup();
        Poller.waitUntilTrue("Issue picker popup did not load.", currentPicker.isOpen());
        assertInRecentIssues();
        Poller.waitUntil("Issue list should contain \"HSP2\", \"HSP1\", \"HSP4\".",
                currentPicker.getRecentIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP2.key(), Issue.HSP1.key(), Issue.HSP4.key()))));

        currentPicker.triggerSearchModeFilterByText(SEARCH_REQUEST_NAME);
        Poller.waitUntilTrue(currentPicker.isInSearchModeFilter());
        Poller.waitUntil("Issue list should contain \"HSP4\".",
                currentPicker.getFilterIssuesSection().getIssueRows(),
                hasItemThat(hasIssueKey(Issue.HSP4.key())));

        currentPicker.getFilterIssuesSection().getIssueRow(Issue.HSP4.key()).selectRow();

        // switch back to the convert issue window
        currentPicker.switchBack();

        Poller.waitUntil("\"HSP4\" expected to be selected on \"HSP3\" convert issue page",
                currentConvertIssuePage.legacyPicker().getValue(),
                Matchers.startsWith(Issue.HSP4.key()));
    }

    private void shouldSelectHsp1AsHsp2Parent()
    {
        currentPicker = currentConvertIssuePage.openIssuePickerPopup();
        Poller.waitUntilTrue("Issue picker popup did not load.", currentPicker.isOpen());
        assertInRecentIssues();
        Poller.waitUntil("Issue list should contain \"HSP3\", \"HSP1\", \"HSP4\".",
                currentPicker.getRecentIssuesSection().getIssueRows(),
                hasItemThat(containsIssueKeys(expectedKeyBuilder(String.class, Issue.HSP3.key(), Issue.HSP1.key(), Issue.HSP4.key()))));

        currentPicker.getRecentIssuesSection().getIssueRow(Issue.HSP1.key()).selectRow();

        // switch back to the convert issue window
        currentPicker.switchBack();

        Poller.waitUntil("\"HSP1\" expected to be selected on \"HSP2\" convert issue page",
                currentConvertIssuePage.legacyPicker().getValue(),
                Matchers.startsWith(Issue.HSP1.key()));
    }

    private <T> Iterable<T> expectedKeyBuilder(final Class<T> targetClass, final Object... args)
    {
        final List<T> expectedKeys = new ArrayList<T>();
        for (final Object issueKey : args)
        {
            expectedKeys.add((T) issueKey);
        }
        return expectedKeys;
    }

    private void assertInRecentIssues()
    {
        Poller.waitUntilTrue(currentPicker.isInSearchModeRecent());
        Poller.waitUntil("Current Issues should be empty", currentPicker.getCurrentIssuesSection().getIssueRows(),
                IterableMatchers.emptyIterable(IssuePickerPopup.IssuePickerRow.class));
    }

    private ConvertIssuePage goToConvertToSubtask(final Issue issue)
    {
        final ViewIssuePage viewIssuePage = jira.goTo(ViewIssuePage.class, issue.key());
        return viewIssuePage.getIssueMenu().invoke(DefaultIssueActions.CONVERT_TO_SUBTASK, ConvertIssuePage.class, issue.id());
    }

    private MoveSubtaskParentPage goToMoveSubtaskParent(final Issue issue)
    {
        final ViewIssuePage viewIssuePage = jira.goTo(ViewIssuePage.class, issue.key());
        final MoveSubtaskChooseOperation moveSubtaskStep1 =
                viewIssuePage.getIssueMenu().invoke(DefaultIssueActions.MOVE, MoveSubtaskChooseOperation.class, issue.id());
        return moveSubtaskStep1.selectChangeParentRadio().next();
    }

    private static enum Issue
    {
        HSP1("HSP-1"),
        HSP2("HSP-2"),
        HSP3("HSP-3"),
        HSP4("HSP-4"),
        HSP5("HSP-5", HSP4),
        MKY1("MKY-1");

        private final String key;
        private final Issue parent;

        private Issue(final String key)
        {
            this(key, null);
        }

        private Issue(final String key, final Issue parent)
        {
            this.key = key;
            this.parent = parent;
        }

        public String key()
        {
            return key;
        }

        public String parent()
        {
            return parent == null ? null : parent.key();
        }

        public long id()
        {
            return Long.parseLong(backdoor.issues().getIssue(key).id);
        }

        private void createIssue()
        {
            //check in which project we should create issue based on issue key
            final String projectKey = IssueKey.from(key()).getProjectKey();
            final IssueCreateResponse issueResponse;
            if (parent() == null)
            {
                //create next issue in project
                issueResponse = backdoor.issues().createIssue(projectKey, "test");
            }
            else
            {
                //create next sub-task in project
                final Long projectId = backdoor.project().getProjectId(projectKey);
                issueResponse = backdoor.issues().createSubtask(projectId.toString(), parent(), "test");
            }
            assertEquals("Created issue does not have expected key", key(), issueResponse.key());
        }

        public static void createIssues()
        {
            for (final Issue issue : values())
            {
                issue.createIssue();
            }
        }
    }

}
