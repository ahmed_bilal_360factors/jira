package com.atlassian.jira.webtest.webdriver.tests.issue;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.matchers.IterableMatchers;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.components.fields.PageElementMatchers;
import com.atlassian.jira.pageobjects.dialogs.quickedit.CreateIssueDialog;
import com.atlassian.jira.pageobjects.elements.AuiFlag;
import com.atlassian.jira.pageobjects.elements.GlobalFlags;
import com.atlassian.jira.pageobjects.elements.GlobalMessage;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertNotNull;

@WebTest ({ Category.WEBDRIVER_TEST, Category.ISSUES })
@Restore ("xml/BlankInstanceWithTextCustomFields.xml")
public class TestCreateIssueWithTextCustomFieldExceedingCharacterLimit extends BaseJiraWebTest
{

    private static final String LONG_TEXT = "thisisvaerylonglinethatexceedsthelimit";
    public static final String SUMMARY = "summary";
    public static final String DESCRIPTION = "description";
    public static final String ENVIRONMENT = "environment";
    public static final String CUSTOMFIELD_10000 = "customfield_10000";
    public static final String CUSTOMFIELD_10001 = "customfield_10001";
    public static final String CUSTOMFIELD_10002 = "customfield_10002";
    public static final String DATA_FIELD_ATTRIBUTE = "data-field";
    public static final String SUMMARY_TEXT = "Some issue";

    @Test
    public void testCannotCreateIssueWithLongTextThatExceedsTheCharacterLimit()
    {
        jira.backdoor().advancedSettings().setTextFieldCharacterLengthLimit(LONG_TEXT.length() - 1);
        jira.gotoHomePage();

        final CreateIssueDialog createIssueDialog = pageBinder.bind(JiraHeader.class).createIssue();
        Poller.waitUntilTrue("CreateIssueDialog was not opened.", createIssueDialog.isOpen());

        fillOutCreateForm(createIssueDialog);

        createIssueDialog.submit(CreateIssueDialog.class, CreateIssueDialog.Type.ISSUE).waitForFormErrors();
        Poller.waitUntil("Expected inline error for text fields they exceed the character limit",
                createIssueDialog.getFormErrorElements(),
                IterableMatchers.hasItemsThat(PageElement.class, PageElementMatchers.containsAttribute(DATA_FIELD_ATTRIBUTE, DESCRIPTION),
                        PageElementMatchers.containsAttribute(DATA_FIELD_ATTRIBUTE, ENVIRONMENT),
                        PageElementMatchers.containsAttribute(DATA_FIELD_ATTRIBUTE, CUSTOMFIELD_10000),
                        PageElementMatchers.containsAttribute(DATA_FIELD_ATTRIBUTE, CUSTOMFIELD_10001),
                        PageElementMatchers.containsAttribute(DATA_FIELD_ATTRIBUTE, CUSTOMFIELD_10002)));
    }

    private void fillOutCreateForm(final CreateIssueDialog createIssueDialog)
    {
        createIssueDialog.fill(SUMMARY, SUMMARY_TEXT);
        createIssueDialog.fill(DESCRIPTION, LONG_TEXT);
        createIssueDialog.fill(ENVIRONMENT, LONG_TEXT);
        createIssueDialog.fill(CUSTOMFIELD_10000, LONG_TEXT);
        createIssueDialog.fill(CUSTOMFIELD_10001, LONG_TEXT);
        createIssueDialog.fill(CUSTOMFIELD_10002, "http://" + LONG_TEXT + ".com");
    }

    @Test
    public void testCanCreateIssueWithLongTextThatDoesNotExceedTheCharacterLimit()
    {
        jira.backdoor().advancedSettings().setTextFieldCharacterLengthLimit(32768);
        jira.gotoHomePage();

        final CreateIssueDialog createIssueDialog = pageBinder.bind(JiraHeader.class).createIssue();
        Poller.waitUntilTrue("CreateIssueDialog was not opened.", createIssueDialog.isOpen());

        fillOutCreateForm(createIssueDialog);

        final GlobalFlags flags = createIssueDialog.submit(GlobalFlags.class);
        Poller.waitUntilTrue("Flags loaded.", flags.flagContainerPresent());

        AuiFlag successMsg = flags.getFlagWithText("has been successfully created");
        assertNotNull("Success message should exist", successMsg);

        assertTrue("Expected message to be of type success", successMsg.getType() == AuiFlag.Type.SUCCESS);
        assertTrue("Expected success message to contain issue summary", successMsg.getMessage().contains("Some issue"));
        assertTrue("Expected success message to contain close button", successMsg.isCloseable());
    }

}

