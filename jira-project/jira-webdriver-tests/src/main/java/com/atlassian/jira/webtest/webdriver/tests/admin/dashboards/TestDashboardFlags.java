package com.atlassian.jira.webtest.webdriver.tests.admin.dashboards;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.elements.AuiFlag;
import com.atlassian.jira.pageobjects.elements.GlobalFlags;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;


@WebTest ({ Category.WEBDRIVER_TEST, Category.DASHBOARDS })
public class TestDashboardFlags extends BaseJiraWebTest
{
    @Before
    public void setUp() throws Exception
    {
        backdoor.restoreBlankInstance();
    }

    @Test
    public void emptyDashboardShowsInfoFlags()
    {
        jira.gotoHomePage();
        GlobalFlags flags = pageBinder.bind(GlobalFlags.class);

        //empty system dashboard should show a flag!
        Poller.waitUntilTrue(flags.flagContainerPresent());
        AuiFlag emptyDashboardFlag = flags.getFlagWithText("This dashboard does not contain any gadgets");
        assertNotNull(emptyDashboardFlag);
        assertEquals(AuiFlag.Type.INFO, emptyDashboardFlag.getType());
        assertEquals("This dashboard does not contain any gadgets or you do not have permission to view them. If you think this is incorrect, please contact your JIRA administrators.",
                emptyDashboardFlag.getMessage());

        emptyDashboardFlag.dismiss();
        assertThat(flags.doesNotContainFlagWithText("This dashboard does not contain any gadgets"), is(true));
    }

    @Test
    public void dashboardShowsInstallationSuccessFlags()
    {
        jira.getTester().gotoUrl(jira.getProductInstance().getBaseUrl() + "/secure/Dashboard.jspa?src=SetupImport");
        GlobalFlags flags = pageBinder.bind(GlobalFlags.class);

        //When src is set there should be a success flat
        Poller.waitUntilTrue(flags.flagContainerPresent());
        AuiFlag importFlag = flags.getFlagWithText("finished importing your existing data");
        assertNotNull(importFlag);
        assertEquals(AuiFlag.Type.SUCCESS, importFlag.getType());
        assertEquals("You have finished importing your existing data, JIRA is ready to use. Please log in and get started.",
                importFlag.getMessage());
    }
}
