package com.atlassian.jira.webtest.webdriver.tests.visualregression;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.pages.admin.workflow.WorkflowsPage;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Dimension;

/**
 *
 * @since v6.4
 */
@WebTest ( { Category.WEBDRIVER_TEST, Category.VISUAL_REGRESSION })
public class TestVisualRegressionWorkflowDesigner extends JiraVisualRegressionTest
{
    private final Dimension DEFAULT_DIMENSION = new Dimension(1024, 768);

    @BeforeClass
    public static void restoreInstance(){
        //we cannot restore instance via @Restore annotation as it doesn't support restoring license from backup file
        backdoor.dataImport().restoreDataFromResource("xml/TestVisualRegressionWorkflowDesigner.zip", "");
    }

    @Test
    public void testWorkflowDesigner()
    {
        resizeWindowToDefaultDimentions();
        jira.goTo(WorkflowsPage.class).openDesigner("Copy of Copy of jira");
        visualComparer.setWaitforJQueryTimeout(0);
        visualComparer.setRefreshAfterResize(false);
        assertUIMatches("workflow-designer");
    }
}
