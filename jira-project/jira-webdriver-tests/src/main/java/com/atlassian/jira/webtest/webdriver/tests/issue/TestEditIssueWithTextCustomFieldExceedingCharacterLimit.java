package com.atlassian.jira.webtest.webdriver.tests.issue;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.components.fields.PageElementMatchers;
import com.atlassian.jira.pageobjects.dialogs.quickedit.EditIssueDialog;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.common.collect.Lists;
import org.hamcrest.Matcher;
import org.junit.Test;

import java.util.Collection;

import static com.atlassian.jira.functest.framework.matchers.IterableMatchers.hasItemsThat;

@WebTest ({ Category.WEBDRIVER_TEST, Category.ISSUES })
@Restore ("xml/BlankInstanceWithTextCustomFields.xml")
public class TestEditIssueWithTextCustomFieldExceedingCharacterLimit extends BaseJiraWebTest
{

    public static final String DESCRIPTION = "description";
    public static final String ENVIRONMENT = "environment";
    public static final String TEXTAREA_FIELD = "customfield_10000";
    public static final String TEXT_FIELD = "customfield_10001";
    public static final String URL_FIELD = "customfield_10002";
    public static final String DATA_FIELD_ATTRIBUTE = "data-field";

    @Test
    public void testInlineEditCannotSaveOnFieldExceedingLimit()
    {
        ViewIssuePage viewIssuePage = jira.goToViewIssue("HSP-2");
        EditIssueDialog editIssueDialog = viewIssuePage.editIssue();

        Poller.waitUntilTrue("EditIssueDialog was not opened.", editIssueDialog.isOpen());

        // limit is off - all fields are ok to edit
        setCharacterLimitToUnlimitedValue();
        viewIssuePage = editIssueDialog.submit(ViewIssuePage.class, "HSP-2");
        editIssueDialog = viewIssuePage.editIssue();

        // now gradually lower the limit value and check that increasing number of fields becomes too long
        setCharacterLimitToOneLessThanFieldValue(TEXTAREA_FIELD, editIssueDialog);
        submitAndWaitForFieldErrors(editIssueDialog, TEXTAREA_FIELD);

        setCharacterLimitToOneLessThanFieldValue(TEXT_FIELD, editIssueDialog);
        submitAndWaitForFieldErrors(editIssueDialog, TEXTAREA_FIELD, TEXT_FIELD);

        setCharacterLimitToOneLessThanFieldValue(DESCRIPTION, editIssueDialog);
        submitAndWaitForFieldErrors(editIssueDialog, TEXTAREA_FIELD, TEXT_FIELD, DESCRIPTION);

        setCharacterLimitToOneLessThanFieldValue(URL_FIELD, editIssueDialog);
        submitAndWaitForFieldErrors(editIssueDialog, TEXTAREA_FIELD, TEXT_FIELD, DESCRIPTION, URL_FIELD);

        setCharacterLimitToOneLessThanFieldValue(ENVIRONMENT, editIssueDialog);
        submitAndWaitForFieldErrors(editIssueDialog, TEXTAREA_FIELD, TEXT_FIELD, DESCRIPTION, URL_FIELD, ENVIRONMENT);

        setCharacterLimitToUnlimitedValue();
        // limit is off again - all fields are ok to edit
        editIssueDialog.submit(ViewIssuePage.class, "HSP-2");
    }

    private void setCharacterLimitToUnlimitedValue()
    {
        jira.backdoor().advancedSettings().setTextFieldCharacterLengthLimit(0);
    }

    private void submitAndWaitForFieldErrors(final EditIssueDialog editIssueDialog, String... fields)
    {
        editIssueDialog.submit(EditIssueDialog.class).waitForFormErrors();
        Collection<Matcher<PageElement>> matchers = Lists.newArrayListWithCapacity(fields.length);
        for (final String fieldId : fields)
        {
            matchers.add(PageElementMatchers.containsAttribute(DATA_FIELD_ATTRIBUTE, fieldId));
        }
        Poller.waitUntil("Expected inline error for text fields they exceed the character limit",
                editIssueDialog.getFormErrorElements(),
                hasItemsThat(PageElement.class, matchers.toArray(new Matcher[0])));
    }

    private void setCharacterLimitToOneLessThanFieldValue(final String fieldId, final EditIssueDialog editIssueDialog)
    {
        final String fieldValue = editIssueDialog.getFieldValue(fieldId);
        jira.backdoor().advancedSettings().setTextFieldCharacterLengthLimit(fieldValue.length() - 1);
    }


}

