package com.atlassian.jira.webtest.webdriver.tests.project;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.pages.project.BrowseProjectsPage;
import com.atlassian.jira.rest.v2.issue.project.ProjectCategoryBean;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static junit.framework.Assert.assertFalse;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @since v6.4
 */
@WebTest ({ Category.WEBDRIVER_TEST, Category.PROJECTS })
public class TestBrowseProjects extends BaseJiraWebTest
{
    public static final String PROJECT_NAME = "project";
    public static final String PROJECT_KEY = "PRJ";
    public static final String PROJECT_LEAD = "admin";
    public static final String PROJECT_CATEGORY_NAME = "category";
    public static final String PROJECT_CATEGORY_DESCRIPTION = "Pretty long project category description.";
    private static List<Project> projects;
    private static List<ProjectCategoryBean> projectCategories;

    @BeforeClass
    public static void setUp()
    {
        backdoor.restoreBlankInstance();
        // remove projects created when restoring blank instance
        backdoor.project().deleteProject("HSP");
        backdoor.project().deleteProject("MKY");

        int numberOfCategories = 2;

        projectCategories = Lists.newArrayListWithExpectedSize(numberOfCategories);

        for (int i = 1; i <= numberOfCategories; i++)
        {
            projectCategories.add(backdoor.project().addProjectCategory(PROJECT_CATEGORY_NAME + " " + i, PROJECT_CATEGORY_DESCRIPTION));
        }

        int numberOfProjects = 50;

        for (int i = 1; i <= numberOfProjects; i++)
        {
            String key = PROJECT_KEY;
            if (i > 26)
            {
                key += String.valueOf((char) (38 + i)) + String.valueOf((char) (38 + i));
            }
            else
            {
                key += String.valueOf((char) (64 + i));
            }
            long projectId = backdoor.project().addProject(
                    PROJECT_NAME + " " + i,
                    key,
                    PROJECT_LEAD
            );
            //add first 10 projects to first category
            if (i <= 10)
            {
                backdoor.project().setProjectCategory(projectId, Long.parseLong(projectCategories.get(0).getId()));
            }
        }

        projects = backdoor.project().getProjects();
    }

    @AfterClass
    public static void tearDown()
    {
        for (ProjectCategoryBean category : projectCategories)
        {
            backdoor.project().deleteProjectCategory(category.getId());
        }

        for (Project project : projects)
        {
            backdoor.project().deleteProject(project.key);
        }
    }

    @Test
    public void shouldFilterWhenChangingCriteria()
    {
        //make sure we have a project in recent category
        BrowseProjectsPage browseProjectsPage = jira.goTo(BrowseProjectsPage.class);

        browseProjectsPage = browseProjectsPage.filterByCategory(BrowseProjectsPage.NO_CATEGORY);

        waitUntilEquals("2", browseProjectsPage.getNumberOfPages());

        browseProjectsPage = browseProjectsPage.filterByCategory(projectCategories.get(0).getId());

        waitUntil(browseProjectsPage.getProjectAt(1).getName(), Matchers.equalToIgnoringCase("Project 1"));
        waitUntil(browseProjectsPage.getProjectAt(10).getName(), Matchers.equalToIgnoringCase("Project 9"));

        browseProjectsPage = browseProjectsPage.filterByText("Project 1");
        waitUntil(browseProjectsPage.getProjectAt(1).getName(), Matchers.equalToIgnoringCase("Project 1"));
        waitUntil(browseProjectsPage.getProjectAt(2).getName(), Matchers.equalToIgnoringCase("Project 10"));

        browseProjectsPage = browseProjectsPage.filterByCategory(BrowseProjectsPage.ALL);
        waitUntil(browseProjectsPage.getProjectAt(1).getName(), Matchers.equalToIgnoringCase("Project 1"));
        waitUntil(browseProjectsPage.getProjectAt(11).getName(), Matchers.equalToIgnoringCase("Project 19"));
    }


    @Test
    public void shouldUpdateUrlWhenFilteringAndNavigating()
    {
        BrowseProjectsPage browseProjectsPage = jira.goTo(BrowseProjectsPage.class);

        Map<String, String> params = BrowseProjectsPage.getFilterParamsFromUrl(jira.getTester().getDriver().getCurrentUrl());
        assertFalse(params.containsKey("selectedCategory"));
        assertFalse(params.containsKey("contains"));
        assertFalse(params.containsKey("page"));

        browseProjectsPage = browseProjectsPage.filterByCategory(BrowseProjectsPage.ALL).goToNextPage();

        params = BrowseProjectsPage.getFilterParamsFromUrl(jira.getTester().getDriver().getCurrentUrl());
        assertThat(params.get("selectedCategory"), is(BrowseProjectsPage.ALL));
        assertThat(params.get("page"), is("2"));
        assertFalse(params.containsKey("contains"));

        browseProjectsPage = browseProjectsPage.filterByText("Project");
        params = BrowseProjectsPage.getFilterParamsFromUrl(jira.getTester().getDriver().getCurrentUrl());
        assertThat(params.get("selectedCategory"), is(BrowseProjectsPage.ALL));
        assertThat(params.get("contains"), is("Project"));
        assertFalse(params.containsKey("page"));

    }

    @Test
    public void shouldFilterProjectsAccordingToUrlParameters()
    {
        BrowseProjectsPage filteredPage = jira.goTo(BrowseProjectsPage.class, BrowseProjectsPage.ALL, "project 5", 1);

        waitUntil(filteredPage.getProjectAt(1).getName(), Matchers.equalToIgnoringCase("Project 5"));
        waitUntil(filteredPage.getProjectAt(2).getName(), Matchers.equalToIgnoringCase("Project 50"));
    }
}