package com.atlassian.jira.webtest.webdriver.tests.postsetup;

import com.atlassian.jira.functest.framework.backdoor.BackdoorControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.JiraWebDriverTest;
import com.atlassian.jira.pageobjects.pages.admin.user.UserBrowserPage;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.webtest.webdriver.util.AUIDialogs;
import com.atlassian.jira.webtest.webdriver.util.AUIFlags;
import com.atlassian.webdriver.utils.element.ElementLocated;
import com.atlassian.webdriver.utils.element.ElementNotLocated;
import com.atlassian.webdriver.utils.element.WebDriverPoller;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.FluentWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @since v6.4
 */
@WebTest ({ Category.WEBDRIVER_TEST, Category.ADMINISTRATION})
public class TestPostSetupAnnouncement extends BaseJiraWebTest
{

    private static final String POST_SETUP_DATABASE_ID = "inline-dialog-post-setup-announcement-database-setup";
    private static final String POST_SETUP_ADMIN_ACCOUNT = "inline-dialog-post-setup-announcement-admin-account-setup";
    private static final String POST_SETUP_USERMAIL_ID = "inline-dialog-post-setup-announcement-create-user-mail-properties-setup";

    public static final int WAIT_FOR_ELEMENT_TIMEOUT = 1;
// replace default license with (old evaluation license):
//        {
//            "jira.active": "true",
//                "jira.ContactEMail": "charlie@atlassian.com",
//                "jira.ContactName": "Charlie",
//                "jira.CreationDate": "2000-01-01",
//                "jira.Evaluation": "true",
//                "jira.LicenseEdition": "ENTERPRISE",
//                "jira.LicenseExpiryDate": "P5H",
//                "jira.LicenseTypeName": "COMMERCIAL",
//                "jira.MaintenanceExpiryDate": "P5H",
//                "jira.NumberOfUsers": "3",
//                "jira.Organisation": "Atlassian",
//                "jira.PartnerName": "",
//                "jira.PurchaseDate": "2000-01-01",
//                "licenseVersion": "2",
//                "ServerID": "BG9T-XUV4-KZZH-B01W"
//        }
    public static final String TIMEBOMBED_EVALUATION_LICENSE = "AAABDA0ODAoPeNptUMtugkAU3fMVJF1jBrQLTCap4qSSyiOK2ri7xdt2GhjMnYHUvy8ydkFiMqs59\n" +
            "zyfjnh206Zz/dANpnMWzgPfjUThBsyfOT+SYLKRJSqN4iyNbBQXaSG2+TbeCQtHhHADVmCQB4wxj\n" +
            "/n9s2AOZBRSCjXykVpxveDwG2VJIrZRvNjc5RploDQiAVnx8huokvgCpgKtJahJ2dT2rr+RHXJDL\n" +
            "TqV1Twg6VvCwNkhdUjxii9fw8J73x9m3tvptPaWzD9aetrWH0jZ5173JD4dedtc1toCGX2Bknroy\n" +
            "Rf/Ycbz/F4kXYcR8uf1vX1LfQOND6fpCyqDClT5kCs6qFprOHT8A706kEwwLAIUMfsr0eYevzAQe\n" +
            "8AIiwHj3QC45KgCFGP11Q1YDWkornFXHmgW1xUXEIOUX02dl";

    private PostSetupAnnouncementsBackdoor postSetupAnnouncementsBackdoor;
    private AUIFlags auiFlags;
    private AUIDialogs auiDialogs;

    @Before
    public void onSetUp()
    {
        JiraWebDriverTest.backdoor.restoreBlankInstance();
        postSetupAnnouncementsBackdoor = new PostSetupAnnouncementsBackdoor(JiraWebDriverTest.jira.environmentData());
        auiFlags = new AUIFlags(JiraWebDriverTest.jira);
        auiDialogs = new AUIDialogs(JiraWebDriverTest.jira);
    }

    @Test
    public void shouldNotShowDatabasePostSetupMessageWhenNonEvaluationLicenseIsInstalled()
    {
        postSetupAnnouncementsBackdoor.reset();
        // jira is restore with default license
        JiraWebDriverTest.jira.gotoHomePage();

        auiFlags.closeAllFlags();
        assertNotContainsDatabasePopup(JiraWebDriverTest.jira);
    }


    @Test
    public void shouldShowDatabasePostSetupMessageWhenNonEvaluationLicenseIsInstalled()
    {
        JiraWebDriverTest.backdoor.license().replace(TIMEBOMBED_EVALUATION_LICENSE);

        postSetupAnnouncementsBackdoor.reset();
        JiraWebDriverTest.jira.gotoHomePage();

        auiFlags.closeAllFlags();
        assertContainsDatabasePopup(JiraWebDriverTest.jira);

        waitUntilPropertyNotOnAnnounceList(PostSetupAnnouncementsBackdoor.DATABASE_SETUP_PROPERTY);
        // on next visit popup should be gone
        JiraWebDriverTest.jira.goToAdminHomePage();

        auiFlags.closeAllFlags();
        assertNotContainsDatabasePopup(JiraWebDriverTest.jira);
    }

    @Test
    public void shouldShowEmailInformationOnAddUserPage()
    {
        // checks if postAnnoucenemtnSetup waits properly to dialog disposal
        UserBrowserPage page = jira.goTo(UserBrowserPage.class);

        auiDialogs.closeAll();
        assertContainsUserMailPopup(JiraWebDriverTest.jira);
    }

    private void waitUntilPropertyNotOnAnnounceList(final String propertyName)
    {
        new FluentWait<String>(propertyName)
                .withTimeout(3, TimeUnit.SECONDS)
                .withMessage("Property " + propertyName + " still awaits to be announced and marked as such!")
                .until(
                        new Function<String, Boolean>()
                        {
                            @Override
                            public Boolean apply(final String property)
                            {
                                final List<PostSetupAnnouncementsBackdoor.ReadyAnnouncement> readyAnnouncements =
                                        postSetupAnnouncementsBackdoor.readyAnnouncements();
                                final boolean propertyOnAnnounceList = Iterables.any(readyAnnouncements, new Predicate<PostSetupAnnouncementsBackdoor.ReadyAnnouncement>()
                                {
                                    @Override
                                    public boolean apply(final PostSetupAnnouncementsBackdoor.ReadyAnnouncement input)
                                    {
                                        return input.getActivityId().equals(property);
                                    }
                                });
                                return !propertyOnAnnounceList;
                            }
                        });
    }

    public static void assertContainsDatabasePopup(JiraTestedProduct jiraTestedProduct)
    {
        WebDriverPoller poller = new WebDriverPoller(jiraTestedProduct.getTester().getDriver());
        poller.waitUntil(new ElementLocated(By.id(POST_SETUP_DATABASE_ID)), WAIT_FOR_ELEMENT_TIMEOUT);
    }

    public static void assertContainsUserMailPopup(JiraTestedProduct jiraTestedProduct)
    {
        WebDriverPoller poller = new WebDriverPoller(jiraTestedProduct.getTester().getDriver());
        poller.waitUntil(new ElementLocated(By.id(POST_SETUP_USERMAIL_ID)), WAIT_FOR_ELEMENT_TIMEOUT);
    }

    public static void assertNotContainsDatabasePopup(JiraTestedProduct jiraTestedProduct)
    {
        WebDriverPoller poller = new WebDriverPoller(jiraTestedProduct.getTester().getDriver());
        poller.waitUntil(new ElementNotLocated(By.id(POST_SETUP_DATABASE_ID)), WAIT_FOR_ELEMENT_TIMEOUT);
    }

    public static void assertContainsAdminAccountPostSetupPopup(JiraTestedProduct jiraTestedProduct)
    {
        WebDriverPoller poller = new WebDriverPoller(jiraTestedProduct.getTester().getDriver());
        poller.waitUntil(new ElementLocated(By.id(POST_SETUP_ADMIN_ACCOUNT)), WAIT_FOR_ELEMENT_TIMEOUT);
    }

    private static class PostSetupAnnouncementsBackdoor extends BackdoorControl<PostSetupAnnouncementsBackdoor>
    {

        private static final GenericType<List<ReadyAnnouncement>> ANNOUNCEMENT_LIST_TYPE = new
                GenericType<List<ReadyAnnouncement>>()
                {
                };
        public static final String DATABASE_SETUP_PROPERTY =
                "database.setup";

        public PostSetupAnnouncementsBackdoor(final JIRAEnvironmentData environmentData)
        {
            super(environmentData);
        }

        public void reset()
        {
            createAnnoncementsResource().path("reset").get(String.class);
        }

        public List<ReadyAnnouncement> readyAnnouncements()
        {
            final List<ReadyAnnouncement> ready = createAnnoncementsResource().path("ready").get(ANNOUNCEMENT_LIST_TYPE);
            return ready;
        }

        private WebResource createAnnoncementsResource()
        {

            return resourceRoot(rootPath)
                    .path("rest")
                    .path("onboarding")
                    .path("1")
                    .path("postsetup_announcement");
        }


        @JsonAutoDetect
        public static class ReadyAnnouncement
        {
            private String activityId;
            private String status;

            public String getActivityId()
            {
                return activityId;
            }

            public void setActivityId(final String activityId)
            {
                this.activityId = activityId;
            }

            public String getStatus()
            {
                return status;
            }

            public void setStatus(final String status)
            {
                this.status = status;
            }
        }
    }
}
