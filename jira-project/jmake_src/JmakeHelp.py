import argparse
from argparse import _SubParsersAction
import html
from CommandExecutor import Callable
from Logger import Logger
from module import JmakeModule
from utils.ConfluenceUtils import ConfluenceUtils
from utils.UrlUtils import UrlUtils


class HelpDecorator:
    def decorate_title(self, title:str):
        pass

    def decorate_section(self, section:str):
        pass


class ConsoleHelpDecorator(HelpDecorator):

    def decorate_title(self, title:str):
        return " => " + title + '\n'

    def decorate_section(self, section:str):
        return section + '\n'


class HtmlHelpDecorator(HelpDecorator):

    def decorate_section(self, section:str):
        return self.__preformatted(html.escape(section))

    def decorate_title(self, title:str):
        return self.__header(self.__bold(html.escape(title)))

    def __tag(self, text, tag_name):
        return '<{1}>{0}</{1}>'.format(text, tag_name)

    def __bold(self, text):
        return self.__tag(text, 'b')

    def __preformatted(self, text):
        return self.__tag(text, 'pre')

    def __header(self, text):
        return self.__tag(text, 'h2')


decorators = {
    'console': ConsoleHelpDecorator,
    'html': HtmlHelpDecorator
}


class UploadHelpAsExtranetPage(Callable):
    def __init__(self, args, text: str, url: UrlUtils=UrlUtils(), confluence: ConfluenceUtils=None):
        super().__init__(args)
        self.text = text
        self.url = url
        self.confluence = confluence if confluence else ConfluenceUtils.extranet(args.extranet_password, url)

    def __call__(self, log: Logger):

        handbook_page = self.confluence.get_page_by_title(title="JIRA Development Handbook")
        if handbook_page is None:
            return self.failure

        jmake_docs_page = self.confluence.get_page_by_title(title="jmake help dump")
        if jmake_docs_page is None:
            return self.failure

        jmake_docs_page.set_contents(self.text).with_parent_pages([handbook_page.get_page_id()])
        jmake_docs_page.save()
        return self.success


class Help(JmakeModule):
    def __init__(self, parser):
        super().__init__()
        self.command = 'help'
        self.description = 'Prints the main help'
        self.parser = parser
        self.prevent_post_commands = True
        self.check_branch = False

    def __call__(self, args, executor):
        if args.full:
            decorator = decorators[args.decorator]()
            buffer = ''

            for title, subparser in self.__subparser_walker('jmake', self.parser):
                buffer = buffer + decorator.decorate_title(title) + decorator.decorate_section(subparser.format_help())
            if args.extranet_password:
                executor.append(UploadHelpAsExtranetPage(args, buffer))
            else:
                executor.append(lambda logger: print(buffer) or 0)
        else:
            executor.append(lambda logger: self.parser.print_help() or 0)

    def __subparser_walker(self, command: str, parser: argparse.ArgumentParser):
        for action in parser._get_positional_actions():
            if isinstance(action, _SubParsersAction):
                for (keyword, subparser) in action.choices.items():
                    subcommand = command + ' ' + keyword
                    yield subcommand, subparser
                    for s, p in self.__subparser_walker(subcommand, subparser):
                        yield s, p

    def define_parser(self, parser: argparse.ArgumentParser):
        parser.add_argument('--full', action='store_true', help='Prints full help for all options.')
        parser.add_argument('--decorator', choices=sorted(decorators.keys()), default='console',
                            help='When full help is printed, use a decorator for the output. Default is "console".')
        parser.add_argument('--extranet-upload-password', dest='extranet_password',
                            help='Uploads the result to a confluence jmake help dump page as the'
                                 'jira-doc-bot user. Give the password as a parameter.')

