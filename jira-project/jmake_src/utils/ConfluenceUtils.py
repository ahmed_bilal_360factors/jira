from json import JSONDecoder, JSONEncoder
from Logger import LOG
from utils.UrlUtils import UrlUtils


class ConfluencePage:

    def __init__(self, utils, page_json):
        super().__init__()
        self.utils = utils
        self.page_json = page_json
        self.__increment_version()
        self.page_id = page_json['id']
        self.dirty = False

    def save(self):
        self.utils.save(self)

    def with_parent_pages(self, parent_pages_ids):
        """
        Calling this will not make a page "dirty", because confluence returns the page in a way it cannot
        consume it again wrt ancestors and page lands at the space root.
        """
        self.page_json["ancestors"] = [{"type": "page", "id": str(page_id)} for page_id in parent_pages_ids]
        return self

    def get_contents(self):
        return self.page_json['body']['storage']['value']

    def set_contents(self, contents):
        current_contents = self.__escape_confluence_specific_tokens(self.page_json['body']['storage']['value'])
        new_contents = "<p />%s<p />" % contents

        if current_contents != new_contents:
            self.page_json['body']['storage']['value'] = new_contents
            self.dirty = True
        return self

    def __increment_version(self):
        currentVersion = int(self.page_json['version']['number'])
        self.page_json['version']['number'] = str(currentVersion + 1)

    def get_page_id(self):
        return self.page_id

    def __escape_confluence_specific_tokens(self, contents:str):
        """
        Confluence failed to do two things for me:
        1. not send the notification when there edit resulted in no changes (same content provided for the
           second time)
        2. process a request with a >'< character in it, but happily gives such output.

        So this manually tries to correct the little things that Confluence output misses.
        """

        replace_map = {
            "'": "&#x27;"
        }
        return ''.join(replace_map[c] if c in replace_map else c for c in contents)


class ConfluenceUtils:

    @staticmethod
    def extranet(extranet_password, url: UrlUtils=UrlUtils()):
        return ConfluenceUtils("https://extranet.atlassian.com", 'jira-doc-bot', extranet_password, url)

    def __init__(self, baseurl: str, user: str, password: str, url: UrlUtils=UrlUtils()):
        self.url = url
        self.user = user
        self.password = password
        self.baseurl = baseurl
        self.json_decoder = JSONDecoder()

    def __copy_property(self, dest:dict, source:dict, *properties):

        prop = properties[0]

        if len(properties) == 1:
            dest[prop] = source[prop]
        else:
            if not prop in dest:
                dest[prop] = {}
            self.__copy_property(dest[prop], source[prop], *properties[1:])

    def __raw_get(self, rest_url):
        LOG.debug('Confluence: GET ' + rest_url)
        return self.url.get(rest_url, self.user, self.password)

    def __raw_put(self, rest_url, data):
        LOG.debug('Confluence: PUT ' + rest_url)
        self.url.put(rest_url, self.user, self.password, data, {
            "Content-Type": "application/json",
            "Accept": "application/json"
        })

    def get_page_by_title(self, title: str):
        search_url = '%s/rest/api/content?title=%s' % (self.baseurl, self.url.quote(title))
        response = self.__raw_get(search_url)
        # take the first result:
        results = self.json_decoder.decode(response)['results']
        if len(results) == 0:
            raise Exception('No such page: title=' + title)
        elif len(results) > 1:
            raise Exception('Ambiguous page title!')
        else:
            return self.__get_page_by_id(page_id=results[0]['id'])

    def __get_page_by_id(self, page_id):
        rest_url = '%s/rest/api/content/%s?expand=body.storage,version' % (self.baseurl, str(page_id))
        response = self.__raw_get(rest_url)
        page = ConfluencePage(self, self.json_decoder.decode(response))
        return page

    def save(self, page: ConfluencePage):

        if not page.dirty:
            LOG.info("Confluence page not changed, aborting store")
            return

        entity = {}
        page_json = page.page_json

        # copy crucial elements:
        for path in ['body.storage.value', 'body.storage.representation', 'version.number',
                     'type', 'title', 'ancestors']:
            self.__copy_property(entity, page_json, *(path.split('.')))

        data = JSONEncoder().encode(entity)
        self.__raw_put("%s/rest/api/content/%s" % (self.baseurl, page.get_page_id()), data)

