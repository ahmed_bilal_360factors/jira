from functools import partial
import os
import random
import subprocess
import signal
import time
import sys
import atexit

from Logger import Logger
from utils.FileUtils import FileUtils
from utils.ProcessUtils import ProcessUtils


class Callable:
    success = 0
    failure = 42
    do_not_proceed = 678

    def __init__(self, args):
        super().__init__()
        self.args = args

    def __call__(self, logger):
        return Callable.success


class SystemCallable(Callable):
    def __init__(self, args, command=None, cwd=None):
        super().__init__(args)
        self.__command = command
        self.__cwd = cwd
        self.with_logged_output = os.getenv('JMAKE_TRACE') is not None
        self.run_in_background = False
        self.log_file = None

    @staticmethod
    def decode_utf_8(binary_line, logger):
        
        try:
            return binary_line.decode('utf-8')
        except Exception as ex:
            logger.error("Cannot decode line " + str(binary_line))
            return ""

    def __call__(self, logger):
        command = self.__command
        env = self.env if hasattr(self, 'env') else os.environ

        if 'JMAKE_DRY' in env and env['JMAKE_DRY']:
            command = "echo '%s'" % command
            logger.warn('Call will be suppressed because you have exported JMAKE_DRY env. variable.')

        executable = self.__execute(command, logger, env)
        logger.debug('Executed process pid=%s' % executable.pid)
        prev_signal_handler = signal.signal(signal.SIGINT, partial(self.__signal_handler, executable))

        try:
            self.sig_int = False
            raw_line = b''
            line_no = 0
            while not self.run_in_background:
                try:
                    line = None
                    byte = executable.stdout.read(1)
                    raw_line += byte
                    if byte == b'' or byte == b'\n':
                        line = self.decode_utf_8(raw_line, logger)
                        if not line and executable.poll() is not None:
                            break
                        raw_line = b''
                    else:
                        if len(raw_line) > 6:
                            partial_line = self.decode_utf_8(raw_line, logger)
                            if partial_line == 'maven2>' or partial_line == 'maven3>':
                                print(partial_line, end='')
                                sys.stdout.flush()
                                while not byte == b'\n':
                                    byte = executable.stdout.read(1)
                                    assert isinstance(byte, bytes)
                                    if byte:
                                        print(self.decode_utf_8(byte, logger), end='')
                                        sys.stdout.flush()
                            else:
                                rest_of_line = executable.stdout.readline()
                                assert isinstance(rest_of_line, bytes)
                                line = partial_line + self.decode_utf_8(rest_of_line, logger)

                            raw_line = b''
                    if line is not None:
                        if self.process_output(logger, line.rstrip('\n'), line_no):
                            if self.with_logged_output:
                                logger.trace(line)
                            else:
                                print(self.__colorize_line(line, logger), end='')
                        line_no += 1
                except IOError:
                    if self.sig_int:
                        logger.debug('Caught SIGINT terminating jmake')
                    else:
                        logger.warn('The spawned process pid=%s was probably killed' % executable.pid)
                    return Callable.do_not_proceed

            if not self.run_in_background:
                executable.communicate()
            else:
                atexit.register(lambda: logger.info('Killing background process %s' % command) or executable.kill())
        finally:
            signal.signal(signal.SIGINT, prev_signal_handler)
            if not self.run_in_background:
                self.returncode = executable.returncode
            else:
                self.returncode = Callable.success

        return Callable.do_not_proceed if self.sig_int else self.returncode

    def command(self, new_command):
        self.__command = new_command

    def background(self):
        self.run_in_background = True
        return self

    def redirect_output(self, log_file):
        self.log_file = log_file
        return self

    def __signal_handler(self, executable, signal, frame):
        self.sig_int = True
        try:
            executable.kill()
        except OSError:
            pass

    def __execute(self, command, logger, env):
        logger.info(
            'Making OS call{0}: {1}'.format(
                ' (in directory: ' + str(self.__cwd) + ')' if self.__cwd is not None else '',
                command))
        if self.log_file is not None:
            logger.info('Redirected output to file %s' % self.log_file)
            out_file = open(self.log_file, mode='a')
        else:
            out_file = subprocess.PIPE

        return subprocess.Popen(command, stdout=out_file, stderr=subprocess.STDOUT, shell=True, cwd=self.__cwd,
            env=env)


    def process_output(self, logger: Logger, line: str, num: int):
        """
        Override to catch simple output lines and return True to display it on stdout or False to suppress it.
        """
        return True

    def __colorize_line(self, line: str, logger: Logger):
        if 'WARN' in line:
            return logger.color_text(line, Logger.L_WARN)
        if 'ERROR' in line:
            return logger.color_text(line, Logger.L_ERROR)
        if 'BUILD SUCCESS' in line:
            return logger.color_text(line, Logger.L_INFO)
        return line


class CommandExecutor:
    def __init__(self):
        self.commands = []
        self.post_commands = []
        self.perform_console_reset = True

    def append(self, command):
        if command is not None:
            self.commands.append(command)

    def append_post(self, command):
        if command is not None:
            self.post_commands.append(command)

    def set_logger(self, logger):
        self.log = logger
        return self


    def execute(self):
        log = getattr(self, 'log', None)
        if log is None: log = Logger().set_none()

        try:
            return_code = self.__execute_commands(self.commands, log)
            return return_code
        finally:

            self.__execute_commands(self.post_commands, log)
            if self.perform_console_reset:
                print('\x1b[0;0m')


    def __execute_commands(self, commands, log):
        try:
            for callable in commands:
                return_code = callable(log)
                if return_code == Callable.do_not_proceed:
                    return return_code
                if return_code != Callable.success:
                    log.error('Last executed command ' + callable.__class__.__name__ + ' returned ' + str(return_code))
                    return return_code
        except KeyboardInterrupt:
            log.info('\njmake interrupted by keyboard')
            return -1
        except IOError as er:
            log.error('I/O Error %s' % er)
            return -1
        return Callable.success


class ExecutionTimeCallable(Callable):
    def __init__(self):
        super().__init__({})
        self.start_time = time.time()

    def __call__(self, logger):
        self.time = str(round(time.time() - self.start_time, 3))
        msg = 'Finished in ' + self.time + 's'
        if not os.getenv('JMAKE_SKIP_COWS', None): cow_say(msg, logger)
        return Callable.success

COWSAY = './bin/cowsay'
COWSAY_DIR = './bin/cows'
LOLCAT = './bin/gems/bin/lolcat'
COWSAY_CMD = COWSAY + ' -f {0} {1} |' + LOLCAT


def cow_say(msg, logger):
    cow_to_print = COWSAY_DIR + os.sep + random.choice(os.listdir(COWSAY_DIR))
    command = COWSAY_CMD.format(cow_to_print, msg)
    return_code = subprocess.call(command, shell=True, stderr=subprocess.PIPE)
    if return_code != 0:
        logger.info(msg)


class GenericTaskCallable(Callable):
    def __init__(self, args, task):
        super().__init__(args)
        self.args = args
        self.task = task

    def __call__(self, logger):
        return self.task(self.args, logger)


class ConditionalCallable(Callable):

    class Result:

        def __init__(self, execute_task, result=None):
            super().__init__()
            self.result = result
            self.execute_task = execute_task

        @staticmethod
        def execute_task():
            return ConditionalCallable.Result(True)

        @staticmethod
        def return_value(value):
            return ConditionalCallable.Result(False, value)

    def __init__(self, args, task, condition):
        super().__init__(args)
        self.condition = condition
        self.task = task

    def __call__(self, logger):
        res = self.condition(logger)
        if res.execute_task:
            return self.task(logger)
        else:
            return res.result


class CombinedCallable(Callable):

    def __init__(self, args, tasks):
        super().__init__(args)
        self.tasks = tasks

    def __call__(self, logger):
        res = Callable.failure

        for task in self.tasks:
            res = task(logger)
            if res != Callable.success:
                return res

        return res


class RemotePython26ScriptCallable(Callable):
    def __init__(self, hostname, script_file, file_utils=FileUtils(), process_utils=ProcessUtils()):
        super().__init__([])
        self.process_utils = process_utils
        self.file_utils = file_utils
        self.script_file = script_file
        self.hostname = hostname

    def __call__(self, logger):
        logger.info('Executing script %s on remote host %s' % (os.path.basename(self.script_file), self.hostname))

        with self.file_utils.open(self.script_file, 'rb', encoding=None) as f:
            if not os.getenv('JMAKE_DRY', None) is None:
                logger.warn('Skipping execution of script due to detected JMAKE_DRY.')
                return Callable.success

            out = self.process_utils.execute_read_input(['ssh', self.hostname, 'cat - | python26'], f.read())
            logger.debug('Output from remote host:\n<<<BEGIN>>>\n%s\n<<<END>>>' % out)

        return Callable.success
