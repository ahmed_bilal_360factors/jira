import json
import re
from urllib.error import URLError, HTTPError
import urllib.request
import time

from CommandExecutor import Callable
from ondemand.HordeLayout import HordeLayout


class HordeStatusChecker:
    def __init__(self, horde_layout: HordeLayout):
        self.horde_layout = horde_layout

    def verify_healthchecks(self, health_check_result):
        for check in health_check_result["status"]:
            if check['isHealthy']:
                pass
            elif re.match(r'Expected SSO cookie domain to be .*, was localhost', check["failureReason"]):
                # white listed: this is hardcoded check in horde which doesn't make sense for us - we want localhost :)
                pass
            else:
                return False

        return True

    def is_running(self):
        try:
            req = urllib.request.Request(self.horde_layout.horde_status_url(), headers={'Accept': 'application/json'})
            urllib.request.urlopen(req)
            return True
        except HTTPError as e:
            if e.code == 503:
                health_check_result = json.loads(e.read().decode('utf8'))
                return self.verify_healthchecks(health_check_result)
            else:
                return False

        except URLError:
            return False


class HordeStatusEnforcer(Callable):
    def __init__(self, args, max_tries=25, timeout=5):
        super().__init__(args)
        self.horde_layout = args.horde_layout
        self.horde_status_checker = HordeStatusChecker(self.horde_layout)
        self.max_tries = max_tries
        self.timeout = timeout
        self.ignore_horde_startup_errors = args.ignore_horde_startup_errors

    def __call__(self, logger):
        tries = 1
        while not self.horde_status_checker.is_running():
            if tries >= self.max_tries:
                if self.ignore_horde_startup_errors:
                    logger.warn(
                        'Horde status at %s returned error - ignoring as requested' % self.horde_layout.horde_status_url())
                    return Callable.success
                else:
                    logger.error(
                        'Horde status at %s returned error - stopping build' % self.horde_layout.horde_status_url())
                    return Callable.do_not_proceed

            logger.info('Horde is not ready: Attempt %d of %d failed. Retrying in %d seconds. Check the logs at %s' % (
            tries, self.max_tries, self.timeout, self.horde_layout.horde_log_file()))
            tries += 1
            time.sleep(self.timeout)

        return Callable.success
