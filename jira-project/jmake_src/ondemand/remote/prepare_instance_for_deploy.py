# IMPORTANT: this code have to work with python 2.6 as this is the best python we have available on unicorns
import re
import os
import subprocess

studio_local_path = '/data/studio_local/jira'

if not os.path.exists(studio_local_path):
    os.makedirs(studio_local_path)

yaml = "/data/icebat/local.yaml"
settings = {
    'jira::version': None
}

with open(yaml, "r") as f:
    for line in f:
        match = re.search('jira::version:\s+"(.*)"', line)
        if match:
            version = match.groups()[0]
            settings['jira::version'] = version

expected_version = 'studio_local'
if settings['jira::version'] is None:
    # update version
    with open(yaml, "a") as f:
        f.write('\njira::version: "%s"' % expected_version)

    #trigger icebat to apply this change
    subprocess.Popen('/sw/ondemand/install/icebatstrap/install/install-studio.sh -s -n j2ee_jira'.split(' ')).wait()

elif settings['jira::version'] != expected_version:
    raise "JIRA version is set to %s instead of %s, please fix that" % (settings['jira::version'], expected_version)