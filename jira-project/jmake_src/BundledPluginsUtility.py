from functools import reduce
import os
import re
from CommandExecutor import Callable
from Logger import LOG, Logger
from utils import PathUtils
from utils.FileUtils import FileUtils


JIRA_PLUGINS_DIR_ABS = os.path.abspath(os.sep.join(['jira-components', 'jira-plugins']))
OD_PLUGINS_DIR_ABS = os.path.abspath(os.sep.join(['jira-ondemand-project', 'jira-ondemand-plugins']))
TARGET_DIR_PATTERN = os.sep.join(['{}', 'target'])
SRC_DIR_PATTERN = os.sep.join(['{}', 'src'])
POM_DIR_PATTERN = os.sep.join(['{}', 'pom.xml'])
BUNDLED_PLUGINS_LIST = PathUtils.abspath(
    os.sep.join(['target', 'jmake-bundled-plugins.list']))
JMAKE_PROFILES_PLUGINS_LIST = os.path.abspath(
    os.sep.join(['target', 'jmake-bundled-plugins-profiles.list']))
PLUGIN_FROM_PATH_EXTRACTOR = r'(.+)' + os.sep + "target.+"


class BundledPluginsUtility:
    def __init__(self,  bundled_plugins_modules, file_utils=FileUtils()):
        super().__init__()
        self.file_utils = file_utils
        self.bundled_plugins_modules = bundled_plugins_modules
        self.bundled_plugins_lists = \
            [PathUtils.abspath(os.sep.join([TARGET_DIR_PATTERN.format(module), 'atlassian-bundled-plugins.list']))
             for module in bundled_plugins_modules]
        self.bundled_plugins_lists_to_pom_map = \
            {PathUtils.abspath(os.sep.join([TARGET_DIR_PATTERN.format(module), 'atlassian-bundled-plugins.list']))
             : POM_DIR_PATTERN.format(module)
             for module in bundled_plugins_modules}

    def get_bundled_plugins_modules(self):
        return self.bundled_plugins_modules

    def find_plugins_to_recompile(self, profiles):
        if not (self.bundled_plugins_list_ok(profiles)):
            return self.get_bundled_plugins_modules()
        else:
            return [module[0] for module in self.__get_all_local_plugins_from_list() if
                    self.__module_needs_recompilation(module)]

    def remember_plugins_profiles(self, logger: Logger, profiles):
        if not 'JMAKE_DRY' in os.environ:
            self.file_utils.write_lines(JMAKE_PROFILES_PLUGINS_LIST, profiles)
        else:
            logger.info('Writing plugin profiles {} to file {}'.format(str(profiles), JMAKE_PROFILES_PLUGINS_LIST))

    def bundled_plugins_list_ok(self, profiles):
        return self.__bundled_plugins_lists_exists() and self.__bundled_plugins_lists_up_to_date() and \
               self.__all_bundled_plugins_in_local_repo() and self.__profiles_up_to_date(profiles)

    def __bundled_plugins_lists_exists(self):
        return all((self.__bundled_plugins_list_exists(x) for x in self.bundled_plugins_lists))

    def __bundled_plugins_list_exists(self, bp_list):
        path_lexists = self.file_utils.file_exists(bp_list)
        if not path_lexists:
            self.__log('Bundled plugins list do not exists : ' + bp_list)
        return path_lexists

    def __bundled_plugins_lists_up_to_date(self):
        return all((self.__bundled_plugins_list_up_to_date(bp_list, pom) for (bp_list, pom) in
                    self.bundled_plugins_lists_to_pom_map.items()))

    def __bundled_plugins_list_up_to_date(self, bp_list, pom):
        if self.file_utils.getmtime(bp_list) < self.file_utils.getmtime('pom.xml'):
            self.__log('Main pom.xml newer than bundled plugins list')
            return False
        pom_up_to_date = self.file_utils.getmtime(bp_list) > self.file_utils.getmtime(pom)
        if not pom_up_to_date:
            self.__log('Bundled plugins pom newer than bundled plugins list')
        return pom_up_to_date

    def __all_bundled_plugins_in_local_repo(self):
        paths_not_in_repo = [line for line in self.get_all_bundled_plugins_paths()
                             if not line.startswith(JIRA_PLUGINS_DIR_ABS)
                             and not line.startswith(OD_PLUGINS_DIR_ABS)
                             and not self.file_utils.file_exists(line)]

        all_plugins_in_repo = len(paths_not_in_repo) == 0
        if not all_plugins_in_repo:
            self.__log('Not all bundled plugins in local maven repository: ' + str(paths_not_in_repo))
        return all_plugins_in_repo

    def get_all_bundled_plugins_paths(self):
        return self.__get_agregate_list()

    def __module_needs_recompilation(self, module):
        module_path, plugin_jar = module
        src_dir = SRC_DIR_PATTERN.format(module_path)
        target_dir = TARGET_DIR_PATTERN.format(module_path)
        pom_file = POM_DIR_PATTERN.format(module_path)
        if not self.file_utils.file_exists(plugin_jar):
            self.__log('Found changes in plugin {} jar file {} not found '.format(module_path, plugin_jar))
            return True

        #check also pom for modification
        last_src_modification = max(
            self.__find_last_modification(src_dir),
            self.file_utils.getmtime(pom_file))

        #if jar is older than target than also recompile
        last_target_modification = min(self.__find_last_modification(target_dir),
                                       self.file_utils.getmtime(plugin_jar))
        if last_src_modification > last_target_modification:
            self.__log('Found changes in plugin {}'.format(module_path))
            return True
        else:
            return False

    @staticmethod
    def __log(msg):
        LOG.info(msg)

    def __find_last_modification(self, dir):
        last_mod_time = 0
        for root, dirs, files in self.file_utils.walk(dir):
            last_mod_time = max(last_mod_time,
                reduce(lambda act_max, file: max(act_max, self.file_utils.getmtime(root + os.sep + file)), files,
                    last_mod_time))
        return last_mod_time

    def __get_all_local_plugins_from_list(self):
        return [(os.path.relpath(re.search(PLUGIN_FROM_PATH_EXTRACTOR, path).group(1)), path) for path in
                self.get_all_bundled_plugins_paths()
                if path.startswith(JIRA_PLUGINS_DIR_ABS) or path.startswith(OD_PLUGINS_DIR_ABS)]

    def __profiles_up_to_date(self, profiles):
        if not self.file_utils.file_exists(JMAKE_PROFILES_PLUGINS_LIST):
            self.__log(
                'File %s does not exist assuming that bundled plugins needs recompilation' % JMAKE_PROFILES_PLUGINS_LIST)
            return False
        previous_profiles = self.file_utils.read_lines(JMAKE_PROFILES_PLUGINS_LIST)
        if frozenset(previous_profiles) == frozenset(profiles):
            return True
        else:
            self.__log('Bundled plugins compiled with different profiles')
            self.__log('Old profiles {} new profiles {}'.format(str(previous_profiles), str(profiles)))
            return False

    def get_plugins_task(self, profiles):
        return lambda logger: self.remember_plugins_profiles(logger, profiles) or Callable.success

    def __get_agregate_list(self):
        def lines_of_files(file_names):
            return [line for path in file_names for line in self.file_utils.read_lines(path)]

        return lines_of_files(self.bundled_plugins_lists)

    def create_agregate_list(self):
        lines = self.__get_agregate_list()
        self.file_utils.write_lines(file_name=BUNDLED_PLUGINS_LIST, lines=lines)

        return Callable.success







