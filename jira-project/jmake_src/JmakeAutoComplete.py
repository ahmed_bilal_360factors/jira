import os
import sys
from CommandExecutor import Callable
from module import JmakeModule
from utils.FileUtils import FileUtils
from utils.ParserUtils import subparser_choices


class AutoComplete(JmakeModule):
    """
    Possible improvements:
        1. (DONE)
            optional long and short forms (like -i and --install) will be auto-completed separately (one will not
            exclude the other)
        2. (DONE)
            'choices' are not mutually exclusive (once you selected 'alacarte' for jmake ondemand run, 'jira-only' will
            still be suggested)
        3. arguments with data (like --jira-port JIRA_PORT) is unaware of the fact that syntax requires a compulsory
            argument, neither that it might have a default.
        4. autocomplete is not aware of positional arguments sequence other than sub-parsers.
    """

    def __init__(self, parser):
        super().__init__()
        self.prevent_post_commands = True
        self.prevent_console_reset = True
        self.prevent_post_diagnostics = True
        self.check_branch = False
        self.main_parser = parser
        self.command = 'auto-complete'
        self.description = 'Used by the jmake autocomplete feature. Unfortunately it is bash that sorts the options, so '\
                           'jmake cannot influence that. You need to install it first (run with -i). This part of jmake '\
                           'is tricky, it WILL work for bash, but you might find some unexpected or unclear behavior.'

    def __call__(self, args, executor):

        # since I do not want any of the options to be caught by the parser, I need to consider them manually:
        if len(args.input) == 1:
            if args.input[0] == '-h' or args.input[0] == '--help':
                executor.append(self.print_autocomplete_help(self.main_parser))
                return
            elif args.input[0] == '-i' or args.input[0] == '--install':
                executor.append(AutoCompleteInstaller())
                return


        # kill logging (no warnings, diagnostics, etc., or bash will munch on it):
        def kill_logging(logger):
            logger.set_none()
            return Callable.success
        executor.append(kill_logging)

        # now, determine auto-complete:
        def determine_autocomplete(logger):
            print(' '.join(self.__determine_autocomplete(args, logger)))
            return Callable.success
        executor.append(determine_autocomplete)

    def __determine_autocomplete(self, args, logger):
        # bug in python: it will cut out '--' from args:
        if sys.argv[len(sys.argv) - 1] == '--':
            args.input.append('--')

        if len(args.input) > 0: args.input[0] = 'jmake'

        parser = self.main_parser
        free_args = args.input[1:]

        while len(free_args):
            command = free_args[0]
            subparsers = subparser_choices(parser)
            if subparsers and command in subparsers.keys():
                parser = subparsers[command]
                free_args = free_args[1:]
            else:
                break

        result = self.map_options(parser)
        result.append(['clean', '--set-default'])
        try:
            contributed = parser.autocomplete_contributor()
            if contributed:
                result.append(contributed)
        except AttributeError:
            pass

        flattened_result = (item for group in result for item in group)
        free_size = len(free_args)
        if free_size > 0:
            last_el = free_args[free_size - 1]
            partial_match = last_el if last_el not in flattened_result else None
            filtered_result = (item for group in result if not [x for x in group if x in free_args] for item in group)

            if partial_match is None:
                result = filtered_result
            else:
                result = (o for o in filtered_result if o.startswith(partial_match))
        else:
            result = flattened_result

        return result

    def define_parser(self, parser):
        parser.add_argument('input', nargs='*', help='user input')
        parser.add_argument('-i', '--install', action='store_true', help='install jmake autocomplete in your system '
                                                                         '(requires sudo).')
        # catch all "words" as positionals, not optionals:
        parser.prefix_chars = ''
        parser.epilog = ''


    def map_options(self, parser):
        options = []

        for action in parser._actions:
            if isinstance(action.choices, dict) and action.choices:
                options.append([x for x in sorted(action.choices.keys())])
            elif isinstance(action.choices, list) and action.choices:
                options.append(action.choices)
            if action.option_strings is not None and action.option_strings:
                options.append(action.option_strings)

        return options

    def print_autocomplete_help(self, parser):
        def print_autocomplete_help_closure(_):
            subparser_choices(parser)['auto-complete'].print_help()
            return Callable.success
        return print_autocomplete_help_closure


BASH = '/bin/bash'
ZSH = '/bin/zsh'


class AutoCompleteInstaller(Callable):

    def __init__(self, env = os.environ, fs:FileUtils = FileUtils()):
        super().__init__(None)
        self.shell = env['SHELL'] if 'SHELL' in env else None
        self.fs = fs

    def get_completion_function_file(self):

        if self.shell == BASH:
            return self.__completion_function_file_bash()
        elif self.shell == ZSH:
            return self.__completion_function_file_zsh()
        else:
            return None

    # VisibleForTesting
    def get_completion_init_file(self):

        if self.shell == ZSH:
            return self.__init_file_zsh()
        else:
            return None

    def __create_autocomplete_dir_if_not_exists(self):
        return self.fs.existing_dir(os.sep.join([self.fs.jmake_user_data_dir(), 'completion']))

    def __completion_function_file_bash(self):
        return os.sep.join([self.__create_autocomplete_dir_if_not_exists(), 'jmake.completion.bash'])

    def __completion_function_file_zsh(self):
        return os.sep.join([self.__create_autocomplete_dir_if_not_exists(), '_jmake'])

    def __init_file_zsh(self):
        return os.sep.join([self.__create_autocomplete_dir_if_not_exists(), 'jmake.completion.zsh'])

    def __install_autocomplete_bash(self, logger):

        if not self.__is_bash_autocomplete_installed(logger):
            return Callable.do_not_proceed

        autocomplete_dir = self.__create_autocomplete_dir_if_not_exists()

        completion_function = self.__completion_function_file_bash()
        if not self.fs.file_exists(completion_function):
            self.fs.write_lines(completion_function, [
                '_jmake() {',
                '\tCOMPREPLY=(`./jmake auto-complete ${COMP_WORDS[@]}`)',
                '} &&',
                'complete -F _jmake ./jmake'
            ])
            logger.info('Created autocomplete function file: ' + completion_function)
        else:
            logger.warn('The autocomplete function file exists. Delete it if you want me to overwrite it: ')
            logger.warn(' $ rm ' + completion_function)

        logger.info('Autocomplete for bash is installed in ' + autocomplete_dir)
        logger.info('In order to use it put this into your .profile/.bashrc and restart your shell:')
        logger.info(' source ' + completion_function)

        return Callable.success

    def __is_bash_autocomplete_installed(self, logger):
        paths = ['/etc/bash_completion.d', # Linux
                 '/usr/local/etc/bash_completion.d', # Mac brew
                 '/opt/local/etc/bash_completion.d', # MacPorts  for bash 3
                 '/opt/local/share/bash-completion/completions']  # MacPorts for bash 4

        # find existing file from the list:
        bash_completion_directory = next((path for path in paths if self.fs.dir_exists(path)), None)

        if not bash_completion_directory:
            logger.error('You need "bash-completion" package installed. It is expected that one of these directories exist on your system:')
            for p in paths:
                logger.error('  ' + p)
            logger.error('Autocomplete installation cannot proceed without "bash-completion".')
            logger.error('Either raise a bug against jmake with your setup detailed, or link your bash-completion location at one of the specified paths.')
            return False

        return True

    def __install_autocomplete_zsh(self, logger):

        autocomplete_dir = self.__create_autocomplete_dir_if_not_exists()

        completion_function = self.__completion_function_file_zsh()
        if not self.fs.file_exists(completion_function):
            self.fs.write_lines(completion_function, [
                '#compdef jmake',
                '_arguments "*: :(`./jmake auto-complete $words`)"'
            ])
            logger.info('Created autocomplete function file: ' + completion_function)
        else:
            logger.warn('The autocomplete function file exists. Delete it if you want me to overwrite it: ')
            logger.warn(' $ rm ' + completion_function)

        completion_init = self.__init_file_zsh()
        if not self.fs.file_exists(completion_init):
            self.fs.write_lines(completion_init, [
                "fpath=(%s $fpath)" % autocomplete_dir,
                "autoload -U compinit && compinit",
                "zstyle ':completion:*' menu select=2"
            ])
            logger.info('Created autocomplete init file: ' + completion_init)
        else:
            logger.warn('The autocomplete init file exists. Delete it if you want me to overwrite it: ')
            logger.warn(' $ rm ' + completion_init)

        logger.info('Autocomplete for zsh is installed in ' + autocomplete_dir)
        logger.info('In order to use it put this into your .profile/.zshrc and restart your shell:')
        logger.info(' source ' + completion_init)

        return Callable.success

    def __call__(self, logger):
        if self.shell == BASH:
            logger.info('Your $SHELL tells me you want autocomplete for bash (%s). zsh is also supported.' % self.shell)
            return self.__install_autocomplete_bash(logger)
        elif self.shell == ZSH:
            logger.info('Your $SHELL tells me you want autocomplete for zsh (%s). bash is also supported.' % self.shell)
            return self.__install_autocomplete_zsh(logger)
        else:
            logger.error('Can''t determine your shell. Your $SHELL = ' + self.shell)

        return Callable.failure
