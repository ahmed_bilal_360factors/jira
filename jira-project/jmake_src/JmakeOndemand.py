import functools
import glob
import re
import os
import subprocess

from BundledPluginsUtility import BundledPluginsUtility
from CommandExecutor import SystemCallable, Callable, RemotePython26ScriptCallable, ConditionalCallable, \
    CombinedCallable
from JmakeDebug import Debug
from JmakeRun import Run
from Logger import LOG
from catalina.WorkspaceLayout import WorkspaceBuilder
from maven.Maven import MavenCallable
from module import JmakeModule
from ondemand.HordeLayout import HordeLayout
from ondemand.HordeRunner import HordeRunner
from ondemand.HordeStatusChecker import HordeStatusEnforcer
from ondemand.LegacyOdSvnSetup import LegacyOdSvnSetup
from ondemand.ManifestoUtils import ManifestoUtils
from utils.FileUtils import FileUtils
from utils.WorkspaceUtils import WorkspaceUtils, WorkspaceValidator


ONDEMAND_PROFILE = 'ondemand'

OD_WEBAPP_PROJECT_DIR = os.sep.join(['jira-ondemand-project', 'jira-ondemand-webapp'])

OD_BUNDLED_PLUGINS = [os.sep.join(['jira-components', 'jira-plugins', 'jira-bundled-plugins-common']),
                      os.sep.join(['jira-ondemand-project', 'jira-ondemand-plugins', 'jira-ondemand-bundled-plugins'])]


class OnDemandInstall(JmakeModule):
    def __init__(self):
        super().__init__()
        self.command = 'install'
        self.description = 'Build and install (to your local maven repository) the OnDemand web application.'

    def __call__(self, args, executor):
        OnDemandInstall.define_install_actions(args, executor)

    @staticmethod
    def define_install_actions(args, executor,
                               plugins_utility: BundledPluginsUtility=BundledPluginsUtility(OD_BUNDLED_PLUGINS)):
        if args.quick:
            maven = MavenCallable(args, 'jira-ondemand-project')
            maven.phase('install').skip_tests().profile('func-mode-plugins')
            executor.append(maven)
        else:
            maven = MavenCallable(args)
            maven.phase('install')
            maven.project('jira-ondemand-project/jira-ondemand-webapp') \
                .project('jira-ondemand-project/jira-ondemand-acceptance-tests').option('-am')
            maven.skip_tests().profile(ONDEMAND_PROFILE).profile('func-mode-plugins')
            executor.append(maven)
            plugins_utility.remember_plugins_profiles(LOG, maven.profiles)

    def define_parser(self, parser):
        MavenCallable.add_maven_switches(parser)
        parser.add_argument('-q', '--quick',
                            help='executes quick build of OnDemand. JIRA core components will be picked up '
                                 '''from some location that only Maven knows (or more likely doesn't)''',
                            action='store_true')


class OnDemandRun(Run):
    template_file = os.sep.join(['.', 'jmake_src', 'data', 'download_poms_pom_template.xml'])

    def __init__(self):
        super().__init__()
        self.description = 'Runs JIRA-only OD instance using your code. This will download a tomcat for you, configure ' \
                           'it and start JIRA and Horde on your local machine. This will also download plugins from ' \
                           'manifesto from given zone/hash (manifesto required you to be in the office or connect via ' \
                           'VPN). This will not enable you to hotswap, JRebel or debug your running instance.'
        self.manifesto = ManifestoUtils()

    def __call__(self, args, executor):
        if args.setup_home:
            executor.append(lambda log: log.warn('Setup home option is not available for ondemand.') or 0)

        super().__call__(args, executor)

    def define_parser(self, parser):
        super().define_parser(parser)
        parser.add_argument('--manifesto-hash', '-mh', action='store', default='default', dest='manifesto_hash',
                            help='Get plugins using a manifesto hash, zone or alias. Should handle most conventions: '
                                 '"dev", "dog", "prod", "jirastudio-dev", etc., or explicit hash. Case insensitive. '
                                 'Defaults to latest dev.')

        parser.add_argument('--manifesto-jira', action='store_true', dest='manifesto_jira',
                            help='Will not build JIRA but will take it from the given manifesto hash using '
                                 '--manifesto-hash parameter. Too bad JIRA repo must be cloned for this...')

        parser.add_argument('--ignore-manifesto-errors', action='store_true', dest='ignore_manifesto_errors',
                            help='Will not stop the build if manifesto calls are unsuccessful. Will start without'
                                 'manifesto driven PSD instead.')

        parser.add_argument('--ignore-horde-startup-errors', action='store_true', dest='ignore_horde_startup_errors',
                            help='Will not stop the build if horde thinks it failed to startup.')

        parser.add_argument('--instant-on', action='store_true', dest='instanton',
                            help='Will run OD in tenancy mode.')

        parser.add_argument('--tenanted', action='store_true', dest='tenanted',
                            help="OD will start tenanted, because a tenant.id file will be written to studio home.")

        self.sample_usage(parser, 'To run an instance that uses your code for JIRA, but uses plugins from manifesto hash 123456abcdef:',
                          ['./jmake ondemand run --manifesto-hash 123456abcdef'])

        self.sample_usage(parser, 'To run an instance that uses JIRA and plugins from current DOG configuration:',
                          ['./jmake ondemand run --manifesto-hash DOG --manifesto-jira'])
        #we should consider using sparse checkouts see
        #http://stackoverflow.com/questions/600079/is-there-any-way-to-clone-a-git-repositorys-sub-directory-only

        parser.autocomplete_contributor = lambda : self.manifesto.generate_all_zones_and_aliases()


    #Override
    def get_tasks_to_build_jira(self, args, workspace_utils=WorkspaceUtils()):

        if not args.manifesto_jira:
            #for workspace build maven must be executed from parent directory
            maven = MavenCallable(args)
            maven.phase('package')

            maven.profile('ondemand').project(OD_WEBAPP_PROJECT_DIR).option('-am')
            maven.property('jira.home', args.layout.jira_home())

            maven.projects.extend(workspace_utils.get_workspace_projects_without_jira(args))
            self.add_devmode_plugins(args, maven)

            if not args.attach_sources:
                maven.property('skipSources')
            maven.skip_tests()
            if args.third_party_licensing:
                maven.profiles.extend(['third-party-licensing'])

            # return builder and manifesto plugins job:
            return [maven]
        else:

            mavenClean = MavenCallable(args, path=OD_WEBAPP_PROJECT_DIR)
            mavenClean.phase('clean')

            return [mavenClean,
                    self.manifesto_jira(args, manifesto=self.manifesto),
                    self.__create_target_dir_for_exploded_webapp(),
                    SystemCallable(args, command='unzip *.war -d ../war/exploded',
                                   cwd=os.sep.join([OD_WEBAPP_PROJECT_DIR, 'target', 'target']))]

    #Override
    def get_tasks_before_build_jira(self, args, executor):
        if args.with_workspace:
            #TODO: untested.
            executor.append(WorkspaceValidator())

        self.check_ports(args, executor)

        executor.append(LegacyOdSvnSetup(args))
        executor.append(HordeRunner(args))

    #Override
    def get_tasks_post_build_jira(self, args, executor):
        executor.append(self.manifesto_plugins(args, manifesto=self.manifesto))
        executor.append(HordeStatusEnforcer(args))

    #Override
    def build_layouts(self, args):
        """ This is called by superclass to build the directory layouts for the needed webapps. """
        args.layout = WorkspaceBuilder.buildLayout(args.tomcat_dir, args.tomcat_version, args.jira_home,
                                                   args.instance_name, True)
        args.horde_layout = HordeLayout(args.layout.jira_home())

    def handle_manifesto_exception(self, args, message, e, logger):
        logger.error(message)
        if args.ignore_manifesto_errors:
            args.manifesto_psd = None
            logger.debug(str(e))
            return Callable.success
        else:
            logger.error(str(e))
            return Callable.failure

    def manifesto_jira(self, args, manifesto: ManifestoUtils=ManifestoUtils(), fs: FileUtils=FileUtils()):

        def manifesto_jira_closure(logger):

            webapp_target_dir = fs.existing_dir(os.sep.join([OD_WEBAPP_PROJECT_DIR, 'target']))

            logger.info('Determining the manifesto hash...')
            try:
                manifesto_hash = manifesto.determine_hash(args.manifesto_hash)
            except Exception as e:
                return self.handle_manifesto_exception('Unable to resolve zone to hash.', args, e, logger)
            logger.info('Will setup the OD instance with the following hash: ' + manifesto_hash)

            logger.info('Determining manifest JIRA version...')
            try:
                manifesto_jira = manifesto.get_od_jira(manifesto_hash)
            except Exception as e:
                return self.handle_manifesto_exception('Unable to determine jira version from manifesto.', args, e,
                                                       logger)
            logger.info('Will use JIRA webapp version: %s.' % manifesto_jira['version'])

            logger.info('Downloading JIRA...')
            try:
                fs.filter_file(OnDemandRun.template_file,
                               os.sep.join([webapp_target_dir, 'pom.xml']),
                               {'${ARTIFACT_ITEMS}': self.__artifact_description_for_pom_template(manifesto_jira)})

                rc = MavenCallable(path=webapp_target_dir).phase('verify').option('-B')(logger)
            except Exception as e:
                return self.handle_manifesto_exception('Unable to download required plugins.', args, e, logger)
            return rc

        return manifesto_jira_closure

    def manifesto_plugins(self, args, manifesto: ManifestoUtils=ManifestoUtils(), fs: FileUtils=FileUtils()):

        def download_pom_location(manifesto_hash):
            return fs.abs_path(os.sep.join(['.', 'target', 'manifesto', 'psd-' + manifesto_hash]))

        def manifesto_plugins_closure(logger):
            logger.info('Determining the manifesto hash...')
            try:
                manifesto_hash = manifesto.determine_hash(args.manifesto_hash)
            except Exception as e:
                return self.handle_manifesto_exception('Unable to resolve zone to hash.', args, e, logger)
            logger.info('Will setup the OD instance with the following hash: ' + manifesto_hash)

            logger.info('Determining plugin list...')
            try:
                manifesto_plugins = manifesto.get_plugins_maven_artifacts(manifesto_hash)
            except Exception as e:
                return self.handle_manifesto_exception('Unable to determine required plugins from manifesto.', args, e,
                                                       logger)
            logger.info('Found %d plugins to install.' % len(manifesto_plugins))

            logger.info('Downloading plugins...')
            try:
                fs.filter_file(OnDemandRun.template_file,
                               os.sep.join([fs.existing_dir(download_pom_location(manifesto_hash)), 'pom.xml']),
                               {'${ARTIFACT_ITEMS}': ''.join(
                                   self.__artifact_description_for_pom_template(v) for k, v in manifesto_plugins.items())})

                rc = MavenCallable(path=download_pom_location(manifesto_hash)).phase('verify').option('-B')(logger)
            except Exception as e:
                return self.handle_manifesto_exception('Unable to download required plugins.', args, e, logger)
            args.manifesto_psd = download_pom_location(os.sep.join([manifesto_hash, 'target']))

            return rc

        return manifesto_plugins_closure

    def __create_target_dir_for_exploded_webapp(self, fs: FileUtils=FileUtils()):
        def create_target_dir_for_exploded_webapp_closure(_):
            fs.existing_dir(os.sep.join([OD_WEBAPP_PROJECT_DIR, 'target', 'war', 'exploded']))
            return 0

        return create_target_dir_for_exploded_webapp_closure

    def __artifact_description_for_pom_template(self, artifact):

        def in_tag(tagname, text):
            return '<{0}>{1}</{0}>'.format(tagname, text)

        return in_tag('artifactItem', ''.join([
            in_tag('groupId', artifact['groupId']),
            in_tag('artifactId', artifact['artifactId']),
            in_tag('version', artifact['version']),
            in_tag('outputDirectory', './target'),
            in_tag('type', artifact['packaging'])
        ]))


class OnDemandDebug(Debug, OnDemandRun):
    def __init__(self):
        super().__init__()
        self.description = 'Runs JIRA using your code. This will download a tomcat for you, configure it and start ' \
                           'JIRA-Only OD instance with Horde on your local machine. To debug it, use the default ' \
                           '"Remote" configuration from your IDEA (port 5005). Once connected this will hot-swap ' \
                           'code changes into your running JIRA (for core, not for plugins).'

    def define_parser(self, parser):
        super().define_parser(parser)
        self.sample_usage(parser, 'To debug your local code with the set of manifesto driven plugins from DEV:',
                          ['./jmake ondemand debug'])


# noinspection PyMethodMayBeStatic
class OnDemandDeploy(JmakeModule):
    def __init__(self):
        super().__init__()
        self.file_utils = FileUtils()
        self.plugins_utility = BundledPluginsUtility(OD_BUNDLED_PLUGINS)
        self.command = 'deploy'
        self.description = 'Build and deploy JIRA OD to unicorn. You should have you unicorn instance setup for local ' \
                           'development, If not read https://extranet.atlassian.com/x/kpOddQ'

    def __call__(self, args, executor):
        if args.quick_mode:
            if args.skip_build:
                return executor.append(
                    lambda logger: logger.error('Cannot use --quick-sync with --skip-build!') or Callable.failure)

            self.prepare_quick_mode(args, executor)
        else:
            self.prepare_standard_mode(args, executor)

        # restart JIRA
        executor.append(
            SystemCallable(args,
                           'ssh {host} "'
                           # stop JIRA
                           'svc -d {service} '
                           '&& svc -k {service} '
                           # remove tomcat cache
                           '&& {{ rm -rf {clean_paths} || true; }} '  # || true to ignore meaningless errors ;)
                           # start JIRA
                           '&& svc -u {service}"'.format(
                               host=args.deploy_host, service='/service/j2ee_jira',
                               clean_paths='/data/service/j2ee_jira/catalina-base/work/Catalina/localhost/_/org/apache/jsp'
                           )))

    def check_for_plugin_updates(self, args, logger):
        modules_to_build = self.plugins_utility.find_plugins_to_recompile([ONDEMAND_PROFILE])
        if len(modules_to_build) == 0:
            return Callable.success
        else:
            logger.error('Changes in plugins detected please use standard deploy')
            return Callable.do_not_proceed if not args.force else Callable.success

    def prepare_quick_mode(self, args, executor):
        # the QUICK mode is on!

        def do_we_need_jira_api(logger):
            logger.info('Checking if jira-api should be synced')

            # sync jira-api package
            jira_api_jars_path = os.sep.join(['jira-components', 'jira-api', 'target', 'jira-api-*.jar'])
            jars = [f for f in glob.glob(jira_api_jars_path) if not re.match('.*sources.jar', f)]
            if len(jars) > 1:
                logger.error('Found more than one jar - expected to find only one, exiting. Found jars: %s' % jars)
                return ConditionalCallable.Result.return_value(Callable.failure)
            elif len(jars) < 1:
                # no jira-api jar? compile again
                logger.warn('JIRA api jar was not found - quite unexpected for me. Did you run clean? Continuing, but '
                            'do expect failures!')
                return ConditionalCallable.Result.execute_task()
            else:
                # check if something has been recompiled
                api_jar = jars[0]

                changed_files_output = subprocess.check_output(
                    ['find', 'jira-components/jira-api/target/classes/', '-newer', '%s' % api_jar],
                    universal_newlines=True
                )

                if changed_files_output.strip() == '':
                    logger.debug('Nothing changed, no sync is required')
                    # nothing changed, ignore jira-api
                    return ConditionalCallable.Result.return_value(Callable.success)
                else:
                    logger.info('JIRA API changes detected, rebuilding')
                    return ConditionalCallable.Result.execute_task()

        # build JIRA-API
        mvn_build_jira_api = MavenCallable(args) \
            .phase('package') \
            .property('maven.test.skip') \
            .project('jira-components/jira-api') \
            .option('-am')

        rsync_jira_api = SystemCallable(
            args,
            'rsync -Ocazv --delete --progress --exclude jira-api-*-sources.jar {0} {1}:{2}'.format(
                'jira-components/jira-api/target/jira-api-*.jar',
                args.deploy_host,
                '/data/studio_local/jira/WEB-INF/lib/'
            )
        )

        executor.append(functools.partial(self.check_for_plugin_updates, args))
        update_jira_api = CombinedCallable(args, [mvn_build_jira_api, rsync_jira_api])
        executor.append(ConditionalCallable(args, update_jira_api, do_we_need_jira_api))

        # sync classes
        executor.append(SystemCallable(args, self.quick_sync_cmd(args.deploy_host)))

    def prepare_standard_mode(self, args, executor, plugins_utility: BundledPluginsUtility=BundledPluginsUtility(OD_BUNDLED_PLUGINS)):
        if args.skip_build:
            executor.append(lambda logger: logger.info('Skip build option activated!') or Callable.success)

        # prepare instance for deploy
        prepare_instance_for_deploy_script_path = os.path.join(
            self.file_utils.dirname(__file__), 'ondemand/remote/prepare_instance_for_deploy.py')

        executor.append(RemotePython26ScriptCallable(
            hostname=args.deploy_host, script_file=prepare_instance_for_deploy_script_path))

        # build OnDemand webapp
        build_jira_maven = MavenCallable(args) \
            .phase('package') \
            .property('maven.test.skip') \
            .project('jira-ondemand-project/jira-ondemand-webapp') \
            .option('-am') \
            .profile(ONDEMAND_PROFILE)

        if not args.skip_build:
            executor.append(build_jira_maven)
            executor.append(plugins_utility.get_plugins_task([ONDEMAND_PROFILE]))
            executor.append(lambda logger: plugins_utility.create_agregate_list())

        # execute rsync
        executor.append(SystemCallable(args, 'rsync -Ocazv --delete --progress {src} {host}:{dst}'.format(
            src='jira-ondemand-project/jira-ondemand-webapp/target/war/exploded/',
            host=args.deploy_host,
            dst='/data/studio_local/jira/'
        )))

    def build_excludes(self, excludes):
        return ['--exclude ' + e for e in excludes]

    def quick_sync_cmd(self, deploy_host):
        cmd = ['rsync', '--delete', '-Ocav']
        cmd += self.build_excludes([
            'rebel.xml',
            'workflows.xml',
            'osworkflow.xml',
            'jira-workflow.xml',
            'studio.default.properties',  # OD webapp produces this file
            'package.html',
            'META-INF',
            'com/atlassian/jira/screenshot/applet/*'  # applet is re-packaged into plugin and ignored in webapp
        ])
        cmd += [
            # sources - order is important here!
            'jira-ondemand-project/jira-ondemand-webapp/target/classes/',
            'jira-components/jira-core/target/classes/',
            'jira-components/jira-webapp-common/target/jira/WEB-INF/classes/',
            # destination
            '%s:/data/studio_local/jira/WEB-INF/classes' % deploy_host
        ]
        return ' '.join(cmd)

    def define_parser(self, parser):
        MavenCallable.add_maven_switches(parser)
        parser.add_argument('deploy_host', help='host to deploy the fireball to')
        parser.add_argument('--force', help='Ignores errors when plugins have changed',action='store_true', dest='force')
        parser.add_argument('--skip-build',
                            help='Do not build JIRA - useful when you want deploy the same version to another instance.'
                                 ' Not supported in quick sync mode!',
                            action='store_true', dest='skip_build')
        parser.add_argument('--quick-sync',
                            help='This puts deploy into quick mode which builds only jira-api jar and synchronizes jira-core classes. '
                                 'It''s required to run normal deploy before using this option.',
                            action='store_true', dest='quick_mode')


class OnDemand(JmakeModule):
    def __init__(self):
        super().__init__()
        self.command = 'ondemand'
        self.description = 'Aids in building, running, DoTing and deploying JIRA in OnDemand mode. '

    def get_submodules(self):
        return [OnDemandInstall(), OnDemandRun(), OnDemandDeploy(), OnDemandDebug()]

