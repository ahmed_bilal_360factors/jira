from argparse import ArgumentParser
from json import JSONEncoder, JSONDecoder
from CommandExecutor import Callable
from Logger import LOG
from module import JmakeModule
from utils.FileUtils import FileUtils

JMAKE_RULES_FILE = '.jmake.rules.json'


class NoSuchRuleException(Exception):
    pass


class Rule:
    def __init__(self, params, add_params):
        self.params = params
        self.add_params = add_params

    def __str__(self):
        return ' => '.join(('"' + ' '.join(x) + '"' for x in [self.params, self.add_params]))

    def __repr__(self):
        return self.__str__()

class RulesStore:
    def __init__(self, fs:FileUtils = FileUtils()):
        self.rules = []
        self.fs = fs

    def __iter__(self):
        for rule in self.rules:
            yield rule

    def find_rule_exact(self, params):
        params_set = set(params)
        for rule in self:
            if params_set == set(rule.params):
                return rule

        raise NoSuchRuleException()

    def find_rules_containing(self, params):
        for rule in self:
            if all(p in params for p in rule.params):
                yield rule

    def add_rule(self, params, add_params):
        try:
            rule = self.find_rule_exact(params)
            rule.add_params = add_params
        except NoSuchRuleException:
            self.rules.append(Rule(params, add_params))

        return self

    def get_add_params(self, params):

        for p in self.___get_additional_params(params, set()):
            yield p


    def ___get_additional_params(self, params, exclude_from_result):

        skip_set = set(exclude_from_result)

        for rule in self.find_rules_containing(params):
            for param in rule.add_params:
                if param not in skip_set:
                    yield param
                    skip_set.add(param)

    def merge_add_params(self, params):

        for p in params:
            yield p

        for p in self.___get_additional_params(params, params):
            yield p


    def remove_rule(self, rule):
        self.rules.remove(rule)
        return self

    def import_from_dicts(self, data):
        try:
            self.rules = [Rule(d["to"], d["add"]) for d in data]
        except Exception as e:
            LOG.warn('Could not use rules data. Will not use workspace rules.')
            LOG.warn(str(e))

    def load_from_file(self):
        if self.fs.file_exists(JMAKE_RULES_FILE):
            json_string = ''
            with open(JMAKE_RULES_FILE, 'r') as f:
                json_string = f.read().strip()
            if json_string:
                data = JSONDecoder().decode(json_string)

                self.import_from_dicts(data)
        return self

    def save(self):
        data = [{"to" : r.params, "add": r.add_params} for r in self.rules]
        with open(JMAKE_RULES_FILE, 'w+') as f:
            f.write(JSONEncoder().encode(data))
        return self


class Defaults(JmakeModule):

    def __init__(self):
        super().__init__()
        self.command = 'workspace-defaults'
        self.description = '''
            Jmake allows to store per-workspace defaults for any calls. This may be handy if you are working on
            more workspaces and you'd like to assign default ports/jirahomes etc. to different JIRAs jmake starts,
            or if you are working on a feature that needs a specific set of switches. See examples below.
            By using the --set-default parameter you tell jmake then whenever you use what is on the left,
            jmake you mean to also use whatever is on the right. You may or may not use thee options and jmake
            will attempt to add the missing bits in there.
            Rules are matched using a partial match, so if you define one for "debug" and another for "ondemand debug"
            and then use "ondemand debug", both will be applied.
        '''
        self.check_branch = False

    def __call__(self, args, executor):
        def print_rules_closure(log):

            log.info('Existing rules:')
            for rule in RulesStore().load_from_file():
                log.info(str(rule))

            log.info('')
            log.info('To add a rule: ')
            log.info(' $ ./jmake [trigger_options...] --set-default [additional_options...]')
            log.info('To remove a rule, leave additional_options empty: ')
            log.info(' $ ./jmake [trigger_options...] --set-default')
            log.info('To remove all rules, delete the rules file: ')
            log.info(' $ rm ' + JMAKE_RULES_FILE)

            return Callable.success

        executor.append(print_rules_closure)

    def define_parser(self, parser: ArgumentParser):

        self.sample_usage(parser, 'When working on emails:',
                          [' $ ./jmake run --set-default --enable-email',
                           ' $ ./jmake debug --set-default --enable-email'])

        self.sample_usage(parser, 'To force a specific port in case we are running ssl:',
                          [' $ ./jmake debug --ssl --set-default --jira-port 8443'])

        self.sample_usage(parser, 'Use ssl always when debugging, when debugging ondemand, use blobstore too:',
                          [' $ ./jmake debug --set-default --ssl',
                           ' $ ./jmake ondemand debug --set-default --blobstore'])


