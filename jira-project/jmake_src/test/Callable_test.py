import os
from unittest import TestCase
import unittest
from Logger import Logger
from CommandExecutor import Callable, SystemCallable, GenericTaskCallable, ConditionalCallable, CombinedCallable, \
    RemotePython26ScriptCallable
from utils.FileUtils import MockFile
from utils.Mocks import Mock

class CallableTest(TestCase):
    def test_callable_stores_args(self):
        args = {'yes': 'it', 'does': 'store them'}
        callable = Callable(args)
        self.assertDictEqual(args, callable.args)


class MockedSubProcess(Mock):
    def __init__(self, trace, returncode):
        super().__init__(pid = 1234,
            trace = trace,
            trace_ptr = 0,
            trace_line_ptr = 0,
            stdout = self,
            returncode = returncode)

    def read(self, numbytes : int):
        if not numbytes == 1:
            raise ValueError('Cannot use this mock with reading differently than 1 byte at a time')
        if self.trace_ptr == len(self.trace):
            return b''
        if self.trace_line_ptr == len(self.trace[self.trace_ptr]):
            self.trace_line_ptr = 0
            self.trace_ptr += 1
            return b'\n'

        text = self.trace[self.trace_ptr][self.trace_line_ptr:self.trace_line_ptr + numbytes]
        self.trace_line_ptr += numbytes
        return bytes(text, encoding='utf-8')

    def readline(self):
        if self.trace_ptr == len(self.trace):
            return b''
        line = self.trace[self.trace_ptr][self.trace_line_ptr:] + '\n'
        self.trace_ptr += 1
        self.trace_line_ptr = 0
        return bytes(line, encoding='utf-8')

    def poll(self):
        return None if self.trace_ptr < len(self.trace) else self.trace_ptr


class MockedSystemCallable(SystemCallable):
    def __init__(self, command, subprocess):
        super().__init__({}, command)
        self.subprocess = subprocess
        self.output_notifications = []

    def _SystemCallable__execute(self, command, logger, env=os.environ):
        self.called_with_env = env
        self.called_command = command
        return self.subprocess

    def is_killed(self):
        return self.subprocess.verify_kill()

    def process_output(self, logger: Logger, line : str, num : int):
        self.output_notifications.append(line)


class SystemCallableTest(TestCase):

    def test_catching_callable_line(self):
        trace = ['line1', 'line2', 'line3 which is very long to test maven:cli hackery', '4', 'end']
        subprocess = MockedSubProcess(trace, 0)
        call = MockedSystemCallable('some', subprocess)

        rc = call(Mock())

        self.assertListEqual(trace, call.output_notifications)

    def test_calling_command_forwards_system_return_code(self):
        command = 'my command'
        call = MockedSystemCallable(command, MockedSubProcess([], 6667))
        rc = call(Mock())

        self.assertEqual(rc, 6667)
        self.assertEqual(command, call.called_command)

    def test_using_env_member(self):
        env = os.environ.copy()
        env.clear()
        env['MY_ENV'] = 987
        call = MockedSystemCallable('some', MockedSubProcess([], 0))
        call.env = env

        call(Mock())
        self.assertDictEqual(call.called_with_env, env)

    def test_catching_sigint_causes_killing_spawned_process_and_halts_jmake(self):
        subprocess = MockedSubProcess(['some output', 'some more output'], 0)
        call = MockedSystemCallable('some', subprocess)

        def read(num_bytes):
            if not subprocess.verify_kill():
                #noinspection PyUnresolvedReferences
                call._SystemCallable__signal_handler(subprocess, None, None)
            raise IOError

        subprocess.read = read

        rc = call(Mock())
        self.assertEqual(True, call.is_killed())
        self.assertEqual(Callable.do_not_proceed, rc)

    def test_dry_runs(self):
        key = 'JMAKE_DRY'
        command = 'ls / | just_testing -rf ;something else'
        call = MockedSystemCallable(command, MockedSubProcess([], 0))
        call.env = dict(os.environ)
        call.env[key] = '1'
        call(Mock())

        self.assertEqual(call.called_command, "echo '%s'" % command)

    def test_that_returncode_field_exists(self):

        call = MockedSystemCallable('a-command', MockedSubProcess([], 1234))
        call(Mock())
        self.assertTrue(hasattr(call, 'returncode'))
        self.assertEqual(call.returncode, 1234)


class GenericTaskCallableTest(TestCase):
    def test_generic_task_callable_executes_task(self):
        fake_logger = "FakeLoggerObj"
        fake_args = "FakeArgsObj"

        test_task_holder = Mock()
        test_task_holder.ordered_test_task(fake_args, fake_logger, toReturn=None)

        sut = GenericTaskCallable(fake_args, test_task_holder.test_task)
        sut(fake_logger)

        test_task_holder.verify_all_ordered()


class ConditionalCallableTest(TestCase):
    def test_conditional_callable_executes_task_when_execute_task_was_used(self):
        fake_logger = "FakeLoggerObj"
        fake_args = "FakeArgsObj"
        expected_result = 666

        mock = Mock()
        mock.expect_test_condition(fake_logger, toReturn=ConditionalCallable.Result.execute_task())
        mock.expect_test_task(fake_logger, toReturn=expected_result)

        sut = ConditionalCallable(fake_args, mock.test_task, mock.test_condition)
        result = sut(fake_logger)

        self.assertEquals(result, expected_result)

    def test_conditional_callable_return_give_value_when_return_value_was_used(self):
        fake_logger = "FakeLoggerObj"
        fake_args = "FakeArgsObj"
        expected_result = 666

        mock = Mock()
        mock.expect_test_condition(fake_logger, toReturn=ConditionalCallable.Result.return_value(expected_result))

        sut = ConditionalCallable(fake_args, None, mock.test_condition)
        result = sut(fake_logger)

        self.assertEquals(result, expected_result)


class CombinedCallableTest(TestCase):
    def test_combined_callable_executes_all_tasks(self):
        fake_logger = "FakeLoggerObj"

        mock = Mock()
        mock.ordered_test_task_1(fake_logger, toReturn=Callable.success)
        mock.ordered_test_task_2(fake_logger, toReturn=Callable.success)
        mock.ordered_test_task_3(fake_logger, toReturn=Callable.success)

        sut = CombinedCallable(None, [mock.test_task_1, mock.test_task_2, mock.test_task_3])
        result = sut(fake_logger)

        mock.verify_all_ordered()
        self.assertEqual(result, Callable.success)

    def test_combined_callable_stops_on_non_successful_tasks(self):
        fake_logger = "FakeLoggerObj"

        mock = Mock()
        mock.ordered_test_task_1(fake_logger, toReturn=Callable.do_not_proceed)

        sut = CombinedCallable(None, [mock.test_task_1, self._fail_if_called("CombinedCallable should skip this task")])
        result = sut(fake_logger)

        mock.verify_all_ordered()
        self.assertEqual(result, Callable.do_not_proceed)

    def test_combined_callable_should_return_value_from_task(self):
        fake_logger = "FakeLoggerObj"

        mock = Mock()
        mock.ordered_test_task_1(fake_logger, toReturn=Callable.success)
        mock.ordered_test_task_2(fake_logger, toReturn=Callable.do_not_proceed)

        sut = CombinedCallable(None, [mock.test_task_1, mock.test_task_2])
        result = sut(fake_logger)

        mock.verify_all_ordered()
        self.assertEqual(result, Callable.do_not_proceed)

    def _fail_if_called(self, msg):
        def fail_if_failed(*args, **kwargs):
            self.fail(msg)
        return fail_if_failed


class RemotePython26ScriptCallableTest(TestCase):
    def test_remote_python_26_script_callable_calls_ssh_with_proper_args(self):
        mock_logger = Mock()
        mock_file_utils = Mock()
        mock_process_utils = Mock()
        hostname = 'test.jira-dev.com'
        script_file = 'some_test_file'
        script_file_contents = 'some script'
        process_output = 'output from process'
        fake_file = MockFile(script_file_contents)

        mock_file_utils.expect_open(script_file, 'rb', encoding=None, toReturn=fake_file)

        mock_process_utils.expect_execute_read_input(
            ['ssh', hostname, 'cat - | python26'], script_file_contents, toReturn=process_output)

        mock_logger.ordered_debug(
            'Output from remote host:\n<<<BEGIN>>>\n%s\n<<<END>>>' % process_output, toReturn=None)

        sut = RemotePython26ScriptCallable(
            hostname, script_file, file_utils=mock_file_utils, process_utils=mock_process_utils)

        result = sut(mock_logger)

        mock_logger.verify_all_ordered()

        self.assertEqual(result, Callable.success)


if __name__ == '__main__':
    unittest.main(verbosity=2)