from unittest import TestCase
from JmakeRules import RulesStore, NoSuchRuleException

def as_dict(add, to):
    return {
        'add': add,
        'to': to
    }

class JmakeRulesTest(TestCase):

    def setUp(self):
        self.rules = RulesStore()

    def test_store_behaves_when_empty(self):
        self.assertRaises(NoSuchRuleException, self.rules.find_rule_exact, ['debug'])
        self.assertEqual([], list(self.rules.find_rules_containing(['debug'])))


    def debug_data(self):
        self.rules.import_from_dicts([
            as_dict(['-p', '1234'], ['debug']),
            as_dict(['--ssl', '--blobstore'], ['ondemand', 'debug']),
            as_dict(['--instance-name', 'snoflake'], ['ondemand', 'debug', 'quickstart'])])

    def test_store_match_exactly(self):
        self.debug_data()
        self.assertEqual(['--ssl', '--blobstore'], self.rules.find_rule_exact(['ondemand', 'debug']).add_params)

    def test_store_merge_add_params(self):
        self.debug_data()
        self.assertEqual(
            'ondemand debug -p 1234 --ssl --blobstore'.split(' '),
            list(self.rules.merge_add_params(['ondemand', 'debug']))
        )

    def test_store_get_add_params(self):
        self.debug_data()
        self.assertEqual(
            '-p 1234 --ssl --blobstore'.split(' '),
            list(self.rules.get_add_params(['ondemand', 'debug']))
        )

    def test_store_merge_add_params_does_not_repeat_used_params(self):
        self.debug_data()
        self.assertEqual(
            'ondemand debug --ssl -p 1234 --blobstore'.split(' '),
            list(self.rules.merge_add_params('ondemand debug --ssl'.split(' ')))
        )








