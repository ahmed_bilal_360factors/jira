# jmake

In a nutshell, jmake is the tool to build, run, and measure JIRA. It abstracts many complex Maven tasks required to work efficiently in JIRA source code.

**Looking for jmake general help? This is not the right place.** Simply run `./jmake` without any arguments.

## Before you Begin

If you are reading this I'd assume you want to extend or fully understand the inner workings of jmake.

Watch [this recorded brownbag session](https://extranet.atlassian.com/pages/viewpage.action?pageId=2208530545) (or go directly to the slides) to jump start your overall understanding of jmake.

## Running tests locally

Simply use `./jmake jm-unit-test` to run all unit tests especially created for jmake.

## Bamboo plans

Bamboo plans are here: [JBAC - CI jmake](https://jira-bamboo.internal.atlassian.com/browse/MASTERTWO-CIJMAKE). Use [branchinator](https://jira-bamboo.internal.atlassian.com/branchinator) to start a build for your branch.

## Issue tracking

Found a new issue that could be fixed later? Raise it here: [JDEV](https://jdog.jira-dev.com/projects/JDEV)