@echo on
set SETTINGSFILE=../settings.xml
set LOCALREPO=../localrepo

call mvn3.bat -X eclipse:eclipse -s %SETTINGSFILE% -Dmaven.repo.local=%cd%\%LOCALREPO% %* 
if %errorlevel% neq 0 exit /b %errorlevel%
