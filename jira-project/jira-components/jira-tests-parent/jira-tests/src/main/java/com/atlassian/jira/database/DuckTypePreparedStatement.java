package com.atlassian.jira.database;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;

import com.atlassian.jira.util.DuckTypeProxyFactory;

/**
 * Mocks out some of PreparedStatement for use as a Duck Type proxy.
 *
 * @since v6.4
 */
@SuppressWarnings ("UnusedDeclaration")
public class DuckTypePreparedStatement
{
    private static final Object NULL = new Object();
    private final DuckTypeConnection mockConnection;
    private final String sql;
    private Map<Integer, Object> params = new HashMap<Integer, Object>();

    public DuckTypePreparedStatement(final DuckTypeConnection mockConnection, final String sql)
    {
        this.mockConnection = mockConnection;
        this.sql = sql;
    }
    // -----------------------------------------------------------------------------------------------------------------
    // Implemented Methods
    // -----------------------------------------------------------------------------------------------------------------

    public ResultSet executeQuery()
    {
        return DuckTypeProxyFactory.newStrictProxyInstance(ResultSet.class, new DuckTypeResultSet(this));
    }

    public int executeUpdate()
    {
        final String boundQuery = bindVariables(sql);
        return mockConnection.getUpdateResults(boundQuery);
    }

    public void close()
    {
        // OK
    }

    public void setNull(final int parameterIndex, final int sqlType)
    {
        params.put(parameterIndex, NULL);
    }

    public void setBoolean(final int parameterIndex, final boolean x)
    {
        params.put(parameterIndex, x);
    }

    public void setByte(final int parameterIndex, final byte x)
    {
        params.put(parameterIndex, x);
    }

    public void setShort(final int parameterIndex, final short x)
    {
        params.put(parameterIndex, x);
    }

    public void setInt(final int parameterIndex, final int x)
    {
        params.put(parameterIndex, x);
    }

    public void setLong(final int parameterIndex, final long x)
    {
        params.put(parameterIndex, x);
    }

    public void setFloat(final int parameterIndex, final float x)
    {
        params.put(parameterIndex, x);
    }

    public void setDouble(final int parameterIndex, final double x)
    {
        params.put(parameterIndex, x);
    }

    public void setBigDecimal(final int parameterIndex, final BigDecimal x)
    {
        params.put(parameterIndex, x);
    }

    public void setString(final int parameterIndex, final String x)
    {
        params.put(parameterIndex, x);
    }

    public void setBytes(final int parameterIndex, final byte[] x)
    {
        params.put(parameterIndex, x);
    }

    public void setDate(final int parameterIndex, final Date x)
    {
        params.put(parameterIndex, x);
    }

    public void setTime(final int parameterIndex, final Time x)
    {
        params.put(parameterIndex, x);
    }

    public void setTimestamp(final int parameterIndex, final Timestamp x)
    {
        params.put(parameterIndex, x);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Housekeeping Methods
    // -----------------------------------------------------------------------------------------------------------------

    @Nonnull
    public Iterable<ResultRow> getExpectedResults()
    {
        final String boundQuery = bindVariables(sql);
        final Iterable<ResultRow> results = mockConnection.getQueryResults(boundQuery);
        if (results == null)
            throw new IllegalStateException("No expectation set up for query: \n" + boundQuery);
        return results;
    }

    private String bindVariables(final String sql)
    {
        StringBuilder sb = new StringBuilder();
        int count = 0;
        for (char ch : sql.toCharArray())
        {
            if (ch == '?')
            {
                sb.append(getBindVariable(++count));
            }
            else
            {
                sb.append(ch);
            }
        }
        return sb.toString();
    }

    private String getBindVariable(final int index)
    {
        final Object val = params.get(index);
        if (val == null)
        {
            if (!params.containsKey(index))
            {
                throw new IllegalStateException("Unbound variable #" + index);
            }
            return "NULL";
        }
        if (val instanceof String)
        {
            return "'" + val + "'";
        }
        else
        {
            return val.toString();
        }
    }
}
