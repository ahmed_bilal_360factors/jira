package com.atlassian.jira.permission;

import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.conditions.AlwaysDisplayCondition;

/**
 * @since v6.3
 */
public class MockProjectPermission implements ProjectPermission
{
    private final String key;
    private final String nameKey;
    private final String descriptionKey;
    private final ProjectPermissionCategory category;
    private final Condition condition;
    private final ProjectPermissionKey projectPermissionKey;

    public MockProjectPermission(String key, String nameKey, String descriptionKey, ProjectPermissionCategory category)
    {
        this.key = key;
        this.projectPermissionKey = new ProjectPermissionKey(key);
        this.nameKey = nameKey;
        this.descriptionKey = descriptionKey;
        this.category = category;
        this.condition = new AlwaysDisplayCondition();
    }

    public String getKey()
    {
        return key;
    }

    @Override
    public ProjectPermissionKey getProjectPermissionKey()
    {
        return projectPermissionKey;
    }

    public String getNameI18nKey()
    {
        return nameKey;
    }

    public String getDescriptionI18nKey()
    {
        return descriptionKey;
    }

    public ProjectPermissionCategory getCategory()
    {
        return category;
    }

    public Condition getCondition()
    {
        return condition;
    }
}
