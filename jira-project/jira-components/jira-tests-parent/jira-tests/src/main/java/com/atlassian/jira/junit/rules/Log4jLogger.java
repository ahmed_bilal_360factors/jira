package com.atlassian.jira.junit.rules;

import java.io.StringWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.WriterAppender;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Watches log4j logs. This can be used to spy on log4j or slf4j when testing.
 *
 * @since 6.4
 */
public class Log4jLogger extends TestWatcher
{
    private final String category;
    private WriterAppender writerAppender;
    private StringWriter writer;

    /**
     * Creates a watcher of the given category
     * @param category the category to watch
     */
    public Log4jLogger(final String category)
    {
        this.category = category;
    }

    /**
     * Creates a watcher of the root category
     */
    public Log4jLogger()
    {
        this("");
    }

    /**
     * Creates a watcher of the given class category
     * @param clazz the class category to watch
     */
    public Log4jLogger(final Class<?> clazz)
    {
        this(clazz.getName());
    }

    @Override
    protected void starting(final Description description)
    {
        writer = new StringWriter();
        writerAppender = new WriterAppender(new SimpleLayout(), writer);
        final Logger logger = getLogger();
        logger.setLevel(Level.ALL);
        logger.addAppender(writerAppender);
    }

    private Logger getLogger()
    {

        return StringUtils.isBlank(category) ?
                Logger.getRootLogger() :
                Logger.getLogger(category);
    }

    @Override
    protected void finished(final Description description)
    {
        Logger.getRootLogger().removeAppender(writerAppender);
    }

    public String getMessage()
    {
        return writer.toString();
    }

    public void reset()
    {
        writer = new StringWriter();
        writerAppender.setWriter(writer);
    }

    public void setLevel(final Level level)
    {
        getLogger().setLevel(level);
    }
}
