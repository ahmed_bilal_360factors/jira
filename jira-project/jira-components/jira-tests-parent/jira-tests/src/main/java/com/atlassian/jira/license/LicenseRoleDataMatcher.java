package com.atlassian.jira.license;

import com.atlassian.fugue.Option;
import com.google.common.collect.Sets;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import java.util.Collections;
import java.util.Set;

public class LicenseRoleDataMatcher extends TypeSafeMatcher<LicenseRoleStore.LicenseRoleData>
{
    private Set<String> groups = Collections.emptySet();
    private Option<String> primaryGroup = Option.none();
    private LicenseRoleId id;

    public LicenseRoleDataMatcher match(LicenseRoleStore.LicenseRoleData data)
    {
        this.id = data.getId();
        this.groups = data.getGroups();
        this.primaryGroup = data.getPrimaryGroup();

        return this;
    }

    public LicenseRoleDataMatcher merge(LicenseRole role)
    {
        id(role.getId());
        groups(role.getGroups());
        primaryGroup(role.getPrimaryGroup());

        return this;
    }

    public LicenseRoleDataMatcher id(String id)
    {
        this.id = LicenseRoleId.valueOf(id);
        return this;
    }

    public LicenseRoleDataMatcher id(LicenseRoleId id)
    {
        this.id = id;
        return this;
    }

    public LicenseRoleDataMatcher groups(Iterable<String> groups)
    {
        this.groups = Sets.newHashSet(groups);
        return this;
    }

    public LicenseRoleDataMatcher groups(String...groups)
    {
        this.groups = Sets.newHashSet(groups);
        return this;
    }

    public LicenseRoleDataMatcher primaryGroup(String defaultGroup)
    {
        this.primaryGroup = Option.option(defaultGroup);
        return this;
    }

    public LicenseRoleDataMatcher primaryGroup(Option<String> defaultGroup)
    {
        this.primaryGroup = defaultGroup;
        return this;
    }

    @Override
    protected boolean matchesSafely(final LicenseRoleStore.LicenseRoleData licenseRoleData)
    {
        return licenseRoleData.getId().equals(id)
                && licenseRoleData.getGroups().equals(groups)
                && licenseRoleData.getPrimaryGroup().equals(primaryGroup);
    }

    @Override
    public void describeTo(final Description description)
    {
        description.appendText(String.format("[%s, %s, %s]", id, groups, primaryGroup));
    }

    @Override
    protected void describeMismatchSafely(final LicenseRoleStore.LicenseRoleData item, final Description mismatchDescription)
    {
        mismatchDescription.appendText(String.format("[%s, %s, %s]", item.getId(), item.getGroups(), item.getPrimaryGroup()));
    }
}
