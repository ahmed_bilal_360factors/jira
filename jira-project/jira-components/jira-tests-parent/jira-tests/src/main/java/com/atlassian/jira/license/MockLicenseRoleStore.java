package com.atlassian.jira.license;

import com.atlassian.fugue.Option;
import com.google.common.collect.Maps;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nonnull;

import static java.util.Arrays.asList;

public class MockLicenseRoleStore implements LicenseRoleStore
{
    private final Map<LicenseRoleId, LicenseRoleData> data = Maps.newHashMap();

    @Nonnull
    @Override
    public LicenseRoleData get(@Nonnull final LicenseRoleId id)
    {
        final LicenseRoleData licenseRoleData = data.get(id);
        if (licenseRoleData == null)
        {
            return new LicenseRoleData(id, Collections.<String>emptyList(), Option.<String>none());
        }
        else
        {
            return licenseRoleData;
        }
    }

    @Nonnull
    @Override
    public LicenseRoleData save(@Nonnull final LicenseRoleData data)
    {
        this.data.put(data.getId(), data);
        return data;
    }


    @Nonnull
    @Override
    public void removeGroup(@Nonnull String groupName)
    {
        for (Map.Entry<LicenseRoleId,LicenseRoleData> e : data.entrySet())
        {
            // whether groupName is actually in the set of groups for LicenseRoleData
            // doesn't matter, as #get can return an empty collection of groups.
            Set<String> eGroups = e.getValue().getGroups();
            if(e.getValue().getGroups().contains(groupName))
            {
                HashSet<String> newGroup = new HashSet<String>(eGroups);
                newGroup.remove(groupName);
                data.put(e.getValue().getId(), new LicenseRoleData(e.getValue().getId(),newGroup,e.getValue().getPrimaryGroup()));
            }
        }
    }


    public MockLicenseRoleStore save(LicenseRoleId id, String... groups)
    {
        save(new LicenseRoleData(id, asList(groups), Option.none(String.class)));
        return this;
    }
}
