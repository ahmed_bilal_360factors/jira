package com.atlassian.jira.database;

import java.util.Arrays;

/**
 * @since v6.4
 */
public class ResultRow
{
    private final Object[] columnValue;

    public ResultRow(Object... columnValue)
    {
        this.columnValue = columnValue;
    }

    public Object getObject(final int columnIndex)
    {
        if (columnIndex > columnValue.length)
        {
            return null;
        }
        return columnValue[columnIndex - 1];
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final ResultRow resultRow = (ResultRow) o;

        if (!Arrays.equals(columnValue, resultRow.columnValue)) { return false; }

        return true;
    }

    @Override
    public int hashCode()
    {
        return columnValue != null ? Arrays.hashCode(columnValue) : 0;
    }
}
