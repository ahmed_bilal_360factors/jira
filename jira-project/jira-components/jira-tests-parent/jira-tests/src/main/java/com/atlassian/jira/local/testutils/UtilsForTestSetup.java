package com.atlassian.jira.local.testutils;

import java.sql.Connection;
import java.util.Collection;
import java.util.Collections;

import com.atlassian.core.ofbiz.CoreFactory;
import com.atlassian.core.ofbiz.util.CoreTransactionUtil;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.JdbcDatasource;

import org.hsqldb.jdbcDriver;
import org.ofbiz.core.entity.GenericDelegator;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.model.ModelEntity;
import org.ofbiz.core.entity.model.ModelReader;
import org.ofbiz.core.entity.model.ModelViewEntity;

public class UtilsForTestSetup
{
    public static void deleteAllEntities() throws GenericEntityException
    {
        loadDatabaseDriver();
        CoreTransactionUtil.setUseTransactions(true);
        CoreTransactionUtil.setIsolationLevel(Connection.TRANSACTION_READ_UNCOMMITTED);
        final boolean beganTransaction = CoreTransactionUtil.begin();
        try
        {
            final GenericDelegator delegator = CoreFactory.getGenericDelegator();
            final ModelReader reader = delegator.getModelReader();
            final Collection<String> entityNames = reader.getEntityNames();
            for (final String entityName : entityNames)
            {
                final ModelEntity modelEntity = delegator.getModelReader().getModelEntity(entityName);
                // Delete only normal (non-view) entities
                if (!(modelEntity instanceof ModelViewEntity))
                {
                    delegator.removeByAnd(entityName, Collections.EMPTY_MAP);
                }
            }
        }
        finally
        {
            CoreTransactionUtil.commit(beganTransaction);
        }
    }

    public static void loadDatabaseDriver()
    {
        // referencing org.hsqldb.jdbcDriver causes a static block to run that registers the hsql driver
        // with the java.sql.DriverManager. If the driver doesn't get registered then apache.commons.dbcp.DriverManagerConnectionFactory
        // can't create any connections when ofbiz's DBCPConnectionFactory wants one. This really only needs to be
        // done once per invocation.
        new jdbcDriver();
    }

    public static DatabaseConfig getDatabaseConfig()
    {
        final JdbcDatasource datasource = new JdbcDatasource("jdbc:hsqldb:mem:jiradb", "org.hsqldb.jdbcDriver", "sa", "", 10, null, 4000L, 5000L);
        return new DatabaseConfig("mysql", "PUBLIC", datasource);
    }

}
