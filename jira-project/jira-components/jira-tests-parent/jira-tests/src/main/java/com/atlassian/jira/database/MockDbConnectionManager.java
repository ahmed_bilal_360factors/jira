package com.atlassian.jira.database;

import java.sql.Connection;

import javax.annotation.Nonnull;

import com.atlassian.jira.util.DuckTypeProxyFactory;

import com.mysema.query.sql.SQLTemplates;

/**
 * @since v6.4
 */
public class MockDbConnectionManager implements DbConnectionManager
{
    private final DuckTypeConnection duckTypeConnection;
    private final Connection proxyConnection;

    public MockDbConnectionManager()
    {
        duckTypeConnection = new DuckTypeConnection();
        proxyConnection = DuckTypeProxyFactory.newStrictProxyInstance(Connection.class, duckTypeConnection);
    }

    @Override
    public <T> T executeQuery(@Nonnull final QueryCallback<T> callback)
    {
        return callback.runQuery(new DbConnnectionImpl(proxyConnection, getDialect()));
    }

    @Override
    public void execute(@Nonnull final SqlCallback callback)
    {
        callback.run(new DbConnnectionImpl(proxyConnection, getDialect()));
    }

    @Nonnull
    @Override
    public SQLTemplates getDialect()
    {
        return SQLTemplates.DEFAULT;
    }

    public void setQueryResults(final String sql, final Iterable<ResultRow> expectedResults)
    {
        duckTypeConnection.setQueryResults(sql, expectedResults);
    }

    public void setUpdateResults(String sql, int rowCount)
    {
        duckTypeConnection.setUpdateResults(sql, rowCount);
    }

    public void assertAllExpectedStatementsWereRun()
    {
        duckTypeConnection.assertAllExpectedStatementsWereRun();
    }
}
