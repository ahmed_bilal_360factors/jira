package com.atlassian.jira.mock;

import com.atlassian.jira.mail.settings.MailSettings;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MockMailSettings implements MailSettings
{
    private final Send send;

    public MockMailSettings()
    {
        send = mock(Send.class);
        when(send.isDisabledViaApplicationProperty()).thenReturn(true);
    }

    @Override
    public Fetch fetch()
    {
        return mock(Fetch.class);
    }

    @Override
    public Send send()
    {
        return send;
    }
}
