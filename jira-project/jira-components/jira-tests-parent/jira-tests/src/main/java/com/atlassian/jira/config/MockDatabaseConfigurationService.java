package com.atlassian.jira.config;

public class MockDatabaseConfigurationService implements DatabaseConfigurationService
{
    private String schemaName = "";

    @Override
    public String getSchemaName()
    {
        return schemaName;
    }

    public void setSchemaName(final String schemaName)
    {
        this.schemaName = schemaName;
    }
}
