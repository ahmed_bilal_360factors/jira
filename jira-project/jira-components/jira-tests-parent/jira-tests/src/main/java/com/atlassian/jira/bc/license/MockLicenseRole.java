package com.atlassian.jira.bc.license;

import com.atlassian.fugue.Option;
import com.atlassian.jira.license.LicenseRole;
import com.atlassian.jira.license.LicenseRoleId;
import com.google.common.collect.Sets;

import java.util.Set;
import javax.annotation.Nonnull;

public class MockLicenseRole implements LicenseRole
{
    private LicenseRoleId id;
    private Set<String> groups;
    private String name;
    private Option<String> primaryGroup = Option.none();

    public MockLicenseRole()
    {
        this.groups = Sets.newHashSet();
    }

    public MockLicenseRole(final LicenseRole copy)
    {
        this.id = copy.getId();
        this.groups = Sets.newHashSet(copy.getGroups());
        this.name = copy.getName();
    }

    public MockLicenseRole id(String id)
    {
        this.id = new LicenseRoleId(id);
        return this;
    }

    public MockLicenseRole id(LicenseRoleId id)
    {
        this.id = id;
        return this;
    }

    public MockLicenseRole name(String name)
    {
        this.name = name;
        return this;
    }

    public MockLicenseRole groups(String... groups)
    {
        this.groups = Sets.newHashSet(groups);
        return this;
    }

    public MockLicenseRole groups(Iterable<String> groups)
    {
        this.groups = Sets.newHashSet(groups);
        return this;
    }

    public MockLicenseRole primaryGroup(String group)
    {
        primaryGroup = Option.option(group);
        return this;
    }

    public MockLicenseRole primaryGroup(Option<String> group)
    {
        primaryGroup = group;
        return this;
    }

    @Nonnull
    @Override
    public LicenseRoleId getId()
    {
        return id;
    }

    @Nonnull
    @Override
    public String getName()
    {
        return name;
    }

    @Nonnull
    @Override
    public Set<String> getGroups()
    {
        return groups;
    }

    @Nonnull
    @Override
    public Option<String> getPrimaryGroup()
    {
        return primaryGroup;
    }

    @Nonnull
    @Override
    public LicenseRole withGroups(@Nonnull final Iterable<String> groups, @Nonnull final Option<String> primaryGroup)
    {
        return new MockLicenseRole(this).groups(groups).primaryGroup(primaryGroup);
    }

    public MockLicenseRole copy()
    {
        return new MockLicenseRole(this);
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final MockLicenseRole that = (MockLicenseRole) o;

        //It is intentional that we only look at id.
        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode()
    {
        //It is intentional that we only look at id.
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString()
    {
        return String.format("[name: %s, groups: %s, id: %s, primary %s]", name, groups, id, primaryGroup);
    }
}
