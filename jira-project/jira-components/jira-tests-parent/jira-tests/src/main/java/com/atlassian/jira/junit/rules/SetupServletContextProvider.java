package com.atlassian.jira.junit.rules;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import com.atlassian.jira.web.ServletContextProviderListener;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import static org.mockito.Mockito.mock;

/**
 * Place a mock ServletContext in the ServletContextProvider and clean it out afterwads.
 *
 * @since v6.4
 */
public class SetupServletContextProvider extends TestWatcher
{
    private ServletContext servletContext;
    private ServletContextProviderListener fakeContextProviderListener;

    @Override
    protected void starting(final Description description)
    {
        super.starting(description);

        servletContext = mock(ServletContext.class);
        fakeContextProviderListener = new ServletContextProviderListener();
        fakeContextProviderListener.contextInitialized(new ServletContextEvent(servletContext));
    }

    @Override
    protected void finished(final Description description)
    {
        fakeContextProviderListener.contextDestroyed(new ServletContextEvent(servletContext));

        super.finished(description);
    }
}
