package com.atlassian.jira.util;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Use this Factory to produce "Duck Type" proxies of a given interface with a delegate object that "implements" a
 * subset of the interface methods.
 * <p
 *
 * @since v6.4
 */
public class DuckTypeProxyFactory
{
    /**
     * Creates a new Proxy instance for the given interface.
     * <p>
     *     If any methods are called on the interface that are not defined in the delegate, then it will throw UnsupportedOperationException.
     *
     * @param proxyInterface the interface to create a proxy for.
     * @param delegate the object to delegate to for implementations (quacks like a duck)
     * @param <T> Type of the interface
     * @return a new Proxy instance for the given interface.
     */
    public static <T> T newStrictProxyInstance(final Class<T> proxyInterface, final Object delegate)
    {
        return newProxyInstance(proxyInterface, delegate, true);
    }

    /**
     * Creates a new Proxy instance for the given interface.
     * <p>
     *     If any methods are called on the interface that are not defined in the delegate, then it will return null.
     *
     * @param proxyInterface the interface to create a proxy for.
     * @param delegate the object to delegate to for implementations (quacks like a duck)
     * @param <T> Type of the interface
     * @return a new Proxy instance for the given interface.
     */
    @SuppressWarnings ("UnusedDeclaration")
    public static <T> T newLooseProxyInstance(final Class<T> proxyInterface, final Object delegate)
    {
        return newProxyInstance(proxyInterface, delegate, false);
    }

    private static <T> T newProxyInstance(final Class<T> proxyInterface, final Object delegate, final boolean strict)
    {
        //noinspection unchecked
        return (T) Proxy.newProxyInstance(
                proxyInterface.getClassLoader(),
                new Class[] { proxyInterface },
                new DelegatingInvocationHandler(delegate, strict));
    }

    private static final class DelegatingInvocationHandler implements InvocationHandler
    {
        private final Object delegate;
        private final boolean strict;
        private final Map<MethodSignature, Method> methods = new HashMap<MethodSignature, Method>();

        public DelegatingInvocationHandler(final Object delegate, final boolean strict)
        {
            this.delegate = delegate;
            this.strict = strict;
            for (Method method : delegate.getClass().getMethods())
            {
                final MethodSignature methodSignature = new MethodSignature(method);
                methods.put(methodSignature, method);
            }
        }

        @Override
        public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable
        {
            // find if the delegate implements this method
            final Method delegateMethod = methods.get(new MethodSignature(method));
            if (delegateMethod == null)
            {
                if (strict)
                {
                    throw new UnsupportedOperationException("The delegate '" + delegate.getClass().getSimpleName() + "' does not implement method " + method);
                }
                else
                {
                    return null;
                }
            }

            try
            {
                return delegateMethod.invoke(delegate, args);
            }
            catch (InvocationTargetException ex)
            {
                // rethrow the original exception as thrown by the delegate
                throw ex.getCause();
            }
            catch (IllegalAccessException ex)
            {
                throw new RuntimeException("Unable to access " + delegate.getClass().getName() + " method " + delegateMethod, ex);
            }
        }

        private static final class MethodSignature
        {
            private final String name;
            private final Class<?>[] paramTypes;

            public MethodSignature(final Method method)
            {
                name = method.getName();
                paramTypes = method.getParameterTypes();
            }

            @SuppressWarnings ("RedundantIfStatement")
            @Override
            public boolean equals(final Object o)
            {
                if (this == o) { return true; }
                if (o == null || getClass() != o.getClass()) { return false; }

                final MethodSignature that = (MethodSignature) o;

                if (!name.equals(that.name)) { return false; }
                if (!Arrays.equals(paramTypes, that.paramTypes)) { return false; }

                return true;
            }

            @Override
            public int hashCode()
            {
                return name.hashCode();
            }
        }
    }
}
