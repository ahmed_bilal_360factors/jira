package com.atlassian.jira.database;

import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nonnull;

import com.atlassian.jira.util.DuckTypeProxyFactory;

/**
 * Mocks out some of PreparedStatement for use as a Duck Type proxy.
 *
 * @since v6.4
 */
public class DuckTypeConnection
{
    private Map<String, Iterable<ResultRow>> queryResults = new HashMap<String, Iterable<ResultRow>>();
    private Map<String, Integer> updateResults = new HashMap<String, Integer>();
    private Set<String> queriesRun = new HashSet<String>();

    // -----------------------------------------------------------------------------------------------------------------
    // Implemented Methods
    // -----------------------------------------------------------------------------------------------------------------

    public PreparedStatement prepareStatement(final String sql)
    {
        return DuckTypeProxyFactory.newStrictProxyInstance(PreparedStatement.class, new DuckTypePreparedStatement(this, sql));
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Housekeeping Methods for setting up and verifying expectations
    // -----------------------------------------------------------------------------------------------------------------

    public void setQueryResults(String sql, Iterable<ResultRow> expectedResults)
    {
        queryResults.put(sql, expectedResults);
    }

    @Nonnull
    public Iterable<ResultRow> getQueryResults(final String sql)
    {
        final Iterable<ResultRow> resultRows = queryResults.get(sql);
        if (resultRows == null)
        {
            // As part of our test asserts we strictly only allow SQL that has been registered as expected.
            // This also makes it easy to write tests:
            //  - let the test fail, review the SQL from the error message, and copy/paste
            throw new AssertionError("Unexpected DB call: \n" + sql);
        }

        queriesRun.add(sql);
        return resultRows;
    }

    public void setUpdateResults(String sql, int rowCount)
    {
        updateResults.put(sql, rowCount);
    }

    public int getUpdateResults(final String sql)
    {
        Integer rowCount = updateResults.get(sql);
        if (rowCount == null)
        {
            // for now, always be strict
            throw new AssertionError("Unexpected DB call: \n" + sql);
        }
        else
        {
            queriesRun.add(sql);
            return rowCount;
        }
    }

    public void assertAllExpectedStatementsWereRun()
    {
        assertStatementsRun(queryResults.keySet());
        assertStatementsRun(updateResults.keySet());
    }

    private void assertStatementsRun(final Set<String> expectedStatements)
    {
        for (String expectedStatement : expectedStatements)
        {
            if (!queriesRun.contains(expectedStatement))
            {
                throw new AssertionError("Statement expected to run but was not: \n" + expectedStatement);
            }
        }
    }
}
