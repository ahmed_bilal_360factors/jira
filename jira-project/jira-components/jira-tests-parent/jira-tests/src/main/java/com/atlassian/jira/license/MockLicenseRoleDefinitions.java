package com.atlassian.jira.license;

import com.atlassian.fugue.Option;
import com.google.common.collect.Sets;

import java.util.Set;
import javax.annotation.Nonnull;

public class MockLicenseRoleDefinitions implements LicenseRoleDefinitions
{
    private Set<LicenseRoleDefinition> defined = Sets.newHashSet();
    private Set<LicenseRoleDefinition> licensed = Sets.newHashSet();

    @Nonnull
    @Override
    public Set<LicenseRoleDefinition> getDefinedRoleDefinitions()
    {
        return defined;
    }

    @Nonnull
    @Override
    public Set<LicenseRoleDefinition> getInstalledRoleDefinitions()
    {
        return licensed;
    }

    @Override
    public boolean isLicenseRoleDefined(@Nonnull final LicenseRoleId licenseRoleId)
    {
        return getDefinedRoleDefinition(licenseRoleId).isDefined();
    }

    @Override
    public boolean isLicenseRoleInstalled(@Nonnull final LicenseRoleId licenseRoleId)
    {
        return getInstalledRoleDefinition(licenseRoleId).isDefined();
    }

    @Nonnull
    @Override
    public Option<LicenseRoleDefinition> getDefinedRoleDefinition(@Nonnull final LicenseRoleId licenseRoleId)
    {
        return findById(defined, licenseRoleId);
    }

    @Nonnull
    @Override
    public Option<LicenseRoleDefinition> getInstalledRoleDefinition(@Nonnull final LicenseRoleId licenseRoleId)
    {
        return findById(licensed, licenseRoleId);
    }

    public MockLicenseRoleDefinitions addDefined(LicenseRoleDefinition def)
    {
        defined.add(def);
        return this;
    }

    public MockLicenseRoleDefinitions addAuthorised(LicenseRoleDefinition def)
    {
        licensed.add(def);
        return this;
    }

    public MockLicenseRoleDefinitions removeAuthorised(LicenseRoleDefinition def)
    {
        licensed.remove(def);
        return this;
    }

    private Option<LicenseRoleDefinition> findById(Iterable<LicenseRoleDefinition> definitions, LicenseRoleId id)
    {
        for (LicenseRoleDefinition definition : definitions)
        {
            if (definition.getLicenseRoleId().equals(id))
            {
                return Option.some(definition);
            }
        }
        return Option.none();
    }
}
