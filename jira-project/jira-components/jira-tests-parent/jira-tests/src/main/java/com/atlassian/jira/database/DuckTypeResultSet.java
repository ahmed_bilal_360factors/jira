package com.atlassian.jira.database;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Iterator;

/**
 * Mocks out some of ResultSet for use as a Duck Type proxy.
 *
 * @since v6.4
 */
@SuppressWarnings ("UnusedDeclaration")
public class DuckTypeResultSet
{
    private final Iterator<ResultRow> resultsIterator;
    private ResultRow currentResultRow = null;
    private boolean wasNull = false;

    public DuckTypeResultSet(final DuckTypePreparedStatement mockPreparedStatement)
    {
        resultsIterator = mockPreparedStatement.getExpectedResults().iterator();
    }

    //@Override
    public boolean next() throws SQLException
    {
        if (resultsIterator.hasNext())
        {
            currentResultRow = resultsIterator.next();
            return true;
        }
        else
        {
            currentResultRow = null;
            return false;
        }
    }

    //@Override
    public void close() throws SQLException
    {
        // OK
    }

    //@Override
    public boolean wasNull() throws SQLException
    {
        return wasNull;
    }

    //@Override
    public String getString(final int columnIndex) throws SQLException
    {
        return (String) currentResultRow.getObject(columnIndex);
    }

    //@Override
    public boolean getBoolean(final int columnIndex) throws SQLException
    {
        Object val = getObject(columnIndex);
        return wasNull ? false : (Boolean)val;

    }

    //@Override
    public byte getByte(final int columnIndex) throws SQLException
    {
        Object val = getObject(columnIndex);
        return wasNull ? 0 : (Byte)val;

    }

    //@Override
    public short getShort(final int columnIndex) throws SQLException
    {
        Object val = getObject(columnIndex);
        return wasNull ? 0 : (Short)val;

    }

    //@Override
    public int getInt(final int columnIndex) throws SQLException
    {
        Object val = getObject(columnIndex);
        return wasNull ? 0 : (Integer)val;

    }

    //@Override
    public long getLong(final int columnIndex) throws SQLException
    {
        Object val = getObject(columnIndex);
        return wasNull ? 0 : (Long)val;
    }

    //@Override
    public float getFloat(final int columnIndex) throws SQLException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    //@Override
    public double getDouble(final int columnIndex) throws SQLException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    //@Override
    public BigDecimal getBigDecimal(final int columnIndex, final int scale) throws SQLException
    {
        Object val = getObject(columnIndex);
        return (BigDecimal)val;
    }

    //@Override
    public byte[] getBytes(final int columnIndex) throws SQLException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    //@Override
    public Date getDate(final int columnIndex) throws SQLException
    {
        Object val = getObject(columnIndex);
        return (Date)val;
    }

    //@Override
    public Time getTime(final int columnIndex) throws SQLException
    {
        Object val = getObject(columnIndex);
        return (Time)val;
    }

    //@Override
    public Timestamp getTimestamp(final int columnIndex) throws SQLException
    {
        Object val = getObject(columnIndex);
        return (Timestamp)val;
    }

    //@Override
    public Object getObject(final int columnIndex) throws SQLException
    {
        Object val = currentResultRow.getObject(columnIndex);
        wasNull = (val == null);
        return val;
    }
}
