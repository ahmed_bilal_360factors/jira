package com.atlassian.jira.matchers;

import java.io.File;
import java.io.IOException;

import com.atlassian.jira.util.RuntimeIOException;

import org.apache.commons.io.FileUtils;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * Matchers for java.io.File
 *
 * @since v6.1
 */
public class FileMatchers
{
    private FileMatchers()
    {
        throw new AssertionError("Don't instantiate me");
    }

    public static Matcher<File> exists() {
        return new TypeSafeMatcher<File>()
        {
            public File testedFile;

            @Override
            protected boolean matchesSafely(final File file)
            {
                testedFile = file;
                return file.exists();
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("that file ");
                description.appendValue(testedFile);
                description.appendText(" exists");
            }
        };
    }

    public static Matcher<File> isFile()
    {
        return new TypeSafeMatcher<File>()
        {
            public File testedFile;

            @Override
            protected boolean matchesSafely(final File file)
            {
                testedFile = file;
                return file.isFile();
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("that file ");
                description.appendValue(testedFile);
                description.appendText(" is a regular file");
            }
        };
    }

    public static Matcher<File> isDirectory()
    {
        return new TypeSafeMatcher<File>()
        {
            public File testedFile;

            @Override
            protected boolean matchesSafely(final File file)
            {
                testedFile = file;
                return file.isDirectory();
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("that file ");
                description.appendValue(testedFile);
                description.appendText(" is a directory");
            }
        };
    }

    public static Matcher<File> isEmptyDirectory()
    {
        return new TypeSafeMatcher<File>()
        {
            private File testedFile;

            @Override
            protected boolean matchesSafely(final File file)
            {
                testedFile = file;
                return file.isDirectory() && isEmpty(file);
            }

            private boolean isEmpty(final File file)
            {
                final File[] files = file.listFiles();
                return (files == null || files.length == 0);
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("that file ");
                description.appendValue(testedFile);
                description.appendText(" is an empty directory");
            }
        };
    }

    public static Matcher<File> named(final String name)
    {
        return new TypeSafeMatcher<File>()
        {
            @Override
            public boolean matchesSafely(final File item)
            {
                return item.getName().equals(name);
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("a file named ")
                        .appendValue(name);
            }

            public void describeMismatchSafely(final Description description, final File item)
            {
                description.appendText("a file named ")
                        .appendValue(name)
                        .appendText(" in ")
                        .appendValue(item.getParentFile());
            }
        };
    }

    public static Matcher<File> hasContent(final String content)
    {
        return new TypeSafeMatcher<File>()
        {
            @Override
            protected boolean matchesSafely(final File file)
            {
                final String fileContent = getFileContent(file);
                return fileContent.equals(content);
            }

            private String getFileContent(final File file)
            {
                final String fileContent;
                try
                {
                    fileContent = FileUtils.readFileToString(file, "UTF-8");
                }
                catch (final IOException e)
                {
                    throw new RuntimeIOException(e);
                }
                return fileContent;
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("got file with content ")
                        .appendValue(content);
            }

            @Override
            protected void describeMismatchSafely(final File item, final Description mismatchDescription)
            {
                mismatchDescription.appendText("file with content ")
                        .appendValue(getFileContent(item));
            }
        };
    }
}
