package com.atlassian.jira.license;

import java.util.Set;

import javax.annotation.Nonnull;

import com.google.common.collect.Sets;

/**
 *
 * @since v6.4
 */
public class MockLicenseRoleDetails implements LicenseRoleDetails
{
    private Set<LicenseRoleId> licenseRoleIds;

    @Nonnull
    @Override
    public Set<LicenseRoleId> getLicenseRoles()
    {
        throw new UnsupportedOperationException("This method is going away!");
    }

    @Nonnull
    @Override
    public Set<LicenseRoleId> getIds()
    {
        if(licenseRoleIds == null)
        {
            licenseRoleIds = Sets.<LicenseRoleId>newHashSet();
        }
        return licenseRoleIds;
    }

    public void setLicenseRoles(Set<LicenseRoleId> licenseRoleIds)
    {
        this.licenseRoleIds = licenseRoleIds;
    }

    public static MockLicenseRoleDetails withLicenseRoleIds(LicenseRoleId... licenseRoleIds)
    {
        MockLicenseRoleDetails mockLicenseRoleDetails = new MockLicenseRoleDetails();
        mockLicenseRoleDetails.setLicenseRoles(Sets.<LicenseRoleId>newHashSet(licenseRoleIds));
        return mockLicenseRoleDetails;
    }

    @Override
    public int getUserLimit(final LicenseRoleId role)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

}
