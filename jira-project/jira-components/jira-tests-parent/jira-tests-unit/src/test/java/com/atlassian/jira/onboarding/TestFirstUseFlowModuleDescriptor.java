package com.atlassian.jira.onboarding;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.module.ModuleFactory;

import org.dom4j.Element;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@RunWith (MockitoJUnitRunner.class)
public class TestFirstUseFlowModuleDescriptor
{
    FirstUseFlowModuleDescriptor descriptor;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    ModuleFactory moduleFactory;

    @Mock
    Element element;

    @Mock
    Plugin plugin;

    @Before
    public void init()
    {
        descriptor = new FirstUseFlowModuleDescriptor(authenticationContext, moduleFactory);
    }


    @Test
    public void testWeightAssignedToDescriptor()
    {
        final int EXPECTED_WEIGHT = 5;
        when(element.attributeValue("weight")).thenReturn(String.valueOf(EXPECTED_WEIGHT));
        descriptor.init(plugin, element);

        assertEquals("Weight should have been assigned", descriptor.getWeight(), EXPECTED_WEIGHT);
    }


    @Test
    public void testMissingWeightAssignedAsUnweighted()
    {
        when(element.attributeValue("weight")).thenReturn(null);
        descriptor.init(plugin, element);

        assertEquals("Weight should have been assigned", descriptor.getWeight(), FirstUseFlowModuleDescriptor.UNWEIGHTED);
    }

    @Test
    public void testIncorrectWeightAssignedAsUnweighted()
    {
        when(element.attributeValue("weight")).thenReturn("5.4");
        descriptor.init(plugin, element);

        assertEquals("Weight should have been assigned", descriptor.getWeight(), FirstUseFlowModuleDescriptor.UNWEIGHTED);
    }
}
