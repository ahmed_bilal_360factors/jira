package com.atlassian.jira.tenancy;

import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.timezone.TimeZoneInfo;
import com.atlassian.jira.timezone.TimeZoneService;
import com.atlassian.tenancy.api.Tenant;
import com.atlassian.tenancy.api.event.TenantArrivedEvent;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @since v6.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestLandlordRequests
{
    @Mock
    private EventPublisher mockEventPublisher;

    @Mock
    private JiraTenantAccessor mockJiraTenantAccessor;

    @Mock
    private TenantPluginBridge mockTenantPluginBridge;

    @Mock
    private TimeZoneService mockTimeZoneService;

    @Mock
    private JiraProperties mockJiraProperties;

    @Mock
    private TimeZoneInfo mockTimeZoneInfo;

    @Mock
    private DateTimeZone mockDateTimeZone;

    @Mock
    private TimeZone mockTimeZone;

    @Mock
    private TenantInitialDataLoader mockTenantInitialDataLoader;

    private DefaultLandlordRequests landlordRequests;

    private List<Tenant> tenants;


    @Before
    public void setup() throws Exception
    {
        landlordRequests = new DefaultLandlordRequests(mockEventPublisher, mockJiraTenantAccessor,
                mockTenantPluginBridge, mockTimeZoneService, mockJiraProperties);
        tenants = Lists.newArrayList();
        doAnswer(new Answer<Void>()
        {
            @Override
            public Void answer(final InvocationOnMock invocationOnMock) throws Throwable
            {
                tenants.add((Tenant)invocationOnMock.getArguments()[0]);
                return null;
            }
        }).when(mockJiraTenantAccessor).addTenant(any(Tenant.class));
        when(mockJiraTenantAccessor.getAvailableTenants()).thenReturn(tenants);
    }

    @Test
    public void testThatTenantIsAccepted() throws Exception
    {
        landlordRequests.acceptTenant("1");
        assertThat(landlordRequests.getTenants(), contains("1"));
        assertThat(landlordRequests.getTenants(), hasSize(1));
    }

    @Test
    public void testThatPluginBridgeIsTriggered() throws Exception
    {
        landlordRequests.acceptTenant("1");
        verify(mockTenantPluginBridge).trigger();
    }

    @Test
    public void testThatEventIsPublished() throws Exception
    {
        landlordRequests.acceptTenant("1");
        verify(mockEventPublisher).publish(any(TenantArrivedEvent.class));
    }

    @Test
    public void testInitialDataLoaders() throws Exception
    {
        landlordRequests.registerTenantInitialDataLoader(mockTenantInitialDataLoader);
        landlordRequests.acceptTenant("1");
        verify(mockTenantInitialDataLoader).start(any(Tenant.class));
    }

    @Test
    public void testTimezonePropertySetsTimeZone() throws Exception
    {
        final TimeZone defaultTimeZone = TimeZone.getDefault();
        final DateTimeZone defaultDateTimeZone = DateTimeZone.getDefault();
        final String timeZoneId = "Europe/Berlin";
        try
        {
            setupTimeZoneMocks(timeZoneId);
            final Map<String, String> tenantProperties = ImmutableMap.of("timezone", timeZoneId);
            landlordRequests.acceptTenant("id", tenantProperties);
            verify(mockTimeZoneService).setDefaultTimeZone(eq(timeZoneId), any(JiraServiceContext.class));
            verify(mockJiraProperties).setProperty("user.timezone", timeZoneId);
            assertThat(TimeZone.getDefault(), is(mockTimeZone));
            assertThat(DateTimeZone.getDefault(), is(DateTimeZone.forID(timeZoneId)));
        }
        finally
        {
            TimeZone.setDefault(defaultTimeZone);
            DateTimeZone.setDefault(defaultDateTimeZone);
        }
    }

    private void setupTimeZoneMocks(final String timeZoneId)
    {
        when(mockTimeZoneService.getDefaultTimeZoneInfo(isA(JiraServiceContext.class))).thenReturn(mockTimeZoneInfo);
        when(mockTimeZoneInfo.getTimeZoneId()).thenReturn(timeZoneId);
        when(mockTimeZoneInfo.toTimeZone()).thenReturn(mockTimeZone);
        when(mockTimeZone.getID()).thenReturn(timeZoneId);
        when(mockTimeZone.clone()).thenReturn(mockTimeZone);
    }
}
