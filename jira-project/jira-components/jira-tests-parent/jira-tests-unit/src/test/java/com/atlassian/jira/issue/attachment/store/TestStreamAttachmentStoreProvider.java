package com.atlassian.jira.issue.attachment.store;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.attachment.FileSystemAttachmentStore;
import com.atlassian.jira.issue.attachment.RemoteAttachmentStore;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider;
import com.atlassian.jira.issue.attachment.store.provider.StreamAttachmentStoreProviderImpl;
import com.atlassian.jira.issue.attachment.store.strategy.StoreOperationStrategy;
import com.atlassian.jira.issue.attachment.store.strategy.StoreOperationStrategyFactory;
import com.atlassian.jira.issue.attachment.store.strategy.get.AttachmentGetStrategiesFactory;
import com.atlassian.jira.issue.attachment.store.strategy.get.DualAttachmentGetStrategy;
import com.atlassian.jira.issue.attachment.store.strategy.move.BackgroundResendingMoveStrategy;
import com.atlassian.jira.issue.attachment.store.strategy.move.MoveTemporaryStrategiesFactory;
import com.atlassian.jira.issue.attachment.store.strategy.move.SingleStoreMoveStrategy;
import com.atlassian.jira.junit.rules.InitMockitoMocks;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider.Mode.FS_ONLY;
import static com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider.Mode.FS_PRIMARY;
import static com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider.Mode.REMOTE_ONLY;
import static com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider.Mode.REMOTE_PRIMARY;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestStreamAttachmentStoreProvider
{
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private BlobStoreAttachmentStoreModeProvider blobStoreAttachmentStoreModeProvider;
    @Mock
    private FileSystemAttachmentStore fileSystemAttachmentStore;
    @Mock
    private RemoteAttachmentStore remoteAttachmentStore;
    @Mock
    private MoveTemporaryStrategiesFactory moveTemporaryStrategiesFactory;
    @Mock
    private AttachmentGetStrategiesFactory attachmentGetStrategiesFactory;
    @Mock
    private StoreOperationStrategyFactory storeOperationStrategyFactory;
    @InjectMocks
    private StreamAttachmentStoreProviderImpl storeProvider;

    @Test
    public void testProvidesFileSystemStoreWhenInFsOnly() throws Exception
    {
        setMode(FS_ONLY);

        final StreamAttachmentStore streamAttachmentStore = storeProvider.getStreamAttachmentStore();

        assertThat(streamAttachmentStore, equalTo((StreamAttachmentStore) fileSystemAttachmentStore));
    }

    @Test
    public void testProvidesRemoteStoreWhenInRemoteOnly() throws Exception
    {
        setMode(REMOTE_ONLY);

        final StreamAttachmentStore streamAttachmentStore = storeProvider.getStreamAttachmentStore();

        assertThat(streamAttachmentStore, equalTo((StreamAttachmentStore) remoteAttachmentStore));
    }

    @Test
    public void testProvidesFileSystemStoreWhenFsOnly() throws Exception
    {
        setMode(FS_ONLY);

        final Option<FileSystemAttachmentStore> resultStore = storeProvider.getFileSystemStore();

        assertThat(resultStore.get(), equalTo((StreamAttachmentStore) fileSystemAttachmentStore));
    }

    @Test
    public void testProvidesFileSystemStoreWhenFsPrimary() throws Exception
    {
        setMode(FS_PRIMARY);

        final Option<FileSystemAttachmentStore> resultStore = storeProvider.getFileSystemStore();

        assertThat(resultStore.get(), equalTo((StreamAttachmentStore) fileSystemAttachmentStore));
    }

    @Test
    public void testProvidesNoFileStoreWhenRemoteOnly() throws Exception
    {
        setMode(REMOTE_ONLY);

        final Option<FileSystemAttachmentStore> resultStore = storeProvider.getFileSystemStore();

        assertThat(resultStore.isDefined(), equalTo(false));
    }

    @Test
    public void testProvidesNoFileStoreWhenRemotePrimary() throws Exception
    {
        setMode(REMOTE_PRIMARY);

        final Option<FileSystemAttachmentStore> resultStore = storeProvider.getFileSystemStore();

        assertThat(resultStore.isDefined(), equalTo(false));
    }

    @Test
    public void testUsesDualGetAndBackgroundResendingStrategiesInFsPrimary() throws Exception
    {
        setMode(FS_PRIMARY);

        final BackgroundResendingMoveStrategy backgroundResendingMoveStrategy = mock(BackgroundResendingMoveStrategy.class);
        when(moveTemporaryStrategiesFactory.createBackgroundResendingStrategy(fileSystemAttachmentStore, remoteAttachmentStore))
                .thenReturn(backgroundResendingMoveStrategy);

        final DualAttachmentGetStrategy dualGetStrategy = mock(DualAttachmentGetStrategy.class);
        when(attachmentGetStrategiesFactory.createDualGetAttachmentStrategy(fileSystemAttachmentStore, remoteAttachmentStore))
                .thenReturn(dualGetStrategy);

        final StoreOperationStrategy storeOperationStrategy = mock(StoreOperationStrategy.class);
        when(storeOperationStrategyFactory.createFailoverOperationStrategy(fileSystemAttachmentStore, remoteAttachmentStore))
                .thenReturn(storeOperationStrategy);

        final StreamAttachmentStore resultStore = storeProvider.getStreamAttachmentStore();

        final DualSendingAttachmentStore expectedStore = new DualSendingAttachmentStore(
                fileSystemAttachmentStore, remoteAttachmentStore, dualGetStrategy, backgroundResendingMoveStrategy, storeOperationStrategy);

        assertThat(resultStore, equalTo((StreamAttachmentStore) expectedStore));
    }

    @Test
    public void testUsesDualGetAndMoveOnlyPrimaryStrategiesInRemotePrimary() throws Exception
    {
        setMode(REMOTE_PRIMARY);

        final SingleStoreMoveStrategy singleStoreMoveStrategy = mock(SingleStoreMoveStrategy.class);
        when(moveTemporaryStrategiesFactory.createMoveOnlyPrimaryStrategy(remoteAttachmentStore))
                .thenReturn(singleStoreMoveStrategy);

        final DualAttachmentGetStrategy dualGetStrategy = mock(DualAttachmentGetStrategy.class);
        when(attachmentGetStrategiesFactory.createDualGetAttachmentStrategy(fileSystemAttachmentStore, remoteAttachmentStore))
                .thenReturn(dualGetStrategy);

        final StoreOperationStrategy storeOperationStrategy = mock(StoreOperationStrategy.class);
        when(storeOperationStrategyFactory.createFailoverOperationStrategy(remoteAttachmentStore, fileSystemAttachmentStore))
                .thenReturn(storeOperationStrategy);

        final StreamAttachmentStore resultStore = storeProvider.getStreamAttachmentStore();

        final DualSendingAttachmentStore expectedStore = new DualSendingAttachmentStore(
                remoteAttachmentStore, fileSystemAttachmentStore, dualGetStrategy, singleStoreMoveStrategy, storeOperationStrategy);

        assertThat(resultStore, equalTo((StreamAttachmentStore) expectedStore));
    }

    private void setMode(final BlobStoreAttachmentStoreModeProvider.Mode mode)
    {
        when(blobStoreAttachmentStoreModeProvider.mode())
                .thenReturn(mode);
    }
}