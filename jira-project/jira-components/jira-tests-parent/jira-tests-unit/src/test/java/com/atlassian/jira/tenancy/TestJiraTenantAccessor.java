package com.atlassian.jira.tenancy;

import javax.servlet.ServletContext;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.dataimport.ImportCompletedEvent;
import com.atlassian.jira.bc.dataimport.ImportStartedEvent;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.extension.JiraStartedEvent;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;
import com.atlassian.tenancy.api.Tenant;
import com.atlassian.tenancy.api.TenantContext;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.matchers.IterableMatchers.emptyIterable;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItem;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestJiraTenantAccessor
{
    @Rule
    public final RuleChain mocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    private TenantContext mockTenantContext;
    @Mock
    private ServletContext mockServletContext;
    @Mock
    private JohnsonEventContainer mockJohnsonEventContainer;
    @Mock
    private ComponentLocator mockComponentLocator;
    @Mock
    private TenancyCondition mockTenancyCondition;
    @Mock @AvailableInContainer
    private ApplicationProperties applicationProperties;

    private Tenant tenant = new JiraTenantImpl("tenant");
    private EventType eventType =  EventType.get("untenanted");
    private DefaultJiraTenantAccessor tenantAccessor;

    @Before
    public void setupMocks()
    {
        when(mockComponentLocator.getComponent(TenancyCondition.class)).thenReturn(mockTenancyCondition);
        when(mockTenancyCondition.isEnabled()).thenReturn(true);
        when(mockServletContext.getAttribute(JohnsonEventContainer.class.getName())).thenReturn(mockJohnsonEventContainer);
        tenantAccessor = new DefaultJiraTenantAccessor(mockTenantContext, mockComponentLocator, mockServletContext);
    }

    @Test
    public void testAddTenant() throws Exception
    {
        tenantAccessor = new DefaultJiraTenantAccessor(mockTenantContext, mockComponentLocator, mockServletContext);
        tenantAccessor.addTenant(tenant);
        assertThat(tenantAccessor.getAvailableTenants(), hasItem(tenant));
    }

    @Test
    public void testTenantIsNotAddedWhenApplicationPropertiesBlank() throws Exception
    {
        tenantAccessor = new DefaultJiraTenantAccessor(mockTenantContext, mockComponentLocator, mockServletContext);
        assertThat(tenantAccessor.getAvailableTenants(), emptyIterable(Tenant.class));
    }

    @Test (expected = IllegalStateException.class)
    public void testIllegalStateExceptionIsThrownDuringImport() throws Exception
    {

        tenantAccessor.onImportStarted(new ImportStartedEvent());
        tenantAccessor.addTenant(tenant);
    }

    public void testAfterImport() throws Exception
    {
        tenantAccessor.onImportStarted(new ImportStartedEvent());
        tenantAccessor.onImportCompleted(new ImportCompletedEvent(true, Option.<Long>none()));
        tenantAccessor.addTenant(tenant);
        assertThat(tenantAccessor.getAvailableTenants(), hasItem(tenant));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testIllegalArgumentExceptionIsThrownWhenAlreadyTenanted() throws Exception
    {
        tenantAccessor.addTenant(tenant);
        tenantAccessor.addTenant(tenant);
    }

    @Test
    public void testJohnsonned() throws Exception
    {
        ArgumentCaptor<Event> eventCapture = ArgumentCaptor.forClass(Event.class);
        tenantAccessor.afterInstantiation();
        verify(mockJohnsonEventContainer).addEvent(eventCapture.capture());
        assertThat(eventCapture.getValue().getKey(), equalTo(eventType));
    }

    @Test
    public void testAfterJiraStarted() throws Exception
    {
        ArgumentCaptor<Event> eventCapture = ArgumentCaptor.forClass(Event.class);
        tenantAccessor.onJiraStarted(new JiraStartedEvent());
        verify(mockJohnsonEventContainer).removeEvent(eventCapture.capture());
        assertThat(eventCapture.getValue().getKey(), equalTo(eventType));
    }
}
