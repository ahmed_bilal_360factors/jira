/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */
package com.atlassian.jira.issue.search;

import java.io.IOException;

import com.atlassian.jira.index.LuceneVersion;
import com.atlassian.jira.issue.index.IssueSearcherFactory;

import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.RAMDirectory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static com.atlassian.jira.issue.index.IndexDirectoryFactory.Name;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestSearchProviderFactoryImpl
{
    @Rule
    public ExpectedException exception = ExpectedException.none();

    private SearchProviderFactoryImpl searchProviderFactory;
    private IssueSearcherFactory issueSearcherFactory;

    @Before
    public void setUp() throws Exception
    {
        issueSearcherFactory = mock(IssueSearcherFactory.class);
        searchProviderFactory = new SearchProviderFactoryImpl(issueSearcherFactory);
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testGetNullSearcher()
    {
        searchProviderFactory.getSearcher(null);
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testGetEmptySearcher()
    {
        searchProviderFactory.getSearcher("");
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testGetUnknownSearcher()
    {
        searchProviderFactory.getSearcher("hi there");
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testGetBlahSearcher()
    {
        searchProviderFactory.getSearcher("blah");
    }

    @Test
    public void testGetIssueSearcher() throws Exception
    {
        final IndexSearcher expectedSearcher = createIndexSearcher();
        when(issueSearcherFactory.getEntitySearcher(eq(Name.ISSUE))).thenReturn(expectedSearcher);

        final IndexSearcher searcher = searchProviderFactory.getSearcher(SearchProviderFactory.ISSUE_INDEX);
        assertThat(searcher, equalTo(expectedSearcher));

        expectedSearcher.close();
    }

    @Test
    public void testGetSearcherThrowsSameException() throws Exception
    {
        final IllegalMonitorStateException expectedException = new IllegalMonitorStateException("something went wrong");
        exception.expect(equalTo(expectedException));

        when(issueSearcherFactory.getEntitySearcher(eq(Name.ISSUE))).thenThrow(expectedException);
        searchProviderFactory.getSearcher(SearchProviderFactory.ISSUE_INDEX);
    }

    @Test
    public void testGetSearcherThrowsSomeRuntimeException() throws Exception
    {
        final NullPointerException expectedException = new NullPointerException("something went wrong");
        exception.expect(equalTo(expectedException));

        when(issueSearcherFactory.getEntitySearcher(eq(Name.ISSUE))).thenThrow(expectedException);
        searchProviderFactory.getSearcher(SearchProviderFactory.ISSUE_INDEX);
    }

    @Test
    public void testGetCommentSearcher() throws Exception
    {
        final IndexSearcher expectedSearcher = createIndexSearcher();
        when(issueSearcherFactory.getEntitySearcher(eq(Name.COMMENT))).thenReturn(expectedSearcher);

        final IndexSearcher searcher = searchProviderFactory.getSearcher(SearchProviderFactory.COMMENT_INDEX);

        assertThat(searcher, equalTo(expectedSearcher));
        expectedSearcher.close();
    }

    @Test
    public void testGetCommentSearcherThrowsSameException() throws Exception
    {
        final IllegalMonitorStateException expectedException = new IllegalMonitorStateException("something went wrong");
        exception.expect(equalTo(expectedException));

        when(issueSearcherFactory.getEntitySearcher(eq(Name.COMMENT))).thenThrow(expectedException);
        searchProviderFactory.getSearcher(SearchProviderFactory.COMMENT_INDEX);
    }

    @Test
    public void testGetCommentSearcherThrowsSomeRuntimeException() throws Exception
    {
        final NullPointerException expectedException = new NullPointerException("something went wrong");
        exception.expect(equalTo(expectedException));

        when(issueSearcherFactory.getEntitySearcher(eq(Name.COMMENT))).thenThrow(expectedException);
        searchProviderFactory.getSearcher(SearchProviderFactory.COMMENT_INDEX);
    }

    private IndexSearcher createIndexSearcher() throws IOException
    {
        final RAMDirectory directory = new RAMDirectory();
        IndexWriterConfig conf = new IndexWriterConfig(LuceneVersion.get(), null);
        conf.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        new IndexWriter(directory, conf).close();
        return new IndexSearcher(directory);
    }
}
