package com.atlassian.jira.issue.fields.config.persistence;

import com.atlassian.cache.CacheManager;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.context.persistence.FieldConfigContextPersister;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * The idea is to rewrite the tests in TestFieldConfigSchemePersisterImpl using Mockito
 * and then remove the old class using Easy Mock.
 *
 * @since v6.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestNewFieldConfigSchemePersisterImpl {

    @Mock
    OfBizDelegator ofBizDelegator;

    @Mock
    ConstantsManager constantsManager;

    @Mock
    FieldConfigPersister fieldConfigPersister;

    @Mock
    FieldConfigContextPersister configContextPersister;

    @Mock
    CacheManager cacheManager;

    @Mock
    FieldConfig fieldConfig;

    @org.junit.Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {

    }

    @Test
    public void testIllegalArgumentToRemoveIssue()
    {
        FieldConfigSchemePersisterImpl persister = new FieldConfigSchemePersisterImpl(null, null, null, null, cacheManager);
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("issueType should not be null!");
        persister.removeByIssueType(null);
    }

    @Test
    public void testIllegalArgumentTogetInvalidFieldConfigSchemeAfterIssueTypeRemoval()
    {
        FieldConfigSchemePersisterImpl persister = new FieldConfigSchemePersisterImpl(null, null, null, null, cacheManager);
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("issueType should not be null!");
        persister.getInvalidFieldConfigSchemeAfterIssueTypeRemoval(null);
    }

    @Test
    public void testGetConfigSchemeIdsForCustomFieldIdShouldThrowExceptionWhenIdIsNull()
    {
        final FieldConfigSchemePersisterImpl persister = new FieldConfigSchemePersisterImpl(ofBizDelegator, constantsManager, fieldConfigPersister, configContextPersister, cacheManager);
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("customFieldId should not be null!");
        persister.getConfigSchemeIdsForCustomFieldId(null);
    }


    @Test
    public void testGetConfigSchemeIdsForCustomFieldIdShouldReturnEmptyListWhenNothingFound() {
        when(ofBizDelegator.findByAnd("FieldConfigScheme", ImmutableMap.of("fieldid", "customfield_10000"))).thenReturn(Collections.<GenericValue>emptyList());

        final FieldConfigSchemePersisterImpl persister = new FieldConfigSchemePersisterImpl(ofBizDelegator, constantsManager, fieldConfigPersister, configContextPersister, cacheManager);

        final List<Long> ids = persister.getConfigSchemeIdsForCustomFieldId("customfield_10000");
        assertTrue(ids.isEmpty());
    }

    @Test
    public void testGetConfigSchemeIdsForCustomFieldIdShouldReturnIdsOfSchemes() {

        final GenericValue gv1 = mock(GenericValue.class);
        when(gv1.getLong("id")).thenReturn(234L);

        final GenericValue gv2 = mock(GenericValue.class);
        when(gv2.getLong("id")).thenReturn(567L);

        when(ofBizDelegator.findByAnd("FieldConfigScheme", ImmutableMap.of("fieldid", "customfield_10020")))
                .thenReturn(ImmutableList.of(gv1, gv2));

        final FieldConfigSchemePersisterImpl persister = new FieldConfigSchemePersisterImpl(ofBizDelegator, constantsManager, fieldConfigPersister, configContextPersister, cacheManager);

        final List<Long> configSchemeIds = persister.getConfigSchemeIdsForCustomFieldId("customfield_10020");
        assertFalse(configSchemeIds.isEmpty());
        assertEquals(new Long(234), configSchemeIds.get(0));
        assertEquals(new Long(567), configSchemeIds.get(1));

    }

    @Test
    public void testGetConfigSchemeForFieldConfigNoSchemesAndExpectDataAccessException() throws Exception {
        final Long fieldConfigId = 10L;

        when(fieldConfig.getId()).thenReturn(fieldConfigId);

        when(ofBizDelegator.findByAnd("FieldConfigSchemeIssueType", MapBuilder.<String, Object>newBuilder().add("fieldconfiguration", fieldConfigId).toMap())).thenReturn(Collections.<GenericValue>emptyList());

        final FieldConfigSchemePersisterImpl configSchemePersister = new FieldConfigSchemePersisterImpl(ofBizDelegator, constantsManager, fieldConfigPersister, configContextPersister, cacheManager);
        expectedException.expect(DataAccessException.class);
        configSchemePersister.getConfigSchemeForFieldConfig(fieldConfig);
    }

}
