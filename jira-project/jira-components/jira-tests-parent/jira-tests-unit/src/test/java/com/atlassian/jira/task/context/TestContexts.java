package com.atlassian.jira.task.context;

import com.atlassian.jira.logging.QuietCountingLogger;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.task.TaskProgressSink;
import com.atlassian.jira.util.collect.Sized;

import org.apache.log4j.Logger;
import org.junit.Test;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * @since v3.13
 */
@SuppressWarnings("ConstantConditions")  // null checks
public class TestContexts
{
    private static final String REINDEX_PERCENT = "admin.indexing.percent.complete";
    private static final String REINDEX_CURRENT = "admin.indexing.current.index";
    private static final String MESSAGE = "Test Message";

    @Test(expected = IllegalArgumentException.class)
    public void testCreateWithNullLogger()
    {
        Contexts.builder().log(null, "Test Message");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateWithNullI18n()
    {
        Contexts.builder().progress(TaskProgressSink.NULL_SINK, null, REINDEX_PERCENT, REINDEX_CURRENT);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateWithNullSink()
    {
        Contexts.builder().progress(null, new MockI18nHelper(), REINDEX_PERCENT, REINDEX_CURRENT);
    }

    @Test
    public void testCreateWithZeroTasks()
    {
        final Context context = Contexts.builder()
                .sized(new Size(0))
                .progress(TaskProgressSink.NULL_SINK, new MockI18nHelper(), REINDEX_PERCENT, REINDEX_CURRENT)
                .log(Logger.getLogger(getClass()), MESSAGE)
                .build();
        assertThat(context, instanceOf(UnboundContext.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateWithNullEvent()
    {
        Contexts.builder().event(null);
    }

    @Test
    public void testCreateWorksWithEvent()
    {
        Contexts.builder()
                .sized(new Size(3))
                .event(new MockJohnsonEvent())
                .progress(TaskProgressSink.NULL_SINK, new MockI18nHelper(), REINDEX_PERCENT, REINDEX_CURRENT)
                .log(Logger.getLogger(getClass()), MESSAGE)
                .build();
    }

    @Test
    public void testSetProgress()
    {
        final QuietCountingLogger countLogger = QuietCountingLogger.create(getClass().getName());
        final CountedTaskProgressSink countedSink = new CountedTaskProgressSink();

        final Context context = Contexts.builder()
                .sized(new Size(10))
                .event(new MockJohnsonEvent())
                .progress(countedSink, new MockI18nHelper(), REINDEX_PERCENT, REINDEX_CURRENT)
                .log(countLogger, MESSAGE)
                .build();

        for (int cnt = 1; cnt <= 8; ++cnt)
        {
            context.start(null).complete();
            assertEquals(cnt, countedSink.getMakeProgressCount());
        }
    }

    private static class CountedTaskProgressSink implements TaskProgressSink
    {
        private int makeProgressCount = 0;

        public void makeProgress(final long taskProgress, final String currentSubTask, final String message)
        {
            makeProgressCount++;
        }

        public int getMakeProgressCount()
        {
            return makeProgressCount;
        }
    }

    static class Size implements Sized
    {
        private final int size;

        public Size(final int size)
        {
            this.size = size;
        }

        public int size()
        {
            return size;
        }

        public boolean isEmpty()
        {
            return size == 0;
        }
    }
}
