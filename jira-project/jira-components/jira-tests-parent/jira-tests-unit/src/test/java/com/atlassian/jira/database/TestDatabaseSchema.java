package com.atlassian.jira.database;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.mock.component.MockComponentWorker;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.text.IsEmptyString.isEmptyString;

public class TestDatabaseSchema
{
    private MockComponentWorker mockComponentWorker;

    @Before
    public void setUp()
    {
        this.mockComponentWorker = new MockComponentWorker();
        ComponentAccessor.initialiseWorker(mockComponentWorker);
    }

    @Test
    public void testDataBaseSchemaReturnEmptyStringEvenIfSchemaIsNull()
    {
        mockComponentWorker.getMockDatabaseConfigurationService().setSchemaName(null);
        assertThat("", isEmptyString());
    }
}