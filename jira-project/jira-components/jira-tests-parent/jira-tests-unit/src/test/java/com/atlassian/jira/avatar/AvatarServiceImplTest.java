package com.atlassian.jira.avatar;

import java.net.URI;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.Supplier;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.mock.propertyset.MockPropertySet;
import com.atlassian.plugin.PluginAccessor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * Unit tests for AvatarServiceImplTest.
 *
 * @since v4.3
 */
@RunWith(MockitoJUnitRunner.class)
public class AvatarServiceImplTest
{
    private static final String BASE_URL = "http://jira.atlassian.com";

    @Mock private AvatarManager avatarManager;
    @Mock private UserManager userManager;
    @Mock private UserPropertyManager userPropertyManager;
    @Mock private VelocityRequestContext context;
    @Mock private VelocityRequestContextFactory factory;
    @Mock private ApplicationProperties applicationProperties;
    @Mock private GravatarSettings gravatarSettings;
    @Mock private Supplier<JiraAvatarSupport> avatarSupportSupplier;
    @Mock private JiraAvatarSupport avatarSupport;
    @Mock private PluginAccessor pluginAccessor;

    private AvatarServiceImpl avatarService;

    @Before
    public void setup()
    {
        when(applicationProperties.getEncoding()).thenReturn("utf-8");
        when(factory.getJiraVelocityRequestContext()).thenReturn(context);
        when(avatarSupportSupplier.get()).thenReturn(avatarSupport);
        avatarService = new AvatarServiceImpl(userManager, avatarManager, userPropertyManager, factory, applicationProperties, gravatarSettings, avatarSupportSupplier, pluginAccessor);
    }

    @Test
    public void shouldReturnCorrectProjectDefaultAvatarURL() throws Exception
    {
        long defaultProjectAvatarId = 10L;

        when(context.getCanonicalBaseUrl()).thenReturn(BASE_URL);
        when(context.getBaseUrl()).thenReturn("/jira");
        when(avatarManager.getDefaultAvatarId(Avatar.Type.PROJECT)).thenReturn(defaultProjectAvatarId);

        URI url = avatarService.getProjectDefaultAvatarURL(Avatar.Size.SMALL);
        assertThat(url.toString(), equalTo("/jira/secure/projectavatar?size=xsmall&avatarId=" + defaultProjectAvatarId));
    }

    @Test
    public void anonymousNotHasCustomAvatar() throws  Exception
    {
        final MockApplicationUser satoshiNakamoto = new MockApplicationUser("SatoshiNakamoto");
        MockPropertySet ps = new MockPropertySet();
        when(userPropertyManager.getPropertySet(satoshiNakamoto)).thenReturn(ps);
        final JiraPluginAvatar mockAv = Mockito.mock(JiraPluginAvatar.class);
        when(avatarSupport.getAvatar(any(ApplicationUser.class), anyString())).thenReturn(mockAv);

        assertThat(avatarService.hasCustomUserAvatar((ApplicationUser) null, null), equalTo(false));
        assertThat(avatarService.hasCustomUserAvatar(null, satoshiNakamoto), equalTo(false));
        assertThat(avatarService.hasCustomUserAvatar(satoshiNakamoto, satoshiNakamoto), equalTo(false));
    }

    @Test
    public void externalAvatarIsCustom() throws Exception
    {
        final MockApplicationUser satoshiNakamoto = new MockApplicationUser("SatoshiNakamoto");
        MockPropertySet ps = new MockPropertySet();
        when(userPropertyManager.getPropertySet(satoshiNakamoto)).thenReturn(ps);

        final JiraPluginAvatar externalAvatar = Mockito.mock(JiraPluginAvatar.class);
        when(externalAvatar.isExternal()).thenReturn(true);
        when(avatarSupport.getAvatar(eq(satoshiNakamoto), anyString())).thenReturn(externalAvatar);
        assertThat(avatarService.hasCustomUserAvatar(satoshiNakamoto, satoshiNakamoto), equalTo(true));
    }

    @Test
    public void anonymousApplicationUserAvatarRequest() throws Exception
    {
        final ApplicationUser user = null;
        when(avatarManager.getAnonymousAvatarId()).thenReturn(123L);
        final Avatar mockAv = Mockito.mock(Avatar.class);
        when(avatarManager.getById(123L)).thenReturn(mockAv);
        final Avatar avatar = avatarService.getAvatar(user, user);
        assertThat(avatar, equalTo(avatar));
    }

    @Test
    public void anonymousUserAvatarRequest() throws Exception
    {
        final Avatar anon = Mockito.mock(Avatar.class);
        when(avatarManager.getAnonymousAvatarId()).thenReturn(123L);
        when(avatarManager.getById(123L)).thenReturn(anon);
        when(userManager.getUserByName("Froboz")).thenReturn(null);
        final Avatar avatar = avatarService.getAvatar(null, "Froboz");
        assertThat(avatar, equalTo(avatar));
    }
}
