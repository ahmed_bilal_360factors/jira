package com.atlassian.jira.security.roles;

/**
 * @since v3.13
 */
public class Main
{
    public static void main(final String[] args) throws Exception
    {
        for (int i = 1; i <= 1000; i++)
        {
            System.out.println("Loop #" + i);
            new TestCachingProjectRoleAndActorStoreForConcurrency().testConcurrentProjectRoleActorUpdate();
        }
    }
}
