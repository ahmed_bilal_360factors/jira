package com.atlassian.jira.ofbiz;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.CustomTypeSafeMatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;

@RunWith (Parameterized.class)
public class TestJiraSQLInterceptorFactory
{
    @Parameterized.Parameters
    public static List<Object[]> data()
    {
        return Arrays.asList(new Object[][] {
                {false, null},
                {false, ""},

                {false, "sel"},
                {false, "ins"},
                {false, "upd"},
                {false, "del"},

                {false, "   sel"},
                {false, "         ins"},
                {false, " upd"},
                {false, "        del"},

                {false, "select * from foo"},
                {false, "SELECT * from foo"},
                {false, "seLecT * from foo"},

                {true, "insert * from foo"},
                {true, "INSERT * from foo"},
                {true, "INSert * from foo"},

                {true, "update * from foo"},
                {true, "UPDATE * from foo"},
                {true, "upDAte * from foo"},

                {true, "delete * from foo"},
                {true, "DELETE * from foo"},
                {true, "DEleTE * from foo"},

                {true, "   delete * from foo"},
                {true, " DELETE * from foo"},
                {true, "        DEleTE * from foo"},
        });
    }

    private static final CustomTypeSafeMatcher mutatingSqlMatcher = new CustomTypeSafeMatcher<String>("a mutating SQL statement")
    {
        @Override
        protected boolean matchesSafely(final String item)
        {
            return JiraSQLInterceptorFactory.isMutatingSQL(item);
        }
    };

    private final boolean expected;
    private final String test;

    public TestJiraSQLInterceptorFactory(final boolean expected, final String test)
    {
        this.expected = expected;
        this.test = test;
    }

    @Test
    public void test()
    {
        assertThat(test, expected ? mutatingSqlMatcher : not(mutatingSqlMatcher));
    }
}
