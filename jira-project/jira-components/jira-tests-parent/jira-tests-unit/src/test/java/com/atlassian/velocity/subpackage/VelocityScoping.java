package com.atlassian.velocity.subpackage;

/**
 * This class is part of {@link com.atlassian.velocity.JiraMethodMapTest}. Is used to test that public methods from
 * public interfaces are resolved before methods from private and package scoped
 *
 * @since v6.4
 */
public interface VelocityScoping
{
    @SuppressWarnings ("UnusedDeclaration")
    void testMethod();
}


