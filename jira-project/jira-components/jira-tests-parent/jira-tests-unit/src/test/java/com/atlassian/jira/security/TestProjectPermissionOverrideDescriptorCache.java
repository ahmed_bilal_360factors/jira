package com.atlassian.jira.security;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.security.plugin.ProjectPermissionOverrideModuleDescriptorImpl;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.DefaultWebItemModuleDescriptor;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * @since v6.4
 */
public class TestProjectPermissionOverrideDescriptorCache
{
    private ProjectPermissionOverrideDescriptorCache spyDescriptorCache;

    @Mock
    private EventPublisher mockEventPublisher;

    @Mock
    private PluginModuleEnabledEvent mockPluginModuleEnabledEvent;

    @Mock
    private PluginModuleDisabledEvent mockPluginModuleDisabledEvent;

    @Mock
    private JiraAuthenticationContext mockJiraAuthenticationContext;

    @Mock
    private ModuleFactory mockModuleFactory;

    @Mock
    private ModuleDescriptor projectPermissionOverrideDescriptor;

    @Mock
    private ModuleDescriptor otherDescriptor;

    @Mock
    private WebInterfaceManager mockWebInterfaceManager;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        otherDescriptor = new DefaultWebItemModuleDescriptor();
        projectPermissionOverrideDescriptor = new ProjectPermissionOverrideModuleDescriptorImpl(mockJiraAuthenticationContext, mockModuleFactory);

        final ProjectPermissionOverrideDescriptorCache descriptorCache = new ProjectPermissionOverrideDescriptorCache(mockEventPublisher);
        spyDescriptorCache = spy(descriptorCache);

        doThrow(CacheReloaded.class).when(spyDescriptorCache).reloadCache();
    }

    @Test
    public void shouldReloadCacheOnPermissionModuleEnabled() {
        expectedException.expect(CacheReloaded.class);
        when(mockPluginModuleEnabledEvent.getModule()).thenReturn(projectPermissionOverrideDescriptor);
        spyDescriptorCache.onPluginModuleEnabledEvent(mockPluginModuleEnabledEvent);
    }

    @Test
    public void shouldReloadCacheOnPermissionModuleDisabled() {
        expectedException.expect(CacheReloaded.class);
        when(mockPluginModuleDisabledEvent.getModule()).thenReturn(projectPermissionOverrideDescriptor);
        spyDescriptorCache.onPluginModuleDisabledEvent(mockPluginModuleDisabledEvent);
    }

    @Test
    public void shouldNotReloadCacheOnOtherModuleEnabled() {
        when(mockPluginModuleEnabledEvent.getModule()).thenReturn(otherDescriptor);
        spyDescriptorCache.onPluginModuleEnabledEvent(mockPluginModuleEnabledEvent);
    }

    @Test
    public void shouldNotReloadCacheOnOtherModuleDisabled() {
        when(mockPluginModuleDisabledEvent.getModule()).thenReturn(otherDescriptor);
        spyDescriptorCache.onPluginModuleDisabledEvent(mockPluginModuleDisabledEvent);
    }

    //this seems to be the best way to check if void method from spy was called
    class CacheReloaded extends RuntimeException {}

}
