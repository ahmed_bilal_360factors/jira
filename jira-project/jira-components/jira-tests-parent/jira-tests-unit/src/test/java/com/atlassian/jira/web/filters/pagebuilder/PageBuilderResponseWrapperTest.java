package com.atlassian.jira.web.filters.pagebuilder;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import com.atlassian.jira.mock.servlet.MockHttpServletResponse;

import com.opensymphony.module.sitemesh.SitemeshBuffer;
import com.opensymphony.module.sitemesh.filter.Buffer;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertSame;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

public class PageBuilderResponseWrapperTest
{
    HttpServletResponse response;
    PageBuilderResponseWrapper wrapper;

    @Before
    public void setup()
    {
        response = new MockHttpServletResponse();
        wrapper = new PageBuilderResponseWrapper(response);
    }

    @Test (expected = IllegalStateException.class)
    public void settingADecoratorAfterExposingTheWriterShouldThrowAnException() throws Exception
    {
        wrapper.getWriter();
        wrapper.onDecoratorSet();
    }

    @Test
    public void settingADecoratorAndContentTypeShouldCauseABufferedWriterToBeExposed() throws Exception
    {
        wrapper.setContentType("text/html");
        wrapper.onDecoratorSet();
        PrintWriter writer = wrapper.getWriter();
        assertTrue(wrapper.isBuffering());

        writer.write("foo");
        assertBufferEquals("foo", wrapper.getBuffer());
    }

    @Test
    public void settingADecoratorWithoutSettingAContentTypeShouldCauseTheWriterOfTheRealResponseToBeExposed() throws Exception
    {
        wrapper.onDecoratorSet();
        assertSame(response.getWriter(), wrapper.getWriter());
    }

    @Test (expected = IllegalStateException.class)
    public void callingGetOutputStreamAfterCallingGetWriterShouldThrowAnException() throws Exception
    {
        wrapper.getWriter();
        wrapper.getOutputStream();
    }

    @Test (expected = IllegalStateException.class)
    public void callingGetOutputStreamAfterSettingADecoratorShouldThrowAnException() throws Exception
    {
        wrapper.onDecoratorSet();
        wrapper.getOutputStream();
    }

    @Test (expected = IllegalStateException.class)
    public void callingGetWriterAfterCallingGetOutputStreamShouldThrowAnException() throws Exception
    {
        wrapper.getOutputStream();
        wrapper.getWriter();
    }

    private void assertBufferEquals(String expected, Buffer actual) throws IOException
    {
        assertBufferEquals(expected, actual.getContents());
    }

    private void assertBufferEquals(String expected, SitemeshBuffer actual)
    {
        String bufferContent = new String(actual.getCharArray(), 0, actual.getBufferLength());
        assertEquals(expected, bufferContent);
    }
}
