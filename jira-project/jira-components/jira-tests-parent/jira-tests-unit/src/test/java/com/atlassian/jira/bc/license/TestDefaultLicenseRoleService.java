package com.atlassian.jira.bc.license;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeMatchers;
import com.atlassian.jira.bc.ValueMatcher;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.LicenseRole;
import com.atlassian.jira.license.LicenseRoleDefinition;
import com.atlassian.jira.license.LicenseRoleDetails;
import com.atlassian.jira.license.LicenseRoleId;
import com.atlassian.jira.license.LicenseRoleManager;
import com.atlassian.jira.license.LicenseRoleMatcher;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.google.common.base.Optional;
import com.google.common.collect.Sets;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Set;

import static com.atlassian.jira.util.NoopI18nHelper.makeTranslation;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestDefaultLicenseRoleService
{
    private static final MockApplicationUser TEST_USER = new MockApplicationUser("User");
    private static final LicenseRoleId TEST_LICENSE_ROLE_ID = new LicenseRoleId("Role");
    private static final LicenseRoleId NONE_ROLE_ID = new LicenseRoleId("None");


    private static final String GROUP_NAME_1 = "Group 1";
    private static final String GROUP_NAME_2 = "Group 2";
    private static final String GROUP_NAME_3 = "Group 3";
    private static final String GROUP_INVALID = "Group That has Been Deleted";

    private static final LicenseRole TEST_ROLE = new MockLicenseRole()
            .id(TEST_LICENSE_ROLE_ID)
            .groups(GROUP_NAME_1, GROUP_NAME_2, GROUP_NAME_3);

    @Mock private GroupManager groupManager;
    @Mock private LicenseRoleManager licenseRoleManager;
    @Mock private GlobalPermissionManager permissionManager;
    @Mock private JiraLicenseManager jiraLicenseManager;
    @Mock private LicenseDetails licenseDetails;
    @Mock private LicenseRoleDetails licenseRoleDetails;
    @Mock private Optional<LicenseRoleDefinition> licenseRoleDefinition;

    private JiraAuthenticationContext context =
            MockSimpleAuthenticationContext.createNoopContext(TEST_USER.getDirectoryUser());

    private DefaultLicenseRoleService defaultLicenseRoleService;

    @Before
    public void setUp()
    {
        defaultLicenseRoleService = new DefaultLicenseRoleService(groupManager, licenseRoleManager, context,
                permissionManager);

        when(groupManager.groupExists(GROUP_NAME_1)).thenReturn(true);
        when(groupManager.groupExists(GROUP_NAME_2)).thenReturn(true);
        when(groupManager.groupExists(GROUP_NAME_3)).thenReturn(true);

        //User is an admin.
        when(permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, TEST_USER))
                .thenReturn(true);

        when(licenseRoleManager.getLicenseRole(TEST_LICENSE_ROLE_ID)).thenReturn(Option.some(TEST_ROLE));
        when(licenseRoleManager.getLicenseRole(NONE_ROLE_ID)).thenReturn(Option.none(LicenseRole.class));
        when(licenseRoleManager.isLicenseRoleInstalled(TEST_LICENSE_ROLE_ID)).thenReturn(true);
    }

    @Test
    public void userHasRoleReturnsTrueWhenUserBelongsToGroupOfLicenseRole()
    {
        when(groupManager.isUserInGroup(TEST_USER.getUsername(), GROUP_NAME_2)).thenReturn(true);
        assertThat(defaultLicenseRoleService.userHasRole(TEST_USER, TEST_LICENSE_ROLE_ID), is(true));
    }

    @Test
    public void userHasRoleReturnsFalseWhenUserDoesNotBelongToGroupOfLicenseRole()
    {
        assertThat(defaultLicenseRoleService.userHasRole(TEST_USER, TEST_LICENSE_ROLE_ID), is(false));
    }

    @Test
    public void userHasRoleReturnsFalseWhenLicenseDoesNotExist()
    {
        when(groupManager.isUserInGroup(eq(TEST_USER.getKey()), isA(String.class))).thenReturn(true);
        assertThat(defaultLicenseRoleService.userHasRole(TEST_USER, NONE_ROLE_ID), is(false));
    }

    @Test
    public void userHasRoleReturnsFalseWhenNoUserProvided()
    {
        when(groupManager.isUserInGroup(isA(String.class), isA(String.class))).thenReturn(true);
        assertThat(defaultLicenseRoleService.userHasRole(null, TEST_LICENSE_ROLE_ID), is(false));
    }

    @Test
    public void getRolesFailsWithForbiddenForNonUsers()
    {
        when(permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, TEST_USER))
                .thenReturn(false);

        assertForbidden(defaultLicenseRoleService.getRoles());
    }

    @Test
    public void getRolesReturnsRolesCorrectly()
    {
        final LicenseRole role1 = new MockLicenseRole()
                .id("id1")
                .name("name1")
                .groups(GROUP_NAME_1, GROUP_NAME_2)
                .primaryGroup(GROUP_NAME_1);

        final LicenseRole role2 = new MockLicenseRole()
                .id("id2")
                .name("name2");

        when(licenseRoleManager.getLicenseRoles())
                .thenReturn(Sets.newHashSet(role1, role2));

        final ServiceOutcome<Set<LicenseRole>> roles = defaultLicenseRoleService.getRoles();
        final ValueMatcher<Set<LicenseRole>> matcher = ServiceOutcomeMatchers.equalTo(
            Matchers.containsInAnyOrder(new LicenseRoleMatcher().merge(role1), new LicenseRoleMatcher().merge(role2)));

        assertThat(roles, matcher);
    }

    @Test
    public void getRoleFailsWithForbiddenForNonUsers()
    {
        when(permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, TEST_USER))
                .thenReturn(false);

        final ServiceOutcome<LicenseRole> outsome = defaultLicenseRoleService.getRole(new LicenseRoleId("w"));
        assertForbidden(outsome);
    }

    @Test
    public void getRoleReturnsRoleThatExistsAndFiltersInvalidGroups()
    {
        final ServiceOutcome<LicenseRole> role = defaultLicenseRoleService.getRole(TEST_LICENSE_ROLE_ID);

        //Need the explict Type for JDK6.
        assertThat(role, ServiceOutcomeMatchers.<LicenseRole>equalTo(new LicenseRoleMatcher().merge(TEST_ROLE)));
    }

    @Test
    public void getRoleReturnsErrorOnRoleThatDoesNotExist()
    {
        final ServiceOutcome<LicenseRole> role = defaultLicenseRoleService.getRole(NONE_ROLE_ID);
        assertRoleDoesNotExist(NONE_ROLE_ID, role);
    }

    @Test
    public void setGroupsFailsForNonAdmin()
    {
        //User is an admin.
        when(permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, TEST_USER))
                .thenReturn(false);

        assertForbidden(defaultLicenseRoleService.setGroups(TEST_LICENSE_ROLE_ID, Sets.newHashSet("gou")));
    }

    @Test
    public void setGroupsFailsWhenRoleDoesNotExist()
    {
        final ServiceOutcome<LicenseRole> outcome = defaultLicenseRoleService.setGroups(NONE_ROLE_ID, Sets.newHashSet("gou"));
        assertRoleDoesNotExist(NONE_ROLE_ID, outcome);
    }

    @Test
    public void setGroupsFailsWhenGroupsDontExist()
    {
        assertBadGroup("whatGroup");
    }

    @Test
    public void setGroupsFailsWhenGroupsContainsNulls()
    {
        assertBadGroup(null);
    }

    @Test
    public void setGroupsSavesWhenGroupsExist()
    {
        Set<String> groups = Sets.newHashSet("one", "two");
        LicenseRole expectedRole = TEST_ROLE.withGroups(groups, Option.none(String.class));
        final LicenseRoleMatcher expectedMatcher = new LicenseRoleMatcher().merge(expectedRole);

        //All groups exist.
        when(groupManager.groupExists(Mockito.any(String.class))).thenReturn(true);
        when(licenseRoleManager.setLicenseRole(Mockito.argThat(expectedMatcher)))
            .thenReturn(expectedRole);

        //Save the groups.
        final ServiceOutcome<LicenseRole> outcome = defaultLicenseRoleService.setGroups(TEST_LICENSE_ROLE_ID, groups);

        //Have the groups been saved.
        verify(licenseRoleManager).setLicenseRole(Mockito.argThat(expectedMatcher));
        //Explict type for JDK6
        assertThat(outcome, ServiceOutcomeMatchers.<LicenseRole>equalTo(expectedMatcher));
    }

    @Test
    public void setRoleFailsForNonAdmin()
    {
        //User is an admin.
        when(permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, TEST_USER))
                .thenReturn(false);

        assertForbidden(defaultLicenseRoleService.setRole(TEST_ROLE));
    }

    @Test
    public void setRoleFailsWhenRoleDoesNotExist()
    {
        final MockLicenseRole iDontExist = new MockLicenseRole().id(NONE_ROLE_ID);
        final ServiceOutcome<LicenseRole> outcome = defaultLicenseRoleService.setRole(iDontExist);
        assertRoleDoesNotExist(NONE_ROLE_ID, outcome);
    }

    @Test
    public void setRoleFailsWhenGroupsDontExist()
    {
        assertBadRoleGroup("whatGroup");
    }

    @Test
    public void setRoleFailsWhenGroupsContainsNulls()
    {
        assertBadRoleGroup(null);
    }

    @Test
    public void setRoleSavesWhenGroupsExist()
    {
        Set<String> groups = Sets.newHashSet("one", "two");
        LicenseRole expectedRole = TEST_ROLE.withGroups(groups, Option.none(String.class));
        final LicenseRoleMatcher expectedMatcher = new LicenseRoleMatcher().merge(expectedRole);

        //All groups exist.
        when(groupManager.groupExists(Mockito.any(String.class))).thenReturn(true);
        when(licenseRoleManager.setLicenseRole(Mockito.argThat(expectedMatcher))).thenReturn(expectedRole);

        //Save the groups.
        final ServiceOutcome<LicenseRole> outcome = defaultLicenseRoleService.setRole(expectedRole);

        //Have the groups been saved.
        verify(licenseRoleManager).setLicenseRole(Mockito.argThat(expectedMatcher));

        //Explict type for JDK6
        assertThat(outcome, ServiceOutcomeMatchers.<LicenseRole>equalTo(expectedMatcher));
    }

    private void assertBadGroup(final String badGroup)
    {
        final ServiceOutcome<LicenseRole> outcome = defaultLicenseRoleService.setGroups(TEST_LICENSE_ROLE_ID,
                Sets.newHashSet(badGroup));

        assertThat(outcome, ServiceOutcomeMatchers.errorMatcher()
                .addReason(ErrorCollection.Reason.VALIDATION_FAILED)
                .addError("groups", makeTranslation("licenserole.service.group.does.not.exist", String.valueOf(badGroup))));
    }

    private void assertBadRoleGroup(final String badGroup)
    {
        LicenseRole expectedRole = TEST_ROLE.withGroups(Collections.singleton(badGroup), Option.none(String.class));

        when(groupManager.groupExists(badGroup)).thenReturn(false);

        //Save the groups.
        final ServiceOutcome<LicenseRole> outcome = defaultLicenseRoleService.setRole(expectedRole);

        assertThat(outcome, ServiceOutcomeMatchers.errorMatcher()
                .addReason(ErrorCollection.Reason.VALIDATION_FAILED)
                .addError("groups", makeTranslation("licenserole.service.group.does.not.exist", String.valueOf(badGroup))));
    }

    private void assertRoleDoesNotExist(final LicenseRoleId id, final ServiceOutcome<?> role)
    {
        assertThat(role, ServiceOutcomeMatchers.errorMatcher()
                .addErrorMessage(makeTranslation("licenserole.service.role.does.not.exist", id.getName()))
                .addReason(ErrorCollection.Reason.NOT_FOUND));
    }

    private void assertForbidden(final ServiceOutcome<?> outcome)
    {
        assertThat(outcome, ServiceOutcomeMatchers.errorMatcher()
                .addErrorMessage(makeTranslation("licenserole.service.permission.denied"))
                .addReason(ErrorCollection.Reason.FORBIDDEN));
    }
}
