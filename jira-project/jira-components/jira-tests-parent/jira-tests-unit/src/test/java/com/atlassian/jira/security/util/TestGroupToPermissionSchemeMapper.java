package com.atlassian.jira.security.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.permission.DefaultPermissionSchemeManager;
import com.atlassian.jira.permission.MockProjectPermission;
import com.atlassian.jira.permission.PermissionSchemeEntry;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.user.MockGroup;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static com.atlassian.jira.permission.ProjectPermissions.ADD_COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestGroupToPermissionSchemeMapper
{
    private Scheme schemeA;
    private Scheme schemeB;
    private Scheme schemeC;
    private Group groupA;
    private Group groupB;
    private Group groupC;

    @Mock
    private PermissionManager permissionManager;

    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    @Before
    public void setUp() throws Exception
    {
        new MockComponentWorker().init()
                .addMock(OfBizDelegator.class, new MockOfBizDelegator())
                .addMock(CrowdService.class, new MockCrowdService());
    }

    @After
    public void tearDownWorker()
    {
        ComponentAccessor.initialiseWorker(null);
    }

    private Group createMockGroup(String groupName)
            throws OperationNotPermittedException, InvalidGroupException
    {
        final Group group = new MockGroup(groupName);
        ComponentAccessor.getCrowdService().addGroup(group);
        return group;
    }

    @Test
    public void testMapper() throws OperationNotPermittedException, InvalidGroupException
    {
        final PermissionSchemeManager permissionSchemeManager = new MockPermissionSchemeManager();

        // Setup a list of schemes
        schemeA = new Scheme("perm", "test scheme A");
        schemeB = new Scheme("perm", "test scheme B");
        schemeC = new Scheme("perm", "test scheme C");

        // Setup Groups
        groupA = createMockGroup("test group A");
        groupB = createMockGroup("test group B");
        groupC = createMockGroup("test group C");

        when(permissionManager.getAllProjectPermissions()).thenReturn(Arrays.<ProjectPermission>asList(
            new MockProjectPermission(ADD_COMMENTS.permissionKey(), null, null, null),
            new MockProjectPermission(BROWSE_PROJECTS.permissionKey(), null, null, null),
            new MockProjectPermission(CREATE_ISSUES.permissionKey(), null, null, null)
        ));

        final GroupToPermissionSchemeMapper mapper = new GroupToPermissionSchemeMapper(permissionSchemeManager, permissionManager);

        // Ensure the mapper returns the correct results
        Collection permissionSchemes = mapper.getMappedValues(groupA.getName());
        List<Scheme> expectedSchemes = Arrays.asList(schemeA);
        assertEquals(expectedSchemes, permissionSchemes);

        permissionSchemes = mapper.getMappedValues(groupB.getName());
        expectedSchemes = Arrays.asList(schemeB, schemeC);
        assertEquals(expectedSchemes, permissionSchemes);

        permissionSchemes = mapper.getMappedValues(groupC.getName());
        expectedSchemes = Arrays.asList(schemeA, schemeC);
        assertEquals(expectedSchemes, permissionSchemes);

        assertTrue(mapper.getMappedValues("non existant group").isEmpty());
    }

    // Build a mock for the manager so that we can return what we want from the methods called by the
    // GroupToPermissionSchemeMapper
    private class MockPermissionSchemeManager extends DefaultPermissionSchemeManager
    {
        public MockPermissionSchemeManager()
        {
            super(null, null, null, null, null, null, null, null, new MemoryCacheManager());
        }

        @Override
        public List<Scheme> getSchemeObjects()
        {
            return Arrays.asList(schemeA, schemeB, schemeC);
        }

        @Override
        public List<PermissionSchemeEntry> getPermissionSchemeEntries(@Nonnull Scheme scheme, @Nonnull ProjectPermissionKey permissionKey)
        {
            if (schemeA.equals(scheme) && permissionKey.equals(ADD_COMMENTS))
            {
                return Arrays.asList(
                        new PermissionSchemeEntry(1L, 11L, permissionKey.permissionKey(), "group", groupA.getName()),
                        new PermissionSchemeEntry(2L, 12L, permissionKey.permissionKey(), "group", groupC.getName())
                );
            }
            else if (schemeB.equals(scheme) && permissionKey.equals(BROWSE_PROJECTS))
            {
                return Arrays.asList(
                        new PermissionSchemeEntry(3L, 13L, permissionKey.permissionKey(), "group", groupB.getName())
                );
            }
            else if (schemeC.equals(scheme) && permissionKey.equals(CREATE_ISSUES))
            {
                // The record of type 'user' should be ignored by the mapper.
                return Arrays.asList(
                        new PermissionSchemeEntry(4L, 14L, permissionKey.permissionKey(), "group", groupB.getName()),
                        new PermissionSchemeEntry(5L, 15L, permissionKey.permissionKey(), "group", groupC.getName()),
                        new PermissionSchemeEntry(6L, 16L, permissionKey.permissionKey(), "user", "test user")
                );
            }
            else
            {
                return Collections.emptyList();
            }
        }
    }
}
