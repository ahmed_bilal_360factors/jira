package com.atlassian.jira.issue.attachment.store.strategy;

import java.util.Iterator;
import java.util.List;

import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import org.hamcrest.CoreMatchers;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import static org.junit.Assert.assertThat;

public class TestDualStoreConsistentOperationStrategy
{
    @Rule
    public TestRule initMock = new InitMockitoMocks(this);
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Mock
    private StreamAttachmentStore primaryStore;
    @Mock
    private StreamAttachmentStore secondaryStore;

    private DualStoreConsistentOperationStrategy operationStrategy;

    @Before
    public void setUp() throws Exception
    {
        operationStrategy = new DualStoreConsistentOperationStrategy(primaryStore, secondaryStore);
    }

    @Test
    public void shouldPerformOperationInBackground() throws Exception
    {
        final List<StreamAttachmentStore> executedStores = Lists.newArrayListWithExpectedSize(2);
        final List<String> results = ImmutableList.of("value0", "value1");
        final Iterator<String> resultsIterator = results.iterator();
        final Function<StreamAttachmentStore, Promise<String>> function = new Function<StreamAttachmentStore, Promise<String>>()
        {
            @Override
            public Promise<String> get(final StreamAttachmentStore input)
            {
                executedStores.add(input);
                return Promises.promise(resultsIterator.next());
            }
        };

        final Promise<String> perform = operationStrategy.perform(function);
        assertThat(perform.claim(), CoreMatchers.equalTo(results.get(0)));
        assertThat(executedStores, IsIterableContainingInOrder.contains(primaryStore, secondaryStore));
    }

    @Test
    public void shouldNotPerformOperationInBackgroundOnFailureFromFirstStore() throws Exception
    {
        final String expectedMessage = "Im just throwing this";
        final List<StreamAttachmentStore> executedStores = Lists.newArrayListWithExpectedSize(2);
        final Function<StreamAttachmentStore, Promise<String>> function = new Function<StreamAttachmentStore, Promise<String>>()
        {
            @Override
            public Promise<String> get(final StreamAttachmentStore input)
            {

                executedStores.add(input);
                return Promises.rejected(new RuntimeException(expectedMessage));
            }
        };

        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage(expectedMessage);
        try
        {
            operationStrategy.perform(function).claim();
        }
        finally
        {
            assertThat(executedStores, IsIterableContainingInOrder.contains(primaryStore));
        }
    }
}