package com.atlassian.jira.web.pagebuilder;

import java.io.PrintWriter;
import java.io.Writer;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;

import com.opensymphony.module.sitemesh.RequestConstants;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InOrder;
import org.mockito.Mock;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DefaultPageBuilderTest
{
    @Mock private ApplicationProperties applicationProperties;
    @Mock private Decorator decorator;
    @Mock private DecoratorListener decoratorListener;
    @Mock private FeatureManager featureManager;
    @Rule public final TestRule mockInContainer = MockitoMocksInContainer.forTest(this);
    @Mock private DecoratablePage page;
    private DefaultPageBuilder pageBuilder;
    @Mock private HttpServletRequest request;
    @Mock private HttpServletResponse response;
    @Mock private ServletContext servletContext;

    @Before
    public void setup() throws Exception
    {
        pageBuilder = new DefaultPageBuilder(applicationProperties, request, response, decoratorListener, servletContext, featureManager);

        // A bunch of stuff in the page builder uses the writer, so let's provide one.
        when(response.getWriter()).thenReturn(mock(PrintWriter.class));
    }

    @Test
    public void settingADecoratorShouldNotifyTheDecoratorListener()
    {
        pageBuilder.setDecorator(decorator);
        verify(decoratorListener).onDecoratorSet();
    }

    @Test
    public void settingAJspDecoratorShouldCauseItsContextToBeSet()
    {
        AbstractJspDecorator jspDecorator = mock(AbstractJspDecorator.class);
        pageBuilder.setDecorator(jspDecorator);
        verify(jspDecorator).setContext(servletContext, request, response);
    }

    @Test (expected = IllegalStateException.class)
    public void settingADecoratorAfterDataHasBeenSentShouldThrowAnException()
    {
        pageBuilder.setDecorator(decorator);
        pageBuilder.flush();
        pageBuilder.setDecorator(decorator);
    }

    @Test (expected = IllegalStateException.class)
    public void callingFlushBeforeSettingADecoratorShouldThrowAnException()
    {
        pageBuilder.flush();
    }

    @Test
    public void settingADecoratorShouldDisableSitemesh()
    {
        pageBuilder.setDecorator(decorator);
        verify(request).setAttribute(RequestConstants.DISABLE_BUFFER_AND_DECORATION, true);
    }

    @Test
    public void theFirstFlushShouldWritePrehead() throws Exception
    {
        when(applicationProperties.getContentType()).thenReturn("text/html");
        when(applicationProperties.getEncoding()).thenReturn("utf8");

        pageBuilder.setDecorator(decorator);
        pageBuilder.flush();

        verify(response).setContentType(applicationProperties.getContentType());
        verify(response).setCharacterEncoding(applicationProperties.getEncoding());
        verify(decorator).writePreHead(response.getWriter());
        verify(decorator).writeOnFlush(response.getWriter());
        verify(response.getWriter()).flush();
    }

    @Test
    public void flushShouldEnsureSessionExists()
    {
        pageBuilder.setDecorator(decorator);
        pageBuilder.flush();
        verify(request, times(1)).getSession(true);
    }

    @Test
    public void preHeadShouldOnlyBeSentOnce()
    {
        pageBuilder.setDecorator(decorator);
        pageBuilder.flush();
        pageBuilder.flush();
        verify(decorator, times(1)).writePreHead(any(Writer.class));
    }

    @Test (expected = IllegalStateException.class)
    public void multipleFinishCallsShouldThrowAnException()
    {
        pageBuilder.setDecorator(decorator);
        pageBuilder.finish(page);
        pageBuilder.finish(page);
    }

    @Test
    public void finishShouldDelegateToThePageWhenNoDecoratorIsUsed() throws Exception
    {
        InOrder order = inOrder(page);
        pageBuilder.finish(page);

        order.verify(page).writeHead(response.getWriter());
        order.verify(page).writeBody(response.getWriter());
    }

    @Test
    public void finishShouldIncorporateInterleaveTheDecoratorWithThePage() throws Exception
    {
        InOrder order = inOrder(page, decorator);
        pageBuilder.setDecorator(decorator);
        pageBuilder.finish(page);

        order.verify(decorator).writePreHead(response.getWriter());
        order.verify(page).writeHead(response.getWriter());
        order.verify(decorator).writePostHead(eq(response.getWriter()), any(DecoratablePage.ParsedHead.class));
        order.verify(decorator).writePreBody(eq(response.getWriter()), any(DecoratablePage.ParsedBody.class));
        order.verify(page).writeBody(response.getWriter());
        order.verify(decorator).writePostBody(eq(response.getWriter()), any(DecoratablePage.ParsedBody.class));
    }
}
