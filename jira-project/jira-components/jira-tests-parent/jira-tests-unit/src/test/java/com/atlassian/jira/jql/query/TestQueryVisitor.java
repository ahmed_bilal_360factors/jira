package com.atlassian.jira.jql.query;

import com.atlassian.jira.jql.clause.DeMorgansVisitor;
import com.atlassian.jira.jql.clause.WorklogClausesTransformerVisitor;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.NotClause;
import com.atlassian.query.clause.OrClause;
import com.atlassian.query.clause.TerminalClause;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static com.atlassian.jira.jql.query.ContextAwareQueryVisitor.ContextAwareQueryVisitorFactory;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
@RunWith (MockitoJUnitRunner.class)
public class TestQueryVisitor
{
    private QueryVisitor queryVisitor;

    @Mock
    private DeMorgansVisitor.DeMorgansVisitorFactory deMorgansVisitorFactory;

    @Mock
    private DeMorgansVisitor deMorgansVisitor;

    @Mock
    private QueryCreationContext queryCreationContext;

    @Mock
    private WorklogClausesTransformerVisitor worklogClausesTransformerVisitor;

    @Mock
    private WorklogClausesTransformerVisitor.Factory worklogsVisitorFactory;

    @Mock
    private ContextAwareQueryVisitorFactory contextAwareQueryVisitorFactory;

    @Mock
    private ContextAwareQueryVisitor contextAwareQueryVisitor;

    @Before
    public void setUp() throws Exception
    {
        final Answer<Object> returnFirstArgument = new Answer<Object>()
        {
            @Override
            public Object answer(final InvocationOnMock invocationOnMock) throws Throwable
            {
                return invocationOnMock.getArguments()[0];
            }
        };
        when(worklogClausesTransformerVisitor.visit(any(AndClause.class))).thenAnswer(returnFirstArgument);
        when(worklogClausesTransformerVisitor.visit(any(OrClause.class))).thenAnswer(returnFirstArgument);
        when(worklogClausesTransformerVisitor.visit(any(TerminalClause.class))).thenAnswer(returnFirstArgument);
        when(worklogClausesTransformerVisitor.visit(any(NotClause.class))).thenAnswer(returnFirstArgument);

        when(worklogsVisitorFactory.create(queryCreationContext)).thenReturn(worklogClausesTransformerVisitor);
        when(contextAwareQueryVisitorFactory.createVisitor(eq(queryCreationContext), any(Clause.class))).thenReturn(contextAwareQueryVisitor);

        when(deMorgansVisitorFactory.createDeMorgansVisitor()).thenReturn(deMorgansVisitor);

        queryVisitor = new QueryVisitor(contextAwareQueryVisitorFactory, worklogsVisitorFactory, deMorgansVisitorFactory);
    }

    /*
     * Make sure the Query returned from the visit will be not be negated when not necessary.
     */
    @Test
    public void testCreateQueryNotNegated()
    {
        final BooleanQuery expectedQuery = new BooleanQuery();

        final NotClause mockNotClause = getMockNotClause(new QueryFactoryResult(expectedQuery));

        final Query actualQuery = queryVisitor.createQuery(mockNotClause, queryCreationContext);
        assertEquals(expectedQuery, actualQuery);
    }

    /*
     * Make sure the Query returned from the visit will be negated if the result of the visit says
     * that it should be.
     */
    @Test
    public void testCreateQueryNegated()
    {
        final Query notQuery = new TermQuery(new Term("qwerty", "dddd"));
        final BooleanQuery expectedQuery = new BooleanQuery();
        expectedQuery.add(notQuery, BooleanClause.Occur.MUST_NOT);

        final NotClause mockNotClause = getMockNotClause(new QueryFactoryResult(notQuery, true));

        final Query actualQuery = queryVisitor.createQuery(mockNotClause, queryCreationContext);
        assertEquals(expectedQuery, actualQuery);
    }

    @Test
    public void testQueryCreatedOnNormalisedClause() throws Exception
    {
        final BooleanQuery expectedQuery = new BooleanQuery();

        final Clause mockNormalisedClause = getMockClause(new QueryFactoryResult(expectedQuery));

        final NotClause mockNotClause = mock(NotClause.class);
        when(mockNotClause.accept(eq(worklogClausesTransformerVisitor))).thenReturn(mockNotClause);
        when(mockNotClause.accept(eq(deMorgansVisitor))).thenReturn(mockNormalisedClause);

        final Query actualQuery = queryVisitor.createQuery(mockNotClause, queryCreationContext);
        assertEquals(expectedQuery, actualQuery);
    }

    private Clause getMockClause(final QueryFactoryResult queryFactoryResult)
    {
        final Clause mockClause = mock(Clause.class);
        when(mockClause.accept(eq(deMorgansVisitor))).thenReturn(mockClause);
        when(mockClause.accept(eq(worklogClausesTransformerVisitor))).thenReturn(mockClause);
        when(mockClause.accept(eq(contextAwareQueryVisitor))).thenReturn(queryFactoryResult);
        return mockClause;
    }

    private NotClause getMockNotClause(final QueryFactoryResult queryFactoryResult)
    {
        final NotClause mockClause = mock(NotClause.class);
        when(mockClause.accept(eq(deMorgansVisitor))).thenReturn(mockClause);
        when(mockClause.accept(eq(worklogClausesTransformerVisitor))).thenReturn(mockClause);
        when(mockClause.accept(eq(contextAwareQueryVisitor))).thenReturn(queryFactoryResult);
        return mockClause;
    }
}
