package com.atlassian.jira.datetime;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertThat;

import static org.hamcrest.Matchers.equalTo;
/**
 * @since v4.4
 */
public class TestLocalDate
{
    @Test
    public void testGetEarlierDate()
    {
        LocalDate localDate1 = new LocalDate(1989, 11, 3);
        LocalDate localDate2 = new LocalDate(1989, 11, 2);
        assertThat(localDate2.getEarlierDate(localDate1), equalTo(localDate2));
        assertThat(localDate1.getEarlierDate(localDate2), equalTo(localDate2));
        assertThat(localDate2.getEarlierDate(null), equalTo(localDate2));
    }

    @Test
    public void testGetLaterDate()
    {
        LocalDate localDate1 = new LocalDate(1989, 11, 3);
        LocalDate localDate2 = new LocalDate(1989, 11, 2);
        assertThat(localDate2.getLaterDate(localDate1), equalTo(localDate1));
        assertThat(localDate1.getLaterDate(localDate2), equalTo(localDate1));
        assertThat(localDate2.getLaterDate(null), equalTo(localDate2));
    }

    @Test
    public void testPlusDays()
    {
        final LocalDate localDate = new LocalDate(1989, 11, 3);
        assertThat(localDate.plusDays(2), equalTo(new LocalDate(1989, 11, 5)));
        assertThat(localDate.plusDays(28), equalTo(new LocalDate(1989, 12, 1)));
        assertThat(localDate.plusDays(365), equalTo(new LocalDate(1990, 11, 3)));
        assertThat(localDate.plusDays(-10), equalTo(new LocalDate(1989, 10, 24)));
    }

    @Test
    public void testMinusDays()
    {
        final LocalDate localDate = new LocalDate(1989, 11, 3);
        assertThat(localDate.minusDays(2), equalTo(new LocalDate(1989, 11, 1)));
        assertThat(localDate.minusDays(5), equalTo(new LocalDate(1989, 10, 29)));
        assertThat(localDate.minusDays(365), equalTo(new LocalDate(1988, 11, 3)));
        assertThat(localDate.minusDays(-10), equalTo(new LocalDate(1989, 11, 13)));
    }

    @Test
    public void testPlusMonths()
    {
        final LocalDate localDate = new LocalDate(1989, 11, 3);
        assertThat(localDate.plusMonths(1), equalTo(new LocalDate(1989, 12, 3)));
        assertThat(localDate.plusMonths(5), equalTo(new LocalDate(1990, 4, 3)));
        assertThat(localDate.plusMonths(-3), equalTo(new LocalDate(1989, 8, 3)));
    }

    @Test
    public void testMinusMonths()
    {
        final LocalDate localDate = new LocalDate(1989, 11, 3);
        assertThat(localDate.minusMonths(2), equalTo(new LocalDate(1989, 9, 3)));
        assertThat(localDate.minusMonths(12), equalTo(new LocalDate(1988, 11, 3)));
        assertThat(localDate.minusMonths(-3), equalTo(new LocalDate(1990, 2, 3)));
    }

    @Test
    public void testPlusYears()
    {
        final LocalDate localDate = new LocalDate(1989, 11, 3);
        assertThat(localDate.plusYears(2), equalTo(new LocalDate(1991, 11, 3)));
        assertThat(localDate.plusYears(-4), equalTo(new LocalDate(1985, 11, 3)));
    }

    @Test
    public void testMinusYears()
    {
        final LocalDate localDate = new LocalDate(1989, 11, 3);
        assertThat(localDate.minusYears(2), equalTo(new LocalDate(1987, 11, 3)));
        assertThat(localDate.minusYears(-3), equalTo(new LocalDate(1992, 11, 3)));
    }

    @Test
    public void testEqual() throws Exception
    {
        LocalDate localDate1 = new LocalDate(1989, 11, 3);
        LocalDate localDate2 = new LocalDate(1989, 11, 3);
        assertTrue(localDate1.equals(localDate2));
    }

    @Test
    public void testNotEqual() throws Exception
    {
        LocalDate localDate1 = new LocalDate(1979, 11, 3);
        LocalDate localDate2 = new LocalDate(1989, 11, 4);
        assertFalse(localDate1.equals(localDate2));
    }

    @Test
    public void testCompareLess() throws Exception
    {
        LocalDate localDate1 = new LocalDate(1989, 11, 3);
        LocalDate localDate2 = new LocalDate(1989, 11, 4);
        assertThat(localDate1.compareTo(localDate2), equalTo(-1));
    }

    @Test
    public void testCompareGreater() throws Exception
    {
        LocalDate localDate1 = new LocalDate(1989, 11, 5);
        LocalDate localDate2 = new LocalDate(1989, 11, 4);
        assertThat(localDate1.compareTo(localDate2), equalTo(1));
    }

}
