package com.atlassian.jira.mail;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.JiraApplicationContext;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.fields.renderer.IssueRenderContext;
import com.atlassian.jira.mail.builder.RenderingMailQueueItem;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.UserLocaleStore;
import com.atlassian.mail.queue.MailQueue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.atlassian.jira.mail.MailServiceQueueItemBuilder.ISSUE;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class MentionMailQueueItemTest
{
    private static final String PERSONAL_EMAIL = "test@gmail.com";
    private static final String PROJECT_EMAIL = "jira_project@gmail.com";
    private static final String TO_EMAIL1 = "to@gmail.com";
    @Mock
    private User from;
    @Mock
    private NotificationRecipient recipient;

    private Map<String, Object> context = new HashMap<String, Object>();
    @Mock
    private IssueRenderContext issueRenderContext;
    @Mock
    private RendererManager renderManager;

    private MailQueue mailQueue = new JiraMailQueueTest.MockMailQueue();
    @Mock
    private JiraApplicationContext jiraApplicationContext;
    @Mock
    private UserLocaleStore userLocaleStore;
    @Mock
    private TemplateContextFactory templateContextFactory;
    @Mock
    private TemplateContext templateContext;
    @Mock
    private Issue issue;
    @Mock
    private Project project;


    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
        when(recipient.getEmail()).thenReturn(TO_EMAIL1);
        when(from.getEmailAddress()).thenReturn(PERSONAL_EMAIL);
        setUpIssue();
        mockStaticDependencies();
    }

    @Test
    public void replyToShouldBeSetToProjectEmail() throws Exception
    {
        context.put(ISSUE, issue);
        MentionMailQueueItem mentionMailQueueItem = new MentionMailQueueItem(from, recipient, context, issueRenderContext, renderManager, mailQueue);
        mentionMailQueueItem.send();
        final RenderingMailQueueItem item = (RenderingMailQueueItem) mentionMailQueueItem.getMailQueue().getQueue().poll();
        assertNotNull(item.getEmail());
        assertThat(item.getEmail().getReplyTo(), is(PROJECT_EMAIL));
    }

    private void setUpIssue()
    {
        when(project.getEmail()).thenReturn(PROJECT_EMAIL);
        when(issue.getProjectObject()).thenReturn(project);
    }

    //We have to mock these dependencies because they're looked up statically in Email and EmailBuilder classes.
    private void mockStaticDependencies()
    {
        MockComponentWorker componentWorker = new MockComponentWorker();
        componentWorker.registerMock(JiraApplicationContext.class, jiraApplicationContext);
        componentWorker.registerMock(UserLocaleStore.class, userLocaleStore);
        when(templateContextFactory.getTemplateContext(Matchers.any(Locale.class), Matchers.any(IssueEvent.class))).thenReturn(templateContext);
        componentWorker.registerMock(TemplateContextFactory.class, templateContextFactory);
        ComponentAccessor.initialiseWorker(componentWorker);
    }
}