package com.atlassian.jira.issue.attachment.zip;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.BulkAttachmentOperations;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.util.collect.CollectionEnclosedIterable;
import com.atlassian.jira.util.collect.EnclosedIterable;
import com.atlassian.jira.util.io.InputStreamConsumer;

import com.google.common.collect.Lists;

import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import io.atlassian.blobstore.client.api.Unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestAttachmentZipFileCreator
{
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private BulkAttachmentOperations bulkAttachmentOperations;
    @Mock
    private AttachmentManager attachmentManager;

    @InjectMocks
    private AttachmentZipFileCreator attachmentZipFileCreator;

    @Test
    public void testZipCreationWithNonUniqueNames() throws IOException
    {
        final MockIssue issue = new MockIssue();
        final EnclosedIterable<Attachment> attachments = CollectionEnclosedIterable.from(Lists.newArrayList(
                mockAttachment("file1.txt"),
                mockAttachment("file2.txt"),
                mockAttachment("file2.txt"),
                mockAttachment("file2.txt"),
                mockAttachment("file3.txt")
        ));

        when(bulkAttachmentOperations.getAttachmentOfIssue(issue))
                .thenReturn(attachments);

        doAnswer(callStreamConsumer)
                .when(attachmentManager).streamAttachmentContent(any(Attachment.class), any(InputStreamConsumer.class));

        File zipFile = null;
        try
        {
            zipFile = attachmentZipFileCreator.toZipFile(issue);
            assertZipFileContent(zipFile, Arrays.asList("file1.txt", "file2.txt", "file2.txt.1", "file2.txt.2", "file3.txt"));
        }
        finally
        {
            FileUtils.deleteQuietly(zipFile);
        }
    }

    @Test
    public void testZipCreation() throws IOException
    {
        final MockIssue issue = new MockIssue();
        final EnclosedIterable<Attachment> attachments = CollectionEnclosedIterable.from(Lists.newArrayList(
                mockAttachment("file1.txt"),
                mockAttachment("file2.txt")));

        when(bulkAttachmentOperations.getAttachmentOfIssue(issue))
                .thenReturn(attachments);

        doAnswer(callStreamConsumer)
                .when(attachmentManager).streamAttachmentContent(any(Attachment.class), any(InputStreamConsumer.class));

        File zipFile = null;
        try
        {
            zipFile = attachmentZipFileCreator.toZipFile(issue);
            assertZipFileContent(zipFile, Arrays.asList("file1.txt", "file2.txt"));
        }
        finally
        {
            FileUtils.deleteQuietly(zipFile);
        }
    }

    private Attachment mockAttachment(final String attachmentName)
    {
        final Attachment attachment = mock(Attachment.class);
        when(attachment.getFilename())
                .thenReturn(attachmentName);

        return attachment;
    }

    private final Answer<Unit> callStreamConsumer = new Answer<Unit>()
    {
        @Override
        public Unit answer(final InvocationOnMock invocation) throws Throwable
        {
            final InputStreamConsumer streamConsumer = (InputStreamConsumer) invocation.getArguments()[1];
            streamConsumer.withInputStream(new ByteArrayInputStream(new byte[1]));
            return Unit.UNIT;
        }
    };

    private void assertZipFileContent(final File zipFile, final List<String> expectedFilesNames) throws IOException
    {
        assertNotNull(zipFile);
        assertTrue(zipFile.exists());
        assertTrue(zipFile.getName().endsWith(".zip"));
        // Open the ZIP file
        final ZipFile zf = new ZipFile(zipFile);

        // Enumerate each entry
        int actualCount = 0;
        for (final Enumeration entries = zf.entries(); entries.hasMoreElements();)
        {
            // Get the entry name
            final String zipEntryName = ((ZipEntry) entries.nextElement()).getName();
            assertTrue("Expecting " + zipEntryName, expectedFilesNames.contains(zipEntryName));
            actualCount++;
        }
        assertEquals("They arent the same size", expectedFilesNames.size(), actualCount);
    }
}
