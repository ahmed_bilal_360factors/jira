package com.atlassian.jira.license;

import com.atlassian.fugue.Option;
import com.atlassian.jira.matchers.OptionMatchers;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.annotation.Nonnull;

import static com.atlassian.jira.matchers.OptionMatchers.none;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;

public class AbstractLicenseRoleTest
{
    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsWithNullGroups()
    {
        new TestingLicenseRole("id", "name", null, Option.none(String.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsWithNullInGroups()
    {
        new TestingLicenseRole("id", "name", Collections.<String>singletonList(null), Option.none(String.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsWithNullPrimaryGroup()
    {
        new TestingLicenseRole("id", "name", Collections.<String>emptyList(), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsWithPrimaryGroupNotListedInGroups()
    {
        new TestingLicenseRole("id", "name", Arrays.asList("one", "two"), Option.option("three"));
    }

    @Test
    public void equalsOnlyListensToId()
    {
        List<LicenseRole> roles = Arrays.<LicenseRole>asList(
                new TestingLicenseRole("id", "name", null, "one"),
                new TestingLicenseRole("id", "name", null, "two", "thee"),
                new TestingLicenseRole("id", "name", "three", "two", "three"),
                new TestingLicenseRole("id", "otherName", "three", "two", "three"));

        for (LicenseRole left : roles)
        {
            for (LicenseRole right : roles)
            {
                assertThat(left.equals(right), Matchers.equalTo(true));
            }
        }

        List<LicenseRole> otherRoles = Arrays.<LicenseRole>asList(
                new TestingLicenseRole("id2", "name", null, "one"),
                new TestingLicenseRole("id2", "name", null, "two", "thee"),
                new TestingLicenseRole("id2", "name", "three", "two", "three"),
                new TestingLicenseRole("id2", "otherName", "three", "two", "three"));

        for (LicenseRole left : roles)
        {
            for (LicenseRole right : otherRoles)
            {
                assertThat(left.equals(right), Matchers.equalTo(false));
            }
        }
    }

    @Test
    public void hashListensToId()
    {
        List<LicenseRole> roles = Arrays.<LicenseRole>asList(
                new TestingLicenseRole("id", "name", null, "one"),
                new TestingLicenseRole("id", "name", null, "two", "thee"),
                new TestingLicenseRole("id", "name", "three", "two", "three"),
                new TestingLicenseRole("id", "otherName", "three", "two", "three"));

        for (LicenseRole left : roles)
        {
            for (LicenseRole right : roles)
            {
                assertThat(left.hashCode() == right.hashCode(), Matchers.equalTo(true));
            }
        }
    }

    @Test
    public void getGroupsReturnsGroups()
    {
        assertThat(new TestingLicenseRole("id", "name", null, "one").getGroups(), contains("one"));
        assertThat(new TestingLicenseRole("id", "name", null, "one", "two").getGroups(), containsInAnyOrder("one", "two"));
    }

    @Test
    public void getPrimaryGroupRetunsPrimary()
    {
        assertThat(new TestingLicenseRole("id", "name", null, "one").getPrimaryGroup(), none());
        assertThat(new TestingLicenseRole("id", "name", "two", "one", "two").getPrimaryGroup(), OptionMatchers.some("two"));
    }

    private static class TestingLicenseRole extends AbstractLicenseRole
    {
        private final String name;
        private final LicenseRoleId id;

        private TestingLicenseRole(final String id, final String name, String primaryGroup, String...groups)
        {
            super(Arrays.asList(groups), Option.option(primaryGroup));
            this.id = LicenseRoleId.valueOf(id);
            this.name = name;
        }

        private TestingLicenseRole(final String id, final String name, final Iterable<String> groups, final Option<String> primaryGroup)
        {
            super(groups, primaryGroup);
            this.name = name;
            this.id = LicenseRoleId.valueOf(id);
        }

        @Nonnull
        @Override
        public LicenseRoleId getId()
        {
            return id;
        }

        @Nonnull
        @Override
        public String getName()
        {
            return name;
        }

        @Nonnull
        @Override
        public LicenseRole withGroups(@Nonnull final Iterable<String> groups, @Nonnull final Option<String> primaryGroup)
        {
            throw new UnsupportedOperationException("Not implemented");
        }
    }
}