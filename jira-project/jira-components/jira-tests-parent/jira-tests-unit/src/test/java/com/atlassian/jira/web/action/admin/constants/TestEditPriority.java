package com.atlassian.jira.web.action.admin.constants;

import com.atlassian.jira.JiraTestUtil;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.PriorityManager;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.action.MockRedirectSanitiser;
import com.atlassian.jira.web.action.RedirectSanitiser;
import com.atlassian.jira.web.action.admin.priorities.EditPriority;
import com.atlassian.jira.web.bean.MockI18nBean;

import com.mockobjects.servlet.MockHttpServletResponse;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericEntityException;

import webwork.action.Action;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestEditPriority
{
    private EditPriority editPriorityAction;

    @AvailableInContainer
    private RedirectSanitiser redirectSanitiser = new MockRedirectSanitiser();
    @AvailableInContainer
    @Mock
    private PriorityManager priorityManager;
    @AvailableInContainer
    @Mock
    private ConstantsManager constantsManager;
    @AvailableInContainer
    @Mock
    private JiraAuthenticationContext authContext;
    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    public TestEditPriority()
    {
        super();
    }

    @Before
    public void setUp() throws Exception
    {
        // use priorities to test the abstract class
        editPriorityAction = new EditPriority(priorityManager);
        when(authContext.getI18nHelper()).thenReturn(new MockI18nBean());
    }

    @Test
    public void testGetConstant() throws GenericEntityException
    {
        assertNull(editPriorityAction.getConstant());
        editPriorityAction.setId("1");
        assertNull(editPriorityAction.getConstant());

        Priority priority = createPriority();
        when(constantsManager.getPriorityObject("1")).thenReturn(priority);

        assertThat(editPriorityAction.getConstant(), priorityMatcher(priority));
    }

    @Test
    public void testValidation() throws Exception
    {
        editPriorityAction.setId("1");

        String result = editPriorityAction.execute();

        assertEquals(Action.INPUT, result);
        assertEquals(1, editPriorityAction.getErrorMessages().size());
        assertEquals("Specified constant does not exist.", editPriorityAction.getErrorMessages().iterator().next());
        assertEquals(3, editPriorityAction.getErrors().size());
        assertEquals("You must specify a URL for the icon of the constant.", editPriorityAction.getErrors().get("iconurl"));
        assertEquals("You must specify a name.", editPriorityAction.getErrors().get("name"));
        assertEquals("You must specify a value for the priority colour.", editPriorityAction.getErrors().get("statusColor"));
    }

    @Test
    public void testDefault() throws Exception
    {
        final Priority priority = createPriority();
        when(constantsManager.getPriorityObject("1")).thenReturn(priority);
        editPriorityAction.setId("1");

        editPriorityAction.doDefault();
        assertEquals("TEST", editPriorityAction.getName());
        assertEquals("This is a test Constant", editPriorityAction.getDescription());
        assertEquals("iconUrl", editPriorityAction.getIconurl());
    }

    @Test
    public void testExecute() throws Exception
    {
        MockHttpServletResponse response = JiraTestUtil.setupExpectedRedirect("ViewPriorities.jspa");

        final Priority priority = createPriority();
        when(constantsManager.getPriorityObject("1")).thenReturn(priority);
        when(priorityManager.getPriority("1")).thenReturn(priority);
        editPriorityAction.setId("1");

        editPriorityAction.setName("MODIFIED");
        editPriorityAction.setDescription("Description Modified");
        editPriorityAction.setIconurl("c:\test");
        editPriorityAction.setStatusColor("#ff0000");

        String result = editPriorityAction.execute();
        assertEquals(Action.NONE, result);

        verify(priorityManager).editPriority(priority, "MODIFIED", "Description Modified", "c:\test", "#ff0000");

        response.verify();
    }

    private Matcher<Object> priorityMatcher(final Priority priority)
    {
        return Matchers.allOf(
                Matchers.hasProperty("description", is(priority.getDescription())),
                Matchers.hasProperty("name", is(priority.getName())),
                Matchers.hasProperty("id", is(priority.getId())),
                Matchers.hasProperty("sequence", is(priority.getSequence())),
                Matchers.hasProperty("iconUrl", is(priority.getIconUrl()))
        );
    }

    private Priority createPriority()
    {
        Priority priority = mock(Priority.class);

        when(priority.getDescription()).thenReturn("This is a test Constant");
        when(priority.getName()).thenReturn("TEST");
        when(priority.getId()).thenReturn("1");
        when(priority.getSequence()).thenReturn(1l);
        when(priority.getIconUrl()).thenReturn("iconUrl");

        return priority;
    }
}
