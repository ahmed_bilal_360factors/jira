package com.atlassian.jira.bc.project.version;


import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.project.version.VersionService.VersionResult;
import com.atlassian.jira.easymock.Mock;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockUser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Spy;

import java.util.Date;

import static com.atlassian.jira.user.ApplicationUsers.from;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;


/**
 * Tests that deprecated methods that take a User call the new methods with ApplicationUser
 *
 * @since v6.4
 */
public class TestDeprecatedDefaultVersionService
{
    private User user;
    private ApplicationUser applicationUser;

    @Spy
    private DefaultVersionService defaultVersionService = new DefaultVersionService(null, null, null, null, null, null, null);
    @Mock
    private VersionResult versionResult;
    @Mock
    private Version version;
    @Mock
    private VersionService.VersionBuilder versionBuilder;
    @Mock
    private VersionService.VersionBuilderValidationResult validationResult;
    @Mock
    private ServiceOutcome<Version> serviceOutcome;
    @Rule
    public MockitoContainer initMockitoMocks = MockitoMocksInContainer.rule(this);

    private Project mockProject;


    @Before
    public void setUp() throws Exception
    {
        user = new MockUser("admin");
        applicationUser = from(user);
        mockProject = new MockProject((Long) null);
    }

    @Test
    public void testGetVersionById()
    {
        doReturn(versionResult).when(defaultVersionService).getVersionById(applicationUser, mockProject, 1L);
        assertEquals(versionResult, defaultVersionService.getVersionById(user, mockProject, 1L));
    }

    @Test
    public void testGetVersionByProjectAndName()
    {
        doReturn(versionResult).when(defaultVersionService).getVersionByProjectAndName(applicationUser, mockProject, "version");
        assertEquals(versionResult, defaultVersionService.getVersionByProjectAndName(user, mockProject, "version"));
    }

    @Test
    public void testGetVersionsByProject()
    {
        doReturn(versionResult).when(defaultVersionService).getVersionsByProject(applicationUser, mockProject);
        assertEquals(versionResult, defaultVersionService.getVersionsByProject(user, mockProject));
    }

    @Test
    public void testValidateReleaseVersion()
    {
        Date date = new Date();
        doReturn(versionResult).when(defaultVersionService).validateReleaseVersion(applicationUser, version, date);
        assertEquals(versionResult, defaultVersionService.validateReleaseVersion(user, version, date));
    }

    @Test
    public void testValidateUnreleaseVersion()
    {
        Date date = new Date();
        doReturn(versionResult).when(defaultVersionService).validateUnreleaseVersion(applicationUser, version, date);
        assertEquals(versionResult, defaultVersionService.validateUnreleaseVersion(user, version, date));
    }

    @Test
    public void testValidateArchiveVersion()
    {
        doReturn(versionResult).when(defaultVersionService).validateArchiveVersion(applicationUser, version);
        assertEquals(versionResult, defaultVersionService.validateArchiveVersion(user, version));
    }

    @Test
    public void testValidateUnarchiveVersion()
    {
        doReturn(versionResult).when(defaultVersionService).validateUnarchiveVersion(applicationUser, version);
        assertEquals(versionResult, defaultVersionService.validateUnarchiveVersion(user, version));
    }

    @Test
    public void testValidateMoveToStartVersionSequence()
    {
        doReturn(versionResult).when(defaultVersionService).validateMoveToStartVersionSequence(applicationUser, 1L);
        assertEquals(versionResult, defaultVersionService.validateMoveToStartVersionSequence(user, 1L));
    }

    @Test
    public void testValidateIncreaseVersionSequence()
    {
        doReturn(versionResult).when(defaultVersionService).validateIncreaseVersionSequence(applicationUser, 1L);
        assertEquals(versionResult, defaultVersionService.validateIncreaseVersionSequence(user, 1L));
    }

    @Test
    public void testValidateDecreaseVersionSequence()
    {
        doReturn(versionResult).when(defaultVersionService).validateDecreaseVersionSequence(applicationUser, 1L);
        assertEquals(versionResult, defaultVersionService.validateDecreaseVersionSequence(user, 1L));
    }

    @Test
    public void testValidateMoveToEndVersionSequence()
    {
        doReturn(versionResult).when(defaultVersionService).validateMoveToEndVersionSequence(applicationUser, 1L);
        assertEquals(versionResult, defaultVersionService.validateMoveToEndVersionSequence(user, 1L));
    }

    @Test
    public void testValidateMoveVersionAfter()
    {
        doReturn(versionResult).when(defaultVersionService).validateMoveVersionAfter(applicationUser, 1L, null);
        assertEquals(versionResult, defaultVersionService.validateMoveVersionAfter(user, 1L, null));
    }

    @Test
    public void testGetUnresolvedIssuesCount()
    {
        doReturn(1L).when(defaultVersionService).getUnresolvedIssuesCount(applicationUser, version);
        assertEquals(1L, defaultVersionService.getUnresolvedIssuesCount(user, version));
    }

    @Test
    public void testValidateCreate()
    {
        doReturn(validationResult).when(defaultVersionService).validateCreate(applicationUser, versionBuilder);
        assertEquals(validationResult, defaultVersionService.validateCreate(user, versionBuilder));
    }

    @Test
    public void testCreate()
    {
        doReturn(serviceOutcome).when(defaultVersionService).create(applicationUser, validationResult);
        assertEquals(serviceOutcome, defaultVersionService.create(user, validationResult));
    }

    @Test
    public void testUpdate()
    {
        doReturn(serviceOutcome).when(defaultVersionService).update(applicationUser, validationResult);
        assertEquals(serviceOutcome, defaultVersionService.update(user, validationResult));
    }
}
