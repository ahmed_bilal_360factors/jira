package com.atlassian.jira.security.roles.actor;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.database.MockDbConnectionManager;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.security.roles.ProjectRoleActor;
import com.atlassian.jira.security.roles.RoleActor;
import com.atlassian.jira.security.roles.RoleActorDoesNotExistException;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;

public class TestUserRoleActorFactory
{
    @Mock
    private UserManager userManager;
    private final MockDbConnectionManager dbConnectionManager = new MockDbConnectionManager();
    private UserRoleActorFactory userRoleActorFactory;

    @Rule
    public InitMockitoMocks mocks = new InitMockitoMocks(this);

    @Before
    public void setUp() throws Exception
    {
        ComponentAccessor.initialiseWorker(new MockComponentWorker());
        setupMockUser("username");
        userRoleActorFactory = new UserRoleActorFactory(userManager, dbConnectionManager);
    }

    @Test
    public void testRoleActorContains() throws Exception
    {
        final RoleActor roleActor = userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "username");
        assertNotNull(roleActor);
        assertTrue(roleActor.contains(setupMockUser("username")));
    }

    @Test
    public void testRoleActorGetUsers() throws Exception
    {
        final ApplicationUser user = setupMockUser("username", "Daniel Boone", "dan@example.com");
        Mockito.when(userManager.getUserByKeyEvenWhenUnknown(eq("username"))).thenReturn(user);

        final RoleActor roleActor = userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "username");
        assertNotNull(roleActor);
        assertTrue(roleActor.contains(user));
        assertEquals(1, roleActor.getUsers().size());
        assertTrue(roleActor.getUsers().contains(user.getDirectoryUser()));
        assertEquals("Daniel Boone", roleActor.getDescriptor());
    }

    @Test
    public void testRoleActorNotContains() throws Exception
    {
        final RoleActor roleActor = userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "username");
        assertNotNull(roleActor);
        assertFalse(roleActor.contains(setupMockUser("test")));
    }

    @Test
    public void testRoleActorNotContainsNull() throws Exception
    {
        final RoleActor roleActor = userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "username");
        assertNotNull(roleActor);
        assertFalse(roleActor.contains((ApplicationUser)null));
    }

    @Test
    public void testUserRoleActorEqualsAndHashcode() throws Exception
    {
        final RoleActor roleActor1 = userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "username");
        final RoleActor roleActor2 = userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "username");
        assertEquals(roleActor1, roleActor2);
        assertEquals(roleActor2, roleActor1);
        assertEquals(roleActor1.hashCode(), roleActor2.hashCode());
    }

    @Test
    public void testUserRoleActorEqualsAndHashcodeIdIrrelevant() throws Exception
    {
        final RoleActor roleActor1 = userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "username");
        final RoleActor roleActor2 = userRoleActorFactory.createRoleActor(2L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "username");
        assertEquals(roleActor1, roleActor2);
        assertEquals(roleActor2, roleActor1);
        assertEquals(roleActor1.hashCode(), roleActor2.hashCode());
    }

    @Test
    public void testUserRoleActorEqualsAndHashcodeProjectRelevant() throws Exception
    {
        final RoleActor roleActor1 = userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "username");
        final RoleActor roleActor2 = userRoleActorFactory.createRoleActor(1L, 2L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "username");
        assertFalse(roleActor1.equals(roleActor2));
        assertFalse(roleActor2.equals(roleActor1));
        assertFalse(roleActor1.hashCode() == roleActor2.hashCode());
    }

    @Test
    public void testUserRoleActorEqualsAndHashcodeProjectRoleRelevant() throws Exception
    {
        final RoleActor roleActor1 = userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "username");
        final RoleActor roleActor2 = userRoleActorFactory.createRoleActor(1L, 1L, 2L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "username");
        assertFalse(roleActor1.equals(roleActor2));
        assertFalse(roleActor2.equals(roleActor1));
        assertFalse(roleActor1.hashCode() == roleActor2.hashCode());
    }

    @Test
    public void testUserRoleActorEqualsAndHashcodeUsernameRelevant() throws Exception
    {
        setupMockUser("another-username");
        final RoleActor roleActor1 = userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "username");
        final RoleActor roleActor2 = userRoleActorFactory.createRoleActor(1L, 1L, 2L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE,
                "another-username");
        assertFalse(roleActor1.equals(roleActor2));
        assertFalse(roleActor2.equals(roleActor1));
        assertFalse(roleActor1.hashCode() == roleActor2.hashCode());
    }

    @Test
    public void testUserRoleActorIncorrectType() throws Exception
    {
        try
        {
            userRoleActorFactory.createRoleActor(1L, 1L, 1L, "blah", "username");
            fail("should be bad!");
        }
        catch (final IllegalArgumentException yay)
        {}
    }

    @Test
    public void testRoleActorOptimizeAggregatesMultiple() throws Exception
    {
        Set userRoleActors = new HashSet();
        setupMockUser("somedude");
        setupMockUser("whatsit");
        userRoleActors.add(userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "username"));
        userRoleActors.add(userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "somedude"));
        userRoleActors.add(userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "whatsit"));
        userRoleActors = userRoleActorFactory.optimizeRoleActorSet(userRoleActors);
        assertNotNull(userRoleActors);
        assertEquals(1, userRoleActors.size());
        final RoleActor roleActor = (RoleActor) userRoleActors.iterator().next();
        assertTrue(roleActor instanceof UserRoleActorFactory.AggregateRoleActor);

        assertTrue(roleActor.contains(setupMockUser("username")));
        assertTrue(roleActor.contains(setupMockUser("somedude")));
        assertFalse(roleActor.contains(setupMockUser("whosee-whatsit")));
        assertFalse(roleActor.contains((ApplicationUser)null));
    }

    @Test
    public void testRoleActorOptimizeDoesNotAggregatesSingle() throws Exception
    {
        Set userRoleActors = new HashSet();
        userRoleActors.add(userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "username"));
        userRoleActors = userRoleActorFactory.optimizeRoleActorSet(userRoleActors);
        assertNotNull(userRoleActors);
        assertEquals(1, userRoleActors.size());
        final RoleActor roleActor = (RoleActor) userRoleActors.iterator().next();
        assertFalse(roleActor instanceof UserRoleActorFactory.AggregateRoleActor);
        assertTrue(roleActor instanceof UserRoleActorFactory.UserRoleActor);
    }

    @Test
    public void testAggregateRoleActorContains() throws Exception
    {
        final ApplicationUser fred = setupMockUser("fred");
        final ApplicationUser jim = setupMockUser("jim");
        final ApplicationUser david = setupMockUser("david");

        Set roleActors = new HashSet();
        roleActors.add(userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "fred"));
        roleActors.add(userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "jim"));
        roleActors.add(userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "david"));

        roleActors = Collections.unmodifiableSet(roleActors);
        roleActors = userRoleActorFactory.optimizeRoleActorSet(roleActors);
        assertNotNull(roleActors);
        final RoleActor roleActor = ((RoleActor) roleActors.iterator().next());

        assertThat(roleActor.getUsers(), containsInAnyOrder(fred.getDirectoryUser(), jim.getDirectoryUser(), david.getDirectoryUser()));
    }

    @Test
    public void testIllegalArgumentExceptionIfEntityNotFoundThrownByFactory() throws Exception
    {
        Mockito.when(userManager.getUserByKeyEvenWhenUnknown(any(String.class))).thenReturn(null);

        try
        {
            userRoleActorFactory.createRoleActor(1L, 1L, 1L, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "fred");
            fail("RoleActorDoesNotExistException should have been thrown");
        }
        catch (final RoleActorDoesNotExistException yay)
        {}
    }

    @Test
    public void testGetAllRoleActorsForUser()
    {
        final ApplicationUser user = setupMockUser("user");

        final ResultRow expectedActor1 = new ResultRow(1L, 100L, 200L, "atlassian-user-role-actor", "user");
        final ResultRow expectedActor2 = new ResultRow(2L, 101L, 201L, "atlassian-user-role-actor", "user");
        final ResultRow expectedActor3 = new ResultRow(3L, 102L, 202L, "atlassian-user-role-actor", "user");

        final List<ResultRow> queryResults = ImmutableList.of(expectedActor1, expectedActor2, expectedActor3);

        dbConnectionManager.setQueryResults
                ("select pra.id, pra.pid, pra.projectroleid, pra.roletype, pra.roletypeparameter\n"
                                + "from projectroleactor pra\n"
                                + "where pra.roletype = 'atlassian-user-role-actor' and pra.roletypeparameter = 'user' and pra.pid is not null",
                        queryResults);

        final Set<ProjectRoleActor> actors = userRoleActorFactory.getAllRoleActorsForUser(user);
        final Set<ResultRow> actorsAsRows = Sets.newHashSet(Iterables.transform(actors, new Function<ProjectRoleActor, ResultRow>()
        {
            @Override
            public ResultRow apply(final ProjectRoleActor projectRoleActor)
            {
                return toResultRow(projectRoleActor);
            }
        }));

        assertThat(actorsAsRows, containsInAnyOrder(expectedActor1, expectedActor2, expectedActor3));
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testGetAllRoleActorsForUserNoActors()
    {
        final ApplicationUser user = setupMockUser("user");

        List<ResultRow> queryResults = ImmutableList.of();

        dbConnectionManager.setQueryResults
                ("select pra.id, pra.pid, pra.projectroleid, pra.roletype, pra.roletypeparameter\n"
                                + "from projectroleactor pra\n"
                                + "where pra.roletype = 'atlassian-user-role-actor' and pra.roletypeparameter = 'user' and pra.pid is not null",
                        queryResults);

        final Set<ProjectRoleActor> actors = userRoleActorFactory.getAllRoleActorsForUser(user);

        assertThat(actors, Matchers.<ProjectRoleActor>empty());
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testGetAllRoleActorsForNullUser()
    {
        final Set<ProjectRoleActor> actors = userRoleActorFactory.getAllRoleActorsForUser(null);

        assertTrue(actors.isEmpty());
    }


    private ApplicationUser setupMockUser(final String name)
    {
        return setupMockUser(name, name, name + "@example.com");
    }

    private ApplicationUser setupMockUser(final String name, final String displayName, final String email)
    {
        ApplicationUser user = new MockApplicationUser(name, displayName, email);
        Mockito.when(userManager.getUserByKeyEvenWhenUnknown(eq(name))).thenReturn(user);
        userManager.updateUser(user);
        return user;
    }

    private ResultRow toResultRow(ProjectRoleActor actor)
    {
        return new ResultRow(actor.getId(), actor.getProjectId(), actor.getProjectRoleId(), actor.getType(), actor.getParameter());
    }
}
