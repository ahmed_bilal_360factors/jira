package com.atlassian.velocity;

import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;

import com.atlassian.fugue.Option;
import com.atlassian.velocity.subpackage.VelocityScoping;
import com.atlassian.velocity.subpackage.VelocityScopingFactory;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.apache.velocity.util.introspection.MethodMap;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class JiraMethodMapTest
{

    @Test
    public void shouldResolveCorrectMethodForPrimitiveTypes()
    {
        doTests(ImmutableList.of(
                getTestCase(Primitives.class, "testBoolean", new Object(), true),
                getTestCase(Primitives.class, "testInt", new Object(), 1),
                getTestCase(Primitives.class, "testShort", new Object(), (short) 1),
                getTestCase(Primitives.class, "testByte", new Object(), (byte) 1),
                getTestCase(Primitives.class, "testChar", new Object(), (char) 1),
                getTestCase(Primitives.class, "testLong", new Object(), 1l),
                getTestCase(Primitives.class, "testFloat", new Object(), 1.0f),
                getTestCase(Primitives.class, "testDouble", new Object(), 1.0d)));
    }

    @Test
    public void shouldResolveCorrectMethodForBoxedTypes()
    {
        doTests(ImmutableList.of(
                getTestCase(BoxedTypes.class, "testBoolean", new Object(), true),
                getTestCase(BoxedTypes.class, "testInt", new Object(), 1),
                getTestCase(BoxedTypes.class, "testShort", new Object(), (short) 1),
                getTestCase(BoxedTypes.class, "testByte", new Object(), (byte) 1),
                getTestCase(BoxedTypes.class, "testChar", new Object(), (char) 1),
                getTestCase(BoxedTypes.class, "testLong", new Object(), 1l),
                getTestCase(BoxedTypes.class, "testFloat", new Object(), 1.0f),
                getTestCase(BoxedTypes.class, "testDouble", new Object(), 1.0d)));
    }

    @Test
    public void shouldResolveCorrectMethodForClassTypes()
    {
        doTests(ImmutableList.of(
                getTestCase(ObjectArguments.class, "testStr", "Str"),
                getTestCase(ObjectArguments.class, "testStr", "Str"),
                //null parameters should be resolved as Object types
                getTestCase(ObjectArguments.class, "testObject", new Object()),
                getTestCase(ObjectArguments.class, "testObject", new Object[] { null }),
                getTestCase(ObjectArguments.class, "testTypes", new Primitives(), new BoxedTypes())
        ));
    }

    @Test
    public void shouldNotResolveWhenNoMethodCanBeMatched()
    {
        doTests(ImmutableList.of(
                getTestCase(Primitives.class, null, "Str"),
                getTestCase(ObjectArguments.class, null, "Str", "Str"),
                getTestCase(ObjectArguments.class, null, new BoxedTypes(), new Primitives())));
    }

    @Test
    public void shouldResolveCorrectForOverloadedMethods()
    {
        doTests(ImmutableList.of(
                getTestCase(getMethod(Overloaded.class, "testStr", String.class), Overloaded.class, "testStr", "str1"),
                getTestCase(getMethod(Overloaded.class, "testStr", String.class, String.class), Overloaded.class, "testStr", "str1", "str2"),
                getTestCase(getMethod(Overloaded.class, "testTypeHierarchy", String.class), Overloaded.class, "testTypeHierarchy", "str1"),
                getTestCase(getMethod(Overloaded.class, "testTypeHierarchy", Object.class), Overloaded.class, "testTypeHierarchy", new Object()),
                getTestCase(getMethod(Overloaded.class, "testTypes", Integer.TYPE), Overloaded.class, "testTypes", 1)
        ));
    }

    @Test
    public void shouldResolveCorrectForInheritedMethods()
    {
        final VelocityScoping privateImpl = VelocityScopingFactory.getTestdObject();
        final Class<? extends VelocityScoping> privateImplClass = privateImpl.getClass();
        doTests(ImmutableList.of(
                getTestCase(getMethod(TestInheritance.class, "testMe"), TestInheritance.class, "testMe"),
                //test for situation when introspecting private/package local class that implements public interface
                //default Class.getMethods will return wrong method
                getTestCase(getMethod(VelocityScoping.class, "testMethod"), privateImplClass, "testMethod")));
    }

    @Test (expected = MethodMap.AmbiguousException.class)
    public void shouldThrowExceptionAmbiguousMethodParameters()
    {
        doTests(ImmutableList.of(getTestCase(getMethod(Ambiguous.class, "testInt", Integer.TYPE), Ambiguous.class, "testInt", 1)));
    }

    private void doTests(final List<TestCase> testCases)
    {
        for (final TestCase testCase : testCases)
        {
            final MethodMap methodMap = new JiraMethodMap(testCase.testedClass);
            final Method method = methodMap.find(testCase.methodName.getOrNull(), testCase.methodParameters);
            assertThat("Wrong method resolved for test case " + testCase.toString(), method, CoreMatchers.equalTo(testCase.expectedMethod.getOrNull()));
        }
    }

    private TestCase getTestCase(final Class<?> testedClass, final String methodName, final Object... methodParameters)
    {
        return new TestCase(testedClass, Option.option(methodName), methodParameters);
    }

    private TestCase getTestCase(final Option<Method> method, final Class<?> testedClass, final String methodName, final Object... methodParameters)
    {
        return new TestCase(method, testedClass, Option.some(methodName), methodParameters);
    }

    private Option<Method> getMethod(final Class<?> testedClass, final String methodName, final Class<?>... paramTypes)
    {
        try
        {
            return Option.some(testedClass.getMethod(methodName, paramTypes));
        }
        catch (final NoSuchMethodException e)
        {
            fail(MessageFormat.format("Method {0} is not defined in class {1} error message: {2}", methodName, testedClass, e.getMessage()));
            return Option.none();
        }
    }

    private Option<Method> getMethodByName(final Class<?> clazz, final Option<String> methodNameOption)
    {
        return methodNameOption.map(new Function<String, Method>()
        {
            @Override
            public Method apply(final String methodName)
            {
                for (final Method method : clazz.getMethods())
                {
                    if (methodName.equals(method.getName()))
                    {
                        return method;
                    }
                }
                fail(MessageFormat.format("Method {0} is not defined in class {1}", methodName, clazz));
                return null;
            }
        });
    }

    @SuppressWarnings ("UnusedDeclaration")
    private interface TestInterface
    {
        void testMe();
    }

    @SuppressWarnings ("UnusedDeclaration")
    public static class Primitives
    {
        public void testBoolean(final Object obj, final boolean arg) {}

        public void testInt(final Object obj, final int arg) {}

        public void testShort(final Object obj, final short arg) {}

        public void testByte(final Object obj, final byte arg) {}

        public void testChar(final Object obj, final char arg) {}

        public void testLong(final Object obj, final long arg) {}

        public void testFloat(final Object obj, final float arg) {}

        public void testDouble(final Object obj, final double arg) {}
    }

    @SuppressWarnings ("UnusedDeclaration")
    public static class BoxedTypes
    {
        public void testBoolean(final Object obj, final Boolean arg) {}

        public void testInt(final Object obj, final Integer arg) {}

        public void testShort(final Object obj, final Short arg) {}

        public void testByte(final Object obj, final Byte arg) {}

        public void testChar(final Object obj, final Character arg) {}

        public void testLong(final Object obj, final Long arg) {}

        public void testFloat(final Object obj, final Float arg) {}

        public void testDouble(final Object obj, final Double arg) {}
    }

    @SuppressWarnings ("UnusedDeclaration")
    public static class ObjectArguments
    {
        public Object testStr(final String arg) {return null;}

        public Object testObject(final Object arg) {return null;}

        public Object testTypes(final Primitives arg1, final BoxedTypes arg2) {return null;}
    }

    @SuppressWarnings ("UnusedDeclaration")
    public static class Overloaded
    {
        public Object testStr(final String arg) {return null;}

        public Object testStr(final String arg, final String agr2) {return null;}

        public Object testTypeHierarchy(final Object arg) {return null;}

        public Object testTypeHierarchy(final String arg) {return null;}

        //those seems to be ambiguous when invoking with Integer but we assume that unboxing should be preferable to other
        //types resolution
        public void testTypes(final int arg)
        {}

        public void testTypes(final Comparable arg) {}

    }

    @SuppressWarnings ("UnusedDeclaration")
    public static class Ambiguous
    {
        public void testInt(final int arg) {}

        public void testInt(final Integer arg) {}
    }

    @SuppressWarnings ("UnusedDeclaration")
    public static abstract class TestAbstract
    {
        public abstract void testMe();
    }

    public static class TestInheritance extends TestAbstract implements TestInterface
    {
        @Override
        public void testMe() {}
    }

    private class TestCase
    {
        private final Class<?> testedClass;
        private final Object[] methodParameters;
        private final Option<String> methodName;
        private final Option<Method> expectedMethod;

        private TestCase(final Class<?> testedClass, final Option<String> methodName, final Object[] methodParameters)
        {
            this(getMethodByName(testedClass, methodName), testedClass, methodName, methodParameters);
        }

        private TestCase(final Option<Method> method, final Class<?> testedClass, final Option<String> methodName, final Object[] methodParameters)
        {
            this.testedClass = testedClass;
            this.methodParameters = methodParameters;
            this.methodName = methodName;
            this.expectedMethod = method;
        }

        @Override
        public String toString()
        {
            return "TestCase{" +
                    "testedClass=" + testedClass +
                    ", methodParameters=" + Arrays.toString(methodParameters) +
                    ", methodName='" + methodName + '\'' +
                    ", expectedMethod=" + expectedMethod +
                    '}';
        }
    }
}