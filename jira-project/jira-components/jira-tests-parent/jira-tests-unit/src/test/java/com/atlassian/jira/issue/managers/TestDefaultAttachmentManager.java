package com.atlassian.jira.issue.managers;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.core.util.WebRequestUtils;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.AttachmentPathManager;
import com.atlassian.jira.config.util.MockAttachmentPathManager;
import com.atlassian.jira.issue.AttachmentIndexManager;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.AttachmentConstants;
import com.atlassian.jira.issue.attachment.AttachmentCopyException;
import com.atlassian.jira.issue.attachment.AttachmentIdSequencer;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.AttachmentStore;
import com.atlassian.jira.issue.attachment.CreateAttachmentParamsBean;
import com.atlassian.jira.issue.attachment.StoreAttachmentResult;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.ThumbnailAccessor;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.mime.MimeManager;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.jira.web.util.AttachmentException;
import com.atlassian.util.concurrent.Promises;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.opensymphony.module.propertyset.PropertySet;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import static com.atlassian.jira.TestFixtures.mockAttachment;
import static com.atlassian.jira.TestFixtures.mockMutableIssue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultAttachmentManager
{

    private final MockApplicationUser user = new MockApplicationUser("userKey", "username");
    public static final long DEFAULT_ATTACHMENT_ID = 1l;

    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    final GenericValue attachment2 = new MockGenericValue(AttachmentConstants.ATTACHMENT_ENTITY_NAME, FieldMap.build("id", new Long(101), "issue", new Long(1), "filename", "C:\temp"));

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    @AvailableInContainer
    private UserKeyService userKeyService;

    @AvailableInContainer
    private AttachmentPathManager attachmentPathManager = new MockAttachmentPathManager();

    @Mock
    private OfBizDelegator ofBizDelegator;

    @Mock
    private UserManager userManager;

    @Mock
    private ComponentLocator componentLocator;

    @Mock
    private IssueFactory issueFactory;

    @Mock
    private IssueManager issueManager;

    @Mock
    private MimeManager mimeManager;

    @Mock
    private AttachmentStore attachmentStore;

    @Mock
    private AttachmentIndexManager attachmentIndexManager;

    @Mock
    private I18nHelper i18nHelper;
    @Mock
    private Attachment attachment;
    @Mock
    private StreamAttachmentStore streamAttachmentStore;
    @Mock
    private AttachmentIdSequencer attachmentIdSequencer;
    @Mock
    private ThumbnailAccessor thumbnailAccessor;

    @Mock
    private PermissionManager permissionManager;

    private DefaultAttachmentManager attachmentManager;

    @Before
    public void init() throws Exception
    {
        when(componentLocator.getComponent(IssueFactory.class)).thenReturn(issueFactory);

        I18nHelper.BeanFactory beanFactory = new MockI18nBean.MockI18nBeanFactory();

        DefaultAttachmentManager manager = new DefaultAttachmentManager(
                issueManager,
                ofBizDelegator,
                mimeManager,
                applicationProperties,
                attachmentPathManager,
                componentLocator,
                beanFactory,
                userManager,
                attachmentStore,
                attachmentIndexManager,
                streamAttachmentStore,
                thumbnailAccessor,
                attachmentIdSequencer
        );

        attachmentManager = spy(manager);
        PropertySet propset = mock(PropertySet.class);
        doReturn(propset).when(attachmentManager).createAttachmentPropertySet(any(GenericValue.class), any(Map.class));

        //plug methods which perform real IO operations
        doReturn(StoreAttachmentResult.created())
                .when(attachmentManager).putAttachmentFileToAttachmentStore(any(AttachmentKey.class), any(File.class));
        when(attachmentIdSequencer.getNextId())
                .thenReturn(DEFAULT_ATTACHMENT_ID);
    }

    private void setUserOsAndLinuxScreenshotApplet(int userOS, boolean screenshotAppletLinuxEnabled)
    {
        when(applicationProperties.getOption(APKeys.JIRA_SCREENSHOTAPPLET_LINUX_ENABLED)).thenReturn(screenshotAppletLinuxEnabled);
        // WARNING: when(attachmentManger.getUsersOS()).thenReturn(userOS); is not reliable.  Read JavaDocs for spy!
        doReturn(userOS).when(attachmentManager).getUsersOS();
    }

    @Test
    public void testIsAttachmentsEnabledAndPathSetHappyPath()
    {
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWATTACHMENTS)).thenReturn(true);
        assertTrue(attachmentManager.attachmentsEnabled());
    }

    @Test
    public void testIsAttachmentsEnabledAndPathSetAttachmentsDisabled()
    {
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWATTACHMENTS)).thenReturn(false);
        assertFalse(attachmentManager.attachmentsEnabled());
    }

    @Test
    public void testIsScreenshotAppletEnabledPropertyTrue()
    {
        when(applicationProperties.getOption(APKeys.JIRA_SCREENSHOTAPPLET_ENABLED)).thenReturn(true);
        assertTrue(attachmentManager.isScreenshotAppletEnabled());
    }

    @Test
    public void testIsScreenshotAppletEnabledPropertyFalse()
    {
        when(applicationProperties.getOption(APKeys.JIRA_SCREENSHOTAPPLET_ENABLED)).thenReturn(false);
        assertFalse(attachmentManager.isScreenshotAppletEnabled());
    }

    @Test
    public void testIsScreenshotAppleSupportedByOSOSX()
    {
        setUserOsAndLinuxScreenshotApplet(WebRequestUtils.MACOSX, false);
        assertTrue(attachmentManager.isScreenshotAppletSupportedByOS());
    }

    @Test
    public void testIsScreenshotAppleSupportedByOSLinuxDisabled()
    {
        setUserOsAndLinuxScreenshotApplet(WebRequestUtils.LINUX, false);
        assertFalse(attachmentManager.isScreenshotAppletSupportedByOS());
    }

    @Test
    public void testIsScreenshotAppleSupportedByOSLinuxEnabled()
    {
        setUserOsAndLinuxScreenshotApplet(WebRequestUtils.LINUX, true);
        assertTrue(attachmentManager.isScreenshotAppletSupportedByOS());
    }

    @Test
    public void testIsScreenshotAppleSupportedByOSWindows()
    {
        setUserOsAndLinuxScreenshotApplet(WebRequestUtils.WINDOWS, false);
        assertTrue(attachmentManager.isScreenshotAppletSupportedByOS());
    }

    @Test
    public void testIsScreenshotAppletSupportedByOSUnsupportedOSLinuxDisabled()
    {
        setUserOsAndLinuxScreenshotApplet(WebRequestUtils.OTHER, false);
        assertFalse(attachmentManager.isScreenshotAppletSupportedByOS());
    }

    @Test
    public void testIsScreenshotAppletSupportedByOSUnsupportedOSLinuxEnabled()
    {
        setUserOsAndLinuxScreenshotApplet(WebRequestUtils.OTHER, true);
        assertTrue(attachmentManager.isScreenshotAppletSupportedByOS());
    }

    @Test
    public void testCreateAttachmentCopySourceFile() throws Exception
    {
        CreateAttachmentParamsBean params = prepareCreateAttachmentParamsBean()
                .attachmentProperties(ImmutableMap.<String, Object>of())
                .copySourceFile(true)
                .build();
        attachmentManager.createAttachmentCopySourceFile(params.getFile(), params.getFilename(), params.getContentType(),
                params.getAuthor().getUsername(), params.getIssue(), params.getAttachmentProperties(),
                params.getCreatedTime());
        verifyStoreParamsToDB(params);
        verifyCreateAttachmentCopyIOOperations(params);
    }

    @Test
    public void testCreateAttachmentCopySourceFileWithParamsAsBean() throws Exception
    {
        CreateAttachmentParamsBean params = prepareCreateAttachmentParamsBean()
                .zip(false).thumbnailable(false).attachmentProperties(ImmutableMap.<String, Object>of())
                .copySourceFile(true)
                .build();
        attachmentManager.createAttachment(params);
        verifyStoreParamsToDB(params);
        verifyCreateAttachmentCopyIOOperations(params);
    }

    @Test
    public void testCreateAttachmentWithParamsAsBeanFullCall() throws Exception{
        CreateAttachmentParamsBean params = prepareCreateAttachmentParamsBean()
                .zip(false).thumbnailable(true).attachmentProperties(ImmutableMap.<String, Object>of())
                .copySourceFile(false)
                .build();
        attachmentManager.createAttachment(params);
        verifyStoreParamsToDB(params);
        verifyCreateAttachmentIOOperations(params);
    }

    @Test
    public void testCreateAttachmentWithParamsAsBeanSimplestCall() throws Exception{
        CreateAttachmentParamsBean params = prepareCreateAttachmentParamsBean().build();
        attachmentManager.createAttachment(params);
        verifyStoreParamsToDB(params);
        verifyCreateAttachmentIOOperations(params);
    }


    /**
     * Tests {@link DefaultAttachmentManager#createAttachment(org.ofbiz.core.entity.GenericValue,
     * com.atlassian.crowd.embedded.api.User, String, String, Long, java.util.Map, java.util.Date)}
     */
    @Test
    public void shouldCreateDatabaseObjectsWhenWeCreateAttachment() throws Exception
    {
        CreateAttachmentParamsBean params = prepareCreateAttachmentParamsBean()
                .attachmentProperties(ImmutableMap.<String, Object>of())
                .build();

        final long expectedAttachmentId = 212312l;
        when(attachmentIdSequencer.getNextId())
                .thenReturn(expectedAttachmentId);

        attachmentManager.createAttachment(params.getIssue().getGenericValue(), params.getAuthor().getDirectoryUser(),
                params.getContentType(), params.getFilename(), params.getFile().length(), params.getAttachmentProperties(),
                params.getCreatedTime());

        verifyStoreParamsToDB(params, expectedAttachmentId);
        verify(attachmentManager, never()).putAttachmentFileToAttachmentStore(any(AttachmentKey.class), any(File.class));
    }

    /**
     * Tests {@link DefaultAttachmentManager#createAttachment(java.io.File, String, String,
     * com.atlassian.crowd.embedded.api.User, com.atlassian.jira.issue.Issue, java.util.Map, java.util.Date)}
     */
    @Test
    public void testCreateAttachmentMethodWithObjects() throws Exception
    {
        CreateAttachmentParamsBean params = prepareCreateAttachmentParamsBean()
                .attachmentProperties(ImmutableMap.<String, Object>of())
                .build();
        attachmentManager.createAttachment(params.getFile(), params.getFilename(), params.getContentType(), params.getAuthor().getDirectoryUser(), params.getIssue(), params.getAttachmentProperties(), params.getCreatedTime());
        verifyStoreParamsToDB(params);
        verifyCreateAttachmentIOOperations(params);
    }

    /**
     * Tests {@link DefaultAttachmentManager#createAttachment(java.io.File, String, String,
     * com.atlassian.crowd.embedded.api.User, com.atlassian.jira.issue.Issue, Boolean, Boolean, java.util.Map,
     * java.util.Date)}
     */
    @Test
    public void testCreateAttachmentWithObjectsFullCall() throws Exception
    {
        CreateAttachmentParamsBean params = prepareCreateAttachmentParamsBean()
                .zip(true).thumbnailable(true).attachmentProperties(ImmutableMap.<String, Object>of())
                .build();
        attachmentManager.createAttachment(params.getFile(), params.getFilename(), params.getContentType(), params.getAuthor().getDirectoryUser(), params.getIssue(), params.getZip(), params.getThumbnailable(), params.getAttachmentProperties(), params.getCreatedTime());
        verifyStoreParamsToDB(params);
        verifyCreateAttachmentIOOperations(params);
    }

    /**
     * Tests {@link DefaultAttachmentManager#createAttachment(java.io.File, String, String,
     * com.atlassian.crowd.embedded.api.User, org.ofbiz.core.entity.GenericValue, java.util.Map, java.util.Date)}
     */
    @Test
    public void testCreateAttachmentWithIssueAsGenericValue() throws Exception
    {
        CreateAttachmentParamsBean params = prepareCreateAttachmentParamsBean()
                .attachmentProperties(ImmutableMap.<String, Object>of())
                .build();
        when(issueFactory.getIssue(params.getIssue().getGenericValue())).thenReturn((MutableIssue)params.getIssue());
        attachmentManager.createAttachment(params.getFile(), params.getFilename(), params.getContentType(), params.getAuthor().getDirectoryUser(), params.getIssue().getGenericValue(), params.getAttachmentProperties(), params.getCreatedTime());
        verifyStoreParamsToDB(params);
        verifyCreateAttachmentIOOperations(params);
    }

    /**
     * Tests {@link DefaultAttachmentManager#createAttachment(java.io.File, String, String,
     * com.atlassian.crowd.embedded.api.User, org.ofbiz.core.entity.GenericValue, Boolean, Boolean, java.util.Map,
     * java.util.Date)}
     */
    @Test
    public void testCreateAttachmentWithIssueAsGenericValueFullCall() throws Exception
    {
        CreateAttachmentParamsBean params = prepareCreateAttachmentParamsBean()
                .zip(true).thumbnailable(false).attachmentProperties(ImmutableMap.<String, Object>of())
                .build();
        when(issueFactory.getIssue(params.getIssue().getGenericValue())).thenReturn((MutableIssue)params.getIssue());
        attachmentManager.createAttachment(params.getFile(), params.getFilename(), params.getContentType(), params.getAuthor().getDirectoryUser(), params.getIssue().getGenericValue(), params.getZip(), params.getThumbnailable(), params.getAttachmentProperties(), params.getCreatedTime());
        verifyStoreParamsToDB(params);
        verifyCreateAttachmentIOOperations(params);
    }

    /**
     * Tests {@link DefaultAttachmentManager#createAttachment(java.io.File, String, String, com.atlassian.crowd.embedded.api.User, org.ofbiz.core.entity.GenericValue)}
     */
    @Test
    public void testCreateAttachmentShortWithIssueAsGenericValue() throws Exception
    {
        CreateAttachmentParamsBean params = prepareCreateAttachmentParamsBean().build();
        when(issueFactory.getIssue(params.getIssue().getGenericValue())).thenReturn((MutableIssue)params.getIssue());
        attachmentManager.createAttachment(params.getFile(), params.getFilename(), params.getContentType(), params.getAuthor().getDirectoryUser(), params.getIssue().getGenericValue());
        verifyStoreParamsToDB(params);
        verifyCreateAttachmentIOOperations(params);
    }


    /**
     * Tests {@link DefaultAttachmentManager#createAttachment(java.io.File, String, String, com.atlassian.crowd.embedded.api.User, com.atlassian.jira.issue.Issue)}
     */
    @Test
    public void testCreateAttachmentShortWithIssueAsObject() throws Exception
    {
        CreateAttachmentParamsBean params = prepareCreateAttachmentParamsBean().build();
        attachmentManager.createAttachment(params.getFile(), params.getFilename(), params.getContentType(), params.getAuthor().getDirectoryUser(), params.getIssue());
        verifyStoreParamsToDB(params);
        verifyCreateAttachmentIOOperations(params);
    }

    @Test
    public void testNoInsertAttachmentEntryToDbWhenPutToStoreFailed() throws Exception
    {
        final CreateAttachmentParamsBean params = prepareCreateAttachmentParamsBean()
                .attachmentProperties(ImmutableMap.<String, Object>of())
                .build();

        doThrow(new RuntimeException())
                .when(attachmentManager).putAttachmentFileToAttachmentStore(any(AttachmentKey.class), any(File.class));

        final I18nHelper mockI18N = mock(I18nHelper.class);

        try
        {
            attachmentManager.createAttachment(params.getFile(), params.getFilename(), params.getContentType(),
                    params.getAuthor().getDirectoryUser(), params.getIssue(), params.getAttachmentProperties(), params.getCreatedTime());
            fail("Should get AttachmentException");
        }
        catch (final AttachmentException e)
        {
            //noinspection unchecked
            verify(ofBizDelegator, times(0)).createValue(eq(AttachmentConstants.ATTACHMENT_ENTITY_NAME), any(Map.class));
        }
    }

    private CreateAttachmentParamsBean.Builder prepareCreateAttachmentParamsBean()
    {
        final File file = mock(File.class);
        when(file.length()).thenReturn(555L);

        final String filename = "myAttachmentFilename";
        final String contentType = "contentType";
        final ApplicationUser user = new MockApplicationUser("userKey", "username");
        final MockIssue issue = new MockIssue(123L);
        issue.setProjectObject(new MockProject(999L, "PKEY"));
        issue.setKey("PKEY-444");
        final CreateAttachmentParamsBean.Builder builder = new CreateAttachmentParamsBean.Builder(file, filename, contentType, user, issue);

        when(ofBizDelegator.createValue(eq(AttachmentConstants.ATTACHMENT_ENTITY_NAME), any(Map.class))).thenAnswer(new Answer<GenericValue>()
        {
            @Override
            public GenericValue answer(InvocationOnMock invocation) throws Throwable
            {
                HashMap<Object, Object> result = Maps.newHashMap((Map<?, ?>) invocation.getArguments()[1]);
                result.put("id", 1L);
                return new MockGenericValue(AttachmentConstants.ATTACHMENT_ENTITY_NAME, result);
            }
        });

        when(mimeManager.getSanitisedMimeType(contentType, filename)).thenReturn(contentType);
        when(userManager.getUserByName(user.getUsername())).thenReturn(user);
        when(userKeyService.getKeyForUsername(user.getUsername())).thenReturn(user.getKey());
        return builder;
    }

    private void verifyStoreParamsToDB(final CreateAttachmentParamsBean params)
    {
        verifyStoreParamsToDB(params, DEFAULT_ATTACHMENT_ID);
    }

    private void verifyStoreParamsToDB(final CreateAttachmentParamsBean params, final long expectedAttachmentId)
    {
        List<Matcher<? super Map<String, Object>>> paramsMatchers = new ArrayList<Matcher<? super Map<String, Object>>>();
        paramsMatchers.add(Matchers.<String, Object>hasEntry("issue", params.getIssue().getId()));
        paramsMatchers.add(Matchers.<String, Object>hasEntry("author", params.getAuthor().getKey()));
        paramsMatchers.add(Matchers.<String, Object>hasEntry("mimetype", params.getContentType()));
        paramsMatchers.add(Matchers.<String, Object>hasEntry("filesize", params.getFile().length()));
        paramsMatchers.add(Matchers.<String, Object>hasEntry("filename", params.getFilename()));
        paramsMatchers.add(Matchers.<String, Object>hasEntry("id", expectedAttachmentId));

        if (params.getZip() != null)
        {
            paramsMatchers.add(Matchers.<String, Object>hasEntry("zip", params.getZip() ? 1 : 0));
        }
        if (params.getThumbnailable() != null)
        {
            paramsMatchers.add(Matchers.<String, Object>hasEntry("thumbnailable", params.getThumbnailable() ? 1 : 0));
        }
        final Matcher<Map<String, Object>> allOfMatchers = Matchers.allOf(paramsMatchers);
        verify(ofBizDelegator).createValue(eq(AttachmentConstants.ATTACHMENT_ENTITY_NAME), argThat(allOfMatchers));
    }


    private void verifyCreateAttachmentIOOperations(CreateAttachmentParamsBean params) throws Exception{
        verify(attachmentManager).safelyPutAttachmentFileToAttachmentStore(
                any(AttachmentKey.class), same(params.getFile()), eq(true), same(params.getAuthor()));

        if(params.getAttachmentProperties() != null)
        {
            verify(attachmentManager).createAttachmentPropertySet(any(GenericValue.class), same(params.getAttachmentProperties()));
        }
    }


    private void verifyCreateAttachmentCopyIOOperations(CreateAttachmentParamsBean params) throws Exception{
        verify(attachmentManager).safelyPutAttachmentFileToAttachmentStore(any(AttachmentKey.class), same(params.getFile()),
                eq(!params.getCopySourceFile()), same(params.getAuthor()));

        if(params.getAttachmentProperties() != null)
        {
            verify(attachmentManager).createAttachmentPropertySet(any(GenericValue.class), same(params.getAttachmentProperties()));
        }
    }

    @Test
    public void testGetAttachment() throws GenericEntityException
    {
        when(ofBizDelegator.findById(AttachmentConstants.ATTACHMENT_ENTITY_NAME, 101L)).thenReturn(attachment2);

        Attachment attachment = attachmentManager.getAttachment(new Long(101));
        assertNotNull(attachment);
        assertEquals(new Long(101), attachment.getId());
    }

    @Test
    @SuppressWarnings ("ConstantConditions")
    public void testOldDeprecatedCreateAttachmentProperlyReactsToNullFileAndFilename() throws Exception
    {
        // This test should prevent us from re-introducing bugs like JDEV-30574
        final CreateAttachmentParamsBean params = prepareCreateAttachmentParamsBean()
                .attachmentProperties(ImmutableMap.<String, Object>of())
                .build();

        final String nullFileName = null;
        final File nullFile = null;

        // createAttachment #1
        assertNull(attachmentManager.createAttachment(
                params.getFile(), nullFileName, params.getContentType(), params.getAuthor().getDirectoryUser(),
                params.getIssue(), params.getAttachmentProperties(), params.getCreatedTime()
        ));

        assertNull(attachmentManager.createAttachment(
                nullFile, params.getFilename(), params.getContentType(), params.getAuthor().getDirectoryUser(),
                params.getIssue(), params.getAttachmentProperties(), params.getCreatedTime()
        ));

        // createAttachment #2
        assertNull(attachmentManager.createAttachment(
                params.getFile(), nullFileName, params.getContentType(), params.getAuthor().getDirectoryUser(),
                params.getIssue().getGenericValue(), params.getAttachmentProperties(), params.getCreatedTime()
        ));

        assertNull(attachmentManager.createAttachment(
                nullFile, params.getFilename(), params.getContentType(), params.getAuthor().getDirectoryUser(),
                params.getIssue().getGenericValue(), params.getAttachmentProperties(), params.getCreatedTime()
        ));

        // createAttachment #3
        assertNull(attachmentManager.createAttachment(
                params.getFile(), nullFileName, params.getContentType(), params.getAuthor().getDirectoryUser(),
                params.getIssue(), params.getZip(), params.getThumbnailable(), params.getAttachmentProperties(),
                params.getCreatedTime()
        ));

        assertNull(attachmentManager.createAttachment(
                nullFile, params.getFilename(), params.getContentType(), params.getAuthor().getDirectoryUser(),
                params.getIssue(), params.getZip(), params.getThumbnailable(), params.getAttachmentProperties(),
                params.getCreatedTime()
        ));

        // createAttachment #4
        assertNull(attachmentManager.createAttachment(
                params.getFile(), nullFileName, params.getContentType(), params.getAuthor().getDirectoryUser(),
                params.getIssue().getGenericValue(), params.getZip(), params.getThumbnailable(), params.getAttachmentProperties(),
                params.getCreatedTime()
        ));

        assertNull(attachmentManager.createAttachment(
                nullFile, params.getFilename(), params.getContentType(), params.getAuthor().getDirectoryUser(),
                params.getIssue().getGenericValue(), params.getZip(), params.getThumbnailable(), params.getAttachmentProperties(),
                params.getCreatedTime()
        ));

        // createAttachment #5
        assertNull(attachmentManager.createAttachment(
                params.getFile(), nullFileName, params.getContentType(), params.getAuthor().getDirectoryUser(),
                params.getIssue()
        ));

        assertNull(attachmentManager.createAttachment(
                nullFile, params.getFilename(), params.getContentType(), params.getAuthor().getDirectoryUser(),
                params.getIssue()
        ));

        // createAttachment #6
        assertNull(attachmentManager.createAttachment(
                params.getFile(), nullFileName, params.getContentType(), params.getAuthor().getDirectoryUser(),
                params.getIssue().getGenericValue()
        ));

        assertNull(attachmentManager.createAttachment(
                nullFile, params.getFilename(), params.getContentType(), params.getAuthor().getDirectoryUser(),
                params.getIssue().getGenericValue()
        ));
    }

    @Test
    public void testCopyAttachment() throws Exception
    {
        Attachment expected = new Attachment(null, attachment2);
        Attachment attachment = mockAttachment(123, "image/jpeg", "dog.jpg", 567, true);
        ApplicationUser user = new MockApplicationUser("John Doe");
        String newIssueKey = "JDEV-30222";
        MutableIssue newIssue = mockMutableIssue(123);

        when(ofBizDelegator.createValue(eq(AttachmentConstants.ATTACHMENT_ENTITY_NAME), any(Map.class))).thenReturn(attachment2);
        when(issueManager.getIssueObject(newIssueKey)).thenReturn(newIssue);
        when(attachmentStore.copy(eq(attachment), any(Attachment.class), eq(newIssueKey))).thenReturn(Promises.promise(expected));

        assertEquals(expected, attachmentManager.copyAttachment(attachment, user, newIssueKey).right().get());
    }

    @Test
    public void testCopyAttachmentFailure() throws Exception
    {
        Attachment attachment = mockAttachment(123, "image/jpeg", "dog.jpg", 567, true);
        ApplicationUser user = new MockApplicationUser("John Doe");
        String newIssueKey = "JDEV-30222";

        when(ofBizDelegator.createValue(eq(AttachmentConstants.ATTACHMENT_ENTITY_NAME), any(Map.class))).thenReturn(attachment2);
        when(attachmentStore.copy(eq(attachment), any(Attachment.class), eq(newIssueKey)))
                .thenReturn(Promises.<Attachment>rejected(new AttachmentCopyException("Expected Failure")));

        assertTrue(attachmentManager.copyAttachment(attachment, user, newIssueKey).isLeft());
    }
}
