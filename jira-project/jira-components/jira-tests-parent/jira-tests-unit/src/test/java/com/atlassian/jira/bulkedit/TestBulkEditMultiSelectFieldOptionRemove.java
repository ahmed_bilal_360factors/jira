package com.atlassian.jira.bulkedit;

import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.fields.ComponentsSystemField;
import com.atlassian.jira.issue.fields.LongIdsValueHolder;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.util.VersionHelperBean;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 *
 * @since v6.4
 */
@RunWith (MockitoJUnitRunner.class)
public class TestBulkEditMultiSelectFieldOptionRemove extends TestCase
{
    @Mock
    private OrderableField field;

    @Mock
    private Issue issue;

    @Mock
    private Map<String,Object> fieldValuesHolder;

    BulkEditMultiSelectFieldOptionRemove bulkEditMultiSelectFieldOptionRemove = new BulkEditMultiSelectFieldOptionRemove();

    private static final String LABEL1 = "Label1";
    private static final String LABEL2 = "Label2";
    private static final String LABEL3 = "Label3";
    private static final String LABEL4 = "Label4";
    private static final String VALUE1 = "Value1";
    private static final String VALUE2 = "Value2";
    private static final String VALUE3 = "Value3";
    private static final String COMPONENTS_PREFIX = LongIdsValueHolder.NEW_VALUE_PREFIX;
    private static final String VERSIONS_PREFIX = VersionHelperBean.NEW_VERSION_RREFIX;

    @Test
    public void testGetFieldValuesMapForComponents() throws Exception
    {
        List<Long> valuesFromIssue = Lists.newArrayList();
        valuesFromIssue.add(new Long(10001));
        valuesFromIssue.add(new Long(10002));
        valuesFromIssue.add(new Long(10003));

        List<Long> valuesFromBulkEdit = Lists.newArrayList();
        valuesFromBulkEdit.add(new Long(10003));
        valuesFromBulkEdit.add(new Long(10004));

        LongIdsValueHolder componentsFromIssue = new LongIdsValueHolder(Lists.newArrayList(valuesFromIssue));
        LongIdsValueHolder componentsFromBulkEdit = new LongIdsValueHolder(Lists.newArrayList(valuesFromBulkEdit));

        final Map<String, Object> issueFieldValuesHolder = Maps.newHashMap();
        issueFieldValuesHolder.put(IssueFieldConstants.COMPONENTS,componentsFromIssue);
        Mockito.doAnswer(new Answer()
        {
            @Override
            public Void answer(final InvocationOnMock invocationOnMock) throws Throwable
            {
                Map<String, Object> valuesHolder = (Map<String, Object>) invocationOnMock.getArguments()[0];
                valuesHolder.putAll(issueFieldValuesHolder);
                return null;
            }
        }).when(field).populateFromIssue(Mockito.anyMapOf(String.class,Object.class), Mockito.eq(issue));

        when(field.getId()).thenReturn(IssueFieldConstants.COMPONENTS);
        when(fieldValuesHolder.get(field.getId())).thenReturn(componentsFromBulkEdit);

        Map<String,Object> valuesMap = bulkEditMultiSelectFieldOptionRemove.getFieldValuesMap(issue,field,fieldValuesHolder);

        assertThat(valuesMap.get(IssueFieldConstants.COMPONENTS), instanceOf(List.class));
        assertThat((Collection<Object>) valuesMap.get(IssueFieldConstants.COMPONENTS), hasSize(2));
        assertThat((Collection<Object>) valuesMap.get(IssueFieldConstants.COMPONENTS), hasItem(new Long(10001)));
        assertThat((Collection<Object>) valuesMap.get(IssueFieldConstants.COMPONENTS), hasItem(new Long(10002)));
    }

    @Test
    public void testGetFieldValuesMapForComponentsNoMatchingValues() throws Exception
    {
        List<Long> valuesFromIssue = Lists.newArrayList();
        valuesFromIssue.add(new Long(10001));
        valuesFromIssue.add(new Long(10002));
        valuesFromIssue.add(new Long(10003));
        List<Long> valuesFromBulkEdit = Lists.newArrayList();
        valuesFromBulkEdit.add(new Long(10004));
        valuesFromBulkEdit.add(new Long(10005));

        Set<String> valuesToAdd = Sets.newHashSet();
        valuesToAdd.add("Component1");
        valuesToAdd.add("Component2");

        LongIdsValueHolder componentsFromIssue = new LongIdsValueHolder(Lists.newArrayList(valuesFromIssue));
        LongIdsValueHolder componentsFromBulkEdit = new LongIdsValueHolder(Lists.newArrayList(valuesFromBulkEdit));
        componentsFromBulkEdit.setValuesToAdd(valuesToAdd);
        final Map<String, Object> issueFieldValuesHolder = Maps.newHashMap();
        issueFieldValuesHolder.put(IssueFieldConstants.COMPONENTS,componentsFromIssue);

        Mockito.doAnswer(new Answer()
        {
            @Override
            public Void answer(final InvocationOnMock invocationOnMock) throws Throwable
            {
                Map<String, Object> valuesHolder = (Map<String, Object>) invocationOnMock.getArguments()[0];
                valuesHolder.putAll(issueFieldValuesHolder);
                return null;
            }
        }).when(field).populateFromIssue(Mockito.anyMapOf(String.class,Object.class), Mockito.eq(issue));
        when(field.getId()).thenReturn(IssueFieldConstants.COMPONENTS);
        when(fieldValuesHolder.get(field.getId())).thenReturn(componentsFromBulkEdit);

        Map<String,Object> valuesMap = bulkEditMultiSelectFieldOptionRemove.getFieldValuesMap(issue,field,fieldValuesHolder);

        assertThat(valuesMap.get(IssueFieldConstants.COMPONENTS), instanceOf(List.class));
        assertThat((Collection<Object>) valuesMap.get(IssueFieldConstants.COMPONENTS), hasSize(3));
        assertThat((Collection<Object>) valuesMap.get(IssueFieldConstants.COMPONENTS), hasItem(new Long(10001)));
        assertThat((Collection<Object>) valuesMap.get(IssueFieldConstants.COMPONENTS), hasItem(new Long(10002)));
        assertThat((Collection<Object>) valuesMap.get(IssueFieldConstants.COMPONENTS), hasItem(new Long(10003)));
    }

    @Test
    public void testGetFieldValuesMapForVersions() throws Exception
    {
        List<Long> valuesFromIssue = Lists.newArrayList();
        valuesFromIssue.add(new Long(10001));
        valuesFromIssue.add(new Long(10002));
        valuesFromIssue.add(new Long(10003));
        List<Long> valuesFromBulkEdit = Lists.newArrayList();
        valuesFromBulkEdit.add(new Long(10002));
        valuesFromBulkEdit.add(new Long(10003));

        Set<String> valuesToAdd = Sets.newHashSet();
        valuesToAdd.add("Version1");

        LongIdsValueHolder versionsFromIssue = new LongIdsValueHolder(Lists.newArrayList(valuesFromIssue));
        LongIdsValueHolder versionsFromBulkEdit = new LongIdsValueHolder(Lists.newArrayList(valuesFromBulkEdit));
        versionsFromBulkEdit.setValuesToAdd(valuesToAdd);
        final Map<String, Object> issueFieldValuesHolder = Maps.newHashMap();
        issueFieldValuesHolder.put(IssueFieldConstants.FIX_FOR_VERSIONS,versionsFromIssue);

        Mockito.doAnswer(new Answer()
        {
            @Override
            public Void answer(final InvocationOnMock invocationOnMock) throws Throwable
            {
                Map<String, Object> valuesHolder = (Map<String, Object>) invocationOnMock.getArguments()[0];
                valuesHolder.putAll(issueFieldValuesHolder);
                return null;
            }
        }).when(field).populateFromIssue(Mockito.anyMapOf(String.class,Object.class), Mockito.eq(issue));
        when(field.getId()).thenReturn(IssueFieldConstants.FIX_FOR_VERSIONS);
        when(fieldValuesHolder.get(field.getId())).thenReturn(versionsFromBulkEdit);

        Map<String,Object> valuesMap = bulkEditMultiSelectFieldOptionRemove.getFieldValuesMap(issue, field, fieldValuesHolder);

        assertThat(valuesMap.get(IssueFieldConstants.FIX_FOR_VERSIONS), instanceOf(List.class));
        assertThat((Collection<Object>) valuesMap.get(IssueFieldConstants.FIX_FOR_VERSIONS), hasSize(1));
        assertThat((Collection<Object>) valuesMap.get(IssueFieldConstants.FIX_FOR_VERSIONS), hasItem(new Long(10001)));
    }

    @Test
    public void testGetFieldValuesMapForLabels() throws Exception
    {
        Set<String> valuesFromIssue = Sets.newHashSet();
        valuesFromIssue.add(LABEL1);
        valuesFromIssue.add(LABEL2);
        valuesFromIssue.add(LABEL3);
        valuesFromIssue.add(LABEL4);

        Set<String> valuesFromBulkEdit = Sets.newHashSet();
        valuesFromBulkEdit.add(LABEL2);
        valuesFromBulkEdit.add(LABEL3);

        final Map<String, Object> issueFieldValuesHolder = Maps.newHashMap();
        issueFieldValuesHolder.put(IssueFieldConstants.LABELS,valuesFromIssue);

        Mockito.doAnswer(new Answer()
        {
            @Override
            public Void answer(final InvocationOnMock invocationOnMock) throws Throwable
            {
                Map<String, Object> valuesHolder = (Map<String, Object>) invocationOnMock.getArguments()[0];
                valuesHolder.putAll(issueFieldValuesHolder);
                return null;
            }
        }).when(field).populateFromIssue(Mockito.anyMapOf(String.class,Object.class), Mockito.eq(issue));
        when(field.getId()).thenReturn(IssueFieldConstants.LABELS);
        when(fieldValuesHolder.get(field.getId())).thenReturn(valuesFromBulkEdit);

        Map<String,Object> valuesMap = bulkEditMultiSelectFieldOptionRemove.getFieldValuesMap(issue,field,fieldValuesHolder);

        assertThat(valuesMap.get(IssueFieldConstants.LABELS), instanceOf(Set.class));
        assertThat((Collection<Object>) valuesMap.get(IssueFieldConstants.LABELS), hasSize(2));
        assertThat((Collection<Object>) valuesMap.get(IssueFieldConstants.LABELS), hasItem(LABEL1));
        assertThat((Collection<Object>) valuesMap.get(IssueFieldConstants.LABELS), hasItem(LABEL4));
    }

    @Test
    public void testGetFieldValuesMapForLabelsNoMatchingValues() throws Exception
    {
        Set<String> valuesFromIssue = Sets.newHashSet();
        valuesFromIssue.add(LABEL1);
        valuesFromIssue.add(LABEL2);
        valuesFromIssue.add(LABEL3);

        Set<String> valuesFromBulkEdit = Sets.newHashSet();
        valuesFromBulkEdit.add(LABEL4);

        final Map<String, Object> issueFieldValuesHolder = Maps.newHashMap();
        issueFieldValuesHolder.put(IssueFieldConstants.LABELS,valuesFromIssue);

        Mockito.doAnswer(new Answer()
        {
            @Override
            public Void answer(final InvocationOnMock invocationOnMock) throws Throwable
            {
                Map<String, Object> valuesHolder = (Map<String, Object>) invocationOnMock.getArguments()[0];
                valuesHolder.putAll(issueFieldValuesHolder);
                return null;
            }
        }).when(field).populateFromIssue(Mockito.anyMapOf(String.class,Object.class), Mockito.eq(issue));
        when(field.getId()).thenReturn(IssueFieldConstants.LABELS);
        when(fieldValuesHolder.get(field.getId())).thenReturn(valuesFromBulkEdit);

        Map<String,Object> valuesMap = bulkEditMultiSelectFieldOptionRemove.getFieldValuesMap(issue,field,fieldValuesHolder);

        assertThat(valuesMap.get(IssueFieldConstants.LABELS), instanceOf(Set.class));
        assertThat((Collection<Object>) valuesMap.get(IssueFieldConstants.LABELS), hasSize(3));
        assertThat((Collection<Object>) valuesMap.get(IssueFieldConstants.LABELS), hasItem(LABEL1));
        assertThat((Collection<Object>) valuesMap.get(IssueFieldConstants.LABELS), hasItem(LABEL2));
        assertThat((Collection<Object>) valuesMap.get(IssueFieldConstants.LABELS), hasItem(LABEL3));
    }

    @Test
    public void testValidateOperationForComponentsOnlyNewValueToAdd() throws Exception
    {
        List<Long> valuesFromBulkEdit = Lists.newArrayList();
        LongIdsValueHolder componentsFromBulkEdit = new LongIdsValueHolder(Lists.newArrayList(valuesFromBulkEdit));
        Set<String> valuesToAdd = Sets.newHashSet();
        valuesToAdd.add(COMPONENTS_PREFIX + VALUE1);
        componentsFromBulkEdit.setValuesToAdd(valuesToAdd);

        when(field.getId()).thenReturn(IssueFieldConstants.COMPONENTS);
        when(fieldValuesHolder.get(field.getId())).thenReturn(componentsFromBulkEdit);

        Boolean result = bulkEditMultiSelectFieldOptionRemove.validateOperation(field, fieldValuesHolder);

        assertThat(result,is(false));
    }

    @Test
    public void testValidateOperationForVersionsOnlyExistingValueToAdd() throws Exception
    {
        List<Long> valuesFromBulkEdit = Lists.newArrayList();
        valuesFromBulkEdit.add(new Long(10001));
        LongIdsValueHolder versionsFromBulkEdit = new LongIdsValueHolder(Lists.newArrayList(valuesFromBulkEdit));

        when(field.getId()).thenReturn(IssueFieldConstants.AFFECTED_VERSIONS);
        when(fieldValuesHolder.get(field.getId())).thenReturn(versionsFromBulkEdit);

        Boolean result = bulkEditMultiSelectFieldOptionRemove.validateOperation(field,fieldValuesHolder);

        assertThat(result,is(true));
    }

    @Test
    public void testValidateOperationForVersionsNoValueToAdd() throws Exception
    {
        List<Long> valuesFromBulkEdit = Lists.newArrayList();
        LongIdsValueHolder versionsFromBulkEdit = new LongIdsValueHolder(Lists.newArrayList(valuesFromBulkEdit));

        when(field.getId()).thenReturn(IssueFieldConstants.FIX_FOR_VERSIONS);
        when(fieldValuesHolder.get(field.getId())).thenReturn(versionsFromBulkEdit);

        Boolean result = bulkEditMultiSelectFieldOptionRemove.validateOperation(field,fieldValuesHolder);

        assertThat(result,is(false));
    }

    @Test
    public void testValidateOperationLabelsNewValueToAdd() throws Exception
    {
        Set<String> valuesHolder = Sets.newHashSet();
        valuesHolder.add("Label1");
        when(field.getId()).thenReturn(IssueFieldConstants.LABELS);
        when(fieldValuesHolder.get(field.getId())).thenReturn(valuesHolder);

        Boolean result = bulkEditMultiSelectFieldOptionRemove.validateOperation(field,fieldValuesHolder);

        assertThat(result,is(true));
    }

    @Test
    public void testValidateOperationLabelsNoValueToAdd() throws Exception
    {
        Set<String> valuesHolder = Sets.newHashSet();
        when(field.getId()).thenReturn(IssueFieldConstants.LABELS);
        when(fieldValuesHolder.get(field.getId())).thenReturn(valuesHolder);

        Boolean result = bulkEditMultiSelectFieldOptionRemove.validateOperation(field,fieldValuesHolder);
        assertThat(result,is(false));
    }

    @Test
    public void testGetFieldValuesToAddForComponents() throws Exception
    {
        List<Long> valuesFromBulkEdit = Lists.newArrayList();
        valuesFromBulkEdit.add(new Long(10003));
        valuesFromBulkEdit.add(new Long(10004));
        LongIdsValueHolder componentsFromBulkEdit = new LongIdsValueHolder(Lists.newArrayList(valuesFromBulkEdit));
        Set<String> valuesToAdd = Sets.newHashSet();
        valuesToAdd.add(COMPONENTS_PREFIX + VALUE1);
        valuesToAdd.add(COMPONENTS_PREFIX + VALUE2);
        valuesToAdd.add(COMPONENTS_PREFIX + VALUE3);
        componentsFromBulkEdit.setValuesToAdd(valuesToAdd);

        when(field.getId()).thenReturn(IssueFieldConstants.COMPONENTS);
        when(fieldValuesHolder.get(field.getId())).thenReturn(componentsFromBulkEdit);

        String values = bulkEditMultiSelectFieldOptionRemove.getFieldValuesToAdd(field,fieldValuesHolder);

        assertThat(values.equals(StringUtils.EMPTY),is(true));
    }

    @Test
    public void testGetFieldValuesToAddForVersions() throws Exception
    {
        List<Long> valuesFromBulkEdit = Lists.newArrayList();
        valuesFromBulkEdit.add(new Long(10003));
        valuesFromBulkEdit.add(new Long(10004));
        LongIdsValueHolder versionsFromBulkEdit = new LongIdsValueHolder(Lists.newArrayList(valuesFromBulkEdit));
        Set<String> valuesToAdd = Sets.newHashSet();
        valuesToAdd.add(VERSIONS_PREFIX + VALUE1);
        valuesToAdd.add(VERSIONS_PREFIX + VALUE2);
        valuesToAdd.add(VERSIONS_PREFIX + VALUE3);
        versionsFromBulkEdit.setValuesToAdd(valuesToAdd);

        when(field.getId()).thenReturn(IssueFieldConstants.FIX_FOR_VERSIONS);
        when(fieldValuesHolder.get(field.getId())).thenReturn(versionsFromBulkEdit);

        String values = bulkEditMultiSelectFieldOptionRemove.getFieldValuesToAdd(field,fieldValuesHolder);

        assertThat(values.equals(StringUtils.EMPTY),is(true));
    }

    @Test
    public void testGetFieldValuesToAddForLabels() throws Exception
    {
        Set<String> valuesFromBulkEdit = Sets.newHashSet();
        valuesFromBulkEdit.add("Label1");
        valuesFromBulkEdit.add("Label2");
        valuesFromBulkEdit.add("Label3");

        when(field.getId()).thenReturn(IssueFieldConstants.LABELS);
        when(fieldValuesHolder.get(field.getId())).thenReturn(valuesFromBulkEdit);

        String values = bulkEditMultiSelectFieldOptionRemove.getFieldValuesToAdd(field,fieldValuesHolder);

        assertThat(values.equals(StringUtils.EMPTY),is(true));
    }
}
