package com.atlassian.jira.upgrade;

import java.io.Serializable;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.beehive.simple.SimpleClusterLockService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.SelectQuery;
import com.atlassian.jira.index.request.ReindexRequestManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.index.IndexLifecycleManager;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.status.RunOutcome;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericEntity;
import org.ofbiz.core.entity.GenericValue;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDelayedUpgradeJobRunner
{
    @Rule
    public MockComponentContainer mockComponentContainer = new MockComponentContainer(this);

    @Mock @AvailableInContainer
    IndexLifecycleManager indexLifecycleManager;
    @Mock @AvailableInContainer
    EntityEngine entityEngine;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private ReindexRequestManager reindexRequestManager;
    @Mock
    private BuildUtilsInfo buildUtilsInfo;

    private int id = 0;
    private SimpleClusterLockService clusterLockService;

    @Before
    public void setUp() throws Exception
    {
        clusterLockService = new SimpleClusterLockService();
        mockComponentContainer.addMock(ClusterLockService.class, clusterLockService);
        when(buildUtilsInfo.getVersion()).thenReturn("1000");
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testTaskRunInOrder() throws Exception
    {
        MockUpgradeTask.setBuildNumbers("100", "200", "300", "400");

        when(applicationProperties.getString(APKeys.JIRA_PATCHED_VERSION)).thenReturn("90");

        GenericEntity task1 = createTask("100");
        GenericEntity task2 = createTask("200");
        GenericEntity task3 = createTask("300");
        GenericEntity task4 = createTask("400");

        SelectQuery.ExecutionContext context = Mockito.mock(SelectQuery.ExecutionContext.class);
        when(entityEngine.run(any(SelectQuery.class))).thenReturn(context);
        when(context.asList()).thenReturn(ImmutableList.of(task1, task3, task2, task4));
        DelayedUpgradeJobRunner runner = new DelayedUpgradeJobRunner(entityEngine, indexLifecycleManager,
                clusterLockService, applicationProperties, reindexRequestManager, buildUtilsInfo);
        JobRunnerResponse response = runner.runJob(getMockJobRunnerRequest());

        InOrder inOrder = inOrder(task1, task2, task3, task4);
        inOrder.verify(task1).set("status", "complete");
        inOrder.verify(task2).set("status", "complete");
        inOrder.verify(task3).set("status", "complete");
        inOrder.verify(task4).set("status", "complete");

        assertThat(response, notNullValue(JobRunnerResponse.class));
        if (response != null)
        {
            assertThat(response.getRunOutcome(), is(RunOutcome.SUCCESS));
        }
    }

    @Test
    public void testTaskFailureAbortsRun() throws Exception
    {
        MockUpgradeTask.setBuildNumbers("100", "200", "300", "400");

        when(applicationProperties.getString(APKeys.JIRA_PATCHED_VERSION)).thenReturn("90");

        GenericEntity task1 = createTask("100");
        GenericEntity task2 = createFailingTask("200");
        GenericEntity task3 = createTask("300");
        GenericEntity task4 = createTask("400");

        SelectQuery.ExecutionContext context = Mockito.mock(SelectQuery.ExecutionContext.class);
        when(entityEngine.run(any(SelectQuery.class))).thenReturn(context);
        when(context.asList()).thenReturn(ImmutableList.of(task1, task3, task2, task4));

        DelayedUpgradeJobRunner runner = new DelayedUpgradeJobRunner(entityEngine, indexLifecycleManager,
                clusterLockService, applicationProperties, reindexRequestManager, buildUtilsInfo);

        JobRunnerResponse response = runner.runJob(getMockJobRunnerRequest());

        verify(task1).set("status", "complete");
        verify(task3, never()).set("status", "complete");
        verify(task4, never()).set("status", "complete");

        verify(applicationProperties).setString(APKeys.JIRA_PATCHED_VERSION, "100");
        verify(applicationProperties, never()).setString(APKeys.JIRA_PATCHED_VERSION, "200");

        assertThat(response, notNullValue(JobRunnerResponse.class));
        if (response != null)
        {
            assertThat(response.getRunOutcome(), is(RunOutcome.FAILED));
            assertThat(response.getMessage(), is("[Upgrade task 200 failed. Error : Crashing for testing purposes]"));
        }
    }

    @Test
    public void testBuildNumberIncremented() throws Exception
    {
        MockUpgradeTask.setBuildNumbers("100", "200", "300", "400");

        when(applicationProperties.getString(APKeys.JIRA_PATCHED_VERSION)).thenReturn("150");

        GenericEntity task1 = createCompletedTask("100");
        GenericEntity task2 = createCompletedTask("200");
        GenericEntity task3 = createCompletedTask("300");
        GenericEntity task4 = createTask("400");

        SelectQuery.ExecutionContext context = Mockito.mock(SelectQuery.ExecutionContext.class);
        when(entityEngine.run(any(SelectQuery.class))).thenReturn(context);
        when(context.asList()).thenReturn(ImmutableList.of(task1, task3, task2, task4));

        DelayedUpgradeJobRunner runner = new DelayedUpgradeJobRunner(entityEngine, indexLifecycleManager,
                clusterLockService, applicationProperties, reindexRequestManager, buildUtilsInfo);
        JobRunnerResponse response = runner.runJob(getMockJobRunnerRequest());

        verify(applicationProperties, never()).setString(APKeys.JIRA_PATCHED_VERSION, "100");
        verify(applicationProperties).setString(APKeys.JIRA_PATCHED_VERSION, "200");
        verify(applicationProperties).setString(APKeys.JIRA_PATCHED_VERSION, "300");
        verify(applicationProperties).setString(APKeys.JIRA_PATCHED_VERSION, "400");

        assertThat(response, notNullValue(JobRunnerResponse.class));
        if (response != null)
        {
            assertThat(response.getRunOutcome(), is(RunOutcome.SUCCESS));
        }
    }

    public JobRunnerRequest getMockJobRunnerRequest()
    {
        JobRunnerRequest jobRunnerRequest = Mockito.mock(JobRunnerRequest.class);
        final Schedule schedule = Schedule.runOnce(DateTime.now().plusMinutes(10).toDate());
        JobConfig config = JobConfig.forJobRunnerKey(JobRunnerKey.of(DelayedUpgradeJobRunner.class.getName()))
                .withSchedule(schedule)
                .withParameters(ImmutableMap.<String, Serializable>of("upgrade.reindex.allowed", true));
        when(jobRunnerRequest.getJobConfig()).thenReturn(config);
        return jobRunnerRequest;
    }

    private GenericValue createTask(final String buildNo)
    {
        return getGenericValue(buildNo, MockUpgradeTask.class.getName(), "pending");
    }

    private GenericValue createCompletedTask(final String buildNo)
    {
        return getGenericValue(buildNo, MockUpgradeTask.class.getName(), "complete");
    }

    private GenericValue createFailingTask(final String buildNo)
    {
        return getGenericValue(buildNo, MockFailingUpgradeTask.class.getName(), "pending");
    }

    private GenericValue getGenericValue(final String buildNo, final String className, final String status)
    {
        GenericValue gv = Mockito.mock(GenericValue.class);
        when(gv.get("id")).thenReturn(id++);
        when(gv.getString("upgradeclass")).thenReturn(className);
        when(gv.getString("targetbuild")).thenReturn(buildNo);
        when(gv.getString("status")).thenReturn(status);
        return gv;
    }
}