package com.atlassian.jira.index;

import javax.annotation.Nonnull;

public class MockIndex implements CloseableIndex
{
    public void close()
    {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    public Result perform(@Nonnull final Operation operation)
    {
        throw new UnsupportedOperationException();
    }
}
