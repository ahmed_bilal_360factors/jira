package com.atlassian.jira.security.roles.actor;

import java.util.List;
import java.util.Set;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.database.MockDbConnectionManager;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleActor;
import com.atlassian.jira.user.ApplicationUser;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestGroupRoleActorFactory
{
    @Rule
    public InitMockitoMocks mockitoMocks = new InitMockitoMocks(this);

    private final MockDbConnectionManager dbConnectionManager = new MockDbConnectionManager();

    @Mock
    private GroupManager groupManager;

    @Mock
    private ApplicationUser user;

    private GroupRoleActorFactory groupRoleActorFactory;

    @Before
    public void setUp() throws NoSuchFieldException, IllegalAccessException
    {
        ComponentAccessor.initialiseWorker(new MockComponentWorker());
        groupRoleActorFactory = new GroupRoleActorFactory(groupManager, dbConnectionManager);
        when(user.getName()).thenReturn("user");
    }

    @Test
    public void testGetAllRoleActorsForUser()
    {
        final List<String> groupNames = ImmutableList.of
                ("group01", "group02", "group03", "group04", "group05", "group06", "group07");

        final ResultRow expectedActor1 = new ResultRow(1L, 100L, 200L, "atlassian-group-role-actor", "group01");
        final ResultRow expectedActor2 = new ResultRow(2L, 101L, 201L, "atlassian-group-role-actor", "group01");
        final ResultRow expectedActor3 = new ResultRow(3L, 102L, 202L, "atlassian-group-role-actor", "group07");
        final ResultRow expectedActor4 = new ResultRow(5L, 103L, 203L, "atlassian-group-role-actor", "group107");
        final ResultRow expectedActor5 = new ResultRow(4L, 104L, 204L, "atlassian-group-role-actor", "group207");
        final List<ResultRow> queryResults = ImmutableList.of(expectedActor1, expectedActor2, expectedActor3, expectedActor4, expectedActor5);

        when(groupManager.getGroupNamesForUser(user)).thenReturn(groupNames);

        dbConnectionManager.setQueryResults
                ("select pra.id, pra.pid, pra.projectroleid, pra.roletype, pra.roletypeparameter\n"
                                + "from projectroleactor pra\n"
                                + "where pra.roletype = 'atlassian-group-role-actor' and pra.pid is not null",
                        queryResults);

        final Set<ProjectRoleActor> actors = groupRoleActorFactory.getAllRoleActorsForUser(user);
        final Set<ResultRow> actorsAsRows = ImmutableSet.copyOf(Iterables.transform(actors, toResultRow));

        assertThat(actorsAsRows, containsInAnyOrder(expectedActor1, expectedActor2, expectedActor3));
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testGetAllRoleActorsForUserCaseInsensitive()
    {
        final List<String> groupNames = ImmutableList.of
                ("group01", "GROUP02", "group03", "group04", "group05", "group06", "group07");

        final ResultRow expectedActor1 = new ResultRow(1L, 100L, 200L, "atlassian-group-role-actor", "group01");
        final ResultRow expectedActor2 = new ResultRow(2L, 101L, 201L, "atlassian-group-role-actor", "group02");
        final ResultRow expectedActor3 = new ResultRow(3L, 102L, 202L, "atlassian-group-role-actor", "GROUP07");
        final ResultRow expectedActor3LowerCaseGroup = new ResultRow(3L, 102L, 202L, "atlassian-group-role-actor", "group07");
        final ResultRow expectedActor4 = new ResultRow(4L, 103L, 203L, "atlassian-group-role-actor", "group107");
        final ResultRow expectedActor5 = new ResultRow(5L, 104L, 204L, "atlassian-group-role-actor", "group207");
        final List<ResultRow> queryResults = ImmutableList.of(expectedActor1, expectedActor2, expectedActor3, expectedActor4, expectedActor5);

        when(groupManager.getGroupNamesForUser(user)).thenReturn(groupNames);

        dbConnectionManager.setQueryResults
                ("select pra.id, pra.pid, pra.projectroleid, pra.roletype, pra.roletypeparameter\n"
                                + "from projectroleactor pra\n"
                                + "where pra.roletype = 'atlassian-group-role-actor' and pra.pid is not null",
                        queryResults);

        final Set<ProjectRoleActor> actors = groupRoleActorFactory.getAllRoleActorsForUser(user);
        final Set<ResultRow> actorsAsRows = ImmutableSet.copyOf(Iterables.transform(actors, toResultRow));

        assertThat(actorsAsRows, containsInAnyOrder(expectedActor1, expectedActor2, expectedActor3LowerCaseGroup));
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testGetAllRoleActorsForUserNoActors()
    {
        final List<String> groupNames = ImmutableList.of("group01", "group02", "group03");

        when(groupManager.getGroupNamesForUser(user)).thenReturn(groupNames);

        final ResultRow expectedActor4 = new ResultRow(1L, 101L, 202L, "atlassian-group-role-actor", "group107", user);
        final ResultRow expectedActor5 = new ResultRow(1L, 101L, 202L, "atlassian-group-role-actor", "group207", user);

        final List<ResultRow> queryResults = ImmutableList.of(expectedActor4, expectedActor5);

        dbConnectionManager.setQueryResults
                ("select pra.id, pra.pid, pra.projectroleid, pra.roletype, pra.roletypeparameter\n"
                                + "from projectroleactor pra\n"
                                + "where pra.roletype = 'atlassian-group-role-actor' and pra.pid is not null",
                        queryResults);

        final Set<ProjectRoleActor> actors = groupRoleActorFactory.getAllRoleActorsForUser(user);

        assertThat(actors, Matchers.<ProjectRoleActor>empty());
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testGetAllRoleActorsForNullUser()
    {
        when(groupManager.getGroupNamesForUser((ApplicationUser) null)).thenThrow(new NullPointerException());

        final Set<ProjectRoleActor> actors = groupRoleActorFactory.getAllRoleActorsForUser(null);

        assertThat(actors, Matchers.<ProjectRoleActor>empty());
    }

    private final Function<ProjectRoleActor, ResultRow> toResultRow = new Function<ProjectRoleActor, ResultRow>()
    {
        @Override
        public ResultRow apply(final ProjectRoleActor actor)
        {
            return new ResultRow(actor.getId(), actor.getProjectId(), actor.getProjectRoleId(), actor.getType(), actor.getParameter());
        }
    };
}
