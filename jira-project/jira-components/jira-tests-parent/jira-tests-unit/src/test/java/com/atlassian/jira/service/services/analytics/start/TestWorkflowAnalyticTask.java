package com.atlassian.jira.service.services.analytics.start;

import java.util.Collection;
import java.util.Map;

import com.atlassian.jira.workflow.WorkflowManager;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestWorkflowAnalyticTask
{

    @Test
    public void getAnalytic()
    {
        WorkflowManager workflowManager = mock(WorkflowManager.class);
        WorkflowAnalyticTask task = new WorkflowAnalyticTask(workflowManager);
        Collection workflows = mock(Collection.class);

        when(workflows.size()).thenReturn(10);
        doReturn(workflows).when(workflowManager).getActiveWorkflows();

        Map<String, Object> analytics = task.getAnalytics();

        assertEquals(10, analytics.get("workflows"));
    }


}
