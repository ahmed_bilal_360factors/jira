package com.atlassian.jira.web.filters.pagebuilder;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.mock.web.pagebuilder.MockDefaultJiraPageBuilderService;
import com.atlassian.jira.web.pagebuilder.DecoratablePage;
import com.atlassian.jira.web.pagebuilder.DecoratorListener;
import com.atlassian.jira.web.pagebuilder.PageBuilderServiceSpi;
import com.atlassian.jira.web.pagebuilder.PageBuilderSpi;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class PageBuilderFilterTest
{
    PageBuilderFilter pageBuilderFilter;

    @Mock private FilterChain mockFilterChain;
    @Mock private FilterConfig mockFilterConfig;
    @Mock private ServletContext mockServletContext;

    private MockDefaultJiraPageBuilderService jiraPageBuilderService;
    private PageBuilderFilterTest.MockHttpServletRequest mockServletRequest;
    private PageBuilderFilterTest.MockHttpServletResponse mockServletResponse;

    @Rule
    public final MockComponentContainer mockContainer = new MockComponentContainer(this);


    @Before
    public void setUp() throws Exception
    {
        initMocks(this);

        jiraPageBuilderService = spy(new MockDefaultJiraPageBuilderService());
        mockContainer.addMockComponent(PageBuilderServiceSpi.class, jiraPageBuilderService);

        mockServletRequest = new PageBuilderFilterTest.MockHttpServletRequest();
        mockServletResponse = new PageBuilderFilterTest.MockHttpServletResponse();

        doReturn(mockServletContext).when(mockFilterConfig).getServletContext();

        pageBuilderFilter = new PageBuilderFilter();
        pageBuilderFilter.init(mockFilterConfig);
    }

    @After
    public void tearDown() throws Exception
    {
        pageBuilderFilter.destroy();
    }

    @Test
    public void shouldInitialiseThePageBuilderService()
            throws Exception
    {
        pageBuilderFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);

        verify(jiraPageBuilderService, times(1))
                .initForRequest(eq(mockServletRequest), eq(mockServletResponse), any(DecoratorListener.class), eq(mockServletContext));
    }

    @Test
    public void shouldWrapTheResponse()
            throws Exception
    {
        doAnswer(new Answer()
        {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                Object[] args = invocation.getArguments();
                assertEquals("Expected exactly 2 arguments to mockFilterChain.doFilter()", 2, args.length);
                assertTrue("Expected 2nd argument to mockFilterChain.doFilter() to be a PageBuilderResponseWrapper", args[1] instanceof PageBuilderResponseWrapper);
                return null;
            }
        })
        .when(mockFilterChain).doFilter(any(HttpServletRequest.class), any(HttpServletResponse.class));

        pageBuilderFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);

        verify(mockFilterChain, times(1))
                .doFilter(any(HttpServletRequest.class), any(HttpServletResponse.class));
    }

    @Test
    public void shouldRunSecondTimeIfPreviousFilterNoLongerOnStack()
            throws Exception
    {
        // 1: put filter on stack, run, take filter off stack
        pageBuilderFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);

        // 2: it has now run once (tried to initialise jiraPageBuilderService once)
        verify(jiraPageBuilderService, times(1))
                .initForRequest(eq(mockServletRequest), eq(mockServletResponse), any(DecoratorListener.class), eq(mockServletContext));

        // 3: put filter on stack, run, take filter off stack
        pageBuilderFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);

        // 4: it has now run twice (tried to initialise jiraPageBuilderService twice)
        verify(jiraPageBuilderService, times(2))
                .initForRequest(eq(mockServletRequest), eq(mockServletResponse), any(DecoratorListener.class), eq(mockServletContext));
    }

    @Test
    public void shouldNotRunSecondTimeIfPreviousFilterStillOnStack()
            throws Exception
    {
        final FilterChain mockFilterChain2 = mock(FilterChain.class);
        final FilterConfig mockFilterConfig2 = mock(FilterConfig.class);
        final PageBuilderFilter pageBuilderFilter2 = new PageBuilderFilter();
        pageBuilderFilter2.init(mockFilterConfig2);

        doAnswer(new Answer()
        {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                Object[] args = invocation.getArguments();
                HttpServletRequest request = (HttpServletRequest) args[0];
                HttpServletResponse response = (HttpServletResponse) args[1];

                // 3: put second filter on stack while the first one is still on the stack, run...
                pageBuilderFilter2.doFilter(request, response, mockFilterChain2);

                return null;
            }
        })
        .when(mockFilterChain).doFilter(any(HttpServletRequest.class), any(HttpServletResponse.class));

        // 1: put filter on stack, run...
        pageBuilderFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);

        // 2: first filter chain gets called once, with a wrapped response...
        verify(mockFilterChain, times(1))
                .doFilter(eq(mockServletRequest), any(PageBuilderResponseWrapper.class));

        // 4: second filter chain gets called once (no point checking for a wrapped response - it'll have the one passed to it from the first filter anyway)
        verify(mockFilterChain2, times(1))
                .doFilter(any(HttpServletRequest.class), any(HttpServletResponse.class));

        // 5: jiraPageBuilderService was initialised /exactly once/
        //    (because the second pageBuilderFilter didn't run while the first one was still on the stack)
        verify(jiraPageBuilderService, times(1))
                .initForRequest(any(HttpServletRequest.class), any(HttpServletResponse.class), any(DecoratorListener.class), any(ServletContext.class));
    }

    @Test
    public void shouldAlwaysResetThePageBuilderService()
            throws Exception
    {
        doAnswer(new Answer()
        {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                // make our PageBuilderFilter's called to mockFilterChain.doFilter() blow up
                throw new Exception("IM IN UR FILTERCHAIN THROWIN UR EXCEPTIONZ");
            }
        })
        .when(mockFilterChain).doFilter(any(HttpServletRequest.class), any(HttpServletResponse.class));

        // run pageBuilderFilter
        try
        {
            pageBuilderFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);
            fail("Was expecting the pageBuilderFilter's called to mockFilterChain.doFilter() to blow up.");
        }
        catch(Exception e) {}  // let's just eat that, shall we...

        // make sure the jiraPageBuilderService still got reset properly
        verify(jiraPageBuilderService, times(1)).clearForRequest();
    }

    @Test
    public void shouldNotRenderDecoratedPageAfterException() throws Exception
    {
        final PageBuilderSpi pageBuilderSpi = jiraPageBuilderService.getSpi();

        doAnswer(new Answer()
        {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                throw new Exception("IM IN UR FILTERCHAIN THROWIN UR EXCEPTIONZ");
            }
        })
        .when(mockFilterChain).doFilter(any(HttpServletRequest.class), any(HttpServletResponse.class));

        try
        {
            pageBuilderFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);
            fail("Was expecting the pageBuilderFilter's called to mockFilterChain.doFilter() to blow up.");
        }
        catch(Exception e) {}  // let's just eat that, shall we...

        // make sure we did not try to render the decorated page
        verify(pageBuilderSpi, times(0)).finish(any(DecoratablePage.class));
    }

    class MockHttpServletRequest
        extends com.atlassian.jira.mock.servlet.MockHttpServletRequest
    {
        private Map attributeMap = new HashMap();

        @Override
        public Object getAttribute(String key)
        {
            return attributeMap.get(key);
        }

        @Override
        public void setAttribute(String key, Object value)
        {
            attributeMap.put(key, value);
        }

        @Override
        public void removeAttribute(final String key)
        {
            attributeMap.remove(key);
        }
    }

    class MockHttpServletResponse
        extends com.atlassian.jira.mock.servlet.MockHttpServletResponse
    {
        boolean committed = false;

        @Override
        public boolean isCommitted()
        {
            return committed;
        }

        public void simulateCommittedResponse() {
            committed = true;
        }
    }
}
