package com.atlassian.jira;

import com.atlassian.event.internal.EventExecutorFactoryImpl;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizApplicationDao;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizDirectoryDao;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizGroupDao;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizInternalMembershipDao;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizUserDao;
import com.atlassian.jira.web.util.ExternalLinkUtilImpl;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.google.common.collect.Sets;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.picocontainer.ComponentAdapter;
import org.picocontainer.Parameter;
import org.quartz.impl.StdScheduler;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.DefaultBeanNameGenerator;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.type.filter.AssignableTypeFilter;
import webwork.action.ActionSupport;

import java.lang.reflect.Constructor;
import java.util.HashSet;
import java.util.Set;

/**
 * A test that makes sure that classes that we inject do not have concrete classes in their constructors
 */
public class TestInjectionParameters
{
    private static final Set<Class> EXCLUSIONS = new HashSet<Class>();

    static
    {
        EXCLUSIONS.add(OfBizApplicationDao.class);
        EXCLUSIONS.add(EventExecutorFactoryImpl.class);
        EXCLUSIONS.add(ExternalLinkUtilImpl.class);
        EXCLUSIONS.add(DefaultPluginEventManager.class);
        // StdScheduler comes from Quartz Scheduler library.
        EXCLUSIONS.add(StdScheduler.class);
        // Ignore these temporarily until we update Crowd Embedded
        EXCLUSIONS.add(OfBizDirectoryDao.class);
        EXCLUSIONS.add(OfBizUserDao.class);
        EXCLUSIONS.add(OfBizGroupDao.class);
        EXCLUSIONS.add(OfBizInternalMembershipDao.class);
    }

    @Test
    public void testConstructorUsage() throws ClassNotFoundException
    {
        final ContainerRegistrar registrar = new ContainerRegistrar();
        final MyComponentContainer container = new MyComponentContainer();

        registrar.registerComponents(container, true);

        final Set<Class> classesToCheck = getWebworkClasses();

        //add all implementation classes from pico.  They could get dodgy stuff injected!
        classesToCheck.addAll(container.getImplementationsToCheck());

        final Set<Class> offenders = new HashSet<Class>();
        classesToCheck.removeAll(EXCLUSIONS);
        for (final Class classToCheck : classesToCheck)
        {
            final Constructor[] constructors = classToCheck.getConstructors();
            for (final Constructor constructor : constructors)
            {
                final Class[] parameterTypes = constructor.getParameterTypes();
                for (final Class parameterType : parameterTypes)
                {
                    if (isOffender(parameterType, container))
                    {
                        offenders.add(classToCheck);
                        break;
                    }
                }
            }
        }
        if (!offenders.isEmpty())
        {
            final StringBuilder out = new StringBuilder();
            out.append("Found ").append(offenders.size()).append(" classes that are injectable and take concrete classes.\n").
                    append("They may break if logging & profiling is enabled. Change these classes to have interfaces injected or add them to the exclusions:\n");
            for (final Class offender : offenders)
            {
                out.append("* ").append(offender.getCanonicalName()).append("\n");
            }
            Assert.fail(out.toString());
        }
    }

    private Set<Class> getWebworkClasses() throws ClassNotFoundException
    {
        //Use Spring ClasspathScanner to find all classes that implement the ActionSupport class in the webwork action package
        final BeanDefinitionRegistry registry = new SimpleBeanDefinitionRegistry();
        final ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(registry);

        //use a default bean name generator so that duplicate bean names are allowed
        scanner.setBeanNameGenerator(new DefaultBeanNameGenerator());
        scanner.addIncludeFilter(new AssignableTypeFilter(ActionSupport.class));

        scanner.scan("com.atlassian.jira.web");

        final Set<Class> classesToCheck = Sets.newHashSet();
        for (String beanName : registry.getBeanDefinitionNames())
        {
            if(!beanName.contains("Test"))
            {
                final BeanDefinition beanDefinition = registry.getBeanDefinition(beanName);
                classesToCheck.add(Thread.currentThread().getContextClassLoader().loadClass(beanDefinition.getBeanClassName()));
            }
        }
        return classesToCheck;
    }

    private boolean isOffender(final Class parameter, final MyComponentContainer container)
    {
        if (parameter.isPrimitive())
        {
            return false;
        }
        if (parameter.isInterface())
        {
            return false;
        }

        //I suppose concrete parameters are allowed if there's no interface for them.
        if (parameter.getInterfaces().length == 0)
        {
            return false;
        }

        if (parameter.equals(StepDescriptor.class) || parameter.equals(ActionDescriptor.class))
        {
            return false;
        }

        final Set<Class<?>> nonProfiledClasses = container.getNonProfiledClasses();
        return !nonProfiledClasses.contains(parameter);
    }

    static class MyComponentContainer extends ComponentContainer
    {
        // Classes that are not registered against and interface will *not* be profiled.  They should
        // be excluded when checking for illegal parameters
        private final Set<Class<?>> nonProfiledClasses = Sets.newHashSet();
        // The actual classes that are components need to be checked for illegal parameters.
        private final Set<Class<?>> implementationsToCheck = Sets.newHashSet();

        @Override
        void instance(final Scope scope, final Object instance)
        {
            nonProfiledClasses.add(instance.getClass());
        }

        @Override
        void instance(final Scope scope, final String key, final Object instance)
        {
            nonProfiledClasses.add(instance.getClass());
        }

        @Override
        <T, S extends T> void instance(final Scope scope, final Class<T> key, final S instance)
        {
            if (!key.isInterface())
            {
                nonProfiledClasses.add(key);
            }
            else
            {
                implementationsToCheck.add(instance.getClass());
            }
        }

        @Override
        <T> T getComponentInstance(final Class<T> key)
        {
            return Mockito.mock(key);
        }

        @Override
        void implementation(final Scope scope, final Class<?> implementation)
        {
            nonProfiledClasses.add(implementation);
        }

        @Override
        void component(final Scope scope, final ComponentAdapter componentAdapter)
        {

        }

        @Override
        void componentWithoutDefaultBehaviour(final Scope scope, final ComponentAdapter componentAdapter)
        {

        }

        @Override
        void transfer(final ComponentManager from, final Scope scope, final Class<?> key)
        {

        }

        @Override
        ComponentAdapter getComponentAdapter(final Class<?> key)
        {
            return (ComponentAdapter) Mockito.mock(key, Mockito.withSettings().extraInterfaces(ComponentAdapter.class));
        }

        @Override
        <T> void implementation(final Scope scope, final Class<? super T> key, final Class<T> implementation)
        {
            if (!key.isInterface())
            {
                nonProfiledClasses.add(key);
            }
            else
            {
                implementationsToCheck.add(implementation);
            }
        }

        @Override
        <T> void implementation(final Scope scope, final Class<? super T> key, final Class<T> implementation, final Object... parameterKeys)
        {
            if (!key.isInterface())
            {
                nonProfiledClasses.add(key);
            }
            else
            {
                implementationsToCheck.add(implementation);
            }
        }

        @Override
        <T> void implementation(final Scope scope, final Class<? super T> key, final Class<T> implementation, final Parameter[] parameters)
        {
            if (!key.isInterface())
            {
                nonProfiledClasses.add(key);
            }
            else
            {
                implementationsToCheck.add(implementation);
            }
        }

        @Override
        <T> void implementationUseDefaultConstructor(final Scope scope, final Class<T> key, final Class<? extends T> implementation)
        {
            if (!key.isInterface())
            {
                nonProfiledClasses.add(key);
            }
            else
            {
                implementationsToCheck.add(implementation);
            }
        }

        public Set<Class<?>> getNonProfiledClasses()
        {
            return nonProfiledClasses;
        }

        public Set<Class<?>> getImplementationsToCheck()
        {
            return implementationsToCheck;
        }
    }
}
