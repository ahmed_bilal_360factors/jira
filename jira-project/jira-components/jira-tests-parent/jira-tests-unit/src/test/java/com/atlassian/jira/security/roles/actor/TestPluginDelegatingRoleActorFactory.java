package com.atlassian.jira.security.roles.actor;

import java.util.List;
import java.util.Set;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.database.MockDbConnectionManager;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.plugin.roles.ProjectRoleActorModuleDescriptor;
import com.atlassian.jira.security.roles.OptimizedRoleActorFactory;
import com.atlassian.jira.security.roles.PluginDelegatingRoleActorFactory;
import com.atlassian.jira.security.roles.ProjectRoleActor;
import com.atlassian.jira.security.roles.RoleActor;
import com.atlassian.jira.security.roles.RoleActorDoesNotExistException;
import com.atlassian.jira.security.roles.RoleActorFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginAccessor;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestPluginDelegatingRoleActorFactory
{
    private PluginDelegatingRoleActorFactory pluginDelegatingRoleActorFactory;

    private final MockDbConnectionManager mockDbConnectionManager = new MockDbConnectionManager();

    @Mock
    private PluginAccessor pluginAccessor;

    @Mock
    private ApplicationUser user;

    @Rule
    public final TestRule mockInContainer = MockitoMocksInContainer.forTest(this);

    @Before
    public void setup()
    {
        pluginDelegatingRoleActorFactory = new PluginDelegatingRoleActorFactory(pluginAccessor, mockDbConnectionManager);
    }

    @Test
    public void getAllRoleActorsForUserOnlyOptimizedFactories()
    {
        final ProjectRoleActorModuleDescriptor descriptor1 = mock(ProjectRoleActorModuleDescriptor.class);
        final ProjectRoleActorModuleDescriptor descriptor2 = mock(ProjectRoleActorModuleDescriptor.class);

        final OptimizedRoleActorFactory factory1 = mock(OptimizedRoleActorFactory.class);
        final OptimizedRoleActorFactory factory2 = mock(OptimizedRoleActorFactory.class);

        final ProjectRoleActor projectRoleActor11 = mock(ProjectRoleActor.class);
        final ProjectRoleActor projectRoleActor12 = mock(ProjectRoleActor.class);
        final ProjectRoleActor projectRoleActor21 = mock(ProjectRoleActor.class);
        final ProjectRoleActor projectRoleActor22 = mock(ProjectRoleActor.class);
        final ProjectRoleActor projectRoleActor23 = mock(ProjectRoleActor.class);

        when(descriptor1.getModule()).thenReturn(factory1);
        when(descriptor1.getKey()).thenReturn("descriptor1");
        when(descriptor2.getModule()).thenReturn(factory2);
        when(descriptor2.getKey()).thenReturn("descriptor2");

        when(factory1.getAllRoleActorsForUser(user)).thenReturn(ImmutableSet.of(projectRoleActor11, projectRoleActor12));
        when(factory2.getAllRoleActorsForUser(user)).thenReturn(ImmutableSet.of(projectRoleActor21, projectRoleActor22, projectRoleActor23));

        when(pluginAccessor.getEnabledModuleDescriptorsByClass(ProjectRoleActorModuleDescriptor.class)).thenReturn(ImmutableList.of(descriptor1, descriptor2));

        final Set<ProjectRoleActor> allRoleActorsForUser = pluginDelegatingRoleActorFactory.getAllRoleActorsForUser(user);

        assertThat(allRoleActorsForUser, containsInAnyOrder(projectRoleActor11, projectRoleActor12, projectRoleActor21, projectRoleActor22, projectRoleActor23));
        mockDbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void getAllRoleActorsForUserWithNotOptimizedFactories()
    {
        final ProjectRoleActorModuleDescriptor descriptor1 = mock(ProjectRoleActorModuleDescriptor.class);
        final ProjectRoleActorModuleDescriptor descriptor2 = mock(ProjectRoleActorModuleDescriptor.class);
        final ProjectRoleActorModuleDescriptor descriptor3 = mock(ProjectRoleActorModuleDescriptor.class);

        final OptimizedRoleActorFactory factory1 = mock(OptimizedRoleActorFactory.class);
        final OptimizedRoleActorFactory factory2 = mock(OptimizedRoleActorFactory.class);
        final RoleActorFactory factory3 = new MockRoleActorFactory();
        final ProjectRoleActor projectRoleActor11 = mock(ProjectRoleActor.class);
        final ProjectRoleActor projectRoleActor12 = mock(ProjectRoleActor.class);
        final ProjectRoleActor projectRoleActor21 = mock(ProjectRoleActor.class);
        final ProjectRoleActor projectRoleActor22 = mock(ProjectRoleActor.class);
        final ProjectRoleActor projectRoleActor23 = mock(ProjectRoleActor.class);
        final ProjectRoleActor projectRoleActor31 = new MockRoleActor(1L, 101L, 201L, "param");
        final ProjectRoleActor projectRoleActor32 = new MockRoleActor(2L, 102L, 202L, "param");
        final ProjectRoleActor projectRoleActor33 = new MockRoleActor(3L, 103L, 203L, "param");

        when(pluginAccessor.getEnabledModuleDescriptorsByClass(ProjectRoleActorModuleDescriptor.class)).thenReturn(ImmutableList.of(descriptor1, descriptor2, descriptor3));

        when(descriptor1.getModule()).thenReturn(factory1);
        when(descriptor1.getKey()).thenReturn("descriptor1");
        when(descriptor2.getModule()).thenReturn(factory2);
        when(descriptor2.getKey()).thenReturn("descriptor2");
        when(descriptor3.getModule()).thenReturn(factory3);
        when(descriptor3.getKey()).thenReturn("descriptor3");
        when(descriptor3.getType()).thenReturn("some-role");

        when(factory1.getAllRoleActorsForUser(user)).thenReturn(ImmutableSet.of(projectRoleActor11, projectRoleActor12));
        when(factory2.getAllRoleActorsForUser(user)).thenReturn(ImmutableSet.of(projectRoleActor21, projectRoleActor22, projectRoleActor23));

        final List<ResultRow> queryResults = ImmutableList.of(toResultRow(projectRoleActor31), toResultRow(projectRoleActor32), toResultRow(projectRoleActor33));

        mockDbConnectionManager.setQueryResults
            ("select pra.id, pra.pid, pra.projectroleid, pra.roletype, pra.roletypeparameter\n" +
            "from projectroleactor pra\n" +
            "where pra.roletype = 'some-role'",
            queryResults);


        final Set<ProjectRoleActor> allRoleActorsForUser = pluginDelegatingRoleActorFactory.getAllRoleActorsForUser(user);

        assertThat(allRoleActorsForUser, containsInAnyOrder(projectRoleActor11, projectRoleActor12, projectRoleActor21, projectRoleActor22, projectRoleActor23, projectRoleActor31, projectRoleActor32, projectRoleActor33));
        mockDbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    private static class MockRoleActorFactory implements RoleActorFactory
    {
        @Override
        public ProjectRoleActor createRoleActor(final Long id, final Long projectRoleId, final Long projectId, final String type, final String parameter)
                throws RoleActorDoesNotExistException
        {
            return new MockRoleActor(id, projectRoleId, projectId, parameter);
        }

        @Override
        public Set<RoleActor> optimizeRoleActorSet(final Set<RoleActor> roleActors)
        {
            return roleActors;
        }
    }

    private static class MockRoleActor extends AbstractRoleActor
    {

        public MockRoleActor(final Long id, final Long projectRoleId, final Long projectId, final String parameter)
        {
            super(id, projectRoleId, projectId, parameter);
        }

        @Override
        public String getType()
        {
            return "MOCK";
        }

        @Override
        public Set<User> getUsers()
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public boolean contains(final ApplicationUser user)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public boolean contains(final User user)
        {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    private ResultRow toResultRow(final ProjectRoleActor actor)
    {
        return new ResultRow(actor.getId(), actor.getProjectId(), actor.getProjectRoleId(), actor.getType(), actor.getParameter());
    }
}
