package com.atlassian.jira.issue.attachment.store;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;

import com.atlassian.fugue.Either;
import com.atlassian.jira.issue.attachment.AttachmentWriteException;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.jira.util.LimitedOutputStream;
import com.atlassian.jira.util.PathTraversalRuntimeException;

import com.googlecode.catchexception.CatchException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import static com.atlassian.jira.matchers.FileMatchers.hasContent;
import static com.atlassian.jira.matchers.FileMatchers.isEmptyDirectory;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.hamcrest.core.IsNot.not;
import static org.mockito.Mockito.mock;

public class TestLocalTemporaryFileStore
{
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private LocalTemporaryFileStore localTemporaryFileStore;
    private File rootDirectoryFolder;

    @Before
    public void setUp() throws Exception
    {
        rootDirectoryFolder = temporaryFolder.newFolder();
        localTemporaryFileStore = new LocalTemporaryFileStore(rootDirectoryFolder, new PathTraversalChecker());
    }

    @Test
    public void testCreateDeleteCycle() throws Exception
    {
        final byte[] contentBytes = "sampleContent".getBytes("UTF-8");
        final ByteArrayInputStream inputStream = new ByteArrayInputStream(contentBytes);

        final TemporaryAttachmentId id = localTemporaryFileStore.createTemporaryFile(inputStream, contentBytes.length);

        localTemporaryFileStore.deleteTemporaryAttachment(id).claim();
    }

    @Test
    public void testShouldDetectPathTraversalWhenDeleting() throws Exception
    {
        final TemporaryAttachmentId tempId = TemporaryAttachmentId.fromString("../../../../../../../etc/passwd");

        expectedException.expect(PathTraversalRuntimeException.class);

        localTemporaryFileStore.deleteTemporaryAttachment(tempId).claim();
    }

    @Test
    public void testDetectPathTraversalWhenAskingForFile() throws Exception
    {
        final TemporaryAttachmentId tempId = TemporaryAttachmentId.fromString("../../../../../../../etc/passwd");

        final Either<Exception, File> temporaryAttachmentFile = localTemporaryFileStore.getTemporaryAttachmentFile(tempId);

        assertThat(temporaryAttachmentFile.left().get(), instanceOf(PathTraversalRuntimeException.class));
    }

    @Test
    public void testNotCreateTemporaryFileWithMoreDataThanSpecifiedBySize() throws Exception
    {
        final long size = 10;
        final int tooBigStreamSize = (int) size + 1;
        final InputStream inputStream = mockStreamWithSize(tooBigStreamSize);

        CatchException.catchException(localTemporaryFileStore).createTemporaryFile(inputStream, size);

        final Exception caughtException = CatchException.caughtException();
        assertThat(caughtException, instanceOf(AttachmentWriteException.class));
        assertThat(caughtException.getCause(), instanceOf(LimitedOutputStream.TooBigIOException.class));
    }

    @Test
    public void testNotLeaveRubbishTempsAfterTooBigIOException() throws Exception
    {
        final long size = 10;
        final int tooBigStreamSize = (int) size + 1;
        final InputStream inputStream = mockStreamWithSize(tooBigStreamSize);

        CatchException.catchException(localTemporaryFileStore).createTemporaryFile(inputStream, size);

        assertThat(rootDirectoryFolder, isEmptyDirectory());
    }

    @Test
    public void testNotCreateTemporaryAttachmentWhenNotEnoughDataProvided() throws Exception
    {
        final long size = 10;
        final int tooSmallStream = (int) size - 1;
        final InputStream inputStream = mockStreamWithSize(tooSmallStream);

        CatchException.catchException(localTemporaryFileStore).createTemporaryFile(inputStream, size);

        final Exception caughtException = CatchException.caughtException();
        assertThat(caughtException, instanceOf(AttachmentWriteException.class));
        assertThat(caughtException.getCause(), instanceOf(NotEnoughDataIOException.class));
    }

    @Test
    public void testNotLeaveRubbishTempsAfterNotEnoughDataException() throws Exception
    {
        final long size = 10;
        final int tooSmallStream = (int) size - 1;
        final InputStream inputStream = mockStreamWithSize(tooSmallStream);

        CatchException.catchException(localTemporaryFileStore).createTemporaryFile(inputStream, size);

        assertThat(rootDirectoryFolder, isEmptyDirectory());
    }

    @Test
    public void testNotLeaveAnyRubbishWhenWrongSizeProvided() throws Exception
    {
        final long size = -1;

        CatchException.catchException(localTemporaryFileStore).createTemporaryFile(mock(InputStream.class), size);

        assertThat(rootDirectoryFolder, isEmptyDirectory());
    }

    @Test
    public void testCreateTemporaryAttachmentWhenNoSizeProvided() throws Exception
    {
        final String sampleContent = "sample content";
        final InputStream inputStream = getStreamWithContent(sampleContent);

        final TemporaryAttachmentId temporaryFile = localTemporaryFileStore.createTemporaryFile(inputStream);

        final File file = localTemporaryFileStore.getTemporaryAttachmentFile(temporaryFile).right().get();

        assertThat(rootDirectoryFolder, not(isEmptyDirectory()));
        assertThat(file, hasContent(sampleContent));
    }

    private InputStream getStreamWithContent(final String sampleContent)
    {
        return new ByteArrayInputStream(sampleContent.getBytes(Charset.forName("UTF-8")));
    }

    private InputStream mockStreamWithSize(final int size)
    {
        return new ByteArrayInputStream(new byte[size]);
    }
}