package com.atlassian.jira.service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobRunnerKey;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.InOrder;
import org.mockito.Mock;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

/**
 * @since v6.4.1
 */
public class TestServiceRunner
{
    @Rule
    public RuleChain mockitoMocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    @AvailableInContainer
    private ServiceManager serviceManager;
    @Mock
    private JiraServiceContainer service;
    @Mock
    private JobRunnerRequest jobRunnerRequest;
    private JobConfig jobConfig;

    /**
     * JobConfig is a final class, so manually mock an object here
     */
    private static JobConfig mockJobConfig()
    {
        Map<String, Serializable> parametersMap = new HashMap<String, Serializable>();
        parametersMap.put(ServiceManager.SERVICE_ID_KEY, 1L);
        return JobConfig.forJobRunnerKey(JobRunnerKey.of("TEST_KEY")).withParameters(parametersMap);
    }

    @Before
    public void setUp() throws Exception
    {
        jobConfig = mockJobConfig();
        when(jobRunnerRequest.getJobConfig()).thenReturn(jobConfig);
        when(serviceManager.getServiceWithId(anyLong())).thenReturn(service);
    }

    @Test
    public void testLoggedInUserIsClearedBeforeServiceIsRun() throws Exception
    {
        ServiceRunner serviceRunner = new ServiceRunner();
        serviceRunner.runJob(jobRunnerRequest);
        InOrder inOrder = inOrder(jiraAuthenticationContext, service);
        inOrder.verify(jiraAuthenticationContext, times(1)).clearLoggedInUser();
        inOrder.verify(service, times(1)).run();
    }
}
