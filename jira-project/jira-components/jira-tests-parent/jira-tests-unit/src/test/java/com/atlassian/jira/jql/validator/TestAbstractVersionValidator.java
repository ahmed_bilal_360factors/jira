package com.atlassian.jira.jql.validator;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.jql.resolver.VersionResolver;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class TestAbstractVersionValidator
{
    @Mock
    private VersionValuesExistValidator versionValuesExistValidator;

    @Mock
    private SupportedOperatorsValidator supportedOperatorsValidator;

    private TestedAbstractVersionValidator versionValidator;

    @Before
    public void setup() throws Exception
    {
        versionValidator = new TestedAbstractVersionValidator(null, null, null, null, null);
    }

    @Test
    public void validateAnonymousUser()
    {
        final QueryCreationContext queryCreationContext = mock(QueryCreationContext.class);
        when(queryCreationContext.getApplicationUser()).thenReturn(null);
        when(queryCreationContext.getUser()).thenReturn(null);

        final TerminalClause terminalClause = mock(TerminalClause.class);

        final MessageSet messageSet = mock(MessageSet.class);
        when(messageSet.hasAnyErrors()).thenReturn(false);

        final MessageSet setFromValuesExistValidator = mock(MessageSet.class);
        when(setFromValuesExistValidator.hasAnyErrors()).thenReturn(false);

        when(supportedOperatorsValidator.validate(any(ApplicationUser.class), same(terminalClause))).thenReturn(messageSet);
        when(versionValuesExistValidator.validate(same(terminalClause), same(queryCreationContext))).thenReturn(setFromValuesExistValidator);

        final MessageSet result = versionValidator.validate(terminalClause, queryCreationContext);

        assertEquals(setFromValuesExistValidator, result);
        Mockito.verify(supportedOperatorsValidator).validate(any(ApplicationUser.class), same(terminalClause));
        Mockito.verify(versionValuesExistValidator).validate(same(terminalClause), same(queryCreationContext));
    }

    @Test
    public void validateAnonymousUserOperatorsValidationError()
    {
        final QueryCreationContext queryCreationContext = mock(QueryCreationContext.class);
        when(queryCreationContext.getApplicationUser()).thenReturn(null);
        when(queryCreationContext.getUser()).thenReturn(null);

        final TerminalClause terminalClause = mock(TerminalClause.class);

        final MessageSet messageSet = mock(MessageSet.class);
        when(messageSet.hasAnyErrors()).thenReturn(true);

        final MessageSet setFromValuesExistValidator = mock(MessageSet.class);
        when(setFromValuesExistValidator.hasAnyErrors()).thenReturn(false);

        when(supportedOperatorsValidator.validate(any(ApplicationUser.class), same(terminalClause))).thenReturn(messageSet);
        when(versionValuesExistValidator.validate(same(terminalClause), same(queryCreationContext))).thenReturn(setFromValuesExistValidator);

        final MessageSet result = versionValidator.validate(terminalClause, queryCreationContext);

        assertEquals(messageSet, result);
        Mockito.verify(supportedOperatorsValidator).validate(any(ApplicationUser.class), same(terminalClause));
        Mockito.verify(versionValuesExistValidator, never()).validate(same(terminalClause), same(queryCreationContext));
    }

    private class TestedAbstractVersionValidator extends AbstractVersionValidator
    {
        TestedAbstractVersionValidator(final VersionResolver versionResolver, final JqlOperandResolver operandResolver,
                final PermissionManager permissionManager, final VersionManager versionManager, final I18nHelper.BeanFactory beanFactory)
        {
            super(versionResolver, operandResolver, permissionManager, versionManager, beanFactory);
        }

        @Override
        SupportedOperatorsValidator getSupportedOperatorsValidator()
        {
            return supportedOperatorsValidator;
        }

        @Override
        VersionValuesExistValidator getVersionValuesExistValidator(final VersionResolver versionResolver,
                final JqlOperandResolver operandResolver, final PermissionManager permissionManager,
                final VersionManager versionManager, final I18nHelper.BeanFactory beanFactory)
        {
            return versionValuesExistValidator;
        }
    }
}