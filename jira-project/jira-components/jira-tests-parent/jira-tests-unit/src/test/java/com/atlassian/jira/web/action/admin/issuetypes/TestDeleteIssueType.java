package com.atlassian.jira.web.action.admin.issuetypes;

import java.util.Collections;

import com.atlassian.jira.JiraTestUtil;
import com.atlassian.jira.config.IssueTypeService;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.action.MockRedirectSanitiser;

import com.mockobjects.servlet.MockHttpServletResponse;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.config.IssueTypeService.IssueTypeDeleteInput;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDeleteIssueType
{
    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    private DeleteIssueType deleteIssueType;

    @AvailableInContainer (instantiateMe = true)
    private MockRedirectSanitiser redirectSanitiser;

    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    private IssueType issueType;
    @Mock
    private IssueTypeService issueTypeService;

    private final MockI18nHelper mockI18nHelper = new MockI18nHelper();

    @Before
    public void setUp() throws Exception
    {
        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(mockI18nHelper);
        deleteIssueType = new DeleteIssueType(issueTypeService);
    }

    @AfterClass
    public static void tearDown() throws Exception
    {
        JiraTestUtil.resetRequestAndResponse();
    }

    @Test
    public void testFindingSurrogateTypesForwardsToIssueTypeService() throws Exception
    {
        final String id = "10000";
        when(issueType.getId()).thenReturn(id);
        when(issueTypeService.getIssueType(any(ApplicationUser.class), eq(id))).thenReturn(some(issueType));
        when(issueTypeService.getAvailableAlternativesForIssueType(any(ApplicationUser.class), eq(id))).thenReturn(Collections.<IssueType>emptyList());

        deleteIssueType.setId(id);
        deleteIssueType.getAvailableIssueTypes();

        verify(issueTypeService).getAvailableAlternativesForIssueType(any(ApplicationUser.class), eq(id));
    }

    @Test
    public void testDeletingForwardsToIssueTypeManager() throws Exception
    {
        MockHttpServletResponse mockHttpServletResponse = JiraTestUtil.setupExpectedRedirect("ViewIssueTypes.jspa");
        String id = "100";
        String newId = "101";

        final IssueTypeService.DeleteValidationResult deleteValidationResult = new IssueTypeService.DeleteValidationResult(
                new SimpleErrorCollection(), some(new IssueTypeDeleteInput(id, some(newId))));
        when(issueTypeService.validateDeleteIssueType(any(ApplicationUser.class), any(IssueTypeDeleteInput.class)))
                .thenReturn(deleteValidationResult);

        deleteIssueType.setId(id);
        deleteIssueType.setNewId(newId);
        deleteIssueType.doExecute();

        verify(issueTypeService).deleteIssueType(any(ApplicationUser.class), eq(deleteValidationResult));
        mockHttpServletResponse.verify();
    }

    @Test
    public void testValidationFailsOnNonExistentSurrogates() throws Exception
    {
        final String id = "10000";
        final String newId = "10012";
        final IssueTypeService.DeleteValidationResult validationResult = new IssueTypeService.DeleteValidationResult(
                new SimpleErrorCollection("admin.errors.issuetypes.no.alternative", ErrorCollection.Reason.VALIDATION_FAILED),
                none(IssueTypeDeleteInput.class));
        when(issueTypeService.validateDeleteIssueType(any(ApplicationUser.class), any(IssueTypeDeleteInput.class)))
                .thenReturn(validationResult);

        deleteIssueType.setId(id);
        deleteIssueType.setNewId(newId);
        deleteIssueType.doValidation();

        assertEquals(singletonList("admin.errors.issuetypes.no.alternative"), deleteIssueType.getErrorMessages());
    }
}
