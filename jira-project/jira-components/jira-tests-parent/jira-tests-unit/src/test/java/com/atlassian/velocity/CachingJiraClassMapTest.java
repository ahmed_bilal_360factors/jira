package com.atlassian.velocity;

import java.lang.reflect.Method;

import com.atlassian.cache.Cache;
import com.atlassian.jira.junit.rules.InitMockitoMocks;

import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class CachingJiraClassMapTest
{
    public static final Object[] ARGUMENTS_INT = new Object[] { "str3", Integer.class };
    public static final Object[] ARGUMENTS_STRINGS_1 = new Object[] { "str1", "str2" };
    public static final Object[] ARGUMENTS_STRINGS_2 = new Object[] { "str1", "str3" };
    public static final String METHOD_NAME1 = "name1";
    public static final String METHOD_NAME2 = "name2";
    public static final Method METHOD1 = getMethod("method1");
    public static final Method METHOD2 = getMethod("method2");

    @Rule
    public TestRule initMockito = new InitMockitoMocks(this);

    @Mock
    private JiraClassMap delegateClassMap;

    @Mock
    private Cache<CachingJiraClassMap.MethodCacheEntry, JiraClassMap> cache;

    @InjectMocks
    private CachingJiraClassMap classMap;

    private static Method getMethod(final String methodName)
    {
        try
        {
            return TestClass.class.getMethod(methodName);
        }
        catch (final NoSuchMethodException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void shouldCacheInvokedMethod() throws Exception
    {
        when(delegateClassMap.findMethod(METHOD_NAME1, ARGUMENTS_STRINGS_1)).
                thenReturn(METHOD1, METHOD2);

        final Method firstResult = classMap.findMethod(METHOD_NAME1, ARGUMENTS_STRINGS_1);
        final Method secondResult = classMap.findMethod(METHOD_NAME1, ARGUMENTS_STRINGS_1);
        assertThat(firstResult, CoreMatchers.sameInstance(METHOD1));
        assertThat(firstResult, CoreMatchers.sameInstance(secondResult));
    }

    @Test
    public void testCacheWhenMethodNotFound() throws Exception
    {
        final Method method = classMap.findMethod(METHOD_NAME1, ARGUMENTS_INT);
        assertNull("No method should be returned for provided parameters", method);
    }

    @Test
    public void shouldDistinguishDifferentParametersTypes() throws Exception
    {
        when(delegateClassMap.findMethod(METHOD_NAME1, ARGUMENTS_STRINGS_1)).
                thenReturn(METHOD1);
        when(delegateClassMap.findMethod(METHOD_NAME1, ARGUMENTS_INT)).
                thenReturn(METHOD2);

        final Method firstResult = classMap.findMethod(METHOD_NAME1, ARGUMENTS_STRINGS_1);
        final Method secondResult = classMap.findMethod(METHOD_NAME1, ARGUMENTS_INT);
        assertThat(firstResult, CoreMatchers.sameInstance(METHOD1));
        assertThat(secondResult, CoreMatchers.sameInstance(METHOD2));
    }

    @Test
    public void shouldNotDistinguishParametersValues() throws Exception
    {
        when(delegateClassMap.findMethod(METHOD_NAME1, ARGUMENTS_STRINGS_1)).
                thenReturn(METHOD1);

        final Method firstResult = classMap.findMethod(METHOD_NAME1, ARGUMENTS_STRINGS_1);
        final Method secondResult = classMap.findMethod(METHOD_NAME1, ARGUMENTS_STRINGS_2);
        assertThat(firstResult, CoreMatchers.sameInstance(METHOD1));
        assertThat(firstResult, CoreMatchers.sameInstance(secondResult));
    }

    @Test
    public void shouldDistinguishMethodNamesParameters() throws Exception
    {
        when(delegateClassMap.findMethod(METHOD_NAME1, ARGUMENTS_STRINGS_1)).
                thenReturn(METHOD1);
        when(delegateClassMap.findMethod(METHOD_NAME2, ARGUMENTS_STRINGS_1)).
                thenReturn(METHOD2);

        final Method firstResult = classMap.findMethod(METHOD_NAME1, ARGUMENTS_STRINGS_1);
        final Method secondResult = classMap.findMethod(METHOD_NAME2, ARGUMENTS_STRINGS_1);
        assertThat(firstResult, CoreMatchers.sameInstance(METHOD1));
        assertThat(secondResult, CoreMatchers.sameInstance(METHOD2));
    }

    @SuppressWarnings ("UnusedDeclaration")
    public static class TestClass
    {
        public void method1() {}

        public void method2() {}
    }
}
