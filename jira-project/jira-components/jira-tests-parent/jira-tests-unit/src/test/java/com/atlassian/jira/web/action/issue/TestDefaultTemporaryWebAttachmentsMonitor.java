package com.atlassian.jira.web.action.issue;

import java.util.Collection;

import javax.servlet.http.HttpSessionBindingEvent;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachment;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.junit.rules.Log4jLogger;
import com.atlassian.jira.matchers.OptionMatchers;
import com.atlassian.util.concurrent.Promises;

import com.googlecode.catchexception.CatchException;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import static com.atlassian.jira.matchers.OptionMatchers.none;
import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.apis.CatchExceptionHamcrestMatchers.hasMessage;
import static com.googlecode.catchexception.apis.CatchExceptionHamcrestMatchers.hasNoCause;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultTemporaryWebAttachmentsMonitor
{
    public static final String FORM_TOKEN = "formToken";
    public static final String ATTACHMENT_ID = "greatId";
    @Rule
    public TestRule initMocks = new InitMockitoMocks(this);
    @Rule
    public Log4jLogger logger = new Log4jLogger();
    @Mock
    private StreamAttachmentStore attachmentStore;
    @Mock
    private TemporaryWebAttachment temporaryWebAttachment;
    @InjectMocks
    private DefaultTemporaryWebAttachmentsMonitor webAttachmentsMonitor;

    @Test
    public void shouldNotReturnAttachmentWhenNotDefinedById() throws Exception
    {
        //having
        //when
        final Option<TemporaryWebAttachment> tempWebAttachment = webAttachmentsMonitor.getById("some.id");
        //then
        assertThat("Temporary attachement should not be returned", tempWebAttachment, none());
    }

    @Test
    public void shouldThrowNPEWhenAttachmentIsNullInAdd()
    {
        //having
        //when
        //noinspection ConstantConditions
        catchException(webAttachmentsMonitor).add(null);

        //then
        //noinspection unchecked
        assertThat(CatchException.<NullPointerException>caughtException(), allOf(
                is(NullPointerException.class),
                hasMessage("temporaryAttachment"),
                hasNoCause()));
    }

    @Test
    public void shouldThrowNPEWhenAttachmentIdIsNullInAdd()
    {
        //having
        //when
        catchException(webAttachmentsMonitor).add(temporaryWebAttachment);

        //then
        //noinspection unchecked
        assertThat(CatchException.<NullPointerException>caughtException(), allOf(
                is(NullPointerException.class),
                hasMessage("attachmentStringId"),
                hasNoCause()));
    }

    @Test
    public void shouldThrowIllegalArgExWhenAddingDuplicatedAttachmentId()
    {
        //having
        addAttachmentWithIdAndToken(ATTACHMENT_ID, FORM_TOKEN);
        when(temporaryWebAttachment.getStringId()).thenReturn(ATTACHMENT_ID);

        //when
        catchException(webAttachmentsMonitor).add(temporaryWebAttachment);

        //then
        //noinspection unchecked
        assertThat(CatchException.<IllegalArgumentException>caughtException(), allOf(
                is(IllegalArgumentException.class),
                hasMessage("Temporary attachment with id='" + ATTACHMENT_ID + "' already in monitor"),
                hasNoCause()));
    }

    @Test
    public void shouldReturnAttachmentWhenPreviouslyInsertedById() throws Exception
    {
        //having
        final String id = "greatId";
        when(temporaryWebAttachment.getStringId()).thenReturn(id);
        webAttachmentsMonitor.add(temporaryWebAttachment);

        //when
        final Option<TemporaryWebAttachment> tempAttachmentById = webAttachmentsMonitor.getById(id);

        //then
        //noinspection RedundantTypeArguments
        assertThat("should return the same object that was inserted", tempAttachmentById,
                OptionMatchers.<TemporaryWebAttachment>some(sameInstance(temporaryWebAttachment)));
    }

    @Test
    public void shouldNotReturnAttachmentIfPreviouslyRemovedById() throws Exception
    {
        //having
        when(temporaryWebAttachment.getStringId()).thenReturn(ATTACHMENT_ID);
        webAttachmentsMonitor.add(temporaryWebAttachment);
        webAttachmentsMonitor.removeById(ATTACHMENT_ID);

        //when
        final Option<TemporaryWebAttachment> tempAttachmentById = webAttachmentsMonitor.getById(ATTACHMENT_ID);

        //then
        assertThat("temporary attachment should not be returned", tempAttachmentById, none());
    }

    @Test
    public void shouldNotAttemptToRemoveAttachmentFromStoreWhenRemovedById() throws Exception
    {
        //having
        when(temporaryWebAttachment.getStringId()).thenReturn(ATTACHMENT_ID);
        webAttachmentsMonitor.add(temporaryWebAttachment);

        //when
        webAttachmentsMonitor.removeById(ATTACHMENT_ID);

        //then
        verify(attachmentStore, never()).deleteTemporaryAttachment(Mockito.<TemporaryAttachmentId>any());
    }

    @Test
    public void shouldLogExceptionWhenRemovingAttachmentFromStoreFailed() throws Exception
    {
        //having
        final TemporaryAttachmentId temporaryAttachmentId = addAttachmentWithIdAndToken(ATTACHMENT_ID, FORM_TOKEN).getTemporaryAttachmentId();
        when(attachmentStore.deleteTemporaryAttachment(temporaryAttachmentId)).thenReturn(Promises.<Unit>rejected(new Exception("serious bug")));

        //when
        webAttachmentsMonitor.cleanByFormToken(FORM_TOKEN);

        //then
        assertThat(logger.getMessage(), containsString("WARN - Got exception while removing temporary attachment. Exception: serious bug"));
        assertThat(webAttachmentsMonitor.getByFormToken(FORM_TOKEN), Matchers.<TemporaryWebAttachment>empty());
    }

    @Test
    public void shouldRemoveAllAttachmentsOnCleanByFormToken()
    {
        //having
        final TemporaryAttachmentId temporaryAttachmentId1 = addAttachmentWithIdAndToken("id1", FORM_TOKEN).getTemporaryAttachmentId();
        final TemporaryAttachmentId temporaryAttachmentId2 = addAttachmentWithIdAndToken("id2", FORM_TOKEN).getTemporaryAttachmentId();
        final TemporaryAttachmentId temporaryAttachmentId3 = addAttachmentWithIdAndToken("id3", "some other token").getTemporaryAttachmentId();
        when(attachmentStore.deleteTemporaryAttachment(any(TemporaryAttachmentId.class)))
                .thenReturn(Promises.promise(Unit.VALUE));
        //when
        webAttachmentsMonitor.cleanByFormToken(FORM_TOKEN);

        //then
        verify(attachmentStore).deleteTemporaryAttachment(temporaryAttachmentId1);
        verify(attachmentStore).deleteTemporaryAttachment(temporaryAttachmentId2);
        verify(attachmentStore, never()).deleteTemporaryAttachment(temporaryAttachmentId3);
    }

    @Test
    public void shouldReturnAttachmentsFilteredByFormToken()
    {
        //having
        final TemporaryWebAttachment temporaryWebAttachment1 = addAttachmentWithIdAndToken("id1", FORM_TOKEN);
        final TemporaryWebAttachment temporaryWebAttachment2 = addAttachmentWithIdAndToken("id2", FORM_TOKEN);
        addAttachmentWithIdAndToken("id3", "some other token");

        //when
        final Iterable<TemporaryWebAttachment> temporaryWebAttachments = webAttachmentsMonitor.getByFormToken(FORM_TOKEN);

        //then
        assertThat(temporaryWebAttachments, containsInAnyOrder(temporaryWebAttachment1, temporaryWebAttachment2));
    }

    @Test
    public void shouldReturnEmptyCollectionWhenNoAttachmentsMatchedByFormToken()
    {
        //having
        addAttachmentWithIdAndToken("id1", FORM_TOKEN);
        addAttachmentWithIdAndToken("id2", FORM_TOKEN);

        //when
        final Collection<TemporaryWebAttachment> temporaryWebAttachments = webAttachmentsMonitor.getByFormToken(FORM_TOKEN + "decoy");

        //then
        assertThat(temporaryWebAttachments, Matchers.<TemporaryWebAttachment>empty());
    }

    @Test
    public void shouldDeleteAllTemporaryAttachmentsOnUnboundFromSession()
    {
        //having
        final TemporaryAttachmentId temporaryAttachmentId1 = addAttachmentWithIdAndToken("id1", FORM_TOKEN).getTemporaryAttachmentId();
        final TemporaryAttachmentId temporaryAttachmentId2 = addAttachmentWithIdAndToken("id2", FORM_TOKEN).getTemporaryAttachmentId();
        final TemporaryAttachmentId temporaryAttachmentId3 = addAttachmentWithIdAndToken("id3", "some other token").getTemporaryAttachmentId();
        when(attachmentStore.deleteTemporaryAttachment(any(TemporaryAttachmentId.class)))
                .thenReturn(Promises.promise(Unit.VALUE));
        //lets remove on attachment to be sure that this will not be deleted
        webAttachmentsMonitor.removeById("id2");

        //when
        webAttachmentsMonitor.valueUnbound(mock(HttpSessionBindingEvent.class));

        //then
        verify(attachmentStore).deleteTemporaryAttachment(temporaryAttachmentId1);
        verify(attachmentStore, never()).deleteTemporaryAttachment(temporaryAttachmentId2);
        verify(attachmentStore).deleteTemporaryAttachment(temporaryAttachmentId3);
    }

    private TemporaryWebAttachment addAttachmentWithIdAndToken(final String attachmentId, final String formToken)
    {
        final TemporaryAttachmentId temporaryAttachmentId = TemporaryAttachmentId.fromString(attachmentId);
        final TemporaryWebAttachment temporaryWebAttachment = mock(TemporaryWebAttachment.class);
        when(temporaryWebAttachment.getStringId()).thenReturn(attachmentId);
        when(temporaryWebAttachment.getFormToken()).thenReturn(formToken);
        when(temporaryWebAttachment.getTemporaryAttachmentId()).thenReturn(temporaryAttachmentId);

        webAttachmentsMonitor.add(temporaryWebAttachment);
        return temporaryWebAttachment;
    }
}