/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

package com.atlassian.jira.web.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.jira.util.mime.MimeManager;

import org.junit.Test;

import static com.atlassian.jira.web.util.FileIconBean.FileIcon;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TestFileIconBean
{
    InputStream emptyStream = new ByteArrayInputStream(new byte[0]);
    MimeManager voidMimeManager = new MimeManager(emptyStream)
    {
        public String getSuggestedMimeType(String fileName)
        {
            return "";
        }
    };

    private final FileIcon mimeType = new FileIcon(".zip", "text/plain", "", "");

    @Test
    public void testDefaultIcon()
    {
        FileIcon defaultIcon = new FileIcon(".*", "application/octet-stream", "file.gif", "File");

        FileIconBean fileBean = new FileIconBean(Collections.EMPTY_LIST, voidMimeManager);
        assertEquals(defaultIcon, fileBean.getFileIcon("unknown-extension.ext", "application/octet-stream"));
    }

    @Test
    public void testGetViaMimeType()
    {
        List fileIcons = EasyList.build(mimeType);
        FileIconBean fileBean = new FileIconBean(fileIcons, voidMimeManager);
        assertEquals(mimeType, fileBean.getFileIcon("", "text/plain"));
    }

    @Test
    public void testGetViaFileName()
    {
        List fileIcons = EasyList.build(mimeType);
        FileIconBean fileBean = new FileIconBean(fileIcons, voidMimeManager);

        assertEquals(mimeType, fileBean.getFileIcon("abc.zip", ""));
    }

    @Test
    public void testGetViaFileNameWithTrailingSpace()
    {
        List fileIcons = EasyList.build(mimeType);
        FileIconBean fileBean = new FileIconBean(fileIcons, voidMimeManager);

        assertEquals(mimeType, fileBean.getFileIcon("abc.zip ", ""));
    }

    @Test
    public void testGetViaMimeManager()
    {
        List fileIcons = EasyList.build(mimeType);

        //dodgy mime manager that always suggests text/plain
        MimeManager mimeManager = new MimeManager(null)
        {
            public String getSuggestedMimeType(String fileName)
            {
                return "text/plain";
            }
        };

        FileIconBean fileBean = new FileIconBean(fileIcons, mimeManager);
        assertEquals(mimeType, fileBean.getFileIcon("", ""));
    }
}
