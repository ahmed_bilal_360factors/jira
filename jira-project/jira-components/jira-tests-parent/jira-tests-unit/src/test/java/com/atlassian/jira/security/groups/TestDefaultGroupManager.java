package com.atlassian.jira.security.groups;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.Query;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.MockUser;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestDefaultGroupManager
{
    @Test
    public void testIsUserInGroupByUsername() throws Exception
    {
        final MockCrowdService mockCrowdService = new MockCrowdService();
        DefaultGroupManager groupManager = new DefaultGroupManager(mockCrowdService, null);
        assertFalse(groupManager.isUserInGroup("fred", "dudes"));

        mockCrowdService.addUserToGroup(new MockUser("fred"), new MockGroup("dudes"));
        assertTrue(groupManager.isUserInGroup("fred", "dudes"));
    }

    @Test
    public void testIsUserInGroupByUserObject() throws Exception
    {
        final MockCrowdService mockCrowdService = new MockCrowdService();
        final DirectoryManager mockDirectoryManager = Mockito.mock(DirectoryManager.class);
        DefaultGroupManager groupManager = new DefaultGroupManager(mockCrowdService, mockDirectoryManager);

        Mockito.when(mockDirectoryManager.isUserNestedGroupMember(1, "fred", "dudes")).thenReturn(false);
        assertFalse(groupManager.isUserInGroup(new MockUser("fred"), new MockGroup("dudes")));
        assertFalse(groupManager.isUserInGroup(new MockUser("fred"), "dudes"));

        Mockito.when(mockDirectoryManager.isUserNestedGroupMember(1, "fred", "dudes")).thenReturn(true);
        assertTrue(groupManager.isUserInGroup(new MockUser("fred"), new MockGroup("dudes")));
        assertTrue(groupManager.isUserInGroup(new MockUser("fred"), "dudes"));
    }

    @Test
    public void testIsUserInGroupHandlesNulls() throws Exception
    {
        // Need to handle null user and null group in order to maintain behaviour from OSUser.
        DefaultGroupManager groupManager = new DefaultGroupManager(null, null);
        assertFalse(groupManager.isUserInGroup(null, new MockGroup("dudes")));
        assertFalse(groupManager.isUserInGroup(new MockUser("fred"), (Group) null));
        assertFalse(groupManager.isUserInGroup(new MockUser("fred"), (String) null));
        assertFalse(groupManager.isUserInGroup((String) null, null));
        assertFalse(groupManager.isUserInGroup(null, (Group) null));
        assertFalse(groupManager.isUserInGroup((User) null, (String) null));
    }

    @Test
    public void testGetAllGroupsPreservesIterableOrder() throws Exception
    {
        final List<Group> orderedGroups = Arrays.<Group>asList(new MockGroup("alpha"), new MockGroup("beta"), new MockGroup("gamma"), new MockGroup("delta"));
        DefaultGroupManager manager = new DefaultGroupManager(new MockCrowdService() {
            {
                for (Group group : orderedGroups) addGroup(group);
            }

            @Override
            public <T> Iterable<T> search(Query<T> query)
            {
                return new Iterable<T>() {
                    @Override
                    public Iterator<T> iterator()
                    {
                        //noinspection unchecked
                        return (Iterator<T>) orderedGroups.iterator();
                    }
                };
            }
        }, null);

        Collection<Group> actualGroups = manager.getAllGroups();
        assertEquals(orderedGroups, new ArrayList<Group>(actualGroups));
    }
}
