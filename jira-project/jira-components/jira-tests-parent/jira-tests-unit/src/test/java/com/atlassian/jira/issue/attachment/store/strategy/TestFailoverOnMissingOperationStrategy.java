package com.atlassian.jira.issue.attachment.store.strategy;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.atlassian.jira.issue.attachment.NoAttachmentDataException;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

import com.google.common.collect.Lists;

import org.hamcrest.CoreMatchers;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import static org.junit.Assert.assertThat;

public class TestFailoverOnMissingOperationStrategy
{

    @Rule
    public TestRule initMock = new InitMockitoMocks(this);
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Mock
    private StreamAttachmentStore primaryStore;
    @Mock
    private StreamAttachmentStore secondaryStore;

    private FailoverOnMissingOperationStrategy operationStrategy;

    @Before
    public void setUp() throws Exception
    {
        operationStrategy = new FailoverOnMissingOperationStrategy(primaryStore, secondaryStore);
    }

    @Test
    public void shouldPerformOperationInBackgroundOnSuccess() throws Exception
    {
        final String expected = "value";
        final AtomicInteger atomicInteger = new AtomicInteger();
        final List<StreamAttachmentStore> executedStores = Lists.newArrayListWithExpectedSize(2);
        final Function<StreamAttachmentStore, Promise<String>> function = new Function<StreamAttachmentStore, Promise<String>>()
        {
            @Override
            public Promise<String> get(final StreamAttachmentStore input)
            {
                executedStores.add(input);
                return Promises.promise(expected + atomicInteger.getAndIncrement());
            }
        };

        final Promise<String> perform = operationStrategy.perform(function);
        assertThat(perform.claim(), CoreMatchers.equalTo(expected + "0"));
        assertThat(executedStores, IsIterableContainingInOrder.contains(primaryStore, secondaryStore));
    }

    @Test
    public void shouldNotUseResultFromSecondStoreOnFailureFromFirstOnGeneralException() throws Exception
    {
        final String expectedMessage = "I'm just throwing this ";
        final List<StreamAttachmentStore> executedStores = Lists.newArrayListWithExpectedSize(2);
        final AtomicInteger atomicInteger = new AtomicInteger();
        final Function<StreamAttachmentStore, Promise<String>> function = new Function<StreamAttachmentStore, Promise<String>>()
        {
            @Override
            public Promise<String> get(final StreamAttachmentStore input)
            {
                executedStores.add(input);
                return Promises.rejected(new RuntimeException(new AssertionError(expectedMessage + atomicInteger.getAndIncrement())));
            }
        };

        final Promise<String> perform = operationStrategy.perform(function);
        assertThat(executedStores, IsIterableContainingInOrder.contains(primaryStore));
        expectedException.expectMessage(expectedMessage + 0);
        expectedException.expect(RuntimeException.class);
        perform.claim();
    }

    @Test
    public void shouldUseResultFromSecondStoreOnFailureFromFirstOnlyWhenAttachmentNotExist() throws Exception
    {
        final AtomicInteger atomicInteger = new AtomicInteger();
        final String expectedMessage = "I'm just throwing this ";
        final Function<StreamAttachmentStore, Promise<String>> function = new Function<StreamAttachmentStore, Promise<String>>()
        {
            @Override
            public Promise<String> get(final StreamAttachmentStore input)
            {

                return Promises.rejected(new NoAttachmentDataException(expectedMessage + atomicInteger.getAndIncrement()));
            }
        };

        final Promise<String> perform = operationStrategy.perform(function);

        expectedException.expectMessage(expectedMessage + 1);
        expectedException.expect(NoAttachmentDataException.class);
        perform.claim();
    }
}