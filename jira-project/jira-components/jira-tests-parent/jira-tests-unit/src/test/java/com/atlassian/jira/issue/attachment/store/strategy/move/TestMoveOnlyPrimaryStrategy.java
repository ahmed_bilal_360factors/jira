package com.atlassian.jira.issue.attachment.store.strategy.move;

import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.jira.issue.attachment.store.MockAttachmentKeys;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestMoveOnlyPrimaryStrategy
{
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private StreamAttachmentStore streamAttachmentStore;

    @InjectMocks
    private SingleStoreMoveStrategy singleStoreMoveStrategy;

    @Test
    public void moveTemporaryToAttachmentDelegatesToStore() throws Exception
    {
        final TemporaryAttachmentId tempId = TemporaryAttachmentId.fromString("tempId");
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();
        final Promise<Unit> expectedPromise = Promises.promise(Unit.VALUE);
        when(streamAttachmentStore.moveTemporaryToAttachment(tempId, attachmentKey))
                .thenReturn(expectedPromise);

        final Promise<Unit> result = singleStoreMoveStrategy.moveTemporaryToAttachment(tempId, attachmentKey);

        assertThat(result, equalTo(expectedPromise));
    }
}