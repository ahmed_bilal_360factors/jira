package com.atlassian.jira.util;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class MemoryPoolsTest
{
    @Test
    public void test_inMiB() throws Exception
    {
        assertEquals("   0 MiB", MemoryPools.inMiB(100));
        assertEquals("   1 MiB", MemoryPools.inMiB(1024 * 1024));
        assertEquals("   5 MiB", MemoryPools.inMiB(5 * 1024 * 1024));

        assertEquals(" 100 MiB", MemoryPools.inMiB(104857600));
        assertEquals(" 256 MiB", MemoryPools.inMiB(268435456));
        assertEquals("1024 MiB", MemoryPools.inMiB(1073741824));

        // test rounding
        assertEquals("   0 MiB", MemoryPools.inMiB(524287));
        assertEquals("   1 MiB", MemoryPools.inMiB(524288));
        assertEquals("   1 MiB", MemoryPools.inMiB(1572863));
        assertEquals("   2 MiB", MemoryPools.inMiB(1572864));
        assertEquals("  20 MiB", MemoryPools.inMiB(20 * 1024 * 1024 + 524287));
        assertEquals("  21 MiB", MemoryPools.inMiB(20 * 1024 * 1024 + 524288));
        assertEquals("  21 MiB", MemoryPools.inMiB(21 * 1024 * 1024 + 524287));
        assertEquals("  22 MiB", MemoryPools.inMiB(21 * 1024 * 1024 + 524288));
    }
}
