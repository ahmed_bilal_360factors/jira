package com.atlassian.jira.issue.attachment;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.NoSuchElementException;

import com.atlassian.core.util.FileSize;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.AttachmentError;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.AttachmentValidator;
import com.atlassian.jira.issue.AttachmentsBulkOperationResult;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.action.issue.TemporaryWebAttachmentsMonitor;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

import org.hamcrest.Matchers;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static com.atlassian.jira.util.ErrorCollection.Reason.SERVER_ERROR;
import static com.atlassian.jira.util.ErrorCollection.Reason.VALIDATION_FAILED;
import static com.atlassian.labs.fugue.FugueMatchers.isLeft;
import static com.atlassian.labs.fugue.FugueMatchers.isRight;
import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static com.googlecode.catchexception.apis.CatchExceptionHamcrestMatchers.hasMessage;
import static com.googlecode.catchexception.apis.CatchExceptionHamcrestMatchers.hasNoCause;
import static java.lang.String.format;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings ("unchecked")
public class TestDefaultTemporaryWebAttachmentManager
{
    @Rule
    public TestRule rule = new InitMockitoMocks(this);

    @Mock
    private AttachmentManager attachmentManager;

    @Mock
    private AttachmentValidator attachmentValidator;

    @Spy
    private final I18nHelper.BeanFactory beanFactory = new MockI18nHelper().factory();

    @Mock (answer = RETURNS_DEEP_STUBS)
    private TemporaryWebAttachmentsMonitorLocator monitorLocator;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private TemporaryWebAttachmentFactory temporaryWebAttachmentFactory;

    @InjectMocks
    private DefaultTemporaryWebAttachmentManager sut;

    private final ApplicationUser user = new MockApplicationUser("testuser");
    private final Issue issue = new MockIssue(1001, "TST-1");
    private final TemporaryWebAttachment mockTemporaryWebAttachment = new TemporaryWebAttachment(
            TemporaryAttachmentId.fromString("Temp1"), "file1", "contentType", "formToken", 1, DateTime.now());

    @Before
    public void setUp() throws Exception
    {
        mockMaxAttachmentSize(10000);
    }

    private void mockMaxAttachmentSize(final long size)
    {
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_ATTACHMENT_SIZE)).thenReturn(String.valueOf(size));
    }

    @Test
    public void testCreateTemporaryWebAttachmentShouldCreateAttachment() throws Exception
    {
        final CreateTemporaryWebAttachmentParams params = newValidCreateTemporaryWebAttachmentParamsForIssue();

        final TemporaryAttachmentId temporaryAttachmentId = TemporaryAttachmentId.fromString("new-temporary-attachment");

        when(attachmentManager.createTemporaryAttachment(params.getInputStream(), params.getSize()))
                .thenReturn(temporaryAttachmentId);

        final TemporaryWebAttachmentsMonitor monitor = mock(TemporaryWebAttachmentsMonitor.class);
        when(monitorLocator.getOrCreate()).thenReturn(monitor);

        final TemporaryWebAttachment expectedAttachment = new TemporaryWebAttachment(
                temporaryAttachmentId,
                params.getFileName(),
                params.getContentType(),
                params.getFormToken(),
                params.getSize(),
                DateTime.now());

        when(temporaryWebAttachmentFactory.create(temporaryAttachmentId, params.getFileName(), params.getContentType(),
                params.getFormToken(), params.getSize()))
                .thenReturn(expectedAttachment);

        final Either<AttachmentError, TemporaryWebAttachment> result = callCreateTemporaryWebAttachment(sut, params);

        assertThat(result, isRight(equalTo(expectedAttachment)));
        verify(monitor).add(expectedAttachment);
    }

    @Test
    public void testCreateTemporaryWebAttachmentShouldFailWhenUserDoesntHaveCreateAttachmentsPermission()
            throws Exception
    {
        final CreateTemporaryWebAttachmentParams params = newValidCreateTemporaryWebAttachmentParamsForIssue();

        final String errorMessage = "No permission!";

        when(attachmentValidator.canCreateTemporaryAttachments(eq(user), eq(params.getTarget()), any(ErrorCollection.class)))
                .thenAnswer(putErrorToErrorCollection(errorMessage));

        final Either<AttachmentError, TemporaryWebAttachment> result = callCreateTemporaryWebAttachment(sut, params);

        final AttachmentError expected = new AttachmentError(
                "An error occurred: " + errorMessage,
                errorMessage,
                params.getFileName(),
                VALIDATION_FAILED
        );

        assertThat(result, isLeft(equalTo(expected)));
    }

    @Test
    public void testCreateTemporaryWebAttachmentShouldFailWithErrorWhenGotExceptionDuringCreationOfTemporaryAttachment()
            throws Exception
    {
        final CreateTemporaryWebAttachmentParams params = newValidCreateTemporaryWebAttachmentParamsForIssue();

        final RuntimeException createAttachmentException = new RuntimeException("Some error");
        doThrow(createAttachmentException)
                .when(attachmentManager).createTemporaryAttachment(params.getInputStream(), params.getSize());

        final Either<AttachmentError, TemporaryWebAttachment> result = callCreateTemporaryWebAttachment(sut, params);

        final AttachmentError expected = new AttachmentError(
                "Exception occurred while attaching file: " + createAttachmentException.toString(),
                format("attachfile.error.io.error [%s] [%s]",
                        params.getFileName(), createAttachmentException.getMessage()),
                params.getFileName(),
                Option.<Exception>some(createAttachmentException),
                SERVER_ERROR
        );

        assertThat(result, isLeft(equalTo(expected)));
    }

    @Test
    public void testCreateTemporaryWebAttachmentShouldFailWithErrorAndCleanupFailedTemporaryAttachmentWhenGotExceptionWhileAddingAttachmentToMonitor()
            throws Exception
    {
        final CreateTemporaryWebAttachmentParams params = newValidCreateTemporaryWebAttachmentParamsForIssue();

        final TemporaryAttachmentId temporaryAttachmentId = TemporaryAttachmentId.fromString("new-temporary-attachment");

        when(attachmentManager.createTemporaryAttachment(params.getInputStream(), params.getSize()))
                .thenReturn(temporaryAttachmentId);

        final TemporaryWebAttachment expectedAttachment = new TemporaryWebAttachment(
                temporaryAttachmentId,
                params.getFileName(),
                params.getContentType(),
                params.getFormToken(),
                params.getSize(),
                DateTime.now());

        when(temporaryWebAttachmentFactory.create(temporaryAttachmentId, params.getFileName(), params.getContentType(),
                params.getFormToken(), params.getSize()))
                .thenReturn(expectedAttachment);

        final RuntimeException putAttachmentException = new RuntimeException("Some error");
        doThrow(putAttachmentException).when(monitorLocator).getOrCreate();

        final Either<AttachmentError, TemporaryWebAttachment> result = callCreateTemporaryWebAttachment(sut, params);

        final AttachmentError expected = new AttachmentError(
                format("Got exception while adding temporary attachment '%s' to attachment monitor: %s",
                        params.getFileName(), putAttachmentException.toString()),
                format("attachfile.error.session.error [%s]", params.getFileName()),
                params.getFileName(),
                Option.<Exception>some(putAttachmentException),
                SERVER_ERROR
        );

        assertThat(result, isLeft(equalTo(expected)));
        verify(attachmentManager).deleteTemporaryAttachment(temporaryAttachmentId);
    }

    @Test
    public void testCreateTemporaryWebAttachmentShouldFailWithZeroSizedFile() throws Exception
    {
        final CreateTemporaryWebAttachmentParams params = newValidCreateTemporaryWebAttachmentParamsForIssue()
                .setAttachmentContent("");

        final Either<AttachmentError, TemporaryWebAttachment> result = callCreateTemporaryWebAttachment(sut, params);

        final AttachmentError expected = new AttachmentError(
                "Bad attachment size: 0",
                format("attachfile.error.io.bad.size.zero [%s]", params.getFileName()),
                params.getFileName(),
                VALIDATION_FAILED
        );

        assertThat(result, isLeft(equalTo(expected)));
    }

    @Test
    public void testCreateTemporaryWebAttachmentShouldFailWithTooBigFile() throws Exception
    {
        final CreateTemporaryWebAttachmentParams params = newValidCreateTemporaryWebAttachmentParamsForIssue()
                .setAttachmentContent(Strings.repeat("X", 2000));

        final int maxAttachmentSize = 1024;
        mockMaxAttachmentSize(maxAttachmentSize);

        final Either<AttachmentError, TemporaryWebAttachment> result = callCreateTemporaryWebAttachment(sut, params);

        final AttachmentError expected = new AttachmentError(
                "Attachment is too large. 2000 > 1024",
                format("attachfile.error.file.large [%s] [%s]", params.getFileName(), FileSize.format(maxAttachmentSize)),
                params.getFileName(),
                VALIDATION_FAILED
        );

        assertThat(result, isLeft(equalTo(expected)));
    }

    @Test
    public void testCreateTemporaryWebAttachmentShouldFailWithBlankFileName() throws Exception
    {
        final CreateTemporaryWebAttachmentParams params = newValidCreateTemporaryWebAttachmentParamsForIssue()
                .setFileName("");

        final Either<AttachmentError, TemporaryWebAttachment> result = callCreateTemporaryWebAttachment(sut, params);

        final AttachmentError expected = new AttachmentError(
                "Attachment file name is blank",
                "attachfile.error.no.name",
                params.getFileName(),
                VALIDATION_FAILED
        );

        assertThat(result, isLeft(equalTo(expected)));
    }

    private CreateTemporaryWebAttachmentParams newValidCreateTemporaryWebAttachmentParamsForIssue()
    {
        return new CreateTemporaryWebAttachmentParams(user, Either.<Issue, Project>left(issue));
    }

    @Test
    public void testCreateTemporaryWebAttachmentShouldFailWithNameThatContainsInvalidCharacters() throws Exception
    {
        final CreateTemporaryWebAttachmentParams params = newValidCreateTemporaryWebAttachmentParamsForIssue()
                .setFileName("invalid ? char");

        final Either<AttachmentError, TemporaryWebAttachment> result = callCreateTemporaryWebAttachment(sut, params);

        final AttachmentError expected = new AttachmentError(
                "Attachment file name contains invalid character ?",
                "attachfile.error.invalidcharacter [?]",
                params.getFileName(),
                VALIDATION_FAILED
        );

        assertThat(result, isLeft(equalTo(expected)));
    }

    @Test
    public void testCreateTemporaryWebAttachmentShouldAssertThatStreamIsNotNull() throws Exception
    {
        callCreateTemporaryWebAttachment(
                catchException(sut),
                newValidCreateTemporaryWebAttachmentParamsForIssue().setInputStream(null)
        );

        assertThat((IllegalArgumentException) caughtException(), allOf(
                is(IllegalArgumentException.class),
                hasMessage("stream should not be null!"),
                hasNoCause()
        ));
    }

    @Test
    public void testCreateTemporaryWebAttachmentShouldAssertThatContentTypeIsNotBlank() throws Exception
    {
        callCreateTemporaryWebAttachment(
                catchException(sut),
                newValidCreateTemporaryWebAttachmentParamsForIssue().setContentType("")
        );

        assertThat((IllegalArgumentException) caughtException(), allOf(
                is(IllegalArgumentException.class),
                hasMessage("contentType should not be empty!"),
                hasNoCause()
        ));
    }

    @Test
    public void testCreateTemporaryWebAttachmentShouldAssertThatTargetIsNotNull() throws Exception
    {
        callCreateTemporaryWebAttachment(
                catchException(sut),
                newValidCreateTemporaryWebAttachmentParamsForIssue().setTarget(null)
        );

        assertThat((IllegalArgumentException) caughtException(), allOf(
                is(IllegalArgumentException.class),
                hasMessage("target should not be null!"),
                hasNoCause()
        ));
    }

    @Test
    public void testCreateTemporaryWebAttachmentShouldAssertThatSizeIsNotNegative() throws Exception
    {
        callCreateTemporaryWebAttachment(
                catchException(sut),
                newValidCreateTemporaryWebAttachmentParamsForIssue().setSize(-1)
        );

        assertThat((IllegalArgumentException) caughtException(), allOf(
                is(IllegalArgumentException.class),
                hasMessage("size should be an non negative number. Got: -1"),
                hasNoCause()
        ));
    }

    private Either<AttachmentError, TemporaryWebAttachment> callCreateTemporaryWebAttachment(
            final TemporaryWebAttachmentManager target, final CreateTemporaryWebAttachmentParams p)
    {
        return target.createTemporaryWebAttachment(p.getInputStream(), p.getFileName(), p.getContentType(),
                p.getSize(), p.getTarget(), p.getFormToken(), p.getUser());
    }

    private static class CreateTemporaryWebAttachmentParams
    {
        private ByteArrayInputStream inputStream;
        private String fileName;
        private int size;
        private Either<Issue, Project> target;
        private final String formToken;
        private String contentType;
        private ApplicationUser user;

        public CreateTemporaryWebAttachmentParams(final ApplicationUser user, final Either<Issue, Project> target)
        {
            this.fileName = "filename.txt";
            this.formToken = "test-form";
            this.contentType = "text/plain";
            this.user = user;
            this.target = target;
            setAttachmentContent("AttachmentContent");
        }

        /**
         * Updates inputStream and size params with provided attachmentContent
         *
         * @param attachmentContent content to use for inputStream and size
         * @return current instance
         */
        public final CreateTemporaryWebAttachmentParams setAttachmentContent(final String attachmentContent)
        {
            final byte[] attachmentContentBytes = attachmentContent.getBytes(Charset.forName("UTF-8"));
            this.inputStream = new ByteArrayInputStream(attachmentContentBytes);
            this.size = attachmentContentBytes.length;
            return this;
        }

        public ByteArrayInputStream getInputStream()
        {
            return inputStream;
        }

        public CreateTemporaryWebAttachmentParams setInputStream(final ByteArrayInputStream inputStream)
        {
            this.inputStream = inputStream;
            return this;
        }

        public String getFileName()
        {
            return fileName;
        }

        public CreateTemporaryWebAttachmentParams setFileName(final String fileName)
        {
            this.fileName = fileName;
            return this;
        }

        public int getSize()
        {
            return size;
        }

        public CreateTemporaryWebAttachmentParams setSize(final int size)
        {
            this.size = size;
            return this;
        }

        public Either<Issue, Project> getTarget()
        {
            return target;
        }

        public CreateTemporaryWebAttachmentParams setTarget(final Either<Issue, Project> target)
        {
            this.target = target;
            return this;
        }

        public String getFormToken()
        {
            return formToken;
        }

        public String getContentType()
        {
            return contentType;
        }

        public CreateTemporaryWebAttachmentParams setContentType(final String contentType)
        {
            this.contentType = contentType;
            return this;
        }

        public ApplicationUser getUser()
        {
            return user;
        }

        public CreateTemporaryWebAttachmentParams setUser(final ApplicationUser user)
        {
            this.user = user;
            return this;
        }
    }

    private Answer<Boolean> putErrorToErrorCollection(final String errorMessage)
    {
        return new Answer<Boolean>()
        {
            @Override
            public Boolean answer(final InvocationOnMock invocation) throws Throwable
            {
                final ErrorCollection errorCollection = (ErrorCollection) invocation.getArguments()[2];
                errorCollection.addErrorMessage(errorMessage);
                return false;
            }
        };
    }

    @Test
    public void testGetTemporaryWebAttachmentUsesMethodFromLocatedMonitor() throws Exception
    {
        final String temporaryAttachmentId = "TestId";
        final Option<TemporaryWebAttachment> expected = mock(Option.class);
        when(monitorLocator.getOrCreate().getById(temporaryAttachmentId)).thenReturn(expected);

        final Option<TemporaryWebAttachment> result = sut.getTemporaryWebAttachment(temporaryAttachmentId);

        assertSame(expected, result);
    }

    @Test
    public void testGetTemporaryWebAttachmentsByFormTokenUsesMethodFromLocatedMonitor() throws Exception
    {
        final String formToken = "FormToken";
        final Collection<TemporaryWebAttachment> expected = mock(Collection.class);
        when(monitorLocator.getOrCreate().getByFormToken(formToken)).thenReturn(expected);

        final Collection<TemporaryWebAttachment> result = sut.getTemporaryWebAttachmentsByFormToken(formToken);

        assertSame(expected, result);
    }

    @Test
    public void testUpdateTemporaryWebAttachmentShouldReplaceAttachmentInMonitor() throws Exception
    {
        final String temporaryAttachmentId = "TestId";
        final TemporaryWebAttachment oldValue = mock(TemporaryWebAttachment.class);
        final TemporaryWebAttachment newValue = mock(TemporaryWebAttachment.class);

        final TemporaryWebAttachmentsMonitor monitor = monitorLocator.getOrCreate();
        when(monitor.getById(temporaryAttachmentId))
                .thenReturn(Option.some(oldValue));

        sut.updateTemporaryWebAttachment(temporaryAttachmentId, newValue);

        verify(monitor).removeById(temporaryAttachmentId);
        verify(monitor).add(newValue);
    }

    @Test
    public void testUpdateTemporaryWebAttachmentShouldRaiseNoSuchElementException() throws Exception
    {
        final String temporaryAttachmentId = "TestId";
        final TemporaryWebAttachment newValue = mock(TemporaryWebAttachment.class);

        when(monitorLocator.getOrCreate().getById(temporaryAttachmentId))
                .thenReturn(Option.<TemporaryWebAttachment>none());

        catchException(sut).updateTemporaryWebAttachment(temporaryAttachmentId, newValue);
        assertThat((NoSuchElementException) caughtException(), allOf(
                is(NoSuchElementException.class),
                hasMessage("Temporary Web Attachment with id TestId does not exist."),
                hasNoCause()
        ));
    }

    @Test
    public void testConvertTemporaryAttachmentsShouldConvertTwoAttachments() throws Exception
    {
        final TemporaryWebAttachment temp2 = new TemporaryWebAttachment(TemporaryAttachmentId.fromString("Temp2"),
                "file2", "contentType2", mockTemporaryWebAttachment.getFormToken(), 2, DateTime.now());

        final ImmutableList<String> attachmentsIds = ImmutableList.of(mockTemporaryWebAttachment.getStringId(), temp2.getStringId());

        final ChangeItemBean changeItemBean1 = mock(ChangeItemBean.class);
        final ChangeItemBean changeItemBean2 = mock(ChangeItemBean.class);

        final TemporaryWebAttachmentsMonitor monitor = monitorLocator.getOrCreate();
        when(monitor.getById(mockTemporaryWebAttachment.getStringId())).thenReturn(Option.some(mockTemporaryWebAttachment));
        when(monitor.getById(temp2.getStringId())).thenReturn(Option.some(temp2));

        final ConvertTemporaryAttachmentParams params = createConvertTemporaryAttachmentParams(mockTemporaryWebAttachment, issue, user);

        when(attachmentManager.convertTemporaryAttachment(params))
                .thenReturn(Either.<AttachmentError, ChangeItemBean>right(changeItemBean1));

        when(attachmentManager.convertTemporaryAttachment(createConvertTemporaryAttachmentParams(temp2, issue, user)))
                .thenReturn(Either.<AttachmentError, ChangeItemBean>right(changeItemBean2));

        final AttachmentsBulkOperationResult<ChangeItemBean> result = sut.convertTemporaryAttachments(user, issue, attachmentsIds);

        assertThat(result.getErrors(), Matchers.<AttachmentError>empty());
        assertThat(result.getResults(), Matchers.containsInAnyOrder(
                changeItemBean1, changeItemBean2
        ));

        verify(monitor).removeById(mockTemporaryWebAttachment.getStringId());
        verify(monitor).removeById(temp2.getStringId());
    }

    @Test
    public void testConvertTemporaryAttachmentsShouldFailWithErrorWhenTemporaryAttachmentDoesntExist() throws Exception
    {
        final ImmutableList<String> attachmentsIds = ImmutableList.of(mockTemporaryWebAttachment.getStringId());

        final TemporaryWebAttachmentsMonitor monitor = monitorLocator.getOrCreate();
        when(monitor.getById(mockTemporaryWebAttachment.getStringId())).thenReturn(Option.<TemporaryWebAttachment>none());

        final AttachmentsBulkOperationResult<ChangeItemBean> result = sut.convertTemporaryAttachments(user, issue, attachmentsIds);

        assertThat(result.getErrors(), Matchers.contains(
                new AttachmentError(
                        "Temporary attachment missing: " + mockTemporaryWebAttachment.getStringId(),
                        "attachfile.error.temp.file.not.exists",
                        "#" + mockTemporaryWebAttachment.getStringId(),
                        SERVER_ERROR
                )
        ));
        assertThat(result.getResults(), Matchers.<ChangeItemBean>empty());
    }

    @Test
    public void testConvertTemporaryAttachmentsShouldFailWithErrorWhenConversionFails() throws Exception
    {
        final ImmutableList<String> attachmentsIds = ImmutableList.of(mockTemporaryWebAttachment.getStringId());

        final TemporaryWebAttachmentsMonitor monitor = monitorLocator.getOrCreate();
        when(monitor.getById(mockTemporaryWebAttachment.getStringId())).thenReturn(Option.some(mockTemporaryWebAttachment));

        final AttachmentError expectedError = mock(AttachmentError.class);

        final ConvertTemporaryAttachmentParams params = createConvertTemporaryAttachmentParams(
                mockTemporaryWebAttachment, issue, user);

        when(attachmentManager.convertTemporaryAttachment(params))
                .thenReturn(Either.<AttachmentError, ChangeItemBean>left(expectedError));

        final AttachmentsBulkOperationResult<ChangeItemBean> result = sut.convertTemporaryAttachments(user, issue, attachmentsIds);

        assertThat(result.getErrors(), Matchers.contains(expectedError));
        assertThat(result.getResults(), Matchers.<ChangeItemBean>empty());
    }

    @Test
    public void testConvertTemporaryAttachmentsShouldFailWithErrorWhenUserDoesntHaveCreateAttachmentPermission()
            throws Exception
    {
        final ImmutableList<String> attachmentsIds = ImmutableList.of(mockTemporaryWebAttachment.getStringId());

        final TemporaryWebAttachmentsMonitor monitor = monitorLocator.getOrCreate();
        when(monitor.getById(mockTemporaryWebAttachment.getStringId())).thenReturn(Option.some(mockTemporaryWebAttachment));

        final String errorMessage = "No permission";
        when(attachmentValidator.canCreateAttachments(eq(user), eq(issue), any(ErrorCollection.class)))
                .thenAnswer(putErrorToErrorCollection(errorMessage));

        final AttachmentsBulkOperationResult<ChangeItemBean> result = sut.convertTemporaryAttachments(user, issue, attachmentsIds);

        assertThat(result.getErrors(), Matchers.contains(new AttachmentError(
                "An error occurred: " + errorMessage,
                errorMessage,
                "",
                VALIDATION_FAILED
        )));
        assertThat(result.getResults(), Matchers.<ChangeItemBean>empty());
    }

    private ConvertTemporaryAttachmentParams createConvertTemporaryAttachmentParams(
            final TemporaryWebAttachment tempAttachment, final Issue issue, final ApplicationUser user)
    {
        return ConvertTemporaryAttachmentParams.builder()
                .setAuthor(user)
                .setIssue(issue)
                .setTemporaryAttachmentId(tempAttachment.getTemporaryAttachmentId())
                .setFilename(tempAttachment.getFilename())
                .setContentType(tempAttachment.getContentType())
                .setFileSize(tempAttachment.getSize())
                .setCreatedTime(tempAttachment.getCreated())
                .build();
    }

    @Test
    public void testClearTemporaryAttachmentsByFormTokenShouldJustSilentlyPassWhenMonitorWasNone() throws Exception
    {
        final Option<TemporaryWebAttachmentsMonitor> monitorOption = Option.none();
        when(monitorLocator.get()).thenReturn(monitorOption);

        sut.clearTemporaryAttachmentsByFormToken("FormToken");
    }

    @Test
    public void testClearTemporaryAttachmentsByFormTokenShouldClearAttachmentsInMonitor() throws Exception
    {
        final String formToken = "FormToken";
        final TemporaryWebAttachmentsMonitor monitor = mock(TemporaryWebAttachmentsMonitor.class);

        final Option<TemporaryWebAttachmentsMonitor> monitorOption = Option.some(monitor);
        when(monitorLocator.get()).thenReturn(monitorOption);

        sut.clearTemporaryAttachmentsByFormToken(formToken);

        verify(monitor).cleanByFormToken(formToken);
    }
}