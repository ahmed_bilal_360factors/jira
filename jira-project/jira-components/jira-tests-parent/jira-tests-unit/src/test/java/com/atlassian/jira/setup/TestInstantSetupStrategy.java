package com.atlassian.jira.setup;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.mockito.Mockito.when;

public class TestInstantSetupStrategy
{
    private static final String EVENT_MSG = "fake johnson event";

    @Rule
    public RuleChain mockitoMocksInContainer = MockitoMocksInContainer.forTest(this);

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    @AvailableInContainer
    public SetupJohnsonUtil johnsonUtil;

    @Mock
    private SetupStrategy.StepSwitcher stepSwitcher;

    private SetupStrategy strategy;

    @Before
    public void setUp()
    {
        strategy = new InstantSetupStrategy();
    }

    @Test
    public void testSetupThrowsExceptionInCaseOfJohnsonEvent() throws Exception
    {
        final Event event = new Event(EventType.get("setup"), EVENT_MSG, EventLevel.get(EventLevel.ERROR));
        when(johnsonUtil.getEvents()).thenReturn(ImmutableList.of(event));

        exception.expect(RuntimeException.class);
        exception.expectMessage(EVENT_MSG);

        strategy.setup(null, stepSwitcher);
    }
}
