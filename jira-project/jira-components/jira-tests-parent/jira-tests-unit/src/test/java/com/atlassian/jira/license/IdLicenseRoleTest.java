package com.atlassian.jira.license;

import com.atlassian.fugue.Option;
import com.atlassian.jira.matchers.OptionMatchers;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.license.LicenseRoleId.valueOf;
import static com.atlassian.jira.matchers.OptionMatchers.none;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;

public class IdLicenseRoleTest
{
    @Test (expected = IllegalArgumentException.class)
    public void cotrFailsWithNullId()
    {
        new IdLicenseRole(null, Collections.<String>emptyList(), Option.none(String.class));
    }

    @Test (expected = IllegalArgumentException.class)
    public void cotrFailsWithNullGroups()
    {
        new IdLicenseRole(valueOf("jack"), null, Option.none(String.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsWithNullInGroups()
    {
        new IdLicenseRole(valueOf("jack"), Collections.<String>singletonList(null), Option.none(String.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsWithNullPrimaryGroup()
    {
        new IdLicenseRole(valueOf("jack"), Collections.<String>emptyList(), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsWithPrimaryGroupNotListedInGroups()
    {
        new IdLicenseRole(valueOf("jack"), Arrays.asList("one", "two"), Option.option("three"));
    }

    @Test
    public void equalsOnlyListensToId()
    {
        List<LicenseRole> roles = Arrays.<LicenseRole>asList(
                newRole("id", null, "one"),
                newRole("id", null, "two", "thee"),
                newRole("id", "three", "two", "three"),
                newRole("id", "three", "two", "three"));

        for (LicenseRole left : roles)
        {
            for (LicenseRole right : roles)
            {
                assertThat(left.equals(right), equalTo(true));
            }
        }

        List<LicenseRole> otherRoles = Arrays.<LicenseRole>asList(
                newRole("id2", null, "one"),
                newRole("id2", null, "two", "thee"),
                newRole("id2", "three", "two", "three"),
                newRole("id2", "three", "two", "three"));

        for (LicenseRole left : roles)
        {
            for (LicenseRole right : otherRoles)
            {
                assertThat(left.equals(right), equalTo(false));
            }
        }
    }

    @Test
    public void hashListensToId()
    {
        List<LicenseRole> roles = Arrays.<LicenseRole>asList(
                newRole("id", null, "one"),
                newRole("id", null, "two", "thee"),
                newRole("id", "three", "two", "three"),
                newRole("id", "three", "two", "three"));

        for (LicenseRole left : roles)
        {
            for (LicenseRole right : roles)
            {
                assertThat(left.hashCode() == right.hashCode(), equalTo(true));
            }
        }
    }

    @Test
    public void getGroupsReturnsGroups()
    {
        assertThat(newRole("id", null, "one").getGroups(), contains("one"));
        assertThat(newRole("id", null, "one", "two").getGroups(), containsInAnyOrder("one", "two"));
    }

    @Test
    public void getPrimaryGroupRetunsPrimary()
    {
        assertThat(newRole("id", null, "one").getPrimaryGroup(), none());
        assertThat(newRole("id", "two", "one", "two").getPrimaryGroup(), OptionMatchers.some("two"));
    }

    @Test
    public void getIdReturnsId()
    {
        assertThat(newRole("id", null, "one").getId(), equalTo(valueOf("id")));
    }

    @Test
    public void getNameReturnsId()
    {
        assertThat(newRole("id", null, "one").getName(), equalTo("id"));
    }

    @Test
    public void withGroupsUpdatesGroupInformation()
    {
        final LicenseRole orig = newRole("id", null, "one");
        final LicenseRole newRole = orig.withGroups(Arrays.asList("one", "two"), Option.none(String.class));

        assertThat(orig, new LicenseRoleMatcher().id("id").name("id").groups("one"));
        assertThat(newRole, new LicenseRoleMatcher().id("id").name("id").groups("one", "two"));
    }

    @Test
    public void withGroupsUpdatesPrimaryGroupInformation()
    {
        final LicenseRole orig = newRole("id", null, "one");
        final LicenseRole newRole = orig.withGroups(Arrays.asList("one", "two"), Option.some("two"));

        assertThat(orig, new LicenseRoleMatcher().id("id").name("id").groups("one"));
        assertThat(newRole, new LicenseRoleMatcher().id("id").name("id").groups("one", "two").primaryGroup("two"));

        final LicenseRole noneRole = newRole.withGroups(Arrays.asList("one", "two"), Option.none(String.class));

        assertThat(newRole, new LicenseRoleMatcher().id("id").name("id").groups("one", "two").primaryGroup("two"));
        assertThat(noneRole, new LicenseRoleMatcher().id("id").name("id").groups("one", "two"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithNullGroups()
    {
        final LicenseRole idLicenseRole = newRole("id", null, "one");
        idLicenseRole.withGroups(null, Option.none(String.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithNullInGroups()
    {
        final LicenseRole idLicenseRole = newRole("id", null, "one");
        idLicenseRole.withGroups(Arrays.asList("abc", null), Option.none(String.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithNullPrimary()
    {
        final LicenseRole idLicenseRole = newRole("id", null, "one");
        idLicenseRole.withGroups(Arrays.asList("abc"), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithPrimaryNotInGroup()
    {
        final LicenseRole idLicenseRole = newRole("id", null, "one");
        idLicenseRole.withGroups(Arrays.asList("abc"), Option.some("def"));
    }

    private static LicenseRole newRole(String id, String primary, String... groups)
    {
        return new IdLicenseRole(valueOf(id), Arrays.asList(groups), Option.option(primary));
    }
}