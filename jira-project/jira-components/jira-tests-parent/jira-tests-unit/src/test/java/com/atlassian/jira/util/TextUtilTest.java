package com.atlassian.jira.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TextUtilTest
{
    @Test
    public void test_lpad() throws Exception
    {
        assertEquals("", TextUtil.lpad("", 0));
        assertEquals("   ", TextUtil.lpad("", 3));
        assertEquals("Cat", TextUtil.lpad("Cat", 3));
        assertEquals("Cat", TextUtil.lpad("Cat", 2));
        assertEquals(" Cat", TextUtil.lpad("Cat", 4));
        assertEquals("     Cat", TextUtil.lpad("Cat", 8));
        assertEquals("Cat", TextUtil.lpad("Cat", 0));
        assertEquals("Cat", TextUtil.lpad("Cat", -1));
        assertEquals("Cat", TextUtil.lpad("Cat", -13));
    }

    @Test
    public void test_rpad() throws Exception
    {
        assertEquals("", TextUtil.rpad("", 0));
        assertEquals("   ", TextUtil.rpad("", 3));
        assertEquals("Rat", TextUtil.rpad("Rat", 3));
        assertEquals("Rat", TextUtil.rpad("Rat", 2));
        assertEquals("Rat ", TextUtil.rpad("Rat", 4));
        assertEquals("Rat     ", TextUtil.rpad("Rat", 8));
        assertEquals("Rat", TextUtil.rpad("Rat", 0));
        assertEquals("Rat", TextUtil.rpad("Rat", -1));
        assertEquals("Rat", TextUtil.lpad("Rat", -13));
    }
}
