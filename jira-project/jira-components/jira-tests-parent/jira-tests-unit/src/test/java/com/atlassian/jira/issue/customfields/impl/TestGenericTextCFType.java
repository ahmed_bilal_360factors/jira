package com.atlassian.jira.issue.customfields.impl;

import com.atlassian.jira.issue.customfields.view.CustomFieldParamsImpl;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.mock.security.MockAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.hamcrest.collection.IsMapContainingKey;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestGenericTextCFType
{
    private final String SHORT_TEXT = getShortText();
    private final String LONG_TEXT = getLongText();

    private static final Long MAX_LEN = 30L;
    public static final String CF_ID = "customfield_1010";

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    TextFieldCharacterLengthValidator textFieldCharacterLengthValidator;

    @Mock (answer = Answers.RETURNS_SMART_NULLS)
    FieldConfig fieldConfig;

    @Mock
    CustomField customField;

    MockAuthenticationContext authenticationContext = new MockAuthenticationContext(null, new MockI18nHelper());

    GenericTextCFType textCFType;

    @Before
    public void setUp()
    {
        textCFType = getFieldTypeUnderTest();
        when(textFieldCharacterLengthValidator.isTextTooLong(SHORT_TEXT)).thenReturn(false);
        when(textFieldCharacterLengthValidator.isTextTooLong(LONG_TEXT)).thenReturn(true);

        when(textFieldCharacterLengthValidator.getMaximumNumberOfCharacters()).thenReturn(MAX_LEN);
        when(fieldConfig.getCustomField()).thenReturn(customField);
        when(customField.getId()).thenReturn(CF_ID);
    }

    protected GenericTextCFType getFieldTypeUnderTest()
    {
        return new GenericTextCFType(null, null, textFieldCharacterLengthValidator, authenticationContext);
    }

    protected String getLongText()
    {
        return "something slightly longer blah blah";
    }

    protected String getShortText()
    {
        return "something";
    }

    @Test
    public void testValidationValueExceedingTheCharacterLimit() throws Exception
    {
        final SimpleErrorCollection errorCollectionToAddTo = new SimpleErrorCollection();
        textCFType.validateFromParams(getCustomFieldParams(LONG_TEXT), errorCollectionToAddTo, fieldConfig);
        assertThat(errorCollectionToAddTo.getReasons(), Matchers.contains(ErrorCollection.Reason.VALIDATION_FAILED));
        assertThat(errorCollectionToAddTo.getErrors(), IsMapContainingKey.hasKey(CF_ID));
    }

    @Test
    public void testValidationValueNotExceedingTheCharacterLimit() throws Exception
    {
        final SimpleErrorCollection errorCollectionToAddTo = new SimpleErrorCollection();
        textCFType.validateFromParams(getCustomFieldParams(SHORT_TEXT), errorCollectionToAddTo, fieldConfig);
        assertThat(errorCollectionToAddTo.getReasons().size(), is(0));
        assertThat(errorCollectionToAddTo.getErrors().size(), is(0));
    }

    @Test
    public void getValueFromCustomFieldParamsValueNotExceedingTheCharacterLimit() throws Exception
    {
        assertThat(textCFType.getValueFromCustomFieldParams(getCustomFieldParams(SHORT_TEXT)), Matchers.is(SHORT_TEXT));
    }

    @Test
    public void getValueFromCustomFieldParamsValueExceedingTheCharacterLimit() throws Exception
    {
        exception.expect(FieldValidationException.class);
        exception.expectMessage(Long.toString(MAX_LEN));
        exception.expectMessage("field.error.text.toolong");

        textCFType.getValueFromCustomFieldParams(getCustomFieldParams(LONG_TEXT));
    }

    private CustomFieldParamsImpl getCustomFieldParams(final String text)
    {
        final CustomFieldParamsImpl params = new CustomFieldParamsImpl();
        params.addValue(ImmutableList.of(text));
        return params;
    }

}
