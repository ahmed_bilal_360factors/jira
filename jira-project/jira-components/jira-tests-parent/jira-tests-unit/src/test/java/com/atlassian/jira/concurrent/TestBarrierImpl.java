package com.atlassian.jira.concurrent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 * Tests for {@link BarrierImpl}
 */
public class TestBarrierImpl
{
    /** Thread executor */
    private ExecutorService executorService;

    @Before
    public void setup()
    {
        executorService = Executors.newFixedThreadPool(2);
    }

    @After
    public void tearDown()
    {
        executorService.shutdownNow();
    }

    /**
     * Block a thread with barrier.raise(), then continue with barrier.lower().
     */
    @Test
    public void testRaiseAndLower() throws ExecutionException, InterruptedException
    {
        final BarrierImpl barrier = new BarrierImpl("testBarrier");
        barrier.raise();
        assertThat(barrier.counter.wouldBlock(), is(true));

        WaitsOnBarrier waitsOnBarrier = new WaitsOnBarrier(barrier);
        final Future result = executorService.submit(waitsOnBarrier);
        waitForThreadsToArriveAtBarrier(1, barrier);
        assertThat("Thread has not gone past raised barrier", waitsOnBarrier.state, equalTo(WaitsOnBarrier.State.BeforeBarrier));

        barrier.lower();
        try
        {
            result.get(1, TimeUnit.SECONDS);
        }
        catch (TimeoutException e)
        {
            fail("Thread did not continue when barrier was lowered");
        }
        assertThat("Thread has gone past lowered barrier", waitsOnBarrier.state, equalTo(WaitsOnBarrier.State.AfterBarrier));
    }

    /**
     * Block a thread with barrier.raise(), then step through a loop one iteration at a time using barrier.lowerThenRaise().
     */
    @Test
    public void testStepThroughLoop() throws ExecutionException, InterruptedException
    {
        final BarrierImpl barrier = new BarrierImpl("testBarrier");
        barrier.raise();
        assertThat(barrier.counter.wouldBlock(), is(true));

        WaitsOnBarrierInLoop waitsOnBarrierInLoop = new WaitsOnBarrierInLoop(barrier);
        final Future result = executorService.submit(waitsOnBarrierInLoop);
        waitForThreadsToArriveAtBarrier(1, barrier);

        assertThat("Loop iteration count starts at zero", waitsOnBarrierInLoop.iterationCount, equalTo(0));

        barrier.lowerThenRaise();
        // The thread will progress through the loop then arrive back at the raised barrier
        waitForThreadsToArriveAtBarrier(1, barrier);
        assertThat("Thread stepped through one iteration", waitsOnBarrierInLoop.iterationCount, equalTo(1));

        barrier.lowerThenRaise();
        waitForThreadsToArriveAtBarrier(1, barrier);
        assertThat("Thread stepped through a second iteration", waitsOnBarrierInLoop.iterationCount, equalTo(2));

        waitsOnBarrierInLoop.finish = true;
        barrier.lower();
        try
        {
            result.get(1, TimeUnit.SECONDS);
        }
        catch (TimeoutException e)
        {
            result.cancel(true);
            fail("Thread did not continue when barrier was lowered");
        }
    }

    /**
     * Wait for at least the specified number of threads to be waiting at the barrier.
     *
     * @param numThreads number of threads
     * @param barrier the barrier
     */
    private void waitForThreadsToArriveAtBarrier(final int numThreads, final BarrierImpl barrier)
            throws ExecutionException, InterruptedException
    {
        WaitsForThreadsToArrive waitsForThreadsToArrive = new WaitsForThreadsToArrive(barrier, numThreads);
        Future result = executorService.submit(waitsForThreadsToArrive);
        try
        {
            // If they never arrive, timeout quickly
            result.get(1, TimeUnit.SECONDS);
        }
        catch (TimeoutException e)
        {
            result.cancel(true);
        }
        assertThat(numThreads + " threads arrived at the barrier", waitsForThreadsToArrive.numArrived, equalTo(numThreads));
    }

    /**
     * This task waits on a barrier
     */
    private static class WaitsOnBarrier implements Runnable
    {
        /** States */
        private enum State
        {
            BeforeBarrier, AfterBarrier;
        }

        /** The state of this task */
        private State state = State.BeforeBarrier;

        /** The barrier */
        private final BarrierImpl barrier;

        /**
         * @param barrier the barrier
         */
        public WaitsOnBarrier(final BarrierImpl barrier) {this.barrier = barrier;}

        @Override
        public void run()
        {
            barrier.await();
            state = State.AfterBarrier;
        }
    }

    /**
     * This task waits on a barrier inside a loop
     */
    private static class WaitsOnBarrierInLoop implements Runnable
    {
        /** The barrier */
        private final BarrierImpl barrier;

        /** The loop iteration count */
        private int iterationCount;

        /** Flag to exit loop */
        private boolean finish;

        /**
         * @param barrier the barrier
         */
        public WaitsOnBarrierInLoop(final BarrierImpl barrier) {this.barrier = barrier;}

        @Override
        public void run()
        {
            while (!finish)
            {
                barrier.await();
                iterationCount++;
            }
        }
    }

    /**
     * This task waits for a given number of threads to arrive at a barrier
     */
    private static class WaitsForThreadsToArrive implements Runnable
    {
        private final BarrierImpl barrier;

        /** The number of threads we expect to arrive at the barrier */
        private final int numThreadsExpected;

        /** The number of threads that have arrived at the barrier */
        private int numArrived;

        /**
         * @param barrier the barrier
         * @param numThreadsExpected the number of threads to wait for
         */
        public WaitsForThreadsToArrive(final BarrierImpl barrier, final int numThreadsExpected)
        {
            this.barrier = barrier;
            this.numThreadsExpected = numThreadsExpected;
        }

        @Override
        public void run()
        {
            numArrived = barrier.counter.getWaitingThreadCount();
            while (numArrived < numThreadsExpected)
            {
                numArrived = barrier.counter.getWaitingThreadCount();
            }
        }
    }
}
