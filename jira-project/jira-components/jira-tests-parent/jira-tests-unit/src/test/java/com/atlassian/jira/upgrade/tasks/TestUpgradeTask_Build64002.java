package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.util.collect.MapBuilder;
import org.junit.Before;
import org.junit.Test;
import org.ofbiz.core.entity.GenericValue;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;

public class TestUpgradeTask_Build64002
{
    private MockOfBizDelegator ofBizDelegator;

    @Before
    public void setUp() throws Exception
    {
        final GenericValue gadget1 = new MockGenericValue("PortletConfiguration", MapBuilder.newBuilder().
                add("id", 10000L).
                add("portalpage", 10010L).
                add("portletId", null).
                add("columnNumber", 0).
                add("position", 0).
                add("gadgetXml", "local/filter-results.xml").
                add("color", "color0").toMap());
        final GenericValue gadget2 = new MockGenericValue("PortletConfiguration", MapBuilder.newBuilder().
                add("id", 10011L).
                add("portalpage", 10010L).
                add("portletId", null).
                add("columnNumber", 0).
                add("position", 2).
                add("gadgetXml", "local/piechart.xml").
                add("color", "color0").toMap());
        final GenericValue newsGadget = new MockGenericValue("PortletConfiguration", MapBuilder.newBuilder().
                add("id", 10013L).
                add("portalpage", 10010L).
                add("portletId", null).
                add("columnNumber", 0).
                add("position", 1).
                add("gadgetXml", "http://www.atlassian.com/gadgets/news.xml").
                add("color", "color0").toMap());

        ofBizDelegator = new MockOfBizDelegator(newArrayList(gadget1, newsGadget, gadget2), newArrayList(gadget1, gadget2));
    }

    @Test
    public void testUpgradeRemovesNewsGadgetOnly() throws Exception
    {
        final UpgradeTask_Build64002 upgradeTask = new UpgradeTask_Build64002(ofBizDelegator);

        assertEquals("64002", upgradeTask.getBuildNumber());
        assertEquals("Removing the JIRA News gadget.", upgradeTask.getShortDescription());

        upgradeTask.doUpgrade(false);

        ofBizDelegator.verify();
    }
}