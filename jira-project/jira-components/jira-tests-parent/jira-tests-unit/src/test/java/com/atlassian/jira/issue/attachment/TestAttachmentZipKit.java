package com.atlassian.jira.issue.attachment;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import com.atlassian.jira.util.IOUtil;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Responsible for holding unit tests for the {@link com.atlassian.jira.issue.attachment.AttachmentZipKit} class.
 */
public class TestAttachmentZipKit
{
    private static final File validZip = new File(TestAttachmentZipKit.class.getResource("/com/atlassian/jira/issue/attachment/valid.zip").getFile());
    private static final File invalidZip = new File(TestAttachmentZipKit.class.getResource("/com/atlassian/jira/issue/attachment/invalid.zip").getFile());
    private static final File emptyZip = new File(TestAttachmentZipKit.class.getResource("/com/atlassian/jira/issue/attachment/empty.zip").getFile());
    private static final String USER_LOCALES = "en_AU";

    private final AttachmentZipKit kit = new AttachmentZipKit();

    @Test
    public void testIsZipDetectsInvalidZipFiles()
    {
        assertFalse(kit.isZip(invalidZip));
    }

    @Test
    public void testIsZipReturnsFalseForANullFile()
    {
        assertFalse(kit.isZip(null));
    }

    @Test
    public void testIsZipReturnsFalseForAnEmptyZipFile()
    {
        assertFalse(kit.isZip(emptyZip));
    }

    @Test
    public void testIsZipReturnsTrueForAValidZipFile()
    {
        assertTrue(kit.isZip(validZip));
    }

    @Test(expected = IOException.class)
    public void testExtractInvalidFile() throws IOException
    {
        kit.extractFile(invalidZip, 2);
    }

    @Test
    public void testExtractFile() throws IOException
    {
        // temp.c
        assertZipEntry("\n", 0);

        // .config/user-dirs.locale
        assertZipEntry(USER_LOCALES, 26);
    }

    @Test
    public void testInvalidIndexes() throws IOException
    {
        InputStream in = kit.extractFile(validZip, 99);
        assertNull(in);

        in = kit.extractFile(validZip, -1);
        assertNull(in);
    }

    private void assertZipEntry(final String content, final int entryIndex) throws IOException
    {
        InputStream in = null;
        try
        {
            in = kit.extractFile(validZip, entryIndex);

            final StringWriter sw = new StringWriter();
            IOUtil.copy(in, sw);

            assertEquals(content, sw.toString());
        }
        finally
        {
            IOUtil.shutdownStream(in);
        }
    }
}
