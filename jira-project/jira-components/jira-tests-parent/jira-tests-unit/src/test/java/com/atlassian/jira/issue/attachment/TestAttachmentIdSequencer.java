package com.atlassian.jira.issue.attachment;

import com.atlassian.jira.junit.rules.InitMockitoMocks;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.ofbiz.core.entity.DelegatorInterface;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class TestAttachmentIdSequencer
{
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private DelegatorInterface delegatorInterface;

    @InjectMocks
    private AttachmentIdSequencer attachmentIdSequencer;

    @Test
    public void returnsIdProvidedByDelegatorInterface() throws Exception
    {
        final Long expectedId = 312l;
        when(delegatorInterface.getNextSeqId(AttachmentConstants.ATTACHMENT_ENTITY_NAME))
                .thenReturn(expectedId);

        final Long result = attachmentIdSequencer.getNextId();

        assertThat(result, equalTo(expectedId));
    }
}