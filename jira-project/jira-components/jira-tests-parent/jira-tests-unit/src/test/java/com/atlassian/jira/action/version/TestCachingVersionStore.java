/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */
package com.atlassian.jira.action.version;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.project.version.CachingVersionStore;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionImpl;
import com.atlassian.jira.project.version.VersionStore;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.hamcrest.collection.IsIterableContainingInOrder;
import org.hamcrest.collection.IsIterableWithSize;
import org.junit.Before;
import org.junit.Test;
import org.ofbiz.core.entity.GenericValue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@SuppressWarnings ({ "deprecation" })
public class TestCachingVersionStore
{
    private MemoryVersionStore underlyingStore;
    private CachingVersionStore cachingVersionStore;

    @Before
    public void setUp() throws Exception
    {
        underlyingStore = new MemoryVersionStore();
        cachingVersionStore = new CachingVersionStore(underlyingStore, new MemoryCacheManager());
    }

    @Test
    public void testGetVersion() throws Exception
    {
        Version version1 = new MockVersion(1, "v1.0");
        Version version2 = new MockVersion(2, "v1.5");
        Version version3 = new MockVersion(3, "v2.0");

        cachingVersionStore.storeVersion(version1);
        cachingVersionStore.storeVersion(version2);

        assertThat(cachingVersionStore.getAllVersions(), IsIterableWithSize.<Version>iterableWithSize(2));
        assertEquals(version1, Iterables.get(cachingVersionStore.getAllVersions(), 0));
        assertEquals(version1, cachingVersionStore.getVersion(new Long(1)));
        assertEquals(version2, Iterables.get(cachingVersionStore.getAllVersions(), 1));
        assertEquals(version2, cachingVersionStore.getVersion(new Long(2)));

        cachingVersionStore.storeVersion(version3);

        assertThat(cachingVersionStore.getAllVersions(), IsIterableWithSize.<Version>iterableWithSize(3));
        assertEquals(version1, Iterables.get(cachingVersionStore.getAllVersions(), 0));
        assertEquals(version1, cachingVersionStore.getVersion(new Long(1)));
        assertEquals(version2, Iterables.get(cachingVersionStore.getAllVersions(), 1));
        assertEquals(version2, cachingVersionStore.getVersion(new Long(2)));
        assertEquals(version3, Iterables.get(cachingVersionStore.getAllVersions(), 2));
        assertEquals(version3, cachingVersionStore.getVersion(new Long(3)));
    }

    @Test
    public void testCreateAndDeleteModifiesUnderlyingVersionCorrectly()
    {
        FieldMap versionParams = FieldMap.build("name", "version1", "id", (long) 1);
        FieldMap versionParams2 = FieldMap.build("name", "version2", "id", (long) 2);
        Version version = new VersionImpl(null, new MockGenericValue("Version", versionParams));
        Version version2 = new VersionImpl(null, new MockGenericValue("Version", versionParams2));

        cachingVersionStore.createVersion(versionParams);
        cachingVersionStore.createVersion(versionParams2);
        //assert created
        assertThat(underlyingStore.getAllVersions(), IsIterableWithSize.<Version>iterableWithSize(2));
        assertThat(underlyingStore.getAllVersions(), IsIterableContainingInOrder.contains(version, version2));

        assertEquals(underlyingStore.getAllVersions(), cachingVersionStore.getAllVersions());

        assertGetVersion(version, (long) 1);
        assertGetVersion(version2, (long) 2);
        assertGetVersion(null, (long) 3);

        cachingVersionStore.deleteVersion(version);
        cachingVersionStore.deleteVersion(version2);

        assertThat(underlyingStore.getAllVersions(), IsIterableWithSize.<Version>iterableWithSize(0));
        assertEquals(underlyingStore.getAllVersions(), cachingVersionStore.getAllVersions());
        assertGetVersion(null, (long) 1);
        assertGetVersion(null, (long) 2);

    }

    private void assertGetVersion(Version version, Long id)
    {
        assertEquals(version, underlyingStore.getVersion(id));
        assertEquals(version, cachingVersionStore.getVersion(id));
    }

    private static class MemoryVersionStore implements VersionStore
    {

        private List<Version> versions = Lists.newArrayList();

        @Nonnull
        public Iterable<Version> getAllVersions()
        {
            List<Version> versionGVs = Lists.newArrayListWithCapacity(versions.size());
            for (final Version version1 : versions)
            {
                versionGVs.add(version1);
            }
            return versionGVs;
        }

        @Nonnull
        public Version createVersion(Map versionParams)
        {
            GenericValue versionGV = new MockGenericValue("Version", versionParams);
            final VersionImpl version = new VersionImpl(null, versionGV);
            versions.add(version);
            return version;
        }

        public void storeVersion(Version version)
        {
            versions.remove(version);
            versions.add(version);
        }

        public void storeVersions(final Collection<Version> versions)
        {
            for (Version version : versions)
            {
                if (version != null)
                {
                    storeVersion(version);
                }
            }
        }

        public Version getVersion(Long id)
        {
            for (final Version version : versions)
            {
                if (id.equals(version.getId()))
                {
                    return version;
                }
            }
            return null;
        }

        public void deleteVersion(Version version)
        {
            versions.remove(version);
        }

        @Nonnull
        @Override
        public Iterable<Version> getVersionsByName(String name)
        {
            throw new UnsupportedOperationException();
        }

        @Nonnull
        @Override
        public Iterable<Version> getVersionsByProject(Long projectId)
        {
            throw new UnsupportedOperationException();
        }
    }
}
