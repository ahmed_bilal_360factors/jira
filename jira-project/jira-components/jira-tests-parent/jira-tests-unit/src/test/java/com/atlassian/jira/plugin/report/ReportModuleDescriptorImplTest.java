package com.atlassian.jira.plugin.report;


import java.text.MessageFormat;
import java.util.Map;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.plugin.webfragment.descriptors.ConditionDescriptorFactory;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.Condition;
import com.atlassian.query.Query;

import com.google.common.collect.ImmutableMap;

import org.dom4j.Element;
import org.dom4j.dom.DOMElement;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ReportModuleDescriptorImplTest
{

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Rule
    public final MockitoContainer mocks = MockitoMocksInContainer.rule(this);
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private ConditionDescriptorFactory conditionDescFactory;
    @Mock
    private ModuleFactory moduleFactory;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private VelocityRequestContextFactory velocityRequestContextFactory;
    @AvailableInContainer
    @Mock
    private SearchService searchService;
    @Mock
    private Plugin plugin;
    private final Element element = new DOMElement("plugin");
    private final String moduleKey = "module-key";
    private final String pluginKey = "plugin-key";
    private final MockProject project = new MockProject(123123L, "PK", "pr-name");
    private final MockUser user = new MockUser("uname");

    private ReportModuleDescriptorImpl descriptor;

    @Before
    public void setUp() throws Exception
    {
        UrlProviderImpl.reset();
        element.addAttribute("key", moduleKey);
        element.addAttribute("class", DummyReport.class.getCanonicalName());
        when(plugin.getKey()).thenReturn(pluginKey);
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(user);
    }

    @Test
    public void shouldIncludeUrlFromParameterInGetUrl() throws Exception
    {
        final String someFancyUrl = "someFancyUrl";
        element.addElement("url").setText(someFancyUrl);

        initDescriptor();

        final String url = descriptor.getUrl(project);
        final String expectedUrl = MessageFormat.format(ReportModuleDescriptorImpl.PARAMS_PATTERN, someFancyUrl, project.getId(), project.getName(), pluginKey + ":" + moduleKey);
        assertThat(url, CoreMatchers.equalTo(expectedUrl));
        assertThat(url, CoreMatchers.containsString(String.valueOf(project.getId())));
        assertThat(url, CoreMatchers.containsString("project-" + String.valueOf(project.getId())));
    }

    @SuppressWarnings ("unchecked")
    @Test
    public void shouldThrowExceptionWhenProviderClassDoesNotImplementUrlProviderInterface()
            throws ClassNotFoundException
    {
        thrown.expect(PluginParseException.class);
        thrown.expectMessage("Provided class com.atlassian.report.CustomUrlProvider does not implement ReportUrlProvider interface");

        element.addElement("url").addAttribute("provider", "com.atlassian.report.CustomUrlProvider");
        when(plugin.loadClass("com.atlassian.report.CustomUrlProvider", ReportModuleDescriptorImpl.class)).thenReturn((Class<Object>) this.getClass().asSubclass(Object.class));

        initDescriptor();
    }

    @SuppressWarnings ("unchecked")
    @Test
    public void shouldThrowExceptionWhenProviderClassIsNotFound()
            throws ClassNotFoundException
    {
        thrown.expect(PluginParseException.class);
        thrown.expectMessage("Cannot load url provider class: com.atlassian.report.CustomUrlProvider");

        element.addElement("url").addAttribute("provider", "com.atlassian.report.CustomUrlProvider");
        when(plugin.loadClass("com.atlassian.report.CustomUrlProvider", ReportModuleDescriptorImpl.class)).thenThrow(ClassNotFoundException.class);

        initDescriptor();
    }

    @Test
    public void shouldIgnoreUrlValueWhenProviderIsSupplied() throws Exception
    {
        final String configureURL = "someFancyUrl";

        setUpUrlProvider();
        element.element("url").setText(configureURL);

        initDescriptor();

        String url = descriptor.getUrl(project);
        assertThat(url, equalTo(UrlProviderImpl.returnValue.get()));
        assertThat(url, not(containsString(configureURL)));
    }

    @Test
    public void shouldProvideIssuesUrlWhenProviderReturnsNone() throws Exception
    {
        setUpUrlProvider();
        UrlProviderImpl.returnValue = Option.none();

        initDescriptor();

        when(searchService.getQueryString(Mockito.eq(user), Mockito.any(Query.class))).thenReturn("issues-jql-query");

        assertThat(descriptor.getUrl(project), containsString("issues-jql-query"));

        Map<String, Object> context = ImmutableMap.of();
        assertThat(descriptor.getUrl(context), equalTo(Option.<String>none()));
        assertThat(UrlProviderImpl.contextArg, Matchers.sameInstance(context));
    }

    @Test
    public void shouldCreateContextWithProjectWhenOldApiMethodIsUsed() throws Exception
    {
        setUpUrlProvider();

        initDescriptor();

        descriptor.getUrl(project);

        assertThat(UrlProviderImpl.contextArg, Matchers.<String, Object>hasEntry("project", project));
        assertThat(UrlProviderImpl.descriptorArg, Matchers.<ReportModuleDescriptor>sameInstance(descriptor));
    }

    @Test
    public void shouldUseDefaultAttributesWhenNotProvided() throws Exception
    {
        initDescriptor();

        final String url = descriptor.getUrl(project);

        final String expectedUrl = MessageFormat.format(ReportModuleDescriptorImpl.PARAMS_PATTERN, "/secure/ConfigureReport!default.jspa", project.getId(), project.getName(), pluginKey + ":" + moduleKey);
        assertThat(url, CoreMatchers.equalTo(expectedUrl));
        assertThat(url, CoreMatchers.containsString(String.valueOf(project.getId())));
        assertThat(descriptor.getCategory(), Matchers.<ReportCategory>equalTo(ReportCategoryImpl.OTHER));
        assertThat(descriptor.getWeight(), equalTo(ReportModuleDescriptorImpl.DEFAULT_WEIGHT));
        assertThat(descriptor.getThumbnailCssClass(), equalTo(ReportModuleDescriptorImpl.DEFAULT_THUMBNAIL_CSS_CLASS));
    }

    @Test
    public void shouldThrowExceptionWhenCategoryValueIsUnknown() throws Exception
    {
        thrown.expect(PluginParseException.class);
        thrown.expectMessage("The report module: plugin-key:module-key specified a category key that is not a valid category: unknown.category");

        element.addElement("category").addAttribute("key", "unknown.category");
        initDescriptor();
    }

    @Test
    public void shouldUseUserProvidedCategory() throws Exception
    {
        element.addElement("category").addAttribute("key", "forecast.management");
        initDescriptor();

        assertThat(descriptor.getCategory(), Matchers.<ReportCategory>equalTo(ReportCategoryImpl.FORECAST_MANAGEMENT));
    }

    @Test
    public void shouldThrowExceptionWhenWeightCannotBeParsedToInteger() throws Exception
    {
        thrown.expect(PluginParseException.class);
        thrown.expectMessage("The report module: plugin-key:module-key specified a weight attribute that is not an integer");

        element.addAttribute("weight", "34.12");
        initDescriptor();
    }

    @Test
    public void shouldUseUserProvidedWeight()
    {
        element.addAttribute("weight", " \t34");
        initDescriptor();

        assertThat(descriptor.getWeight(), equalTo(34));
    }

    @Test
    public void shouldUseDefaultClassWhenThumbnailElementProvidedButClassIsEmpty()
    {
        element.addElement("thumbnail").addAttribute("cssClass", "   ");

        initDescriptor();

        assertThat(descriptor.getThumbnailCssClass(), equalTo(ReportModuleDescriptorImpl.DEFAULT_THUMBNAIL_CSS_CLASS));
    }

    @Test
    public void shouldUseUserProvidedClass()
    {
        element.addElement("thumbnail").addAttribute("cssClass", "my-awesome-class");

        initDescriptor();

        assertThat(descriptor.getThumbnailCssClass(), equalTo("my-awesome-class"));
    }
    
    @Test
    public void shouldCorrectlyExtractTheCondition()
    {
        Condition condition = mock(Condition.class);
        when(conditionDescFactory.retrieveCondition(plugin, element)).thenReturn(condition);

        initDescriptor();

        assertThat(descriptor.getCondition(), is(condition));
    }

    private void initDescriptor()
    {
        descriptor = new ReportModuleDescriptorImpl(jiraAuthenticationContext, moduleFactory, conditionDescFactory);
        descriptor.init(plugin, element);
        descriptor.enabled();
    }

    @SuppressWarnings ("unchecked")
    private void setUpUrlProvider() throws Exception
    {
        element.addElement("url").addAttribute("provider", "com.atlassian.report.CustomUrlProvider");
        when(plugin.loadClass("com.atlassian.report.CustomUrlProvider", ReportModuleDescriptorImpl.class)).thenReturn((Class<Object>) UrlProviderImpl.class.asSubclass(Object.class));
    }


    public static class UrlProviderImpl implements ReportUrlProvider
    {
        private final static String DEFAULT_URL = "/some/custom/url";
        public static Option<String> returnValue = Option.some(DEFAULT_URL);
        public static ReportModuleDescriptor descriptorArg;
        public static Map<String, Object> contextArg;

        @Override
        public Option<String> getUrl(final ReportModuleDescriptor reportModule, final Map<String, Object> context)
        {
            descriptorArg = reportModule;
            contextArg = context;
            return returnValue;
        }

        public static void reset()
        {
            returnValue = Option.some(DEFAULT_URL);
            descriptorArg = null;
            contextArg = null;
        }
    }

    public static class DummyReport implements Report
    {
        @Override
        public void init(final ReportModuleDescriptor reportModuleDescriptor)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void validate(final ProjectActionSupport action, final Map params)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String generateReportHtml(final ProjectActionSupport action, final Map params) throws Exception
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public boolean isExcelViewSupported()
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String generateReportExcel(final ProjectActionSupport action, final Map params) throws Exception
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public boolean showReport()
        {
            throw new UnsupportedOperationException("Not implemented");
        }
    }
}
