package com.atlassian.jira.bc.workflow;

import com.atlassian.jira.workflow.function.issue.UpdateIssueFieldFunction;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.atlassian.jira.bc.workflow.DefaultWorkflowFunctionDescriptorFactory.CLASS_NAME_KEY;
import static com.atlassian.jira.bc.workflow.DefaultWorkflowFunctionDescriptorFactory.FIELD_NAME_KEY;
import static com.atlassian.jira.bc.workflow.DefaultWorkflowFunctionDescriptorFactory.FIELD_VALUE_KEY;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("unchecked")
public class TestDefaultWorkflowFunctionDescriptorFactory
{
    private DefaultWorkflowFunctionDescriptorFactory workflowFunctionDescriptorFactory =
            new DefaultWorkflowFunctionDescriptorFactory();

    @Test
    public void testCreateUpdateIssueFieldFunctionWithValidParams()
    {
        String fieldName = "myfield";
        String fieldValue = "extractors are awesome";

        FunctionDescriptor descriptor = workflowFunctionDescriptorFactory.updateIssueField(fieldName, fieldValue);
        Map<String, String> args = descriptor.getArgs();
        assertThat(descriptor.getType(), equalTo("class"));
        assertThat(args, hasEntry(FIELD_NAME_KEY, fieldName));
        assertThat(args, hasEntry(FIELD_VALUE_KEY, fieldValue));
        assertThat(args, hasEntry(CLASS_NAME_KEY, UpdateIssueFieldFunction.class.getName()));
    }
}
