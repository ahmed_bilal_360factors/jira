package com.atlassian.jira.plugin.attachment;

import java.io.File;
import java.util.List;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestZipAttachmentProcessor
{
    private static final File validZip = new File(TestZipAttachmentProcessor.class.getResource("/com/atlassian/jira/issue/attachment/valid.zip").getFile());
    private static final File validOsxZip = new File(TestZipAttachmentProcessor.class.getResource("/com/atlassian/jira/issue/attachment/valid_osx.zip").getFile());
    private static final File invalidZip = new File(TestZipAttachmentProcessor.class.getResource("/com/atlassian/jira/issue/attachment/invalid.zip").getFile());
    private static final File emptyZip = new File(TestZipAttachmentProcessor.class.getResource("/com/atlassian/jira/issue/attachment/empty.zip").getFile());

    private final ZipAttachmentProcessor processor = new ZipAttachmentProcessor();

    @Test
    public void testProcessAttachmentInvalidZip() throws Exception
    {
        final List<AttachmentArchiveEntry> entries =
                processor.processAttachment(invalidZip);

        assertEquals(0, entries.size());
    }

    @Test
    public void testProcessAttachmentEmptyZip() throws Exception
    {
        final List<AttachmentArchiveEntry> entries =
                processor.processAttachment(emptyZip);

        assertEquals(0, entries.size());
    }

    @Test
    public void testProcessAttachmentValidZip() throws Exception
    {
        final List<AttachmentArchiveEntry> entries =
                processor.processAttachment(validZip);

        final AttachmentArchiveEntry entry1 = entries.get(0);
        assertEquals(entry1.getName(), "temp.c");
        assertEquals(entry1.getSize(), 1);
        assertEquals(entry1.getEntryIndex(), 0);

        final AttachmentArchiveEntry entry2 = entries.get(1);
        assertEquals(entry2.getName(), ".config/compiz/compizconfig/config");
        assertEquals(entry2.getSize(), 56);
        assertEquals(entry2.getEntryIndex(), 1);

        final AttachmentArchiveEntry last = entries.get(28);
        assertEquals(last.getName(), ".config/Trolltech.conf");
        assertEquals(last.getSize(), 7688);
        assertEquals(last.getEntryIndex(), 28);

        assertEquals(29, entries.size());
    }

    @Test
    public void testProcessAttachmentValidOsxZip() throws Exception
    {
        final List<AttachmentArchiveEntry> entries =
                processor.processAttachment(validOsxZip);

        final AttachmentArchiveEntry entry1 = entries.get(0);
        assertEquals(entry1.getName(), "config-autostart-gnome-at-session.desktop");
        assertEquals(entry1.getSize(), 3993);
        assertEquals(entry1.getEntryIndex(), 0);

        final AttachmentArchiveEntry entry2 = entries.get(1);
        assertEquals(entry2.getName(), "__MACOSX/._config-autostart-gnome-at-session.desktop");
        assertEquals(entry2.getSize(), 476);
        assertEquals(entry2.getEntryIndex(), 2);

        assertEquals(2, entries.size());
    }
}