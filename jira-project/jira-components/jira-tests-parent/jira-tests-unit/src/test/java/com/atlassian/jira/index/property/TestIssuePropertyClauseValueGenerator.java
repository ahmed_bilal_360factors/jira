package com.atlassian.jira.index.property;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.atlassian.crowd.model.user.User;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.jql.values.ClauseValuesGenerator;

import com.google.common.collect.Lists;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermEnum;
import org.apache.lucene.search.IndexSearcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.jql.values.ClauseValuesGenerator.Result;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestIssuePropertyClauseValueGenerator
{
    public static final String LUCENE_FIELD_NAME = "field";
    @Mock
    private IssueIndexManager indexManager;
    @Mock
    private IndexSearcher indexSearcher;
    @Mock
    private IndexReader indexReader;

    private IssuePropertyClauseValueGenerator generator;

    @Before
    public void setUp()
    {
        when(indexManager.getIssueSearcher()).thenReturn(indexSearcher);
        when(indexSearcher.getIndexReader()).thenReturn(indexReader);
        this.generator = new IssuePropertyClauseValueGenerator(LUCENE_FIELD_NAME, indexManager);
    }

    @Test
    public void testSuggestedResultsDontExceedMaxValue() throws Exception
    {
        final List<Term> terms = Lists.newArrayList(term("abcd"), term("abcde"), term("abcdef"), term("abcdefg"));
        when(indexReader.terms(any(Term.class))).thenReturn(new TestTermEnum(terms.iterator()));
        final int maxNumResults = 2;
        final ClauseValuesGenerator.Results values = generator.getPossibleValues(mock(User.class), "field", "a", maxNumResults);

        assertThat(values.getResults(), hasSize(lessThanOrEqualTo(maxNumResults)));
    }

    @Test
    public void testSuggestedResultsDontReturnDuplicates() throws Exception
    {
        final ArrayList<Term> terms = Lists.newArrayList(term("abcd"), term("abcd"), term("abcd"));
        when(indexReader.terms(any(Term.class))).thenReturn(new TestTermEnum(terms.iterator()));
        final int maxNumResults = 3;
        final ClauseValuesGenerator.Results values = generator.getPossibleValues(mock(User.class), "field", "a", maxNumResults);

        assertThat(values.getResults(), hasSize(equalTo(1)));
        assertThat(values.getResults(), hasItem(new Result("abcd")));
    }

    @Test
    public void testSuggestedResultsFilteredWithPrefix() throws Exception
    {
        final ArrayList<Term> terms = Lists.newArrayList(term("abcd"), term("www"), term("xyz"));
        when(indexReader.terms(any(Term.class))).thenReturn(new TestTermEnum(terms.iterator()));
        final int maxNumResults = 3;
        final ClauseValuesGenerator.Results values = generator.getPossibleValues(mock(User.class), "field", "a", maxNumResults);

        assertThat(values.getResults(), hasSize(equalTo(1)));
        assertThat(values.getResults(), hasItem(new Result("abcd")));
    }

    private Term term(final String value)
    {
        return new Term(LUCENE_FIELD_NAME, value);
    }

    private static class TestTermEnum extends TermEnum
    {
        private final Iterator<Term> terms;

        TestTermEnum(final Iterator<Term> terms) {this.terms = terms;}

        @Override
        public boolean next() throws IOException
        {
            return terms.hasNext();
        }

        @Override
        public Term term()
        {
            return terms.next();
        }

        @Override
        public int docFreq()
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void close()
        {
        }
    }
}