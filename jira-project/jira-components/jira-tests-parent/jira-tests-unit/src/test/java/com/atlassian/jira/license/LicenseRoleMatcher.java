package com.atlassian.jira.license;

import com.atlassian.fugue.Option;
import com.google.common.base.Objects;
import com.google.common.collect.Sets;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.util.Set;

import static com.atlassian.fugue.Option.none;
import static java.util.Arrays.asList;

public class LicenseRoleMatcher extends TypeSafeDiagnosingMatcher<LicenseRole>
{
    private String name;
    private Set<String> groups = Sets.newHashSet();
    private Option<String> primaryGroup = none();
    private LicenseRoleId id;

    public LicenseRoleMatcher()
    {
    }

    public LicenseRoleMatcher merge(LicenseRole role)
    {
        id(role.getId());
        name(role.getName());
        groups(role.getGroups());
        primaryGroup(role.getPrimaryGroup());

        return this;
    }

    public LicenseRoleMatcher merge(LicenseRoleDefinition definition)
    {
        id(definition.getLicenseRoleId());
        name(definition.getName());

        return this;
    }

    public LicenseRoleMatcher id(String id)
    {
        this.id = new LicenseRoleId(id);
        return this;
    }

    public LicenseRoleMatcher id(LicenseRoleId id)
    {
        this.id = id;
        return this;
    }

    public LicenseRoleMatcher name(String name)
    {
        this.name = name;
        return this;
    }

    public LicenseRoleMatcher groups(String...groups)
    {
        this.groups(asList(groups));
        return this;
    }

    public LicenseRoleMatcher groups(Iterable<String> groups)
    {
        this.groups = Sets.newHashSet(groups);
        return this;
    }

    public LicenseRoleMatcher primaryGroup(String primaryGroup)
    {
        this.primaryGroup = Option.option(primaryGroup);
        return this;
    }

    public LicenseRoleMatcher primaryGroup(Option<String> primaryGroup)
    {
        this.primaryGroup = primaryGroup;
        return this;
    }

    @Override
    protected boolean matchesSafely(final LicenseRole item, final Description mismatchDescription)
    {
        if (Objects.equal(name, item.getName())
                && Objects.equal(groups, item.getGroups())
                && Objects.equal(id, item.getId())
                && Objects.equal(primaryGroup, item.getPrimaryGroup()))
        {
            return true;
        }
        else
        {
            mismatchDescription.appendValue(String.format("[name: %s, groups: %s, id: %s, primary: %s]",
                    item.getName(), item.getGroups(), item.getId(), item.getPrimaryGroup()));

            return false;
        }
    }

    @Override
    public void describeTo(final Description description)
    {
        description.appendText(String.format("[name: %s, groups: %s, id: %s, primary: %s]",
                name, groups, id, primaryGroup));
    }
}
