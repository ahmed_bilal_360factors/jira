package com.atlassian.jira.issue.attachment.store.strategy.move;

import com.atlassian.jira.issue.attachment.FileSystemAttachmentStore;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class TestMoveTemporaryStrategiesFactory
{

    private final SendToStoreFunctionFactory sendToStoreFunctionFactory = mock(SendToStoreFunctionFactory.class);
    private final MoveTemporaryStrategiesFactory strategiesFactory = new MoveTemporaryStrategiesFactory(sendToStoreFunctionFactory);

    @Test
    public void testCreateMoveOnlyPrimaryStrategy() throws Exception
    {
        final StreamAttachmentStore primaryStore = mock(StreamAttachmentStore.class);

        final SingleStoreMoveStrategy singleStoreMoveStrategy = strategiesFactory.createMoveOnlyPrimaryStrategy(primaryStore);

        final SingleStoreMoveStrategy expectedStrategy = new SingleStoreMoveStrategy(primaryStore);

        assertThat(singleStoreMoveStrategy, equalTo(expectedStrategy));
    }

    @Test
    public void testCreateBackgroundResendingMoveStrategy() throws Exception
    {

        final FileSystemAttachmentStore fileStore = mock(FileSystemAttachmentStore.class);
        final StreamAttachmentStore secondaryStore = mock(StreamAttachmentStore.class);

        final BackgroundResendingMoveStrategy backgroundResendingStrategy = strategiesFactory.createBackgroundResendingStrategy(
                fileStore, secondaryStore);

        final BackgroundResendingMoveStrategy expectedStrategy = new BackgroundResendingMoveStrategy(fileStore, secondaryStore, sendToStoreFunctionFactory);

        assertThat(backgroundResendingStrategy, equalTo(expectedStrategy));
    }
}