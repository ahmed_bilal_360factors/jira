package com.atlassian.jira.issue.util;

import com.atlassian.jira.entity.EntityEngineImpl;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MovedIssueKeyStoreImplTest
{
    @Test
    public void recordMovedIssueKey() throws Exception
    {
        final MockOfBizDelegator mockOfBizDelegator = new MockOfBizDelegator();
        MovedIssueKeyStoreImpl movedIssueKeyStore = new MovedIssueKeyStoreImpl(new EntityEngineImpl(mockOfBizDelegator), mockOfBizDelegator);

        // starts with no mapping
        assertEquals(null, movedIssueKeyStore.getMovedIssueId("ABC-123"));
        // move issue key
        movedIssueKeyStore.recordMovedIssueKey("ABC-123", 45L);
        assertEquals(new Long(45), movedIssueKeyStore.getMovedIssueId("ABC-123"));

        // JRA-42354 we can sometimes get a new issue taking over an old key
        movedIssueKeyStore.recordMovedIssueKey("ABC-123", 120L);
        assertEquals(new Long(120L), movedIssueKeyStore.getMovedIssueId("ABC-123"));
    }
}
