package com.atlassian.jira.issue.attachment.store.strategy.get;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.store.MockAttachmentKeys;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDualAttachmentGetStrategy
{
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private StreamAttachmentStore primaryStore;
    @Mock
    private StreamAttachmentStore secondaryStore;

    private final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();

    private DualAttachmentGetStrategy dualAttachmentGetStrategy;

    @Before
    public void setUp() throws Exception
    {
        dualAttachmentGetStrategy = new DualAttachmentGetStrategy(primaryStore, secondaryStore);
    }

    @Test
    public void shouldExistInPrimary() throws Exception
    {
        when(primaryStore.exists(attachmentKey))
                .thenReturn(Promises.promise(true));

        final Boolean result = dualAttachmentGetStrategy.exists(attachmentKey).claim();

        assertThat(result, equalTo(true));
    }

    @Test
    public void shouldExistIfPresentInSecondaryButNotInPrimary() throws Exception
    {
        when(primaryStore.exists(attachmentKey))
                .thenReturn(Promises.promise(false));

        when(secondaryStore.exists(attachmentKey))
                .thenReturn(Promises.promise(true));

        final Boolean result = dualAttachmentGetStrategy.exists(attachmentKey).claim();

        assertThat(result, equalTo(true));
    }

    @Test
    public void shouldAskSecondaryStoreForExistenceWhenPrimaryThrowsException() throws Exception
    {
        when(primaryStore.exists(attachmentKey))
                .thenReturn(Promises.<Boolean>rejected(new RuntimeException()));

        when(secondaryStore.exists(attachmentKey))
                .thenReturn(Promises.promise(true));

        final Boolean result = dualAttachmentGetStrategy.exists(attachmentKey).claim();

        assertThat(result, equalTo(true));
    }

    @Test
    public void shouldLoadFromPrimaryStoreWhenExistsAndIgnoreAnySecondaryErrors() throws Exception
    {
        //noinspection unchecked
        final Function<AttachmentGetData,String> processor = mock(Function.class);

        final String expectedString = "expectedString";
        when(primaryStore.getAttachmentData(attachmentKey, processor))
                .thenReturn(Promises.promise(expectedString));

        final String result = dualAttachmentGetStrategy.getAttachmentData(attachmentKey, processor).claim();

        assertThat(result, equalTo(expectedString));
    }

    @Test
    public void shouldGenerateLoadOnSecondaryStoreEvenWhenFoundAttachmentInPrimary() throws Exception
    {
        //noinspection unchecked
        final Function<AttachmentGetData,String> processor = mock(Function.class);
        final ByteArrayInputStream secondaryStoreStream = new ByteArrayInputStream("Some mock bytes".getBytes("UTF-8"));

        final String expectedString = "expectedString";
        when(primaryStore.getAttachmentData(attachmentKey, processor))
                .thenReturn(Promises.promise(expectedString));

        //noinspection unchecked
        doAnswer(new Answer()
        {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                final Object[] arguments = invocation.getArguments();
                //noinspection unchecked
                final Function<InputStream, String> processor = (Function<InputStream, String>) arguments[1];
                processor.get(secondaryStoreStream);
                return Promises.promise(expectedString);
            }
        }).when(secondaryStore).getAttachment(eq(attachmentKey), any(Function.class));

        final String result = dualAttachmentGetStrategy.getAttachmentData(attachmentKey, processor).claim();

        assertThat(result, equalTo(expectedString));
        assertThat("Stream provided by secondary store should be read during call and be empty afterwards",
                secondaryStoreStream.available(), equalTo(0));
    }

    @Test
    public void shouldIgnoreErrorsOnSecondaryStoreWhenJustGeneratingFakeLoad() throws Exception
    {
        //noinspection unchecked
        final Function<AttachmentGetData,String> processor = mock(Function.class);

        final String expectedString = "expectedString";
        when(primaryStore.getAttachmentData(attachmentKey, processor))
                .thenReturn(Promises.promise(expectedString));

        //noinspection unchecked
        when(secondaryStore.getAttachment(eq(attachmentKey), any(Function.class)))
                .thenReturn(Promises.<String>rejected(new RuntimeException()));

        final String result = dualAttachmentGetStrategy.getAttachmentData(attachmentKey, processor).claim();

        assertThat(result, equalTo(expectedString));
    }

    @Test
    public void shouldLoadAttachmentFromSecondaryStoreWhenFailedFromPrimary() throws Exception
    {
        //noinspection unchecked
        final Function<AttachmentGetData,String> processor = mock(Function.class);
        final String expectedString = "expectedString";

        when(primaryStore.getAttachmentData(attachmentKey, processor))
                .thenReturn(Promises.<String>rejected(new RuntimeException()));

        when(secondaryStore.getAttachmentData(attachmentKey, processor))
                .thenReturn(Promises.promise(expectedString));

        final String result = dualAttachmentGetStrategy.getAttachmentData(attachmentKey, processor).claim();

        assertThat(result, equalTo(expectedString));
    }

    @Test
    public void shouldPropagateExceptionWhenFailedToLoadFromBothStores() throws Exception
    {
        //noinspection unchecked
        final Function<AttachmentGetData,String> processor = mock(Function.class);

        when(primaryStore.getAttachmentData(attachmentKey, processor))
                .thenReturn(Promises.<String>rejected(new RuntimeException()));

        when(secondaryStore.getAttachmentData(attachmentKey, processor))
                .thenReturn(Promises.<String>rejected(new IOException()));

        final Promise<String> result = dualAttachmentGetStrategy.getAttachmentData(attachmentKey, processor);

        catchException(result).claim();

        assertThat(caughtException().getCause(), instanceOf(IOException.class));
    }
}