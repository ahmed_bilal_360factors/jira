package com.atlassian.jira.dashboard.permission;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.atlassian.fugue.Option;
import com.atlassian.gadgets.GadgetId;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.Vote;
import com.atlassian.gadgets.dashboard.DashboardId;
import com.atlassian.gadgets.dashboard.DashboardState;
import com.atlassian.gadgets.dashboard.Layout;
import com.atlassian.gadgets.dashboard.spi.DashboardPermissionService;
import com.atlassian.gadgets.plugins.GadgetLocationTranslator;
import com.atlassian.gadgets.plugins.PluginGadgetSpec;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.MockGlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;

import com.atlassian.plugin.web.Condition;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.gadgets.Vote.ALLOW;
import static com.atlassian.gadgets.Vote.DENY;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestJiraGadgetPermissionManager
{
    private ApplicationUser admin = new MockApplicationUser("admin");
    private ApplicationUser user = new MockApplicationUser("user");

    @Mock private DashboardPermissionService dashboardPermissionService;
    @Mock private Condition enabledCondition;
    @Mock private Condition localCondition;
    @Mock private ModuleDescriptor moduleDescriptor;
    @Mock private PermissionManager permissionManager;
    @Mock private Plugin plugin;
    @Mock private PluginAccessor pluginAccessor;

    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @AvailableInContainer public JiraAuthenticationContext jiraAuthenticationContext = Mockito.mock(JiraAuthenticationContext.class);
    @AvailableInContainer public GlobalPermissionManager globalPermissionManager = MockGlobalPermissionManager.withSystemGlobalPermissions();
    @AvailableInContainer public GadgetLocationTranslator gadgetLocationTranslator = Mockito.mock(GadgetLocationTranslator.class);

    private JiraGadgetPermissionManager jiraGadgetPermissionManager;
    
    @Before
    public void setUp() 
    {
        when(plugin.getKey()).thenReturn("com.atlassian.jira.gadgets");
        jiraGadgetPermissionManager = new JiraGadgetPermissionManager(permissionManager, pluginAccessor, dashboardPermissionService);
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void testVoteOnNull()
    {
        jiraGadgetPermissionManager.voteOn(null, null);
    }

    @Test
    public void testGetNonPluginGadgetWithNonConformingURL() throws URISyntaxException
    {
        URI uri = new URI("somerandomthing:stuff");
        createGadgetStateAndAddToTranslationService("1", uri.toASCIIString());
        Option<PluginGadgetSpec> gadgetSpec = jiraGadgetPermissionManager.getPluginGadgetSpec(uri);
        assertThat(gadgetSpec.isEmpty(), is(true));
        verify(pluginAccessor, never()).getEnabledPluginModule(anyString());
    }

    @Test
    public void testGetNonPluginGadgetWithConformingURL() throws URISyntaxException
    {
        URI uri = new URI("rest/gadgets/1.0/g/com.atlassian.jira.gadgets:admin-gadget/gadgets/admin-gadget.xml");
        createGadgetStateAndAddToTranslationService("1", uri.toASCIIString());
        Option<PluginGadgetSpec> gadgetSpec = jiraGadgetPermissionManager.getPluginGadgetSpec(uri);
        assertThat(gadgetSpec.isEmpty(), is(true));
        verify(pluginAccessor).getEnabledPluginModule("com.atlassian.jira.gadgets:admin-gadget");
    }

    @Test
    public void testVoteOnLoginGadget()
    {
        PluginGadgetSpec mockSpec = createPluginGadgetSpec("login-gadget", "rest/login.xml");
        Vote vote = jiraGadgetPermissionManager.voteOn(mockSpec, user);
        assertEquals(DENY, vote);

        //login gadget should be shown for logged out user.
        vote = jiraGadgetPermissionManager.voteOn(mockSpec,(ApplicationUser)  null);
        assertEquals(ALLOW, vote);
    }

    @Test
    public void testVoteOnGadgetNoRolesSpecified()
    {
        ModuleDescriptor mockModuleDescriptor2 = mock(ModuleDescriptor.class);
        PluginGadgetSpec mockSpec = createPluginGadgetSpec("intro-gadget", "rest/intro.xml");
        PluginGadgetSpec mockSpec2 = createPluginGadgetSpec("intro-gadget2", "rest/intro.xml", MapBuilder.<String, String>newBuilder().add("roles-required", "").toMap());

        when(moduleDescriptor.getParams()).thenReturn(Collections.<String, String>emptyMap());
        when(mockModuleDescriptor2.getParams()).thenReturn(MapBuilder.<String, String>newBuilder().add("roles-required", "").toMap());
        when(pluginAccessor.getEnabledPluginModule("com.atlassian.jira.gadgets:intro-gadget")).thenReturn(moduleDescriptor);
        when(pluginAccessor.getEnabledPluginModule("com.atlassian.jira.gadgets:intro-gadget2")).thenReturn(mockModuleDescriptor2);

        Vote vote = jiraGadgetPermissionManager.voteOn(mockSpec, user);
        assertEquals(ALLOW, vote);

        vote = jiraGadgetPermissionManager.voteOn(mockSpec2, (ApplicationUser) null);
        assertEquals(ALLOW, vote);
    }

    @Test
    public void testVoteOnGadgetInvalidRolesSpecified()
    {
        PluginGadgetSpec mockSpec = createPluginGadgetSpec("intro-gadget", "rest/intro.xml",
                MapBuilder.<String, String>newBuilder().add("roles-required", "someinvalidmumbojumbo").toMap());
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);

        Vote vote = jiraGadgetPermissionManager.voteOn(mockSpec, user);
        assertEquals(Vote.PASS, vote);
    }

    @Test
    public void testVoteOnGadgetGlobalRolesSpecified()
    {
        PluginGadgetSpec mockSpec = createPluginGadgetSpec("intro-gadget", "rest/intro.xml",
                MapBuilder.<String, String>newBuilder().add("roles-required", "use").toMap());

        when(permissionManager.hasPermission(Permissions.ADMINISTER, admin)).thenReturn(false);
        when(permissionManager.hasPermission(Permissions.USE, admin)).thenReturn(true);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);
        when(permissionManager.hasPermission(Permissions.USE, user)).thenReturn(false);

        Vote vote = jiraGadgetPermissionManager.voteOn(mockSpec, admin);
        assertEquals(ALLOW, vote);

        vote = jiraGadgetPermissionManager.voteOn(mockSpec, user);
        assertEquals(DENY, vote);
    }

    @Test
    public void testVoteOnGadgetProjectRolesSpecified() throws Exception
    {
        PluginGadgetSpec mockSpec = createPluginGadgetSpec("intro-gadget", "rest/intro.xml",
                MapBuilder.<String, String>newBuilder().add("roles-required", "browse").toMap());

        when(permissionManager.hasPermission(Permissions.ADMINISTER, admin)).thenReturn(false);
        when(permissionManager.hasProjects(BROWSE_PROJECTS, admin)).thenReturn(true);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);
        when(permissionManager.hasProjects(BROWSE_PROJECTS, user)).thenReturn(false);

        Vote vote = jiraGadgetPermissionManager.voteOn(mockSpec, admin);
        assertEquals(ALLOW, vote);

        vote = jiraGadgetPermissionManager.voteOn(mockSpec, user);
        assertEquals(DENY, vote);
    }

    @Test
    public void testVoteOnGadgetGlobalAdmin() throws Exception
    {
        PluginGadgetSpec mockSpec = createPluginGadgetSpec("intro-gadget", "rest/intro.xml",
                MapBuilder.<String, String>newBuilder().add("roles-required", "browse").toMap());

        when(permissionManager.hasPermission(Permissions.ADMINISTER, admin)).thenReturn(true);

        Vote vote = jiraGadgetPermissionManager.voteOn(mockSpec, admin);
        assertEquals(ALLOW, vote);
    }

    @Test
    public void testFilterNullDashboardState()
    {
        try
        {
            jiraGadgetPermissionManager.filterGadgets(null, (ApplicationUser) null);
            fail("Should have thrown exception");
        }
        catch (Exception e)
        {
            //yay
        }
    }

    @Test
    public void testFilterReadOnlyDashboardState()
    {
        PluginGadgetSpec loginGadget = createPluginGadgetSpec("login-gadget", "login.xml");

        List<List<GadgetState>> columns = new ArrayList<List<GadgetState>>();
        List<GadgetState> columnOne = new ArrayList<GadgetState>();
        columnOne.add(createGadgetStateAndAddToTranslationService("100", "rest/gadgets/1.0/g/someothergadget.xml"));
        columnOne.add(createGadgetStateAndAddToTranslationService("100", "rest/gadgets/1.0/g/someothergadget2.xml"));
        columnOne.add(createGadgetStateAndAddToTranslationService("100", "http://www.atlassian.com/stream.xml"));
        List<GadgetState> columnTwo = new ArrayList<GadgetState>();
        columnTwo.add(createGadgetStateAndAddToTranslationService("100", "rest/gadgets/1.0/g/com.atlassian.jira.gadgets:login-gadget/login.xml"));
        columnTwo.add(createGadgetStateAndAddToTranslationService("100", "http://www.atlassian.com/stream3.xml"));
        columns.add(columnOne);
        columns.add(columnTwo);

        DashboardState state = DashboardState.dashboard(DashboardId.valueOf("1")).title("System Dashboard").columns(columns).build();

        when(dashboardPermissionService.isWritableBy(DashboardId.valueOf("1"), user.getName())).thenReturn(false);
        when(moduleDescriptor.getModule()).thenReturn(loginGadget);
        when(pluginAccessor.getEnabledPluginModule("com.atlassian.jira.gadgets:login-gadget")).thenReturn(moduleDescriptor);

        DashboardState filteredState = jiraGadgetPermissionManager.filterGadgets(state, user);

        assertNotSame(state, filteredState);
        assertEquals(Layout.AA, filteredState.getLayout());
        Iterator<GadgetState> stateIterator = filteredState.getGadgetsInColumn(DashboardState.ColumnIndex.ZERO).iterator();
        assertEquals(URI.create("rest/gadgets/1.0/g/someothergadget.xml"), stateIterator.next().getGadgetSpecUri());
        assertEquals(URI.create("rest/gadgets/1.0/g/someothergadget2.xml"), stateIterator.next().getGadgetSpecUri());
        assertEquals(URI.create("http://www.atlassian.com/stream.xml"), stateIterator.next().getGadgetSpecUri());
        assertFalse(stateIterator.hasNext());
        stateIterator = filteredState.getGadgetsInColumn(DashboardState.ColumnIndex.ONE).iterator();
        assertEquals(URI.create("http://www.atlassian.com/stream3.xml"), stateIterator.next().getGadgetSpecUri());
        assertFalse(stateIterator.hasNext());
    }

    @Test
    public void testFilterWriteableDashboardState()
    {
        List<List<GadgetState>> columns = new ArrayList<List<GadgetState>>();
        List<GadgetState> columnOne = new ArrayList<GadgetState>();
        columnOne.add(GadgetState.gadget(GadgetId.valueOf("100")).specUri(URI.create("rest/gadgets/1.0/g/someothergadget.xml")).build());
        columnOne.add(GadgetState.gadget(GadgetId.valueOf("100")).specUri(URI.create("rest/gadgets/1.0/g/someothergadget2.xml")).build());
        columnOne.add(GadgetState.gadget(GadgetId.valueOf("100")).specUri(URI.create("http://www.atlassian.com/stream.xml")).build());
        List<GadgetState> columnTwo = new ArrayList<GadgetState>();
        columnTwo.add(GadgetState.gadget(GadgetId.valueOf("100")).specUri(URI.create("rest/gadgets/1.0/g/com.atlassian.jira.gadgets:login-gadget/login.xml")).build());
        columnTwo.add(GadgetState.gadget(GadgetId.valueOf("100")).specUri(URI.create("http://www.atlassian.com/stream3.xml")).build());
        columns.add(columnOne);
        columns.add(columnTwo);

        when(dashboardPermissionService.isWritableBy(DashboardId.valueOf("1"), user.getName())).thenReturn(true);

        DashboardState state = DashboardState.dashboard(DashboardId.valueOf("1")).title("System Dashboard").columns(columns).build();
        DashboardState filteredState = jiraGadgetPermissionManager.filterGadgets(state, user);
        assertEquals(state, filteredState);
    }

    @Test
    public void testExtractModuleKey()
    {
        String key = jiraGadgetPermissionManager.extractModuleKey("http://localhost:8090/jira/rest/gadgets/1.0/g/com.atlassian.jira.gadgets:admin-gadget/gadgets/admin-gadget.xml");
        assertEquals("com.atlassian.jira.gadgets:admin-gadget", key);
        key = jiraGadgetPermissionManager.extractModuleKey("rest/gadgets/1.0/g/com.atlassian.jira.gadgets:admin-gadget/gadgets/admin-gadget.xml");
        assertEquals("com.atlassian.jira.gadgets:admin-gadget", key);
        key = jiraGadgetPermissionManager.extractModuleKey("/rest/gadgets/1.0/g/com.atlassian.jira.gadgets:admin-gadget/gadgets/admin-gadget.xml");
        assertEquals("com.atlassian.jira.gadgets:admin-gadget", key);
        key = jiraGadgetPermissionManager.extractModuleKey("rest/gadgets/1.0/g/some/gadgets/admin-gadget.xml");
        assertNull(key);
        key = jiraGadgetPermissionManager.extractModuleKey("rest/gadgets/1.0/g/admin-gadget.xml");
        assertNull(key);
    }

    @Test
    public void voteOnAcceptsGadgetWhenAllConditionsPass()
    {
        PluginGadgetSpec spec = createPluginGadgetSpec("awesome-gadget", "rest/awesome.xml",
                MapBuilder.<String, String>emptyMap(), enabledCondition, localCondition);

        when(enabledCondition.shouldDisplay(anyMap())).thenReturn(true);
        when(localCondition.shouldDisplay(anyMap())).thenReturn(true);
        when(moduleDescriptor.getModule()).thenReturn(spec);
        when(pluginAccessor.getEnabledPluginModule("com.atlassian.jira.gadgets:awesome-gadget")).thenReturn(moduleDescriptor);

        assertThat(jiraGadgetPermissionManager.voteOn(spec, user), equalTo(ALLOW));
        verify(enabledCondition).shouldDisplay(anyMap());
        verify(localCondition).shouldDisplay(anyMap());
    }

    @Test
    public void voteOnDeniesGadgetWhenEnabledConditionFails()
    {
        PluginGadgetSpec spec = createPluginGadgetSpec("awesome-gadget", "rest/awesome.xml",
                MapBuilder.<String, String>emptyMap(), enabledCondition, localCondition);

        when(enabledCondition.shouldDisplay(anyMap())).thenReturn(false);
        when(localCondition.shouldDisplay(anyMap())).thenReturn(true);
        when(moduleDescriptor.getModule()).thenReturn(spec);
        when(pluginAccessor.getEnabledPluginModule("com.atlassian.jira.gadgets:awesome-gadget")).thenReturn(moduleDescriptor);

        assertThat(jiraGadgetPermissionManager.voteOn(spec, user), equalTo(DENY));
        verify(enabledCondition).shouldDisplay(anyMap());
        verify(localCondition, never()).shouldDisplay(anyMap());
    }

    @Test
    public void voteOnDeniesGadgetWhenLocalConditionFails()
    {
        PluginGadgetSpec spec = createPluginGadgetSpec("awesome-gadget", "rest/awesome.xml",
                MapBuilder.<String, String>emptyMap(), enabledCondition, localCondition);

        when(enabledCondition.shouldDisplay(anyMap())).thenReturn(true);
        when(localCondition.shouldDisplay(anyMap())).thenReturn(false);
        when(moduleDescriptor.getModule()).thenReturn(spec);
        when(pluginAccessor.getEnabledPluginModule("com.atlassian.jira.gadgets:awesome-gadget")).thenReturn(moduleDescriptor);

        assertThat(jiraGadgetPermissionManager.voteOn(spec, user), equalTo(DENY));
        verify(enabledCondition).shouldDisplay(anyMap());
        verify(localCondition).shouldDisplay(anyMap());
    }

    private PluginGadgetSpec createPluginGadgetSpec (String moduleKey, String location)
    {
        return createPluginGadgetSpec(moduleKey, location, MapBuilder.<String, String>emptyMap());
    }

    private PluginGadgetSpec createPluginGadgetSpec (String moduleKey, String location, Map<String, String> params)
    {
        return createPluginGadgetSpec(moduleKey, location, params, null);
    }

    private PluginGadgetSpec createPluginGadgetSpec (String moduleKey, String location, Map<String, String> params, Condition condition)
    {
        return createPluginGadgetSpec(moduleKey, location, params, condition, condition);
    }

    private PluginGadgetSpec createPluginGadgetSpec (String moduleKey, String location, Map<String, String> params, Condition enabled, Condition local)
    {
        return PluginGadgetSpec.builder()
            .plugin(plugin)
            .moduleKey(moduleKey)
            .location(location)
            .params(params)
            .enabledCondition(enabled)
            .localCondition(local)
            .build();
    }

    private GadgetState createGadgetStateAndAddToTranslationService (String id, String url)
    {
        GadgetState gadgetState = GadgetState.gadget(GadgetId.valueOf(id)).specUri(URI.create(url)).build();
        when(gadgetLocationTranslator.translate(URI.create(url))).thenReturn(URI.create(url));
        return gadgetState;
    }
}
