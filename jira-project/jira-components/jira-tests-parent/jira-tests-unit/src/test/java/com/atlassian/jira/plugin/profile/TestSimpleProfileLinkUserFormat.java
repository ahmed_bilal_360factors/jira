package com.atlassian.jira.plugin.profile;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.plugin.userformat.SimpleProfileLinkUserFormat;
import com.atlassian.jira.plugin.userformat.UserFormatModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.user.util.UserUtilImpl;
import com.atlassian.jira.util.BaseUrl;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.MapBuilder;

import com.google.common.collect.Maps;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
@RunWith (MockitoJUnitRunner.class)
public class TestSimpleProfileLinkUserFormat
{

    @Mock I18nHelper i18nHelper;
    @Mock JiraAuthenticationContext jiraAuthenticationContext;
    ApplicationUser user;
    @Mock UserFormatModuleDescriptor userFormatModuleDescriptor;
    @Mock UserKeyService userKeyService;
    @Mock UserManager userManager;
    @Mock BaseUrl baseUrl;
    UserUtil userUtil;

    @Before
    public void setUp()
    {
        when(i18nHelper.getText(anyString())).thenReturn("Anonymous");

        user = new MockApplicationUser("userKey", "username", "User Name", "username@example.com");
        userUtil = new UserUtilImpl(null, null, null, null, null, null, null, null, null,
                null, null, null, null, userManager, null, null, new MemoryCacheManager());

        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(jiraAuthenticationContext.getUser()).thenReturn(user);
        when(userKeyService.getUsernameForKey("userKey")).thenReturn("username");
        when(userManager.getUserByKey("userKey")).thenReturn(user);
        when(baseUrl.getBaseUrl()).thenReturn("/jira");
    }

    @Test
    public void testGetHTMLNullUser()
    {
        when(userFormatModuleDescriptor.getHtml("view", new TestDataBuilder().user(null)
                .username(null)
                .fullname(null)
                .build())).thenReturn("HTML");

        SimpleProfileLinkUserFormat userFormat = newSimpleProfileLinkUserFormat(i18nHelper, jiraAuthenticationContext, userFormatModuleDescriptor, null, null);
        assertThat(userFormat.format(null, "id"), is("HTML"));
    }

    @Test
    public void testGetHTMLUnknownUser()
    {
        // userManager.getUserByKey() will return null, thus an unknown user.
        when(userKeyService.getUsernameForKey("unknownUserKey")).thenReturn("unknownusername");
        when(userFormatModuleDescriptor.getHtml("view", new TestDataBuilder().user(null)
                .username("unknownusername")
                .fullname("unknownusername")
                .build())).thenReturn("Who?!");

        SimpleProfileLinkUserFormat userFormat = newSimpleProfileLinkUserFormat(i18nHelper, jiraAuthenticationContext, userFormatModuleDescriptor, userKeyService, userUtil);
        assertThat(userFormat.format("unknownUserKey", "id"), is("Who?!"));

        verify(userManager).getUserByKey("unknownUserKey");
        verify(userKeyService).getUsernameForKey("unknownUserKey");
        verifyNoMoreInteractions(userManager, userKeyService);
    }

    @Test
    public void testGetHTMLKnownUserWithFullName()
    {
        when(userFormatModuleDescriptor.getHtml("view", new TestDataBuilder().user(user).build())).thenReturn("HTML");

        SimpleProfileLinkUserFormat userFormat = newSimpleProfileLinkUserFormat(i18nHelper, jiraAuthenticationContext, userFormatModuleDescriptor, null, userUtil);
        assertThat(userFormat.format("userKey", "id"), is("HTML"));

        verify(userManager).getUserByKey("userKey");
        verifyNoMoreInteractions(userManager, userKeyService);
    }

    @Test
    public void testGetHTMLWithParams()
    {
        when(userFormatModuleDescriptor.getHtml("view", new TestDataBuilder().user(user).build())).thenReturn("HTML");

        SimpleProfileLinkUserFormat userFormat = newSimpleProfileLinkUserFormat(i18nHelper, jiraAuthenticationContext, userFormatModuleDescriptor, null, userUtil);
        assertThat(userFormat.format("userKey", "id", Maps.<String, Object>newHashMap()), is("HTML"));

        verify(userManager).getUserByKey("userKey");
        verifyNoMoreInteractions(userManager, userKeyService);
    }

    @Test
    public void testGetHTMLKnownUserWithNoFullName()
    {
        user = new MockApplicationUser("userKey", "username", "", "username@example.com");
        when(userManager.getUserByKey("userKey")).thenReturn(user);
        when(userFormatModuleDescriptor.getHtml("view", new TestDataBuilder().user(user)
                .fullname("username")
                .build())).thenReturn("HTML");

        SimpleProfileLinkUserFormat userFormat = newSimpleProfileLinkUserFormat(i18nHelper, jiraAuthenticationContext, userFormatModuleDescriptor, userKeyService, userUtil);
        assertThat(userFormat.format("userKey", "id"), is("HTML"));

        verify(userManager).getUserByKey("userKey");
        verifyNoMoreInteractions(userManager, userKeyService);
    }

    private SimpleProfileLinkUserFormat newSimpleProfileLinkUserFormat(final I18nHelper i18nHelper,
            final JiraAuthenticationContext jiraAuthenticationContext,
            final UserFormatModuleDescriptor moduleDescriptor,
            final UserKeyService userKeyService,
            final UserUtil userUtil)
    {
        return new SimpleProfileLinkUserFormat(i18nHelper, jiraAuthenticationContext, moduleDescriptor, userKeyService, userUtil, baseUrl);
    }

    private static class TestDataBuilder
    {
        final Map<String, Object> map;

        public TestDataBuilder()
        {
            map = MapBuilder.<String, Object>newBuilder()
                    .add("defaultFullName", "Anonymous")
                    .add("fullName", "User Name")
                    .add("id", "id")
                    .add("username", "username")
                    .add("baseUrl", "/jira").toHashMap();
        }

        public TestDataBuilder user(ApplicationUser user)
        {
            map.put("user", user);
            return this;
        }

        public TestDataBuilder fullname(String fullname)
        {
            map.put("fullName", fullname);
            return this;
        }

        public TestDataBuilder username(String username)
        {
            map.put("username", username);
            return this;
        }

        public Map<String, Object> build()
        {
            return new HashMap<String, Object>(map);
        }
    }
}