package com.atlassian.jira.issue.attachment.store.strategy.move;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.AttachmentFileGetData;
import com.atlassian.jira.issue.attachment.AttachmentStreamGetData;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.util.concurrent.Effect;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static com.googlecode.catchexception.CatchException.catchException;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class TestResendingAttachmentStreamCreator
{
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Mock
    private IOUtilsWrapper ioUtilsWrapper;

    @InjectMocks
    private ResendingAttachmentStreamCreator resendingAttachmentStreamCreator;

    @Test
    public void shouldReuseFileIfPossible() throws Exception
    {
        final File existingFile = mock(File.class);
        final AttachmentFileGetData attachmentFileGetData = new AttachmentFileGetData(existingFile);

        final InputStream streamToExistingFile = mock(InputStream.class);
        when(ioUtilsWrapper.openInputStream(existingFile))
                .thenReturn(streamToExistingFile);

        final Pair<InputStream, Effect<Object>> inputStreamWithCloseHandler = resendingAttachmentStreamCreator
                .getInputStreamWithCloseHandler(attachmentFileGetData);

        assertThat(inputStreamWithCloseHandler.first(), equalTo(streamToExistingFile));
    }

    @Test
    public void shouldProvideCleanupEffectWhichWillCloseStreamToExistingFile() throws Exception
    {
        final File existingFile = mock(File.class);
        final AttachmentFileGetData attachmentFileGetData = new AttachmentFileGetData(existingFile);

        final InputStream streamToExistingFile = mock(InputStream.class);
        when(ioUtilsWrapper.openInputStream(existingFile))
                .thenReturn(streamToExistingFile);

        final Pair<InputStream, Effect<Object>> inputStreamWithCloseHandler = resendingAttachmentStreamCreator
                .getInputStreamWithCloseHandler(attachmentFileGetData);

        verifyNoMoreInteractions(streamToExistingFile);
        inputStreamWithCloseHandler.second().apply(Unit.VALUE);
        verify(streamToExistingFile).close();
        verify(existingFile, times(0)).delete();
    }

    @Test
    public void shouldCopyStreamIfFileNotAvailable() throws Exception
    {
        final InputStream originalStream = mock(InputStream.class);
        final AttachmentStreamGetData attachmentStreamGetData = getAttachmentStreamGetData(originalStream);

        final File tempFile = mock(File.class);
        when(ioUtilsWrapper.createTempFile(any(String.class), any(String.class)))
                .thenReturn(tempFile);

        final FileOutputStream outputStreamToTempFile = mock(FileOutputStream.class);
        when(ioUtilsWrapper.openOutputStream(tempFile))
                .thenReturn(outputStreamToTempFile);

        final InputStream streamToTempFile = mock(InputStream.class);
        when(ioUtilsWrapper.openInputStream(tempFile))
                .thenReturn(streamToTempFile);

        final Pair<InputStream, Effect<Object>> inputStreamWithCloseHandler = resendingAttachmentStreamCreator
                .getInputStreamWithCloseHandler(attachmentStreamGetData);

        assertThat(inputStreamWithCloseHandler.first(), equalTo(streamToTempFile));
        verify(ioUtilsWrapper).copy(originalStream, outputStreamToTempFile);
    }

    @Test
    public void shouldProvideCleanupWhichWillCloseStreamAndDeleteTempFile() throws Exception
    {
        final InputStream originalStream = mock(InputStream.class);
        final AttachmentStreamGetData attachmentStreamGetData = getAttachmentStreamGetData(originalStream);

        final File tempFile = mock(File.class);
        when(ioUtilsWrapper.createTempFile(any(String.class), any(String.class)))
                .thenReturn(tempFile);

        final InputStream streamToTempFile = mock(InputStream.class);
        when(ioUtilsWrapper.openInputStream(tempFile))
                .thenReturn(streamToTempFile);

        final Pair<InputStream, Effect<Object>> inputStreamWithCloseHandler = resendingAttachmentStreamCreator
                .getInputStreamWithCloseHandler(attachmentStreamGetData);

        verifyNoMoreInteractions(tempFile, streamToTempFile);
        inputStreamWithCloseHandler.second().apply(Unit.VALUE);
        verify(streamToTempFile).close();
        verify(tempFile).delete();
    }

    @Test
    public void shouldDeleteTemporaryFileWhenFailedOpenStreamToIt() throws Exception
    {
        final InputStream originalStream = mock(InputStream.class);
        final AttachmentStreamGetData attachmentStreamGetData = getAttachmentStreamGetData(originalStream);

        final File tempFile = mock(File.class);
        when(ioUtilsWrapper.createTempFile(any(String.class), any(String.class)))
                .thenReturn(tempFile);

        when(ioUtilsWrapper.openInputStream(tempFile))
                .thenThrow(new RuntimeException());

        catchException(resendingAttachmentStreamCreator).getInputStreamWithCloseHandler(attachmentStreamGetData);

        verify(tempFile).delete();
    }

    private AttachmentStreamGetData getAttachmentStreamGetData(final InputStream originalStream)
    {
        final long size = 213l;
        return new AttachmentStreamGetData(originalStream, size);
    }
}