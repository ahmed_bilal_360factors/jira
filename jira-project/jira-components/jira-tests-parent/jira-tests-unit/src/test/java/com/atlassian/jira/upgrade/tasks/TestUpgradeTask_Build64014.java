package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.config.DefaultFeatureManager;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


public class TestUpgradeTask_Build64014
{
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private DefaultFeatureManager defaultFeatureManager;

    private final GenericValue expectedSwitchDarkFeature = new MockGenericValue("Feature", ImmutableMap.of("featureName", "com.atlassian.jira.projects.ProjectCentricNavigation.Switch",
                                                                                             "featureType", "site",
                                                                                             "userKey", ""));

    private final GenericValue expectedIssueNavDarkFeature = new MockGenericValue("Feature", ImmutableMap.of("featureName", "com.atlassian.jira.projects.issuenavigator",
                                                                                             "featureType", "site",
                                                                                             "userKey", ""));
    @Test
    public void testFeatureValuesAddedIfNoPreviousEntry() throws Exception
    {
        MockOfBizDelegator ofBizDelegator = new MockOfBizDelegator(Lists.<GenericValue>newArrayList(), Lists.newArrayList(expectedSwitchDarkFeature, expectedIssueNavDarkFeature));

        UpgradeTask_Build64014 upgradeTask = new UpgradeTask_Build64014(ofBizDelegator, defaultFeatureManager);
        upgradeTask.doUpgrade(false);

        ofBizDelegator.verify();
        verify(defaultFeatureManager, times(1)).onClearCache(null);
    }

    @Test
    public void testFeatureValuesAddedIfSetup() throws Exception
    {
        MockOfBizDelegator ofBizDelegator = new MockOfBizDelegator(Lists.<GenericValue>newArrayList(), Lists.newArrayList(expectedSwitchDarkFeature, expectedIssueNavDarkFeature));

        UpgradeTask_Build64014 upgradeTask = new UpgradeTask_Build64014(ofBizDelegator, defaultFeatureManager);
        upgradeTask.doUpgrade(true);

        ofBizDelegator.verify();
        verify(defaultFeatureManager, times(1)).onClearCache(null);
    }

    @Test
    public void testFeatureOnlyAddedOnceIfAlreadyExists() throws Exception
    {
        MockOfBizDelegator ofBizDelegator = new MockOfBizDelegator(Lists.newArrayList(expectedSwitchDarkFeature, expectedIssueNavDarkFeature), Lists.newArrayList(expectedSwitchDarkFeature, expectedIssueNavDarkFeature));

        UpgradeTask_Build64014 upgradeTask = new UpgradeTask_Build64014(ofBizDelegator, defaultFeatureManager);
        upgradeTask.doUpgrade(true);

        ofBizDelegator.verify();

        verify(defaultFeatureManager, times(1)).onClearCache(null);
    }
}
