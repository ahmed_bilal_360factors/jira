package com.atlassian.jira.config.util;

import java.io.File;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.ReplicationSettingsConfiguredEvent;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v6.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestSecondaryJiraHome
{
    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private ClusterManager clusterManager;

    @Mock
    private JiraHome jiraHome;

    @Mock
    private EventPublisher eventPublisher;

    private SecondaryJiraHome secondaryJiraHome;

    @Captor
    ArgumentCaptor<ReplicationSettingsConfiguredEvent> eventArgumentCaptor;

    @Before
    public void setup()
    {
        secondaryJiraHome = new SecondaryJiraHome(jiraHome, applicationProperties, clusterManager, eventPublisher);
    }

    @Test
    public void isEnabled()
    {
        when(applicationProperties.getOption(APKeys.JIRA_SECONDARY_STORAGE)).thenReturn(false);
        when(clusterManager.isClusterLicensed()).thenReturn(false);
        assertFalse(secondaryJiraHome.isEnabled());

        when(applicationProperties.getOption(APKeys.JIRA_SECONDARY_STORAGE)).thenReturn(false);
        when(clusterManager.isClusterLicensed()).thenReturn(true);
        assertFalse(secondaryJiraHome.isEnabled());

        when(applicationProperties.getOption(APKeys.JIRA_SECONDARY_STORAGE)).thenReturn(true);
        when(clusterManager.isClusterLicensed()).thenReturn(true);
        assertTrue(secondaryJiraHome.isEnabled());
    }

    @Test
    public void getHomePath()
    {
        when(applicationProperties.getOption(APKeys.JIRA_SECONDARY_STORAGE)).thenReturn(false);
        when(clusterManager.isClusterLicensed()).thenReturn(false);
        assertNull(secondaryJiraHome.getLocalHome());

        // Even setting the flag should keep returning null
        secondaryJiraHome.setEnabled(false);
        assertNull(secondaryJiraHome.getLocalHome());


        when(applicationProperties.getOption(APKeys.JIRA_SECONDARY_STORAGE)).thenReturn(true);
        when(clusterManager.isClusterLicensed()).thenReturn(true);

        // We refresh the state
        secondaryJiraHome.setEnabled(true);

        final File localHome = secondaryJiraHome.getLocalHome();
        assertNotNull(localHome);
        assertEquals(localHome.getAbsolutePath(), secondaryJiraHome.getDefaultPath());
    }

    @Test
    public void refreshLocation()
    {
        when(applicationProperties.getString(APKeys.JIRA_SECONDARY_HOME)).thenReturn(null);
        secondaryJiraHome.refreshLocation(true);
        assertEquals(secondaryJiraHome.getLocalHome().getAbsolutePath(), secondaryJiraHome.getDefaultPath());

        when(applicationProperties.getString(APKeys.JIRA_SECONDARY_HOME)).thenReturn(" ");
        secondaryJiraHome.refreshLocation(true);
        assertEquals(secondaryJiraHome.getLocalHome().getAbsolutePath(), secondaryJiraHome.getDefaultPath());

        secondaryJiraHome.refreshLocation(false);
        assertNull(secondaryJiraHome.getLocalHome());
    }

    @Test
    public void applySettingsAllFalse()
    {
        applySettings(false, false, false, false);
    }

    @Test
    public void applySettingsAttachmentsOnly()
    {
        applySettings(true, false, false, false);
    }

    @Test
    public void applySettingsPluginsOnly()
    {
        applySettings(false, true, false, false);
    }

    @Test
    public void applySettingsIndexOnly()
    {
        applySettings(false, false, true, false);
    }

    @Test
    public void applySettingsAvatarOnly()
    {
        applySettings(false, false, false, true);
    }

    @Test
    public void applySettingsAllTrue()
    {
        applySettings(true, true, true, true);
    }

    private void applySettings(final boolean attachments, final boolean plugins, final boolean indexSnapshots,
            final boolean avatars)
    {
        secondaryJiraHome.applySettings(attachments, plugins, indexSnapshots, avatars);
        verify(applicationProperties).setOption(APKeys.JIRA_SECONDARY_STORAGE, attachments || plugins || indexSnapshots || avatars);
        verify(applicationProperties).setOption(JiraHomeChangeEvent.FileType.ATTACHMENT.getKey(), attachments);
        verify(applicationProperties).setOption(JiraHomeChangeEvent.FileType.PLUGIN.getKey(), plugins);
        verify(applicationProperties).setOption(JiraHomeChangeEvent.FileType.INDEX_SNAPSHOT.getKey(), indexSnapshots);
        verify(applicationProperties).setOption(JiraHomeChangeEvent.FileType.AVATAR.getKey(), avatars);

        verify(eventPublisher).publish(eventArgumentCaptor.capture());
        ReplicationSettingsConfiguredEvent event = eventArgumentCaptor.getValue();
        assertThat(event.isAttachments(), equalTo(attachments));
        assertThat(event.isAvatars(), equalTo(avatars));
        assertThat(event.isIndexSnaphots(), equalTo(indexSnapshots));
        assertThat(event.isPlugins(), equalTo(plugins));
    }
}
