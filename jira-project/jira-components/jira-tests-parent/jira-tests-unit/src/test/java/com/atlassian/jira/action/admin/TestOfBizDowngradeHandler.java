package com.atlassian.jira.action.admin;

import com.atlassian.jira.ofbiz.OfBizDelegator;

import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import junit.framework.AssertionFailedError;

import static org.mockito.Mockito.mock;

/**
 * @since v6.2.1
 */
public class TestOfBizDowngradeHandler
{
    static final String MESSAGE = "\n\n" +
            "The OfBizDowngradeHandler should always be a no-op on master.  If you are seeing this\n" +
            "message, then someone has added a downgrade task.  If this is a stable branch, then\n" +
            "that is fine; just add an @Ignore for this test on the stable branch.  However, when\n" +
            "you merge the stable branch into master, you need to remove both the downgrade logic\n" +
            "and the @Ignore to keep master clean!\n\nUnexpected call to: ";

    @Test
    public void testDowngradeShouldNotDoAnythingOnMaster()
    {
        final OfBizDelegator cantTouchThis = mock(OfBizDelegator.class, new Answer<Object>()
        {
            @Override
            public Object answer(final InvocationOnMock howVeryDareYou) throws Throwable
            {
                throw new AssertionFailedError(MESSAGE + howVeryDareYou);
            }
        });

        new OfBizDowngradeHandler(cantTouchThis).downgrade();
    }
}
