package com.atlassian.jira.issue.attachment.store.strategy.move;

import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.FileSystemAttachmentStore;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.jira.issue.attachment.store.MockAttachmentKeys;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestBackgroundResendingMoveStrategy
{

    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private StreamAttachmentStore secondaryStore;
    @Mock
    private FileSystemAttachmentStore primaryStore;
    @Mock
    private SendToStoreFunctionFactory sendToStoreFunctionFactory;

    private BackgroundResendingMoveStrategy backgroundResendingMoveStrategy;

    @Before
    public void setUp() throws Exception
    {
        this.backgroundResendingMoveStrategy = new BackgroundResendingMoveStrategy(primaryStore, secondaryStore, sendToStoreFunctionFactory);
    }

    @Test
    public void shouldMoveTempToAttachmentInPrimary() throws Exception
    {
        final TemporaryAttachmentId tempId = TemporaryAttachmentId.fromString("tempId");
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();
        when(primaryStore.moveTemporaryToAttachment(tempId, attachmentKey))
                .thenReturn(Promises.promise(Unit.VALUE));

        backgroundResendingMoveStrategy.moveTemporaryToAttachment(tempId, attachmentKey).claim();

        verify(primaryStore).moveTemporaryToAttachment(tempId, attachmentKey);
    }

    @Test
    public void shouldStartBackgroundSendToSecondaryStoreFromPrimary() throws Exception
    {
        final TemporaryAttachmentId tempId = TemporaryAttachmentId.fromString("tempId");
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();
        when(primaryStore.moveTemporaryToAttachment(tempId, attachmentKey))
                .thenReturn(Promises.promise(Unit.VALUE));

        final AttachmentGetData attachmentGetData = mock(AttachmentGetData.class);
        callStreamProcessingFunctionWithStream(attachmentKey, attachmentGetData);

        //noinspection unchecked
        final Function<AttachmentGetData, Unit> mockFunction = mock(Function.class);
        when(sendToStoreFunctionFactory.createFunction(attachmentKey, secondaryStore))
                .thenReturn(mockFunction);

        final Promise<Unit> result = backgroundResendingMoveStrategy.moveTemporaryToAttachment(tempId, attachmentKey);

        assertThat(result.claim(), equalTo(Unit.VALUE));
        verify(mockFunction).get(attachmentGetData);
    }

    @Test
    public void shouldIgnoreAnyErrorsInBackgroundSending() throws Exception
    {
        final TemporaryAttachmentId tempId = TemporaryAttachmentId.fromString("tempId");
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();
        when(primaryStore.moveTemporaryToAttachment(tempId, attachmentKey))
                .thenReturn(Promises.promise(Unit.VALUE));

        //noinspection unchecked
        when(primaryStore.getAttachment(any(AttachmentKey.class), any(Function.class)))
                .thenThrow(new RuntimeException());

        final Promise<Unit> result = backgroundResendingMoveStrategy.moveTemporaryToAttachment(tempId, attachmentKey);

        assertThat(result.claim(), equalTo(Unit.VALUE));
    }

    private void callStreamProcessingFunctionWithStream(final AttachmentKey attachmentKey, final AttachmentGetData attachmentGetData)
    {
        //noinspection unchecked
        doAnswer(new Answer()
        {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                //noinspection unchecked
                final Function<AttachmentGetData, Unit> inputStreamFunction =
                        (Function<AttachmentGetData, Unit>) invocation.getArguments()[1];
                return inputStreamFunction.get(attachmentGetData);
            }
        }).when(primaryStore).getAttachmentData(eq(attachmentKey), any(Function.class));
    }
}