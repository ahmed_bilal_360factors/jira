package com.atlassian.jira.index.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.beehive.simple.SimpleClusterLockService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.mock.security.MockAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.sharing.index.SharedEntityIndexManager;
import com.atlassian.jira.task.MockTaskDescriptor;
import com.atlassian.jira.task.TaskContext;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.index.IndexLifecycleManager;
import com.atlassian.jira.web.bean.MockI18nBean;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultReindexRequestService
{
    @Mock
    private ReindexRequestManager reindexRequestManager;

    private ClusterLockService clusterLockService = new SimpleClusterLockService();

    @Mock
    private IssueManager issueManager;

    @Mock
    private IssueIndexManager issueIndexManager;

    @Mock
    private SharedEntityIndexManager sharedEntityIndexManager;

    @Mock
    private IndexLifecycleManager indexLifecycleManager;

    @Mock
    private TaskManager taskManager;

    @Mock
    private SearchProvider searchProvider;

    @Mock
    private SearchProviderFactory searchProviderFactory;

    @Mock
    private SearchService searchService;

    @Mock
    private PermissionManager permissionManager;

    private I18nHelper.BeanFactory i18nFactory = new MockI18nBean.MockI18nBeanFactory();
    private MockAuthenticationContext jiraAuthenticationContext = new MockAuthenticationContext(new MockUser("mctest"));

    private DefaultReindexRequestService reindexRequestService;

    @Before
    public void setUp()
    {
        reindexRequestService = new DefaultReindexRequestService(reindexRequestManager, permissionManager, jiraAuthenticationContext);
        when(permissionManager.hasPermission(eq(Permissions.ADMINISTER), any(ApplicationUser.class))).thenReturn(true);

        when(reindexRequestManager.transitionStatus(anyListOf(ReindexRequest.class), any(ReindexStatus.class)))
                .thenAnswer(new Answer<List<ReindexRequest>>()
                {
                    @Override
                    public List<ReindexRequest> answer(InvocationOnMock invocation) throws Throwable
                    {
                        @SuppressWarnings("unchecked") List<ReindexRequest> list = (List<ReindexRequest>)invocation.getArguments()[0];
                        ReindexStatus newStatus = (ReindexStatus)invocation.getArguments()[1];
                        List<ReindexRequest> results = new ArrayList<ReindexRequest>(list.size());

                        for (ReindexRequest request : list)
                        {
                            results.add(new ReindexRequest(request.getId(), request.getType(), request.getRequestTime(), request.getStartTime(), request.getCompletionTime(), request.getExecutionNodeId(), newStatus, request.getAffectedIndexes(), request.getSharedEntities(), request.getSources()));
                        }
                        return results;
                    }
                });
    }

    @Test
    public void testGetReindexProgress()
    {
        when(reindexRequestManager.getReindexProgress(ImmutableSet.of(1L))).thenReturn(ImmutableSet.of(
                new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, 123L, null, null, null, ReindexStatus.PENDING, ImmutableSet.<AffectedIndex>of(), ImmutableSet.<SharedEntityType>of())
        ));

        ReindexRequest request = reindexRequestService.getReindexProgress(1L);
        assertEquals("Wrong ID.", Long.valueOf(1L), request.getId());
        assertEquals("Wrong status.", ReindexStatus.PENDING, request.getStatus());
    }

    @Test
    public void testProcessRequestsNoPending()
            throws Exception
    {
        Set<ReindexRequest> requests = reindexRequestService.processRequests(EnumSet.of(ReindexRequestType.IMMEDIATE), false);
        assertThat(requests, hasSize(0));
    }

    @Test
    public void testProcessRequestsWithPending()
            throws Exception
    {
        when(reindexRequestManager.isReindexInProgress()).thenReturn(false);
        when(reindexRequestManager.processPendingRequests(false, EnumSet.of(ReindexRequestType.IMMEDIATE), false)).thenReturn(
                ImmutableSet.of(new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, 123L, null, null, null, ReindexStatus.ACTIVE, ImmutableSet.<AffectedIndex>of(), ImmutableSet.<SharedEntityType>of())));

        Set<ReindexRequest> requests = reindexRequestService.processRequests(EnumSet.of(ReindexRequestType.IMMEDIATE), false);
        assertThat(requests, hasSize(1));

        //Should be transitioned to active, from then on async reindex will transition further
        ReindexRequest result = requests.iterator().next();
        assertEquals("Wrong ID.", Long.valueOf(1L), result.getId());
        assertEquals("Wrong status.", ReindexStatus.ACTIVE, result.getStatus());
    }

    @Test
    public void testProcessRequestsWithSharedEntities()
            throws Exception
    {
        when(reindexRequestManager.isReindexInProgress()).thenReturn(false);
        when(reindexRequestManager.processPendingRequests(false, EnumSet.of(ReindexRequestType.IMMEDIATE), false)).thenReturn(
                ImmutableSet.of(new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, 123L, null, null, null, ReindexStatus.ACTIVE, ImmutableSet.<AffectedIndex>of(), ImmutableSet.<SharedEntityType>of(SharedEntityType.SEARCH_REQUEST))));

        Set<ReindexRequest> requests = reindexRequestService.processRequests(EnumSet.of(ReindexRequestType.IMMEDIATE), false);
        assertThat(requests, hasSize(1));

        //Should be transitioned to active, from then on async reindex will transition further
        ReindexRequest result = requests.iterator().next();
        assertEquals("Wrong ID.", Long.valueOf(1L), result.getId());
        assertEquals("Wrong status.", ReindexStatus.ACTIVE, result.getStatus());
    }

    @Test(expected=PermissionException.class)
    public void testProcessRequestsNoPermission()
            throws Exception
    {
        when(permissionManager.hasPermission(eq(Permissions.ADMINISTER), any(ApplicationUser.class))).thenReturn(false);
        reindexRequestService.processRequests(EnumSet.of(ReindexRequestType.IMMEDIATE), false);
    }

    @Test(expected=IllegalStateException.class)
    public void testProcessRequestsWithActiveIndexTask()
            throws Exception
    {
        when(reindexRequestManager.isReindexInProgress()).thenReturn(true);
        when(reindexRequestManager.getPendingReindexRequests(EnumSet.of(ReindexRequestType.IMMEDIATE))).thenReturn(
                ImmutableList.of(new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, 123L, null, null, null, ReindexStatus.PENDING, ImmutableSet.<AffectedIndex>of(), ImmutableSet.<SharedEntityType>of())));
        when(taskManager.getLiveTask(any(TaskContext.class))).thenReturn(new MockTaskDescriptor<Serializable>());

        reindexRequestService.processRequests(EnumSet.of(ReindexRequestType.IMMEDIATE), false);
    }

    @Test
    public void testProcessRequestsIndexFailure()
            throws Exception
    {
        when(reindexRequestManager.isReindexInProgress()).thenReturn(false);
        when(reindexRequestManager.processPendingRequests(false, EnumSet.of(ReindexRequestType.IMMEDIATE), false)).thenReturn(
                ImmutableSet.of(new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, 123L, null, null, null, ReindexStatus.FAILED, ImmutableSet.<AffectedIndex>of(), ImmutableSet.<SharedEntityType>of())));

        reindexRequestService = new DefaultReindexRequestService(reindexRequestManager, permissionManager,
                jiraAuthenticationContext);

        Set<ReindexRequest> requests = reindexRequestService.processRequests(EnumSet.of(ReindexRequestType.IMMEDIATE), false);
        assertThat(requests, hasSize(1));

        //Should be transitioned to failure, DefaultReindexRequestServiceForTestingThatAlwaysFails makes failure immediate
        ReindexRequest result = requests.iterator().next();
        assertEquals("Wrong ID.", Long.valueOf(1L), result.getId());
        assertEquals("Wrong status.", ReindexStatus.FAILED, result.getStatus());
    }
}
