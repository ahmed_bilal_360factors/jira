package com.atlassian.jira.startup;

import com.atlassian.jira.junit.rules.SetupServletContextProvider;
import com.atlassian.jira.web.ServletContextProvider;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestJiraWebappStartupCheck
{
    @Rule
    public SetupServletContextProvider setupContextServletProvider = new SetupServletContextProvider();

    private JiraWebappStartupCheck jiraWebappStartupCheck;

    @Before
    public void setUp() throws Exception
    {
        jiraWebappStartupCheck = new JiraWebappStartupCheck();
    }

    @Test
    public void getNameIsNotEmpty()
    {
        // We don't much care what the name is, but it should be something
        assertThat(jiraWebappStartupCheck.getName(), notNullValue());
        assertThat(jiraWebappStartupCheck.getName(), not(isEmpty()));
    }

    @Test
    public void isOkIfGetRealPathIsNotNull()
    {
        when(ServletContextProvider.getServletContext().getRealPath(anyString())).thenReturn("/somePath");
        assertThat(jiraWebappStartupCheck.isOk(), is(true));
    }

    @Test
    public void notIsOkIfGetRealPathIsNull()
    {
        // By default, mock methods return null, so when(...) required.
        assertThat(jiraWebappStartupCheck.isOk(), is(false));
    }

    @Test
    public void getFaultDescriptionIsStringKnownToSupport()
    {
        // This string is repeated because it must match exactly
        final String EXPECTED_FAULT = "Running JIRA from a packed WAR is not supported. Configure your Servlet"
                + " container to unpack the WAR before running it.";
        assertThat(jiraWebappStartupCheck.getFaultDescription(), is(EXPECTED_FAULT));
    }

    @Test
    public void getHTMLFaultDescriptionIsNotEmpty()
    {
        // This is the message presented to the user, and the exact format is not critical to other tools
        assertThat(jiraWebappStartupCheck.getHTMLFaultDescription(), notNullValue());
        assertThat(jiraWebappStartupCheck.getHTMLFaultDescription(), not(isEmpty()));
    }

    @Test
    public void stop()
    {
        // We don't assume anything about stop, but check it doesn't throw
        jiraWebappStartupCheck.stop();
    }

    private static final Matcher<String> IS_EMPTY = new TypeSafeMatcher<String>()
    {
        @Override
        protected boolean matchesSafely(final String string)
        {
            return string.isEmpty();
        }

        @Override
        public void describeTo(final Description description)
        {
            description.appendText("empty string");
        }
    };

    private static Matcher<String> isEmpty()
    {
        return IS_EMPTY;
    }
}
