package com.atlassian.jira.issue.status.category;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItem;

/**
 * Test Case for StatusCategoryImpl
 *
 * @since v6.1
 */
public class TestStatusCategoryImpl
{
    public static final String ALIAS_TO_DO = "To Do";
    public static final String ALIAS_DONE = "Done";

    @Test
    public void testGetDefault()
    {
        StatusCategory statusCategory = StatusCategoryImpl.getDefault();

        assertEquals("id of status category should match", 1L, (long) statusCategory.getId());
        assertEquals(StatusCategory.UNDEFINED, statusCategory.getKey());
        assertEquals("medium-gray", statusCategory.getColorName());
    }

    @Test
    public void testGetUserVisibleCategoriesWithLogicalProgressSequence()
    {
        List<StatusCategory> categories = StatusCategoryImpl.getUserVisibleCategories();
        assertEquals(3, categories.size());
        assertEquals(StatusCategoryImpl.TO_DO, categories.get(0).getKey());
        assertEquals(StatusCategoryImpl.IN_PROGRESS, categories.get(1).getKey());
        assertEquals(StatusCategoryImpl.COMPLETE, categories.get(2).getKey());
    }

    @Test
    public void testGetAliases()
    {
        testGetAliases(StatusCategoryImpl.TO_DO, ALIAS_TO_DO);
        testGetAliases(StatusCategoryImpl.COMPLETE, ALIAS_DONE);
    }

    @Test
    public void testFindByNameUsingAlias()
    {
        assertSame(StatusCategoryImpl.findByKey(StatusCategoryImpl.TO_DO), StatusCategoryImpl.findByName(ALIAS_TO_DO));
        assertSame(StatusCategoryImpl.findByKey(StatusCategoryImpl.COMPLETE), StatusCategoryImpl.findByName(ALIAS_DONE));
    }

    @Test
    public void testFindByName()
    {
        assertSame(StatusCategoryImpl.findByKey(StatusCategoryImpl.TO_DO), StatusCategoryImpl.findByName("New"));
        assertSame(StatusCategoryImpl.findByKey(StatusCategoryImpl.COMPLETE), StatusCategoryImpl.findByName("Complete"));
        assertSame(StatusCategoryImpl.findByKey(StatusCategoryImpl.IN_PROGRESS), StatusCategoryImpl.findByName("In Progress"));
        assertSame(StatusCategoryImpl.findByKey(StatusCategoryImpl.UNDEFINED), StatusCategoryImpl.findByName("No Category"));
    }

    private void testGetAliases(String categoryKey, String expectedAlias)
    {
        StatusCategory category = StatusCategoryImpl.findByKey(categoryKey);
        assertEquals(1, category.getAliases().size());
        assertThat(category.getAliases(), hasItem(expectedAlias));
    }
}
