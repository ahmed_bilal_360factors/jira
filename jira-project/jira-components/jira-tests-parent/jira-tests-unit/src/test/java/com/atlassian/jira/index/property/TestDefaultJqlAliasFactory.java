package com.atlassian.jira.index.property;

import com.atlassian.jira.index.IndexDocumentConfiguration;
import com.atlassian.jira.issue.customfields.converters.DoubleConverter;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.jql.ClauseHandler;
import com.atlassian.jira.jql.ClauseInformation;
import com.atlassian.jira.jql.DefaultClauseHandler;
import com.atlassian.jira.jql.DefaultValuesGeneratingClauseHandler;
import com.atlassian.jira.jql.context.SimpleClauseContextFactory;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.util.JqlDateSupport;
import com.atlassian.jira.portal.ColumnNamesValuesGenerator;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.PluginAccessor;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.fugue.Option.some;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultJqlAliasFactory
{
    @Mock private SimpleClauseContextFactory clauseContextFactory;
    @Mock private DoubleConverter doubleConverter;
    @Mock private JqlDateSupport jqlDateSupport;
    @Mock private JqlOperandResolver operandResolver;
    @Mock private JiraAuthenticationContext authenticationContext;
    @Mock private IssuePropertyClauseValueGeneratorFactory valuesGeneratorFactory;
    @Mock private PluginAccessor pluginAccessor;

    private DefaultJqlAliasFactory jqlAliasFactory;

    @Before
    public void setUp()
    {
        when(valuesGeneratorFactory.create(Mockito.any(com.atlassian.query.clause.Property.class))).thenReturn(
                new IssuePropertyClauseValueGenerator("fieldname", mock(IssueIndexManager.class)));
        this.jqlAliasFactory = new DefaultJqlAliasFactory(clauseContextFactory, doubleConverter, jqlDateSupport, operandResolver, authenticationContext, valuesGeneratorFactory, pluginAccessor);
    }

    @Test
    public void testGeneratingClauseHandlerCreatedOnlyForStringType() throws Exception
    {
        JqlAlias alias = jqlAliasFactory.createAlias("pluginkey", "some property key",
                new IndexDocumentConfiguration.ExtractConfiguration("path", IndexDocumentConfiguration.Type.STRING, some("alias")), "alias");

        ClauseHandler clauseHandler = alias.getClauseHandler();

        assertThat(clauseHandler, instanceOf(DefaultValuesGeneratingClauseHandler.class));
    }

    @Test
    public void testNonGeneratingClauseHandlerCreatedForDateType() throws Exception
    {
        JqlAlias alias = jqlAliasFactory.createAlias("pluginkey", "some property key",
                new IndexDocumentConfiguration.ExtractConfiguration("path", IndexDocumentConfiguration.Type.DATE, some("alias")), "alias");

        ClauseHandler clauseHandler = alias.getClauseHandler();

        assertThat(clauseHandler, instanceOf(DefaultClauseHandler.class));
    }

    @Test
    public void testPluginKeyUsedAsNameDifferentatiorWhenPluginDisabled() throws Exception
    {
        // plugin disabled
        when(pluginAccessor.getPlugin("pluginkey")).thenReturn(null);

        JqlAlias alias = jqlAliasFactory.createAlias("pluginkey", "some property key",
                new IndexDocumentConfiguration.ExtractConfiguration("path", IndexDocumentConfiguration.Type.DATE, some("alias")), "alias");

        ClauseHandler clauseHandler = alias.getClauseHandler();

        ClauseInformation information = clauseHandler.getInformation();

        assertThat(information, instanceOf(AliasClauseInformation.class));

        AliasClauseInformation aliasInformation = (AliasClauseInformation) information;

        assertThat(aliasInformation.getPluginName(), is("pluginkey"));
    }
}