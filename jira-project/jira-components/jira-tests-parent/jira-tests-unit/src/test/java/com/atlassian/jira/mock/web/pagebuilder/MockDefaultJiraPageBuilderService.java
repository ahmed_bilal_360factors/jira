package com.atlassian.jira.mock.web.pagebuilder;

import com.atlassian.jira.web.pagebuilder.Decorator;
import com.atlassian.jira.web.pagebuilder.DecoratorListener;
import com.atlassian.jira.web.pagebuilder.JiraPageBuilderService;
import com.atlassian.jira.web.pagebuilder.PageBuilder;
import com.atlassian.jira.web.pagebuilder.PageBuilderServiceSpi;
import com.atlassian.jira.web.pagebuilder.PageBuilderSpi;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

/**
 * A mock for DefaultJiraPageBuilderService that is able to return a PageBuilder that has its
 * <code>setDecorator()</code> method cause <code>DecoratorListener.onSetDecorator()</code> to be called.
 */
public class MockDefaultJiraPageBuilderService extends com.atlassian.plugin.webresource.assembler.DefaultPageBuilderService implements JiraPageBuilderService, PageBuilderServiceSpi
{
    private PageBuilder pageBuilder;
    private PageBuilderSpi pageBuilderSpi;
    private DecoratorListener decoratorListener;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private ServletContext servletContext;

    public MockDefaultJiraPageBuilderService()
    {
        this(mock(WebResourceIntegration.class), mock(WebResourceAssemblerFactory.class));
    }

    public MockDefaultJiraPageBuilderService(
            WebResourceIntegration webResourceIntegration, WebResourceAssemblerFactory webResourceAssemblerFactory)
    {
        super(webResourceIntegration, webResourceAssemblerFactory);
    }

    @Override
    public void initForRequest(final HttpServletRequest request, final HttpServletResponse response,
            final DecoratorListener decoratorListener, final ServletContext servletContext)
    {
        if (this.request != null)
        {
            throw new IllegalStateException("This mock only supports one request at a time.");
        }
        this.request = request;
        this.response = response;
        this.decoratorListener = decoratorListener;
        this.servletContext = servletContext;
    }

    @Override
    public void clearForRequest()
    {
        request = null;
        response = null;
        decoratorListener = null;
        servletContext = null;
        pageBuilder = null;
        pageBuilderSpi = null;
    }

    @Override
    public PageBuilder get()
    {
        if (pageBuilder == null)
        {
            final DecoratorListener listener = this.decoratorListener;
            pageBuilder = mock(PageBuilder.class);
            // When pageBuilder.setDecorator(...) is called, call decoratorListener.onSetDecorator().
            doAnswer(new Answer()
            {
                @Override
                public Object answer(final InvocationOnMock invocation) throws Throwable
                {
                    listener.onDecoratorSet();
                    return null;
                }
            }).when(pageBuilder).setDecorator(any(Decorator.class));
        }
        return pageBuilder;
    }

    @Override
    public PageBuilderSpi getSpi()
    {
        if (pageBuilderSpi == null)
        {
            pageBuilderSpi = mock(PageBuilderSpi.class);
        }
        return pageBuilderSpi;
    }
}
