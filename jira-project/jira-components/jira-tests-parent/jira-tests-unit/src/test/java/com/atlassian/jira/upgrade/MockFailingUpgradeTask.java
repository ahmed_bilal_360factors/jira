package com.atlassian.jira.upgrade;

import com.atlassian.jira.issue.index.IssueIndexingParams;

/**
* A mock upgrade task for testing purposes, that will throw an exception when run.
*
* @since v6.4
*/
public class MockFailingUpgradeTask extends AbstractUpgradeTask
{
    public MockFailingUpgradeTask()
    {
        super();
    }

    @Override
    public String getBuildNumber()
    {
        return "200";
    }

    @Override
    public String getShortDescription()
    {
        return "test";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception
    {
        throw new RuntimeException("Crashing for testing purposes");
    }

    @Override
    public ScheduleOption getScheduleOption()
    {
        return ScheduleOption.AFTER_JIRA_STARTED;
    }
}
