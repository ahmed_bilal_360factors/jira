package com.atlassian.jira.bc.workflow;

import com.atlassian.jira.workflow.condition.PermissionCondition;
import com.opensymphony.workflow.loader.ConditionDescriptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.atlassian.jira.bc.workflow.DefaultWorkflowConditionDescriptorFactory.CLASS_NAME_KEY;
import static com.atlassian.jira.bc.workflow.DefaultWorkflowConditionDescriptorFactory.PERMISSION_KEY;
import static com.atlassian.jira.permission.ProjectPermissions.ADD_COMMENTS;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("unchecked")
public class TestDefaultWorkflowConditionDescriptorFactory
{
    private DefaultWorkflowConditionDescriptorFactory workflowFunctionDescriptorFactory =
            new DefaultWorkflowConditionDescriptorFactory();

    @Test
    public void testCreatePermissionConditionDescriptorWithValidPermission()
    {
        ConditionDescriptor descriptor = workflowFunctionDescriptorFactory.permission(ADD_COMMENTS);
        Map<String, String> args = descriptor.getArgs();
        assertThat(descriptor.getType(), equalTo("class"));
        assertThat(args, hasEntry(PERMISSION_KEY, ADD_COMMENTS.permissionKey()));
        assertThat(args, hasEntry(CLASS_NAME_KEY, PermissionCondition.class.getName()));
    }
}
