package com.atlassian.jira.jql.query;

import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.util.JqlLocalDateSupport;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class WorklogDateClauseQueryFactoryTest
{
    @Rule
    public RuleChain mocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    private QueryCreationContext queryCreationContext;
    @Mock
    private IssueIdFilterQueryFactory issueIdFilterQueryFactory;
    @Mock
    private QueryProjectRoleAndGroupPermissionsDecorator queryPermissionsDecorator;
    @Mock
    private JqlOperandResolver operandResolver = MockJqlOperandResolver.createSimpleSupport();
    @Mock
    private JqlLocalDateSupport jqlLocalDateSupport;
    @Mock
    private JqlOperandResolver jqlOperandResolver;

    @InjectMocks
    private WorklogDateClauseQueryFactory worklogDateClauseQueryFactory;

    @Test
    public void testGetQuery() throws Exception
    {
        final String clauseName = SystemSearchConstants.forWorklogDate().getJqlClauseNames().getPrimaryName();
        final TerminalClauseImpl terminalClause = new TerminalClauseImpl(clauseName, Operator.LESS_THAN, "2014-08-16");
        final TermQuery queryWithPermissions = new TermQuery(new Term("field", "value"));
        final TermQuery issueIdFilterQuery = new TermQuery(new Term("issue_id", "5"));

        when(queryPermissionsDecorator.appendPermissionFilterQuery(any(Query.class), any(QueryCreationContext.class), eq(DocumentConstants.WORKLOG_LEVEL), eq(DocumentConstants.WORKLOG_LEVEL_ROLE)))
                .thenReturn(queryWithPermissions);
        when(queryPermissionsDecorator.decorateWorklogQueryWithPermissionChecks(any(Query.class), any(QueryCreationContext.class)))
                .thenReturn(queryWithPermissions);
        when(issueIdFilterQueryFactory.createIssueIdFilterQuery(eq(queryWithPermissions), anyString())).thenReturn(issueIdFilterQuery);

        QueryFactoryResult expectedResult = new QueryFactoryResult(issueIdFilterQuery);

        final QueryFactoryResult result = worklogDateClauseQueryFactory.getQuery(queryCreationContext, terminalClause);

        assertThat(result, equalTo(expectedResult));

        verify(queryPermissionsDecorator).decorateWorklogQueryWithPermissionChecks(any(Query.class), any(QueryCreationContext.class));
        verify(issueIdFilterQueryFactory).createIssueIdFilterQuery(eq(queryWithPermissions), anyString());
    }
}