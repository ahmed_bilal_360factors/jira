package com.atlassian.jira.issue.attachment.store.provider;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.ComponentManagerShutdownEvent;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.util.BoundedExecutorServiceWrapper;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class TestBoundedExecutorServiceWrapperProvider
{
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private EventPublisher eventPublisher;

    private BoundedExecutorServiceWrapperProvider executorProvider;

    @Before
    public void setUp() throws Exception
    {
        executorProvider = new BoundedExecutorServiceWrapperProvider();
    }

    @After
    public void tearDown() throws Exception
    {
        executorProvider.stop(null);
    }

    @Test
    public void shouldProvideSameInstance() throws Exception
    {
        final BoundedExecutorServiceWrapper firstInstance = executorProvider.provide(eventPublisher);
        final BoundedExecutorServiceWrapper secondInstance = executorProvider.provide(eventPublisher);

        assertThat(firstInstance, equalTo(secondInstance));
    }

    @Test
    public void shouldRegisterHimselfForShutdownEventOnlyOnce() throws Exception
    {
        executorProvider.provide(eventPublisher);
        executorProvider.provide(eventPublisher);

        verify(eventPublisher, times(1)).register(executorProvider);
    }

    @Test
    public void shouldTerminateExecutorOnShutdownEvent() throws Exception
    {
        final BoundedExecutorServiceWrapper executor = executorProvider.provide(eventPublisher);

        executorProvider.stop(mock(ComponentManagerShutdownEvent.class));

        assertThat("Executor should be terminated", executor.isTerminated(), equalTo(true));
    }

    @Test
    public void shouldProvideNewInstanceAfterShutdownEvent() throws Exception
    {
        final BoundedExecutorServiceWrapper firstInstance = executorProvider.provide(eventPublisher);

        executorProvider.stop(mock(ComponentManagerShutdownEvent.class));

        final BoundedExecutorServiceWrapper secondInstance = executorProvider.provide(eventPublisher);

        assertThat(firstInstance, is(not(equalTo(secondInstance))));
    }
}