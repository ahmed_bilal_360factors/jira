package com.atlassian.jira.issue.attachment.store;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.attachment.FileSystemAttachmentStore;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.store.provider.BackwardCompatibleStoreAdapterProvider;
import com.atlassian.jira.issue.attachment.store.provider.StreamAttachmentStoreProvider;
import com.atlassian.jira.junit.rules.InitMockitoMocks;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestBackwardCompatibleStoreAdapterProvider
{
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private StreamAttachmentStoreProvider streamStoreProvider;
    @Mock
    private StreamAttachmentStore streamStore;
    @Mock
    private FileSystemAttachmentStore fileSystemStore;

    @InjectMocks
    private BackwardCompatibleStoreAdapterProvider backwardCompatibleStoreAdapterProvider;


    @Test
    public void shouldCreateAdapterWithAvailavleFileSystemStore() throws Exception
    {
        when(streamStoreProvider.getStreamAttachmentStore())
                .thenReturn(streamStore);

        when(streamStoreProvider.getFileSystemStore())
                .thenReturn(Option.some(fileSystemStore));

        final BackwardCompatibleStoreAdapter adapter = backwardCompatibleStoreAdapterProvider.provide();

        assertThat(adapter.hasFileSystemAvailable(), equalTo(true));
        assertThat(adapter.getStreamAttachmentStore(), equalTo(streamStore));
    }
}