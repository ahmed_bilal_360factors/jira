package com.atlassian.jira.jql.query;

import java.util.Collections;
import java.util.List;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.search.optimizers.DeterminedProjectsExtractor;
import com.atlassian.jira.jql.clause.DeMorgansVisitor;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.ChangedClause;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.NotClause;
import com.atlassian.query.clause.OrClause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.clause.WasClause;
import com.atlassian.query.operand.FunctionOperand;
import com.atlassian.query.operator.Operator;

import com.google.common.collect.ImmutableSet;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestContextAwareQueryVisitor
{
    private DeMorgansVisitor deMorgansVisitor;

    @Mock
    private QueryRegistry queryRegistry;

    @Mock
    private WasClauseQueryFactory wasClauseQueryFactory;

    @Mock
    private ChangedClauseQueryFactory changedClauseQueryFactory;

    @Mock
    private DeterminedProjectsExtractor determinedProjectsExtractor;

    @Mock
    private QueryCreationContext queryCreationContext;

    @Mock
    private Clause clause;

    private ContextAwareQueryVisitor queryVisitor;

    @Before
    public void setUp() throws Exception
    {
        final Answer<Object> returnFirstArgument = new Answer<Object>()
        {
            @Override
            public Object answer(final InvocationOnMock invocationOnMock) throws Throwable
            {
                return invocationOnMock.getArguments()[0];
            }
        };
        when(determinedProjectsExtractor.extractDeterminedProjectsFromClause(any(Clause.class))).thenReturn(Collections.<String>emptySet());
        deMorgansVisitor = new DeMorgansVisitor();
        queryVisitor = new ContextAwareQueryVisitor(queryRegistry, wasClauseQueryFactory, changedClauseQueryFactory, queryCreationContext, clause);
    }

    @Test
    public void testVisitAndClauseMustNotOccur() throws Exception
    {
        final Clause mockClause = getMockClause(new QueryFactoryResult(new BooleanQuery(), true));
        final Clause mockClause2 = getMockClause(new QueryFactoryResult(new BooleanQuery(), true));
        final AndClause andClause = new AndClause(mockClause, mockClause2);

        final QueryFactoryResult query = queryVisitor.visit(andClause);

        assertFalse(query.mustNotOccur());
        assertTrue(query.getLuceneQuery() instanceof BooleanQuery);
        BooleanQuery booleanQuery = (BooleanQuery) query.getLuceneQuery();
        final BooleanClause[] booleanClauses = booleanQuery.getClauses();
        assertEquals(2, booleanClauses.length);
        assertEquals(BooleanClause.Occur.MUST_NOT, booleanClauses[0].getOccur());
        assertEquals(BooleanClause.Occur.MUST_NOT, booleanClauses[1].getOccur());
    }

    @Test
    public void testVisitAndClauseHappyPath() throws Exception
    {
        final Clause mockClause = getMockClause(new QueryFactoryResult(new BooleanQuery(), true));
        final Clause mockClause2 = getMockClause(QueryFactoryResult.createFalseResult());
        final AndClause andClause = new AndClause(mockClause, mockClause2);

        final QueryFactoryResult query = queryVisitor.visit(andClause);

        assertFalse(query.mustNotOccur());
        assertTrue(query.getLuceneQuery() instanceof BooleanQuery);
        BooleanQuery booleanQuery = (BooleanQuery) query.getLuceneQuery();
        final BooleanClause[] booleanClauses = booleanQuery.getClauses();
        assertEquals(2, booleanClauses.length);
        assertEquals(BooleanClause.Occur.MUST_NOT, booleanClauses[0].getOccur());
        assertEquals(BooleanClause.Occur.MUST, booleanClauses[1].getOccur());
    }

    @Test
    public void testVisitOrClauseContainsMustNotOccur() throws Exception
    {
        final Clause mockClause = getMockClause(new QueryFactoryResult(new BooleanQuery(), true));
        final Clause mockClause2 = getMockClause(QueryFactoryResult.createFalseResult());
        final OrClause orClause = new OrClause(mockClause, mockClause2);

        final QueryFactoryResult query = queryVisitor.visit(orClause);

        assertFalse(query.mustNotOccur());
        assertTrue(query.getLuceneQuery() instanceof BooleanQuery);
        BooleanQuery booleanQuery = (BooleanQuery) query.getLuceneQuery();
        final BooleanClause[] booleanClauses = booleanQuery.getClauses();
        assertEquals(2, booleanClauses.length);
        assertEquals(BooleanClause.Occur.SHOULD, booleanClauses[0].getOccur());

        BooleanQuery expectedQuery = new BooleanQuery();
        expectedQuery.add(new BooleanQuery(), BooleanClause.Occur.MUST_NOT);
        assertEquals(expectedQuery, booleanClauses[0].getQuery());
        assertEquals(BooleanClause.Occur.SHOULD, booleanClauses[1].getOccur());
        assertEquals(new BooleanQuery(), booleanClauses[1].getQuery());
    }

    @Test
    public void testVisitOrClauseHappyPath() throws Exception
    {
        final Clause mockClause = getMockClause(QueryFactoryResult.createFalseResult());
        final Clause mockClause2 = getMockClause(QueryFactoryResult.createFalseResult());

        OrClause orClause = new OrClause(mockClause, mockClause2);

        final QueryFactoryResult query = queryVisitor.visit(orClause);

        assertFalse(query.mustNotOccur());
        assertTrue(query.getLuceneQuery() instanceof BooleanQuery);
        BooleanQuery booleanQuery = (BooleanQuery) query.getLuceneQuery();
        final BooleanClause[] booleanClauses = booleanQuery.getClauses();
        assertEquals(2, booleanClauses.length);
        assertEquals(BooleanClause.Occur.SHOULD, booleanClauses[0].getOccur());
        assertEquals(BooleanClause.Occur.SHOULD, booleanClauses[1].getOccur());
    }

    @Test
    public void testVisitTerminalClauseHappyPath() throws Exception
    {
        final TerminalClause terminalClause = new TerminalClauseImpl("blah", Operator.EQUALS, "blee");
        final BooleanQuery returnQuery = new BooleanQuery();

        final ClauseQueryFactory mockClauseQueryFactory = getMockClauseQueryFactory(terminalClause, new QueryFactoryResult(returnQuery));
        final QueryRegistry mockQueryRegistry = getMockQueryRegistry(terminalClause, Collections.singletonList(mockClauseQueryFactory));

        final ContextAwareQueryVisitor queryVisitor = new ContextAwareQueryVisitor(mockQueryRegistry, wasClauseQueryFactory,
                changedClauseQueryFactory, determinedProjectsExtractor, queryCreationContext, clause);
        final QueryFactoryResult query = queryVisitor.visit(terminalClause);

        assertFalse(query.mustNotOccur());
        assertEquals(returnQuery, query.getLuceneQuery());
    }

    @Test
    public void testVisitTerminalClauseMultipleFactoriesHappyPath() throws Exception
    {
        final TerminalClause terminalClause = new TerminalClauseImpl("blah", Operator.EQUALS, "blee");

        final BooleanQuery returnQuery = new BooleanQuery();
        final ClauseQueryFactory mockClauseQueryFactory = getMockClauseQueryFactory(terminalClause, new QueryFactoryResult(returnQuery));

        final BooleanQuery returnQuery2 = new BooleanQuery();
        returnQuery2.add(new TermQuery(new Term("bad", "query")), BooleanClause.Occur.MUST);
        final ClauseQueryFactory mockClauseQueryFactory2 = getMockClauseQueryFactory(terminalClause, new QueryFactoryResult(returnQuery2, true));

        final QueryRegistry mockQueryRegistry = getMockQueryRegistry(terminalClause, CollectionBuilder.newBuilder(mockClauseQueryFactory, mockClauseQueryFactory2).asList());

        final ContextAwareQueryVisitor queryVisitor = new ContextAwareQueryVisitor(mockQueryRegistry, wasClauseQueryFactory,
                changedClauseQueryFactory, determinedProjectsExtractor, queryCreationContext, clause);
        final QueryFactoryResult query = queryVisitor.visit(terminalClause);

        //The expected query.
        BooleanQuery expectedSubQuery = new BooleanQuery();
        expectedSubQuery.add(returnQuery2, BooleanClause.Occur.MUST_NOT);

        BooleanQuery expectedQuery = new BooleanQuery();
        expectedQuery.add(returnQuery, BooleanClause.Occur.SHOULD);
        expectedQuery.add(expectedSubQuery, BooleanClause.Occur.SHOULD);

        assertFalse(query.mustNotOccur());
        assertEquals(expectedQuery, query.getLuceneQuery());
    }

    @Test
    public void testVisitNotClauseHappyPath() throws Exception
    {
        NotClause badNot = new NotClause(new TerminalClauseImpl("blah", Operator.EQUALS, "blee"));
        final NotClause mockNotClause = mock(NotClause.class);
        when(mockNotClause.accept(deMorgansVisitor)).thenReturn(badNot);

        try
        {
            queryVisitor.visit(mockNotClause);
            fail("Should throw an exception.");
        }
        catch (IllegalStateException e)
        {
            // expected
        }
    }

    @Test
    public void testVisitTerminalClauseNoQueryFactory() throws Exception
    {
        final TerminalClause terminalClause = new TerminalClauseImpl("blah", Operator.EQUALS, "blee");

        final QueryRegistry mockQueryRegistry = getMockQueryRegistry(terminalClause, Collections.<ClauseQueryFactory>emptyList());

        ContextAwareQueryVisitor queryVisitor = new ContextAwareQueryVisitor(mockQueryRegistry, wasClauseQueryFactory, changedClauseQueryFactory, queryCreationContext, clause);

        final QueryFactoryResult queryFactoryResult = queryVisitor.visit(terminalClause);

        assertFalse(queryFactoryResult.mustNotOccur());
        assertEquals(new BooleanQuery(), queryFactoryResult.getLuceneQuery());
    }

    @Test
    public void testContextAwareQueryVisitorWithWasClauseShouldUseProvidedWasClauseQueryFactory() throws Exception
    {
        ApplicationUser user = new MockApplicationUser("mockedUser");
        MockComponentWorker componentWorker = new MockComponentWorker();
        ComponentAccessor.initialiseWorker(componentWorker);

        final WasClause wasClause = mock(WasClause.class);
        final WasClauseQueryFactory wasClauseQueryFactory = mock(WasClauseQueryFactory.class);

        final QueryFactoryResult expectedResult = QueryFactoryResult.createFalseResult();
        when(wasClauseQueryFactory.create(user.getDirectoryUser(), wasClause)).thenReturn(expectedResult);

        final ContextAwareQueryVisitor queryVisitor = new ContextAwareQueryVisitor(queryRegistry, wasClauseQueryFactory, changedClauseQueryFactory, new QueryCreationContextImpl(user), clause);

        final QueryFactoryResult actualResult = queryVisitor.visit(wasClause);
        assertEquals(expectedResult, actualResult);

        verify(wasClauseQueryFactory).create(user.getDirectoryUser(), wasClause);
    }

    @Test
    public void testContextAwareQueryVisitorWithChangedClauseShouldUseProvidedChangedClauseQueryFactory() throws Exception
    {
        ApplicationUser user = new MockApplicationUser("mockedUser");
        MockComponentWorker componentWorker = new MockComponentWorker();
        ComponentAccessor.initialiseWorker(componentWorker);

        final ChangedClause changedClause = mock(ChangedClause.class);
        final ChangedClauseQueryFactory changedClauseQueryFactory = mock(ChangedClauseQueryFactory.class);

        final QueryFactoryResult expectedResult = QueryFactoryResult.createFalseResult();
        when(changedClauseQueryFactory.create(user.getDirectoryUser(), changedClause)).thenReturn(expectedResult);

        final ContextAwareQueryVisitor queryVisitor = new ContextAwareQueryVisitor(queryRegistry, wasClauseQueryFactory, changedClauseQueryFactory, new QueryCreationContextImpl(user), clause);

        final QueryFactoryResult actualResult = queryVisitor.visit(changedClause);
        assertEquals(expectedResult, actualResult);

        verify(changedClauseQueryFactory).create(user.getDirectoryUser(), changedClause);
    }

    @Test
    public void textContextAwareQueryVisitorCorrectDeterminedProjectsPassed() throws Exception
    {
        final TerminalClause projectClause = new TerminalClauseImpl("project", Operator.IS, "PROJ1");
        final TerminalClause versionsClause = new TerminalClauseImpl("fixVersion", Operator.IS, new FunctionOperand("releasedVersions"));
        final AndClause andClause = new AndClause(projectClause, versionsClause);

        final QueryFactoryResult projectClauseQueryFactoryResult = new QueryFactoryResult(new BooleanQuery(), true);
        final ClauseQueryFactory projectClauseQueryFactory = getMockClauseQueryFactory(projectClause, projectClauseQueryFactoryResult);

        final QueryFactoryResult versionsClauseQueryFactoryResult = new QueryFactoryResult(new BooleanQuery(), true);
        final ClauseQueryFactory versionsClauseQueryFactory = getMockClauseQueryFactory(versionsClause, versionsClauseQueryFactoryResult);

        when(queryRegistry.getClauseQueryFactory(queryCreationContext, projectClause)).thenReturn(Collections.singletonList(projectClauseQueryFactory));
        when(queryRegistry.getClauseQueryFactory(queryCreationContext, versionsClause)).thenReturn(Collections.singletonList(versionsClauseQueryFactory));
        when(determinedProjectsExtractor.extractDeterminedProjectsFromClause(andClause)).thenReturn(ImmutableSet.of("PROJ1"));

        andClause.accept(queryVisitor);

        final QueryCreationContext versionsCreationContext = new QueryCreationContextImpl(queryCreationContext, ImmutableSet.of("PROJ1"));
        verify(versionsClauseQueryFactory).getQuery(eq(versionsCreationContext), same(versionsClause));
    }

    @Test
    public void textContextAwareQueryVisitorCorrectDeterminedProjectsPassedMoreComplexCase() throws Exception
    {
        final TerminalClause projectClause = new TerminalClauseImpl("project", Operator.IS, "PROJ1");
        final TerminalClause versionsClause = new TerminalClauseImpl("fixVersion", Operator.IS, new FunctionOperand("releasedVersions"));
        final AndClause andClause = new AndClause(projectClause, versionsClause);

        final TerminalClause projectClause2 = new TerminalClauseImpl("project", Operator.IS, "PROJ2");
        final TerminalClause versionsClause2 = new TerminalClauseImpl("fixVersion", Operator.IN, new FunctionOperand("releasedVersions"));
        final AndClause andClause2 = new AndClause(projectClause2, versionsClause2);

        final OrClause orClause = new OrClause(andClause, andClause2);

        final ClauseQueryFactory mockClauseQueryFactory = mock(ClauseQueryFactory.class);
        final QueryFactoryResult queryFactoryResult = new QueryFactoryResult(new BooleanQuery(), true);

        when(mockClauseQueryFactory.getQuery(any(QueryCreationContext.class), eq(projectClause))).thenReturn(queryFactoryResult);
        when(mockClauseQueryFactory.getQuery(any(QueryCreationContext.class), eq(versionsClause))).thenReturn(queryFactoryResult);
        when(mockClauseQueryFactory.getQuery(any(QueryCreationContext.class), eq(projectClause2))).thenReturn(queryFactoryResult);
        when(mockClauseQueryFactory.getQuery(any(QueryCreationContext.class), eq(versionsClause2))).thenReturn(queryFactoryResult);

        when(queryRegistry.getClauseQueryFactory(eq(queryCreationContext), any(TerminalClause.class))).thenReturn(Collections.singletonList(mockClauseQueryFactory));
        when(determinedProjectsExtractor.extractDeterminedProjectsFromClause(andClause)).thenReturn(ImmutableSet.of("PROJ1"));
        when(determinedProjectsExtractor.extractDeterminedProjectsFromClause(andClause2)).thenReturn(ImmutableSet.of("PROJ2"));

        orClause.accept(queryVisitor);

        final QueryCreationContext versionsCreationContext = new QueryCreationContextImpl(queryCreationContext, ImmutableSet.of("PROJ1"));
        final QueryCreationContext versionsCreationContext2 = new QueryCreationContextImpl(queryCreationContext, ImmutableSet.of("PROJ2"));

        verify(mockClauseQueryFactory).getQuery(eq(versionsCreationContext), same(versionsClause));
        verify(mockClauseQueryFactory).getQuery(eq(versionsCreationContext2), same(versionsClause2));
        verify(mockClauseQueryFactory).getQuery(any(QueryCreationContext.class), same(projectClause));
        verify(mockClauseQueryFactory).getQuery(any(QueryCreationContext.class), same(projectClause2));
    }

    private Clause getMockClause(QueryFactoryResult queryFactoryResult)
    {
        final Clause mockClause = mock(Clause.class);
        when(mockClause.accept(eq(deMorgansVisitor))).thenReturn(mockClause);
        when(mockClause.accept(any(ContextAwareQueryVisitor.class))).thenReturn(queryFactoryResult);
        return mockClause;
    }

    private ClauseQueryFactory getMockClauseQueryFactory(final TerminalClause terminalClause, final QueryFactoryResult queryFactoryResult)
    {
        final ClauseQueryFactory mockClauseQueryFactory = mock(ClauseQueryFactory.class);
        when(mockClauseQueryFactory.getQuery(any(QueryCreationContext.class), eq(terminalClause)))
                .thenReturn(queryFactoryResult);
        return mockClauseQueryFactory;
    }

    private QueryRegistry getMockQueryRegistry(final TerminalClause terminalClause, final List<ClauseQueryFactory> clauseQueryFactories)
    {
        final QueryRegistry mockQueryRegistry = mock(QueryRegistry.class);
        when(mockQueryRegistry.getClauseQueryFactory(eq(queryCreationContext), eq(terminalClause)))
                .thenReturn(clauseQueryFactories);
        return mockQueryRegistry;
    }
}
