package com.atlassian.jira.jql.values;

import java.util.Locale;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.mock.MockProjectManager;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @since v4.0
 */
public class TestProjectCategoryClauseValuesGenerator
{
    private MockProjectManager projectManager;
    private ProjectCategoryClauseValuesGenerator valuesGenerator;

    @Before
    public void setUp() throws Exception
    {
        this.projectManager = new MockProjectManager();
        this.valuesGenerator = new ProjectCategoryClauseValuesGenerator(projectManager)
        {
            @Override
            Locale getLocale(final User searcher)
            {
                return Locale.ENGLISH;
            }
        };
    }

    @Test
    public void testGetPossibleValuesHappyPath() throws Exception
    {
        projectManager.createProjectCategory("Aa it", null);
        projectManager.createProjectCategory("A it", null);
        projectManager.createProjectCategory("B it", null);
        projectManager.createProjectCategory("C it", null);

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "type", "", 5);

        assertEquals(4, possibleValues.getResults().size());
        assertEquals(new ClauseValuesGenerator.Result("Aa it"), possibleValues.getResults().get(0));
        assertEquals(new ClauseValuesGenerator.Result("A it"), possibleValues.getResults().get(1));
        assertEquals(new ClauseValuesGenerator.Result("B it"), possibleValues.getResults().get(2));
        assertEquals(new ClauseValuesGenerator.Result("C it"), possibleValues.getResults().get(3));
    }

    @Test
    public void testGetPossibleValuesHappyPathWithSort() throws Exception
    {
        projectManager.createProjectCategory("Cat it", null);
        projectManager.createProjectCategory("ant it", null);
        projectManager.createProjectCategory("Zebra it", null);
        projectManager.createProjectCategory("Bat it", null);

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "type", "", 5);

        assertEquals(4, possibleValues.getResults().size());
        assertEquals(new ClauseValuesGenerator.Result("ant it"), possibleValues.getResults().get(0));
        assertEquals(new ClauseValuesGenerator.Result("Bat it"), possibleValues.getResults().get(1));
        assertEquals(new ClauseValuesGenerator.Result("Cat it"), possibleValues.getResults().get(2));
        assertEquals(new ClauseValuesGenerator.Result("Zebra it"), possibleValues.getResults().get(3));
    }

    @Test
    public void testGetPossibleValuesMatchFullValue() throws Exception
    {
        projectManager.createProjectCategory("Aa it", null);
        projectManager.createProjectCategory("A it", null);
        projectManager.createProjectCategory("B it", null);
        projectManager.createProjectCategory("C it", null);

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "type", "Aa it", 5);

        assertEquals(1, possibleValues.getResults().size());
        assertEquals(new ClauseValuesGenerator.Result("Aa it"), possibleValues.getResults().get(0));
    }

    @Test
    public void testGetPossibleValuesExactMatchWithOthers() throws Exception
    {
        projectManager.createProjectCategory("Aa it", null);
        projectManager.createProjectCategory("Aa it blah", null);
        projectManager.createProjectCategory("B it", null);
        projectManager.createProjectCategory("C it", null);

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "type", "Aa it", 5);

        assertEquals(2, possibleValues.getResults().size());
        assertEquals(new ClauseValuesGenerator.Result("Aa it"), possibleValues.getResults().get(0));
        assertEquals(new ClauseValuesGenerator.Result("Aa it blah"), possibleValues.getResults().get(1));
    }

    @Test
    public void testGetPossibleValuesMatchNone() throws Exception
    {
        projectManager.createProjectCategory("Aa it", null);
        projectManager.createProjectCategory("A it", null);
        projectManager.createProjectCategory("B it", null);
        projectManager.createProjectCategory("C it", null);

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "type", "Z", 5);

        assertEquals(0, possibleValues.getResults().size());
    }

    @Test
    public void testGetPossibleValuesMatchSome() throws Exception
    {
        projectManager.createProjectCategory("Aa it", null);
        projectManager.createProjectCategory("A it", null);
        projectManager.createProjectCategory("B it", null);
        projectManager.createProjectCategory("C it", null);

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "type", "a", 5);

        assertEquals(2, possibleValues.getResults().size());
        assertEquals(new ClauseValuesGenerator.Result("Aa it"), possibleValues.getResults().get(0));
        assertEquals(new ClauseValuesGenerator.Result("A it"), possibleValues.getResults().get(1));
    }
    
    @Test
    public void testGetPossibleValuesMatchToLimit() throws Exception
    {
        projectManager.createProjectCategory("Aa it", null);
        projectManager.createProjectCategory("A it", null);
        projectManager.createProjectCategory("B it", null);
        projectManager.createProjectCategory("C it", null);

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "type", "", 3);

        assertEquals(3, possibleValues.getResults().size());
        assertEquals(new ClauseValuesGenerator.Result("Aa it"), possibleValues.getResults().get(0));
        assertEquals(new ClauseValuesGenerator.Result("A it"), possibleValues.getResults().get(1));
        assertEquals(new ClauseValuesGenerator.Result("B it"), possibleValues.getResults().get(2));
    }

}
