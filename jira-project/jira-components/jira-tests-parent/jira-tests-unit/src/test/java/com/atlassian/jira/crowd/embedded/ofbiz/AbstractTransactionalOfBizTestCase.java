package com.atlassian.jira.crowd.embedded.ofbiz;

import java.util.Locale;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.jira.auditing.AuditingManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.ofbiz.DefaultOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.ofbiz.core.entity.GenericDelegator;
import org.ofbiz.core.entity.GenericDelegatorUtils;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.ofbiz.core.entity.TransactionUtil.beginLocalTransaction;
import static org.ofbiz.core.entity.TransactionUtil.rollbackLocalTransaction;

public abstract class AbstractTransactionalOfBizTestCase
{
    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private AuditingManager auditingManager;
    @Mock
    @AvailableInContainer
    private I18nHelper.BeanFactory beanFactory;
    @Mock
    private I18nHelper i18nHelper;
    @Mock
    @AvailableInContainer
    private UserManager userManager;
    @Mock
    private Directory directory;


    private GenericDelegator delegator;
    private OfBizDelegator ofBizDelegator;

    @Before
    public void setUpOfBiz() throws Exception
    {
        delegator = GenericDelegatorUtils.createGenericDelegator("default");
        ofBizDelegator = new DefaultOfBizDelegator(delegator);
        assertTrue("Should get a new local transaction", beginLocalTransaction(getDataSourceName(), -1));
    }

    @Before
    public void setUpAuditLog() {
        when(beanFactory.getInstance(Locale.ENGLISH)).thenReturn(i18nHelper);
        when(userManager.getDirectory(anyLong())).thenReturn(directory);
    }

    @After
    public void tearDownOfBiz() throws Exception
    {
        rollbackLocalTransaction(true);
        ofBizDelegator = null;
    }

    protected final GenericDelegator getGenericDelegator()
    {
        return delegator;
    }

    protected OfBizDelegator getOfBizDelegator()
    {
        return ofBizDelegator;
    }

    protected String getDataSourceName()
    {
        return "defaultDS";
    }

}
