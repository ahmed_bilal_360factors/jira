package com.atlassian.jira.issue.fields.rest.json;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.MockIssueConstant;
import com.atlassian.jira.issue.fields.option.IssueConstantOption;
import com.atlassian.jira.issue.fields.option.Option;
import com.atlassian.jira.issue.fields.rest.json.beans.SuggestionBean;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.BaseUrl;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;

public class TestSuggestionBeanFactory
{
    @Rule
    public RuleChain chain = MockitoMocksInContainer.forTest(this);

    private SuggestionBeanFactory suggestionBeanFactory;

    @Mock
    private BaseUrl baseUrl;

    @Mock
    private AvatarService avatarService;


    private final String PROJECT_AVATAR_URL = "http://www.example.com/avatar";
    private final String JIRA_BASE_URL = "http://www.jira.com/jira/";
    private final String PROJECT_SUGGESTION_NAME_FORMAT = "%s (%s)";
    private final List<Project> PROJECTS = ImmutableList.<Project>of(
            new MockProject(1L, "TST", "test"),
            new MockProject(2L, "PR", "project")
    );

    @Before
    public void setup() throws URISyntaxException
    {
        when(avatarService.getProjectAvatarAbsoluteURL(isA(Project.class), isA(Avatar.Size.class)))
                .thenReturn(new URI(PROJECT_AVATAR_URL));

        when(baseUrl.getBaseUri()).thenReturn(new URI(JIRA_BASE_URL));

        this.suggestionBeanFactory = new SuggestionBeanFactory(avatarService, baseUrl);
    }

    @Test
    public void shouldCreateSuggestionsFromProjects()
    {
        final Collection<SuggestionBean> result = this.suggestionBeanFactory.projectSuggestions(PROJECTS, 2L);

        assertThat(result, Matchers.containsInAnyOrder(
                suggestionBeanMatcher(projectSuggestionName(PROJECTS.get(0)), PROJECTS.get(0).getId().toString(), PROJECT_AVATAR_URL, false),
                suggestionBeanMatcher(projectSuggestionName(PROJECTS.get(1)), PROJECTS.get(1).getId().toString(), PROJECT_AVATAR_URL, true)
        ));
    }

    @Test
    public void shouldCreateSuggestionsFromOptions()
    {
        final IssueConstant issueConstant1 = new MockIssueConstant("1", "First issue constant");
        issueConstant1.setIconUrl("path/to/image"); //path to image file
        final IssueConstant issueConstant2 = new MockIssueConstant("2", "Second issue constant");
        issueConstant2.setIconUrl("http://example.com/image"); //absolute url
        final IssueConstant issueConstant3 = new MockIssueConstant("3", "Third issue constant");
        issueConstant3.setIconUrl("/absolute/path/to/image"); //path to image file

        final List<Option> options = ImmutableList.<Option>of(
            new IssueConstantOption(issueConstant1),
            new IssueConstantOption(issueConstant2),
            new IssueConstantOption(issueConstant3)
        );

        final Collection<SuggestionBean> result = this.suggestionBeanFactory.optionSuggestions(options, "2");

        final String issueConstant1IconUrl = JIRA_BASE_URL + issueConstant1.getIconUrl();
        final String issueConstant3IconUrl = JIRA_BASE_URL + issueConstant3.getIconUrl().substring(1);

        assertThat(result, Matchers.containsInAnyOrder(
                suggestionBeanMatcher(issueConstant1.getName(), issueConstant1.getId(), issueConstant1IconUrl, false),
                suggestionBeanMatcher(issueConstant2.getName(), issueConstant2.getId(), issueConstant2.getIconUrl(), true),
                suggestionBeanMatcher(issueConstant3.getName(), issueConstant3.getId(), issueConstant3IconUrl, false)
        ));
    }

    private String projectSuggestionName(Project p)
    {
        return String.format(PROJECT_SUGGESTION_NAME_FORMAT, p.getName(), p.getKey());
    }

    private Matcher<SuggestionBean> suggestionBeanMatcher(final String label, final String value, final String icon, final boolean selected)
    {
        return Matchers.allOf(
                Matchers.<SuggestionBean>hasProperty("label", Matchers.equalTo(label)),
                Matchers.hasProperty("value", Matchers.equalTo(value)),
                Matchers.hasProperty("icon", Matchers.equalTo(icon)),
                Matchers.hasProperty("selected", Matchers.equalTo(selected))
        );
    }
}
