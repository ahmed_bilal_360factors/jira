package com.atlassian.jira.acceptance.jira.security;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.association.NodeAssociationStoreImpl;
import com.atlassian.jira.event.MockEventPublisher;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.permission.DefaultPermissionSchemeManager;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.scheme.DefaultSchemeFactory;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeEntity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.ofbiz.core.entity.GenericValue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestPermissions
{
    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @AvailableInContainer
    private final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();

    @AvailableInContainer(interfaceClass = PermissionSchemeManager.class)
    private final DefaultPermissionSchemeManager permissionSchemeManager = new DefaultPermissionSchemeManager(null, null, null,
            ofBizDelegator, new DefaultSchemeFactory(), new NodeAssociationStoreImpl(ofBizDelegator), null, new MockEventPublisher(), new MemoryCacheManager());

    @Test
    public void testManageSchemesAndEntities() throws Exception
    {
        Scheme schemeObject = permissionSchemeManager.createSchemeObject("My Permission Scheme", "testing");
        GenericValue schemeGV = permissionSchemeManager.getScheme(schemeObject.getId());
        assertNotNull(schemeGV);

        assertEquals(0, permissionSchemeManager.getPermissionSchemeEntries(schemeObject, ProjectPermissions.BROWSE_PROJECTS).size());
        GenericValue schemeEntityGV = permissionSchemeManager.createSchemeEntity(schemeGV, new SchemeEntity("group", "developers", ProjectPermissions.BROWSE_PROJECTS.permissionKey()));
        assertEquals(1, permissionSchemeManager.getEntities(schemeGV).size());
        assertEquals(1, permissionSchemeManager.getPermissionSchemeEntries(schemeObject, ProjectPermissions.BROWSE_PROJECTS).size());
        assertEquals(0, permissionSchemeManager.getPermissionSchemeEntries(schemeObject, ProjectPermissions.ASSIGN_ISSUES).size());

        permissionSchemeManager.deleteEntity(schemeEntityGV.getLong("id"));
        assertEquals(0, permissionSchemeManager.getEntities(schemeGV).size());
        assertEquals(0, permissionSchemeManager.getPermissionSchemeEntries(schemeObject, ProjectPermissions.BROWSE_PROJECTS).size());
        assertEquals(0, permissionSchemeManager.getPermissionSchemeEntries(schemeObject, ProjectPermissions.ASSIGN_ISSUES).size());
    }

}
