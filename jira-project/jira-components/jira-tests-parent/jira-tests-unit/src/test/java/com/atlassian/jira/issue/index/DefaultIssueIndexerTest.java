package com.atlassian.jira.issue.index;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.index.Index;
import com.atlassian.jira.issue.index.IndexDirectoryFactory.Name;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.EnumMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class DefaultIssueIndexerTest
{
    @Mock
    private IndexDirectoryFactory indexDirectoryFactory;
    @Mock
    private DefaultIssueIndexer.CommentRetriever commentRetriever;
    @Mock
    private DefaultIssueIndexer.ChangeHistoryRetriever changeHistoryRetriever;
    @Mock
    private DefaultIssueIndexer.WorklogRetriever worklogRetriever;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private IssueDocumentFactory issueDocumentFactory;
    @Mock
    private CommentDocumentFactory commentDocumentFactory;
    @Mock
    private ChangeHistoryDocumentFactory changeHistoryDocumentFactory;
    @Mock
    private WorklogDocumentFactory worklogDocumentFactory;

    private Map<Name, Index.Manager> indexManagers;
    private DefaultIssueIndexer defaultIssueIndexer;

    @Before
    public void setUp() throws Exception
    {
        indexManagers = new EnumMap<Name, Index.Manager>(Name.class);
        for (Name indexName : Name.values())
        {
            indexManagers.put(indexName, mock(Index.Manager.class));
        }

        when(indexDirectoryFactory.get()).thenReturn(indexManagers);
        defaultIssueIndexer = new DefaultIssueIndexer(indexDirectoryFactory, commentRetriever,
                changeHistoryRetriever, worklogRetriever, applicationProperties, issueDocumentFactory,
                commentDocumentFactory, changeHistoryDocumentFactory, worklogDocumentFactory);
    }

    @Test
    public void testDeleteIndexesOnSelectedIndexes()
    {
        defaultIssueIndexer.deleteIndexes(IssueIndexingParams.builder().withoutIssues().withComments().withWorklogs().build());
        verify(indexManagers.get(Name.ISSUE), never()).deleteIndexDirectory();
        verify(indexManagers.get(Name.COMMENT)).deleteIndexDirectory();
        verify(indexManagers.get(Name.CHANGE_HISTORY), never()).deleteIndexDirectory();
        verify(indexManagers.get(Name.WORKLOG)).deleteIndexDirectory();
    }

    @Test
    public void testDeleteIndexesWhenAllIndexAreSelected()
    {
        defaultIssueIndexer.deleteIndexes(IssueIndexingParams.INDEX_ALL);
        for(Index.Manager manager : indexManagers.values())
        {
            verify(manager).deleteIndexDirectory();
        }
    }


}