package com.atlassian.jira.crowd.embedded.ofbiz;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.beehive.simple.SimpleClusterLockService;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.search.query.entity.GroupQuery;
import com.atlassian.crowd.search.query.entity.restriction.MatchMode;
import com.atlassian.crowd.search.query.entity.restriction.Property;
import com.atlassian.crowd.search.query.entity.restriction.PropertyRestriction;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.jira.crowd.embedded.TestData;

import com.google.common.collect.ImmutableSet;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.crowd.embedded.TestData.DIRECTORY_ID;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OfBizGroupDaoTest extends AbstractTransactionalOfBizTestCase
{
    private OfBizGroupDao groupDao;

    @Before
    public void setUp() throws Exception
    {
        final CacheManager cacheManager = new MemoryCacheManager();
        final DirectoryDao directoryDao = new OfBizDirectoryDao(getOfBizDelegator(), cacheManager);
        final OfBizInternalMembershipDao internalmembershipDao = new OfBizInternalMembershipDao(getOfBizDelegator(), cacheManager);
        final ClusterLockService clusterLockService = new SimpleClusterLockService();
        groupDao = new OfBizGroupDao(getOfBizDelegator(), directoryDao, internalmembershipDao, cacheManager, clusterLockService);
    }

    @After
    public void tearDown() throws Exception
    {
        groupDao = null;
    }

    @Test
    public void testAddAndFindGroupByName() throws Exception
    {
        final Group createdGroup = groupDao.add(TestData.Group.getTestData());

        TestData.Group.assertEqualsTestGroup(createdGroup);

        final Group retrievedGroup = groupDao.findByName(TestData.DIRECTORY_ID, TestData.Group.NAME);

        TestData.Group.assertEqualsTestGroup(retrievedGroup);
    }

    @Test
    public void testAddAndStoreAttributesAndFindGroupWithAttributesByName() throws Exception
    {
        final Group createdGroup = groupDao.add(TestData.Group.getTestData());
        TestData.Group.assertEqualsTestGroup(createdGroup);

        groupDao.storeAttributes(createdGroup, TestData.Attributes.getTestData());

        final GroupWithAttributes retrievedGroup = groupDao.findByNameWithAttributes(TestData.DIRECTORY_ID, TestData.Group.NAME);
        TestData.Group.assertEqualsTestGroup(retrievedGroup);
        TestData.Attributes.assertEqualsTestData(retrievedGroup);
    }

    @Test
    public void testUpdateGroup() throws Exception
    {
        final Group createdGroup = groupDao.add(TestData.Group.getTestData());
        TestData.Group.assertEqualsTestGroup(createdGroup);

        final boolean updatedIsActive = false;
        final String updatedDescription = "updated Description";

        groupDao.update(TestData.Group.getGroup(createdGroup.getName(), createdGroup.getDirectoryId(), updatedIsActive, updatedDescription, createdGroup.getType()));

        final Group updatedGroup = groupDao.findByName(TestData.DIRECTORY_ID, TestData.Group.NAME);

        assertEquals(TestData.Group.NAME, updatedGroup.getName());
        assertEquals(TestData.DIRECTORY_ID, updatedGroup.getDirectoryId());
        assertEquals(TestData.Group.TYPE, updatedGroup.getType());
        assertEquals(updatedIsActive, updatedGroup.isActive());
        assertEquals(updatedDescription, updatedGroup.getDescription());
    }

    @Test
    public void testRemoveGroup() throws Exception
    {
        groupDao.add(TestData.Group.getTestData());
        assertNotNull(groupDao.findByName(TestData.DIRECTORY_ID, TestData.Group.NAME));

        groupDao.remove(TestData.Group.getTestData());
        try
        {
            groupDao.findByName(TestData.DIRECTORY_ID, TestData.Group.NAME);
            fail("Should have thrown a user not found exception");
        }
        catch (GroupNotFoundException e)
        {
            assertEquals(TestData.Group.NAME, e.getGroupName());
        }
    }

    @Test
    public void testRemoveAttribute() throws Exception
    {
        final Group createdGroup = groupDao.add(TestData.Group.getTestData());
        groupDao.storeAttributes(createdGroup, TestData.Attributes.getTestData());

        TestData.Attributes.assertEqualsTestData(groupDao.findByNameWithAttributes(TestData.DIRECTORY_ID, TestData.Group.NAME));

        groupDao.removeAttribute(createdGroup, TestData.Attributes.ATTRIBUTE1);
        final GroupWithAttributes groupWithLessAttributes = groupDao.findByNameWithAttributes(TestData.DIRECTORY_ID, TestData.Group.NAME);

        assertNull(groupWithLessAttributes.getValue(TestData.Attributes.ATTRIBUTE1));
    }

    @Test
    public void testSearchAllGroupNames()
    {
        final String groupName2 = "group2";
        groupDao.add(TestData.Group.getTestData());
        groupDao.add(TestData.Group.getGroup(groupName2, DIRECTORY_ID, true, "d", GroupType.GROUP));

        @SuppressWarnings("unchecked")
        final GroupQuery<String> query = mock(GroupQuery.class);
        when(query.getReturnType()).thenReturn(String.class);

        final List<String> groupNames = groupDao.search(DIRECTORY_ID, query);
        assertThat(groupNames, containsInAnyOrder(TestData.Group.NAME, groupName2));
    }

    @Test
    public void testSearchGroupNamesWithRestriction()
    {
        final String groupName2 = "group2";
        groupDao.add(TestData.Group.getTestData());
        groupDao.add(TestData.Group.getGroup(groupName2, DIRECTORY_ID, true, "d", GroupType.GROUP));

        @SuppressWarnings("unchecked")
        final GroupQuery<String> query = mock(GroupQuery.class);
        when(query.getReturnType()).thenReturn(String.class);
        mockSearchRestriction(query);

        final List<String> groupNames = groupDao.search(DIRECTORY_ID, query);
        assertThat(groupNames, contains(TestData.Group.NAME));
    }

    @Test
    public void testSearchAllGroups()
    {
        final String groupName2 = "group2";
        final Group group1 = TestData.Group.getTestData();
        final Group group2 = TestData.Group.getGroup(groupName2, DIRECTORY_ID, true, "d", GroupType.GROUP);
        groupDao.add(group1);
        groupDao.add(group2);

        @SuppressWarnings("unchecked")
        final GroupQuery<Group> query = mock(GroupQuery.class);
        when(query.getReturnType()).thenReturn(Group.class);

        final List<Group> groups = groupDao.search(DIRECTORY_ID, query);
        assertThat(groups, containsInAnyOrder(group1, group2));
    }

    @Test
    public void testSearchGroupsWithRestriction()
    {
        final String groupName2 = "group2";
        final Group group1 = TestData.Group.getTestData();
        final Group group2 = TestData.Group.getGroup(groupName2, DIRECTORY_ID, true, "d", GroupType.GROUP);
        groupDao.add(group1);
        groupDao.add(group2);

        @SuppressWarnings("unchecked")
        final GroupQuery<Group> query = mock(GroupQuery.class);
        when(query.getReturnType()).thenReturn(Group.class);
        mockSearchRestriction(query);

        final List<Group> groups = groupDao.search(DIRECTORY_ID, query);
        assertThat(groups, contains(group1));
    }

    @Test
    public void testSearchAllGroupsWithAttributes() throws GroupNotFoundException
    {
        final Group group1 = TestData.Group.getTestData();
        final Map<String,Set<String>> group1attrs = TestData.Attributes.getTestData();

        final String groupName2 = "group2";
        final Group group2 = TestData.Group.getGroup(groupName2, DIRECTORY_ID, true, "d", GroupType.GROUP);

        groupDao.add(group1);
        groupDao.add(group2);
        groupDao.storeAttributes(group1, group1attrs);

        @SuppressWarnings("unchecked")
        final GroupQuery<GroupWithAttributes> query = mock(GroupQuery.class);
        when(query.getReturnType()).thenReturn(GroupWithAttributes.class);

        List<GroupWithAttributes> groups = groupDao.search(DIRECTORY_ID, query);

        // copy and reverse sort by name to guarantee a stable order
        groups = new ArrayList<GroupWithAttributes>(groups);
        Collections.sort(groups, new Comparator<GroupWithAttributes>()
        {
            @Override
            public int compare(final GroupWithAttributes o1, final GroupWithAttributes o2)
            {
                return o2.getName().compareTo(o1.getName());
            }
        });

        assertThat(groups, hasSize(2));
        final GroupWithAttributes foundGroup1 = groups.get(0);
        TestData.Group.assertEqualsTestGroup(foundGroup1);
        TestData.Attributes.assertEqualsTestData(foundGroup1);
        final GroupWithAttributes foundGroup2 = groups.get(1);
        assertThat(foundGroup2.getName(), equalTo(groupName2));
        assertThat("Expecting no attributes on group2", foundGroup2.getKeys(), hasSize(0));
    }

    @Test
    public void testSearchGroupsWithAttributesWithRestriction() throws GroupNotFoundException
    {
        final Group group1 = TestData.Group.getTestData();
        final Map<String,Set<String>> group1attrs = TestData.Attributes.getTestData();

        final String groupName2 = "group2";
        final Group group2 = TestData.Group.getGroup(groupName2, DIRECTORY_ID, true, "d", GroupType.GROUP);

        groupDao.add(group1);
        groupDao.add(group2);
        groupDao.storeAttributes(group1, group1attrs);

        @SuppressWarnings("unchecked")
        final GroupQuery<GroupWithAttributes> query = mock(GroupQuery.class);
        when(query.getReturnType()).thenReturn(GroupWithAttributes.class);
        mockSearchRestriction(query);

        List<GroupWithAttributes> groups = groupDao.search(DIRECTORY_ID, query);

        assertThat(groups, hasSize(1));
        final GroupWithAttributes foundGroup1 = groups.get(0);
        TestData.Group.assertEqualsTestGroup(foundGroup1);
        TestData.Attributes.assertEqualsTestData(foundGroup1);
    }

    @Test
    public void testRemoveAll()
    {
        final Group createdGroup = groupDao.add(TestData.Group.getTestData());

        BatchResult<String> batchResult = groupDao.removeAllGroups(DIRECTORY_ID,
                ImmutableSet.of(createdGroup.getName(), "non-existent-group"));

        assertThat(batchResult.getSuccessfulEntities(), contains(createdGroup.getName()));
        assertThat(batchResult.getFailedEntities(), contains("non-existent-group"));
    }

    // Note: Search restrictions prevent us from using the in-memory cache to implement a search.
    // We should test both code paths where possible.
    @SuppressWarnings("unchecked")
    private static void mockSearchRestriction(GroupQuery<?> query)
    {
        final Property<String> name = mock(Property.class);
        when(name.getPropertyName()).thenReturn(GroupEntity.NAME);
        when(name.getPropertyType()).thenReturn(String.class);

        final PropertyRestriction<String> restriction = mock(PropertyRestriction.class);
        when(restriction.getProperty()).thenReturn(name);
        when(restriction.getMatchMode()).thenReturn(MatchMode.STARTS_WITH);
        when(restriction.getValue()).thenReturn("T");

        when(query.getSearchRestriction()).thenReturn(restriction);
    }
}
