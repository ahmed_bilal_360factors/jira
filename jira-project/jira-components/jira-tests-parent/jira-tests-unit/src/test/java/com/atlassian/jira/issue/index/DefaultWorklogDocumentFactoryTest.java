package com.atlassian.jira.issue.index;

import com.atlassian.fugue.Option;
import com.atlassian.jira.index.SearchExtractorRegistrationManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.worklog.WorklogImpl;
import com.atlassian.jira.issue.worklog.WorklogManager;
import org.apache.lucene.document.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

@RunWith (MockitoJUnitRunner.class)
public final class DefaultWorklogDocumentFactoryTest
{
    @Mock
    private SearchExtractorRegistrationManager searchExtractorManager;

    @InjectMocks
    private DefaultWorklogDocumentFactory documentFactory;

    @Test
    public void ifAuthorIsNullThenItsNotAddedToTheDocument()
    {
        WorklogImpl worklog = new WorklogImpl(mock(WorklogManager.class), mock(Issue.class), 42L, null, "comment", new Date(), "groupLevel", 1L, 10L);
        Option<Document> document = documentFactory.apply(worklog);
        assertThat(document.get().getFieldable(DocumentConstants.WORKLOG_AUTHOR), nullValue());

    }

    @Test
    public void ifAuthorIsNotlThenItsNotAddedToTheDocument()
    {
        WorklogImpl worklog = new WorklogImpl(mock(WorklogManager.class), mock(Issue.class), 42L, "author", "comment", new Date(), "groupLevel", 1L, 10L);
        Option<Document> document = documentFactory.apply(worklog);
        assertThat(document.get().getFieldable(DocumentConstants.WORKLOG_AUTHOR), not(nullValue()));

    }
}
