package com.atlassian.jira.issue.attachment.store;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.concurrent.Callable;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.AttachmentDirectoryAccessor;
import com.atlassian.jira.issue.attachment.AttachmentFileGetData;
import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.NoAttachmentDataException;
import com.atlassian.jira.issue.attachment.StoreAttachmentBean;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.jira.util.ExecutorServiceWrapper;
import com.atlassian.jira.util.PathTraversalRuntimeException;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promises;

import org.apache.commons.io.IOUtils;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.mockito.ArgumentMatcher;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static com.atlassian.jira.matchers.FileMatchers.exists;
import static com.atlassian.jira.matchers.FileMatchers.hasContent;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultFileSystemAttachmentStore
{
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private DefaultFileSystemAttachmentStore attachmentStore;

    public static final String SAMPLE_ATTACHMENT_CONTENT = "sampleAttachmentContent";
    private AttachmentDirectoryAccessor directoryAccessor;
    private EventPublisher eventPublisher;

    private Matcher<JiraHomeChangeEvent> eventAddMatcher = new ArgumentMatcher<JiraHomeChangeEvent>()
    {
        @Override
        public boolean matches(final Object o)
        {
            JiraHomeChangeEvent event = (JiraHomeChangeEvent) o;
            return event.getAction() == JiraHomeChangeEvent.Action.FILE_ADD && event.getFileType() == JiraHomeChangeEvent.FileType.ATTACHMENT;
        }
    };

    private Matcher<JiraHomeChangeEvent> eventDeleteMatcher = new ArgumentMatcher<JiraHomeChangeEvent>()
    {
        @Override
        public boolean matches(final Object o)
        {
            JiraHomeChangeEvent event = (JiraHomeChangeEvent) o;
            return event.getAction() == JiraHomeChangeEvent.Action.FILE_DELETED && event.getFileType() == JiraHomeChangeEvent.FileType.ATTACHMENT;
        }
    };


    @Before
    public void setUp() throws Exception
    {
        final File attachmentRootDirectory = temporaryFolder.newFolder();
        final File temporaryFolder = this.temporaryFolder.newFolder();
        final PathTraversalChecker pathTraversalChecker = new PathTraversalChecker();
        final LocalTemporaryFileStore localTemporaryFileStore = new LocalTemporaryFileStore(temporaryFolder, pathTraversalChecker);
        directoryAccessor = mockDirectoryAccessor(attachmentRootDirectory, temporaryFolder);
        eventPublisher = mock(EventPublisher.class);
        attachmentStore = new DefaultFileSystemAttachmentStore(directoryAccessor, localTemporaryFileStore, getMockExecutor(), eventPublisher);
    }

    @Test
    public void testSuccessReadWriteScenario() throws Exception
    {
        final AttachmentKey attachmentKey = putSampleAttachmentToStore();

        verifyAttachmentWithContentExists(SAMPLE_ATTACHMENT_CONTENT, attachmentKey);
        verify(eventPublisher).publish(argThat(eventAddMatcher));
    }

    @Test
    public void providesFileHandlerToExistingFile() throws Exception
    {
        final AttachmentKey attachmentKey = putSampleAttachmentToStore();

        final File file = attachmentStore.getAttachmentData(attachmentKey, new Function<AttachmentGetData, File>()
        {
            @Override
            public File get(final AttachmentGetData attachmentGetData)
            {
                final AttachmentFileGetData attachmentFileGetData = (AttachmentFileGetData) attachmentGetData;
                return attachmentFileGetData.getFile();
            }
        }).claim();

        assertThat(file, exists());
        assertThat(file, hasContent(SAMPLE_ATTACHMENT_CONTENT));
    }

    @Test
    public void canReadFileAfterMoveFromNewKey() throws Exception
    {
        final AttachmentKey oldKey = getOldKey();
        final AttachmentKey newKey = getNewKey();

        final StoreAttachmentBean storeBean = getStoreBeanWithContent(SAMPLE_ATTACHMENT_CONTENT)
                .withKey(oldKey)
                .build();

        attachmentStore.putAttachment(storeBean).claim();
        attachmentStore.moveAttachment(oldKey, newKey).claim();
        verifyAttachmentWithContentExists(SAMPLE_ATTACHMENT_CONTENT, newKey);
    }

    @Test
    public void canAccessFileViaBothKeysWhenCopied() throws Exception
    {
        final AttachmentKey oldKey = getOldKey();
        final AttachmentKey newKey = getNewKey();

        final StoreAttachmentBean storeBean = getStoreBeanWithContent(SAMPLE_ATTACHMENT_CONTENT)
                .withKey(oldKey)
                .build();

        attachmentStore.putAttachment(storeBean).claim();
        attachmentStore.copyAttachment(oldKey, newKey).claim();
        verifyAttachmentWithContentExists(SAMPLE_ATTACHMENT_CONTENT, newKey);
        verifyAttachmentWithContentExists(SAMPLE_ATTACHMENT_CONTENT, oldKey);
    }

    @Test
    public void cannotReadFileAfterMoveFromOldKey() throws Exception
    {
        final AttachmentKey oldKey = getOldKey();
        final AttachmentKey newKey = getNewKey();

        final StoreAttachmentBean storeBean = getStoreBeanWithContent(SAMPLE_ATTACHMENT_CONTENT)
                .withKey(oldKey)
                .build();

        attachmentStore.putAttachment(storeBean).claim();
        attachmentStore.moveAttachment(oldKey, newKey).claim();

        expectedException.expect(NoAttachmentDataException.class);

        //noinspection unchecked
        attachmentStore.getAttachment(oldKey, mock(Function.class)).claim();
    }

    @Test
    public void cannotMoveFileWhenAttachmentDoesNotExist() throws Exception
    {
        final AttachmentKey oldKey = getOldKey();
        final AttachmentKey newKey = getNewKey();

        expectedException.expect(NoAttachmentDataException.class);
        attachmentStore.moveAttachment(oldKey, newKey).claim();
    }

    @Test
    public void createTemporaryAndMoveItToAttachment() throws Exception
    {
        final ByteArrayInputStream inputStream = getByteArrayInputStream(SAMPLE_ATTACHMENT_CONTENT);
        final TemporaryAttachmentId tempId = attachmentStore.putTemporaryAttachment(inputStream, inputStream.available()).claim();

        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();

        attachmentStore.moveTemporaryToAttachment(tempId, attachmentKey).claim();
        verifyAttachmentWithContentExists(SAMPLE_ATTACHMENT_CONTENT, attachmentKey);
    }

    @Test
    public void existsShouldReturnFalseWhenAttachmentIsMissing() throws Exception
    {
        final AttachmentKey notExistingAttachmentKey = MockAttachmentKeys.getKey();
        final Boolean exists = attachmentStore.exists(notExistingAttachmentKey).claim();

        assertThat(exists, equalTo(false));
    }

    @Test
    public void existsShouldReturnTrueWhenAttachmentIsExisting() throws Exception
    {
        final AttachmentKey attachmentKey = putSampleAttachmentToStore();
        final Boolean exists = attachmentStore.exists(attachmentKey).claim();

        assertThat(exists, equalTo(true));
    }

    @Test
    public void deleteAttachment() throws Exception
    {
        final AttachmentKey attachmentKey = putSampleAttachmentToStore();

        verifyAttachmentWithContentExists(SAMPLE_ATTACHMENT_CONTENT, attachmentKey);

        reset(eventPublisher);

        attachmentStore.deleteAttachment(attachmentKey).claim();

        final Boolean exists = attachmentStore.exists(attachmentKey).claim();
        assertThat(exists, equalTo(false));

        verify(eventPublisher).publish(argThat(eventDeleteMatcher));
    }

    @Test
    public void testIgnoreDeletingNotExistingAttachment() throws Exception
    {
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();

        attachmentStore.deleteAttachment(attachmentKey).claim();
    }

    @Test
    public void testDeletingAttachmentContainerForIssue() throws Exception
    {
        final Issue issue = mock(Issue.class);

        final File issueAttachmentFolder = temporaryFolder.newFolder();
        when(directoryAccessor.getAttachmentDirectory(issue))
                .thenReturn(issueAttachmentFolder);

        attachmentStore.deleteAttachmentContainerForIssue(issue).claim();

        assertThat("Issue attachments folder should get deleted", issueAttachmentFolder.exists(), equalTo(false));
    }

    @Test
    public void testDetectPathTraversalAttackOnTemporaryId() throws Exception
    {
        final TemporaryAttachmentId temporaryId = TemporaryAttachmentId.fromString("../../../../../../etc/passwd");

        expectedException.expect(PathTraversalRuntimeException.class);

        attachmentStore.moveTemporaryToAttachment(temporaryId, getNewKey()).claim();
    }

    private AttachmentKey putSampleAttachmentToStore()
    {
        final StoreAttachmentBean storeAttachmentBean = getStoreBeanWithContent(SAMPLE_ATTACHMENT_CONTENT)
                .withId(1l).build();

        attachmentStore.putAttachment(storeAttachmentBean).claim();

        return storeAttachmentBean.getAttachmentKey();
    }

    private AttachmentKey getNewKey()
    {
        return AttachmentKey.builder()
                .withIssueKey("NEW-2")
                .withProjectKey("NEW")
                .withAttachmentId(2l)
                .withAttachmentFilename("otherName")
                .build();
    }

    private AttachmentKey getOldKey()
    {
        return AttachmentKey.builder()
                .withIssueKey("OLD-1")
                .withProjectKey("OLD")
                .withAttachmentId(1l)
                .withAttachmentFilename("fileName")
                .build();
    }

    private void verifyAttachmentWithContentExists(final String expectedContent, final AttachmentKey attachmentKey)
    {
        final Function<InputStream, String> verifyContentProcessor = new Function<InputStream, String>()
        {
            @Override
            public String get(final InputStream input)
            {
                try
                {
                    return IOUtils.toString(input, "UTF-8");
                }
                catch (final IOException e)
                {
                    throw new RuntimeException(e);
                }
            }
        };

        final String attachmentContent = attachmentStore.getAttachment(attachmentKey, verifyContentProcessor).claim();
        assertThat(attachmentContent, equalTo(expectedContent));
    }

    private StoreAttachmentBean.Builder getStoreBeanWithContent(final String sampleAttachmentContent)
    {
        final ByteArrayInputStream byteArrayInputStream = getByteArrayInputStream(sampleAttachmentContent);

        return new StoreAttachmentBean.Builder(byteArrayInputStream)
                .withFileName("sampleName")
                .withOriginalProjectKey("FOO")
                .withIssueKey("FOO-1")
                .withSize((long) byteArrayInputStream.available());
    }

    private ByteArrayInputStream getByteArrayInputStream(final String sampleAttachmentContent)
    {
        final byte[] bytes = sampleAttachmentContent.getBytes(Charset.forName("UTF-8"));
        return new ByteArrayInputStream(bytes);
    }

    private AttachmentDirectoryAccessor mockDirectoryAccessor(final File attachmentRootDirectory, final File temporaryFolder)
    {
        final AttachmentDirectoryAccessor directoryAccessor = mock(AttachmentDirectoryAccessor.class);

        when(directoryAccessor.getAttachmentRootPath())
                .thenReturn(attachmentRootDirectory);

        when(directoryAccessor.getTemporaryAttachmentDirectory())
                .thenReturn(temporaryFolder);

        return directoryAccessor;
    }

    public ExecutorServiceWrapper getMockExecutor()
    {
        final ExecutorServiceWrapper mock = mock(ExecutorServiceWrapper.class);

        doAnswer(new Answer()
        {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                final Callable callable = (Callable) invocation.getArguments()[0];
                try
                {
                    final Object result = callable.call();
                    return Promises.promise(result);
                }
                catch (final Exception exception)
                {
                    return Promises.rejected(exception);
                }
            }
        }).when(mock).submit(any(Callable.class));
        return mock;
    }
}