package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.junit.rules.MockHttp;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.mockobjects.servlet.MockHttpServletResponse;
import org.easymock.MockControl;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestUserIsSysAdminCondition
{
    @Rule
    public final MockHttp mockHttp = MockHttp.withMocks(new MockHttpServletRequest(), new MockHttpServletResponse());

    final ApplicationUser user = new MockApplicationUser("name");

    @Test
    public void testAnonymousUserIsNotSystemAdmin() throws Exception
    {
        final MockControl mockControl = MockControl.createControl(PermissionManager.class);
        final PermissionManager mock = (PermissionManager) mockControl.getMock();
        mockControl.expectAndReturn(mock.hasPermission(Permissions.SYSTEM_ADMIN, (ApplicationUser) null), false);
        mockControl.replay();
        assertFalse(new UserIsSysAdminCondition(mock).shouldDisplay(null, null));
        mockControl.verify();
    }

    @Test
    public void testAnonymousUserIsSystemAdmin() throws Exception
    {
        final MockControl mockControl = MockControl.createControl(PermissionManager.class);
        final PermissionManager mock = (PermissionManager) mockControl.getMock();
        mockControl.expectAndReturn(mock.hasPermission(Permissions.SYSTEM_ADMIN, (ApplicationUser) null), true);
        mockControl.replay();
        assertTrue(new UserIsSysAdminCondition(mock).shouldDisplay(null, null));
        mockControl.verify();
    }

    @Test
    public void testUserIsNotSystemAdmin() throws Exception
    {

        final MockControl mockControl = MockControl.createControl(PermissionManager.class);
        final PermissionManager mock = (PermissionManager) mockControl.getMock();
        mockControl.expectAndReturn(mock.hasPermission(Permissions.SYSTEM_ADMIN, user), false);
        mockControl.replay();
        assertFalse(new UserIsSysAdminCondition(mock).shouldDisplay(user, null));
        mockControl.verify();
    }

    @Test
    public void testUserIsSystemAdmin() throws Exception
    {
        final MockControl mockControl = MockControl.createControl(PermissionManager.class);
        final PermissionManager mock = (PermissionManager) mockControl.getMock();
        mockControl.expectAndReturn(mock.hasPermission(Permissions.SYSTEM_ADMIN, user), true);
        mockControl.replay();
        assertTrue(new UserIsSysAdminCondition(mock).shouldDisplay(user, null));
        mockControl.verify();
    }

}
