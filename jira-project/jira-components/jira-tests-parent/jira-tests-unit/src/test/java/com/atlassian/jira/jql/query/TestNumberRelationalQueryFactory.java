package com.atlassian.jira.jql.query;

import java.util.Collections;

import com.atlassian.jira.issue.customfields.converters.DoubleConverterImpl;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.mock.security.MockAuthenticationContext;
import com.atlassian.query.operator.Operator;

import org.apache.lucene.util.NumericUtils;
import org.junit.Test;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

/**
 * @since v4.0
 */
public class TestNumberRelationalQueryFactory
{
    private static final String FIELD_NAME = "durationField";

    @Test
    public void testDoesNotSupportOperators() throws Exception
    {
        _testDoesNotSupportOperator(Operator.EQUALS);
        _testDoesNotSupportOperator(Operator.NOT_EQUALS);
        _testDoesNotSupportOperator(Operator.LIKE);
        _testDoesNotSupportOperator(Operator.IN);
        _testDoesNotSupportOperator(Operator.IS);
    }

    private void _testDoesNotSupportOperator(Operator operator) throws Exception
    {
        final NumberRelationalQueryFactory factory = createFactory(0.0);

        final QueryFactoryResult result = factory.createQueryForSingleValue("testField", operator, Collections.singletonList(createLiteral(1000L)));
        assertEquals(QueryFactoryResult.createFalseResult(), result);
    }

    @Test
    public void testCreateForMultipleValues() throws Exception
    {
        final NumberRelationalQueryFactory factory = createFactory(0.0);
        final QueryFactoryResult result = factory.createQueryForMultipleValues("testField", Operator.IN, Collections.singletonList(createLiteral(1000L)));
        QueryFactoryResult expectedResult = QueryFactoryResult.createFalseResult();
        assertEquals(expectedResult, result);
    }

    @Test
    public void testCreateForEmptyOperand() throws Exception
    {
        final NumberRelationalQueryFactory factory = createFactory(0.0);
        final QueryFactoryResult result = factory.createQueryForEmptyOperand("testField", Operator.IN);
        QueryFactoryResult expectedResult = QueryFactoryResult.createFalseResult();
        assertEquals(expectedResult, result);
    }

    @Test
    public void testCreateForSingleValueEmptyLiteral() throws Exception
    {
        final NumberRelationalQueryFactory factory = createFactory(0.0);
        final QueryFactoryResult result = factory.createQueryForSingleValue("testField", Operator.LESS_THAN, Collections.singletonList(new QueryLiteral()));
        QueryFactoryResult expectedResult = QueryFactoryResult.createFalseResult();
        assertEquals(expectedResult, result);
    }

    @Test
    public void testLessThanWithEmptyIndexValue() throws Exception
    {
        assertQueryHappy(0.0, Operator.LESS_THAN, "+sort_durationField:[* TO " + NumericUtils.doubleToPrefixCoded(30.0) + "} -sort_durationField:" + NumericUtils.doubleToPrefixCoded(0.0), 30L);
        assertQueryHappy(0.0, Operator.LESS_THAN, "+sort_durationField:[* TO " + NumericUtils.doubleToPrefixCoded(30.0) + "} -sort_durationField:" + NumericUtils.doubleToPrefixCoded(0.0), "30");
        assertQueryHappy(0.0, Operator.LESS_THAN, "+sort_durationField:[* TO " + NumericUtils.doubleToPrefixCoded(-30.0) + "} -sort_durationField:" + NumericUtils.doubleToPrefixCoded(0.0), -30L);
        assertQueryHappy(0.0, Operator.LESS_THAN, "+sort_durationField:[* TO " + NumericUtils.doubleToPrefixCoded(-30.0) + "} -sort_durationField:" + NumericUtils.doubleToPrefixCoded(0.0), "-30");
    }

    @Test
    public void testLessThan() throws Exception
    {
        assertQueryHappy(Operator.LESS_THAN, "sort_durationField:[* TO " + NumericUtils.doubleToPrefixCoded(30.0) + "}", 30L);
        assertQueryHappy(Operator.LESS_THAN, "sort_durationField:[* TO " + NumericUtils.doubleToPrefixCoded(30.0) + "}", "30");
        assertQueryHappy(Operator.LESS_THAN, "sort_durationField:[* TO " + NumericUtils.doubleToPrefixCoded(-30.0) + "}", -30L);
        assertQueryHappy(Operator.LESS_THAN, "sort_durationField:[* TO " + NumericUtils.doubleToPrefixCoded(-30.0) + "}", "-30");
    }

    @Test
    public void testLessThanEquals() throws Exception
    {
        assertQueryHappy(Operator.LESS_THAN_EQUALS, "sort_durationField:[* TO " + NumericUtils.doubleToPrefixCoded(30.0) + "]", 30L);
        assertQueryHappy(Operator.LESS_THAN_EQUALS, "sort_durationField:[* TO " + NumericUtils.doubleToPrefixCoded(30.0) + "]", "30");
        assertQueryHappy(Operator.LESS_THAN_EQUALS, "sort_durationField:[* TO " + NumericUtils.doubleToPrefixCoded(-30.0) + "]", -30L);
        assertQueryHappy(Operator.LESS_THAN_EQUALS, "sort_durationField:[* TO " + NumericUtils.doubleToPrefixCoded(-30.0) + "]", "-30");
    }

    @Test
    public void testGreaterThan() throws Exception
    {
        assertQueryHappy(Operator.GREATER_THAN, "sort_durationField:{" + NumericUtils.doubleToPrefixCoded(30.0) + " TO *]", 30L);
        assertQueryHappy(Operator.GREATER_THAN, "sort_durationField:{" + NumericUtils.doubleToPrefixCoded(30.0) + " TO *]", "30");
        assertQueryHappy(Operator.GREATER_THAN, "sort_durationField:{" + NumericUtils.doubleToPrefixCoded(-30.0) + " TO *]", -30L);
        assertQueryHappy(Operator.GREATER_THAN, "sort_durationField:{" + NumericUtils.doubleToPrefixCoded(-30.0) + " TO *]", "-30");
    }

    @Test
    public void testGreaterThanEquals() throws Exception
    {
        assertQueryHappy(Operator.GREATER_THAN_EQUALS, "sort_durationField:[" + NumericUtils.doubleToPrefixCoded(30.0) + " TO *]", 30L);
        assertQueryHappy(Operator.GREATER_THAN_EQUALS, "sort_durationField:[" + NumericUtils.doubleToPrefixCoded(30.0) + " TO *]", "30");
        assertQueryHappy(Operator.GREATER_THAN_EQUALS, "sort_durationField:[" + NumericUtils.doubleToPrefixCoded(-30.0) + " TO *]", -30L);
        assertQueryHappy(Operator.GREATER_THAN_EQUALS, "sort_durationField:[" + NumericUtils.doubleToPrefixCoded(-30.0) + " TO *]", "-30");
    }

    private void assertQueryHappy(Operator op, String luceneQuery, final Object value)
    {
        assertQueryHappy(null, op, luceneQuery, value);
    }

    private void assertQueryHappy(final Double emptyIndexValue, Operator op, String luceneQuery, final Object value)
    {
        final NumberRelationalQueryFactory factory = createFactory(emptyIndexValue);

        QueryFactoryResult query = factory.createQueryForSingleValue(FIELD_NAME, op, Collections.singletonList(createQL(value)));
        assertFalse(query.mustNotOccur());
        assertNotNull(query.getLuceneQuery());
        assertEquals(luceneQuery, query.getLuceneQuery().toString());
    }

    private QueryLiteral createQL(Object value)
    {
        if (value instanceof String)
        {
            return createLiteral((String) value);
        }
        else if (value instanceof Long)
        {
            return createLiteral((Long) value);
        }
        throw new IllegalArgumentException();
    }

    private static NumberRelationalQueryFactory createFactory(final Double emptyIndexValue)
    {
        return new NumberRelationalQueryFactory(new DoubleConverterImpl(new MockAuthenticationContext(null)), emptyIndexValue);
    }
}
