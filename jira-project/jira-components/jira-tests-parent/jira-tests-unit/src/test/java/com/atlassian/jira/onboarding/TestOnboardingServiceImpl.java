package com.atlassian.jira.onboarding;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.jira.bc.security.login.LoginInfo;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.module.ModuleFactory;

import com.google.common.collect.Lists;

import org.dom4j.Element;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestOnboardingServiceImpl
{
    final String USERNAME = "username";

    @Mock
    private ApplicationUser user;

    @Mock
    PluginAccessor pluginAccessor;

    @Mock
    OnboardingStore onboardingStore;

    @Mock
    LoginService loginService;

    @Mock
    LoginInfo loginInfo;

    @Mock
    GlobalPermissionManager globalPermissionManager;

    private OnboardingServiceImpl onboardingService;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    ModuleFactory moduleFactory;

    @Mock
    FeatureManager featureManager;


    /**
     * Generates a descriptor with a given weight and flow.
     *
     * @param flow generator for descriptor
     * @param weight for the descriptor
     * @return descriptor representing flow
     */
    private FirstUseFlowModuleDescriptor mockDescriptor(final @Nonnull FirstUseFlow flow, @Nullable String weight)
    {
        Plugin plugin = mock(Plugin.class);
        Element element = mock(Element.class);
        when(element.attributeValue("weight")).thenReturn(weight);

        FirstUseFlowModuleDescriptor descriptor = spy(new FirstUseFlowModuleDescriptor(authenticationContext, moduleFactory));
        descriptor.init(plugin, element);

        doReturn(flow).when(descriptor).getModule();

        return descriptor;
    }

    private FirstUseFlow mockFirstUseFlow()
    {
        FirstUseFlow flow = mock(FirstUseFlow.class);
        when(flow.isApplicable(Mockito.any(ApplicationUser.class))).thenReturn(true);
        return flow;
    }

    private void markStartedFirstUseFlow(final String key)
    {
        when(onboardingStore.isSet(user, OnboardingStore.STARTED_FLOW_KEY)).thenReturn(true);
        when(onboardingStore.getString(user, OnboardingStore.STARTED_FLOW_KEY)).thenReturn(key);
    }

    @Before
    public void init()
    {
        onboardingService = spy(new OnboardingServiceImpl(pluginAccessor, onboardingStore, globalPermissionManager, featureManager, loginService));

        when(onboardingStore.isSet(user, OnboardingStore.STARTED_FLOW_KEY)).thenReturn(false);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, user)).thenReturn(false);
        when(featureManager.isEnabled(OnboardingService.DARK_FEATURE_DISABLE_ONBOARDING_FLAG)).thenReturn(false);
        when(featureManager.isEnabled(OnboardingService.DARK_FEATURE_DISABLE_USER_CHECKS_FLAG)).thenReturn(false);

        when(user.getUsername()).thenReturn(USERNAME);
        when(loginService.getLoginInfo(USERNAME)).thenReturn(loginInfo);
        when(loginInfo.getLoginCount()).thenReturn(1L);
    }

    @Test
    public void getFirstUseFlowShouldReturnNullIfNoFlows()
    {
        //Setup
        final List<FirstUseFlowModuleDescriptor> empty = Lists.newArrayList();
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(empty);

        //Execute
        FirstUseFlow flow = onboardingService.getFirstUseFlow(user);

        //Test
        assertNull("Should have no flow to return", flow);
    }

    @Test
    public void getFirstUseFlowShouldReturnFlowIfByItself()
    {
        //Setup
        FirstUseFlow flow = mockFirstUseFlow();
        FirstUseFlowModuleDescriptor flowDescriptor = mockDescriptor(flow, "2");
        final List<FirstUseFlowModuleDescriptor> list = Lists.newArrayList(flowDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(list);

        //Execute
        FirstUseFlow result = onboardingService.getFirstUseFlow(user);

        //Test
        assertEquals("Should have picked the largest weight flow", flow, result);
    }

    @Test
    public void getFirstUseFlowShouldReturnresult()
    {
        FirstUseFlow otherFlow = mockFirstUseFlow();
        FirstUseFlow largestFlow = mockFirstUseFlow();
        FirstUseFlowModuleDescriptor smallestFlowDescriptor = mockDescriptor(otherFlow, "2");
        FirstUseFlowModuleDescriptor middleFlowDescriptor = mockDescriptor(otherFlow, "5");
        FirstUseFlowModuleDescriptor largestFlowDescriptor = mockDescriptor(largestFlow, "7");
        List<FirstUseFlowModuleDescriptor> list = Lists.newArrayList(smallestFlowDescriptor, middleFlowDescriptor, largestFlowDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(list);

        //Loop used to test all combinations of ordering.
        for (int i = 0; i < list.size(); ++i)
        {
            FirstUseFlow flow = onboardingService.getFirstUseFlow(user);

            assertEquals("Should have picked the largest weight flow", largestFlowDescriptor.getModule(), flow);

            Collections.rotate(list, 1);
        }
    }

    @Test
    public void unweightedFlowsCanStillBeReceived()
    {
        //Setup
        FirstUseFlow flow = mockFirstUseFlow();
        FirstUseFlowModuleDescriptor unweightedFlow = mockDescriptor(flow, null);
        List<FirstUseFlowModuleDescriptor> list = Lists.newArrayList(unweightedFlow);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(list);

        //Execute
        FirstUseFlow result = onboardingService.getFirstUseFlow(user);

        //Test
        assertEquals("Should have picked the largest weight flow", flow, result);
    }

    @Test
    public void weightedBeatsUnweightFlowsCanStillBeReceived()
    {
        //Setup
        FirstUseFlow unweightedFlow = mockFirstUseFlow();
        FirstUseFlow weightedFlow = mockFirstUseFlow();
        FirstUseFlowModuleDescriptor unweightedFlowDescriptor = mockDescriptor(unweightedFlow, null);
        FirstUseFlowModuleDescriptor weightedFlowDescriptor = mockDescriptor(weightedFlow, "0");
        List<FirstUseFlowModuleDescriptor> list = Lists.newArrayList(unweightedFlowDescriptor, weightedFlowDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(list);

        //Execute
        FirstUseFlow result = onboardingService.getFirstUseFlow(user);

        //Test
        assertEquals("Should have picked the largest weight flow", weightedFlow, result);
    }

    @Test
    public void userShouldReturnToTheFlowCurrentlyStoredAsUserProperty()
    {
        // preparing a started flow
        final String STARTED_KEY = "started-flow";
        FirstUseFlow startedFlow = mockFirstUseFlow();
        FirstUseFlowModuleDescriptor startedFlowDescriptor = mockDescriptor(startedFlow, "100");
        when(startedFlowDescriptor.getKey()).thenReturn(STARTED_KEY);
        markStartedFirstUseFlow(STARTED_KEY);

        // add it to the list of flow module descriptors
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(Arrays.asList(startedFlowDescriptor));

        // let's try to retrieve it
        FirstUseFlow result = onboardingService.getFirstUseFlow(user);

        assertEquals("returned flow should be the one marked as started", startedFlow, result);
    }

    @Test
    public void userPropertySetForStartedFlowShouldGetRightFlowEvenIfThereIsAnotherFlowWithHigherWeight()
    {
        //Setup
        final String STARTED_KEY = "some_key";
        final String UNSTARTED_KEY = "other_key";

        FirstUseFlow startedFlow = mockFirstUseFlow();
        FirstUseFlowModuleDescriptor startedFlowDescriptor = mockDescriptor(startedFlow, "5");
        when(startedFlowDescriptor.getKey()).thenReturn(STARTED_KEY);

        FirstUseFlow unstartedFlow = mockFirstUseFlow();
        FirstUseFlowModuleDescriptor unstartedFlowDescriptor = mockDescriptor(unstartedFlow, "10");
        when(unstartedFlowDescriptor.getKey()).thenReturn(UNSTARTED_KEY);

        markStartedFirstUseFlow(STARTED_KEY);

        List<FirstUseFlowModuleDescriptor> list = Lists.newArrayList(unstartedFlowDescriptor, startedFlowDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(list);


        //Execute
        FirstUseFlow result = onboardingService.getFirstUseFlow(user);


        //Test
        assertEquals("Should have picked the stored flow", startedFlow, result);
    }

    @Test
    public void testEqualWeightedPicksFirst()
    {
        FirstUseFlow otherFlow = mockFirstUseFlow();
        FirstUseFlow largestFlow = mockFirstUseFlow();
        FirstUseFlowModuleDescriptor smallestFlowDescriptor = mockDescriptor(otherFlow, "2");
        FirstUseFlowModuleDescriptor middleFlowDescriptor = mockDescriptor(otherFlow, "7");
        FirstUseFlowModuleDescriptor largestFlowDescriptor = mockDescriptor(largestFlow, "7");
        List<FirstUseFlowModuleDescriptor> list = Lists.newArrayList(smallestFlowDescriptor, middleFlowDescriptor, largestFlowDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(list);

        FirstUseFlow flow = onboardingService.getFirstUseFlow(user);
        assertEquals("Should have picked the first largest weighted flow", middleFlowDescriptor.getModule(), flow);
    }

    @Test
    public void testNoFlowReturnedIfUserHasCompletedOneBefore()
    {
        when(onboardingStore.getBoolean(user, OnboardingStore.FIRST_USE_FLOW_COMPLETED)).thenReturn(true);

        FirstUseFlowModuleDescriptor descriptor = mockDescriptor(mockFirstUseFlow(), "10");
        List<FirstUseFlowModuleDescriptor> list = Lists.newArrayList(descriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(list);

        //Execute
        FirstUseFlow result = onboardingService.getFirstUseFlow(user);


        //Test
        assertNull("Should not pick any flow since the user has completed one", result);
    }

    @Test
    public void startedFlowNotFoundShouldReturnNull()
    {
        //Setup
        final String STARTED_KEY = "some_key";
        final String UNSTARTED_KEY = "other_key";

        FirstUseFlow largestFlow = mockFirstUseFlow();
        FirstUseFlowModuleDescriptor largestFlowDescriptor = mockDescriptor(largestFlow, "7");
        when(largestFlowDescriptor.getKey()).thenReturn(UNSTARTED_KEY);

        markStartedFirstUseFlow(STARTED_KEY);

        List<FirstUseFlowModuleDescriptor> list = Lists.newArrayList(largestFlowDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(list);


        //Execute
        FirstUseFlow result = onboardingService.getFirstUseFlow(user);


        //Test
        assertNull("Should not return another flow", result);
    }


    @Test
    public void sysAdminGetsAFirstUseFlowInBTF()
    {
        //Setup
        FirstUseFlow flow = mockFirstUseFlow();
        FirstUseFlowModuleDescriptor flowDescriptor = mockDescriptor(flow, "2");
        final List<FirstUseFlowModuleDescriptor> list = Lists.newArrayList(flowDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(list);

        when(globalPermissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, user)).thenReturn(true);

        //Execute
        FirstUseFlow result = onboardingService.getFirstUseFlow(user);

        //Test
        assertEquals("Sysadmin should have a first use flow to complete", flow, result);
    }

    @Test
    public void sysAdminDoesNotGetAFirstUseFlowInOnDemand()
    {
        //Setup
        FirstUseFlow flow = mockFirstUseFlow();
        FirstUseFlowModuleDescriptor flowDescriptor = mockDescriptor(flow, "2");
        final List<FirstUseFlowModuleDescriptor> list = Lists.newArrayList(flowDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(list);
        when(featureManager.isOnDemand()).thenReturn(true);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, user)).thenReturn(true);

        //Execute
        FirstUseFlow result = onboardingService.getFirstUseFlow(user);

        //Test
        assertNull("Sysadmin should not have a first use flow to complete", result);
    }

    @Test
    public void testFirstUseFlowCanVetoBeingChosen() throws Exception
    {
        FirstUseFlow firstFlow = mockFirstUseFlow();
        FirstUseFlow secondFlow = mockFirstUseFlow();
        when(firstFlow.isApplicable(user)).thenReturn(false);

        FirstUseFlowModuleDescriptor firstDescriptor = mockDescriptor(firstFlow, "100");
        FirstUseFlowModuleDescriptor secondDescriptor = mockDescriptor(secondFlow, "10");
        final List<FirstUseFlowModuleDescriptor> list = Lists.newArrayList(firstDescriptor, secondDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(list);

        FirstUseFlow result = onboardingService.getFirstUseFlow(user);

        assertEquals("The higher weighted descriptor should have vetoed being chosen", secondFlow, result);
    }

    @Test
    public void testUserWithNoFirstUseFlowAndAlreadyLoggedInDoesNotStartAFlow()
    {
        //Setup
        FirstUseFlow flow = mockFirstUseFlow();
        FirstUseFlowModuleDescriptor flowDescriptor = mockDescriptor(flow, "2");
        final List<FirstUseFlowModuleDescriptor> list = Lists.newArrayList(flowDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(list);
        when(loginInfo.getLoginCount()).thenReturn(2L);


        //Execute
        FirstUseFlow result = onboardingService.getFirstUseFlow(user);

        //Test
        assertNull("Should not start a flow if haven't already and already logged in previously", result);
    }

    /**
     * There are some circumstances under which we may not get the login count information for a user. They include
     * scenarios where JIRA is not the authenticator, such as SSO (Atlassian ID). In any situation where we get a login
     * count of null or zero, we should not get any flows.
     *
     * @see <a href="https://jdog.jira-dev.com/browse/JDEV-32063">JDEV-32063</a>
     */
    @Test
    public void testUserWhoNoLoginCountInfoShouldNotBeOnboarded()
    {
        //Setup
        FirstUseFlow flow = mockFirstUseFlow();
        FirstUseFlowModuleDescriptor flowDescriptor = mockDescriptor(flow, "2");
        final List<FirstUseFlowModuleDescriptor> list = Lists.newArrayList(flowDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(list);

        //Execute
        when(loginInfo.getLoginCount()).thenReturn(null);
        FirstUseFlow resultOnNull = onboardingService.getFirstUseFlow(user);

        when(loginInfo.getLoginCount()).thenReturn(0L);
        FirstUseFlow resultOnZero = onboardingService.getFirstUseFlow(user);

        //Test
        assertNull("Should not have chosen any flow, since the user could be from an LDAP server without login count info", resultOnNull);
        assertNull("Should not have chosen any flow, since the user could be from an LDAP server without login count info", resultOnZero);
    }

    @Test
    public void testNoLoginCountDarkFeatureStartsFlowRegardlessOfLoginCount()
    {
        //Setup
        FirstUseFlow flow = mockFirstUseFlow();
        FirstUseFlowModuleDescriptor flowDescriptor = mockDescriptor(flow, "2");
        final List<FirstUseFlowModuleDescriptor> list = Lists.newArrayList(flowDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(list);
        when(featureManager.isEnabled(OnboardingService.DARK_FEATURE_DISABLE_USER_CHECKS_FLAG)).thenReturn(true);
        when(loginInfo.getLoginCount()).thenReturn(2L);


        //Execute
        FirstUseFlow result = onboardingService.getFirstUseFlow(user);

        //Test
        assertEquals("Should start a flow if no login count check dark feature enabled", flow, result);
    }

    @Test
    public void testFlowStartedDoesNotStoreResolvedProperty()
    {
        // setup: user with a loginCount of 1 should see the onboarding, therefore resolved property should not be stored
        FirstUseFlow flow = mockFirstUseFlow();
        FirstUseFlowModuleDescriptor flowDescriptor = mockDescriptor(flow, "2");
        final List<FirstUseFlowModuleDescriptor> list = Lists.newArrayList(flowDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(list);
        when(loginInfo.getLoginCount()).thenReturn(1L);

        FirstUseFlow result = onboardingService.getFirstUseFlow(user);

        // resolved property must not be stored as the onboarding is in progress
        verify(onboardingStore, times(0)).setBoolean(user, OnboardingStore.FIRST_USE_FLOW_RESOLVED, true);
        assertNotNull("result should not be null as the onboarding will be displayed for this user", result);
    }

    @Test
    public void testResolvedPropertyIsSetWhenOnboardingIsNotShown()
    {
        // setup: user with a loginCount of 2 should not see the onboarding, therefore resolved property should be stored
        FirstUseFlow flow = mockFirstUseFlow();
        FirstUseFlowModuleDescriptor flowDescriptor = mockDescriptor(flow, "2");
        final List<FirstUseFlowModuleDescriptor> list = Lists.newArrayList(flowDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class)).thenReturn(list);
        when(loginInfo.getLoginCount()).thenReturn(2L);

        FirstUseFlow result = onboardingService.getFirstUseFlow(user);

        // resolved property should have set as true
        verify(onboardingStore, times(1)).setBoolean(user, OnboardingStore.FIRST_USE_FLOW_RESOLVED, true);
        assertNull("result should be null as the onboarding will not be displayed for this user", result);
    }

    @Test
    public void testFirstUseFlowIsNullWhenResolvedPropertyIsStoredAsTrue()
    {
        // prepare the tested object to return the resolved property as true
        when(onboardingStore.getBoolean(user, OnboardingStore.FIRST_USE_FLOW_RESOLVED)).thenReturn(true);

        FirstUseFlow result = onboardingService.getFirstUseFlow(user);

        assertNull("result should be null as the onboarding will not be displayed for this user", result);
    }
}
