package com.atlassian.jira;

import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.attachment.Attachment;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public final class TestFixtures {
    public static Attachment mockAttachment(long id)
    {
        Attachment attachment = mock(Attachment.class);
        when(attachment.getId()).thenReturn(id);
        return attachment;
    }

    public static Attachment mockAttachment(long issueId, String mimeType, String filename, long filesize, boolean isThumbnailable)
    {
        Attachment attachment = mock(Attachment.class);
        when(attachment.getIssueId()).thenReturn(issueId);
        when(attachment.getMimetype()).thenReturn(mimeType);
        when(attachment.getFilename()).thenReturn(filename);
        when(attachment.getFilesize()).thenReturn(filesize);
        when(attachment.isThumbnailable()).thenReturn(isThumbnailable);
        return attachment;
    }

    public static MutableIssue mockMutableIssue(long id) {
        MutableIssue mutableIssue = mock(MutableIssue.class);
        when(mutableIssue.getId()).thenReturn(id);
        return mutableIssue;
    }
}
