package com.atlassian.jira.setup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.setup.SetupStrategy.Status;
import com.atlassian.jira.util.concurrent.Gate;

import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableMap;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class TestAsynchronousJiraSetup
{
    @Rule
    public MockitoContainer mockitoMocksInContainer = MockitoMocksInContainer.rule(this);

    @AvailableInContainer
    @Mock
    private JiraAuthenticationContext authenticationContext;

    enum Step
    {
        step1,
        step2
    }

    public static final int REPEAT_CONCURRENT = 10;
    public static final SetupStrategy<Void, Step> DO_NOTHING_STRATEGY = new SetupStrategy<Void, Step>()
    {
        @Override
        public void setup(final Void nothing, final StepSwitcher switcher)
                throws Exception
        {
            //do nothing
        }

        @Override
        public ImmutableMap<Step, Status> getInitialSteps()
        {
            return ImmutableMap.of(Step.step1, Status.PENDING);
        }
    };
    public static final SetupStrategy.StepTask DO_NOTHING_TASK = new SetupStrategy.StepTask()
    {
        @Override
        public void run() throws Exception
        {
            //do nothing
        }
    };

    @Test (expected = RuntimeException.class)
    public void testSetupFinishedCanNotBeAccessedWithoutSessionId() throws Exception
    {
        final AsynchronousJiraSetup<Void, Step> setup = new AsynchronousJiraSetup<Void, Step>(DO_NOTHING_STRATEGY);
        final String sessionId = "ID";
        setup.isSetupFinished(sessionId);
    }

    @Test (expected = RuntimeException.class)
    public void testGetStatusOnceStepIsDoneCanNotBeAccessedWithoutSessionId() throws Exception
    {
        final AsynchronousJiraSetup<Void, Step> setup = new AsynchronousJiraSetup<Void, Step>(DO_NOTHING_STRATEGY);
        final String sessionId = "ID";
        setup.getStatusOnceStepIsDone(sessionId, Step.step1);
    }

    @Test
    public void testSetupFinishedCanBeAccessedWithSessionId() throws Exception
    {
        final AsynchronousJiraSetup<Void, Step> setup = new AsynchronousJiraSetup<Void, Step>(DO_NOTHING_STRATEGY);
        final String sessionId = "ID";
        setup.setupJIRA(sessionId, null);
        setup.isSetupFinished(sessionId);
    }

    @Test
    public void testGetStatusOnceStepIsDoneCanBeAccessedWithSessionId() throws Exception
    {
        final AsynchronousJiraSetup<Void, Step> setup = new AsynchronousJiraSetup<Void, Step>(DO_NOTHING_STRATEGY);
        final String sessionId = "ID";
        setup.setupJIRA(sessionId, null);
        setup.getStatusOnceStepIsDone(sessionId, Step.step2);
    }

    @Test
    public void testGetStatusOnceStepIsDoneWillReturnImmediatelyIfAskedAboutDifferentStep() throws InterruptedException
    {
        for (int r = 0; r < REPEAT_CONCURRENT; r++)
        {
            final String sessionId = "ID";
            final Gate step1Gate = new Gate(1);
            final Gate step2Gate = new Gate(1);
            final AsynchronousJiraSetup<Void, Step> setup = new AsynchronousJiraSetup<Void, Step>(new SetupStrategy<Void, Step>()
            {
                @Override
                public void setup(final Void nothing, final StepSwitcher<Step> switcher)
                        throws Exception
                {
                    try
                    {
                        switcher.withStep(Step.step1, DO_NOTHING_TASK);
                        step1Gate.ready();
                        switcher.withStep(Step.step2, new SetupStrategy.StepTask()
                        {
                            @Override
                            public void run() throws Exception
                            {
                                step2Gate.ready();
                            }
                        });
                    }
                    catch (final Exception e)
                    {
                        throw new RuntimeException(e);
                    }
                }

                @Override
                public ImmutableMap<Step, Status> getInitialSteps()
                {
                    return ImmutableMap.of(Step.step1, Status.PENDING);
                }
            });
            setup.setupJIRA(sessionId, null);
            step1Gate.go();

            final AsynchronousJiraSetup.SetupStatus<Step> status;
            status = setup.getStatusOnceStepIsDone(sessionId, Step.step1);
            assertThat(status.getSteps(), Matchers.hasEntry(Step.step2, Status.PENDING));
            step2Gate.go();
        }
    }


    @Test
    public void testGetStatusOnceStepIsDoneWillWaitOnCurrentStep() throws InterruptedException
    {
        for (int r = 0; r < REPEAT_CONCURRENT; r++)
        {
            final String sessionId = "ID";
            final Gate step1Gate = new Gate(1);
            final Gate step2Gate = new Gate(1);
            final AtomicBoolean waitDone = new AtomicBoolean(false);
            final AsynchronousJiraSetup<Void, Step> setup = new AsynchronousJiraSetup<Void, Step>(new SetupStrategy<Void, Step>()
            {
                @Override
                public void setup(final Void nothing, final StepSwitcher<Step> switcher)
                        throws Exception
                {
                    try
                    {
                        switcher.withStep(Step.step1, DO_NOTHING_TASK);
                        step1Gate.ready();
                        switcher.withStep(Step.step2, DO_NOTHING_TASK);
                        step2Gate.ready();
                    }
                    catch (final Exception e)
                    {
                        throw new RuntimeException(e);
                    }
                }

                @Override
                public ImmutableMap<Step, Status> getInitialSteps()
                {
                    return ImmutableMap.of(Step.step1, Status.PENDING);
                }
            });

            setup.setupJIRA(sessionId, null);

            final Thread thread = new Thread(new Runnable()
            {
                @Override
                public void run()
                {
                    setup.getStatusOnceStepIsDone(sessionId, Step.step1);
                    waitDone.set(true);
                }
            });
            thread.start();
            //we actually wait
            poll(Matchers.equalTo(Thread.State.WAITING), new Supplier<Thread.State>()
            {
                @Override
                public Thread.State get()
                {
                    return thread.getState();
                }
            });
            assertFalse(waitDone.get());
            step1Gate.go();
            poll(Matchers.equalTo(true), new Supplier<Boolean>()
            {
                @Override
                public Boolean get()
                {
                    return waitDone.get();
                }
            });
            assertThat(setup.getStatusOnceStepIsDone(sessionId, Step.step1).getSteps(),
                    Matchers.hasEntry(Step.step1, Status.SUCCESS));

            step2Gate.go();
        }
    }

    @Test
    public void testGetStatusOnceStepIsDoneWillReleaseAllWaitingThreads() throws InterruptedException
    {
        for (int r = 0; r < REPEAT_CONCURRENT; r++)
        {
            final String sessionId = "ID";
            final Gate step1Gate = new Gate(1);
            final int numberOfThreads = 10;
            final List<Thread> threads = new ArrayList<Thread>();
            final List<ImmutableMap<Step, Status>> results = Collections.synchronizedList(new ArrayList<ImmutableMap<Step, Status>>());
            final AsynchronousJiraSetup<Void, Step> setup = new AsynchronousJiraSetup<Void, Step>(new SetupStrategy<Void, Step>()
            {
                @Override
                public void setup(final Void nothing, final StepSwitcher<Step> switcher)
                        throws Exception
                {
                    try
                    {
                        switcher.withStep(Step.step1, DO_NOTHING_TASK);
                        step1Gate.ready();
                    }
                    catch (final Exception e)
                    {
                        throw new RuntimeException(e);
                    }
                }

                @Override
                public ImmutableMap<Step, Status> getInitialSteps()
                {
                    return ImmutableMap.of(Step.step1, Status.PENDING);
                }
            });

            setup.setupJIRA(sessionId, null);


            final Runnable runnable = new Runnable()
            {
                @Override
                public void run()
                {
                    results.add(setup.getStatusOnceStepIsDone(sessionId, Step.step1).getSteps());
                }
            };
            for (int i = 0; i < numberOfThreads; i++)
            {
                final Thread thread = new Thread(runnable);
                threads.add(thread);
                thread.start();
                //let's check all threads are actually waiting
                poll(Matchers.equalTo(Thread.State.WAITING), new Supplier<Thread.State>()
                {
                    @Override
                    public Thread.State get()
                    {
                        return thread.getState();
                    }
                });
            }
            step1Gate.go();
            for (final Thread thread : threads)
            {
                thread.join(5000);
                assertFalse(thread.isAlive());
            }
            assertThat(results, Matchers.hasSize(numberOfThreads));
            for (final ImmutableMap<Step, Status> result : results)
            {
                assertThat(result, Matchers.hasEntry(Step.step1, Status.SUCCESS));
                assertThat(result.entrySet(), Matchers.hasSize(1));
            }
        }
    }

    @Test
    public void testSetupFinishedWillBeTrueAfterSetupFinishes() throws InterruptedException
    {
        for (int r = 0; r < REPEAT_CONCURRENT; r++)
        {
            final String sessionId = "ID";
            final Gate gate = new Gate(1);
            final AsynchronousJiraSetup<Void, Step> setup = new AsynchronousJiraSetup<Void, Step>(new SetupStrategy<Void, Step>()
            {
                @Override
                public void setup(final Void nothing, final StepSwitcher<Step> switcher)
                        throws Exception
                {
                    gate.ready();
                }

                @Override
                public ImmutableMap<Step, Status> getInitialSteps()
                {
                    return ImmutableMap.of(Step.step1, Status.PENDING);
                }
            });
            setup.setupJIRA(sessionId, null);
            assertFalse(setup.isSetupFinished(sessionId));
            gate.go();
            poll(Matchers.equalTo(true), new Supplier<Boolean>()
            {

                @Override
                public Boolean get()
                {
                    return setup.isSetupFinished(sessionId);
                }
            });
        }
    }

    @Test
    public void testSetupCanBeCalledOnlyOnce() throws InterruptedException
    {
        for (int r = 0; r < REPEAT_CONCURRENT; r++)
        {
            final AsynchronousJiraSetup<Void, Step> setup = new AsynchronousJiraSetup<Void, Step>(DO_NOTHING_STRATEGY);
            final String sessionId = "ID";
            final AtomicInteger successCount = new AtomicInteger(0);
            final AtomicInteger errorCount = new AtomicInteger(0);
            final int numberOfThreads = 100;

            for (int i = 0; i < numberOfThreads; i++)
            {
                new Thread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            setup.setupJIRA(sessionId, null);
                            successCount.incrementAndGet();
                        }
                        catch (final RuntimeException e)
                        {
                            errorCount.incrementAndGet();
                        }
                    }
                }).start();
            }

            poll(Matchers.equalTo(1), new Supplier<Integer>()
            {

                @Override
                public Integer get()
                {
                    return successCount.get();
                }
            });
            poll(Matchers.equalTo(numberOfThreads - 1), new Supplier<Integer>()
            {

                @Override
                public Integer get()
                {
                    return errorCount.get();
                }
            });
        }
    }

    private <T> void poll(final Matcher<? super T> matcher, final Supplier<T> supplier)
    {
        final int numRetries = 100;
        final int millis = 50;
        for (int i = 0; i < numRetries; i++)
        {
            if (matcher.matches(supplier.get()))
            {
                return;
            }
            try
            {
                Thread.sleep(millis);
            }
            catch (final InterruptedException e)
            {
                throw new RuntimeException(e);
            }
        }
        assertThat("After " + numRetries * millis + "ms", supplier.get(), matcher);
    }
}