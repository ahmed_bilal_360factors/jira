package com.atlassian.jira.datetime;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class JodaDateTimeFormatterTest
{
    private static final DateTimeZone JIRA_TZ = DateTimeZone.forID("Australia/Sydney");
    private static final Locale JIRA_LOCALE = Locale.ENGLISH;

    private static final DateTimeZone BRAZIL_TZ = DateTimeZone.forID("America/Sao_Paulo");
    private static final Locale BRAZIL_LOCALE = new Locale("pt_BR");

    private DateTimeFormatterFactoryStub dateTimeFormatterFactory;

    @Test
    public void testBrazilDstCutoverParse() throws Exception
    {
        // Test with Sydney Locale
        dateTimeFormatterFactory.userTimeZone(JIRA_TZ);
        dateTimeFormatterFactory.userLocale(JIRA_LOCALE);
        DateTimeFormatter formatter = dateTimeFormatterFactory.formatter()
                .withZone(JIRA_TZ.toTimeZone())
                .withLocale(JIRA_LOCALE)
                .forLoggedInUser();

        GregorianCalendar calendar = new GregorianCalendar(JIRA_TZ.toTimeZone());
        calendar.setTimeInMillis(0);
        calendar.set(2010, Calendar.OCTOBER, 17, 0, 0, 0);
        assertThat(formatter.parse("17/Oct/10"), is(calendar.getTime()));

        // Test with Sao Paulo Locale
        dateTimeFormatterFactory.userTimeZone(BRAZIL_TZ);
        dateTimeFormatterFactory.userLocale(BRAZIL_LOCALE);
        formatter = dateTimeFormatterFactory.formatter()
                .withZone(BRAZIL_TZ.toTimeZone())
                .withLocale(BRAZIL_LOCALE)
                .forLoggedInUser();

        calendar = new GregorianCalendar(BRAZIL_TZ.toTimeZone());
        calendar.setTimeInMillis(0);
        calendar.set(2010, Calendar.OCTOBER, 17, 0, 0, 0);
        assertThat(formatter.parse("17/Oct/10"), is(calendar.getTime()));
    }

    @Test
    public void testNormalDateTimeParse() throws Exception
    {
        // Test with Sydney Locale
        dateTimeFormatterFactory.userTimeZone(JIRA_TZ);
        dateTimeFormatterFactory.userLocale(JIRA_LOCALE);
        dateTimeFormatterFactory.style(DateTimeStyle.COMPLETE);
        DateTimeFormatter formatter = dateTimeFormatterFactory.formatter()
                .withZone(JIRA_TZ.toTimeZone())
                .withLocale(JIRA_LOCALE)
                .forLoggedInUser();

        GregorianCalendar calendar = new GregorianCalendar(JIRA_TZ.toTimeZone());
        calendar.setTimeInMillis(0);
        calendar.set(2010, Calendar.JANUARY, 17, 3, 43, 0);
        assertThat(formatter.parse("17/Jan/10 03:43 AM"), is(calendar.getTime()));

        calendar.setTimeInMillis(0);
        calendar.set(2011, Calendar.FEBRUARY, 20, 3, 43, 0);
        assertThat(formatter.parse("20/Feb/11 03:43 AM"), is(calendar.getTime()));

        calendar.setTimeInMillis(0);
        calendar.set(2012, Calendar.MARCH, 30, 12, 43, 0);
        assertThat(formatter.parse("30/Mar/12 12:43 PM"), is(calendar.getTime()));

        calendar.setTimeInMillis(0);
        calendar.set(2013, Calendar.APRIL, 12, 15, 43, 0);
        assertThat(formatter.parse("12/Apr/13 03:43 PM"), is(calendar.getTime()));

    }

    @Test
    public void testInvalidDateTimeParse() throws Exception
    {
        // Test with Sydney Locale
        dateTimeFormatterFactory.userTimeZone(JIRA_TZ);
        dateTimeFormatterFactory.userLocale(JIRA_LOCALE);
        dateTimeFormatterFactory.style(DateTimeStyle.COMPLETE);
        DateTimeFormatter formatter = dateTimeFormatterFactory.formatter()
                .withZone(JIRA_TZ.toTimeZone())
                .withLocale(JIRA_LOCALE)
                .forLoggedInUser();

        assertDateInvalid(formatter, "17/Jan/10 15:43 AM");
        assertDateInvalid(formatter, "29/Feb/11 03:43 AM");
        assertDateInvalid(formatter, "30/Pig/12 12:43 PM");
        assertDateInvalid(formatter, "12/Apr/13 03:43 qM");

    }

    public void assertDateInvalid(final DateTimeFormatter formatter, final String textDate)
    {
        try
        {
            formatter.parse(textDate);
            fail(textDate + "is invalid");
        }
        catch (IllegalArgumentException ex)
        {
            // Ignore
        }
    }

    @Before
    public void setUpFactoryStub() throws Exception
    {
        dateTimeFormatterFactory = new DateTimeFormatterFactoryStub();
        dateTimeFormatterFactory.style(DateTimeStyle.DATE);
        dateTimeFormatterFactory.relativeDates(false);
        dateTimeFormatterFactory.jiraTimeZone(JIRA_TZ);
        dateTimeFormatterFactory.jiraLocale(JIRA_LOCALE);
    }

}