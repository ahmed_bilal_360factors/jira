package com.atlassian.jira.mail.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;

import javax.mail.BodyPart;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mail.TemplateUser;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.Consumer;
import com.atlassian.jira.web.ServletContextProviderListener;

import com.google.common.base.Function;
import com.google.common.base.Throwables;

import org.apache.commons.io.IOUtils;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.hamcrest.core.StringStartsWith;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static com.google.common.collect.Iterables.transform;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MailAttachmentsManagerImplTest
{

    @Rule
    public MockitoContainer initMockitoMocks = MockitoMocksInContainer.rule(this);

    @Mock
    private UserManager userManagerMock;
    @Mock
    private AvatarService avatarServiceMock;
    @Mock
    private AvatarManager avatarManagerMock;
    @Mock
    private ApplicationProperties applicationProperties;
    @AvailableInContainer
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private ServletContext servletContext;
    private MailAttachmentsManagerImpl mailImagesManager;

    @Before
    public void setUp() throws Exception
    {
        new ServletContextProviderListener().contextInitialized(
                new ServletContextEvent(servletContext)
        );
        mailImagesManager = new MailAttachmentsManagerImpl(avatarServiceMock, userManagerMock, avatarManagerMock, applicationProperties);
    }

    @Test
    public void testAddAvatarImage() throws Exception
    {
        String url = mailImagesManager.getAvatarUrl(new MockApplicationUser("user1"));

        assertThat(url, CoreMatchers.startsWith("cid:" + MailAttachmentsManagerImpl.CID_PREFIX));
    }

    @Test
    public void testAddAvatarImagesDoesNotDuplicateAttachmentsForSameUser() throws Exception
    {
        String username = "user1";
        MockApplicationUser user = new MockApplicationUser(username);
        MockUser oldUser = new MockUser(username); //To create TemplateUser

        when(userManagerMock.getUserByName(username)).thenReturn(user);

        mailImagesManager.getAvatarUrl(user);
        mailImagesManager.getAvatarUrl(TemplateUser.getUser(oldUser));
        mailImagesManager.getAvatarUrl(username);

        assertThat(mailImagesManager.getAttachmentsCount(), equalTo(1));
    }

    @SuppressWarnings ("unchecked")
    @Test
    public void shouldAddIssueTypeWithAvatarAsImage() throws IOException
    {
        // given
        IssueType issueType = mock(IssueType.class);
        when(issueType.getId()).thenReturn("id0");
        when(issueType.getName()).thenReturn("name");
        when(issueType.getIconUrlHtml()).thenReturn("some-icon-url");
        final Avatar avatar = mock(Avatar.class);
        final byte[] avatarBytes = ("an-avatar-" + Math.random()).getBytes("UTF-8");
        when(issueType.getAvatar()).thenReturn(avatar);
        doAnswer(new Answer()
        {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                Consumer<InputStream> consumer = (Consumer<InputStream>) invocation.getArguments()[2];

                final ByteArrayInputStream avatarData = new ByteArrayInputStream(avatarBytes);
                consumer.consume(avatarData);
                return null;
            }
        }).when(avatarManagerMock)
                .readAvatarData(refEq(avatar), any(AvatarManager.ImageSize.class), any(Consumer.class));

        // when
        final String issueTypeIconUrl = mailImagesManager.getIssueTypeIconUrl(issueType);
        final Iterable<BodyPart> bodyParts = mailImagesManager.buildAttachmentsBodyParts();

        // then
        final Iterable<byte[]> bodyPartsBytes = transform(bodyParts, EXTRACT_BODY_PART_BYTES);

        assertThat(issueTypeIconUrl, notNullValue());
        assertThat(bodyPartsBytes, contains(avatarBytes));
    }

    @Test
    public void shouldAddIssueTypeWithoutAvatarAsImage() throws IOException
    {
        // given
        IssueType issueType = mock(IssueType.class);
        final byte[] avatarBytes = ("an-avatar-" + Math.random()).getBytes("UTF-8");
        when(issueType.getAvatar()).thenReturn(null);
        final String someIconUrl = "some-icon-url";

        when(issueType.getIconUrlHtml()).thenReturn(someIconUrl);
        when(servletContext.getResource(someIconUrl)).thenReturn(new URL("http://a.b.c"));
        when(servletContext.getResourceAsStream(someIconUrl)).thenReturn(new ByteArrayInputStream(avatarBytes));
        when(servletContext.getMimeType(someIconUrl)).thenReturn("mime/type");

        // when
        final String issueTypeIconUrl = mailImagesManager.getIssueTypeIconUrl(issueType);
        final Iterable<BodyPart> bodyParts = mailImagesManager.buildAttachmentsBodyParts(); 

        // then
        final Iterable<byte[]> bodyPartsBytes = transform(bodyParts, EXTRACT_BODY_PART_BYTES);

        assertThat(issueTypeIconUrl, notNullValue());
        assertThat(bodyPartsBytes, contains(avatarBytes));
    }

    public static final Function<BodyPart, byte[]> EXTRACT_BODY_PART_BYTES = new Function<BodyPart, byte[]>()
    {
        @Override
        public byte[] apply(final BodyPart next)
        {
            byte[] partContent = null;
            try
            {
                partContent = IOUtils.toByteArray(next.getInputStream());
            }
            catch (Exception e)
            {
                Throwables.propagate(e);
            }

            return partContent;
        }
    };

    @Test
    public void shouldNotStoreIssueTypeImagesDuplicates() throws IOException
    {
        // given
        IssueType issueType = mock(IssueType.class);
        when(issueType.getAvatar()).thenReturn(null);
        final String someIconUrl = "some-icon-url";
        when(issueType.getIconUrlHtml()).thenReturn(someIconUrl);
        when(servletContext.getResource(someIconUrl)).thenReturn(new URL("http://a.b.c"));

        // when
        mailImagesManager.getIssueTypeIconUrl(issueType);
        mailImagesManager.getIssueTypeIconUrl(issueType);

        // then
        assertThat(mailImagesManager.getAttachmentsCount(), Matchers.equalTo(1));
    }

    @Test
    public void shouldNotStoreIssueTypeAvatarsDuplicates() throws IOException
    {
        // given
        IssueType issueType = mock(IssueType.class);
        when(issueType.getId()).thenReturn("id0");
        when(issueType.getName()).thenReturn("name");
        when(issueType.getIconUrlHtml()).thenReturn("some-icon-url");
        final Avatar avatar = mock(Avatar.class);
        when(issueType.getAvatar()).thenReturn(avatar);

        // when
        mailImagesManager.getIssueTypeIconUrl(issueType);
        mailImagesManager.getIssueTypeIconUrl(issueType);

        // then
        assertThat(mailImagesManager.getAttachmentsCount(), Matchers.equalTo(1));
    }

    @Test
    public void testAddingMultipleTimeSameImageAlwaysReturnsSameCid() throws Exception
    {
        String username = "user1";
        MockApplicationUser user = new MockApplicationUser(username);
        MockUser oldUser = new MockUser(username); //To create TemplateUser

        when(userManagerMock.getUserByName(username)).thenReturn(user);

        final String cid1 = mailImagesManager.getAvatarUrl(user);
        final String cid2 = mailImagesManager.getAvatarUrl(TemplateUser.getUser(oldUser));
        final String cid3 = mailImagesManager.getAvatarUrl(username);

        assertThat(cid1, startsWith("cid:"));
        assertThat(cid2, startsWith("cid:"));
        assertThat(cid3, startsWith("cid:"));

        assertThat(cid1, equalTo(cid2));
        assertThat(cid2, equalTo(cid3));
    }

    @Test
    public void testAddAvatarShouldNotAddAttachmentIfUsingAnExternalGravatar() throws Exception
    {
        final ApplicationUser loggedInUser = new MockApplicationUser("admin");
        final ApplicationUser avatarUser = new MockApplicationUser("SomeUser");
        String exampleUrl = "http://example.org";

        when(jiraAuthenticationContext.getUser()).thenReturn(loggedInUser);
        when(avatarServiceMock.isUsingExternalAvatar(loggedInUser, avatarUser)).thenReturn(true);
        when(avatarServiceMock.getAvatarUrlNoPermCheck(avatarUser, Avatar.Size.defaultSize())).thenReturn(new URI(exampleUrl));

        String url = mailImagesManager.getAvatarUrl(avatarUser);

        assertThat(url, equalTo(exampleUrl));
    }

    @Test
    public void testAddAvatarShouldAddAttachmentIfGravatarIsEnabledButUserHasInternalAvatar() throws Exception
    {
        final ApplicationUser loggedInUser = new MockApplicationUser("admin");
        final ApplicationUser avatarUser = new MockApplicationUser("SomeUser");

        when(jiraAuthenticationContext.getUser()).thenReturn(loggedInUser);
        when(avatarServiceMock.isUsingExternalAvatar(loggedInUser, avatarUser)).thenReturn(false);

        String url = mailImagesManager.getAvatarUrl(avatarUser);

        assertThat(url, StringStartsWith.startsWith("cid:" + MailAttachmentsManagerImpl.CID_PREFIX));
    }

    @Test
    public void testGetAbsoluteUrl() throws Exception
    {
        when(applicationProperties.getString(APKeys.JIRA_BASEURL)).thenReturn("http://this/is/jira");

        assertThat(mailImagesManager.getAbsoluteUrl("/foo/bar"), equalTo("http://this/is/jira/foo/bar"));
        assertThat(mailImagesManager.getAbsoluteUrl("foo/bar"), equalTo("http://this/is/jira/foo/bar"));

        when(applicationProperties.getString(APKeys.JIRA_BASEURL)).thenReturn("https://this/is/jira/");

        assertThat(mailImagesManager.getAbsoluteUrl("/foo/bar"), equalTo("https://this/is/jira/foo/bar"));
        assertThat(mailImagesManager.getAbsoluteUrl("foo/bar"), equalTo("https://this/is/jira/foo/bar"));
    }
}
