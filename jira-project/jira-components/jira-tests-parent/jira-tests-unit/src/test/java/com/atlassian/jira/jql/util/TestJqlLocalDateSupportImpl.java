package com.atlassian.jira.jql.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import com.atlassian.jira.datetime.LocalDate;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.util.ConstantClock;
import com.atlassian.query.operand.SingleValueOperand;

import com.google.common.collect.Lists;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestJqlLocalDateSupportImpl
{
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private TimeZoneManager timeZoneManager;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testNullConstructorArguments() throws Exception
    {
        exception.expect(CoreMatchers.instanceOf(IllegalArgumentException.class));
        new JqlLocalDateSupportImpl(null, timeZoneManager);
    }

    @Test
    public void testParseDateIllegalLongArgument() throws Exception
    {
        final JqlLocalDateSupport support = new JqlLocalDateSupportImpl(timeZoneManager);
        exception.expect(CoreMatchers.instanceOf(IllegalArgumentException.class));
        support.convertToLocalDate((Long) null);
    }

    @Test
    public void testParseDateIllegalStringArgument()
    {
        final JqlLocalDateSupport support = new JqlLocalDateSupportImpl(timeZoneManager);
        exception.expect(CoreMatchers.instanceOf(IllegalArgumentException.class));
        support.convertToLocalDate((String) null);
    }

    @Test
    public void testConvertDurationToLocalDate() throws Exception
    {
        final String validDuration = "-2d";
        when(timeZoneManager.getLoggedInUserTimeZone()).thenReturn(TimeZone.getDefault());

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2011);
        calendar.set(Calendar.MONTH, 4);
        calendar.set(Calendar.DAY_OF_MONTH, 11);

        final LocalDate expectedLocalDate = new LocalDate(2011, 5, 9);
        JqlLocalDateSupportImpl jqlLocalDateSupport = new JqlLocalDateSupportImpl(new ConstantClock(calendar.getTime()), timeZoneManager);

        assertEquals(expectedLocalDate, jqlLocalDateSupport.convertToLocalDate(validDuration));
    }

    @Test
    public void testConvertDateStringToLocalDate() throws Exception
    {
        final String dateString = "2011/5/1";
        when(timeZoneManager.getLoggedInUserTimeZone()).thenReturn(TimeZone.getDefault());

        final LocalDate expectedLocalDate = new LocalDate(2011, 5, 1);
        JqlLocalDateSupportImpl jqlLocalDateSupport = new JqlLocalDateSupportImpl(timeZoneManager);
        assertEquals(expectedLocalDate, jqlLocalDateSupport.convertToLocalDate(dateString));
    }

    @Test
    public void testConvertLongToLocalDate() throws Exception
    {
        when(timeZoneManager.getLoggedInUserTimeZone()).thenReturn(TimeZone.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2011);
        calendar.set(Calendar.MONTH, 4);
        calendar.set(Calendar.DAY_OF_MONTH, 11);
        final LocalDate expectedLocalDate = new LocalDate(2011, 5, 11);
        JqlLocalDateSupport jqlLocalDateSupport = new JqlLocalDateSupportImpl(timeZoneManager);

        assertEquals(expectedLocalDate, jqlLocalDateSupport.convertToLocalDate(calendar.getTimeInMillis()));
    }

    @Test
    public void testGetIndexedLocalDateValue() throws Exception
    {
        LocalDate localDate = new LocalDate(2011, 5, 11);

        JqlLocalDateSupport jqlLocalDateSupport = new JqlLocalDateSupportImpl(timeZoneManager);
        assertEquals("20110511", jqlLocalDateSupport.getIndexedValue(localDate));
    }

    @Test
    public void testValidateDurationString() throws Exception
    {
        when(timeZoneManager.getLoggedInUserTimeZone()).thenReturn(TimeZone.getDefault());

        JqlLocalDateSupport jqlLocalDateSupport = new JqlLocalDateSupportImpl(timeZoneManager);
        assertTrue(jqlLocalDateSupport.validate("-2d"));
    }

    @Test
    public void testValidateStringIsNull() throws Exception
    {
        JqlLocalDateSupport jqlLocalDateSupport = new JqlLocalDateSupportImpl(timeZoneManager);

        assertThat(jqlLocalDateSupport.validate(null), equalTo(false));
    }

    @Test
    public void testValidateDateString() throws Exception
    {
        JqlLocalDateSupport jqlLocalDateSupport = new JqlLocalDateSupportImpl(timeZoneManager);

        assertTrue(jqlLocalDateSupport.validate("2011/1/1"));
    }

    @Test
    public void testValidationDurationStringFails() throws Exception
    {
        when(timeZoneManager.getLoggedInUserTimeZone()).thenReturn(TimeZone.getDefault());
        JqlLocalDateSupport jqlLocalDateSupport = new JqlLocalDateSupportImpl(timeZoneManager);

        assertFalse(jqlLocalDateSupport.validate("-21312312wefsf"));
    }

    @Test
    public void testValidationDateStringFails() throws Exception
    {
        when(timeZoneManager.getLoggedInUserTimeZone()).thenReturn(TimeZone.getDefault());
        JqlLocalDateSupport jqlLocalDateSupport = new JqlLocalDateSupportImpl(timeZoneManager);

        assertFalse(jqlLocalDateSupport.validate("2011/20398329482/12"));
    }

    @Test
    public void testGetLocalDateString() throws Exception
    {
        LocalDate localDate = new LocalDate(1989, 11, 3);
        JqlLocalDateSupport jqlLocalDateSupport = new JqlLocalDateSupportImpl(timeZoneManager);

        assertEquals("1989-11-3", jqlLocalDateSupport.getLocalDateString(localDate));
    }

    @Test
    public void testGetLocalDateStringIsNull() throws Exception
    {
        JqlLocalDateSupport jqlLocalDateSupport = new JqlLocalDateSupportImpl(timeZoneManager);

        exception.expect(CoreMatchers.instanceOf(IllegalArgumentException.class));
        jqlLocalDateSupport.getLocalDateString(null);
    }

    @Test
    public void testGettngLocalDateFromStringQueryLiteral()
    {
        JqlLocalDateSupport jqlLocalDateSupport = new JqlLocalDateSupportImpl(timeZoneManager);

        final QueryLiteral literal = new QueryLiteral(new SingleValueOperand("2011/1/1"), "2011/1/1");

        final LocalDate localDate = jqlLocalDateSupport.getLocalDateFromQueryLiteral(literal);
        assertEquals(new LocalDate(2011, 1, 1), localDate);
    }

    @Test
    public void testGettngLocalDateFromLongQueryLiteral() throws ParseException
    {
        JqlLocalDateSupport jqlLocalDateSupport = new JqlLocalDateSupportImpl(timeZoneManager);

        final long time = new SimpleDateFormat("dd/MM/yyyy").parse("01/01/2011").getTime();
        final QueryLiteral literal = new QueryLiteral(new SingleValueOperand("2011/1/1"), time);

        final LocalDate localDate = jqlLocalDateSupport.getLocalDateFromQueryLiteral(literal);
        assertEquals(new LocalDate(2011, 1, 1), localDate);
    }

    /**
     * This is very bad API, but it it expected that for JqlLocalSupportDate returns null, even if the operand doesn't
     * have string/long date value.
     */
    @Test
    public void testGettingLocalDateFromNullQueryLiteal()
    {
        JqlLocalDateSupport jqlLocalDateSupport = new JqlLocalDateSupportImpl(timeZoneManager);

        final QueryLiteral literal = new QueryLiteral(new SingleValueOperand("abcd"));

        assertThat(jqlLocalDateSupport.getLocalDateFromQueryLiteral(literal), nullValue());
    }

    @Test
    public void testGettingLocalDatesFromQueryLiterals() throws ParseException
    {
        JqlLocalDateSupport jqlLocalDateSupport = new JqlLocalDateSupportImpl(timeZoneManager);

        final QueryLiteral literal1 = new QueryLiteral(new SingleValueOperand("2011/1/1"), "2011/1/1");
        final QueryLiteral literal2 = new QueryLiteral(new SingleValueOperand("2011/1/1"), new SimpleDateFormat("dd/MM/yyyy").parse("01/01/2011").getTime());
        final QueryLiteral literal3 = new QueryLiteral(new SingleValueOperand("2011/1/1"));

        final List<LocalDate> queryLiterals = jqlLocalDateSupport.getLocalDatesFromQueryLiterals(Lists.newArrayList(literal1, literal2, literal3));

        final LocalDate expectedLocalDate = new LocalDate(2011, 1, 1);
        assertThat(queryLiterals, Matchers.containsInAnyOrder(expectedLocalDate, expectedLocalDate, null));
    }

}
