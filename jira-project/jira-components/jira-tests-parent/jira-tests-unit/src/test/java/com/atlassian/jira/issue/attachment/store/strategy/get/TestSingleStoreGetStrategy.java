package com.atlassian.jira.issue.attachment.store.strategy.get;

import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.store.MockAttachmentKeys;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promises;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestSingleStoreGetStrategy
{

    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private StreamAttachmentStore streamAttachmentStore;

    private final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();

    @InjectMocks
    private SingleStoreAttachmentGetStrategy singleStoreAttachmentGetStrategy;

    @Test
    public void testDelegatesAttachmentGetting() throws Exception
    {
        final Function<AttachmentGetData, String> processor = mock(Function.class);

        final String expectedString = "expectedString";
        when(streamAttachmentStore.getAttachmentData(attachmentKey, processor))
                .thenReturn(Promises.promise(expectedString));

        final String result = singleStoreAttachmentGetStrategy.getAttachmentData(attachmentKey, processor).claim();

        assertThat(result, equalTo(expectedString));
    }

    @Test
    public void testDelegatesExistenceCheckToStore() throws Exception
    {
        when(streamAttachmentStore.exists(attachmentKey))
                .thenReturn(Promises.promise(true));

        final Boolean result = singleStoreAttachmentGetStrategy.exists(attachmentKey).claim();

        assertThat(result, equalTo(true));
    }
}