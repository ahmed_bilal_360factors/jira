package com.atlassian.jira.service.services.analytics.start;

import java.util.Map;

import com.atlassian.jira.user.util.UserUtil;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestUserAnalyticTask
{

    @Test
    public void testAnalytic()
    {
        UserUtil userUtil = mock(UserUtil.class);
        UserAnalyticTask task = new UserAnalyticTask(userUtil);

        when(userUtil.getTotalUserCount()).thenReturn(1000);
        when(userUtil.getActiveUserCount()).thenReturn(500);

        Map<String, Object> analytics = task.getAnalytics();

        assertEquals(1000, analytics.get("users.total"));
        assertEquals(500, analytics.get("users.active"));
    }
}