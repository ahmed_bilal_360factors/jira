package com.atlassian.jira.timezone;

import java.util.TimeZone;

import com.atlassian.core.user.preferences.Preferences;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.crowd.embedded.TestData;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.user.preferences.PreferenceKeys;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserManager;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.when;


public class TimeZoneManagerImplTest
{
    @Rule
    public RuleChain chain = MockitoMocksInContainer.forTest(this);
    private TimeZoneManagerImpl timeZoneManager;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private UserPreferencesManager userPreferencesManager;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private UserManager userManager;
    @Mock
    private Preferences preferences;

    @Before
    public void setup()
    {
        timeZoneManager = new TimeZoneManagerImpl(jiraAuthenticationContext, userPreferencesManager, applicationProperties, userManager);
    }

    @Test
    public void testGetLoggedInUserTimeZone() throws Exception
    {
        final String userTimeZoneId = "Africa/Nouakchott";
        MockUser user = new MockUser("LoggedInUser");
        when(preferences.getString(PreferenceKeys.USER_TIMEZONE)).thenReturn(userTimeZoneId);
        when(userManager.isUserExisting(eq(ApplicationUsers.from(user)))).thenReturn(true);
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(user);
        when(applicationProperties.getString(eq(APKeys.JIRA_DEFAULT_TIMEZONE))).thenReturn(null);
        when(userPreferencesManager.getPreferences(eq(user))).thenReturn(preferences);

        assertThat(timeZoneManager.getLoggedInUserTimeZone(), equalTo(TimeZone.getTimeZone(userTimeZoneId)));
    }

    @Test
    public void testGetTimeZoneForUser() throws Exception
    {
        final String userTimeZoneId = "Africa/Nouakchott";
        MockUser user = new MockUser("Test");
        when(preferences.getString(PreferenceKeys.USER_TIMEZONE)).thenReturn(userTimeZoneId);
        when(userManager.isUserExisting(eq(ApplicationUsers.from(user)))).thenReturn(true);
        when(applicationProperties.getString(eq(APKeys.JIRA_DEFAULT_TIMEZONE))).thenReturn(null);
        when(userPreferencesManager.getPreferences(eq(user))).thenReturn(preferences);

        assertThat(timeZoneManager.getTimeZoneforUser(user), equalTo(TimeZone.getTimeZone(userTimeZoneId)));
    }

    @Test
    public void testGetTimeZoneForUserWhenUserNotSetOwnTimeZone() throws Exception
    {
        final MockUser user = new MockUser("Test");
        when(applicationProperties.getString(eq(APKeys.JIRA_DEFAULT_TIMEZONE))).thenReturn(null);
        when(userPreferencesManager.getPreferences(eq(user))).thenReturn(null);

        assertThat(timeZoneManager.getTimeZoneforUser(user), equalTo(TimeZone.getDefault()));
    }

    @Test
    public void testGetTimeZoneForUserWhenUserIsInvalid() throws Exception
    {
        final String userTimeZoneId = "Africa/Nouakchott";
        MockUser user = new MockUser("Test");
        when(preferences.getString(PreferenceKeys.USER_TIMEZONE)).thenReturn(userTimeZoneId);
        when(userPreferencesManager.getPreferences(any(User.class))).thenReturn(preferences);

        when(userManager.isUserExisting(eq(ApplicationUsers.from(user)))).thenReturn(false);
        assertThat(timeZoneManager.getTimeZoneforUser(user), equalTo(TimeZone.getDefault()));

        when(userManager.isUserExisting(isNull(ApplicationUser.class))).thenReturn(false);
        assertThat(timeZoneManager.getTimeZoneforUser(null), equalTo(TimeZone.getDefault()));
    }

    @Test
    public void testGetDefaultTimezone() throws Exception
    {
        final String jiraDefaultTimeZoneId = "Africa/Nouakchott";
        final TimeZone jiraDefaultTimeZone = TimeZone.getTimeZone(jiraDefaultTimeZoneId);

        when(applicationProperties.getString(eq(APKeys.JIRA_DEFAULT_TIMEZONE))).thenReturn(jiraDefaultTimeZoneId);
        assertThat(timeZoneManager.getDefaultTimezone(), equalTo(jiraDefaultTimeZone));

        when(applicationProperties.getString(eq(APKeys.JIRA_DEFAULT_TIMEZONE))).thenReturn(null);
        assertThat(timeZoneManager.getDefaultTimezone(), equalTo(TimeZone.getDefault()));

    }
}