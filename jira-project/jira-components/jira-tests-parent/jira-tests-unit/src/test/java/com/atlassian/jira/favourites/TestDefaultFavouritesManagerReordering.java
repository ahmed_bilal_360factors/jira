package com.atlassian.jira.favourites;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.sharing.ShareManager;
import com.atlassian.jira.sharing.SharedEntity;
import com.atlassian.jira.sharing.SharedEntityAccessor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.DelegatingApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;

import com.google.common.collect.ImmutableList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for the reordering of favourites in the DefaultFavouriteManager
 *
 * @since v3.13
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultFavouritesManagerReordering
{
    private static final Long ID_123 = 123L;
    private static final Long ID_456 = 456L;
    private static final Long ID_789 = 789L;
    private static final Long ID_101112 = 101112L;
    private static final SharedEntity.TypeDescriptor<PortalPage> ENTITY_TYPE = PortalPage.ENTITY_TYPE;

    private ApplicationUser user;

    private SharedEntity sharedEntity123;
    private SharedEntity sharedEntity456;
    private SharedEntity sharedEntity789;
    private SharedEntity sharedEntity101112;
    private Map<Long,SharedEntity> mapOfEntities;

    @Mock private SharedEntityAccessor.Factory sharedEntityAccessorFactory;
    @Mock private SharedEntityAccessor<SharedEntity> sharedEntityAccessor;
    @Mock private FavouritesStore favouriteStore;
    @Mock private ShareManager shareManager;

    private FavouritesManager<SharedEntity> favouritesManager;

    @Before
    public void setUp()
    {
        user = new MockApplicationUser("user");

        sharedEntity123 = new SharedEntity.Identifier(ID_123, ENTITY_TYPE, user);
        sharedEntity456 = new SharedEntity.Identifier(ID_456, ENTITY_TYPE, user);
        sharedEntity789 = new SharedEntity.Identifier(ID_789, ENTITY_TYPE, user);
        sharedEntity101112 = new SharedEntity.Identifier(ID_101112, ENTITY_TYPE, user);

        mapOfEntities = new HashMap<Long,SharedEntity>(4);
        mapOfEntities.put(ID_123, sharedEntity123);
        mapOfEntities.put(ID_456, sharedEntity456);
        mapOfEntities.put(ID_789, sharedEntity789);
        mapOfEntities.put(ID_101112, sharedEntity101112);

        favouritesManager = new DefaultFavouritesManager(favouriteStore, sharedEntityAccessorFactory, shareManager);
    }

    @After
    public void tearDown()
    {
        user = null;
        sharedEntity123 = null;
        sharedEntity456 = null;
        sharedEntity789 = null;
        sharedEntity101112 = null;
        mapOfEntities = null;
        sharedEntityAccessorFactory = null;
        sharedEntityAccessor = null;
        favouriteStore = null;
        shareManager = null;
        favouritesManager = null;
    }

    /**
     * Make sure that the page moves up correctly.
     *
     * @throws PermissionException just throw this up the stack in the test.
     */
    @Test
    public void testIncreaseSequence() throws PermissionException
    {
        _testHappyPath(sharedEntity456, ImmutableList.of(sharedEntity456, sharedEntity123, sharedEntity789, sharedEntity101112), new IncreaseReorderCommand());
    }

    /**
     * Make sure that increasing the first page is a no-op.
     *
     * @throws PermissionException just throw this up the stack in the test.
     */
    @Test
    public void testIncreaseFirstIsNoop() throws PermissionException
    {
        _testHappyPath(sharedEntity123, ImmutableList.of(sharedEntity123, sharedEntity456, sharedEntity789, sharedEntity101112), new IncreaseReorderCommand());
    }

    /**
     * Make sure that a page moves down correctly.
     *
     * @throws PermissionException just throw this up the stack in the test.
     */
    @Test
    public void testDecreaseSequence() throws PermissionException
    {
        _testHappyPath(sharedEntity456, ImmutableList.of(sharedEntity123, sharedEntity789, sharedEntity456, sharedEntity101112), new DecreaseReorderCommand());
    }

    /**
     * Make sure that decreasing the last page is a no-op.
     *
     * @throws PermissionException just throw this up the stack in the test.
     */
    @Test
    public void testDecreaseLastIsNoop() throws PermissionException
    {
        _testHappyPath(sharedEntity101112, ImmutableList.of(sharedEntity123, sharedEntity456, sharedEntity789, sharedEntity101112), new DecreaseReorderCommand());
    }

    /**
     * Make sure that a page moves to start correctly.
     *
     * @throws PermissionException just throw this up the stack in the test.
     */
    @Test
    public void testMoveToStart() throws PermissionException
    {
        _testHappyPath(sharedEntity789, ImmutableList.of(sharedEntity789, sharedEntity123, sharedEntity456, sharedEntity101112), new StartReorderCommand());
    }

    /**
     * Make moving start page to the start is a no-op.
     *
     * @throws PermissionException just throw this up the stack in the test.
     */
    @Test
    public void testMoveStartToStartIsNoop() throws PermissionException
    {
        _testHappyPath(sharedEntity123, ImmutableList.of(sharedEntity123, sharedEntity456, sharedEntity789, sharedEntity101112), new StartReorderCommand());
    }

    /**
     * Make sure that a page moves to end correctly.
     *
     * @throws PermissionException just throw this up the stack in the test.
     */
    @Test
    public void testMoveToEnd() throws PermissionException
    {
        _testHappyPath(sharedEntity456, ImmutableList.of(sharedEntity123, sharedEntity789, sharedEntity101112, sharedEntity456), new EndReorderingCommand());
    }

    /**
     * Make sure that moving the end page to the end is a no-op.
     *
     * @throws PermissionException just throw this up the stack in the test.
     */
    @Test
    public void testMoveEndToEndIsNoop() throws PermissionException
    {
        _testHappyPath(sharedEntity101112, ImmutableList.of(sharedEntity123, sharedEntity456, sharedEntity789, sharedEntity101112), new EndReorderingCommand());
    }

    /**
     * Dead favourites should be deleted.
     *
     * @throws com.atlassian.jira.exception.PermissionException just pass it up the stack as a test error.
     */
    @Test
    public void testDeleteDeadFavourite() throws PermissionException
    {
        mockTargetFavourite(sharedEntity123);

        final List<Long> favList = ImmutableList.of(ID_123, ID_456, ID_789);
        when(favouriteStore.getFavouriteIds(user, ENTITY_TYPE)).thenReturn(favList);

        // first entity should be available
        when(sharedEntityAccessor.getSharedEntity(ID_123)).thenReturn(sharedEntity123);
        when(sharedEntityAccessor.hasPermissionToUse(user.getDirectoryUser(), sharedEntity123)).thenReturn(true);

        // second entity should not exist -- no need to mock the null return

        // third entity should be available.
        when(sharedEntityAccessor.getSharedEntity(ID_789)).thenReturn(sharedEntity789);
        when(sharedEntityAccessor.hasPermissionToUse(user.getDirectoryUser(), sharedEntity789)).thenReturn(true);

        // we should remove this entity.
        when(favouriteStore.removeFavourite(user, sharedEntity456)).thenReturn(true);

        // we should only update these sequences.
        favouriteStore.updateSequence(user, ImmutableList.of(sharedEntity789, sharedEntity123));

        favouritesManager.decreaseFavouriteSequence(user, sharedEntity123);
        verify(favouriteStore).removeFavourite(user, sharedEntity456);
    }

    /**
     * Dead favourites should be deleted. Check what happens when the dead favourite is the one being moved.
     *
     * @throws com.atlassian.jira.exception.PermissionException just pass it up the stack as a test error.
     */
    @Test
    public void testDeleteDeadFavouriteWhenMovingIsDead() throws PermissionException
    {
        mockTargetFavourite(sharedEntity123);

        final List<Long> favList = ImmutableList.of(ID_123, ID_789);
        when(favouriteStore.getFavouriteIds(user, ENTITY_TYPE)).thenReturn(favList);

        // should return an entity.
        when(sharedEntityAccessor.getSharedEntity(ID_789)).thenReturn(sharedEntity789);
        when(sharedEntityAccessor.hasPermissionToUse(user.getDirectoryUser(), sharedEntity789)).thenReturn(true);

        // should remove invalid favourite.
        when(favouriteStore.removeFavourite(user, sharedEntity123)).thenReturn(true);

        // should only save valid shares.
        favouriteStore.updateSequence(user, ImmutableList.of(sharedEntity789));

        favouritesManager.decreaseFavouriteSequence(user, sharedEntity123);

        verify(favouriteStore).removeFavourite(user, sharedEntity123);
    }

    /**
     * Make sure that reordering up works as expected when you no longer have permission to see a favourite.
     *
     * @throws PermissionException just throw this as a test failure.
     */
    @Test
    public void testIncreaseWithGap() throws PermissionException
    {
        _testReorderWithGap(sharedEntity789, ImmutableList.of(sharedEntity789, sharedEntity123, sharedEntity101112, sharedEntity456), new IncreaseReorderCommand());
    }

    /**
     * Make sure that reordering up works as expected when you no longer have permission to see a favourite.
     *
     * @throws PermissionException just throw this as a test failure.
     */
    @Test
    public void testDecreaseWithGap() throws PermissionException
    {
        _testReorderWithGap(sharedEntity123, ImmutableList.of(sharedEntity789, sharedEntity123, sharedEntity101112, sharedEntity456), new DecreaseReorderCommand());
    }

    /**
     * Make sure that reordering up works as expected when you no longer have permission to see a favourite.
     *
     * @throws PermissionException just throw this as a test failure.
     */
    @Test
    public void testMoveToStartWithGap() throws PermissionException
    {
        _testReorderWithGap(sharedEntity101112, ImmutableList.of(sharedEntity101112, sharedEntity123, sharedEntity789, sharedEntity456), new StartReorderCommand());
    }

    /**
     * Make sure that reordering up works as expected when you no longer have permission to see a favourite.
     *
     * @throws PermissionException just throw this as a test failure.
     */
    @Test
    public void testMoveToEndWithGap() throws PermissionException
    {
        _testReorderWithGap(sharedEntity123, ImmutableList.of(sharedEntity789, sharedEntity101112, sharedEntity123, sharedEntity456), new EndReorderingCommand());
    }

    /**
     * Make sure that reordering a SharedEntity that is no longer accessible works as expected.
     *
     * @throws com.atlassian.jira.exception.PermissionException just throw for test failure.
     */
    @Test
    public void testReorderOnNoPermission() throws PermissionException
    {
        final List<SharedEntity> reorderList = ImmutableList.of(sharedEntity123, sharedEntity789, sharedEntity101112, sharedEntity456);

        _testReorderWithGap(sharedEntity456, reorderList, new IncreaseReorderCommand());
        _testReorderWithGap(sharedEntity456, reorderList, new DecreaseReorderCommand());
        _testReorderWithGap(sharedEntity456, reorderList, new StartReorderCommand());
        _testReorderWithGap(sharedEntity456, reorderList, new EndReorderingCommand());
    }

    private void _testReorderWithGap(final SharedEntity entityUnderTest, final List<SharedEntity> reorderList, final ReorderingCommand command) throws PermissionException
    {
        reset(favouriteStore, sharedEntityAccessor, sharedEntityAccessorFactory);
        mockTargetFavourite(entityUnderTest);

        final List<Long> favList = ImmutableList.of(ID_123, ID_456, ID_789, ID_101112);
        when(favouriteStore.getFavouriteIds(user, ENTITY_TYPE)).thenReturn(favList);

        for (final Long favId : favList)
        {
            final SharedEntity sharedEntity = mapOfEntities.get(favId);
            when(sharedEntityAccessor.getSharedEntity(favId)).thenReturn(sharedEntity);
            when(sharedEntityAccessor.hasPermissionToUse(user.getDirectoryUser(), sharedEntity)).thenReturn(!favId.equals(ID_456));
        }

        command.resequence(favouritesManager, user.getDirectoryUser(), entityUnderTest);

        verify(favouriteStore).updateSequence(user, reorderList);
    }

    private void _testHappyPath(final SharedEntity entityUnderTest, final List<SharedEntity> reorderList, final ReorderingCommand command) throws PermissionException
    {
        mockTargetFavourite(entityUnderTest);

        final List<Long> favList = ImmutableList.of(ID_123, ID_456, ID_789, ID_101112);
        when(favouriteStore.getFavouriteIds(user, ENTITY_TYPE)).thenReturn(favList);

        for (final Long favId : favList)
        {
            final SharedEntity sharedEntity = mapOfEntities.get(favId);
            when(sharedEntityAccessor.getSharedEntity(favId)).thenReturn(sharedEntity);
            when(sharedEntityAccessor.hasPermissionToUse(user.getDirectoryUser(), sharedEntity)).thenReturn(true);
        }


        command.resequence(favouritesManager, user.getDirectoryUser(), entityUnderTest);
        verify(favouriteStore).updateSequence(user, reorderList);
    }

    private void mockTargetFavourite(final SharedEntity targetFavourite)
    {
        when(sharedEntityAccessorFactory.getSharedEntityAccessor(targetFavourite.getEntityType())).thenReturn(sharedEntityAccessor);
    }

    interface ReorderingCommand
    {
        void resequence(FavouritesManager<SharedEntity> favouritesManager, User user, SharedEntity entityUnderTest) throws PermissionException;
    }

    static class IncreaseReorderCommand implements ReorderingCommand
    {
        public void resequence(final FavouritesManager<SharedEntity> favouritesManager, final User user, final SharedEntity entityUnderTest) throws PermissionException
        {
            favouritesManager.increaseFavouriteSequence(new DelegatingApplicationUser(user.getName(), user), entityUnderTest);
        }
    }

    static class DecreaseReorderCommand implements ReorderingCommand
    {
        public void resequence(final FavouritesManager<SharedEntity> favouritesManager, final User user, final SharedEntity entityUnderTest) throws PermissionException
        {
            favouritesManager.decreaseFavouriteSequence(new DelegatingApplicationUser(user.getName(), user), entityUnderTest);
        }
    }

    static class StartReorderCommand implements ReorderingCommand
    {
        public void resequence(final FavouritesManager<SharedEntity> favouritesManager, final User user, final SharedEntity entityUnderTest) throws PermissionException
        {
            favouritesManager.moveToStartFavouriteSequence(new DelegatingApplicationUser(user.getName(), user), entityUnderTest);
        }
    }

    static class EndReorderingCommand implements ReorderingCommand
    {
        public void resequence(final FavouritesManager<SharedEntity> favouritesManager, final User user, final SharedEntity entityUnderTest) throws PermissionException
        {
            favouritesManager.moveToEndFavouriteSequence(new DelegatingApplicationUser(user.getName(), user), entityUnderTest);
        }
    }
}
