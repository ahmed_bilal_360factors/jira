package com.atlassian.jira.plugin.permission;

import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.permission.ProjectPermissionCategory;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.util.validation.ValidationException;

import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.conditions.ConditionLoadingException;
import com.google.common.collect.Maps;
import org.dom4j.DocumentException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @since v6.3
 */
@RunWith(MockitoJUnitRunner.class)
public class TestProjectPermissionModuleDescriptorImpl
{
    private static final String CATEGORY = "PROJECTS";
    private static final String COND_CLASS = "extractors.are.awesome.Condition";
    private static final String DESC_KEY = "permission.description.key";
    private static final String KEY = "permission.key";
    private static final String NAME_KEY = "permission.name.key";

    @Mock private Condition condition;
    private ProjectPermissionModuleDescriptorMockUtil util = new ProjectPermissionModuleDescriptorMockUtil();

    @Test
    public void parsesValidConditionIfExist() throws ConditionLoadingException, DocumentException
    {
        util.mockWebFragmentHelperForCondition(COND_CLASS, condition);
        ProjectPermissionModuleDescriptor descriptor = util.createDescriptor(CATEGORY, COND_CLASS, NAME_KEY, DESC_KEY, KEY);
        assertThat(descriptor.getModule().getCondition(), equalTo(condition));
    }

    @Test
    public void parsesValidProjectPermission() throws DocumentException
    {
        ProjectPermissionModuleDescriptor descriptor = util.createDescriptor(
                CATEGORY, NAME_KEY, DESC_KEY, KEY);
        ProjectPermission permission = descriptor.getModule();

        assertEquals(KEY, permission.getKey());
        assertEquals(NAME_KEY, permission.getNameI18nKey());
        assertEquals(DESC_KEY, permission.getDescriptionI18nKey());
        assertEquals(ProjectPermissionCategory.PROJECTS, permission.getCategory());
    }

    @Test
    public void projectPermissionDefaultsToAlwaysDisplayConditionIfNull() throws DocumentException
    {
        ProjectPermissionModuleDescriptor descriptor = util.createDescriptor(
                CATEGORY, NAME_KEY, DESC_KEY, KEY);
        ProjectPermission permission = descriptor.getModule();

        assertThat(permission.getCondition().getClass().getName(), containsString("AlwaysDisplayCondition"));
        assertThat(permission.getCondition().shouldDisplay(Maps.<String, Object>newHashMap()), is(true));
    }

    @Test(expected = PluginParseException.class)
    public void projectPermissionHasToHaveValidConditionIfExist() throws ConditionLoadingException, DocumentException
    {
        util.mockWebFragmentHelperForCondition(COND_CLASS, ConditionLoadingException.class);
        util.createDescriptor(CATEGORY, COND_CLASS, NAME_KEY, DESC_KEY, KEY);
    }

    @Test(expected = ValidationException.class)
    public void projectPermissionHasToHaveKey() throws DocumentException
    {
        util.createDescriptor(CATEGORY, NAME_KEY, DESC_KEY, "");
    }

    @Test(expected = ValidationException.class)
    public void projectPermissionHasToHaveName() throws DocumentException
    {
        util.createDescriptor(CATEGORY, "", DESC_KEY, KEY);
    }

    @Test(expected = ValidationException.class)
    public void projectPermissionHasToHaveCategory() throws DocumentException
    {
        util.createDescriptor("", NAME_KEY, DESC_KEY, KEY);
    }

    @Test(expected = PluginParseException.class)
    public void projectPermissionCategoryHasToBeValid() throws DocumentException
    {
        util.createDescriptor("NAUGHTY", NAME_KEY, DESC_KEY, KEY);
    }

    @Test(expected = PluginParseException.class)
    public void cannotOverrideSystemProjectPermission() throws DocumentException
    {
        util.createDescriptor(CATEGORY, NAME_KEY, DESC_KEY, "ADMINISTER_PROJECTS");
    }
}
