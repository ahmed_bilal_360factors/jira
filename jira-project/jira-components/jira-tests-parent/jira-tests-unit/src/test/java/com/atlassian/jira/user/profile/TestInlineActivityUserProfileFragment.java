package com.atlassian.jira.user.profile;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v6.4
 */
@RunWith (MockitoJUnitRunner.class)
public class TestInlineActivityUserProfileFragment
{
    @Mock
    PluginAccessor pluginAccessor;
    @Mock
    I18nHelper.BeanFactory beanFactory;
    @Mock
    SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    User user;
    @Captor
    ArgumentCaptor<Map<String, Object>> contextCaptor;

    InlineActivityUserProfileFragment inlineActivityUserProfileFragment;

    @Before
    public void setUp()
    {
        this.inlineActivityUserProfileFragment = spy(new InlineActivityUserProfileFragment(pluginAccessor, beanFactory, soyTemplateRenderer));
        doReturn("/my-jira").when(inlineActivityUserProfileFragment).getRequestContextPath();
        doReturn("awesome-admin").when(inlineActivityUserProfileFragment).getJiraUrlEncoded(null);
    }

    @Test
    public void shouldPutContextUrlForInlineActivityStream() throws SoyException
    {
        inlineActivityUserProfileFragment.getFragmentHtml(user, user);
        verify(soyTemplateRenderer, times(1)).render(anyString(), anyString(), contextCaptor.capture());

        assertEquals("correct URL was given to the template", contextCaptor.getValue().get("activityStreamUrl"), "/my-jira/activity?maxResults=10&streams=user+IS+awesome-admin");
    }

    @Test
    public void shouldReturnEmptyStringInCaseOfSoyException() throws SoyException
    {
        when(soyTemplateRenderer.render(anyString(), anyString(), anyMap())).thenThrow(SoyException.class);

        String html = inlineActivityUserProfileFragment.getFragmentHtml(user, user);
        assertEquals("", html);
    }

    @Test
    public void shouldReturnCorrectFragmentVisibilityDependingOnPluginStatus()
    {
        when(pluginAccessor.isPluginModuleEnabled(anyString())).thenReturn(Boolean.FALSE);
        assertFalse("plugin disabled so fragment should not be visible (false)", inlineActivityUserProfileFragment.showFragment(null, null));

        when(pluginAccessor.isPluginModuleEnabled(anyString())).thenReturn(Boolean.TRUE);
        assertTrue("plugin enabled so fragment should be visible (true)", inlineActivityUserProfileFragment.showFragment(null, null));
    }
}
