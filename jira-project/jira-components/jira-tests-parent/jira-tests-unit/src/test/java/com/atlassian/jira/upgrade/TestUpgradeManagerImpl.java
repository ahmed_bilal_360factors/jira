package com.atlassian.jira.upgrade;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.bean.export.AutoExport;
import com.atlassian.jira.bean.export.IllegalXMLCharactersException;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.ReindexMessageManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.index.request.ReindexRequestManager;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingParams;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.upgrade.MockUpgradeTask;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.DowngradeUtilsImpl;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.index.IndexLifecycleManager;
import com.atlassian.jira.web.util.OutlookDate;
import com.atlassian.jira.web.util.OutlookDateManager;
import com.atlassian.scheduler.SchedulerHistoryService;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.ofbiz.core.entity.EntityCondition;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import static com.atlassian.jira.config.properties.APKeys.JIRA_PATCHED_VERSION;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestUpgradeManagerImpl
{
    private static final String VERSION = "4.0";
    private static final String ENTITY_UPGRADE_HISTORY = "UpgradeHistory";
    private static final String FIELD_TARGETBUILD = "targetbuild";
    private static final String FIELD_TIMEPERFORMED = "timeperformed";
    private static final String FIELD_TARGETVERSION = "targetversion";
    private static final String FIELD_UPGRADECLASS = "upgradeclass";
    private static final String FIELD_STATUS = "status";
    private static final String STATUS_PENDING = "pending";
    private static final String STATUS_COMPLETE = "complete";

    private static final Collection<MockUpgradeTask> ALL_UPGRADES = ImmutableList.of(
            new MockUpgradeTask("1.0", "short desc"),
            new MockUpgradeTask("1.0.1", "short desc2"),
            new MockUpgradeTask("1.2", "short desc3"),
            new MockUpgradeTask("1.2.3", "short desc4"),
            new MockUpgradeTask("1.3", "short desc5"),
            new MockUpgradeTask("27", "short desc6"),
            new MockUpgradeTask("0.9", "short desc7"),
            new MockUpgradeTask("1.1", "short desc8")
    );
    private static final String ENTITY_UPGRADE_VERSION_HISTORY = "UpgradeVersionHistory";

    @Mock private ApplicationProperties mockApplicationProperties;
    @Mock private BuildUtilsInfo mockBuildUtilsInfo;
    @Mock private BuildVersionRegistry mockBuildVersionRegistry;
    @Mock private DowngradeUtilsImpl mockDowngradeUtilsImpl;
    @Mock private EventPublisher mockEventPublisher;
    @Mock private FeatureManager mockFeatureManager;
    @Mock private I18nHelper mockI18nHelper;
    @Mock private I18nHelper.BeanFactory mockI18HelperFactory;
    @Mock private IndexLifecycleManager mockIndexLifecycleManager;
    @Mock private JiraLicenseService mockJiraLicenseService;
    @Mock private LicenseDetails mockLicenseDetails;
    @Mock private OfBizDelegator mockDelegator;
    @SuppressWarnings("deprecation")
    @Mock private OutlookDateManager mockDateManager;
    @Mock private ReindexMessageManager mockReindexMessageManager;
    @Mock private ReindexRequestManager mockReindexRequestManager;
    @Mock private SchedulerService mockSchedulerService;
    @Mock private SchedulerHistoryService mockSchedulerHistoryService;
    @Mock private EntityEngine entityEngine;
    @Mock private ClusterLockService clusterLockService;

    private UpgradeManager upgradeManager;

    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
        when(mockJiraLicenseService.getLicense()).thenReturn(mockLicenseDetails);
        when(mockLicenseDetails.isLicenseSet()).thenReturn(false);  // we bypass license checks if it is not set.
        when(mockBuildUtilsInfo.getCurrentBuildNumber()).thenReturn("99999");
        upgradeManager = createForTest();
    }

    private UpgradeManagerImpl createForTest()
    {
        return createForTest(ALL_UPGRADES);
    }

    private UpgradeManagerImpl createForTest(final Collection<MockUpgradeTask> allUpgrades)
    {
        return new UpgradeManagerImpl(mockJiraLicenseService, mockBuildUtilsInfo, mockI18HelperFactory,
                mockApplicationProperties, mockBuildVersionRegistry, mockDelegator, mockEventPublisher,
                mockIndexLifecycleManager, mockDateManager, mockFeatureManager, allUpgrades, allUpgrades, mockDowngradeUtilsImpl,
                mockReindexMessageManager, mockReindexRequestManager, mockSchedulerService, mockSchedulerHistoryService,
                entityEngine, clusterLockService);
    }
    
    private UpgradeManagerImpl createForTest(Iterable<UpgradeTask> upgradeTasks, Iterable<UpgradeTask> setupUpgradeTasks)
    {
        return new UpgradeManagerImpl(mockJiraLicenseService, mockBuildUtilsInfo, mockI18HelperFactory,
                mockApplicationProperties, mockBuildVersionRegistry, mockDelegator, mockEventPublisher,
                mockIndexLifecycleManager, mockDateManager, mockFeatureManager, upgradeTasks, setupUpgradeTasks,
                mockDowngradeUtilsImpl, mockReindexMessageManager, mockReindexRequestManager, mockSchedulerService,
                mockSchedulerHistoryService, entityEngine, clusterLockService);
    }

    private UpgradeManagerImpl createForTest(final AutoExport mockAutoExport)
    {
        return new UpgradeManagerImpl(mockJiraLicenseService, mockBuildUtilsInfo, mockI18HelperFactory,
                mockApplicationProperties, mockBuildVersionRegistry, mockDelegator, mockEventPublisher,
                mockIndexLifecycleManager, mockDateManager, mockFeatureManager, ALL_UPGRADES, ALL_UPGRADES, mockDowngradeUtilsImpl,
                mockReindexMessageManager, mockReindexRequestManager, mockSchedulerService, mockSchedulerHistoryService,
                entityEngine, clusterLockService)
        {
            @Override
            protected AutoExport getAutoExport(String defaultBackupPath)
            {
                return mockAutoExport;
            }
        };
    }

    private UpgradeManagerImpl createForTest(final UpgradeHistoryItem upgradeHistoryItem)
    {
        return new UpgradeManagerImpl(mockJiraLicenseService, mockBuildUtilsInfo, mockI18HelperFactory,
                mockApplicationProperties, mockBuildVersionRegistry, mockDelegator, mockEventPublisher,
                mockIndexLifecycleManager, mockDateManager, mockFeatureManager, ALL_UPGRADES, ALL_UPGRADES,
                mockDowngradeUtilsImpl, mockReindexMessageManager, mockReindexRequestManager, mockSchedulerService,
                mockSchedulerHistoryService, entityEngine, clusterLockService)
        {
            @Override
            UpgradeHistoryItem getUpgradeHistoryItemFromTasks() throws GenericEntityException
            {
                return upgradeHistoryItem;
            }
        };
    }

    @Test
    public void testDoUpdate() throws IllegalXMLCharactersException, IndexException
    {
        when(mockDelegator.findAll("UpgradeHistory")).thenReturn(Collections.<GenericValue>emptyList());

        when(mockApplicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("100");
        when(mockApplicationProperties.getOption(APKeys.JIRA_AUTO_EXPORT)).thenReturn(true);
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(400);
        when(mockBuildUtilsInfo.getDatabaseBuildNumber()).thenReturn(100);

        final UpgradeManager tested = createForTest();
        final UpgradeManager.Status status = tested.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());

        // we should have no errors
        assertTrue("Unexpected errors: " + status.getErrors(), status.successful());
    }

    @Test
    public void testDoUpgradeIfNeededAndAllowed_BadLicense_TooOldForBuild() throws IllegalXMLCharactersException, IndexException
    {
        when(mockApplicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("100");

        final Date theDate = new Date();


        when(mockBuildUtilsInfo.getCurrentBuildDate()).thenReturn(theDate);
        when(mockBuildUtilsInfo.getVersion()).thenReturn("v99");

        when(mockLicenseDetails.isLicenseSet()).thenReturn(true);
        when(mockLicenseDetails.isMaintenanceValidForBuildDate(theDate)).thenReturn(false);
        when(mockLicenseDetails.hasLicenseTooOldForBuildConfirmationBeenDone()).thenReturn(false);
        when(mockLicenseDetails.getMaintenanceEndString(any(OutlookDate.class))).thenReturn("today");

        final UpgradeManager tested = createForTest();
        final UpgradeManager.Status status = tested.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());

        // we should have one error
        assertFalse(status.successful());
        assertEquals(1, status.getErrors().size());
    }

    @Test
    public void testDoUpgradeIfNeededAndAllowed_BadLicense_ValidationFailed()
            throws IllegalXMLCharactersException, IndexException
    {
        when(mockApplicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("100");

        final Date theDate = new Date();

        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrorMessage("shite happens");
        JiraLicenseService.ValidationResult validationResult = mock(JiraLicenseService.ValidationResult.class);

        when(mockI18HelperFactory.getInstance(any(Locale.class))).thenReturn(null);

        when(mockBuildUtilsInfo.getCurrentBuildDate()).thenReturn(theDate);
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);

        when(mockLicenseDetails.isLicenseSet()).thenReturn(true);
        when(mockLicenseDetails.isMaintenanceValidForBuildDate(theDate)).thenReturn(true);

        when(mockLicenseDetails.getLicenseString()).thenReturn("licString");
        when(mockJiraLicenseService.validate(any(I18nHelper.class), eq("licString"))).thenReturn(validationResult);

        when(validationResult.getErrorCollection()).thenReturn(errorCollection);


        final UpgradeManager tested = createForTest();
        final UpgradeManager.Status status = tested.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());
        assertFalse(status.successful());
        assertEquals(1, status.getErrors().size());
    }

    @Test
    public void testUpgradesInOrder()
    {
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("0.1");
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);

        final UpgradeManagerImpl tested = createForTest();
        final SortedMap<String, UpgradeTask> upgrades = tested.getRelevantUpgradesFromList(tested.getAllUpgrades()).getUnAppliedUpgrades();
        final Iterator<UpgradeTask> iterator = upgrades.values().iterator();

        UpgradeTask task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc7");

        task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc");

        task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc2");

        task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc8");

        task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc3");

        task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc4");

        task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc5");

        task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc6");
    }

    @Test
    public void testUpgradesSubset0_9() throws IndexException
    {
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("0.9");
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);

        final UpgradeManagerImpl tested = createForTest();
        final SortedMap<String, UpgradeTask> upgrades = tested.getRelevantUpgradesFromList(tested.getAllUpgrades()).getUnAppliedUpgrades();
        final Iterator<UpgradeTask> iterator = upgrades.values().iterator();

        UpgradeTask task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc");

        task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc2");

        task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc8");

        task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc3");

        task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc4");

        task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc5");

        task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc6");
    }

    @Test
    public void testUpgradesSubset1_1()
    {
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("1.1");
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);

        final UpgradeManagerImpl tested = createForTest();

        final SortedMap<String, UpgradeTask> upgrades = tested.getRelevantUpgradesFromList(tested.getAllUpgrades()).getUnAppliedUpgrades();
        final Iterator<UpgradeTask> iterator = upgrades.values().iterator();

        UpgradeTask task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc3");

        task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc4");

        task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc5");

        task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc6");
    }

    @Test
    public void testUpgradesSubset1_3()
    {
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("1.3");
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);

        final UpgradeManagerImpl tested = createForTest();
        final SortedMap<String, UpgradeTask> upgrades = tested.getRelevantUpgradesFromList(tested.getAllUpgrades()).getUnAppliedUpgrades();
        final Iterator<UpgradeTask> iterator = upgrades.values().iterator();

        final UpgradeTask task = iterator.next();
        assertEquals(task.getShortDescription(), "short desc6");
    }

    @Test
    public void testUpgradesSubset27()
    {
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("27");
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);

        final UpgradeManagerImpl tested = createForTest();
        final SortedMap<String, UpgradeTask> upgrades = tested.getRelevantUpgradesFromList(tested.getAllUpgrades()).getUnAppliedUpgrades();
        assertEquals(upgrades.size(), 0);
    }

    @Test
    public void testDoUpgradeIfNeededNoExport() throws IllegalXMLCharactersException
    {
        when(mockDelegator.findAll("UpgradeHistory")).thenReturn(Collections.<GenericValue>emptyList());

        when(mockApplicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("100");
        when(mockApplicationProperties.getOption(APKeys.JIRA_AUTO_EXPORT)).thenReturn(true);
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(400);
        when(mockBuildUtilsInfo.getDatabaseBuildNumber()).thenReturn(100);

        final UpgradeManagerImpl tested = createForTest();
        tested.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());
    }

    @Test
    public void testDoUpgradeIfNeededWithExportErrors() throws Exception
    {
        when(mockApplicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("100");
        when(mockApplicationProperties.getOption(APKeys.JIRA_AUTO_EXPORT)).thenReturn(true);
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(400);
        when(mockBuildUtilsInfo.getDatabaseBuildNumber()).thenReturn(100);

        final String tempDir = "somedir";

        final AutoExport mockAutoExport = mock(AutoExport.class);
        when(mockAutoExport.exportData()).thenThrow(new Exception("There was an error."));

        final UpgradeManagerImpl tested = createForTest(mockAutoExport);
        final UpgradeManager.Status status = tested.doUpgradeIfNeededAndAllowed(tempDir, UpgradeManagerParams.builder().build());
        assertFalse(status.successful());
        final Collection<String> errors = status.getErrors();
        assertEquals(1, errors.size());

        final String message = errors.iterator().next();
        assertTrue(message.startsWith("Error occurred during export before upgrade:"));
    }

    @Test
    public void testDoUpgradeIfNeededWithExportIllegalCharacters() throws Exception
    {
        when(mockApplicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("100");
        when(mockApplicationProperties.getOption(APKeys.JIRA_AUTO_EXPORT)).thenReturn(true);
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(400);
        when(mockBuildUtilsInfo.getDatabaseBuildNumber()).thenReturn(100);

        final String expectedExportPath = "somedir";

        final AutoExport mockAutoExport = mock(AutoExport.class);
        when(mockAutoExport.exportData()).thenThrow(new IllegalXMLCharactersException("Bad characters."));

        final UpgradeManagerImpl tested = createForTest(mockAutoExport);
        Collection<String> errors = null;
        try
        {
            tested.doUpgradeIfNeededAndAllowed(expectedExportPath, UpgradeManagerParams.builder().build());
            fail("IllegalXMLCharactersException should have been thrown.");
        }
        catch (final IllegalXMLCharactersException e)
        {
            assertNull(errors);
            assertEquals("Bad characters.", e.getMessage());
        }
    }

    @Test
    public void testDoUpdateNoAutoExportInOnDemandJRADEV11718() throws Exception
    {
        when(mockDelegator.findAll("UpgradeHistory")).thenReturn(Collections.<GenericValue>emptyList());
        when(mockFeatureManager.isOnDemand()).thenReturn(true);

        when(mockApplicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("100");
        when(mockApplicationProperties.getOption(APKeys.JIRA_AUTO_EXPORT)).thenReturn(true);
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(400);
        when(mockBuildUtilsInfo.getDatabaseBuildNumber()).thenReturn(100);

        final AutoExport mockAutoExport = mock(AutoExport.class);

        final UpgradeManager tested = createForTest(mockAutoExport);
        final UpgradeManager.Status status = tested.doUpgradeIfNeededAndAllowed("somedir", UpgradeManagerParams.builder().build());

        // we should have no errors
        assertTrue("Unexpected errors: " + status.getErrors(), status.successful());
    }

    @Test
    public void testDoUpgradeIfNeededWithExport() throws Exception
    {
        when(mockDelegator.findAll("UpgradeHistory")).thenReturn(Collections.<GenericValue>emptyList());

        when(mockApplicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("100");
        when(mockApplicationProperties.getOption(APKeys.JIRA_AUTO_EXPORT)).thenReturn(true);
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(400);
        when(mockBuildUtilsInfo.getDatabaseBuildNumber()).thenReturn(100);

        final String expectedExportPath = "somedir";

        final AutoExport mockAutoExport = mock(AutoExport.class);
        when(mockAutoExport.exportData()).thenReturn(expectedExportPath);

        final UpgradeManagerImpl tested = createForTest(mockAutoExport);
        final UpgradeManager.Status status = tested.doUpgradeIfNeededAndAllowed(expectedExportPath, UpgradeManagerParams.builder().build());
        assertTrue("Unexpected errors: " + status.getErrors(), status.successful());
        assertEquals(expectedExportPath, tested.getExportFilePath());
    }

    @Test
    public void testGetUpgradeHistoryItemFromTasksNone() throws Exception
    {
        when(mockDelegator.findByCondition(anyString(), any(EntityCondition.class), anyCollectionOf(String.class),
                anyListOf(String.class))).thenReturn(Collections.<GenericValue>emptyList());
        final UpgradeManagerImpl tested = createForTest();
        final UpgradeHistoryItem result = tested.getUpgradeHistoryItemFromTasks();
        assertNull(result);
    }

    @Test
    public void testGetUpgradeHistoryItemFromTasksHappyPath() throws Exception
    {
        when(mockDelegator.findByCondition(anyString(), any(EntityCondition.class), anyCollectionOf(String.class),
                anyListOf(String.class))).thenReturn(Collections.<GenericValue>singletonList(
                new MockGenericValue("UpgradeHistoryLastClassForTargetBuild", ImmutableMap.of(
                        "upgradeclass", "UpgradeTask_Build106"
                )))
        );

        final BuildVersionImpl buildVersion = new BuildVersionImpl("106", "XYZ");
        when(mockBuildVersionRegistry.getVersionForBuildNumber("106")).thenReturn(buildVersion);


        final UpgradeManagerImpl tested = createForTest();

        final UpgradeHistoryItem result = tested.getUpgradeHistoryItemFromTasks();
        final UpgradeHistoryItem expected = new UpgradeHistoryItemImpl(null, "106", "XYZ", "106", null, true);
        assertEquals(result, expected);
    }

    @Test
    public void testGetUpgradeHistoryItemFromTasksBadClass() throws Exception
    {
        when(mockDelegator.findByCondition(anyString(), any(EntityCondition.class), anyCollectionOf(String.class),
                anyListOf(String.class))).thenReturn(Collections.<GenericValue>singletonList(
                new MockGenericValue("UpgradeHistoryLastClassForTargetBuild", ImmutableMap.of(
                        "upgradeclass", "BADCLASS"
                )))
        );

        final UpgradeManagerImpl tested = createForTest();
        final UpgradeHistoryItem result = tested.getUpgradeHistoryItemFromTasks();
        assertNull(result);
    }

    @Test
    public void testGetUpgradeHistoryNoPrevious() throws Exception
    {
        when(mockDelegator.findAll(Mockito.anyString(), anyListOf(String.class))).thenReturn(Collections.<GenericValue>singletonList(
                new MockGenericValue("UpgradeVersionHistory", MapBuilder.build(
                        "timeperformed", null,
                        "targetbuild", "400",
                        "targetversion", VERSION
                )))
        );

        final UpgradeManagerImpl tested = createForTest((UpgradeHistoryItem) null);

        final List<UpgradeHistoryItem> result = tested.getUpgradeHistory();
        final UpgradeHistoryItem expected = new UpgradeHistoryItemImpl(null, "400", VERSION, null, null);
        assertEquals(1, result.size());
        assertEquals(expected, result.get(0));
    }

    @Test
    public void testGetUpgradeHistoryPrevious() throws Exception
    {
        when(mockDelegator.findAll(anyString(), anyListOf(String.class))).thenReturn(Collections.<GenericValue>singletonList(
                new MockGenericValue("UpgradeVersionHistory", MapBuilder.build(
                        "timeperformed", null,
                        "targetbuild", "400",
                        "targetversion", VERSION
                )))
        );

        final UpgradeHistoryItem expected1 = new UpgradeHistoryItemImpl(null, "400", VERSION, "300", "3.0");
        final UpgradeHistoryItem expected2 = new UpgradeHistoryItemImpl(null, "300", "3.0", null, null);

        final UpgradeManagerImpl tested = createForTest(expected2);

        final List<UpgradeHistoryItem> result = tested.getUpgradeHistory();
        assertEquals(2, result.size());
        assertEquals(expected1, result.get(0));
        assertEquals(expected2, result.get(1));
    }

    @Test
    public void testUpgradesWithReindex() throws IllegalXMLCharactersException, IndexException, PermissionException
    {
        when(mockDelegator.findAll("UpgradeHistory")).thenReturn(Collections.<GenericValue>emptyList());

        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("0.9");
        when(mockApplicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");
        when(mockApplicationProperties.getOption(APKeys.JIRA_AUTO_EXPORT)).thenReturn(true);
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(400);
        when(mockBuildUtilsInfo.getDatabaseBuildNumber()).thenReturn(100);
        when(mockIndexLifecycleManager.size()).thenReturn(8);
        when(mockIndexLifecycleManager.reIndexAll(any(Context.class))).thenReturn(1l);
        when(mockReindexRequestManager.isReindexRequested(EnumSet.of(ReindexRequestType.IMMEDIATE))).thenReturn(true);

        final UpgradeManagerImpl man = createForTest();
        UpgradeManager.Status status = man.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());

        // Need to assert the errors collection here as the upgrade task swallows Throwable
        assertTrue("Unexpected errors: " + status.getErrors(), status.successful());
        verify(mockReindexRequestManager).processPendingRequests(true, EnumSet.of(ReindexRequestType.IMMEDIATE), false);
    }

    @Test
    public void testUpgradesWithoutReindex() throws IllegalXMLCharactersException, IndexException
    {
        when(mockDelegator.findAll("UpgradeHistory")).thenReturn(Collections.<GenericValue>emptyList());

        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("1.2");
        when(mockApplicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");
        when(mockApplicationProperties.getOption(APKeys.JIRA_AUTO_EXPORT)).thenReturn(true);
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(400);
        when(mockBuildUtilsInfo.getDatabaseBuildNumber()).thenReturn(100);

        final UpgradeManagerImpl tested = createForTest();
        tested.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());
    }

    @Test
    public void testSequencerIsRefreshedAfterStandardUpgrade() throws Exception
    {
        when(mockDelegator.findAll("UpgradeHistory")).thenReturn(Collections.<GenericValue>emptyList());

        when(mockApplicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("100");
        when(mockApplicationProperties.getOption(APKeys.JIRA_AUTO_EXPORT)).thenReturn(true);
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(400);
        when(mockBuildUtilsInfo.getDatabaseBuildNumber()).thenReturn(100);

        UpgradeManager tested = createForTest();
        UpgradeManager.Status status = tested.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());
        assertTrue("Unexpected errors: " + status.getErrors(), status.successful());
        verify(mockDelegator).refreshSequencer();
    }

    @Test
    public void testSequencerIsRefreshedAfterUpgradeWithErrors() throws Exception
    {
        when(mockDelegator.findAll("UpgradeHistory")).thenReturn(Collections.<GenericValue>emptyList());

        when(mockApplicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("100");
        when(mockApplicationProperties.getOption(APKeys.JIRA_AUTO_EXPORT)).thenReturn(true);
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(400);
        when(mockBuildUtilsInfo.getDatabaseBuildNumber()).thenReturn(100);

        final UpgradeManager tested = createForTest(ImmutableList.of(taskWithError(false)), Collections.<UpgradeTask>emptyList());
        UpgradeManager.Status status = tested.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());
        assertFalse(status.successful());
        assertEquals(1, status.getErrors().size());
        verify(mockDelegator).refreshSequencer();
    }

    private UpgradeTask taskWithError(boolean isSetup) throws Exception
    {
        final UpgradeTask answer = mock(UpgradeTask.class);
        when(answer.getBuildNumber()).thenReturn("150");
        when(answer.getShortDescription()).thenReturn("Testing task");
        when(answer.getScheduleOption()).thenReturn(UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED);
        doThrow(new Exception("Surprise!!!")).when(answer).doUpgrade(isSetup);
        return answer;
    }

    @Test
    public void testIndexingNotPerformedWhenDisabledViaSystemProperty() throws Exception
    {
        System.setProperty("upgrade.reindex.allowed","false");
        when(mockDelegator.findAll("UpgradeHistory")).thenReturn(Collections.<GenericValue>emptyList());

        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("0.9");
        when(mockApplicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");
        when(mockApplicationProperties.getOption(APKeys.JIRA_AUTO_EXPORT)).thenReturn(true);
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(400);
        when(mockBuildUtilsInfo.getDatabaseBuildNumber()).thenReturn(100);
        when(mockI18HelperFactory.getInstance(Locale.ENGLISH)).thenReturn(mockI18nHelper);

        final UpgradeManagerImpl man = createForTest();
        UpgradeManager.Status status = man.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());

        assertTrue("Unexpected errors: " + status.getErrors(), status.successful());
        verify(mockIndexLifecycleManager, never()).reIndexAll(any(Context.class), any(IssueIndexingParams.class));
    }

    @Test
    public void testDelayedTaskNotRun() throws Exception
    {
        when(mockDelegator.findAll("UpgradeHistory")).thenReturn(Collections.<GenericValue>emptyList());
        when(mockDelegator.findByAnd(ENTITY_UPGRADE_HISTORY, FieldMap.build(FIELD_STATUS, STATUS_PENDING)))
                .thenReturn(ImmutableList.of((GenericValue) new MockGenericValue(ENTITY_UPGRADE_HISTORY)));
        when(mockApplicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("100");
        when(mockApplicationProperties.getOption(APKeys.JIRA_AUTO_EXPORT)).thenReturn(true);
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(400);
        when(mockBuildUtilsInfo.getDatabaseBuildNumber()).thenReturn(100);

        MockUpgradeTask upgradeTask = Mockito.mock(MockUpgradeTask.class);
        when(upgradeTask.getBuildNumber()).thenReturn("301");
        when(upgradeTask.getShortDescription()).thenReturn("delayable task");
        when(upgradeTask.getClassName()).thenReturn("TestClass");
        when(upgradeTask.getScheduleOption()).thenReturn(UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED);

        ImmutableList.Builder<UpgradeTask> tasksBuilder = ImmutableList.<UpgradeTask>builder().addAll(ALL_UPGRADES).add(upgradeTask);

        final UpgradeManager tested = createForTest(tasksBuilder.build(), ImmutableList.<UpgradeTask>of());
        UpgradeManager.Status status = tested.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());
        assertTrue(status.successful());
        verify(mockSchedulerService).scheduleJob(any(JobId.class), any(JobConfig.class));
        verify(upgradeTask, never()).doUpgrade(false);
        verify(mockDelegator, times(1)).createValue(eq("UpgradeHistory"), argThat(new PendingTaskMatcher()));
    }

    @Test
    public void testDelayedTaskNotScheduledWhenDisabled() throws Exception
    {
        when(mockDelegator.findAll("UpgradeHistory")).thenReturn(Collections.<GenericValue>emptyList());

        when(mockApplicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");
        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("100");
        when(mockApplicationProperties.getOption(APKeys.JIRA_AUTO_EXPORT)).thenReturn(true);
        when(mockApplicationProperties.getOption(APKeys.JIRA_UPGRADE_FORCE_MANUAL_SCHEDULE)).thenReturn(true);
        when(mockBuildUtilsInfo.getVersion()).thenReturn(VERSION);
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(400);
        when(mockBuildUtilsInfo.getDatabaseBuildNumber()).thenReturn(100);

        MockUpgradeTask upgradeTask = Mockito.mock(MockUpgradeTask.class);
        when(upgradeTask.getBuildNumber()).thenReturn("301");
        when(upgradeTask.getShortDescription()).thenReturn("delayable task");
        when(upgradeTask.getClassName()).thenReturn("TestClass");
        when(upgradeTask.getScheduleOption()).thenReturn(UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED);

        ImmutableList.Builder<UpgradeTask> tasksBuilder = ImmutableList.<UpgradeTask>builder().addAll(ALL_UPGRADES).add(upgradeTask);

        final UpgradeManager tested = createForTest(tasksBuilder.build(), ImmutableList.<UpgradeTask>of());
        UpgradeManager.Status status = tested.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());
        assertTrue(status.successful());
        verify(mockSchedulerService, never()).scheduleJob(any(JobId.class), any(JobConfig.class));
        verify(upgradeTask, never()).doUpgrade(false);
        verify(mockDelegator, times(1)).createValue(eq("UpgradeHistory"), argThat(new PendingTaskMatcher()));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testHasUpgradeTaskRun() throws Exception
    {
        UpgradeManagerImpl upgradeManager = createForTest();
        when(mockDelegator.findByAnd(anyString(), any(Map.class))).thenReturn(Collections.<GenericValue>singletonList(
                new MockGenericValue("UpgradeHistoryLastClassForTargetBuild", ImmutableMap.of(
                        "upgradeclass", "UpgradeTask_Build301", "status", "complete"
                ))));

        assertThat(upgradeManager.hasUpgradeTaskRun(com.atlassian.jira.upgrade.MockUpgradeTask.class), is(true));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testHasUpgradeTaskNotRun() throws Exception
    {
        UpgradeManagerImpl upgradeManager = createForTest();
        when(mockDelegator.findByAnd(anyString(), any(Map.class))).thenReturn(Collections.<GenericValue>singletonList(
                new MockGenericValue("UpgradeHistoryLastClassForTargetBuild", ImmutableMap.of(
                        "upgradeclass", "UpgradeTask_Build301", "status", "pending"
                ))));

        assertThat(upgradeManager.hasUpgradeTaskRun(com.atlassian.jira.upgrade.MockUpgradeTask.class), is(false));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testHasUpgradeTaskNotFound() throws Exception
    {
        UpgradeManagerImpl upgradeManager = createForTest();
        when(mockDelegator.findByAnd(anyString(), any(Map.class))).thenReturn(Collections.<GenericValue>emptyList());

        assertThat(upgradeManager.hasUpgradeTaskRun(com.atlassian.jira.upgrade.MockUpgradeTask.class), is(false));
    }

    @Test
    public void testOnlyDelayedTasksGetScheduledAsDelayed()
            throws IllegalXMLCharactersException, SchedulerServiceException
    {
        ImmutableList<MockUpgradeTask> tasks = getUpgradeTasks(
                UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED,
                UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED,
                UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED);

        upgradeManager = createForTest(tasks);
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(1);
        when(mockApplicationProperties.getString(APKeys.JIRA_PATCHED_VERSION)).thenReturn("0");
        upgradeManager.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());

        ArgumentCaptor<Map> capturedArguments = ArgumentCaptor.forClass(Map.class);
        verify(mockDelegator, times(3)).createValue(eq(ENTITY_UPGRADE_HISTORY), (Map<String, Object>) capturedArguments.capture());

        List<Map> invocations = capturedArguments.getAllValues();
        for (int i = 0 ; i < invocations.size(); i++)
        {
            Map<String, Object> invocation = invocations.get(i);
            MockUpgradeTask task = tasks.get(i);
            assertThat((String) invocation.get(FIELD_UPGRADECLASS), equalTo(task.getClass().getName()));
            assertThat((String) invocation.get(FIELD_TARGETBUILD), equalTo(task.getBuildNumber()));
            assertThat((String) invocation.get(FIELD_STATUS), equalTo(STATUS_PENDING));
        }

        verify(mockSchedulerService).scheduleJob(eq(JobId.of(DelayedUpgradeJobRunner.class.getName())), isA(JobConfig.class));
    }

    @Test
    public void testSchedulerServiceDoesNotScheduleWhenOptionDisabled()
            throws IllegalXMLCharactersException, SchedulerServiceException
    {
        when(mockApplicationProperties.getOption(eq(APKeys.JIRA_UPGRADE_FORCE_MANUAL_SCHEDULE))).thenReturn(true);
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(1);
        when(mockApplicationProperties.getString(APKeys.JIRA_PATCHED_VERSION)).thenReturn("0");

        ImmutableList<MockUpgradeTask> tasks = getUpgradeTasks(
                UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED);

        upgradeManager = createForTest(tasks);

        upgradeManager.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());

        verify(mockDelegator, times(1)).createValue(eq(ENTITY_UPGRADE_HISTORY), isA(Map.class));

        verify(mockSchedulerService, times(0)).scheduleJob(eq(JobId.of(DelayedUpgradeJobRunner.class.getName())), isA(JobConfig.class));
    }

    @Test
    public void testOnlyImmediateTasksGetScheduledAsImmediate() throws IllegalXMLCharactersException
    {
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(1);
        when(mockApplicationProperties.getString(APKeys.JIRA_PATCHED_VERSION)).thenReturn("0");

        ImmutableList<MockUpgradeTask> tasks = getUpgradeTasks(
                UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED,
                UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED,
                UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED);

        upgradeManager = createForTest(tasks);
        upgradeManager.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());

        ArgumentCaptor<Map> capturedArguments = ArgumentCaptor.forClass(Map.class);
        verify(mockDelegator, times(3)).createValue(eq(ENTITY_UPGRADE_HISTORY), (Map<String, Object>) capturedArguments.capture());

        List<Map> invocations = capturedArguments.getAllValues();
        for (int i = 0 ; i < invocations.size(); i++)
        {
            Map<String, Object> invocation = invocations.get(i);
            MockUpgradeTask task = tasks.get(i);
            assertThat((String) invocation.get(FIELD_UPGRADECLASS), equalTo(task.getClass().getName()));
            assertThat((String) invocation.get(FIELD_TARGETBUILD), equalTo(task.getBuildNumber()));
            assertThat((String) invocation.get(FIELD_STATUS), equalTo(STATUS_COMPLETE));
            assertThat(task.isDoUpgradeCalled(), equalTo(true));
        }
    }

    @Test
    public void testADelayedTaskBecomesImmediateWhenImmediateHasHigherBuildNumber() throws IllegalXMLCharactersException
    {
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(1);
        when(mockApplicationProperties.getString(APKeys.JIRA_PATCHED_VERSION)).thenReturn("0");

        ImmutableList<MockUpgradeTask> tasks = getUpgradeTasks(
                UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED,
                UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED);

        upgradeManager = createForTest(tasks);
        upgradeManager.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());

        ArgumentCaptor<Map> capturedArguments = ArgumentCaptor.forClass(Map.class);
        verify(mockDelegator, times(2)).createValue(eq(ENTITY_UPGRADE_HISTORY), (Map<String, Object>) capturedArguments.capture());

        List<Map> invocations = capturedArguments.getAllValues();
        for (int i = 0 ; i < invocations.size(); i++)
        {
            Map<String, Object> invocation = invocations.get(i);
            MockUpgradeTask task = tasks.get(i);
            assertThat((String) invocation.get(FIELD_STATUS), equalTo(STATUS_COMPLETE));
            assertThat(task.isDoUpgradeCalled(), equalTo(true));
        }
    }

    @Test
    public void testNoChangeWhenADelayedTaskHasAGreaterBuildNumber() throws IllegalXMLCharactersException
    {
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(1);
        when(mockApplicationProperties.getString(APKeys.JIRA_PATCHED_VERSION)).thenReturn("0");

        ImmutableList<MockUpgradeTask> tasks = getUpgradeTasks(
                UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED,
                UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED);

        upgradeManager = createForTest(tasks);
        upgradeManager.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());

        ArgumentCaptor<Map> capturedArguments = ArgumentCaptor.forClass(Map.class);
        verify(mockDelegator, times(2)).createValue(eq(ENTITY_UPGRADE_HISTORY), (Map<String, Object>) capturedArguments.capture());

        List<Map> invocations = capturedArguments.getAllValues();

        assertThat((String) invocations.get(0).get(FIELD_STATUS), equalTo(STATUS_COMPLETE));
        assertThat(tasks.get(0).isDoUpgradeCalled(), equalTo(true));

        assertThat((String) invocations.get(1).get(FIELD_STATUS), equalTo(STATUS_PENDING));
        assertThat(tasks.get(1).isDoUpgradeCalled(), equalTo(false));
    }

    @Test
    public void testSomeDelayedTasksBecomeImmediate() throws IllegalXMLCharactersException
    {
        when(mockBuildUtilsInfo.getApplicationBuildNumber()).thenReturn(1);
        when(mockApplicationProperties.getString(APKeys.JIRA_PATCHED_VERSION)).thenReturn("0");

        ImmutableList<MockUpgradeTask> tasks = getUpgradeTasks(
                UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED,
                UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED,
                UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED,
                UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED);

        upgradeManager = createForTest(tasks);
        upgradeManager.doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().build());

        ArgumentCaptor<Map> capturedArguments = ArgumentCaptor.forClass(Map.class);
        verify(mockDelegator, times(4)).createValue(eq(ENTITY_UPGRADE_HISTORY), (Map<String, Object>) capturedArguments.capture());

        List<Map> invocations = capturedArguments.getAllValues();
        //All but the last task should have been made to run immediately
        int lastTaskNumber = invocations.size()-1;
        for (int i = 0 ; i < lastTaskNumber; i++)
        {
            Map<String, Object> invocation = invocations.get(i);
            MockUpgradeTask task = tasks.get(i);
            assertThat((String) invocation.get(FIELD_STATUS), equalTo(STATUS_COMPLETE));
            assertThat(task.isDoUpgradeCalled(), equalTo(true));
        }

        assertThat((String) invocations.get(lastTaskNumber).get(FIELD_STATUS), equalTo(STATUS_PENDING));
        assertThat(tasks.get(lastTaskNumber).isDoUpgradeCalled(), equalTo(false));
    }

    private class PendingTaskMatcher extends ArgumentMatcher<Map<String, Object>>
    {
        @Override
        @SuppressWarnings("unchecked")
        public boolean matches(final Object argument)
        {
            return((Map<String, Object>) argument).get("status").equals("pending");
        }
    }

    private ImmutableList<MockUpgradeTask> getUpgradeTasks(UpgradeTask.ScheduleOption...schedulingOptions)
    {
        long startNumber = 100l;
        ImmutableList.Builder<MockUpgradeTask> builder = ImmutableList.builder();
        for (UpgradeTask.ScheduleOption scheduleOption : schedulingOptions)
        {
            builder.add(new MockUpgradeTask(String.valueOf(startNumber), "Short desc of task " + startNumber, scheduleOption));
            startNumber++;
        }

        return builder.build();
    }

}