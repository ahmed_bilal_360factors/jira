package com.atlassian.jira.issue.attachment.zip;

import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.util.Consumer;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.annotation.Nonnull;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class TestAttachmentZipEntryStreamConsumer
{

    public static final String UTF_8 = "UTF-8";
    private static final int EXISTING_ZIP_ENTRY_INDEX = 1;
    private static final int NOT_EXISTING_ZIP_ENTRY_INDEX = 4;

    private AttachmentZipEntryStreamConsumer zipEntryStreamConsumer;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private Consumer<ZipArchiveEntry> onZipEntryExists;


    @Test
    public void appropriateZipEntryShouldGetTransmittedToOutputStream() throws Exception
    {
        final ByteArrayOutputStream resultOutputStream = new ByteArrayOutputStream();
        zipEntryStreamConsumer = new AttachmentZipEntryStreamConsumer(resultOutputStream, onZipEntryExists, EXISTING_ZIP_ENTRY_INDEX);

        zipEntryStreamConsumer.withInputStream(getInputStreamWithZippedBytes());

        assertThat(resultOutputStream.toString(UTF_8), equalTo("content2"));
    }

    @Test
    public void onZipEntryExistsMethodShouldGetCalledBeforeAnythingGetsWrittenToOutput() throws Exception
    {
        final ByteArrayOutputStream resultOutputStream = new ByteArrayOutputStream();
        final AtomicBoolean methodCalled = new AtomicBoolean(false);
        final Consumer<ZipArchiveEntry> onZipEntryExists = new Consumer<ZipArchiveEntry>()
        {
            @Override
            public void consume(@Nonnull final ZipArchiveEntry element)
            {
                assertThat(resultOutputStream.size(), equalTo(0));
                methodCalled.set(true);
            }
        };
        zipEntryStreamConsumer = new AttachmentZipEntryStreamConsumer(resultOutputStream, onZipEntryExists, EXISTING_ZIP_ENTRY_INDEX);

        zipEntryStreamConsumer.withInputStream(getInputStreamWithZippedBytes());

        assertThat(methodCalled.get(), equalTo(true));
    }

    @Test
    public void shouldGetExceptionWhenEntryWithGivenIndexNotFound() throws Exception
    {
        final ByteArrayOutputStream resultOutputStream = new ByteArrayOutputStream();
        zipEntryStreamConsumer = new AttachmentZipEntryStreamConsumer(resultOutputStream, onZipEntryExists, NOT_EXISTING_ZIP_ENTRY_INDEX);

        expectedException.expect(ZipEntryNotFoundException.class);

        zipEntryStreamConsumer.withInputStream(getInputStreamWithZippedBytes());
    }

    @Test
    public void shouldNotCallOnZipEntryExistsMethodWhenEntryNotFound() throws Exception
    {
        final ByteArrayOutputStream resultOutputStream = new ByteArrayOutputStream();
        zipEntryStreamConsumer = new AttachmentZipEntryStreamConsumer(resultOutputStream, onZipEntryExists, NOT_EXISTING_ZIP_ENTRY_INDEX);

        try
        {
            zipEntryStreamConsumer.withInputStream(getInputStreamWithZippedBytes());
        }
        catch (final ZipEntryNotFoundException e)
        {
            verify(onZipEntryExists, times(0)).consume(any(ZipArchiveEntry.class));
        }
    }

    private ByteArrayInputStream getInputStreamWithZippedBytes() throws IOException
    {
        final byte[] zipBytes = new ZipBytesBuilder()
                .addEntry("content1", "content1_name")
                .addEntry("content2", "content2_name")
                .addEntry("content3", "content3_name")
                .build();
        return new ByteArrayInputStream(zipBytes);
    }

    private static class ZipBytesBuilder
    {
        private final ByteArrayOutputStream zipBytesStream = new ByteArrayOutputStream();
        private final ZipOutputStream zipOutputStream = new ZipOutputStream(zipBytesStream);

        ZipBytesBuilder addEntry(final String content, final String entryName) throws IOException
        {
            zipOutputStream.putNextEntry(new ZipEntry(entryName));
            zipOutputStream.write(content.getBytes(Charset.forName(UTF_8)));
            return this;
        }

        byte[] build() throws IOException
        {
            zipOutputStream.close();
            return zipBytesStream.toByteArray();
        }
    }
}