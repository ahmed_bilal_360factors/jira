package com.atlassian.jira.license;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.extras.api.AtlassianLicense;
import com.atlassian.extras.api.Product;
import com.atlassian.extras.api.jira.JiraLicense;
import com.atlassian.extras.common.LicenseException;
import com.atlassian.jira.auditing.AuditingManager;
import com.atlassian.jira.auditing.RecordRequest;
import com.atlassian.jira.auditing.handlers.SystemAuditEventHandler;
import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.util.ExternalLinkUtil;
import com.atlassian.license.SIDManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Tests {@link JiraLicenseManager}.
 *
 * @since v6.3
 */
@SuppressWarnings ({ "deprecation", "unchecked", "CollectionWithoutInitialCapacity" })
public class TestJiraLicenseManagerImpl
{
    public static final CoreFeatures DARK_FEATURE = CoreFeatures.LICENSE_ROLES_ENABLED;
    private static final String BAD_LICENSE = "I'm a bad license";
    private static final String GOOD_LICENSE = "I'm a good license";
    private static final String OTHER_LICENSE = "I'm an other license";
    private static final String USER_NAME = "userName";
    private static final LicenseRoleId TEST_LICENSE_ROLE_ID = new LicenseRoleId("Role");
    private static final String VALID_LICENSE_NOT_EXPIRED = "Valid License - not expired";
    private static final String EXPIRED_LICENSE_ONE = "Expired License One";
    private static final String EXPIRED_LICESNSE_TWO = "Expired Licesnse Two";

    @Rule
    public MockitoContainer mockitoContainer = MockitoMocksInContainer.rule(this);
    @Mock
    private AtlassianLicense atlassianLicense;
    @Mock
    private JiraLicense jiraLicense;
    @Mock
    private BuildUtilsInfo buildUtilsInfo;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private SIDManager sidManager;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private ExternalLinkUtil externalLinkUtil;
    @Mock
    private DateTimeFormatter dateTimeFormatter;
    @Mock
    private I18nHelper.BeanFactory i18nFactory;
    @Mock
    private MultiLicenseStore multiLicenseStore;
    @Mock
    private LicenseDetailsFactory licenseDetailsFactory;
    @Mock
    private FeatureManager featureManager;
    @Mock
    private LicenseDetails licenseDetails;
    @Mock
    private LicenseDetails differentLicenseDetails;
    @Mock
    private LicenseDetails validLicenseDetails;
    @Mock
    private LicenseDetails expiredLicenseDetails;
    @Mock
    private LicenseRoleDetails licenseRoleDetails;
    @Mock
    private LicenseRoleDetails licenseRoleDetails2;
    @AvailableInContainer
    @Mock
    private AuditingManager auditingManager;
    @AvailableInContainer
    @Mock
    private SystemAuditEventHandler systemAuditEventHandler;

    private JiraLicenseManager jiraLicenseManager;

    @Before
    public void setUp()
    {
        jiraLicenseManager = new JiraLicenseManagerImpl(buildUtilsInfo, sidManager, eventPublisher,
                multiLicenseStore, featureManager, licenseDetailsFactory);
    }

    @Test
    public void testBlankLicenseStringIsInvalid()
    {
        assertFalse(jiraLicenseManager.isDecodeable(""));
    }

    @Test
    public void testGettingAnInvalidStoredLicenseThrowsException()
    {
        when(multiLicenseStore.retrieve()).thenReturn(newArrayList(BAD_LICENSE));
        when(licenseDetailsFactory.getLicense(BAD_LICENSE)).thenThrow(new LicenseException());

        try
        {
            jiraLicenseManager.getLicense();
            fail("Should have barfed");
        }
        catch (LicenseException expected) {}
    }

    @Test
    public void testGettingLicenseReturnsLicense()
    {
        when(multiLicenseStore.retrieve()).thenReturn(newArrayList(GOOD_LICENSE));
        when(licenseDetailsFactory.getLicense(GOOD_LICENSE)).thenReturn(licenseDetails);
        when(atlassianLicense.getProductLicense(Product.JIRA)).thenReturn(jiraLicense);

        final LicenseDetails actualLicenseDetails = jiraLicenseManager.getLicense(GOOD_LICENSE);
        assertNotNull(actualLicenseDetails);
        assertEquals(licenseDetails, actualLicenseDetails);

        verify(licenseDetailsFactory).getLicense(GOOD_LICENSE);
    }

    @Test
    public void testGettingInvalidLicenseStringThrowException()
    {
        when(licenseDetailsFactory.getLicense(BAD_LICENSE)).thenThrow(new LicenseException());

        try
        {
            jiraLicenseManager.getLicense(BAD_LICENSE);
            fail("Should have barfed");
        }
        catch (LicenseException expected) {}
    }

    @Test
    public void testLicenseManagerDelegatesLookup() throws Exception
    {
        jiraLicenseManager.getLicense(GOOD_LICENSE);
        verify(licenseDetailsFactory).getLicense(GOOD_LICENSE);
        verifyNoMoreInteractions(atlassianLicense, jiraLicense, buildUtilsInfo, sidManager,
                eventPublisher, multiLicenseStore, featureManager, licenseDetailsFactory, licenseDetails);
    }

    @Test
    public void testGettingProvidedLicenseReturnsLicense() throws Exception
    {
        when(licenseDetailsFactory.getLicense(GOOD_LICENSE)).thenReturn(licenseDetails);
        when(atlassianLicense.getProductLicense(Product.JIRA)).thenReturn(jiraLicense);

        LicenseDetails actualLicenseDetails = jiraLicenseManager.getLicense(GOOD_LICENSE);

        assertNotNull(actualLicenseDetails);
        assertEquals(licenseDetails, actualLicenseDetails);
    }

    @Test
    public void testSettingLicenseWhichNeedsConfirmationReset_63()
    {
        when(licenseDetailsFactory.isDecodeable(anyString())).thenReturn(true);
        assertLicenseCanBeSetAndTestConfirmationPaths63(true, true);
    }

    @Test
    public void testSetLicense_HappyAndNeedsConfirmationReset_70() throws Exception
    {
        when(featureManager.isEnabled(DARK_FEATURE)).thenReturn(true);
        when(licenseDetailsFactory.isDecodeable(anyString())).thenReturn(true);
        assertLicenseCanBeSetAndTestConfirmationPaths70(true, true);
    }

    @Test
    public void testSetLicense_InvalidLicenseInput()
    {
        when(licenseDetailsFactory.getLicense(BAD_LICENSE)).thenThrow(new LicenseException());

        try
        {
            jiraLicenseManager.setLicense(BAD_LICENSE);
            fail("Should have barfed");
        }
        catch (IllegalArgumentException expected)
        {
        }
    }

    @Test
    public void testSetLicense_InvalidLicenseInput70()
    {
        when(featureManager.isEnabled(DARK_FEATURE)).thenReturn(true);
        when(licenseDetailsFactory.getLicense(BAD_LICENSE)).thenThrow(new LicenseException());

        try
        {
            jiraLicenseManager.setLicense(BAD_LICENSE);
            fail("Should have barfed");
        }
        catch (IllegalArgumentException expected)
        {
        }
    }

    @Test
    public void testSetLicense_HappyMayNeedNeedsConfirmationReset() throws Exception
    {
        when(licenseDetailsFactory.isDecodeable(anyString())).thenReturn(true);
        assertLicenseCanBeSetAndTestConfirmationPaths63(true, false);
    }


    @Test
    public void testSetLicense_HappyMayNeedNeedsConfirmationReset_70() throws Exception
    {
        when(featureManager.isEnabled(DARK_FEATURE)).thenReturn(true);
        when(licenseDetailsFactory.isDecodeable(anyString())).thenReturn(true);
        assertLicenseCanBeSetAndTestConfirmationPaths70(true, false);
    }

    @Test
    public void testSettingLicenseWhichMayNeedConfirmationReset2()
    {
        when(licenseDetailsFactory.isDecodeable(anyString())).thenReturn(true);
        assertLicenseCanBeSetAndTestConfirmationPaths63(false, true);
    }

    @Test
    public void testConfirmProceedUnderEvaluationTermsWithThreeLicensesSecondDidNotEpireMaintenance()
    {
        ImmutableList<String> licenses = ImmutableList.of(EXPIRED_LICENSE_ONE, VALID_LICENSE_NOT_EXPIRED, EXPIRED_LICESNSE_TWO);
        when(licenseDetailsFactory.getLicense(EXPIRED_LICENSE_ONE)).thenReturn(licenseDetails);
        when(licenseDetailsFactory.getLicense(VALID_LICENSE_NOT_EXPIRED)).thenReturn(validLicenseDetails);
        when(licenseDetailsFactory.getLicense(EXPIRED_LICESNSE_TWO)).thenReturn(expiredLicenseDetails);
        when(licenseDetails.isMaintenanceValidForBuildDate(any(Date.class))).thenReturn(true);
        when(differentLicenseDetails.isMaintenanceValidForBuildDate(any(Date.class))).thenReturn(false);
        when(expiredLicenseDetails.isMaintenanceValidForBuildDate(any(Date.class))).thenReturn(true);
        final ImmutableList<LicenseDetails> listedLicenseDetails = ImmutableList.of(licenseDetails, expiredLicenseDetails);
        when(multiLicenseStore.retrieve()).thenReturn(licenses);

        jiraLicenseManager.confirmProceedUnderEvaluationTerms(USER_NAME);

        BaseMatcher<ConfirmEvaluationLicenseEvent> confirmationEvent = new BaseMatcher<ConfirmEvaluationLicenseEvent>()
        {
            private boolean isUserNameGood = true;
            private boolean isLicenseGood = false;
            private boolean isEventObjectGood = true;

            @Override
            public boolean matches(final Object o)
            {
                if (!(o instanceof ConfirmEvaluationLicenseEvent))
                {
                    return false;
                }
                isEventObjectGood = true;
                final ConfirmEvaluationLicenseEvent matchEvent = (ConfirmEvaluationLicenseEvent) o;
                boolean eventsAreMatching = USER_NAME.equals(matchEvent.getUserWhoConfirmed());
                isUserNameGood = eventsAreMatching;
                for (LicenseDetails licenseDetailsInEvent : matchEvent.getExpiredLicenses())
                {
                    eventsAreMatching = eventsAreMatching && (licenseDetails == licenseDetailsInEvent || expiredLicenseDetails == licenseDetailsInEvent);
                }
                return eventsAreMatching;
            }

            @Override
            public void describeTo(final Description description)
            {
                if (!isEventObjectGood)
                { description.appendText("CHECK: ConfirmEvaluationLicenseEvent object Type"); }
                if (!isUserNameGood) { description.appendText("CHECK: User Name "); }
                if (!isLicenseGood) { description.appendText("CHECK: Wrong License"); }
            }
        };

        verify(eventPublisher).publish(argThat(confirmationEvent));
    }

    @Test
    public void testSetLicenseHappyMayNeedNeedsConfirmationReset2and70() throws Exception
    {
        ImmutableList<String> licenses = ImmutableList.of();
        when(multiLicenseStore.retrieve()).thenReturn(licenses);

        jiraLicenseManager.confirmProceedUnderEvaluationTerms(USER_NAME);
        when(featureManager.isEnabled(DARK_FEATURE)).thenReturn(true);
        when(licenseDetailsFactory.isDecodeable(anyString())).thenReturn(true);
        assertLicenseCanBeSetAndTestConfirmationPaths70(false, true);
    }

    @Test
    public void testConfirmProceed()
    {
        ImmutableList<String> licenses = ImmutableList.of();
        when(multiLicenseStore.retrieve()).thenReturn(licenses);

        jiraLicenseManager.confirmProceedUnderEvaluationTerms(USER_NAME);
        verify(multiLicenseStore).confirmProceedUnderEvaluationTerms(USER_NAME);
    }

    private void assertLicenseCanBeSetAndTestConfirmationPaths63(final boolean isMaintenanceValidForBuildDate, final boolean isUsingOldLicenseToRunNewJiraBuild)
    {
        when(licenseDetailsFactory.getLicense(GOOD_LICENSE)).thenReturn(licenseDetails);
        when(atlassianLicense.getProductLicense(Product.JIRA)).thenReturn(jiraLicense);

        final Date date = new Date();

        when(buildUtilsInfo.getCurrentBuildDate()).thenReturn(date);
        if (isMaintenanceValidForBuildDate)
        {
            when(licenseDetails.isMaintenanceValidForBuildDate(date)).thenReturn(true);
            when(licenseDetails.hasLicenseTooOldForBuildConfirmationBeenDone()).thenReturn(true);
        }


        final LicenseDetails actualLicenseDetails = jiraLicenseManager.setLicense(GOOD_LICENSE);


        if (isMaintenanceValidForBuildDate && isUsingOldLicenseToRunNewJiraBuild)
        {
            verify(multiLicenseStore).resetOldBuildConfirmation();
        }
        assertNotNull(actualLicenseDetails);
        assertEquals(licenseDetails, actualLicenseDetails);

        verify(licenseDetailsFactory).getLicense(GOOD_LICENSE);
        verify(eventPublisher).publish(any());
        verify(multiLicenseStore).store(eq(newArrayList(GOOD_LICENSE)));
    }

    private void assertLicenseCanBeSetAndTestConfirmationPaths70(final boolean isMaintenanceValidForBuildDate, final boolean isUsingOldLicenseToRunNewJiraBuild)
    {
        when(licenseDetailsFactory.getLicense(GOOD_LICENSE)).thenReturn(licenseDetails);
        when(atlassianLicense.getProductLicense(Product.JIRA)).thenReturn(jiraLicense);
        when(featureManager.isEnabled(DARK_FEATURE)).thenReturn(true);
        when(licenseDetails.getLicenseRoles()).thenReturn(licenseRoleDetails);
        when(multiLicenseStore.retrieve()).thenReturn(new ArrayList<String>());

        final Date date = new Date();

        when(buildUtilsInfo.getCurrentBuildDate()).thenReturn(date);
        if (isMaintenanceValidForBuildDate)
        {
            when(licenseDetails.isMaintenanceValidForBuildDate(date)).thenReturn(true);
            when(licenseDetails.hasLicenseTooOldForBuildConfirmationBeenDone()).thenReturn(true);
        }

        final LicenseDetails actualLicenseDetails = jiraLicenseManager.setLicense(GOOD_LICENSE);

        assertNotNull(actualLicenseDetails);
        assertEquals(licenseDetails, actualLicenseDetails);
        verify(licenseDetailsFactory, atLeast(2)).getLicense(GOOD_LICENSE);
        verify(eventPublisher, atLeast(1)).publish(any());
        verify(multiLicenseStore).store(eq(newArrayList(GOOD_LICENSE)));

        if (isMaintenanceValidForBuildDate && isUsingOldLicenseToRunNewJiraBuild)
        {
            verify(multiLicenseStore).resetOldBuildConfirmation();
        }

    }

    @Test
    public void testGetServerIdWithExistingServerId()
    {
        final String expectedServerId = "A server ID";

        when(multiLicenseStore.retrieveServerId()).thenReturn(expectedServerId);

        final String actualSID = jiraLicenseManager.getServerId();
        assertEquals(expectedServerId, actualSID);
        verify(multiLicenseStore, never()).storeServerId(expectedServerId);
    }

    @Test
    public void testGetServerIdWithNonExistingServerId()
    {
        final String expectedServerId = "A server ID";
        when(multiLicenseStore.retrieveServerId()).thenReturn(null);
        when(sidManager.generateSID()).thenReturn(expectedServerId);

        final String actual = jiraLicenseManager.getServerId();

        verify(multiLicenseStore).storeServerId(expectedServerId);
        assertEquals(expectedServerId, actual);
    }

    @Test
    public void identicalLicenseShouldNotBeStoredTwice()
    {
        when(licenseDetailsFactory.isDecodeable("example")).thenReturn(true);
        when(featureManager.isEnabled(CoreFeatures.LICENSE_ROLES_ENABLED)).thenReturn(true);
        when(licenseDetailsFactory.getLicense("example")).thenReturn(licenseDetails);
        when(licenseDetails.getLicenseRoles()).thenReturn(licenseRoleDetails);

        LicenseRoleId licenseRoleId = new LicenseRoleId("role-1");
        Set<LicenseRoleId> licenseRoleIds = new HashSet<LicenseRoleId>();
        licenseRoleIds.add(licenseRoleId);
        when(licenseRoleDetails.getIds()).thenReturn(licenseRoleIds);
        when(multiLicenseStore.retrieve()).thenReturn(ImmutableList.of("example"));

        LicenseDetails details = jiraLicenseManager.setLicenseNoEvent("example");

        assertEquals(details, licenseDetails);
        verify(multiLicenseStore).store(newArrayList("example"));
    }

    @Test
    public void existingDifferentLicenseShouldNotBeOverridden()
    {
        when(licenseDetailsFactory.isDecodeable("example")).thenReturn(true);
        when(featureManager.isEnabled(CoreFeatures.LICENSE_ROLES_ENABLED)).thenReturn(true);
        when(licenseDetailsFactory.getLicense("example")).thenReturn(licenseDetails);
        when(licenseDetails.getLicenseRoles()).thenReturn(licenseRoleDetails);
        when(licenseDetails.getLicenseString()).thenReturn("example");
        when(licenseRoleDetails.getIds()).thenReturn(newHashSet(new LicenseRoleId("role-1")));
        when(licenseDetailsFactory.getLicense("different")).thenReturn(differentLicenseDetails);
        when(differentLicenseDetails.getLicenseRoles()).thenReturn(licenseRoleDetails2);
        when(differentLicenseDetails.getLicenseString()).thenReturn("different");
        when(licenseRoleDetails2.getIds()).thenReturn(newHashSet(new LicenseRoleId("role-2")));
        when(multiLicenseStore.retrieve()).thenReturn(ImmutableList.of("different"));

        LicenseDetails details = jiraLicenseManager.setLicenseNoEvent("example");

        assertEquals(details, licenseDetails);
        verify(multiLicenseStore).store(newArrayList("example", "different"));
    }

    @Test
    public void existingDifferentLicenseWithSameRoleShouldBeReplaced()
    {
        when(licenseDetailsFactory.isDecodeable("example")).thenReturn(true);
        when(featureManager.isEnabled(CoreFeatures.LICENSE_ROLES_ENABLED)).thenReturn(true);

        when(licenseDetailsFactory.getLicense("example")).thenReturn(licenseDetails);
        when(licenseDetails.getLicenseRoles()).thenReturn(licenseRoleDetails);
        when(licenseRoleDetails.getIds()).thenReturn(newHashSet(new LicenseRoleId("role-1")));

        when(licenseDetailsFactory.getLicense("different")).thenReturn(differentLicenseDetails);
        when(differentLicenseDetails.getLicenseRoles()).thenReturn(licenseRoleDetails2);
        when(licenseRoleDetails2.getIds()).thenReturn(newHashSet(new LicenseRoleId("role-1")));


        when(multiLicenseStore.retrieve()).thenReturn(ImmutableList.of("different"));

        LicenseDetails details = jiraLicenseManager.setLicenseNoEvent("example");

        assertEquals(details, licenseDetails);
        verify(multiLicenseStore).store(newArrayList("example"));
    }

    @Test
    public void newLicenseEventIsPublishedAndAuditWhenLicenseIsAddedOrEdited()
    {
        when(licenseDetailsFactory.isDecodeable("example")).thenReturn(true);
        when(featureManager.isEnabled(CoreFeatures.LICENSE_ROLES_ENABLED)).thenReturn(true);
        when(licenseDetailsFactory.getLicense("example")).thenReturn(licenseDetails);
        when(licenseDetails.getLicenseRoles()).thenReturn(licenseRoleDetails);

        LicenseRoleId licenseRoleId = new LicenseRoleId("role-1");
        Set<LicenseRoleId> licenseRoleIds = new HashSet<LicenseRoleId>();
        licenseRoleIds.add(licenseRoleId);
        when(licenseRoleDetails.getIds()).thenReturn(licenseRoleIds);
        when(multiLicenseStore.retrieve()).thenReturn(ImmutableList.of("example"));

        jiraLicenseManager.setLicense("example");

        verify(eventPublisher, times(1)).publish(any());
        verify(auditingManager, times(0)).store(Matchers.<RecordRequest>any());
    }

    @Test
    public void newLicenseEventIsNotPublishedButAuditedWhenPICOIsNotInitialized()
    {
        when(licenseDetailsFactory.isDecodeable("example")).thenReturn(true);
        when(featureManager.isEnabled(CoreFeatures.LICENSE_ROLES_ENABLED)).thenReturn(true);
        when(licenseDetailsFactory.getLicense("example")).thenReturn(licenseDetails);
        when(licenseDetails.getLicenseRoles()).thenReturn(licenseRoleDetails);

        LicenseRoleId licenseRoleId = new LicenseRoleId("role-1");
        Set<LicenseRoleId> licenseRoleIds = new HashSet<LicenseRoleId>();
        licenseRoleIds.add(licenseRoleId);
        when(licenseRoleDetails.getIds()).thenReturn(licenseRoleIds);
        when(multiLicenseStore.retrieve()).thenReturn(ImmutableList.of("example"));

        jiraLicenseManager.setLicenseNoEvent("example");

        verify(eventPublisher, times(0)).publish(any());
        verify(auditingManager, times(1)).store(Matchers.<RecordRequest>any());
    }

    @Test (expected = IllegalArgumentException.class)
    public void clearAndSetShouldRejectBrokenLicenss()
    {
        String notEncoded = "lmnop";
        when(licenseDetailsFactory.isDecodeable(notEncoded)).thenReturn(false);

        jiraLicenseManager.clearAndSetLicense(notEncoded);

        verify(eventPublisher, never()).publish(Mockito.isA(NewLicenseEvent.class));
    }

    @Test
    public void clearAndSetShouldStoreNewLicense()
    {
        String expectedToStore = "lmnop";
        when(licenseDetailsFactory.isDecodeable(expectedToStore)).thenReturn(true);
        jiraLicenseManager.clearAndSetLicense("lmnop");

        ArgumentCaptor<Iterable> iterableArgumentCaptor = ArgumentCaptor.forClass(Iterable.class);

        verify(multiLicenseStore).store(iterableArgumentCaptor.capture());
        Iterable value = iterableArgumentCaptor.getValue();
        assertEquals(1, Iterables.size(value));
        assertTrue(value.iterator().next().equals(expectedToStore));

        verify(eventPublisher).publish(Mockito.isA(NewLicenseEvent.class));
    }

    @Test
    public void expiredLicenseShouldAllowRoleLicensing()
    {
        when(multiLicenseStore.retrieve()).thenReturn(newArrayList(GOOD_LICENSE));
        when(licenseDetailsFactory.getLicense(GOOD_LICENSE)).thenReturn(licenseDetails);
        when(licenseDetails.getLicenseRoles()).thenReturn(licenseRoleDetails);
        when(licenseRoleDetails.getIds()).thenReturn(ImmutableSet.of(TEST_LICENSE_ROLE_ID));
        when(licenseDetails.isExpired()).thenReturn(true);

        boolean licensed = jiraLicenseManager.isLicensed(TEST_LICENSE_ROLE_ID);

        assertTrue(licensed);
    }

    @Test
    public void goodLicenseShouldAllowRoleLicensing()
    {
        when(multiLicenseStore.retrieve()).thenReturn(newArrayList(GOOD_LICENSE));
        when(licenseDetailsFactory.getLicense(GOOD_LICENSE)).thenReturn(licenseDetails);
        when(licenseDetails.getLicenseRoles()).thenReturn(licenseRoleDetails);
        when(licenseRoleDetails.getIds()).thenReturn(ImmutableSet.of(TEST_LICENSE_ROLE_ID));
        when(licenseDetails.isExpired()).thenReturn(false);

        boolean licensed = jiraLicenseManager.isLicensed(TEST_LICENSE_ROLE_ID);

        assertTrue(licensed);
    }

    @Test
    public void newLicenseEventIsNotAuditedWhenLicenseIsAddedOrEditedForOnDemandInstance()
    {
        when(featureManager.isOnDemand()).thenReturn(true);
        when(licenseDetailsFactory.isDecodeable("example")).thenReturn(true);
        when(featureManager.isEnabled(CoreFeatures.LICENSE_ROLES_ENABLED)).thenReturn(true);
        when(licenseDetailsFactory.getLicense("example")).thenReturn(licenseDetails);
        when(licenseDetails.getLicenseRoles()).thenReturn(licenseRoleDetails);

        LicenseRoleId licenseRoleId = new LicenseRoleId("role-1");
        Set<LicenseRoleId> licenseRoleIds = new HashSet<LicenseRoleId>();
        licenseRoleIds.add(licenseRoleId);
        when(licenseRoleDetails.getIds()).thenReturn(licenseRoleIds);
        when(multiLicenseStore.retrieve()).thenReturn(ImmutableList.of("example"));

        jiraLicenseManager.setLicenseNoEvent("example");

        verify(auditingManager, never()).store(Matchers.<RecordRequest>any());
    }
}
