package com.atlassian.jira.issue.fields.rest.json;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.TimeZone;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.UserJsonBean;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.util.EmailFormatter;

import com.google.common.collect.Lists;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;

public class DefaultUserBeanFactoryTest
{
    @Rule
    public RuleChain chain = MockitoMocksInContainer.forTest(this);
    private DefaultUserBeanFactory userBeanFactory;
    private MockApplicationUser loggedInUser;
    @Mock
    private JiraBaseUrls jiraBaseUrl;
    @Mock
    private TimeZoneManager timeZoneManager;
    @Mock
    private EmailFormatter emailFormatter;
    @Mock
    @AvailableInContainer
    private AvatarService avatarService;
    @AvailableInContainer
    private MockApplicationProperties applicationProperties = new MockApplicationProperties();

    @Before
    public void setup() throws URISyntaxException
    {
        applicationProperties.setEncoding("UTF-8");
        when(avatarService.getAvatarAbsoluteURL(isA(ApplicationUser.class), isA(ApplicationUser.class), isA(Avatar.Size.class))).thenReturn(new URI("http://www.example.com/avatar"));

        loggedInUser = new MockApplicationUser("ID2135", "loggedInUser", "Logged User", "loggedIndUser@atlassian.com");
        userBeanFactory = new DefaultUserBeanFactory(jiraBaseUrl, emailFormatter, timeZoneManager);
        when(jiraBaseUrl.restApi2BaseUrl()).thenReturn("rest/api/");
    }

    @Test
    public void testCreateBeanForUser() throws Exception
    {
        final String username = "testUser";
        final String displayName = "Test User";
        final String email = "user@atlassian.com";
        final String timeZone = "Europe/Berlin";
        final MockUser user = new MockUser(username, displayName, email);

        when(emailFormatter.formatEmail(eq(email), eq(loggedInUser.getDirectoryUser()))).thenReturn(email);
        when(timeZoneManager.getTimeZoneforUser(user)).thenReturn(TimeZone.getTimeZone(timeZone));

        final UserJsonBean userJsonBean = userBeanFactory.createBean(user, loggedInUser);
        assertThat(userJsonBean, userJsonBeanMatcher(username, displayName, email, timeZone, ApplicationUsers.getKeyFor(user)));
    }

    @Test
    public void testCreateBeanForApplicationUser() throws Exception
    {
        final String username = "appuser";
        final String displayName = "Application User";
        final String email = "aplicationUser@atlassian.com";
        final String timeZone = "Antarctica/McMurdo";
        final String userKey = "ID2134";
        final MockApplicationUser applicationUser = new MockApplicationUser(userKey, username, displayName, email);

        when(emailFormatter.formatEmail(eq(email), eq(loggedInUser.getDirectoryUser()))).thenReturn(email);
        when(timeZoneManager.getTimeZoneforUser(eq(applicationUser.getDirectoryUser()))).thenReturn(TimeZone.getTimeZone(timeZone));

        final UserJsonBean userJsonBean = userBeanFactory.createBean(applicationUser, loggedInUser);
        assertThat(userJsonBean, userJsonBeanMatcher(username, displayName, email, timeZone, userKey));
    }

    @Test
    public void testCreateBeanWhenTimeZoneIsNotSet() throws Exception
    {
        final MockApplicationUser applicationUser = new MockApplicationUser("ID2134", "appuser", "Application User", "aplicationUser@atlassian.com");
        when(timeZoneManager.getTimeZoneforUser(isA(User.class))).thenReturn(null);

        final UserJsonBean userJsonBean = userBeanFactory.createBean(applicationUser, loggedInUser);
        assertThat(userJsonBean.getTimeZone(), Matchers.nullValue());
    }

    @Test
    public void testCreateBeanCollection() throws Exception
    {
        final String username1 = "appuser";
        final String displayName1 = "Application User";
        final String email1 = "aplicationUser@atlassian.com";
        final String timeZone1 = "Antarctica/McMurdo";
        final String userKey1 = "ID213";
        final MockApplicationUser applicationUser1 = new MockApplicationUser(userKey1, username1, displayName1, email1);

        when(emailFormatter.formatEmail(eq(email1), eq(loggedInUser.getDirectoryUser()))).thenReturn(email1);
        when(timeZoneManager.getTimeZoneforUser(eq(applicationUser1.getDirectoryUser()))).thenReturn(TimeZone.getTimeZone(timeZone1));

        final String username2 = "appuser";
        final String displayName2 = "Application User";
        final String email2 = "aplicationUser@atlassian.com";
        final String timeZone2 = "Antarctica/McMurdo";
        final String userKey2 = "ID2134";
        final MockApplicationUser applicationUser2 = new MockApplicationUser(userKey2, username2, displayName2, email2);

        when(emailFormatter.formatEmail(eq(email2), eq(loggedInUser.getDirectoryUser()))).thenReturn(email2);
        when(timeZoneManager.getTimeZoneforUser(eq(applicationUser2.getDirectoryUser()))).thenReturn(TimeZone.getTimeZone(timeZone2));

        final Collection<ApplicationUser> userCollection = Lists.<ApplicationUser>newArrayList(applicationUser1, applicationUser2);
        final Collection<UserJsonBean> beanCollection = userBeanFactory.createBeanCollection(userCollection, loggedInUser, jiraBaseUrl, emailFormatter, timeZoneManager);

        assertThat(beanCollection, Matchers.<UserJsonBean>hasItems(
                userJsonBeanMatcher(username1, displayName1, email1, timeZone1, userKey1),
                userJsonBeanMatcher(username2, displayName2, email2, timeZone2, userKey2)
        ));
    }

    private Matcher<UserJsonBean> userJsonBeanMatcher(final String user, final String userName, final String email, final String timeZone, final String userKey)
    {
        return Matchers.allOf(
                Matchers.<UserJsonBean>hasProperty("self", Matchers.equalTo("rest/api/user?username=" + user)),
                Matchers.hasProperty("name", Matchers.equalTo(user)),
                Matchers.hasProperty("displayName", Matchers.equalTo(userName)),
                Matchers.hasProperty("emailAddress", Matchers.equalTo(email)),
                Matchers.hasProperty("active", Matchers.equalTo(true)),
                Matchers.hasProperty("timeZone", Matchers.equalTo(timeZone)),
                Matchers.hasProperty("key", Matchers.equalTo(userKey))
        );
    }
}