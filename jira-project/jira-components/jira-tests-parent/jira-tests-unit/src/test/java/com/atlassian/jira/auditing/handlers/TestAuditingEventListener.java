package com.atlassian.jira.auditing.handlers;

import com.atlassian.jira.auditing.AuditingEventListener;
import com.atlassian.jira.auditing.AuditingManager;
import com.atlassian.jira.auditing.RecordRequest;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests {@link AuditingEventListener}
 *
 * @since v6.3
 */
public class TestAuditingEventListener
{
    @Rule
    public MockitoContainer mockitoContainer = MockitoMocksInContainer.rule(this);
    @Mock
    private AuditingManager auditingManager;
    @Mock
    private PermissionChangeHandler permissionChangeHandler;
    @Mock
    private GroupEventHandler groupEventHandler;
    @Mock
    private SchemeEventHandler schemeEventHandler;
    @Mock
    private UserEventHandler userEventHandler;
    @Mock
    private WorkflowEventHandler workflowEventHandler;
    @Mock
    private NotificationChangeHandler notificationChangeHandler;
    @Mock
    private FieldLayoutSchemeChangeHandler fieldLayoutSchemeChangeHandler;
    @Mock
    private ProjectEventHandler projectEventHandler;
    @Mock
    private ProjectComponentEventHandler projectComponentEventHandler;
    @Mock
    private VersionEventHandler versionEventHandler;
    @Mock
    private SystemAuditEventHandler systemAuditEventHandler;
    @Mock
    private FeatureManager featureManager;

    private AuditingEventListener auditingEventListener;

    @Before
    public void setUp()
    {
        auditingEventListener = new AuditingEventListener(auditingManager, permissionChangeHandler, groupEventHandler, schemeEventHandler, userEventHandler, workflowEventHandler, notificationChangeHandler, fieldLayoutSchemeChangeHandler, projectEventHandler, projectComponentEventHandler, versionEventHandler, systemAuditEventHandler, featureManager);
    }

    @Test
    public void shouldNotAuditOnNewLicenseAdded()
    {
        when(featureManager.isOnDemand()).thenReturn(true);
        auditingEventListener.onNewLicenseAdded(null);
        verify(auditingManager, never()).store((RecordRequest) anyObject());
    }

    @Test
    public void shouldNotAuditOnExtendTrialLicense()
    {
        when(featureManager.isOnDemand()).thenReturn(true);
        auditingEventListener.onExtendTrialLicense(null);
        verify(auditingManager, never()).store((RecordRequest) anyObject());

    }
}
