package com.atlassian.jira.issue.attachment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.store.BackwardCompatibleStoreAdapter;
import com.atlassian.jira.issue.attachment.store.MockAttachmentKeys;
import com.atlassian.jira.issue.attachment.store.provider.CachedBackwardCompatibleStoreAdapterProvider;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.util.AttachmentException;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestBackwardCompatibleAttachmentStore
{
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private AttachmentDirectoryAccessor directoryAccessor;

    @Mock
    private ThumbnailAccessor thumbnailAccessor;

    @Mock
    private StoreAttachmentBeanMapper storeAttachmentBeanMapper;

    @Mock
    private CachedBackwardCompatibleStoreAdapterProvider backwardCompatibleStoreAdapterProvider;

    @Mock
    private BackwardCompatibleStoreAdapter backwardCompatibleStoreAdapter;

    @Mock
    private AttachmentKeyMapper attachmentKeyMapper;

    @Mock
    private ProjectManager projectManager;

    @InjectMocks
    private BackwardCompatibleAttachmentStore store;

    @Before
    public void setUp()
    {
        when(backwardCompatibleStoreAdapterProvider.provide())
                .thenReturn(backwardCompatibleStoreAdapter);
    }
    @Test
    public void testPutFileToStore() throws Exception
    {
        final Attachment attachment = mock(Attachment.class);
        final File sourceFile = mock(File.class);
        final InputStream streamToFile = mock(InputStream.class);

        final StoreAttachmentBean storeAttachmentBean = new StoreAttachmentBean.Builder(streamToFile)
                .withId(1l)
                .build();

        when(storeAttachmentBeanMapper.mapToBean(attachment, sourceFile))
                .thenReturn(Either.<FileNotFoundException, StoreAttachmentBean>right(storeAttachmentBean));

        when(backwardCompatibleStoreAdapter.putAttachment(storeAttachmentBean))
                .thenReturn(Promises.promise(StoreAttachmentResult.created()));

        final Promise<Attachment> attachmentPromise = store.putAttachment(attachment, sourceFile);

        assertThat(attachmentPromise.claim(), equalTo(attachment));
    }

    @Test
    public void testPutInputStreamToStore() throws Exception
    {
        final Attachment attachment = mock(Attachment.class);
        final InputStream source = mock(InputStream.class);

        final StoreAttachmentBean storeAttachmentBean = new StoreAttachmentBean.Builder(source)
                .withId(1l)
                .build();

        when(storeAttachmentBeanMapper.mapToBean(attachment, source))
                .thenReturn(storeAttachmentBean);

        when(backwardCompatibleStoreAdapter.putAttachment(storeAttachmentBean))
                .thenReturn(Promises.promise(StoreAttachmentResult.created()));

        final Promise<Attachment> attachmentPromise = store.putAttachment(attachment, source);

        assertThat(attachmentPromise.claim(), equalTo(attachment));
    }

    @Test
    public void testIsHealthy()
    {
        final ErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("TEST");

        final Option<ErrorCollection> expectedErrorsOption = Option.some(errors);
        when(backwardCompatibleStoreAdapter.errors())
                .thenReturn(expectedErrorsOption);

        final Option<ErrorCollection> result = store.errors();

        assertThat(result, equalTo(expectedErrorsOption));
    }

    @Test
    public void testDeleteAttachmentDeletesFromBothStores()
    {
        final Attachment attachment = mock(Attachment.class);

        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();
        when(attachmentKeyMapper.fromAttachment(attachment))
                .thenReturn(attachmentKey);

        when(backwardCompatibleStoreAdapter.deleteAttachment(attachmentKey))
                .thenReturn(Promises.promise(Unit.VALUE));

        store.deleteAttachment(attachment);

        verify(backwardCompatibleStoreAdapter).deleteAttachment(attachmentKey);
    }

    @Test
    public void testGetThumbnailFile() throws Exception
    {
        final Issue issue = mock(Issue.class);
        final Attachment attachment = mock(Attachment.class);
        final File mockFile = mock(File.class);

        when(thumbnailAccessor.getThumbnailFile(issue, attachment))
                .thenReturn(mockFile);

        final File thumbnailFile = store.getThumbnailFile(issue, attachment);

        assertThat(thumbnailFile, equalTo(mockFile));
    }

    @Test
    public void testGetThumbnailFileByAttachment() throws Exception
    {
        final Attachment attachment = mock(Attachment.class);
        final File mockFile = mock(File.class);

        when(thumbnailAccessor.getThumbnailFile(attachment))
                .thenReturn(mockFile);

        final File thumbnailFile = store.getThumbnailFile(attachment);

        assertThat(thumbnailFile, equalTo(mockFile));
    }

    @Test
    public void testGetLegacyThumbnailFile() throws Exception
    {
        final Attachment attachment = mock(Attachment.class);
        final File mockFile = mock(File.class);

        when(thumbnailAccessor.getLegacyThumbnailFile(attachment))
                .thenReturn(mockFile);

        final File thumbnailFile = store.getLegacyThumbnailFile(attachment);

        assertThat(thumbnailFile, equalTo(mockFile));
    }

    @Test
    public void testGetThumbnailDirectory() throws Exception
    {
        final Issue issue = mock(Issue.class);
        final File mockFile = mock(File.class);

        when(directoryAccessor.getThumbnailDirectory(issue))
                .thenReturn(mockFile);

        final File result = store.getThumbnailDirectory(issue);

        assertThat(result, equalTo(mockFile));
    }

    @Test
    public void testGetAttachmentDirectoryByIssueKey() throws Exception
    {
        final String issueKey = "FOO-1";
        final File mockFile = mock(File.class);

        when(directoryAccessor.getAttachmentDirectory(issueKey))
                .thenReturn(mockFile);

        final File result = store.getAttachmentDirectory(issueKey);

        assertThat(result, equalTo(mockFile));
    }

    @Test
    public void testGetAttachmentDirectoryByIssue() throws Exception
    {
        final Issue issue = mock(Issue.class);
        final File mockFile = mock(File.class);

        when(directoryAccessor.getAttachmentDirectory(issue))
                .thenReturn(mockFile);

        final File result = store.getAttachmentDirectory(issue);

        assertThat(result, equalTo(mockFile));
    }

    @Test
    public void testGetAttachmentDirectoryWithCreateFlag() throws Exception
    {
        final Issue issue = mock(Issue.class);
        final boolean create = false;
        final File mockFile = mock(File.class);

        when(directoryAccessor.getAttachmentDirectory(issue, create))
                .thenReturn(mockFile);

        final File result = store.getAttachmentDirectory(issue, create);

        assertThat(result, equalTo(mockFile));
    }

    @Test
    public void testGetAttachmentDirectoryByIssueKeyAndProjectKey() throws Exception
    {
        final String issueKey = "FOO-1";
        final String projectKey = "FOO";
        final String attachmentDirectory = "attachmentDirectory";
        final File mockFile = mock(File.class);

        when(directoryAccessor.getAttachmentDirectory(attachmentDirectory, projectKey, issueKey))
                .thenReturn(mockFile);

        final File result = store.getAttachmentDirectory(attachmentDirectory, projectKey, issueKey);

        assertThat(result, equalTo(mockFile));
    }

    @Test
    public void testCheckValidAttachmentDirectory() throws Exception
    {
        final Issue issue = mock(Issue.class);
        store.checkValidAttachmentDirectory(issue);
    }

    @Test
    public void testCheckInvalidAttachmentDirectory() throws Exception
    {
        final Issue issue = mock(Issue.class);

        expectedException.expect(AttachmentException.class);

        doThrow(new AttachmentException("message"))
                .when(directoryAccessor).checkValidAttachmentDirectory(issue);

        store.checkValidAttachmentDirectory(issue);
    }

    @Test
    public void testCheckValidTemporaryAttachmentDirectory() throws Exception
    {
        store.checkValidTemporaryAttachmentDirectory();
    }

    @Test
    public void testCheckInvalidTemporaryAttachmentDirectory() throws Exception
    {
        expectedException.expect(AttachmentException.class);

        doThrow(new AttachmentException("message"))
                .when(directoryAccessor).checkValidTemporaryAttachmentDirectory();

        store.checkValidTemporaryAttachmentDirectory();
    }

    @Test
    public void testGetAttachmentFile() throws Exception
    {
        final Issue issue = mock(Issue.class);
        final Attachment attachment = mock(Attachment.class);
        final File mockFile = mock(File.class);
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();

        when(attachmentKeyMapper.fromAttachment(attachment))
                .thenReturn(attachmentKey);

        when(backwardCompatibleStoreAdapter.getAttachmentFile(attachmentKey))
                .thenReturn(mockFile);

        final File attachmentFile = store.getAttachmentFile(issue, attachment);

        assertThat(attachmentFile, equalTo(mockFile));
    }

    @Test
    public void testGetAttachmentWithStreamProcessor() throws Exception
    {
        final Attachment attachment = mock(Attachment.class);
        //noinspection unchecked
        final Function<InputStream, String> inputStreamProcessor = mock(Function.class);
        final String expectedString = "resultString";
        final Promise<String> resultPromise = Promises.promise(expectedString);
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();

        when(attachmentKeyMapper.fromAttachment(attachment))
                .thenReturn(attachmentKey);

        when(backwardCompatibleStoreAdapter.getAttachment(attachmentKey, inputStreamProcessor))
                .thenReturn(resultPromise);

        final Promise<String> result = store.getAttachment(attachment, inputStreamProcessor);

        assertThat(result.claim(), equalTo(expectedString));
    }

    @Test
    public void testGetAttachmentFileByAdapter() throws Exception
    {
        final AttachmentStore.AttachmentAdapter adapter = mock(AttachmentStore.AttachmentAdapter.class);
        final File attachmentDirectory = mock(File.class);
        final File mockFile = mock(File.class);

        when(backwardCompatibleStoreAdapter.getAttachmentFile(adapter, attachmentDirectory))
                .thenReturn(mockFile);

        final File attachmentFile = store.getAttachmentFile(adapter, attachmentDirectory);

        assertThat(attachmentFile, equalTo(mockFile));
    }

    @Test
    public void testDeleteAttachmentContainerForIssueWhenHaveFileBasedStore() throws Exception
    {
        final Issue issue = mock(Issue.class);

        when(backwardCompatibleStoreAdapter.deleteAttachmentContainerForIssue(issue))
                .thenReturn(Promises.promise(Unit.VALUE));

        store.deleteAttachmentContainerForIssue(issue);

        verify(backwardCompatibleStoreAdapter).deleteAttachmentContainerForIssue(issue);
    }

    @Test
    public void testMoveAttachmentToNewIssue() throws Exception
    {
        final Attachment attachment = mock(Attachment.class);
        final String newIssueKey = "NEW-1";
        final String newProjectKey = "NEW";
        final String projectKey = "FOO";
        final String oldIssueKey = "FOO-2";
        final Long attachmentId = 123l;
        final String fileName = "FileName";

        final AttachmentKey oldAttachmentKey = new AttachmentKey(projectKey, oldIssueKey, attachmentId, fileName);
        final AttachmentKey expectedNewAttachmentKey = new AttachmentKey(newProjectKey, newIssueKey, attachmentId, fileName);

        when(attachmentKeyMapper.fromAttachment(attachment))
                .thenReturn(oldAttachmentKey);

        when(backwardCompatibleStoreAdapter.moveAttachment(oldAttachmentKey, expectedNewAttachmentKey))
                .thenReturn(Promises.promise(Unit.VALUE));

        mockProjectManager(newProjectKey);

        store.move(attachment, newIssueKey).claim();

        verify(backwardCompatibleStoreAdapter).moveAttachment(oldAttachmentKey, expectedNewAttachmentKey);
    }

    private void mockProjectManager(final String newProjectKey)
    {
        final Project projectObject = mock(Project.class);
        when(projectManager.getProjectObjByKey(newProjectKey))
                .thenReturn(projectObject);

        when(projectObject.getOriginalKey())
                .thenReturn(newProjectKey);
    }
}
