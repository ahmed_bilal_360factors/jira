package com.atlassian.jira.tenancy;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.plugin.SplitStartupPluginSystemLifecycle;
import com.atlassian.plugins.landlord.spi.LandlordRequests;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @since v6.4
 */
public class TestTenantPluginBridge
{
    @Mock
    SplitStartupPluginSystemLifecycle pluginManager;
    @Mock
    ApplicationProperties applicationProperties;
    @Mock
    TenancyCondition tenancyCondition;
    @Mock
    JiraTenantAccessor jiraTenantAccessor;
    @Mock
    @AvailableInContainer
    LandlordRequests landlordRequests;
    @Rule
    public RuleChain mockitoMocksInContainer =  MockitoMocksInContainer.forTest(this);

    private TenantPluginBridge tenantPluginBridge;

    @Before
    public void initMocks()
    {
        tenantPluginBridge = new TenantPluginBridge(pluginManager, applicationProperties, tenancyCondition, jiraTenantAccessor);
    }

    @Test
    public void testTriggerDoesNotStartPluginsAlone()
    {
        tenantPluginBridge.trigger();
        verify(pluginManager, never()).lateStartup();
    }

    @Test
    public void testTriggerStartsPluginsAfterStart()
    {
        tenantPluginBridge.start();
        tenantPluginBridge.trigger();
        verify(pluginManager).lateStartup();
    }


    @Test
    public void testStartStartsPluginsAfterTrigger()
    {
        tenantPluginBridge.trigger();
        tenantPluginBridge.start();
        verify(pluginManager).lateStartup();
    }

    @Test
    public void testStartDoesNotStartPluginsAlone()
    {
        tenantPluginBridge.start();
        verify(pluginManager, never()).lateStartup();
    }

    @Test
    public void testWithNoBaseUrl() throws Exception
    {
        when(applicationProperties.getDefaultBackedText("jira.baseurl")).thenReturn(null);
        when(tenancyCondition.isEnabled()).thenReturn(false);
        tenantPluginBridge.afterInstantiation();
        verify(landlordRequests, never()).acceptTenant(anyString());
    }

    @Test
    public void testTenantAccepted() throws Exception
    {
        when(applicationProperties.getDefaultBackedText("jira.baseurl")).thenReturn("base");
        when(tenancyCondition.isEnabled()).thenReturn(false);
        tenantPluginBridge.afterInstantiation();
        verify(landlordRequests).acceptTenant("base");
    }

    @Test
    public void testTenantDarkFeature() throws Exception
    {
        when(applicationProperties.getDefaultBackedText("jira.baseurl")).thenReturn("base");
        when(tenancyCondition.isEnabled()).thenReturn(true);
        tenantPluginBridge.afterInstantiation();
        verify(landlordRequests, never()).acceptTenant("base");
    }

    @Test
    public void testBaseUrlRestartsJIRAWhenTenanted() throws Exception
    {
        when(applicationProperties.getDefaultBackedText("jira.baseurl")).thenReturn("base");
        when(tenancyCondition.isEnabled()).thenReturn(true);
        tenantPluginBridge.afterInstantiation();
        tenantPluginBridge.start();
        verify(pluginManager).lateStartup();
        verify(jiraTenantAccessor).addTenant(isA(JiraTenantImpl.class));
    }

}
