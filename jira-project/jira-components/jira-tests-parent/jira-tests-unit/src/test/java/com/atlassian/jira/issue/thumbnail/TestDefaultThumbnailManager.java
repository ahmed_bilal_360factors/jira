package com.atlassian.jira.issue.thumbnail;

import com.atlassian.core.util.thumbnail.Thumber;
import com.atlassian.core.util.thumbnail.Thumbnail;
import com.atlassian.core.util.thumbnail.ThumbnailDimension;
import com.atlassian.core.util.thumbnail.ThumbnailUtil;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.ThumbnailConfiguration;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.AttachmentStore;
import com.atlassian.jira.issue.attachment.ThumbnailAccessor;
import com.atlassian.jira.util.Function;
import com.atlassian.jira.util.io.InputStreamConsumer;
import com.atlassian.jira.util.mime.MimeManager;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;
import com.google.common.base.Predicate;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import static com.atlassian.core.util.thumbnail.Thumbnail.MimeType.PNG;
import static java.io.File.createTempFile;
import static org.apache.commons.io.FileUtils.deleteQuietly;
import static org.apache.commons.io.FileUtils.iterateFiles;
import static org.apache.commons.io.FileUtils.writeStringToFile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestDefaultThumbnailManager
{
    private static final int MAX_HEIGHT = 60;
    private static final int MAX_WIDTH = 50;

    private static final long ATTACHMENT_ID = 666;

    private static final String FILE_NAME = "anyName";

    private static final String UNSUPPORTED_MIME_TYPE = "foo/bar";
    private static final String BASE_URL = "http://testing";
    private static final String BROKEN_THUMBNAIL_URL = "/images/broken_thumbnail.png";

    private static final IOFileFilter THUMBNAIL_FILTER = new IOFileFilter()
    {
        @Override
        public boolean accept(final File file)
        {
            return file.getName().startsWith("thumbnail");
        }

        @Override
        public boolean accept(final File dir, final String name)
        {
            return false;
        }
    };

    private static final File TEMP_DIR = new File(System.getProperty("java.io.tmpdir"));

    @Mock
    private AttachmentManager mockAttachmentManager;
    @Mock
    private AttachmentStore mockAttachmentStore;
    @Mock
    private ThumbnailAccessor mockThumbnailAccessor;
    @Mock
    private MimeManager mockMimeManager;
    @Mock
    private ThumbnailConfiguration mockThumbnailConfiguration;
    @Mock
    private VelocityRequestContextFactory mockVelocityRequestContextFactory;
    @Mock
    private ApplicationProperties applicationProperties;

    @InjectMocks
    private DefaultThumbnailManager thumbnailManager;

    @Mock
    private Attachment mockAttachment;
    @Mock
    private File mockFile;
    @Mock
    private Issue mockIssue;
    @Mock
    private Predicate<ThumbnailDimension> mockRasterBasedRenderingThreshold;
    @Mock
    private Thumber mockThumber;
    @Mock
    private Thumbnail mockThumbnail;
    @Mock
    private ThumbnailedImage mockThumbnailedImage;
    @Mock
    private User mockUser;
    @Mock
    private VelocityRequestContext mockVelocityRequestContext;

    @Before
    public void setUp()
    {
        when(applicationProperties.getEncoding()).thenReturn("UTF-8");

        when(mockThumbnailConfiguration.getMaxHeight()).thenReturn(MAX_HEIGHT);
        when(mockThumbnailConfiguration.getMaxWidth()).thenReturn(MAX_WIDTH);

        when(mockVelocityRequestContext.getCanonicalBaseUrl()).thenReturn(BASE_URL);
        when(mockVelocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(mockVelocityRequestContext);

        when(mockAttachment.getFilename()).thenReturn(FILE_NAME);
        when(mockAttachment.getId()).thenReturn(ATTACHMENT_ID);
    }

    @After
    public void tearDown()
    {
        deleteTemporaryThumbnailFiles();
    }

    private void deleteTemporaryThumbnailFiles()
    {
        final Iterator thumbnails = iterateFiles(TEMP_DIR, THUMBNAIL_FILTER, null);
        while (thumbnails.hasNext())
        {
            deleteQuietly((File) thumbnails.next());
        }
    }

    @Test
    public void attachmentKnownNotToBeThumbnailableShouldBeReportedAsSuch()
    {
        // Set up
        when(mockAttachment.isThumbnailable()).thenReturn(false);

        // Invoke and check
        assertThumbnailable(false);
    }

    @Test
    public void attachmentKnownToBeThumbnailableShouldBeReportedAsSuch()
    {
        // Set up
        when(mockAttachment.isThumbnailable()).thenReturn(true);

        // Invoke and check
        assertThumbnailable(true);
    }

    private void assertThumbnailable(final boolean expectedValue)
    {
        // Invoke
        final boolean actualThumbnailable = thumbnailManager.isThumbnailable(mockIssue, mockAttachment);

        // Check
        assertThat(actualThumbnailable, is(expectedValue));
    }

    @Test
    public void unsupportedFileTypesShouldNotBeThumbnailable()
    {
        // Set up
        when(mockAttachment.isThumbnailable()).thenReturn(null);
        Promise<Boolean> notThumbnailable = Promises.promise(false);
        when(mockAttachmentStore.getAttachment(eq(mockAttachment), any(Function.class))).thenReturn(notThumbnailable);

        // Invoke and check
        assertThumbnailable(false);
        verifyThumbnailableUpdatedTo(false);
    }

    private void verifyThumbnailableUpdatedTo(final boolean valueExpectedToBeStored)
    {
        verify(mockAttachmentManager, only()).setThumbnailable(mockAttachment, valueExpectedToBeStored);
    }

    @Test
    public void fileWithMimeTypeNotSupportedByThumberShouldNotBeThumbnailable()
    {
        assertFileWithMimeTypeIsThumbnailable(UNSUPPORTED_MIME_TYPE, false);
    }

    @Test
    public void fileWithMimeTypeSupportedByThumberShouldBeThumbnailable()
    {
        final String supportedMimeTypeUpper = ThumbnailUtil.getThumbnailMimeTypes().get(0).toUpperCase();
        assertFileWithMimeTypeIsThumbnailable(supportedMimeTypeUpper, true);
    }

    private void assertFileWithMimeTypeIsThumbnailable(final String mimeType, final boolean expectedValue)
    {
        // Set up
        when(mockAttachment.isThumbnailable()).thenReturn(null);
        Promise<Boolean> thumbnailable = Promises.promise(true);
        when(mockAttachmentStore.getAttachment(eq(mockAttachment), any(Function.class))).thenReturn(thumbnailable);
        when(mockThumber.isFileSupportedImage(mockFile)).thenReturn(true);
        when(mockMimeManager.getSuggestedMimeType(FILE_NAME)).thenReturn(mimeType);

        // Invoke and check
        assertThumbnailable(expectedValue);
        verifyThumbnailableUpdatedTo(expectedValue);
    }

    @Test
    public void gettingThumbnailsForIssueShouldReturnEmptyListIfNoneAreThumbnailable()
    {
        // Set up
        when(mockAttachment.isThumbnailable()).thenReturn(false);
        when(mockAttachmentManager.getAttachments(mockIssue)).thenReturn(Collections.singletonList(mockAttachment));

        // Invoke
        final Collection<Thumbnail> thumbnails = thumbnailManager.getThumbnails(mockIssue, mockUser);

        // Check
        assertThat(thumbnails, org.hamcrest.Matchers.<Thumbnail>emptyIterable());
    }

    @Test
    public void thumbnailShouldBeNullForNonThumbnailableAttachment()
    {
        // Set up
        when(mockAttachment.isThumbnailable()).thenReturn(false);

        // Invoke
        final Thumbnail thumbnail = thumbnailManager.getThumbnail(mockAttachment);

        // Check
        assertThat(thumbnail, is(nullValue()));
    }

    @Test
    public void streamingThumbnailShouldProvideCorrectContent() throws IOException
    {
        // Set up
        final String content = "this is some content";
        final File thumbnailFile = createTempFile("thumbnail", null);
        try
        {
            writeStringToFile(thumbnailFile, content);
            when(mockThumbnailAccessor.getThumbnailFile(mockAttachment)).thenReturn(thumbnailFile);

            final String actualContents = thumbnailManager.streamThumbnailContent(mockAttachment, new InputStreamConsumer<String>()
            {
                @Override
                public String withInputStream(final InputStream input) throws IOException
                {
                    return IOUtils.toString(input);
                }
            });

            assertThat(actualContents, is(content));
        }
        finally
        {
            deleteQuietly(thumbnailFile);
        }
    }

    @Test
    public void shouldReturnBrokenThumbnailIfUnableToCreateThumbnail() throws Exception
    {
        // Set up
        final String testGifFileName = Math.random() + ".gif";
        final File thumbnailFile = new File(TEMP_DIR, testGifFileName);
        when(mockAttachment.isThumbnailable()).thenReturn(true);
        when(mockThumbnailAccessor.getThumbnailFile(mockIssue, mockAttachment)).thenReturn(thumbnailFile);
        when(mockAttachmentManager.streamAttachmentContent(eq(mockAttachment), any(InputStreamConsumer.class))).thenThrow(new FileNotFoundException());

        // Invoke
        final Thumbnail thumbnail = thumbnailManager.getThumbnail(mockIssue, mockAttachment);

        // Check
        assertNotNull(thumbnail);
        assertEquals(BrokenThumbnail.class, thumbnail.getClass());
    }

    @Test
    public void shouldCreateThumbnailIfOneDoesNotExist() throws Exception
    {
        // Set up
        final String testGifFileName = Math.random() + ".gif";
        final File thumbnailFile = new File(TEMP_DIR, testGifFileName);
        when(mockAttachment.isThumbnailable()).thenReturn(true);
        when(mockThumbnailAccessor.getThumbnailFile(mockIssue, mockAttachment)).thenReturn(thumbnailFile);
        when(mockAttachmentManager.streamAttachmentContent(eq(mockAttachment), any(InputStreamConsumer.class))).then(new Answer<Object>()
        {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                InputStreamConsumer<?> f = (InputStreamConsumer<?>) invocation.getArguments()[1];
                InputStream is = getClass().getResourceAsStream("thumb.gif");
                return f.withInputStream(is);
            }
        });

        // Invoke
        final Thumbnail thumbnail = thumbnailManager.getThumbnail(mockIssue, mockAttachment);

        // Check
        assertNotNull(thumbnail);
        assertThat(thumbnail.getAttachmentId(), is(ATTACHMENT_ID));
        assertThat(thumbnail.getFilename(), is(testGifFileName));
        assertThat(thumbnail.getHeight(), is(60));
        assertThat(thumbnail.getMimeType(), is(PNG));
        assertThat(thumbnail.getWidth(), is(44));
    }

    @Test
    public void shouldBeAbleToGetThumbnailThatExists()
    {
        // Set up
        final String testGifFileName = "thumb.gif";
        final File thumbnailFile = getFile(testGifFileName);
        when(mockAttachment.isThumbnailable()).thenReturn(true);
        when(mockThumbnailAccessor.getThumbnailFile(mockIssue, mockAttachment)).thenReturn(thumbnailFile);

        // Invoke
        final Thumbnail thumbnail = thumbnailManager.getThumbnail(mockIssue, mockAttachment);

        // Check
        assertNotNull(thumbnail);
        assertThat(thumbnail.getAttachmentId(), is(ATTACHMENT_ID));
        assertThat(thumbnail.getFilename(), is(testGifFileName));
        assertThat(thumbnail.getHeight(), is(306));
        assertThat(thumbnail.getMimeType(), is(PNG));
        assertThat(thumbnail.getWidth(), is(227));
    }

    @Test
    public void brokenThumbnailShouldHaveDefaultImageUrl()
    {
        final ThumbnailedImage thumbnailedImage = thumbnailManager.toThumbnailedImage(new BrokenThumbnail(ATTACHMENT_ID));
        assertEquals(BASE_URL + BROKEN_THUMBNAIL_URL, thumbnailedImage.getImageURL());
    }

    @Test
    public void nullThumbnailShouldHaveNullImage()
    {
        final ThumbnailedImage thumbnailedImage = thumbnailManager.toThumbnailedImage(null);
        assertThat(thumbnailedImage, is(nullValue()));
    }

    @Test
    public void nonNullThumbnailShouldHaveAnImage()
    {
        when(mockThumbnail.getFilename()).thenReturn(FILE_NAME);
        when(mockThumbnail.getHeight()).thenReturn(MAX_HEIGHT);
        when(mockThumbnail.getWidth()).thenReturn(MAX_WIDTH);
        when(mockThumbnail.getAttachmentId()).thenReturn(ATTACHMENT_ID);
        when(mockThumbnail.getMimeType()).thenReturn(PNG);

        final ThumbnailedImage thumbnailedImage = thumbnailManager.toThumbnailedImage(mockThumbnail);

        assertNotNull(thumbnailedImage);
        assertEquals(FILE_NAME, thumbnailedImage.getFilename());
        assertEquals(MAX_HEIGHT, thumbnailedImage.getHeight());
        assertEquals(MAX_WIDTH, thumbnailedImage.getWidth());
        assertEquals(ATTACHMENT_ID, thumbnailedImage.getAttachmentId());
        assertEquals(PNG.name(), thumbnailedImage.getMimeType());
        assertEquals(String.format("%s/secure/thumbnail/%s/%s", BASE_URL, ATTACHMENT_ID, FILE_NAME), thumbnailedImage.getImageURL());
    }

    /**
     * Make sure that the scaling algorithm provided by <code>Thumber</code> has not changed.
     */
    @Test
    public void testScaleSize()
    {
        _testScaleSize(200, 200, 400, 300, 200, 150);//should scale using width
        _testScaleSize(200, 200, 300, 400, 150, 200);//should scale using height
        _testScaleSize(200, 200, 400, 400, 200, 200);//should scale using either
        _testScaleSize(400, 400, 200, 200, 200, 200);//shouldn't scale at all
        _testScaleSize(200, 200, 200, 200, 200, 200);//shouldn't scale at all
    }

    private void _testScaleSize(int maxWidth, int maxHeight, int originalWidth, int originalHeight,
            int expectedWidth, int expectedHeight)
    {
        final ThumbnailDimension widthHeightHelper = new Thumber().
                determineScaleSize(maxWidth, maxHeight, originalWidth, originalHeight);
        assertEquals(expectedWidth, widthHeightHelper.getWidth());
        assertEquals(expectedHeight, widthHeightHelper.getHeight());
    }

    private File getFile(final String name)
    {
        final URL fileUrl = getClass().getResource(name);
        if (fileUrl == null)
        {
            throw new IllegalArgumentException("Can't find '" + name + "' in this package");
        }
        try
        {
            return new File(fileUrl.toURI());
        }
        catch (URISyntaxException e)
        {
            throw new IllegalStateException(e);
        }
    }

}
