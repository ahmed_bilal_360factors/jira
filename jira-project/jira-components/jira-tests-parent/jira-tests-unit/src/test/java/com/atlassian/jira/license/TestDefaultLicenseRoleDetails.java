package com.atlassian.jira.license;

import java.util.Set;

import com.atlassian.extras.decoder.api.LicenseDecoder;
import com.atlassian.extras.decoder.v2.Version2LicenseDecoder;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


/**
 * Tests {@link com.atlassian.jira.license.DefaultLicenseRoleDetails}.
 *
 * @since v6.3
 */
public class TestDefaultLicenseRoleDetails
{
    public static final LicenseDecoder LICENSE_DECODER = new Version2LicenseDecoder();

    public static final LicenseRoleId LIC_ROLE_ID_SOFTWARE = new LicenseRoleId( "com.atlassian.jira.software" );
    public static final LicenseRoleId LIC_ROLE_SERVICEDESK = new LicenseRoleId( "com.atlassian.servicedesk" );
    public static final LicenseRoleId LIC_ROLE_ID_PLATFORM = new LicenseRoleId( "com.atlassian.jira.platform" );

    /**
     * Sample (version 2) license with 3 embedded roles.
     *
     * Corresponds to the following properties:
     * <pre>
     *  Organisation=Dolphin Handlers Inc
     *  ContactName=flipper
     *  ContactEMail=flipper@dhi.com
     *
     *  ServerID=BZQK-F9EL-I0CG-KANJ
     *  SEN=2016178
     *
     *  PurchaseDate=2020-01-01
     *  LicenseExpiryDate=2020-01-01
     *  MaintenanceExpiryDate=2020-01-01
     *  CreationDate=2020-01-01
     *
     *  NumberOfUsers=101
     *  Evaluation=false
     *
     *  jira.LicenseTypeName=COMMERCIAL
     *  jira.LicenseEdition=ENTERPRISE
     *  jira.active=true
     *
     *  jira.product.com.atlassian.jira.platform.active=true
     *  jira.product.com.atlassian.jira.platform.NumberOfUsers=11
     *  jira.product.com.atlassian.jira.software.active=true
     *  jira.product.com.atlassian.jira.software.NumberOfUsers=33
     *  jira.product.com.atlassian.servicedesk.active=true
     *  jira.product.com.atlassian.servicedesk.NumberOfUsers=22
     * </pre>
     */
    public static final String LICENSE_WITH_3_ROLES
            = "AAABUw0ODAoPeNqVUk1Pg0AQve+vIPEMWWjV2oTESlfFtrQWvHgbYWhXtwvZXdD+e5G2idT4lexp3\n"
            + "syb997sSVKhNapWlutZbn9I6fB0YLE4sTzq9skzV+CUqsiq1DhpsXHACNCag3RaSBe5eQWFTlRtn\n"
            + "lDN8weNSvu9HukWXOqSoJAGUhPBBv1c8LJERRaVStegcQwGfY961KZu88iUpyg1sreSq+0xuCdiM\n"
            + "+DiwHSZrfmHQBKjqlGFY//q8X5iX1+wqR3S4MaejKK7nZ0Dd8YNL6TPooQtF8swZr+6LQWYvFBNP\n"
            + "TW8Rt+oCkmjQhqUINNv5P7A2mRTN2oy1C8dzj+OdDP2PBKzqNnsnrnnAzJXK5BcQ2tyXIhyzaV1C\n"
            + "zITTbMVypQEClv0H4K7MRzdeD/5xcg+8GRbYnv8YD6bsWUQjqaE1SCqncQchMa/f7jPW94Bi932b\n"
            + "zAsAhRABc11vuI/4oHZ2Ugh8or9qzgJlwIUVrDZW53cKrMRa1piM6/N2fjcDfE=X02go";

    /**
     * Sample (version 2) license with 1 embedded role.
     *
     * Corresponds to the following properties:
     * <pre>
     *  Organisation=Dolphin Handlers Inc
     *  ContactName=flipper
     *  ContactEMail=flipper@dhi.com
     *
     *  ServerID=BZQK-F9EL-I0CG-KANJ
     *  SEN=2016178
     *
     *  PurchaseDate=2020-01-01
     *  LicenseExpiryDate=2020-01-01
     *  MaintenanceExpiryDate=2020-01-01
     *  CreationDate=2020-01-01
     *
     *  NumberOfUsers=101
     *  Evaluation=false
     *
     *  jira.LicenseTypeName=COMMERCIAL
     *  jira.LicenseEdition=ENTERPRISE
     *  jira.active=true
     *
     *  jira.product.com.atlassian.jira.software.active=true
     *  jira.product.com.atlassian.jira.software.NumberOfUsers=69
     * </pre>
     */
    public static final String LICENSE_WITH_1_ROLE
            = "AAABLQ0ODAoPeNqNkUFPwkAQhe/7Kzbx3Ga3ISgkTcSyagUK0nrxNm6nsGbZNrtblH9vKSER4sHrm\n"
            + "7zvzZu5KVqkk3ZDeUT5YMzYmHMq8oJGjA9I1u4+0C6rN4fWxZxxklgEr2ozBY9xxCIWMB4c9dp4k\n"
            + "D6DHcaVVk2DlnwqC+FcSTQORamOtlhkhViv1mkuiNiDbntYXIF2eGaIBSh9htyXWxXKencBKw4N9\n"
            + "knJcrEQ6ySdzE/zxtZlK/3REILX4JwCE/YjV1f+CyyGXYLaY+xtiyfTb6GLNh4NGIniu1H2cN1za\n"
            + "TdglDutPa11s1WGPoMpdXchmhpJcrR7tOk0fnh/nQWPIzEPUpY8BbNJ9kJykXU0PuS3d+R8mT9z/\n"
            + "l3n8kXDEVm1Vm7B4TXxB5Edo+EwLAIUN16a9L9l4vZ8kuK2Za93WK0E7XwCFCme/9N60BS/IPaEn\n"
            + "ov61BNlR/TDX02f3";

    /**
     * Sample (version 2) license with no embedded roles.
     *
     * Corresponds to the following properties:
     * <pre>
     *  Organisation=Dolphin Handlers Inc
     *  ContactName=flipper
     *  ContactEMail=flipper@dhi.com
     *
     *  ServerID=BZQK-F9EL-I0CG-KANJ
     *  SEN=2016178
     *
     *  PurchaseDate=2020-01-01
     *  LicenseExpiryDate=2020-01-01
     *  MaintenanceExpiryDate=2020-01-01
     *  CreationDate=2020-01-01
     *
     *  NumberOfUsers=101
     *  Evaluation=false
     *
     *  jira.LicenseTypeName=COMMERCIAL
     *  jira.LicenseEdition=ENTERPRISE
     *  jira.active=true
     * </pre>
     */
    public static final String LICENSE_WITH_0_ROLES
            = "AAABCg0ODAoPeNptjz1vgzAYhHf/CkudiWySfiSSpVJwWxoCKdClm2tegiswyBjU/PuSRAyNut7pn\n"
            + "ru7yQfA3nDA1MV0ubldb1ZLzLMcu4SuUDw0X2CS8qMH0zNKKPINCKtaHQgLzCUucQh1TnqrrZA2F\n"
            + "g2wslZdBwZ9KyMWkZKge+CFOsUYj3Oe7tMw44iPoh7OMFaKuoeZwXdC1TPksajUQrbNH1h+7ODc5\n"
            + "Ce7HU/90Isu/pRWIzBrBkATRVvQQkvgP50yx+vJiTkIrfrLgqCtu0pp/Cp0UU9ncaglysCMYMKAP\n"
            + "X2+b53nNY+ckPgvztaL31DG44lG7+j9A5pP/tuzH4ysRA/X+i94R3n0MC0CFD4N65Y60t+Ydx7t1\n"
            + "Y74TA2ybD4UAhUAhEVEdf5B2ItR156vFhFjwH0MK7g=X02dl";

    /**
     * Sample (version 2) license with 3 embedded roles, 1 inactive.
     *
     * Corresponds to the following properties (as JSON):
     * <pre>
     *  Organisation=Dolphin Handlers Inc
     *  ContactName=flipper
     *  ContactEMail=flipper@dhi.com
     *
     *  ServerID=BZQK-F9EL-I0CG-KANJ
     *  SEN=2016178
     *
     *  PurchaseDate=2020-01-01
     *  LicenseExpiryDate=2020-01-01
     *  MaintenanceExpiryDate=2020-01-01
     *  CreationDate=2020-01-01
     *
     *  NumberOfUsers=101
     *  Evaluation=false
     *
     *  jira.LicenseTypeName=COMMERCIAL
     *  jira.LicenseEdition=ENTERPRISE
     *  jira.active=true
     *
     *  jira.product.com.atlassian.jira.platform.active=true
     *  jira.product.com.atlassian.jira.platform.NumberOfUsers=11
     *  jira.product.com.atlassian.jira.software.active=true
     *  jira.product.com.atlassian.jira.software.NumberOfUsers=33
     *  jira.product.com.atlassian.servicedesk.active=false
     *  jira.product.com.atlassian.servicedesk.NumberOfUsers=22
     *
     *  licenseVersion=2
     * </pre>
     */
    public static final String LICENSE_WITH_SERVICE_DESK_INACTIVE_ROLE
            = "AAABXw0ODAoPeNqVUlFPwjAQfu+vWOLzlnUoKskScVSdwkA2ffDt3G5QLd3Sdij/3jIkARJFkz5c7\n"
            + "9rv++67O8kadPrNzKGBQzu9s27PBizNnMCnp+SNK/BqVRVNbry8WnhgBGjNQXptSVel+QCFXtIsX\n"
            + "lGNyyeNSoedDtlPUJ+SqJIGcpPAAsNS8LpGRSaNyuegcQAGw8APfNen9pAhz1FqZJ81V6vD4jcQG\n"
            + "wEXW6SrYs7XAkmKaokqHoTXL48P7s0lG7qxH926D/3kftPOFrvghlcyZEnGppNpnLKj3dYCTFkpm\n"
            + "88NX2JoVIPEqpAGJcj8B7m/oFpvllZNgfp9i1mC0PjXP/smBwFJWWKpaZeeX5CxmoHkGtouB5Wo5\n"
            + "1w6dyALYR87scxJpLCt/kPxvg8HQ6ZEbLx9ttc1a7CB2rVrdwTZqsZ2HaLxaMSmUdwfErYE0Ww0H\n"
            + "7VifwV3Wb4ADan8/DAsAhQZpcfiP91pvd3l/Z9yWV2GWIQ7QgIUbWoAeWc10LDyszWMCK1mNI3G/\n"
            + "ng=X02h9";

    /**
     * Sample (version 2) license with 3 embedded roles, 1 unlimited, 1 with 0 seats, 1 with 1 seat.
     *
     * Corresponds to the following properties (as JSON):
     * <pre>
     *  Organisation=Dolphin Handlers Inc
     *  ContactName=flipper
     *  ContactEMail=flipper@dhi.com
     *
     *  ServerID=BZQK-F9EL-I0CG-KANJ
     *  SEN=2016178
     *
     *  PurchaseDate=2020-01-01
     *  LicenseExpiryDate=2020-01-01
     *  MaintenanceExpiryDate=2020-01-01
     *  CreationDate=2020-01-01
     *
     *  NumberOfUsers=101
     *  Evaluation=false
     *
     *  jira.LicenseTypeName=COMMERCIAL
     *  jira.LicenseEdition=ENTERPRISE
     *  jira.active=true
     *
     *  jira.product.com.atlassian.jira.platformactive=true
     *  jira.product.com.atlassian.jira.platform.NumberOfUsers=1
     *  jira.product.com.atlassian.jira.software.active=true
     *  jira.product.com.atlassian.jira.software.NumberOfUsers=-1
     *  jira.product.com.atlassian.servicedesk.active=true
     *  jira.product.com.atlassian.servicedesk.NumberOfUsers=0
     *
     *  licenseVersion=2
     * </pre>
     */
    public static final String
            LICENSE_WITH_SOFTWARE_UNLIMITED_ROLES
            = "AAABWQ0ODAoPeNqVkl9PgzAUxd/7KUh8hlD8v4TEyariNjYH+uDbFS5btRTSlum+vQxc4mbmNOlLe\n"
            + "3vP+d2Te5TUaPXruUU9ix73Ti97lFosTizPpSfklStwKlVmdWqctCwcMAK05iCdtqTL3LyDQieqi\n"
            + "xdUk/xRo9K+Tcn2A3UpCUppIDURFOjnglcVKjKtVboAjQMw6Huu59oubQ4Z8RSlRvZRcbXaLX4Js\n"
            + "TFwsVG6yhZ8DUhiVEtU4cC/fn4Y2jeXbGSHbnBrD/vR/cFxKgEmL1XRqPMl+kbV2PVseDJueCl9F\n"
            + "iVsNp2FMSMNhDQoQaZ7aH/xbKJZNsIZ6jfnh+Xhlu2IXRKzqDGmZ/T8gkzUHCTX0OIOSlEtuLTuQ\n"
            + "Gai+WuFMiWBwrb6D96tjHbsKRFdRk/NbW3qdUr7okxWFbarEEzGYzYLwv6IsCWIukPOQWj8+/p9d\n"
            + "/kE8On8GDAsAhRpkqVVOzFlu/uyu/1KaeatrwQiYAIUFipB1gupJFemKRXD2LYgnMMAAPo=X02h1";

    @Test
    public void testLicenseRoleIdEquals()
    {
        assertEquals( "LicenseRoleIds test equality by value",
                LIC_ROLE_ID_SOFTWARE, new LicenseRoleId( "com.atlassian.jira.software" ) );
    }

    @Test
    public void testSingleLicenseRoleIsDetected()
    {
        LicenseRoleDetails lic = new DefaultLicenseRoleDetails( LICENSE_WITH_1_ROLE, LICENSE_DECODER );
        Set<LicenseRoleId> role = lic.getIds();

        assertEquals( "1 license role is detected", 1, role.size() );

        assertEquals(
            "Role '" + LIC_ROLE_ID_SOFTWARE.getName() + "' provides 33 seats",
            69, lic.getUserLimit(LIC_ROLE_ID_SOFTWARE) );

        assertEquals(
            "Role '" + LIC_ROLE_SERVICEDESK.getName() + "' provides 0 seats",
            0, lic.getUserLimit(LIC_ROLE_SERVICEDESK) );

        assertEquals(
            "Role '" + LIC_ROLE_ID_PLATFORM.getName() + "' provides 0 seats",
            0, lic.getUserLimit(LIC_ROLE_ID_PLATFORM) );
    }


    @Test
    public void testMultipleLicenseRolesAreDetected()
    {
        LicenseRoleDetails lic = new DefaultLicenseRoleDetails( LICENSE_WITH_3_ROLES, LICENSE_DECODER );
        Set<LicenseRoleId> role = lic.getIds();

        assertEquals( "3 license roles are detected", 3, role.size() );

        assertEquals(
            "Role '" + LIC_ROLE_ID_SOFTWARE.getName() + "' provides 33 seats",
            33, lic.getUserLimit(LIC_ROLE_ID_SOFTWARE) );

        assertEquals(
            "Role '" + LIC_ROLE_SERVICEDESK.getName() + "' provides 22 seats",
            22, lic.getUserLimit(LIC_ROLE_SERVICEDESK) );

        assertEquals(
            "Role '" + LIC_ROLE_ID_PLATFORM.getName() + "' provides 11 seats",
            11, lic.getUserLimit(LIC_ROLE_ID_PLATFORM) );
    }


    @Test
    public void testNoLicenseRolesAreDetected()
    {
        LicenseRoleDetails lic = new DefaultLicenseRoleDetails( LICENSE_WITH_0_ROLES, LICENSE_DECODER );
        Set<LicenseRoleId> role = lic.getIds();

        assertEquals( "0 license roles are detected", 0, role.size() );

        assertEquals(
            "Role '" + LIC_ROLE_ID_SOFTWARE.getName() + "' provides 0 seats",
            0, lic.getUserLimit(LIC_ROLE_ID_SOFTWARE) );

        assertEquals(
            "Role '" + LIC_ROLE_SERVICEDESK.getName() + "' provides 0 seats",
            0, lic.getUserLimit(LIC_ROLE_SERVICEDESK) );

        assertEquals(
            "Role '" + LIC_ROLE_ID_PLATFORM.getName() + "' provides 0 seats",
            0, lic.getUserLimit(LIC_ROLE_ID_PLATFORM) );
    }


    @Test
    public void testUnlimitedRoleLimits()
    {
        LicenseRoleDetails lic = new DefaultLicenseRoleDetails(LICENSE_WITH_SOFTWARE_UNLIMITED_ROLES, LICENSE_DECODER );
        Set<LicenseRoleId> role = lic.getIds();

        assertEquals( "2 license roles are detected", 2, role.size() );

        assertEquals(
            "Role '" + LIC_ROLE_ID_SOFTWARE.getName() + "' provides unlimited seats",
            -1, lic.getUserLimit(LIC_ROLE_ID_SOFTWARE) );

        assertEquals(
            "Role '" + LIC_ROLE_SERVICEDESK.getName() + "' provides 0 seats",
            0, lic.getUserLimit(LIC_ROLE_SERVICEDESK) );
    }
}
