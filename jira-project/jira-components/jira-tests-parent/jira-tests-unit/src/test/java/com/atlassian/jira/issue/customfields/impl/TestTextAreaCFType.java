package com.atlassian.jira.issue.customfields.impl;

import com.atlassian.jira.imports.project.customfield.NoTransformationCustomFieldImporter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;

/**
 * @since v3.13
 */
@RunWith (MockitoJUnitRunner.class)
public class TestTextAreaCFType extends TestGenericTextCFType
{
    @Test
    public void testGetProjectImporter() throws Exception
    {
        TextAreaCFType textAreaCFType = new TextAreaCFType(null, null, null, null);
        assertTrue(textAreaCFType.getProjectImporter() instanceof NoTransformationCustomFieldImporter);
    }

    @Override
    protected GenericTextCFType getFieldTypeUnderTest()
    {
        return new TextAreaCFType(null, null, textFieldCharacterLengthValidator, authenticationContext);
    }

}
