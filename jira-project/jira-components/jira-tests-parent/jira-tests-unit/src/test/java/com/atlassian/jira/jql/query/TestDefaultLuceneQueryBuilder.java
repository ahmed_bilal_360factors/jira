package com.atlassian.jira.jql.query;

import com.atlassian.jira.issue.search.ClauseTooComplexSearchException;
import com.atlassian.jira.issue.search.util.LuceneQueryModifier;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;

import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestDefaultLuceneQueryBuilder
{
    @Rule
    public ExpectedException exception = ExpectedException.none();

    private LuceneQueryModifier luceneQueryModifier;

    @Before
    public void setUp() throws Exception
    {
        luceneQueryModifier = mock(LuceneQueryModifier.class);
    }

    @Test
    public void testCreateLuceneQueryHappyPath() throws Exception
    {
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.EQUALS, "blah");
        final QueryCreationContextImpl context = new QueryCreationContextImpl((ApplicationUser) null);
        final Query falseLuceneQuery = QueryFactoryResult.createFalseResult().getLuceneQuery();

        final QueryVisitor queryVisitor = mock(QueryVisitor.class);
        when(queryVisitor.createQuery(any(TerminalClause.class), any(QueryCreationContext.class))).thenReturn(falseLuceneQuery);
        when(luceneQueryModifier.getModifiedQuery(eq(falseLuceneQuery))).thenReturn(falseLuceneQuery);

        final DefaultLuceneQueryBuilder builder = new DefaultLuceneQueryBuilder(queryVisitor, luceneQueryModifier);

        assertThat(builder.createLuceneQuery(context, clause), equalTo(falseLuceneQuery));
    }

    @Test
    public void testCreateLuceneQueryVistorThrowsUp() throws Exception
    {
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.EQUALS, "blah");
        final QueryCreationContextImpl context = new QueryCreationContextImpl((ApplicationUser) null);

        final QueryVisitor queryVisitor = mock(QueryVisitor.class);
        when(queryVisitor.createQuery(eq(clause), eq(context))).thenThrow(new ContextAwareQueryVisitor.JqlTooComplex(clause));
        final DefaultLuceneQueryBuilder builder = new DefaultLuceneQueryBuilder(queryVisitor, luceneQueryModifier);

        exception.expect(ClauseTooComplexSearchException.class);
        builder.createLuceneQuery(context, clause);
    }

    @Test
    public void testCreateLuceneQueryModifierThrowsUp() throws Exception
    {
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.EQUALS, "blah");
        final QueryCreationContextImpl context = new QueryCreationContextImpl((ApplicationUser) null);
        final Query flaseLuceneQuery = QueryFactoryResult.createFalseResult().getLuceneQuery();

        final QueryVisitor queryVisitor = mock(QueryVisitor.class);
        when(queryVisitor.createQuery(any(TerminalClause.class), any(QueryCreationContext.class))).thenReturn(flaseLuceneQuery);
        when(luceneQueryModifier.getModifiedQuery(eq(flaseLuceneQuery))).thenThrow(new BooleanQuery.TooManyClauses());

        final DefaultLuceneQueryBuilder builder = new DefaultLuceneQueryBuilder(queryVisitor, luceneQueryModifier);
        exception.expect(ClauseTooComplexSearchException.class);
        builder.createLuceneQuery(context, clause);
    }
}
