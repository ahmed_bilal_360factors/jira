package com.atlassian.jira.upgrade.tasks;

import java.util.EnumSet;
import java.util.Map;
import java.util.Set;

import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestService;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.OfBizDelegator;

import com.google.common.collect.Maps;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.stub;
import static org.mockito.Mockito.verify;

/**
 * @since v5.0.3
 */
@RunWith(MockitoJUnitRunner.class)
public class TestUpgradeTask_Build754
{
    @Rule
    public MockComponentContainer mockComponentContainer = new MockComponentContainer(this);

    private static final String ENTITY_NAME = "SearchRequest";
    private static final String FIELD_OWNER = "user";
    private static final String FIELD_AUTHOR = "author";

    @Mock
    private OfBizDelegator delegator;
    private UpgradeTask_Build754 uprgadeTask;

    @Mock @AvailableInContainer
    ReindexRequestService mockReindexRequestService;

    @Before
    public void setUp() throws Exception
    {
//        new MockComponentWorker().init();
        uprgadeTask = new UpgradeTask_Build754(delegator);
    }

    @Test
    public void checkCorrectBuildNumber()
    {
        assertThat(uprgadeTask.getBuildNumber(), equalTo("754"));
    }
    
    @Test
    public void doUpgrade()
    {
        GenericValue emptyUserName = mockGv("", "");
        GenericValue badGoodUser = mockGv("BadOwner", "gooduser");
        GenericValue goodBadUser = mockGv("goodowner", "BadUser");
        GenericValue badBadUser = mockGv("BadOwner", "BadUser");
        GenericValue goodGoodUser = mockGv("goodowner", "gooduser");

        stub(delegator.findAll(ENTITY_NAME)).toReturn(newArrayList(emptyUserName, badGoodUser, goodBadUser, badBadUser, goodGoodUser));

        uprgadeTask.doUpgrade(false);

        assertSearchRequest(emptyUserName, "", "");
        assertSearchRequest(badGoodUser, "badowner", "gooduser");
        assertSearchRequest(goodBadUser, "goodowner", "baduser");
        assertSearchRequest(badBadUser, "badowner", "baduser");
        assertSearchRequest(goodGoodUser, "goodowner", "gooduser");

        verify(delegator).store(badGoodUser);
        verify(delegator).store(goodBadUser);
        verify(delegator).store(badBadUser);
        verify(delegator, never()).store(emptyUserName);
        verify(delegator, never()).store(goodGoodUser);

        verify(mockReindexRequestService).requestReindex(eq(ReindexRequestType.IMMEDIATE), eq(EnumSet.of(AffectedIndex.SHAREDENTITY)), eq(EnumSet.of(SharedEntityType.SEARCH_REQUEST)));
    }

    private void assertSearchRequest(GenericValue emptyUserName, String owner, String author)
    {
        assertThat(emptyUserName.getString(FIELD_OWNER), equalTo(owner));
        assertThat(emptyUserName.getString(FIELD_AUTHOR), equalTo(author));
    }

    private static GenericValue mockGv(String owner, String author)
    {
        Map<String, Object> fields = Maps.newLinkedHashMap();
        fields.put(FIELD_OWNER, owner);
        fields.put(FIELD_AUTHOR, author);

        return new MockGenericValue(ENTITY_NAME, fields);
    }
}
