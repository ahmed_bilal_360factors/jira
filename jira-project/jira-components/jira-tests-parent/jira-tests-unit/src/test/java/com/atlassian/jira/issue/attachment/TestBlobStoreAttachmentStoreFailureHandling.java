package com.atlassian.jira.issue.attachment;

import java.io.InputStream;

import javax.annotation.Nullable;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.attachment.store.BlobStoreAttachmentStore;
import com.atlassian.jira.issue.attachment.store.MockAttachmentKeys;
import com.atlassian.jira.issue.attachment.store.UniqueIdentifierGenerator;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.atlassian.blobstore.client.api.Access;
import io.atlassian.blobstore.client.api.BlobStoreService;
import io.atlassian.blobstore.client.api.Failure;
import io.atlassian.blobstore.client.api.HeadResult;
import io.atlassian.blobstore.client.api.PutResult;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestBlobStoreAttachmentStoreFailureHandling
{
    private final Failure testFailure = new Failure()
    {
        @Override
        public String message()
        {
            return "TEST";
        }
    };

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Rule
    public final TestRule mockInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private BlobStoreService mockBlobStoreService;

    @Mock
    private UniqueIdentifierGenerator uniqueIdentifierGenerator;

    @InjectMocks
    private BlobStoreAttachmentStore store;

    @Test
    public void testFailureOnGetReturnsFailedPromise()
    {
        final Long id = 1l;
        final Either<Failure, Option<String>> getFailure = Either.left(testFailure);
        final Promise<Either<Failure, Option<String>>> getFailurePromise = Promises.promise(getFailure);
        when(mockBlobStoreService.get(eq(id.toString()), any(Access.class), any(com.google.common.base.Function.class))).thenReturn(getFailurePromise);

        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey(id);

        expectedException.expect(AttachmentReadException.class);

        store.getAttachment(attachmentKey, new Function<InputStream, Object>()
        {
            @Override
            public Object get(@Nullable final InputStream input)
            {
                throw new UnsupportedOperationException("Not implemented");
            }
        }).claim();
    }

    @Test
    public void testNotFoundOnGetReturnsFailedPromise()
    {
        final Long id = 1l;
        final Either<Failure, Option<String>> getNotFound = Either.right(Option.<String>none());
        final Promise<Either<Failure, Option<String>>> getFailurePromise = Promises.promise(getNotFound);
        when(mockBlobStoreService.get(eq(id.toString()), any(Access.class), any(com.google.common.base.Function.class))).thenReturn(getFailurePromise);

        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey(id);

        expectedException.expect(NoAttachmentDataException.class);

        store.getAttachment(attachmentKey, new Function<InputStream, Object>()
        {
            @Override
            public Object get(@Nullable final InputStream input)
            {
                throw new UnsupportedOperationException("Not implemented");
            }
        }).claim();
    }

    @Test
    public void testFailureOnPutReturnsFailedPromise() throws Exception
    {
        final Long id = 1l;
        final Long size = 123l;
        final Either<Failure, PutResult> putFailure = Either.left(testFailure);
        final Promise<Either<Failure, PutResult>> putFailurePromise = Promises.promise(putFailure);
        when(mockBlobStoreService.put(eq(id.toString()), any(InputStream.class), any(Long.class))).thenReturn(putFailurePromise);

        final InputStream is = mock(InputStream.class);
        final StoreAttachmentBean storeAttachmentBean = new StoreAttachmentBean.Builder(is)
                .withId(id)
                .withSize(size)
                .build();

        expectedException.expect(AttachmentWriteException.class);

        store.putAttachment(storeAttachmentBean).claim();
    }

    @Test
    public void testFailureOnExistsReturnsFailedPromise() throws Exception
    {
        final Long id = 1l;
        final Either<Failure, Option<HeadResult>> existsFailure = Either.left(testFailure);
        final Promise<Either<Failure, Option<HeadResult>>> existsFailurePromise = Promises.promise(existsFailure);
        when(mockBlobStoreService.head(eq(id.toString()), any(Access.class))).thenReturn(existsFailurePromise);

        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey(id);

        expectedException.expect(AttachmentReadException.class);

        store.exists(attachmentKey).claim();
    }

    @Test
    public void testFailureOnDeleteReturnsFailedPromise() throws Exception
    {
        final Long id = 1l;
        final Either<Failure, Boolean> deleteFailure = Either.left(testFailure);
        final Promise<Either<Failure, Boolean>> existsFailurePromise = Promises.promise(deleteFailure);
        when(mockBlobStoreService.delete(eq(id.toString()))).thenReturn(existsFailurePromise);

        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey(id);

        store.deleteAttachment(attachmentKey).claim();
    }

    @Test
    public void testFailureOnMoveAttachment() throws Exception
    {
        final AttachmentKey oldAttachmentKey = MockAttachmentKeys.getKey(1);
        final AttachmentKey newAttachmentKey = MockAttachmentKeys.getKey(2);

        when(mockBlobStoreService.move(String.valueOf(oldAttachmentKey.getAttachmentId()), String.valueOf(newAttachmentKey.getAttachmentId())))
                .thenReturn(getRejectedPromise());

        expectedException.expect(AttachmentMoveException.class);

        store.moveAttachment(oldAttachmentKey, newAttachmentKey).claim();
    }

    @Test
    public void testFailureOnMoveTemporaryAttachment() throws Exception
    {
        final AttachmentKey oldAttachmentKey = MockAttachmentKeys.getKey(1);
        final TemporaryAttachmentId tempId = TemporaryAttachmentId.fromString("tempId");

        when(mockBlobStoreService.move(tempId.toStringId(), String.valueOf(oldAttachmentKey.getAttachmentId())))
                .thenReturn(getRejectedPromise());

        expectedException.expect(AttachmentMoveException.class);

        store.moveTemporaryToAttachment(tempId, oldAttachmentKey).claim();
    }

    @Test
    public void testFailureOnCopyAttachment() throws Exception
    {
        final AttachmentKey oldAttachmentKey = MockAttachmentKeys.getKey(1);
        final AttachmentKey newAttachmentKey = MockAttachmentKeys.getKey(2);

        when(mockBlobStoreService.copy(String.valueOf(oldAttachmentKey.getAttachmentId()), String.valueOf(newAttachmentKey.getAttachmentId())))
                .thenReturn(getRejectedPromise());

        expectedException.expect(AttachmentRuntimeException.class);

        store.copyAttachment(oldAttachmentKey, newAttachmentKey).claim();
    }

    @Test
    public void testFailureOnDeleteTemporaryAttachment() throws Exception
    {
        final TemporaryAttachmentId tempId = TemporaryAttachmentId.fromString("tempId");
        final Either<Failure, Boolean> deleteFailure = Either.left(testFailure);
        final Promise<Either<Failure, Boolean>> existsFailurePromise = Promises.promise(deleteFailure);
        when(mockBlobStoreService.delete(eq("tempId"))).thenReturn(existsFailurePromise);

        store.deleteTemporaryAttachment(tempId).claim();
    }

    private Promise<Either<Failure, PutResult>> getRejectedPromise()
    {
        final Failure mockFailure = mock(Failure.class);
        return Promises.promise(Either.<Failure, PutResult>left(mockFailure));
    }
}
