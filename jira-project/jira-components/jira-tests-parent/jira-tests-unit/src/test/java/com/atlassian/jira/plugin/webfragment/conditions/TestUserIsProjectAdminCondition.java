package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.junit.rules.MockHttp;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.mockobjects.servlet.MockHttpServletResponse;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;

public class TestUserIsProjectAdminCondition
{
    @Rule
    public final MockHttp mockHttp = MockHttp.withMocks(new MockHttpServletRequest(), new MockHttpServletResponse());

    private final ApplicationUser mockUser = mock(ApplicationUser.class);
    private final PermissionManager mockPermissionManager = mock(PermissionManager.class);

    private final UserIsProjectAdminCondition condition = new UserIsProjectAdminCondition(mockPermissionManager);

    @After
    public void tearDown() throws Exception
    {
        ExecutingHttpRequest.clear();
    }

    @Test
    public void testShouldDisplayWithNullJiraHelper()
    {
        assertFalse(condition.shouldDisplay(mockUser, null));
    }

    @Test
    public void testShouldDisplayWithNullUser()
    {
        assertFalse(condition.shouldDisplay(null, new JiraHelper()));
    }
}
