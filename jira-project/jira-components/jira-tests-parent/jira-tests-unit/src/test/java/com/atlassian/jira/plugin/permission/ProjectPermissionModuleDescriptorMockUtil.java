package com.atlassian.jira.plugin.permission;

import com.atlassian.jira.mock.plugin.MockPlugin;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.plugin.webfragment.descriptors.ConditionDescriptorFactoryImpl;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.WebFragmentHelper;
import com.atlassian.plugin.web.conditions.ConditionLoadingException;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.atlassian.plugin.module.ModuleFactory.LEGACY_MODULE_FACTORY;
import static java.lang.String.format;
import static org.dom4j.DocumentHelper.parseText;
import static org.mockito.Mockito.when;

public class ProjectPermissionModuleDescriptorMockUtil
{
    private static final String CATEGORY = "category=\"%s\"";
    private static final String CONDITION = "<condition class=\"%s\"/>";
    private static final String I18N_DESCRIPTION_KEY = "i18n-description-key=\"%s\"";
    private static final String I18N_NAME_KEY = "i18n-name-key=\"%s\"";
    private static final String KEY = "key=\"%s\"";
    private static final String PROJECT_PERMISSION = "<project-permission %s %s %s %s > %s </project-permission>";

    @Mock public WebFragmentHelper webFragmentHelper;
    @InjectMocks public ConditionDescriptorFactoryImpl conditionDescriptorFactory;
    public Plugin plugin;

    public ProjectPermissionModuleDescriptorMockUtil()
    {
        MockitoAnnotations.initMocks(this);
        plugin = new MockPlugin("plugin.key");
    }

    public ProjectPermissionModuleDescriptorImpl createDescriptor(
            String category, String i18nNameKey,
            String i18nDescriptionKey, String key) throws DocumentException
    {
        return createDescriptor(category, "", i18nNameKey, i18nDescriptionKey, key);
    }

    public ProjectPermissionModuleDescriptorImpl createDescriptor(
            String category, String condition,
            String i18nNameKey, String i18nDescriptionKey, String key) throws DocumentException
    {
        ProjectPermissionModuleDescriptorImpl descriptor = createDescriptor();
        String elementXml = format(PROJECT_PERMISSION,
                formatOrEmpty(CATEGORY, category),
                formatOrEmpty(I18N_DESCRIPTION_KEY, i18nDescriptionKey),
                formatOrEmpty(I18N_NAME_KEY, i18nNameKey),
                formatOrEmpty(KEY, key),
                formatOrEmpty(CONDITION, condition));
        Document document = parseText(elementXml);
        descriptor.init(plugin, document.getRootElement());
        descriptor.enabled();
        return descriptor;
    }

    public void mockWebFragmentHelperForCondition (String className, Condition condition) throws ConditionLoadingException
    {
        when(webFragmentHelper.loadCondition(className, plugin)).thenReturn(condition);
    }

    public void mockWebFragmentHelperForCondition (String className, Class<? extends Exception> e) throws ConditionLoadingException
    {
        when(webFragmentHelper.loadCondition(className, plugin)).thenThrow(e);
    }

    private ProjectPermissionModuleDescriptorImpl createDescriptor()
    {
        return new ProjectPermissionModuleDescriptorImpl(new MockSimpleAuthenticationContext(null), LEGACY_MODULE_FACTORY, conditionDescriptorFactory);
    }

    private String formatOrEmpty(String key, String value)
    {
        return value == null || value.isEmpty() ? "" : format(key, value);
    }
}
