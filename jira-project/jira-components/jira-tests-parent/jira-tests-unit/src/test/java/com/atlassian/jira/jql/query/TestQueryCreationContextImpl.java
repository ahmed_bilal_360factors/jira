package com.atlassian.jira.jql.query;

import java.util.Collections;

import com.atlassian.jira.user.ApplicationUser;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestQueryCreationContextImpl
{
    @Mock
    private ApplicationUser user;


    @Test
    public void testConstructWithUser()
    {
        final QueryCreationContext queryCreationContext = new QueryCreationContextImpl(user);
        assertFalse(queryCreationContext.isSecurityOverriden());
    }

    @Test
    public void testSubQuerySecurityOverridden()
    {
        final QueryCreationContext contextSecurityOverridden = new QueryCreationContextImpl(user, true, Collections.<String>emptySet());
        final QueryCreationContext subContextSecurityOverridden = new QueryCreationContextImpl(contextSecurityOverridden, Collections.<String>emptySet());

        assertTrue(subContextSecurityOverridden.isSecurityOverriden());
    }

    @Test
    public void testSubQuerySecurityNotOverridden()
    {
        final QueryCreationContext contextSecurityNotOverridden = new QueryCreationContextImpl(user, false, Collections.<String>emptySet());
        final QueryCreationContext subContextSecurityNotOverridden = new QueryCreationContextImpl(contextSecurityNotOverridden, Collections.<String>emptySet());

        assertFalse(subContextSecurityNotOverridden.isSecurityOverriden());
    }
}