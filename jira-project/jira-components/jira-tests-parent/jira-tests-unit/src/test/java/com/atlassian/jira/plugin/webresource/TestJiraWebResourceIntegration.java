package com.atlassian.jira.plugin.webresource;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.i18n.CachingI18nFactory;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestJiraWebResourceIntegration
{
    private static final String ABSOLUTE_URL = "http://example.com/path";
    private static final String RELATIVE_URL = "/path";

    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private BuildUtilsInfo buildUtilsInfo;
    @Mock
    private CachingI18nFactory cachingI18nFactory;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private FeatureManager featureManager;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private JiraHome jiraHome;
    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private VelocityRequestContext velocityRequestContext;
    @Mock
    private VelocityRequestContextFactory velocityRequestContextFactory;
    private WebResourceIntegration wri;

    @Rule
    public InitMockitoMocks initMocks = new InitMockitoMocks(this);

    @Before
    public void setUp()
    {
        wri = new JiraWebResourceIntegration(pluginAccessor, applicationProperties, velocityRequestContextFactory,
                buildUtilsInfo, jiraAuthenticationContext, cachingI18nFactory, jiraHome, eventPublisher,
                featureManager);

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
    }

    @After
    public void tearDown()
    {
        ExecutingHttpRequest.clear();
    }

    @Test
    public void baseUrlCdnStrategyTakesPrecedenceOverFixedCdnStrategy()
    {
        when(featureManager.isEnabled(JiraPrefixCDNStrategy.ENABLED_FEATURE_KEY)).thenReturn(true);
        when(featureManager.isEnabled(JiraBaseUrlCDNStrategy.ENABLED_FEATURE_KEY)).thenReturn(true);
        when(featureManager.isEnabled(JiraWebResourceIntegration.CDN_DISABLED_FEATURE_KEY)).thenReturn(false);

        assertTrue(wri.getCDNStrategy() instanceof JiraBaseUrlCDNStrategy);
    }

    @Test
    public void getBaseUrlWithRelativeUrlModeShouldReturnARelativeUrlEvenWhenVelocityRequestContextGetBaseUrlReturnsAnAbsoluteUrl()
    {
        MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        httpServletRequest.setContextPath(RELATIVE_URL);
        ExecutingHttpRequest.set(httpServletRequest, null);

        when(velocityRequestContext.getBaseUrl()).thenReturn(ABSOLUTE_URL);

        assertEquals(RELATIVE_URL, wri.getBaseUrl(UrlMode.RELATIVE));
    }

    @Test
    public void getBaseUrlWithRelativeUrlModeUsesAutoModeWhenNoExecutingHttpRequestIsAvailable()
    {
        ExecutingHttpRequest.clear();
        when(velocityRequestContext.getBaseUrl()).thenReturn(ABSOLUTE_URL);
        assertEquals(ABSOLUTE_URL, wri.getBaseUrl(UrlMode.RELATIVE));
    }

    @Test
    public void getBaseUrlWithRelativeUrlModeShouldReturnARelativeUrlWhenVelocityRequestContextGetBaseUrlReturnsARelativeUrl()
    {
        MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        httpServletRequest.setContextPath(RELATIVE_URL);
        ExecutingHttpRequest.set(httpServletRequest, null);

        when(velocityRequestContext.getBaseUrl()).thenReturn(RELATIVE_URL);

        assertEquals(RELATIVE_URL, wri.getBaseUrl(UrlMode.RELATIVE));
    }
}
