package com.atlassian.velocity.subpackage;

/**
 * This class is part of {@link com.atlassian.velocity.JiraMethodMapTest}. Is used to test that public methods from
 * public interfaces are resolved before methods from private and package scoped
 *
 * @since v6.4
 */

public class VelocityScopingFactory
{
    public static VelocityScoping getTestdObject()
    {
        return new VelocityScopingImpl();
    }

    private static class VelocityScopingImpl implements VelocityScoping
    {
        @Override
        public void testMethod() {}
    }
}

