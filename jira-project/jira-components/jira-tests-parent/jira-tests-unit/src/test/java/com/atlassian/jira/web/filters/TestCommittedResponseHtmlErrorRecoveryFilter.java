package com.atlassian.jira.web.filters;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.util.InternalServerErrorDataSource;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import com.google.common.collect.ImmutableMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Test case for the {@link com.atlassian.jira.web.filters.CommittedResponseHtmlErrorRecoveryFilter}
 *
 * @since 6.4
 */
public class TestCommittedResponseHtmlErrorRecoveryFilter
{
    private static final String SERVLET_EXCEPTION_MESSAGE = "IM IN UR SERVLET THROWIN UR EXCEPTIONZ";

    private CommittedResponseHtmlErrorRecoveryFilter committedResponseHtmlErrorRecoveryFilter;

    @Mock private I18nHelper mockI18nHelper;
    @Mock private FilterChain mockFilterChain;
    @Mock private HttpServletRequest mockServletRequest;
    @Mock private HttpServletResponse mockServletResponse;
    @Mock private SoyTemplateRenderer mockSoyTemplateRenderer;

    @Before
    public void setUp() throws Exception
    {
        initMocks(this);

        JiraAuthenticationContext mockJiraAuthenticationContext = mock(JiraAuthenticationContext.class);
        doReturn(mockI18nHelper).when(mockJiraAuthenticationContext).getI18nHelper();
        doReturn("Copy the content below and send it to your JIRA Administrator")
                .when(mockI18nHelper).getText("500.send.to.your.jira.admin");

        new MockComponentWorker()
                .addMock(JiraAuthenticationContext.class, mockJiraAuthenticationContext)
                .addMock(SoyTemplateRenderer.class, mockSoyTemplateRenderer)
                .init();

        // mock ServletContext
        ServletContext mockServletContext = mock(ServletContext.class);
        doReturn("Server Info.").when(mockServletContext).getServerInfo();
        doReturn(1).when(mockServletContext).getMinorVersion();
        doReturn(2).when(mockServletContext).getMajorVersion();

        // mock HttpSession
        HttpSession mockHttpSession = mock(HttpSession.class);
        doReturn(mockServletContext).when(mockHttpSession).getServletContext();

        // mock ServletRequest
        doReturn("/jira").when(mockServletRequest).getContextPath();
        doReturn(new StringBuffer("http://localhost/jira/foobar")).when(mockServletRequest).getRequestURL();
        doReturn(8090).when(mockServletRequest).getServerPort();
        doReturn(mock(Enumeration.class)).when(mockServletRequest).getAttributeNames();
        doReturn(mockHttpSession).when(mockServletRequest).getSession();
        doReturn(Boolean.TRUE)
                .when(mockServletRequest).getAttribute(CommittedResponseHtmlErrorRecoveryFilter.ENABLE_COMMITTED_RESPONSE_HTML_ERROR_RECOVERY);

        // mock ServletResponse
        doReturn(true).when(mockServletResponse).isCommitted();
        doReturn(mock(PrintWriter.class)).when(mockServletResponse).getWriter();

        // mock FilterChain
        doThrow(new ServletException(SERVLET_EXCEPTION_MESSAGE))
                .when(mockFilterChain).doFilter(any(HttpServletRequest.class), any(HttpServletResponse.class));

        committedResponseHtmlErrorRecoveryFilter = new CommittedResponseHtmlErrorRecoveryFilter();
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testExceptionRethrowWhenResponseNotCommitted() throws Exception
    {
        doReturn(false).when(mockServletResponse).isCommitted();

        try
        {
            committedResponseHtmlErrorRecoveryFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);
            fail("Expected ServletException from further down filterchain to be rethrown when servletResponse is not committed.");
        } catch (ServletException e) {
            assertEquals(SERVLET_EXCEPTION_MESSAGE, e.getMessage());
        }
    }

    @Test
    public void testExceptionRethrowWhenEnableFlagNotSet() throws Exception
    {
        doReturn(Boolean.FALSE)
                .when(mockServletRequest).getAttribute(CommittedResponseHtmlErrorRecoveryFilter.ENABLE_COMMITTED_RESPONSE_HTML_ERROR_RECOVERY);

        try
        {
            committedResponseHtmlErrorRecoveryFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);
            fail("Expected ServletException from further down filterchain to be rethrown when ENABLE_COMMITTED_RESPONSE_HTML_ERROR_RECOVERY flag is not set.");
        } catch (ServletException e) {
            assertEquals(SERVLET_EXCEPTION_MESSAGE, e.getMessage());
        }
    }

    @Test
    public void testServletExceptionIsHandledProperly() throws Exception
    {
        doAnswer(new Answer() {     // first call to SoyTemplateRenderer.render()
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                Object[] args = invocation.getArguments();
                assertEquals("Expected exactly 4 arguments to SoyTemplateRenderer.render()", 4, args.length);
                assertTrue("Expected 4th argument to SoyTemplateRenderer.render() to be an ImmutableMap", args[3] instanceof ImmutableMap);
                ImmutableMap<String, Object> map = (ImmutableMap<String, Object>) args[3];
                ImmutableMap<String, Object> technicalDetails = (ImmutableMap<String, Object>) map.get("technicalDetails");
                assertNotNull("Expected map for SoyTemplateRenderer.render() to include technicalDetails", technicalDetails);
                return null;
            }
        })
        .doAnswer(new Answer()
        {    // second call to SoyTemplateRenderer.render()
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                Object[] args = invocation.getArguments();
                assertEquals("Expected exactly 4 arguments to SoyTemplateRenderer.render()", 4, args.length);
                assertTrue("Expected 4th argument to SoyTemplateRenderer.render() to be an ImmutableMap", args[3] instanceof ImmutableMap);
                ImmutableMap<String, Object> map = (ImmutableMap<String, Object>) args[3];
                String errorPageHtml = (String) map.get("errorPageHtml");
                assertNotNull("Expected map for SoyTemplateRenderer.render() to include errorPageHtml", errorPageHtml);
                return null;
            }
        })
        .when(mockSoyTemplateRenderer).render(any(PrintWriter.class), anyString(), anyString(), any(ImmutableMap.class));

        try
        {
            committedResponseHtmlErrorRecoveryFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);
        } catch (ServletException e) {
            fail("Expected ServletException from further down filterchain to be handled, not rethrown.");
        }

        verify(mockServletRequest, times(1)).setAttribute(eq(InternalServerErrorDataSource.JAVAX_SERVLET_ERROR_EXCEPTION), any());
        verify(mockServletRequest, times(1)).setAttribute(eq(InternalServerErrorDataSource.JAVAX_SERVLET_ERROR_MESSAGE), any());
        verify(mockSoyTemplateRenderer, times(2)).render(any(PrintWriter.class), anyString(), anyString(), any(ImmutableMap.class));
    }

    @Test
    public void testRuntimeExceptionIsAlsoHandled() throws Exception {
        // what if our mock FilterChain throws a RuntimeException instead ?
        doThrow(new RuntimeException(SERVLET_EXCEPTION_MESSAGE))
                .when(mockFilterChain).doFilter(any(HttpServletRequest.class), any(HttpServletResponse.class));
        try
        {
            committedResponseHtmlErrorRecoveryFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);
        } catch (RuntimeException e) {
            fail("Expected RuntimeException from further down filterchain to be handled.");
        }
    }

    @Test
    public void testIOExceptionIsAlsoHandled() throws Exception {
        // what if our mock FilterChain throws an IOException instead ?
        doThrow(new IOException(SERVLET_EXCEPTION_MESSAGE))
                .when(mockFilterChain).doFilter(any(HttpServletRequest.class), any(HttpServletResponse.class));
        try
        {
            committedResponseHtmlErrorRecoveryFilter.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);
        } catch (IOException e) {
            fail("Expected IOException from further down filterchain to be handled.");
        }
    }
}

