package com.atlassian.jira.bc.project.version;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.Locale;
import java.util.Map;

import com.atlassian.core.test.util.DuckTypeProxy;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.MockJiraServiceContext;
import com.atlassian.jira.easymock.EasyMockAnnotations;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockPermissionManager;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectImpl;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.plugin.ProjectPermissionOverrideModuleDescriptor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.util.DateFieldFormat;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.bean.I18nBean;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;

import com.google.common.collect.Lists;
import com.googlecode.catchexception.apis.internal.hamcrest.ExceptionMessageMatcher;

import org.hamcrest.Matchers;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.ofbiz.core.entity.GenericEntityException;

import static com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult.Reason.BAD_NAME;
import static com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult.Reason.BAD_PROJECT;
import static com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult.Reason.BAD_RELEASE_DATE;
import static com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult.Reason.BAD_START_DATE;
import static com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult.Reason.DUPLICATE_NAME;
import static com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult.Reason.FORBIDDEN;
import static com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult.Reason.VERSION_NAME_TOO_LONG;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.user.ApplicationUsers.from;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests the DefaultVersionService class.
 *
 * @since v3.13
 */
public class TestDefaultVersionService
{
    @Rule
    public MockitoContainer initMockitoMocks = MockitoMocksInContainer.rule(this);
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private I18nBean.BeanFactory nopI18nFactory;

    @AvailableInContainer
    @Mock
    private PluginEventManager pluginEventManager;
    @AvailableInContainer
    @Mock
    private PluginAccessor pluginAccessor;

    @Mock
    private VersionManager versionManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private IssueManager issueManager;
    @Mock
    private SearchProvider searchProvider;
    @Mock
    private DateFieldFormat dateFieldFormat;
    @Mock
    private ProjectManager projectManager;

    private DefaultVersionService service;
    private JiraServiceContext context;
    private User user;
    private ApplicationUser applicationUser;

    @Before
    public void setUp() throws Exception
    {
        EasyMockAnnotations.initMocks(this);

        // use a no-op i18n bean
        NoopI18nHelper i18nHelper = new NoopI18nHelper();
        when(nopI18nFactory.getInstance(Mockito.any(User.class))).thenReturn(i18nHelper);
        when(nopI18nFactory.getInstance(Mockito.any(ApplicationUser.class))).thenReturn(i18nHelper);
        when(nopI18nFactory.getInstance(Mockito.any(Locale.class))).thenReturn(i18nHelper);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(Mockito.eq(ProjectPermissionOverrideModuleDescriptor.class)))
                .thenReturn(Lists.<ProjectPermissionOverrideModuleDescriptor>newArrayList());

        context = new MockJiraServiceContext();
        user = new MockUser("admin");
        applicationUser = from(user);

        service = new DefaultVersionService(versionManager, permissionManager, issueManager, searchProvider, nopI18nFactory, dateFieldFormat, projectManager);
    }

    @Test
    public void testDeleteVersionBadActionArguments()
    {
        final PermissionManager permissionManager = new MockPermissionManager(false);
        final VersionManager mockManager = mock(VersionManager.class);

        final Long versionId = 10000L;

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockManager, permissionManager, null, null, null, dateFieldFormat, null);

        expectedException.expect(IllegalArgumentException.class);
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, null, null));
    }

    @Test
    public void testDeleteInvalidVersionToDelete()
    {
        final PermissionManager permissionManager = new MockPermissionManager(false);
        final VersionManager mockVersionManager = mock(VersionManager.class);
        final Long versionId = 10000L;

        when(mockVersionManager.getVersion(versionId)).thenReturn(null);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, null, dateFieldFormat, null);
        // cant use null versionId
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, null, VersionService.REMOVE, VersionService.REMOVE));

        // cant use versionId that does not exist
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, VersionService.REMOVE));
    }

    /**
     * When swapping in a version for the one that is being deleted, the new version cannot be the same as the one which
     * is being deleted. It must also exist.
     */
    @Test
    public void testNoPermissionForProject()
    {
        final MockPermissionManager permissionManager = MyPermissionManager.createPermissionManager(false);

        final Long versionId = 10000L;
        final Long nonExistsVersionId = 9999L;
        final Long swapVersionId = 10001L;
        final Long swapVersionBadProjectId = 10002L;

        final Long goodProjectId = 20000L;
        final Long badProjectId = 20001L;

        final Version version = mockVersion(goodProjectId);
        final Version swapVersionBadProject = mockVersion(badProjectId);
        final Version swapVersion = mockVersion(goodProjectId);

        final Map<Long, Version> versions = MapBuilder.newBuilder(versionId, version).add(swapVersionId, swapVersion)
                .add(swapVersionBadProjectId, swapVersionBadProject).add(nonExistsVersionId, null).toMap();
        final VersionManager mockVersionManager = createMappedVersionManager(versions);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, null, dateFieldFormat, null);
        // if user doesn't have admin permission then fail
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, VersionService.REMOVE));

        // reset permissions and it should pass
        permissionManager.setDefaultPermission(true);
        assertHasNoErrors(context, defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, VersionService.REMOVE));
    }

    /**
     * When swapping in a version for the one that is being deleted, the new version cannot be the same as the one which
     * is being deleted. It must also exist.
     */
    @Test
    public void testBadDeleteSwapArguments()
    {
        final MockPermissionManager permissionManager = MyPermissionManager.createPermissionManager(true);

        final Long versionId = 10000L;
        final Long nonExistsVersionId = 9999L;
        final Long swapVersionId = 10001L;
        final Long swapVersionBadProjectId = 10002L;

        final Long goodProjectId = 20000L;
        final Long badProjectId = 20001L;

        final Version version = mockVersion(goodProjectId);
        final Version swapVersionBadProject = mockVersion(badProjectId);
        final Version swapVersion = mockVersion(goodProjectId);

        final Map<Long, Version> versions = MapBuilder.newBuilder(versionId, version).add(swapVersionId, swapVersion)
                .add(swapVersionBadProjectId, swapVersionBadProject).add(nonExistsVersionId, null).toMap();
        final VersionManager mockVersionManager = createMappedVersionManager(versions);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, null, dateFieldFormat, null);
        // first check SWAP functionality for Affects Version
        // null id, bad version id and same version id will all fail
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, new SwapVersionAction(null), VersionService.REMOVE));
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, new SwapVersionAction(nonExistsVersionId), VersionService.REMOVE));
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, new SwapVersionAction(versionId), VersionService.REMOVE));

        // good version id but different project ids will fail
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, new SwapVersionAction(swapVersionBadProjectId), VersionService.REMOVE));

        // if user has admin permission then pass
        assertHasNoErrors(context, defaultVersionService.validateDelete(context, versionId, new SwapVersionAction(swapVersionId), VersionService.REMOVE));

        // then check SWAP functionality for Fix Version
        // null id, bad version id and same version id will all fail
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, new SwapVersionAction(null)));
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, new SwapVersionAction(nonExistsVersionId)));
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, new SwapVersionAction(versionId)));

        // good version id but different project ids will fail
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, new SwapVersionAction(swapVersionBadProjectId)));

        // good id will work
        assertHasNoErrors(context, defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, new SwapVersionAction(swapVersionId)));
    }

    /**
     * When swapping in a version for the one that is being deleted, the new version cannot be the same as the one which
     * is being deleted. It must also exist.
     */
    @Test
    public void testBadMergeSwapArguments()
    {
        final MockPermissionManager permissionManager = MyPermissionManager.createPermissionManager(false);

        final Long versionId = 10000L;
        final Long nonExistsVersionId = 9999L;
        final Long swapVersionId = 10001L;
        final Long swapVersionBadProjectId = 10002L;

        final Long goodProjectId = 20000L;
        final Long badProjectId = 20001L;

        final Version version = mockVersion(goodProjectId);
        final Version swapVersionBadProject = mockVersion(badProjectId);
        final Version swapVersion = mockVersion(goodProjectId);

        final Map<Long, Version> versions = MapBuilder.newBuilder(versionId, version).add(swapVersionId, swapVersion)
                .add(swapVersionBadProjectId, swapVersionBadProject).add(nonExistsVersionId, null).toMap();
        final VersionManager mockVersionManager = createMappedVersionManager(versions);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, null, dateFieldFormat, null);
        // first check SWAP functionality for Affects Version
        // null id, bad version id and same version id will all fail
        assertHasErrorsAndFlush(context, defaultVersionService.validateMerge(context, versionId, null));
        assertHasErrorsAndFlush(context, defaultVersionService.validateMerge(context, versionId, nonExistsVersionId));
        assertHasErrorsAndFlush(context, defaultVersionService.validateMerge(context, versionId, versionId));

        // good version id but different project ids will fail
        assertHasErrorsAndFlush(context, defaultVersionService.validateMerge(context, versionId, swapVersionBadProjectId));

        // if user doesn't have admin permission then fail
        assertHasErrorsAndFlush(context, defaultVersionService.validateMerge(context, versionId, swapVersionId));

        // reset permissions and it should pass
        permissionManager.setDefaultPermission(true);
        assertHasNoErrors(context, defaultVersionService.validateMerge(context, versionId, swapVersionId));
    }

    @Test
    public void testValidateDeleteResult()
    {
        final MockPermissionManager permissionManager = MyPermissionManager.createPermissionManager(true);
        final Long versionId = 10000L;
        final Long affectsSwapVersionId = 10001L;
        final Long fixSwapVersionId = 10002L;

        final Long projectId = 20000L;

        final Version version = mockVersion(projectId);
        final Version affectsSwapVersion = mockVersion(projectId);
        final Version fixSwapVersion = mockVersion(projectId);

        final Map<Long, Version> versions = MapBuilder.build(versionId, version, affectsSwapVersionId,
                affectsSwapVersion, fixSwapVersionId, fixSwapVersion);
        final VersionManager mockVersionManager = createMappedVersionManager(versions);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, null, dateFieldFormat, null);
        // if not swapping in any versions for Affects or Fix, the returned Versions in the result object will be null,
        // regardless of id passed into method
        VersionService.ValidationResult result = defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, VersionService.REMOVE);
        assertHasNoErrors(context, result);
        assertEquals(version, result.getVersionToDelete());
        assertNull(result.getAffectsSwapVersion());
        assertNull(result.getFixSwapVersion());

        result = defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, VersionService.REMOVE);
        assertHasNoErrors(context, result);
        assertEquals(version, result.getVersionToDelete());
        assertNull(result.getAffectsSwapVersion());
        assertNull(result.getFixSwapVersion());

        // if swapping versions, the result object must contain the version that was referenced by the id
        result = defaultVersionService.validateDelete(context, versionId, new SwapVersionAction(affectsSwapVersionId), new SwapVersionAction(fixSwapVersionId));
        assertHasNoErrors(context, result);
        assertEquals(version, result.getVersionToDelete());
        assertEquals(affectsSwapVersion, result.getAffectsSwapVersion());
        assertEquals(fixSwapVersion, result.getFixSwapVersion());
    }

    @Test
    public void testValidateMergeResult()
    {
        final MockPermissionManager permissionManager = MyPermissionManager.createPermissionManager(true);
        final Long versionId = 10000L;
        final Long swapVersionId = 10001L;

        final Long projectId = 20000L;

        final Version version = mockVersion(projectId);
        final Version swapVersion = mockVersion(projectId);

        final Map<Long, Version> versions = MapBuilder.build(versionId, version, swapVersionId, swapVersion);
        final VersionManager mockVersionManager = createMappedVersionManager(versions);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, null, dateFieldFormat, null);

        // if swapping versions, the result object must contain the version that was referenced by the id
        VersionService.ValidationResult result = defaultVersionService.validateMerge(context, versionId, swapVersionId);
        assertHasNoErrors(context, result);
        assertEquals(version, result.getVersionToDelete());
        assertEquals(swapVersion, result.getAffectsSwapVersion());
        assertEquals(swapVersion, result.getFixSwapVersion());
    }

    @Test
    public void testDeleteWithInvalidResult() throws Exception
    {
        final Long projectId = 20000L;
        final Version version = mockVersion(projectId);
        final Version affectsSwapVersion = mockVersion(projectId);
        final Version fixSwapVersion = mockVersion(projectId);

        // create dummy result object
        ValidationResultImpl badResult = new ValidationResultImpl(new SimpleErrorCollection(), version, affectsSwapVersion, fixSwapVersion, false, Collections.<VersionService.ValidationResult.Reason>emptySet());

        // test delete call with invalid result - should get exception
        DefaultVersionService defaultVersionService = new DefaultVersionService(null, null, null, null, null, dateFieldFormat, null);

        expectedException.expect(IllegalArgumentException.class);
        defaultVersionService.delete(context, badResult);
    }

    @Test
    public void testMergeWithInvalidResult() throws Exception
    {
        final Long projectId = 20000L;
        final Version version = mockVersion(projectId);
        final Version affectsSwapVersion = mockVersion(projectId);
        final Version fixSwapVersion = mockVersion(projectId);

        // create dummy result object
        ValidationResultImpl badResult = new ValidationResultImpl(new SimpleErrorCollection(), version, affectsSwapVersion, fixSwapVersion, false, Collections.<VersionService.ValidationResult.Reason>emptySet());

        // test merge call with invalid result - should get exception
        DefaultVersionService defaultVersionService = new DefaultVersionService(null, null, null, null, null, dateFieldFormat, null);

        expectedException.expect(IllegalArgumentException.class);
        defaultVersionService.merge(context, badResult);
    }

    @Test
    public void testValidateCreateVersionDateOk()
    {
        final String versionName = "versionName";
        long projectId = 373738L;
        final Project project = new MockProject(projectId);

        final VersionManager mockVersionManager = mock(VersionManager.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final ProjectManager projectManager = mock(ProjectManager.class);

        when(permissionManager.hasPermission(Permissions.ADMINISTER, applicationUser))
                .thenReturn(true);

        when(projectManager.getProjectObj(projectId))
                .thenReturn(project);

        when(mockVersionManager.getVersions(projectId))
                .thenReturn(Collections.<Version>emptyList());

        Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
        calendar.set(2000, Calendar.MARCH, 1, 1, 1, 1);
        calendar.set(Calendar.MILLISECOND, 101);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        calendar = Calendar.getInstance(Locale.ENGLISH);
        calendar.set(2000, Calendar.APRIL, 1, 1, 1, 1);
        calendar.set(Calendar.MILLISECOND, 101);

        Date inputReleaseDate = calendar.getTime();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date whenReleaseDate = calendar.getTime();

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, projectManager)
        {
            @Override
            DefaultVersionService.ValidateResult validateCreateParameters(User validateUser, Project validateProject, String validateVersion, String startDate, String releaseDate)
            {
                assertSame(user, validateUser);
                assertSame(project, validateProject);
                assertEquals(versionName, validateVersion);
                assertNull(startDate);
                assertNull(releaseDate);

                return new ValidateResult(new SimpleErrorCollection(), Collections.<CreateVersionValidationResult.Reason>emptySet());
            }
        };

        VersionService.CreateVersionValidationResult result = defaultVersionService.validateCreateVersion(user, project, versionName, inputReleaseDate, null, null);

        assertTrue(result.isValid());
        assertSame(project, result.getProject());
        assertEquals(versionName, result.getVersionName());
        assertNull(result.getStartDate());
        assertEquals(whenReleaseDate, result.getReleaseDate());
    }

    @Test
    public void testValidateCreateVersionDateBad()
    {
        final String versionName = "versionName";
        long projectId = 373738L;
        final Project project = new MockProject(projectId);
        final String badDate = "invalid date";

        final VersionManager mockVersionManager = mock(VersionManager.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final ProjectManager projectManager = mock(ProjectManager.class);

        when(permissionManager.hasPermission(Permissions.ADMINISTER, applicationUser))
                .thenReturn(true);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user))
                .thenReturn(true);

        when(projectManager.getProjectObj(projectId))
                .thenReturn(project);

        when(mockVersionManager.getVersions(projectId))
                .thenReturn(Collections.<Version>emptyList());

        when(dateFieldFormat.parseDatePicker(badDate)).thenThrow(new IllegalArgumentException());
        when(dateFieldFormat.getFormatHint()).thenReturn("dd-MMM-yy");

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, projectManager);

        VersionService.CreateVersionValidationResult result = defaultVersionService.validateCreateVersion(user, project, versionName, badDate, null, null);

        assertFalse(result.isValid());
        assertEquals(EnumSet.of(BAD_RELEASE_DATE), result.getReasons());
    }

    @Test
    public void testValidateCreateVersionDateStringOk()
    {
        final String versionName = "versionName";
        long projectId = 373738L;
        final Project project = new MockProject(projectId);

        final String startDate = "22/10/21";
        final Date parsedStartDate = new Date(new Date().getTime() - 10000);
        final String releaseDate = "27/10/21";
        final Date parsedReleaseDate = new LocalDate(2012, 9, 27).toDate();
        final VersionManager mockVersionManager = mock(VersionManager.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final ProjectManager projectManager = mock(ProjectManager.class);

        when(permissionManager.hasPermission(Permissions.ADMINISTER, applicationUser))
                .thenReturn(true);

        when(projectManager.getProjectObj(projectId))
                .thenReturn(project);

        when(mockVersionManager.getVersions(projectId))
                .thenReturn(Collections.<Version>emptyList());

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, projectManager)
        {

            @Override
            DefaultVersionService.ValidateResult validateCreateParameters(User validateUser, Project validateProject, String validateVersion, String validateStartDate, String validateReleaseDate)
            {
                assertSame(user, validateUser);
                assertSame(project, validateProject);
                assertEquals(versionName, validateVersion);
                if (validateStartDate != null)
                {
                    assertEquals(startDate, validateStartDate);
                }
                assertEquals(releaseDate, validateReleaseDate);

                return new ValidateResult(new SimpleErrorCollection(), Collections.<CreateVersionValidationResult.Reason>emptySet(), validateStartDate == null ? null : parsedStartDate, parsedReleaseDate);
            }
        };

        VersionService.CreateVersionValidationResult result = defaultVersionService.validateCreateVersion(user, project, versionName, releaseDate, null, null);

        assertTrue(result.isValid());
        assertSame(project, result.getProject());
        assertEquals(versionName, result.getVersionName());
        assertNull(result.getStartDate());
        assertEquals(parsedReleaseDate, result.getReleaseDate());
    }

    @Test
    public void testValidateCreateVersionDateStringBad()
    {
        final String startDate = "22/10/21";
        final String releaseDate = "27/10/21";
        final String error = "this is an error";
        final VersionService.CreateVersionValidationResult.Reason reason = BAD_NAME;
        final String versionName = "versionName";
        final Project project = new MockProject(373738L);

        final VersionManager mockVersionManager = mock(VersionManager.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, null)
        {
            @Override
            DefaultVersionService.ValidateResult validateCreateParameters(User validateUser, Project validateProject, String validateVersion, String validateStartDate, String validateReleaseDate)
            {
                assertSame(user, validateUser);
                assertSame(project, validateProject);
                assertEquals(versionName, validateVersion);
                if (validateStartDate != null)
                {
                    assertEquals(startDate, validateStartDate);
                }
                assertEquals(releaseDate, validateReleaseDate);

                SimpleErrorCollection errors = new SimpleErrorCollection();
                errors.addErrorMessage(error);
                return new ValidateResult(errors, EnumSet.of(reason));
            }
        };

        VersionService.CreateVersionValidationResult result = defaultVersionService.validateCreateVersion(user, project, versionName, releaseDate, null, null);

        assertFalse(result.isValid());
        assertEquals(errors(error), result.getErrorCollection());
        assertEquals(EnumSet.of(reason), result.getReasons());
    }

    @Test
    public void testValidateCreateVersionForNullValues()
    {
        String whenedError = "admin.errors.must.specify.valid.project{[]}";
        final VersionManager mockVersionManager = mock(VersionManager.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        DefaultVersionService.ValidateResult result = defaultVersionService.validateCreateParameters(user, null, null, null, null);

        assertFalse(result.isValid());
        assertEquals(errors(whenedError), result.getErrors());
        assertNull(result.getParsedStartDate());
        assertNull(result.getParsedReleaseDate());
        assertEquals(EnumSet.of(BAD_PROJECT), result.getReasons());
    }

    @Test
    public void testValidateCreateVersionNoPermission()
    {
        String whenedError = "admin.errors.version.no.permission{[]}";
        final VersionManager mockVersionManager = mock(VersionManager.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);

        Project mockProject = new MockProject(363637);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);
        when(permissionManager.hasPermission(ADMINISTER_PROJECTS, mockProject, applicationUser)).thenReturn(false);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        DefaultVersionService.ValidateResult result = defaultVersionService.validateCreateParameters(user, mockProject, null, null, null);

        assertFalse(result.isValid());
        assertEquals(errors(whenedError), result.getErrors());
        assertNull(result.getParsedStartDate());
        assertNull(result.getParsedReleaseDate());
        assertEquals(EnumSet.of(FORBIDDEN), result.getReasons());
    }

    @Test
    public void testValidateCreateVersionNullVersionName()
    {
        checkVersionNameError(null);
    }

    @Test
    public void testValidateCreateVersionEmptyVersionName()
    {
        checkVersionNameError("");
    }

    private void checkVersionNameError(String versionName)
    {
        String whenedMessage = "admin.errors.enter.valid.version.name{[]}";
        final VersionManager mockVersionManager = mock(VersionManager.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);

        Project mockProject = new ProjectImpl(null);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);
        when(permissionManager.hasPermission(ADMINISTER_PROJECTS, mockProject, applicationUser)).thenReturn(true);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        DefaultVersionService.ValidateResult result = defaultVersionService.validateCreateParameters(user, mockProject, versionName, null, null);

        assertFalse(result.isValid());
        assertEquals(errorMap("name", whenedMessage), result.getErrors());
        assertNull(result.getParsedStartDate());
        assertNull(result.getParsedReleaseDate());
        assertEquals(EnumSet.of(BAD_NAME), result.getReasons());
    }

    @Test
    public void testValidateCreateVersionAlreadyExists()
    {
        String whenedMessage = "admin.errors.version.already.exists{[]}";

        final VersionManager mockVersionManager = mock(VersionManager.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);

        Project mockProject = new MockProject(1L);

        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        final MockVersion mockVersion = new MockVersion(1, "name");
        when(mockVersionManager.getVersions(1L)).thenReturn(CollectionBuilder.<Version>list(mockVersion));

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        DefaultVersionService.ValidateResult result = defaultVersionService.validateCreateParameters(user, mockProject, "name", null, null);

        assertFalse(result.isValid());
        assertEquals(errorMap("name", whenedMessage), result.getErrors());
        assertNull(result.getParsedStartDate());
        assertNull(result.getParsedReleaseDate());
        assertEquals(EnumSet.of(DUPLICATE_NAME), result.getReasons());
    }

    @Test
    public void testValidateCreateVersionOk()
    {
        final VersionManager mockVersionManager = mock(VersionManager.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final ProjectManager projectManager = mock(ProjectManager.class);

        Project mockProject = new ProjectImpl(null)
        {
            public Long getId()
            {
                return 1L;
            }
        };

        when(permissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(true);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        final MockVersion mockVersion = new MockVersion(1, "1.0");
        when(mockVersionManager.getVersions(1L)).thenReturn(CollectionBuilder.<Version>list(mockVersion));

        when(projectManager.getProjectObj(1L)).thenReturn(mockProject);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, projectManager);

        VersionService.CreateVersionValidationResult result = defaultVersionService.validateCreateVersion(user, mockProject, "1.1", (String) null, null, null);
        assertTrue(result.isValid());
        assertFalse(result.getErrorCollection().hasAnyErrors());
        assertFalse(result.getProject() == null);
        assertFalse(result.getVersionName() == null);
        assertTrue(result.getStartDate() == null);
        assertTrue(result.getReleaseDate() == null);
    }

    @Test
    public void testValidateCreateVersionDatesOk() throws ParseException
    {
        final VersionManager mockVersionManager = mock(VersionManager.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);

        final String startDate = "2007-01-01";
        Date startDateResult = new Date();
        final String releaseDate = "2008-01-01";
        Date releaseDateResult = new Date();

        Project mockProject = new MockProject(1L);

        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        final MockVersion mockVersion = new MockVersion(1, "1.0");
        when(mockVersionManager.getVersions(1L)).thenReturn(CollectionBuilder.<Version>list(mockVersion));

        when(dateFieldFormat.parseDatePicker(startDate)).thenReturn(startDateResult);
        when(dateFieldFormat.parseDatePicker(releaseDate)).thenReturn(releaseDateResult);

        

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        DefaultVersionService.ValidateResult result = defaultVersionService.validateCreateParameters(user, mockProject, "1.1", startDate, releaseDate);
        assertTrue(result.isValid());
        assertEquals(startDateResult, result.getParsedStartDate());
        assertEquals(releaseDateResult, result.getParsedReleaseDate());
        assertTrue(result.getReasons().isEmpty());
    }

    @Test
    public void testValidateCreateVersionDatesWrongOrder() throws ParseException
    {
        final VersionManager mockVersionManager = mock(VersionManager.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);

        final String startDate = "2007-01-01";
        Date startDateResult = new Date();
        final String releaseDate = "2008-01-01";
        Date releaseDateResult = new Date(System.currentTimeMillis() - 1000);

        Project mockProject = new MockProject(1L);

        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        final MockVersion mockVersion = new MockVersion(1, "1.0");
        when(mockVersionManager.getVersions(1L)).thenReturn(CollectionBuilder.<Version>list(mockVersion));

        when(dateFieldFormat.parseDatePicker(startDate)).thenReturn(startDateResult);
        when(dateFieldFormat.parseDatePicker(releaseDate)).thenReturn(releaseDateResult);

        

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        DefaultVersionService.ValidateResult result = defaultVersionService.validateCreateParameters(user, mockProject, "1.1", startDate, releaseDate);
        assertFalse(result.isValid());
        assertEquals(startDateResult, result.getParsedStartDate());
        assertEquals(releaseDateResult, result.getParsedReleaseDate());
    }

    @Test
    public void testValidateCreateVersionStartDateFail() throws Exception
    {
        final VersionManager mockVersionManager = mock(VersionManager.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final I18nHelper mockI18nBean = mock(I18nHelper.class);

        final String format = "dd-MMMMMMM-y";
        final Project mockProject = new MockProject(1L);
        final MockVersion mockVersion = new MockVersion(1, "1.0");
        final Locale locale = new Locale("en");
        final String date = "2008-01-01";
        String whenedMessage = "admin.errors.incorrect.date.format{[dd-MMMMMMM-y]}";

        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);
        when(mockVersionManager.getVersions(1L)).thenReturn(CollectionBuilder.<Version>list(mockVersion));
        when(mockI18nBean.getLocale()).thenReturn(locale);
        when(dateFieldFormat.parseDatePicker(date)).thenThrow(new IllegalArgumentException());
        when(dateFieldFormat.getFormatHint()).thenReturn(format);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        DefaultVersionService.ValidateResult result = defaultVersionService.validateCreateParameters(user, mockProject, "1.1", date, null);

        assertFalse(result.isValid());
        assertEquals(errorMap("startDate", whenedMessage), result.getErrors());
        assertNull(result.getParsedStartDate());
        assertNull(result.getParsedReleaseDate());
        assertEquals(EnumSet.of(BAD_START_DATE), result.getReasons());
    }

    @Test
    public void testValidateCreateVersionReleaseDateFail() throws Exception
    {
        final VersionManager mockVersionManager = mock(VersionManager.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final I18nHelper mockI18nBean = mock(I18nHelper.class);

        final String format = "dd-MMMMMMM-y";
        final Project mockProject = new MockProject(1L);
        final MockVersion mockVersion = new MockVersion(1, "1.0");
        final Locale locale = new Locale("en");
        final String date = "2008-01-01";
        String whenedMessage = "admin.errors.incorrect.date.format{[dd-MMMMMMM-y]}";

        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);
        when(mockVersionManager.getVersions(1L)).thenReturn(CollectionBuilder.<Version>list(mockVersion));
        when(mockI18nBean.getLocale()).thenReturn(locale);
        when(dateFieldFormat.parseDatePicker(date)).thenThrow(new IllegalArgumentException());
        when(dateFieldFormat.getFormatHint()).thenReturn(format);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        DefaultVersionService.ValidateResult result = defaultVersionService.validateCreateParameters(user, mockProject, "1.1", null, date);

        assertFalse(result.isValid());
        assertEquals(errorMap("releaseDate", whenedMessage), result.getErrors());
        assertNull(result.getParsedStartDate());
        assertNull(result.getParsedReleaseDate());
        assertEquals(EnumSet.of(BAD_RELEASE_DATE), result.getReasons());
    }

    @Test
    public void testCreateVersionNameTooLong() throws Exception
    {
        final VersionManager mockVersionManager = mock(VersionManager.class);
        final Project mockProject = new MockProject(1L, "MKY");
        final PermissionManager permissionManager = mock(PermissionManager.class);
        String longText = "";

        while (longText.length() < 256)
        {
            longText += "a";
        }

        String whenedMessage = "admin.errors.version.name.toolong{[]}";

        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);
        when(mockVersionManager.getVersions(mockProject.getId())).thenReturn(Collections.<Version>emptyList());

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, null);
        DefaultVersionService.ValidateResult result = defaultVersionService.validateCreateParameters(user, mockProject, longText, null, null);

        assertFalse(result.isValid());
        assertEquals(errorMap("name", whenedMessage), result.getErrors());
        assertNull(result.getParsedStartDate());
        assertNull(result.getParsedReleaseDate());
        assertEquals(EnumSet.of(VERSION_NAME_TOO_LONG), result.getReasons());

        
    }

    @Test
    public void testCreateVersionReleased() throws Exception
    {
        final VersionManager versionManager = mock(VersionManager.class);
        final Date date = new LocalDate().toDate();

        long projectId = 1L;
        Project mockProject = new MockProject(projectId);

        final MockVersion mockVersion = new MockVersion(1, "1.1");
        when(versionManager.getVersions(projectId)).thenReturn(Collections.<Version>emptyList());
        when(versionManager.createVersion("1.1", null, date, null, 1L, null)).thenReturn(mockVersion);

        final PermissionManager permissionManager = mock(PermissionManager.class);

        when(permissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(true);

        final ProjectManager projectManager = mock(ProjectManager.class);

        when(projectManager.getProjectObj(projectId)).thenReturn(mockProject);

        VersionService.CreateVersionValidationResult request = new VersionService.CreateVersionValidationResult(new SimpleErrorCollection(), mockProject, "1.1", null, date, null, null);
        DefaultVersionService defaultVersionService = new DefaultVersionService(versionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, projectManager);

        Version version = defaultVersionService.createVersion(user, request);

        assertFalse(version == null);
        assertEquals("1.1", version.getName());
    }

    @Test
    public void testCreateVersionWrongRequest() throws Exception
    {
        final VersionManager versionManager = mock(VersionManager.class);

        DefaultVersionService defaultVersionService = new DefaultVersionService(versionManager, null, null, null, null, dateFieldFormat, null);
        VersionService.CreateVersionValidationResult request = new VersionService.CreateVersionValidationResult(new SimpleErrorCollection(), null, null, null, null, null, null);

        expectedException.expect(RuntimeException.class);
        defaultVersionService.createVersion(user, request);
    }

    @Test
    public void testCreateVersionOk() throws Exception
    {
        final VersionManager mockVersionManager = mock(VersionManager.class);

        final Date startDate = new LocalDate().toDate();
        final Date releaseDate = new LocalDate().toDate();
        long projectId = 1L;
        Project project = new MockProject(projectId);

        final MockVersion mockVersion = new MockVersion(1, "1.1");
        final PermissionManager permissionManager = mock(PermissionManager.class);

        when(permissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(true);

        final ProjectManager projectManager = mock(ProjectManager.class);

        when(projectManager.getProjectObj(projectId)).thenReturn(project);

        when(mockVersionManager.getVersions(projectId))
                .thenReturn(Collections.<Version>emptyList());
        when(mockVersionManager.createVersion("1.1", startDate, releaseDate, null, 1L, null)).thenReturn(mockVersion);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, projectManager);
        VersionService.CreateVersionValidationResult request = new VersionService.CreateVersionValidationResult(new SimpleErrorCollection(), project, "1.1", startDate, releaseDate, null, null);

        Version version = defaultVersionService.createVersion(user, request);
        assertFalse(version == null);
        assertEquals("1.1", version.getName());
    }

    @Test
    public void testGetVersionByIdNullId()
    {
        DefaultVersionService defaultVersionService = new DefaultVersionService(null, null, null, null, nopI18nFactory, dateFieldFormat, null);

        expectedException.expect(Matchers.instanceOf(IllegalArgumentException.class));
        defaultVersionService.getVersionById(applicationUser, null, null);
    }

    @Test
    public void testGetVersionByIdNoPermission()
    {
        

        PermissionManager mockPermissionManager = mock(PermissionManager.class);
        Project mockProject = new MockProject((Long) null);

        when(mockPermissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(false);
        when(mockPermissionManager.hasPermission(ADMINISTER_PROJECTS, mockProject, applicationUser)).thenReturn(false);
        when(mockPermissionManager.hasPermission(BROWSE_PROJECTS, mockProject, applicationUser)).thenReturn(false);

        DefaultVersionService defaultVersionService = new DefaultVersionService(null, mockPermissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        final VersionService.VersionResult result = defaultVersionService.getVersionById(applicationUser, mockProject, 1L);

        assertFalse(result.isValid());
        assertEquals("admin.errors.version.no.read.permission{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void testGetVersionsByProjectNoPermission()
    {
        PermissionManager mockPermissionManager = mock(PermissionManager.class);
        Project mockProject = new MockProject((Long) null);

        when(mockPermissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(false);
        when(mockPermissionManager.hasPermission(ADMINISTER_PROJECTS, mockProject, applicationUser)).thenReturn(false);
        when(mockPermissionManager.hasPermission(BROWSE_PROJECTS, mockProject, applicationUser)).thenReturn(false);

        DefaultVersionService defaultVersionService = new DefaultVersionService(null, mockPermissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        final VersionService.VersionsResult result = defaultVersionService.getVersionsByProject(applicationUser, mockProject);

        assertFalse(result.isValid());
        assertEquals("admin.errors.version.no.read.permission{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void testGetVersionByProjectAndNameNoPermission()
    {
        PermissionManager mockPermissionManager = mock(PermissionManager.class);
        Project mockProject = new MockProject((Long) null);

        when(mockPermissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(false);
        when(mockPermissionManager.hasPermission(ADMINISTER_PROJECTS, mockProject, applicationUser)).thenReturn(false);
        when(mockPermissionManager.hasPermission(BROWSE_PROJECTS, mockProject, applicationUser)).thenReturn(false);

        DefaultVersionService defaultVersionService = new DefaultVersionService(null, mockPermissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        
        final VersionService.VersionResult result = defaultVersionService.getVersionByProjectAndName(applicationUser, mockProject, "Version 1");

        assertFalse(result.isValid());
        assertEquals("admin.errors.version.no.read.permission{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void testGetVersionByIdNoVersion()
    {
        PermissionManager mockPermissionManager = mock(PermissionManager.class);
        Project mockProject = new MockProject((Long) null);

        when(mockPermissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(false);
        when(mockPermissionManager.hasPermission(ADMINISTER_PROJECTS, mockProject, applicationUser)).thenReturn(true);

        VersionManager mockVersionManager = mock(VersionManager.class);
        when(mockVersionManager.getVersion(1L)).thenReturn(null);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, mockPermissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        
        final VersionService.VersionResult result = defaultVersionService.getVersionById(applicationUser, mockProject, 1L);

        assertFalse(result.isValid());
        assertEquals("admin.errors.version.not.exist.with.id{[1]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void testGetVersionByIdSuccess()
    {
        Project mockProject = new MockProject((Long) null);
        PermissionManager mockPermissionManager = mock(PermissionManager.class);

        when(mockPermissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(true);

        Version mockVersion = mock(Version.class);

        VersionManager mockVersionManager = mock(VersionManager.class);
        when(mockVersionManager.getVersion(1L)).thenReturn(mockVersion);

        DefaultVersionService defaultVersionService =
                new DefaultVersionService(mockVersionManager, mockPermissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        final VersionService.VersionResult result = defaultVersionService.getVersionById(applicationUser, mockProject, 1L);

        assertTrue(result.isValid());
        assertEquals(mockVersion, result.getVersion());
    }

    @Test
    public void testValidateReleaseVersionNoVersion()
    {
        DefaultVersionService defaultVersionService = new DefaultVersionService(null, null, null, null, nopI18nFactory, dateFieldFormat, null);

        expectedException.expect(IllegalArgumentException.class);
        defaultVersionService.validateReleaseVersion(applicationUser, null, (Date) null);
    }

    @Test
    public void testValidateReleaseVersionNoProject()
    {
        final Version mockVersion = mock(Version.class);
        when(mockVersion.getProjectObject()).thenReturn(null);

        DefaultVersionService defaultVersionService = new DefaultVersionService(null, null, null, null, nopI18nFactory, dateFieldFormat, null);

        final VersionService.ReleaseVersionValidationResult result =
                defaultVersionService.validateReleaseVersion(applicationUser, mockVersion, (Date) null);

        assertFalse(result.isValid());
        assertEquals("admin.errors.must.specify.valid.project{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void testValidateReleaseVersionNoPermission()
    {
        final Project mockProject = new MockProject((Long) null);
        final Version mockVersion = mock(Version.class);

        when(mockVersion.getProjectObject()).thenReturn(mockProject);

        PermissionManager mockPermissionManager = mock(PermissionManager.class);
        when(mockPermissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(false);
        when(mockPermissionManager.hasPermission(ADMINISTER_PROJECTS, mockProject, applicationUser)).thenReturn(false);

        DefaultVersionService defaultVersionService = new DefaultVersionService(null, mockPermissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        final VersionService.ReleaseVersionValidationResult result =
                defaultVersionService.validateReleaseVersion(applicationUser, mockVersion, (Date) null);

        assertFalse(result.isValid());
        assertEquals("admin.errors.version.no.permission{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void testValidateReleaseVersionNoVersionName()
    {
        final Project mockProject = new MockProject((Long) null);
        final MockVersion mockVersion = new MockVersion(678L, "");
        mockVersion.setProjectObject(mockProject);

        PermissionManager mockPermissionManager = mock(PermissionManager.class);
        when(mockPermissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(false);
        when(mockPermissionManager.hasPermission(ADMINISTER_PROJECTS, mockProject, applicationUser)).thenReturn(true);

        DefaultVersionService defaultVersionService = new DefaultVersionService(null, mockPermissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        final VersionService.ReleaseVersionValidationResult result =
                defaultVersionService.validateReleaseVersion(applicationUser, mockVersion, (Date) null);

        assertFalse(result.isValid());
        assertEquals("admin.errors.enter.valid.version.name{[]}", result.getErrorCollection().getErrors().get("name"));
    }

    @Test
    public void testValidateReleaseVersionNoReleaseDateAndReleased()
    {
        final Project mockProject = new MockProject((Long) null);

        MockVersion version = new MockVersion(474747, "JIRA 3.13");
        version.setReleased(true);
        version.setProjectObject(mockProject);

        PermissionManager mockPermissionManager = mock(PermissionManager.class);

        when(mockPermissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(true);

        DefaultVersionService defaultVersionService = new DefaultVersionService(null, mockPermissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        final VersionService.ReleaseVersionValidationResult result =
                defaultVersionService.validateReleaseVersion(applicationUser, version, (Date) null);

        assertFalse(result.isValid());
        assertEquals("admin.errors.release.already.released{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void testValidateReleaseVersionSuccess() throws GenericEntityException
    {
        final Project mockProject = new MockProject((Long) null);

        MockVersion version = new MockVersion(474747, "JIRA 3.13");
        version.setReleased(false);
        version.setProjectObject(mockProject);

        PermissionManager mockPermissionManager = mock(PermissionManager.class);
        when(mockPermissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(true);

        DefaultVersionService defaultVersionService = new DefaultVersionService(null, mockPermissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        final Date releaseDate = new Date();
        final VersionService.ReleaseVersionValidationResult result =
                defaultVersionService.validateReleaseVersion(applicationUser, version, releaseDate);

        assertTrue(result.isValid());
        assertEquals(version, result.getVersion());
        assertEquals(releaseDate, result.getReleaseDate());
    }

    /*
     * Note, this does not test the checkVersionDetails validation, since the all the testValidateRelease**() methods
     * above already test this.
     */
    @Test
    public void testValidateUnreleaseVersionError() throws GenericEntityException
    {
        final Project mockProject = new MockProject((Long) null);
        MockVersion version = new MockVersion(474747, "JIRA 3.13");
        version.setReleased(false);
        version.setProjectObject(mockProject);

        PermissionManager mockPermissionManager = mock(PermissionManager.class);
        when(mockPermissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(true);

        DefaultVersionService defaultVersionService = new DefaultVersionService(null, mockPermissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        final VersionService.ReleaseVersionValidationResult result =
                defaultVersionService.validateUnreleaseVersion(applicationUser, version, (Date) null);

        assertFalse(result.isValid());
        assertEquals("admin.errors.release.not.released{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void testValidateUnreleaseVersionSuccess()
    {
        final Project mockProject = new MockProject((Long) null);

        Version version = mock(Version.class);
        when(version.getProjectObject()).thenReturn(mockProject);
        when(version.getName()).thenReturn("JIRA 3.13");
        when(version.isReleased()).thenReturn(true);

        PermissionManager mockPermissionManager = mock(PermissionManager.class);
        when(mockPermissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(true);

        DefaultVersionService defaultVersionService = new DefaultVersionService(null, mockPermissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        final VersionService.ReleaseVersionValidationResult result =
                defaultVersionService.validateUnreleaseVersion(applicationUser, version, (Date) null);

        assertTrue(result.isValid());
        assertEquals(version, result.getVersion());
    }

    @Test
    public void testReleaseVersionNoResult()
    {
        DefaultVersionService defaultVersionService = new DefaultVersionService(null, null, null, null, null, dateFieldFormat, null);
        expectedException.expect(new ExceptionMessageMatcher<IllegalArgumentException>("You can not release a version with a null validation result."));
        defaultVersionService.releaseVersion(null);
    }

    @Test
    public void testReleaseVersionInvalidResult()
    {
        DefaultVersionService defaultVersionService = new DefaultVersionService(null, null, null, null, null, dateFieldFormat, null);

        ErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("Something bad happened!");
        VersionService.ReleaseVersionValidationResult result = new VersionService.ReleaseVersionValidationResult(errors);

        expectedException.expect(new ExceptionMessageMatcher<IllegalArgumentException>("You can not release a version with an invalid validation result."));
        defaultVersionService.releaseVersion(result);
    }

    @Test
    public void testReleaseVersionSuccess() throws GenericEntityException
    {
        final Date releaseDate = new Date();

        Version version = mock(Version.class);

        version.setReleaseDate(releaseDate);
        version.setReleased(true);
        when(version.getId()).thenReturn(99L);

        Version versionDb = mock(Version.class);

        VersionManager versionManager = mock(VersionManager.class);
        versionManager.releaseVersion(version, true);

        when(versionManager.getVersion(99L)).thenReturn(versionDb);
        when(versionDb.getId()).thenReturn(99L);

        DefaultVersionService defaultVersionService = new DefaultVersionService(versionManager, null, null, null, null, dateFieldFormat, null);

        ErrorCollection errors = new SimpleErrorCollection();
        VersionService.ReleaseVersionValidationResult result = new VersionService.ReleaseVersionValidationResult(errors, version, releaseDate);

        final Version releasedVersion = defaultVersionService.releaseVersion(result);
        assertEquals(versionDb, releasedVersion);
    }

    @Test
    public void testUnreleaseVersionNoResult()
    {
        DefaultVersionService defaultVersionService = new DefaultVersionService(null, null, null, null, null, dateFieldFormat, null);

        expectedException.expect(new ExceptionMessageMatcher<IllegalArgumentException>("You can not unrelease a version with a null validation result."));
        defaultVersionService.unreleaseVersion(null);
    }

    @Test
    public void testUnreleaseVersionInvalidResult()
    {
        DefaultVersionService defaultVersionService = new DefaultVersionService(null, null, null, null, null, dateFieldFormat, null);

        ErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("Something bad happened!");
        VersionService.ReleaseVersionValidationResult result = new VersionService.ReleaseVersionValidationResult(errors);
        expectedException.expect(new ExceptionMessageMatcher<IllegalStateException>("You can not unrelease a version with an invalid validation result."));

        defaultVersionService.unreleaseVersion(result);
    }

    @Test
    public void testUnreleaseVersionSuccess() throws GenericEntityException
    {
        final Date releaseDate = new Date();

        Version mockVersion = mock(Version.class);
        mockVersion.setReleaseDate(releaseDate);
        mockVersion.setReleased(false);
        when(mockVersion.getId()).thenReturn(99L);

        MockVersion databaseVersion = new MockVersion();

        VersionManager mockVersionManager = mock(VersionManager.class);
        mockVersionManager.releaseVersion(mockVersion, false);

        when(mockVersionManager.getVersion(99L)).thenReturn(databaseVersion);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, null, null, null, null, dateFieldFormat, null);
        ErrorCollection errors = new SimpleErrorCollection();
        VersionService.ReleaseVersionValidationResult result = new VersionService.ReleaseVersionValidationResult(errors, mockVersion, releaseDate);
        final Version releasedVersion = defaultVersionService.unreleaseVersion(result);
        assertEquals(databaseVersion, releasedVersion);
    }

    /*
     * Note, this does not test the checkVersionDetails validation, since the all the testValidateRelease**() methods
     * above already test this.
     */
    @Test
    public void testValidateArchiveVersionError() throws GenericEntityException
    {
        final MockProject mockProject = new MockProject(3737L);
        MockVersion mockVersion = new MockVersion(4747, "JIRA 3.13");
        mockVersion.setArchived(true);
        mockVersion.setProjectObject(mockProject);

        PermissionManager mockPermissionManager = mock(PermissionManager.class);
        when(mockPermissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(true);

        DefaultVersionService defaultVersionService = new DefaultVersionService(null, mockPermissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        final VersionService.ArchiveVersionValidationResult result =
                defaultVersionService.validateArchiveVersion(applicationUser, mockVersion);

        assertFalse(result.isValid());
        assertEquals("admin.errors.archive.already.archived{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void testValidateArchiveVersionSuccess() throws GenericEntityException
    {
        final MockProject mockProject = new MockProject(3737L);
        MockVersion mockVersion = new MockVersion(4747, "JIRA 3.13");
        mockVersion.setArchived(false);
        mockVersion.setProjectObject(mockProject);

        PermissionManager mockPermissionManager = mock(PermissionManager.class);
        when(mockPermissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(true);

        DefaultVersionService defaultVersionService = new DefaultVersionService(null, mockPermissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        final VersionService.ArchiveVersionValidationResult result =
                defaultVersionService.validateArchiveVersion(applicationUser, mockVersion);

        assertTrue(result.isValid());
        assertEquals(mockVersion, result.getVersion());
    }

    /*
     * Note, this does not test the checkVersionDetails validation, since the all the testValidateRelease**() methods
     * above already test this.
     */
    @Test
    public void testValidateUnarchiveVersionError() throws GenericEntityException
    {
        final MockProject mockProject = new MockProject(3737L);
        MockVersion mockVersion = new MockVersion(4747, "JIRA 3.13");
        mockVersion.setArchived(false);
        mockVersion.setProjectObject(mockProject);

        PermissionManager mockPermissionManager = mock(PermissionManager.class);
        when(mockPermissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(true);

        DefaultVersionService defaultVersionService = new DefaultVersionService(null, mockPermissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        final VersionService.ArchiveVersionValidationResult result =
                defaultVersionService.validateUnarchiveVersion(applicationUser, mockVersion);

        assertFalse(result.isValid());
        assertEquals("admin.errors.archive.not.archived{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void testValidateUnarchiveVersionSuccess() throws GenericEntityException
    {
        final MockProject mockProject = new MockProject(3737L);
        MockVersion mockVersion = new MockVersion(4747, "JIRA 3.13");
        mockVersion.setArchived(true);
        mockVersion.setProjectObject(mockProject);

        PermissionManager mockPermissionManager = mock(PermissionManager.class);
        when(mockPermissionManager.hasPermission(Permissions.ADMINISTER, applicationUser)).thenReturn(true);

        DefaultVersionService defaultVersionService = new DefaultVersionService(null, mockPermissionManager, null, null, nopI18nFactory, dateFieldFormat, null);

        final VersionService.ArchiveVersionValidationResult result =
                defaultVersionService.validateUnarchiveVersion(applicationUser, mockVersion);

        assertTrue(result.isValid());
        assertEquals(mockVersion, result.getVersion());
    }

    @Test
    public void testArchiveVersionNoResult()
    {
        DefaultVersionService defaultVersionService = new DefaultVersionService(null, null, null, null, null, dateFieldFormat, null);

        expectedException.expect(new ExceptionMessageMatcher<IllegalArgumentException>("You can not archive a version with a null validation result."));
        defaultVersionService.archiveVersion(null);
    }

    @Test
    public void testArchiveVersionInvalidResult()
    {
        DefaultVersionService defaultVersionService = new DefaultVersionService(null, null, null, null, null, dateFieldFormat, null);
        ErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("Something bad happened!");
        VersionService.ArchiveVersionValidationResult result = new VersionService.ArchiveVersionValidationResult(errors);
        expectedException.expect(new ExceptionMessageMatcher<IllegalStateException>("You can not archive a version with an invalid validation result."));
        defaultVersionService.archiveVersion(result);
    }

    @Test
    public void testArchiveVersionSuccess()
    {
        MockVersion dbVersion = new MockVersion(99, "Something");
        MockVersion version = new MockVersion(99, null);
        version.setArchived(false);

        VersionManager mockVersionManager = mock(VersionManager.class);
        mockVersionManager.archiveVersion(version, true);

        when(mockVersionManager.getVersion(99L)).thenReturn(dbVersion);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, null, null, null, null, dateFieldFormat, null);

        ErrorCollection errors = new SimpleErrorCollection();
        VersionService.ArchiveVersionValidationResult result = new VersionService.ArchiveVersionValidationResult(errors, version);
        final Version releasedVersion = defaultVersionService.archiveVersion(result);
        assertEquals(dbVersion, releasedVersion);        
    }

    @Test
    public void testUnarchiveVersionNoResult()
    {
        DefaultVersionService defaultVersionService = new DefaultVersionService(null, null, null, null, null, dateFieldFormat, null);
        expectedException.expect(new ExceptionMessageMatcher<IllegalArgumentException>("You can not unarchive a version with a null validation result."));
        defaultVersionService.unarchiveVersion(null);
    }

    @Test
    public void testUnarchiveVersionInvalidResult()
    {
        DefaultVersionService defaultVersionService = new DefaultVersionService(null, null, null, null, null, dateFieldFormat, null);
        ErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("Something bad happened!");
        VersionService.ArchiveVersionValidationResult result = new VersionService.ArchiveVersionValidationResult(errors);

        expectedException.expect(new ExceptionMessageMatcher<IllegalStateException>("You can not unarchive a version with an invalid validation result."));
        defaultVersionService.unarchiveVersion(result);
    }

    @Test
    public void testUnarchiveVersionSuccess()
    {
        Version mockVersion = mock(Version.class);
        mockVersion.setArchived(false);
        when(mockVersion.getId()).thenReturn(99L);

        Version mockVersionDB = mock(Version.class);

        VersionManager mockVersionManager = mock(VersionManager.class);
        mockVersionManager.archiveVersion(mockVersion, false);

        when(mockVersionManager.getVersion(99L)).thenReturn(mockVersionDB);
        when(mockVersionDB.getId()).thenReturn(99L);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, null, null, null, null, dateFieldFormat, null);

        ErrorCollection errors = new SimpleErrorCollection();
        VersionService.ArchiveVersionValidationResult result = new VersionService.ArchiveVersionValidationResult(errors, mockVersion);
        final Version releasedVersion = defaultVersionService.unarchiveVersion(result);
        assertEquals(mockVersionDB, releasedVersion);
    }

    @Test
    public void testIsOverdueNoVersion()
    {
        DefaultVersionService service = new DefaultVersionService(null, null, null, null, null, dateFieldFormat, null);
        expectedException.expect(Matchers.instanceOf(IllegalArgumentException.class));
        service.isOverdue(null);
    }

    @Test
    public void testIsOverdueProject()
    {
        VersionManager versionManager = mock(VersionManager.class);
        Version version = new MockVersion(18388338L, "Brenden");

        when(versionManager.isVersionOverDue(version)).thenReturn(false).thenReturn(true);
        DefaultVersionService service = new DefaultVersionService(versionManager, null, null, null, null, dateFieldFormat, null);

        assertFalse(service.isOverdue(version));
        assertTrue(service.isOverdue(version));
    }

    @Test
    public void testDatesAreConvertedToMidnight()
    {
        long projectId = 1234L;
        MockProject mockProject = new MockProject(projectId);

        final Version mockVersion = new MockVersion(1L, "Version Name", mockProject);
        final DateTime now = new DateTime();
        final Date midnightNow = new LocalDate(now.getMillis()).toDate();
        final Date nonMidnight = new DateTime(now).withHourOfDay(4).toDate();

        final VersionService.VersionBuilder builder = new VersionService.VersionBuilder(mockVersion)
                .startDate(nonMidnight)
                .releaseDate(nonMidnight);

        final VersionManager mockVersionManager = mock(VersionManager.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final ProjectManager projectManager = mock(ProjectManager.class);

        when(projectManager.getProjectObj(projectId))
                .thenReturn(mockProject);

        when(permissionManager.hasPermission(Permissions.ADMINISTER, applicationUser))
                .thenReturn(true);

        when(mockVersionManager.isDuplicateName(mockVersion, "Version Name"))
                .thenReturn(false);

        DefaultVersionService defaultVersionService = new DefaultVersionService(mockVersionManager, permissionManager, null, null, nopI18nFactory, dateFieldFormat, projectManager);
        VersionService.VersionBuilderValidationResult result = defaultVersionService.validateUpdate(applicationUser, builder);

        assertThat(result.getResult().startDate, Matchers.is(midnightNow));
        assertThat(result.getResult().releaseDate, Matchers.is(midnightNow));
    }

    @Test
    public void testDeleteWithMergeValidationResultPerformsMerge()
    {
        Version versionToDelete = mockVersion(1L);
        Version versionToMergeTo = mockVersion(2L);
        ValidationResultImpl validationResult = new ValidationResultImpl(
                new SimpleErrorCollection(),
                versionToDelete,
                versionToMergeTo,
                versionToMergeTo,
                true,
                Collections.<VersionService.ValidationResult.Reason>emptySet());

        service.delete(context, validationResult);

        verify(versionManager).merge(context.getLoggedInApplicationUser(), versionToDelete, versionToMergeTo);
    }

    @Test
    public void testDeleteWithDeleteValidationResultPerformsDeleteAndRemove()
    {
        Version versionToDelete = mockVersion(1L);
        ValidationResultImpl validationResult = new ValidationResultImpl(
                new SimpleErrorCollection(),
                versionToDelete,
                null,
                null,
                true,
                Collections.<VersionService.ValidationResult.Reason>emptySet());

        service.delete(context, validationResult);

        verify(versionManager).deleteAndRemoveFromIssues(context.getLoggedInApplicationUser(), versionToDelete);
    }

    private ErrorCollection errorMap(String name, String message)
    {
        SimpleErrorCollection collection = new SimpleErrorCollection();
        collection.addError(name, message);
        return collection;
    }

    private ErrorCollection errors(String... args)
    {
        SimpleErrorCollection collection = new SimpleErrorCollection();
        for (String arg : args)
        {
            collection.addErrorMessage(arg);
        }
        return collection;
    }

    private VersionManager createMappedVersionManager(final Map<Long, Version> versions)
    {
        return (VersionManager) DuckTypeProxy.getProxy(VersionManager.class, new Object()
        {
            public Version getVersion(Long id)
            {
                return versions.get(id);
            }
        });
    }

    private void assertHasErrorsAndFlush(JiraServiceContext context, VersionService.ValidationResult result)
    {
        assertNotNull(result);
        assertFalse(context.getErrorCollection().getFlushedErrorMessages().isEmpty());
        assertFalse(result.isValid());
    }

    private void assertHasNoErrors(JiraServiceContext context, VersionService.ValidationResult result)
    {
        assertNotNull(result);
        if (context.getErrorCollection().hasAnyErrors())
        {
            assertFalse(context.getErrorCollection().getErrorMessages().iterator().next(), context.getErrorCollection().hasAnyErrors());
        }
        assertTrue(result.isValid());
    }

    private MockVersion mockVersion(final Long projectId)
    {
        return new MockVersion()
        {
            public Project getProjectObject()
            {
                return new MockProject(projectId);
            }

            public Long getProjectId()
            {
                return projectId;
            }
        };
    }
}
