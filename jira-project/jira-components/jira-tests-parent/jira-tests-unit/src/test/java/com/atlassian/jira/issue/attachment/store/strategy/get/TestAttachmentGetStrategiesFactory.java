package com.atlassian.jira.issue.attachment.store.strategy.get;

import com.atlassian.jira.issue.attachment.FileSystemAttachmentStore;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class TestAttachmentGetStrategiesFactory
{

    private final AttachmentGetStrategiesFactory attachmentGetStrategiesFactory = new AttachmentGetStrategiesFactory();

    @Test
    public void shouldProvideDualAttachmentGetStrategy() throws Exception
    {
        final FileSystemAttachmentStore primaryStore = mock(FileSystemAttachmentStore.class);
        final StreamAttachmentStore secondaryStore = mock(StreamAttachmentStore.class);

        final DualAttachmentGetStrategy result = attachmentGetStrategiesFactory.createDualGetAttachmentStrategy(
                primaryStore, secondaryStore);

        final DualAttachmentGetStrategy expectedStrategy = new DualAttachmentGetStrategy(primaryStore, secondaryStore);

        assertThat(result, equalTo(expectedStrategy));
    }

    @Test
    public void testCreateSingleStoreGetStrategy() throws Exception
    {
        final StreamAttachmentStore singleStore = mock(StreamAttachmentStore.class);

        final SingleStoreAttachmentGetStrategy result = attachmentGetStrategiesFactory.createSingleStoreGetStrategy(singleStore);


        final SingleStoreAttachmentGetStrategy expectedStrategy = new SingleStoreAttachmentGetStrategy(singleStore);
        assertThat(result, equalTo(expectedStrategy));
    }
}