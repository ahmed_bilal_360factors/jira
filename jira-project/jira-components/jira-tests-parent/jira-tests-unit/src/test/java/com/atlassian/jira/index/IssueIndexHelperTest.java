package com.atlassian.jira.index;

import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IssueIndexer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class IssueIndexHelperTest
{
    @Mock
    private IssueManager issueManager;
    @Mock
    private IssueIndexer issueIndexer;
    @Mock
    private IssueFactory issueFactory;

    @Test
    public void testEnsureCapacity() throws Exception
    {
        IssueIndexHelper issueIndexHelper = new IssueIndexHelper(issueManager, issueIndexer, issueFactory);

        checkArrayExpandsCorrectly(issueIndexHelper, 0, 0);
        checkArrayExpandsCorrectly(issueIndexHelper, 0, 1);
        checkArrayExpandsCorrectly(issueIndexHelper, 1, 1);
        checkArrayExpandsCorrectly(issueIndexHelper, 2, 2);
        checkArrayExpandsCorrectly(issueIndexHelper, 10, 11);
        checkArrayExpandsCorrectly(issueIndexHelper, 10, 100);
        checkArrayExpandsCorrectly(issueIndexHelper, 1000, 1000);
        checkArrayExpandsCorrectly(issueIndexHelper, 1000, 1001);
        checkArrayExpandsCorrectly(issueIndexHelper, 1000, 999);
        checkArrayExpandsCorrectly(issueIndexHelper, 1000, 99);
        checkArrayExpandsCorrectly(issueIndexHelper, 1000, 1);
    }

    private void checkArrayExpandsCorrectly(final IssueIndexHelper issueIndexHelper, final int currentSize, final int entryToAdd)
    {
        long[] issueIds = new long[currentSize];
        issueIds = issueIndexHelper.ensureCapacity(issueIds, entryToAdd + 1);
        issueIds[entryToAdd] = 0L;
    }
}