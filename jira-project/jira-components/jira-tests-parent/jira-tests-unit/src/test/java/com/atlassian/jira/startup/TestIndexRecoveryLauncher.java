package com.atlassian.jira.startup;

import com.atlassian.core.util.DateUtils;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.index.ha.IndexRecoveryManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.task.TaskProgressSink;
import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestIndexRecoveryLauncher
{
    @Mock @AvailableInContainer
    private IndexRecoveryManager indexRecoveryManager;

    @Mock @AvailableInContainer
    private ApplicationProperties applicationProperties;

    @Mock @AvailableInContainer
    private ClusterManager clusterManager;

    @Mock
    private Appender mockAppender;

    @Captor
    private ArgumentCaptor<LoggingEvent> captorLoggingEvent;

    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);
    private Level previousLevel;

    @Before
    public void setup()
    {
        when(applicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");

        // Add an appender to the log4J logger so we can spy on what is logged.
        org.apache.log4j.Logger root = org.apache.log4j.Logger.getRootLogger();
        root.addAppender(mockAppender);
        previousLevel = root.getLevel();
        root.setLevel(Level.INFO);
    }

    @After
    public void tearDown() throws Exception
    {
        // remove this appender
        org.apache.log4j.Logger root = org.apache.log4j.Logger.getRootLogger();
        root.removeAppender(mockAppender);
        root.setLevel(previousLevel);
    }

    @Test
    public void testHappyPath() throws Exception
    {
        Date start = new Date();
        Date end = new Date(start.getTime());
        DateUtils.DateRange range = new DateUtils.DateRange(start, end);

        when(indexRecoveryManager.getDurationToRecover()).thenReturn(range);

        IndexRecoveryLauncher launcher = new IndexRecoveryLauncher();
        launcher.start();

        verify(indexRecoveryManager).reindexIssuesIn(eq(range), any(TaskProgressSink.class));
        verify(mockAppender, never()).doAppend(captorLoggingEvent.capture());
    }

    @Test
    public void testDBAhead() throws Exception
    {
        Date start = new Date();
        Date end = new Date(start.getTime() + TimeUnit.MINUTES.toMillis(5));
        DateUtils.DateRange range = new DateUtils.DateRange(start, end);

        when(indexRecoveryManager.getDurationToRecover()).thenReturn(range);

        IndexRecoveryLauncher launcher = new IndexRecoveryLauncher();
        launcher.start();

        verify(indexRecoveryManager).reindexIssuesIn(eq(range), any(TaskProgressSink.class));
        verify(mockAppender, times(2)).doAppend(captorLoggingEvent.capture());
        LoggingEvent loggingEvent = captorLoggingEvent.getAllValues().get(0);
        assertThat(loggingEvent.getMessage().toString(), is("Recover Issue Index - start"));
        loggingEvent = captorLoggingEvent.getAllValues().get(1);
        assertThat(loggingEvent.getMessage().toString(), is("Recover Issue Index - end"));
    }

    @Test
    public void testDBBehind() throws Exception
    {
        Date start = new Date();
        Date end = new Date(start.getTime() - TimeUnit.MINUTES.toMillis(5));
        DateUtils.DateRange range = new DateUtils.DateRange(start, end);

        when(indexRecoveryManager.getDurationToRecover()).thenReturn(range);

        IndexRecoveryLauncher launcher = new IndexRecoveryLauncher();
        launcher.start();

        verify(indexRecoveryManager).reindexIssuesIn(eq(range), any(TaskProgressSink.class));
        verify(mockAppender, times(2)).doAppend(captorLoggingEvent.capture());
        LoggingEvent loggingEvent = captorLoggingEvent.getAllValues().get(0);
        assertThat(loggingEvent.getMessage().toString(), is("Recover Issue Index - start"));
        loggingEvent = captorLoggingEvent.getAllValues().get(1);
        assertThat(loggingEvent.getMessage().toString(), is("Recover Issue Index - end"));
    }

    @Test
    public void testTooFarBehind() throws Exception
    {
        Date start = new Date();
        Date end = new Date(start.getTime() + TimeUnit.DAYS.toMillis(2));
        DateUtils.DateRange range = new DateUtils.DateRange(start, end);

        when(indexRecoveryManager.getDurationToRecover()).thenReturn(range);

        IndexRecoveryLauncher launcher = new IndexRecoveryLauncher();
        launcher.start();

        verify(indexRecoveryManager, never()).reindexIssuesIn(eq(range), any(TaskProgressSink.class));

        verify(mockAppender, times(1)).doAppend(captorLoggingEvent.capture());
        LoggingEvent loggingEvent = captorLoggingEvent.getAllValues().get(0);
        assertThat(loggingEvent.getMessage().toString(),
                is("Issue index is out of date with the database by more than 24 hours. Automatic recovery is not attempted"));
    }

    @Test
    public void testInCluster() throws Exception
    {
        when(clusterManager.isClustered()).thenReturn(Boolean.TRUE);

        IndexRecoveryLauncher launcher = new IndexRecoveryLauncher();
        launcher.start();

        verify(indexRecoveryManager, never()).getDurationToRecover();
        verify(indexRecoveryManager, never()).reindexIssuesIn(any(DateUtils.DateRange.class), any(TaskProgressSink.class));
    }

    @Test
    public void testInRecovery() throws Exception
    {
        when(applicationProperties.getOption("disaster.recovery")).thenReturn(Boolean.TRUE);

        IndexRecoveryLauncher launcher = new IndexRecoveryLauncher();
        launcher.start();

        verify(indexRecoveryManager, never()).getDurationToRecover();
        verify(indexRecoveryManager, never()).reindexIssuesIn(any(DateUtils.DateRange.class), any(TaskProgressSink.class));
    }
}
