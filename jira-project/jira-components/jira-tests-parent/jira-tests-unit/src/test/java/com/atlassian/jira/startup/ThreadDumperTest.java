package com.atlassian.jira.startup;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @since 6.4
 */
@RunWith (MockitoJUnitRunner.class)
public class ThreadDumperTest
{
    public static final int LONG_WAIT = 200;
    public static final int SHORT_WAIT = 1;
    public static final int MEDIUM_WAIT = 20;

    private @Mock Logger log;

    @Test
    public void loggingShouldBeEnabledByDefault()
    {
        ThreadDumper threadDumper = new ThreadDumper(SHORT_WAIT);

        assertTrue(threadDumper.isLogEnabled());
    }

    @Test
    public void cancelDumpShouldClearLogFlag()
    {
        ThreadDumper threadDumper = new ThreadDumper(SHORT_WAIT);
        threadDumper.cancelDump();

        assertFalse(threadDumper.isLogEnabled());
    }

    @Test
    public void shouldLogDisabledShouldExitEarly()
    {
        ThreadDumper threadDumper = new ThreadDumper(SHORT_WAIT, log);
        threadDumper.cancelDump();

        threadDumper.run();
        verify(log).debug(anyString());
        verifyNoMoreInteractions(log);
    }

    @Test
    public void logsShouldOccurWhenInterrupted() throws Exception
    {
        ThreadDumper threadDumper = new ThreadDumper(LONG_WAIT, log);

        Thread thread = new Thread(threadDumper, "Thread-dumping thread from + " + this.getClass().getSimpleName());
        thread.setDaemon(true);
        thread.interrupt();

        thread.start();

        thread.join();

        verify(log).error(anyString());
        verifyNoMoreInteractions(log);
    }

    @Test
    public void logsShouldOccurWhenInterruptExceptioned() throws Exception
    {
        ThreadDumper threadDumper = new ThreadDumper(LONG_WAIT, log);

        Thread thread = new Thread(threadDumper, "Thread-dumping thread from + " + this.getClass().getSimpleName());
        thread.setDaemon(true);
        thread.start();

        Thread.sleep(MEDIUM_WAIT); // let the other thread get started.

        thread.interrupt();

        thread.join();

        verify(log).error(eq("Thread logging thread interrupted. Will log threads immediately."), any(InterruptedException.class));
        verify(log).error(anyString()); // the actual thread dump.
        verifyNoMoreInteractions(log);
    }

    @Test
    public void timeoutsLessThanOneShouldBecomeOne()
    {
        assertEquals(SHORT_WAIT, new ThreadDumper(0).getTimeout());
        assertEquals(SHORT_WAIT, new ThreadDumper(-SHORT_WAIT).getTimeout());
    }

    @Test
    public void nothingShouldHappenWhenThreadCancelledAfterLogging() throws Exception
    {
        ThreadDumper threadDumper = new ThreadDumper(SHORT_WAIT, log);

        Thread thread = new Thread(threadDumper, "Thread-dumping thread from + " + this.getClass().getSimpleName());
        thread.setDaemon(true);
        thread.start();
        thread.join();

        threadDumper.cancelDump();

        verify(log).error(anyString()); // the actual thread dump.
        verifyNoMoreInteractions(log);
    }

    @Test
     public void cancellingBeforeLoggingShouldNotLog() throws Exception
    {
        ThreadDumper threadDumper = new ThreadDumper(LONG_WAIT, log);

        Thread thread = new Thread(threadDumper, "Thread-dumping thread from + " + this.getClass().getSimpleName());
        thread.setDaemon(true);
        thread.start();

        Thread.sleep(MEDIUM_WAIT);

        threadDumper.cancelDump();
        thread.join();

        verify(log).info(eq("Thread logging requested and JIRA shutdown completed within specified timeout " + LONG_WAIT + "ms. Hooray!"));
        verifyNoMoreInteractions(log);
    }
}
