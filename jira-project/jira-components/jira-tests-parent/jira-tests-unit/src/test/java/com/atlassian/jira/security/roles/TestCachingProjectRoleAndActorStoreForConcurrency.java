package com.atlassian.jira.security.roles;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.Nullable;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.core.test.util.DuckTypeProxy;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.mock.MockProjectRoleManager;
import com.atlassian.jira.mock.MockProjectRoleManager.MockRoleActor;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static com.atlassian.jira.mock.Strict.strict;
import static com.atlassian.jira.security.roles.ProjectRoleActor.USER_ROLE_ACTOR_TYPE;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SuppressWarnings("UnusedParameters")
public class TestCachingProjectRoleAndActorStoreForConcurrency
{
    private static final Set<User> NO_USERS = ImmutableSet.of();

    /**
     * test that multiple clients hitting the getAllProjectRoles() method concurrently do not bloat the cache with
     * duplicates.
     */
    @Test
    public void testGetAllProjectRolesRaceCondition() throws Exception
    {
        final List<ProjectRole> allRoles = MockProjectRoleManager.DEFAULT_ROLE_TYPES;
        final int WORKERS = 10;
        final AtomicInteger allProjectsCalledCount = new AtomicInteger(0);

        // ducktype mock implements part of ProjectRoleAndActorStore
        final Object mock = new Object()
        {
            @SuppressWarnings("unused")
            public Collection<ProjectRole> getAllProjectRoles()
            {
                allProjectsCalledCount.incrementAndGet();
                try
                {
                    Thread.sleep(20);
                }
                catch (final InterruptedException e)
                {
                    throw new RuntimeException(e);
                }
                return allRoles;
            }
        };
        final ProjectRoleAndActorStore slowProjectRoleAndActorStore = (ProjectRoleAndActorStore)DuckTypeProxy.getProxy(
                ProjectRoleAndActorStore.class,
                ImmutableList.of(mock));

        final CachingProjectRoleAndActorStore cachingProjectRoleAndActorStore = new CachingProjectRoleAndActorStore(
                slowProjectRoleAndActorStore,
                new MockProjectRoleManager.MockRoleActorFactory(),
                new MemoryCacheManager());

        final AtomicInteger calledCount = new AtomicInteger(0);
        final CountDownLatch workersRunning = new CountDownLatch(WORKERS);
        final CountDownLatch workersRan = new CountDownLatch(WORKERS);

        final ExecutorService pool = Executors.newCachedThreadPool();
        try
        {
            // Using the latch to make certain that all threads meet where we want them to.
            // This allows us to force all the threads into the code at a point in which they could possibly be
            // populating the cache with duplicates. As the cache uses locks to ensure the delegate doesn't get called
            // multiple times, we can't block inside the delegate (called method).
            for (int i = 0; i < WORKERS; i++)
            {
                pool.submit(new Runnable()
                {
                    public void run()
                    {
                        workersRunning.countDown();
                        try
                        {
                            workersRunning.await(10, SECONDS);
                        }
                        catch (final InterruptedException e)
                        {
                            throw new RuntimeException(e);
                        }
                        cachingProjectRoleAndActorStore.getAllProjectRoles();
                        calledCount.getAndIncrement();
                        workersRan.countDown();
                    }
                });
            }

            workersRan.await(10, SECONDS);
            assertEquals(WORKERS, calledCount.get());

            assertEquals(3, cachingProjectRoleAndActorStore.getAllProjectRoles().size());
        }
        finally
        {
            pool.shutdown();
        }
    }

    /**
     * makes sure an iterator on the Collection returned by getAllProjectRoles will not throw CCME even if the
     * underlying collection is concurrently modified.
     */
    @Test
    public void testGetAllProjectRolesDoesntThrowConcurrentMod() throws Exception
    {
        final List<ProjectRole> allRoles = MockProjectRoleManager.DEFAULT_ROLE_TYPES;
        @SuppressWarnings("unused")
        final Object mock = new Object()
        {
            public Collection<ProjectRole> getAllProjectRoles()
            {
                return allRoles;
            }
        };
        final ProjectRoleAndActorStore slowProjectRoleAndActorStore = (ProjectRoleAndActorStore) DuckTypeProxy.getProxy(
                ProjectRoleAndActorStore.class,
                ImmutableList.of(mock));

        final CachingProjectRoleAndActorStore cachingProjectRoleAndActorStore = new CachingProjectRoleAndActorStore(slowProjectRoleAndActorStore,
            new MockProjectRoleManager.MockRoleActorFactory(), new MemoryCacheManager());

        final Iterator<ProjectRole> it = cachingProjectRoleAndActorStore.getAllProjectRoles().iterator();
        it.next();

        cachingProjectRoleAndActorStore.clearCaches();

        it.next();
    }

    /**
     * makes sure an iterator on the Collection returned by getAllProjectRoles will not throw CCME even if the
     * underlying collection is concurrently modified.
     */
    @Test
    public void testRemovingRoleDoesntCauseExistingRoleListToThrowConcurrentModSingleThreaded() throws Exception
    {
        final List<ProjectRole> allRoles = MockProjectRoleManager.DEFAULT_ROLE_TYPES;
        @SuppressWarnings("unused")
        final Object mock = new Object()
        {
            public Collection<ProjectRole> getAllProjectRoles()
            {
                return allRoles;
            }

            public void deleteProjectRole(final ProjectRole projectRole)
            {}
        };
        final ProjectRoleAndActorStore slowProjectRoleAndActorStore = (ProjectRoleAndActorStore) DuckTypeProxy.getProxy(
                ProjectRoleAndActorStore.class,
                ImmutableList.of(mock));

        final CachingProjectRoleAndActorStore cachingProjectRoleAndActorStore = new CachingProjectRoleAndActorStore(slowProjectRoleAndActorStore,
            new MockProjectRoleManager.MockRoleActorFactory(), new MemoryCacheManager());

        final Iterator<ProjectRole> it = cachingProjectRoleAndActorStore.getAllProjectRoles().iterator();
        it.next();
        cachingProjectRoleAndActorStore.deleteProjectRole(MockProjectRoleManager.PROJECT_ROLE_TYPE_3);
        it.next();
    }

    /**
     * makes sure an iterator on the Collection returned by getAllProjectRoles will not throw CCME even if the
     * underlying collection is concurrently modified by another thread.
     */
    @Test
    public void testRemovingRoleDoesntCauseExistingRoleListToThrowConcurrentModMultiThread() throws Exception
    {
        final List<ProjectRole> allRoles = MockProjectRoleManager.DEFAULT_ROLE_TYPES;
        @SuppressWarnings("unused")
        final Object mock = new Object()
        {
            public Collection<ProjectRole> getAllProjectRoles()
            {
                return allRoles;
            }

            public void deleteProjectRole(final ProjectRole projectRole)
            {}
        };
        final ProjectRoleAndActorStore slowProjectRoleAndActorStore = (ProjectRoleAndActorStore)DuckTypeProxy.getProxy(
                ProjectRoleAndActorStore.class,
                ImmutableList.of(mock));

        final CachingProjectRoleAndActorStore cachingProjectRoleAndActorStore = new CachingProjectRoleAndActorStore(
                slowProjectRoleAndActorStore,
                new MockProjectRoleManager.MockRoleActorFactory(),
                new MemoryCacheManager());

        final Iterator<ProjectRole> it = cachingProjectRoleAndActorStore.getAllProjectRoles().iterator();
        final ExecutorService pool = Executors.newCachedThreadPool();
        try
        {
            final Future<?> future = pool.submit(new Runnable()
            {
                public void run()
                {
                    cachingProjectRoleAndActorStore.deleteProjectRole(MockProjectRoleManager.PROJECT_ROLE_TYPE_3);
                }
            });
            assertNull(future.get());

            it.next();
            it.next();

            assertNull(future.get());
        }
        finally
        {
            pool.shutdown();
        }
    }

    /**
     * makes sure an iterator on the Collection returned by getAllProjectRoles will not throw CCME even if the
     * underlying collection is concurrently modified.
     */
    @Test
    public void testAddingRoleDoesntCauseExistingRoleListToThrowConcurrentModSingleThread() throws Exception
    {
        final List<ProjectRole> allRoles = MockProjectRoleManager.DEFAULT_ROLE_TYPES;

        @SuppressWarnings("unused")
        final Object mock = new Object()
        {
            public Collection<ProjectRole> getAllProjectRoles()
            {
                return allRoles;
            }

            public ProjectRole addProjectRole(final ProjectRole projectRole)
            {
                return projectRole;
            }
        };

        final ProjectRoleAndActorStore slowProjectRoleAndActorStore = (ProjectRoleAndActorStore)DuckTypeProxy.getProxy(
                ProjectRoleAndActorStore.class,
                ImmutableList.of(mock));

        final CachingProjectRoleAndActorStore cachingProjectRoleAndActorStore = new CachingProjectRoleAndActorStore(
                slowProjectRoleAndActorStore,
                new MockProjectRoleManager.MockRoleActorFactory(),
                new MemoryCacheManager());

        final Iterator<ProjectRole> it = cachingProjectRoleAndActorStore.getAllProjectRoles().iterator();
        it.next();
        cachingProjectRoleAndActorStore.addProjectRole(MockProjectRoleManager.PROJECT_ROLE_TYPE_3);
        // should not throw ConcurrentMod
        it.next();
    }

    /**
     * makes sure an iterator on the Collection returned by getAllProjectRoles will not throw CCME even if the
     * underlying collection is concurrently modified by another thread.
     */
    @Test
    public void testAddingRoleDoesntCauseExistingRoleListToThrowConcurrentModMultiThread() throws Exception
    {
        final List<ProjectRole> allRoles = MockProjectRoleManager.DEFAULT_ROLE_TYPES;

        @SuppressWarnings("unused")
        final Object mock = new Object()
        {
            public Collection<ProjectRole> getAllProjectRoles()
            {
                return allRoles;
            }

            public void deleteProjectRole(final ProjectRole projectRole)
            {}
        };
        final ProjectRoleAndActorStore mockProjectRoleAndActorStore = (ProjectRoleAndActorStore) DuckTypeProxy.getProxy(
                ProjectRoleAndActorStore.class,
                ImmutableList.of(mock));

        final CachingProjectRoleAndActorStore cachingProjectRoleAndActorStore = new CachingProjectRoleAndActorStore(
                mockProjectRoleAndActorStore,
                new MockProjectRoleManager.MockRoleActorFactory(),
                new MemoryCacheManager());

        final Iterator<ProjectRole> it = cachingProjectRoleAndActorStore.getAllProjectRoles().iterator();
        final ExecutorService pool = Executors.newCachedThreadPool();
        try
        {
            final Future<?> future = pool.submit(new Runnable()
            {
                public void run()
                {
                    cachingProjectRoleAndActorStore.deleteProjectRole(MockProjectRoleManager.PROJECT_ROLE_TYPE_3);
                }
            });

            // make sure the mod has actually happened in the other thread
            assertNull(future.get());

            it.next();
            it.next();

            // make sure there weren't any exceptions in the other thread
            assertNull(future.get());
        }
        finally
        {
            pool.shutdown();
        }
    }

    /**
     * makes sure an iterator on the Collection returned by getAllProjectRoles will not throw CCME even if the
     * underlying collection is concurrently modified.
     */
    @Test
    public void testCacheClearingDoesntCauseExistingRoleListToThrowConcurrentMod() throws Exception
    {
        final List<ProjectRole> allRoles = MockProjectRoleManager.DEFAULT_ROLE_TYPES;
        @SuppressWarnings("unused")
        final Object mock = new Object()
        {
            public Collection<ProjectRole> getAllProjectRoles()
            {
                return allRoles;
            }
        };
        final ProjectRoleAndActorStore slowProjectRoleAndActorStore = (ProjectRoleAndActorStore) DuckTypeProxy.getProxy(
                ProjectRoleAndActorStore.class,
                ImmutableList.of(mock));

        final CachingProjectRoleAndActorStore cachingProjectRoleAndActorStore = new CachingProjectRoleAndActorStore(slowProjectRoleAndActorStore,
            new MockProjectRoleManager.MockRoleActorFactory(), new MemoryCacheManager());

        final Iterator<ProjectRole> it = cachingProjectRoleAndActorStore.getAllProjectRoles().iterator();
        it.next();
        cachingProjectRoleAndActorStore.clearCaches();
        it.next();
    }

    @Test
    public void testConcurrentProjectRoleActorUpdate() throws Exception
    {
        final long projectId = 1L;
        final Long roleId = MockProjectRoleManager.PROJECT_ROLE_TYPE_1.getId();
        final RoleActor testuser = new MockRoleActor(1L, roleId, null, NO_USERS, USER_ROLE_ACTOR_TYPE, "testuser");
        final RoleActor fred = new MockRoleActor(2L, roleId, null, NO_USERS, USER_ROLE_ACTOR_TYPE, "fred");

        final ProjectRoleActors staleProjectRoleActorsWithUsers = new ProjectRoleActorsImpl(projectId, roleId,
                ImmutableSet.of(testuser));
        final ProjectRoleActors updatedProjectRoleActorsWithUsers = new ProjectRoleActorsImpl(projectId, roleId,
                ImmutableSet.of(testuser, fred));

        final CountDownLatch getCalled = new CountDownLatch(1);
        final CountDownLatch updateDone = new CountDownLatch(1);

        // Should call once for the stale data then again for the updated data
        final ProjectRoleAndActorStore delegate = mock(ProjectRoleAndActorStore.class);
        when(delegate.getProjectRoleActors(roleId, projectId))
                .thenAnswer(new Answer<ProjectRoleActors>()
                {
                    @Override
                    public ProjectRoleActors answer(final InvocationOnMock invocation) throws Exception
                    {
                        getCalled.countDown();
                        // It is ok for this to timeout; it just means that the cache technology blocks
                        // the invalidation request until the getter (me!) is done to ensure safety
                        updateDone.await(1L, SECONDS);
                        return staleProjectRoleActorsWithUsers;
                    }
                })
                .thenReturn(updatedProjectRoleActorsWithUsers)
                .thenAnswer(strict());

        final CachingProjectRoleAndActorStore fixture = new CachingProjectRoleAndActorStore(
                delegate, new MockProjectRoleManager.MockRoleActorFactory(), new MemoryCacheManager());

        // Thread responsible for invoking the get
        final ExecutorService pool = Executors.newCachedThreadPool();
        try
        {
            final Future<?> getter = pool.submit(new Callable<Void>()
            {
                @Nullable
                public Void call() throws Exception
                {
                    ProjectRoleActors roleActors = fixture.getProjectRoleActors(roleId, projectId);

                    // The first call overlaps the update and may return either stale or updated information,
                    // depending on the details of the cache implementation.
                    assertThat(roleActors.getRoleActors(), anyOf(hasSize(1), hasSize(2)));

                    assertThat("update should finish", updateDone.await(5L, SECONDS), is(true));

                    // Once the update is done, further gets must always see updated results
                    roleActors = fixture.getProjectRoleActors(roleId, projectId);
                    assertThat(roleActors.getRoleActors(), containsInAnyOrder(testuser, fred));
                    return null;
                }
            });

            // Thread responsible for invoking the update
            final Future<?> updater = pool.submit(new Callable<Void>()
            {
                @Nullable
                public Void call() throws Exception
                {
                    getCalled.await();
                    fixture.updateProjectRoleActors(updatedProjectRoleActorsWithUsers);
                    updateDone.countDown();
                    return null;
                }
            });

            // make sure nothing bad happened in the clients and wait for them to complete...
            getter.get();
            updater.get();

            // The value we get from the get call should always be the value set by the update call
            ProjectRoleActors roleActors = fixture.getProjectRoleActors(roleId, projectId);
            assertThat(roleActors.getRoleActors(), containsInAnyOrder(testuser, fred));
        }
        finally
        {
            pool.shutdown();
        }
    }
}
