package com.atlassian.jira.issue;

import com.atlassian.jira.issue.customfields.impl.NumberCFType;
import com.atlassian.jira.issue.customfields.impl.RenderableTextCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestDefaultIssueTextFieldCharacterLengthValidator
{
    private static final String LONG_TEXT = "This is a very long line that does not fit within the limit";
    private static final String SHORT_TEXT = "This a short text";
    private static final String CF1_ID = "customfield_10010";
    private static final String CF2_ID = "customfield_10020";
    static ModifiedValue DUMMY_VALUE = new ModifiedValue("OLD", "NEW");

    @Rule
    public MockComponentContainer container = new MockComponentContainer(this);

    DefaultIssueTextFieldCharacterLengthValidator defaultIssueTextFieldCharacterLengthValidator;
    @Mock
    private TextFieldCharacterLengthValidator textFieldCharacterLengthValidator;
    @Mock
    @AvailableInContainer
    private CustomFieldManager customFieldManager;
    @Mock
    private Field descriptionField;
    @Mock
    private Field environmentField;
    @Mock
    private Field summaryField;
    @Mock
    MutableIssue issue;
    @Mock
    CustomField customField1;
    @Mock
    CustomField customField2;

    RenderableTextCFType customField1Type;

    NumberCFType customField2Type;

    Map<String, ModifiedValue> modifiedFields;

    @Before
    public void setUp()
    {
        when(textFieldCharacterLengthValidator.isTextTooLong(LONG_TEXT)).thenReturn(true);
        when(textFieldCharacterLengthValidator.isTextTooLong(SHORT_TEXT)).thenReturn(false);

        defaultIssueTextFieldCharacterLengthValidator =
                new DefaultIssueTextFieldCharacterLengthValidator(textFieldCharacterLengthValidator);
        when(descriptionField.getId()).thenReturn(IssueFieldConstants.DESCRIPTION);
        when(environmentField.getId()).thenReturn(IssueFieldConstants.ENVIRONMENT);
        when(summaryField.getId()).thenReturn(IssueFieldConstants.SUMMARY);

        when(customFieldManager.getCustomFieldObjects(issue)).thenReturn(ImmutableList.of(customField1, customField2));
        when(customFieldManager.getCustomFieldObject(CF1_ID)).thenReturn(customField1);
        when(customFieldManager.getCustomFieldObject(CF2_ID)).thenReturn(customField2);

        customField1Type = new RenderableTextCFType(null, null, null, null);
        when(customField1.getCustomFieldType()).thenReturn(customField1Type);
        when(customField1.getId()).thenReturn(CF1_ID);

        customField2Type = new NumberCFType(null, null, null);
        when(customField2.getCustomFieldType()).thenReturn(customField2Type);
        when(customField2.getId()).thenReturn(CF2_ID);

        modifiedFields = Maps.newHashMap();
        when(issue.getModifiedFields()).thenReturn(modifiedFields);
    }

    private void markFieldAsModified(String... fieldIds)
    {
        for (final String fieldId: fieldIds)
        {
            modifiedFields.put(fieldId, DUMMY_VALUE);
        }
    }

    @Test
    public void validateAllFieldChecksDescriptionLength()
    {
        when(issue.getDescription()).thenReturn(SHORT_TEXT);
        defaultIssueTextFieldCharacterLengthValidator.validateAllFields(issue);
        verify(textFieldCharacterLengthValidator).isTextTooLong(SHORT_TEXT);
    }

    @Test
    public void validateModifiedFieldChecksModifiedDescriptionLength()
    {
        when(issue.getDescription()).thenReturn(SHORT_TEXT);
        markFieldAsModified(IssueFieldConstants.DESCRIPTION);

        defaultIssueTextFieldCharacterLengthValidator.validateModifiedFields(issue);

        verify(textFieldCharacterLengthValidator).isTextTooLong(SHORT_TEXT);
    }

    @Test
    public void validateModifiedFieldsIgnoresUnmodifiedDescriptionLength()
    {
        when(issue.getDescription()).thenReturn(SHORT_TEXT);

        defaultIssueTextFieldCharacterLengthValidator.validateModifiedFields(issue);

        verify(textFieldCharacterLengthValidator, never()).isTextTooLong(SHORT_TEXT);
    }

    @Test
    public void validateModifiedFieldsDoesNotValidateSummaryFieldLength()
    {
        when(issue.getDescription()).thenReturn(LONG_TEXT);
        markFieldAsModified(IssueFieldConstants.SUMMARY);

        assertThat(defaultIssueTextFieldCharacterLengthValidator.validateModifiedFields(issue).isValid(), is(true));

        verify(textFieldCharacterLengthValidator, never()).isTextTooLong(LONG_TEXT);
    }

    @Test
    public void validateAllFieldsDoesNotValidateSummaryFieldLength()
    {
        when(issue.getSummary()).thenReturn(LONG_TEXT);

        assertThat(defaultIssueTextFieldCharacterLengthValidator.validateAllFields(issue).isValid(), is(true));

        verify(textFieldCharacterLengthValidator, never()).isTextTooLong(LONG_TEXT);
    }

    @Test
    public void validateAllFieldChecksEnvironmentLength()
    {
        when(issue.getEnvironment()).thenReturn(SHORT_TEXT);
        defaultIssueTextFieldCharacterLengthValidator.validateAllFields(issue);
        verify(textFieldCharacterLengthValidator).isTextTooLong(SHORT_TEXT);
    }

    @Test
    public void validateModifiedFieldsChecksModifiedEnvironmentLength()
    {
        when(issue.getEnvironment()).thenReturn(SHORT_TEXT);
        markFieldAsModified(IssueFieldConstants.ENVIRONMENT);

        defaultIssueTextFieldCharacterLengthValidator.validateModifiedFields(issue);

        verify(textFieldCharacterLengthValidator).isTextTooLong(SHORT_TEXT);
    }

    @Test
    public void validateModifiedFieldsIgnoresUnmodifiedEnvironmentLength()
    {
        when(issue.getEnvironment()).thenReturn(SHORT_TEXT);

        defaultIssueTextFieldCharacterLengthValidator.validateModifiedFields(issue);

        verify(textFieldCharacterLengthValidator, never()).isTextTooLong(SHORT_TEXT);
    }

    @Test
    public void validateAllFieldsResultReturnsCorrectMaximumNumberOfCharacters()
    {
        final long LIMIT = 32767;
        when(textFieldCharacterLengthValidator.getMaximumNumberOfCharacters()).thenReturn(LIMIT);
        assertThat(defaultIssueTextFieldCharacterLengthValidator.validateAllFields(issue).getMaximumNumberOfCharacters(), is(LIMIT));
    }

    @Test
    public void validateModifiedFieldsResultReturnsCorrectMaximumNumberOfCharacters()
    {
        final long LIMIT = 100000;
        when(textFieldCharacterLengthValidator.getMaximumNumberOfCharacters()).thenReturn(LIMIT);
        assertThat(defaultIssueTextFieldCharacterLengthValidator.validateModifiedFields(issue).getMaximumNumberOfCharacters(), is(LIMIT));
    }

    @Test
    public void validateAllFieldsResultIsValidReturnsTrueWhenValidationOk()
    {
        assertThat(defaultIssueTextFieldCharacterLengthValidator.validateAllFields(issue).isValid(), is(true));
    }

    @Test
    public void validateModifiedFieldsResultIsValidReturnsTrueWhenValidationOk()
    {
        assertThat(defaultIssueTextFieldCharacterLengthValidator.validateModifiedFields(issue).isValid(), is(true));
    }

    @Test
    public void validateAllFieldsResultIsEmptyWhenValidationOk()
    {
        assertThat(defaultIssueTextFieldCharacterLengthValidator.validateAllFields(issue).getInvalidFieldIds().size(), is(0));
    }

    @Test
    public void validateModifiedFieldsResultIsEmptyWhenValidationOk()
    {
        assertThat(defaultIssueTextFieldCharacterLengthValidator.validateModifiedFields(issue).getInvalidFieldIds().size(), is(0));
    }

    @Test
    public void validateAllFieldsReturnsValidationResultWhenDescriptionTooLong()
    {
        when(issue.getDescription()).thenReturn(LONG_TEXT);

        final IssueTextFieldCharacterLengthValidator.ValidationResult validationResult = defaultIssueTextFieldCharacterLengthValidator.validateAllFields(issue);
        assertThat(validationResult.isValid(), is(false));
        assertThat(validationResult.getInvalidFieldIds(), containsInAnyOrder(IssueFieldConstants.DESCRIPTION));
    }

    @Test
    public void validateAllFieldsReturnsValidationResultWhenEnvironmentTooLong()
    {
        when(issue.getEnvironment()).thenReturn(LONG_TEXT);

        final IssueTextFieldCharacterLengthValidator.ValidationResult validationResult = defaultIssueTextFieldCharacterLengthValidator.validateAllFields(issue);
        assertThat(validationResult.isValid(), is(false));
        assertThat(validationResult.getInvalidFieldIds(), containsInAnyOrder(IssueFieldConstants.ENVIRONMENT));
    }

    @Test
    public void validateAllFieldsReturnsValidationResultWhenTextCustomFieldTooLong()
    {
        when(issue.getCustomFieldValue(customField1)).thenReturn(LONG_TEXT);
        when(issue.getCustomFieldValue(customField2)).thenReturn(new Long(10));

        final IssueTextFieldCharacterLengthValidator.ValidationResult validationResult = defaultIssueTextFieldCharacterLengthValidator.validateAllFields(issue);
        assertThat(validationResult.isValid(), is(false));
        assertThat(validationResult.getInvalidFieldIds(), containsInAnyOrder(CF1_ID));
    }

    @Test
    public void validateAllFieldsReturnsValidationResultWhenMultipleFieldsTooLong()
    {
        when(issue.getDescription()).thenReturn(LONG_TEXT);
        when(issue.getEnvironment()).thenReturn(LONG_TEXT);
        when(issue.getCustomFieldValue(customField1)).thenReturn(LONG_TEXT);
        when(issue.getCustomFieldValue(customField2)).thenReturn(new Long(10));

        assertThat(defaultIssueTextFieldCharacterLengthValidator.validateAllFields(issue).getInvalidFieldIds(),
                containsInAnyOrder(IssueFieldConstants.DESCRIPTION, IssueFieldConstants.ENVIRONMENT, CF1_ID));
    }

    @Test
    public void validateModifiedFieldsReturnsValidationResultWhenDescriptionTooLong()
    {
        when(issue.getDescription()).thenReturn(LONG_TEXT);
        markFieldAsModified(IssueFieldConstants.DESCRIPTION);

        final IssueTextFieldCharacterLengthValidator.ValidationResult validationResult = defaultIssueTextFieldCharacterLengthValidator.validateModifiedFields(issue);
        assertThat(validationResult.isValid(), is(false));
        assertThat(validationResult.getInvalidFieldIds(), containsInAnyOrder(IssueFieldConstants.DESCRIPTION));
    }

    @Test
    public void validateModifiedFieldsReturnsValidationResultOkWhenDescriptionTooLongButUnchanged()
    {
        when(issue.getDescription()).thenReturn(LONG_TEXT);

        final IssueTextFieldCharacterLengthValidator.ValidationResult validationResult = defaultIssueTextFieldCharacterLengthValidator.validateModifiedFields(issue);
        assertThat(validationResult.isValid(), is(true));
        assertThat(validationResult.getInvalidFieldIds().size(), is(0));
    }

    @Test
    public void validateModifiedFieldsReturnsValidationResultWhenEnvironmentTooLong()
    {
        when(issue.getEnvironment()).thenReturn(LONG_TEXT);
        markFieldAsModified(IssueFieldConstants.ENVIRONMENT);

        final IssueTextFieldCharacterLengthValidator.ValidationResult validationResult = defaultIssueTextFieldCharacterLengthValidator.validateModifiedFields(issue);
        assertThat(validationResult.isValid(), is(false));
        assertThat(validationResult.getInvalidFieldIds(), containsInAnyOrder(IssueFieldConstants.ENVIRONMENT));
    }

    @Test
    public void validateModifiedFieldsReturnsValidationResultOkWhenEnvironmentTooLongButUnchanged()
    {
        when(issue.getEnvironment()).thenReturn(LONG_TEXT);

        final IssueTextFieldCharacterLengthValidator.ValidationResult validationResult = defaultIssueTextFieldCharacterLengthValidator.validateModifiedFields(issue);
        assertThat(validationResult.isValid(), is(true));
        assertThat(validationResult.getInvalidFieldIds().size(), is(0));
    }

    @Test
    public void validateModifiedFieldsReturnsValidationResultWhenTextCustomFieldTooLong()
    {
        when(issue.getCustomFieldValue(customField1)).thenReturn(LONG_TEXT);
        when(issue.getCustomFieldValue(customField2)).thenReturn(new Long(10));
        markFieldAsModified(CF1_ID);

        final IssueTextFieldCharacterLengthValidator.ValidationResult validationResult = defaultIssueTextFieldCharacterLengthValidator.validateModifiedFields(issue);
        assertThat(validationResult.isValid(), is(false));
        assertThat(validationResult.getInvalidFieldIds(), containsInAnyOrder(CF1_ID));
    }

    @Test
    public void validateModifiedFieldsReturnsValidationResultOkWhenTextCustomFieldTooLongButUnchanged()
    {
        when(issue.getCustomFieldValue(customField1)).thenReturn(LONG_TEXT);
        when(issue.getCustomFieldValue(customField2)).thenReturn(new Long(10));

        final IssueTextFieldCharacterLengthValidator.ValidationResult validationResult = defaultIssueTextFieldCharacterLengthValidator.validateModifiedFields(issue);
        assertThat(validationResult.isValid(), is(true));
        assertThat(validationResult.getInvalidFieldIds().size(), is(0));
    }

    @Test
    public void validateModifiedFieldsReturnsValidationResultWhenMultipleFieldsTooLong()
    {
        when(issue.getDescription()).thenReturn(LONG_TEXT);
        when(issue.getEnvironment()).thenReturn(LONG_TEXT);
        when(issue.getCustomFieldValue(customField1)).thenReturn(LONG_TEXT);
        when(issue.getCustomFieldValue(customField2)).thenReturn(new Long(10));
        markFieldAsModified(CF1_ID);
        markFieldAsModified(IssueFieldConstants.DESCRIPTION);
        markFieldAsModified(IssueFieldConstants.ENVIRONMENT);

        assertThat(defaultIssueTextFieldCharacterLengthValidator.validateModifiedFields(issue).getInvalidFieldIds(),
                containsInAnyOrder(IssueFieldConstants.DESCRIPTION, IssueFieldConstants.ENVIRONMENT, CF1_ID));
    }

    @Test
    public void validateModifiedFieldsReturnsValidationResultOkWhenMultipleFieldsTooLongButUnchanged()
    {
        when(issue.getCustomFieldValue(customField1)).thenReturn(LONG_TEXT);
        when(issue.getEnvironment()).thenReturn(LONG_TEXT);
        when(issue.getCustomFieldValue(customField2)).thenReturn(new Long(10));

        final IssueTextFieldCharacterLengthValidator.ValidationResult validationResult = defaultIssueTextFieldCharacterLengthValidator.validateModifiedFields(issue);
        assertThat(validationResult.isValid(), is(true));
        assertThat(validationResult.getInvalidFieldIds().size(), is(0));
    }

}
