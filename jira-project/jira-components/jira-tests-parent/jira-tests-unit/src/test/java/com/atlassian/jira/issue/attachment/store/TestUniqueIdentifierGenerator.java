package com.atlassian.jira.issue.attachment.store;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

public class TestUniqueIdentifierGenerator
{

    @Test
    public void testGeneratedIdentifierContainsClusterNodeId() throws Exception
    {
        final String clusterNodeId = "clusterNodeId";
        final UniqueIdentifierGenerator uniqueIdentifierGenerator = new UniqueIdentifierGenerator("prefix", clusterNodeId);

        assertThat(uniqueIdentifierGenerator.getNextId(), containsString(clusterNodeId));
    }

    @Test
    public void testGeneratedIdentifierContainsProvidedPrefix() throws Exception
    {
        final String prefix = "id";
        final UniqueIdentifierGenerator uniqueIdentifierGenerator = new UniqueIdentifierGenerator(prefix, "whatever");

        assertThat(uniqueIdentifierGenerator.getNextId(), containsString(prefix));
    }

    @Test
    public void testGenerateDifferentIdentifiers() throws Exception
    {
        final UniqueIdentifierGenerator uniqueIdentifierGenerator = new UniqueIdentifierGenerator("prefix", "clusterNodeId");

        final String firstId = uniqueIdentifierGenerator.getNextId();
        final String secondId = uniqueIdentifierGenerator.getNextId();

        assertThat(firstId, is(not(equalTo(secondId))));
    }
}