package com.atlassian.jira.issue.attachment;

import com.atlassian.fugue.Either;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.AnswerWith;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.Mockito;

import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ATTACHMENTS;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class TestDefaultAttachmentValidator
{
    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    @Mock
    private ApplicationUser user;

    @Mock
    private Issue issue;

    @Mock
    private Project project;

    @Mock
    private I18nHelper.BeanFactory i18nFactory;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private IssueManager issueManager;

    @Mock
    private AttachmentManager attachmentManager;

    private ErrorCollection errorCollection;

    private DefaultAttachmentValidator attachmentValidator;

    @Before
    public void setUp() throws Exception
    {
        errorCollection = new SimpleErrorCollection();
        attachmentValidator = new DefaultAttachmentValidator(
                i18nFactory, permissionManager, issueManager, attachmentManager);

        when(i18nHelper.getText(anyString())).thenAnswer(AnswerWith.firstParameter());
        when(i18nHelper.getText(anyString(), anyString())).thenAnswer(AnswerWith.firstParameter());

        Mockito.when(i18nFactory.getInstance(user)).thenReturn(i18nHelper);
    }

    // -- canCreateTemporaryAttachments with project
    @Test
    public void testCanCreateTemporaryAttachmentsForProjectForIssueWhenAttachmentsDisabled()
    {
        setAttachmentsEnabled(false);

        assertValidationFailed(
                attachmentValidator.canCreateTemporaryAttachments(user, Either.<Issue, Project>right(project), errorCollection),
                "attachment.service.error.attachments.disabled"
        );
    }

    @Test
    public void testCanCreateTemporaryAttachmentsForProjectWhenNoPermissionForAttachToProject()
    {
        setAttachmentsEnabled(true);
        setPermissionForProject(CREATE_ATTACHMENTS, project, user, false);

        assertValidationFailed(
                attachmentValidator.canCreateTemporaryAttachments(user, Either.<Issue, Project>right(project), errorCollection),
                "attachment.service.error.create.no.permission.project"
        );
    }
    @Test
    public void testCanCreateTemporaryAttachmentsForProjectHappyPath()
    {
        setAttachmentsEnabled(true);
        setPermissionForProject(CREATE_ATTACHMENTS, project, user, true);

        assertValidationSucceded(
                attachmentValidator.canCreateTemporaryAttachments(user, Either.<Issue, Project>right(project), errorCollection)
        );
    }

    // -- canCreateTemporaryAttachments with issue
    @Test
    public void testCanCreateTemporaryAttachmentsForIssueWhenAttachmentsDisabled()
    {
        setAttachmentsEnabled(false);

        assertValidationFailed(
                attachmentValidator.canCreateTemporaryAttachments(user, Either.<Issue, Project>left(issue), errorCollection),
                "attachment.service.error.attachments.disabled"
        );
    }

    @Test
    public void testCanCreateTemporaryAttachmentsForIssueWithoutCreateAttachmentPermission()
    {
        setAttachmentsEnabled(true);
        setPermissionForIssue(CREATE_ATTACHMENTS, issue, user, false);

        assertValidationFailed(
                attachmentValidator.canCreateTemporaryAttachments(user, Either.<Issue, Project>left(issue), errorCollection),
                "attachment.service.error.create.no.permission"
        );
    }

    @Test
    public void testCanCreateTemporaryAttachmentsForIssueHappyPath()
    {
        setAttachmentsEnabled(true);
        setPermissionForIssue(CREATE_ATTACHMENTS, issue, user, true);

        assertValidationSucceded(
                attachmentValidator.canCreateTemporaryAttachments(user, Either.<Issue, Project>left(issue), errorCollection)
        );
    }

    // -- canCreateAttachments
    @Test
    public void testCanCreateAttachmentsWhenAttachmentsAreDisabled()
    {
        setAttachmentsEnabled(false);

        assertValidationFailed(
                attachmentValidator.canCreateAttachments(user, issue, errorCollection),
                "attachment.service.error.attachments.disabled"
        );
    }

    @Test
    public void testCanCreateAttachmentsWithoutCreateAttachmentPermission()
    {
        setAttachmentsEnabled(true);
        setPermissionForIssue(CREATE_ATTACHMENTS, issue, user, false);

        assertValidationFailed(
                attachmentValidator.canCreateAttachments(user, issue, errorCollection),
                "attachment.service.error.create.no.permission"
        );
    }

    @Test
    public void testCanCreateAttachmentsWhenIssueIsNotEditable()
    {
        setAttachmentsEnabled(true);
        setPermissionForIssue(CREATE_ATTACHMENTS, issue, user, true);
        setIssueEditable(issue, false);

        assertValidationFailed(
                attachmentValidator.canCreateAttachments(user, issue, errorCollection),
                "attachment.service.error.create.issue.non.editable"
        );
    }

    @Test
    public void testCanCreateAttachmentsHappyPath()
    {
        setAttachmentsEnabled(true);
        setPermissionForIssue(CREATE_ATTACHMENTS, issue, user, true);
        setIssueEditable(issue, true);

        assertValidationSucceded(
                attachmentValidator.canCreateAttachments(user, issue, errorCollection)
        );
    }

    @Test
    public void attachmentCreationShouldPassDespiteExistingErrors()
    {
        setAttachmentsEnabled(true);
        setPermissionForIssue(CREATE_ATTACHMENTS, issue, user, true);
        setIssueEditable(issue, true);
        final String existingErrorMessage = "Test error message";
        errorCollection.addErrorMessage(existingErrorMessage);

        assertTrue("Validation should pass", attachmentValidator.canCreateAttachments(user, issue, errorCollection));

        // error should still be in error collection
        verifyErrorMessage(existingErrorMessage);
    }

    private void assertValidationFailed(final boolean validationResult, final String expectedError)
    {
        assertFalse("Validation should fail", validationResult);
        verifyErrorMessage(expectedError);
    }

    private void assertValidationSucceded(final boolean validationResult)
    {
        assertTrue("Validation should pass", validationResult);
        assertFalse("Error collection should be empty", errorCollection.hasAnyErrors());
    }

    private void verifyErrorMessage(final String message)
    {
        assertThat(errorCollection.getErrorMessages(), contains(message));
    }

    private void setIssueEditable(final Issue issue, final boolean editable)
    {
        when(issueManager.isEditable(issue)).thenReturn(editable);
    }

    private void setPermissionForIssue(final ProjectPermissionKey permissionKey, final Issue issue, final ApplicationUser user, final boolean hasPermission)
    {
        when(permissionManager.hasPermission(eq(permissionKey), eq(issue), eq(user))).thenReturn(hasPermission);
    }

    private void setPermissionForProject(final ProjectPermissionKey permissionKey, final Project project, final ApplicationUser user, final boolean hasPermission)
    {
        when(permissionManager.hasPermission(eq(permissionKey), eq(project), eq(user))).thenReturn(hasPermission);
    }

    private void setAttachmentsEnabled(final boolean enabled)
    {
        when(attachmentManager.attachmentsEnabled()).thenReturn(enabled);
    }
}