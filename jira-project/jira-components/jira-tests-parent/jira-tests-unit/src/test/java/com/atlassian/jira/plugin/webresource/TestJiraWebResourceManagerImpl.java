package com.atlassian.jira.plugin.webresource;

import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestJiraWebResourceManagerImpl
{

    private JiraWebResourceManagerImpl webResourceManager;
    private WebResourceIntegration mockWebResourceIntegration;

    @Before
    public void setUp() throws Exception
    {
        mockWebResourceIntegration = mock(WebResourceIntegration.class);
        Map<String, Object> cache = spy(Maps.<String, Object>newHashMap());
        when(mockWebResourceIntegration.getRequestCache()).thenReturn(cache);

        PluginResourceLocator pluginResourceLocator = mock(PluginResourceLocator.class);
        WebResourceUrlProvider webResourceUrlProvider = mock(WebResourceUrlProvider.class);
        ResourceBatchingConfiguration resourceBatchingConfiguration = mock(ResourceBatchingConfiguration.class);
        webResourceManager = new JiraWebResourceManagerImpl(pluginResourceLocator, mockWebResourceIntegration, webResourceUrlProvider, resourceBatchingConfiguration);
    }

    @Test
    public void putMetadataShouldUseASpecificCacheKey()
    {
        webResourceManager.putMetadata("key", "value");
        verify(mockWebResourceIntegration.getRequestCache()).put(eq("jira.metadata.map"), any(Map.class));
    }

    @Test
    public void getMetadataShouldUseASpecificCacheKey()
    {
        Map<String, String> metadata = MapBuilder.singletonMap("key", "value");
        mockWebResourceIntegration.getRequestCache().put("jira.metadata.map", metadata);
        assertEquals(metadata, webResourceManager.getMetadata());
    }

    @Test
    public void getMetadataShouldDrainPutMetadataAndFallbackToReturningAnEmptyMap()
    {
        webResourceManager.putMetadata("key", "value");
        assertEquals(MapBuilder.singletonMap("key", "value"), webResourceManager.getMetadata());
        assertTrue(webResourceManager.getMetadata().isEmpty());

        webResourceManager.putMetadata("key2", "value2");
        assertEquals(MapBuilder.singletonMap("key2", "value2"), webResourceManager.getMetadata());
        assertTrue(webResourceManager.getMetadata().isEmpty());
    }

    @Test
    public void getMetadataCanBeUsedBeforePutMetadata()
    {
        assertTrue(webResourceManager.getMetadata().isEmpty());
    }
}
