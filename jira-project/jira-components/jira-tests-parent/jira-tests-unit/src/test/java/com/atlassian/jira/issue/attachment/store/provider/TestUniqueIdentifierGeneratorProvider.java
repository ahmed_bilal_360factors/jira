package com.atlassian.jira.issue.attachment.store.provider;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.issue.attachment.store.UniqueIdentifierGenerator;
import com.atlassian.jira.junit.rules.InitMockitoMocks;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class TestUniqueIdentifierGeneratorProvider
{
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private ClusterManager clusterManager;

    private final UniqueIdentifierGeneratorProvider identifierGeneratorProvider = new UniqueIdentifierGeneratorProvider();

    @Test
    public void shouldUseClusterNodeIdProvidedByManager() throws Exception
    {
        final String nodeId = "clusterNodeId";
        when(clusterManager.getNodeId())
                .thenReturn(nodeId);

        final UniqueIdentifierGenerator identifierGenerator = identifierGeneratorProvider.provide(clusterManager);

        assertThat(identifierGenerator.getNextId(), containsString(nodeId));
    }

    @Test
    public void shouldUseDefaultNonClusterIdWhenNotInClusteredEnvironment() throws Exception
    {
        final UniqueIdentifierGenerator identifierGenerator = identifierGeneratorProvider.provide(clusterManager);

        assertThat(identifierGenerator.getNextId(), containsString(UniqueIdentifierGeneratorProvider.NON_CLUSTER_NODE_ID));
    }
}