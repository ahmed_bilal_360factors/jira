package com.atlassian.jira.issue.fields.rest;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.rest.FieldHtmlFactoryImpl.FieldRenderItemWithTab;
import com.atlassian.jira.issue.fields.rest.json.beans.FieldHtmlBean;
import com.atlassian.jira.issue.fields.rest.json.beans.FieldTab;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.operation.ScreenableIssueOperation;
import com.atlassian.jira.issue.transport.CollectionParams;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.util.I18nHelper;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import webwork.action.Action;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class TestFieldHtmlFactoryImpl
{
    @Mock
    private I18nHelper.BeanFactory beanFactory;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private FieldManager fieldManager;

    @Mock
    private FieldScreenRendererFactory fieldScreenRendererFactory;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private OperationContext operationContext;

    @Mock
    private Map<String, Object> fieldValuesHolder;

    @Before
    public void setUp()
    {
        when(beanFactory.getInstance(any(User.class))).thenReturn(i18nHelper);
        when(i18nHelper.getText(any(String.class))).thenAnswer(new Answer<String>()
        {
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable
            {
                return invocation.getArguments()[0].toString();
            }
        });
        when(operationContext.getFieldValuesHolder()).thenReturn(fieldValuesHolder);
    }

    @Test
    public void testGetInlineEditFieldsNonEmptyCustomField()
    {
        final FieldRenderItemWithTab fieldRenderWithTab = mock(FieldRenderItemWithTab.class);
        when(fieldRenderWithTab.getEditHtml(any(Action.class), any(OperationContext.class), any(Issue.class), any(Map.class))).thenReturn("someHtml");
        when(fieldRenderWithTab.getId()).thenReturn("someId");
        when(fieldRenderWithTab.getNameKey()).thenReturn("someNameKey");

        final FieldTab fieldTab = mock(FieldTab.class);
        when(fieldRenderWithTab.getFieldTab()).thenReturn(fieldTab);

        final CustomFieldParams customFieldParams = mock(CustomFieldParams.class);
        when(fieldValuesHolder.get(Mockito.eq("someId"))).thenReturn(customFieldParams);
        when(customFieldParams.getAllValues()).thenReturn(Collections.singletonList("someValue"));

        final CustomField field = mock(CustomField.class);
        when(field.getValue(null)).thenReturn("value");
        when(fieldRenderWithTab.getOrderableField()).thenReturn(field);

        final FieldHtmlFactoryImpl fieldHtmlFactory = createFieldHtmlFactoryImplForEditTest(Lists.newArrayList(fieldRenderWithTab));
        final List<FieldHtmlBean> editFields = fieldHtmlFactory.getInlineEditFields(null, operationContext, null, null, false);

        assertEquals(1, editFields.size());
        final FieldHtmlBean fieldHtmlBean = editFields.get(0);
        assertEquals("someId", fieldHtmlBean.getId());
        assertEquals("someHtml", fieldHtmlBean.getEditHtml());
        assertEquals("someNameKey", fieldHtmlBean.getLabel());
        assertEquals(fieldTab, fieldHtmlBean.getTab());
    }

    @Test
    public void testGetInlineEditFieldsEmptyPropertyField()
    {
        final FieldRenderItemWithTab fieldRenderWithTab = mock(FieldRenderItemWithTab.class);
        when(fieldRenderWithTab.getEditHtml(any(Action.class), any(OperationContext.class), any(Issue.class), any(Map.class))).thenReturn("someHtml");
        when(fieldRenderWithTab.getId()).thenReturn("someId");
        when(fieldRenderWithTab.getNameKey()).thenReturn("someNameKey");

        final FieldTab fieldTab = mock(FieldTab.class);
        when(fieldRenderWithTab.getFieldTab()).thenReturn(fieldTab);

        final CollectionParams collectionParams = mock(CollectionParams.class);
        when(fieldValuesHolder.get(Mockito.eq("someId"))).thenReturn(collectionParams);
        when(collectionParams.getAllValues()).thenReturn(Collections.emptyList());

        final OrderableField field = mock(OrderableField.class);
        when(fieldRenderWithTab.getOrderableField()).thenReturn(field);

        final FieldHtmlFactoryImpl fieldHtmlFactory = createFieldHtmlFactoryImplForEditTest(Lists.newArrayList(fieldRenderWithTab));
        final List<FieldHtmlBean> editFields = fieldHtmlFactory.getInlineEditFields(null, operationContext, null, null, false);

        assertEquals(1, editFields.size());
        final FieldHtmlBean fieldHtmlBean = editFields.get(0);
        assertEquals("someId", fieldHtmlBean.getId());
        assertEquals("someHtml", fieldHtmlBean.getEditHtml());
        assertEquals("someNameKey", fieldHtmlBean.getLabel());
        assertEquals(fieldTab, fieldHtmlBean.getTab());
    }

    @Test
    public void testGetInlineEditFieldsNonEmptyCustomFieldWithEmptyHtml()
    {
        final FieldRenderItemWithTab fieldRenderWithTab = mock(FieldRenderItemWithTab.class);
        when(fieldRenderWithTab.getEditHtml(any(Action.class), any(OperationContext.class), any(Issue.class), any(Map.class))).thenReturn("\n");
        when(fieldRenderWithTab.getId()).thenReturn("someId");
        when(fieldRenderWithTab.getNameKey()).thenReturn("someNameKey");

        final FieldTab fieldTab = mock(FieldTab.class);
        when(fieldRenderWithTab.getFieldTab()).thenReturn(fieldTab);

        final CustomFieldParams customFieldParams = mock(CustomFieldParams.class);
        when(fieldValuesHolder.get(Mockito.eq("someId"))).thenReturn(customFieldParams);
        when(customFieldParams.getAllValues()).thenReturn(Collections.singletonList("someValue"));

        final CustomField field = mock(CustomField.class);
        when(field.getValue(null)).thenReturn("value");
        when(fieldRenderWithTab.getOrderableField()).thenReturn(field);

        final FieldHtmlFactoryImpl fieldHtmlFactory = createFieldHtmlFactoryImplForEditTest(Lists.newArrayList(fieldRenderWithTab));
        final List<FieldHtmlBean> editFields = fieldHtmlFactory.getInlineEditFields(null, operationContext, null, null, false);

        assertEquals(0, editFields.size());
    }

    @Test
    public void testGetInlineEditFieldsEmptyCustomField()
    {
        final FieldRenderItemWithTab fieldRenderWithTab = mock(FieldRenderItemWithTab.class);
        when(fieldRenderWithTab.getEditHtml(any(Action.class), any(OperationContext.class), any(Issue.class), any(Map.class))).thenReturn("someHtml");
        when(fieldRenderWithTab.getId()).thenReturn("someId");
        when(fieldRenderWithTab.getNameKey()).thenReturn("someNameKey");

        final FieldTab fieldTab = mock(FieldTab.class);
        when(fieldRenderWithTab.getFieldTab()).thenReturn(fieldTab);

        final CustomFieldParams customFieldParams = mock(CustomFieldParams.class);
        when(fieldValuesHolder.get(Mockito.eq("someId"))).thenReturn(customFieldParams);
        when(customFieldParams.getAllValues()).thenReturn(Collections.emptyList());

        final CustomField field = mock(CustomField.class);
        when(field.getValue(null)).thenReturn(null);
        when(fieldRenderWithTab.getOrderableField()).thenReturn(field);

        final FieldHtmlFactoryImpl fieldHtmlFactory = createFieldHtmlFactoryImplForEditTest(Lists.newArrayList(fieldRenderWithTab));
        final List<FieldHtmlBean> editFields = fieldHtmlFactory.getInlineEditFields(null, operationContext, null, null, false);

        assertEquals(0, editFields.size());
    }

    @Test
    public void testGetInlineEditFieldsCustomFieldWithEmptyResult()
    {
        final FieldRenderItemWithTab fieldRenderWithTab = mock(FieldRenderItemWithTab.class);
        when(fieldRenderWithTab.getEditHtml(any(Action.class), any(OperationContext.class), any(Issue.class), any(Map.class))).thenReturn("someHtml");
        when(fieldRenderWithTab.getId()).thenReturn("someId");
        when(fieldRenderWithTab.getNameKey()).thenReturn("someNameKey");

        final FieldTab fieldTab = mock(FieldTab.class);
        when(fieldRenderWithTab.getFieldTab()).thenReturn(fieldTab);

        final CustomFieldParams customFieldParams = mock(CustomFieldParams.class);
        when(fieldValuesHolder.get(Mockito.eq("someId"))).thenReturn(customFieldParams);
        when(customFieldParams.getAllValues()).thenReturn(Collections.emptyList());

        final CustomField field = mock(CustomField.class);
        when(field.getValue(null)).thenReturn(Collections.emptyList());
        when(fieldRenderWithTab.getOrderableField()).thenReturn(field);

        final FieldHtmlFactoryImpl fieldHtmlFactory = createFieldHtmlFactoryImplForEditTest(Lists.newArrayList(fieldRenderWithTab));
        final List<FieldHtmlBean> editFields = fieldHtmlFactory.getInlineEditFields(null, operationContext, null, null, false);

        assertEquals(1, editFields.size());
    }

    private FieldHtmlFactoryImpl createFieldHtmlFactoryImplForEditTest(final List<FieldRenderItemWithTab> fieldRendersWithTabs)
    {
        return new FieldHtmlFactoryImpl(beanFactory, fieldManager, fieldScreenRendererFactory, permissionManager)
        {
            @Override
            List<FieldRenderItemWithTab> getPopulatedRenderableItems(final Issue issue, final Boolean retainValues, final Map<String, Object> fieldValuesHolder, final ScreenableIssueOperation operation)
            {
                return fieldRendersWithTabs;
            }
        };
    }
}