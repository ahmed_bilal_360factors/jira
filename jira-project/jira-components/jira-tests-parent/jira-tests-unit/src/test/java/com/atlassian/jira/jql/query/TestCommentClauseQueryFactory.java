package com.atlassian.jira.jql.query;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.issue.search.util.LuceneQueryModifier;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operator.Operator;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static com.atlassian.query.operator.Operator.GREATER_THAN;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestCommentClauseQueryFactory
{
    @Rule
    public RuleChain mocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    private QueryCreationContext queryCreationContext;
    @Mock
    private IssueIdFilterQueryFactory issueIdFilterQueryFactory;
    @Mock
    private QueryProjectRoleAndGroupPermissionsDecorator queryPermissionsDecorator;
    @Mock
    private JqlOperandResolver operandResolver = MockJqlOperandResolver.createSimpleSupport();
    @Mock
    private LuceneQueryModifier luceneQueryModifier;

    private CommentClauseQueryFactory commentClauseQueryFactory;

    @Before
    public void setUp() throws Exception
    {
        when(queryCreationContext.isSecurityOverriden()).thenReturn(false);
        commentClauseQueryFactory = new CommentClauseQueryFactory(issueIdFilterQueryFactory, queryPermissionsDecorator, luceneQueryModifier, operandResolver);
    }

    @Test
    public void testValidateClauseOperators() throws Exception
    {
        assertOperatorValidity(false, Operator.GREATER_THAN);
        assertOperatorValidity(false, Operator.GREATER_THAN_EQUALS);
        assertOperatorValidity(false, Operator.LESS_THAN);
        assertOperatorValidity(false, Operator.LESS_THAN_EQUALS);
        assertOperatorValidity(false, Operator.EQUALS);
        assertOperatorValidity(false, Operator.NOT_EQUALS);
        assertOperatorValidity(false, Operator.IN);
    }

    @Test
    public void testValidateClause() throws Exception
    {
        assertOperatorValidity(true, Operator.LIKE);
        assertOperatorValidity(true, Operator.NOT_LIKE);
    }

    @Test
    public void testGetQuery() throws Exception
    {
        final String clauseName = SystemSearchConstants.forComments().getJqlClauseNames().getPrimaryName();
        final TerminalClauseImpl terminalClause = new TerminalClauseImpl(clauseName, Operator.LIKE, "test");
        final TermQuery modifiedQuery = new TermQuery(new Term(clauseName, "test"));
        final TermQuery queryWithPermissions = new TermQuery(new Term("field", "value"));
        final TermQuery issueIdFilterQuery = new TermQuery(new Term("issue_id", "5"));

        when(operandResolver.isValidOperand(any(Operand.class))).thenReturn(false);
        when(luceneQueryModifier.getModifiedQuery(any(Query.class))).thenReturn(modifiedQuery);
        when(queryPermissionsDecorator.appendPermissionFilterQuery(eq(modifiedQuery), any(QueryCreationContext.class), eq(DocumentConstants.COMMENT_LEVEL), eq(DocumentConstants.COMMENT_LEVEL_ROLE)))
                .thenReturn(queryWithPermissions);
        when(issueIdFilterQueryFactory.createIssueIdFilterQuery(eq(queryWithPermissions), anyString())).thenReturn(issueIdFilterQuery);

        QueryFactoryResult expectedResult = new QueryFactoryResult(issueIdFilterQuery);

        final QueryFactoryResult result = commentClauseQueryFactory.getQuery(queryCreationContext, terminalClause);
        assertThat(result, equalTo(expectedResult));

        verify(luceneQueryModifier).getModifiedQuery(any(Query.class));
        verify(queryPermissionsDecorator).appendPermissionFilterQuery(any(Query.class), any(QueryCreationContext.class), anyString(), anyString());
        verify(issueIdFilterQueryFactory).createIssueIdFilterQuery(any(Query.class), anyString());
    }

    @Test
    public void testGetQueryInvalidClause() throws Exception
    {
        final QueryFactoryResult result = commentClauseQueryFactory.getQuery(queryCreationContext, new TerminalClauseImpl(IssueFieldConstants.COMMENT, GREATER_THAN, "test"));
        assertEquals(QueryFactoryResult.createFalseResult(), result);
    }

    private void assertOperatorValidity(boolean expected, Operator operator)
    {
        final boolean result = commentClauseQueryFactory.isClauseValid(new TerminalClauseImpl(IssueFieldConstants.COMMENT, operator, "test"));
        assertThat("Validity of " + operator.name(), result, is(expected));
    }

}
