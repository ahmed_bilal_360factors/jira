package com.atlassian.jira.index.request;

import java.util.Date;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.core.util.Clock;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.Node;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.mock.security.MockAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.sharing.index.SharedEntityIndexManager;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.index.IndexLifecycleManager;
import com.atlassian.jira.web.bean.MockI18nBean;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.when;
import static com.atlassian.jira.util.AnswerWith.firstParameter;

@RunWith(MockitoJUnitRunner.class)
public class TestReindexRequestManager
{
    private static final String NODE_ID = "node1";
    private static final String OTHER_NODE_ID = "node2";

    @Mock
    private ReindexRequestDao reindexRequestDao;

    @Mock
    private Clock clock;

    @Mock
    private ReindexRequestCoalescer coalescer;

    @Mock
    private ClusterManager clusterManager;

    private Date now;

    private ReindexRequestManager manager;
    @Mock
    private ClusterLockService clusterLockService;
    @Mock
    private TaskManager taskDescriptorHelper;
    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private IssueManager issueManager;

    @Mock
    private IssueIndexManager issueIndexManager;

    @Mock
    private SharedEntityIndexManager sharedEntityIndexManager;

    @Mock
    private IndexLifecycleManager indexLifecycleManager;

    @Mock
    private TaskManager taskManager;

    @Mock
    private SearchProvider searchProvider;

    @Mock
    private SearchProviderFactory searchProviderFactory;

    @Mock
    private SearchService searchService;

    @Mock
    private PermissionManager permissionManager;

    private I18nHelper.BeanFactory i18nFactory = new MockI18nBean.MockI18nBeanFactory();

    @Before
    public void setUp()
    {
        when(clusterManager.getNodeId()).thenReturn(NODE_ID);
        when(clusterManager.findLiveNodes()).thenReturn(ImmutableList.of(new Node(NODE_ID, Node.NodeState.ACTIVE)));
        when(clusterManager.isClustered()).thenReturn(true);
        when(coalescer.coalesce(anyListOf(ReindexRequest.class))).thenAnswer(firstParameter());
        manager = new DefaultReindexRequestManager(reindexRequestDao, clock, coalescer, clusterManager, clusterLockService, taskDescriptorHelper, indexLifecycleManager, i18nFactory, i18nHelper);
        now = new Date();
        when(clock.getCurrentDate()).thenReturn(now);
    }

    @Test
    public void testRequestReindex()
    {
        when(reindexRequestDao.writeRequest(any(ReindexRequestBase.class)))
                .thenAnswer(firstParameter());
        when(reindexRequestDao.writeRequest(any(ReindexRequestBase.class)))
                .thenAnswer(new Answer<ReindexRequestBase>()
                {
                    @Override
                    public ReindexRequestBase answer(InvocationOnMock invocation) throws Throwable
                    {
                        ReindexRequestBase req = (ReindexRequestBase)invocation.getArguments()[0];
                        return new ReindexRequestBase(1L, req.getType(), req.getRequestTime(), req.getStartTime(), req.getCompletionTime(), req.getExecutionNodeId(), req.getStatus());
                    }
                });


        ReindexRequest request =
                manager.requestReindex(ReindexRequestType.IMMEDIATE, EnumSet.noneOf(AffectedIndex.class), EnumSet.noneOf(SharedEntityType.class));

        assertEquals("Wrong request type.", ReindexRequestType.IMMEDIATE, request.getType());
        assertEquals("Wrong ID.", Long.valueOf(1L), request.getId());
        assertEquals("Wrong request time.", now.getTime(), request.getRequestTime());
        assertEquals("Wrong status.", ReindexStatus.PENDING, request.getStatus());
        assertEquals("Wrong affected indexes.", EnumSet.noneOf(AffectedIndex.class), request.getAffectedIndexes());
        assertEquals("Wrong shared entities.", EnumSet.noneOf(SharedEntityType.class), request.getSharedEntities());
    }

    @Test
    public void testTransitionToRunning()
    {
        ReindexRequest request = new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, now.getTime() - 1000L,
                null, null, null, ReindexStatus.PENDING,
                EnumSet.noneOf(AffectedIndex.class), EnumSet.noneOf(SharedEntityType.class));

        //Transition a pending request to running
        List<ReindexRequest> result = manager.transitionStatus(ImmutableList.of(request), ReindexStatus.RUNNING);
        assertThat(result, hasSize(1));
        ReindexRequest updatedRequest = result.get(0);

        assertEquals("Wrong status.", ReindexStatus.RUNNING, updatedRequest.getStatus());
        assertEquals("Wrong start time.", Long.valueOf(now.getTime()), updatedRequest.getStartTime());
        assertNull("Wrong completion time.", updatedRequest.getCompletionTime());
    }

    @Test
    public void testTransitionToComplete()
    {
        ReindexRequest request = new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, now.getTime() - 1000L,
                now.getTime() - 500L, null, clusterManager.getNodeId(), ReindexStatus.RUNNING,
                EnumSet.noneOf(AffectedIndex.class), EnumSet.noneOf(SharedEntityType.class));

        //Transition a pending request to complete
        List<ReindexRequest> result = manager.transitionStatus(ImmutableList.of(request), ReindexStatus.COMPLETE);
        assertThat(result, hasSize(1));
        ReindexRequest updatedRequest = result.get(0);

        assertEquals("Wrong status.", ReindexStatus.COMPLETE, updatedRequest.getStatus());
        assertEquals("Wrong start time.", Long.valueOf(now.getTime() - 500L), updatedRequest.getStartTime());
        assertEquals("Wrong completion time.", Long.valueOf(now.getTime()), updatedRequest.getCompletionTime());
    }

    @Test
    public void testIsReindexInProgressNothingRunning()
    {
        assertFalse("Should not have reindex in progress.", manager.isReindexInProgress());
    }

    @Test
    public void testIsReindexInProgressSomethingRunning()
    {
        when(reindexRequestDao.getRequestsWithStatus(ReindexStatus.RUNNING)).thenReturn(
                ImmutableList.of(new ReindexRequestBase(1L, ReindexRequestType.IMMEDIATE, now.getTime(), now.getTime(), null, NODE_ID, ReindexStatus.RUNNING))
        );
        assertTrue("Should have reindex in progress.", manager.isReindexInProgress());
    }

    @Test
    public void testGetPendingRequests()
    {
        when(reindexRequestDao.getRequestsWithStatus(ReindexStatus.PENDING)).thenReturn(
                ImmutableList.of(new ReindexRequestBase(1L, ReindexRequestType.IMMEDIATE, now.getTime(), null, null, null, ReindexStatus.PENDING))
        );
        Iterable<ReindexRequest> requests = manager.getPendingReindexRequests(EnumSet.of(ReindexRequestType.IMMEDIATE));
        Iterator<ReindexRequest> iterator = requests.iterator();
        assertThat(iterator.hasNext(), is(true));
        ReindexRequest request = iterator.next();
        assertEquals("Wrong ID.", Long.valueOf(1L), request.getId());
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void testGetReindexProgress()
    {
        when(reindexRequestDao.findRequestById(1L)).thenReturn(
                new ReindexRequestBase(1L, ReindexRequestType.IMMEDIATE, now.getTime(), now.getTime(), null, NODE_ID, ReindexStatus.RUNNING));

        Set<ReindexRequest> requests = manager.getReindexProgress(ImmutableSet.of(1L));
        assertThat(requests, hasSize(1));
        ReindexRequest request = requests.iterator().next();

        assertEquals("Wrong ID.", Long.valueOf(1L), request.getId());
        assertEquals("Wrong state.", ReindexStatus.RUNNING, request.getStatus());
    }

    @Test
    public void testGetReindexProgressNotFound()
    {
        Set<ReindexRequest> requests = manager.getReindexProgress(ImmutableSet.of(75L));
        assertThat(requests, hasSize(0));
    }

    @Test
    public void testFailRunningRequestsFromNode()
    {
        when(reindexRequestDao.getRequestsWithStatus(ReindexStatus.RUNNING)).thenReturn(
                ImmutableList.of(
                        new ReindexRequestBase(1L, ReindexRequestType.IMMEDIATE, now.getTime(), now.getTime(), null, NODE_ID, ReindexStatus.RUNNING),
                        new ReindexRequestBase(2L, ReindexRequestType.IMMEDIATE, now.getTime(), now.getTime(), null, OTHER_NODE_ID, ReindexStatus.RUNNING),
                        new ReindexRequestBase(3L, ReindexRequestType.IMMEDIATE, now.getTime(), now.getTime(), null, NODE_ID, ReindexStatus.ACTIVE)
                ));
        List<ReindexRequest> failedRequests = manager.failRunningRequestsFromNode(OTHER_NODE_ID);

        assertThat(failedRequests, hasSize(1));
        ReindexRequest transitionedRequest = failedRequests.get(0);
        assertEquals("Wrong request ID.", Long.valueOf(2L), transitionedRequest.getId());
        assertEquals("Wrong status.", ReindexStatus.FAILED, transitionedRequest.getStatus());
    }

    @Test
    public void testFailRunningRequestsFromDeadNodes()
    {
        when(reindexRequestDao.getRequestsWithStatus(ReindexStatus.RUNNING)).thenReturn(
                ImmutableList.of(
                        new ReindexRequestBase(1L, ReindexRequestType.IMMEDIATE, now.getTime(), now.getTime(), null, NODE_ID, ReindexStatus.RUNNING),
                        new ReindexRequestBase(2L, ReindexRequestType.IMMEDIATE, now.getTime(), now.getTime(), null, OTHER_NODE_ID, ReindexStatus.RUNNING),
                        new ReindexRequestBase(3L, ReindexRequestType.IMMEDIATE, now.getTime(), now.getTime(), null, NODE_ID, ReindexStatus.ACTIVE)
                ));
        List<ReindexRequest> failedRequests = manager.failRunningRequestsFromDeadNodes();

        assertThat(failedRequests, hasSize(1));
        ReindexRequest transitionedRequest = failedRequests.get(0);
        assertEquals("Wrong request ID.", Long.valueOf(2L), transitionedRequest.getId());
        assertEquals("Wrong status.", ReindexStatus.FAILED, transitionedRequest.getStatus());
    }

    @Test
    public void testClearAll()
    {
        ReindexRequest request = new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, now.getTime() - 1000L,
                null, null, null, ReindexStatus.PENDING,
                EnumSet.noneOf(AffectedIndex.class), EnumSet.noneOf(SharedEntityType.class));

        manager.clearAll();
        Iterable<ReindexRequest> result = manager.getPendingReindexRequests(EnumSet.allOf(ReindexRequestType.class));
        assertThat(result.iterator().hasNext(), is(false));
    }

}
