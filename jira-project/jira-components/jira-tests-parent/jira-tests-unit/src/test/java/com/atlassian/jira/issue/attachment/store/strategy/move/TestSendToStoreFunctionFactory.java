package com.atlassian.jira.issue.attachment.store.strategy.move;

import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.store.MockAttachmentKeys;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.util.concurrent.Function;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class TestSendToStoreFunctionFactory
{
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private ResendingAttachmentStreamCreator resendingAttachmentStreamCreator;

    @InjectMocks
    private SendToStoreFunctionFactory sendToStoreFunctionFactory;

    @Test
    public void testCreatesResendingToStoreFunction() throws Exception
    {
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();
        final StreamAttachmentStore store = mock(StreamAttachmentStore.class);

        final Function<AttachmentGetData, Unit> result = sendToStoreFunctionFactory.createFunction(attachmentKey, store);

        final ResendToStoreFunction expectedFunction = new ResendToStoreFunction(attachmentKey, store, resendingAttachmentStreamCreator);

        assertThat(result, instanceOf(ResendToStoreFunction.class));
        assertThat( (ResendToStoreFunction) result, equalTo(expectedFunction));
    }
}