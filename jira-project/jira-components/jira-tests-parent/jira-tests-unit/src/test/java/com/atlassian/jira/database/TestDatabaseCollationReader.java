package com.atlassian.jira.database;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.Datasource;
import com.atlassian.jira.ofbiz.OfBizConnectionFactory;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDatabaseCollationReader
{

    // These values are used to create the DatabaseCollationReader
    private String expectedCollation;
    private String databaseType;
    private boolean collationQueryFails;
    private DatasourceType datasourceType;

    private enum DatasourceType
    {
        JNDI, JDBC
    }

    @Before
    public void before()
    {
        expectedCollation = null;
        databaseType = null;
        collationQueryFails = false;
        datasourceType = DatasourceType.JDBC;
    }

    @Test
    public void testFindCollation() throws Exception
    {
        databaseType = "postgres72";
        expectedCollation = "C";
        DatabaseCollationReader collationReader = getDatabaseCollationReader();

        String collation = collationReader.findCollation();

        assertEquals(expectedCollation, collation);
    }

    @Test
    public void testFindCollationForHsqlReturnsNull() throws Exception
    {
        databaseType = "hsql";
        expectedCollation = null;
        DatabaseCollationReader collationReader = getDatabaseCollationReader();

        String collation = collationReader.findCollation();

        assertEquals(expectedCollation, collation);
    }

    @Test
    public void testFindCollationForUnknownDatabaseReturnsNull() throws Exception
    {
        databaseType = "unknown";
        expectedCollation = null;
        DatabaseCollationReader collationReader = getDatabaseCollationReader();

        String collation = collationReader.findCollation();

        assertEquals(expectedCollation, collation);
    }

    @Test (expected = SQLException.class)
    public void testFindCollationThrowsSqlExceptionWhenQueryFails() throws Exception
    {
        databaseType = "postgres72";
        expectedCollation = "C";
        collationQueryFails = true;
        DatabaseCollationReader collationReader = getDatabaseCollationReader();

        collationReader.findCollation();
    }

    @Test
    public void testFindCollationWorksWithJndiDatasource() throws Exception
    {
        datasourceType = DatasourceType.JNDI;
        databaseType = "postgres72";
        expectedCollation = "C";
        DatabaseCollationReader collationReader = getDatabaseCollationReader();

        String collation = collationReader.findCollation();

        assertEquals(expectedCollation, collation);
    }

    @Test
    public void testNoSqlStringSubstitution()
    {
        String[] queries = DatabaseCollationReader.getCollationQueries();

        for (String query : queries)
        {
            // Simple SQL injection check. Checks that no %s substitution shenanigans are going on
            assertThat(query, not(containsString("%")));
        }
    }

    private DatabaseCollationReader getDatabaseCollationReader()
    {
        try
        {
            ResultSet mockResult = mock(ResultSet.class);
            when(mockResult.getString(1)).thenReturn(expectedCollation);
            OfBizConnectionFactory connectionFactory = mock(OfBizConnectionFactory.class, RETURNS_DEEP_STUBS);
            when(connectionFactory.getConnection().getMetaData().getURL()).thenReturn(getJdbcUri());
            if (collationQueryFails)
            {
                when(connectionFactory.getConnection().prepareStatement(anyString()).executeQuery()).thenThrow(new SQLException());
            }
            else
            {
                when(connectionFactory.getConnection().prepareStatement(anyString()).executeQuery()).thenReturn(mockResult);
            }

            Datasource datasource = mock(Datasource.class, RETURNS_DEEP_STUBS);
            if (datasourceType == DatasourceType.JDBC)
            {
                when(datasource.getDatasource(anyString(), anyString(), anyString()).getJdbcDatasource().getDriverClassName()).thenReturn(getDriverClassName());
                when(datasource.getDatasource(anyString(), anyString(), anyString()).getJdbcDatasource().getUri()).thenReturn(getJdbcUri());
            }
            else if (datasourceType == DatasourceType.JNDI)
            {
                when(datasource.getDatasource(anyString(), anyString(), anyString()).getJdbcDatasource()).thenReturn(null);
                when(datasource.getDatasource(anyString(), anyString(), anyString()).getJdbcDatasource()).thenReturn(null);
            }
            DatabaseConfig databaseConfig = new DatabaseConfig(databaseType, "schema", datasource);

            return new DatabaseCollationReader(connectionFactory, databaseConfig);
        }
        catch (SQLException e)
        {
            // Swallow mock exceptions
            return null;
        }
    }

    private String getJdbcUri()
    {
        if (databaseType.contains("postgres"))
        {
            return "jdbc:postgresql://localhost:5432/jiradb";
        }
        else if (databaseType.contains("hsql"))
        {
            return "jdbc:hsqldb:somewhere/database/jiradb";
        }
        else
        {
            return "unknown";
        }
    }

    private String getDriverClassName()
    {
        if (databaseType.contains("postgres"))
        {
            return "org.postgresql.Driver";
        }
        else if (databaseType.contains("hsql"))
        {
            return "org.hsqldb.jdbc.JDBCDriver";
        }
        else
        {
            return "unknown";
        }
    }
}