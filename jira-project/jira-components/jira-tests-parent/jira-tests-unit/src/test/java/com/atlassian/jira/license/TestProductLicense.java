package com.atlassian.jira.license;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @since 6.4
 */
public class TestProductLicense
{
    @Test
    public void plainStringShouldStoreAndRetrieveTransparently()
    {
        String licenseString = "example";
        ProductLicense productLicense = new ProductLicense(licenseString);
        assertEquals(licenseString, productLicense.getLicenseKey());
    }

    @Test
    public void allWhiteSpaceShouldBeRemoved()
    {
        String assortedSpaces = "\u2029\u3000\u2028\u2003\u2009\u000B\u001C\u001D\u001E\u001F\t\n\f\r ";
        String key = assortedSpaces +"_a_" + assortedSpaces + "_b_" + assortedSpaces;
        ProductLicense productLicense = new ProductLicense(key);
        assertEquals("_a__b_", productLicense.getLicenseKey());
    }

    @Test(expected = NullPointerException.class)
    public void nullShouldThrowException()
    {
        //noinspection ConstantConditions
        new ProductLicense(null);
    }

    @Test
    public void emptyStringShouldYieldEmptyString()
    {
        ProductLicense productLicense = new ProductLicense("");
        assertEquals("", productLicense.getLicenseKey());
    }
}
