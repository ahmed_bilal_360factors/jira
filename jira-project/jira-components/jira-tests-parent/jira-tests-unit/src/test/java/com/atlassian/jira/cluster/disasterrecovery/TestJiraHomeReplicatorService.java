package com.atlassian.jira.cluster.disasterrecovery;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;

import com.atlassian.beehive.db.spi.ClusterLockDao;
import com.atlassian.beehive.simple.SimpleClusterLockService;
import com.atlassian.fugue.Option;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.AttachmentPathManager;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.config.util.SecondaryJiraHome;
import com.atlassian.jira.plugin.PluginPath;
import com.atlassian.jira.util.PathUtils;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;

import com.google.common.io.Files;
import com.google.common.io.OutputSupplier;

import org.apache.commons.io.FileUtils;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent.Action;
import static com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent.FileType;
import static com.atlassian.jira.matchers.FileMatchers.exists;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isA;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v6.4
 */
@RunWith (MockitoJUnitRunner.class)
public class TestJiraHomeReplicatorService
{
    @Mock
    private JiraHome jiraHome;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private SecondaryJiraHome secondaryJiraHome;

    @Mock
    private AttachmentPathManager attachmentPathManager;

    @Mock
    private AvatarManager avatarManager;

    @Mock
    private PluginPath pluginPath;

    @Mock
    private SchedulerService schedulerService;

    @Mock
    private ExecutorService executorService;

    @Mock
    private ClusterLockDao clusterLockDao;

    private JiraHomeReplicatorService service;

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    private File primaryHome;
    private File secondaryHome;

    @Before
    public void setup()
    {
        // Can't use InjectMocks, Mockito gets confused between JiraHome and SecondaryJiraHome.
        service = new JiraHomeReplicatorService(jiraHome, secondaryJiraHome, applicationProperties,
                attachmentPathManager, avatarManager, pluginPath, new SimpleClusterLockService(),
                schedulerService, executorService);

        primaryHome = temporaryFolder.newFolder("folder1");
        secondaryHome = temporaryFolder.newFolder("folder2");

        when(jiraHome.getHome()).thenReturn(primaryHome);
        when(jiraHome.getHomePath()).thenReturn(primaryHome.getAbsolutePath());
        when(secondaryJiraHome.getHome()).thenReturn(secondaryHome);
    }

    @Test
    public void onFileAdd() throws SchedulerServiceException
    {
        testBaseFor(Action.FILE_ADD, FileType.ATTACHMENT);
        testBaseFor(Action.FILE_ADD, FileType.AVATAR);
        testBaseFor(Action.FILE_ADD, FileType.INDEX_SNAPSHOT);
        testBaseFor(Action.FILE_ADD, FileType.PLUGIN);
    }


    @Test
    public void onFileRemoved() throws SchedulerServiceException
    {
        testBaseFor(Action.FILE_DELETED, FileType.ATTACHMENT);
        testBaseFor(Action.FILE_DELETED, FileType.AVATAR);
        testBaseFor(Action.FILE_DELETED, FileType.INDEX_SNAPSHOT);
        testBaseFor(Action.FILE_DELETED, FileType.PLUGIN);
    }

    @Test
    public void testGetRelativePathOf()
    {
        when(jiraHome.getHomePath()).thenReturn("/test1/");

        CopyTask copyTask = new CopyTask(null, null, null, null);

        // Testing repetition
        assertThat(copyTask.getRelativePathOf(jiraHome, new File("/test1/test2/test1/test2")).get(), is("test2/test1/test2"));

        // Testing the path in the middle
        assertThat(copyTask.getRelativePathOf(jiraHome, new File("/test3/tet2/test1/textX")), is(Option.<String>none()));

    }

    @Test
    public void testCopyTask() throws IOException
    {
        File file = new File(primaryHome, "test-file.txt");
        FileUtils.writeStringToFile(file, "Test data for transferring");

        new CopyTask(file, jiraHome, secondaryJiraHome, executorService).run();

        assertThat(Files.equal(file, new File(PathUtils.joinPaths(secondaryHome.getAbsolutePath(), file.getName()))), is(true));
    }

    @Test
    public void testCopyTaskWithSubdirectories() throws IOException
    {
        File fileInSubdirectory = new File(PathUtils.joinPaths(primaryHome.getAbsolutePath(), "subdir1", "subdir2", "test-subdir.txt"));
        FileUtils.writeStringToFile(fileInSubdirectory, "Test data for transferring into subdirectory");

        new CopyTask(fileInSubdirectory, jiraHome, secondaryJiraHome, executorService).run();

        assertThat(Files.equal(fileInSubdirectory, new File(PathUtils.joinPaths(secondaryHome.getAbsolutePath(), "subdir1", "subdir2", "test-subdir.txt"))), is(true));

    }

    /**
     * Check the safety of the copy task if two tasks happen to run at once. A cyclic barrier is used to force some
     * interleaving of the writes to the output stream.
     */
    @Test
    public void testSimultaneousCopySafety() throws IOException, InterruptedException
    {
        final File src = new File(primaryHome, "test-file.txt");
        FileUtils.writeStringToFile(src, "Test data for transferring");

        final File dest = new File(secondaryHome, "test-file.txt");
        final CyclicBarrier barrier = new CyclicBarrier(2);

        final Thread thread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    Files.copy(src, new BlockingStreamSupplier(dest, barrier, 10));
                }
                catch (IOException e)
                {
                    throw new RuntimeException(e);
                }
            }
        });
        thread.start();
        Files.copy(src, new BlockingStreamSupplier(dest, barrier, 20));
        thread.join();

        assertThat(Files.equal(src, dest), is(true));
    }

    @Test
    public void testDeleteTask() throws IOException
    {
        // Create file in primary
        File file = new File(primaryHome, "test-file.txt");
        FileUtils.writeStringToFile(file, "Test data for transferring");

        // Create file in secondary
        File file2 = new File(secondaryHome, "test-file.txt");
        FileUtils.writeStringToFile(file2 , "Test data for transferring");

        assertThat(file, exists());
        assertThat(file2, exists());

        new DeleteTask(file, jiraHome, secondaryJiraHome).run();

        assertThat(file2, not(exists()));
    }

    @Test
    public void testDeleteTaskWithSubdirectories() throws IOException
    {
        File fileInSubdirectory = new File(PathUtils.joinPaths(primaryHome.getAbsolutePath(), "subdir1", "subdir2", "test-subdir.txt"));
        FileUtils.writeStringToFile(fileInSubdirectory, "Test data for transferring into subdirectory");

        // Create file in secondary
        File file2 = new File(PathUtils.joinPaths(secondaryHome.getAbsolutePath(), "subdir1", "subdir2", "test-subdir.txt"));
        FileUtils.writeStringToFile(file2 , "Test data for transferring into subdirectory");

        assertThat(fileInSubdirectory, exists());
        assertThat(file2, exists());

        new DeleteTask(fileInSubdirectory, jiraHome, secondaryJiraHome).run();

        assertThat(file2, not(exists()));
    }

    @Test
    public void testDrainQueueTask()
    {
        reset(executorService);

        when(secondaryJiraHome.isEnabled()).thenReturn(true);
        when(applicationProperties.getOption(FileType.PLUGIN.getKey())).thenReturn(true);

        service.drainQueue();

        verify(executorService, times(0)).submit(isA(CopyTask.class));

        service.onFileChangeEvent(new JiraHomeChangeEvent(Action.FILE_ADD, FileType.PLUGIN, new File("foo")));
        service.onFileChangeEvent(new JiraHomeChangeEvent(Action.FILE_ADD, FileType.PLUGIN, new File("bar")));
        service.onFileChangeEvent(new JiraHomeChangeEvent(Action.FILE_ADD, FileType.PLUGIN, new File("baz")));

        service.drainQueue();

        verify(executorService, times(3)).submit(isA(CopyTask.class));

        service.drainQueue();

        verify(executorService, times(3)).submit(isA(CopyTask.class));
    }

    @Test
    public void testReplicateJiraHome() throws Exception
    {
        reset(executorService);

        final File attachmentDir = new File(primaryHome, "testA");
        final File avatarDir = new File(primaryHome, "testB");
        final File pluginDir = new File(primaryHome, "testC");
        final File exportDir = new File(primaryHome, "testD");
        final String attachmentSubDir = PathUtils.joinPaths(attachmentDir.getAbsolutePath(), "foo", "bar");
        final String attachmentSubDir2 = PathUtils.joinPaths(attachmentDir.getAbsolutePath(), "foo", "baz");

        final File secondaryAttachmentDir = new File(secondaryHome, "testA");
        final String secondaryAttachmentSubDir = PathUtils.joinPaths(secondaryAttachmentDir.getAbsolutePath(), "foo", "bar");

        when(jiraHome.getExportDirectory()).thenReturn(exportDir);
        when(attachmentPathManager.getAttachmentPath()).thenReturn(attachmentDir.getAbsolutePath());
        when(avatarManager.getAvatarBaseDirectory()).thenReturn(avatarDir);
        when(pluginPath.getInstalledPluginsDirectory()).thenReturn(pluginDir);

        File attachmentToAdd1 = new File(attachmentSubDir, "attachment-1");
        FileUtils.writeStringToFile(attachmentToAdd1, "Test data for transferring");

        File attachmentToAdd2 = new File(attachmentSubDir2, "attachment-2");
        FileUtils.writeStringToFile(attachmentToAdd2, "Test data for transferring");

        File attachmentToAdd3 = new File(attachmentSubDir, "attachment-3");
        FileUtils.writeStringToFile(attachmentToAdd3, "Test data for transferring");

        File attachmentToIgnore = new File(attachmentSubDir, "attachment-4");
        FileUtils.writeStringToFile(attachmentToIgnore, "Test data for transferring");

        File attachmentToIgnore2 = new File(secondaryAttachmentSubDir, "attachment-4");
        FileUtils.writeStringToFile(attachmentToIgnore2, "Test data for transferring");

        File attachmentToReplace = new File(secondaryAttachmentSubDir, "attachment-3");
        FileUtils.writeStringToFile(attachmentToReplace , "Corrupt file");

        File deletedAttachment = new File(attachmentSubDir, "attachment-5");
        File attachmentToDelete = new File(secondaryAttachmentSubDir, "attachment-5");
        FileUtils.writeStringToFile(attachmentToDelete , "Test data for transferring");

        File avatarToAdd = new File(avatarDir, "avatar");
        FileUtils.writeStringToFile(avatarToAdd , "Test data for transferring");

        File pluginToAdd = new File(pluginDir, "plugin");
        FileUtils.writeStringToFile(pluginToAdd , "Test data for transferring");

        Map<FileType, JiraHomeReplicatorService.ReplicationResult> result = service.performReplication();

        assertThat(result.get(FileType.ATTACHMENT), is(nullValue()));
        assertThat(result.get(FileType.AVATAR), is(nullValue()));
        assertThat(result.get(FileType.PLUGIN), is(nullValue()));
        assertThat(result.get(FileType.INDEX_SNAPSHOT), is(nullValue()));

        verify(executorService, never()).submit(isA(CopyTask.class));
        verify(executorService, never()).submit(isA(DeleteTask.class));

        when(applicationProperties.getOption(startsWith("jira.secondary.store."))).thenReturn(true);

        result = service.performReplication();

        assertThat(result.get(FileType.ATTACHMENT).getSourceFileCount(), is(4));
        assertThat(result.get(FileType.ATTACHMENT).getDestinationFileCount(), is(3));
        assertThat(result.get(FileType.ATTACHMENT).getCopiedFileCount(), is(3));
        assertThat(result.get(FileType.ATTACHMENT).getDeletedFileCount(), is(1));

        assertThat(result.get(FileType.AVATAR).getCopiedFileCount(), is(1));

        assertThat(result.get(FileType.PLUGIN).getCopiedFileCount(), is(1));

        ArgumentCaptor<ReplicatorTask> captor = ArgumentCaptor.forClass(ReplicatorTask.class);
        verify(executorService, times(6)).submit(captor.capture());
        final List<ReplicatorTask> capturedTasks = captor.getAllValues();
        final Matcher<Iterable<ReplicatorTask>> expectedTasks = containsInAnyOrder(
                new TaskMatcher(CopyTask.class, attachmentToAdd1),
                new TaskMatcher(CopyTask.class, attachmentToAdd2),
                new TaskMatcher(CopyTask.class, attachmentToAdd3),
                new TaskMatcher(CopyTask.class, avatarToAdd),
                new TaskMatcher(CopyTask.class, pluginToAdd),
                new TaskMatcher(DeleteTask.class, deletedAttachment));
        assertThat(capturedTasks, expectedTasks);
    }

    private static class TaskMatcher extends TypeSafeMatcher<ReplicatorTask>
    {
        private final Class clazz;
        private final File file;

        private TaskMatcher(final Class clazz, final File file)
        {
            this.clazz = clazz;
            this.file = file;
        }

        @Override
        protected boolean matchesSafely(final ReplicatorTask item)
        {
            return item.getClass() == clazz && file.equals(item.file);
        }

        @Override
        public void describeTo(final Description description)
        {
            description.appendText("a task of type ").appendValue(clazz.getSimpleName()).appendText(" and file ").appendValue(file.getName());
        }
    }

    /**
     * Test the base action with all the flags and all types of file types
     *
     * @param action the action
     * @param fileType the file type
     */
    private void testBaseFor(Action action, FileType fileType) throws SchedulerServiceException
    {
        reset(executorService);

        JiraHomeChangeEvent event = new JiraHomeChangeEvent(action, fileType, new File("blabla"));

        service.onFileChangeEvent(event);
        verify(executorService, times(0)).submit(any(Runnable.class));

        // Enabled only one flag
        when(secondaryJiraHome.isEnabled()).thenReturn(true);

        service.onFileChangeEvent(event);
        verify(executorService, times(0)).submit(any(Runnable.class));

        // Enabling both flags
        when(applicationProperties.getOption(fileType.getKey())).thenReturn(true);

        service.onFileChangeEvent(event);
        if (fileType == FileType.PLUGIN)
        {
            verify(schedulerService).scheduleJob(any(JobId.class), any(JobConfig.class));
        }
        else
        {
            verify(executorService).submit(isA(action == Action.FILE_ADD ? CopyTask.class : DeleteTask.class));
        }
    }

    private static class BlockingStreamSupplier implements OutputSupplier<OutputStream>
    {
        private final File file;
        private final CyclicBarrier barrier;
        private final int blockIndex;

        public BlockingStreamSupplier(File file, CyclicBarrier barrier, int blockIndex)
        {
            this.file = file;
            this.barrier = barrier;
            this.blockIndex = blockIndex;
        }

        @Override
        public OutputStream getOutput() throws IOException
        {
            final FileOutputStream fos = new FileOutputStream(file);
            return new OutputStream()
            {
                private int count = 0;

                @Override
                public void write(int b) throws IOException
                {
                    if (count++ == blockIndex)
                    {
                        try
                        {
                            barrier.await();
                        }
                        catch (Exception e)
                        {
                            throw new RuntimeException(e);
                        }
                    }
                    fos.write(b);
                }

                @Override
                public void close() throws IOException
                {
                    fos.close();
                }
            };
        }
    }
}
