package com.atlassian.jira.issue.attachment.store;

import java.io.File;
import java.io.InputStream;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.AttachmentStore;
import com.atlassian.jira.issue.attachment.FileSystemAttachmentStore;
import com.atlassian.jira.issue.attachment.StoreAttachmentBean;
import com.atlassian.jira.issue.attachment.StoreAttachmentResult;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestBackwardCompatibleStoreAdapter
{
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private StreamAttachmentStore streamAttachmentStore;

    @Mock
    private FileSystemAttachmentStore fileSystemAttachmentStore;


    private BackwardCompatibleStoreAdapter storeAdapter;

    @Before
    public void setUp()
    {
        storeAdapter = new BackwardCompatibleStoreAdapter(streamAttachmentStore, Option.some(fileSystemAttachmentStore));
    }


    @Test
    public void testGetFileByAttachmentKey() throws Exception
    {
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();

        final File mockFile = mock(File.class);
        when(fileSystemAttachmentStore.getAttachmentFile(attachmentKey))
                .thenReturn(mockFile);

        final File result = storeAdapter.getAttachmentFile(attachmentKey);

        assertThat(result, equalTo(mockFile));
    }

    @Test
    public void testGetFileByAttachmentAdapter() throws Exception
    {
        final File attachmentDir = mock(File.class);

        final AttachmentStore.AttachmentAdapter attachmentAdapter = mock(AttachmentStore.AttachmentAdapter.class);
        final File expectedFile = mock(File.class);
        when(fileSystemAttachmentStore.getAttachmentFile(attachmentAdapter, attachmentDir))
                .thenReturn(expectedFile);
        
        final File resultFile = storeAdapter.getAttachmentFile(attachmentAdapter, attachmentDir);

        assertThat(resultFile, equalTo(expectedFile));
    }

    @Test
    public void testGetsUnsupportedOperationWhenAccessingFilesButFileStoreIsNotAvailable() throws Exception
    {
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();

        final BackwardCompatibleStoreAdapter storeAdapter = getAdapterStoreWithoutFileSystem();

        expectedException.expect(UnsupportedOperationException.class);

        storeAdapter.getAttachmentFile(attachmentKey);
    }

    @Test
    public void testGetAttachmentData() throws Exception
    {
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();
        final Function<AttachmentGetData, String> attachmentGetDataProcessor = mock(Function.class);

        final String expectedResult = "expectedResult";
        when(streamAttachmentStore.getAttachmentData(attachmentKey, attachmentGetDataProcessor))
                .thenReturn(Promises.promise(expectedResult));

        final String result = storeAdapter.getAttachmentData(attachmentKey, attachmentGetDataProcessor).claim();

        assertThat(result, equalTo(expectedResult));
    }

    @Test
    public void testDeleteAttachmentContainerForIssueWhenFileStoreAvailable() throws Exception
    {
        final Issue issue = mock(Issue.class);

        when(fileSystemAttachmentStore.deleteAttachmentContainerForIssue(issue))
                .thenReturn(Promises.promise(Unit.VALUE));

        storeAdapter.deleteAttachmentContainerForIssue(issue);

        verify(fileSystemAttachmentStore, atLeastOnce()).deleteAttachmentContainerForIssue(issue);
    }

    @Test
    public void testIgnoreDeleteAttachmentContainerForIssueWhenFileBasedStoreNotPresent() throws Exception
    {
        final Issue issue = mock(Issue.class);
        final BackwardCompatibleStoreAdapter storeAdapter = getAdapterStoreWithoutFileSystem();

        final Unit unit = storeAdapter.deleteAttachmentContainerForIssue(issue).claim();

        assertThat(unit, equalTo(Unit.VALUE));
    }

    @Test
    public void testPutAttachment() throws Exception
    {
        final StoreAttachmentBean storeAttachmentBean = mock(StoreAttachmentBean.class);


        final Promise<StoreAttachmentResult> expectedResult = Promises.promise(StoreAttachmentResult.created());
        when(streamAttachmentStore.putAttachment(storeAttachmentBean))
                .thenReturn(expectedResult);

        final Promise<StoreAttachmentResult> resultPromise = storeAdapter.putAttachment(storeAttachmentBean);

        assertThat(resultPromise.claim(), equalTo(expectedResult.claim()));
    }

    @Test
    public void testGetAttachmentWithStreamProcessor() throws Exception
    {
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();
        final Function<InputStream, String> streamProcessor = mock(Function.class);

        final String expectedString = "expected string value";
        when(streamAttachmentStore.getAttachment(attachmentKey, streamProcessor))
                .thenReturn(Promises.promise(expectedString));

        final Promise<String> result = storeAdapter.getAttachment(attachmentKey, streamProcessor);

        assertThat(result.claim(), equalTo(expectedString));
    }

    @Test
    public void testMoveAttachment() throws Exception
    {
        final AttachmentKey oldAttachmentKey = MockAttachmentKeys.getKey();
        final AttachmentKey newAttachmentKey = MockAttachmentKeys.getKey();

        when(streamAttachmentStore.moveAttachment(oldAttachmentKey, newAttachmentKey))
                .thenReturn(Promises.promise(Unit.VALUE));

        final Promise<Unit> result = storeAdapter.moveAttachment(oldAttachmentKey, newAttachmentKey);

        assertThat(result.claim(), equalTo(Unit.VALUE));
    }

    @Test
    public void testCopyAttachment() throws Exception
    {
        final AttachmentKey oldAttachmentKey = MockAttachmentKeys.getKey(1l);
        final AttachmentKey newAttachmentKey = MockAttachmentKeys.getKey(2l);

        when(streamAttachmentStore.copyAttachment(oldAttachmentKey, newAttachmentKey))
                .thenReturn(Promises.promise(Unit.VALUE));

        final Promise<Unit> result = storeAdapter.copyAttachment(oldAttachmentKey, newAttachmentKey);

        assertThat(result.claim(), equalTo(Unit.VALUE));
    }

    @Test
    public void testDeleteAttachment() throws Exception
    {
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();

        when(streamAttachmentStore.deleteAttachment(attachmentKey))
                .thenReturn(Promises.promise(Unit.VALUE));

        final Promise<Unit> result = storeAdapter.deleteAttachment(attachmentKey);

        assertThat(result.claim(), equalTo(Unit.VALUE));
    }

    @Test
    public void testCreateTemporaryAttachment() throws Exception
    {
        final InputStream dataStream = mock(InputStream.class);
        final long size = 123l;

        final TemporaryAttachmentId temporaryFileId = TemporaryAttachmentId.fromString("temp-file-id");
        when(streamAttachmentStore.putTemporaryAttachment(dataStream, size))
                .thenReturn(Promises.promise(temporaryFileId));
        
        final Promise<TemporaryAttachmentId> result = storeAdapter.putTemporaryAttachment(dataStream, size);

        assertThat(result.claim(), equalTo(temporaryFileId));
    }

    @Test
    public void testMoveTemporaryFile() throws Exception
    {
        final TemporaryAttachmentId temporaryFileId = TemporaryAttachmentId.fromString("temp-file-id");
        final AttachmentKey destinationKey = MockAttachmentKeys.getKey();

        when(streamAttachmentStore.moveTemporaryToAttachment(temporaryFileId, destinationKey))
                .thenReturn(Promises.promise(Unit.VALUE));

        final Promise<Unit> result = storeAdapter.moveTemporaryToAttachment(temporaryFileId, destinationKey);

        assertThat(result.claim(), equalTo(Unit.VALUE));
    }

    @Test
    public void testDeleteTemporaryAttachment() throws Exception
    {
        final TemporaryAttachmentId temporaryFileId = TemporaryAttachmentId.fromString("temp-file-id");

        when(streamAttachmentStore.deleteTemporaryAttachment(temporaryFileId))
                .thenReturn(Promises.promise(Unit.VALUE));

        final Promise<Unit> result = storeAdapter.deleteTemporaryAttachment(temporaryFileId);

        assertThat(result.claim(), equalTo(Unit.VALUE));
    }

    @Test
    public void testExists() throws Exception
    {
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();

        final Boolean expectedValue = true;
        when(streamAttachmentStore.exists(attachmentKey))
                .thenReturn(Promises.promise(expectedValue));

        final Promise<Boolean> result = storeAdapter.exists(attachmentKey);

        assertThat(result.claim(), equalTo(expectedValue));
    }

    @Test
    public void testErrors() throws Exception
    {
        final ErrorCollection errorsCollection = new SimpleErrorCollection();
        errorsCollection.addError("failed", "message");

        when(streamAttachmentStore.errors())
                .thenReturn(Option.some(errorsCollection));

        final Option<ErrorCollection> result = storeAdapter.errors();

        assertThat(result.get().getErrors(), hasEntry(equalTo("failed"), equalTo("message")));
    }

    public BackwardCompatibleStoreAdapter getAdapterStoreWithoutFileSystem()
    {
        return new BackwardCompatibleStoreAdapter(streamAttachmentStore, Option.<FileSystemAttachmentStore>none());
    }
}