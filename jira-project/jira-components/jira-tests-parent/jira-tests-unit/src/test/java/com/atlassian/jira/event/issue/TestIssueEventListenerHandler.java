package com.atlassian.jira.event.issue;

import java.util.List;

import com.atlassian.event.spi.ListenerInvoker;
import com.atlassian.jira.mock.MockListenerManager;
import com.atlassian.jira.mock.event.MockIssueEventListener;

import com.google.common.collect.Lists;

import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;

/**
 * @since v4.1
 */
public class TestIssueEventListenerHandler
{
    @Test
    public void testGetsIssueEventListnerForIssueEvent() throws Exception
    {
        final IssueEventListenerHandler issueEventListenerHandler = new IssueEventListenerHandler();
        final List<? extends ListenerInvoker> invokers = issueEventListenerHandler.getInvokers(new MockIssueEventListener(new MockListenerManager()));

        // Copy to drop the bounded type
        final List<Object> copy = Lists.<Object>newArrayList(invokers);
        assertThat(copy, Matchers.contains(instanceOf(IssueEventListenerHandler.IssueEventInvoker.class)));
    }

    @Test
    public void testGetsIssueEventListenerForNonIssueEvent() throws Exception
    {
        final IssueEventListenerHandler issueEventListenerHandler = new IssueEventListenerHandler();
        final List<? extends ListenerInvoker> invokers = issueEventListenerHandler.getInvokers(new Object());
        assertThat(invokers, hasSize(0));
    }

}
