package com.atlassian.jira.issue.attachment.store.strategy.move;

import java.io.InputStream;

import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.AttachmentStreamGetData;
import com.atlassian.jira.issue.attachment.StoreAttachmentBean;
import com.atlassian.jira.issue.attachment.StoreAttachmentResult;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.store.MockAttachmentKeys;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.util.concurrent.Effect;
import com.atlassian.util.concurrent.Effects;
import com.atlassian.util.concurrent.Promises;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestResendToStoreFunction
{
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    private final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();
    @Mock
    private StreamAttachmentStore store;
    @Mock
    private ResendingAttachmentStreamCreator resendingAttachmentStreamCreator;

    private ResendToStoreFunction resendToStoreFunction;

    @Before
    public void setUp() throws Exception
    {
        resendToStoreFunction = new ResendToStoreFunction(attachmentKey, store, resendingAttachmentStreamCreator);
    }

    @Test
    public void shouldPutToSecondaryStorePreparedResendingStream() throws Exception
    {
        final long expectedSize = 1433l;
        final AttachmentGetData attachmentGetData = getAttachmentGetData(expectedSize);

        final InputStream resendingStream = mock(InputStream.class);
        when(resendingAttachmentStreamCreator.getInputStreamWithCloseHandler(attachmentGetData))
                .thenReturn(Pair.of(resendingStream, Effects.noop()));

        when(store.putAttachment(any(StoreAttachmentBean.class)))
                .thenReturn(Promises.promise(StoreAttachmentResult.created()));

        final Unit unit = resendToStoreFunction.get(attachmentGetData);

        assertThat(unit, equalTo(Unit.VALUE));
        final ArgumentCaptor<StoreAttachmentBean> storeAttachmentBeanCaptor = ArgumentCaptor.forClass(StoreAttachmentBean.class);
        verify(store).putAttachment(storeAttachmentBeanCaptor.capture());
        final StoreAttachmentBean passedStoreBean = storeAttachmentBeanCaptor.getValue();
        assertThat(passedStoreBean.getStream(), equalTo(resendingStream));
        assertThat(passedStoreBean.getSize(), equalTo((Long) expectedSize));
    }

    @Test
    public void shouldExecuteCleanupAfterFailedPut() throws Exception
    {
        final long expectedSize = 1433l;
        final AttachmentGetData attachmentGetData = getAttachmentGetData(expectedSize);

        //noinspection unchecked
        final Effect<Object> cleanupEffect = mock(Effect.class);
        when(resendingAttachmentStreamCreator.getInputStreamWithCloseHandler(attachmentGetData))
                .thenReturn(Pair.of(mock(InputStream.class), cleanupEffect));

        when(store.putAttachment(any(StoreAttachmentBean.class)))
                .thenReturn(Promises.<StoreAttachmentResult>rejected(new RuntimeException()));

        final Unit unit = resendToStoreFunction.get(attachmentGetData);

        assertThat(unit, equalTo(Unit.VALUE));
        verify(cleanupEffect).apply(anyObject());
    }

    @Test
    public void shouldExecuteCleanupAfterSuccessPut() throws Exception
    {
        final long expectedSize = 1433l;
        final AttachmentGetData attachmentGetData = getAttachmentGetData(expectedSize);

        //noinspection unchecked
        final Effect<Object> cleanupEffect = mock(Effect.class);
        when(resendingAttachmentStreamCreator.getInputStreamWithCloseHandler(attachmentGetData))
                .thenReturn(Pair.of(mock(InputStream.class), cleanupEffect));

        when(store.putAttachment(any(StoreAttachmentBean.class)))
                .thenReturn(Promises.promise(StoreAttachmentResult.created()));

        final Unit unit = resendToStoreFunction.get(attachmentGetData);

        assertThat(unit, equalTo(Unit.VALUE));
        verify(cleanupEffect).apply(anyObject());
    }

    @Test
    public void shouldExecuteCleanupWhenUnexpectedExceptionThrown() throws Exception
    {
        final long expectedSize = 1433l;
        final AttachmentGetData attachmentGetData = getAttachmentGetData(expectedSize);

        //noinspection unchecked
        final Effect<Object> cleanupEffect = mock(Effect.class);
        when(resendingAttachmentStreamCreator.getInputStreamWithCloseHandler(attachmentGetData))
                .thenReturn(Pair.of(mock(InputStream.class), cleanupEffect));

        final IllegalArgumentException exception = new IllegalArgumentException();
        when(store.putAttachment(any(StoreAttachmentBean.class)))
                .thenThrow(exception);

        catchException(resendToStoreFunction).get(attachmentGetData);

        verify(cleanupEffect).apply(anyObject());
        assertThat(caughtException(), equalTo((Exception) exception));
    }

    private AttachmentGetData getAttachmentGetData(final long expectedSize)
    {
        return new AttachmentStreamGetData(mock(InputStream.class), expectedSize);
    }
}