package com.atlassian.jira.web.action.admin.issuetypes;

import com.atlassian.jira.config.IssueTypeService;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.action.MockRedirectSanitiser;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import webwork.action.Action;
import webwork.action.ServletActionContext;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.config.IssueTypeService.IssueTypeUpdateInput;
import static com.atlassian.jira.config.IssueTypeService.UpdateValidationResult;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestEditIssueType
{
    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    @Mock
    private IssueTypeService issueTypeService;
    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private ApplicationUser applicationUser;
    @AvailableInContainer (instantiateMe = true)
    private MockRedirectSanitiser redirectSanitiser;

    private EditIssueType editIssueType;

    @Before
    public void setUp()
    {
        this.editIssueType = new EditIssueType(issueTypeService);
        when(authenticationContext.getUser()).thenReturn(applicationUser);

        String issueTypeId = "1";
        long avatarId = 1l;
        String description = "description";
        String name = "name";

        editIssueType.setDescription(description);
        editIssueType.setAvatarId(avatarId);
        editIssueType.setName(name);
        editIssueType.setId(issueTypeId);
    }

    @Test
    public void testDoExecuteWithValidInput() throws Exception
    {
        ServletActionContext.setResponse(new com.atlassian.jira.mock.servlet.MockHttpServletResponse());

        IssueTypeUpdateInput issueTypeUpdateInput = IssueTypeUpdateInput.builder()
                .setDescription(editIssueType.getDescription())
                .setAvatarId(editIssueType.getAvatarId())
                .setName(editIssueType.getName())
                .build();

        UpdateValidationResult validationResult = UpdateValidationResult.ok(issueTypeUpdateInput, mock(IssueType.class));
        when(issueTypeService.validateUpdateIssueType(eq(applicationUser), eq(editIssueType.getId()), eq(issueTypeUpdateInput)))
                .thenReturn(validationResult);

        String executeResult = editIssueType.doExecute();

        verify(issueTypeService).updateIssueType(applicationUser, validationResult);
        assertNotSame(Action.ERROR, executeResult);
    }

    @Test
    public void testErrorOnInvalidInput() throws Exception
    {
        IssueTypeUpdateInput issueTypeUpdateInput = IssueTypeUpdateInput.builder()
                .setDescription(editIssueType.getDescription())
                .setAvatarId(editIssueType.getAvatarId())
                .setName(editIssueType.getName())
                .build();

        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrorMessage("some_error", ErrorCollection.Reason.CONFLICT);
        UpdateValidationResult validationResult = UpdateValidationResult.error(errorCollection);
        when(issueTypeService.validateUpdateIssueType(eq(applicationUser), eq(editIssueType.getId()), eq(issueTypeUpdateInput)))
                .thenReturn(validationResult);

        String executeResult = editIssueType.doExecute();

        assertEquals(Action.ERROR, executeResult);
    }
}
