package com.atlassian.jira.web.action;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.onboarding.OnboardingService;
import com.atlassian.jira.plugin.myjirahome.MyJiraHomeLinker;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.login.LoginManagerImpl;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.internal.verification.Times;

import webwork.action.ServletActionContext;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(JUnit4.class)
public class TestMyJiraHome
{
    @Mock private JiraAuthenticationContext mockJiraAuthenticationContext;
    @Mock private ApplicationUser mockUser;
    @Mock private HttpServletResponse mockHttpServletResponse;
    @Mock private UserManager userManager;
    @Mock private HttpServletRequest httpRequest;
    @Mock private MyJiraHomeLinker myJiraHomeLinker;
    @Mock private OnboardingService onboardingService;

    final TestMyJiraHome self = this;

    private MyJiraHome action;

    @Before
    public void setUpMocks()
    {
        initMocks(this);
        ComponentAccessor.initialiseWorker(new MockComponentWorker()
                .addMock(JiraAuthenticationContext.class, mockJiraAuthenticationContext)
                .addMock(RedirectSanitiser.class, new MockRedirectSanitiser())
        );

        ServletActionContext.setResponse(mockHttpServletResponse);

        action = new MyJiraHome(myJiraHomeLinker, userManager, onboardingService) {
            public HttpServletRequest getHttpRequest()
            {
                return self.getHttpRequest();
            }
        };
    }

    @Test
    public void testExecuteAsAnonymousUser() throws Exception
    {
        expectAnonymousUser();

        action.doExecute();

        verify(myJiraHomeLinker, new Times(0)).getDefaultUserHome();
        verifyRedirect(MyJiraHomeLinker.DEFAULT_HOME_OD_ANON);
    }


    @Test
    public void testExecuteAsAuthenticatedUser() throws Exception
    {

        expectAuthenticatedUser();

        action.doExecute();

        verifyRedirect(MyJiraHomeLinker.DEFAULT_HOME_NOT_ANON);
    }

    @Test
    public void testExecuteAsAuthenticatedNonJiraUser() throws Exception
    {
        expectAuthenticatedNonJiraUser();

        action.doExecute();

        verify(myJiraHomeLinker).getDefaultUserHome();
        verifyRedirect(MyJiraHomeLinker.DEFAULT_HOME_NOT_ANON);
    }

    private void expectAnonymousUser()
    {
        when(mockJiraAuthenticationContext.getUser()).thenReturn(null);
        when(myJiraHomeLinker.getHomeLink((ApplicationUser)null)).thenReturn(MyJiraHomeLinker.DEFAULT_HOME_OD_ANON);
        when(httpRequest.getAttribute(LoginManagerImpl.AUTHORISED_FAILURE)).thenReturn(false);
    }

    private void expectAuthenticatedUser()
    {
        when(mockJiraAuthenticationContext.getUser()).thenReturn(mockUser);
        when(myJiraHomeLinker.getHomeLink(mockUser)).thenReturn(MyJiraHomeLinker.DEFAULT_HOME_NOT_ANON);
    }

    private void expectAuthenticatedNonJiraUser()
    {
        String testUserKey = "testUser:1234";
        when(mockJiraAuthenticationContext.getUser()).thenReturn(null);
        when(httpRequest.getAttribute(LoginManagerImpl.AUTHORISED_FAILURE)).thenReturn(true);
        when(httpRequest.getAttribute(LoginManagerImpl.AUTHORISING_USER_KEY)).thenReturn(testUserKey);
        when(userManager.getUserByKey(testUserKey)).thenReturn(mockUser);
        when(myJiraHomeLinker.getDefaultUserHome()).thenReturn(MyJiraHomeLinker.DEFAULT_HOME_NOT_ANON);
    }

    private void verifyRedirect(final String expectedUrl) throws IOException
    {
        verify(mockHttpServletResponse).sendRedirect(eq(expectedUrl));
    }

    public HttpServletRequest getHttpRequest()
    {
        return httpRequest;
    }
}
