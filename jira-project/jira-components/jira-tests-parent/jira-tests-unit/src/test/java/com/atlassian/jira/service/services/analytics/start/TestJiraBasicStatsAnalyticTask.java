package com.atlassian.jira.service.services.analytics.start;


import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentImpl;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenImpl;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenScheme;
import com.atlassian.jira.issue.fields.screen.FieldScreenSchemeImpl;
import com.atlassian.jira.issue.fields.screen.FieldScreenSchemeManager;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelImpl;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizListIterator;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionImpl;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.scheme.SchemeManagerFactory;
import com.atlassian.jira.util.collect.MapBuilder;

import com.google.common.collect.Lists;
import org.junit.Test;
import org.ofbiz.core.entity.EntityCondition;
import org.ofbiz.core.entity.EntityFindOptions;
import org.ofbiz.core.entity.GenericValue;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestJiraBasicStatsAnalyticTask
{

    @Test
    public void testAnalytic()
    {
        OfBizDelegator delegator = mock(OfBizDelegator.class);
        ConstantsManager constantsManager = mock(ConstantsManager.class);
        final VersionManager versionManager = mock(VersionManager.class);
        final ProjectComponentManager projectComponentManager = mock(ProjectComponentManager.class);
        final IssueSecurityLevelManager issueSecurityLevelManager = mock(IssueSecurityLevelManager.class);
        final FieldScreenManager fieldScreenManager = mock(FieldScreenManager.class);
        final FieldScreenSchemeManager fieldScreenSchemeManager = mock(FieldScreenSchemeManager.class);

        Collection statusCollection = mock(Collection.class);
        Collection resolutionCollection = mock(Collection.class);
        Collection prioritiesCollection = mock(Collection.class);
        OfBizListIterator iterator = mock(OfBizListIterator.class);

        // Preparing the list iterator
        when(delegator.findListIteratorByCondition(eq("PermissionSchemeCount"),
                any(EntityCondition.class), any(EntityCondition.class), anyListOf(String.class), anyListOf(String.class),
                any(EntityFindOptions.class))).thenReturn(iterator);

        List<GenericValue> genericValues = Arrays.asList((GenericValue) new MockGenericValue("PermissionScheme", MapBuilder.newBuilder("scheme", "0", "count", 10l).toMap()));
        when(iterator.iterator()).thenReturn(genericValues.iterator());

        when(statusCollection.size()).thenReturn(10);
        when(resolutionCollection.size()).thenReturn(20);
        when(prioritiesCollection.size()).thenReturn(30);

        when(delegator.getCount(Entity.Name.ISSUE)).thenReturn(10l);
        when(delegator.getCount(Entity.Name.PROJECT)).thenReturn(20l);
        when(delegator.getCount(Entity.Name.COMMENT)).thenReturn(30l);
        when(delegator.getCount(CustomField.ENTITY_TABLE_NAME)).thenReturn(3l);
        when(delegator.getCount(SchemeManagerFactory.PERMISSION_SCHEME_MANAGER)).thenReturn(1l);
        when(constantsManager.getAllIssueTypeIds()).thenReturn(Arrays.asList("1", "2", "3"));

        // Had to rever the order of the mock to avoid warnings
        doReturn(statusCollection).when(constantsManager).getStatusObjects();
        doReturn(resolutionCollection).when(constantsManager).getResolutionObjects();
        doReturn(prioritiesCollection).when(constantsManager).getPriorityObjects();

        when(versionManager.getAllVersions()).thenReturn(Lists.<Version>newArrayList(new VersionImpl(null, new MockGenericValue("Version", Collections.emptyMap()))));

        when(projectComponentManager.findAll()).thenReturn(
                Lists.<ProjectComponent>newArrayList(new ProjectComponentImpl("frontend", "some desc", "admin", 1L),
                        new ProjectComponentImpl("backend", "other desc", "admin", 1L)));
        when(issueSecurityLevelManager.getAllIssueSecurityLevels()).thenReturn(Lists.<IssueSecurityLevel>newArrayList(new IssueSecurityLevelImpl(1L,"level 1","level desc", 2L)));

        when(fieldScreenManager.getFieldScreens()).thenReturn(Lists.<FieldScreen>newArrayList(new FieldScreenImpl(null)));

        when(fieldScreenSchemeManager.getFieldScreenSchemes()).thenReturn(Lists.<FieldScreenScheme>newArrayList(new FieldScreenSchemeImpl(null)));

        JiraBasicStatsAnalyticTask task = new JiraBasicStatsAnalyticTask(delegator, constantsManager, versionManager,
                projectComponentManager, issueSecurityLevelManager, fieldScreenManager, fieldScreenSchemeManager);

        Map<String, Object> analytics = task.getAnalytics();

        assertEquals(10l, analytics.get("issues"));
        assertEquals(20l, analytics.get("projects"));
        assertEquals(30l, analytics.get("comments"));
        assertEquals(3l, analytics.get("customfields"));
        assertEquals(3, analytics.get("issuetypes"));
        assertEquals(1, analytics.get("permissionschemes"));
        assertEquals(10, analytics.get("status"));
        assertEquals(20, analytics.get("resolutions"));
        assertEquals(30, analytics.get("priorities"));
        assertEquals(1, analytics.get("versions"));
        assertEquals(2, analytics.get("components"));
        assertEquals(1, analytics.get("issuesecuritylevels"));
        assertEquals(1, analytics.get("screens"));
        assertEquals(1, analytics.get("screensschemes"));
        assertEquals(10, analytics.get("permission.scheme.avg"));

        assertEquals(15, analytics.size());
    }

}
