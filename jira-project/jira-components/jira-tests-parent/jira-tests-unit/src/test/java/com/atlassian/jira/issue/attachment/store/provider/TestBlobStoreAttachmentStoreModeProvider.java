package com.atlassian.jira.issue.attachment.store.provider;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.JiraPropertiesImpl;
import com.atlassian.jira.config.properties.PropertiesAccessor;
import com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider.Mode;

import org.junit.Test;

import static com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider.FS_ONLY_FLAG;
import static com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider.FS_PRIMARY_FLAG;
import static com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider.INITIAL_MODE_SYSPROP;
import static com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider.REMOTE_ONLY_FLAG;
import static com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider.REMOTE_PRIMARY_FLAG;
import static com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider.Mode.FS_ONLY;
import static com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider.Mode.FS_PRIMARY;
import static com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider.Mode.REMOTE_ONLY;
import static com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider.Mode.REMOTE_PRIMARY;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class TestBlobStoreAttachmentStoreModeProvider
{

    @Test
    public void testRemoteOnlyFlagHasHighestPriority() throws Exception
    {
        FeatureManager fm = featureManager(REMOTE_ONLY_FLAG, REMOTE_PRIMARY_FLAG, FS_PRIMARY_FLAG, FS_ONLY_FLAG);
        JiraProperties jp = jiraPropertiesWithInitialMode(FS_ONLY.name());
        BlobStoreAttachmentStoreModeProvider modeProvider = new BlobStoreAttachmentStoreModeProvider(fm, jp);
        assertEquals(REMOTE_ONLY, modeProvider.mode());
    }

    @Test
    public void testRemotePrimaryFlagHasHigherPriorityThanFSFlags() throws Exception
    {
        FeatureManager fm = featureManager(REMOTE_PRIMARY_FLAG, FS_PRIMARY_FLAG, FS_ONLY_FLAG);
        JiraProperties jp = jiraPropertiesWithInitialMode(FS_ONLY.name());
        BlobStoreAttachmentStoreModeProvider modeProvider = new BlobStoreAttachmentStoreModeProvider(fm, jp);
        assertEquals(REMOTE_PRIMARY, modeProvider.mode());
    }

    @Test
    public void testFSPrimaryFlagHasHigherPriorityThanFSOnlyFlag() throws Exception
    {
        FeatureManager fm = featureManager(FS_PRIMARY_FLAG, FS_ONLY_FLAG);
        JiraProperties jp = jiraPropertiesWithInitialMode(FS_ONLY.name());
        BlobStoreAttachmentStoreModeProvider modeProvider = new BlobStoreAttachmentStoreModeProvider(fm, jp);
        assertEquals(FS_PRIMARY, modeProvider.mode());
    }

    @Test
    public void testFlagOverridesInitialMode_FsOnly() throws Exception
    {
        FeatureManager fm = featureManager(FS_ONLY_FLAG);
        JiraProperties jp = jiraPropertiesWithInitialMode(REMOTE_ONLY.name());
        BlobStoreAttachmentStoreModeProvider modeProvider = new BlobStoreAttachmentStoreModeProvider(fm, jp);
        assertEquals(FS_ONLY, modeProvider.mode());
    }

    @Test
    public void testFlagOverridesInitialMode_FsPrimary() throws Exception
    {
        FeatureManager fm = featureManager(FS_PRIMARY_FLAG);
        JiraProperties jp = jiraPropertiesWithInitialMode(FS_ONLY.name());
        BlobStoreAttachmentStoreModeProvider modeProvider = new BlobStoreAttachmentStoreModeProvider(fm, jp);
        assertEquals(FS_PRIMARY, modeProvider.mode());
    }

    @Test
    public void testFlagOverridesInitialMode_RemotePrimary() throws Exception
    {
        FeatureManager fm = featureManager(REMOTE_PRIMARY_FLAG);
        JiraProperties jp = jiraPropertiesWithInitialMode(FS_ONLY.name());
        BlobStoreAttachmentStoreModeProvider modeProvider = new BlobStoreAttachmentStoreModeProvider(fm, jp);
        assertEquals(REMOTE_PRIMARY, modeProvider.mode());
    }

    @Test
    public void testFlagOverridesInitialMode_RemoteOnly() throws Exception
    {
        FeatureManager fm = featureManager(REMOTE_ONLY_FLAG);
        JiraProperties jp = jiraPropertiesWithInitialMode(FS_ONLY.name());
        BlobStoreAttachmentStoreModeProvider modeProvider = new BlobStoreAttachmentStoreModeProvider(fm, jp);
        assertEquals(REMOTE_ONLY, modeProvider.mode());
    }

    @Test
    public void testInitialModeIsHonouredIfNoFlagsAreSet() throws Exception
    {
        FeatureManager fm = featureManager();

        for (Mode initialMode : Mode.values())
        {
            JiraProperties jp = jiraPropertiesWithInitialMode(initialMode.name());
            BlobStoreAttachmentStoreModeProvider modeProvider = new BlobStoreAttachmentStoreModeProvider(fm, jp);
            assertEquals(initialMode, modeProvider.mode());
        }
    }

    @Test
    public void testInitialModeFallsBackToFsOnlyIfInvalid() throws Exception
    {
        FeatureManager fm = featureManager();
        JiraProperties jp = jiraPropertiesWithInitialMode("garbage");
        BlobStoreAttachmentStoreModeProvider modeProvider = new BlobStoreAttachmentStoreModeProvider(fm, jp);
        assertEquals(FS_ONLY, modeProvider.mode());
    }

    @Test
    public void testInitialModeFallsBackToFsOnlyIfNotSet() throws Exception
    {
        FeatureManager fm = featureManager();
        PropertiesAccessor pa = mock(PropertiesAccessor.class);
        JiraProperties jp = new JiraPropertiesImpl(pa);
        BlobStoreAttachmentStoreModeProvider modeProvider = new BlobStoreAttachmentStoreModeProvider(fm, jp);
        assertEquals(FS_ONLY, modeProvider.mode());
    }

    private FeatureManager featureManager(String... enabledFlags)
    {
        FeatureManager fm = mock(FeatureManager.class);

        for (String flag : enabledFlags)
        {
            when(fm.isEnabled(flag)).thenReturn(true);
        }

        return fm;
    }

    private JiraProperties jiraPropertiesWithInitialMode(String mode)
    {
        PropertiesAccessor pa = mock(PropertiesAccessor.class);
        when(pa.getProperty(INITIAL_MODE_SYSPROP)).thenReturn(mode);
        return new JiraPropertiesImpl(pa);
    }
}
