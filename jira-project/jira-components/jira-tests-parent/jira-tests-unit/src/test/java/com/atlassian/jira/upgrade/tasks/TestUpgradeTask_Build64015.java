package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.local.runner.ListeningMockitoRunner;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.portal.CachingPortletConfigurationStore;
import com.atlassian.jira.util.collect.MapBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;

@RunWith (ListeningMockitoRunner.class)
public class TestUpgradeTask_Build64015
{
    private MockOfBizDelegator ofBizDelegator;
    @Mock
    private CachingPortletConfigurationStore cachingPortletConfigurationStore;

    @Before
    public void setUp() throws Exception
    {
        final GenericValue gadget1 = new MockGenericValue("PortletConfiguration", MapBuilder.newBuilder().
                add("id", 10000L).
                add("portalpage", 10010L).
                add("portletId", null).
                add("columnNumber", 0).
                add("position", 0).
                add("gadgetXml", "local/filter-results.xml").
                add("color", "color0").toMap());
        final GenericValue gadget2 = new MockGenericValue("PortletConfiguration", MapBuilder.newBuilder().
                add("id", 10011L).
                add("portalpage", 10010L).
                add("portletId", null).
                add("columnNumber", 0).
                add("position", 2).
                add("gadgetXml", "local/piechart.xml").
                add("color", "color0").toMap());
        final GenericValue bugzillaGadget = new MockGenericValue("PortletConfiguration", MapBuilder.newBuilder().
                add("id", 10013L).
                add("portalpage", 10010L).
                add("portletId", null).
                add("columnNumber", 0).
                add("position", 1).
                add("gadgetXml", "rest/gadgets/1.0/g/com.atlassian.jira.gadgets:bugzilla/gadgets/bugzilla-id-search.xml").
                add("color", "color0").toMap());

        ofBizDelegator = new MockOfBizDelegator(newArrayList(gadget1, bugzillaGadget, gadget2), newArrayList(gadget1, gadget2));
    }

    @Test
    public void testUpgradeRemovesBugzillaGadgetOnly() throws Exception
    {
        final UpgradeTask_Build64015 upgradeTask = new UpgradeTask_Build64015(ofBizDelegator, cachingPortletConfigurationStore);

        assertEquals("64015", upgradeTask.getBuildNumber());
        assertEquals("Removing the JIRA Bugzilla ID search gadget.", upgradeTask.getShortDescription());

        upgradeTask.doUpgrade(false);

        ofBizDelegator.verify();
    }
}