package com.atlassian.jira.jql.query;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleImpl;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserUtil;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.Arrays.asList;
import static org.apache.lucene.search.BooleanClause.Occur.SHOULD;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class QueryProjectRoleAndGroupPermissionsDecoratorTest
{

    private static final ApplicationUser ANONYMOUS = null;
    private static final String GROUP_1 = "group1";
    private static final String GROUP_2 = "group2";
    private static final String GROUP_3 = "group3";
    private static final ProjectRoleImpl PROJECT_ROLE_1 = new ProjectRoleImpl(11L, "one", "o");
    private static final ProjectRoleImpl PROJECT_ROLE_2 = new ProjectRoleImpl(22L, "two", "tt");
    private static final ProjectRoleImpl PROJECT_ROLE_3 = new ProjectRoleImpl(33L, "three", "ttt");
    private static final String LEVEL_FIELD = "level";
    private static final String LEVEL_ROLE_FIELD = "role_level";

    QueryProjectRoleAndGroupPermissionsDecorator queryDecorator;

    @Rule
    public RuleChain mocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ProjectRoleManager projectRoleManager;
    @Mock
    private QueryCreationContext queryCreationContext;
    @Mock
    @AvailableInContainer
    private UserUtil userUtil;

    @Before
    public void setUp()
    {
        List<Project> projects = Lists.<Project>newArrayList(
                new MockProject(12),
                new MockProject(14));
        when(queryCreationContext.isSecurityOverriden()).thenReturn(false);
        when(queryCreationContext.getApplicationUser()).thenReturn(ANONYMOUS);
        when(permissionManager.getProjects(ProjectPermissions.BROWSE_PROJECTS, ANONYMOUS)).thenReturn(projects);

        queryDecorator = spy(new QueryProjectRoleAndGroupPermissionsDecorator(permissionManager, projectRoleManager));

    }

    @Test
    public void testAddPermissionFilterQuery()
    {
        final Query query = new TermQuery(new Term("field", "value"));
        final Query permissionQuery = queryDecorator.createPermissionQuery(queryCreationContext, LEVEL_FIELD, LEVEL_ROLE_FIELD);
        final BooleanQuery expectedQuery = new BooleanQuery();
        expectedQuery.add(query, BooleanClause.Occur.MUST);
        expectedQuery.add(permissionQuery, BooleanClause.Occur.MUST);

        final Query result = queryDecorator.appendPermissionFilterQuery(query, queryCreationContext, LEVEL_FIELD, LEVEL_ROLE_FIELD);
        assertThat(result, equalTo((Query) expectedQuery));
    }

    @Test
    public void testAddPermissionFilterQueryUserHasNoRightsToAnyProject()
    {
        final Query query = new TermQuery(new Term("field", "value"));

        when(permissionManager.getProjects(ProjectPermissions.BROWSE_PROJECTS, ANONYMOUS)).thenReturn(new TreeSet<Project>());

        final Query result = queryDecorator.appendPermissionFilterQuery(query, queryCreationContext, LEVEL_FIELD, LEVEL_ROLE_FIELD);
        assertThat(result, equalTo((Query) new BooleanQuery()));
    }

    @Test
    public void testGetPermissionFilterQuery() throws Exception
    {
        final List<Long> projectIds = queryDecorator.getVisibleProjectIds(queryCreationContext.getApplicationUser());
        final BooleanQuery levelQuery = queryDecorator.createLevelRestrictionQuery(projectIds, queryCreationContext.getApplicationUser(), LEVEL_FIELD, LEVEL_ROLE_FIELD);
        final BooleanQuery projectVisibilityQuery = queryDecorator.createProjectVisibilityQuery(projectIds);
        final BooleanQuery expectedQuery = new BooleanQuery();
        expectedQuery.add(levelQuery, BooleanClause.Occur.MUST);
        expectedQuery.add(projectVisibilityQuery, BooleanClause.Occur.MUST);

        final Query result = queryDecorator.createPermissionQuery(queryCreationContext, LEVEL_FIELD, LEVEL_ROLE_FIELD);

        assertThat(result, equalTo((Query) expectedQuery));
    }

    @Test
    public void testGetPermissionFilterQueryUserHasNoRightsToAnyProject()
    {
        when(permissionManager.getProjects(ProjectPermissions.BROWSE_PROJECTS, ANONYMOUS)).thenReturn(new TreeSet<Project>());

        final Query result = queryDecorator.createPermissionQuery(queryCreationContext, LEVEL_FIELD, LEVEL_ROLE_FIELD);
        assertThat(result, equalTo((Query) new BooleanQuery()));
    }

    @Test
    public void testGetPermissionFilterSecurityIsOverridden()
    {
        when(queryCreationContext.isSecurityOverriden()).thenReturn(true);

        final Query result = queryDecorator.createPermissionQuery(queryCreationContext, LEVEL_FIELD, LEVEL_ROLE_FIELD);

        assertThat(result, equalTo((Query) new MatchAllDocsQuery()));
    }

    @Test
    public void testCreateProjectVisibilityQueryEmptyProjectIds() throws Exception
    {
        final BooleanQuery result = queryDecorator.createProjectVisibilityQuery(Collections.<Long>emptyList());
        assertThat(result, equalTo(new BooleanQuery()));
    }

    @Test
    public void testCreateProjectVisibilityQuery() throws Exception
    {
        final BooleanQuery expectedQuery = new BooleanQuery();
        expectedQuery.add(new TermQuery(new Term(DocumentConstants.PROJECT_ID, "23")), SHOULD);
        expectedQuery.add(new TermQuery(new Term(DocumentConstants.PROJECT_ID, "33")), SHOULD);
        expectedQuery.add(new TermQuery(new Term(DocumentConstants.PROJECT_ID, "34")), SHOULD);

        final BooleanQuery result = queryDecorator.createProjectVisibilityQuery(newArrayList(23L, 33L, 34L));
        assertThat(result, equalTo(expectedQuery));
    }

    @Test
    public void testCreateLevelRestrictionQueryForGroupRestrictions() throws Exception
    {
        final List<Long> projectIds = newArrayList(1L, 2L, 3L);

        final ApplicationUser mockUser = new MockApplicationUser("testDude");

        when(projectRoleManager.createProjectIdToProjectRolesMap(mockUser, projectIds))
                .thenReturn(new ProjectRoleManager.ProjectIdToProjectRoleIdsMap());

        final SortedSet<String> groups = new TreeSet<String>();
        groups.addAll(ImmutableList.of("group1", "group2", "group3"));
        when(userUtil.getGroupNamesForUser(anyString())).thenReturn(groups);

        final BooleanQuery levelQuery = queryDecorator.createLevelRestrictionQuery(projectIds, mockUser, LEVEL_FIELD, LEVEL_ROLE_FIELD);
        BooleanQuery expectedQuery = new BooleanQuery();
        Query noGroupOrProjectQuery = queryDecorator.createNoGroupOrProjectRoleLevelQuery(LEVEL_FIELD, LEVEL_ROLE_FIELD);
        expectedQuery.add(noGroupOrProjectQuery, SHOULD);
        BooleanQuery groupQuery = new BooleanQuery();
        for (String g : groups)
        {
            groupQuery.add(new TermQuery(new Term(DocumentConstants.COMMENT_LEVEL, g)), SHOULD);
        }
        expectedQuery.add(groupQuery, SHOULD);
        assertThat(levelQuery, equalTo(expectedQuery));
    }

    @Test
    public void testCreateLevelRestrictionQueryForRoleRestrictions() throws Exception
    {
        List<Long> projectIds = newArrayList(1L, 2L, 3L);

        final ApplicationUser mockUser = new MockApplicationUser("testDude");
        final ProjectRoleManager.ProjectIdToProjectRoleIdsMap idsMap = new ProjectRoleManager.ProjectIdToProjectRoleIdsMap();
        idsMap.add(1L, 123L);
        idsMap.add(2L, 345L);
        when(projectRoleManager.createProjectIdToProjectRolesMap(mockUser, projectIds))
                .thenReturn(idsMap);

        when(userUtil.getGroupNamesForUser(anyString())).thenReturn(new TreeSet<String>());

        Query noGroupOrProjectQuery = queryDecorator.createNoGroupOrProjectRoleLevelQuery(LEVEL_FIELD, LEVEL_ROLE_FIELD);
        Query roleQuery = queryDecorator.createProjectRoleLevelQuery(idsMap, LEVEL_ROLE_FIELD);
        BooleanQuery expectedQuery = new BooleanQuery();
        expectedQuery.add(noGroupOrProjectQuery, SHOULD);
        expectedQuery.add(roleQuery, SHOULD);

        final BooleanQuery levelQuery = queryDecorator.createLevelRestrictionQuery(projectIds, mockUser, LEVEL_FIELD, LEVEL_ROLE_FIELD);

        assertThat(levelQuery, equalTo(expectedQuery));
    }

    @Test
    public void testCreateLevelRestrictionQueryForNullUser() throws Exception
    {
        final List<Long> projectIds = newArrayList(1L, 2L, 3L);

        final BooleanQuery levelQuery = queryDecorator.createLevelRestrictionQuery(projectIds, ANONYMOUS, LEVEL_FIELD, LEVEL_ROLE_FIELD);

        BooleanQuery expectedQuery = new BooleanQuery();
        Query noGroupOrProjectQuery = queryDecorator.createNoGroupOrProjectRoleLevelQuery(LEVEL_FIELD, LEVEL_ROLE_FIELD);
        expectedQuery.add(noGroupOrProjectQuery, SHOULD);
        assertThat(levelQuery, equalTo(expectedQuery));
    }


    @Test
    public void testCreateNoGroupOrProjectRoleLevelQuery()
    {
        final Query query = queryDecorator.createNoGroupOrProjectRoleLevelQuery(LEVEL_FIELD, LEVEL_ROLE_FIELD);
        assertThat(query.toString(), equalTo("+" + LEVEL_FIELD + ":-1 +" + LEVEL_ROLE_FIELD + ":-1"));
    }

    @Test
    public void testCreateProjectRoleLevelQuery() throws Exception
    {
        Query result = queryDecorator.createProjectRoleLevelQuery(null, LEVEL_ROLE_FIELD);
        assertThat(result, equalTo((Query) new BooleanQuery()));

        result = queryDecorator.createProjectRoleLevelQuery(new ProjectRoleManager.ProjectIdToProjectRoleIdsMap(), LEVEL_ROLE_FIELD);
        assertThat(result, equalTo((Query) new BooleanQuery()));

        final ProjectRoleManager.ProjectIdToProjectRoleIdsMap map = new ProjectRoleManager.ProjectIdToProjectRoleIdsMap();
        map.add(1L, null);
        map.add(2L, PROJECT_ROLE_1.getId());
        map.add(2L, PROJECT_ROLE_2.getId());
        map.add(3L, PROJECT_ROLE_1.getId());
        map.add(3L, PROJECT_ROLE_2.getId());
        map.add(3L, PROJECT_ROLE_3.getId());

        final Query query = queryDecorator.createProjectRoleLevelQuery(map, LEVEL_ROLE_FIELD);
        final String queryString = query.toString();

        // Project 1: No project role terms
        assertThat(queryString, not(hasProjectRole(1L, PROJECT_ROLE_1)));
        assertThat(queryString, not(hasProjectRole(1L, PROJECT_ROLE_2)));
        assertThat(queryString, not(hasProjectRole(1L, PROJECT_ROLE_3)));

        // Project 2: Project roles 1 and 2
        assertThat(queryString, hasProjectRole(2L, PROJECT_ROLE_1));
        assertThat(queryString, hasProjectRole(2L, PROJECT_ROLE_2));
        assertThat(queryString, not(hasProjectRole(2L, PROJECT_ROLE_3)));

        // Project 3: All of them
        assertThat(queryString, hasProjectRole(3L, PROJECT_ROLE_1));
        assertThat(queryString, hasProjectRole(3L, PROJECT_ROLE_2));
        assertThat(queryString, hasProjectRole(3L, PROJECT_ROLE_3));
    }

    @Test
    public void testCreateFieldInProjectAndUserInRoleQuery()
    {
        Query result = queryDecorator.createFieldInProjectAndUserInRoleQuery(0L, null, LEVEL_ROLE_FIELD);
        assertThat(result, equalTo((Query) new BooleanQuery()));

        result = queryDecorator.createFieldInProjectAndUserInRoleQuery(null, 0L, LEVEL_ROLE_FIELD);
        assertThat(result, equalTo((Query) new BooleanQuery()));

        final Long projectId = 123L;
        final Long projectRoleId = 567L;

        final Query query = queryDecorator.createFieldInProjectAndUserInRoleQuery(projectId, projectRoleId, LEVEL_ROLE_FIELD);
        assertThat(query.toString(), hasProjectRole(projectId, projectRoleId));
    }

    @Test
    public void testCreateGroupLevelQuery() throws Exception
    {
        Query result = queryDecorator.createGroupLevelQuery(null, LEVEL_FIELD);
        assertThat(result, equalTo((Query) new BooleanQuery()));

        result = queryDecorator.createGroupLevelQuery(new HashSet<String>(), LEVEL_FIELD);
        assertThat(result, equalTo((Query) new BooleanQuery()));

        Set<String> groups = newHashSet(GROUP_1);
        Query query = queryDecorator.createGroupLevelQuery(groups, LEVEL_FIELD);
        assertThat(query.toString(), equalTo(LEVEL_FIELD + ":" + GROUP_1));

        groups = newHashSet(GROUP_1, GROUP_2);
        query = queryDecorator.createGroupLevelQuery(groups, LEVEL_FIELD);
        assertThat(asList(query.toString().split(" ")), containsInAnyOrder(LEVEL_FIELD + ":" + GROUP_1, LEVEL_FIELD + ":" + GROUP_2));

        groups = newHashSet(GROUP_1, GROUP_2, GROUP_3);
        query = queryDecorator.createGroupLevelQuery(groups, LEVEL_FIELD);
        assertThat(asList(query.toString().split(" ")), containsInAnyOrder(LEVEL_FIELD + ":" + GROUP_1, LEVEL_FIELD + ":" + GROUP_2, LEVEL_FIELD + ":" + GROUP_3));
    }

    @Test
    public void testGetVisibleProjectIds() throws Exception
    {
        List<Project> projects = Lists.<Project>newArrayList(
                new MockProject(12),
                new MockProject(14));

        when(permissionManager.getProjects(ProjectPermissions.BROWSE_PROJECTS, ANONYMOUS)).thenReturn(projects);
        assertThat(queryDecorator.getVisibleProjectIds(ANONYMOUS), equalTo((List<Long>) ImmutableList.of(12L, 14L)));
    }

    @Test
    public void testDecorateWorklogQuery()
    {
        Query query = mock(Query.class);
        queryDecorator.decorateWorklogQueryWithPermissionChecks(query, queryCreationContext);
        verify(queryDecorator).appendPermissionFilterQuery(query, queryCreationContext, DocumentConstants.WORKLOG_LEVEL, DocumentConstants.WORKLOG_LEVEL_ROLE);
    }

    static Matcher<String> hasProjectRole(final long projectId, final ProjectRole role)
    {
        return new ProjectAndRoleQueryMatcher(projectId, role.toString(), role.getId());
    }

    static Matcher<String> hasProjectRole(final long projectId, final long roleId)
    {
        return new ProjectAndRoleQueryMatcher(projectId, String.valueOf(roleId), roleId);
    }

    static class ProjectAndRoleQueryMatcher extends TypeSafeMatcher<String>
    {
        private final long projectId;
        private final String role;
        private final String expected;

        ProjectAndRoleQueryMatcher(final long projectId, final String role, final long roleId)
        {
            this.projectId = projectId;
            this.role = role;
            this.expected = "(+projid:" + projectId + " +" + LEVEL_ROLE_FIELD + ":" + roleId + ')';
        }

        @Override
        protected boolean matchesSafely(final String queryString)
        {
            // Either a ()'d substring, or the entire string equal without the ()'s
            return queryString.contains(expected) ||
                    queryString.equals(expected.substring(1, expected.length() - 1));
        }

        @Override
        public void describeTo(final Description description)
        {
            description.appendText("query string containing term for project ID ").appendValue(projectId)
                    .appendText(" and project role ").appendValue(role).appendText("; expected=[")
                    .appendText(expected).appendText("]");
        }
    }
}