package com.atlassian.jira.issue.managers;

import java.io.File;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

import com.atlassian.fugue.Function2;
import com.atlassian.fugue.Option;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.bc.issue.properties.IssuePropertyHelper;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.entity.property.JsonEntityPropertyManager;
import com.atlassian.jira.event.issue.property.IssuePropertySetEvent;
import com.atlassian.jira.io.InputStreamFunctionFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.*;
import com.atlassian.jira.issue.attachment.store.strategy.move.IOUtilsWrapper;
import com.atlassian.jira.plugin.attachment.AttachmentArchive;
import com.atlassian.jira.plugin.attachment.AttachmentArchiveEntry;
import com.atlassian.jira.plugin.attachment.AttachmentArchiveEntryBuilder;
import com.atlassian.jira.plugin.attachment.AttachmentArchiveImpl;
import com.atlassian.jira.plugin.attachment.AttachmentProcessor;
import com.atlassian.jira.plugin.attachment.AttachmentProcessorModuleDescriptor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.util.concurrent.Promise;

import com.atlassian.util.concurrent.Promises;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestDefaultAttachmentIndexManager
{
    public static final String ISSUE_PROPERTY_TYPE = "ISSUE";
    public static final String EMPTY_RESULT = "[]";
    public static final int MAX_ENTRIES = 100;
    private final String extension = "zip";
    private final String alphaEntryJson = "{\"entryIndex\":0,\"name\":\"alpha.java\",\"mediaType\":\"text/x-java-source\","
            + "\"size\":20,\"directory\":false,\"directoryDepth\":0,\"modified\":\"2014-09-16T15:49:07\"}";
    private final String betaEntryJson = "{\"entryIndex\":1,\"name\":\"beta.jar\",\"mediaType\":\"application/java-archive\","
            + "\"size\":27,\"directory\":false,\"directoryDepth\":0,\"modified\":\"2013-10-17T21:13:59\"}";
    @Mock
    public Function2<ApplicationUser, EntityProperty, IssuePropertySetEvent> createSetPropertyEventFunction;
    @Mock
    public JSONObject longJson0;
    @Mock
    private JsonEntityPropertyManager jsonEntityPropertyManager;
    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private StreamAttachmentStore attachmentStore;
    @Mock
    private IssuePropertyHelper issuePropertyHelper;
    @Mock
    private EntityPropertyType entityPropertyType;
    @Mock
    private AttachmentProcessor attachmentProcessor;
    @Mock
    private AttachmentProcessorModuleDescriptor attachmentProcessorModuleDescriptor;
    @Mock
    private EntityProperty emptyResult;
    @Mock
    private File file;
    @Mock
    private Attachment attachment;
    @Mock
    private Issue issue;
    @Mock
    private IOUtilsWrapper fileFactory;
    @Mock
    private InputStreamFunctionFactory inputStreamFunctionFactory;
    @Mock
    private AttachmentKeyMapper attachmentKeyMapper;
    @Mock
    private com.atlassian.util.concurrent.Function<InputStream, Unit> saveStream;
    private AttachmentArchiveEntry alphaEntry;
    private AttachmentArchiveEntry betaEntry;
    private DefaultAttachmentIndexManager indexManager;

    @Before
    public void setUp() throws Exception
    {
        alphaEntry = new AttachmentArchiveEntryBuilder()
                .entryIndex(0)
                .name("alpha.java")
                .size(20)
                .mediaType("text/x-java-source")
                .build();

        betaEntry = new AttachmentArchiveEntryBuilder()
                .entryIndex(1)
                .name("beta.jar")
                .size(27)
                .mediaType("application/java-archive")
                .build();

        final List<AttachmentProcessorModuleDescriptor> descriptors = Lists.newArrayList(
                attachmentProcessorModuleDescriptor
        );

        when(attachment.getFilename()).thenReturn("testFile.zIP");

        when(attachmentProcessorModuleDescriptor.getFileExtensions()).thenReturn(Lists.newArrayList(extension));
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(AttachmentProcessorModuleDescriptor.class))
                .thenReturn(descriptors);
        when(attachmentProcessorModuleDescriptor.getModule()).thenReturn(attachmentProcessor);

        when(issuePropertyHelper.getEntityPropertyType()).thenReturn(entityPropertyType);
        when(issuePropertyHelper.createSetPropertyEventFunction()).thenReturn(createSetPropertyEventFunction);
        when(entityPropertyType.getDbEntityName()).thenReturn(ISSUE_PROPERTY_TYPE);

        when(emptyResult.getValue()).thenReturn(EMPTY_RESULT);

        when(longJson0.toString()).thenReturn("0123456789");

        indexManager = new DefaultAttachmentIndexManager(
                pluginAccessor,
                attachmentStore,
                jsonEntityPropertyManager,
                issuePropertyHelper,
                fileFactory,
                inputStreamFunctionFactory,
                attachmentKeyMapper
        );
    }

    @Test
    public void testProcessAttachmentAndCreateIndexEmptyFile() throws Exception
    {
        final AttachmentArchive emptyIndex = AttachmentArchiveImpl.fromJSONArrayQuiet("[]", MAX_ENTRIES);

        when(attachmentProcessor.processAttachment(any(File.class))).thenReturn(emptyAttachmentArchive());

        final Option<AttachmentArchive> result = indexManager.processAttachmentAndCreateIndex(file, attachment, issue, MAX_ENTRIES);

        assertEquals(emptyIndex, result.get());
    }

    @Test
    public void testProcessAttachmentAndCreateIndexInvalidOrEmptyFile() throws Exception
    {
        final AttachmentArchive emptyIndex = AttachmentArchiveImpl.fromJSONArrayQuiet("[]", MAX_ENTRIES);

        when(attachmentProcessor.processAttachment(any(File.class))).thenReturn(emptyAttachmentArchive());

        final Option<AttachmentArchive> result = indexManager.processAttachmentAndCreateIndex(file, attachment, issue, MAX_ENTRIES);

        assertEquals(emptyIndex, result.get());
    }

    @Test
    public void testProcessAttachmentAndCreateIndexNoPlugins() throws Exception
    {
        final Option<AttachmentArchive> noIndex = Option.none();

        when(pluginAccessor.getEnabledModuleDescriptorsByClass(AttachmentProcessorModuleDescriptor.class))
                .thenReturn(Collections.<AttachmentProcessorModuleDescriptor>emptyList());
        when(attachmentProcessor.processAttachment(any(File.class))).thenReturn(emptyAttachmentArchive());

        final Option<AttachmentArchive> result = indexManager.processAttachmentAndCreateIndex(file, attachment, issue, MAX_ENTRIES);

        assertEquals(noIndex, result);
    }

    @Test
    public void testProcessAttachmentAndCreateIndexSingleEntry() throws Exception
    {
        final AttachmentArchive index = AttachmentArchiveImpl.fromJSONArrayQuiet("[" + alphaEntryJson + "]", MAX_ENTRIES);

        when(attachmentProcessor.processAttachment(any(File.class))).thenReturn(ImmutableList.of(alphaEntry));

        final Option<AttachmentArchive> result = indexManager.processAttachmentAndCreateIndex(file, attachment, issue, MAX_ENTRIES);

        assertEquals(index, result.get());
    }

    @Test
    public void testProcessAttachmentAndCreateIndex() throws Exception
    {
        final AttachmentArchive index = AttachmentArchiveImpl.fromJSONArrayQuiet("[" + alphaEntryJson + "," + betaEntryJson + "]", MAX_ENTRIES);

        when(attachmentProcessor.processAttachment(any(File.class))).thenReturn(ImmutableList.of(alphaEntry, betaEntry));

        final Option<AttachmentArchive> result = indexManager.processAttachmentAndCreateIndex(file, attachment, issue, MAX_ENTRIES);

        assertEquals(index, result.get());
    }

    @Test
    public void testProcessAttachmentAndCreateIndexIndexValidationCorrect() throws Exception
    {
        when(attachmentProcessor.processAttachment(any(File.class))).thenReturn(emptyAttachmentArchive());

        indexManager.processAttachmentAndCreateIndex(file, attachment, issue);

        verify(jsonEntityPropertyManager).put(any(ApplicationUser.class), any(String.class), any(Long.class),
                any(String.class), any(String.class), any(Function2.class), anyBoolean());
    }

    @Test
    public void shouldGenerateIndex() throws Exception
    {
        final File temporaryFile = mock(File.class);
        final AttachmentKey dummyAttachmentKey = AttachmentKey.builder()
                .withAttachmentFilename("dummyAttachmentFilename")
                .withAttachmentId(6237846L)
                .withIssueKey("dummyIssueKey")
                .withProjectKey("dummyProjectKey")
                .build();
        final AttachmentArchive dummyArchive =
                AttachmentArchiveImpl.fromJSONArrayQuiet("[" + alphaEntryJson + "]", MAX_ENTRIES);
        when(jsonEntityPropertyManager.get(any(String.class), any(Long.class), any(String.class))).thenReturn(null);
        when(fileFactory
                .processTemporaryFile(eq("attachment-toc-"), eq(".tmp"), any(com.google.common.base.Function.class)))
                .thenAnswer(new Answer<Option<AttachmentArchive>>()
                {
                    @Override
                    public Option<AttachmentArchive> answer(final InvocationOnMock invocation) throws Throwable
                    {
                        final Function<File, Option<AttachmentArchive>> generateIndex =
                                (Function<File, Option<AttachmentArchive>>) invocation.getArguments()[2];
                        assertTrue(generateIndex.apply(temporaryFile).isDefined());
                        return Option.some(dummyArchive);
                    }
                });
        when(inputStreamFunctionFactory.saveStream(temporaryFile)).thenReturn(saveStream);
        when(attachmentKeyMapper.fromAttachment(attachment)).thenReturn(dummyAttachmentKey);
        final Promise<Unit> emptyPromise = Promises.promise(null);
        when(attachmentStore.getAttachment(dummyAttachmentKey, saveStream)).thenReturn(emptyPromise);

        final Option<AttachmentArchive> result = indexManager.getAttachmentContents(attachment, issue, MAX_ENTRIES);

        verify(jsonEntityPropertyManager).put(
                any(ApplicationUser.class), any(String.class), any(Long.class),
                any(String.class), any(String.class), any(Function2.class), anyBoolean()
        );
        verify(attachmentStore).getAttachment(dummyAttachmentKey, saveStream);
        assertEquals(dummyArchive, result.get());
    }

    @Test
    public void testGetAttachmentIndex() throws Exception
    {
        when(jsonEntityPropertyManager.get(any(String.class), any(Long.class), any(String.class))).thenReturn(emptyResult);

        final Option<AttachmentArchive> result = indexManager.getAttachmentContents(attachment, issue, MAX_ENTRIES);

        final AttachmentArchive expected = AttachmentArchiveImpl.fromJSONArrayQuiet("[]", MAX_ENTRIES);
        verify(jsonEntityPropertyManager, never()).put(any(ApplicationUser.class), any(String.class), any(Long.class),
                any(String.class), any(String.class), any(Function2.class), anyBoolean());
        assertEquals(expected, result.get());
    }

    @Test
    public void testRemoveAttachmentIndex() throws Exception
    {
        indexManager.removeAttachmentIndex(attachment, issue);

        verify(jsonEntityPropertyManager).delete(ISSUE_PROPERTY_TYPE, issue.getId(), attachment.getId().toString());
    }

    @Test
    public void totalLengthPredicate() throws Exception
    {
        final DefaultAttachmentIndexManager.TotalLengthPredicate totalLengthPredicate = new DefaultAttachmentIndexManager.TotalLengthPredicate(31);

        final List<JSONObject> jsons = Lists.newArrayList(longJson0, longJson0, longJson0);
        final List<JSONObject> filtered = Lists.newArrayList(Iterables.filter(jsons, totalLengthPredicate));

        assertEquals(2, filtered.size());
    }

    @Test
    public void missingAttachmentDataShouldFailGracefully() throws Exception
    {
        final File temporaryFile = mock(File.class);
        final AttachmentKey dummyAttachmentKey = AttachmentKey.builder()
                .withAttachmentFilename("dummyAttachmentFilename")
                .withAttachmentId(6237846L)
                .withIssueKey("dummyIssueKey")
                .withProjectKey("dummyProjectKey")
                .build();
        when(jsonEntityPropertyManager.get(any(String.class), any(Long.class), any(String.class))).thenReturn(null);
        when(fileFactory
                .processTemporaryFile(eq("attachment-toc-"), eq(".tmp"), any(com.google.common.base.Function.class)))
                .thenAnswer(new Answer<Option<AttachmentArchive>>() {
                    @Override
                    public Option<AttachmentArchive> answer(final InvocationOnMock invocation) throws Throwable {
                        final Function<File, Option<AttachmentArchive>> generateIndex =
                                (Function<File, Option<AttachmentArchive>>) invocation.getArguments()[2];
                        return generateIndex.apply(temporaryFile);
                    }
                });
        when(inputStreamFunctionFactory.saveStream(temporaryFile)).thenReturn(saveStream);
        when(attachmentKeyMapper.fromAttachment(attachment)).thenReturn(dummyAttachmentKey);
        final Promise<Unit> failedPromise = Promises.rejected(new NoAttachmentDataException("no attachment data"));
        when(attachmentStore.getAttachment(dummyAttachmentKey, saveStream)).thenReturn(failedPromise);

        final Option<AttachmentArchive> result = indexManager.getAttachmentContents(attachment, issue, MAX_ENTRIES);
        verify(attachmentStore).getAttachment(dummyAttachmentKey, saveStream);
        assertTrue(result.isEmpty());
    }

    private List<AttachmentArchiveEntry> emptyAttachmentArchive()
    {
        return Collections.emptyList();
    }
}