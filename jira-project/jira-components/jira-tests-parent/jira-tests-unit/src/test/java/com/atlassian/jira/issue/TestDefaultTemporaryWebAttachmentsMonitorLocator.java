package com.atlassian.jira.issue;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.junit.rules.MockHttp;
import com.atlassian.jira.matchers.OptionMatchers;
import com.atlassian.jira.mock.servlet.MockHttpSession;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.jira.web.action.issue.TemporaryWebAttachmentsMonitor;

import com.googlecode.catchexception.CatchException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static com.atlassian.jira.matchers.OptionMatchers.none;
import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.apis.CatchExceptionHamcrestMatchers.hasMessage;
import static com.googlecode.catchexception.apis.CatchExceptionHamcrestMatchers.hasNoCause;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestDefaultTemporaryWebAttachmentsMonitorLocator
{
    //lets use proper session to do more black box testing
    private final MockHttpSession session = new MockHttpSession();
    @Rule
    public TestRule initMock = new InitMockitoMocks(this);
    @Rule
    public MockHttp httpContext = MockHttp.withMockitoMocks();
    @Mock
    private StreamAttachmentStore streamAttachmentStore;
    @InjectMocks
    private DefaultTemporaryWebAttachmentsMonitorLocator tempMonitorLocator;

    @Before
    public void setUp() throws Exception
    {
        when(httpContext.mockRequest().getSession()).thenReturn(session);
    }

    @Test
    public void shouldNotCreateMonitorIfOneDoesNotExist() throws Exception
    {
        //having
        //when
        final Option<TemporaryWebAttachmentsMonitor> temporaryWebAttachmentsMonitors = tempMonitorLocator.get();

        //then
        assertThat("TemporaryWebAttachmentsMonitor should not be created", temporaryWebAttachmentsMonitors, none());
    }

    @Test
    public void shouldNotCreateMonitorIfNotRunningInRequestScope() throws Exception
    {
        //having
        tempMonitorLocator.getOrCreate();
        ExecutingHttpRequest.clear();

        //when
        final Option<TemporaryWebAttachmentsMonitor> temporaryWebAttachmentsMonitors = tempMonitorLocator.get();

        //then
        assertThat("should not create monitor when running without http context", temporaryWebAttachmentsMonitors, none());
    }

    @Test
    public void shouldReturnPreviouslyCreatedMonitorFromSession() throws Exception
    {
        //having
        final TemporaryWebAttachmentsMonitor monitor = tempMonitorLocator.getOrCreate();
        //when
        final Option<TemporaryWebAttachmentsMonitor> temporaryWebAttachmentsMonitors = tempMonitorLocator.get();
        //then
        assertThat("Should return previously configured instance", temporaryWebAttachmentsMonitors,
                OptionMatchers.<TemporaryWebAttachmentsMonitor>some(sameInstance(monitor)));
    }

    @Test
    public void shouldThrowIllegalStateExceptionOnGetOrCreateIfRunningOutsideRequestScope() throws Exception
    {
        //having
        ExecutingHttpRequest.clear();

        //when
        catchException(tempMonitorLocator).getOrCreate();

        //then
        //noinspection unchecked
        assertThat(CatchException.<IllegalStateException>caughtException(), allOf(
                is(IllegalStateException.class),
                hasMessage("Session doesn't exists, this method cannot work outside HTTP session!"),
                hasNoCause()
        ));
    }

    @Test
    public void shouldCreateMonitorOnGetOrCreate() throws Exception
    {
        //having
        //when
        final TemporaryWebAttachmentsMonitor temporaryWebAttachmentsMonitors = tempMonitorLocator.getOrCreate();

        //then
        assertThat("Should create monitor", temporaryWebAttachmentsMonitors, notNullValue());
    }

    @Test
    public void shouldReturnMonitorStoredInSession() throws Exception
    {
        //having
        final TemporaryWebAttachmentsMonitor firstMonitor = tempMonitorLocator.getOrCreate();

        //when
        final TemporaryWebAttachmentsMonitor secondMonitor = tempMonitorLocator.getOrCreate();

        //then
        assertSame("Subsequent calls should return the same monitor", firstMonitor, secondMonitor);
    }

    @Test
    public void monitorShouldBeStoredInSession() throws Exception
    {
        //having
        final TemporaryWebAttachmentsMonitor firstMonitor = tempMonitorLocator.getOrCreate();
        session.invalidate();

        //when
        final TemporaryWebAttachmentsMonitor secondMonitor = tempMonitorLocator.getOrCreate();

        //then
        assertNotSame("Object value should be stored in session", firstMonitor, secondMonitor);
    }
}