package com.atlassian.jira.license;

import com.atlassian.fugue.Option;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.license.LicenseRoleStore.LicenseRoleData;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OfBizLicenseRoleStoreTest
{
    private static final String ENTITY_NAME = "LicenseRoleGroup";
    private static final String COL_NAME = "licenseRoleName";
    private static final String COL_GROUP = "groupId";
    private static final String COL_PRIMARY = "primaryGroup";

    @Rule
    public TestRule role = new InitMockitoMocks(this);

    @Mock private OfBizDelegator delegator;

    @Test
    public void getRoleNoRoleStored()
    {
        final String id = "what";

        final OfBizLicenseRoleStore roleStore = new OfBizLicenseRoleStore(delegator);
        final LicenseRoleData data = roleStore.get(LicenseRoleId.valueOf(id));

        assertThat(data, new LicenseRoleDataMatcher().id(id));
    }

    @Test
    public void getRoleStoredWithoutPrimary()
    {
        final String id = "what";

        when(delegator.findByAnd(ENTITY_NAME, ImmutableMap.of(COL_NAME, id)))
                .thenReturn(makeEntries(id, null, "one", "two", "three"));

        final OfBizLicenseRoleStore roleStore = new OfBizLicenseRoleStore(delegator);
        final LicenseRoleData data = roleStore.get(LicenseRoleId.valueOf(id));

        assertThat(data, new LicenseRoleDataMatcher().id(id).groups("one", "two", "three"));
    }

    @Test
    public void getRoleStoredWithAllNullPrimary()
    {
        final String id = "what";

        when(delegator.findByAnd(ENTITY_NAME, ImmutableMap.of(COL_NAME, id)))
                .thenReturn(makePrimaryNull(makeEntries(id, null, "one", "two", "three")));

        final OfBizLicenseRoleStore roleStore = new OfBizLicenseRoleStore(delegator);
        final LicenseRoleData data = roleStore.get(LicenseRoleId.valueOf(id));

        assertThat(data, new LicenseRoleDataMatcher().id(id).groups("one", "two", "three"));
    }

    @Test
    public void getRoleStoredWithPrimary()
    {
        final String id = "what";

        when(delegator.findByAnd(ENTITY_NAME, ImmutableMap.of(COL_NAME, id)))
                .thenReturn(makeEntries(id, "one", "one", "two", "three"));

        final OfBizLicenseRoleStore roleStore = new OfBizLicenseRoleStore(delegator);
        final LicenseRoleData data = roleStore.get(LicenseRoleId.valueOf(id));

        assertThat(data, new LicenseRoleDataMatcher().id(id).groups("one", "two", "three").primaryGroup("one"));
    }

    @Test
    public void getRoleStoredWithMultiplePrimary()
    {
        final String id = "what";

        List<GenericValue> values = Lists.newArrayList();
        Iterables.addAll(values, makeEntries(id, "one", "one"));
        Iterables.addAll(values, makeEntries(id, "two", "two"));
        Iterables.addAll(values, makeEntries(id, "abc", "abc"));

        when(delegator.findByAnd(ENTITY_NAME, ImmutableMap.of(COL_NAME, id)))
                .thenReturn(values);

        final OfBizLicenseRoleStore roleStore = new OfBizLicenseRoleStore(delegator);
        final LicenseRoleData data = roleStore.get(LicenseRoleId.valueOf(id));

        assertThat(data, new LicenseRoleDataMatcher().id(id).groups("one", "abc", "two").primaryGroup("abc"));
    }

    @Test
    public void setRoleDeleteAll()
    {
        final String id = "what";

        final GenericValue one = makeEntry(id, "one", true);
        final GenericValue two = makeEntry(id, "two", false);
        final GenericValue three = makeEntry(id, "three", false);

        when(delegator.findByAnd(ENTITY_NAME, ImmutableMap.of(COL_NAME, id)))
                .thenReturn(Lists.newArrayList(one, two, three));

        LicenseRoleData data =
            new LicenseRoleData(LicenseRoleId.valueOf(id), Collections.<String>emptyList(), Option.<String>none());

        final OfBizLicenseRoleStore roleStore = new OfBizLicenseRoleStore(delegator);
        roleStore.save(data);

        verify(delegator).removeValue(one);
        verify(delegator).removeValue(two);
        verify(delegator).removeValue(three);
    }

    @Test
    public void setRoleRemoveGroup()
    {
        final String id = "what";

        final GenericValue one = makeEntry(id, "one", false);
        final GenericValue two = makeEntry(id, "two", false);
        final GenericValue three = makeEntry(id, "three", false);

        when(delegator.findByAnd(ENTITY_NAME, ImmutableMap.of(COL_NAME, id)))
                .thenReturn(Lists.newArrayList(one, two, three));

        LicenseRoleData data =
            new LicenseRoleData(LicenseRoleId.valueOf(id), Sets.newHashSet("one", "two"), Option.<String>none());

        final OfBizLicenseRoleStore roleStore = new OfBizLicenseRoleStore(delegator);
        roleStore.save(data);

        verify(delegator).removeValue(three);
    }

    @Test
    public void setRoleAddGroupGroup()
    {
        final String id = "what";

        final GenericValue one = makeEntry(id, "one", false);
        final GenericValue two = makeEntry(id, "two", false);

        when(delegator.findByAnd(ENTITY_NAME, ImmutableMap.of(COL_NAME, id)))
                .thenReturn(Lists.newArrayList(one, two));

        LicenseRoleData data =
            new LicenseRoleData(LicenseRoleId.valueOf(id), Sets.newHashSet("one", "two", "three"), Option.<String>none());

        final OfBizLicenseRoleStore roleStore = new OfBizLicenseRoleStore(delegator);
        roleStore.save(data);

        verify(delegator).createValue(ENTITY_NAME, createFieldMap(id, "three", false));
    }

    @Test
    public void setRoleAddGroupGroupAndSetPrimary()
    {
        final String id = "what";

        final GenericValue one = makeEntry(id, "one", true);
        final GenericValue two = makeEntry(id, "two", false);

        when(delegator.findByAnd(ENTITY_NAME, ImmutableMap.of(COL_NAME, id)))
                .thenReturn(Lists.newArrayList(one, two));

        LicenseRoleData data =
            new LicenseRoleData(LicenseRoleId.valueOf(id), Sets.newHashSet("one", "two", "three"), Option.option("three"));

        final OfBizLicenseRoleStore roleStore = new OfBizLicenseRoleStore(delegator);
        roleStore.save(data);

        verify(delegator).createValue(ENTITY_NAME, createFieldMap(id, "three", true));
        verify(delegator).store(makeEntry(id, "one", false));
    }

    @Test
    public void setRoleAddGroupGroupAndUnSetPrimary()
    {
        final String id = "what";

        final GenericValue one = makeEntry(id, "one", true);
        final GenericValue two = makeEntry(id, "two", false);

        when(delegator.findByAnd(ENTITY_NAME, ImmutableMap.of(COL_NAME, id)))
                .thenReturn(Lists.newArrayList(one, two));

        LicenseRoleData data =
            new LicenseRoleData(LicenseRoleId.valueOf(id), Sets.newHashSet("one", "two", "three"), Option.<String>none());

        final OfBizLicenseRoleStore roleStore = new OfBizLicenseRoleStore(delegator);
        roleStore.save(data);

        verify(delegator).createValue(ENTITY_NAME, createFieldMap(id, "three", false));
        verify(delegator).store(makeEntry(id, "one", false));
    }

    @Test
    public void setRoleReSetPrimary()
    {
        final String id = "what";

        final GenericValue one = makeEntry(id, "one", true);
        final GenericValue two = makeEntry(id, "two", false);

        when(delegator.findByAnd(ENTITY_NAME, ImmutableMap.of(COL_NAME, id)))
                .thenReturn(Lists.newArrayList(one, two));

        LicenseRoleData data =
            new LicenseRoleData(LicenseRoleId.valueOf(id), Sets.newHashSet("one", "two"), Option.some("two"));

        final OfBizLicenseRoleStore roleStore = new OfBizLicenseRoleStore(delegator);
        roleStore.save(data);

        verify(delegator).store(makeEntry(id, "one", false));
        verify(delegator).store(makeEntry(id, "two", true));
    }

    @Test
    public void setRoleSetPrimary()
    {
        final String id = "what";

        final GenericValue one = makeEntry(id, "one", false);
        final GenericValue two = makeEntry(id, "two", false);

        when(delegator.findByAnd(ENTITY_NAME, ImmutableMap.of(COL_NAME, id)))
                .thenReturn(Lists.newArrayList(one, two));

        LicenseRoleData data =
            new LicenseRoleData(LicenseRoleId.valueOf(id), Sets.newHashSet("one", "two"), Option.some("two"));

        final OfBizLicenseRoleStore roleStore = new OfBizLicenseRoleStore(delegator);
        roleStore.save(data);

        verify(delegator).store(makeEntry(id, "two", true));
    }

    @Test
    public void setRoleSetPrimaryIsNullInDB()
    {
        final String id = "what";

        final GenericValue one = makeEntry(id, "one", null);
        final GenericValue two = makeEntry(id, "two", false);

        when(delegator.findByAnd(ENTITY_NAME, ImmutableMap.of(COL_NAME, id)))
                .thenReturn(Lists.newArrayList(one, two));

        LicenseRoleData data =
            new LicenseRoleData(LicenseRoleId.valueOf(id), Sets.newHashSet("one", "two"), Option.some("two"));

        final OfBizLicenseRoleStore roleStore = new OfBizLicenseRoleStore(delegator);
        roleStore.save(data);

        verify(delegator).store(makeEntry(id, "two", true));
    }

    @Test
    public void setRoleClearPrimary()
    {
        final String id = "what";

        final GenericValue one = makeEntry(id, "one", true);
        final GenericValue two = makeEntry(id, "two", true);

        when(delegator.findByAnd(ENTITY_NAME, ImmutableMap.of(COL_NAME, id)))
                .thenReturn(Lists.newArrayList(one, two));

        LicenseRoleData data =
            new LicenseRoleData(LicenseRoleId.valueOf(id), Sets.newHashSet("one", "two"), Option.none(String.class));

        final OfBizLicenseRoleStore roleStore = new OfBizLicenseRoleStore(delegator);
        roleStore.save(data);

        verify(delegator).store(makeEntry(id, "two", false));
        verify(delegator).store(makeEntry(id, "one", false));
    }

    private static List<GenericValue> makeEntries(String id, String primaryName, String...groups)
    {
        List<GenericValue> lists = Lists.newArrayList();
        for (String group : groups)
        {
            lists.add(makeEntry(id, group, primaryName != null && primaryName.equals(group)));
        }
        return lists;
    }

    private static GenericValue makeEntry(String id, String groupName, Boolean primary)
    {
        Map<String, Object> fields = createFieldMap(id, groupName, primary);

        return new MockGenericValue(ENTITY_NAME, fields);
    }

    private static Map<String, Object> createFieldMap(final String id, final String groupName, final Boolean primary)
    {
        Map<String, Object> fields = Maps.newHashMap();
        fields.put(COL_NAME, id);
        fields.put(COL_GROUP, groupName);
        fields.put(COL_PRIMARY, primary);
        return fields;
    }

    private static List<GenericValue> makePrimaryNull(List<GenericValue> values)
    {
        for (GenericValue value : values)
        {
            value.set(COL_PRIMARY, null);
        }
        return values;
    }

}