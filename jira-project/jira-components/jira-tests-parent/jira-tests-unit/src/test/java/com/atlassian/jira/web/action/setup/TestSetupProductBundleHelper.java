package com.atlassian.jira.web.action.setup;

import java.util.Arrays;
import java.util.Collection;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.onboarding.FirstUseFlow;
import com.atlassian.jira.onboarding.OnboardingService;
import com.atlassian.jira.user.MockApplicationUser;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.junit.runners.Parameterized.Parameters;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@RunWith (Parameterized.class)
public class TestSetupProductBundleHelper
{
    @Rule
    public final RuleChain mocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock @AvailableInContainer private OnboardingService onboardingService;

    @Mock
    private SetupSharedVariables setupSharedVariables;

    private final MockApplicationUser applicationUser = new MockApplicationUser("fred");

    private final String selectedBundle;
    private final String expectedDefaultUrl;

    private SetupProductBundleHelper setupProductBundleHelper;

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            { SetupProductBundle.BUNDLE_TRACKING, SetupProductBundleHelper.REDIRECT_DEFAULT_URL },
            { SetupProductBundle.BUNDLE_DEVELOPMENT, SetupProductBundleHelper.REDIRECT_DEVELOPMENT_URL },
            { SetupProductBundle.BUNDLE_SERVICEDESK, SetupProductBundleHelper.REDIRECT_SERVICEDESK_URL}
        });
    }

    public TestSetupProductBundleHelper(final String selectedBundle, final String expected) {
        this.selectedBundle = selectedBundle;
        this.expectedDefaultUrl = expected;
    }

    @Before
    public void setUp() throws Exception
    {
        when(setupSharedVariables.getSelectedBundle()).thenReturn(selectedBundle);
        when(setupSharedVariables.getBundleHasLicenseError()).thenReturn(false);

        setupProductBundleHelper = new SetupProductBundleHelper(setupSharedVariables);
    }

    @Test
    public void helperRedirectsToDefaultUrlOnChosenBundle()
    {
        // given

        // when
        final String url = setupProductBundleHelper.getSetupCompleteRedirectUrl(applicationUser);

        // then
        assertEquals(expectedDefaultUrl, url);
    }

    @Test
    public void helperRedirectsToOnboardingServiceHintRegardlessOfChosenBundle()
    {
        // given
        final String onboardingServiceUrl = setUpOnboardingService();

        // when
        final String url = setupProductBundleHelper.getSetupCompleteRedirectUrl(applicationUser);

        // then
        assertEquals(onboardingServiceUrl, url);
    }


    @Test
    public void helperRedirectsToErrorPageOnErrorsWhenAddonIsChosen()
    {
        // given
        when(setupSharedVariables.getBundleHasLicenseError()).thenReturn(true);

        // when
        final String url = setupProductBundleHelper.getSetupCompleteRedirectUrl(applicationUser);

        // then:
        final boolean addonIsChosen = selectedBundle != SetupProductBundle.BUNDLE_TRACKING;
        final String expected = addonIsChosen ? SetupProductBundleHelper.REDIRECT_ERROR_PAGE_URL : SetupProductBundleHelper.REDIRECT_DEFAULT_URL;
        assertEquals(expected, url);
    }

    @Test
    public void helperIgnoresOnboardingUrlWhenRedirectingToErrorPage()
    {
        // given
        when(setupSharedVariables.getBundleHasLicenseError()).thenReturn(true);
        final String onboardingServiceUrl = setUpOnboardingService();

        // when
        final String url = setupProductBundleHelper.getSetupCompleteRedirectUrl(applicationUser);

        // then:
        final boolean addonIsChosen = selectedBundle != SetupProductBundle.BUNDLE_TRACKING;
        final String expected = addonIsChosen ? SetupProductBundleHelper.REDIRECT_ERROR_PAGE_URL : onboardingServiceUrl;
        assertEquals(expected, url);
    }


    private String setUpOnboardingService()
    {
        final String onboardingServiceUrl = "OnboardingServiceUrl.jspa";
        final FirstUseFlow firstUserFlow = mock(FirstUseFlow.class);
        when(firstUserFlow.getUrl()).thenReturn(onboardingServiceUrl);
        when(onboardingService.getFirstUseFlow(applicationUser)).thenReturn(firstUserFlow);
        return onboardingServiceUrl;
    }
}
