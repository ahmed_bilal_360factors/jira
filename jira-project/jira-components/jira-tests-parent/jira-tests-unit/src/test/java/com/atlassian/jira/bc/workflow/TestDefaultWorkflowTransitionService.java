package com.atlassian.jira.bc.workflow;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.google.common.collect.Lists;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.ConditionDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import com.opensymphony.workflow.loader.RestrictionDescriptor;
import com.opensymphony.workflow.loader.ResultDescriptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.List;
import java.util.Map;

import static com.atlassian.jira.matchers.OptionMatchers.none;
import static com.atlassian.jira.matchers.OptionMatchers.some;
import static com.atlassian.jira.workflow.JiraWorkflow.ACTION_SCREEN_ATTRIBUTE;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("unchecked")
public class TestDefaultWorkflowTransitionService
{
    private static final String ACTION_NAME = "actions";
    private static final String WORKFLOW_NAME = "extractors are awesome";

    /**
     * Mocked data
     */
    @Mock private ActionDescriptor actionDescriptor;
    @Mock private ApplicationUser applicationUser;
    @Mock private ConditionDescriptor conditionDescriptor;
    @Mock private JiraWorkflow draftJiraWorkflow;
    @Mock private FieldScreen fieldScreen;
    @Mock private FunctionDescriptor functionDescriptor;
    @Mock private JiraWorkflow jiraWorkflow;

    /**
     * Mocked services
     */
    @Mock private I18nHelper i18nHelper;
    @Mock private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock private WorkflowService workflowService;

    @InjectMocks private DefaultWorkflowTransitionService workflowTransitionService;

    @Before
    public void setup()
    {
        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(jiraAuthenticationContext.getUser()).thenReturn(applicationUser);
        when(jiraWorkflow.getName()).thenReturn(WORKFLOW_NAME);
        when(draftJiraWorkflow.getActionsByName(ACTION_NAME)).thenReturn(newArrayList(actionDescriptor));
        when(workflowService.getDraftWorkflow(any(JiraServiceContext.class), eq(WORKFLOW_NAME))).thenReturn(draftJiraWorkflow);
    }

    @Test
    public void addConditionReturnsErrorIfDraftCouldNotBeCreated()
    {
        when(workflowService.getDraftWorkflow(any(JiraServiceContext.class), eq(WORKFLOW_NAME))).thenReturn(null);
        when(workflowService.createDraftWorkflow(any(JiraServiceContext.class), eq(WORKFLOW_NAME))).thenAnswer(mockErrorFromWorkflowService());
        ErrorCollection errorCollection = workflowTransitionService.addConditionToWorkflow("actions", conditionDescriptor, jiraWorkflow);
        assertThat(errorCollection.hasAnyErrors(), is(true));
    }

    @Test
    public void addConditionReturnsErrorWithInvalidParams()
    {
        ErrorCollection errorCollection = workflowTransitionService.addConditionToWorkflow(null, null, null);
        assertThat(errorCollection.hasAnyErrors(), is(true));
    }

    @Test
    public void addConditionReturnsErrorOnNoActionsFoundForWorkflow()
    {
        when(draftJiraWorkflow.getActionsByName("")).thenReturn(Lists.<ActionDescriptor>newArrayList());
        ErrorCollection errorCollection = workflowTransitionService.addConditionToWorkflow("", conditionDescriptor, jiraWorkflow);
        assertThat(errorCollection.hasAnyErrors(), is(true));
        verify(workflowService, never()).updateWorkflow(any(JiraServiceContext.class), any(JiraWorkflow.class));
    }

    @Test
    public void addConditionUpdatesWorkflow()
    {
        ArgumentCaptor<RestrictionDescriptor> argument = ArgumentCaptor.forClass(RestrictionDescriptor.class);

        ErrorCollection errorCollection = workflowTransitionService.addConditionToWorkflow("actions", conditionDescriptor, jiraWorkflow);
        assertThat(errorCollection.hasAnyErrors(), is(false));
        //Make sure workflow is updated.
        verify(workflowService).updateWorkflow(new JiraServiceContextImpl(applicationUser, errorCollection), draftJiraWorkflow);
        verify(actionDescriptor).setRestriction(argument.capture());
        List<ConditionDescriptor> conditionDescriptors = argument.getValue().getConditionsDescriptor().getConditions();
        assertThat(conditionDescriptors, hasItem(conditionDescriptor));
    }

    @Test
    public void addFunctionReturnsErrorIfDraftCouldNotBeCreated()
    {
        when(workflowService.getDraftWorkflow(any(JiraServiceContext.class), eq(WORKFLOW_NAME))).thenReturn(null);
        when(workflowService.createDraftWorkflow(any(JiraServiceContext.class), eq(WORKFLOW_NAME))).thenAnswer(mockErrorFromWorkflowService());
        ErrorCollection errorCollection = workflowTransitionService.addPostFunctionToWorkflow("actions", functionDescriptor, jiraWorkflow);
        assertThat(errorCollection.hasAnyErrors(), is(true));
    }

    @Test
    public void addFunctionReturnsErrorWithInvalidParams()
    {
        ErrorCollection errorCollection = workflowTransitionService.addPostFunctionToWorkflow(null, null, null);
        assertThat(errorCollection.hasAnyErrors(), is(true));
    }

    @Test
    public void addFunctionReturnsErrorOnNoActionsFoundForWorkflow()
    {
        when(draftJiraWorkflow.getActionsByName("")).thenReturn(Lists.<ActionDescriptor>newArrayList());
        ErrorCollection errorCollection = workflowTransitionService.addPostFunctionToWorkflow("", functionDescriptor, jiraWorkflow);
        assertThat(errorCollection.hasAnyErrors(), is(true));
        verify(workflowService, never()).updateWorkflow(any(JiraServiceContext.class), any(JiraWorkflow.class));
    }

    @Test
    public void addFunctionUpdatesWorkflow()
    {
        ResultDescriptor resultDescriptor = mock(ResultDescriptor.class);
        List<FunctionDescriptor> existingPostFunctions = newArrayList(mock(FunctionDescriptor.class));
        when(resultDescriptor.getPostFunctions()).thenReturn(existingPostFunctions);
        when(actionDescriptor.getUnconditionalResult()).thenReturn(resultDescriptor);

        ErrorCollection errorCollection = workflowTransitionService.addPostFunctionToWorkflow("actions", functionDescriptor, jiraWorkflow);
        assertThat(errorCollection.hasAnyErrors(), is(false));
        assertThat(existingPostFunctions, hasItem(functionDescriptor));
        //Make sure workflow is updated.
        verify(workflowService).updateWorkflow(new JiraServiceContextImpl(applicationUser, errorCollection), draftJiraWorkflow);
    }

    @Test
    public void setScreenReturnsErrorIfDraftCouldNotBeCreated()
    {
        when(workflowService.getDraftWorkflow(any(JiraServiceContext.class), eq(WORKFLOW_NAME))).thenReturn(null);
        when(workflowService.createDraftWorkflow(any(JiraServiceContext.class), eq(WORKFLOW_NAME))).thenAnswer(mockErrorFromWorkflowService());
        ErrorCollection errorCollection = workflowTransitionService.setScreen("actions", fieldScreen, jiraWorkflow);
        assertThat(errorCollection.hasAnyErrors(), is(true));
    }

    @Test
    public void setScreenReturnsErrorWithInvalidParams()
    {
        ErrorCollection errorCollection = workflowTransitionService.setScreen(null, null, null);
        assertThat(errorCollection.hasAnyErrors(), is(true));
    }

    @Test
    public void setScreenReturnsErrorOnNoActionsFoundForWorkflow()
    {
        when(draftJiraWorkflow.getActionsByName("")).thenReturn(Lists.<ActionDescriptor>newArrayList());
        ErrorCollection errorCollection = workflowTransitionService.setScreen("", fieldScreen, jiraWorkflow);
        assertThat(errorCollection.hasAnyErrors(), is(true));
        verify(workflowService, never()).updateWorkflow(any(JiraServiceContext.class), any(JiraWorkflow.class));
    }

    @Test
    public void setScreenUpdatesActionAndWorkflow()
    {
        when(fieldScreen.getId()).thenReturn(1L);

        Map<String, String> metaAttributes = newHashMap();
        when(actionDescriptor.getMetaAttributes()).thenReturn(metaAttributes);

        ErrorCollection errorCollection = workflowTransitionService.setScreen("actions", fieldScreen, jiraWorkflow);
        assertThat(errorCollection.hasAnyErrors(), is(false));
        assertThat(metaAttributes.get(ACTION_SCREEN_ATTRIBUTE), equalTo("1"));

        verify(workflowService).updateWorkflow(new JiraServiceContextImpl(applicationUser, errorCollection), draftJiraWorkflow);
    }

    @Test
    public void draftOfCreatesNewDraftIfUnavailable()
    {
        when(workflowService.getDraftWorkflow(any(JiraServiceContext.class), eq(WORKFLOW_NAME))).thenReturn(null);
        when(workflowService.createDraftWorkflow(any(JiraServiceContext.class), eq(WORKFLOW_NAME))).thenReturn(draftJiraWorkflow);

        Option<JiraWorkflow> getDraft = workflowTransitionService.draftOf(new SimpleErrorCollection(), jiraWorkflow);
        assertThat(getDraft, some(draftJiraWorkflow));
        verify(workflowService).getDraftWorkflow(any(JiraServiceContext.class), eq(WORKFLOW_NAME));
        verify(workflowService).createDraftWorkflow(any(JiraServiceContext.class), eq(WORKFLOW_NAME));
    }

    @Test
    public void draftOfRetrievesCurrentDraftIfAvailable()
    {
        Option<JiraWorkflow> getDraft = workflowTransitionService.draftOf(new SimpleErrorCollection(), jiraWorkflow);
        assertThat(getDraft, some(draftJiraWorkflow));
        verify(workflowService).getDraftWorkflow(any(JiraServiceContext.class), eq(WORKFLOW_NAME));
        verify(workflowService, never()).createDraftWorkflow(any(JiraServiceContext.class), eq(WORKFLOW_NAME));
    }

    @Test
    public void draftOfReturnsEmptyIfError()
    {
        when(workflowService.getDraftWorkflow(any(JiraServiceContext.class), eq(WORKFLOW_NAME))).thenReturn(null);

        Option<JiraWorkflow> getDraft = workflowTransitionService.draftOf(new SimpleErrorCollection(), jiraWorkflow);
        assertThat(getDraft, none());
        verify(workflowService).getDraftWorkflow(any(JiraServiceContext.class), eq(WORKFLOW_NAME));
        verify(workflowService).createDraftWorkflow(any(JiraServiceContext.class), eq(WORKFLOW_NAME));
    }

    /**
     * Methods from WorkflowService are using the given ErrorCollection as a mutable object to pass error back to caller.
     * Since ErrorCollection is not part of the the return type, the above behaviour could not be mocked.
     *
     * This is to achieve the aforementioned behaviour via callback/Answer API from mockito.
     */
    private Answer mockErrorFromWorkflowService()
    {
        return new Answer()
        {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable
            {
                JiraServiceContext context = (JiraServiceContext) invocation.getArguments()[0];
                //Add error message to fake up an error from workflow service.
                context.getErrorCollection().addErrorMessage("fake error message from workflow service");
                return null;
            }
        };
    }
}
