package com.atlassian.jira.issue.managers;

import java.io.File;
import java.util.Map;

import com.atlassian.fugue.Either;
import com.atlassian.jira.issue.AttachmentError;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.AttachmentConstants;
import com.atlassian.jira.issue.attachment.AttachmentIdSequencer;
import com.atlassian.jira.issue.attachment.AttachmentRuntimeException;
import com.atlassian.jira.issue.attachment.AttachmentStore;
import com.atlassian.jira.issue.attachment.ThumbnailAccessor;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.junit.rules.Log4jLogger;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.mime.MimeManager;
import com.atlassian.jira.web.bean.I18nBean;
import com.atlassian.util.concurrent.Promises;

import com.google.common.collect.Lists;

import org.hamcrest.Matcher;
import org.hamcrest.collection.IsMapContaining;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.ofbiz.core.entity.GenericValue;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class TestDefaultAttachmentManagerMoveAndCopyWithExceptions
{
    public static final File NOT_EXISTENT_FILE = new File("I do not exist on filesystem but who cares");

    @Rule
    public TestRule initMockito = new InitMockitoMocks(this);
    @Rule
    public Log4jLogger expectedLog = new Log4jLogger();
    @InjectMocks
    private DefaultAttachmentManager attachmentManager;
    @Mock
    private ThumbnailAccessor thumbnailAccessor;
    @Mock
    private IssueManager issueManager;
    @Mock
    private AttachmentIdSequencer attachmentIdSequencer;
    @Mock(answer = Answers.RETURNS_MOCKS)
    private I18nBean.BeanFactory i18nBeanFactory;
    @Mock
    private OfBizDelegator ofBizDelegator;

    @SuppressWarnings ("deprecation")
    @Mock
    private AttachmentStore attachmentStore;
    @Mock
    private Issue issue;
    @Mock
    private Attachment attachment;

    private ApplicationUser user = new MockApplicationUser("admin");
    @Mock
    private GenericValue issueGV;
    @Mock
    private GenericValue attachmentGV;
    @Mock
    private MimeManager mimeManager;

    @Test
    public void shouldLogMessageWhenErrorOnMoveAttachments() throws Exception
    {
        //noinspection deprecation
        when(issue.getGenericValue()).thenReturn(issueGV);
        when(issueGV.getRelatedOrderBy(eq("ChildFileAttachment"), Mockito.anyListOf(String.class))).
                thenReturn(Lists.newArrayList(attachmentGV));
        when(attachmentGV.getEntityName()).thenReturn("ChildFileAttachment");
        when(attachmentGV.getLong("id")).thenReturn(123l);
        when(thumbnailAccessor.getThumbnailFile(eq(issue), any(Attachment.class))).thenReturn(NOT_EXISTENT_FILE);

        final String newIssueKey = "TEST-2";
        when(attachmentStore.move(any(Attachment.class), eq(newIssueKey))).
                thenReturn(Promises.<Void>rejected(new AttachmentRuntimeException("Attachment move failed")));
        //we have to prepare thumbnail directory as this should be deleted at the end
        when(thumbnailAccessor.getThumbnailDirectory(issue, false)).thenReturn(NOT_EXISTENT_FILE);

        attachmentManager.moveAttachments(issue, newIssueKey);

        assertThat(expectedLog.getMessage(), containsString("WARN - Attachment move failed"));
    }

    @Test
    public void shouldDoNothingWhenErrorOnCopyAttachments() throws Exception
    {

        final String newIssueKey = "TEST-2";
        final Long attachmentId=456123l;
        when(issueManager.getIssueObject(newIssueKey)).thenReturn(new MockIssue(1));
        when(attachmentStore.copy(eq(attachment), any(Attachment.class), eq(newIssueKey))).
                thenReturn(Promises.<Attachment>rejected(new AttachmentRuntimeException("Why you no copy")));
        when(attachmentIdSequencer.getNextId()).thenReturn(attachmentId);
        when(attachmentGV.getLong("id")).thenReturn(attachmentId);

        @SuppressWarnings ("unchecked")
        final Matcher<Map<String, Object>> fieldsMatcher = (Matcher)IsMapContaining.hasEntry("id", attachmentId);
        when(ofBizDelegator.createValue(eq(AttachmentConstants.ATTACHMENT_ENTITY_NAME), argThat( fieldsMatcher))).
                thenReturn(attachmentGV);

        final Either<AttachmentError, Attachment> copyAttachment = attachmentManager.copyAttachment(attachment, user, newIssueKey);

        assertTrue("Expected exception to be returned",copyAttachment.isLeft());
        final AttachmentError attachmentError = copyAttachment.left().get();
        final String expectedErrorMessage = "Unable to copy attachment to issue with key TEST-2. Exception: Why you no cop";
        assertThat(attachmentError.getLogMessage(), containsString(expectedErrorMessage));
        assertThat(expectedLog.getMessage(), containsString("WARN - " + expectedErrorMessage));

    }
}