package com.atlassian.jira.issue.customfields.impl;

import com.atlassian.jira.imports.project.customfield.NoTransformationCustomFieldImporter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;

/**
 * @since v3.13
 */
@RunWith (MockitoJUnitRunner.class)
public class TestReadOnlyCFType extends TestGenericTextCFType
{
    @Test
    public void testGetProjectImporter() throws Exception
    {
        ReadOnlyCFType readOnlyCFType = new ReadOnlyCFType(null, null, null, null);
        assertTrue(readOnlyCFType.getProjectImporter() instanceof NoTransformationCustomFieldImporter);
    }

    @Override
    protected GenericTextCFType getFieldTypeUnderTest()
    {
        return new ReadOnlyCFType(null, null, textFieldCharacterLengthValidator, authenticationContext);
    }
}
