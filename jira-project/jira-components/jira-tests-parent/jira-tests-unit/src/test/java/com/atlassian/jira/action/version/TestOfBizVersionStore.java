/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */
package com.atlassian.jira.action.version;

import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.project.version.OfBizVersionStore;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionImpl;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.hamcrest.collection.IsIterableWithSize;
import org.junit.Test;
import org.ofbiz.core.entity.GenericValue;

import static com.google.common.collect.Iterables.get;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@SuppressWarnings("deprecation")
public class TestOfBizVersionStore
{

    Version version1 = new VersionImpl(null, new MockGenericValue("Version", FieldMap.build("name", "version1", "id", (long) 1, "sequence", (long) 0)));
    Version version2 = new VersionImpl(null, new MockGenericValue("Version", FieldMap.build("name", "version2", "id", (long) 2, "sequence", (long) 1)));

    @Test
    public void testGetAllVersionsReturnsCorrectDBValues()
    {
        MockOfBizDelegator delegator = new MockOfBizDelegator(ImmutableList.of(version1.getGenericValue(), version2.getGenericValue()), ImmutableList.of(version1.getGenericValue(), version2.getGenericValue()));
        OfBizVersionStore versionStore = new OfBizVersionStore(delegator, null);

        Iterable<Version> versions = versionStore.getAllVersions();
        assertThat(versions, IsIterableWithSize.<Version>iterableWithSize(2));
        assertEquals(version1, get(versions, 0));
        assertEquals(version2, get(versions, 1));
        delegator.verify();
    }

    @Test
    public void testGetAllVersionsReturnsEmptyListWhenNoVersions()
    {
        MockOfBizDelegator delegator = new MockOfBizDelegator(ImmutableList.<GenericValue>of(), ImmutableList.<GenericValue>of());
        OfBizVersionStore versionStore = new OfBizVersionStore(delegator, null);

        Iterable<Version> versions = versionStore.getAllVersions();
        assertThat(versions, IsIterableWithSize.<Version>iterableWithSize(0));
        delegator.verify();
    }

    @Test
    public void testCreateVersion()
    {
        Version version1 = new VersionImpl(null, new MockGenericValue("Version", FieldMap.build("name", "version1")));
        MockOfBizDelegator delegator = new MockOfBizDelegator(ImmutableList.<GenericValue>of(), ImmutableList.of(version1.getGenericValue()));
        OfBizVersionStore versionStore = new OfBizVersionStore(delegator, null);

        Version version = versionStore.createVersion(ImmutableMap.<String, Object>of("name", "version1"));
        assertEquals("version1", version.getName());
        delegator.verify();
    }

    @Test
    public void testGetVersionReturnsCorrectDatabaseVersion()
    {
        MockOfBizDelegator delegator = new MockOfBizDelegator(ImmutableList.of(version1.getGenericValue(), version2.getGenericValue()),
                ImmutableList.of(version1.getGenericValue(), version2.getGenericValue()));
        OfBizVersionStore versionStore = new OfBizVersionStore(delegator, null);

        Version version = versionStore.getVersion(new Long(1));
        assertEquals(version1, version);
    }

    @Test
    public void testGetVersionReturnsNullIfNoVersionFound()
    {
        MockOfBizDelegator delegator = new MockOfBizDelegator(ImmutableList.<GenericValue>of(), ImmutableList.<GenericValue>of());
        OfBizVersionStore versionStore = new OfBizVersionStore(delegator, null);

        Version version = versionStore.getVersion(new Long(1));
        assertEquals(null, version);
    }

    @Test
    public void testDeleteVersionRemovesVersionFromDatabase()
    {
        MockOfBizDelegator delegator = new MockOfBizDelegator(ImmutableList.of(version1.getGenericValue(), version2.getGenericValue()),
                ImmutableList.of(version2.getGenericValue()));
        OfBizVersionStore versionStore = new OfBizVersionStore(delegator, null);

        versionStore.deleteVersion(version1);
        delegator.verify();
    }
}
