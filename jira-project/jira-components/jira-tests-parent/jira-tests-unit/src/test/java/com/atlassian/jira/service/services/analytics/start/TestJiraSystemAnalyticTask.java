package com.atlassian.jira.service.services.analytics.start;


import java.util.Map;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.system.ReleaseInfo;
import com.atlassian.jira.util.system.SystemInfoUtils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestJiraSystemAnalyticTask
{


    @Test
    public void testJiraSystemAnalytic()
    {
        BuildUtilsInfo info = mock(BuildUtilsInfo.class);
        SystemInfoUtils systemInfoUtils = mock(SystemInfoUtils.class);
        ClusterManager clusterManager = mock(ClusterManager.class);
        JiraProperties jiraProperties = mock(JiraProperties.class);

        JiraSystemAnalyticTask task = new JiraSystemAnalyticTask(info, systemInfoUtils, clusterManager, jiraProperties);

        when(info.getVersionNumbers()).thenReturn(new int[] { 6, 3, 9 });
        when(info.getCurrentBuildNumber()).thenReturn("6339");
        when(clusterManager.isClusterLicensed()).thenReturn(true);

        final Map<String, Object> analytics = task.getAnalytics();

        assertEquals(6, analytics.get("release.version"));
        assertEquals(3, analytics.get("major.version"));
        assertEquals(9, analytics.get("minor.version"));
        assertEquals("6339", analytics.get("build.number"));
        assertTrue((Boolean) analytics.get("license.dc"));

    }

}
