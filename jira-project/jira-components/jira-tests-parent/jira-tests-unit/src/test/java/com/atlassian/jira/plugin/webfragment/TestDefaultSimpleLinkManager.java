package com.atlassian.jira.plugin.webfragment;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.local.runner.ListeningMockitoRunner;
import com.atlassian.jira.plugin.webfragment.descriptors.SimpleLinkFactoryModuleDescriptor;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.SimpleLink;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.api.model.WebFragmentBuilder;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests for DefaultSimpleLinkManager
 */
@RunWith (ListeningMockitoRunner.class)
public class TestDefaultSimpleLinkManager
{
    /** A section name */
    private static final String SECTION = "section";

    @Mock
    private DynamicWebInterfaceManager dynamicWebInterfaceManager;
    @Mock
    private SimpleLinkFactoryModuleDescriptors simpleLinkFactoryModuleDescriptors;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private VelocityRequestContextFactory velocityRequestContextFactory;
    @Mock
    private User user;
    @Mock
    private WebResourceUrlProvider webResourceUrlProvider;
    private JiraHelper jiraHelper;
    private DefaultSimpleLinkManager simpleLinkManager;

    @Before
    public void setUp() throws Exception
    {
        simpleLinkManager = new DefaultSimpleLinkManager(dynamicWebInterfaceManager, simpleLinkFactoryModuleDescriptors, jiraAuthenticationContext, webResourceUrlProvider, velocityRequestContextFactory);
        jiraHelper = new JiraHelper();
    }

    @Test
    public void testGetLinksWithNoIdUsesKey() throws Exception
    {
        when(dynamicWebInterfaceManager.getDisplayableWebItems(eq(SECTION), anyMap())).thenReturn(Arrays.asList(new WebFragmentBuilder(0).id("mykey").webItem("").url("http://url").build()));
        when(simpleLinkFactoryModuleDescriptors.get()).thenReturn(Collections.<SimpleLinkFactoryModuleDescriptor>emptyList());
        when(webResourceUrlProvider.getStaticResourcePrefix("NONE", UrlMode.RELATIVE)).thenReturn("/s/none/_/");

        List<SimpleLink> links = simpleLinkManager.getLinksForSection(SECTION, user, jiraHelper);
        assertEquals(1, links.size());
        assertEquals("You have broken something Studio depends on (JRADEV-6587)", "mykey", links.get(0).getId());
    }

    @Test
    public void testGetLinksWithId() throws Exception
    {
        when(dynamicWebInterfaceManager.getDisplayableWebItems(eq(SECTION), anyMap())).thenReturn(Arrays.asList(new WebFragmentBuilder("mykey", 0).id("myid").webItem("").url("http://url").build()));
        when(simpleLinkFactoryModuleDescriptors.get()).thenReturn(Collections.<SimpleLinkFactoryModuleDescriptor>emptyList());
        when(webResourceUrlProvider.getStaticResourcePrefix("NONE", UrlMode.RELATIVE)).thenReturn("/s/none/_/");

        List<SimpleLink> links = simpleLinkManager.getLinksForSection(SECTION, user, jiraHelper);
        assertEquals(1, links.size());
        assertEquals("myid", links.get(0).getId());
    }

    @Test
    public void testIconLinkPrefix() throws Exception
    {
        when(dynamicWebInterfaceManager.getDisplayableWebItems(eq(SECTION), anyMap())).thenReturn(Arrays.asList(
                new WebFragmentBuilder("mykey", 0).id("myid").addParam("iconUrl", "blah").webItem("").url("http://url").build()));
        when(simpleLinkFactoryModuleDescriptors.get()).thenReturn(Collections.<SimpleLinkFactoryModuleDescriptor>emptyList());
        when(webResourceUrlProvider.getStaticResourcePrefix(UrlMode.RELATIVE)).thenReturn("/s/a/b/c/_/");

        List<SimpleLink> links = simpleLinkManager.getLinksForSection(SECTION, user, jiraHelper, true);
        assertEquals(1, links.size());
        assertEquals("myid", links.get(0).getId());
        assertEquals("/s/a/b/c/_/blah", links.get(0).getIconUrl());

        VelocityRequestContext vrc = mock(VelocityRequestContext.class);
        when(vrc.getBaseUrl()).thenReturn("thebaseurl/");
        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(vrc);

        links = simpleLinkManager.getLinksForSection(SECTION, user, jiraHelper, false);
        assertEquals(1, links.size());
        assertEquals("myid", links.get(0).getId());
        assertEquals("thebaseurl/blah", links.get(0).getIconUrl());
    }

    @Test
    public void testIconLinkPrefixBaseUrlNotDuplicated() throws Exception
    {
        when(dynamicWebInterfaceManager.getDisplayableWebItems(eq(SECTION), anyMap())).thenReturn(Arrays.asList(
                new WebFragmentBuilder("mykey", 0).id("myid").addParam("iconUrl", "thebaseurl/blah").webItem("").url("http://url").build()));
        when(simpleLinkFactoryModuleDescriptors.get()).thenReturn(Collections.<SimpleLinkFactoryModuleDescriptor>emptyList());
        when(webResourceUrlProvider.getStaticResourcePrefix(UrlMode.RELATIVE)).thenReturn("/s/a/b/c/_/");

        VelocityRequestContext vrc = mock(VelocityRequestContext.class);
        when(vrc.getBaseUrl()).thenReturn("thebaseurl/");
        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(vrc);

        final List<SimpleLink> links = simpleLinkManager.getLinksForSection(SECTION, user, jiraHelper, false);
        assertEquals(1, links.size());
        assertEquals("myid", links.get(0).getId());
        assertEquals("thebaseurl/blah", links.get(0).getIconUrl());
    }

    /**
     * If a plugin module throws a RuntimeException, the DefaultSimpleLinkManager should gracefully ignore the module.
     */
    @Test
    public void testPluginModuleExceptionIsHandled()
    {
        when(dynamicWebInterfaceManager.getDisplayableWebItems(eq(SECTION), anyMap())).thenReturn(Collections.<WebItem>emptyList());
        final SimpleLinkFactoryModuleDescriptor descriptor = createModuleThatThrows(new RuntimeException());
        when(simpleLinkFactoryModuleDescriptors.get()).thenReturn(Collections.singleton(descriptor));
        final List<SimpleLink> links = simpleLinkManager.getLinksForSection(SECTION, user, jiraHelper, false);
        assertThat("No links are added when plugin module throws a RuntimeException, and the exception is not re-thrown", links, hasSize(0));
    }

    /**
     * If a plugin module throws a LinkageError (e.g. AbstractMethodError) due to binary incompatibility,  the
     * DefaultSimpleLinkManager should gracefully ignore the module.
     */
    @Test
    public void testPluginModuleLinkageErrorIsHandled()
    {
        when(dynamicWebInterfaceManager.getDisplayableWebItems(eq(SECTION), anyMap())).thenReturn(Collections.<WebItem>emptyList());
        final SimpleLinkFactoryModuleDescriptor descriptor = createModuleThatThrows(new LinkageError());
        when(simpleLinkFactoryModuleDescriptors.get()).thenReturn(Collections.singleton(descriptor));
        try
        {
            final List<SimpleLink> links = simpleLinkManager.getLinksForSection(SECTION, user, jiraHelper, false);
            assertThat("No links are added when plugin module throws LinkageError, and the error is not re-thrown", links, hasSize(0));
        }
        catch (LinkageError e)
        {
            // Let's not blow up the whole test run by throwing an Error if this test fails.
            fail("LinkageError from plugin module was not handled gracefully");
        }
    }

    /**
     * If a plugin module throws a RuntimeException, subsequent modules can still add links.
     */
    @Test
    public void testSubsequentLinksAreAddedDespiteException()
    {
        when(dynamicWebInterfaceManager.getDisplayableWebItems(eq(SECTION), anyMap())).thenReturn(Collections.<WebItem>emptyList());
        final SimpleLinkFactoryModuleDescriptor brokenModuleDescriptor = createModuleThatThrows(new RuntimeException());
        final SimpleLinkFactoryModuleDescriptor workingModuleDescriptor = createModuleThatReturnsALink();
        when(simpleLinkFactoryModuleDescriptors.get()).thenReturn(Arrays.asList(brokenModuleDescriptor, workingModuleDescriptor));
        final List<SimpleLink> links = simpleLinkManager.getLinksForSection(SECTION, user, jiraHelper, false);
        assertThat("Links from subsequent plugins are added even after a plugin throws an Exception", links, hasSize(1));
    }

    /**
     * Create a module descriptor that provides a module that throws an exception when getLinks() is called
     * @return the module descriptor
     */
    private SimpleLinkFactoryModuleDescriptor createModuleThatThrows(final Throwable throwable)
    {
        final SimpleLinkFactoryModuleDescriptor descriptor = mock(SimpleLinkFactoryModuleDescriptor.class);
        when(descriptor.getSection()).thenReturn(SECTION);
        final SimpleLinkFactory linkFactory = mock(SimpleLinkFactory.class);
        when(linkFactory.getLinks(eq(user), anyMap())).thenThrow(throwable);
        when(descriptor.getModule()).thenReturn(linkFactory);
        return descriptor;
    }

    /**
     * Create a module descriptor that provides a module that returns a single link
     * @return the module descriptor
     */
    private SimpleLinkFactoryModuleDescriptor createModuleThatReturnsALink()
    {
        final SimpleLinkFactoryModuleDescriptor descriptor = mock(SimpleLinkFactoryModuleDescriptor.class);
        when(descriptor.getSection()).thenReturn(SECTION);
        final SimpleLinkFactory linkFactory = mock(SimpleLinkFactory.class);
        when(linkFactory.getLinks(eq(user), anyMap())).thenReturn(Collections.singletonList(mock(SimpleLink.class)));
        when(descriptor.getModule()).thenReturn(linkFactory);
        return descriptor;
    }
}
