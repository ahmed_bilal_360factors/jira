package com.atlassian.jira.issue.attachment.store;

import java.io.InputStream;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.AttachmentStreamGetData;
import com.atlassian.jira.issue.attachment.StoreAttachmentBean;
import com.atlassian.jira.issue.attachment.StoreAttachmentResult;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.jira.issue.attachment.store.strategy.StoreOperationStrategy;
import com.atlassian.jira.issue.attachment.store.strategy.get.AttachmentGetStrategy;
import com.atlassian.jira.issue.attachment.store.strategy.move.MoveTemporaryAttachmentStrategy;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class TestDualSendingAttachmentStore
{
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private StreamAttachmentStore primaryAttachmentStore;
    @Mock
    private StreamAttachmentStore secondaryAttachmentStore;
    @Mock
    private AttachmentGetStrategy attachmentGetStrategy;
    @Mock
    private MoveTemporaryAttachmentStrategy moveTemporaryAttachmentStrategy;
    @Mock
    private StoreOperationStrategy operationStrategy;

    private DualSendingAttachmentStore dualSendingAttachmentStore;

    @Before
    public void setUp()
    {
        dualSendingAttachmentStore = new DualSendingAttachmentStore(
                primaryAttachmentStore,
                secondaryAttachmentStore,
                attachmentGetStrategy,
                moveTemporaryAttachmentStrategy,
                operationStrategy);
    }

    @Test
    public void testPutAttachmentCreatesTempAndMovesItToDestinationKey() throws Exception
    {
        final StoreAttachmentBean storeBean = getStoreAttachmentBean();

        final TemporaryAttachmentId tempFileId = TemporaryAttachmentId.fromString("temp-file-id");
        when(primaryAttachmentStore.putTemporaryAttachment(storeBean.getStream(), storeBean.getSize()))
                .thenReturn(Promises.promise(tempFileId));

        when(moveTemporaryAttachmentStrategy.moveTemporaryToAttachment(tempFileId, storeBean.getAttachmentKey()))
                .thenReturn(Promises.promise(Unit.VALUE));

        final StoreAttachmentResult result = dualSendingAttachmentStore.putAttachment(storeBean).claim();

        assertThat(result, equalTo(StoreAttachmentResult.created()));
        verifyZeroInteractions(secondaryAttachmentStore);
    }

    @Test
    public void shouldUseProvidedGetStrategyToRetrieveAttachmentData() throws Exception
    {
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();
        final Function<AttachmentGetData, String> processor = mock(Function.class);

        final String expectedResultString = "expectedResultString";
        when(attachmentGetStrategy.getAttachmentData(attachmentKey, processor))
                .thenReturn(Promises.promise(expectedResultString));

        final String result = dualSendingAttachmentStore.getAttachmentData(attachmentKey, processor).claim();

        assertThat(result, equalTo(expectedResultString));
    }

    @Test
    public void shouldUseProvidedGetStrategyToRetrieveAttachmentStream() throws Exception
    {
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();
        final Function<InputStream, String> processor = mock(Function.class);

        final String expectedResultString = "expectedResultString";
        final InputStream mockAttachmentStream = mock(InputStream.class);

        when(processor.get(mockAttachmentStream)).thenReturn(expectedResultString);

        doAnswer(new Answer()
        {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable
            {
                final Object[] arguments = invocation.getArguments();
                final Function<AttachmentGetData, String> processor = (Function<AttachmentGetData, String>) arguments[1];
                return Promises.promise(processor.get(new AttachmentStreamGetData(mockAttachmentStream, 0)));
            }
        }).when(attachmentGetStrategy).getAttachmentData(eq(attachmentKey), any(Function.class));

        final String result = dualSendingAttachmentStore.getAttachment(attachmentKey, processor).claim();

        assertThat(result, equalTo(expectedResultString));
    }

    @Test
    public void testMoveAttachmentUsesOperationStrategy() throws Exception
    {
        final AttachmentKey oldAttachmentKey = MockAttachmentKeys.getKey(1l);
        final AttachmentKey newAttachmentKey = MockAttachmentKeys.getKey(2l);

        final Promise<Unit> expectedPromise = Promises.promise(Unit.VALUE);
        when(primaryAttachmentStore.moveAttachment(oldAttachmentKey, newAttachmentKey))
                .thenReturn(expectedPromise);
        invokeFunctionOnFirstStoreForOperation();

        final Promise<Unit> result = dualSendingAttachmentStore.moveAttachment(oldAttachmentKey, newAttachmentKey);

        assertThat(result, equalTo(expectedPromise));
        verify(primaryAttachmentStore).moveAttachment(oldAttachmentKey, newAttachmentKey);
        verifyZeroInteractions(secondaryAttachmentStore);
    }

    @Test
    public void testCopyAttachmentUsesOperationStrategy() throws Exception
    {
        final AttachmentKey oldAttachmentKey = MockAttachmentKeys.getKey(1l);
        final AttachmentKey newAttachmentKey = MockAttachmentKeys.getKey(2l);

        final Promise<Unit> expectedPromise = Promises.promise(Unit.VALUE);
        when(primaryAttachmentStore.copyAttachment(oldAttachmentKey, newAttachmentKey))
                .thenReturn(expectedPromise);
        //noinspection unchecked
        invokeFunctionOnFirstStoreForOperation();

        final Promise<Unit> result = dualSendingAttachmentStore.copyAttachment(oldAttachmentKey, newAttachmentKey);

        assertThat(result, equalTo(expectedPromise));
        verify(primaryAttachmentStore).copyAttachment(oldAttachmentKey, newAttachmentKey);
        verifyZeroInteractions(secondaryAttachmentStore);
    }

    @Test
    public void testDeleteAttachmentUsesOperationStrategy() throws Exception
    {
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey(1l);

        final Promise<Unit> expectedPromise = Promises.promise(Unit.VALUE);
        when(primaryAttachmentStore.deleteAttachment(attachmentKey))
                .thenReturn(expectedPromise);
        //noinspection unchecked
        invokeFunctionOnFirstStoreForOperation();

        final Promise<Unit> result = dualSendingAttachmentStore.deleteAttachment(attachmentKey);

        assertThat(result, equalTo(expectedPromise));
        verify(primaryAttachmentStore).deleteAttachment(attachmentKey);
        verifyZeroInteractions(secondaryAttachmentStore);
    }

    @Test
    public void testCreateTemporaryAttachmentCreatesOnlyInPrimary() throws Exception
    {
        final InputStream mockStream = mock(InputStream.class);
        final long size = 123l;

        final TemporaryAttachmentId tempFileId = TemporaryAttachmentId.fromString("temp-file-id");
        when(primaryAttachmentStore.putTemporaryAttachment(mockStream, size))
                .thenReturn(Promises.promise(tempFileId));

        final TemporaryAttachmentId result = dualSendingAttachmentStore.putTemporaryAttachment(mockStream, size).claim();

        assertThat(result, equalTo(tempFileId));
        verifyNoMoreInteractions(secondaryAttachmentStore);
    }

    @Test
    public void testDeleteTemporaryPerformedInPrimaryOnly() throws Exception
    {
        final TemporaryAttachmentId tempFileId = TemporaryAttachmentId.fromString("temp-file-id");

        final Promise<Unit> expectedPromise = Promises.promise(Unit.VALUE);
        when(primaryAttachmentStore.deleteTemporaryAttachment(tempFileId))
                .thenReturn(expectedPromise);

        final Promise<Unit> result = dualSendingAttachmentStore.deleteTemporaryAttachment(tempFileId);

        assertThat(result, equalTo(expectedPromise));
        verifyZeroInteractions(secondaryAttachmentStore);
    }

    @Test
    public void testExistsDelegatesToGetStrategy() throws Exception
    {
        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey();

        final Boolean expectedResult = true;
        when(attachmentGetStrategy.exists(attachmentKey))
                .thenReturn(Promises.promise(expectedResult));

        final Promise<Boolean> result = dualSendingAttachmentStore.exists(attachmentKey);

        assertThat(result.claim(), equalTo(expectedResult));
    }

    @Test
    public void testErrorsCombinedFromBothStores() throws Exception
    {
        final ErrorCollection primaryErrors = new SimpleErrorCollection();
        primaryErrors.addErrorMessage("primaryError");

        when(primaryAttachmentStore.errors())
                .thenReturn(Option.some(primaryErrors));

        final ErrorCollection secondaryErrors = new SimpleErrorCollection();
        secondaryErrors.addErrorMessage("secondaryError");
        when(secondaryAttachmentStore.errors())
                .thenReturn(Option.some(secondaryErrors));

        final ErrorCollection errorCollection = dualSendingAttachmentStore.errors().get();

        assertThat(errorCollection.getErrorMessages(), containsInAnyOrder("primaryError", "secondaryError"));
    }

    @Test
    public void testErrorsFromPrimaryIfNotErrorsInSecondary() throws Exception
    {
        final ErrorCollection primaryErrors = new SimpleErrorCollection();
        primaryErrors.addErrorMessage("primaryError");

        when(primaryAttachmentStore.errors())
                .thenReturn(Option.some(primaryErrors));

        when(secondaryAttachmentStore.errors())
                .thenReturn(Option.<ErrorCollection>none());

        final ErrorCollection errorCollection = dualSendingAttachmentStore.errors().get();

        assertThat(errorCollection.getErrorMessages(), containsInAnyOrder("primaryError"));
    }

    @Test
    public void testErrorsFromSecondaryIfNotErrorsInPrimary() throws Exception
    {
        when(primaryAttachmentStore.errors())
                .thenReturn(Option.<ErrorCollection>none());

        final ErrorCollection secondaryErrors = new SimpleErrorCollection();
        secondaryErrors.addErrorMessage("secondaryError");
        when(secondaryAttachmentStore.errors())
                .thenReturn(Option.some(secondaryErrors));

        final ErrorCollection errorCollection = dualSendingAttachmentStore.errors().get();

        assertThat(errorCollection.getErrorMessages(), containsInAnyOrder("secondaryError"));
    }

    private void invokeFunctionOnFirstStoreForOperation()
    {

        final Answer<Promise<Object>> answer = new Answer<Promise<Object>>()
        {
            @Override
            public Promise<Object> answer(final InvocationOnMock invocation) throws Throwable
            {
                //noinspection unchecked
                return (Promise<Object>) ((Function) invocation.getArguments()[0]).get(primaryAttachmentStore);
            }
        };
        //noinspection unchecked
        when(operationStrategy.perform(Mockito.any(Function.class)))
                .thenAnswer(answer);
    }

    private StoreAttachmentBean getStoreAttachmentBean()
    {
        final long id = 123;
        final Long size = 123l;
        final InputStream mockStream = mock(InputStream.class);
        return new StoreAttachmentBean.Builder(mockStream)
                .withId(id)
                .withFileName("fileName")
                .withIssueKey("FOO-1")
                .withOriginalProjectKey("FOO")
                .withSize(size)
                .build();
    }
}