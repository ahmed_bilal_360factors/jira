package com.atlassian.jira.user.flag;

import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

class JsonLongMatcher extends BaseMatcher<String>
{
    private String key;
    private long value;

    public JsonLongMatcher(String key, long value)
    {
        this.key = key;
        this.value = value;
    }

    @Override
    public boolean matches(Object item)
    {
        if (item instanceof String)
        {
            try
            {
                JSONObject jsonObject = new JSONObject((String) item);
                return value == jsonObject.getLong(key);
            }
            catch (JSONException e)
            {
                return false;
            }
        }

        return false;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText(
                "a JSON object containing the entry "
                        + key + " : " + value
                        + " at the top level");
    }
}
