package com.atlassian.jira.jql.validator;

import java.util.Collections;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.Operand;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestValuesExistValidator
{
    @Mock
    private JqlOperandResolver jqlOperandResolver;

    @Mock
    private I18nHelper.BeanFactory beanFactory;

    @Rule
    public InitMockitoMocks mocks = new InitMockitoMocks(this);

    private final MessageSet.Level level = MessageSet.Level.WARNING;

    private ValuesExistValidator valuesExistValidator;

    @Before
    public void setup()
    {
        valuesExistValidator = new TestedValuesExistValidator(jqlOperandResolver, beanFactory, level);
    }

    @Test
    public void testValidateAnonymousUser()
    {
        final TerminalClause clause = mock(TerminalClause.class);
        final QueryCreationContext queryCreationContext = mock(QueryCreationContext.class);
        final User searcher = mock(User.class);

        when(jqlOperandResolver.getValues(same(queryCreationContext), any(Operand.class), same(clause))).thenReturn(Collections.<QueryLiteral>emptyList());
        when(jqlOperandResolver.getValues(same(searcher), any(Operand.class), same(clause))).thenReturn(Collections.<QueryLiteral>emptyList());

        when(jqlOperandResolver.isValidOperand(any(Operand.class))).thenReturn(false);

        final MessageSet validateWithQueryCreationContextResult = valuesExistValidator.validate(clause, queryCreationContext);
        final MessageSet validateWithoutQueryCreationContextResult = valuesExistValidator.validate(searcher, clause);

        assertFalse(validateWithQueryCreationContextResult.hasAnyErrors());
        assertFalse(validateWithoutQueryCreationContextResult.hasAnyErrors());
    }

    public static class TestedValuesExistValidator extends ValuesExistValidator
    {

        TestedValuesExistValidator(final JqlOperandResolver operandResolver, final I18nHelper.BeanFactory beanFactory, final MessageSet.Level level)
        {
            super(operandResolver, beanFactory, level);
        }

        @Override
        boolean stringValueExists(final User searcher, final String value)
        {
            return false;
        }

        @Override
        boolean longValueExist(final User searcher, final Long value)
        {
            return false;
        }
    }
}