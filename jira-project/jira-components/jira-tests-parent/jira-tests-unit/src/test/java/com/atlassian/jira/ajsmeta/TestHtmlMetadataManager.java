package com.atlassian.jira.ajsmeta;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mail.settings.MailSettings;
import com.atlassian.jira.mock.MockFeatureManager;
import com.atlassian.jira.mock.MockMailSettings;
import com.atlassian.jira.mock.security.MockAuthenticationContext;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.plugin.keyboardshortcut.KeyboardShortcutManager;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.velocity.MockVelocityRequestContextFactory;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.jira.web.util.ProductVersionDataBean;
import com.atlassian.jira.web.util.ProductVersionDataBeanProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestHtmlMetadataManager
{
    HtmlMetadataManager htmlMetadataManager;

    @Mock
    ApplicationProperties applicationProperties;
    @Mock
    BuildUtilsInfo buildUtilsInfo;
    FeatureManager featureManager;
    JiraAuthenticationContext authenticationContext;
    @Mock
    JiraProperties jiraSystemProperties;
    @Mock
    JiraWebResourceManager jiraWebResourceManager;
    @Mock
    KeyboardShortcutManager keyboardShortcutManager;
    MailSettings mailSettings;
    @Mock
    ProductVersionDataBeanProvider productVersionDataBeanProvider;
    @Mock
    PermissionManager permissionManager;
    VelocityRequestContextFactory velocityRequestContextFactory;

    @Rule
    public InitMockitoMocks initMocks = new InitMockitoMocks(this);

    @Before
    public void setup()
    {
        authenticationContext = new MockAuthenticationContext(new MockUser("someone"));
        featureManager = new MockFeatureManager();
        mailSettings = new MockMailSettings();
        velocityRequestContextFactory = new MockVelocityRequestContextFactory("http://issues.example.com/jira");
        when(productVersionDataBeanProvider.get()).thenReturn(mock(ProductVersionDataBean.class));
        ExecutingHttpRequest.set(new MockHttpServletRequest(), null);

        htmlMetadataManager = new HtmlMetadataManager(jiraWebResourceManager, buildUtilsInfo, authenticationContext,
                applicationProperties, keyboardShortcutManager, velocityRequestContextFactory, featureManager,
                productVersionDataBeanProvider, permissionManager, mailSettings, jiraSystemProperties);
    }

    @After
    public void tearDown()
    {
        ExecutingHttpRequest.clear();
    }

    @Test
    public void includeMetadataShouldWriteMetaTagsWithNamesPrefixedWithAjs() throws Exception
    {
        when(jiraWebResourceManager.getMetadata()).thenReturn(MapBuilder.build("key", "value"));
        StringWriter writer = new StringWriter();
        htmlMetadataManager.includeMetadata(writer);
        assertThat(writer.toString(), is("<meta name=\"ajs-key\" content=\"value\">\n"));
    }

    @Test
    public void requireCommonMetadataShouldPutSomeStuffIntoTheWebResourceManager()
    {
        htmlMetadataManager.requireCommonMetadata();
        verify(jiraWebResourceManager, atLeastOnce()).putMetadata(anyString(), anyString());
    }
}
