package com.atlassian.jira.index.request;

import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class TestDefaultReindexRequestCoalescer
{
    private DefaultReindexRequestCoalescer coalescer;

    @Before
    public void setUp()
    {
        coalescer = new DefaultReindexRequestCoalescer();
    }

    @Test
    public void testWithNoRequests()
    {
        List<ReindexRequest> results = coalescer.coalesce(ImmutableList.<ReindexRequest>of());
        assertThat(results, hasSize(0));
    }

    @Test
    public void testSingleRequest()
    {
        ReindexRequest request = new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, 1000L, null, null, null,
                                        ReindexStatus.PENDING,
                                        EnumSet.of(AffectedIndex.ISSUE), EnumSet.noneOf(SharedEntityType.class));
        List<ReindexRequest> requests = ImmutableList.of(request);
        List<ReindexRequest> results = coalescer.coalesce(requests);
        assertThat(requests, equalTo(results));
    }

    /**
     * Simple coalescence test that combined two requests differing only by ID.
     */
    @Test
    public void testCombination()
    {
        ReindexRequest request1 = new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.ISSUE), EnumSet.noneOf(SharedEntityType.class));
        ReindexRequest request2 = new ReindexRequest(2L, ReindexRequestType.IMMEDIATE, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.ISSUE), EnumSet.noneOf(SharedEntityType.class));
        List<ReindexRequest> requests = ImmutableList.of(request1, request2);
        List<ReindexRequest> results = coalescer.coalesce(requests);

        assertThat(results, hasSize(1));
        ReindexRequest combined = results.get(0);

        assertNull("Combined request should not have ID.", combined.getId());
        assertThat("Wrong request type.", ReindexRequestType.IMMEDIATE, equalTo(combined.getType()));
        assertThat("Wrong request time.", 1000L, equalTo(combined.getRequestTime()));
        assertNull("Should not have start time.", combined.getStartTime());
        assertNull("Should not have completion time.", combined.getCompletionTime());
        assertThat("Wrong status.", ReindexStatus.PENDING, equalTo(combined.getStatus()));
        assertThat("Wrong affected indexes.", EnumSet.of(AffectedIndex.ISSUE), equalTo(combined.getAffectedIndexes()));
        assertThat("Wrong shared entities.", EnumSet.noneOf(SharedEntityType.class), equalTo(combined.getSharedEntities()));

        //Check sources
        assertThat(combined.getSources(), hasSize(2));
        assertThat(combined.getSources(), containsInAnyOrder(request1, request2));
    }

    /**
     * Tests that affected indexes normalization is working.
     */
    @Test
    public void testNormalizedAffectedIndexes()
    {
        ReindexRequest request1 = new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.CHANGEHISTORY, AffectedIndex.COMMENT, AffectedIndex.ISSUE, AffectedIndex.WORKLOG), EnumSet.noneOf(SharedEntityType.class));
        ReindexRequest request2 = new ReindexRequest(2L, ReindexRequestType.IMMEDIATE, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.ALL), EnumSet.noneOf(SharedEntityType.class));
        List<ReindexRequest> requests = ImmutableList.of(request1, request2);
        List<ReindexRequest> results = coalescer.coalesce(requests);

        assertThat(results, hasSize(1));
        ReindexRequest combined = results.get(0);

        assertNull("Combined request should not have ID.", combined.getId());
        assertThat("Wrong request type.", ReindexRequestType.IMMEDIATE, equalTo(combined.getType()));
        assertThat("Wrong request time.", 1000L, equalTo(combined.getRequestTime()));
        assertNull("Should not have start time.", combined.getStartTime());
        assertNull("Should not have completion time.", combined.getCompletionTime());
        assertThat("Wrong status.", ReindexStatus.PENDING, equalTo(combined.getStatus()));
        assertThat("Wrong affected indexes.", EnumSet.of(AffectedIndex.CHANGEHISTORY, AffectedIndex.COMMENT, AffectedIndex.ISSUE, AffectedIndex.WORKLOG), equalTo(combined.getAffectedIndexes()));
        assertThat("Wrong shared entities.", EnumSet.noneOf(SharedEntityType.class), equalTo(combined.getSharedEntities()));

        //Check sources
        assertThat(combined.getSources(), hasSize(2));
        ReindexRequest normalizedRequest2 = new ReindexRequest(2L, ReindexRequestType.IMMEDIATE, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.CHANGEHISTORY, AffectedIndex.COMMENT, AffectedIndex.ISSUE, AffectedIndex.WORKLOG), EnumSet.noneOf(SharedEntityType.class));
        assertThat(combined.getSources(), containsInAnyOrder(request1, normalizedRequest2));
    }

    /**
     * Tests that affected indexes normalization is working with at least one shared entity.
     */
    @Test
    public void testNormalizedAffectedIndexesWithSharedEntity()
    {
        ReindexRequest request1 = new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.CHANGEHISTORY, AffectedIndex.COMMENT, AffectedIndex.ISSUE, AffectedIndex.WORKLOG, AffectedIndex.SHAREDENTITY), EnumSet.of(SharedEntityType.PORTAL_PAGE));
        ReindexRequest request2 = new ReindexRequest(2L, ReindexRequestType.IMMEDIATE,  1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.CHANGEHISTORY, AffectedIndex.COMMENT, AffectedIndex.ISSUE, AffectedIndex.WORKLOG), EnumSet.of(SharedEntityType.PORTAL_PAGE));
        List<ReindexRequest> requests = ImmutableList.of(request1, request2);
        List<ReindexRequest> results = coalescer.coalesce(requests);

        assertThat(results, hasSize(1));
        ReindexRequest combined = results.get(0);

        assertNull("Combined request should not have ID.", combined.getId());
        assertThat("Wrong request type.", ReindexRequestType.IMMEDIATE, equalTo(combined.getType()));
        assertThat("Wrong request time.", 1000L, equalTo(combined.getRequestTime()));
        assertNull("Should not have start time.", combined.getStartTime());
        assertNull("Should not have completion time.", combined.getCompletionTime());
        assertThat("Wrong status.", ReindexStatus.PENDING, equalTo(combined.getStatus()));
        assertThat("Wrong affected indexes.", EnumSet.of(AffectedIndex.CHANGEHISTORY, AffectedIndex.COMMENT, AffectedIndex.ISSUE, AffectedIndex.WORKLOG, AffectedIndex.SHAREDENTITY), equalTo(combined.getAffectedIndexes()));
        assertThat("Wrong shared entities.", EnumSet.of(SharedEntityType.PORTAL_PAGE), equalTo(combined.getSharedEntities()));

        //Check sources
        assertThat(combined.getSources(), hasSize(2));
        ReindexRequest normalizedRequest2 = new ReindexRequest(2L, ReindexRequestType.IMMEDIATE, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.CHANGEHISTORY, AffectedIndex.COMMENT, AffectedIndex.ISSUE, AffectedIndex.WORKLOG, AffectedIndex.SHAREDENTITY), EnumSet.of(SharedEntityType.PORTAL_PAGE));
        assertThat(combined.getSources(), containsInAnyOrder(request1, normalizedRequest2));
    }

    /**
     * Tests that affected indexes normalization is working with at least one shared entity.
     */
    @Test
    public void testNormalizedAffectedIndexesWithNoneSharedEntity()
    {
        ReindexRequest request1 = new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.CHANGEHISTORY, AffectedIndex.COMMENT, AffectedIndex.ISSUE, AffectedIndex.WORKLOG), EnumSet.of(SharedEntityType.NONE));
        ReindexRequest request2 = new ReindexRequest(2L, ReindexRequestType.IMMEDIATE, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.CHANGEHISTORY, AffectedIndex.COMMENT, AffectedIndex.ISSUE, AffectedIndex.WORKLOG), EnumSet.noneOf(SharedEntityType.class));
        List<ReindexRequest> requests = ImmutableList.of(request1, request2);
        List<ReindexRequest> results = coalescer.coalesce(requests);

        assertThat(results, hasSize(1));
        ReindexRequest combined = results.get(0);

        assertNull("Combined request should not have ID.", combined.getId());
        assertThat("Wrong request type.", ReindexRequestType.IMMEDIATE, equalTo(combined.getType()));
        assertThat("Wrong request time.", 1000L, equalTo(combined.getRequestTime()));
        assertNull("Should not have start time.", combined.getStartTime());
        assertNull("Should not have completion time.", combined.getCompletionTime());
        assertThat("Wrong status.", ReindexStatus.PENDING, equalTo(combined.getStatus()));
        assertThat("Wrong affected indexes.", EnumSet.of(AffectedIndex.CHANGEHISTORY, AffectedIndex.COMMENT, AffectedIndex.ISSUE, AffectedIndex.WORKLOG), equalTo(combined.getAffectedIndexes()));
        assertThat("Wrong shared entities.", EnumSet.noneOf(SharedEntityType.class), equalTo(combined.getSharedEntities()));

        //Check sources
        assertThat(combined.getSources(), hasSize(2));
        ReindexRequest normalizedRequest1 = new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.CHANGEHISTORY, AffectedIndex.COMMENT, AffectedIndex.ISSUE, AffectedIndex.WORKLOG), EnumSet.noneOf(SharedEntityType.class));
        assertThat(combined.getSources(), containsInAnyOrder(normalizedRequest1, request2));
    }

    /**
     * An enclosing immediate request with potential child delayed request can be combined.
     */
    @Test
    public void testCombineEnclosingImmediate()
    {
        ReindexRequest request1 = new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.ISSUE, AffectedIndex.COMMENT), EnumSet.noneOf(SharedEntityType.class));
        ReindexRequest request2 = new ReindexRequest(2L, ReindexRequestType.DELAYED, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.ISSUE), EnumSet.noneOf(SharedEntityType.class));
        List<ReindexRequest> requests = ImmutableList.of(request1, request2);
        List<ReindexRequest> results = coalescer.coalesce(requests);

        assertThat(results, hasSize(1));
        ReindexRequest combined = results.get(0);

        assertNull("Combined request should not have ID.", combined.getId());
        assertThat("Wrong request type.", ReindexRequestType.IMMEDIATE, equalTo(combined.getType()));
        assertThat("Wrong request time.", 1000L, equalTo(combined.getRequestTime()));
        assertNull("Should not have start time.", combined.getStartTime());
        assertNull("Should not have completion time.", combined.getCompletionTime());
        assertThat("Wrong status.", ReindexStatus.PENDING, equalTo(combined.getStatus()));
        assertThat("Wrong affected indexes.", EnumSet.of(AffectedIndex.ISSUE, AffectedIndex.COMMENT), equalTo(combined.getAffectedIndexes()));
        assertThat("Wrong shared entities.", EnumSet.noneOf(SharedEntityType.class), equalTo(combined.getSharedEntities()));

        //Check sources
        assertThat(combined.getSources(), hasSize(2));
        assertThat(combined.getSources(), containsInAnyOrder(request1, request2));
    }

    /**
     * An enclosing immediate request with potential child delayed request can be combined, this time in reversed order.
     */
    @Test
    public void testCombineEnclosingImmediateReversedOrder()
    {
        ReindexRequest request1 = new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.ISSUE, AffectedIndex.COMMENT), EnumSet.noneOf(SharedEntityType.class));
        ReindexRequest request2 = new ReindexRequest(2L, ReindexRequestType.DELAYED, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.ISSUE), EnumSet.noneOf(SharedEntityType.class));
        List<ReindexRequest> requests = ImmutableList.of(request2, request1);
        List<ReindexRequest> results = coalescer.coalesce(requests);

        assertThat(results, hasSize(1));
        ReindexRequest combined = results.get(0);

        assertNull("Combined request should not have ID.", combined.getId());
        assertThat("Wrong request type.", ReindexRequestType.IMMEDIATE, equalTo(combined.getType()));
        assertThat("Wrong request time.", 1000L, equalTo(combined.getRequestTime()));
        assertNull("Should not have start time.", combined.getStartTime());
        assertNull("Should not have completion time.", combined.getCompletionTime());
        assertThat("Wrong status.", ReindexStatus.PENDING, equalTo(combined.getStatus()));
        assertThat("Wrong affected indexes.", EnumSet.of(AffectedIndex.ISSUE, AffectedIndex.COMMENT), equalTo(combined.getAffectedIndexes()));
        assertThat("Wrong shared entities.", EnumSet.noneOf(SharedEntityType.class), equalTo(combined.getSharedEntities()));

        //Check sources
        assertThat(combined.getSources(), hasSize(2));
        assertThat(combined.getSources(), containsInAnyOrder(request1, request2));
    }

    /**
     * We have immediate reindex for issue and delayed reindex for issue and changehistory.
     * Coalescer should realize that issue is already done in immediate and spit out the second delayed task
     * that only does changehistory.
     */
    @Test
    public void testPartialDelayedWorkGoesToImmediate()
    {
        ReindexRequest request1 = new ReindexRequest(1L, ReindexRequestType.IMMEDIATE, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.ISSUE), EnumSet.noneOf(SharedEntityType.class));
        ReindexRequest request2 = new ReindexRequest(2L, ReindexRequestType.DELAYED, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.ISSUE, AffectedIndex.CHANGEHISTORY), EnumSet.noneOf(SharedEntityType.class));

        List<ReindexRequest> requests = ImmutableList.of(request1, request2);
        List<ReindexRequest> results = coalescer.coalesce(requests);

        assertThat(results, hasSize(2));

        ReindexRequest result1 = results.get(0);
        ReindexRequest result2 = results.get(1);

        assertThat(request1, equalTo(result1));

        //Second request should be modified to do less work
        assertThat(request2.getId(), equalTo(result2.getId()));
        assertThat(ReindexRequestType.DELAYED, equalTo(result2.getType()));
        assertThat(EnumSet.of(AffectedIndex.CHANGEHISTORY), equalTo(result2.getAffectedIndexes()));
    }

    /**
     * Same as previous test but queue the requests in the opposite order.
     */
    @Test
    public void testPartialDelayedWorkGoesToImmediateReverseOrder()
    {
        ReindexRequest request1 = new ReindexRequest(1L, ReindexRequestType.DELAYED, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.ISSUE, AffectedIndex.COMMENT), EnumSet.noneOf(SharedEntityType.class));
        ReindexRequest request2 = new ReindexRequest(2L, ReindexRequestType.IMMEDIATE, 1000L, null, null, null,
                ReindexStatus.PENDING,
                EnumSet.of(AffectedIndex.ISSUE), EnumSet.noneOf(SharedEntityType.class));
        List<ReindexRequest> requests = ImmutableList.of(request1, request2);
        List<ReindexRequest> results = coalescer.coalesce(requests);

        //Should be changed into an immediate request that performs issue and a delayed request that does comment only
        assertThat(results, hasSize(2));
        assertThat(request2, equalTo(results.get(0)));
        assertThat(request1.getId(), equalTo(results.get(1).getId()));
        assertThat(ReindexRequestType.DELAYED, equalTo(results.get(1).getType()));
        assertThat(EnumSet.of(AffectedIndex.COMMENT), equalTo(results.get(1).getAffectedIndexes()));
    }
}
