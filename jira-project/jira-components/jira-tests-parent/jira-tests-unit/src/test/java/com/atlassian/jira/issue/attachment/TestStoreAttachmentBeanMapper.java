package com.atlassian.jira.issue.attachment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.atlassian.fugue.Either;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.IOUtil;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestStoreAttachmentBeanMapper
{

    private final StoreAttachmentBeanMapper storeAttachmentBeanMapper = new StoreAttachmentBeanMapper();

    @Test
    public void testMapToBeanFromFileOpensStream() throws Exception
    {
        final File file = File.createTempFile("test", "temp");
        file.deleteOnExit();

        final String expectedStreamContent = "test content";
        writeExpectedContentToTemp(file, expectedStreamContent);

        final Attachment attachment = mockAttachmentObject();

        final Either<FileNotFoundException, StoreAttachmentBean> result = storeAttachmentBeanMapper.mapToBean(attachment, file);

        final StoreAttachmentBean storeAttachmentBean = result.right().get();
        final InputStream stream = storeAttachmentBean.getStream();
        final String streamContent = IOUtil.toString(stream, "UTF-8");

        assertThat(streamContent, equalTo(expectedStreamContent));
        assertAttachmentMetadata(result.right().get(), attachment);
    }

    private void writeExpectedContentToTemp(final File file, final String expectedStreamContent) throws IOException
    {
        FileOutputStream fileOutputStream = null;
        try
        {
            fileOutputStream = new FileOutputStream(file, false);
            fileOutputStream.write(expectedStreamContent.getBytes("UTF-8"));
        }
        finally
        {
            IOUtils.closeQuietly(fileOutputStream);
        }
    }

    @Test
    public void testMapToBeanFromFileWhenFileNotExists() throws Exception
    {
        final File file = new File("not_existing_sample_file.txt.xml");
        final Attachment attachment = mockAttachmentObject();

        final Either<FileNotFoundException, StoreAttachmentBean> result = storeAttachmentBeanMapper.mapToBean(attachment, file);

        assertTrue("result should be left and contain FileNotFoundException", result.isLeft());
        assertThat(result.left().get(), instanceOf(FileNotFoundException.class));
    }

    @Test
    public void testMapToBeanFromStream() throws Exception
    {
        final Attachment attachment = mockAttachmentObject();
        final InputStream stream = mock(InputStream.class);

        final StoreAttachmentBean result = storeAttachmentBeanMapper.mapToBean(attachment, stream);

        assertThat(result.getStream(), equalTo(stream));
        assertAttachmentMetadata(result, attachment);
    }

    private Attachment mockAttachmentObject()
    {
        final Attachment mock = mock(Attachment.class);

        when(mock.getId())
                .thenReturn(1l);
        when(mock.getFilename())
                .thenReturn("sampleFileName");
        when(mock.getFilesize())
                .thenReturn(1l);

        final Issue issue = mock(Issue.class);
        when(mock.getIssue())
                .thenReturn(issue);
        when(issue.getKey())
                .thenReturn("FOO-1");

        final Project project = mock(Project.class);
        when(issue.getProjectObject())
                .thenReturn(project);
        when(project.getOriginalKey())
                .thenReturn("FOO");

        return mock;
    }

    private void assertAttachmentMetadata(final StoreAttachmentBean result, final Attachment attachment)
    {
        assertThat(result.getId(), equalTo((Long)attachment.getId()));
        assertThat(result.getFileName(), equalTo(attachment.getFilename()));
        assertThat(result.getIssueKey(), equalTo(attachment.getIssue().getKey()));
        assertThat(result.getOriginalProjectKey(), equalTo(attachment.getIssue().getProjectObject().getOriginalKey()));
        assertThat(result.getSize(), equalTo(attachment.getFilesize()));
    }
}