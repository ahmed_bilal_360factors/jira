package com.atlassian.jira.util;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class TestClassNameAbbreviator
{
    @Test
    public void testGetAbbreviatedName() throws Exception
    {
        ClassNameAbbreviator abbreviator = ClassNameAbbreviator.DEFAULT;

        assertThat(abbreviator.getAbbreviatedName(""), is(""));
        assertThat(abbreviator.getAbbreviatedName("."), is("."));
        assertThat(abbreviator.getAbbreviatedName(".a.b.c"), is(".a.b.c"));
        assertThat(abbreviator.getAbbreviatedName("......"), is("......"));
        assertThat(abbreviator.getAbbreviatedName("Count"), is("Count"));
        assertThat(abbreviator.getAbbreviatedName(".Count"), is(".Count"));
        assertThat(abbreviator.getAbbreviatedName(".ount."), is(".ount."));
        assertThat(abbreviator.getAbbreviatedName(".Count."), is(".Count."));
        assertThat(abbreviator.getAbbreviatedName("evictionCount"), is("evictionCount"));
        assertThat(abbreviator.getAbbreviatedName("cache.evictionCount"), is("cache.evictionCount"));
        assertThat(abbreviator.getAbbreviatedName("cache.evictionCount."), is("cache.evictionCount."));
        assertThat(abbreviator.getAbbreviatedName("com.tempoplugin.scheduler.ao.GracePeriodServiceImpl.grace.period.cache.evictionCount"), is("c.t.s.a.GracePeriodServiceImpl.grace.period.cache.evictionCount"));
        assertThat(abbreviator.getAbbreviatedName("com.atlassian.jira.plugin.userformat.configuration.PropertySetBackedUserFormatTypeConfiguration.mappingPSRef.evictionCount"), is("c.a.j.p.u.c.PropertySetBackedUserFormatTypeConfiguration.mappingPSRef.evictionCount"));
        assertThat(abbreviator.getAbbreviatedName("net.customware.plugins.connector.core.integration.license.dataCache.evictionCount"), is("n.c.p.c.core.integration.license.dataCache.evictionCount"));
    }

    @Test
    public void testGetAbbreviatedNameNoMin() throws Exception
    {
        ClassNameAbbreviator abbreviator = new ClassNameAbbreviator(0);

        assertThat(abbreviator.getAbbreviatedName(""), is(""));
        assertThat(abbreviator.getAbbreviatedName("."), is("."));
        assertThat(abbreviator.getAbbreviatedName(".a.b.c"), is(".a.b.c"));
        assertThat(abbreviator.getAbbreviatedName("......"), is("......"));
        assertThat(abbreviator.getAbbreviatedName("Count"), is("Count"));
        assertThat(abbreviator.getAbbreviatedName(".Count"), is(".Count"));
        assertThat(abbreviator.getAbbreviatedName(".ount."), is(".o."));
        assertThat(abbreviator.getAbbreviatedName(".Count."), is(".Count."));
        assertThat(abbreviator.getAbbreviatedName("evictionCount"), is("e"));
        assertThat(abbreviator.getAbbreviatedName("cache.evictionCount"), is("c.e"));
        assertThat(abbreviator.getAbbreviatedName("cache.evictionCount."), is("c.e."));
        assertThat(abbreviator.getAbbreviatedName("com.tempoplugin.scheduler.ao.GracePeriodServiceImpl.grace.period.cache.evictionCount"), is("c.t.s.a.GracePeriodServiceImpl.grace.period.cache.evictionCount"));
        assertThat(abbreviator.getAbbreviatedName("com.atlassian.jira.plugin.userformat.configuration.PropertySetBackedUserFormatTypeConfiguration.mappingPSRef.evictionCount"), is("c.a.j.p.u.c.PropertySetBackedUserFormatTypeConfiguration.mappingPSRef.evictionCount"));
        assertThat(abbreviator.getAbbreviatedName("net.customware.plugins.connector.core.integration.license.dataCache.evictionCount"), is("n.c.p.c.c.i.l.d.e"));
    }

    @Test
    public void testGetAbbreviatedNameMin2() throws Exception
    {
        ClassNameAbbreviator abbreviator = new ClassNameAbbreviator(2);

        assertThat(abbreviator.getAbbreviatedName(""), is(""));
        assertThat(abbreviator.getAbbreviatedName("."), is("."));
        assertThat(abbreviator.getAbbreviatedName(".a.b.c"), is(".a.b.c"));
        assertThat(abbreviator.getAbbreviatedName("......"), is("......"));
        assertThat(abbreviator.getAbbreviatedName("Count"), is("Count"));
        assertThat(abbreviator.getAbbreviatedName(".Count"), is(".Count"));
        assertThat(abbreviator.getAbbreviatedName(".ount."), is(".ount."));
        assertThat(abbreviator.getAbbreviatedName(".Count."), is(".Count."));
        assertThat(abbreviator.getAbbreviatedName("evictionCount"), is("evictionCount"));
        assertThat(abbreviator.getAbbreviatedName("cache.evictionCount"), is("cache.evictionCount"));
        assertThat(abbreviator.getAbbreviatedName("cache.evictionCount."), is("c.evictionCount."));
        assertThat(abbreviator.getAbbreviatedName("com.tempoplugin.scheduler.ao.GracePeriodServiceImpl.grace.period.cache.evictionCount"), is("c.t.s.a.GracePeriodServiceImpl.grace.period.cache.evictionCount"));
        assertThat(abbreviator.getAbbreviatedName("com.atlassian.jira.plugin.userformat.configuration.PropertySetBackedUserFormatTypeConfiguration.mappingPSRef.evictionCount"), is("c.a.j.p.u.c.PropertySetBackedUserFormatTypeConfiguration.mappingPSRef.evictionCount"));
        assertThat(abbreviator.getAbbreviatedName("net.customware.plugins.connector.core.integration.license.dataCache.evictionCount"), is("n.c.p.c.c.i.l.dataCache.evictionCount"));
    }
}