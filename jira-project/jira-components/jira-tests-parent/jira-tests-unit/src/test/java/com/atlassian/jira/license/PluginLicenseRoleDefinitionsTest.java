package com.atlassian.jira.license;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.plugin.MockModuleDescriptor;
import com.atlassian.jira.mock.plugin.MockPlugin;
import com.atlassian.jira.mock.plugin.MockPluginAccessor;
import com.atlassian.jira.mock.plugin.NullModuleDescriptor;
import com.atlassian.jira.plugin.license.LicenseRoleModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import javax.annotation.Nonnull;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class PluginLicenseRoleDefinitionsTest
{
    @Rule
    public TestRule rule = new InitMockitoMocks(this);

    @Mock private JiraLicenseManager jiraLicenseManager;
    @Mock private EventPublisher publisher;

    private MockPluginAccessor pluginAccessor = new MockPluginAccessor();
    private PluginLicenseRoleDefinitions definitions;
    private MockLicenseRoleModuleDescriptor role1;
    private MockLicenseRoleModuleDescriptor role2;
    private MockLicenseRoleModuleDescriptor role3;
    private MockPlugin plugin1;

    @Before
    public void before()
    {
        plugin1 = new MockPlugin("one");
        MockPlugin plugin2 = new MockPlugin("two");

        role1 = new MockLicenseRoleModuleDescriptor("one", plugin1);
        role2 = new MockLicenseRoleModuleDescriptor("two", plugin2);
        role3 = new MockLicenseRoleModuleDescriptor("three", plugin2);

        plugin1.addModuleDescriptor(role1);
        plugin2.addModuleDescriptor(role2);
        plugin2.addModuleDescriptor(role3);

        pluginAccessor.addPlugin(plugin1);
        pluginAccessor.addPlugin(plugin2);

        definitions = new PluginLicenseRoleDefinitions(pluginAccessor, jiraLicenseManager, publisher);

        when(jiraLicenseManager.isLicensed(role1.getModule().getLicenseRoleId())).thenReturn(true);
        when(jiraLicenseManager.isLicensed(role2.getModule().getLicenseRoleId())).thenReturn(true);
    }

    @Test
    public void getDefinedRoleDefReturnsDefinedRoles()
    {
        assertThat(definitions.getDefinedRoleDefinitions(), Matchers.containsInAnyOrder(
            role1.getModule(), role2.getModule(), role3.getModule()
        ));
    }

    @Test
    public void isLicenseRoleDefinedReturnsCorrectly()
    {
        assertThat(definitions.isLicenseRoleDefined(role1.getModule().getLicenseRoleId()), Matchers.equalTo(true));
        assertThat(definitions.isLicenseRoleDefined(role2.getModule().getLicenseRoleId()), Matchers.equalTo(true));
        assertThat(definitions.isLicenseRoleDefined(role3.getModule().getLicenseRoleId()), Matchers.equalTo(true));
    }

    @Test
    public void getDefinedRoleDefinitionReturnsCorrectly()
    {
        assertThat(definitions.getDefinedRoleDefinition(role1.getModule().getLicenseRoleId()),
                Matchers.equalTo(Option.some(role1.getModule())));

        assertThat(definitions.getDefinedRoleDefinition(role2.getModule().getLicenseRoleId()),
                Matchers.equalTo(Option.some(role2.getModule())));

        assertThat(definitions.getDefinedRoleDefinition(role3.getModule().getLicenseRoleId()),
                Matchers.equalTo(Option.some(role3.getModule())));
    }

    @Test
    public void getAuthenticatedRoleDefsReturnsAuthenticatedRoles()
    {
        assertThat(definitions.getInstalledRoleDefinitions(), Matchers.containsInAnyOrder(
                role1.getModule(), role2.getModule()
        ));
    }

    @Test
    public void isLicenseRoleAuthenticatedReturnsCorrectly()
    {
        assertThat(definitions.isLicenseRoleInstalled(role1.getModule().getLicenseRoleId()), Matchers.equalTo(true));
        assertThat(definitions.isLicenseRoleInstalled(role2.getModule().getLicenseRoleId()), Matchers.equalTo(true));
        assertThat(definitions.isLicenseRoleInstalled(role3.getModule().getLicenseRoleId()), Matchers.equalTo(false));
    }

    @Test
    public void getAuthenticatedRoleDefinitionReturnsCorrectly()
    {
        assertThat(definitions.getInstalledRoleDefinition(role1.getModule().getLicenseRoleId()),
                Matchers.equalTo(Option.some(role1.getModule())));

        assertThat(definitions.getInstalledRoleDefinition(role2.getModule().getLicenseRoleId()),
                Matchers.equalTo(Option.some(role2.getModule())));

        assertThat(definitions.getInstalledRoleDefinition(role3.getModule().getLicenseRoleId()),
                Matchers.equalTo(Option.none(LicenseRoleDefinition.class)));
    }

    @Test
    public void irrelevantModuleDisabledDoesNotFireEvent()
    {
        definitions.pluginModuleDisabled(new PluginModuleDisabledEvent(new NullModuleDescriptor(plugin1, "null"), false));
        verifyNoMoreInteractions(publisher);
    }

    @Test
    public void irrelevantModuleEnabledDoesNotFireEvent()
    {
        definitions.pluginModuleEnabled(new PluginModuleEnabledEvent(new NullModuleDescriptor(plugin1, "null")));
        verifyNoMoreInteractions(publisher);
    }

    @Test
    public void licenseRoleEnabledTriggersEvent()
    {
        definitions.pluginModuleEnabled(new PluginModuleEnabledEvent(role2));
        verify(publisher).publish(new LicenseRoleDefinedEvent(role2.getLicenseRoleId()));
    }

    @Test
    public void licenseRoleDisabledTriggersEvent()
    {
        definitions.pluginModuleDisabled(new PluginModuleDisabledEvent(role1, false));
        verify(publisher).publish(new LicenseRoleUndefinedEvent(role1.getLicenseRoleId()));
    }

    public static class MockLicenseRoleModuleDescriptor extends MockModuleDescriptor<LicenseRoleDefinition>
            implements LicenseRoleModuleDescriptor
    {
        private MockLicenseRoleDefinition definition;

        public MockLicenseRoleModuleDescriptor(String id, Plugin plugin)
        {
            super(LicenseRoleDefinition.class, plugin, id);
            definition = new MockLicenseRoleDefinition(LicenseRoleId.valueOf(id), id);
        }

        @Override
        public LicenseRoleDefinition getModule()
        {
            return definition;
        }

        @Nonnull
        @Override
        public LicenseRoleId getLicenseRoleId()
        {
            return definition.getLicenseRoleId();
        }
    }
}