package com.atlassian.jira.issue.attachment.store.provider;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.FeatureEvent;
import com.atlassian.jira.issue.attachment.store.BackwardCompatibleStoreAdapter;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.web.action.admin.EditAttachmentSettingsEvent;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestCachedBackwardCompatibleStoreAdapterProvider
{
    @Rule
    public InitMockitoMocks initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private BackwardCompatibleStoreAdapterProvider backwardCompatibleStoreAdapterProvider;
    @Mock
    private EventPublisher eventPublisher;

    @InjectMocks
    private CachedBackwardCompatibleStoreAdapterProvider provider;

    @Test
    public void testRegisterHimselfOnStart() throws Exception
    {
        provider.start();

        verify(eventPublisher).register(provider);
    }

    @Test
    public void testProvidesSameInstance() throws Exception
    {
        when(backwardCompatibleStoreAdapterProvider.provide())
                .thenReturn(mock(BackwardCompatibleStoreAdapter.class),mock(BackwardCompatibleStoreAdapter.class));

        final BackwardCompatibleStoreAdapter firstInstance = provider.provide();
        final BackwardCompatibleStoreAdapter secondInstance = provider.provide();

        assertThat(firstInstance, equalTo(secondInstance));
    }

    @Test
    public void testProvidesNewInstanceAfterFeatureEvent() throws Exception
    {
        when(backwardCompatibleStoreAdapterProvider.provide())
                .thenReturn(mock(BackwardCompatibleStoreAdapter.class),mock(BackwardCompatibleStoreAdapter.class));

        final BackwardCompatibleStoreAdapter firstInstance = provider.provide();
        provider.onDarkFeatureEvent(mock(FeatureEvent.class));
        final BackwardCompatibleStoreAdapter secondInstance = provider.provide();

        assertThat(firstInstance, is(not(equalTo(secondInstance))));
    }

    @Test
    public void testProvidesNewInstanceAfterAttachmentSettingChanged() throws Exception
    {
        when(backwardCompatibleStoreAdapterProvider.provide())
                .thenReturn(mock(BackwardCompatibleStoreAdapter.class),mock(BackwardCompatibleStoreAdapter.class));

        final BackwardCompatibleStoreAdapter firstInstance = provider.provide();
        provider.onEditAttachmentSettings(mock(EditAttachmentSettingsEvent.class));
        final BackwardCompatibleStoreAdapter secondInstance = provider.provide();

        assertThat(firstInstance, is(not(equalTo(secondInstance))));
    }
}