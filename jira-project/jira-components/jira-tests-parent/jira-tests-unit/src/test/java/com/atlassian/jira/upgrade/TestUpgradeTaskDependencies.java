package com.atlassian.jira.upgrade;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.help.MockHelpUrl;
import com.atlassian.jira.issue.index.IssueIndexingParams;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import electric.xml.Document;
import electric.xml.Element;
import electric.xml.Elements;
import electric.xml.ParseException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * This test checks that all upgrade tasks only depend upon upgrade tasks whose scheduling option is compatible.
 *
 * @since v6.4
 */
@RunWith (MockitoJUnitRunner.class)
public class TestUpgradeTaskDependencies
{
    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    private static final String UPGRADES_XML = "upgrades.xml";
    private static final BuildNumComparator BUILD_NUM_COMPARATOR = new BuildNumComparator();

    @Mock @AvailableInContainer HelpUrls helpUrls;

    @Before
    public void setup()
    {
        when(helpUrls.getUrl(any(String.class))).thenReturn(MockHelpUrl.simpleUrl("key"));
    }

    @Test
    public void testDependencies()
            throws ParseException, IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, InvocationTargetException
    {
        Map<String, UpgradeTask> upgradeTasks = getUpgradeTasks();

        checkTasks(upgradeTasks);
    }

    public void checkTasks(final Map<String, UpgradeTask> upgradeTasks)
    {
        for (UpgradeTask upgradeTask : upgradeTasks.values())
        {
            if (upgradeTask.dependsUpon() != null)
            {
                UpgradeTask dependent = upgradeTasks.get(upgradeTask.dependsUpon());
                assertThat("Upgrade Task " + upgradeTask.getBuildNumber() + " depends upon " + upgradeTask.dependsUpon() +
                                " which does not exist.",
                        dependent, notNullValue(UpgradeTask.class));

                if (upgradeTask.getScheduleOption() == UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED)
                {
                    assertThat("Upgrade Task " + upgradeTask.getBuildNumber() + " depends upon " + dependent.getBuildNumber() +
                            ". That won't work as they have incompatible schedule options.", dependent.getScheduleOption(), is(UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED));

                }
                assertThat("Upgrade Task " + upgradeTask.getBuildNumber() + " depends upon " + dependent.getBuildNumber() +
                                " which has a later build number",
                        BUILD_NUM_COMPARATOR.compare(upgradeTask.getBuildNumber(), dependent.getBuildNumber()) > 0, is(true));
            }
        }
    }

    private Map<String, UpgradeTask> getUpgradeTasks()
            throws ClassNotFoundException, IOException, ParseException, IllegalAccessException, InvocationTargetException, InstantiationException
    {
        Map<String, UpgradeTask> allTasks = new HashMap<String, UpgradeTask>();

        final InputStream is = ClassLoaderUtils.getResourceAsStream(UPGRADES_XML, this.getClass());
        try
        {
            final Document doc = new Document(is);
            final Element root = doc.getRoot();
            final Elements actions = root.getElements("upgrade");

            while (actions.hasMoreElements())
            {
                final Element action = (Element) actions.nextElement();
                final String buildNo = action.getAttribute("build");
                final String className = action.getElement("class").getTextString();

                final Class<?> componentClass = getClass().getClassLoader().loadClass(className);
                Constructor<?>[] constructors = componentClass.getConstructors();
                Object[] params = getMockedConstructorParams(constructors[0].getParameterTypes());

                final UpgradeTask task = (UpgradeTask) constructors[0].newInstance(params);
                allTasks.put(buildNo, task);
            }
        }
        finally
        {
            is.close();
        }
        return allTasks;
    }

    private Object[] getMockedConstructorParams(final Class<?>[] parameterTypes)
    {
        Object[] params = new Object[parameterTypes.length];
        int i = 0;
        for (Class<?> parameterType : parameterTypes)
        {
            params[i++] = Mockito.mock(parameterType);
        }
        return params;
    }

    @Test
    public void testThisClass()
    {
        checkTasks(getGoodTasks());
        try
        {
            checkTasks(getMissingDependentTasks());
            fail("There are bad dependencies in the mix");
        }
        catch(java.lang.AssertionError ex)
        {
            assertThat(ex.getMessage(), startsWith("Upgrade Task 8 depends upon 7 which does not exist."));
        }
        try
        {
            checkTasks(getBadlyScheduledTasks());
            fail("There are bad dependencies in the mix");
        }
        catch(java.lang.AssertionError ex)
        {
            assertThat(ex.getMessage(), startsWith("Upgrade Task 3 depends upon 2. That won't work as they have incompatible schedule options."));
        }
        try
        {
            checkTasks(getBadOrderedTasks());
            fail("There are bad dependencies in the mix");
        }
        catch(java.lang.AssertionError ex)
        {
            assertThat(ex.getMessage(), startsWith("Upgrade Task 4 depends upon 5 which has a later build number"));
        }
    }

    private Map<String, UpgradeTask> getGoodTasks()
    {
        Map<String, UpgradeTask> allTasks = new HashMap<String, UpgradeTask>();
        allTasks.put("1", new MockTask("1", UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED, null));
        allTasks.put("2", new MockTask("2", UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED, null));
        allTasks.put("3", new MockTask("3", UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED, "2"));
        allTasks.put("4", new MockTask("4", UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED, "2"));
        allTasks.put("5", new MockTask("5", UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED, "4"));
        return allTasks;
    }

    private Map<String, UpgradeTask> getMissingDependentTasks()
    {
        Map<String, UpgradeTask> allTasks = new HashMap<String, UpgradeTask>();
        allTasks.put("1", new MockTask("1", UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED, null));
        allTasks.put("2", new MockTask("2", UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED, null));
        allTasks.put("3", new MockTask("3", UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED, "2"));
        allTasks.put("8", new MockTask("8", UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED, "7"));
        return allTasks;
    }

    private Map<String, UpgradeTask> getBadlyScheduledTasks()
    {
        Map<String, UpgradeTask> allTasks = new HashMap<String, UpgradeTask>();
        allTasks.put("1", new MockTask("1", UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED, null));
        allTasks.put("2", new MockTask("2", UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED, null));
        allTasks.put("3", new MockTask("3", UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED, "2"));
        allTasks.put("4", new MockTask("4", UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED, "2"));
        allTasks.put("5", new MockTask("5", UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED, "4"));
        return allTasks;
    }

    private Map<String, UpgradeTask> getBadOrderedTasks()
    {
        Map<String, UpgradeTask> allTasks = new HashMap<String, UpgradeTask>();
        allTasks.put("1", new MockTask("1", UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED, null));
        allTasks.put("2", new MockTask("2", UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED, null));
        allTasks.put("3", new MockTask("3", UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED, "2"));
        allTasks.put("4", new MockTask("4", UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED, "5"));
        allTasks.put("5", new MockTask("5", UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED, "3"));
        return allTasks;
    }


    private class MockTask extends AbstractUpgradeTask
    {
        private final String build;
        private final ScheduleOption scheduleOption;
        private final String dependsOn;

        public MockTask(final String build, final ScheduleOption scheduleOption, final String dependsOn)
        {
            super();
            this.build = build;
            this.scheduleOption = scheduleOption;
            this.dependsOn = dependsOn;
        }

        @Override
        public String getBuildNumber()
        {
            return build;
        }

        @Override
        public String getShortDescription()
        {
            return "Upgrade Task " + build;
        }

        @Override
        public void doUpgrade(final boolean setupMode) throws Exception
        {

        }

        @Nullable
        @Override
        public String dependsUpon()
        {
            return dependsOn;
        }

        @Override
        public ScheduleOption getScheduleOption()
        {
            return scheduleOption;
        }
    }
}
