package com.atlassian.jira;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.atlassian.core.util.ImageInfo;
import com.atlassian.jira.avatar.AvatarTagger;

import com.google.common.base.Function;
import com.google.common.base.Throwables;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith (Parameterized.class)
public class TestIconsForMetadata
{
    static final Function<File, Iterable<File>> LIST_PNG_IN_FOLDER = new Function<File, Iterable<File>>()
    {
        @Override
        public Iterable<File> apply(final File file)
        {
            @SuppressWarnings ("unchecked") final Collection<File> icons = FileUtils.listFiles(file, new SuffixFileFilter(".png"), TrueFileFilter.INSTANCE);

            if (icons.size() == 0)
            {
                try
                {
                    throw new RuntimeException("It's not a folder with icons: " + file.getCanonicalPath());
                }
                catch (final IOException e)
                {
                    Throwables.propagate(e);
                }
            }

            return icons;
        }
    };

    private static final File jiraComponentAbs = new File("jira-components");
    private static final File jiraComponentRel = new File("../..");
    private static final File jiraComponents = jiraComponentAbs.exists() ? jiraComponentAbs : jiraComponentRel;

    static final File[] iconPaths = new File[] {
            new File(jiraComponents, "jira-webapp-common/src/main/webapp/images/icons"),
            new File(jiraComponents, "jira-webapp-common/src/main/webapp/images/mail"),
            new File(jiraComponents, "jira-core/src/main/resources/avatars")
    };

    final public File testedFile;

    public TestIconsForMetadata(final File testedFile)
    {
        this.testedFile = testedFile;
    }

    @Parameterized.Parameters
    public static List<Object[]> getIconsThatShouldBeTagged()
    {
        final Iterable<Iterable<File>> folderContents = Iterables.transform(Arrays.asList(iconPaths), LIST_PNG_IN_FOLDER);
        final Iterable<File> iconFiles = Iterables.concat(folderContents);

        final Iterable<Object[]> parametersIterable = Iterables.transform(iconFiles, new Function<File, Object[]>()
        {
            @Override
            public Object[] apply(final File file)
            {
                return new Object[] { file };
            }
        });

        return Lists.newArrayList(parametersIterable);
    }

    @Test
    public void shouldFileBeTaggedAsSystemImage() throws IOException
    {
        final InputStream fileStream = new FileInputStream(testedFile);
        try
        {
            final ImageInfo imageInfo = new ImageInfo();
            imageInfo.setInput(fileStream);
            imageInfo.setExpectedPngTextKeyword(AvatarTagger.JIRA_SYSTEM_IMAGE_TYPE);
            assertThat(
                    String.format(
                            "Image %s file desn't contain %s (needed ie. by email handlers to filter out these resources). "
                                    + "Use com.atlassian.jira.avatar.TestResourceTagging for tagging", testedFile.getCanonicalPath(), AvatarTagger.JIRA_SYSTEM_IMAGE_TYPE),
                    imageInfo.check(),
                    is(true));
        }
        finally
        {
            fileStream.close();
        }
    }
}
