package com.atlassian.jira.appconsistency.db;

import java.sql.SQLException;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.Datasource;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.database.DatabaseCollationReader;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.contains;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestCollationCheck
{

    // These values are used to create the CollationCheck
    private String collation;
    private String databaseType;
    private boolean collationQueryFails;

    @Before
    public void before()
    {
        collation = null;
        databaseType = null;
        collationQueryFails = false;
    }

    @Test
    public void collationNotSupportedShowsWarning()
    {
        databaseType = "postgres72";
        collation = "en_AU.UTF-8";
        CollationCheck collationCheck = getCollationCheck();

        boolean isOk = collationCheck.isOk();

        assertTrue(isOk);
        assertWarning(collationCheck, "You are using an unsupported postgres72 collation: en_AU.UTF-8");
    }

    @Test
    public void collationSupportedShowsNoWarning()
    {
        databaseType = "postgres72";
        collation = "POSIX";
        CollationCheck collationCheck = getCollationCheck();

        boolean isOk = collationCheck.isOk();

        assertTrue(isOk);
        assertNoWarning(collationCheck);
    }

    @Test
    public void usingHsqlShowsNoWarning()
    {
        databaseType = "hsql";
        collation = null;
        CollationCheck collationCheck = getCollationCheck();

        boolean isOk = collationCheck.isOk();

        assertTrue(isOk);
        assertNoWarning(collationCheck);
    }

    @Test
    public void usingUnknownDatabaseShowsWarning()
    {
        databaseType = "unknown database";
        collation = null;
        CollationCheck collationCheck = getCollationCheck();

        boolean isOk = collationCheck.isOk();

        assertTrue(isOk);
        assertWarning(collationCheck, "Your database is not supported");
    }

    @Test
    public void readingCollationFailsShowsWarning()
    {
        databaseType = "postgres72";
        collationQueryFails = true;
        CollationCheck collationCheck = getCollationCheck();

        boolean isOk = collationCheck.isOk();

        assertTrue(isOk);
        assertWarning(collationCheck, "collation could not be read");
    }

    private CollationCheck getCollationCheck()
    {
        try
        {
            DatabaseCollationReader collationReader = mock(DatabaseCollationReader.class);
            if (collationQueryFails)
            {
                when(collationReader.findCollation()).thenThrow(new SQLException());
            }
            else
            {
                when(collationReader.findCollation()).thenReturn(collation);
            }

            DatabaseConfig databaseConfig = new DatabaseConfig(databaseType, "schema", mock(Datasource.class));

            JiraProperties jiraProperties = mock(JiraProperties.class);
            when(jiraProperties.getProperty("line.separator")).thenReturn("\n");

            final CollationCheck.WarningLogger mockWarningLogger = mock(CollationCheck.WarningLogger.class);

            CollationCheck collationCheck = new CollationCheck(databaseConfig, collationReader, jiraProperties) {
                @Override
                WarningLogger getWarningLogger()
                {
                    return mockWarningLogger;
                }
            };

            return collationCheck;
        }
        catch (Exception e)
        {
            // Swallow mock exceptions
            return null;
        }
    }

    private void assertWarning(final CollationCheck collationCheck, String warningMessage)
    {
        verify(collationCheck.getWarningLogger(), times(1)).showWarning(contains(warningMessage));
    }

    private void assertNoWarning(CollationCheck collationCheck)
    {
        verify(collationCheck.getWarningLogger(), never()).showWarning(anyString());
    }
}

