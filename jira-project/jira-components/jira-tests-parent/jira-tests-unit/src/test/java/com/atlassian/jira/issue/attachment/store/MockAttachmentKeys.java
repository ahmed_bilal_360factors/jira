package com.atlassian.jira.issue.attachment.store;

import com.atlassian.jira.issue.attachment.AttachmentKey;

public class MockAttachmentKeys
{
    public static AttachmentKey getKey()
    {
        return AttachmentKey.builder()
                .withIssueKey("FOO-1")
                .withProjectKey("FOO")
                .withAttachmentFilename("filename")
                .withAttachmentId(1l)
                .build();
    }

    public static AttachmentKey getKey(long id)
    {
        return AttachmentKey.builder()
                .withIssueKey("FOO-1")
                .withProjectKey("FOO")
                .withAttachmentFilename("filename")
                .withAttachmentId(id)
                .build();
    }

}
