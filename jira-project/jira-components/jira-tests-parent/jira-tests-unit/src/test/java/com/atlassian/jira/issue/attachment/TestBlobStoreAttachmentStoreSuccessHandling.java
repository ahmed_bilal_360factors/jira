package com.atlassian.jira.issue.attachment;

import java.io.File;
import java.io.InputStream;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.store.BlobStoreAttachmentStore;
import com.atlassian.jira.issue.attachment.store.MockAttachmentKeys;
import com.atlassian.jira.issue.attachment.store.UniqueIdentifierGenerator;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import io.atlassian.blobstore.client.api.Access;
import io.atlassian.blobstore.client.api.BlobStoreService;
import io.atlassian.blobstore.client.api.Failure;
import io.atlassian.blobstore.client.api.HeadResult;
import io.atlassian.blobstore.client.api.PutResult;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class TestBlobStoreAttachmentStoreSuccessHandling
{
    @Rule
    public final TestRule mockInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private BlobStoreService mockService;

    @Mock
    private UniqueIdentifierGenerator uniqueIdentifierGenerator;

    @InjectMocks
    private BlobStoreAttachmentStore store;

    @Test
    public void testSuccessOnGet() throws Exception
    {
        final Long id = 1l;
        final File tmp = File.createTempFile("jira-blobstore", "tmp");
        try
        {
            Either<Failure, Option<File>> getSuccess = Either.right(Option.some(tmp));
            Promise<Either<Failure, Option<File>>> getSuccessPromise = Promises.promise(getSuccess);
            when(mockService.get(eq(id.toString()), any(Access.class), any(com.google.common.base.Function.class))).thenReturn(getSuccessPromise);

            final AttachmentKey attachmentKey = MockAttachmentKeys.getKey(id);

            final File result = store.getAttachment(attachmentKey, new Function<InputStream, File>()
            {
                @Override
                public File get(final InputStream input)
                {
                    // This doesn't get called in this test as we are mocking out the execution of the function
                    return tmp;
                }
            }).claim();
            assertEquals(tmp.getAbsolutePath(), result.getAbsolutePath());
        }
        finally
        {
            tmp.delete();
        }
    }

    @Test
    public void testSuccessOnPut() throws Exception
    {
        final Long id = 1l;
        final Long size = 123l;

        final Either<Failure, PutResult> putSuccess = Either.right(PutResult.created("1"));
        final Promise<Either<Failure, PutResult>> putSuccessPromise = Promises.promise(putSuccess);
        when(mockService.put(eq(id.toString()), any(InputStream.class), any(Long.class))).thenReturn(putSuccessPromise);

        final InputStream is = mock(InputStream.class);
        final StoreAttachmentBean storeAttachmentBean = new StoreAttachmentBean.Builder(is)
                .withId(id)
                .withSize(size)
                .build();

        final StoreAttachmentResult result = store.putAttachment(storeAttachmentBean).claim();

        assertThat(result, equalTo(StoreAttachmentResult.created()));
    }

    @Test
    public void testSuccessOnExists() throws Exception
    {
        final Long id = 1l;
        final Either<Failure, Option<HeadResult>> getSuccess = Either.right(Option.some(HeadResult.create("ABC", 1)));
        final Promise<Either<Failure, Option<HeadResult>>> getSuccessPromise = Promises.promise(getSuccess);
        when(mockService.head(eq(id.toString()), any(Access.class))).thenReturn(getSuccessPromise);

        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey(id);

        assertTrue(store.exists(attachmentKey).claim());
    }

    @Test
    public void testMoveAttachment() throws Exception
    {
        final AttachmentKey oldAttachmentKey = MockAttachmentKeys.getKey(1);
        final AttachmentKey newAttachmentKey = MockAttachmentKeys.getKey(2);

        final PutResult putResult = PutResult.created("1");
        when(mockService.move(String.valueOf(oldAttachmentKey.getAttachmentId()), String.valueOf(newAttachmentKey.getAttachmentId())))
                .thenReturn(Promises.promise(Either.<Failure, PutResult>right(putResult)));

        store.moveAttachment(oldAttachmentKey, newAttachmentKey).claim();

        verify(mockService).move(String.valueOf(oldAttachmentKey.getAttachmentId()), String.valueOf(newAttachmentKey.getAttachmentId()));
    }

    @Test
    public void testIgnoreMoveAttachmentWhenNewKeyIsTheSameAsOld() throws Exception
    {
        final AttachmentKey oldAttachmentKey = MockAttachmentKeys.getKey(1);
        final AttachmentKey newAttachmentKey = MockAttachmentKeys.getKey(1);

        final Promise<Unit> result = store.moveAttachment(oldAttachmentKey, newAttachmentKey);

        assertThat(result.claim(), equalTo(Unit.VALUE));
        verifyNoMoreInteractions(mockService);
    }

    @Test
    public void testCopyAttachment() throws Exception
    {
        final AttachmentKey oldAttachmentKey = MockAttachmentKeys.getKey(1);
        final AttachmentKey newAttachmentKey = MockAttachmentKeys.getKey(2);

        final PutResult putResult = PutResult.created("1");
        when(mockService.copy(String.valueOf(oldAttachmentKey.getAttachmentId()), String.valueOf(newAttachmentKey.getAttachmentId())))
                .thenReturn(Promises.promise(Either.<Failure, PutResult>right(putResult)));

        store.copyAttachment(oldAttachmentKey, newAttachmentKey).claim();

        verify(mockService).copy(String.valueOf(oldAttachmentKey.getAttachmentId()), String.valueOf(newAttachmentKey.getAttachmentId()));
    }

    @Test
    public void testSuccessOnDelete() throws Exception
    {
        final Long id = 1l;
        Promise<Either<Failure,Boolean>> getSuccessPromise = Promises.promise(Either.<Failure, Boolean>right(true));
        when(mockService.delete(eq(id.toString()))).thenReturn(getSuccessPromise);

        final AttachmentKey attachmentKey = MockAttachmentKeys.getKey(id);

        store.deleteAttachment(attachmentKey).claim();
        verify(mockService).delete(id.toString());
    }

    @Test
    public void testCreateTemporaryAttachment() throws Exception
    {
        final InputStream inputStream = mock(InputStream.class);
        final long size = 123123l;
        final String tempId = "tempId";

        when(uniqueIdentifierGenerator.getNextId())
                .thenReturn(tempId);

        when(mockService.put(tempId, inputStream, size))
                .thenReturn(Promises.promise(Either.<Failure, PutResult>right(PutResult.created("1l"))));

        final TemporaryAttachmentId result = store.putTemporaryAttachment(inputStream, size).claim();

        assertThat(result.toStringId(), equalTo(tempId));
    }

    @Test
    public void testMoveTempToRealAttachment() throws Exception
    {
        final AttachmentKey destinationKey = MockAttachmentKeys.getKey(1);
        final TemporaryAttachmentId tempId = TemporaryAttachmentId.fromString("tempId");

        final PutResult putResult = PutResult.created("1");
        when(mockService.move(tempId.toStringId(), String.valueOf(destinationKey.getAttachmentId())))
                .thenReturn(Promises.promise(Either.<Failure, PutResult>right(putResult)));

        store.moveTemporaryToAttachment(tempId, destinationKey).claim();

        verify(mockService).move(tempId.toStringId(), String.valueOf(destinationKey.getAttachmentId()));
    }

    @Test
    public void testSuccessOnTemporaryDelete() throws Exception
    {
        final TemporaryAttachmentId tempId = TemporaryAttachmentId.fromString("tempId");
        Promise<Either<Failure,Boolean>> getSuccessPromise = Promises.promise(Either.<Failure, Boolean>right(true));
        when(mockService.delete(eq("tempId"))).thenReturn(getSuccessPromise);

        store.deleteTemporaryAttachment(tempId).claim();
        verify(mockService).delete("tempId");
    }
}
