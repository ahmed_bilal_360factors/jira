package com.atlassian.jira.license;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.event.directory.DirectoryCreatedEvent;
import com.atlassian.crowd.event.directory.RemoteDirectorySynchronisedEvent;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.license.MockLicenseRole;
import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.config.FeatureDisabledEvent;
import com.atlassian.jira.config.FeatureEnabledEvent;
import com.atlassian.jira.config.FeatureEvent;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.matchers.OptionMatchers;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.MockUser;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import static com.atlassian.jira.config.CoreFeatures.LICENSE_ROLES_ENABLED;
import static com.atlassian.jira.license.LicenseRoleStore.LicenseRoleData;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestDefaultLicenseRoleManager
{
    private static final String LICENSE_ROLE_ID1 = "role.one";
    private static final String LICENSE_ROLE_ID2 = "role.two";
    private static final String LICENSE_ROLE_ID3 = "role.three";
    private static final String GROUP1 = "group.one";
    private static final String GROUP2 = "group.two";
    private static final String GROUP3 = "group.three";
    private static final String GROUP4 = "group.four";
    private static final String OHS_GROUP = "ohs.group";
    private static final String VANILLA_GROUP = "vanilla.group";
    private static final String SOFTWARE_GROUP = "software.group";
    private static final int USERS_ACTIVE_IN_SOFTWARE = 3;
    private static final int USERS_ACTIVE_IN_VANILLA = 2;
    private static final int USERS_ACTIVE_UNIQUE_IN_VANILLA_AND_SOFTWARE = 4;
    private static final int USERS_ACTIVE_IN_SOFTWARE_AND_OHS = 7;
    private static final int USERS_ACTIVE_IN_VANILLA_AND_OHS = 6;
    private static final int USERS_ACTIVE_IN_VANILLA_AND_OHS_UNIQUE_TO_SOFTWARE= 5;
    private static final int USERS_ACTIVE_IN_UNDEFINED_OR_NULL_LICENSE_ROLE = 0;
    private static final int UNIQUE_USERS_IN_ALL_GROUPS = 8;

    @Mock private CacheManager cacheManager;
    @Mock private Cache<String, Option<LicenseRole>> roles;
    @Mock private GroupManager groupManager;
    @Mock private Cache<LicenseRoleId, Integer> activeUserPerRoleCacheImpl;
    @Mock private LicenseRole licenseRoleVanillaJira;
    @Mock private LicenseRole licenseRoleSoftware;
    @Mock Directory directory;
    @Mock RemoteDirectory remoteDirectory;
    @Mock FeatureEvent featureEvent;
    @Mock LicenseRoleStore mockedStore;
    private LicenseRoleId roleIdSoftware = new LicenseRoleId("com.atlassian.jira.software");
    private LicenseRoleId roleIdPlatform = new LicenseRoleId("com.atlassian.jira.platform");
    private LicenseRoleId roleIdUndefined = new LicenseRoleId("undefined");
    private MockUser johnInactive;
    private Set<User> usersSoftware;
    private Set<User> usersJiraVanilla;
    // OHS stands for Occupational Health & Safety
    private Set<User> usersInOhs;

    private MockLicenseRoleStore store = new MockLicenseRoleStore();
    private MockLicenseRoleDefinitions definitions = new MockLicenseRoleDefinitions();
    private DefaultLicenseRoleManager manager;

    private LicenseRoleDefinition def1 = new MockLicenseRoleDefinition(LICENSE_ROLE_ID1);
    private LicenseRoleDefinition def2 = new MockLicenseRoleDefinition(LICENSE_ROLE_ID2);
    private LicenseRoleDefinition def3 = new MockLicenseRoleDefinition(LICENSE_ROLE_ID3);

    private LicenseRole role1 = new MockLicenseRole().id(def1.getLicenseRoleId()).groups(GROUP1).name(LICENSE_ROLE_ID1);
    private LicenseRole role2 = new MockLicenseRole().id(def2.getLicenseRoleId()).groups(GROUP2).name(LICENSE_ROLE_ID2);
    private LicenseRole role3 = new MockLicenseRole().id(def3.getLicenseRoleId()).groups(GROUP3, GROUP4).name(LICENSE_ROLE_ID3);

    @Before
    public void before()
    {
        cacheManager = new MemoryCacheManager();

        manager = new DefaultLicenseRoleManager(cacheManager, store, definitions, groupManager);

        definitions.addAuthorised(new MockLicenseRoleDefinition(LICENSE_ROLE_ID1));
        definitions.addAuthorised(new MockLicenseRoleDefinition(LICENSE_ROLE_ID2));
        definitions.addAuthorised(new MockLicenseRoleDefinition(LICENSE_ROLE_ID3));

        when(groupManager.groupExists(GROUP1)).thenReturn(true);
        when(groupManager.groupExists(GROUP2)).thenReturn(true);
        when(groupManager.groupExists(GROUP3)).thenReturn(true);
        when(groupManager.groupExists(GROUP4)).thenReturn(true);
        when(groupManager.groupExists(SOFTWARE_GROUP)).thenReturn(true);
        when(groupManager.groupExists(VANILLA_GROUP)).thenReturn(true);
        when(groupManager.groupExists(OHS_GROUP)).thenReturn(true);

        //Necessary pre-condition to all cache flush test to ensure the two groups have different populations.
        assertThat("Two groups have different populations",
                USERS_ACTIVE_UNIQUE_IN_VANILLA_AND_SOFTWARE, Matchers.greaterThan(USERS_ACTIVE_IN_SOFTWARE));

        johnInactive = new MockUser("NoJohn");
        johnInactive.setActive(false);
        usersSoftware = new HashSet<User>(Arrays.asList(new MockUser("John"),
                new MockUser("Stephani"), johnInactive, new MockUser("George")));
        usersJiraVanilla = new HashSet<User>(Arrays.asList(new MockUser("John"), new MockUser("Helen")));
        usersInOhs = new HashSet<User>(Arrays.asList(new MockUser("Jim"), new MockUser("Sophie"),
                new MockUser("Peter"), new MockUser("Alex"), johnInactive));
        when(groupManager.getUsersInGroup(SOFTWARE_GROUP)).thenReturn(usersSoftware);
        when(groupManager.getUsersInGroup(VANILLA_GROUP)).thenReturn(usersJiraVanilla);
        when(groupManager.getUsersInGroup(OHS_GROUP)).thenReturn(usersInOhs);
        when(licenseRoleSoftware.getId()).thenReturn(roleIdSoftware);
    }

    private void defineExtraLicenses()
    {
        definitions.addAuthorised(new MockLicenseRoleDefinition(roleIdSoftware.getName()));
        definitions.addDefined(new MockLicenseRoleDefinition(roleIdSoftware.getName()));
        definitions.addAuthorised(new MockLicenseRoleDefinition(roleIdUndefined.getName()));
        definitions.addDefined(new MockLicenseRoleDefinition(roleIdUndefined.getName()));
    }

    @Test
    public void getLicenseRoleIsCached()
    {
        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP1), Option.<String>none()));
        definitions.removeAuthorised(def2);

        assertThat("Ensure role1 is cached by making initial request", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));

        assertThat("Ensure role1 is cached by making initial request", manager.getLicenseRole(role2.getId()),
                OptionMatchers.none());
        // Initial cache load finished with role1 having group 1 and role2 not existing.
        // Now we change the state of the store and ensure that the cache has not changed.
        store.save(new LicenseRoleData(def2.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        definitions.addAuthorised(def2);
        Option<LicenseRole> licenseRoleOption=manager.getLicenseRole(role2.getId());
        assertThat("Ensure cache still retains original role2", licenseRoleOption,OptionMatchers.none());

        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.some(GROUP2)));
        assertThat("Verify that role1 is cached from initial request", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));
    }

    @Test
    public void getLicenseRolesReturnsAuthenticatedRoles()
    {
        //Role3 is no longer authorised.
        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP1), Option.<String>none()));
        store.save(new LicenseRoleData(def2.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        store.save(new LicenseRoleData(def3.getLicenseRoleId(), ImmutableSet.of(GROUP3), Option.<String>none()));
        definitions.removeAuthorised(def3);

        assertThat("Verify that onliy defined roles 1 and 2 are identified by the manager",
                manager.getLicenseRoles(), Matchers.<LicenseRole>containsInAnyOrder(
                new LicenseRoleMatcher().merge(role1),
                new LicenseRoleMatcher().merge(role2)
        ));
    }

    @Test
    public void getGroupsForAuthenticatedLicenseRolesOnlyReturnsAuthenticatedGroups()
    {
        //Role2 is no longer authorised.
        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP1), Option.<String>none()));
        store.save(new LicenseRoleData(def2.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        store.save(new LicenseRoleData(def3.getLicenseRoleId(), ImmutableSet.of(GROUP3, GROUP4),
                Option.<String>none()));
        definitions.removeAuthorised(def2);

        assertThat("Verify group2 (part of undefined role2) is not part of groups for installed licenses",
                manager.getGroupsForInstalledLicenseRoles(), Matchers.containsInAnyOrder(GROUP1, GROUP3, GROUP4));
    }

    @Test
    public void isAuthenticated()
    {
        //Role2 is no longer authorised.
        definitions.removeAuthorised(def2);

        assertThat("Verify role1 is installed",
                manager.isLicenseRoleInstalled(def1.getLicenseRoleId()), Matchers.equalTo(true));
        assertThat("Verify role2 is  not installed",
                manager.isLicenseRoleInstalled(def2.getLicenseRoleId()), Matchers.equalTo(false));
        assertThat("Verify role1 is installed",
                manager.isLicenseRoleInstalled(def3.getLicenseRoleId()), Matchers.equalTo(true));
    }

    @Test
    public void clearCacheClearsTheRoleCache()
    {
        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP1), Option.<String>none()));
        definitions.removeAuthorised(def2);

        assertThat("Verify role1 is cached making initial request", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));
        assertThat("Verify role2 is cached making initial request", manager.getLicenseRole(role2.getId()),
                OptionMatchers.none());
        // Initial cache load finished with role1 having group 1 and role2 not existing.
        // Now we change the state of the store and ensure that the cache has not changed.
        store.save(new LicenseRoleData(def2.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        definitions.addAuthorised(def2);
        Option<LicenseRole> licenseRoleOption=manager.getLicenseRole(role2.getId());
        assertThat("Ensure role2 is still cached from initial request", licenseRoleOption,OptionMatchers.none());

        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        assertThat("Ensure role1 is still cached from initial request", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));

        // now verify that the cache has been reloaded with new values
        manager.clearCache(ClearCacheEvent.INSTANCE);
        assertThat("Verify role2 is reloaded in the cache", manager.getLicenseRole(role2.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role2)));
        assertThat("Verify role1 is reloaded in the cache", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1).groups(GROUP2)));
    }

    @Test
    public void newLicenseClearsTheCache()
    {
        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP1), Option.<String>none()));
        definitions.removeAuthorised(def2);

        assertThat("Verify role1 is cached making initial request", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));
        assertThat("Verify role2 is cached making initial request", manager.getLicenseRole(role2.getId()),
                OptionMatchers.none());
        // Initial cache load finished with role1 having group 1 and role2 not existing.
        // Now we change the state of the store and ensure that the cache has not changed.
        store.save(new LicenseRoleData(def2.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        definitions.addAuthorised(def2);
        Option<LicenseRole> licenseRoleOption=manager.getLicenseRole(role2.getId());
        assertThat("Ensure role2 is still cached from initial request", licenseRoleOption,OptionMatchers.none());

        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        assertThat("Ensure role1 is still cached from initial request", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));

        // now verify that the cache has been reloaded with new values
        manager.onLicenseChanged(new NewLicenseEvent(new MockLicenseDetails()));

        assertThat("Verify role2 is reloaded in the cache", manager.getLicenseRole(role2.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role2)));
        assertThat("Verify role1 is reloaded in the cache", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1).groups(GROUP2)));
    }

    @Test
    public void licenseRoleDefinedClearsCacheEntry()
    {
        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP1), Option.<String>none()));
        definitions.removeAuthorised(def2);

        assertThat("Verify role1 is cached making initial request", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));
        assertThat("Verify role2 is cached making initial request", manager.getLicenseRole(role2.getId()),
                OptionMatchers.none());
        // Initial cache load finished with role1 having group 1 and role2 not existing.
        // Now we change the state of the store and ensure that the cache has not changed.
        store.save(new LicenseRoleData(def2.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        definitions.addAuthorised(def2);
        Option<LicenseRole> licenseRoleOption=manager.getLicenseRole(role2.getId());
        assertThat("Ensure role2 is still cached from initial request", licenseRoleOption,OptionMatchers.none());

        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        assertThat("Ensure role1 is still cached from initial request", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));

        // now verify that the cache has been reloaded with new values
        final LicenseRoleId id = LicenseRoleId.valueOf(LICENSE_ROLE_ID2);
        manager.onLicenseRoleDefined(new LicenseRoleDefinedEvent(id));

        // role2 has new values, role1 has not been flushed.
        assertThat("Verify role2 is now defined", manager.getLicenseRole(role2.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role2)));
        assertThat("Verify role1 is still not updated in the cache", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));
    }

    @Test
    public void licenseRoleUndefinedClearsCacheEntry()
    {
        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP1), Option.<String>none()));
        store.save(new LicenseRoleData(def2.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));

        assertThat("Verify role1 is cached making initial request", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));
        assertThat("Verify role2 is cached making initial request", manager.getLicenseRole(role2.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role2)));
        // Initial cache load finished with role1 having group 1 and role2 not existing.
        // Now we change the state of the store and ensure that the cache has not changed.
        definitions.removeAuthorised(def2);

        assertThat("Ensure that role2 is still cached from initial request", manager.getLicenseRole(role2.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role2)));

        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        assertThat("Ensure role1 is still cached from initial request", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));

        // now verify that the cache has been selectively reloaded with new values
        final LicenseRoleId id = LicenseRoleId.valueOf(LICENSE_ROLE_ID2);
        manager.onLicenseRoleUndefined(new LicenseRoleUndefinedEvent(id));

        assertThat("Verify role1 is still cached from initial request", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1).groups(GROUP1)));
        assertThat("Verify role2 is undefined" ,manager.getLicenseRole(role2.getId()), OptionMatchers.none());


    }

    @Test
    public void featureEnabledClearsCache()
    {
        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP1), Option.<String>none()));
        definitions.removeAuthorised(def2);

        assertThat("Ensure that role1 is cached", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));
        assertThat("Ensure that role2 is cached", manager.getLicenseRole(role2.getId()),
                OptionMatchers.none());
        // Initial cache load finished with role1 having group 1 and role2 not existing.
        // Now we change the state of the store and ensure that the cache has not changed.
        store.save(new LicenseRoleData(def2.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        definitions.addAuthorised(def2);
        Option<LicenseRole> licenseRoleOption=manager.getLicenseRole(role2.getId());
        assertThat("Ensure that cache still retains role2 as undefined", licenseRoleOption, OptionMatchers.none());

        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        assertThat("Ensure the cache retains the old role1", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));

        // now verify that the cache has been reloaded with new values
        manager.onFeatureChange(new FeatureEnabledEvent(CoreFeatures.LICENSE_ROLES_ENABLED.featureKey()));

        assertThat("Verify role2 is now defined", manager.getLicenseRole(role2.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role2)));
        assertThat("Verify that role1 has now reloaded from the store", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1).groups(GROUP2)));

    }

    @Test
    public void featureDisabledClearsCache()
    {
        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP1), Option.<String>none()));
        definitions.removeAuthorised(def2);

        assertThat("Ensure that role1 is cached", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));
        assertThat("Ensure that role2 is cached", manager.getLicenseRole(role2.getId()),
                OptionMatchers.none());
        // Initial cache load finished with role1 having group 1 and role2 not existing.
        // Now we change the state of the store and ensure that the cache has not changed.
        store.save(new LicenseRoleData(def2.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        definitions.addAuthorised(def2);
        Option<LicenseRole> licenseRoleOption=manager.getLicenseRole(role2.getId());
        assertThat("Ensure that cache still retains role2 as undefined", licenseRoleOption, OptionMatchers.none());

        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        assertThat("Ensure the cache retains the old role1", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));

        // now verify that the cache has been reloaded with new values
        manager.onFeatureChange(new FeatureDisabledEvent(CoreFeatures.LICENSE_ROLES_ENABLED.featureKey()));

        assertThat("Verify role2 is now defined", manager.getLicenseRole(role2.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role2)));
        assertThat("Verify that role1 has now reloaded from the store", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1).groups(GROUP2)));
    }

    @Test
    public void groupDeletedClearsCache()
    {
        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP1), Option.<String>none()));
        definitions.removeAuthorised(def2);

        assertThat("Ensure that role1 is cached", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));
        assertThat("Ensure that role2 is cached", manager.getLicenseRole(role2.getId()),
                OptionMatchers.none());
        // Initial cache load finished with role1 having group 1 and role2 not existing.
        // Now we change the state of the store and ensure that the cache has not changed.
        store.save(new LicenseRoleData(def2.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        definitions.addAuthorised(def2);
        Option<LicenseRole> licenseRoleOption=manager.getLicenseRole(role2.getId());
        assertThat("Ensure that cache still retains role2 as undefined", licenseRoleOption, OptionMatchers.none());

        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        assertThat("Ensure the cache retains the old role1", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));

        // now verify that the cache has been reloaded with new values
        manager.onGroupDeleted(null);

        assertThat("Verify role2 is now defined", manager.getLicenseRole(role2.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role2)));
        assertThat("Verify that role1 has now reloaded from the store", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1).groups(GROUP2)));
    }

    @Test
    public void groupCreatedClearsCache()
    {
        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP1), Option.<String>none()));
        definitions.removeAuthorised(def2);

        assertThat("Ensure that role1 is cached", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));
        assertThat("Ensure that role2 is cached", manager.getLicenseRole(role2.getId()),
                OptionMatchers.none());

        store.save(new LicenseRoleData(def2.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        definitions.addAuthorised(def2);
        Option<LicenseRole> licenseRoleOption=manager.getLicenseRole(role2.getId());
        assertThat("Ensure that cache still retains role2 as undefined", licenseRoleOption, OptionMatchers.none());

        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));
        assertThat("Ensure the cache retains the old role1", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));

        manager.onGroupCreated(null);

        assertThat("Verify role2 is now defined", manager.getLicenseRole(role2.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role2)));
        assertThat("Verify that role1 has now reloaded from the store", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1).groups(GROUP2)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setLicenseRoleFailsForUnAuthorisedRole()
    {
        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP1), Option.<String>none()));
        definitions.removeAuthorised(def1);

        manager.setLicenseRole(role1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setLicenseRoleFailsForInvalidGroupp()
    {
        //Group 4 is invalid in role3.
        when(groupManager.groupExists(GROUP4)).thenReturn(false);
        manager.setLicenseRole(role3);
    }

    @Test
    public void setLicenseDelegatesToStoreAndClearsCache()
    {
        final LicenseRoleId licenseRoleId = def1.getLicenseRoleId();
        final LicenseRole input = new MockLicenseRole()
                .name(licenseRoleId.getName())
                .id(licenseRoleId)
                .groups(GROUP1, GROUP2, GROUP3)
                .primaryGroup(GROUP2);

        //start with default license for role1 and role2
        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(GROUP1), Option.<String>none()));
        store.save(new LicenseRoleData(def2.getLicenseRoleId(), ImmutableSet.of(GROUP2), Option.<String>none()));

        //ensure licenses are different before starting test
        assertThat("input != role1 ", role1, Matchers.not(new LicenseRoleMatcher().merge(input)));
        // populate cache with role1.
        assertThat("Ensure that role1 is cached", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role1)));
        assertThat("Ensure that role2 is cached", manager.getLicenseRole(role2.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role2)));
        store.save(new LicenseRoleData(def2.getLicenseRoleId(), ImmutableSet.of(GROUP1, GROUP2), Option.<String>none()));

        // Set the role, check it has been stored, and the licenseRole has been changed in the cache.
        // Also role2 will be flushed
        final LicenseRole output = manager.setLicenseRole(input);
        assertThat("Verify that role1 is the one that was set", manager.getLicenseRole(role1.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(input)));
        assertThat("Verify role 2 in not modified while storing role1", manager.getLicenseRole(role2.getId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(role2)));

        assertThat("Verify setLicenseRole returned the input", output, new LicenseRoleMatcher().merge(input));
        assertThat("Verify the store has the new role1 version",
                store.get(licenseRoleId), new LicenseRoleDataMatcher().merge(input));
    }

    @Test
    public void setLicenseReturnsEmptyRoleIfCacheCantFindIt()
    {
        final LicenseRoleId licenseRoleId = def1.getLicenseRoleId();
        final LicenseRole input = new MockLicenseRole()
                .id(licenseRoleId)
                .groups(GROUP1, GROUP2, GROUP3)
                .primaryGroup(GROUP2);

        when(mockedStore.get(licenseRoleId)).thenReturn(
                new LicenseRoleData(licenseRoleId, Collections.<String>emptyList(), Option.<String>none()));
        LicenseRoleManager licenseRoleManager= new DefaultLicenseRoleManager(cacheManager, mockedStore, definitions, groupManager);
        final LicenseRole output = licenseRoleManager.setLicenseRole(input);

        assertThat("Verify the manager got an empty license role when cache failed to load it from store",
                output, new LicenseRoleMatcher().id(licenseRoleId).name(licenseRoleId.getName()));
        verify(mockedStore, times(1)).save(Mockito.<LicenseRoleData>any());
    }

    @Test
    public void licenseRoleLoaderReturnsNoneRoleNotAuthenticated()
    {
        final LicenseRoleId licenseRoleId = def1.getLicenseRoleId();
        store.save(licenseRoleId, GROUP1);

        definitions.removeAuthorised(def1);

        final Option<LicenseRole> load = manager.getLicenseRole(licenseRoleId);
        assertThat("Verify role1 is not loaded in the cache when it is not defined",load, OptionMatchers.none());
    }

    @Test
    public void licenseRoleLoaderReturnsOnlyValidGroups()
    {
        final LicenseRoleId licenseRoleId = def1.getLicenseRoleId();
        store.save(licenseRoleId, GROUP1, GROUP2);
        when(groupManager.groupExists(GROUP2)).thenReturn(false);

        final Option<LicenseRole> load = manager.getLicenseRole(licenseRoleId);
        assertThat("Verify that when Group2 is non-existent is removed from the license role's groups",
                load, OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(def1).groups(GROUP1)));
    }

    @Test
    public void licenseRoleLoaderReturnsRemovesInvalidPrimaryGroup()
    {
        final LicenseRoleId licenseRoleId = def1.getLicenseRoleId();

        // check when the group is not in the license groups
        store.save(new LicenseRoleData(licenseRoleId, Arrays.asList(GROUP1, GROUP2), Option.some(GROUP3)));
        Option<LicenseRole> load = manager.getLicenseRole(licenseRoleId);
        assertThat("Verify that when GROUP3 is not part of the license role's groups is removed from primary",
                load, OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(def1).groups(GROUP1, GROUP2)));

        // Check when the group manager indicates that group DOES NOT exist
        store.save(new LicenseRoleData(licenseRoleId, Arrays.asList(GROUP1, GROUP2, GROUP3), Option.some(GROUP3)));
        when(groupManager.groupExists(GROUP3)).thenReturn(false);
        load = manager.getLicenseRole(licenseRoleId);
        assertThat("Verify that GROUP3 being non-existent will be removed from primary group",
                load, OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher().merge(def1).groups(GROUP1, GROUP2)));
    }

    @Test
    public void licenseRoleLoaderReturnsValidPrimaryGroup()
    {
        final LicenseRoleId licenseRoleId = def1.getLicenseRoleId();
        store.save(new LicenseRoleData(licenseRoleId, Arrays.asList(GROUP1, GROUP2), Option.some(GROUP2)));

        final Option<LicenseRole> load = manager.getLicenseRole(licenseRoleId);
        assertThat("Primary group is returned by the cache",
                load, OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher()
                        .merge(def1).groups(GROUP1, GROUP2).primaryGroup(GROUP2)));
    }

    @Test
    public void removeGroupFromLicenseRolesRemovesGroupAndClearsCache()
    {
        store.save(new LicenseRoleData(def1.getLicenseRoleId(), Arrays.asList(GROUP1, GROUP2), Option.some(GROUP2)));
        store.save(new LicenseRoleData(def2.getLicenseRoleId(), Arrays.asList( GROUP1), Option.some(GROUP1)));

        assertThat("Pull role1 in to cache", manager.getLicenseRole(def1.getLicenseRoleId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher()
                        .merge(def1).groups(GROUP1, GROUP2).primaryGroup(GROUP2)));
        assertThat("Pull role2 in to cache", manager.getLicenseRole(def2.getLicenseRoleId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher()
                        .merge(def2).groups(GROUP1).primaryGroup(GROUP1)));

        //check that cacje doesn't change after the store changes.
        store.save(new LicenseRoleData(def2.getLicenseRoleId(), Arrays.asList( GROUP1, GROUP3), Option.some(GROUP3)));

        assertThat("Ensure role2 has not been updated in cache",
                manager.getLicenseRole(def2.getLicenseRoleId()), OptionMatchers.<LicenseRole>some(
                new LicenseRoleMatcher().merge(def2).groups(GROUP1).primaryGroup(GROUP1)));

        manager.removeGroupFromLicenseRoles(GROUP2);

        assertThat("Verify Group2 has been removed from role1", manager.getLicenseRole(def1.getLicenseRoleId()),
                OptionMatchers.<LicenseRole>some(new LicenseRoleMatcher()
                        .merge(def1).groups(GROUP1).primaryGroup(Option.<String>none())));
        assertThat("Verify that whole cache was cleared, hence new role2 has been updated",
                manager.getLicenseRole(def2.getLicenseRoleId()), OptionMatchers.<LicenseRole>some(
                        new LicenseRoleMatcher().merge(def2).groups(GROUP1, GROUP3).primaryGroup(GROUP3)));
    }

    @Test
    public void getUserCountForExistingRoleIdWithOneGroupMustReturnNumberOfUsersInGroup()
    {
        // When: Software role has software group only.
        defineExtraLicenses();
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP), Option.some(SOFTWARE_GROUP)));

        int activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 3 users (one is inactive)
        assertThat("Active Users with software group only", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

    }

    @Test
    public void getUserCountForExistingRoleIdWith2GroupsMustReturnSuperSetSize()
    {
        // software license role has vanilla and software groups
        // software group has one inactive and 3 active
        // vanilla group has one duplicate user with software and total of 2 active
        defineExtraLicenses();
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP, VANILLA_GROUP), Option.some(SOFTWARE_GROUP)));

        int activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 4 users (extra one from vanilla)
        assertThat("Active Users after Cache clear", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_UNIQUE_IN_VANILLA_AND_SOFTWARE));
    }

    @Test
    public void usersPerLicenseCacheFlushOnClearCacheEvent()
    {
        // When: Software role has software group only.
        defineExtraLicenses();
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP), Option.some(SOFTWARE_GROUP)));

        int activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 3 users (one is inactive)
        assertThat("Active Users with software group only", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        // Add vanilla group to software license role
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP, VANILLA_GROUP), Option.some(SOFTWARE_GROUP)));
        //Verify that cache was not flushed.
        activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 3 users (extra one from vanilla)
        assertThat("Active Users after Cache clear", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        // Send ClearCache event
        manager.clearCache(new ClearCacheEvent(new HashMap<String, Object>()));

        activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 4 users (extra one from vanilla)
        assertThat("Active Users after Cache clear", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_UNIQUE_IN_VANILLA_AND_SOFTWARE));
    }

    @Test
    public void usersPerLicenseCacheflushedWhenDirectoryModifiedEvent()
    {
        // When: Software role has software group only.
        defineExtraLicenses();
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP), Option.some(SOFTWARE_GROUP)));

        int activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 3 users (one is inactive)
        assertThat("Active Users with software group only", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        // Add vanilla group to software license role
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP, VANILLA_GROUP), Option.some(SOFTWARE_GROUP)));
        //Verify that cache was not flushed.
        activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 3 users (extra one from vanilla)
        assertThat("Active Users after Cache clear", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        // Send ClearCache event
        manager.onDirectoryModified(new DirectoryCreatedEvent(this, directory));

        activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 4 users (extra one from vanilla)
        assertThat("Active Users after Cache clear", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_UNIQUE_IN_VANILLA_AND_SOFTWARE));
    }

    @Test
    public void usersPerLicenseCacheflushedOnDirectorySynchronisationEvent()
    {
        // When: Software role has software group only.
        defineExtraLicenses();
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP), Option.some(SOFTWARE_GROUP)));

        int activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 3 users (one is inactive)
        assertThat("Active Users with software group only", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        // Add vanilla group to software license role
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP, VANILLA_GROUP), Option.some(SOFTWARE_GROUP)));
        //Verify that cache was not flushed.
        activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 3 users (extra one from vanilla)
        assertThat("Active Users after Cache clear", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        // Send ClearCache event
        manager.onDirectorySynchronisation(
                new RemoteDirectorySynchronisedEvent(this, remoteDirectory));

        activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 4 users (extra one from vanilla)
        assertThat("Active Users after Cache clear", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_UNIQUE_IN_VANILLA_AND_SOFTWARE));
    }

    @Test
    public void usersPerLicenseCacheflushedOnFeatureEvent()
    {
        // When: Software role has software group only.
        defineExtraLicenses();
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP), Option.some(SOFTWARE_GROUP)));

        when(featureEvent.feature()).thenReturn(LICENSE_ROLES_ENABLED.featureKey());

        int activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 3 users (one is inactive)
        assertThat("Active Users with software group only", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        // Add vanilla group to software license role
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP, VANILLA_GROUP), Option.some(SOFTWARE_GROUP)));
        //Verify that cache was not flushed.
        activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 3 users (extra one from vanilla)
        assertThat("Active Users after Cache clear", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        // Send Feature event enabling license roles
        manager.onFeatureChange(featureEvent);

        activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 4 users (extra one from vanilla)
        assertThat("Active Users after Cache clear", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_UNIQUE_IN_VANILLA_AND_SOFTWARE));
    }

    @Test
    public void usersPerLicenseCacheflushedOnLicenseRoleUndefinedEvent()
    {
        // When: Software role has software group only.
        defineExtraLicenses();
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP), Option.some(SOFTWARE_GROUP)));
        when(featureEvent.feature()).thenReturn(LICENSE_ROLES_ENABLED.featureKey());

        int activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 3 users (one is inactive)
        assertThat("Active Users with software group only", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        // Add vanilla group to software license role
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP, VANILLA_GROUP), Option.some(SOFTWARE_GROUP)));
        //Verify that cache was not flushed.
        activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 4 users (extra one from vanilla)
        assertThat("Active Users after Cache clear", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        // Send Feature event enabling license roles
        manager.onLicenseRoleUndefined(new LicenseRoleUndefinedEvent(roleIdSoftware));

        activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 4 users (extra one from vanilla)
        assertThat("Active Users after Cache clear", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_UNIQUE_IN_VANILLA_AND_SOFTWARE));
    }

    @Test
    public void usersPerLicenseCacheflushedOnLicenseRoleDefinedEvent()
    {
        // When: Software role has software group only.
        defineExtraLicenses();
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP), Option.some(SOFTWARE_GROUP)));
        when(featureEvent.feature()).thenReturn(LICENSE_ROLES_ENABLED.featureKey());

        int activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 3 users (one is inactive)
        assertThat("Active Users with software group only", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        // Add vanilla group to software license role
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP, VANILLA_GROUP), Option.some(SOFTWARE_GROUP)));
        //Verify that cache was not flushed.
        activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 4 users (extra one from vanilla)
        assertThat("Active Users after Cache clear", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        // Send Feature event enabling license roles
        manager.onLicenseRoleDefined(new LicenseRoleDefinedEvent(roleIdSoftware));

        activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 4 users (extra one from vanilla)
        assertThat("Active Users after Cache clear", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_UNIQUE_IN_VANILLA_AND_SOFTWARE));
    }

    @Test
    public void usersPerLicenseCacheflushedOnLicenseChangedEvent()
    {
        // When: Software role has software group only.
        defineExtraLicenses();
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP), Option.some(SOFTWARE_GROUP)));
        when(featureEvent.feature()).thenReturn(LICENSE_ROLES_ENABLED.featureKey());

        int activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 3 users (one is inactive)
        assertThat("Active Users with software group only", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        // Add vanilla group to software license role
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP, VANILLA_GROUP), Option.some(SOFTWARE_GROUP)));
        //Verify that cache was not flushed.
        activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 4 users (extra one from vanilla)
        assertThat("Active Users after Cache clear", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        // Send Feature event enabling license roles
        manager.onLicenseChanged(new NewLicenseEvent(new MockLicenseDetails()));

        activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 4 users (extra one from vanilla)
        assertThat("Active Users after Cache clear", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_UNIQUE_IN_VANILLA_AND_SOFTWARE));
    }

    @Test
    public void getUserCountWhenLicenseUndefinedMustReturnZero()
    {
        int activeSoftwareUsers = manager.getUserCount(roleIdUndefined);

        assertThat("Active Users after Cache clear", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_UNDEFINED_OR_NULL_LICENSE_ROLE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserCountForNullRoleId()
    {
        int activeSoftwareUsers = manager.getUserCount(null);
    }

    @Test
    public void getUserCountWithWithGroupsContainingSameUsersNotDoubleCounted()
    {
        // software license role has OHS and software groups
        // software group has one inactive and 3 active
        // vanilla group has total of 2 active and no duplicates to software.
        defineExtraLicenses();
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP, OHS_GROUP), Option.some(SOFTWARE_GROUP)));

        // Send ClearCache event
        manager.clearCache(new ClearCacheEvent(new HashMap<String, Object>()));

        int activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        // Expect to have 5 users 3 Software + 2 OHS
        assertThat("Active Users for OHS and Vanilla", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE_AND_OHS));
    }


    @Test
    public void usersPerLicenseCacheFlushedWhenRemovingGroupFromRoles()
    {
        defineExtraLicenses();
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP, OHS_GROUP),
                Option.some(SOFTWARE_GROUP)));
        store.save(new LicenseRoleData(role1.getId(), ImmutableSet.of(SOFTWARE_GROUP), Option.<String>none()));
        when(featureEvent.feature()).thenReturn(LICENSE_ROLES_ENABLED.featureKey());

        int activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        assertThat("Software Role has software and OHS users", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE_AND_OHS));
        assertThat("Role1 has software users", manager.getUserCount(role1.getId()),
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(VANILLA_GROUP), Option.<String>none()));
        assertThat("Role1 is still cached with Software Users", manager.getUserCount(role1.getId()),
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        manager.removeGroupFromLicenseRoles(OHS_GROUP);

        assertThat("Software users are now only Software after OHS group removed", manager.getUserCount(roleIdSoftware),
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));
        assertThat("Role1 is pulled in to the cached with Vanilla Users", manager.getUserCount(role1.getId()),
                Matchers.equalTo(USERS_ACTIVE_IN_VANILLA));
    }

    @Test
    public void usersPerLicenseCacheFlushedWhenAddingGroupToRole()
    {
        defineExtraLicenses();
        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP),
                Option.some(SOFTWARE_GROUP)));
        store.save(new LicenseRoleData(role1.getId(), ImmutableSet.of(SOFTWARE_GROUP), Option.<String>none()));
        when(featureEvent.feature()).thenReturn(LICENSE_ROLES_ENABLED.featureKey());

        int activeSoftwareUsers = manager.getUserCount(roleIdSoftware);

        assertThat("Software Role has software and OHS users", activeSoftwareUsers,
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));
        assertThat("Role1 has software users", manager.getUserCount(role1.getId()),
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        store.save(new LicenseRoleData(def1.getLicenseRoleId(), ImmutableSet.of(VANILLA_GROUP), Option.<String>none()));
        assertThat("Role1 is still cached with Software Users", manager.getUserCount(role1.getId()),
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));

        LicenseRole newSoftwareRole = new MockLicenseRole().groups(SOFTWARE_GROUP,OHS_GROUP).id(roleIdSoftware).
                primaryGroup(Option.some(SOFTWARE_GROUP));
        manager.setLicenseRole(newSoftwareRole);

        assertThat("Software users are now only Software after OHS group removed", manager.getUserCount(roleIdSoftware),
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE_AND_OHS));

        assertThat("Cache did not refresh role1 active users still Software", manager.getUserCount(role1.getId()),
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));
    }

    @Test
    public void usersForPlatformLicenseAreNotDoubleCountedWithOtherActiveRoles()
    {
        defineExtraLicenses();
        definitions.addAuthorised(new MockLicenseRoleDefinition(roleIdPlatform.getName()));
        definitions.addDefined(new MockLicenseRoleDefinition(roleIdPlatform.getName()));
        definitions.removeAuthorised(new MockLicenseRoleDefinition(LICENSE_ROLE_ID1));
        definitions.removeAuthorised(new MockLicenseRoleDefinition(LICENSE_ROLE_ID2));
        definitions.removeAuthorised(new MockLicenseRoleDefinition(LICENSE_ROLE_ID3));

        store.save(new LicenseRoleData(roleIdSoftware, ImmutableSet.of(SOFTWARE_GROUP),
                Option.some(SOFTWARE_GROUP)));
        store.save(new LicenseRoleData(roleIdPlatform, ImmutableSet.of(SOFTWARE_GROUP, OHS_GROUP, VANILLA_GROUP),
                Option.some(VANILLA_GROUP)));

        //Preconditions:
        assertThat("Software group has different amount of users from combined OHS and Vanilla",
                USERS_ACTIVE_IN_SOFTWARE, Matchers.not(Matchers.equalTo(USERS_ACTIVE_IN_VANILLA_AND_OHS)));

        //then:
        assertThat("Cache did not refresh role1 active users still Software", manager.getUserCount(roleIdSoftware),
                Matchers.equalTo(USERS_ACTIVE_IN_SOFTWARE));
        assertThat("Cache did not refresh role1 active users still Software", manager.getUserCount(roleIdPlatform),
                Matchers.equalTo(USERS_ACTIVE_IN_VANILLA_AND_OHS_UNIQUE_TO_SOFTWARE));

        definitions.removeAuthorised(new MockLicenseRoleDefinition(roleIdSoftware.getName()));
        manager.clearCache(null);
        assertThat("Cache did not refresh role1 active users still Software", manager.getUserCount(roleIdPlatform),
                Matchers.equalTo(UNIQUE_USERS_IN_ALL_GROUPS));

    }
}
