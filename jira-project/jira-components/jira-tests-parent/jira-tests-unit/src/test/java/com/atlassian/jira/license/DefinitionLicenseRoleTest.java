package com.atlassian.jira.license;

import com.atlassian.fugue.Option;
import com.atlassian.jira.matchers.OptionMatchers;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.matchers.OptionMatchers.none;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class DefinitionLicenseRoleTest
{
    @Test (expected = IllegalArgumentException.class)
    public void cotrFailsWithDefinition()
    {
        new DefinitionLicenseRole(null, Collections.<String>emptyList(), Option.none(String.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsWithNullGroups()
    {
        new DefinitionLicenseRole(newDef("id", "name"), null, Option.none(String.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsWithNullInGroups()
    {
        new DefinitionLicenseRole(newDef("id", "name"), Collections.<String>singletonList(null), Option.none(String.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsWithNullPrimaryGroup()
    {
        new DefinitionLicenseRole(newDef("id", "name"), Collections.<String>emptyList(), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsWithPrimaryGroupNotListedInGroups()
    {
        new DefinitionLicenseRole(newDef("id", "name"), Arrays.asList("one", "two"), Option.option("three"));
    }

    @Test
    public void equalsOnlyListensToId()
    {
        List<LicenseRole> roles = Arrays.<LicenseRole>asList(
                newRole("id", "name", null, "one"),
                newRole("id", "name", null, "two", "thee"),
                newRole("id", "name", "three", "two", "three"),
                newRole("id", "otherName", "three", "two", "three"));

        for (LicenseRole left : roles)
        {
            for (LicenseRole right : roles)
            {
                assertThat(left.equals(right), equalTo(true));
            }
        }

        List<LicenseRole> otherRoles = Arrays.<LicenseRole>asList(
                newRole("id2", "name", null, "one"),
                newRole("id2", "name", null, "two", "thee"),
                newRole("id2", "name", "three", "two", "three"),
                newRole("id2", "otherName", "three", "two", "three"));

        for (LicenseRole left : roles)
        {
            for (LicenseRole right : otherRoles)
            {
                assertThat(left.equals(right), equalTo(false));
            }
        }
    }

    @Test
    public void hashListensToId()
    {
        List<LicenseRole> roles = Arrays.<LicenseRole>asList(
                newRole("id", "name", null, "one"),
                newRole("id", "name", null, "two", "thee"),
                newRole("id", "name", "three", "two", "three"),
                newRole("id", "otherName", "three", "two", "three"));

        for (LicenseRole left : roles)
        {
            for (LicenseRole right : roles)
            {
                assertThat(left.hashCode() == right.hashCode(), equalTo(true));
            }
        }
    }

    @Test
    public void getGroupsReturnsGroups()
    {
        assertThat(newRole("id", "name", null, "one").getGroups(), contains("one"));
        assertThat(newRole("id", "name", null, "one", "two").getGroups(), containsInAnyOrder("one", "two"));
    }

    @Test
    public void getPrimaryGroupRetunsPrimary()
    {
        assertThat(newRole("id", "name", null, "one").getPrimaryGroup(), none());
        assertThat(newRole("id", "name", "two", "one", "two").getPrimaryGroup(), OptionMatchers.some("two"));
    }

    @Test
    public void getIdReturnsIdFromDef()
    {
        assertThat(newRole("id", "name", null, "one").getId(), equalTo(LicenseRoleId.valueOf("id")));
    }

    @Test
    public void getNameReturnsNameFromDef()
    {
        assertThat(newRole("id", "name", null, "one").getName(), equalTo("name"));
    }

    @Test
    public void withGroupsUpdatesGroupInformation()
    {
        final LicenseRole orig = newRole("id", "name", null, "one");
        final LicenseRole newRole = orig.withGroups(Arrays.asList("one", "two"), Option.none(String.class));

        assertThat(orig, new LicenseRoleMatcher().id("id").name("name").groups("one"));
        assertThat(newRole, new LicenseRoleMatcher().id("id").name("name").groups("one", "two"));
    }

    @Test
    public void withGroupsUpdatesPrimaryGroupInformation()
    {
        final LicenseRole orig = newRole("id", "name", null, "one");
        final LicenseRole newRole = orig.withGroups(Arrays.asList("one", "two"), Option.some("two"));

        assertThat(orig, new LicenseRoleMatcher().id("id").name("name").groups("one"));
        assertThat(newRole, new LicenseRoleMatcher().id("id").name("name").groups("one", "two").primaryGroup("two"));

        final LicenseRole noneRole = newRole.withGroups(Arrays.asList("one", "two"), Option.none(String.class));

        assertThat(newRole, new LicenseRoleMatcher().id("id").name("name").groups("one", "two").primaryGroup("two"));
        assertThat(noneRole, new LicenseRoleMatcher().id("id").name("name").groups("one", "two"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithNullGroups()
    {
        final LicenseRole idLicenseRole = newRole("id", null, "one");
        idLicenseRole.withGroups(null, Option.none(String.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithNullInGroups()
    {
        final LicenseRole idLicenseRole = newRole("id", null, "one");
        idLicenseRole.withGroups(Arrays.asList("abc", null), Option.none(String.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithNullPrimary()
    {
        final LicenseRole idLicenseRole = newRole("id", null, "one");
        idLicenseRole.withGroups(Arrays.asList("abc"), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithPrimaryNotInGroup()
    {
        final LicenseRole idLicenseRole = newRole("id", null, "one");
        idLicenseRole.withGroups(Arrays.asList("abc"), Option.some("def"));
    }

    private static DefinitionLicenseRole newRole(String id, String name, String primary, String...groups)
    {
        return new DefinitionLicenseRole(newDef(id, name), Arrays.asList(groups), Option.option(primary));
    }

    private static LicenseRoleDefinition newDef(String id, String name)
    {
        return new MockLicenseRoleDefinition(id, name);
    }
}