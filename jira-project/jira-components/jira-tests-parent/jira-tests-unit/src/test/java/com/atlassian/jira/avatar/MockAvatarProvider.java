package com.atlassian.jira.avatar;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.atlassian.jira.user.ApplicationUser;

/**
 * One shot {@link com.atlassian.jira.avatar.JiraAvatarSupport} that just returns a singleton
 * {@link com.atlassian.jira.avatar.JiraPluginAvatar} bean.
 */
public class MockAvatarProvider implements JiraAvatarSupport
{
    private JiraPluginAvatar jiraPluginAvatar;

    public MockAvatarProvider(final String ownerId, final String url, final int size, final String contentType, final boolean external, final byte[] bytes)
    {

        this.jiraPluginAvatar = new JiraPluginAvatar()
        {
            @Override
            public String getOwnerId()
            {
                return ownerId;
            }

            @Override
            public String getUrl()
            {
                return url;
            }

            @Override
            public int getSize()
            {
                return size;
            }

            @Override
            public String getContentType()
            {
                return contentType;
            }

            @Override
            public boolean isExternal()
            {
                return external;
            }

            @Override
            public InputStream getBytes() throws IOException
            {
                return new ByteArrayInputStream(bytes);
            }
        };
    }

    public MockAvatarProvider(final Avatar a)
    {
        final String owner = a.getOwner();

    }

    @Override
    public JiraPluginAvatar getAvatar(final String email, final String size)
    {
        return jiraPluginAvatar;
    }

    @Override
    public JiraPluginAvatar getAvatar(final ApplicationUser user, final String size)
    {
        return jiraPluginAvatar;
    }

    @Override
    public JiraPluginAvatar getAvatarById(final Long avatarId, final String size)
    {
        return jiraPluginAvatar;
    }

}
