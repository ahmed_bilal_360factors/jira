package com.atlassian.jira.issue.attachment.store.strategy.move;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import com.google.common.io.Files;

import org.apache.commons.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import static com.atlassian.jira.matchers.FileMatchers.hasContent;
import static com.atlassian.jira.matchers.FileMatchers.isFile;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;

public class TestIOUtilsWrapper
{
    private static final String SAMPLE_FILE_CONTENT = "sample file content";
    public static final String UTF_8 = "UTF-8";

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    private final IOUtilsWrapper ioUtilsWrapper = new IOUtilsWrapper();

    @Test
    public void testOpenInputStreamToFile() throws Exception
    {
        final File file = createSampleFile();

        InputStream inputStream = null;
        try
        {
            inputStream = ioUtilsWrapper.openInputStream(file);

            final String streamContent = IOUtils.toString(inputStream, UTF_8);
            assertThat(streamContent, equalTo(SAMPLE_FILE_CONTENT));
        }
        finally
        {
            IOUtils.closeQuietly(inputStream);
        }
    }

    @Test
    public void testOpenOutputStreamToFile() throws Exception
    {
        final File file = temporaryFolder.newFile();

        FileOutputStream outputStream = null;
        try
        {
            outputStream = ioUtilsWrapper.openOutputStream(file);

            outputStream.write(SAMPLE_FILE_CONTENT.getBytes(UTF_8));

            assertThat(file, hasContent(SAMPLE_FILE_CONTENT));
        }
        finally
        {
            IOUtils.closeQuietly(outputStream);
        }
    }

    @Test
    public void testCopyStreamToOutput() throws Exception
    {
        final File file = temporaryFolder.newFile();

        FileOutputStream outputStream = null;
        try
        {
            outputStream = new FileOutputStream(file);
            final InputStream inputStream = new ByteArrayInputStream(SAMPLE_FILE_CONTENT.getBytes(UTF_8));

            ioUtilsWrapper.copy(inputStream, outputStream);

            assertThat(file, hasContent(SAMPLE_FILE_CONTENT));
        }
        finally
        {
            IOUtils.closeQuietly(outputStream);
        }
    }

    @Test
    public void testCreateTemporaryFile() throws Exception
    {
        final File tempFile = ioUtilsWrapper.createTempFile("some_prefix", "some_suffix");
        tempFile.deleteOnExit();

        assertThat(tempFile, isFile());
        assertThat(tempFile.getName(), allOf(startsWith("some_prefix"), endsWith("some_suffix")));
    }

    private File createSampleFile() throws Exception
    {
        final File file = temporaryFolder.newFile();
        Files.write(SAMPLE_FILE_CONTENT.getBytes(UTF_8), file);
        return file;
    }
}