package com.atlassian.jira.database;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.Datasource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class TestSupportedCollations
{

    private final List<String> supportedOracleCollations = Arrays.asList("BINARY");
    private final List<String> supportedMySqlCollations = Arrays.asList("utf8_bin");
    private final List<String> supportedPostgresCollations = Arrays.asList("C", "POSIX");
    private final List<String> supportedSqlServerCollations = Arrays.asList("SQL_Latin1_General_CP437_CI_AI", "SQL_Latin1_General_CI_AI");
    private final List<String> emptyList = Arrays.asList();

    @Test
    public void testSupportedCollationsForOracle() throws Exception
    {
        assertSupportedCollationsForDatabase("oracle11g", supportedOracleCollations);
    }

    @Test
    public void testSupportedCollationsForMySql() throws Exception
    {
        assertSupportedCollationsForDatabase("mysql", supportedMySqlCollations);
    }

    @Test
    public void testSupportedCollationsForPostgres() throws Exception
    {
        assertSupportedCollationsForDatabase("postgres84", supportedPostgresCollations);
    }

    @Test
    public void testSupportedCollationsForSqlServer() throws Exception
    {
        assertSupportedCollationsForDatabase("mssql", supportedSqlServerCollations);
    }

    @Test
    public void testSupportedCollationsForHsql() throws Exception
    {
        assertSupportedCollationsForDatabase("hsql", emptyList);
    }

    @Test
    public void testSupportedCollationsForUnknownDatabase() throws Exception
    {
        assertSupportedCollationsForDatabase("unknown", emptyList);
    }

    @Test
    public void testOracleIsSupported() throws Exception
    {
        assertIsSupported("oracle11g", supportedOracleCollations);
    }

    @Test
    public void testMySqlIsSupported() throws Exception
    {
        assertIsSupported("mysql", supportedMySqlCollations);
    }

    @Test
    public void testPostgresIsSupported() throws Exception
    {
        assertIsSupported("postgres84", supportedPostgresCollations);
    }

    @Test
    public void testHsqlIsSupported() throws Exception
    {
        assertIsSupported("hsql", emptyList);
    }

    @Test
    public void testSqlServerIsSupported() throws Exception
    {
        assertIsSupported("mssql", supportedSqlServerCollations);
    }

    @Test
    public void testUnknownDatabaseNotSupported() throws Exception
    {
        assertNotSupported("unknown", supportedSqlServerCollations);
    }

    @Test
    public void testUnknownCollationNotSupported() throws Exception
    {
        assertNotSupported("mssql", Arrays.asList("unknown"));
    }

    // Helpers

    private void assertSupportedCollationsForDatabase(final String databaseType, final List<String> expectedCollations)
    {
        Datasource mockDatasource = mock(Datasource.class);
        DatabaseConfig databaseConfig = new DatabaseConfig(databaseType, null, mockDatasource);
        SupportedCollations supportedCollations = new SupportedCollations();

        Collection<String> actualCollations = supportedCollations.forDatabase(databaseConfig);

        assertTrue(CollectionUtils.isEqualCollection(expectedCollations, actualCollations));
    }

    private void assertIsSupported(final String databaseType, final List<String> expectedCollations)
    {
        Datasource mockDatasource = mock(Datasource.class);
        DatabaseConfig databaseConfig = new DatabaseConfig(databaseType, null, mockDatasource);
        SupportedCollations supportedCollations = new SupportedCollations();

        for (String collation : expectedCollations)
        {
            assertTrue(supportedCollations.isSupported(databaseConfig, collation));
        }
    }

    private void assertNotSupported(final String databaseType, final List<String> expectedCollations)
    {
        Datasource mockDatasource = mock(Datasource.class);
        DatabaseConfig databaseConfig = new DatabaseConfig(databaseType, null, mockDatasource);
        SupportedCollations supportedCollations = new SupportedCollations();

        for (String collation : expectedCollations)
        {
            assertFalse(supportedCollations.isSupported(databaseConfig, collation));
        }
    }
}
