package com.atlassian.jira.bulkedit.operation;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.LongIdsValueHolder;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @since v6.4
 */
@RunWith (MockitoJUnitRunner.class)
public class TestBulkEditOperation extends TestCase
{
    @Mock
    private IssueManager issueManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private ProjectManager projectManager;

    @Mock
    private FieldManager fieldManager;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private OrderableField field;

    BulkEditOperation bulkEditOperation = new BulkEditOperation(issueManager, permissionManager, projectManager, fieldManager, authenticationContext);

    @Test
    public void testSubtractValuesForLongIdsValueHolder()
    {
        List<Long> fieldValuesBefore = Lists.newArrayList();
        fieldValuesBefore.add(new Long(10001));
        fieldValuesBefore.add(new Long(10002));
        List<Long> fieldValuesAfter = Lists.newArrayList();
        fieldValuesAfter.add(new Long(10001));
        fieldValuesAfter.add(new Long(10002));
        fieldValuesAfter.add(new Long(10004));

        LongIdsValueHolder valueHolderAfter = new LongIdsValueHolder(Lists.newArrayList(fieldValuesAfter));
        LongIdsValueHolder valueHolderBefore = new LongIdsValueHolder(Lists.newArrayList(fieldValuesBefore));

        Collection result = bulkEditOperation.subtractValues((Collection) valueHolderAfter, (Collection) valueHolderBefore);

        assertThat(result.size(), equalTo(1));
        assertThat(result.contains(new Long(10004)), is(true));
        assertThat(result, instanceOf(List.class));
    }

    @Test
    public void testSubtractValuesForSets()
    {
        Set<String> fieldValuesBefore = Sets.newHashSet();
        fieldValuesBefore.add("Label1");
        fieldValuesBefore.add("Label2");
        Set<String> fieldValuesAfter = Sets.newHashSet();
        fieldValuesAfter.add("Label1");
        fieldValuesAfter.add("Label2");
        fieldValuesAfter.add("Label3");
        fieldValuesAfter.add("Label4");

        Collection result = bulkEditOperation.subtractValues((Collection) fieldValuesAfter, (Collection) fieldValuesBefore);

        assertThat(result, instanceOf(Set.class));
        assertThat(result.size(), equalTo(2));
        assertThat(result.contains("Label3"), is(true));
        assertThat(result.contains("Label4"), is(true));
    }

    @Test
    public void testUpdateFieldValuesHolderComponents()
    {
        List<Long> fieldValuesOld = Lists.newArrayList();
        fieldValuesOld.add(new Long(10001));
        fieldValuesOld.add(new Long(10002));
        List<Long> fieldValuesNew = Lists.newArrayList();
        fieldValuesNew.add(new Long(10003));
        fieldValuesNew.add(new Long(10004));

        LongIdsValueHolder valueHolderOld = new LongIdsValueHolder(Lists.newArrayList(fieldValuesOld));
        valueHolderOld.setValuesToAdd(Sets.newHashSet("NewValue"));
        LongIdsValueHolder valueHolderNew = new LongIdsValueHolder(Lists.newArrayList(fieldValuesNew));

        Map<String,Object> fieldValuesHolder = Maps.newHashMap();
        fieldValuesHolder.put(IssueFieldConstants.COMPONENTS, valueHolderOld);

        when(field.getId()).thenReturn(IssueFieldConstants.COMPONENTS);

        bulkEditOperation.updateFieldValuesHolder(field, (Collection) valueHolderNew, fieldValuesHolder, true);

        assertThat((Collection<Object>) fieldValuesHolder.get(IssueFieldConstants.COMPONENTS), instanceOf(LongIdsValueHolder.class));
        assertThat(((Collection<Object>) fieldValuesHolder.get(IssueFieldConstants.COMPONENTS)).size(), equalTo(4));
        assertThat((Collection<Object>) fieldValuesHolder.get(IssueFieldConstants.COMPONENTS), hasItem(new Long(10001)));
        assertThat((Collection<Object>) fieldValuesHolder.get(IssueFieldConstants.COMPONENTS), hasItem(new Long(10002)));
        assertThat((Collection<Object>) fieldValuesHolder.get(IssueFieldConstants.COMPONENTS), hasItem(new Long(10003)));
        assertThat((Collection<Object>) fieldValuesHolder.get(IssueFieldConstants.COMPONENTS), hasItem(new Long(10004)));
        assertThat(((LongIdsValueHolder) fieldValuesHolder.get(IssueFieldConstants.COMPONENTS)).getValuesToAdd().isEmpty(), is(true));
    }

    @Test
    public void testUpdateFieldValuesHolderVersions()
    {
        List<Long> fieldValuesOld = Lists.newArrayList();
        fieldValuesOld.add(new Long(10001));
        fieldValuesOld.add(new Long(10002));
        List<Long> fieldValuesNew = Lists.newArrayList();
        fieldValuesNew.add(new Long(10003));
        fieldValuesNew.add(new Long(10004));

        LongIdsValueHolder valueHolderOld = new LongIdsValueHolder(Lists.newArrayList(fieldValuesOld));
        valueHolderOld.setValuesToAdd(Sets.newHashSet("NewValue"));
        LongIdsValueHolder valueHolderNew = new LongIdsValueHolder(Lists.newArrayList(fieldValuesNew));

        Map<String,Object> fieldValuesHolder = Maps.newHashMap();
        fieldValuesHolder.put(IssueFieldConstants.FIX_FOR_VERSIONS, valueHolderOld);

        when(field.getId()).thenReturn(IssueFieldConstants.FIX_FOR_VERSIONS);

        bulkEditOperation.updateFieldValuesHolder(field, (Collection) valueHolderNew, fieldValuesHolder, false);

        assertThat((Collection<Object>) fieldValuesHolder.get(IssueFieldConstants.FIX_FOR_VERSIONS), instanceOf(LongIdsValueHolder.class));
        assertThat(((Collection<Object>) fieldValuesHolder.get(IssueFieldConstants.FIX_FOR_VERSIONS)).size(), equalTo(4));
        assertThat((Collection<Object>) fieldValuesHolder.get(IssueFieldConstants.FIX_FOR_VERSIONS), hasItem(new Long(10001)));
        assertThat((Collection<Object>) fieldValuesHolder.get(IssueFieldConstants.FIX_FOR_VERSIONS), hasItem(new Long(10002)));
        assertThat((Collection<Object>) fieldValuesHolder.get(IssueFieldConstants.FIX_FOR_VERSIONS), hasItem(new Long(10003)));
        assertThat((Collection<Object>) fieldValuesHolder.get(IssueFieldConstants.FIX_FOR_VERSIONS), hasItem(new Long(10004)));
        assertThat(((LongIdsValueHolder) fieldValuesHolder.get(IssueFieldConstants.FIX_FOR_VERSIONS)).getValuesToAdd().isEmpty(), is(false));
        assertThat(((LongIdsValueHolder) fieldValuesHolder.get(IssueFieldConstants.FIX_FOR_VERSIONS)).getValuesToAdd(), hasItem("NewValue"));
        assertThat(((LongIdsValueHolder) fieldValuesHolder.get(IssueFieldConstants.FIX_FOR_VERSIONS)).getValuesToAdd().size(), equalTo(1));
    }

    @Test
    public void testUpdateFieldValuesHolderLabels()
    {

        Set<String> fieldValuesOld = Sets.newHashSet();
        fieldValuesOld.add("Label1");
        fieldValuesOld.add("Label2");
        Set<String> fieldValuesNew = Sets.newHashSet();
        fieldValuesNew.add("Label3");

        Map<String,Object> fieldValuesHolder = Maps.newHashMap();
        fieldValuesHolder.put(IssueFieldConstants.LABELS, fieldValuesOld);

        when(field.getId()).thenReturn(IssueFieldConstants.LABELS);

        bulkEditOperation.updateFieldValuesHolder(field, (Collection) fieldValuesNew, fieldValuesHolder, false);

        assertThat((Collection<Object>) fieldValuesHolder.get(IssueFieldConstants.LABELS), instanceOf(Set.class));
        assertThat(((Collection<Object>) fieldValuesHolder.get(IssueFieldConstants.LABELS)).size(), equalTo(2));
        assertThat((Collection<Object>) fieldValuesHolder.get(IssueFieldConstants.LABELS), hasItem("Label1"));
        assertThat((Collection<Object>) fieldValuesHolder.get(IssueFieldConstants.LABELS), hasItem("Label2"));
    }

}
