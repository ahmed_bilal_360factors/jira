package com.atlassian.jira.util.system;

import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizFactory;
import com.atlassian.jira.service.services.analytics.JiraLifecycleAnalyticsService;
import com.atlassian.jira.upgrade.UpgradeManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Map;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ExtendedSystemInfoUtilsImplTest
{
    @Mock
    JiraLicenseService jiraLicenseService;

    @Mock
    BuildUtilsInfo buildUtilsInfo;

    @Mock
    UpgradeManager upgradeManager;

    @Mock
    ApplicationProperties applicationProperties;

    @Mock
    JiraLifecycleAnalyticsService jiraLifecycleAnalyticsService;

    @Mock
    I18nHelper i18nHelper;

    @InjectMocks ExtendedSystemInfoUtilsImpl extendedSystemInfoUtils;

    @Before
    public void setUp() throws Exception
    {
        when(i18nHelper.getText(anyString())).thenAnswer(new Answer<String>()
        {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable
            {
                Object[] args = invocation.getArguments();
                return (String) args[0];
            }
        });

        when(jiraLifecycleAnalyticsService.getUsageStats(true)).thenReturn(getJiraLifecycleAnalyticsServiceKeys());

        final MockComponentWorker componentAccessorWorker = new MockComponentWorker();
        componentAccessorWorker.registerMock(OfBizDelegator.class, OfBizFactory.getOfBizDelegator());
        ComponentAccessor.initialiseWorker(componentAccessorWorker);
    }

    @Test
    public void shouldAddThreeParamsToDatabaseStatsManually()
    {
        final Map<String, String> usageStats = extendedSystemInfoUtils.getUsageStats();
        Assert.assertEquals(16, usageStats.size());
        Assert.assertThat(usageStats.keySet(), Matchers.contains(
                "admin.systeminfo.attachments",
                "admin.systeminfo.comments",
                "admin.systeminfo.components",
                "admin.systeminfo.custom.fields",
                "admin.systeminfo.groups",
                "admin.systeminfo.issues",
                "admin.systeminfo.issuesecuritylevels",
                "admin.systeminfo.issuetypes",
                "admin.systeminfo.priorities",
                "admin.systeminfo.projects",
                "admin.systeminfo.resolutions",
                "admin.systeminfo.screens",
                "admin.systeminfo.screensschemes",
                "admin.systeminfo.statuses",
                "admin.systeminfo.users",
                "admin.systeminfo.versions"));

    }

    private Map<String, Object> getJiraLifecycleAnalyticsServiceKeys() {
        return ImmutableMap.<String,Object>builder()
                .put("issues", 0)
                .put("projects", 0)
                .put("comments", 0)
                .put("customfields", 0)
                .put("issuetypes", 0)
                .put("permissionschemes", 0)
                .put("status", 0)
                .put("resolutions", 0)
                .put("priorities", 0)
                .put("versions", 0)
                .put("components", 0)
                .put("issuesecuritylevels", 0)
                .put("screens", 0)
                .put("screensschemes", 0)
                .put("permission.scheme.avg", 0).build();

    }

}