package com.atlassian.jira.issue.index;

import java.io.IOException;

import com.atlassian.jira.config.util.IndexingConfiguration;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.junit.rules.Log4jLogger;
import com.atlassian.jira.util.searchers.MockSearcherFactory;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.AlreadyClosedException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static com.atlassian.jira.issue.index.IndexDirectoryFactory.Name;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestSearcherCache
{
    @Rule
    public InitMockitoMocks mocks = new InitMockitoMocks(this);
    @Rule
    public Log4jLogger logger = new Log4jLogger();
    @Mock
    IssueIndexer issueIndexer;
    @Mock
    IndexingConfiguration indexingConfiguration;

    @Before
    public void setUp() throws Exception
    {
        when(issueIndexer.openEntitySearcher(any(Name.class))).thenReturn(MockSearcherFactory.getCleanSearcher());
    }

    /**
     * Check that the ThreadLocalSearcherCache is only cached once per request-cache
     */
    @Test
    public void testGetSearcher() throws IOException
    {
        final SearcherCache cache = SearcherCache.getThreadLocalCache();
        final IndexSearcher searcher = cache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, Name.ISSUE);
        final IndexSearcher anotherSearcher = cache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, Name.ISSUE);

        assertSame(searcher, anotherSearcher);

        cache.closeSearchers();
        when(issueIndexer.openEntitySearcher(any(Name.class))).thenReturn(MockSearcherFactory.getCleanSearcher());
        final IndexSearcher newSearcher = cache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, Name.ISSUE);

        assertNotSame(searcher, newSearcher);
    }

    /**
     * Check that the ThreadLocalSearcherCache is only cached once per request-cache
     */
    @Test
    public void testGetCommentSearcher() throws IOException
    {
        final SearcherCache cache = SearcherCache.getThreadLocalCache();
        final IndexSearcher searcher = cache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, Name.COMMENT);
        final IndexSearcher anotherSearcher = cache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, Name.COMMENT);

        assertSame(searcher, anotherSearcher);

        cache.closeSearchers();
        when(issueIndexer.openEntitySearcher(any(Name.class))).thenReturn(MockSearcherFactory.getCleanSearcher());
        final IndexSearcher newSearcher = cache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, Name.COMMENT);

        assertNotSame(searcher, newSearcher);
    }

    /**
     * Check that the ThreadLocalSearcherCache is only cached once per requestcache
     */
    @Test
    public void testGetReader() throws IOException
    {
        final SearcherCache cache = SearcherCache.getThreadLocalCache();
        final IndexSearcher searcher = cache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, Name.ISSUE);
        final IndexReader reader = searcher.getIndexReader();
        final IndexReader anotherReader = cache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, Name.ISSUE).getIndexReader();

        assertSame(reader, anotherReader);

        cache.closeSearchers();
        when(issueIndexer.openEntitySearcher(any(Name.class))).thenReturn(MockSearcherFactory.getCleanSearcher());
        final IndexReader newReader = cache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, Name.ISSUE).getIndexReader();

        assertNotSame(reader, newReader);
    }

    @Test
    public void testResetSearchers() throws IOException
    {
        final SearcherCache cache = SearcherCache.getThreadLocalCache();
        cache.closeSearchers();

        final IndexSearcher issueSearcher = mock(IndexSearcher.class);
        final IndexSearcher commentSearcher = mock(IndexSearcher.class);
        final IndexSearcher worklogSearcher = mock(IndexSearcher.class);

        when(issueIndexer.openEntitySearcher(any(Name.class))).thenReturn(issueSearcher);
        cache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, Name.ISSUE);

        when(issueIndexer.openEntitySearcher(any(Name.class))).thenReturn(commentSearcher);
        cache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, Name.COMMENT);

        when(issueIndexer.openEntitySearcher(any(Name.class))).thenReturn(worklogSearcher);
        cache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, Name.WORKLOG);

        cache.closeSearchers();

        verify(issueSearcher).close();
        verify(commentSearcher).close();
        verify(worklogSearcher).close();
    }

    @Test
    public void testCloseSearchers() throws IOException
    {
        final SearcherCache cache = SearcherCache.getThreadLocalCache();
        cache.closeSearchers();

        final IndexSearcher issueSearcher = mock(IndexSearcher.class);
        final IndexSearcher commentSearcher = mock(IndexSearcher.class);
        final IndexSearcher worklogSearcher = mock(IndexSearcher.class);
        final IndexSearcher changeHistorySearcher = mock(IndexSearcher.class);

        doThrow(new IOException()).when(issueSearcher).close();
        doThrow(new AlreadyClosedException("blah")).when(commentSearcher).close();
        doThrow(new RuntimeException("blah")).when(changeHistorySearcher).close();

        when(issueIndexer.openEntitySearcher(any(Name.class))).thenReturn(issueSearcher);
        cache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, Name.ISSUE);

        when(issueIndexer.openEntitySearcher(any(Name.class))).thenReturn(commentSearcher);
        cache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, Name.COMMENT);

        when(issueIndexer.openEntitySearcher(any(Name.class))).thenReturn(changeHistorySearcher);
        cache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, Name.CHANGE_HISTORY);

        when(issueIndexer.openEntitySearcher(any(Name.class))).thenReturn(worklogSearcher);
        cache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, Name.WORKLOG);

        cache.closeSearchers();

        verify(issueSearcher).close();
        verify(commentSearcher).close();
        verify(changeHistorySearcher).close();
        verify(worklogSearcher).close();
        final String logMessage = logger.getMessage();
        assertThat(logMessage, containsString("ERROR - Error while resetting searcher: COMMENT message: blah"));
        assertThat(logMessage, containsString("ERROR - Error while resetting searcher: ISSUE message: null"));
        assertThat(logMessage, containsString("ERROR - Error while resetting searcher: CHANGE_HISTORY message: blah"));
    }

    @Test
    public void testResetNullSearchers() throws IOException
    {
        //nothing to assert really.  Just need to make sure this test passes without any exceptions.
        SearcherCache.getThreadLocalCache().closeSearchers();
    }
}
