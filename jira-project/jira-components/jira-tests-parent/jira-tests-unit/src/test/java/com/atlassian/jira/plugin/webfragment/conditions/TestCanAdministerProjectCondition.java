package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.mock.servlet.MockHttpServletResponse;
import com.atlassian.jira.plugin.webfragment.JiraWebInterfaceManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.ExecutingHttpRequest;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;

import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestCanAdministerProjectCondition
{
    @Rule
    public MockComponentContainer container = new MockComponentContainer(this);

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private ProjectManager projectManager;

    @Mock
    @AvailableInContainer
    private UserUtil mockUserUtil;

    private JiraAuthenticationContext jac;
    private MockApplicationUser user;
    private CanAdministerProjectCondition canAdministerProjectCondition;

    @Before
    public void setup()
    {
        user = new MockApplicationUser("bbain");
        when(mockUserUtil.getUserByName("bbain")).thenReturn(user);

        jac = new MockSimpleAuthenticationContext(user, Locale.ENGLISH, new NoopI18nHelper());
        ExecutingHttpRequest.set(new MockHttpServletRequest(), new MockHttpServletResponse());
        canAdministerProjectCondition = new CanAdministerProjectCondition(permissionManager, projectManager);
    }

    @After
    public void tearDown()
    {
        ExecutingHttpRequest.clear();
    }

    @Test
    public void testShouldDisplayNoProject()
    {
        assertFalse(canAdministerProjectCondition.shouldDisplay(Collections.<String, Object>emptyMap()));
    }

    @Test
    public void testShouldDisplayUserNoPermission()
    {
        checkNoPermissionForUser(user);
    }

    @Test
    public void testShouldDisplayAnonymousNoPermission()
    {
        checkNoPermissionForUser(null);
    }

    @Test
    public void testShouldDisplayUserAdminPermission()
    {
        checkAdminPermissionForUser(user);
    }

    @Test
    public void testShouldDisplayAnonymousAdminPermission()
    {
        checkAdminPermissionForUser(null);
    }

    @Test
    public void testShouldDisplaylUserProjectAdminPermission()
    {
        checkProjectAdminPermissionForUser(user);
    }

    @Test
    public void testShouldDisplayAnonymousProjectAdminPermission()
    {
        checkProjectAdminPermissionForUser(null);
    }

    @Test
    public void testShouldDisplayUserNoRequest()
    {
        checkProjectAdminNoRequest(user);
    }

    @Test
    public void testShouldDisplayAnonymousNoRequest()
    {
        checkProjectAdminNoRequest(null);
    }

    private void checkProjectAdminNoRequest(ApplicationUser user)
    {
        ExecutingHttpRequest.set(null, null);
        jac.setLoggedInUser(user);
        Project project = new MockProject(2881L, "ONE");

        when(permissionManager.hasPermission(ADMINISTER_PROJECTS, project, user)).thenReturn(true);

        assertTrue(canAdministerProjectCondition.shouldDisplay(createProjectContext(user, project)));
        assertTrue(canAdministerProjectCondition.shouldDisplay(createProjectContext(user, project)));

        verify(permissionManager, times(2)).hasPermission(Permissions.ADMINISTER, user);
        verify(permissionManager, times(2)).hasPermission(ADMINISTER_PROJECTS, project, user);
    }

    private void checkProjectAdminPermissionForUser(ApplicationUser user)
    {
        jac.setLoggedInUser(user);

        Project project = new MockProject(2881L, "ONE");

        when(permissionManager.hasPermission(ADMINISTER_PROJECTS, project, user)).thenReturn(true);

        assertTrue(canAdministerProjectCondition.shouldDisplay(createProjectContext(user, project)));
        assertTrue(canAdministerProjectCondition.shouldDisplay(createProjectContext(user, project)));

        verify(permissionManager, times(1)).hasPermission(Permissions.ADMINISTER, user);
        verify(permissionManager, times(1)).hasPermission(ADMINISTER_PROJECTS, project, user);
    }

    private void checkAdminPermissionForUser(ApplicationUser user)
    {
        jac.setLoggedInUser(user);

        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        Project project = new MockProject(2881L, "ONE");
        assertTrue(canAdministerProjectCondition.shouldDisplay(createProjectContext(user, project)));
        assertTrue(canAdministerProjectCondition.shouldDisplay(createProjectContext(user, project)));

        verify(permissionManager, times(1)).hasPermission(Permissions.ADMINISTER, user);
    }

    private void checkNoPermissionForUser(ApplicationUser user)
    {
        jac.setLoggedInUser(user);

        Project project = new MockProject(2881L, "ONE");
        assertFalse(canAdministerProjectCondition.shouldDisplay(createProjectContext(user, project)));
        assertFalse(canAdministerProjectCondition.shouldDisplay(createProjectContext(user, project)));

        verify(permissionManager, times(1)).hasPermission(Permissions.ADMINISTER, user);
    }

    private static Map<String, Object> createProjectContext(final ApplicationUser user, Project project)
    {
        return MapBuilder.build(
                JiraWebInterfaceManager.CONTEXT_KEY_USERNAME, user != null ? user.getUsername() : null,
                JiraWebInterfaceManager.CONTEXT_KEY_HELPER, new JiraHelper(ExecutingHttpRequest.get(), project));
    }
}
