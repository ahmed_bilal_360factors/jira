package com.atlassian.jira.upgrade;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * @since v4.4
 */
public class TestAbstractUpgradeTask
{
    @Test
    public void testTableNotPrefixedWithBlankSchema() throws Exception
    {
        String result = AbstractImmediateUpgradeTask.ensureTablePrefixed("jiraissues", null);
        assertEquals("Table should not be prefixed for null schema", "jiraissues", result);

        result = AbstractImmediateUpgradeTask.ensureTablePrefixed("jiraissues", "");
        assertEquals("Table should not be prefixed for blank schema", "jiraissues", result);
    }

    @Test
    public void testTablePrefixedWithUnmatchedSchema() throws Exception
    {
        // A table with no similarity to the schema name should be prefixed
        String result = AbstractImmediateUpgradeTask.ensureTablePrefixed("userassociation", "jira");
        assertEquals("Table should be prefixed with schema", "jira.userassociation", result);

        // A table starting with the schema - but not the '.' - should be prefixed
        result = AbstractImmediateUpgradeTask.ensureTablePrefixed("jiraissues", "jira");
        assertEquals("Table should be prefixed with schema", "jira.jiraissues", result);
    }

    @Test
    public void testTableNotPrefixedWithMatchedSchema() throws Exception
    {
        // A table already prefixed should not be modified.
        String result = AbstractImmediateUpgradeTask.ensureTablePrefixed("jira.jiraissues", "jira");
        assertEquals("Prefixed table should be returned unchanged", "jira.jiraissues", result);
    }
}
