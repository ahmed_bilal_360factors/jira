package com.atlassian.jira.security.plugin;

import com.atlassian.fugue.Option;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.plugin.permission.ProjectPermissionModuleDescriptor;
import com.atlassian.jira.plugin.permission.ProjectPermissionModuleDescriptorMockUtil;
import com.atlassian.jira.plugin.util.PluginModuleTrackerFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.conditions.ConditionLoadingException;
import com.google.common.collect.Lists;
import org.dom4j.DocumentException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;

import static com.atlassian.jira.matchers.OptionMatchers.none;
import static com.atlassian.jira.matchers.OptionMatchers.some;
import static com.atlassian.jira.permission.ProjectPermissionCategory.ISSUES;
import static com.atlassian.jira.permission.ProjectPermissionCategory.PROJECTS;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectPermissionTypesManagerImpl
{
    private static final String FALSE_COND_CLASS = "extractors.are.awesome.FalseCondition";
    private static final String TRUE_COND_CLASS = "extractors.are.awesome.TrueCondition";

    @InjectMocks private PluginModuleTrackerFactory pluginModuleTrackerFactory;

    @Mock private Condition falseCondition;
    @Mock private PluginAccessor pluginAccessor;
    @Mock private PluginEventManager pluginEventManager;
    @Mock private Condition trueCondition;

    @Mock @AvailableInContainer private JiraAuthenticationContext jiraAuthenticationContext;

    @Rule public MockComponentContainer container = new MockComponentContainer(this);

    private ProjectPermissionModuleDescriptor descriptorWithFalseCondition;
    private ProjectPermissionModuleDescriptor descriptorWithTrueCondition;
    private ProjectPermissionTypesManager projectPermissionTypesManager;
    private ProjectPermissionModuleDescriptorMockUtil util;

    @Before
    public void setup() throws ConditionLoadingException, DocumentException
    {
        util = new ProjectPermissionModuleDescriptorMockUtil();
        when(falseCondition.shouldDisplay(anyMap())).thenReturn(false);
        when(trueCondition.shouldDisplay(anyMap())).thenReturn(true);
        util.mockWebFragmentHelperForCondition(FALSE_COND_CLASS, falseCondition);
        util.mockWebFragmentHelperForCondition(TRUE_COND_CLASS, trueCondition);

        descriptorWithFalseCondition = util.createDescriptor(
                "PROJECTS", FALSE_COND_CLASS, "false.name.key", "false.description.key", "false.key");
        descriptorWithTrueCondition = util.createDescriptor(
                "ISSUES", TRUE_COND_CLASS, "true.name.key", "true.description.key", "true.key");

        when(pluginAccessor.getEnabledModuleDescriptorsByClass(ProjectPermissionModuleDescriptor.class)).thenReturn(
                Lists.newArrayList(descriptorWithFalseCondition, descriptorWithTrueCondition));
        projectPermissionTypesManager = new ProjectPermissionTypesManagerImpl(pluginModuleTrackerFactory);
    }

    @Test
    public void allChecksCondition()
    {
        Collection<ProjectPermission> all = projectPermissionTypesManager.all();
        assertThat(all, hasSize(1));
        assertThat(all.iterator().next(), equalTo(descriptorWithTrueCondition.getModule()));
    }

    @Test
    public void withCategoryChecksCondition()
    {
        Collection<ProjectPermission> withProjectsCategory = projectPermissionTypesManager.withCategory(PROJECTS);
        assertThat(withProjectsCategory, hasSize(0));

        Collection<ProjectPermission> withIssuesCategory = projectPermissionTypesManager.withCategory(ISSUES);
        assertThat(withIssuesCategory, hasSize(1));
        assertThat(withIssuesCategory.iterator().next(), equalTo(descriptorWithTrueCondition.getModule()));
    }

    @Test
    public void withKeyChecksCondition()
    {
        Option<ProjectPermission> withFalseKey = projectPermissionTypesManager.withKey(new ProjectPermissionKey("false.key"));
        assertThat(withFalseKey, none());

        Option<ProjectPermission> withTrueKey = projectPermissionTypesManager.withKey(new ProjectPermissionKey("true.key"));
        assertThat(withTrueKey, some(descriptorWithTrueCondition.getModule()));
    }

    @Test
    public void existsChecksCondition()
    {
        boolean withFalseKey = projectPermissionTypesManager.exists(new ProjectPermissionKey("false.key"));
        assertThat("Calling exists() with a false key should return false", withFalseKey, is(false));

        boolean withTrueKey = projectPermissionTypesManager.exists(new ProjectPermissionKey("true.key"));
        assertThat("Calling exists() with a true key should return true", withTrueKey, is(true));
    }
}
