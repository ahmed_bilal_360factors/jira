package com.atlassian.jira.issue.customfields.impl;

import com.atlassian.jira.imports.project.customfield.NoTransformationCustomFieldImporter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @since v3.13
 */
@RunWith (MockitoJUnitRunner.class)
public class TestURLCFType extends TestGenericTextCFType
{
    @Test
    public void testGetProjectImporter() throws Exception
    {
        URLCFType urlcfType = new URLCFType(null, null, null, null);
        assertTrue(urlcfType.getProjectImporter() instanceof NoTransformationCustomFieldImporter);
    }

    // JRA-14998 - URLs should be trimmed before validating
    @Test
    public void testUrlIsTrimmed() throws Exception
    {
        URLCFType urlcfType = new URLCFType(null, null, null, null);
        assertEquals("http://www.atlassian.com", urlcfType.getSingularObjectFromString("  http://www.atlassian.com  "));
        assertEquals("http://www.atlassian.com", urlcfType.getSingularObjectFromString("http://www.atlassian.com"));
    }

    @Override
    protected GenericTextCFType getFieldTypeUnderTest()
    {
        return new URLCFType(null, null, textFieldCharacterLengthValidator, authenticationContext);
    }

    protected String getLongText()
    {
        return "http://thisurlistoolongtofitwithinthelimit.com";
    }

    protected String getShortText()
    {
        return "http://shortenough.com";
    }
}
