package com.atlassian.jira.bc.license;

import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestJiraLicenseServiceImpl
{
    private static final String EXPECTED_MSG = "expectedMsg";
    private static final String BAD_LIC_STRING = "somebadstring";
    private static final String GOOD_LIC_STRING = "adam goodes";
    private static final String USER_NAME = "userName";

    @Mock
    JiraLicenseManager licenseManager;
    @Mock
    UserUtil userUtil;
    @Mock
    I18nHelper i18n;
    @Mock
    LicenseDetails licenseDetails;

    JiraLicenseServiceImpl licenseService;

    @Before
    public void setup()
    {
        when(licenseManager.getLicense(anyString())).thenReturn(licenseDetails);
        when(licenseDetails.getLicenseVersion()).thenReturn(2);

        licenseService = new JiraLicenseServiceImpl(licenseManager, userUtil);
    }

    @Test
    public void testGetLicense()
    {
        when(licenseManager.getLicense()).thenReturn(licenseDetails);

        LicenseDetails actualLicenseDetails = licenseService.getLicense();
        assertSame(licenseDetails, actualLicenseDetails);
    }

    @Test
    public void testSetLicense_NullValidationResult()
    {
        try
        {
            licenseService.setLicense(null);
            fail("Should have barfed");
        }
        catch (IllegalStateException expected)
        {
        }
    }

    @Test
    public void testSetLicense_ValidationResultHasNoErrorCollection()
    {
        JiraLicenseService.ValidationResult validationResult = mock(JiraLicenseService.ValidationResult.class);
        when(validationResult.getErrorCollection()).thenReturn(null);
        try
        {
            licenseService.setLicense(validationResult);
            fail("Should have barfed");
        }
        catch (IllegalStateException expected)
        {
        }
        verify(validationResult).getErrorCollection();
    }

    @Test
    public void testSetLicense_ValidationResultHasErrorCollectionWithErrors()
    {
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addError("shite", "happens");

        JiraLicenseService.ValidationResult validationResult = mock(JiraLicenseService.ValidationResult.class);
        when(validationResult.getErrorCollection()).thenReturn(errorCollection);
        try
        {
            licenseService.setLicense(validationResult);
            fail("Should have barfed");
        }
        catch (IllegalStateException expected)
        {
        }

        verify(validationResult, times(2)).getErrorCollection();
    }


    @Test
    public void testSetLicense_HappyPath()
    {
        when(licenseManager.setLicense(GOOD_LIC_STRING)).thenReturn(licenseDetails);

        JiraLicenseService.ValidationResult validationResult = mock(JiraLicenseService.ValidationResult.class);
        when(validationResult.getErrorCollection()).thenReturn(new SimpleErrorCollection());
        when(validationResult.getLicenseString()).thenReturn(GOOD_LIC_STRING);

        LicenseDetails actualLicenseDetails = licenseService.setLicense(validationResult);
        assertSame(licenseDetails, actualLicenseDetails);
        verify(licenseManager).setLicense(GOOD_LIC_STRING);
        verify(validationResult).getLicenseString();

    }

    @Test
    public void validateLicensesShouldAlwaysCheckAllLicenses()
    {
        List<String> licenses = ImmutableList.of("valid", "invalid", "valid again");

        when(licenseManager.isDecodeable("valid")).thenReturn(true);
        when(licenseManager.isDecodeable("invalid")).thenReturn(false);
        when(licenseManager.isDecodeable("valid again")).thenReturn(true);

        when(i18n.getText("setup.error.invalidlicensekey")).thenReturn("whatever - some error message");


        Iterable<JiraLicenseService.ValidationResult> actual = licenseService.validate(i18n, licenses);


        assertEquals(licenses.size(), Iterables.size(actual));

        Iterator<JiraLicenseService.ValidationResult> iterator = actual.iterator();
        assertFalse("valid license", iterator.next().getErrorCollection().hasAnyErrors());
        assertTrue("invalid license", iterator.next().getErrorCollection().hasAnyErrors());
        assertFalse("second valid license", iterator.next().getErrorCollection().hasAnyErrors());
    }

    @Test
    public void emptyValidateLicensesShouldReturnEmptyResults()
    {
        Iterable<JiraLicenseService.ValidationResult> actual = licenseService.validate(i18n, ImmutableList.<String>of());
        assertEquals("results should be empty", 0, Iterables.size(actual));
    }


    @Test
    public void testValidate_InvalidString()
    {
        when(licenseManager.isDecodeable(BAD_LIC_STRING)).thenReturn(false);
        when(i18n.getText("setup.error.invalidlicensekey")).thenReturn(EXPECTED_MSG);

        final JiraLicenseService.ValidationResult validationResult = licenseService.validate(i18n, BAD_LIC_STRING);
        assertValidationResult(validationResult, EXPECTED_MSG);
    }

    @Test
    public void testValidate_InvalidLicenseVersion()
    {
        final int invalidLicenseVersion = 1;

        when(licenseManager.isDecodeable(BAD_LIC_STRING)).thenReturn(true);
        when(licenseManager.getLicense(BAD_LIC_STRING)).thenReturn(licenseDetails);
        when(licenseDetails.getLicenseVersion()).thenReturn(invalidLicenseVersion);
        when(i18n.getText(eq("setup.error.invalidlicensekey.v1.license.version"), anyString(), anyString())).thenReturn(EXPECTED_MSG);
        when(userUtil.getTotalUserCount()).thenReturn(10);
        when(userUtil.getActiveUserCount()).thenReturn(5);

        final JiraLicenseService.ValidationResult validationResult = licenseService.validate(i18n, BAD_LIC_STRING);
        assertValidationResult(validationResult, EXPECTED_MSG);
    }

    @Test
    public void testConfirmProceed()
    {
        licenseService.confirmProceedUnderEvaluationTerms(USER_NAME);
        verify(licenseManager).confirmProceedUnderEvaluationTerms(USER_NAME);
    }

    @Test
    public void testGetServerId() throws Exception
    {
        final String serverId = "A server ID";
        when(licenseManager.getServerId()).thenReturn(serverId);
        assertEquals(serverId, licenseService.getServerId());
    }

    private void assertValidationResult(final JiraLicenseService.ValidationResult validationResult, final String expectedMsg)
    {
        assertNotNull(validationResult);
        assertTrue(validationResult.getErrorCollection().hasAnyErrors());
        assertEquals(expectedMsg, validationResult.getErrorCollection().getErrors().get("license"));
    }
}
