package com.atlassian.jira.bc.license;

import com.atlassian.fugue.Option;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.LicenseRole;
import com.atlassian.jira.license.LicenseRoleDetails;
import com.atlassian.jira.license.LicenseRoleId;
import com.atlassian.jira.license.LicenseRoleManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.MockApplicationUser;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestLicenseRoleAuthorizationServiceImpl
{
    private static final MockApplicationUser TEST_USER = new MockApplicationUser("User");
    private static final LicenseRoleId TEST_LICENSE_ROLE_ID = new LicenseRoleId("Role");
    private static final String GROUP_NAME_1 = "Group 1";

    @Mock private LicenseRoleManager licenseRoleManager;
    @Mock private GroupManager groupManager;
    @Mock private LicenseDetails licenseDetails;
    @Mock private LicenseRoleDetails licenseRoleDetails;
    private LicenseRoleAuthorizationServiceImpl licenseRoleAuthorizationService;

    @Before
    public void setUp()
    {
        licenseRoleAuthorizationService = new LicenseRoleAuthorizationServiceImpl(licenseRoleManager,
                groupManager);
        when(groupManager.groupExists(GROUP_NAME_1)).thenReturn(true);
    }

    @Test
    public void checkIsLicenseRoleInstalled()
    {
        when(licenseRoleManager.isLicenseRoleInstalled(TEST_LICENSE_ROLE_ID)).thenReturn(true);

        assertThat(licenseRoleAuthorizationService.isLicenseRoleInstalled(TEST_LICENSE_ROLE_ID), Matchers.equalTo(true));
        assertThat(licenseRoleAuthorizationService.isLicenseRoleInstalled(LicenseRoleId.valueOf("random")), Matchers.equalTo(false));
    }

    @Test
    public void anonymousUserShouldNeverBeInARole()
    {
        boolean authenticated = licenseRoleAuthorizationService.canUseRole(null, TEST_LICENSE_ROLE_ID);
        assertThat(authenticated, Matchers.equalTo(false));
    }

    @Test
    public void userCannotUseRoleWhenLicenseRoleInvalid()
    {
        when(licenseRoleManager.getLicenseRole(TEST_LICENSE_ROLE_ID))
                .thenReturn(Option.none(LicenseRole.class));

        boolean authenticated = licenseRoleAuthorizationService.canUseRole(TEST_USER, TEST_LICENSE_ROLE_ID);
        assertThat(authenticated, Matchers.equalTo(false));
    }

    @Test
    public void userCannotUseRoleWhenNotInGroup()
    {
        when(licenseRoleManager.getLicenseRole(TEST_LICENSE_ROLE_ID))
                .thenReturn(Option.<LicenseRole>some(new MockLicenseRole().id("role").groups("one", "two")));

        boolean authenticated = licenseRoleAuthorizationService.canUseRole(TEST_USER, TEST_LICENSE_ROLE_ID);
        assertThat(authenticated, Matchers.equalTo(false));
    }

    @Test
    public void userCanUseRoleWhenInGroup()
    {
        when(licenseRoleManager.getLicenseRole(TEST_LICENSE_ROLE_ID))
                .thenReturn(Option.<LicenseRole>some(new MockLicenseRole().id("role").groups("one", GROUP_NAME_1)));

        when(groupManager.isUserInGroup(TEST_USER.getUsername(), GROUP_NAME_1)).thenReturn(true);

        boolean authenticated = licenseRoleAuthorizationService.canUseRole(TEST_USER, TEST_LICENSE_ROLE_ID);
        assertThat(authenticated, Matchers.equalTo(true));
    }

    @Test
    public void isLicensedShouldDelegateDirectlyToJiraLicenseManager()
    {
        licenseRoleAuthorizationService.isLicenseRoleInstalled(TEST_LICENSE_ROLE_ID);

        verify(licenseRoleManager).isLicenseRoleInstalled(TEST_LICENSE_ROLE_ID);
        verifyNoMoreInteractions(groupManager, licenseRoleManager, licenseDetails, licenseRoleDetails);
    }

    @Test
    public void countActiveUsersShallDelegateToLicenseRoleManager()
    {
        when(licenseRoleManager.getUserCount(TEST_LICENSE_ROLE_ID)).thenReturn(20);

        assertThat("There shall be 20 users for the test license role",
                licenseRoleAuthorizationService.getUserCount(TEST_LICENSE_ROLE_ID), Matchers.equalTo(20));
    }
}
