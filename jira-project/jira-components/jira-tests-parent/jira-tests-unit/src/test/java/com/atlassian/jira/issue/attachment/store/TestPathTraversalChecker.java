package com.atlassian.jira.issue.attachment.store;

import java.io.File;

import com.atlassian.fugue.Option;
import com.atlassian.jira.util.PathTraversalRuntimeException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

public class TestPathTraversalChecker
{
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    private final PathTraversalChecker pathTraversalChecker = new PathTraversalChecker();

    @Test
    public void testValidFileUnderSecureDirectory() throws Exception
    {
        final File secureFolder = temporaryFolder.newFolder();
        final File fileUnderSecureFolder = new File(secureFolder, "justAFile");

        final Option<Exception> exception = pathTraversalChecker.validateFileInSecureDirectory(fileUnderSecureFolder, secureFolder);
        assertThat(exception.isEmpty(), equalTo(true));
    }

    @Test
    public void testPathTraversalAttempFile() throws Exception
    {
        final File secureFolder = temporaryFolder.newFolder();
        final File fileUnderSecureFolder = new File(secureFolder, "../pathTraversalAttackFile");

        final Option<Exception> exception = pathTraversalChecker.validateFileInSecureDirectory(fileUnderSecureFolder, secureFolder);
        assertThat(exception.isDefined(), equalTo(true));
        assertThat(exception.get(), instanceOf(PathTraversalRuntimeException.class));
    }
}