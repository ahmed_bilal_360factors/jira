package com.atlassian.jira.issue.customfields.impl;

import com.atlassian.jira.imports.project.customfield.NoTransformationCustomFieldImporter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;

/**
 * @since v3.13
 */
@RunWith (MockitoJUnitRunner.class)
public class TestRenderableTextCFType extends TestGenericTextCFType
{
    @Test
    public void testGetProjectImporter() throws Exception
    {
        RenderableTextCFType renderableTextCFType = new RenderableTextCFType(null, null, null, null);
        assertTrue(renderableTextCFType.getProjectImporter() instanceof NoTransformationCustomFieldImporter);
    }

    @Override
    protected GenericTextCFType getFieldTypeUnderTest()
    {
        return new RenderableTextCFType(null, null, textFieldCharacterLengthValidator, authenticationContext);
    }

}
