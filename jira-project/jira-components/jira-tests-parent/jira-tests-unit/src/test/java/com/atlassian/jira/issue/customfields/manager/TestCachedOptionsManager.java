package com.atlassian.jira.issue.customfields.manager;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.action.issue.customfields.option.MockOption;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.config.FieldConfigImpl;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigManager;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.CollectionReorderer;
import com.atlassian.jira.util.collect.CollectionBuilder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import static com.atlassian.jira.util.collect.MapBuilder.newBuilder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
@RunWith(MockitoJUnitRunner.class)
public class TestCachedOptionsManager
{
    @Mock private OfBizDelegator ofBizDelegator;
    @Mock private FieldConfigManager fieldConfigManager;

    private CacheManager cacheManager;

    @Before
    public void setUp() throws Exception
    {
        cacheManager = new MemoryCacheManager();
    }

    @Test
    public void testGetAllOptions() throws Exception
    {
        // Set up
        final Option option = new MockOption(null, null, null, null, null, 10L);
        when(ofBizDelegator.findAll("CustomFieldOption", DefaultOptionsManager.ORDER_BY_LIST))
                .thenReturn(Collections.<GenericValue>emptyList());

        final AtomicBoolean called = new AtomicBoolean(false);
        final OptionsManager optionsManager = new CachedOptionsManager(ofBizDelegator, fieldConfigManager, cacheManager)
        {
            @Override
            List<Option> convertGVsToOptions(final List<GenericValue> optionGvs)
            {
                called.set(true);
                return CollectionBuilder.newBuilder(option).asList();
            }
        };

        // Invoke and check (twice)
        assertOptions(option, called, optionsManager);
        assertOptions(option, called, optionsManager);
    }

    private void assertOptions(final Option option, final AtomicBoolean called, final OptionsManager optionsManager)
    {
        // Invoke
        final List<Option> result = optionsManager.getAllOptions();

        // Check
        assertTrue(called.get());
        assertTrue(result.contains(option));
        assertEquals(1, result.size());
    }

    @Test
    public void testFindByOptionValue()
    {
        final Option option1 = new MockOption(null, null, null, "value", null, 10L);
        final AtomicInteger called = new AtomicInteger(0);
        final OptionsManager manager = new CachedOptionsManager(ofBizDelegator, fieldConfigManager, cacheManager)
        {
            @Override
            public List<Option> getAllOptions()
            {
                called.getAndIncrement();
                return CollectionBuilder.newBuilder(option1, new MockOption(null, null, null, "DiffValue", null, 10L)).asList();
            }
        };

        List<Option> result = manager.findByOptionValue("VALUE");
        assertEquals(1, result.size());
        assertTrue(result.contains(option1));

        result = manager.findByOptionValue("Value");
        assertEquals(1, result.size());
        assertTrue(result.contains(option1));

        assertEquals(1, called.get());
    }

    @Test
    public void testRemoveCustomFieldConfigOptions()
    {
        // Set up
        when(ofBizDelegator.removeByAnd("CustomFieldOption", newBuilder("customfieldconfig", 1L).toMap())).thenReturn(1);
        final OptionsManager manager = new CachedOptionsManager(ofBizDelegator, fieldConfigManager, cacheManager);
        final FieldConfigImpl fieldConfig = new FieldConfigImpl(1L, "", "", null, "");

        // Invoke
        manager.removeCustomFieldConfigOptions(fieldConfig);
    }
}
