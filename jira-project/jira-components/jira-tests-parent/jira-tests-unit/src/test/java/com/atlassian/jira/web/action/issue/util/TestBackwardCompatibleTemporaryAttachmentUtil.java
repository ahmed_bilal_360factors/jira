package com.atlassian.jira.web.action.issue.util;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.atlassian.jira.issue.AttachmentError;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.AttachmentsBulkOperationResult;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.TemporaryAttachmentsMonitorLocator;
import com.atlassian.jira.issue.attachment.TemporaryAttachment;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachment;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachmentManager;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.junit.rules.Log4jLogger;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.web.action.issue.TemporaryAttachmentsMonitor;
import com.atlassian.jira.web.bean.I18nBean;

import com.google.common.collect.ImmutableList;
import com.googlecode.catchexception.CatchException;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static com.atlassian.fugue.Option.some;
import static com.googlecode.catchexception.CatchException.caughtException;
import static com.googlecode.catchexception.apis.CatchExceptionHamcrestMatchers.hasMessage;
import static com.googlecode.catchexception.apis.CatchExceptionHamcrestMatchers.hasNoCause;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings ({ "ThrowableResultOfMethodCallIgnored", "unchecked", "deprecation" })
public class TestBackwardCompatibleTemporaryAttachmentUtil
{
    public static final String FORM_TOKEN = "token";

    @Rule
    public final Log4jLogger expectedLog = new Log4jLogger();

    @Rule
    public final TestRule initMockitoMocks = new InitMockitoMocks(this);

    @SuppressWarnings ("deprecation")
    @Mock
    private TemporaryAttachmentsMonitorLocator temporaryAttachmentsMonitorLocator;

    @Mock
    private TemporaryWebAttachmentManager temporaryWebAttachmentManager;

    @Mock
    private AttachmentManager attachmentManager;

    @Mock
    private I18nBean.BeanFactory i18nFactory;

    @Mock
    private Issue issue;

    private final ApplicationUser user = new MockApplicationUser("tester");

    @InjectMocks
    BackwardCompatibleTemporaryAttachmentUtil sut;

    @Before
    public void setUp() throws Exception
    {
        when(i18nFactory.getInstance(user)).thenReturn(new MockI18nHelper());
    }

    @Test
    public void shouldFindAttachmentsInNewMonitor() throws Exception
    {
        final TemporaryWebAttachment temporaryWebAttachmentMock = mock(TemporaryWebAttachment.class);
        final TemporaryWebAttachment temporaryWebAttachmentMock2 = mock(TemporaryWebAttachment.class);

        when(temporaryWebAttachmentManager.getTemporaryWebAttachment("1")).thenReturn(some(temporaryWebAttachmentMock));
        when(temporaryWebAttachmentManager.getTemporaryWebAttachment("2")).thenReturn(some(temporaryWebAttachmentMock2));

        assertTrue("Expected all attachments to exist in the new monitor",
                sut.allAttachmentsExists(ImmutableList.of("1", "2")));
    }

    @Test
    public void shouldFallbackToOldMonitorIfNewMonitorDoesntHaveThoseAttachments() throws Exception
    {
        final TemporaryAttachment mockAttachment = createMockTemporaryAttachmentWithExistingFile();
        final TemporaryAttachment mockAttachment2 = createMockTemporaryAttachmentWithExistingFile();

        final TemporaryAttachmentsMonitor monitor = mock(TemporaryAttachmentsMonitor.class);
        when(temporaryAttachmentsMonitorLocator.get(false)).thenReturn(monitor);

        when(monitor.getById(1L)).thenReturn(mockAttachment);
        when(monitor.getById(2L)).thenReturn(mockAttachment2);

        assertTrue("Expected all attachments to exist in the old monitor",
                sut.allAttachmentsExists(ImmutableList.of("1", "2")));
    }

    @Test
    public void shouldFallbackToOldMonitorIfNotAllAttachmentsWereFoundInNewMonitor() throws Exception
    {
        final TemporaryWebAttachment temporaryWebAttachmentMock = mock(TemporaryWebAttachment.class);
        final TemporaryAttachment mockAttachment = createMockTemporaryAttachmentWithExistingFile();
        final TemporaryAttachment mockAttachment2 = createMockTemporaryAttachmentWithExistingFile();

        final TemporaryAttachmentsMonitor monitor = mock(TemporaryAttachmentsMonitor.class);
        when(temporaryAttachmentsMonitorLocator.get(false)).thenReturn(monitor);
        when(monitor.getById(1L)).thenReturn(mockAttachment);
        when(monitor.getById(2L)).thenReturn(mockAttachment2);
        when(temporaryWebAttachmentManager.getTemporaryWebAttachment("1")).thenReturn(some(temporaryWebAttachmentMock));

        assertTrue("Expected attachments to exist in the old monitor if not all of them were found in the new monitor.",
                sut.allAttachmentsExists(ImmutableList.of("1", "2")));
    }

    @Test
    public void shouldReturnFalseIfAttachmentsAreInMixedMonitors() throws Exception
    {
        final TemporaryWebAttachment temporaryWebAttachmentMock = mock(TemporaryWebAttachment.class);
        final TemporaryAttachment temporaryAttachment = createMockTemporaryAttachmentWithExistingFile();

        final TemporaryAttachmentsMonitor monitor = mock(TemporaryAttachmentsMonitor.class);
        when(temporaryAttachmentsMonitorLocator.get(false)).thenReturn(monitor);

        when(temporaryWebAttachmentManager.getTemporaryWebAttachment("1")).thenReturn(some(temporaryWebAttachmentMock));
        when(monitor.getById(2L)).thenReturn(temporaryAttachment);

        assertFalse("Expected to get false as not all attachments were in single monitor (new/old).",
                sut.allAttachmentsExists(ImmutableList.of("1", "2")));
    }

    private TemporaryAttachment createMockTemporaryAttachmentWithExistingFile()
    {
        final TemporaryAttachment attachment = mock(TemporaryAttachment.class);
        final File attachment1fileMock = mock(File.class);
        when(attachment1fileMock.exists()).thenReturn(true);
        when(attachment.getFile()).thenReturn(attachment1fileMock);
        return attachment;
    }

    @Test
    public void shouldReturnFalseWhenFallbackToOldMonitorAndOldMonitorIsNotAvailable() throws Exception
    {
        final ImmutableList<String> attachmentIds = ImmutableList.of("1", "2");

        when(temporaryAttachmentsMonitorLocator.get(false)).thenReturn(null);
        assertFalse("Expected to get false as monitor was not located!",
                sut.allAttachmentsExists(attachmentIds));
    }

    @Test
    public void shouldUseNewManagerToConvertAttachmentsWhenAttachmentIdsAreNotLong() throws Exception
    {
        final List<String> ids = ImmutableList.of("attachment-1", "attachment-2");
        final AttachmentsBulkOperationResult<ChangeItemBean> expectedResult = mock(AttachmentsBulkOperationResult.class);
        when(temporaryWebAttachmentManager.convertTemporaryAttachments(user, issue, ids)).thenReturn(expectedResult);

        final AttachmentsBulkOperationResult<ChangeItemBean> result = sut.convertTemporaryAttachments(user, issue, ids);

        assertSame(expectedResult, result);
    }

    @Test
    public void shouldUseNewManagerToConvertAttachmentsWhenThereIsAtLeaseOneNonLongId() throws Exception
    {
        final List<String> ids = ImmutableList.of("attachment-1", "2");
        final AttachmentsBulkOperationResult<ChangeItemBean> expectedResult = mock(AttachmentsBulkOperationResult.class);
        when(temporaryWebAttachmentManager.convertTemporaryAttachments(user, issue, ids)).thenReturn(expectedResult);

        final AttachmentsBulkOperationResult<ChangeItemBean> result = sut.convertTemporaryAttachments(user, issue, ids);

        assertSame(expectedResult, result);
    }

    @Test
    public void shouldUseOldConversionMethodWhenAllIdsAreLongParsable() throws Exception
    {
        final List<String> ids = ImmutableList.of("1");
        final List<Long> longIds = ImmutableList.of(1L);
        final AttachmentsBulkOperationResult<ChangeItemBean> expectedResult = mock(AttachmentsBulkOperationResult.class);

        final TemporaryAttachmentsMonitor monitor = mock(TemporaryAttachmentsMonitor.class);
        when(temporaryAttachmentsMonitorLocator.get(false)).thenReturn(monitor);

        when(attachmentManager.tryConvertTemporaryAttachments(user, issue, longIds, monitor)).thenReturn(expectedResult);

        final AttachmentsBulkOperationResult<ChangeItemBean> result = sut.convertTemporaryAttachments(user, issue, ids);

        assertSame(expectedResult, result);
    }

    @Test
    public void shouldReturnErrorWhenUsingOldMethodAndMonitorIsNotAvailable() throws Exception
    {
        final List<String> ids = ImmutableList.of("1");

        when(temporaryAttachmentsMonitorLocator.get(false)).thenReturn(null);

        final AttachmentsBulkOperationResult<ChangeItemBean> result = sut.convertTemporaryAttachments(user, issue, ids);

        final AttachmentsBulkOperationResult<ChangeItemBean> expectedResult = new AttachmentsBulkOperationResult<ChangeItemBean>(
                ImmutableList.of(new AttachmentError(
                        "Couldn't get temporary attachments monitor - session probably expired!",
                        "attachment.temporary.id.session.time.out",
                        "",
                        ErrorCollection.Reason.SERVER_ERROR
                )), Collections.<ChangeItemBean>emptyList()
        );
        assertThat(result, Matchers.equalTo(expectedResult));
    }

    @Test
    public void shouldLogWarningWhenUsingOldConversionMethod() throws Exception
    {
        final List<String> ids = ImmutableList.of("1");

        final TemporaryAttachmentsMonitor monitor = mock(TemporaryAttachmentsMonitor.class);
        when(temporaryAttachmentsMonitorLocator.get(false)).thenReturn(monitor);

        sut.convertTemporaryAttachments(user, issue, ids);
        final String loggedMessage = expectedLog.getMessage().trim();
        final String expectedLogMessage = "WARN - Using deprecated method for adding attachments to issue!"
                + " Someone still uses TemporaryAttachmentsMonitor - you can probably track him by looking"
                + " at the referrer or previous requests to JIRA.";

        assertThat(loggedMessage, Matchers.equalTo(expectedLogMessage));
    }

    @Test
    public void shouldReturnTemporaryAttachmentsFromManagerWhenAvailable() throws Exception
    {
        final TemporaryWebAttachment temporaryWebAttachment = mock(TemporaryWebAttachment.class);
        when(temporaryWebAttachmentManager.getTemporaryWebAttachmentsByFormToken(FORM_TOKEN))
                .thenReturn(ImmutableList.of(temporaryWebAttachment));

        final Collection<TemporaryWebAttachment> result =
                (Collection<TemporaryWebAttachment>) sut.getTemporaryWebAttachmentsByFormToken(FORM_TOKEN);

        assertThat(result, Matchers.contains(temporaryWebAttachment));
    }

    @Test
    public void shouldReturnTemporaryAttachmentsFromOldMonitorWhenNotAvailableInManager() throws Exception
    {
        final TemporaryAttachment temporaryAttachment = mock(TemporaryAttachment.class);

        when(temporaryWebAttachmentManager.getTemporaryWebAttachmentsByFormToken(FORM_TOKEN))
                .thenReturn(Collections.<TemporaryWebAttachment>emptyList());

        final TemporaryAttachmentsMonitor monitor = mock(TemporaryAttachmentsMonitor.class);
        when(temporaryAttachmentsMonitorLocator.get(false)).thenReturn(monitor);
        when(monitor.getByFormToken(FORM_TOKEN)).thenReturn(ImmutableList.of(temporaryAttachment));

        final Collection<TemporaryAttachment> result =
                (Collection<TemporaryAttachment>) sut.getTemporaryWebAttachmentsByFormToken(FORM_TOKEN);

        assertThat(result, Matchers.contains(temporaryAttachment));
    }

    @Test
    public void shouldReturnEmptyListWhenWhenNotAvailableInManagerAndOldMonitor() throws Exception
    {
        when(temporaryWebAttachmentManager.getTemporaryWebAttachmentsByFormToken(FORM_TOKEN))
                .thenReturn(Collections.<TemporaryWebAttachment>emptyList());

        final TemporaryAttachmentsMonitor monitor = mock(TemporaryAttachmentsMonitor.class);
        when(temporaryAttachmentsMonitorLocator.get(false)).thenReturn(monitor);
        when(monitor.getByFormToken(FORM_TOKEN)).thenReturn(Collections.<TemporaryAttachment>emptyList());

        final Collection<Object> result = (Collection<Object>) sut.getTemporaryWebAttachmentsByFormToken(FORM_TOKEN);

        assertThat(result, Matchers.empty());
    }

    @Test
    public void shouldReturnEmptyListWhenWhenNotAvailableInManagerAndOldMonitorIsNotAvailable() throws Exception
    {
        when(temporaryWebAttachmentManager.getTemporaryWebAttachmentsByFormToken(FORM_TOKEN))
                .thenReturn(Collections.<TemporaryWebAttachment>emptyList());

        when(temporaryAttachmentsMonitorLocator.get(false)).thenReturn(null);

        final Collection<Object> result = (Collection<Object>) sut.getTemporaryWebAttachmentsByFormToken(FORM_TOKEN);

        assertThat(result, Matchers.empty());
    }

    @Test
    public void shouldReturnTemporaryAttachmentsFromManagerEvenIfItIsAlsoAvailableInOldMonitor() throws Exception
    {
        final TemporaryWebAttachment temporaryWebAttachment = mock(TemporaryWebAttachment.class);
        final TemporaryAttachment temporaryAttachment = mock(TemporaryAttachment.class);

        when(temporaryWebAttachmentManager.getTemporaryWebAttachmentsByFormToken(FORM_TOKEN))
                .thenReturn(ImmutableList.of(temporaryWebAttachment));

        final TemporaryAttachmentsMonitor monitor = mock(TemporaryAttachmentsMonitor.class);
        when(temporaryAttachmentsMonitorLocator.get(false)).thenReturn(monitor);
        when(monitor.getByFormToken(FORM_TOKEN)).thenReturn(ImmutableList.of(temporaryAttachment));

        final Collection<TemporaryWebAttachment> result =
                (Collection<TemporaryWebAttachment>) sut.getTemporaryWebAttachmentsByFormToken(FORM_TOKEN);

        assertThat(result, Matchers.contains(temporaryWebAttachment));
    }

    @Test
    public void shouldClearAttachmentsOnlyInManagerWhenOldMonitorIsNotAvailable() throws Exception
    {
        sut.clearTemporaryAttachmentsByFormToken(FORM_TOKEN);
        when(temporaryAttachmentsMonitorLocator.get(false)).thenReturn(null);
        verify(temporaryWebAttachmentManager).clearTemporaryAttachmentsByFormToken(FORM_TOKEN);
    }

    @Test
    public void shouldClearAttachmentsInManagerAndOldMonitorIfAvailable() throws Exception
    {
        final TemporaryAttachmentsMonitor monitor = mock(TemporaryAttachmentsMonitor.class);
        when(temporaryAttachmentsMonitorLocator.get(false)).thenReturn(monitor);

        sut.clearTemporaryAttachmentsByFormToken(FORM_TOKEN);

        verify(monitor).clearEntriesForFormToken(FORM_TOKEN);
    }

    @Test
    public void shouldFailWhenAttemptToClearAttachmentsWithNullToken() throws Exception
    {
        CatchException.catchException(sut).clearTemporaryAttachmentsByFormToken(null);

        assertThat((IllegalArgumentException) caughtException(), allOf(
                is(IllegalArgumentException.class),
                hasMessage("formToken should not be null!"),
                hasNoCause()
        ));
    }
}