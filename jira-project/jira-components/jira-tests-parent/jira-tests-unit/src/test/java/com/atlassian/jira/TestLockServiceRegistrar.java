package com.atlassian.jira;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.beehive.db.spi.ClusterLockDao;
import com.atlassian.beehive.simple.SimpleClusterLockService;
import com.atlassian.jira.cluster.ClusterNodeProperties;
import com.atlassian.jira.cluster.lock.JiraClusterLockDao;
import com.atlassian.jira.cluster.lock.JiraClusterNodeHeartBeatDao;
import com.atlassian.jira.cluster.lock.NullJiraClusterNodeHeartBeatDao;
import com.atlassian.jira.cluster.lock.StartableDatabaseClusterLockService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.picocontainer.ComponentAdapter;

import static com.atlassian.jira.ComponentContainer.Scope.INTERNAL;
import static com.atlassian.jira.ComponentContainer.Scope.PROVIDED;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestLockServiceRegistrar
{
    @Mock private ComponentContainer mockComponentContainer;
    @Mock private ComponentAdapter<?> mockComponentAdapter;

    private void setUpClustering()
    {
        final ClusterNodeProperties mockClusterNodeProperties = mock(ClusterNodeProperties.class);
        when(mockClusterNodeProperties.getNodeId()).thenReturn("anyNodeId");
        when(mockComponentContainer.getComponentInstance(ClusterNodeProperties.class))
                .thenReturn(mockClusterNodeProperties);
    }

    @Test
    public void shouldRegisterSimpleClusterLockServiceWhenNotClustered()
    {
        when(mockComponentContainer.getComponentAdapter(NullJiraClusterNodeHeartBeatDao.class))
                .thenReturn(mockComponentAdapter);

        // Invoke
        LockServiceRegistrar.registerLockService(mockComponentContainer);

        // Check
        verify(mockComponentContainer).implementation(
                PROVIDED, ClusterLockService.class, SimpleClusterLockService.class);
        verify(mockComponentContainer).implementation(INTERNAL,
                NullJiraClusterNodeHeartBeatDao.class, NullJiraClusterNodeHeartBeatDao.class);
        verify(mockComponentContainer).getComponentAdapter(NullJiraClusterNodeHeartBeatDao.class);
    }

    @Test
    public void shouldRegisterDatabaseBackedClusterLockServiceWhenClustered()
    {
        when(mockComponentContainer.getComponentAdapter(JiraClusterNodeHeartBeatDao.class))
                .thenReturn(mockComponentAdapter);

        // Set up
        setUpClustering();

        // Invoke
        LockServiceRegistrar.registerLockService(mockComponentContainer);

        // Check
        verify(mockComponentContainer).implementation(
                PROVIDED, ClusterLockService.class, StartableDatabaseClusterLockService.class);
        verify(mockComponentContainer).implementation(
                INTERNAL, ClusterLockDao.class, JiraClusterLockDao.class);
        verify(mockComponentContainer).implementation(INTERNAL,
                JiraClusterNodeHeartBeatDao.class, JiraClusterNodeHeartBeatDao.class);
        verify(mockComponentContainer).getComponentAdapter(JiraClusterNodeHeartBeatDao.class);
    }
}
