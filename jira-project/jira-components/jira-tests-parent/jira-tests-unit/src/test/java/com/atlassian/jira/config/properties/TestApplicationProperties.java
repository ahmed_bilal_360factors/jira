package com.atlassian.jira.config.properties;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.property.BooleanApplicationPropertySetEvent;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestApplicationProperties
{
    @Mock
    private ApplicationPropertiesStore applicationPropertiesStore;
    @Mock @AvailableInContainer
    private EventPublisher eventPublisher;
    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    private ApplicationPropertiesImpl applicationProperties;

    @Before
    public void setUp() throws Exception
    {
        this.applicationProperties = new ApplicationPropertiesImpl(applicationPropertiesStore);
    }

    @Test
    public void testGettingProperties()
    {
        when(applicationPropertiesStore.getStringFromDb(APKeys.JIRA_TIMETRACKING_DAYS_PER_WEEK)).thenReturn(null);
        when(applicationPropertiesStore.getStringFromDb(APKeys.JIRA_TIMETRACKING_HOURS_PER_DAY)).thenReturn(null);

        when(applicationPropertiesStore.getString(APKeys.JIRA_TIMETRACKING_DAYS_PER_WEEK)).thenReturn("7");
        when(applicationPropertiesStore.getString(APKeys.JIRA_TIMETRACKING_HOURS_PER_DAY)).thenReturn("24");

        assertNull(applicationProperties.getString(APKeys.JIRA_TIMETRACKING_DAYS_PER_WEEK));
        assertNull(applicationProperties.getString(APKeys.JIRA_TIMETRACKING_HOURS_PER_DAY));

        assertEquals("7", applicationProperties.getDefaultBackedString(APKeys.JIRA_TIMETRACKING_DAYS_PER_WEEK));
        assertEquals("24", applicationProperties.getDefaultBackedString(APKeys.JIRA_TIMETRACKING_HOURS_PER_DAY));
    }

    @Test
    public void testSettingOptionPublishesEvent()
    {
        final String propertyName = "option";
        applicationProperties.setOption(propertyName, true);
        verify(eventPublisher).publish(argThat(
                allOf(instanceOf(BooleanApplicationPropertySetEvent.class),
                        hasProperty("propertyKey", equalTo(propertyName)))));
    }
}
