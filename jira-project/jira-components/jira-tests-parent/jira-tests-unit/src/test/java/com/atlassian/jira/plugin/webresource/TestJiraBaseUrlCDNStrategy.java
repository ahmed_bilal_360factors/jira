package com.atlassian.jira.plugin.webresource;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.i18n.CachingI18nFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestJiraBaseUrlCDNStrategy
{
    @Test
    public void testWebResourceIntegrationNotEnabledOrDisabled()
    {
        FeatureManager featureManager = mock(FeatureManager.class);
        when(featureManager.isEnabled(JiraBaseUrlCDNStrategy.ENABLED_FEATURE_KEY)).thenReturn(false);
        when(featureManager.isEnabled(JiraWebResourceIntegration.CDN_DISABLED_FEATURE_KEY)).thenReturn(false);

        WebResourceIntegration wri = createWebResourceIntegration(featureManager);

        assertNull(wri.getCDNStrategy());
    }

    @Test
    public void testWebResourceIntegrationEnabledNotDisabled()
    {
        FeatureManager featureManager = mock(FeatureManager.class);
        when(featureManager.isEnabled(JiraBaseUrlCDNStrategy.ENABLED_FEATURE_KEY)).thenReturn(true);
        when(featureManager.isEnabled(JiraWebResourceIntegration.CDN_DISABLED_FEATURE_KEY)).thenReturn(false);

        WebResourceIntegration wri = createWebResourceIntegration(featureManager);

        assertNotNull(wri.getCDNStrategy());
    }

    @Test
    public void testWebResourceIntegrationNotEnabledAndDisabled()
    {
        FeatureManager featureManager = mock(FeatureManager.class);
        when(featureManager.isEnabled(JiraBaseUrlCDNStrategy.ENABLED_FEATURE_KEY)).thenReturn(false);
        when(featureManager.isEnabled(JiraWebResourceIntegration.CDN_DISABLED_FEATURE_KEY)).thenReturn(true);

        WebResourceIntegration wri = createWebResourceIntegration(featureManager);

        assertNull(wri.getCDNStrategy());
    }

    @Test
    public void testWebResourceIntegrationEnabledAndDisabled()
    {
        FeatureManager featureManager = mock(FeatureManager.class);
        when(featureManager.isEnabled(JiraBaseUrlCDNStrategy.ENABLED_FEATURE_KEY)).thenReturn(true);
        when(featureManager.isEnabled(JiraWebResourceIntegration.CDN_DISABLED_FEATURE_KEY)).thenReturn(true);

        WebResourceIntegration wri = createWebResourceIntegration(featureManager);

        assertNull(wri.getCDNStrategy());
    }

    @Test
    public void testStrategyNoPrefixNull()
    {
        JiraSystemProperties.getInstance().unsetProperty(JiraBaseUrlCDNStrategy.PREFIX_SYSTEM_PROPERTY);

        JiraBaseUrlCDNStrategy strategy = new JiraBaseUrlCDNStrategy(mockApplicationProperties("so.domain.com"));

        assertFalse(strategy.supportsCdn());
    }

    @Test
    public void testStrategyNoPrefixEmptyString()
    {
        JiraSystemProperties.getInstance().setProperty(JiraBaseUrlCDNStrategy.PREFIX_SYSTEM_PROPERTY, "");

        JiraBaseUrlCDNStrategy strategy = new JiraBaseUrlCDNStrategy(mockApplicationProperties("much.host.org"));

        assertFalse(strategy.supportsCdn());
    }

    @Test
    public void testStrategyPrefix()
    {
        JiraSystemProperties.getInstance().setProperty(JiraBaseUrlCDNStrategy.PREFIX_SYSTEM_PROPERTY, "//blah.cdn.com");

        JiraBaseUrlCDNStrategy strategy = new JiraBaseUrlCDNStrategy(mockApplicationProperties("http://very.cname.net"));

        assertTrue(strategy.supportsCdn());
        assertEquals("//blah.cdn.com/very.cname.net/my/url.js?a", strategy.transformRelativeUrl("/my/url.js?a"));
    }

    @Test
    public void testStrategyPrefixHttps()
    {
        JiraSystemProperties.getInstance().setProperty(JiraBaseUrlCDNStrategy.PREFIX_SYSTEM_PROPERTY, "//blah.cdn.com");

        JiraBaseUrlCDNStrategy strategy = new JiraBaseUrlCDNStrategy(mockApplicationProperties("https://very.cname.net"));

        assertTrue(strategy.supportsCdn());
        assertEquals("//blah.cdn.com/very.cname.net/my/url.js?a", strategy.transformRelativeUrl("/my/url.js?a"));
    }

    @Test
    public void testStrategyPrefixWithPathInBaseUrl()
    {
        JiraSystemProperties.getInstance().setProperty(JiraBaseUrlCDNStrategy.PREFIX_SYSTEM_PROPERTY, "//blah.cdn.com");

        JiraBaseUrlCDNStrategy strategy = new JiraBaseUrlCDNStrategy(mockApplicationProperties("http://very.cname.net/path/to/jira"));

        assertTrue(strategy.supportsCdn());
        assertEquals("//blah.cdn.com/very.cname.net/my/url.js?a", strategy.transformRelativeUrl("/my/url.js?a"));
    }

    @Test
    public void testStrategyPrefixBadBaseUrl()
    {
        JiraSystemProperties.getInstance().setProperty(JiraBaseUrlCDNStrategy.PREFIX_SYSTEM_PROPERTY, "//blah.cdn.com");

        JiraBaseUrlCDNStrategy strategy = new JiraBaseUrlCDNStrategy(mockApplicationProperties("oh.noes.not.a.url"));

        assertFalse(strategy.supportsCdn());
    }

    private WebResourceIntegration createWebResourceIntegration(FeatureManager featureManager)
    {
        return new JiraWebResourceIntegration(mock(PluginAccessor.class),
                mock(ApplicationProperties.class), mock(VelocityRequestContextFactory.class),
                mock(BuildUtilsInfo.class), mock(JiraAuthenticationContext.class), mock(CachingI18nFactory.class),
                mock(JiraHome.class), mock(EventPublisher.class), featureManager);
    }

    private ApplicationProperties mockApplicationProperties(String baseUrl)
    {
        ApplicationProperties applicationProperties = mock(ApplicationProperties.class);
        when(applicationProperties.getString(APKeys.JIRA_BASEURL)).thenReturn(baseUrl);
        return applicationProperties;
    }
}
