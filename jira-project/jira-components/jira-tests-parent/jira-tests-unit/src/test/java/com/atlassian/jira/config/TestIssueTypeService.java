package com.atlassian.jira.config;

import com.atlassian.fugue.Option;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.types.issuetype.IssueTypeTypeAvatarService;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.lang.Pair;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Collections;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.config.IssueTypeService.CreateValidationResult;
import static com.atlassian.jira.config.IssueTypeService.DeleteValidationResult;
import static com.atlassian.jira.config.IssueTypeService.IssueTypeCreateInput;
import static com.atlassian.jira.config.IssueTypeService.IssueTypeDeleteInput;
import static com.atlassian.jira.config.IssueTypeService.IssueTypeResult;
import static com.atlassian.jira.config.IssueTypeService.IssueTypeUpdateInput;
import static com.atlassian.jira.config.IssueTypeService.UpdateValidationResult;
import static com.atlassian.jira.util.ErrorCollection.Reason.CONFLICT;
import static com.atlassian.jira.util.ErrorCollection.Reason.FORBIDDEN;
import static com.atlassian.jira.util.ErrorCollection.Reason.NOT_FOUND;
import static com.atlassian.jira.util.ErrorCollection.Reason.NOT_LOGGED_IN;
import static com.atlassian.jira.util.ErrorCollection.Reason.VALIDATION_FAILED;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestIssueTypeService
{
    private static final long AVATAR_ID = 1l;
    private static final Long DEFAULT_ISSUE_TYPE_AVATAR_ID = 10l;
    private static final Long DEFAULT_SUBTASK_AVATAR_ID = 11l;
    private static final String ISSUE_TYPE_VALID_NAME = "issue_type_name";
    private static final String DESCRIPTION = "description";
    private static final String ISSUE_TYPE_ID = String.valueOf(1l);

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    private ApplicationUser user;
    @Mock
    private ConstantsManager constantsManager;
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private IssueTypeTypeAvatarService avatarService;
    @Mock
    private IssueTypeManager issueTypeManager;
    @Mock
    private I18nHelper i18n;
    @Mock
    private IssueTypeSchemeManager schemeManager;
    @Mock
    private IssueTypeSchemeManager issueTypeSchemeManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private SubTaskManager subTaskManager;
    @Mock
    private ApplicationProperties applicationProperties;

    private DefaultIssueTypeService issueTypeService;
    private Avatar validAvatar;
    private MockIssueType issueType;

    @Before
    public void setUp()
    {
        this.validAvatar = mock(Avatar.class);
        when(avatarService.getAvatar(user, 1l)).thenReturn(validAvatar);
        when(validAvatar.getId()).thenReturn(AVATAR_ID);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)).thenReturn(true);
        when(constantsManager.validateName(eq(ISSUE_TYPE_VALID_NAME), Matchers.any(Option.class))).thenReturn(Option.<Pair<String, ErrorCollection.Reason>>none());
        doAnswer(new Answer<String>()
        {
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable
            {
                return (String) invocation.getArguments()[0];
            }
        }).when(i18n).getText(Matchers.anyString());
        issueType = new MockIssueType(ISSUE_TYPE_ID, ISSUE_TYPE_VALID_NAME);
        assumeVisibleIssueTypes(issueType);
        when(applicationProperties.getString(APKeys.JIRA_DEFAULT_ISSUETYPE_AVATAR_ID)).thenReturn(DEFAULT_ISSUE_TYPE_AVATAR_ID.toString());
        when(applicationProperties.getString(APKeys.JIRA_DEFAULT_ISSUETYPE_SUBTASK_AVATAR_ID)).thenReturn(DEFAULT_SUBTASK_AVATAR_ID.toString());
        when(subTaskManager.isSubTasksEnabled()).thenReturn(true);

        this.issueTypeService = new DefaultIssueTypeService(constantsManager, avatarService,
                i18n, globalPermissionManager, issueTypeManager, schemeManager, permissionManager, issueTypeSchemeManager, subTaskManager, applicationProperties);

    }

    @Test
    public void validateCreateIssueTypePassesWithValidParameters()
    {
        CreateValidationResult validationResult =
                issueTypeService.validateCreateIssueType(user, getValidIssueTypeCreateInput());

        assertThat(validationResult.isValid(), is(true));
        assertThat(validationResult.getIssueTypeInput().get(), is(getValidIssueTypeCreateInput()));
    }

    @Test
    public void validateCreateIssueTypeWithInvalidName()
    {
        String invalidName = "invalidName";
        IssueTypeCreateInput issueTypeCreateInput = IssueTypeCreateInput.builder()
                .setDescription(DESCRIPTION)
                .setType(IssueTypeCreateInput.Type.STANDARD)
                .setName(invalidName)
                .build();

        when(constantsManager.validateName(invalidName, Option.<IssueType>none())).thenReturn(some(Pair.of("invalid", VALIDATION_FAILED)));

        CreateValidationResult validationResult = issueTypeService.validateCreateIssueType(user, issueTypeCreateInput);

        assertContainsReason(validationResult, VALIDATION_FAILED);
    }

    @Test
    public void validateCreateWithoutIssueTypeType()
    {
        exception.expect(IllegalArgumentException.class);
        IssueTypeCreateInput.builder()
                .setDescription(DESCRIPTION)
                .setName(ISSUE_TYPE_VALID_NAME)
                .build();
    }

    @Test
    public void validateCreateSubTaskWhenSubtasksDisabled()
    {
        when(subTaskManager.isSubTasksEnabled()).thenReturn(false);

        IssueTypeCreateInput issueTypeCreateInput = IssueTypeCreateInput.builder()
                .setDescription(DESCRIPTION)
                .setName(ISSUE_TYPE_VALID_NAME)
                .setType(IssueTypeCreateInput.Type.SUBTASK)
                .build();

        CreateValidationResult validationResult = issueTypeService.validateCreateIssueType(user, issueTypeCreateInput);

        assertContainsReason(validationResult, VALIDATION_FAILED);
        assertThat(validationResult.getErrorCollection().getErrorMessages(), contains("admin.errors.subtasks.disabled"));
    }

    @Test
    public void validateCreateIssueTypeWithAnonymousUser()
    {
        CreateValidationResult validationResult = issueTypeService.validateCreateIssueType(null, getValidIssueTypeCreateInput());

        assertContainsReason(validationResult, NOT_LOGGED_IN);
    }

    @Test
    public void validateCreateIssueTypeWithoutPermissions()
    {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)).thenReturn(false);

        CreateValidationResult validationResult = issueTypeService.validateCreateIssueType(user, getValidIssueTypeCreateInput());

        assertContainsReason(validationResult, FORBIDDEN);
    }

    @Test
    public void createsValidSubTaskType()
    {
        IssueTypeCreateInput issueTypeCreateInput = IssueTypeCreateInput.builder()
                .setDescription(DESCRIPTION)
                .setName(ISSUE_TYPE_VALID_NAME)
                .setType(IssueTypeCreateInput.Type.SUBTASK)
                .build();

        CreateValidationResult createValidationResult = CreateValidationResult.ok(issueTypeCreateInput);
        IssueType expectedIssueType = mock(IssueType.class);

        when(issueTypeManager.createSubTaskIssueType(eq(ISSUE_TYPE_VALID_NAME), eq(DESCRIPTION), anyLong()))
                .thenReturn(expectedIssueType);

        IssueTypeResult issueType = issueTypeService.createIssueType(user, createValidationResult);

        assertThat(issueType.getIssueType(), is(expectedIssueType));
    }

    @Test
    public void createValidIssueType()
    {
        IssueTypeCreateInput issueTypeCreateInput = getValidIssueTypeCreateInput();

        CreateValidationResult createValidationResult = CreateValidationResult.ok(issueTypeCreateInput);
        IssueType expectedIssueType = mock(IssueType.class);

        when(issueTypeManager.createIssueType(eq(ISSUE_TYPE_VALID_NAME), eq(DESCRIPTION), anyLong())).thenReturn(expectedIssueType);

        IssueTypeResult issueType = issueTypeService.createIssueType(user, createValidationResult);

        assertThat(issueType.getIssueType(), is(expectedIssueType));
    }

    @Test
    public void validateUpdateWithValidArguments()
    {
        IssueTypeUpdateInput issueTypeUpdateInput = getValidIssueTypeUpdateInput();

        UpdateValidationResult validationResult = issueTypeService.validateUpdateIssueType(user, ISSUE_TYPE_ID, issueTypeUpdateInput);

        assertThat(validationResult.isValid(), is(true));
    }

    @Test
    public void validateUpdateForNonExistingIssueType()
    {
        IssueTypeUpdateInput issueTypeUpdateInput = getValidIssueTypeUpdateInput();
        when(permissionManager.getProjects(ProjectPermissions.BROWSE_PROJECTS, user)).thenReturn(Collections.<Project>emptyList());

        UpdateValidationResult validationResult = issueTypeService.validateUpdateIssueType(user, ISSUE_TYPE_ID, issueTypeUpdateInput);

        assertContainsReason(validationResult, NOT_FOUND);
    }

    @Test
    public void validateUpdateSuccessfulWithoutAvatarChange()
    {
        IssueTypeUpdateInput issueTypeCreateInput = IssueTypeUpdateInput.builder()
                .setDescription(DESCRIPTION)
                .setName(ISSUE_TYPE_VALID_NAME)
                .build();

        UpdateValidationResult validationResult = issueTypeService.validateUpdateIssueType(user, ISSUE_TYPE_ID, issueTypeCreateInput);

        assertThat(validationResult.isValid(), is(true));
    }

    @Test
    public void validateUpdateSuccessfulWithoutNameChange()
    {
        IssueTypeUpdateInput issueTypeUpdateInput = IssueTypeUpdateInput.builder()
                .setDescription(DESCRIPTION)
                .setAvatarId(AVATAR_ID)
                .build();

        UpdateValidationResult validationResult = issueTypeService.validateUpdateIssueType(user, ISSUE_TYPE_ID, issueTypeUpdateInput);

        assertThat(validationResult.isValid(), is(true));
    }

    @Test
    public void validateUpdateWithInvalidName()
    {
        when(constantsManager.validateName("invalid-name", Option.<IssueType>some(issueType))).thenReturn(some(Pair.of("invalid", VALIDATION_FAILED)));

        IssueTypeUpdateInput issueTypeUpdateInput = IssueTypeUpdateInput.builder()
                .setName("invalid-name")
                .setDescription(DESCRIPTION)
                .build();

        UpdateValidationResult validationResult = issueTypeService.validateUpdateIssueType(user, ISSUE_TYPE_ID, issueTypeUpdateInput);

        assertContainsReason(validationResult, VALIDATION_FAILED);
    }

    @Test
    public void validateUpdateWithInvalidAvatar()
    {
        IssueTypeUpdateInput issueTypeCreateInput = IssueTypeUpdateInput.builder()
                .setAvatarId(100l)
                .build();

        UpdateValidationResult validationResult = issueTypeService.validateUpdateIssueType(user, ISSUE_TYPE_ID, issueTypeCreateInput);

        assertContainsReason(validationResult, VALIDATION_FAILED);
        assertThat(validationResult.getErrorCollection().getErrors().values(), contains("admin.errors.project.no.avatar.with.id"));
    }

    @Test
    public void validateUpdateWithoutUser()
    {
        UpdateValidationResult validationResult = issueTypeService.validateUpdateIssueType(null, ISSUE_TYPE_ID, getValidIssueTypeUpdateInput());

        assertContainsReason(validationResult, NOT_LOGGED_IN);
    }

    @Test
    public void validateUpdateWithNonAdminUser()
    {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)).thenReturn(false);

        UpdateValidationResult validationResult = issueTypeService.validateUpdateIssueType(user, ISSUE_TYPE_ID, getValidIssueTypeUpdateInput());

        assertContainsReason(validationResult, FORBIDDEN);
    }

    @Test
    public void updateIssueTypeWithNameChange()
    {
        IssueType issueTypeToUpdate = new MockIssueType(ISSUE_TYPE_ID, "old-name", true, validAvatar);
        UpdateValidationResult updateValidationResult = UpdateValidationResult.ok(IssueTypeUpdateInput.builder().setName("new-name").build(), issueTypeToUpdate);

        issueTypeService.updateIssueType(user, updateValidationResult);
        verify(issueTypeManager).updateIssueType(issueTypeToUpdate, "new-name", null, AVATAR_ID);
    }

    @Test
    public void validateIssueTypeDelete()
    {
        IssueType alternative = issueType("2");
        assumeVisibleIssueTypes(issueType, alternative);

        assumeIssueTypeDoesNotHaveAssociatedIssues();

        DeleteValidationResult validationResult =
                issueTypeService.validateDeleteIssueType(user, new IssueTypeDeleteInput("1", some(alternative.getId())));

        assertThat(validationResult.isValid(), is(true));
    }

    @Test
    public void validateIssueTypeDeleteWithoutLoggedInUser()
    {
        final DeleteValidationResult validationResult = issueTypeService.validateDeleteIssueType(null, new IssueTypeDeleteInput("1", some("2")));
        assertContainsReason(validationResult, NOT_LOGGED_IN);
    }

    @Test
    public void validateIssueTypeDeleteWithNonAdminUser()
    {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)).thenReturn(false);

        final DeleteValidationResult validationResult =
                issueTypeService.validateDeleteIssueType(user, new IssueTypeDeleteInput("1", some(issueType("2").getId())));

        assertContainsReason(validationResult, FORBIDDEN);
    }

    @Test
    public void validateIssueTypeDeleteWithNonExistingReplacement()
    {
        final DeleteValidationResult validationResult =
                issueTypeService.validateDeleteIssueType(user, new IssueTypeDeleteInput("1", some("2")));

        assertContainsReason(validationResult, NOT_FOUND);
        assertThat(validationResult.getErrorCollection().getErrorMessages(), contains("admin.error.issue.type.delete.alternative.not.exist"));
    }

    @Test
    public void validateIssueTypeDeleteWithNonExistingIssueType()
    {
        final DeleteValidationResult validationResult =
                issueTypeService.validateDeleteIssueType(user, new IssueTypeDeleteInput("100", some("2")));

        assertContainsReason(validationResult, NOT_FOUND);
        assertThat(validationResult.getErrorCollection().getErrorMessages(), hasItem("admin.error.issue.type.delete.not.exist"));
    }

    @Test
    public void validateIssueTypeDeleteWithInvalidAlternative()
    {
        IssueType providedReplacement = issueType("2");
        IssueType availableReplacement = issueType("3");

        assumeIssueTypeHasAssociatedIssues();
        assumeVisibleIssueTypes(issueType, providedReplacement);
        assumeAvailableAlternativesForIssueType(availableReplacement);

        final DeleteValidationResult validateResult =
                issueTypeService.validateDeleteIssueType(user, new IssueTypeDeleteInput("1", Option.some(providedReplacement.getId())));

        assertContainsReason(validateResult, CONFLICT);
    }

    @Test
    public void validateIssueTypeDeleteWithAssociatedIssues()
    {
        assumeIssueTypeHasAssociatedIssues();
        assumeVisibleIssueTypes(issueType, issueType("2"));
        assumeNoAlternativesForIssueType();

        DeleteValidationResult validationResult =
                issueTypeService.validateDeleteIssueType(user, new IssueTypeDeleteInput("1", some("2")));

        assertContainsReason(validationResult, NOT_FOUND);
    }

    @Test
    public void validateIssueTypeDeleteWithoutAlternativeWhenAlternativeNotRequired()
    {
        final DeleteValidationResult validationResult =
                issueTypeService.validateDeleteIssueType(user, new IssueTypeDeleteInput("1", Option.<String>none()));

        assertThat(validationResult.isValid(), is(true));
    }

    @Test
    public void deleteIssueType()
    {
        issueTypeService.deleteIssueType(user,
                new DeleteValidationResult(new SimpleErrorCollection(),
                        some(new IssueTypeDeleteInput("1", some("2")))));

        verify(issueTypeManager).removeIssueType("1", "2");
    }

    private static void assertContainsReason(final ServiceResult result, final ErrorCollection.Reason reason)
    {
        assertThat("Service result is invalid", result.isValid(), is(false));
        assertThat("Service result returned with reason " + reason, result.getErrorCollection().getReasons(), hasItem(reason));
    }

    private IssueTypeCreateInput getValidIssueTypeCreateInput()
    {
        return IssueTypeCreateInput.builder()
                .setDescription(DESCRIPTION)
                .setType(IssueTypeCreateInput.Type.STANDARD)
                .setName(ISSUE_TYPE_VALID_NAME)
                .build();
    }

    private IssueTypeUpdateInput getValidIssueTypeUpdateInput()
    {
        return IssueTypeUpdateInput.builder()
                .setDescription(DESCRIPTION)
                .setAvatarId(AVATAR_ID)
                .setName(ISSUE_TYPE_VALID_NAME)
                .build();
    }

    private IssueType issueType(String id)
    {
        IssueType mockedIssueType = mock(IssueType.class);
        when(mockedIssueType.getId()).thenReturn(id);
        when(issueTypeManager.getIssueType(id)).thenReturn(mockedIssueType);
        return mockedIssueType;
    }

    private void assumeVisibleIssueTypes(IssueType... issueTypeList)
    {
        Project project = mock(Project.class);
        when(permissionManager.getProjects(ProjectPermissions.BROWSE_PROJECTS, user)).thenReturn(newArrayList(project));
        when(issueTypeSchemeManager.getIssueTypesForProject(project)).thenReturn(newArrayList(issueTypeList));
    }

    private void assumeIssueTypeHasAssociatedIssues()
    {
        when(issueTypeManager.hasAssociatedIssues(issueType)).thenReturn(true);
    }

    private void assumeIssueTypeDoesNotHaveAssociatedIssues()
    {
        when(issueTypeManager.hasAssociatedIssues(issueType)).thenReturn(false);
    }

    private void assumeAvailableAlternativesForIssueType(IssueType... issueTypeList)
    {
        when(issueTypeManager.getAvailableIssueTypes(issueType)).thenReturn(newArrayList(issueTypeList));
    }

    private void assumeNoAlternativesForIssueType()
    {
        assumeAvailableAlternativesForIssueType();
    }
}
