
package com.atlassian.jira.plugin.webfragment.conditions;

import java.util.Collections;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.DelegatingApplicationUser;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.plugin.web.Condition;

import com.google.common.collect.ImmutableMap;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class TestAbstractWebCondition
{
    private static final Map<String, Object> ANONYMOUS = Collections.emptyMap();

    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    private User expectedUser;
    private ApplicationUser expectedAppUser;

    @Mock
    @AvailableInContainer
    private UserUtil mockUserUtil;

    @Before
    public void setUp()
    {
        expectedUser = null;
        expectedAppUser = null;
    }

    @Test
    public void testShouldDisplayWithUser()
    {
        asFred();
        checkShouldDisplay(true, context("user", expectedUser));
    }

    @Test
    public void testShouldDisplayWithUsername()
    {
        asFred();
        checkShouldDisplay(true, context("username", "fred"));
    }

    @Test
    public void testShouldNotDisplayWithUser()
    {
        checkShouldDisplay(false, context("username", "fred"));
    }

    @Test
    public void testShouldDisplayAnonymous()
    {
        checkShouldDisplay(true, ANONYMOUS);
    }

    @Test
    public void testShouldNotDisplayAnonymous()
    {
        checkShouldDisplay(false, ANONYMOUS);
    }

    @Test
    public void testShouldReturnFalseIfExceptionOccur()
    {
        checkShouldDisplay(false, ANONYMOUS, new AbstractWebCondition()
        {
            @Override
            public boolean shouldDisplay(final ApplicationUser user, final JiraHelper jiraHelper)
            {
                throw new LinkageError("That's really unexpected error here!");
            }
        });
    }

    private void asFred()
    {
        expectedUser = new MockUser("fred");
        expectedAppUser = new DelegatingApplicationUser("fred", expectedUser);
        when(mockUserUtil.getUserByName("fred")).thenReturn(expectedAppUser);
    }

    private void checkShouldDisplay(final boolean expectedResult, final Map<String, Object> context)
    {
        checkShouldDisplay(expectedResult, context, new AbstractWebCondition()
        {
            @Override
            public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper)
            {
                assertEquals(expectedAppUser, user);
                return expectedResult;
            }
        });
    }

    private void checkShouldDisplay(final boolean expectedResult, final Map<String, Object> context, final Condition condition)
    {
        assertEquals(expectedResult, condition.shouldDisplay(context));
    }

    private Map<String, Object> context(String key, Object value)
    {
        return ImmutableMap.of(key, value);
    }
}
