package com.atlassian.jira.upgrade;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import electric.xml.Element;
import electric.xml.Elements;
import electric.xml.ParseException;
import org.apache.commons.io.IOUtils;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.InputStream;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import static com.atlassian.jira.matchers.FileMatchers.isFile;
import static org.junit.Assert.assertThat;

/**
 * Check that the version of the latest upgrade task in the upgrades.xml is less than or equal to the jira.build.number
 * in the distribution and root poms
 * @since 6.4
 */
public class TestUpgradeVersionNumbers
{
    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    private static final String UPGRADES_XML = "upgrades.xml";
    private static final String UNIT_TESTS_PATH = "/jira-components/jira-tests-parent/jira-tests-unit";
    private final File parentPom;
    private final File distributionPom;
    private static final BuildNumComparator BUILD_NUM_COMPARATOR = new BuildNumComparator();
    private final String latestBuildNo;
    private final DocumentBuilder documentBuilder;
    private final XPathFactory xPathFactory = XPathFactory.newInstance();

    public TestUpgradeVersionNumbers() throws ParserConfigurationException
    {
        final String rootPath = getProjectRootPath();
        parentPom = new File(rootPath + "pom.xml");
        distributionPom = new File(rootPath + "jira-distribution/pom.xml");
        latestBuildNo = getUpgradeTasks().last();
        documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    }

    @Test
    public void testLatestUpgradeTaskAgainstDistPom()
    {
        String distVersionNo = getJiraVersionNumberFromPom(distributionPom);
        assertThat(latestBuildNo, BuildNumberMatcher.lessThatEqualTo(distVersionNo));
    }

    @Test
    public void testLatestUpgradeTaskAgainstRootPom()
    {
        String rootVersionNo = getJiraVersionNumberFromPom(parentPom);
        assertThat(latestBuildNo, BuildNumberMatcher.lessThatEqualTo(rootVersionNo));
    }

    private String getJiraVersionNumberFromPom(final File pomFile)
    {
        assertThat(pomFile, isFile());
        try
        {
            final XPathExpression xpath = xPathFactory.newXPath().compile("/project/properties");

            final Document doc = documentBuilder.parse(pomFile);
            doc.getDocumentElement().normalize();
            final Node propsNode = (Node) xpath.evaluate(doc, XPathConstants.NODE);
            final NodeList props = propsNode.getChildNodes();

            for (int i = 0; i < props.getLength(); i++)
            {
                final Node node = props.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName().equals("jira.build.number"))
                {
                    return node.getTextContent();
                }
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        return null;
    }

    private String getProjectRootPath()
    {
        final File distributionPomFile = new File("jira-distribution/pom.xml");
        final String rootPath;
        if (distributionPomFile.exists())
        {
            // CWD is the project root
            rootPath = "";
        }
        else
        {
            // Where I am? Maybe in unit-tests?
            final String cwd = System.getProperty("user.dir");
            if (cwd.endsWith(UNIT_TESTS_PATH))
            {
                rootPath = cwd.replace(UNIT_TESTS_PATH, "/");
            }
            else
            {
                // Not in unit tests - no idea where - project structure has changed?
                throw new AssertionError("I'm lost, I don't know where I am! Please fix me. "
                        + "The current working directory is: " + cwd);
            }
        }
        return rootPath;
    }

    private SortedSet<String> getUpgradeTasks()
    {
        SortedSet<String> sortedTasks = new TreeSet<String>(BUILD_NUM_COMPARATOR);

        final InputStream is = ClassLoaderUtils.getResourceAsStream(UPGRADES_XML, this.getClass());
        try
        {
            final electric.xml.Document doc = new electric.xml.Document(is);
            final Element root = doc.getRoot();
            final Elements actions = root.getElements("upgrade");

            while (actions.hasMoreElements())
            {
                final Element action = (Element) actions.nextElement();
                final String buildNo = action.getAttribute("build");

                sortedTasks.add(buildNo);
            }
        }
        catch (ParseException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            IOUtils.closeQuietly(is);
        }
        return sortedTasks;
    }

    public static class BuildNumberMatcher
    {
        private static final BuildNumComparator BUILD_NUM_COMPARATOR = new BuildNumComparator();
        @Factory
        public static Matcher<String> lessThatEqualTo(final String buildNo)
        {
            return new TypeSafeMatcher<String>()
            {
                @Override
                protected boolean matchesSafely(final String item)
                {
                    return BUILD_NUM_COMPARATOR.compare(item, buildNo) <= 0;
                }

                @Override
                public void describeTo(final Description description)
                {
                    description.appendValue(buildNo);
                }
            };
        }
    }
}
