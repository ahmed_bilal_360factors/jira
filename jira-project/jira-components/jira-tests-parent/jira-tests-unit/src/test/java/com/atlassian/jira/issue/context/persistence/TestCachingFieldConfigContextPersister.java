package com.atlassian.jira.issue.context.persistence;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;

import org.junit.Before;
import org.junit.Test;
import org.ofbiz.core.entity.GenericValue;

import static com.atlassian.jira.ofbiz.FieldMap.build;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestCachingFieldConfigContextPersister
{
    public static final long FIELD_CONFIGURATION_ID = 30303L;
    private MockOfBizDelegator mockOfBizDelegator;

    @Before
    public void setUp() throws Exception
    {
        mockOfBizDelegator = new MockOfBizDelegator();
    }

    @Test
    public void test_getRelevantConfigSchemeId_GlobalIssueType()
    {
        final long projectId = 24L;
        final Project project = new MockProject(projectId);
        final long fieldConfigSchemeId = 10101L;
        final String fieldId = "cf_12";

        // config scheme 10101L : NULL issue type
        createFieldConfigSchemeIssueTypeValueInOfBiz(null, fieldConfigSchemeId, FIELD_CONFIGURATION_ID);
        createConfigurationContextValueInOfBiz(projectId, fieldConfigSchemeId, fieldId);

        CachingFieldConfigContextPersister persister = createSUT();
        final Long configSchemeId = persister.getRelevantConfigSchemeId(issueContext(project, "1"), fieldId);

        assertEquals(Long.valueOf(fieldConfigSchemeId), configSchemeId);
    }

    /**
     * JRA-40663 Test that we handle old crap data with a "-1" instead of NULL for "all issue types".
     */
    @Test
    public void test_getRelevantConfigSchemeId_GlobalIssueTypeLegacy()
    {
        final Long configSchemeId = 10101L;
        final long projectId = 24L;
        final Project project = new MockProject(projectId);
        final String fieldId = "cf_12";

        // config scheme 10101L : "-1" issue type (JRA-40663)
        createFieldConfigSchemeIssueTypeValueInOfBiz("-1", configSchemeId, FIELD_CONFIGURATION_ID);
        createConfigurationContextValueInOfBiz(projectId, configSchemeId, fieldId);

        CachingFieldConfigContextPersister persister = createSUT();
        final Long relevantConfigSchemeId = persister.getRelevantConfigSchemeId(issueContext(project, "1"), fieldId);

        assertEquals(configSchemeId, relevantConfigSchemeId);
    }

    @Test
    public void test_getRelevantConfigSchemeId_IssueTypes()
    {
        final long projectId = 24L;
        final Project project = new MockProject(projectId);
        final Long configSchemeId = 10101L;
        final String issueTypeOne = "1";
        final String issueTypeThree = "3";
        final String customFieldId = "cf_12";

        // config scheme 10101L : explicit issue types
        createFieldConfigSchemeIssueTypeValueInOfBiz(issueTypeOne, configSchemeId, FIELD_CONFIGURATION_ID);
        createFieldConfigSchemeIssueTypeValueInOfBiz(issueTypeThree, configSchemeId, FIELD_CONFIGURATION_ID);
        createConfigurationContextValueInOfBiz(24L, 10101L, customFieldId);

        CachingFieldConfigContextPersister persister = createSUT();

        // Test IssueType 1
        final Long csForIssueTypeOne = persister.getRelevantConfigSchemeId(
                issueContext(project, issueTypeOne), customFieldId);

        assertEquals(configSchemeId, csForIssueTypeOne);

        // Test IssueType 2 - NOT in scope
        final Long csForIssueTypeTwo = persister.getRelevantConfigSchemeId(issueContext(project, "2"), customFieldId);
        assertEquals(null, csForIssueTypeTwo);

        // Test IssueType 3
        final Long csForIssueTypeThree = persister.getRelevantConfigSchemeId(
                issueContext(project, issueTypeThree), customFieldId);

        assertEquals(configSchemeId, csForIssueTypeThree);
    }

    /**
     * Test for JRA-40697.
     * JIRA would NPE when it saw a Project with null Project Category but only if there was non-null projectcategory
     * values in the ConfigurationContext table.
     */
    @Test
    public void testNPE() throws Exception
    {
        final String fieldId = "cf_12";
        mockOfBizDelegator.createValue("ConfigurationContext", build("projectcategory", 10000L, "key", fieldId));

        CachingFieldConfigContextPersister persister = createSUT();
        final Project project = new MockProject(15L);

        // Confirm that the Mock Project really has a null Project Cat
        assertNull(project.getProjectCategoryObject());
        // now call getRelevantConfigSchemeId() ... we just want to prove that it does not NPE - JRA-40697
        persister.getRelevantConfigSchemeId(project, fieldId, true);
    }

    private CachingFieldConfigContextPersister createSUT()
    {
        return new CachingFieldConfigContextPersister(mockOfBizDelegator, null, new MemoryCacheManager());
    }

    private GenericValue createFieldConfigSchemeIssueTypeValueInOfBiz(final String issueType,
            final long fieldConfigSchemeId, final long fieldConfigurationId)
    {
        return mockOfBizDelegator.createValue("FieldConfigSchemeIssueType", build(
                "issuetype", issueType,
                "fieldconfigscheme", fieldConfigSchemeId,
                "fieldconfiguration", fieldConfigurationId
        ));
    }

    private void createConfigurationContextValueInOfBiz(final long projectId, final long fieldConfigSchemeId,
            final String fieldId)
    {
        mockOfBizDelegator.createValue("ConfigurationContext", build(
                "projectcategory", null,
                "project", projectId,
                "fieldconfigscheme", fieldConfigSchemeId,
                "key", fieldId
        ));
    }

    private IssueContext issueContext(final Project project, final String issueTypeId)
    {
        final IssueContext issueContext = mock(IssueContext.class);
        when(issueContext.getProjectObject()).thenReturn(project);
        when(issueContext.getIssueTypeId()).thenReturn(issueTypeId);
        return issueContext;
    }
}
