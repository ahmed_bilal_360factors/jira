package com.atlassian.jira.sharing.index;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.index.ha.ReplicatedIndexManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.Strict;
import com.atlassian.jira.sharing.MockSharedEntity;
import com.atlassian.jira.sharing.SharedEntity;
import com.atlassian.jira.sharing.SharedEntity.SharePermissions;
import com.atlassian.jira.sharing.SharedEntity.TypeDescriptor;
import com.atlassian.jira.sharing.SharedEntityAccessor;
import com.atlassian.jira.sharing.index.DefaultSharedEntityIndexer.EntityDocumentFactory;
import com.atlassian.jira.sharing.type.ShareTypeFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.DelegatingApplicationUser;
import com.atlassian.jira.user.MockUser;

import com.google.common.collect.ImmutableSet;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.RAMDirectory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultSharedEntityIndexer
{
    private static final String TEST_DEFAULT_SHARED_ENTITY_INDEXER = "TestDefaultSharedEntityIndexer";
    static final TypeDescriptor<SharedEntity> TYPE_DESCRIPTOR = SharedEntity.TypeDescriptor.Factory.get().create(TEST_DEFAULT_SHARED_ENTITY_INDEXER);

    @Rule
    public final MockitoContainer container = MockitoMocksInContainer.rule(this);

    @Mock private QueryFactory queryFactory;
    @Mock private SharedEntityAccessor.Factory accessorFactory;
    @Mock private ReplicatedIndexManager replicatedIndexManager;
    @Mock private EntityDocumentFactory documentFactory;
    @Mock private DirectoryFactory directoryFactory;

    @AvailableInContainer
    private final ApplicationProperties applicationProperties = new MockApplicationProperties();

    private final User user = new MockUser("testClear");
    private final ApplicationUser appUser = new DelegatingApplicationUser("key", user);
    private SharedEntity entity;


    @Before
    public void setUp()
    {
        entity = new MockSharedEntity(1L, TYPE_DESCRIPTOR, appUser, SharePermissions.PRIVATE)
        {
            @Override
            public String getName()
            {
                return "indexMeName";
            }

            @Override
            public String getDescription()
            {
                return "indexMeDescription";
            }
        };

        @SuppressWarnings("unchecked")
        final SharedEntityAccessor<SharedEntity> sharedEntityAccessor = mock(SharedEntityAccessor.class, new Strict());
        when(accessorFactory.getSharedEntityAccessor(TYPE_DESCRIPTOR)).thenReturn(sharedEntityAccessor);
    }

    @Test
    public void testGetSearcher() throws Exception
    {
        final SharedEntityIndexer indexer = getIndexerInstance();
        indexer.getSearcher(TYPE_DESCRIPTOR);
    }

    @Test
    public void testClear() throws Exception
    {
        final SharedEntityIndexer indexer = getIndexerInstance();
        indexer.clear(TYPE_DESCRIPTOR);
    }


    @Test
    public void testOptimize() throws Exception
    {
        when(directoryFactory.get(TYPE_DESCRIPTOR)).thenReturn(new RAMDirectory());

        final SharedEntityIndexer indexer = getIndexerInstance();
        indexer.optimize(TYPE_DESCRIPTOR);
    }

    @Test
    public void testShutdown() throws Exception
    {
        final SharedEntityIndexer indexer = getIndexerInstance();
        indexer.shutdown(TYPE_DESCRIPTOR);
    }

    @Test
    public void testIndex() throws Exception
    {
        when(documentFactory.get(entity)).thenReturn(new TestEntityDocument());
        when(directoryFactory.get(TYPE_DESCRIPTOR)).thenReturn(new RAMDirectory());

        final SharedEntityIndexer indexer = getIndexerInstance();
        indexer.index(entity);
        verify(replicatedIndexManager).indexSharedEntity(entity);
    }

    @Test
    public void testIndexWithReplicationEnabled() throws Exception
    {
        when(documentFactory.get(entity)).thenReturn(new TestEntityDocument());
        when(directoryFactory.get(TYPE_DESCRIPTOR)).thenReturn(new RAMDirectory());

        final SharedEntityIndexer indexer = getIndexerInstance();
        indexer.index(ImmutableSet.of(entity), true);
        verify(replicatedIndexManager).indexSharedEntity(entity);
    }

    @Test
    public void testIndexWithReplicationDisabled() throws Exception
    {
        when(documentFactory.get(entity)).thenReturn(new TestEntityDocument());
        when(directoryFactory.get(TYPE_DESCRIPTOR)).thenReturn(new RAMDirectory());

        final SharedEntityIndexer indexer = getIndexerInstance();
        indexer.index(ImmutableSet.of(entity), false);
        verify(replicatedIndexManager, never()).indexSharedEntity(entity);
    }

    @Test
    public void testDeindex() throws Exception
    {
        when(documentFactory.get(entity)).thenReturn(new TestEntityDocument());
        when(directoryFactory.get(TYPE_DESCRIPTOR)).thenReturn(new RAMDirectory());

        final SharedEntityIndexer indexer = getIndexerInstance();
        indexer.deIndex(entity);
        verify(replicatedIndexManager).deIndexSharedEntity(entity);
    }

    @Test
    public void testDeindexWithReplicationEnabled() throws Exception
    {
        when(documentFactory.get(entity)).thenReturn(new TestEntityDocument());
        when(directoryFactory.get(TYPE_DESCRIPTOR)).thenReturn(new RAMDirectory());

        final SharedEntityIndexer indexer = getIndexerInstance();
        indexer.deIndex(ImmutableSet.of(entity), true);
        verify(replicatedIndexManager).deIndexSharedEntity(entity);
    }

    @Test
    public void testDeindexWithReplicationDisabled() throws Exception
    {
        when(documentFactory.get(entity)).thenReturn(new TestEntityDocument());
        when(directoryFactory.get(TYPE_DESCRIPTOR)).thenReturn(new RAMDirectory());

        final SharedEntityIndexer indexer = getIndexerInstance();
        indexer.deIndex(ImmutableSet.of(entity), false);
        verify(replicatedIndexManager, never()).deIndexSharedEntity(entity);
    }

    /**
     * Test DefaultEntityDocumentFactory inner class
     */
    @Test
    public void testDefaultEntityDocumentFactoryGet()
    {
        final Document expectedDocument = new Document();
        final SharedEntityDocumentFactory sharedEntityDocumentFactory = mock(SharedEntityDocumentFactory.class);
        when(sharedEntityDocumentFactory.create(entity)).thenReturn(expectedDocument);

        final ShareTypeFactory expectedShareTypeFactory = mock(ShareTypeFactory.class);
        final DefaultSharedEntityIndexer.DefaultEntityDocumentFactory documentFactoryUnderTest =
                new DefaultSharedEntityIndexer.DefaultEntityDocumentFactory(expectedShareTypeFactory)
        {
            @Override
            SharedEntityDocumentFactory createDocumentFactory(final ShareTypeFactory shareTypeFactory)
            {
                assertSame(expectedShareTypeFactory, shareTypeFactory);
                return sharedEntityDocumentFactory;
            }
        };

        final DefaultSharedEntityIndexer.EntityDocument entityDocument = documentFactoryUnderTest.get(entity);
        assertNotNull(entityDocument);
        final Term term = entityDocument.getIdentifyingTerm();
        assertNotNull(term);
        assertEquals("id:1", term.toString());

        // cant call this because deep down in the Field builders is invokes the UserManager code and up comes JIRA
        final Document document = entityDocument.getDocument();
        assertNotNull(document);
        assertSame(expectedDocument, document);
    }

    private DefaultSharedEntityIndexer getIndexerInstance()
    {
        return new DefaultSharedEntityIndexer(queryFactory, accessorFactory, documentFactory, directoryFactory,
                replicatedIndexManager, applicationProperties);
    }

    @Test
    public void testFoo()
    {
        getIndexerInstance();
    }



    static class TestEntityDocument implements DefaultSharedEntityIndexer.EntityDocument
    {
        public Document getDocument()
        {
            return new Document();
        }

        public Term getIdentifyingTerm()
        {
            return new Term("test", "23");
        }

        public TypeDescriptor<?> getType()
        {
            return TYPE_DESCRIPTOR;
        }
    }
}
