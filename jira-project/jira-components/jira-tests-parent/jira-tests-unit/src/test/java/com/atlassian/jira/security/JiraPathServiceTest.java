package com.atlassian.jira.security;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JiraPathServiceTest
{
    private static final String ADMIN = "admin";

    @Test
    public void testGetRequiredRoles() throws Exception
    {
        assertRequiredRoles(null);
        assertRequiredRoles("");
        assertRequiredRoles("/");
        assertRequiredRoles("/secure/");
        assertRequiredRoles("/secure/fred/");
        assertRequiredRoles("/secure/fred/admin/");
        assertRequiredRoles("/secure/admin");
        assertRequiredRoles("/secure/admin/", ADMIN);
        assertRequiredRoles("/secure/admin/fred", ADMIN);
    }


    private static void assertRequiredRoles(final String path, final String... expectedRoles)
    {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getServletPath()).thenReturn(path);

        final JiraPathService service = new JiraPathService();
        assertThat(service.getRequiredRoles(request), containsInAnyOrder(expectedRoles));
    }
}