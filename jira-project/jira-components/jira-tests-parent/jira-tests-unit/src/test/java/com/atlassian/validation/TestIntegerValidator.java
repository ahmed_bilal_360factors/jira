package com.atlassian.validation;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestIntegerValidator
{

    final IntegerValidator validator = new IntegerValidator();

    final String[] CORRECT_VALUES = { "0", "-1", "128", "255", "15001900", Integer.toString(Integer.MAX_VALUE) };
    final String[] INCORRECT_VALUES = { "e", "0.01", "1e6", Long.toString(Long.MAX_VALUE) };

    @Test
    public void acceptsCorrectValues()
    {
        for (final String val : CORRECT_VALUES)
        {
            assertTrue("Should accept correct value: '" + val + "'", validator.validate(val).isValid());
        }
    }

    @Test
    public void rejectsIncorrectValues()
    {
        for (final String val : INCORRECT_VALUES)
        {
            assertFalse("Should reject incorrect value: '" + val + "'", validator.validate(val).isValid());
        }
    }

    @Test
    public void rejectsValueGreaterThanMaxIntAsTooBig()
    {
        long l = Integer.MAX_VALUE + 1L;
        final Validator.Result result = validator.validate(Long.toString(l));
        assertThat(result, instanceOf(Failure.class));
        assertThat(result.getErrorMessage(), containsString("Value must be no more than"));
    }

    @Test
    public void rejectsNotANumberAsUnparsable()
    {
        final Validator.Result result = validator.validate("foo");
        assertThat(result, instanceOf(Failure.class));
        assertThat(result.getErrorMessage(), containsString("cannot be parsed to an integer"));
    }


}
