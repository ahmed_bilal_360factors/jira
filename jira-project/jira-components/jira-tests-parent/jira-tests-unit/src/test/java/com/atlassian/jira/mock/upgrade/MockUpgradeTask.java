package com.atlassian.jira.mock.upgrade;

import java.util.Collection;
import java.util.Collections;

import javax.annotation.Nullable;

import com.atlassian.jira.upgrade.UpgradeTask;

public class MockUpgradeTask implements UpgradeTask
{
    private final String version;
    private final String shortDescription;
    private final ScheduleOption scheduleOption;
    private boolean doUpgradeCalled;

    public MockUpgradeTask(final String version, final String shortDescription)
    {
        this.version = version;
        this.shortDescription = shortDescription;
        scheduleOption = ScheduleOption.BEFORE_JIRA_STARTED;
    }

    public MockUpgradeTask(final String version, final String shortDescription, final ScheduleOption scheduleOption)
    {
        this.version = version;
        this.shortDescription = shortDescription;
        this.scheduleOption = scheduleOption;
    }

    public String getBuildNumber()
    {
        return version;
    }

    public String getShortDescription()
    {
        return shortDescription;
    }

    public void doUpgrade(boolean setupMode)
    {
        doUpgradeCalled = true;
    }

    public Collection<String> getErrors()
    {
        return Collections.emptyList();
    }

    @Override
    public String toString()
    {
        return "version: " + version + ", shortDesctription: " + shortDescription;
    }

    public String getClassName()
    {
        return "MockUpgradeTask" + version;
    }

    @Nullable
    @Override
    public String dependsUpon()
    {
        return null;
    }

    @Override
    public ScheduleOption getScheduleOption()
    {
        return scheduleOption;
    }

    public boolean isDoUpgradeCalled()
    {
        return doUpgradeCalled;
    }
}
