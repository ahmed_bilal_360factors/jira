package com.atlassian.jira.mail;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.JiraApplicationContext;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.mail.builder.RenderingMailQueueItem;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.UserLocaleStore;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.atlassian.jira.mail.MailServiceQueueItemBuilder.ISSUE;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;


public class MailServiceQueueItemBuilderTest
{

    private static final String PERSONAL_EMAIL = "test@gmail.com";
    private static final String PROJECT_EMAIL = "jira_project@gmail.com";
    public static final String RECIPIENT_EMAIL = "to@gmail.com";
    @Mock
    private User replyTo;
    @Mock
    private NotificationRecipient recipient;

    private String subjectTemplatePath = "";
    private String bodyTemplatePath = "";

    private Map<String, Object> context = new HashMap<String, Object>();

    MailServiceQueueItemBuilder builder;
    @Mock
    private com.atlassian.jira.mail.TemplateContext templateContext;
    @Mock
    private JiraApplicationContext jiraApplicationContext;
    @Mock
    private UserLocaleStore userLocaleStore;
    @Mock
    private TemplateContextFactory templateContextFactory;
    @Mock
    private Issue issue;
    @Mock
    private Project project;


    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
        when(recipient.getEmail()).thenReturn(RECIPIENT_EMAIL);
        when(replyTo.getEmailAddress()).thenReturn(PERSONAL_EMAIL);
        setUpIssue();
        mockStaticDependencies();
        builder = new MailServiceQueueItemBuilder(replyTo, recipient, subjectTemplatePath, bodyTemplatePath, context);
    }

    @Test
    public void buildQueueItemMethodShouldUsePersonalEmailAsReplyTo() throws Exception
    {
        context.put(ISSUE, issue);
        RenderingMailQueueItem item = (RenderingMailQueueItem) builder.buildQueueItem();
        assertNotNull(item.getEmail());
        assertThat(item.getEmail().getReplyTo(), is(PERSONAL_EMAIL));
    }

    @Test
    public void buildQueueItemUsingProjectEmailAsReplyToMethodShouldUseProjectEmailAsReplyTo() throws Exception
    {
        context.put(ISSUE, issue);
        RenderingMailQueueItem item = (RenderingMailQueueItem) builder.buildQueueItemUsingProjectEmailAsReplyTo();
        assertNotNull(item.getEmail());
        assertThat(item.getEmail().getReplyTo(), is(PROJECT_EMAIL));
    }

    @Test
    public void replyToShouldBeNullIfProjectEmailIsNull() throws Exception
    {
        when(project.getEmail()).thenReturn(null);
        context.put(ISSUE, issue);
        RenderingMailQueueItem item = (RenderingMailQueueItem) builder.buildQueueItemUsingProjectEmailAsReplyTo();
        assertNotNull(item.getEmail());
        assertNull(item.getEmail().getReplyTo());

    }

    @Test
    public void fromEmailShouldSetToPersonalEmailIfNoIssueInContext() throws Exception
    {
        RenderingMailQueueItem item = (RenderingMailQueueItem) builder.buildQueueItemUsingProjectEmailAsReplyTo();
        assertNotNull(item.getEmail());
        assertThat(item.getEmail().getFrom(), is(PERSONAL_EMAIL));

    }

    @Test
    public void fromEmailShouldSetToProjectEmailIfIssueInContext() throws Exception
    {
        context.put(ISSUE, issue);
        RenderingMailQueueItem item = (RenderingMailQueueItem) builder.buildQueueItemUsingProjectEmailAsReplyTo();
        assertNotNull(item.getEmail());
        assertThat(item.getEmail().getFrom(), is(PROJECT_EMAIL));
    }

    private void setUpIssue()
    {
        when(project.getEmail()).thenReturn(PROJECT_EMAIL);
        when(issue.getProjectObject()).thenReturn(project);
    }
    //We have to mock these dependencies because they're looked up statically in Email and EmailBuilder classes.
    private void mockStaticDependencies()
    {
        MockComponentWorker componentWorker = new MockComponentWorker();
        componentWorker.registerMock(JiraApplicationContext.class, jiraApplicationContext);
        componentWorker.registerMock(UserLocaleStore.class, userLocaleStore);
        when(templateContextFactory.getTemplateContext(Matchers.any(Locale.class))).thenReturn(templateContext);
        when(templateContextFactory.getTemplateContext(Matchers.any(Locale.class), Matchers.any(IssueEvent.class))).thenReturn(templateContext);
        componentWorker.registerMock(TemplateContextFactory.class, templateContextFactory);
        ComponentAccessor.initialiseWorker(componentWorker);
    }
}