package com.atlassian.jira.issue.attachment;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestAttachmentKeyMapper
{

    private final AttachmentKeyMapper attachmentKeyMapper = new AttachmentKeyMapper();

    @Test
    public void testMapAttachmentObjectToAttachmentKey() throws Exception
    {
        final Attachment attachment = mock(Attachment.class);
        final String fileName = "fileName";
        final long attachmentId = 12145l;
        final String issueKey = "FOO-1";
        final String originalProjectKey = "FOO";

        when(attachment.getFilename()).thenReturn(fileName);
        when(attachment.getId()).thenReturn(attachmentId);

        final Issue mockIssue = mock(Issue.class);
        final Project project = mock(Project.class);

        when(attachment.getIssueObject()).thenReturn(mockIssue);
        when(mockIssue.getKey()).thenReturn(issueKey);
        when(mockIssue.getProjectObject()).thenReturn(project);
        when(project.getOriginalKey()).thenReturn(originalProjectKey);

        final AttachmentKey attachmentKey = attachmentKeyMapper.fromAttachment(attachment);

        assertThat(attachmentKey.getAttachmentFilename(), equalTo(fileName));
        assertThat(attachmentKey.getAttachmentId(), equalTo(attachmentId));
        assertThat(attachmentKey.getIssueKey(), equalTo(issueKey));
        assertThat(attachmentKey.getProjectKey(), equalTo(originalProjectKey));
    }
}