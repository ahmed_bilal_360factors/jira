package com.atlassian.jira.auditing.handlers;

import com.atlassian.fugue.Option;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.permission.PermissionAddedEvent;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.PermissionTypeManager;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.security.type.SecurityType;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/*
 * @since v6.3
 */
@RunWith (MockitoJUnitRunner.class)
public class PermissionChangeHandlerImplTest
{
    @Mock
    private PermissionSchemeManager permissionSchemeManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private PermissionTypeManager permissionTypeManager;

    @Mock
    private I18nHelper.BeanFactory beanFactory;

    @Mock
    private SchemeEntity schemeEntity;

    @Mock
    private I18nHelper i18Helper;

    @Mock
    private SecurityType securityType;

    private PermissionChangeHandlerImpl permissionChangeHandler;

    @Before
    public void setUp()
    {
        ComponentAccessor.initialiseWorker(new MockComponentWorker().addMock(I18nHelper.BeanFactory.class, beanFactory));

        when(beanFactory.getInstance(Locale.ENGLISH)).thenReturn(i18Helper);
        when(i18Helper.getText(anyString())).thenReturn("some text value");
        when(permissionManager.getProjectPermission(any(ProjectPermissionKey.class))).thenReturn(Option.<ProjectPermission>none());
        when(permissionTypeManager.getSchemeType("scheme type")).thenReturn(securityType);
        when(securityType.getDisplayName()).thenReturn("some name");

        permissionChangeHandler = new PermissionChangeHandlerImpl(
                permissionSchemeManager, permissionManager, permissionTypeManager, beanFactory);
    }


    @Test
    public void schemeEntityTypeIdCanBeInteger()
    {
        permissionChangeHandler.computeChangedValues(
                new PermissionAddedEvent(1L, new SchemeEntity("scheme type", 1)));
    }

    @Test
    public void schemeEntityTypeIdCanBeLong()
    {
        permissionChangeHandler.computeChangedValues(
                new PermissionAddedEvent(1L, new SchemeEntity("scheme type", 1L)));
    }

    @Test
    public void schemeEntityTypeIdCanBeString()
    {
        permissionChangeHandler.computeChangedValues(
                new PermissionAddedEvent(1L, new SchemeEntity("scheme type", "a permission name")));
    }
}
