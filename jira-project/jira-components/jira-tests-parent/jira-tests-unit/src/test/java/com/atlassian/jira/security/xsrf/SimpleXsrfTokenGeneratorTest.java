package com.atlassian.jira.security.xsrf;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.jira.bc.license.JiraServerIdProvider;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.mock.servlet.MockHttpServletResponse;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.util.velocity.HttpSessionBackedVelocityRequestSession;
import com.atlassian.jira.util.velocity.SimpleVelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.web.ExecutingHttpRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 */
@RunWith(MockitoJUnitRunner.class)
public class SimpleXsrfTokenGeneratorTest
{
    private Map<String, String> cookieMap;
    private HttpServletRequest servletRequest;
    @Mock private JiraAuthenticationContext authenticationContext;
    private MockUser user;
    @Mock private JiraServerIdProvider serverIdProvider;
    
    @InjectMocks
    SimpleXsrfTokenGenerator xsrfTokenGenerator;

    @Before
    public void setUp() throws Exception
    {
        user = new MockUser("fred");

        cookieMap = new HashMap<String, String>();
        servletRequest = newServletRequest(cookieMap);
        HttpServletResponse servletResponse = newServletResponse(cookieMap);
        ExecutingHttpRequest.set(servletRequest, servletResponse);
    }

    @After
    public void tearDown() throws Exception
    {
        ExecutingHttpRequest.clear();
    }

    @Test
    public void testGenerateTokenFromHttpRequest()
    {
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
        when(serverIdProvider.getServerId()).thenReturn("F*CK3R");
        String tokenGen1 = xsrfTokenGenerator.generateToken(servletRequest);
        // the second time around we should have one and hence not get another
        String tokenGen2 = xsrfTokenGenerator.generateToken(servletRequest);

        assertTokensAreKosher(xsrfTokenGenerator, tokenGen1, tokenGen2);

    }

    @Test
    public void generateTokenFromHttpRequestDoesNotExplodeIfTokenIsEmpty()
    {
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
        when(serverIdProvider.getServerId()).thenReturn("F*CK3R");
        cookieMap.put("atlassian.xsrf.token", "");

        xsrfTokenGenerator.generateToken(servletRequest);
    }

    @Test
    public void testGenerateTokenFromHttpRequestNoCreate()
    {
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
        when(serverIdProvider.getServerId()).thenReturn("F*CK3R");
        String token = xsrfTokenGenerator.generateToken(servletRequest, false);

        assertNull(token);
    }

    @Test
    public void testGenerateTokenFromThreadLocalHttpRequest()
    {
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
        when(serverIdProvider.getServerId()).thenReturn("F*6CK3R");
        String tokenGen1 = xsrfTokenGenerator.generateToken();
        // the second time around we should have one and hence not get another
        String tokenGen2 = xsrfTokenGenerator.generateToken();

        assertTokensAreKosher(xsrfTokenGenerator, tokenGen1, tokenGen2);
    }

    @Test
    public void testGenerateTokenFromThreadLocalHttpRequestNoCreate()
    {
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
        when(serverIdProvider.getServerId()).thenReturn("F*CK3R");
        String token = xsrfTokenGenerator.generateToken(false);

        assertNull(token);
    }

    @Test
    public void testGenerateTokenFromVelocityContext()
    {
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
        when(serverIdProvider.getServerId()).thenReturn("F*CK3R");

        VelocityRequestContext context = new SimpleVelocityRequestContext(null, null, null, new HttpSessionBackedVelocityRequestSession(servletRequest));
        String tokenGen1 = xsrfTokenGenerator.generateToken(context);
        // the second time around we should have one and hence not get another
        String tokenGen2 = xsrfTokenGenerator.generateToken(context);

        assertTokensAreKosher(xsrfTokenGenerator, tokenGen1, tokenGen2);
    }

    @Test
    public void testGenerateTokenWithNullRequest()
    {
        ExecutingHttpRequest.clear();

        final String token = xsrfTokenGenerator.generateToken();
        assertNull(token);
    }

    @Test
    public void testGetXsrfTokenName()
    {
        assertEquals("atlassian.xsrf.token", xsrfTokenGenerator.getXsrfTokenName());
    }

    @Test
    public void testValidateToken()
    {
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
        when(serverIdProvider.getServerId()).thenReturn("F*CK3R");

        String tokenGen1 = xsrfTokenGenerator.generateToken(servletRequest);

        assertFalse(xsrfTokenGenerator.validateToken(servletRequest, tokenGen1 + "1"));
        assertFalse(xsrfTokenGenerator.validateToken(servletRequest, tokenGen1 + "abc"));
        assertFalse(xsrfTokenGenerator.validateToken(servletRequest, "abc123"));
        assertFalse(xsrfTokenGenerator.validateToken(servletRequest, null));
        assertFalse(xsrfTokenGenerator.validateToken(servletRequest, ""));

        assertTrue(xsrfTokenGenerator.validateToken(servletRequest, tokenGen1));
    }

    @Test
    public void testWasGeneratedByAuthLoggedIn()
    {
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
        when(serverIdProvider.getServerId()).thenReturn("F*CK3R");

        final String token = xsrfTokenGenerator.generateToken();
        assertTrue(xsrfTokenGenerator.generatedByAuthenticatedUser(token));
    }

    @Test
    public void testWasGeneratedByAuthLoggedOut()
    {
        when(authenticationContext.getLoggedInUser()).thenReturn(null);
        when(serverIdProvider.getServerId()).thenReturn("F*CK3R");

        final String token = xsrfTokenGenerator.generateToken();
        assertFalse(xsrfTokenGenerator.generatedByAuthenticatedUser(token));
    }

    @Test
    public void getGetTokenReturnsNull()
    {
        assertNull(xsrfTokenGenerator.getToken(servletRequest));
    }

    @Test
    public void getGetTokenReturnsLastGenerated()
    {
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
        when(serverIdProvider.getServerId()).thenReturn("F*CK3R");

        final String token = xsrfTokenGenerator.generateToken();

        assertEquals(token, xsrfTokenGenerator.getToken(servletRequest));
    }

    private void assertTokensAreKosher(XsrfTokenGenerator xsrfTokenGenerator, String tokenGen1, String tokenGen2)
    {
        assertNotNull(tokenGen1);

        Object token1 = cookieMap.get(xsrfTokenGenerator.getXsrfTokenName());
        assertNotNull(token1);
        assertEquals(tokenGen1, token1);

        assertNotNull(tokenGen2);
        assertEquals(tokenGen1, tokenGen2);

        Object token2 = cookieMap.get(xsrfTokenGenerator.getXsrfTokenName());
        assertNotNull(token2);
        assertEquals(tokenGen1, token2);
        assertEquals(tokenGen2, token2);
    }


    private MockHttpServletRequest newServletRequest(final Map<String, String> cookieMap)
    {
        return new MockHttpServletRequest()
        {
            @Override
            public Cookie[] getCookies()
            {
                return this.toCookies(cookieMap);
            }
        };
    }

    private HttpServletResponse newServletResponse(final Map<String, String> cookieMap)
    {
        return new MockHttpServletResponse()
        {
            @Override
            public void addCookie(Cookie cookie)
            {
                cookieMap.put(cookie.getName(), cookie.getValue());
            }
        };
    }
}
