package com.atlassian.jira.onboarding.postsetup;

import com.atlassian.fugue.Option;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class PostSetupAnnouncementsTest
{
    public static final String ACTIVITY_1 = "activity1";
    public static final String ACTIVITY_2 = "activity2";
    public static final String ACTIVITY_3 = "activity3";
    @InjectMocks
    PostSetupAnnouncements testObj;

    @Mock
    PostSetupAnnouncementStore postSetupAnnouncementStore;

    @Mock
    PostSetupAnnouncementProvider postSetupAnnouncementProvider;

    @Mock
    AnnouncementStatusChecker announceChecker;

    @Mock
    AnnouncementStatusChecker announcedChecker;

    @Mock
    AnnouncementAccessPolicy announcementAccessPolicy;

    @Mock
    private com.atlassian.jira.onboarding.postsetup.AnnouncementFilter permitAllFilter;

    ApplicationUser thisUserCanRead = new MockApplicationUser("thisUserCanRead");
    ApplicationUser thisUserCanWrite = new MockApplicationUser("thisUserCanWrite");

    @Before
    public void setUp() throws Exception
    {
        when(announceChecker.computeStatus()).thenReturn(PostSetupAnnouncementStatus.Status.ANNOUNCE);

        when(announcementAccessPolicy.canRead(Mockito.eq(Option.some(thisUserCanRead)))).thenReturn(true);
        when(announcementAccessPolicy.canUpdate(Mockito.eq(Option.some(thisUserCanRead)))).thenReturn(false);

        when(announcementAccessPolicy.canRead(Mockito.eq(Option.some(thisUserCanWrite)))).thenReturn(false);
        when(announcementAccessPolicy.canUpdate(Mockito.eq(Option.some(thisUserCanWrite)))).thenReturn(true);
        when(announcementAccessPolicy.getAnnouncementFilter()).thenReturn(permitAllFilter);
        when(permitAllFilter.canUserSeeAnnouncement(Mockito.any(Option.class), Mockito.any(String.class))).thenReturn(true);
    }


    @Test
    public void shouldCopyCurrentStatusesToEmptyStore()
    {
        // given
        when(postSetupAnnouncementStore.getAllStatuses()).thenReturn(Collections.<PostSetupAnnouncementStatus>emptyList());

        when(postSetupAnnouncementProvider.getAnouncements()).thenReturn(
                Lists.newArrayList(
                        new PostSetupAnnouncement(ACTIVITY_1, announceChecker),
                        new PostSetupAnnouncement(ACTIVITY_2, announceChecker))
        );

        // when
        testObj.recalculateStatuses(Option.some(thisUserCanWrite));

        // then
        verify(postSetupAnnouncementStore).updateStatuses(argThat(Matchers.containsInAnyOrder(
                new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.ANNOUNCE),
                new PostSetupAnnouncementStatus(ACTIVITY_2, PostSetupAnnouncementStatus.Status.ANNOUNCE)
        )));
    }

    @Test (expected = SecurityException.class)
    public void shouldDenyStatusUpdateForUserWithoutAccess()
    {
        // given
        when(postSetupAnnouncementStore.getAllStatuses()).thenReturn(Collections.<PostSetupAnnouncementStatus>emptyList());

        when(postSetupAnnouncementProvider.getAnouncements()).thenReturn(
                Lists.newArrayList(
                        new PostSetupAnnouncement(ACTIVITY_1, announceChecker),
                        new PostSetupAnnouncement(ACTIVITY_2, announceChecker))
        );

        // when
        testObj.recalculateStatuses(Option.some(thisUserCanRead));

        // then - SecurityException
    }

    @Test
    public void shouldDoNotMakeStatusUpdateForUserWithoutAccess()
    {
        // given
        when(postSetupAnnouncementStore.getAllStatuses()).thenReturn(Collections.<PostSetupAnnouncementStatus>emptyList());

        when(postSetupAnnouncementProvider.getAnouncements()).thenReturn(
                Lists.newArrayList(
                        new PostSetupAnnouncement(ACTIVITY_1, announceChecker),
                        new PostSetupAnnouncement(ACTIVITY_2, announceChecker))
        );

        // when
        try
        {
            testObj.recalculateStatuses(Option.some(thisUserCanRead));
        }
        catch (SecurityException x)
        {
        }

        // then
        verify(postSetupAnnouncementStore, Mockito.never()).updateStatuses(Mockito.<Iterable<PostSetupAnnouncementStatus>>any());
    }

    @Test
    public void shouldIgnoreStatusOfPreviouslyAnnouncedActivities()
    {
        // given
        final ArrayList<PostSetupAnnouncementStatus> activity1Announced =
                Lists.newArrayList(new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.ANNOUNCED));
        when(postSetupAnnouncementStore.getAllStatuses()).thenReturn(activity1Announced);

        AnnouncementStatusChecker anouncedChecker = mock(AnnouncementStatusChecker.class);
        when(anouncedChecker.computeStatus()).thenReturn(null);

        when(postSetupAnnouncementProvider.getAnouncements()).thenReturn(
                Lists.newArrayList(
                        new PostSetupAnnouncement(ACTIVITY_1, anouncedChecker),
                        new PostSetupAnnouncement(ACTIVITY_2, announceChecker))
        );

        // when
        testObj.recalculateStatuses(Option.some(thisUserCanWrite));

        // then
        verify(anouncedChecker, never()).computeStatus();
        verify(postSetupAnnouncementStore).updateStatuses(argThat(contains(
                new PostSetupAnnouncementStatus(ACTIVITY_2, PostSetupAnnouncementStatus.Status.ANNOUNCE)
        )));
    }

    @Test
    public void shouldIgnoreStatusOfPreviouslyFullfilledActivities()
    {
        // given
        final ArrayList<PostSetupAnnouncementStatus> activity1Announced =
                Lists.newArrayList(new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.FULLFILLED));
        when(postSetupAnnouncementStore.getAllStatuses()).thenReturn(activity1Announced);

        AnnouncementStatusChecker anouncedChecker = mock(AnnouncementStatusChecker.class);
        when(anouncedChecker.computeStatus()).thenReturn(null);

        when(postSetupAnnouncementProvider.getAnouncements()).thenReturn(
                Lists.newArrayList(
                        new PostSetupAnnouncement(ACTIVITY_1, anouncedChecker),
                        new PostSetupAnnouncement(ACTIVITY_2, announceChecker))
        );

        // when
        testObj.recalculateStatuses(Option.some(thisUserCanWrite));

        // then
        verify(anouncedChecker, never()).computeStatus();
        verify(postSetupAnnouncementStore).updateStatuses(argThat(contains(
                new PostSetupAnnouncementStatus(ACTIVITY_2, PostSetupAnnouncementStatus.Status.ANNOUNCE)
        )));
    }

    @Test
    public void shouldUpdateStatusOfAnnouncedActivity()
    {
        // given
        when(postSetupAnnouncementProvider.getAnouncements()).thenReturn(
                Lists.newArrayList(
                        new PostSetupAnnouncement(ACTIVITY_1, announceChecker),
                        new PostSetupAnnouncement(ACTIVITY_2, announceChecker))
        );
        // when
        testObj.activitiesAnnounced(Option.some(thisUserCanWrite), Lists.newArrayList(ACTIVITY_1, ACTIVITY_2));

        // then
        verify(postSetupAnnouncementStore).updateStatuses(argThat(Matchers.containsInAnyOrder(
                new PostSetupAnnouncementStatus(ACTIVITY_2, PostSetupAnnouncementStatus.Status.ANNOUNCED),
                new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.ANNOUNCED)
        )));
    }

    @Test (expected = SecurityException.class)
    public void shouldDenyStatusUpdateWhenUserHasNoAccess()
    {
        // given
        when(postSetupAnnouncementProvider.getAnouncements()).thenReturn(
                Lists.newArrayList(
                        new PostSetupAnnouncement(ACTIVITY_1, announceChecker),
                        new PostSetupAnnouncement(ACTIVITY_2, announceChecker))
        );
        // when
        testObj.activitiesAnnounced(Option.some(thisUserCanRead), Lists.newArrayList(ACTIVITY_1, ACTIVITY_2));
    }

    @Test
    public void shouldDoNotMakeAnyChangesWhenUserHasNoAccess()
    {
        // given
        when(postSetupAnnouncementProvider.getAnouncements()).thenReturn(
                Lists.newArrayList(
                        new PostSetupAnnouncement(ACTIVITY_1, announceChecker),
                        new PostSetupAnnouncement(ACTIVITY_2, announceChecker))
        );
        // when
        try
        {
            testObj.activitiesAnnounced(Option.some(thisUserCanRead), Lists.newArrayList(ACTIVITY_1, ACTIVITY_2));
        }
        catch (SecurityException x)
        {
        }

        // then
        verify(postSetupAnnouncementStore, Mockito.never()).updateStatuses(Mockito.<Iterable<PostSetupAnnouncementStatus>>any());
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void shouldFailFastWhenNotExistingStatusIsUpdated()
    {
        // given
        when(postSetupAnnouncementProvider.getAnouncements()).thenReturn(
                Lists.newArrayList(
                        new PostSetupAnnouncement(ACTIVITY_1, announceChecker),
                        new PostSetupAnnouncement(ACTIVITY_2, announceChecker))
        );

        // when
        expectedEx.expect(NoSuchElementException.class);
        expectedEx.expectMessage(Matchers.containsString(ACTIVITY_3));

        testObj.activitiesAnnounced(Option.some(thisUserCanWrite), Lists.newArrayList(ACTIVITY_3));
    }

    @Test
    public void shouldGetStatusesReadyToBeAnnonced()
    {
        // given
        final ArrayList<PostSetupAnnouncementStatus> activities =
                Lists.newArrayList(
                        new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.ANNOUNCED),
                        new PostSetupAnnouncementStatus(ACTIVITY_2, PostSetupAnnouncementStatus.Status.AWAITS),
                        new PostSetupAnnouncementStatus(ACTIVITY_3, PostSetupAnnouncementStatus.Status.ANNOUNCE),
                        new PostSetupAnnouncementStatus("activity4", PostSetupAnnouncementStatus.Status.FULLFILLED)
                );
        when(postSetupAnnouncementStore.getAllStatuses()).thenReturn(activities);

        // when
        final List<PostSetupAnnouncementStatus> unannoncedActivites =
                testObj.getReadyAnnouncements(Option.some(thisUserCanRead));

        //then
        assertThat(
                unannoncedActivites,
                contains(
                        new PostSetupAnnouncementStatus(ACTIVITY_3, PostSetupAnnouncementStatus.Status.ANNOUNCE)
                )
        );
    }

    @Test
    public void shouldObeyAnnouncementFilter()
    {
        // given
        final ArrayList<PostSetupAnnouncementStatus> activities =
                Lists.newArrayList(
                        new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.ANNOUNCE),
                        new PostSetupAnnouncementStatus(ACTIVITY_2, PostSetupAnnouncementStatus.Status.ANNOUNCE)
                );
        when(postSetupAnnouncementStore.getAllStatuses()).thenReturn(activities);
        Mockito.reset(announcementAccessPolicy);
        when(announcementAccessPolicy.canRead(Mockito.any(Option.class))).thenReturn(true);
        final AnnouncementFilter filter = mock(AnnouncementFilter.class);
        when(filter.canUserSeeAnnouncement(Option.some(thisUserCanRead), ACTIVITY_1)).thenReturn(true);
        when(filter.canUserSeeAnnouncement(Option.some(thisUserCanRead), ACTIVITY_2)).thenReturn(false);

        when(announcementAccessPolicy.getAnnouncementFilter()).thenReturn(filter);

        // when
        final List<PostSetupAnnouncementStatus> unannoncedActivites =
                testObj.getReadyAnnouncements(Option.some(thisUserCanRead));

        //then
        assertThat(
                unannoncedActivites,
                contains(
                        new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.ANNOUNCE)
                )
        );
    }

    @Test
    public void shouldShowEmptyStatusesListWhenUserCannotRead()
    {
        // given
        final ArrayList<PostSetupAnnouncementStatus> activities =
                Lists.newArrayList(
                        new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.ANNOUNCED),
                        new PostSetupAnnouncementStatus(ACTIVITY_2, PostSetupAnnouncementStatus.Status.AWAITS),
                        new PostSetupAnnouncementStatus(ACTIVITY_3, PostSetupAnnouncementStatus.Status.ANNOUNCE),
                        new PostSetupAnnouncementStatus("activity4", PostSetupAnnouncementStatus.Status.FULLFILLED)
                );
        when(postSetupAnnouncementStore.getAllStatuses()).thenReturn(activities);

        // when
        final List<PostSetupAnnouncementStatus> unannoncedActivites =
                testObj.getReadyAnnouncements(Option.some(thisUserCanWrite));

        //then
        assertThat(
                unannoncedActivites,
                Matchers.<PostSetupAnnouncementStatus>empty()
        );
    }

    @Test
    public void shouldNotInformAboutReadyAnnouncementsWhenThereIsNothingInAnnouncedState()
    {
        // given
        final ArrayList<PostSetupAnnouncementStatus> activities =
                Lists.newArrayList(
                        new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.ANNOUNCED),
                        new PostSetupAnnouncementStatus(ACTIVITY_2, PostSetupAnnouncementStatus.Status.AWAITS),
                        new PostSetupAnnouncementStatus("activity4", PostSetupAnnouncementStatus.Status.FULLFILLED)
                );
        when(postSetupAnnouncementStore.getAllStatuses()).thenReturn(activities);

        final boolean hasAnnouncements = testObj.hasAnnouncements(Option.some(thisUserCanRead));

        assertThat("There are no announcements", hasAnnouncements, is(false));
    }

    @Test
    public void shouldInformAboutReadyAnnouncementsWhenTherIsSthInAnnouncedState()
    {
        // given
        final ArrayList<PostSetupAnnouncementStatus> activities =
                Lists.newArrayList(
                        new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.ANNOUNCED),
                        new PostSetupAnnouncementStatus(ACTIVITY_2, PostSetupAnnouncementStatus.Status.AWAITS),
                        new PostSetupAnnouncementStatus(ACTIVITY_3, PostSetupAnnouncementStatus.Status.ANNOUNCE),
                        new PostSetupAnnouncementStatus("activity4", PostSetupAnnouncementStatus.Status.FULLFILLED)
                );
        when(postSetupAnnouncementStore.getAllStatuses()).thenReturn(activities);

        final boolean hasAnnouncements = testObj.hasAnnouncements(Option.some(thisUserCanRead));

        assertThat("There should be announcements", hasAnnouncements, is(true));
    }

    @Test
    public void shouldUserWithoutAccessAlwaysSeeThatThereAreNoAnnouncements()
    {
        // given
        final ArrayList<PostSetupAnnouncementStatus> activities =
                Lists.newArrayList(
                        new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.ANNOUNCED),
                        new PostSetupAnnouncementStatus(ACTIVITY_2, PostSetupAnnouncementStatus.Status.AWAITS),
                        new PostSetupAnnouncementStatus(ACTIVITY_3, PostSetupAnnouncementStatus.Status.ANNOUNCE),
                        new PostSetupAnnouncementStatus("activity4", PostSetupAnnouncementStatus.Status.FULLFILLED)
                );
        when(postSetupAnnouncementStore.getAllStatuses()).thenReturn(activities);

        final boolean hasAnnouncements = testObj.hasAnnouncements(Option.some(thisUserCanWrite));

        assertThat("There should be no announcements", hasAnnouncements, is(false));
    }

    @Test
    public void resetShouldSetStatusOffAllActivitiesToAnnounce()
    {
        // given
        when(postSetupAnnouncementProvider.getAnouncements()).thenReturn(
                Lists.newArrayList(
                        new PostSetupAnnouncement(ACTIVITY_1, announceChecker),
                        new PostSetupAnnouncement(ACTIVITY_2, announceChecker))
        );
        // when
        testObj.reset(Option.some(thisUserCanWrite));

        // then
        verify(postSetupAnnouncementStore).updateStatuses(argThat(Matchers.containsInAnyOrder(
                new PostSetupAnnouncementStatus(ACTIVITY_2, PostSetupAnnouncementStatus.Status.AWAITS),
                new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.AWAITS)
        )));
    }

    @Test
    public void resetShouldDoNotMakeAnyChangesWhenUserHasNoAccess()
    {
        // given
        when(postSetupAnnouncementProvider.getAnouncements()).thenReturn(
                Lists.newArrayList(
                        new PostSetupAnnouncement(ACTIVITY_1, announceChecker),
                        new PostSetupAnnouncement(ACTIVITY_2, announceChecker))
        );
        // when
        try
        {
            testObj.reset(Option.some(thisUserCanRead));
        }
        catch (SecurityException e)
        {
        }

        // then
        verify(postSetupAnnouncementStore, Mockito.never())
                .updateStatuses(Mockito.<Iterable<PostSetupAnnouncementStatus>>any());
    }

    @Test (expected = SecurityException.class)
    public void resetShouldThrowWhenUserHasNoAccess()
    {
        // given
        when(postSetupAnnouncementProvider.getAnouncements()).thenReturn(
                Lists.newArrayList(
                        new PostSetupAnnouncement(ACTIVITY_1, announceChecker),
                        new PostSetupAnnouncement(ACTIVITY_2, announceChecker))
        );
        // when
        testObj.reset(Option.some(thisUserCanRead));
    }
}