package com.atlassian.jira.onboarding.postsetup.checks;

import java.util.Date;

import com.atlassian.core.util.Clock;
import com.atlassian.extras.api.jira.JiraLicense;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;

import com.google.common.collect.ImmutableList;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class EvaluationLicenseAgeCheckTest
{
    @Mock
    JiraLicenseManager licenseManager;

    @Mock
    Clock clock;

    @InjectMocks
    EvaluationLicenseAgeCheck testObj;

    @Mock
    private LicenseDetails nonEvaluationLicense;

    @Mock
    private LicenseDetails evaluationLicense;

    @Mock
    private JiraLicense jiraEvaluationLicense;

    private Date now;

    @Before
    public void setUp()
    {
        now = new DateTime(2000, 1, 1, 0, 0).toDate();

        when(nonEvaluationLicense.isEvaluation()).thenReturn(false);
        when(evaluationLicense.isEvaluation()).thenReturn(true);
        when(evaluationLicense.getJiraLicense()).thenReturn(jiraEvaluationLicense);
        when(jiraEvaluationLicense.getPurchaseDate()).thenReturn(now);
    }

    @Test
    public void shouldSkipCheckOfNonEvaluationLicenses()
    {
        // given
        final ImmutableList<LicenseDetails> nonEvalutaionLicense = ImmutableList.of(
                nonEvaluationLicense
        );
        when(licenseManager.getLicenses()).thenReturn(nonEvalutaionLicense);

        // when
        final EvaluationLicenseAgeCheck.LicenseChecker currentLicenseCheck = testObj.getCurrentLicenseCheck();
        final boolean licenseOld = currentLicenseCheck.isEvaluationLicenseOld(Period.days(1));

        // then
        assertThat("Non evaluation license should be always ignored", licenseOld, is(false));
    }

    @Test
    public void shouldDetectNonEvaluationLicense()
    {
        // given
        final ImmutableList<LicenseDetails> nonEvalutaionLicenses = ImmutableList.of(
                nonEvaluationLicense
        );
        when(licenseManager.getLicenses()).thenReturn(nonEvalutaionLicenses);

        // when
        final EvaluationLicenseAgeCheck.LicenseChecker currentLicenseCheck = testObj.getCurrentLicenseCheck();
        final boolean isEvaluation = currentLicenseCheck.isEvaluation();

        // then
        assertThat("This is non evaluation license.", isEvaluation, is(false));
    }

    @Test
    public void shouldDetectEvaluationLicense()
    {
        // given
        final ImmutableList<LicenseDetails> evalutaionLicenses = ImmutableList.of(
                evaluationLicense
        );
        when(licenseManager.getLicenses()).thenReturn(evalutaionLicenses);

        // when
        final EvaluationLicenseAgeCheck.LicenseChecker currentLicenseCheck = testObj.getCurrentLicenseCheck();
        final boolean isEvaluation = currentLicenseCheck.isEvaluation();

        // then
        assertThat("This is evaluation license.", isEvaluation, is(true));
    }

    @Test
    public void shouldIgnoreEvaluationLicensesWhenNonEvaluationArePresent()
    {
        // given
        final ImmutableList<LicenseDetails> licenses = ImmutableList.of(
                nonEvaluationLicense,
                evaluationLicense
        );
        when(licenseManager.getLicenses()).thenReturn(licenses);
        when(clock.getCurrentDate()).thenReturn(new DateTime(2100, 1, 1, 0, 0).toDate());

        // when
        final EvaluationLicenseAgeCheck.LicenseChecker currentLicenseCheck = testObj.getCurrentLicenseCheck();
        final boolean licenseOld = currentLicenseCheck.isEvaluationLicenseOld(Period.days(1));

        // then
        assertThat("Non evaluation license should be always ignored", licenseOld, is(false));
    }

    @Test
    public void shouldReportTwoDaysOldEvaluationLicense()
    {
        // given
        final ImmutableList<LicenseDetails> licenses = ImmutableList.of(
                evaluationLicense
        );
        when(licenseManager.getLicenses()).thenReturn(licenses);
        when(clock.getCurrentDate()).thenReturn(new DateTime(2000, 1, 2, 0, 1).toDate());

        // when
        final EvaluationLicenseAgeCheck.LicenseChecker currentLicenseCheck = testObj.getCurrentLicenseCheck();
        final boolean licenseOld = currentLicenseCheck.isEvaluationLicenseOld(Period.days(1));

        // then
        assertThat("Two days evaluation license have to be one day old!", licenseOld, is(true));
    }

    @Test
    public void shouldNotReport23HoursOldEvaluationLicense()
    {
        // given
        final ImmutableList<LicenseDetails> licenses = ImmutableList.of(
                evaluationLicense
        );
        when(licenseManager.getLicenses()).thenReturn(licenses);
        when(clock.getCurrentDate()).thenReturn(new DateTime(2000, 1, 1, 23, 0).toDate());

        // when
        final EvaluationLicenseAgeCheck.LicenseChecker currentLicenseCheck = testObj.getCurrentLicenseCheck();
        final boolean licenseOld = currentLicenseCheck.isEvaluationLicenseOld(Period.days(1));

        // then
        assertThat("23hours evaluation license have not to be one day old!", licenseOld, is(false));
    }
}