package com.atlassian.jira.onboarding.postsetup;

import java.util.Collections;
import java.util.List;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.mock.MockApplicationProperties;

import com.google.common.collect.ImmutableList;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;

public class AppPropertiesAnnouncementStoreTest
{
    public static final String ACTIVITY_1 = "activity1";
    public static final String ACTIVITY_2 = "activity2";
    private ApplicationProperties applicationProperties;
    private AppPropertiesAnnouncementStore testObj;

    @Before
    public void setUp() throws Exception
    {
        applicationProperties = new MockApplicationProperties();
        testObj = new AppPropertiesAnnouncementStore(applicationProperties);
    }

    @Test
    public void shouldReturnEmptyPropertyListInitially()
    {
        // when
        final List<PostSetupAnnouncementStatus> allStatuses = testObj.getAllStatuses();

        // expect
        assertThat(allStatuses, Matchers.<PostSetupAnnouncementStatus>emptyIterable());
    }

    @Test
    public void shouldKeepUpdatedStatuses()
    {
        // given
        final PostSetupAnnouncementStatus status1 = new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.AWAITS);
        final PostSetupAnnouncementStatus status2 = new PostSetupAnnouncementStatus(ACTIVITY_2, PostSetupAnnouncementStatus.Status.AWAITS);

        // when
        testObj.updateStatuses(
                ImmutableList.of(
                        status1,
                        status2
                )
        );
        final List<PostSetupAnnouncementStatus> allStatuses = testObj.getAllStatuses();

        // then
        assertThat(allStatuses, containsInAnyOrder(status1, status2));
    }

    @Test
    public void shouldOverwriteStatuses()
    {
        //given
        final PostSetupAnnouncementStatus status1 = new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.AWAITS);
        final PostSetupAnnouncementStatus status2 = new PostSetupAnnouncementStatus(ACTIVITY_2, PostSetupAnnouncementStatus.Status.AWAITS);
        testObj.updateStatuses(
                ImmutableList.of(
                        status1,
                        status2
                )
        );

        // when
        final PostSetupAnnouncementStatus status1_1 = new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.ANNOUNCED);
        testObj.updateStatuses(
                ImmutableList.of(
                        status1_1
                )
        );

        final List<PostSetupAnnouncementStatus> allStatuses = testObj.getAllStatuses();
        assertThat(allStatuses, containsInAnyOrder(status1_1, status2));
    }

    @Test
    public void shouldKeepNotModifiedStatueses()
    {
        //given
        final PostSetupAnnouncementStatus status1 = new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.AWAITS);
        final PostSetupAnnouncementStatus status2 = new PostSetupAnnouncementStatus(ACTIVITY_2, PostSetupAnnouncementStatus.Status.AWAITS);
        testObj.updateStatuses(
                ImmutableList.of(
                        status1,
                        status2
                )
        );

        // when
        final PostSetupAnnouncementStatus status1_1 = new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.ANNOUNCED);
        testObj.updateStatuses(
                Collections.<PostSetupAnnouncementStatus>emptyList()
        );

        final List<PostSetupAnnouncementStatus> allStatuses = testObj.getAllStatuses();
        assertThat(allStatuses, containsInAnyOrder(status1, status2));
    }

    @Test
    public void shouldStorePropertiesInUnderlyingPersitentStore() {
        // given
        final PostSetupAnnouncementStatus status1 = new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.AWAITS);
        final PostSetupAnnouncementStatus status2 = new PostSetupAnnouncementStatus(ACTIVITY_2, PostSetupAnnouncementStatus.Status.AWAITS);
        testObj.updateStatuses(
                ImmutableList.of(
                        status1,
                        status2
                )
        );

        // when
        AppPropertiesAnnouncementStore newStoreWithTheSameProps = new AppPropertiesAnnouncementStore(applicationProperties);
        final List<PostSetupAnnouncementStatus> allStatuses = newStoreWithTheSameProps.getAllStatuses();

        // then
        assertThat(allStatuses, containsInAnyOrder(status1, status2));
    }

    @Test
    public void shouldTryToRecoverDestroyedProperties() {
        // given
        final PostSetupAnnouncementStatus status1 = new PostSetupAnnouncementStatus(ACTIVITY_1, PostSetupAnnouncementStatus.Status.AWAITS);
        testObj.updateStatuses(
                ImmutableList.of(
                        status1
                )
        );
        // now destroy all properties
        for( String key: applicationProperties.getKeys() ) {
            applicationProperties.setText( key, "a=3=4;;;novalue;=AWAITS;"+applicationProperties.getString(key) );
        }

        // when
        AppPropertiesAnnouncementStore newStoreWithTheSameProps = new AppPropertiesAnnouncementStore(applicationProperties);
        final List<PostSetupAnnouncementStatus> allStatuses = newStoreWithTheSameProps.getAllStatuses();

        // then
        assertThat(allStatuses, containsInAnyOrder(status1));
    }
}