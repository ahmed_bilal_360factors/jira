package com.atlassian.jira.onboarding.postsetup.checks;

import com.atlassian.jira.onboarding.postsetup.PostSetupAnnouncementStatus;
import org.joda.time.Period;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class AdminAccountCredentialsCheckerTest
{

    @InjectMocks
    AdminAccountCredentialsChecker testObj;

    @Mock
    EvaluationLicenseAgeCheck licenseAgeCheck;

    @Mock
    AnnounceWhenInstantPathTakenCheck instantPathTakenCheck;

    @Mock
    EvaluationLicenseAgeCheck.LicenseChecker checker;

    @Before
    public void setUp() throws Exception
    {
        when(licenseAgeCheck.getCurrentLicenseCheck()).thenReturn(checker);
    }


    @Test
    public void shouldFulfillWhenNonInstantPathTaken() {
        // given
        when(instantPathTakenCheck.isSetUpWithInstantPath()).thenReturn(false);

        // when
        final PostSetupAnnouncementStatus.Status status = testObj.computeStatus();

        // then
        assertThat(status, is(PostSetupAnnouncementStatus.Status.FULLFILLED));
    }

    @Test
    public void shouldFulfillWhenLicenseIsNotEvaluation() {
        // given
        when(instantPathTakenCheck.isSetUpWithInstantPath()).thenReturn(true);
        when(checker.isEvaluation()).thenReturn(false);
        when(checker.isEvaluationLicenseOld(Mockito.<Period>any())).thenReturn(false);

        // when
        final PostSetupAnnouncementStatus.Status status = testObj.computeStatus();

        // then
        assertThat(status, is(PostSetupAnnouncementStatus.Status.FULLFILLED));
    }

    @Test
    public void shouldAwaitWhenLicenseIsNotOldEnough() {
        // given
        when(instantPathTakenCheck.isSetUpWithInstantPath()).thenReturn(true);
        when(checker.isEvaluation()).thenReturn(true);
        when(checker.isEvaluationLicenseOld(Mockito.<Period>any())).thenReturn(false);

        // when
        final PostSetupAnnouncementStatus.Status status = testObj.computeStatus();

        // then
        assertThat(status, is(PostSetupAnnouncementStatus.Status.AWAITS));
    }

    @Test
    public void shouldAnnounceWhenLicenseIsOldEnough() {
        // given
        when(instantPathTakenCheck.isSetUpWithInstantPath()).thenReturn(true);
        when(checker.isEvaluation()).thenReturn(true);
        when(checker.isEvaluationLicenseOld(Mockito.<Period>any())).thenReturn(true);

        // when
        final PostSetupAnnouncementStatus.Status status = testObj.computeStatus();

        // then
        assertThat(status, is(PostSetupAnnouncementStatus.Status.ANNOUNCE));
    }
}