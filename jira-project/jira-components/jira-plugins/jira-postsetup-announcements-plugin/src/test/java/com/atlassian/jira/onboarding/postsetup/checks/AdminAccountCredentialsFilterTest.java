package com.atlassian.jira.onboarding.postsetup.checks;

import com.atlassian.fugue.Option;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.onboarding.postsetup.PostSetupAnnouncementProvider;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

@RunWith (MockitoJUnitRunner.class)
public class AdminAccountCredentialsFilterTest
{
    private static final String INSTANT_SETUP_USER_KEY = "instant_setup_user";
    private static final String NON_INSTANT_SETUP_USER_KEY = "other_user";

    @Mock
    ApplicationProperties applicationProperties;

    @InjectMocks
    AdminAccountCredentialsFilter testObj;

    private Option<ApplicationUser> INSTANT_SETUP_APP_USER = Option.some(
            (ApplicationUser) new MockApplicationUser(INSTANT_SETUP_USER_KEY, "user name"));
    private Option<ApplicationUser> NON_INSTANT_SETUP_APP_USER = Option.some(
            (ApplicationUser) new MockApplicationUser(NON_INSTANT_SETUP_USER_KEY, "user name"));

    @Test
    public void shouldReturnFalseWhenInstantSetupUserIsNotDefined()
    {
        // given
        Mockito.when(applicationProperties.getString(APKeys.JIRA_SETUP_INSTANT_USER)).thenReturn(null);

        // when
        final boolean canUserSeeAnnouncement = testObj.canUserSeeAnnouncement(INSTANT_SETUP_APP_USER, PostSetupAnnouncementProvider.ADMIN_ACCOUNT_SETUP);

        // then
        assertThat(canUserSeeAnnouncement, is(false));
    }

    @Test
    public void shouldReturnFalseWhenGivenUserDoesntDoSetup()
    {
        // given
        Mockito.when(applicationProperties.getString(APKeys.JIRA_SETUP_INSTANT_USER)).thenReturn(INSTANT_SETUP_USER_KEY);

        // when
        final boolean canUserSeeAnnouncement = testObj.canUserSeeAnnouncement(NON_INSTANT_SETUP_APP_USER, PostSetupAnnouncementProvider.ADMIN_ACCOUNT_SETUP);

        // then
        assertThat(canUserSeeAnnouncement, is(false));
    }

    @Test
    public void shouldReturnTrueWhenGivenUserDoSetup()
    {
        // given
        Mockito.when(applicationProperties.getString(APKeys.JIRA_SETUP_INSTANT_USER)).thenReturn(INSTANT_SETUP_USER_KEY);

        // when
        final boolean canUserSeeAnnouncement = testObj.canUserSeeAnnouncement(INSTANT_SETUP_APP_USER, PostSetupAnnouncementProvider.ADMIN_ACCOUNT_SETUP);

        // then
        assertThat(canUserSeeAnnouncement, is(true));
    }

    @Test
    public void shouldReturnFalseWhenThereIsNoCurrentUser()
    {
        // given
        Mockito.when(applicationProperties.getString(APKeys.JIRA_SETUP_INSTANT_USER)).thenReturn(INSTANT_SETUP_USER_KEY);

        // when
        final boolean canUserSeeAnnouncement = testObj.canUserSeeAnnouncement(Option.<ApplicationUser>none(), PostSetupAnnouncementProvider.ADMIN_ACCOUNT_SETUP);

        // then
        assertThat(canUserSeeAnnouncement, is(false));
    }

}