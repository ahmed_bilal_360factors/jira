package com.atlassian.jira.onboarding.postsetup.checks;

import com.atlassian.jira.mail.settings.MailSettings;
import com.atlassian.jira.onboarding.postsetup.PostSetupAnnouncementStatus;
import com.atlassian.mail.server.MailServerManager;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;


@RunWith (MockitoJUnitRunner.class)
public class OutgoingMailSetUpCheckerTest
{
    @InjectMocks
    OutgoingMailSetUpChecker testObj;

    @Mock
    MailSettings mailSettings;

    @Mock
    MailSettings.Send sendSettings;

    @Mock
    MailServerManager mailServerManager;


    @Before
    public void setUp() throws Exception
    {
        Mockito.when(mailSettings.send()).thenReturn(sendSettings);
    }

    @Test
    public void shouldAnnounceWhenOugoingMailEnabledAndNotSetup() {
        // given
        Mockito.when(sendSettings.isModifiable()).thenReturn(true);
        Mockito.when(sendSettings.isDisabled()).thenReturn(true);
        Mockito.when(sendSettings.isEnabled()).thenReturn(false);
        Mockito.when(mailServerManager.isDefaultSMTPMailServerDefined()).thenReturn(false);

        //when
        final PostSetupAnnouncementStatus.Status status = testObj.computeStatus();

        //then
        assertThat(status, Matchers.equalTo(PostSetupAnnouncementStatus.Status.ANNOUNCE));
    }

    @Test
    public void shouldFullfillWhenOugoingMailDisabledAtSystemLevel() {
        // given
        Mockito.when(sendSettings.isModifiable()).thenReturn(false);
        Mockito.when(sendSettings.isDisabled()).thenReturn(true);
        Mockito.when(sendSettings.isEnabled()).thenReturn(false);
        Mockito.when(mailServerManager.isDefaultSMTPMailServerDefined()).thenReturn(false);

        //when
        final PostSetupAnnouncementStatus.Status status = testObj.computeStatus();

        //then
        assertThat(status, Matchers.equalTo(PostSetupAnnouncementStatus.Status.AWAITS));
    }

    @Test
    public void shouldFullfillWhenOugoingMailIsConfigured() {
        // given
        Mockito.when(sendSettings.isModifiable()).thenReturn(true);
        Mockito.when(sendSettings.isDisabled()).thenReturn(false);
        Mockito.when(sendSettings.isEnabled()).thenReturn(true);
        Mockito.when(mailServerManager.isDefaultSMTPMailServerDefined()).thenReturn(true);

        //when
        final PostSetupAnnouncementStatus.Status status = testObj.computeStatus();

        //then
        assertThat(status, Matchers.equalTo(PostSetupAnnouncementStatus.Status.FULLFILLED));
    }

    @Test
    public void shouldAnnounceWhenMailIsDisabledDespiteFactThatIsConfiguredAlso() { // really?
        // given
        Mockito.when(sendSettings.isModifiable()).thenReturn(true);
        Mockito.when(sendSettings.isDisabled()).thenReturn(true);
        Mockito.when(sendSettings.isEnabled()).thenReturn(false);
        Mockito.when(mailServerManager.isDefaultSMTPMailServerDefined()).thenReturn(true);

        //when
        final PostSetupAnnouncementStatus.Status status = testObj.computeStatus();

        //then
        assertThat(status, Matchers.equalTo(PostSetupAnnouncementStatus.Status.ANNOUNCE));
    }
}