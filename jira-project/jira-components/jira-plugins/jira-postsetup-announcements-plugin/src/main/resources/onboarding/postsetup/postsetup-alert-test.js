AJS.test.require('com.atlassian.jira.jira-postsetup-announcements-plugin:post-setup-announcements-lib', function () {
    'use strict';

    var $ = require("jquery");
    var _ = require("underscore");
    var Announcements;

    var fakeAnchor = {
        scrollIntoView: function () {

        }
    };

    var ANNOUNCEMENT_1 = 'thisIsAnnouncement1';
    var ANNOUNCEMENT_2 = 'thisIsAnnouncement2';
    var ANNOUNCEMENT_RESOLVED;
    var ANNOUNCEMENT_UNRESOLVED;
    var unresolvedAnchorDefer;

    module('PostSetup announcements: controller', {
        setup: function () {
            this.context = AJS.test.context();
            var fixture = AJS.$('#qunit-fixture');

            this.xhr = sinon.useFakeXMLHttpRequest();
            this.requests = [];
            this.xhr.onCreate = _.bind(
                function (xhr) {
                    this.requests.push(xhr);
                }, this);
            this.InlineDialogMock = sinon.spy(function () { return $('#mock-popup', fixture)});
            this.context.mock('aui/inline-dialog', this.InlineDialogMock);

            Announcements = this.context.require("jira/postsetup/announcements-view-lib");

            this.popup = $("<div id='mock-popup'><div class='" + Announcements.closeElementClass + "' /><div class='" + Announcements.actionElementClass + "' /></div>");
            fixture.append(this.popup);

            ANNOUNCEMENT_RESOLVED = {};
            ANNOUNCEMENT_RESOLVED[ANNOUNCEMENT_1] = new Announcements.Announcement(
                ANNOUNCEMENT_1,
                function ($popup, $content) {},
                function () { return $.Deferred().resolve(fakeAnchor).promise() },
                'w',
                0,
                true);

            ANNOUNCEMENT_UNRESOLVED = {};
            unresolvedAnchorDefer = $.Deferred();
            ANNOUNCEMENT_UNRESOLVED[ANNOUNCEMENT_1] = new Announcements.Announcement(
                ANNOUNCEMENT_1,
                function ($popup, $content) {},
                function () { return unresolvedAnchorDefer.promise() },
                'w',
                0,
                true);

        },

        teardown: function () {
            this.popup.remove();
            this.xhr.restore();
        }
    });

    test('should send dismiss request when dialog is shown', function () {
        var announcements = new Announcements.AnnouncementsController(ANNOUNCEMENT_UNRESOLVED, [ANNOUNCEMENT_1]);
        announcements.createFlags();

        ok(this.requests.length == 0, "Should not send requests until dialog shown");

        unresolvedAnchorDefer.resolve(fakeAnchor);

        var request = this.requests[0];
        ok(request.url.indexOf("/rest/onboarding/1/postsetup_announcement/announced") >= 0, "Invalid request uri");
        var body = JSON.parse(request.requestBody);
        deepEqual( body, [ANNOUNCEMENT_1], "Invalid announcements disimissed");
        equal( request.method, "POST");
    });

    test('should show popup when announcement is available', function () {
        var announcements = new Announcements.AnnouncementsController(ANNOUNCEMENT_RESOLVED, [ANNOUNCEMENT_1]);
        announcements.createFlags();

        var call = this.InlineDialogMock.getCall(0);
        sinon.assert.calledWith(this.InlineDialogMock, fakeAnchor);
        ok(call.args[1].indexOf(ANNOUNCEMENT_1) >= 0, "announcement.1 not used in popup id");
    });

    test('should not show popup when other announcement is announced', function () {
        var announcements = new Announcements.AnnouncementsController(ANNOUNCEMENT_RESOLVED, ["otherAnnouncement"]);
        announcements.createFlags();

        sinon.assert.notCalled(this.InlineDialogMock);
    });

    test('should send analytics event when announcement is visible', function () {
        var analyticEventHandler = sinon.spy();
        AJS.bind('analyticsEvent', analyticEventHandler);

        try {
            var announcements = new Announcements.AnnouncementsController(ANNOUNCEMENT_RESOLVED, [ANNOUNCEMENT_1]);
            announcements.createFlags();
        } finally {
            AJS.unbind('analyticsEvent', analyticEventHandler);
        }

        sinon.assert.called(analyticEventHandler);
        var call = analyticEventHandler.getCall(0);
        ok(call.args[0].type === 'analyticsEvent', "raised event " + call.args[0].type + " but should be analytics");
        ok(call.args[1].name === "post.setup.announcement." + ANNOUNCEMENT_1 + ".show", "raised analytics event " + call.args[1].name);
    });

    test('should send analytics event when announcement is dismissed', function () {
        var analyticEventHandler = sinon.spy();
        AJS.bind('analyticsEvent', analyticEventHandler);

        try {
            var announcements = new Announcements.AnnouncementsController(ANNOUNCEMENT_RESOLVED, [ANNOUNCEMENT_1]);
            announcements.createFlags();

            $("." + Announcements.closeElementClass, this.popup).click();
        }
        finally {
            AJS.unbind('analyticsEvent', analyticEventHandler);
        }

        sinon.assert.calledTwice(analyticEventHandler);
        var call = analyticEventHandler.getCall(1);
        ok(call.args[0].type === 'analyticsEvent', "raised event " + call.args[0].type + " but should be analytics");
        ok(call.args[1].name === "post.setup.announcement." + ANNOUNCEMENT_1 + ".dismiss", "raised analytics event " + call.args[1].name);
    });

    test('should send analytics event when action is taken on announcement', function () {
        var analyticEventHandler = sinon.spy();
        AJS.bind('analyticsEvent', analyticEventHandler);
        try {
            var announcements = new Announcements.AnnouncementsController(ANNOUNCEMENT_RESOLVED, [ANNOUNCEMENT_1]);
            announcements.createFlags();

            $("." + Announcements.actionElementClass, this.popup).click();
        } finally {
            AJS.unbind('analyticsEvent', analyticEventHandler);
        }

        sinon.assert.calledTwice(analyticEventHandler);
        var call = analyticEventHandler.getCall(1);
        ok(call.args[0].type === 'analyticsEvent', "raised event " + call.args[0].type + " but should be analytics");
        ok(call.args[1].name === "post.setup.announcement." + ANNOUNCEMENT_1 + ".action", "raised analytics event " + call.args[1].name);
    });

    module('PostSetup announcements: conflicts', {
        setup: function () {
            this.context = AJS.test.context();
            var fixture = AJS.$('#qunit-fixture');

            this.xhr = sinon.useFakeXMLHttpRequest();
            this.InlineDialogMock = sinon.spy(function (anchor, id, callback, param) {
                callback(fixture, fixture, sinon.spy());

                return fixture;
            });
            this.context.mock('aui/inline-dialog', this.InlineDialogMock);

            Announcements = this.context.require("jira/postsetup/announcements-view-lib");

            this.getFakeAnchor1Promise = sinon.stub().returns($.Deferred().resolve(fakeAnchor).promise());
            this.getFakeAnchor2Promise = sinon.stub().returns($.Deferred().resolve(fakeAnchor).promise());
            this.rejectAnchorPromise = sinon.stub().returns($.Deferred().reject().promise());
            this.pendingAnchorPromise = sinon.stub().returns($.Deferred().promise());

            this.generateAnnoucencement1 = sinon.spy();
            this.generateAnnoucencement2 = sinon.spy();
        },

        teardown: function () {
            this.xhr.restore();
        }
    });

    test('should schedule only one blocking announcement at once', function() {
        var ANNOUNCEMENT_ALL_BLOCKING = {};
        ANNOUNCEMENT_ALL_BLOCKING[ANNOUNCEMENT_1] = new Announcements.Announcement(
            ANNOUNCEMENT_1,
            this.generateAnnoucencement1,
            this.getFakeAnchor1Promise,
            'w',
            0,
            true);
        ANNOUNCEMENT_ALL_BLOCKING[ANNOUNCEMENT_2] = new Announcements.Announcement(
            ANNOUNCEMENT_2,
            this.generateAnnoucencement2,
            this.getFakeAnchor2Promise,
            'w',
            1,
            true);
        var announcements = new Announcements.AnnouncementsController(ANNOUNCEMENT_ALL_BLOCKING, [ANNOUNCEMENT_1, ANNOUNCEMENT_2]);
        announcements.createFlags();

        sinon.assert.calledOnce(this.getFakeAnchor1Promise);
        sinon.assert.calledOnce(this.generateAnnoucencement1);

        sinon.assert.notCalled(this.getFakeAnchor2Promise);
        sinon.assert.notCalled(this.generateAnnoucencement2)
    });

    test('pending anchor should prevent next announcement', function() {
        var ANNOUNCEMENT_ALL_BLOCKING_FIRST_PENDING = {};
        ANNOUNCEMENT_ALL_BLOCKING_FIRST_PENDING[ANNOUNCEMENT_1] = new Announcements.Announcement(
            ANNOUNCEMENT_1,
            this.generateAnnoucencement1,
            this.pendingAnchorPromise,
            'w',
            0,
            true);
        ANNOUNCEMENT_ALL_BLOCKING_FIRST_PENDING[ANNOUNCEMENT_2] = new Announcements.Announcement(
            ANNOUNCEMENT_2,
            this.generateAnnoucencement2,
            this.getFakeAnchor2Promise,
            'w',
            1,
            true);

        var announcements = new Announcements.AnnouncementsController(ANNOUNCEMENT_ALL_BLOCKING_FIRST_PENDING, [ANNOUNCEMENT_1, ANNOUNCEMENT_2]);
        announcements.createFlags();

        sinon.assert.calledOnce(this.pendingAnchorPromise);
        sinon.assert.notCalled(this.generateAnnoucencement1);

        sinon.assert.notCalled(this.getFakeAnchor2Promise);
        sinon.assert.notCalled(this.generateAnnoucencement2)
    });

    test('announcements should obey weight defined', function() {
        // given
        var ANNOUNCEMENT_ALL_BLOCKING_FIRST_PENDING = {};
        var getAnchorForAnnouncement1 = sinon.stub().returns($.Deferred().promise());
        var getAnchorForAnnouncement2 = sinon.stub().returns($.Deferred().promise());
        ANNOUNCEMENT_ALL_BLOCKING_FIRST_PENDING[ANNOUNCEMENT_1] = new Announcements.Announcement(
                ANNOUNCEMENT_1,
                sinon.spy(),
                getAnchorForAnnouncement1,
                'w',
                0,
                true);
        ANNOUNCEMENT_ALL_BLOCKING_FIRST_PENDING[ANNOUNCEMENT_2] = new Announcements.Announcement(
                ANNOUNCEMENT_2,
                sinon.spy(),
                getAnchorForAnnouncement2,
                'w',
                1,
                true);

        // when
        var announcements = new Announcements.AnnouncementsController(ANNOUNCEMENT_ALL_BLOCKING_FIRST_PENDING, [ANNOUNCEMENT_2, ANNOUNCEMENT_1]);
        announcements.createFlags();

        // then
        sinon.assert.calledOnce(getAnchorForAnnouncement1);
        sinon.assert.notCalled(getAnchorForAnnouncement2);
    });

    test('rejected anchor should allow next announcement', function() {
        var ANNOUNCEMENT_ALL_BLOCKING_FIRST_REJECTED = {};
        ANNOUNCEMENT_ALL_BLOCKING_FIRST_REJECTED[ANNOUNCEMENT_1] = new Announcements.Announcement(
            ANNOUNCEMENT_1,
            this.generateAnnoucencement1,
            this.rejectAnchorPromise,
            'w',
            0,
            true);
        ANNOUNCEMENT_ALL_BLOCKING_FIRST_REJECTED[ANNOUNCEMENT_2] = new Announcements.Announcement(
            ANNOUNCEMENT_2,
            this.generateAnnoucencement2,
            this.getFakeAnchor2Promise,
            'w',
            1,
            true);

        var announcements = new Announcements.AnnouncementsController(ANNOUNCEMENT_ALL_BLOCKING_FIRST_REJECTED, [ANNOUNCEMENT_1, ANNOUNCEMENT_2]);
        announcements.createFlags();

        sinon.assert.notCalled(this.generateAnnoucencement1);

        sinon.assert.calledOnce(this.getFakeAnchor2Promise);
        sinon.assert.calledOnce(this.generateAnnoucencement2)
    });

    test('pending non blocking announcement should allow next announcement', function() {
        var ANNOUNCEMENT_FIRST_NON_BLOCKING_PENDING = {};
        ANNOUNCEMENT_FIRST_NON_BLOCKING_PENDING[ANNOUNCEMENT_1] = new Announcements.Announcement(
            ANNOUNCEMENT_1,
            this.generateAnnoucencement1,
            this.pendingAnchorPromise,
            'w',
            0,
            false);
        ANNOUNCEMENT_FIRST_NON_BLOCKING_PENDING[ANNOUNCEMENT_2] = new Announcements.Announcement(
            ANNOUNCEMENT_2,
            this.generateAnnoucencement2,
            this.getFakeAnchor2Promise,
            'w',
            1,
            true);

        var announcements = new Announcements.AnnouncementsController(ANNOUNCEMENT_FIRST_NON_BLOCKING_PENDING, [ANNOUNCEMENT_1, ANNOUNCEMENT_2]);
        announcements.createFlags();

        sinon.assert.calledOnce(this.pendingAnchorPromise);

        sinon.assert.calledOnce(this.getFakeAnchor2Promise);
        sinon.assert.calledOnce(this.generateAnnoucencement2)
    });

    test('resolved non blocking announcement should stop next announcement', function() {
        var ANNOUNCEMENT_FIRST_NON_BLOCKING = {};
        ANNOUNCEMENT_FIRST_NON_BLOCKING[ANNOUNCEMENT_1] = new Announcements.Announcement(
            ANNOUNCEMENT_1,
            this.generateAnnoucencement1,
            this.getFakeAnchor1Promise,
            'w',
            0,
            false);
        ANNOUNCEMENT_FIRST_NON_BLOCKING[ANNOUNCEMENT_2] = new Announcements.Announcement(
            ANNOUNCEMENT_2,
            this.generateAnnoucencement2,
            this.getFakeAnchor2Promise,
            'w',
            1,
            true);

        var announcements = new Announcements.AnnouncementsController(ANNOUNCEMENT_FIRST_NON_BLOCKING, [ANNOUNCEMENT_1, ANNOUNCEMENT_2]);
        announcements.createFlags();

        sinon.assert.calledOnce(this.getFakeAnchor1Promise);
        sinon.assert.calledOnce(this.generateAnnoucencement1);

        sinon.assert.notCalled(this.getFakeAnchor2Promise);
        sinon.assert.notCalled(this.generateAnnoucencement2)
    });

    module('PostSetup announcements: view helpers', {
        setup: function () {
            this.context = AJS.test.context();
            this.fixture = AJS.$('#qunit-fixture');

            this.context.mock('aui/inline-dialog', function () {});
            Announcements = this.context.require("jira/postsetup/announcements-view-lib");

            this.clock = sinon.useFakeTimers();
        },

        teardown: function () {
            this.clock.restore();
        }
    });

    test('holdOnTillFlagsAreGone - without flags it should resolve instantly', function () {
        var doneSpy = sinon.spy();

        var inputPromise = function () { return $.Deferred().resolve(fakeAnchor).promise() };
        var testObj = Announcements.holdOnTillFlagsAreGone(inputPromise)();
        testObj.done(doneSpy);
        this.clock.tick(1);
        sinon.assert.calledWith(doneSpy, fakeAnchor);
    });

    test('holdOnTillFlagsAreGone - should resolve after flags disappear', function () {
        var doneSpy = sinon.spy();

        var inputPromise = function () { return $.Deferred().resolve(fakeAnchor).promise() };
        var testObj = Announcements.holdOnTillFlagsAreGone(inputPromise)();
        testObj.done(doneSpy);

        var flag = $("<div class='aui-flag' aria-hidden=false />");
        this.fixture.append(flag);

        this.clock.tick(20);
        sinon.assert.notCalled(doneSpy);

        flag.remove();
        $(document).trigger('aui-flag-close');
        sinon.assert.calledWith(doneSpy, fakeAnchor);
    });

    test('holdOnTillFlagsAreGone - should wait till all flags are gone', function () {
        var doneSpy = sinon.spy();

        var inputPromise = function () { return $.Deferred().resolve(fakeAnchor).promise() };
        var testObj = Announcements.holdOnTillFlagsAreGone(inputPromise)();
        testObj.done(doneSpy);

        var flag1 = $("<div class='aui-flag' aria-hidden=false />");
        this.fixture.append(flag1);
        var flag2 = $("<div class='aui-flag' aria-hidden=false />");
        this.fixture.append(flag2);

        this.clock.tick(20);
        flag1.remove();
        $(document).trigger('aui-flag-close');

        sinon.assert.notCalled(doneSpy);
        flag2.remove();
        $(document).trigger('aui-flag-close');

        sinon.assert.calledWith(doneSpy, fakeAnchor);
    });

    test('holdOnTillDialogsAreGone - without dialogs it should resolve after timeout', function () {
        var doneSpy = sinon.spy();

        var inputPromise = function () { return $.Deferred().resolve(fakeAnchor).promise() };
        var testObj = Announcements.holdOnTillDialogsAreGone(inputPromise)();
        testObj.done(doneSpy);
        this.clock.tick(800);
        sinon.assert.calledWith(doneSpy, fakeAnchor);

        // can't test more of this: unable to trick is(":visible")
    });
});