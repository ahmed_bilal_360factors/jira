(function () {

    'use strict';

    require(['jquery', 'jira/postsetup/announcements-view-lib'], function ($, Announcements) {
        // import
        var Announcement = Announcements.Announcement;
        var AnnouncementsController = Announcements.AnnouncementsController;

        var ifOnPage = Announcements.ifOnPage;
        var findElement = Announcements.findElement;
        var inViewPort = Announcements.inViewPort;
        var holdOnTillFlagsAreGone = Announcements.holdOnTillFlagsAreGone;
        var holdOnTillDialogsAreGone = Announcements.holdOnTillDialogsAreGone;
        var soyTemplateAnnouncementBody = Announcements.soyTemplateAnnouncementBody;
        var GRAVITY_LEFT = Announcements.GRAVITY_LEFT;
        var GRAVITY_BELOW = Announcements.GRAVITY_BELOW;

        function init() {
            var DEFINED_ANNOUNCEMENTS = {
                'database.setup': new Announcement(
                    'database.setup',
                    soyTemplateAnnouncementBody(JIRA.Templates.PostSetup.dbDiscoveryContent),
                    holdOnTillFlagsAreGone(findElement("#system-admin-menu")),
                    GRAVITY_BELOW,
                    0,
                    true),
                'admin.account.setup': new Announcement(
                    'admin.account.setup',
                    soyTemplateAnnouncementBody(JIRA.Templates.PostSetup.adminAccountDiscoveryContent),
                    holdOnTillFlagsAreGone(findElement("#user-options")),
                    GRAVITY_BELOW,
                    10,
                    true),
                'app.properties.setup': new Announcement(
                    'app.properties.setup',
                    soyTemplateAnnouncementBody(JIRA.Templates.PostSetup.appPropertiesDiscoveryContent),
                    ifOnPage("admin/ViewApplicationProperties.jspa",
                        holdOnTillFlagsAreGone(findElement("#edit-app-properties"))),
                    GRAVITY_BELOW,
                    20,
                    true
                ),
                'mail.properties.setup': new Announcement(
                    'mail.properties.setup',
                    soyTemplateAnnouncementBody(JIRA.Templates.PostSetup.mailPropertiesDiscoveryContent),
                    ifOnPage("admin/ViewApplicationProperties.jspa",
                        inViewPort(findElement("#outgoing_mail"))),
                    GRAVITY_LEFT,
                    -10, // this is async, it doesn't block other announcements
                    false
                ),
                'create.user.mail.properties.setup': new Announcement(
                    'create.user.mail.properties.setup',
                    soyTemplateAnnouncementBody(JIRA.Templates.PostSetup.userBrowserEmailDiscovery),
                    ifOnPage("admin/user/UserBrowser.jspa",
                        holdOnTillDialogsAreGone(findElement("#invite_user"))),
                    GRAVITY_BELOW,
                    30,
                    true
                )

                // don't forget to whitelist analytics events when you are adding new annoucements
            };

            var announcements =
                WRM.data.claim('com.atlassian.jira.jira-postsetup-announcements-plugin:post-setup-announcements.announcements') || [];
            var controller = new AnnouncementsController(DEFINED_ANNOUNCEMENTS, announcements);
            controller.createFlags();
        }

        $(function () {
            init();
        });
    });
})();