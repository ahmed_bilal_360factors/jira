package com.atlassian.jira.onboarding.postsetup;

import java.util.List;

import javax.annotation.Nonnull;

import com.atlassian.fugue.Option;
import com.atlassian.jira.user.ApplicationUser;

/**
 * @since v6.4
 */
public interface PostSetupAnnouncementService
{
    /**
     * Forces recalculation of post setup announcement statuses
     */
    void recalculateStatuses(Option<ApplicationUser> user) throws SecurityException;

    /**
     * Marks given announcements as announced, so they no longer will be reported as ready
     */
    void activitiesAnnounced(final Option<ApplicationUser> user, @Nonnull List<String> activityIds)
            throws SecurityException;

    /**
     * Get list of post setup announcements that are ready to be shown to user.
     */
    @Nonnull
    List<PostSetupAnnouncementStatus> getReadyAnnouncements(final Option<ApplicationUser> user);

    /**
     * Gives information if there are any announcements ready to show to user.
     */
    boolean hasAnnouncements(final Option<ApplicationUser> user);

    /**
     * Reset all announcements to await state. Next call to recalculateStatuses will
     * recalculate all registered announcements.
     */
    void reset(final Option<ApplicationUser> user) throws SecurityException;
}
