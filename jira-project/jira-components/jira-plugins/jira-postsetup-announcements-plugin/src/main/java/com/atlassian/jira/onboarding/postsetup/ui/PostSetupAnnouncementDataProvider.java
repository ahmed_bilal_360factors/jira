package com.atlassian.jira.onboarding.postsetup.ui;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

import javax.inject.Inject;

import com.atlassian.fugue.Option;
import com.atlassian.jira.onboarding.postsetup.PostSetupAnnouncementService;
import com.atlassian.jira.onboarding.postsetup.PostSetupAnnouncementStatus;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

/**
 * @since v6.4
 */
@Scanned
public class PostSetupAnnouncementDataProvider implements WebResourceDataProvider
{
    private final PostSetupAnnouncementService postSetupAnnouncements;
    private final JiraAuthenticationContext authContext;

    @Inject
    public PostSetupAnnouncementDataProvider(
            final PostSetupAnnouncementService postSetupAnnouncements,
            @ComponentImport final JiraAuthenticationContext authContext)
    {
        this.postSetupAnnouncements = postSetupAnnouncements;
        this.authContext = authContext;
    }

    @Override
    public Jsonable get()
    {
        return new Jsonable()
        {
            @Override
            public void write(final Writer writer) throws IOException
            {
                try
                {
                    getAnnouncementsData().write(writer);
                }
                catch (JSONException e)
                {
                    throw new JsonMappingException(e);
                }
            }
        };
    }

    public JSONArray getAnnouncementsData()
    {
        final Option<ApplicationUser> applicationUser = Option.option(authContext.getUser());

        final List<String> activitiesIds = Lists.transform(postSetupAnnouncements.getReadyAnnouncements(applicationUser), new Function<PostSetupAnnouncementStatus, String>()
        {
            @Override
            public String apply(final PostSetupAnnouncementStatus input)
            {
                return input.getActivityId();
            }
        });

        return new JSONArray(activitiesIds);
    }
}
