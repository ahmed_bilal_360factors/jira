package com.atlassian.jira.onboarding.postsetup.checks;

import javax.inject.Inject;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.onboarding.postsetup.AnnouncementStatusChecker;
import com.atlassian.jira.onboarding.postsetup.PostSetupAnnouncementStatus;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import org.springframework.stereotype.Component;

/**
 * @since v6.4
 */
@Component
public class AnnounceWhenInstantPathTakenCheck implements AnnouncementStatusChecker
{
    private final ApplicationProperties applicationProperties;

    @Inject
    public AnnounceWhenInstantPathTakenCheck(@ComponentImport ApplicationProperties applicationProperties) {

        this.applicationProperties = applicationProperties;
    }

    @Override
    public PostSetupAnnouncementStatus.Status computeStatus()
    {
        return isSetUpWithInstantPath() ?
                PostSetupAnnouncementStatus.Status.ANNOUNCE :
                PostSetupAnnouncementStatus.Status.FULLFILLED;
    }

    public boolean isSetUpWithInstantPath() {
        return applicationProperties.getOption(APKeys.JIRA_SETUP_IS_INSTANT);
    }
}
