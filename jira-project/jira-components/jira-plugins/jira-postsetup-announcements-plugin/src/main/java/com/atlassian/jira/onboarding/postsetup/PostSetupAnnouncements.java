package com.atlassian.jira.onboarding.postsetup;

import com.atlassian.fugue.Option;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import javax.inject.Inject;

import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Lists.newArrayList;

/**
 * @since v6.4
 */
@ExportAsService (PostSetupAnnouncementService.class)
@Component
public class PostSetupAnnouncements implements PostSetupAnnouncementService
{
    final PostSetupAnnouncementStore postSetupAnnouncementStore;
    final PostSetupAnnouncementProvider postSetupAnnouncementProvider;
    final AnnouncementAccessPolicy announcementAccessPolicy;

    @Inject
    public PostSetupAnnouncements(
            final PostSetupAnnouncementStore postSetupAnnouncementStore,
            final PostSetupAnnouncementProvider postSetupAnnouncementProvider,
            final AnnouncementAccessPolicy announcementAccessPolicy)
    {
        this.postSetupAnnouncementStore = postSetupAnnouncementStore;
        this.postSetupAnnouncementProvider = postSetupAnnouncementProvider;
        this.announcementAccessPolicy = announcementAccessPolicy;
    }

    @Override
    public void recalculateStatuses(Option<ApplicationUser> user)
    {
        if (!announcementAccessPolicy.canUpdate(user))
        {
            throw new SecurityException("User can't recalculate statuses");
        }

        final Set<String> announcedActivities = getDoneActivityIds();
        final List<PostSetupAnnouncement> registeredActivities = postSetupAnnouncementProvider.getAnouncements();

        final Iterable<PostSetupAnnouncement> notAnnouncedActivities = filter(registeredActivities, new Predicate<PostSetupAnnouncement>()
        {
            @Override
            public boolean apply(final PostSetupAnnouncement input)
            {
                final boolean wasActivityAnnounced = announcedActivities.contains(input.getAnouncementId());
                return !wasActivityAnnounced;
            }
        });
        final Iterable<PostSetupAnnouncementStatus> currentStatuses = Iterables.transform(
                notAnnouncedActivities,
                new Function<PostSetupAnnouncement, PostSetupAnnouncementStatus>()
                {
                    @Override
                    public PostSetupAnnouncementStatus apply(final PostSetupAnnouncement input)
                    {
                        final PostSetupAnnouncementStatus.Status status = input.computeStatus();

                        return new PostSetupAnnouncementStatus(input.getAnouncementId(), status);
                    }
                });

        postSetupAnnouncementStore.updateStatuses(currentStatuses);
    }

    @Override
    public void activitiesAnnounced(final Option<ApplicationUser> user, final List<String> activityIds)
    {
        if (!announcementAccessPolicy.canUpdate(user))
        {
            throw new SecurityException("User can't mark activities as announced");
        }
        final Set<String> allActivitiesIds = getAllActivitiesIds();
        final Set<String> notRegisteredActivities =
                Sets.difference(ImmutableSet.copyOf(activityIds), allActivitiesIds).immutableCopy();

        if (!notRegisteredActivities.isEmpty())
        {
            throw new NoSuchElementException(Iterables.toString(notRegisteredActivities));
        }

        final Iterable<PostSetupAnnouncementStatus> currentStatuses =
                Lists.transform(activityIds, new Function<String, PostSetupAnnouncementStatus>()
                {
                    @Override
                    public PostSetupAnnouncementStatus apply(final String activityId)
                    {
                        return new PostSetupAnnouncementStatus(activityId, PostSetupAnnouncementStatus.Status.ANNOUNCED);
                    }
                });

        postSetupAnnouncementStore.updateStatuses(currentStatuses);
    }

    @Override
    public List<PostSetupAnnouncementStatus> getReadyAnnouncements(final Option<ApplicationUser> user)
    {
        if (!announcementAccessPolicy.canRead(user))
        {
            return Collections.emptyList();
        }
        final List<PostSetupAnnouncementStatus> allStatuses = postSetupAnnouncementStore.getAllStatuses();
        final List<PostSetupAnnouncementStatus> readyAnouncements = newArrayList(
                filter(allStatuses, new Predicate<PostSetupAnnouncementStatus>()
                {
                    @Override
                    public boolean apply(final PostSetupAnnouncementStatus announcementStatus)
                    {
                        return isReadyForAnnouncement(user, announcementStatus);
                    }
                }));

        return readyAnouncements;
    }

    @Override
    public boolean hasAnnouncements(final Option<ApplicationUser> user)
    {
        if (!announcementAccessPolicy.canRead(user))
        {
            return false;
        }
        final List<PostSetupAnnouncementStatus> allStatuses = postSetupAnnouncementStore.getAllStatuses();
        final boolean isAnyAnnouncementReady = Iterables.any(allStatuses, new Predicate<PostSetupAnnouncementStatus>()
        {
            @Override
            public boolean apply(final PostSetupAnnouncementStatus announcementStatus)
            {
                return isReadyForAnnouncement(user, announcementStatus);
            }
        });

        return isAnyAnnouncementReady;
    }

    @Override
    public void reset(final Option<ApplicationUser> user)
    {
        if (!announcementAccessPolicy.canUpdate(user))
        {
            throw new SecurityException("User cannot reset");
        }
        Iterable<PostSetupAnnouncementStatus> initialStatuses = Iterables.transform(
                getAllActivitiesIds(),
                new Function<String, PostSetupAnnouncementStatus>()
                {
                    @Override
                    public PostSetupAnnouncementStatus apply(final String activtyId)
                    {
                        return new PostSetupAnnouncementStatus(activtyId, PostSetupAnnouncementStatus.Status.AWAITS);
                    }
                });

        postSetupAnnouncementStore.updateStatuses(initialStatuses);
    }

    private Set<String> getAllActivitiesIds()
    {
        final List<PostSetupAnnouncement> allStatuses = postSetupAnnouncementProvider.getAnouncements();

        return Sets.newHashSet(
                Iterables.transform(allStatuses, new Function<PostSetupAnnouncement, String>()
                {
                    @Override
                    public String apply(final PostSetupAnnouncement input)
                    {
                        return input.getAnouncementId();
                    }
                }));
    }

    private Set<String> getDoneActivityIds()
    {
        final List<PostSetupAnnouncementStatus> allStatuses = postSetupAnnouncementStore.getAllStatuses();
        final Iterable<PostSetupAnnouncementStatus> announcedStatuses = filter(allStatuses, new Predicate<PostSetupAnnouncementStatus>()
        {
            @Override
            public boolean apply(final PostSetupAnnouncementStatus input)
            {
                return isStatusDone(input.getActivityStatus());
            }
        });
        return Sets.newHashSet(
                Iterables.transform(announcedStatuses, new Function<PostSetupAnnouncementStatus, String>()
                {
                    @Override
                    public String apply(final PostSetupAnnouncementStatus input)
                    {
                        return input.getActivityId();
                    }
                }));
    }

    private boolean isStatusDone(final PostSetupAnnouncementStatus.Status activityStatus)
    {
        return activityStatus == PostSetupAnnouncementStatus.Status.ANNOUNCED ||
                activityStatus == PostSetupAnnouncementStatus.Status.FULLFILLED;
    }

    private boolean isReadyForAnnouncement(final Option<ApplicationUser> user, final PostSetupAnnouncementStatus input)
    {
        final AnnouncementFilter filter = announcementAccessPolicy.getAnnouncementFilter();

        return input.getActivityStatus() == PostSetupAnnouncementStatus.Status.ANNOUNCE &&
                filter.canUserSeeAnnouncement(user, input.getActivityId());
    }
}
