package com.atlassian.jira.onboarding.postsetup;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import org.springframework.stereotype.Component;

/**
 * @since v6.4
 */
@Component
@ExportAsService (PostSetupAnnouncementStore.class)
public class AppPropertiesAnnouncementStore implements PostSetupAnnouncementStore
{
    private static final String ALL_PROPERTIES = "com.atlassian.jira.onboarding.postsetup.AppPropertiesPostSetupAnnouncementStore.all";
    private static final Set<String> ALL_STATUSES = Sets.newHashSet(
            Iterables.transform(
                    Arrays.asList(PostSetupAnnouncementStatus.Status.values()),
                    new Function<PostSetupAnnouncementStatus.Status, String>()
                    {
                        @Override
                        public String apply(final PostSetupAnnouncementStatus.Status input)
                        {
                            return input.name();
                        }
                    }
            ));

    private static final Predicate<Object> IS_VALUE_VALID_STATUS = new Predicate<Object>()
    {
        @Override
        public boolean apply(final Object value)
        {
            return ALL_STATUSES.contains(value);
        }
    };
    private static final char PROPERTIES_SEPARATOR = ';';
    private static final String KEY_SEPARATOR = "=";

    final ApplicationProperties applicationProperties;

    @Inject
    public AppPropertiesAnnouncementStore(@ComponentImport final ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
    }

    @Override
    @Nonnull
    public List<PostSetupAnnouncementStatus> getAllStatuses()
    {
        final Map<String, String> activityIdMap = extractProperties();
        final Map<String, String> announcementProperties = Maps.filterValues(activityIdMap, IS_VALUE_VALID_STATUS);

        Iterable<PostSetupAnnouncementStatus> statuses =
                Iterables.transform(announcementProperties.entrySet(), new Function<Map.Entry<String, String>, PostSetupAnnouncementStatus>()
                {
                    @Override
                    public PostSetupAnnouncementStatus apply(final Map.Entry<String, String> entry)
                    {
                        final PostSetupAnnouncementStatus.Status activityStatus =
                                PostSetupAnnouncementStatus.Status.valueOf(entry.getValue());

                        return new PostSetupAnnouncementStatus(
                                propertyNameToActivityId(entry.getKey()),
                                activityStatus);
                    }
                });

        return Lists.newArrayList(statuses);
    }

    @Override
    public void updateStatuses(@Nonnull final Iterable<PostSetupAnnouncementStatus> postSetupActivityStatuses)
    {
        final Map<String, String> originalProperties = extractProperties();
        final Map<String, String> activityIdMap = Maps.newHashMap(originalProperties);
        for (PostSetupAnnouncementStatus status : postSetupActivityStatuses)
        {
            activityIdMap.put(activityIdToPropertyName(status), status.getActivityStatus().name());
        }
        if (!originalProperties.equals(activityIdMap))
        {
            saveProperties(activityIdMap);
        }
    }

    private String activityIdToPropertyName(final PostSetupAnnouncementStatus status)
    {
        return status.getActivityId();
    }

    private String propertyNameToActivityId(final String name)
    {
        return name;
    }

    /**
     * @return immutable map containing properties extracted from applicationProperties
     */
    @Nonnull
    private Map<String, String> extractProperties()
    {
        final String applicationPropertiesText = applicationProperties.getText(ALL_PROPERTIES);
        if (null == applicationPropertiesText)
        {
            return MapBuilder.emptyMap();
        }

        Splitter propertiesStringSplitter = Splitter.on(PROPERTIES_SEPARATOR)
                .trimResults()
                .omitEmptyStrings();
        final MapBuilder<String, String> propertyMapBuilder = MapBuilder.newBuilder();
        // not using splitter to be more permissive and try to recover destroyed entries
        for (String propertyString : propertiesStringSplitter.split(applicationPropertiesText))
        {
            String[] keyAndValue = propertyString.split(KEY_SEPARATOR);
            if (keyAndValue.length == 2)
            {
                final String key = keyAndValue[0].trim();
                final String value = keyAndValue[1].trim();

                if (key.length() > 0 && value.length() > 0)
                {
                    propertyMapBuilder.add(key, value);
                }
            }
        }

        return propertyMapBuilder.toMap();
    }

    private void saveProperties(@Nonnull Map<String, String> propertyMap)
    {
        final String propertiesText = Joiner.on(PROPERTIES_SEPARATOR).withKeyValueSeparator(KEY_SEPARATOR).join(propertyMap);

        applicationProperties.setText(ALL_PROPERTIES, propertiesText);
    }
}
