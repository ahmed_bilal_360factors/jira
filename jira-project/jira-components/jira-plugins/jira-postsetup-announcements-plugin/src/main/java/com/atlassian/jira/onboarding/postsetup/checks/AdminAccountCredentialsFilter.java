package com.atlassian.jira.onboarding.postsetup.checks;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Suppliers;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.onboarding.postsetup.AnnouncementFilter;
import com.atlassian.jira.onboarding.postsetup.PostSetupAnnouncementProvider;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

import static com.google.common.base.Suppliers.memoize;

/**
 * @since v6.4
 */
@Component
public class AdminAccountCredentialsFilter implements AnnouncementFilter
{
    private final Supplier<Option<String>> instantSetupUserSupplier;

    @Inject
    public AdminAccountCredentialsFilter(@ComponentImport final ApplicationProperties applicationProperties)
    {
        this.instantSetupUserSupplier = memoize(new Supplier<Option<String>>()
        {
            @Override
            public Option<String> get()
            {
                return Option.option(applicationProperties.getString(APKeys.JIRA_SETUP_INSTANT_USER));
            }
        });
    }

    @Override
    public boolean canUserSeeAnnouncement(final Option<ApplicationUser> applicationUsers, final String announcedActivity)
    {
        final Option<String> instantSetupUser = instantSetupUserSupplier.get();

        if (announcedActivity.equals(PostSetupAnnouncementProvider.ADMIN_ACCOUNT_SETUP))
        {
            return applicationUsers.fold(Suppliers.alwaysFalse(), new Function<ApplicationUser, Boolean>()
            {
                @Override
                public Boolean apply(final ApplicationUser user)
                {
                    return instantSetupUser.fold(Suppliers.alwaysFalse(), new Function<String, Boolean>()
                    {
                        @Override
                        public Boolean apply(final String input)
                        {
                            return user.getKey().equals(input);
                        }
                    });
                }
            });
        }
        else
        {
            return true;
        }
    }
}
