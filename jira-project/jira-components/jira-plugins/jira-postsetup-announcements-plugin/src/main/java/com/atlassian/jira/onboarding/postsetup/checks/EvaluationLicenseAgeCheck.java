package com.atlassian.jira.onboarding.postsetup.checks;

import java.util.Date;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import com.atlassian.core.util.Clock;
import com.atlassian.fugue.Option;
import com.atlassian.fugue.Suppliers;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.springframework.stereotype.Component;

/**
 * Check if there is evaluation license that is old enough
 *
 * @since v6.4
 */
@Component
public class EvaluationLicenseAgeCheck
{
    private final JiraLicenseManager licenseManager;
    private final Clock clock;

    @Inject
    public EvaluationLicenseAgeCheck(@ComponentImport final JiraLicenseManager licenseManager, @ComponentImport final Clock clock)
    {
        this.licenseManager = licenseManager;
        this.clock = clock;
    }

    @Nonnull
    public LicenseChecker getCurrentLicenseCheck()
    {
        return new LicenseChecker(licenseManager.getLicenses(), clock);
    }

    public class LicenseChecker
    {
        private final Iterable<LicenseDetails> licenses;
        private final Clock clock;

        public LicenseChecker(final Iterable<LicenseDetails> allLicenses, final Clock clock)
        {
            this.licenses = allLicenses;
            this.clock = clock;
        }

        public boolean isEvaluation()
        {
            return isEvaluation(licenses);
        }

        public boolean isEvaluationLicenseOld(final Period period)
        {
            final Iterable<LicenseDetails> allLicenses = ImmutableList.copyOf(licenses);
            final boolean evaluation = isEvaluation(allLicenses);

            Option<LicenseDetails> evaluationLicense = evaluation ?
                    com.atlassian.fugue.Iterables.first(allLicenses) : Option.<LicenseDetails>none();

            final Boolean isEvaluatingEnough = evaluationLicense.fold(Suppliers.alwaysFalse(), new Function<LicenseDetails, Boolean>()
            {
                @Override
                public Boolean apply(final LicenseDetails input)
                {
                    return isLicenseThisOld(input, period);
                }
            });

            return isEvaluatingEnough;
        }

        private boolean isEvaluation(final Iterable<LicenseDetails> allLicenses)
        {
            final boolean allEvaluationLicenses = Iterables.all(allLicenses, new Predicate<LicenseDetails>()
            {
                @Override
                public boolean apply(final LicenseDetails input)
                {
                    return input.isEvaluation();
                }
            });
            return allEvaluationLicenses;
        }

        private Boolean isLicenseThisOld(final LicenseDetails input, final Period period)
        {
            Date licensePurchaseDate = input.getJiraLicense().getPurchaseDate();
            return new DateTime(licensePurchaseDate).withPeriodAdded(period, 1).isBefore(new DateTime(clock.getCurrentDate()));
        }
    }
}
