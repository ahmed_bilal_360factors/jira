package com.atlassian.jira.onboarding.postsetup;

import com.atlassian.fugue.Option;
import com.atlassian.jira.user.ApplicationUser;

/**
 * @since v6.4
 */
public interface AnnouncementAccessPolicy
{
    boolean canRead(Option<ApplicationUser> user);
    boolean canUpdate(Option<ApplicationUser> user);

    AnnouncementFilter getAnnouncementFilter();
}
