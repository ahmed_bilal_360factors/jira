package com.atlassian.jira.onboarding.postsetup;

import java.util.List;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import com.atlassian.jira.onboarding.postsetup.checks.AdminAccountCredentialsChecker;
import com.atlassian.jira.onboarding.postsetup.checks.AnnounceWhenInstantPathTakenCheck;
import com.atlassian.jira.onboarding.postsetup.checks.DatabaseSetUpChecker;
import com.atlassian.jira.onboarding.postsetup.checks.OutgoingMailSetUpChecker;

import com.google.common.collect.ImmutableList;

import org.springframework.stereotype.Component;

/**
 * @since v6.4
 */
@Component
public class PostSetupAnnouncementProvider {
    public static final String DATABASE_SETUP = "database.setup";
    public static final String ADMIN_ACCOUNT_SETUP = "admin.account.setup";
    public static final String APP_PROPERTIES_SETUP = "app.properties.setup";
    public static final String MAIL_PROPERTIES_SETUP = "mail.properties.setup";
    public static final String CU_MAIL_PROPERTIES_SETUP = "create.user.mail.properties.setup";

    private final ImmutableList<PostSetupAnnouncement> annoucements;

    @Inject
    public PostSetupAnnouncementProvider(
            final DatabaseSetUpChecker databaseSetUpChecker,
            final AnnounceWhenInstantPathTakenCheck announceWhenInstantPathTakenCheck,
            final OutgoingMailSetUpChecker mailSetUpChecker,
            final AdminAccountCredentialsChecker adminAccountCredentialsChecker) {
        annoucements = ImmutableList.of(
                new PostSetupAnnouncement(DATABASE_SETUP, databaseSetUpChecker),
                new PostSetupAnnouncement(ADMIN_ACCOUNT_SETUP, adminAccountCredentialsChecker),
                new PostSetupAnnouncement(APP_PROPERTIES_SETUP, announceWhenInstantPathTakenCheck),
                new PostSetupAnnouncement(MAIL_PROPERTIES_SETUP, mailSetUpChecker),
                new PostSetupAnnouncement(CU_MAIL_PROPERTIES_SETUP, mailSetUpChecker)
        );
    }

    @Nonnull
    public List<PostSetupAnnouncement> getAnouncements() {
        return annoucements;
    }
}
