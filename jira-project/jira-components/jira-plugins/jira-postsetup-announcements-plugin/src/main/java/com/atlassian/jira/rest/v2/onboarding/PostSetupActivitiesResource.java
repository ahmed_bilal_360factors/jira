package com.atlassian.jira.rest.v2.onboarding;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.fugue.Option;
import com.atlassian.jira.onboarding.postsetup.PostSetupAnnouncementService;
import com.atlassian.jira.onboarding.postsetup.PostSetupAnnouncementStatus;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

/**
 * @since v6.4
 */

@Scanned
@Path ("postsetup_announcement")
@Consumes ({ MediaType.APPLICATION_JSON })
@Produces ({ MediaType.APPLICATION_JSON })
public class PostSetupActivitiesResource
{
    private final PostSetupAnnouncementService announcements;
    private final JiraAuthenticationContext authContext;

    @Inject
    public PostSetupActivitiesResource(
            final PostSetupAnnouncementService announcements,
            @ComponentImport final JiraAuthenticationContext authContext)
    {
        this.announcements = announcements;
        this.authContext = authContext;
    }

    @GET
    @Path ("ready")
    public Response getReadyAnnouncements()
    {
        final Option<ApplicationUser> applicationUser = Option.option(authContext.getUser());

        final List<PostSetupAnnouncementStatus> readyAnnouncemends = this.announcements.getReadyAnnouncements(applicationUser);

        Iterable<PostSetupActivityStatusBean> readyAnnouncementsBeans =
                Iterables.transform(
                        readyAnnouncemends,
                        PostSetupActivityStatusBean.FROM_ANNOUNCEMENT_STATUS);

        return Response.ok(ImmutableList.copyOf(readyAnnouncementsBeans)).build();
    }

    @GET
    @Path ("reset")
    @VisibleForTesting
    public Response reset()
    {
        final Option<ApplicationUser> applicationUser = Option.option(authContext.getUser());
        this.announcements.reset(applicationUser);

        return Response.ok().build();
    }

    @GET
    @Path ("recalculate")
    public Response recalculate()
    {
        final Option<ApplicationUser> applicationUser = Option.option(authContext.getUser());
        announcements.recalculateStatuses(applicationUser);

        return Response.ok().build();
    }

    @POST
    @Path ("announced")
    public Response acitiviesAnnounced(List<String> announcedActivities)
    {
        final Option<ApplicationUser> applicationUser = Option.option(authContext.getUser());
        announcements.activitiesAnnounced(applicationUser, announcedActivities);

        return Response.ok().build();
    }

    @SuppressWarnings ({ "UnusedDeclaration" })
    @XmlRootElement (name = "postSetupActivityStatus")
    static public class PostSetupActivityStatusBean
    {
        @XmlElement
        private String activityId;
        @XmlElement
        private PostSetupAnnouncementStatus.Status status;

        PostSetupActivityStatusBean(final String activityId, final PostSetupAnnouncementStatus.Status status)
        {
            this.activityId = activityId;
            this.status = status;
        }

        private static Function<PostSetupAnnouncementStatus, PostSetupActivityStatusBean> FROM_ANNOUNCEMENT_STATUS =
                new Function<PostSetupAnnouncementStatus, PostSetupActivityStatusBean>()
                {
                    @Override
                    public PostSetupActivityStatusBean apply(final PostSetupAnnouncementStatus input)
                    {
                        return new PostSetupActivityStatusBean(input.getActivityId(), input.getActivityStatus());
                    }
                };

        public String getActivityId()
        {
            return activityId;
        }

        public PostSetupAnnouncementStatus.Status getStatus()
        {
            return status;
        }
    }
}
