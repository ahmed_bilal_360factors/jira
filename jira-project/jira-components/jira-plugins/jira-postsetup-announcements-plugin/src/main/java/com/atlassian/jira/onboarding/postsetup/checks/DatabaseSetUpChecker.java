package com.atlassian.jira.onboarding.postsetup.checks;

import javax.inject.Inject;

import com.atlassian.jira.onboarding.postsetup.AnnouncementStatusChecker;
import com.atlassian.jira.onboarding.postsetup.PostSetupAnnouncementStatus;

import org.joda.time.Period;
import org.springframework.stereotype.Component;

/**
 * @since v6.4
 */
@Component
public class DatabaseSetUpChecker implements AnnouncementStatusChecker
{
    public static final Period INSECURE_DB_EVALUATION_PERIOD = Period.days(21);
    private final EvaluationLicenseAgeCheck licenseAgeCheck;
    private final InsecureDatabaseCheck databaseCheck;

    @Inject
    public DatabaseSetUpChecker(final EvaluationLicenseAgeCheck licenseAgeCheck, InsecureDatabaseCheck databaseCheck)
    {
        this.licenseAgeCheck = licenseAgeCheck;
        this.databaseCheck = databaseCheck;
    }

    @Override
    public PostSetupAnnouncementStatus.Status computeStatus()
    {
        final EvaluationLicenseAgeCheck.LicenseChecker currentLicenseCheck = licenseAgeCheck.getCurrentLicenseCheck();
        if ( !currentLicenseCheck.isEvaluation() ) {
            return PostSetupAnnouncementStatus.Status.FULLFILLED;
        }

        PostSetupAnnouncementStatus.Status status = PostSetupAnnouncementStatus.Status.AWAITS;
        if ( currentLicenseCheck.isEvaluationLicenseOld(INSECURE_DB_EVALUATION_PERIOD) ) {
            status = databaseCheck.isDatabaseInsecure() ?
                    PostSetupAnnouncementStatus.Status.ANNOUNCE :
                    PostSetupAnnouncementStatus.Status.FULLFILLED;
        }

        return status;
    }
}
