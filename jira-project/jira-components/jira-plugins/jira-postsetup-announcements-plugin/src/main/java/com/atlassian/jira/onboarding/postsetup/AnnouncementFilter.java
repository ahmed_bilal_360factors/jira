package com.atlassian.jira.onboarding.postsetup;

import com.atlassian.fugue.Option;
import com.atlassian.jira.user.ApplicationUser;

/**
 * @since v6.4
 */
public interface AnnouncementFilter
{
    boolean canUserSeeAnnouncement(Option<ApplicationUser> applicationUsers, String announcedActivity);
}
