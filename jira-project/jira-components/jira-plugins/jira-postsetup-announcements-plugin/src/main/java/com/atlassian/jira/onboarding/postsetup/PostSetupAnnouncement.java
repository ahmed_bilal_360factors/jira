package com.atlassian.jira.onboarding.postsetup;

/**
 * @since v6.4
 */
public class PostSetupAnnouncement
{
    private final String anouncementId;
    private final AnnouncementStatusChecker announcementStatusChecker;

    public PostSetupAnnouncement(final String anouncementId, final AnnouncementStatusChecker announcementStatusChecker) {

        this.anouncementId = anouncementId;
        this.announcementStatusChecker = announcementStatusChecker;
    }

    public String getAnouncementId()
    {
        return anouncementId;
    }

    public PostSetupAnnouncementStatus.Status computeStatus(){
        return announcementStatusChecker.computeStatus();
    }
}
