package com.atlassian.jira.onboarding.postsetup.checks;

import javax.inject.Inject;

import com.atlassian.jira.mail.settings.MailSettings;
import com.atlassian.jira.onboarding.postsetup.AnnouncementStatusChecker;
import com.atlassian.jira.onboarding.postsetup.PostSetupAnnouncementStatus;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import org.springframework.stereotype.Component;

/**
 * @since v6.4
 */
@Component
public class OutgoingMailSetUpChecker implements AnnouncementStatusChecker
{
    private final MailSettings mailSettings;
    private final MailServerManager mailServerManager;

    @Inject
    public OutgoingMailSetUpChecker(
            @ComponentImport final MailSettings mailSettings, @ComponentImport final MailServerManager mailServerManager)
    {
        this.mailSettings = mailSettings;
        this.mailServerManager = mailServerManager;
    }

    @Override
    public PostSetupAnnouncementStatus.Status computeStatus()
    {
        if (!mailSettings.send().isModifiable())    // not modifiable -> changed by system property
        {
            return PostSetupAnnouncementStatus.Status.AWAITS;
        }

        final boolean mailConfiugured = mailServerManager.isDefaultSMTPMailServerDefined();
        final boolean mailEnabled = mailSettings.send().isEnabled();

        if (mailConfiugured && mailEnabled)
        {
            return PostSetupAnnouncementStatus.Status.FULLFILLED;
        }
        return PostSetupAnnouncementStatus.Status.ANNOUNCE;
    }
}
