package com.atlassian.jira.onboarding.postsetup.checks;

import com.atlassian.jira.onboarding.postsetup.AnnouncementStatusChecker;
import com.atlassian.jira.onboarding.postsetup.PostSetupAnnouncementStatus;
import org.joda.time.Period;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * @since v6.4
 */
@Component
public class AdminAccountCredentialsChecker implements AnnouncementStatusChecker
{
    public static final Period CREDENTIALS_ANNOUNCE_PERIOD = Period.days(1);
    private final EvaluationLicenseAgeCheck licenseAgeCheck;
    private final AnnounceWhenInstantPathTakenCheck checkInstantPath;

    @Inject
    public AdminAccountCredentialsChecker(final EvaluationLicenseAgeCheck licenseAgeCheck, AnnounceWhenInstantPathTakenCheck checkInstantPath)
    {
        this.licenseAgeCheck = licenseAgeCheck;
        this.checkInstantPath = checkInstantPath;
    }

    @Override
    public PostSetupAnnouncementStatus.Status computeStatus()
    {
        if (!checkInstantPath.isSetUpWithInstantPath())
        {
            return PostSetupAnnouncementStatus.Status.FULLFILLED;
        }
        final EvaluationLicenseAgeCheck.LicenseChecker currentLicenseCheck = licenseAgeCheck.getCurrentLicenseCheck();
        if (!currentLicenseCheck.isEvaluation())
        {
            return PostSetupAnnouncementStatus.Status.FULLFILLED;
        }

        boolean itsTimeToShowAnnouncement = currentLicenseCheck.isEvaluationLicenseOld(CREDENTIALS_ANNOUNCE_PERIOD);
        return itsTimeToShowAnnouncement ?
                PostSetupAnnouncementStatus.Status.ANNOUNCE :
                PostSetupAnnouncementStatus.Status.AWAITS;
    }
}
