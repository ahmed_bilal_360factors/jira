package com.atlassian.jira.onboarding.postsetup;

/**
 * @since v6.4
 */
public class PostSetupAnnouncementStatus
{
    // status flows
    //        /-> FULLFILLED   --- when user i.e. configured given part of jira (fullfilled status given by AnnouncementStatusChecker)
    // AWAITS -> ANNOUNCE -> ANNOUNCED  --- when announcement is ready to be shown (i.e. some time passed)

    public enum Status {
        AWAITS,
        ANNOUNCE,
        FULLFILLED, ANNOUNCED
    }
    private final String activityId;
    private final Status activityStatus;

    public PostSetupAnnouncementStatus(final String activityId, final Status activityStatus)
    {
        this.activityId = activityId;
        this.activityStatus = activityStatus;
    }

    public Status getActivityStatus()
    {
        return activityStatus;
    }

    public String getActivityId()
    {
        return activityId;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final PostSetupAnnouncementStatus that = (PostSetupAnnouncementStatus) o;

        if (!activityId.equals(that.activityId)) { return false; }
        if (!activityStatus.equals(that.activityStatus)) { return false; }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = activityId.hashCode();
        result = 31 * result + activityStatus.hashCode();
        return result;
    }

    @Override
    public String toString()
    {
        return "PostSetupActivityStatus{" +
                "activityId='" + activityId + '\'' +
                ", activityStatus=" + activityStatus +
                '}';
    }
}
