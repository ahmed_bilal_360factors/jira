package com.atlassian.jira.onboarding.postsetup.checks;

import com.atlassian.jira.util.system.SystemInfoUtils;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import com.google.common.collect.ImmutableSet;

import java.util.Set;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

/**
 * @since v6.4
 */
@Component
public class InsecureDatabaseCheck
{
    private final Set<String> INSECURE_DATABASE_TYPES = ImmutableSet.of("hsql", "h2");

    private final SystemInfoUtils systemInfoUtils;

    @Inject
    public InsecureDatabaseCheck(@ComponentImport final SystemInfoUtils systemInfoUtils) {
        this.systemInfoUtils = systemInfoUtils;
    }

    public boolean isDatabaseInsecure() {
        final String databaseType = systemInfoUtils.getDatabaseType().toLowerCase();

        return INSECURE_DATABASE_TYPES.contains(databaseType);
    }
}
