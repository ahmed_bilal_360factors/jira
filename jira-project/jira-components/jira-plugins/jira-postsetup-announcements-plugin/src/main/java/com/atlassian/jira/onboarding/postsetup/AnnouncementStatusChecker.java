package com.atlassian.jira.onboarding.postsetup;

/**
 * @since v6.4
 */
public interface AnnouncementStatusChecker
{
    PostSetupAnnouncementStatus.Status computeStatus();
}
