package com.atlassian.jira.onboarding.postsetup;

import java.util.List;

import javax.annotation.Nonnull;

import com.atlassian.jira.util.NotNull;

/**
 * @since v6.4
 */
public interface PostSetupAnnouncementStore
{
    @Nonnull
    List<PostSetupAnnouncementStatus> getAllStatuses();

    void updateStatuses(@Nonnull Iterable<PostSetupAnnouncementStatus> postSetupActivityStatuses);
}
