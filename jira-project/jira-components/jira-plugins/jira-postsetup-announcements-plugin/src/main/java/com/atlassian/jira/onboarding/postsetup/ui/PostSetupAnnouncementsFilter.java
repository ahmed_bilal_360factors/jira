package com.atlassian.jira.onboarding.postsetup.ui;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.atlassian.fugue.Option;
import com.atlassian.jira.onboarding.postsetup.PostSetupAnnouncementService;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

/**
 * @since v6.4
 */
@Scanned
public class PostSetupAnnouncementsFilter implements Filter
{
    private final JiraWebResourceManager webResourceManager;
    private final PostSetupAnnouncementService postSetupAnnouncements;
    private final JiraAuthenticationContext authContext;

    @Inject
    public PostSetupAnnouncementsFilter(
            @ComponentImport final JiraWebResourceManager webResourceManager,
            final PostSetupAnnouncementService postSetupAnnouncements,
            @ComponentImport final JiraAuthenticationContext authContext)
    {
        this.webResourceManager = webResourceManager;
        this.postSetupAnnouncements = postSetupAnnouncements;
        this.authContext = authContext;
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException
    {
        final Option<ApplicationUser> applicationUser = Option.option(authContext.getUser());

        try
        {
            // this is candidate to move to some scheduled job
            postSetupAnnouncements.recalculateStatuses(applicationUser);
        }
        catch (SecurityException e)
        {
            // if user can't do the recalculation i don't care.
        }
        if (postSetupAnnouncements.hasAnnouncements(applicationUser))
        {
            webResourceManager.requireResource("com.atlassian.jira.jira-postsetup-announcements-plugin:post-setup-announcements");
        }
        chain.doFilter(request, response);
    }

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException
    {
    }

    @Override
    public void destroy()
    {
    }
}
