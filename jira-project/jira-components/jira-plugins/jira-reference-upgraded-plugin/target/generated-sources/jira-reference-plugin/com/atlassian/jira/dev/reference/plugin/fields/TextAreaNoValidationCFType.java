package com.atlassian.jira.dev.reference.plugin.fields;

import java.util.Collection;

import javax.annotation.Nonnull;

import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.security.JiraAuthenticationContext;

import com.opensymphony.util.TextUtils;

public class TextAreaNoValidationCFType extends GenericTextCFType
{

    public TextAreaNoValidationCFType(CustomFieldValuePersister customFieldValuePersister,
            GenericConfigManager genericConfigManager, TextFieldCharacterLengthValidator textFieldCharacterLengthValidator, final JiraAuthenticationContext jiraAuthenticationContext)
    {
        super(customFieldValuePersister, genericConfigManager, textFieldCharacterLengthValidator, jiraAuthenticationContext);
    }

    @Nonnull
    @Override
    protected PersistenceFieldType getDatabaseType()
    {
        return PersistenceFieldType.TYPE_UNLIMITED_TEXT;
    }

    @Override
    public String getValueFromCustomFieldParams(final CustomFieldParams relevantParams) throws FieldValidationException
    {
        // copied from super super class AbstractSingleFieldType in order to bypass validation code from direct superclass GenericTextCFType
        if (relevantParams == null)
        {
            return null;
        }

        Collection normalParams = relevantParams.getValuesForKey(null); //single field types should not scope their parameters
        if (normalParams == null || normalParams.isEmpty())
        { return null; }

        String singleParam = (String) normalParams.iterator().next();
        if (TextUtils.stringSet(singleParam)) //only validate if the value is set
        {
            return getSingularObjectFromString(singleParam);
        }
        else
        {
            return null;
        }
    }

    @Override
    public Object accept(VisitorBase visitor)
    {
        if (visitor instanceof Visitor)
        {
            return ((Visitor) visitor).visitBrokenTextArea(this);
        }

        return super.accept(visitor);
    }

    public interface Visitor<T> extends VisitorBase<T>
    {
        T visitBrokenTextArea(TextAreaNoValidationCFType textAreaCustomFieldType);
    }
}
