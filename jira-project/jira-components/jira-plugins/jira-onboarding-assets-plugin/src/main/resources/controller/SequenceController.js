/* global Promise */
define('jira/onboarding/sequence-controller', [
    'jquery',
    'underscore',
    'jira/onboarding/analytics'
], function ($, _, OnboardingAnalytics) {
    'use strict';

    /**
     * Class used to represent a sequence definition
     * @name FirstUseFlow
     * @global
     *
     * @property {Sequence} instance
     * @property {String|Function} resolve, if a string this is the key to what sequence it should go to. If it is a
     *      function this is executed and the string returned is what sequence it goes to next. If null or nothing
     *      returned it does not do another sequence.
     * @property {String|Function} reject, if a string this is the key to what sequence it should go to. If it is a
     *      function this is executed and the string returned is what sequence it goes to next. If null or nothing
     *      returned it does not do another sequence.
     **/

    /**
     * @typedef {Object} Sequence
     * @global
     *
     * @property {SequenceInitFunction} init
     */

    /**
     * @typedef {Function} SequenceInitFunction
     * @global
     *
     * @param {Node} container used to render the Sequence
     * @param {OnboardingAnalytics} [analytics] object for recording analytics events
     * @returns {Promise} for when the Sequence is complete
     */

    /**
     * @typedef {Object} SequenceControllerOptions
     *
     * @property {Node} container to place any sequence content
     * @property {FirstUseFlowFactory} factory to create a flow for the controller
     * @property {String} [currentFlowKey] current flow that the server thinks we are completing
     *
     */


    /**
     * Create a sequence controller
     * @class SequenceController
     *
     * @param {SequenceControllerOptions} options
     * @constructor
     */
    var SequenceController = function(options) {
        options = options || {};
        if (!options.container) {
            throw new Error('Should have passed a container to put the sequence content');
        } else {
            var isjQuery = options.container instanceof $;
            var isDomNode = options.container instanceof HTMLElement;
            if (isjQuery) {
                options.container = options.container.get(0);
            }
            if (!isjQuery && !isDomNode) {
                throw new Error('Container must be a DOM node');
            }
        }
        if (!options.flow) {
            throw new Error('Should have flow information for controller');
        }
        if (typeof options.flow !== 'function') {
            throw new Error('First use flow should be a function');
        }

        this.container = options.container;
        this.flow = options.flow(_.bind(this.complete, this));
        this.currentFlowKey = options.flowKey;


        if (!this.flow.sequences) {
            throw new Error('Should have sequences in the controller');
        }

        if (!this.flow.start) {
            throw new Error('Should have a start point for the flow');
        }

        if (!this.flow.sequences[this.flow.start]) {
            throw new Error('Start point should actually be a sequence');
        }
    };

    /**
     * Start with the first sequence. If there is non supplied by the server it begins at the beginning.
     *
     * @param {String} [sequenceKey] to start, if not provided it starts at the default start position.
     */
    SequenceController.prototype.start = function(sequenceKey) {
        if (!sequenceKey || !this.hasSequence(sequenceKey)) {
            sequenceKey = this.flow.start;
        }

        this._executeSequence(sequenceKey);
    };

    /**
     * Execute a sequence binding to the promise returned by the sequence init function
     * @param sequenceKey to begin executing
     * @private
     */
    SequenceController.prototype._executeSequence = function(sequenceKey) {
        var instance = this;
        var sequence = this.flow.sequences[sequenceKey];

        var element = document.createElement('div');
        $(this.container).html(element);

        var analytics = new OnboardingAnalytics(this.flow.key, sequenceKey);
        analytics.pushEvent(OnboardingAnalytics.EVENTS.STARTED_SEQUENCE);

        sequence.instance.init(element, analytics).then(function() {
            instance._completeSequence(sequence, 'resolve', arguments);
        }, function() {
            instance._completeSequence(sequence, 'reject', arguments);
        });
    };

    /**
     * Called when a sequence is finished. Calls the corresponding function, resolve or reject and executes the
     * next sequence if there is one.
     * @param {Object} sequence that was just completed
     * @param {String} result either 'resolve' or reject
     * @param {Object[]} resultArguments returned by the promise
     * @returns {boolean}
     * @private
     */
    SequenceController.prototype._completeSequence = function(sequence, result, resultArguments) {
        var nextSequenceKey;
        if (typeof sequence[result] === 'string') {
            nextSequenceKey = sequence[result];
        } else if (typeof sequence[result] === 'function') {
            nextSequenceKey = sequence[result].apply(this, resultArguments);
        }

        // TODO: log if wrong sequence key
        //If there is no next sequence key or the sequence cannot be found
        if (!nextSequenceKey || !this.hasSequence(nextSequenceKey)) {
            return false;
        }

        this._updateCurrentSequence(nextSequenceKey);
        this._executeSequence(nextSequenceKey);
    };

    /**
     * Checks whether the flow has the given sequence key
     * @param sequenceKey to check
     * @returns {boolean} whether it is in the flow
     */
    SequenceController.prototype.hasSequence = function(sequenceKey) {
        return (this.flow.sequences[sequenceKey] !== undefined);
    };

    /**
     * Tell the server what the current sequence you are up to.
     * @param sequenceKey
     */
    SequenceController.prototype._updateCurrentSequence = function(sequenceKey) {
        if (this.currentFlowKey) {
            $.post(createCurrentSequenceUrl(this.currentFlowKey, sequenceKey));
        }
    };

    /**
     * Tell the server that we have completed the flow
     */
    SequenceController.prototype.complete = function() {
        return new Promise(_.bind(function(resolve) {
            if (this.currentFlowKey) {
                $.post(createCompleteFlowUrl(this.currentFlowKey)).always(resolve);
            } else {
                resolve();
            }
        }, this));
    };


    /**
     * Create url for completing a flow.
     * @param flow that is being completed
     * @returns {string} REST URL for posting to server.
     */
    function createCompleteFlowUrl(flow) {
        return AJS.contextPath() + '/rest/onboarding/1.0/flow/' + flow + '/complete';
    }

    /**
     * Generate the url to update the current sequence
     * @param flow completing
     * @param sequence that is being start
     * @returns {string} REST URL for updating
     */
    function createCurrentSequenceUrl(flow, sequence) {
        return AJS.contextPath() + '/rest/onboarding/1.0/flow/' + flow + '/sequence/current/' + sequence;
    }

    return SequenceController;
});
