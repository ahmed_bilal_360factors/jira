/* global AJS, _, require, QUnit, sinon, module, test, asyncTest, expect, equal, ok */
AJS.test.require('com.atlassian.jira.jira-onboarding-assets-plugin:sequence-controller-component', function() {
    'use strict';

    var SequenceController;

    var DEFAULT_FLOW_KEY = 'flowKey';

    var mockAnalytics = {
        pushEvent: function() {}
    };
    var MOCK_ANALYTICS = function() {
        return mockAnalytics;
    };

    MOCK_ANALYTICS.EVENTS = {
        STARTED_SEQUENCE: 'started',
        COMPLETED: 'completed',
        FAILED: 'failed'
    };

    module('jira/onboarding/sequence-controller - construction', {
        setup: function() {
            this.context = AJS.test.context();
            this.context.mock('jira/onboarding/analytics', MOCK_ANALYTICS);
            SequenceController = this.context.require('jira/onboarding/sequence-controller');
        }
    });

    test('Needs a non-null container value', 1, function() {
        try {
            new SequenceController();
        } catch (e) {
            ok(e.message.indexOf('Should have passed a container') > -1, 'should complain about not having an element');
        }
    });

    test('Needs a DOM element for a container', 1, function() {
        try {
            new SequenceController({
                container: {}
            });
        } catch (e) {
            ok(e.message.indexOf('DOM') > -1, 'should complain about not having an element');
        }
    });

    test('Needs a flow function', 2, function() {
        try {
            new SequenceController({
                container: document.createElement('div'),
                flow: {}
            });
        } catch (e) {
            ok(e.message.indexOf('flow') > -1, 'should be something about a flow');
            ok(e.message.indexOf('function') > -1, 'should complain about not having a function');
        }
    });

    test('Can be initialised successfully', function() {
        var controller = new SequenceController({
            container: document.createElement('div'),
            flow: function() {
                return {
                    start: 'a',
                    sequences: {
                        'a': {
                            instance: new (function sequence() {})
                        }
                    }
                }
            }
        });
        equal(typeof controller.start, 'function');
    });

    module('jira/onboarding/sequence-controller', {
        setup: function () {
            this.server = sinon.fakeServer.create();
            this.element = AJS.$('#qunit-fixture');

            this.context = AJS.test.context();
            this.context.mock('jira/onboarding/analytics', MOCK_ANALYTICS);
            SequenceController = this.context.require('jira/onboarding/sequence-controller');
        },

        teardown: function () {
            this.server.restore();
        }
    });

    test('If no current sequence then it begins at the default start sequence', function() {
        expect(1);
        var flow = function() {
            return {
                start: 'b',
                sequences: {
                    'a': {
                        instance: {
                            init: function() {
                                ok(false);
                                return Promise.resolve();
                            }
                        }
                    },
                    'b': {
                        instance: {
                            init: function() {
                               ok(true);
                               return Promise.resolve();
                            }
                        }
                    }
                }
            };
        };


        var controller = new SequenceController({
            container: this.element,
            flow: flow
        });
        controller.start();
    });

    test('If a server stored sequence is defined it starts on that one', function() {
        expect(1);
        var flow = function() {
            return {
                start: 'a',
                sequences: {
                    'a': {
                        instance: {
                            init: function() {
                                ok(false);
                                return Promise.resolve();
                            }
                        }
                    },
                    'b': {
                        instance: {
                            init: function() {
                                ok(true);
                                return Promise.resolve();
                            }
                        }
                    }
                }
            };
        };

        var controller = new SequenceController({
            container: this.element,
            flow: flow
        });
        controller.start('b');
    });

    asyncTest('If a string is supplied to the resolve/reject key that is the sequence that is completed next', function() {
        expect(4);
        var flow = function() {
            return {
                start: 'a',
                sequences: {
                    'a': {
                        instance: {
                            init: function() {
                                ok(true);
                                return Promise.resolve();
                            }
                        },
                        resolve: 'b'
                    },
                    'b': {
                        instance: {
                            init: function() {
                                ok(true);
                                return Promise.resolve();
                            }
                        }
                    }
                }
            };
        };

        var controller = new SequenceController({
            container: this.element,
            flow: flow,
            flowKey: DEFAULT_FLOW_KEY
        });
        controller.start();

        var server = this.server;

        _.defer(function() {
            equal(server.requests.length, 1, 'Should have a request that it is running a new sequence');
            equal(server.requests[0].url, AJS.contextPath() + '/rest/onboarding/1.0/flow/flowKey/sequence/current/b', 'Should indicate starting second');
            QUnit.start();
        });
    });

    asyncTest('If a function is supplied to the resolve/reject key the result is the sequence to next be completed', function() {
        expect(4);
        var flow = function() {
            return {
                start: 'a',
                sequences: {
                    'a': {
                        instance: {
                            init: function() {
                                ok(true);
                                return Promise.resolve();
                            }
                        },
                        resolve: function() {
                            return 'b';
                        }
                    },
                    'b': {
                        instance: {
                            init: function() {
                                ok(true);
                                return Promise.resolve();
                            }
                        }
                    }
                }
            };
        };

        var controller = new SequenceController({
            container: this.element,
            flow: flow,
            flowKey: DEFAULT_FLOW_KEY
        });
        controller.start();

        var server = this.server;

        _.defer(function() {
            equal(server.requests.length, 1, 'Should have a request that it is running a new sequence');
            equal(server.requests[0].url, AJS.contextPath() + '/rest/onboarding/1.0/flow/flowKey/sequence/current/b', 'Should indicate starting third');
            QUnit.start();
        });
    });

    asyncTest('If a resolution results in complete being called a post request is sent', function() {
        expect(2);
        var flow = function(complete) {
            return {
                start: 'a',
                sequences: {
                    'a': {
                        instance: {
                            init: function() {
                                return Promise.resolve();
                            }
                        },
                        resolve: function() {
                            complete();
                        }
                    }
                }
            };
        };

        var controller = new SequenceController({
            container: this.element,
            flow: flow,
            flowKey: DEFAULT_FLOW_KEY
        });
        controller.start();

        var server = this.server;

        _.defer(function() {
            equal(server.requests.length, 1, 'Should have a request that it is running a new sequence');
            equal(server.requests[0].url, AJS.contextPath() + '/rest/onboarding/1.0/flow/flowKey/complete', 'Should indicate completed flow');
            QUnit.start();
        });
    });


    asyncTest('Test that data is passed from resolution to resolve function if supplied', function() {
        expect(1);
        var flow = function() {
            return {
                start: 'a',
                sequences: {
                    'a': {
                        instance: {
                            init: function() {
                                return Promise.resolve(6);
                            }
                        },
                        resolve: function(reason) {
                            equal(reason, 6);
                            QUnit.start();
                        }
                    }
                }
            };
        };

        var controller = new SequenceController({
            container: this.element,
            flow: flow
        });
        controller.start();
    });

    asyncTest('Test that data is passed from rejection to reject function if supplied', function() {
        expect(1);
        var flow = function() {
            return {
                start: 'a',
                sequences: {
                    'a': {
                        instance: {
                            init: function() {
                                return Promise.reject(6);
                            }
                        },
                        reject: function(reason) {
                            equal(reason, 6);
                            QUnit.start();
                        }
                    }
                }
            };
        };

        var controller = new SequenceController({
            container: this.element,
            flow: flow
        });
        controller.start();
    });

    asyncTest('sequences are passed an element and analytics object', function() {
        expect(3);
        var sequenceA = {
            init: sinon.spy(function() {
                return Promise.resolve();
            })
        };
        function runAssertions() {
            equal(sequenceA.init.callCount, 1);
            ok(sequenceA.init.args[0][0] instanceof HTMLElement, 'first argument should be an element');
            equal(sequenceA.init.args[0][1], mockAnalytics, 'second argument should be the analytics object');
            QUnit.start();
        }
        var flow = function() {
            return {
                start: 'a',
                sequences: {
                    'a': {
                        instance: sequenceA,
                        resolve: runAssertions
                    }
                }
            };
        };

        var controller = new SequenceController({
            container: this.element,
            flow: flow
        });
        controller.start();
    });

    asyncTest('events triggered (and handled) in one sequence should not be triggered in subsequent ones', function() {
        expect(3);
        function sequenceWithAView(name) {
            return {
                clickHandler: sinon.spy(),
                init: function(element) {
                    element.addEventListener('click', this.clickHandler);
                    element.click();
                    return Promise.resolve();
                }
            }
        }
        var sequenceA = sequenceWithAView('a');
        var sequenceB = sequenceWithAView('b');
        var sequenceC = sequenceWithAView('c');
        function runAssertions() {
            equal(sequenceA.clickHandler.callCount, 1);
            equal(sequenceB.clickHandler.callCount, 1);
            equal(sequenceC.clickHandler.callCount, 1);
            QUnit.start();
        }
        var flow = function() {
            return {
                start: 'a',
                sequences: {
                    'a': {
                        instance: sequenceA,
                        resolve: 'b'
                    },
                    'b': {
                        instance: sequenceB,
                        resolve: 'c'
                    },
                    'c': {
                        instance: sequenceC,
                        resolve: runAssertions
                    }
                }
            };
        };

        var controller = new SequenceController({
            container: this.element,
            flow: flow
        });
        controller.start();
    });
});
