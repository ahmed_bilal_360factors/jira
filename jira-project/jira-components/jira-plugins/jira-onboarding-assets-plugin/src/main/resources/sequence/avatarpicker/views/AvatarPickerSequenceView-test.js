AJS.test.require("com.atlassian.jira.jira-onboarding-assets-plugin:avatar-picker-component", function() {
    "use strict"


    var AvatarPickerSequenceView = require('jira/onboarding/avatar-picker-sequence-view');

    var STUB_AVATARS = {
        system:[
            {"id":"10070","isSystemAvatar":true,"isSelected":false,"urls":{"48x48":"http://localhost:8090/jira/secure/useravatar?avatarId=10070"},"selected":false},
            {"id":"10071","isSystemAvatar":true,"isSelected":false,"urls":{"48x48":"http://localhost:8090/jira/secure/useravatar?avatarId=10071"},"selected":false},
            {"id":"10072","isSystemAvatar":true,"isSelected":true,"urls":{"48x48":"http://localhost:8090/jira/secure/useravatar?avatarId=10072"},"selected":false}
        ],
        custom:[
            {"id":"10073","isSystemAvatar":false,"isSelected":false,"urls":{"48x48":"http://localhost:8090/jira/secure/useravatar?avatarId=10073"},"selected":false},
            {"id":"10074","isSystemAvatar":false,"isSelected":false,"urls":{"48x48":"http://localhost:8090/jira/secure/useravatar?avatarId=10074"},"selected":false}
        ]
    };

    var FULLNAME = "bob bob";
    var USERNAME = "myName";
    var DEFAULT_AVATAR_ID = "10092";
    var DEFAULT_AVATAR_URL = "http://url.com";

    module('onboarding/avatar-picker-sequence-view', {
        setup: function () {

            this.server = sinon.fakeServer.create();

            this.element = AJS.$("#qunit-fixture");

            this.view = new AvatarPickerSequenceView({
                el: this.element,
                username: USERNAME,
                userFullname: FULLNAME,
                userAvatarId: DEFAULT_AVATAR_ID,
                userAvatarUrl: DEFAULT_AVATAR_URL,
                defaultUserAvatarId: DEFAULT_AVATAR_ID
            });

            self.completed = false;
            this.view.on("done", _.bind(function() {
                this.completed = true;
            }, this));

            this.view.render();

            this.clickChooseAvatar = _.bind(function() {
                AJS.$(".choose-avatar-button").click();
                if (!this.receivedAvatars) {
                    this.server.requests[this.server.requests.length - 1].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_AVATARS));
                    this.receivedAvatars = true;
                }
            }, this);

            this.selectAvatar = _.bind(function(avatarImage) {
                avatarImage.click();
                this.server.requests[this.server.requests.length - 1].respond(200, { "Content-Type": "application/json" }, JSON.stringify({}));
            }, this);
        },

        teardown: function () {
            this.server.restore();
            getAvatarPickerDialog().remove();
            AJS.$(".aui-blanket").remove();
        }
    });

    function getAvatarPickerDialog() {
        return AJS.$("#user-avatar-picker");
    }

    test("Test current avatar id and src used", function() {
        var selectedImage = AJS.$(".selected-avatar-image", this.element);
        equal(selectedImage.attr('data-id'), DEFAULT_AVATAR_ID, "Avatar on screen's id should be the one in the meta tag");
        equal(selectedImage.attr('src'), DEFAULT_AVATAR_URL, "Avatar on screen's url should be the one in the meta tag");
    });

    test("Test clicking next calls the resolve function", function() {
        ok(!this.completed, "Should not be completed until next is clicked");
        this.element.find(".avatar-picker-done").click();
        ok(this.completed, "Should have called the resolved function on completion");
    });

    test("Test clicking choose an avatar brings up the dialog", function() {
        ok(!getAvatarPickerDialog().is(":visible"), "Dialog should not be shown at the start");
        this.clickChooseAvatar();
        ok(getAvatarPickerDialog().is(":visible"), "Dialog should be shown at the start after button click");
    });

    test("Test avatar picker dialog cancel button closes dialog", function() {
        this.clickChooseAvatar();

        var avatarPickerDialog = getAvatarPickerDialog();

        ok(avatarPickerDialog.is(":visible"), "Dialog should be shown at the start after button click");

        AJS.$(".cancel", avatarPickerDialog).click();

        ok(!avatarPickerDialog.is(":visible"), "Dialog should be closed on cancel click");

    });

    test("Test avatars from server are displayed onto dialog", function() {
        this.clickChooseAvatar();

        var avatarPickerDialog = getAvatarPickerDialog();

        var avatars = AJS.$(".jira-avatar", avatarPickerDialog);

        var expectedAvatarCount = STUB_AVATARS.custom.length + STUB_AVATARS.system.length;
        equal(avatars.length, expectedAvatarCount, "Should have created avatars for all returned avatars");
    });

    test("Test selecting an avatar does a post to the server to update it", function() {
        this.clickChooseAvatar();

        var avatarPickerDialog = getAvatarPickerDialog();

        var avatars = AJS.$(".jira-avatar", avatarPickerDialog);

        var currentRequestLength = this.server.requests.length;

        avatars.first().find("img").click();

        equal(this.server.requests.length, currentRequestLength + 1, "clicking an avatar should do a post to the server");
        var url = this.server.requests[this.server.requests.length - 1].url.split("?")[0];
        equal(url, "/rest/api/latest/user/avatar", "Expected to start with given url");
    });

    test("Test selecting an avatar closes the dialog", function() {
        this.clickChooseAvatar();

        var avatarPickerDialog = getAvatarPickerDialog();

        var avatars = AJS.$(".jira-avatar", avatarPickerDialog);

        this.selectAvatar(avatars.first().find("img"));

        ok(!avatarPickerDialog.is(":visible"), "selecting an avatar should hide the dialog");
    });

    test("Test selecting an avatar changes the avatar display on the page", function() {
        this.clickChooseAvatar();

        var avatarPickerDialog = getAvatarPickerDialog();

        var avatars = AJS.$(".jira-avatar", avatarPickerDialog);

        this.selectAvatar(avatars.first().find("img"));

        var expectedId = STUB_AVATARS.system[0].id;
        var expectedUrl = STUB_AVATARS.system[0].urls["48x48"];

        var selectedImage = AJS.$(".selected-avatar-image", this.element);
        equal(selectedImage.attr('data-id'), expectedId, "Selecting an avatar should update the avatar id");
        equal(selectedImage.attr('src'), expectedUrl, "Selecting an avatar should update the avatar url");
    });

    test("Test selecting an avatar changes the avatar displayed in the header", function() {

        //Add header icon
        AJS.$("#qunit-fixture").append('<div id="user-options"><span class="aui-avatar-inner"><img src="/jira/secure/useravatar?size=small&amp;avatarId=10092" alt="�dmin <A New Beginning>"></span></div>');

        this.clickChooseAvatar();

        var avatarPickerDialog = getAvatarPickerDialog();

        var avatars = AJS.$(".jira-avatar", avatarPickerDialog);

        this.selectAvatar(avatars.first().find("img"));

        var expectedUrl = STUB_AVATARS.system[0].urls["48x48"];

        equal(AJS.$("#qunit-fixture").find("#user-options img").attr('src'), expectedUrl + "&size=small", "Header avatar image should be updated");
    });

    test("Primary button moves from choose avatar to next on avatar selection", function() {

        ok(AJS.$(".choose-avatar-button", this.element).hasClass('aui-button-primary'), "Choose avatar button should be primary at start");
        ok(!AJS.$(".avatar-picker-done", this.element).hasClass('aui-button-primary'), "Next button should not be primary at start");

        this.clickChooseAvatar();
        var avatarPickerDialog = getAvatarPickerDialog();

        var avatars = AJS.$(".jira-avatar", avatarPickerDialog);
        this.selectAvatar(avatars.first().find("img"));

        ok(!AJS.$(".choose-avatar-button", this.element).hasClass('aui-button-primary'), "Choose avatar button should not be primary after avatar selection");
        ok(AJS.$(".avatar-picker-done", this.element).hasClass('aui-button-primary'), "Next button should be primary after avatar selection");
    });



    test("Primary button does not move from choose avatar to next on avatar dialog cancel", function() {

        ok(AJS.$(".choose-avatar-button", this.element).hasClass('aui-button-primary'), "Choose avatar button should be primary at start");
        ok(!AJS.$(".avatar-picker-done", this.element).hasClass('aui-button-primary'), "Next button should not be primary at start");

        this.clickChooseAvatar();
        var avatarPickerDialog = getAvatarPickerDialog();
        AJS.$(".cancel", avatarPickerDialog).click();

        ok(AJS.$(".choose-avatar-button", this.element).hasClass('aui-button-primary'), "Choose avatar button should still be primary on cancel");
        ok(!AJS.$(".avatar-picker-done", this.element).hasClass('aui-button-primary'), "Next button should still not be primary on cancel");
    });
});
