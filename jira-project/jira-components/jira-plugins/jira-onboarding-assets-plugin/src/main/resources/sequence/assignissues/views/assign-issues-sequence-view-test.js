/* global Promise, require, sinon, module, test, ok, equal */
AJS.test.require("com.atlassian.jira.jira-onboarding-assets-plugin:assign-issues-component", function() {
    "use strict";

    var $ = require("jquery");
    var Backbone = require("backbone");

    var fakeIssueOne = {
        "id": 11110,
        "key": "FAKE-1",
        "issueType": 3,
        "summary": "A fake issue"
    };

    module("jira/onboarding/assignissues/views/assign-issues-sequence-view", {
        setup: function() {
            this.$el = $("<div></div>").appendTo("#qunit-fixture");
            this.mockWidget = sinon.spy(Backbone.View);
            this.context = AJS.test.context();
            this.context.mock("jira/onboarding/assignissues/widgets/issue-assignee-picker-widget", this.mockWidget);
        },
        newView: function(opts) {
            opts = opts || {};
            var options = $.extend(true, { el: this.$el }, opts);
            var View = this.context.require("jira/onboarding/assignissues/views/assign-issues-sequence-view");
            return new View(options);
        }
    });

    test("can render the view", function() {
        this.newView().render();
        var html = this.$el.html();
        ok($.trim(html).length > 0, "should have rendered some HTML.");
    });

    test("renders issues created via the project issue creator widget", function() {
        this.newView({ issues: [fakeIssueOne] }).render();
        ok(this.$el.html().indexOf(fakeIssueOne.summary) > -1, "should have rendered the issue data");
    });

    test("fires a done event when the user clicks the next button", function() {
        var view = this.newView({ issues: [fakeIssueOne] }).render();
        var $nextButton = view.$("#next");
        var onNext = sinon.spy();
        view.on("done", onNext);

        $nextButton.click();
        equal(onNext.callCount, 1, "should have triggered a done event");
    });

    test("fires a skip event when the user clicks the skip link", function() {
        var view = this.newView().render();
        var onSkip = sinon.spy();
        view.on("skip", onSkip);
        view.$("#skip").click();
        equal(onSkip.callCount, 1, "should have triggered a skip event");
    });

});
