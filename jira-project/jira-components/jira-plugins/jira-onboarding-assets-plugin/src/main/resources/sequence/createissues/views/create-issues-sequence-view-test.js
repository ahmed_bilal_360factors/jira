/* global Promise, require, sinon, module, test, ok, equal */
AJS.test.require("com.atlassian.jira.jira-onboarding-assets-plugin:create-issues-component", function() {
    "use strict";

    var $ = require("jquery");
    var Backbone = require("backbone");

    var fakeWidget = new Backbone.View();
    fakeWidget.init = function() {};

    var fakeIssueOne = {
        "id": 11110,
        "key": "FAKE-1",
        "issueType": 3,
        "summary": "A fake issue"
    };

    module("CreateIssuesSequence", {
        setup: function() {
            this.$el = $("<div></div>").appendTo("#qunit-fixture");
            this.context = AJS.test.context();
            this.context.mock("jira/onboarding/createissues/issuecreator/project-issue-create-widget", function() {
                return fakeWidget;
            });
            this.sandbox = sinon.sandbox.create();
        },
        teardown: function() {
            this.sandbox.restore();
        },
        newView: function(opts) {
            opts = opts || {};
            var options = $.extend(true, { el: this.$el }, opts);
            var View = this.context.require("jira/onboarding/createissues/views/create-issues-sequence-view");
            return new View(options);
        }
    });

    test("can render the view", function() {
        this.newView().render();
        var html = this.$el.html();
        ok($.trim(html).length > 0, "should have rendered some HTML.");
    });

    test("renders project data", function() {
        var template = this.sandbox.spy(JIRA.Onboarding.Sequence.CreateIssues, "render");
        var projectData = { name: "the project's name is foo" };
        this.newView({model: projectData}).render();
        ok(template.calledOnce);
        equal(typeof template.args[0][0], "object");
        equal(template.args[0][0].projectName, projectData.name);
    });

    test("issue table hidden when there are no issues", function() {
        var view = this.newView().render();
        var $issueTable = view.$(".issue-table");
        equal($issueTable.length, 1, "should have an issue table");
        equal($issueTable.is(":hidden"), true, "issue table with no issues is hidden");
    });

    test("issue table visible when issue is created", function() {
        var view = this.newView().render();
        fakeWidget.trigger("issueCreated", fakeIssueOne);
        var $issueTable = view.$(".issue-table");
        equal($issueTable.length, 1, "should have an issue table");
        equal($issueTable.is(":visible"), true, "issue table should be visible");
    });

    test("renders sequence controls", function() {
        var view = this.newView().render();
        var $nextButton = view.$("#next");
        var $skipButton = view.$("#skip");
        equal($nextButton.length, 1, "should be a next button");
        equal($nextButton.is(":disabled"), true, "next button should not be interactive");
        equal($skipButton.length, 1, "should be a skip button");
        equal($skipButton.is(":disabled"), false, "should always be skippable");
    });

    test("renders issues created via the project issue creator widget", function() {
        this.newView().render();
        ok(this.$el.html().indexOf(fakeIssueOne.summary) === -1, "should not have issue data");
        fakeWidget.trigger("issueCreated", fakeIssueOne);
        ok(this.$el.html().indexOf(fakeIssueOne.summary) > -1, "should have rendered the issue data");
    });

    test("enables the next button when the user has created an issue", function() {
        var view = this.newView().render();
        var $nextButton = view.$("#next");
        fakeWidget.trigger("issueCreated", fakeIssueOne);
        equal($nextButton.is(":disabled"), false, "next button should be clickable now");
    });

    test("fires a done event when the user clicks the next button", function() {
        var view = this.newView().render();
        var $nextButton = view.$("#next");
        var onNext = sinon.spy();
        view.on("done", onNext);

        $nextButton.click();
        equal(onNext.callCount, 0, "should not have triggered a done event because the button is still disabled");

        fakeWidget.trigger("issueCreated", fakeIssueOne);
        // The button should be enabled now.
        $nextButton.click();
        equal(onNext.callCount, 1, "should have triggered a done event");
    });

    test("fires a skip event when the user clicks the skip link", function() {
        var view = this.newView().render();
        var onSkip = sinon.spy();
        view.on("skip", onSkip);
        view.$("#skip").click();
        equal(onSkip.callCount, 1, "should have triggered a skip event");
    });

});
