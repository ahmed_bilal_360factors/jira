/* global define, Promise */
define('jira/onboarding/assignissues/widgets/issue-assignee-picker-widget', ['require'], function(require) {
    var $ = require('jquery');
    var Backbone = require('backbone');
    var AssigneePicker = require('jira/field/assignee-picker');
    var Templates = JIRA.Templates.Fields.Pickers.User;

    function updateIssueAssignee(issueKey, assigneeName)
    {
        var url = AJS.contextPath() + '/rest/api/2/issue/' + issueKey + '/assignee';
        var name = assigneeName || null;
        return $.ajax({
            type: 'PUT',
            url: url,
            contentType: 'application/json',
            data: JSON.stringify({ name: name }),
            dataType: 'json'
        });
    }

    function parseJSONResponse(xhr) {
        var response = {};
        try {
            response = JSON.parse(xhr.responseText);
        } catch (e) {
            // ignore
        }
        return response;
    }

    var AssigneePickerWithoutForm = AssigneePicker.extend({
        init: function(options) {
            this._super(options);
            this.options.submitInputVal = false;
        },
        submitForm: function() {
            if (!this.suggestionsVisible) {
                this.handleFreeInput();
            }
        }
    });

    return Backbone.View.extend({
        initialize: function(options) {
            options = options || {};
            this.assigneeOptions = options.assigneeOptions || [];
            this.issueKey = options.issueKey;
        },

        render: function () {
            var view = this;
            var issueKey = this.issueKey;
            this.$el.html(Templates.assignee({
                assigneeOptions: {options: view.assigneeOptions},
                isLoggedInUserAssignable: false,
                issueKey: issueKey,
                field: {'id': 'assignee-' + issueKey}
            }));
            this._picker = new AssigneePickerWithoutForm({
                element: this.$el.find('.js-assignee-picker')
            });
            this._picker.model.$element.on('selected', function (e, descriptor) {
                var assigneeUsername = descriptor.value();
                view._assigneeSelect(issueKey, assigneeUsername);
            });
        },

        _assigneeSelect: function (issueKey, assigneeUsername) {
            var view = this;
            var result = updateIssueAssignee(issueKey, assigneeUsername);
            result.done(function() {
                view.trigger('issueAssigned', issueKey, assigneeUsername);
            });
            result.fail(function(xhr) {
                var response = parseJSONResponse(xhr);
                if (typeof response.errors === 'object' && response.errors['assignee']) {
                    view._picker.showErrorMessage();
                    view._picker.$errorMessage.text(response.errors['assignee']);
                } else if (response.errorMessages && response.errorMessages.length) {
                    view._picker.showErrorMessage();
                    view._picker.$errorMessage.text(response.errorMessages.join('\n'));
                }
            });
            view.trigger('change', issueKey, assigneeUsername);
        }
    });
});
