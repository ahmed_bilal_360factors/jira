/* global Promise, require, sinon, module, test, ok, equal */
AJS.test.require("com.atlassian.jira.jira-onboarding-assets-plugin:assign-issues-component", function() {

    var $ = require("jquery");
    var Backbone = require("backbone");

    var fakeIssueOne = {
        "id": 11110,
        "key": "FAKE-1",
        "issueType": 3,
        "summary": "A fake issue"
    };

    module("jira/onboarding/assign-issues-sequence", {
        setup: function() {
            this.sandbox = sinon.sandbox.create();
            this.clock = this.sandbox.useFakeTimers();
            var View = Backbone.View.extend({ render: this.sandbox.stub() });
            var fakeView = this.fakeView = new View();
            var FakeViewClass = this.FakeViewClass = this.sandbox.stub().returns(fakeView);
            this.context = AJS.test.context();
            this.context.mock('jira/onboarding/assignissues/views/assign-issues-sequence-view', FakeViewClass);
            this.analytics = {
                pushEvent: this.sandbox.spy()
            };
            this.$el = $("<div></div>");
        },
        teardown: function() {
            this.sandbox.restore();
        }
    });

    test("Can be constructed", 1, function() {
        var AssignIssuesSequence = this.context.require("jira/onboarding/assign-issues-sequence");

        try {
            new AssignIssuesSequence();
            ok(true, "Expected no errors to be thrown when no options provided");
        } catch(e) {
            equal(e.message, false, "Expected no errors raised, but got one.");
        }
    });

    test("#init rejects immediately if user has no permission to assign issues", function() {
        var AssignIssuesSequence = this.context.require("jira/onboarding/assign-issues-sequence");
        var sequence = new AssignIssuesSequence();
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick(100);

        equal(promise.isRejected(), true, "No permissions");
        equal(promise.reason(), AssignIssuesSequence.EVENTS.NO_PERMISSION, "Reason should be that there was no permission.");
        ok(this.analytics.pushEvent.calledWith(AssignIssuesSequence.EVENTS.NO_PERMISSION), "should have fired NO_PERMISSION event");
    });

    test("#init rejects immediately if no issues provided", function() {
        var AssignIssuesSequence = this.context.require("jira/onboarding/assign-issues-sequence");
        var sequence = new AssignIssuesSequence({canAssignIssues: true});
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick(100);

        equal(promise.isRejected(), true, "No issues were provided.");
        equal(promise.reason(), AssignIssuesSequence.EVENTS.NO_ISSUES, "Reason should be that no issues were provided.");
        ok(this.analytics.pushEvent.calledWith(AssignIssuesSequence.EVENTS.NO_ISSUES), "should have fired NO_ISSUES event");
    });

    test("#init rejects immediately if empty array of issues provided", function() {
        var AssignIssuesSequence = this.context.require("jira/onboarding/assign-issues-sequence");
        var sequence = new AssignIssuesSequence({issues: [], canAssignIssues: true});
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick(100);

        equal(promise.isRejected(), true, "No issues were provided.");
        equal(promise.reason(), AssignIssuesSequence.EVENTS.NO_ISSUES, "Reason should be that no issues were provided.");
        ok(this.analytics.pushEvent.calledWith(AssignIssuesSequence.EVENTS.NO_ISSUES), "should have fired NO_ISSUES event");
    });

    test("#init rejects immediately if no users other than logged in user exist", function() {
        // The question of the number of users who are assignable is left aside for the moment,
        // since accurate knowledge of that would require we know the project or issue key ahead of time,
        // and that's a logistically difficult venture at present.
        var AssignIssuesSequence = this.context.require("jira/onboarding/assign-issues-sequence");
        var sequence = new AssignIssuesSequence({issues: [fakeIssueOne], totalUsers: 1, canAssignIssues: true});
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick(100);

        equal(promise.isRejected(), true, "Only one user in the system.");
        equal(promise.reason(), AssignIssuesSequence.EVENTS.INSUFFICIENT_USERS, "Reason should be that not enough users exist.");
        ok(this.analytics.pushEvent.calledWith(AssignIssuesSequence.EVENTS.INSUFFICIENT_USERS), "should have fired INSUFFICIENT_USERS event");
    });

    test("#init works when at least one issue passed in", function() {
        var AssignIssuesSequence = this.context.require("jira/onboarding/assign-issues-sequence");
        var sequence = new AssignIssuesSequence({issues: [fakeIssueOne], totalUsers: 3, canAssignIssues: true});
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick(100);

        equal(promise.isPending(), true, "Should be in flight.");
    });

    test("#init initialises the view with the provided element", function() {
        var AssignIssuesSequence = this.context.require("jira/onboarding/assign-issues-sequence");
        var sequence = new AssignIssuesSequence({issues: [fakeIssueOne], totalUsers: 3, canAssignIssues: true});
        sequence.init(this.$el, this.analytics);

        this.clock.tick(100);

        ok(this.FakeViewClass.calledOnce, "should construct the view");

        var callArg = this.FakeViewClass.getCall(0).args[0];
        ok(typeof callArg === "object", "should be an object");
        ok(callArg.el === this.$el, "our element was passed in to the view");
        ok(this.fakeView.render.calledOnce, "should render the view immediately");
    });

    test("resolves if view fires a 'done' event", function() {
        var AssignIssuesSequence = this.context.require("jira/onboarding/assign-issues-sequence");
        var sequence = new AssignIssuesSequence({issues: [fakeIssueOne], totalUsers: 3, canAssignIssues: true});
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick(100);
        equal(promise.isPending(), true, "nothing has happened yet");

        this.fakeView.trigger("done");
        this.clock.tick(100);

        equal(promise.isPending(), false, "should be finished");
        equal(promise.isRejected(), false, "should have finished successfully");
        equal(promise.isFulfilled(), true, "should have finished successfully");
        ok(this.analytics.pushEvent.calledWith(AssignIssuesSequence.EVENTS.DONE), "should have fired success event");
    });

    test("rejects if view fires a 'skip' event", function() {
        var AssignIssuesSequence = this.context.require("jira/onboarding/assign-issues-sequence");
        var sequence = new AssignIssuesSequence({issues: [fakeIssueOne], totalUsers: 3, canAssignIssues: true});
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick(100);
        equal(promise.isPending(), true, "nothing has happened yet");

        this.fakeView.trigger("skip");
        this.clock.tick(100);

        equal(promise.isPending(), false, "should be finished");
        equal(promise.isRejected(), true, "should have finished with a skip");
        equal(promise.isFulfilled(), false, "should have finished with a skip");
        ok(this.analytics.pushEvent.calledWith(AssignIssuesSequence.EVENTS.SKIPPED), "should have fired skip event");
    });
});
