define("jira/onboarding/nutshell-sequence/models/sequence-model", ["require"], function (require) {
    "use strict";
    
    var _ = require("underscore");
    var Backbone = require("backbone");
    var SequenceStep = require("jira/onboarding/nutshell-sequence/models/sequence-step");

    /**
     * An implementation of the concept of a list iterator.
     * 
     * @class SequenceModel
     * 
     * @see java.util.ListIterator
     */
    return Backbone.Model.extend({
        defaults: {
            startFrom: 0,
            steps: []
        },
        /**
         * @constructor
         * 
         * @param {number}    startFrom=0 - 0-based index that the cursor starts from
         * @param {Array.<T>} steps=[]    - List of steps that the iterator iterates through
         */
        initialize: function (attributes) {
            // use `currentIndex` internally and set it to the initial value `startFrom`
            this.on("change:currentIndex", function (model, value) {
                var clampedValue = Math.max(0, Math.min(value, this.get("steps").length - 1));
                // trigger only if value is within bounds
                if (value !== clampedValue) {
                    this.set("currentIndex", clampedValue);
                } else {
                    this.trigger("step", value);
                }
            }, this);
            this.reset();
        },
        /**
         * @return {T} the current element in the list
         */
        current: function () {
            return this.get("steps")[this.get("currentIndex")];
        },
        /**
         * @return {number} 0-based index of the current element
         */
        currentIndex: function () {
            return this.get("currentIndex");
        },
        /**
         * Advance the cursor to the next element in the list
         *
         * @fires SequenceModel#step unless the end has been reached
         * @fires SequenceModel#done when the end has been reached
         */
        next: function () {
            if (this.hasNext()) {
                this.set("currentIndex", this.get("currentIndex") + 1);
            } else {
                this.trigger("done");
            }
        },
        /**
         * @return {boolean} whether there's another element in the list after the current one
         */
        hasNext: function () {
            return (this.get("currentIndex") < this.get("steps").length - 1);
        },
        /**
         * Advance the cursor to the previous element in the list
         *
         * @fires SequenceModel#step
         */
        prev: function () {
            if (this.hasPrev()) {
                this.set("currentIndex", this.get("currentIndex") - 1);
            }
        },
        /**
         * @return {boolean} whether there's another element in the list before the current one
         */
        hasPrev: function () {
            return (this.get("currentIndex") > 0);
        },
        /**
         * Reset the cursor to the index specified at instantiation
         *
         * @fires SequenceModel#step
         */
        reset: function () {
            this.set("currentIndex", this.get("startFrom"));
        }
    });
});
