/* global Promise, require, sinon, module, test, ok, equal */
AJS.test.require("com.atlassian.jira.jira-onboarding-assets-plugin:create-issues-component", function() {
    "use strict";

    var $ = require("jquery");
    var Backbone = require("backbone");

    module("CreateIssuesSequence", {
        setup: function() {
            this.sandbox = sinon.sandbox.create();
            this.clock = this.sandbox.useFakeTimers();
            var View = Backbone.View.extend({ render: this.sandbox.stub() });
            var fakeView = this.fakeView = new View();
            var FakeViewClass = this.FakeViewClass = sinon.stub().returns(fakeView);
            this.context = AJS.test.context();
            this.context.mock("jira/onboarding/createissues/views/create-issues-sequence-view", FakeViewClass);
            this.analytics = {
                pushEvent: this.sandbox.spy()
            };
            this.$el = $("<div></div>");
        },
        teardown: function() {
            this.sandbox.restore();
        }
    });

    test("Can be constructed", 1, function() {
        var CreateIssuesSequence = this.context.require("jira/onboarding/create-issues-sequence");

        try {
            new CreateIssuesSequence();
            ok(true, "Expected no errors to be thrown when no options provided");
        } catch(e) {
            equal(e.message, false, "Expected no errors raised, but got one.");
        }
    });

    test("#init rejects immediately if user has no permission to create issues", function() {
        var CreateIssuesSequence = this.context.require("jira/onboarding/create-issues-sequence");
        var sequence = new CreateIssuesSequence();

        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick(100);

        equal(promise.isRejected(), true, "No permissions");
        equal(promise.reason(), CreateIssuesSequence.EVENTS.NO_PERMISSION, "Reason should be that there was no permission.");
        ok(this.analytics.pushEvent.calledWith(CreateIssuesSequence.EVENTS.NO_PERMISSION), "should have fired NO_PERMISSION event");
    });

    test("#init rejects immediately if no project ID was provided", function() {
        var CreateIssuesSequence = this.context.require("jira/onboarding/create-issues-sequence");
        var sequence = new CreateIssuesSequence({ canCreateIssues: true });
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick(100);

        equal(promise.isRejected(), true, "No project ID was provided.");
        equal(promise.reason(), CreateIssuesSequence.EVENTS.NO_PROJECT, "Reason should be that no project ID was provided.");
    });

    test("#init works if project ID is provided", function() {
        var CreateIssuesSequence = this.context.require("jira/onboarding/create-issues-sequence");
        var sequence = new CreateIssuesSequence({
            canCreateIssues: true,
            projectId: 12345
        });
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick(100);

        equal(promise.isPending(), true, "should be in flight");
    });

    test("#init initialises the view with the provided element", function() {
        var CreateIssuesSequence = this.context.require("jira/onboarding/create-issues-sequence");
        var sequence = new CreateIssuesSequence({
            canCreateIssues: true,
            projectId: 12345
        });
        sequence.init(this.$el, this.analytics);

        this.clock.tick(100);

        ok(this.FakeViewClass.calledOnce, "should construct the view");

        var callArg = this.FakeViewClass.getCall(0).args[0];
        ok(typeof callArg === "object", "should be an object");
        ok(callArg.el === this.$el, "our element was passed in to the view");
        ok(this.fakeView.render.calledOnce, "should render the view immediately");
    });

    test("resolves if view fires a 'done' event", function() {
        var CreateIssuesSequence = this.context.require("jira/onboarding/create-issues-sequence");
        var sequence = new CreateIssuesSequence({
            canCreateIssues: true,
            projectId: 10001
        });
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick(100);
        equal(promise.isPending(), true, "nothing has happened yet");

        this.fakeView.trigger("done");
        this.clock.tick(100);

        equal(promise.isPending(), false, "should be finished");
        equal(promise.isFulfilled(), true, "should have finished successfully");
        equal(promise.isRejected(), false, "should have finished successfully");
        ok(this.analytics.pushEvent.calledWith(CreateIssuesSequence.EVENTS.DONE), "should have fired success event");
    });

    test("rejects if view fires a 'skip' event", function() {
        var CreateIssuesSequence = this.context.require("jira/onboarding/create-issues-sequence");
        var sequence = new CreateIssuesSequence({
            canCreateIssues: true,
            projectId: 10001
        });
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick(100);
        equal(promise.isPending(), true, "nothing has happened yet");

        this.fakeView.trigger("skip");
        this.clock.tick(100);

        equal(promise.isPending(), false, "should be finished");
        equal(promise.isFulfilled(), false, "should have finished with a skip");
        equal(promise.isRejected(), true, "should have finished with a skip");
        ok(this.analytics.pushEvent.calledWith(CreateIssuesSequence.EVENTS.SKIPPED), "should have fired skip event");
    });

    test("returns all issues created when resolved", function() {
        var CreateIssuesSequence = this.context.require("jira/onboarding/create-issues-sequence");
        var sequence = new CreateIssuesSequence({
            canCreateIssues: true,
            projectId: 10001
        });
        var promise = sequence.init(this.$el, this.analytics);

        var fakeIssueOne = { key: "FAKE-1", summary: "I'm a fake", id: 12345 };
        var fakeIssueTwo = { key: "FAKE-2", summary: "I'm also fake", id: 12350 };
        this.fakeView.trigger("issueCreated", fakeIssueOne);
        this.fakeView.trigger("issueCreated", fakeIssueTwo);
        this.fakeView.trigger("done");
        this.clock.tick(100);

        var result = promise.value();
        ok(result instanceof Object, "should get an object back");
        ok(result.issues instanceof Array, "should have an array of created issues");
        equal(result.issues[0], fakeIssueOne);
        equal(result.issues[1], fakeIssueTwo);
    });
});
