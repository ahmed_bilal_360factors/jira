AJS.test.require("com.atlassian.jira.jira-onboarding-assets-plugin:create-project-component", function() {
    "use strict"

    var $ = require('jquery');
    var CreateProjectView = require('jira/onboarding/create-project-sequence-view');

    function stubProject(name, key) {
        return {
            name: name,
            key: key
        };
    }

    var SAMPLE_PROJECTS = [
        stubProject('Test Project', 'TEST'),
        stubProject('Another', 'ANO')
    ];

    var STUB_PROJECT_NAME = 'Stash';
    var STUB_PROJECT_KEY = 'STA';
    var STUB_LEAD_NAME = 'admin';
    var AGILE_PROJECT_TEMPLATE_KEY = 'com.atlassian.jira-core-project-templates:jira-issuetracking';

    /**
     * Split a query list into the seperate key/value pairs
     * @param {String} request
     * @returns {Object}
     */
    function getPostParams(request) {
        var params = {};
        _.each(request.requestBody.split('&'), function (entry) {
            var entryValues = entry.split('=');
            params[entryValues[0]] = entryValues[1];
        });

        return params;
    }

    function setInputFieldValue(name, value) {
        var keyElement = this.$el.find('input[name='+name+']');
        keyElement.focus();
        keyElement.val(value);
        this.clock.tick(100);
    }

    function createProject(name, key) {
        this.setInputFieldValue('key', key);
        this.setInputFieldValue('name', name);
    }

    module('CreateProjectSequenceView construction', {
        setup: function() {
            this.$el = $('<div/>').appendTo('#qunit-fixture');
            this.server = sinon.fakeServer.create();
        },
        teardown: function() {
            this.server.restore();
        }
    });

    test('focuses the first input field upon creating the view', function() {
        var activeElement = document.activeElement;
        var view = new CreateProjectView({
            el: this.$el,
            leadName: STUB_LEAD_NAME
        });
        ok(document.activeElement === activeElement);
        view.render();
        ok(document.activeElement !== activeElement, 'active element should have changed upon rendering the view');
        ok(document.activeElement === view.$('input:first')[0], 'should be the first input element in the view');
    });

    module('CreateProjectSequenceView tests', {
        setup: function () {
            this.server = sinon.fakeServer.create();
            this.clock = sinon.useFakeTimers();
            this.$el = $('<div/>').appendTo('#qunit-fixture');

            this.server.respondWith([200, { 'Content-Type': 'application/json' }, JSON.stringify(SAMPLE_PROJECTS)]);
            this.createProjectSequenceView = new CreateProjectView({
                el: this.$el,
                leadName: STUB_LEAD_NAME
            });
            this.createProjectSequenceView.render();
            this.clock.tick(100);

            this.createProject = _.bind(createProject, this);
            this.setInputFieldValue = _.bind(setInputFieldValue, this);
        },
        teardown: function () {
            this.createProjectSequenceView.remove();
            this.clock.restore();
            this.server.restore();
        }
    });

    test('Test getPostParams works as expected', function() {
        var params = getPostParams({ requestBody : 'arg1=val1&arg2=val2'});
        deepEqual(params, { arg1 : 'val1', arg2 : 'val2'});
    });

    test('Test submit button is disabled at the beginning', function () {
        ok($('input[type=submit]', this.$el).attr('disabled'), 'Should be disabled');
        equal($('input[type=submit]', this.$el).attr('aria-disabled'), 'true', 'Should be aria disabled');
    });

    test('Test requested project names for fields', function () {
        equal(this.server.requests.length, 1, 'Should have one current request');
    });

    test('Test setting project name as current project has error', function () {
        this.server.respond();
        ok(!this.$el.find('.error').is(':visible'), 'Should be no error messages');

        this.setInputFieldValue('name', SAMPLE_PROJECTS[0].name);

        var errors = this.$el.find('.error');
        ok(errors.is(':visible'), 'Should have an error message');
        equal($(errors.get(0)).text(), 'admin.errors.project.with.that.name.already.exists', 'Should be an error message for project name already existing');
    });

    test('Test a key has been assigned when changing name', function () {
        this.server.respond();
        this.setInputFieldValue('name', STUB_PROJECT_NAME);

        var keyValue = this.$el.find('[name=key]').val();
        ok(keyValue.length > 0, 'Key should have been assigned');
    });

    test('Already used key should show an error', function () {
        this.server.respond();
        this.setInputFieldValue('key', STUB_PROJECT_NAME);

        var validationResponse = {
            errors: {
                'projectKey': 'errorMessage'
            }
        };

        this.server.requests.pop().respond(200, { 'Content-Type': 'application/json' }, JSON.stringify(validationResponse));

        var errors = this.$el.find('.error');
        ok(errors.is(':visible'), 'Should have an error message');
    });

    test('Valid key does not show error', function () {
        this.server.respond();
        this.setInputFieldValue('key', STUB_PROJECT_NAME);

        this.server.requests.pop().respond(200, { 'Content-Type': 'application/json' }, JSON.stringify('{}'));

        var errors = this.$el.find('.error');
        ok(!errors.is(':visible'), 'Should have no error message');
    });

    test('Valid name and key should enable the submit button', function () {
        this.server.respond();
        this.createProject(STUB_PROJECT_NAME, STUB_PROJECT_KEY);

        ok(!$('input[type=submit]', this.$el).attr('disabled'), 'Should not be disabled');
        equal($('input[type=submit]', this.$el).attr('aria-disabled'), 'false', 'Should not be aria disabled');
    });

    test('Project creation fields should be assigned correctly', function () {
        this.server.respond();
        this.createProject(STUB_PROJECT_NAME, STUB_PROJECT_KEY);

        var requestCount = this.server.requests.length;

        $('input[type=submit]', this.$el).click();

        // first request should check for a valid websudo session (if necessary)
        equal(this.server.requests.length - requestCount, 1, 'Should have another request on a submit');
        this.server.requests.pop().respond(200);

        var expectedParams = {
            name: STUB_PROJECT_NAME,
            key: STUB_PROJECT_KEY,
            lead: STUB_LEAD_NAME,
            projectTemplateWebItemKey: encodeURIComponent(AGILE_PROJECT_TEMPLATE_KEY)
        };
        // second request will create the project by POSTing data
        var params = getPostParams(this.server.requests.pop());

        deepEqual(params, expectedParams);
    });

    test('Invalid or missing websudo session should open websudo dialog', function () {
        this.server.respond();
        this.createProject(STUB_PROJECT_NAME, STUB_PROJECT_KEY);

        $('input[type=submit]', this.$el).click();
        // indicates a websudo session should be initialised
        this.server.requests.pop().respond(401);

        this.server.requests.pop().respond(200, {'Content-type': 'text/html'}, 'Dialog content here');

        // let's inspect basic dialog elements, as this should be completely tested by SmartAjaxWebSudo
        equal($('#smart-websudo').size(), 1, 'WebSudo dialog should be visible');
        equal($('#smart-websudo .cancel').size(), 1, 'A cancel button should be provided');

        // dismiss the dialog
        $('#smart-websudo .cancel').click();
        equal($('#smart-websudo').size(), 0, 'WebSudo dialog should not be visible');
    });

    test('Failed project creation 400 shows error messages', function () {
        this.server.respond();
        this.createProject(STUB_PROJECT_NAME, STUB_PROJECT_KEY);

        $('input[type=submit]', this.$el).click();
        this.server.requests.pop().respond(200);

        var submitResponse = {
            errors: {
                projectKey: 'Some error'
            }
        };

        this.server.requests.pop().respond(400, { 'Content-Type': 'application/json' }, JSON.stringify(submitResponse));

        equal(this.$el.find('.form-errors .title').length, 1 , "Error should be visible");
    });

    test('Failed project creation other shows server error message', function () {
        this.server.respond();
        this.createProject(STUB_PROJECT_NAME, STUB_PROJECT_KEY);

        $('input[type=submit]', this.$el).click();
        this.server.requests.pop().respond(200);

        var submitResponse = {
            errors: {
                projectKey: 'Some error'
            }
        };
        this.server.requests.pop().respond(500, { 'Content-Type': 'application/json' }, JSON.stringify(submitResponse));

        equal(this.$el.find('.form-errors .title').length, 1 , "Error should be visible");
    });

    test('Unauthorised shows a link to login with the correct redirect url', function () {
        this.server.respond();
        this.createProject(STUB_PROJECT_NAME, STUB_PROJECT_KEY);

        $('input[type=submit]', this.$el).click();
        this.server.requests.pop().respond(200);

        this.server.requests.pop().respond(401, { 'Content-Type': 'application/json' }, JSON.stringify('{}'));

        equal(this.$el.find('.form-errors .title').length, 1 , "Error should be visible");
    });

    test('Project successful creation triggers event with redirect url', function () {
        var redirectUrl = '/test';

        var submitResponse = {
            redirectUrl: redirectUrl
        };

        this.doneHandler = sinon.spy();

        this.createProjectSequenceView.bind('done', this.doneHandler);

        this.server.respond();
        this.createProject(STUB_PROJECT_NAME, STUB_PROJECT_KEY);

        this.clock.tick(100);

        $('input[type=submit]', this.$el).click();
        this.server.requests.pop().respond(200); // for the websudo verification

        this.server.requests.pop().respond(200, { 'Content-Type': 'application/json' }, JSON.stringify(submitResponse));

        ok(this.doneHandler.calledOnce, 'Should have returned redirect url on finish');
        ok(this.doneHandler.withArgs(submitResponse).calledOnce, "Called with redirect args");
    });

    test('Skipping project creation creates event', function() {
        this.skipHandler = sinon.spy();

        this.createProjectSequenceView.bind('skip', this.skipHandler);

        this.$el.find('#skip').click();

        this.clock.tick(100);

        ok(this.skipHandler.calledOnce, 'Should have called skip event handler');
    });

});
