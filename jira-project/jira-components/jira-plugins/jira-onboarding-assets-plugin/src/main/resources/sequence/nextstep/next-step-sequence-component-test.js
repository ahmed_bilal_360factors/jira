AJS.test.require('com.atlassian.jira.jira-onboarding-assets-plugin:next-step-component', function() {
    'use strict';

    var NextStepSequenceComponent;


    module('onboarding/next-step-sequence-component', {
        setup: function () {
            this.element = AJS.$('#qunit-fixture');
            this.clock = sinon.useFakeTimers();


            this.redirectSpy = sinon.spy();

            this.context = AJS.test.context();
            this.context.mock('jira/util/browser', {
                reloadViaWindowLocation: this.redirectSpy
            });

            this.analyticsSpy = sinon.spy();

            this.analytics = {
                pushEvent: this.analyticsSpy
            };

            NextStepSequenceComponent = this.context.require('jira/onboarding/next-step-sequence-component');
        },

        teardown: function() {
            this.clock.restore();
        }
    });

    test('Creating a view with no permission data throws an error', function() {
        try {
            new NextStepSequenceComponent();
            ok(false, 'Should have thrown an error on creation with no permissions');
        } catch (e) {
            ok(true);
        }

    });

    asyncTest('No valid permissions results in the sequence being reject', function() {
        var view = new NextStepSequenceComponent({
            canBrowseProjects: false,
            canCreateIssues: false,
            canSearchIssues: false
        });

        view.init(this.element, this.analytics).then(function() {
            ok(false, 'Should not have been resolved');
            QUnit.start();
        },
        function() {
            ok(true);
            QUnit.start();
        });
    });

    asyncTest('A valid permission results in the sequence being resolved', function() {
        var view = new NextStepSequenceComponent({
            canBrowseProjects: false,
            canCreateIssues: true,
            canSearchIssues: false
        });

        view.init(this.element, this.analytics).then(function() {
            ok(true);
            QUnit.start();
        },
        function() {
            ok(false, 'Should have been resolved');
            QUnit.start();
        });
    });

    test('Should attempt to redirect on step click', function() {

        var view = new NextStepSequenceComponent({
            canBrowseProjects: false,
            canCreateIssues: true,
            canSearchIssues: false
        });

        view.init(this.element, this.analytics);

        this.clock.tick(100);

        this.element.find('.next-step').click();

        this.clock.tick(100);

        ok(this.redirectSpy.calledOnce, 'Should have called redirect url');
    });

    test('Analytics is called when a step is completed', function() {
        var view = new NextStepSequenceComponent({
            canBrowseProjects: false,
            canCreateIssues: true,
            canSearchIssues: false
        });

        view.init(this.element, this.analytics);

        this.clock.tick(100);

        this.element.find('.next-step[data-step-key=' + NextStepSequenceComponent.STEPS.CREATE_ISSUE.key + ']').click();

        this.clock.tick(100);

        ok(this.analyticsSpy.calledOnce, 'Analytics push event should have been called');
        ok(this.analyticsSpy.withArgs(NextStepSequenceComponent.STEPS.CREATE_ISSUE.key).calledOnce, 'Should have created a createissues event');
    });
});
