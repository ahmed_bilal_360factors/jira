/* global define */

//Note: create-project-field depends on a later version of JIRA, e.g. 6.3.5 works.
define('jira/onboarding/create-project-sequence-view', [
    'jquery',
    'backbone',
    'underscore',
    'jira/field/create-project-field',
    'jira/onboarding/services/create-project-service'
],
function($, Backbone, _, CreateProjectField, CreateProjectService) {

    var MAX_NAME_LENGTH = 80;
    var MAX_KEY_LENGTH = 10;
    var PROJECT_TEMPLATE_KEY = 'com.atlassian.jira-core-project-templates:jira-issuetracking';

    var BAD_REQUEST = 400;
    var UNAUTHORISED = 401;

    var LOGIN_URL = AJS.contextPath() + '/login.jsp?os_destination=' + encodeURIComponent(window.location.href);

    var Templates = JIRA.Onboarding.Sequence.CreateProject;

    /**
     * @module CreateProjectSequenceView
     * @global
     *
     * @event CreateProjectSequenceView#skip
     * @event CreateProjectSequenceView#done
     * @param {Object} response - response message
     */
    return Backbone.View.extend({

        events: {
            'submit': '_createProject',
            'click #skip': '_skip'
        },

        initialize: function(options) {
            this.maxKeyLength = options.maxKeyLength || MAX_KEY_LENGTH;
            this.maxNameLength = options.maxNameLength || MAX_NAME_LENGTH;
            this.leadName = options.leadName;
        },

        render: function() {
            this.$el.html(Templates.render({
                maxNameLength: this.maxNameLength,
                maxKeyLength: this.maxKeyLength,
                projectTemplateKey: PROJECT_TEMPLATE_KEY
            }));

            this.createProjectField = new CreateProjectField({
                element: this.$el
            });
            this.$('input').first().focus();

            this.createProjectField.bind('updated.Name', _.bind(this._formChange, this));
            this.createProjectField.bind('updated.Key', _.bind(this._formChange, this));
        },

        /**
         * Create the project by calling the REST service.
         * @param event
         * @private
         */
        _createProject: function(event) {
            event.preventDefault();
            var view = this;

            var fields = this._getProjectFields();
            if (fields) {

                this._showLoadingSpinner();
                this._disableCreateProject();


                CreateProjectService.createProject(fields).done(function(response) {
                    view.trigger('done', response);
                }).fail(function(response) {
                    // response can be null when the user cancels the websudo dialog
                    if (typeof response !== 'undefined') {
                        view._generateErrorMessage(fields, response);
                    }

                    view._enableCreateProject();
                    view._hideLoadingSpinner();
                });
            }
        },

        /**
         * Function called when the name or key changes. This enables or disables the create button.
         * @private
         */
        _formChange: function() {
            if (!this.loading) {
                if (this._validate()) {
                    this._enableCreateProject();
                } else {
                    this._disableCreateProject();
                }
            }
        },

        /**
         * Skip the creation of the project
         * @param event
         * @private
         */
        _skip: function(event) {
            event.preventDefault();

            this.trigger('skip');
        },

        /**
         * Get fields to send to the REST service to create the project. Null if invalid input.
         * @returns {null|Object}
         * @private
         */
        _getProjectFields: function() {
            if (this._validate()) {
                return {
                    name: this._getProjectName(),
                    key: this._getProjectKey(),
                    lead:this._getLeadName(),
                    projectTemplateWebItemKey: this._getTemplateName()
                };
            }
            return null;
        },


        /**
         * Spinner Functions
         */
        _showLoadingSpinner: function() {
            this.loading = true;
            this.$el.find('.submit-spinner').addClass('show');
        },

        _hideLoadingSpinner: function() {
            this.loading = false;
            this.$el.find('.submit-spinner').removeClass('show');
        },



        /**
         * Project button functions
         */
        _enableCreateProject: function() {
            this.$el.find('input[type=submit]').removeAttr('disabled').attr('aria-disabled', 'false');
        },

        _disableCreateProject: function() {
            this.$el.find('input[type=submit]').attr('disabled', true).attr('aria-disabled', 'true');
        },


        /**
         * Getters
         */
        _getProjectName: function() {
            return this.createProjectField.$nameElement.val();
        },

        _getProjectKey: function() {
            return this.createProjectField.$keyElement.val();
        },

        _getTemplateName: function() {
            return PROJECT_TEMPLATE_KEY;
        },

        _getLeadName: function() {
            return this.leadName;
        },


        /**
         * Validate whether the inputted fields are correct
         * @returns {boolean}
         * @private
         */
        _validate: function() {
            var valid = true;
            var projectName = this._getProjectName();
            var projectKey = this._getProjectKey();

            if (!projectName) {
                valid = false;
            } else if (!projectKey) {
                valid = false;
            } else if (this.$el.find('.error').is(':visible')) {
                valid = false;
            }

            return valid;
        },

        /**
         * Create an error message to display if the project cannot be created on the server.
         * @param fields for the project trying to create
         * @param response from the server
         * @private
         */
        _generateErrorMessage: function(fields, response) {
            var content = '';
            var title = '';
            if (response.status === BAD_REQUEST) {
                var responseJSON = $.parseJSON(response.responseText);
                title = AJS.I18n.getText('admin.errors.project.could.not.create', fields.name);
                content = Templates.errors({
                    errors: responseJSON.errors
                });
            } else if (response.status === UNAUTHORISED) {
                title = AJS.I18n.getText('common.forms.ajax.unauthorised.alert');
                content = Templates.loginLink({
                    loginUrl: LOGIN_URL
                });
            } else {
                title = AJS.I18n.getText('500.title');
            }

            this.$el.find('.form-errors').html(AJS.messages.error({
                title: title,
                body: content
            }));
        }
    });
});