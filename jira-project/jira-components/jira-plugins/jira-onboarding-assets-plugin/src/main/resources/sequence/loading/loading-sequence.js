/* global define, Promise */
define('jira/onboarding/loading-sequence', ['require'], function(require) {
    'use strict';

    var LoadingSequenceView = require('jira/onboarding/loading/views/loading-sequence-view');

    /**
     * @param {Object} options
     * @parma {Promise|null} options.loading - the promise to wait for
     * @constructor
     */
    var LoadingSequence = function(options) {
        options = options || {};
        this.loading = options.loading || null;
        this.timeout = options.timeout != null ? options.timeout : -1;
    };

    LoadingSequence.prototype = {
        init: function(container) {
            if (!this.loading) {
                return Promise.reject(LoadingSequence.EVENTS.NOTHING_TO_LOAD);
            }

            new LoadingSequenceView({
                el: container
            }).render();

            if (this.timeout < 0) {
                return this.loading;
            }

            return Promise.resolve(this.loading)
                .timeout(this.timeout)
                .then(null, function(reason) {
                    if (reason instanceof Promise.TimeoutError) {
                        return Promise.reject(LoadingSequence.EVENTS.TIMEOUT);
                    }
                });
        }
    };

    LoadingSequence.EVENTS = {
        NOTHING_TO_LOAD: "nothingToLoad",
        TIMEOUT: "timeout"
    };

    return LoadingSequence;
});