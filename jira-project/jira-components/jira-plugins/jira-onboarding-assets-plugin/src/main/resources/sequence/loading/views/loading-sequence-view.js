define('jira/onboarding/loading/views/loading-sequence-view', ['require'], function(require) {
    'use strict';

    var Backbone = require('backbone');
    var Templates = JIRA.Onboarding.Sequence.Loading;

    return Backbone.View.extend({
        render: function() {
            this.$el.html(Templates.render());
            this.$('.onboarding-sequence-loading').spin('large');
        }
    });
});