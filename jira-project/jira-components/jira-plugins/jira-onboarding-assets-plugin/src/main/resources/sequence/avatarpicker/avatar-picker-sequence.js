/* Global: Promise */

define('jira/onboarding/avatar-picker-sequence', [
    'jquery',
    'jira/onboarding/avatar-picker-sequence-view'
], function ($, AvatarPickerSequenceView) {
    'use strict';

    /**
     * @name  AvatarPickerSequenceOptions
     * @global
     *
     * @property {String} remoteUser username of user
     * @property {String} remoteUserFullname full name of the user
     * @property {Number} userAvatarId id of the avatar for the user
     * @property {String} userAvatarUrl url of the avatar for the user
     * @property {Number} defaultUserAvatarId id of the default avatar for the system
     */


    /**
     * @class AvatarPickerSequence
     * @global
     *
     * Create an AvatarPickerSequence
     * @param {AvatarPickerSequenceOptions} options
     * @constructor
     */
    var AvatarPickerSequence = function(options) {
        this._validateOptions(options);

        this.username = options.username;
        this.userFullname = options.userFullname;
        this.userAvatarId = options.userAvatarId;
        this.userAvatarUrl = options.userAvatarUrl;
        this.defaultUserAvatarId = options.defaultUserAvatarId;
    };


    AvatarPickerSequence.prototype._validateOptions = function(options) {
        if (!options.username) {
            throw new Error('Should have supplied the username of the current user');
        }

        if (!options.userFullname) {
            throw new Error('Should have supplied the fullname of the current user');
        }

        if (!options.userAvatarUrl) {
            throw new Error('Should have supplied the avatar url of the current user');
        }

        if (!options.defaultUserAvatarId) {
            throw new Error('Should have supplied the default avatar id');
        }
    };


    /**
     * @type SequenceInitFunction
     */
    AvatarPickerSequence.prototype.init = function(container, onboardingAnalytics) {
        var self = this;

        return new Promise(function(resolve) {
            var avatarPickerSequenceView = new AvatarPickerSequenceView({
                el: container,
                username: self.username,
                userFullname: self.userFullname,
                userAvatarId: self.userAvatarId,
                userAvatarUrl: self.userAvatarUrl,
                defaultUserAvatarId: self.defaultUserAvatarId
            });

            avatarPickerSequenceView.on('selectAvatar', function(avatarId) {
                onboardingAnalytics.pushEvent(AvatarPickerSequence.EVENTS.AVATAR_SELECTED, {
                    avatarId: avatarId
                });
            });

            avatarPickerSequenceView.once('done', function() {
                resolve();
            });


            avatarPickerSequenceView.render();
        });
    };

    AvatarPickerSequence.EVENTS = {
        AVATAR_SELECTED: 'avatarSelected'
    };

    return AvatarPickerSequence;
});
