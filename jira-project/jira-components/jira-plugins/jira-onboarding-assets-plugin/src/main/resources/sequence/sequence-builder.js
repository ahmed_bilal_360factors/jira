/* global Promise, define */
define('jira/onboarding/sequence-builder', ['underscore'], function(_) {

    /**
     * Builder for creating a sequence that allows for receiving configuration data asynchronously.
     *
     * @param {Function} SequenceCtor constructor function for a sequence
     * @param {Object} [initialData]
     * @constructor
     */
    var SequenceBuilder = function(SequenceCtor, initialData) {
        this.constructor = SequenceCtor;
        this.data = _.extend({}, initialData);
    };

    /**
     * Add some more data to the data already collected.
     * Any data provided will override existing data with the same keys.
     *
     * @param {Object} data
     * @returns {SequenceBuilder} itself
     */
    SequenceBuilder.prototype.addData = function(data) {
        _.extend(this.data, data);
        return this;
    };

    /**
     * @returns {Sequence} a newly constructed sequence object with all the data.
     */
    SequenceBuilder.prototype.build = function() {
        var SequenceCtor = this.constructor;
        return new SequenceCtor(this.data);
    };

    /**
     * Proxy to initialise a {@link Sequence} object.
     * It will call {@link #build} to get a constructed {@link Sequence} object,
     * then return the result of calling {@link Sequence#init} on that.
     *
     * @type {SequenceInitFunction}
     * @returns {Promise}
     */
    SequenceBuilder.prototype.init = function(container, analytics) {
        return this.build().init(container, analytics);
    };

    return SequenceBuilder;
});
