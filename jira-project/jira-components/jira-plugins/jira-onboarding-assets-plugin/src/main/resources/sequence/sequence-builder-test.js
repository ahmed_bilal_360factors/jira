/* global Promise, require, sinon, module, test, ok, equal */
AJS.test.require(["com.atlassian.jira.jira-onboarding-assets-plugin:sequence-common"], function() {
    var SequenceBuilder = require("jira/onboarding/sequence-builder");

    module("SequenceBuilder");

    test("can construct a sequence object", function() {
        var fakeSequence = { init: sinon.spy() };
        var FakeSequenceCtor = sinon.spy(function() {
            return fakeSequence;
        });
        var builder = new SequenceBuilder(FakeSequenceCtor);
        sinon.assert.notCalled(FakeSequenceCtor);
        var result = builder.build();
        sinon.assert.calledOnce(FakeSequenceCtor);
        sinon.assert.calledWith(FakeSequenceCtor, {});
        equal(result, fakeSequence, "build method returns constructed sequence object");
    });

    test("can construct a sequence object with initial data", function() {
        var FakeSequenceCtor = sinon.spy();
        var builder = new SequenceBuilder(FakeSequenceCtor, { foo: "bar" });
        builder.build();
        sinon.assert.calledWith(FakeSequenceCtor, { foo: "bar" });
    });

    test("can add data through the builder", function() {
        var FakeSequenceCtor = sinon.spy();
        var builder = new SequenceBuilder(FakeSequenceCtor);
        builder.addData({ foo: "bar" });
        builder.build();
        sinon.assert.calledWith(FakeSequenceCtor, { foo: "bar" });
    });

    test("added data for the sequence accumulates", function() {
        var FakeSequenceCtor = sinon.spy();
        var builder = new SequenceBuilder(FakeSequenceCtor, { foo: "bar" });
        builder.addData({ bar: "baz" });
        builder.addData({ one: "more" });
        builder.build();
        sinon.assert.calledWith(FakeSequenceCtor, { foo: "bar", bar: "baz", one: "more" });
    });

    test("newer data will override older data", function() {
        var FakeSequenceCtor = sinon.spy();
        var builder = new SequenceBuilder(FakeSequenceCtor, { foo: "bar", one: "more" });
        builder.addData({ foo: "boohoo" });
        builder.build();
        sinon.assert.calledWith(FakeSequenceCtor, { foo: "boohoo", one: "more" });
    });

    module("SequenceBuilder Proxying", {
        setup: function() {
            this.container = document.createElement('div');
            this.analytics = {};
        }
    });

    test("can act as a proxy Sequence object", function() {
        var fakeSequence = { init: sinon.spy() };
        var FakeSequenceCtor = sinon.spy(function() {
            return fakeSequence;
        });
        var builder = new SequenceBuilder(FakeSequenceCtor);
        builder.addData({ foo: "bar" });
        builder.init(this.container, this.analytics);
        sinon.assert.calledWith(FakeSequenceCtor, { foo: "bar" });
        sinon.assert.calledWith(fakeSequence.init, this.container, this.analytics);
    });
});
