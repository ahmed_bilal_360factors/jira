/* Global: Promise */

define('jira/onboarding/next-step-sequence-component', [
    'jquery',
    'underscore',
    'jira/onboarding/next-step-sequence-view',
    'jira/util/browser'
], function ($, _, NextStepSequenceView, Browser) {
    'use strict';

    /**
     * @name NextStepSequenceComponentOptions
     * @global
     *
     * @property {boolean} canBrowseProjects
     * @property {boolean} canCreateIssues
     * @property {boolean} canSearchIssues
     */


    /**
     * @class NextStepSequenceComponent
     * @global
     *
     * Represents the actual component for the Next Step sequence.
     * @param {NextStepSequenceComponentOptions} options
     * @constructor
     */
    var NextStepSequenceComponent = function(options) {
        options = options || {};
        this._validateOptions(options);

        this.canBrowseProjects = options.canBrowseProjects;
        this.canCreateIssues = options.canCreateIssues;
        this.canSearchIssues = options.canSearchIssues;

        this.steps = this._generateSteps();
    };

    /**
     * Validate the options
     * @param {NextStepSequenceComponentOptions} options
     * @private
     */
    NextStepSequenceComponent.prototype._validateOptions = function(options) {
        if (typeof options.canBrowseProjects === 'undefined') {
            throw new Error('Should have defined a permission to browse projects');
        }

        if (typeof options.canCreateIssues === 'undefined') {
            throw new Error('Should have defined a permission to create issues');
        }

        if (typeof options.canSearchIssues === 'undefined') {
            throw new Error('Should have defined a permission to search issues');
        }
    };

    /**
     * Starts the sequence, returning the promise for the controller
     *
     * @param {Node} container
     * @param {OnboardingAnalytics} analytics for onboarding
     * @returns {Promise}
     */
    NextStepSequenceComponent.prototype.init = function(container, analytics) {
        var instance = this;
        return new Promise(function(resolve, reject) {
            if (instance.steps.length === 0) {
                reject(NextStepSequenceComponent.REJECTION_TYPES.NO_PERMISSIONS);
            } else {
                resolve();

                var nextStepSequenceView = new NextStepSequenceView({
                    el: container,
                    steps: instance.steps
                });

                nextStepSequenceView.on('stepSelected', function(step) {
                    analytics.pushEvent(step.key);

                    Browser.reloadViaWindowLocation(step.url);
                });

                nextStepSequenceView.render();
            }
        });
    };

    /**
     * Given the permissions it determines what steps are valid for this sequence
     * @returns {Step[]}
     * @private
     */
    NextStepSequenceComponent.prototype._generateSteps = function() {
        var steps = [];

        if (this.canBrowseProjects) {
            steps.push(NextStepSequenceComponent.STEPS.BROWSE_PROJECTS);
        }

        if (this.canCreateIssues) {
            steps.push(NextStepSequenceComponent.STEPS.CREATE_ISSUE);
        }

        if (this.canSearchIssues) {
            steps.push(NextStepSequenceComponent.STEPS.SEARCH_ISSUES);
        }

        return steps;
    };

    /**
     * Define the steps provided in the NextStepSequenceComponent
     * @typedef {Object.<String, Step>} Steps
     */
    NextStepSequenceComponent.STEPS = {
        BROWSE_PROJECTS: {
            key: 'browseprojects',
            url: AJS.contextPath() + '/secure/BrowseProjects.jspa?selectedCategory=all',
            label: AJS.I18n.getText('onboarding.next.step.projects.label'),
            icon: AJS.contextPath() +  '/download/resources/com.atlassian.jira.jira-onboarding-assets-plugin:next-step-component/explore.svg'
        },
        CREATE_ISSUE: {
            key: 'createissue',
            url: AJS.contextPath() + '/secure/CreateIssue!default.jspa',
            label: AJS.I18n.getText('onboarding.next.step.create.issue.label'),
            icon: AJS.contextPath() + '/download/resources/com.atlassian.jira.jira-onboarding-assets-plugin:next-step-component/create.svg'
        },
        SEARCH_ISSUES: {
            key: 'searchissues',
            url: AJS.contextPath() + '/issues',
            label: AJS.I18n.getText('onboarding.next.step.search.issues.label'),
            icon: AJS.contextPath() + '/download/resources/com.atlassian.jira.jira-onboarding-assets-plugin:next-step-component/search.svg'
        }
    };

    NextStepSequenceComponent.REJECTION_TYPES = {
        NO_PERMISSIONS: 'nopermissions'
    };

    return NextStepSequenceComponent;
});
