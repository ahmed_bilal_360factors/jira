/* Global: Promise */

define('jira/onboarding/next-step-sequence', [
    'jira/onboarding/next-step-sequence-component'
], function (NextStepSequenceComponent) {
    'use strict';

    /**
     * Wrapper for the next step sequence to collect the data from the resource mananger and apply it to the component
     * @class NextStepSequence
     * @global
     *
     * @constructor
     */
    var NextStepSequence = function(options) {
        this.component = new NextStepSequenceComponent(options);
    };

    /**
     * Start the sequence
     * @param {Node} container for sequence to populate
     * @param {OnboardingAnalytics} analytics for onboarding
     * @returns {Promise} for the sequence being completed
     */
    NextStepSequence.prototype.init = function(container, analytics) {
        return this.component.init(container, analytics);
    };

    NextStepSequence.REJECTION_TYPES = NextStepSequenceComponent.REJECTION_TYPES;

    return NextStepSequence;
});
