/* global Promise, require, sinon, module, test, ok, equal */
AJS.test.require("com.atlassian.jira.jira-onboarding-assets-plugin:loading-sequence", function() {

    var $ = require('jquery');
    var Backbone = require("backbone");

    module("jira/onboarding/loading-sequence", {
        setup: function() {
            this.sandbox = sinon.sandbox.create();
            this.clock = this.sandbox.useFakeTimers();
            this.viewRender = this.sandbox.stub();
            var View = Backbone.View.extend({ render: this.viewRender });
            var fakeView = this.fakeView = new View();
            var FakeViewClass = this.FakeViewClass = this.sandbox.stub().returns(fakeView);
            this.context = AJS.test.context();
            this.context.mock('jira/onboarding/loading/views/loading-sequence-view', FakeViewClass);
            this.$el = $("<div></div>");

            // Set the bluebird promise scheduler to use setTimeout so that we can use sinon's fake clock in all situations
            Promise.setScheduler(function(fn) { setTimeout(fn, 0); });
        },
        teardown: function() {
            this.sandbox.restore();
        }
    });

    test("Can be constructed", 1, function() {
        var LoadingSequence = this.context.require("jira/onboarding/loading-sequence");

        try {
            new LoadingSequence();
            ok(true, "Expected no errors to be thrown when no options provided");
        } catch(e) {
            equal(e.message, false, "Expected no errors raised, but got one.");
        }
    });

    test("#init rejects immediately if there is nothing to load", function() {
        var LoadingSequence = this.context.require("jira/onboarding/loading-sequence");
        var sequence = new LoadingSequence();
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick();

        equal(promise.isRejected(), true, "Nothing to load");
        equal(promise.reason(), LoadingSequence.EVENTS.NOTHING_TO_LOAD, "Reason should be that there is nothing to load.");
    });

    test("#init resolves immediately if the promise it is waiting for is already resolved", function() {
        var RESOLVE_VALUE = "result";
        var LoadingSequence = this.context.require("jira/onboarding/loading-sequence");
        var sequence = new LoadingSequence({loading: Promise.resolve(RESOLVE_VALUE)});
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick();

        equal(promise.isResolved(), true, "Sequence resolves immediately");
        equal(promise.value(), RESOLVE_VALUE, "Sequence returns the value of the promise it waits for");
    });

    test("#init rejects immediately if the promise it is waiting for is already rejected", function() {
        var REASON = "reason";
        var LoadingSequence = this.context.require("jira/onboarding/loading-sequence");
        var sequence = new LoadingSequence({loading: Promise.reject(REASON)});
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick();

        equal(promise.isRejected(), true, "Sequence rejects immediately");
        equal(promise.reason(), REASON, "Sequence returns the rejection reason of the promise it waits for");
    });

    test("#init renders loading view until the promise it waits for is resolved", function() {
        var RESOLVE_VALUE = "result";
        var LoadingSequence = this.context.require("jira/onboarding/loading-sequence");
        var deferred = $.Deferred();
        var sequence = new LoadingSequence({loading: Promise.resolve(deferred) });
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick();

        equal(promise.isFulfilled(), false, "Sequence is not fulfilled immediately");
        equal(this.viewRender.callCount, 1, "View is rendered");

        deferred.resolve(RESOLVE_VALUE);
        this.clock.tick();

        equal(promise.isResolved(), true, "Sequence is resolved when the promise it waits for is resolved");
        equal(promise.value(), RESOLVE_VALUE, "Sequence returns the value of the promise it waits for");
    });

    test("#init with timeout will reject after timeout time", function() {
        var TIMEOUT = 1000;
        var LoadingSequence = this.context.require("jira/onboarding/loading-sequence");
        var sequence = new LoadingSequence({
            loading: new Promise(function(){}),
            timeout: TIMEOUT
        });
        var promise = sequence.init(this.$el, this.analytics);

        this.clock.tick();

        equal(promise.isFulfilled(), false, "Sequence is not fulfilled immediately");
        equal(this.viewRender.callCount, 1, "View is rendered");

        this.clock.tick(TIMEOUT);

        equal(promise.isRejected(), true, "Sequence rejects after timeout");
        equal(promise.reason(), LoadingSequence.EVENTS.TIMEOUT, "Rejection reason is timeout");
    });
});
