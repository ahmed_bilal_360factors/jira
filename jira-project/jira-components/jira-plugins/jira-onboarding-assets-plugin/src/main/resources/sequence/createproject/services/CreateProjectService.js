/* global define */
define('jira/onboarding/services/create-project-service', [
    'jquery'
], function($) {

    return {
        /**
         * Start the sequence of calls to create a project.
         * @param fields
         */
        createProject: function(fields) {
            var self = this;
            var deferred = new $.Deferred();

            // first, let's check whether or not a websudo session needs to be initialised
            $.ajax({
                url: AJS.contextPath() + '/rest/project-templates/1.0/templates',
                type: 'GET',
                timeout: 50000,
                headers: {
                    'X-Atlassian-Token': 'nocheck'
                }
            }).
            done(function() {
                // a 200 response means we can proceed to the project creation call directly
                self._doCreateProject(fields).done(deferred.resolve).fail(deferred.reject);
            }).
            fail(function() {
                // we should open a websudo dialog in order to start a new session
                self._openWebSudoDialog(fields).done(deferred.resolve).fail(deferred.reject);
            });

            return deferred.promise();
        },

        /**
         * Opens SmartAjax WebSudo dialog.
         * @param fields
         * @private
         */
        _openWebSudoDialog: function(fields) {
            var self = this;
            var deferred = new $.Deferred();

            JIRA.SmartAjax.showWebSudoDialog({
                success: _.bind(function(fields, close) {
                    close();
                    this._doCreateProject(fields).done(deferred.resolve).fail(deferred.reject);
                }, self, fields)
            });
            // adding a cancel listener to showWebSudoDialog introduces a side-effect that redirects the user to the project management page
            // therefore we need to listen to specific dialog events to detect when it is closed by a user interaction (reason is null when it is closed via close())
            $(document).on('Dialog.beforeHide', function(dialog, $el, reason) {
                if (reason === "cancel" || reason === "esc") {
                    deferred.reject();
                }
            });

            return deferred.promise();
        },

        /**
         * Execute a POST request to create a project with the data provided.
         * @param fields
         * @private
         */
        _doCreateProject: function(fields) {
            var deferred = new $.Deferred();

            $.ajax({
                url: AJS.contextPath() + '/rest/project-templates/1.0/templates',
                type: 'POST',
                data: fields,
                timeout: 50000,
                headers: {
                    'X-Atlassian-Token': 'nocheck'
                }
            }).done(deferred.resolve).fail(deferred.reject);

            return deferred.promise();
        }

    };
});