/* global define, Promise */
define('jira/onboarding/create-project-sequence', [
    'jquery',
    'underscore',
    'jira/onboarding/create-project-sequence-view'
], function($, _, CreateProjectSequenceView) {
    "use strict";

    var CreateProjectSequence = function(options) {
        var sanitizedOptions = this.validateOptions(options);

        _.extend(this, sanitizedOptions);
    };

    CreateProjectSequence.prototype.validateOptions = function(options) {
        if (!options.username) {
            throw new Error('Should have supplied the username of the current user');
        }

        if (typeof options.canCompleteCreateProjectSequence === 'undefined') {
            throw new Error('Should have defined whether they can complete the create project sequence');
        }

        return _.pick(options, 'username', 'canCompleteCreateProjectSequence', 'maxKeyLength', 'maxNameLength');
    };

    /**
     *
     * @param {Node} container
     * @param {OnboardingAnalytics} analytics
     * @returns {Promise}
     */
    CreateProjectSequence.prototype.init = function(container, analytics) {
        var instance = this;

        return new Promise(function (resolve, reject) {

            if (instance.canCompleteCreateProjectSequence && instance.username) {
                var createProjectView = new CreateProjectSequenceView({
                    el: container,
                    leadName: instance.username,
                    maxKeyLength: instance.maxKeyLength,
                    maxNameLength: instance.maxNameLength
                });

                createProjectView.once('done', function (response) {
                    analytics.pushEvent(CreateProjectSequence.EVENTS.CREATED);
                    resolve(response);
                });

                createProjectView.once('skip', function () {
                    analytics.pushEvent(CreateProjectSequence.EVENTS.SKIPPED);
                    reject(CreateProjectSequence.EVENTS.SKIPPED);
                });

                createProjectView.render();
            } else {
                analytics.pushEvent(CreateProjectSequence.EVENTS.NO_PERMISSION);
                reject(CreateProjectSequence.EVENTS.NO_PERMISSION);
            }
        });
    };

    CreateProjectSequence.EVENTS = {
        CREATED: 'projectCreated',
        SKIPPED: 'skipped',
        NO_PERMISSION: 'noPermission'
    };

    return CreateProjectSequence;
});