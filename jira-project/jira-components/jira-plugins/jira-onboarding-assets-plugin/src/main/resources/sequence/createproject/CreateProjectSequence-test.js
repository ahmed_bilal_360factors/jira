AJS.test.require("com.atlassian.jira.jira-onboarding-assets-plugin:create-project-component", function() {
    "use strict"

    var $ = require('jquery');

    module('CreateProjectSequence tests', {
        setup: function () {

            var createProjectSequenceView = {

                render: sinon.spy(),
                once: function (name, handler) {
                    this._events[name] = handler;
                },
                trigger: function (name, argument) {
                    this._events[name](argument);
                },
                _events: {}
            };

            this.createProjectSequenceView = createProjectSequenceView;

            this.CreateProjectSequenceView = function () {
                return createProjectSequenceView;
            };

            this.context = AJS.test.context();
            this.context.mock('jira/onboarding/create-project-sequence-view', this.CreateProjectSequenceView);

            this.analyticsSpy = sinon.spy();

            this.analytics = {
                pushEvent: this.analyticsSpy
            };

            this.server = sinon.fakeServer.create();

            this.$el = $('<div/>');

            $('#qunit-fixture').append(this.$el);

            this.clock = sinon.useFakeTimers();
        },

        teardown: function () {
            this.clock.restore();
            this.server.restore();
        }
    });

    test('Rendered if has permission to create a project', sinon.test(function () {
        var CreateProjectSequence = this.context.require('jira/onboarding/create-project-sequence');
        var createProjectSequence = new CreateProjectSequence({ username : "admin", canCompleteCreateProjectSequence : true});

        createProjectSequence.init(this.$el, this.analytics);

        ok(this.createProjectSequenceView.render.calledOnce);
    }));

    test('Test redirects to intro to agile if cannot create project', sinon.test(function () {
        var CreateProjectSequence = this.context.require('jira/onboarding/create-project-sequence');
        var createProjectSequence = new CreateProjectSequence({ username : "admin", canCompleteCreateProjectSequence : false});

        var promise = createProjectSequence.init(this.$el, this.analytics);

        this.clock.tick(100);

        ok(promise.isRejected());
        equal(promise.reason(), CreateProjectSequence.EVENTS.NO_PERMISSION, 'Reason for rejection should be the user does not have permission');
        ok(this.analyticsSpy.calledOnce, 'Analytics push event should have been called');
        ok(this.analyticsSpy.withArgs(CreateProjectSequence.EVENTS.NO_PERMISSION).calledOnce, 'Analytics push event with NO_PERMISSION arg');
    }));

    test('Test redirects to intro to agile if no username on page', sinon.test(function () {
        var CreateProjectSequence = this.context.require('jira/onboarding/create-project-sequence');
        var createProjectSequence = new CreateProjectSequence({ username : "admin", canCompleteCreateProjectSequence : false});

        var promise = createProjectSequence.init(this.$el, this.analytics);

        this.clock.tick(100);

        ok(promise.isRejected());
        equal(promise.reason(), CreateProjectSequence.EVENTS.NO_PERMISSION, 'Reason for rejection should be the user does not have permission');
        ok(this.analyticsSpy.calledOnce, 'Analytics push event should have been called');
        ok(this.analyticsSpy.withArgs(CreateProjectSequence.EVENTS.NO_PERMISSION).calledOnce, 'Analytics push event with NO_PERMISSION arg');
    }));

    test('Test finishing the view redirects to the specified url', sinon.test(function () {
        var CreateProjectSequence = this.context.require('jira/onboarding/create-project-sequence');
        var createProjectSequence = new CreateProjectSequence({ username : "admin", canCompleteCreateProjectSequence : true});

        var promise = createProjectSequence.init(this.$el, this.analytics);

        var returnUrl = '/test';

        var response = {
            returnUrl: returnUrl
        };

        this.createProjectSequenceView.trigger('done', {
            returnUrl: returnUrl
        });

        this.clock.tick(100);
        ok(promise.isFulfilled());
        deepEqual(promise.value(), response, 'Value being fulfilled should be the return url');
        ok(this.analyticsSpy.calledOnce, 'Analytics push event should have been called');
        ok(this.analyticsSpy.withArgs(CreateProjectSequence.EVENTS.CREATED).calledOnce, 'Analytics push event with CREATED arg');
    }));

    test('Test skipping the project creation redirects to intro to agile', sinon.test(function () {
        var CreateProjectSequence = this.context.require('jira/onboarding/create-project-sequence');
        var createProjectSequence = new CreateProjectSequence({ username : "admin", canCompleteCreateProjectSequence : true});

        var promise = createProjectSequence.init(this.$el, this.analytics);

        this.createProjectSequenceView.trigger('skip');

        ok(promise.isRejected());
        equal(promise.reason(), CreateProjectSequence.EVENTS.SKIPPED, 'Reason for rejection should be the user skipped');
        ok(this.analyticsSpy.calledOnce, 'Analytics push event should have been called');
        ok(this.analyticsSpy.withArgs(CreateProjectSequence.EVENTS.SKIPPED).calledOnce, 'Analytics push event with SKIPPED arg');

    }));
});
