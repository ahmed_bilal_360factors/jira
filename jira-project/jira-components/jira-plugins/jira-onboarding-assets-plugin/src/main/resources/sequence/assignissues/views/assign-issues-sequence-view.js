/* global define, Promise */
define('jira/onboarding/assignissues/views/assign-issues-sequence-view', ['require'], function(require) {
    var $ = require('jquery');
    var _ = require('underscore');
    var Backbone = require('backbone');
    var LoggedInUser = require('jira/util/users/logged-in-user');
    var IssueAssigneePicker = require('jira/onboarding/assignissues/widgets/issue-assignee-picker-widget');
    var Templates = JIRA.Onboarding.Sequence.AssignIssues;

    var IssueModel = Backbone.Model.extend({
        defaults: {
            'assigneeField': { name: null }
        }
    });

    var IssueCollection = Backbone.Collection.extend({
        model: IssueModel
    });

    var getCurrentUserAvatarUrl = _.once(function() {
        return $('#user-options').find('.aui-avatar-inner > img').attr('src');
    });

    function adjustCurrentUserAvatarUrl(assigneeOptions) {
        _.each(assigneeOptions, function(option) {
            if (option.optionGroup) {
                option.groupOptions = adjustCurrentUserAvatarUrl(option.groupOptions);
                return;
            }
            if (option.optionName === LoggedInUser.username()) {
                option.avatarURL = getCurrentUserAvatarUrl();
            }
        });
        return assigneeOptions;
    }

    return Backbone.View.extend({
        initialize: function(options) {
            options = options || {};
            this.assigneeOptions = adjustCurrentUserAvatarUrl(options.assigneeOptions || []);
            this.issues = new IssueCollection(options.issues || []);
        },

        events: {
            'click #skip': '_skip',
            'click #next': '_next',
            'submit': '_submit'
        },

        render: function() {
            var view = this;
            this.$el.html(Templates.render({
                issues: this.issues.toJSON()
            }));
            this.$('.js-assignee-picker-placeholder').each(function(i, el) {
                var picker = new IssueAssigneePicker({
                    assigneeOptions: view.assigneeOptions,
                    issueKey: el.getAttribute('data-issuekey'),
                    el: el
                });
                picker.render();
                view.listenTo(picker, 'issueAssigned', view._assigneeSelect);
            });
            return this;
        },

        _assigneeSelect: function(issueKey, assigneeUsername) {
            this.trigger('issueAssigned', {
                issueKey: issueKey,
                assigneeUsername: assigneeUsername
            });
        },

        _submit: function onSubmit(e) {
            e.preventDefault();
            return false;
        },

        _skip: function onSkip(e) {
            e.preventDefault();
            this.trigger('skip');
        },

        _next: function onNext(e) {
            e.preventDefault();
            this.trigger('done');
        }

    });
});
