define('jira/onboarding/avatar-picker-sequence-view', [
    'jquery',
    'backbone'
], function($, Backbone) {
    return Backbone.View.extend({

        Templates: JIRA.Onboarding.Sequence.AvatarPicker,

        events: {
            'submit form' : 'next',
            'click .choose-avatar-button': 'chooseAvatar'
        },

        initialize: function(options) {

            this._validateOptions(options);

            this.username = options.username;
            this.userFullname = options.userFullname;
            this.userAvatarId = options.userAvatarId;
            this.userAvatarUrl = options.userAvatarUrl;
            this.defaultUserAvatarId = options.defaultUserAvatarId;
        },

        _validateOptions: function(options) {
            if (!options.username) {
                throw new Error('Should have supplied the username of the current user');
            }

            if (!options.userFullname) {
                throw new Error('Should have supplied the fullname of the current user');
            }

            if (!options.userAvatarUrl) {
                throw new Error('Should have supplied the avatar url of the current user');
            }

            if (!options.defaultUserAvatarId) {
                throw new Error('Should have supplied the default avatar id');
            }
        },

        render: function() {
            var self = this;

            this.$el.html(this.Templates.render({
                userFullname: self.userFullname,
                avatarImageUrl: self.userAvatarUrl,
                avatarId: self.userAvatarId
            }));

            JIRA.createUserAvatarPickerDialog({
                trigger: $(".choose-avatar-button", this.$el),
                username: self.username,
                defaultAvatarId: self.defaultUserAvatarId,
                /**
                 * Callback used when an avatar is selected
                 * @param avatar chosen
                 * @param avatarSrc url for the avatar image
                 */
                select: function(avatar, avatarSrc) {

                    self.trigger('selectAvatar', avatar.getId());
                    self._updateUserAvatar(avatar.getId(), avatarSrc);

                    $('.choose-avatar-button', self.$el).removeClass('aui-button-primary');
                    $('.avatar-picker-done', self.$el).addClass('aui-button-primary');
                }
            });
        },

        /**
         * Update the user avatar images on the sequence page
         * @param avatarId of the avatar
         * @param avatarSrcUrl for the avatar image
         * @private
         */
        _updateUserAvatar: function(avatarId, avatarSrcUrl) {
            $(".selected-avatar-image", this.$el).attr({
                'data-id': avatarId,
                src: avatarSrcUrl
            });


            //Update profile picture top right of screen
            var userIcon = $("#user-options").find(".aui-avatar-inner > img");
            if (userIcon) {
                userIcon.attr('src', avatarSrcUrl + "&size=small");
            }
        },

        /**
         * Stop the choose avatar button causing a submit event to occur.
         * @param event
         */
        chooseAvatar: function(event) {
            event.preventDefault();
        },

        /**
         * calls the done function when the next button has been completed. This will resolve the sequence promise.
         * @param event
         */
        next: function(event) {
            event.preventDefault();
            this.trigger('done');
        }
    });
});