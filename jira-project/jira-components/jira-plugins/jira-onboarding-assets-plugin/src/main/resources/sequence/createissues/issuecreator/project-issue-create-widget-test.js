/* global require, module, test, asyncTest, start, ok, equal */
AJS.test.require("com.atlassian.jira.jira-onboarding-assets-plugin:create-issues-component", function() {

    var Backbone = require("backbone");

    var FakeInlineBootstrap = Backbone.Model.extend({
        setJQLs: sinon.spy()
    });
    var FakeDefaultTrigger = Backbone.View.extend({
        render: function() {
            this.$el.html("<div data-faketrigger></div>");
        }
    });
    var fakeWidgetActivate = sinon.spy();
    var fakeWidgetFocus = sinon.spy();
    var FakeWidget = Backbone.View.extend({
        initialize: function() {
            this.view = new Backbone.View();
        },
        setCurrentJQLs: sinon.spy(),
        activate: fakeWidgetActivate,
        focus: fakeWidgetFocus,
        render: function() {
            this.$el.html("<div data-fakewidget></div>");
        },
        dropdownView: {
            bind: sinon.spy()
        }
    });

    module("ProjectIssueCreateWidget", {
        setup: function() {
            this.context = AJS.test.context();
            this.context.mock("jira/inline-issue-create/widget", FakeWidget);
            this.context.mock("jira/inline-issue-create/views/default-trigger", FakeDefaultTrigger);
            this.context.mock("jira/inline-issue-create/entities/inline-bootstrap", FakeInlineBootstrap);
        },
        teardown: function() {
            fakeWidgetFocus.reset();
            fakeWidgetActivate.reset();
        }
    });

    test("is a backbone view", function() {
        var el = document.createElement("div");
        var IICWidget = this.context.require("jira/onboarding/createissues/issuecreator/project-issue-create-widget");
        var widget = new IICWidget({
            el: el
        });

        ok(widget instanceof Backbone.View);
        ok(Object.hasOwnProperty.call(widget, "$el"), "should have a jQuery reference to the element");
        ok(widget.$el.get(0) === el, "should be the element we passed in");
    });

    test("IIC widget should be activated and focused on startup", function() {
        var IICWidget = this.context.require("jira/onboarding/createissues/issuecreator/project-issue-create-widget");
        var widget = new IICWidget();
        widget.init();
        ok(fakeWidgetActivate.calledOnce);
        ok(fakeWidgetFocus.calledOnce);
    });

});
