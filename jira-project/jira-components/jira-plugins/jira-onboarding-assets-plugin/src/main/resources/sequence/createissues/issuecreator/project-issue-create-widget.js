/* global Promise, define */
define('jira/onboarding/createissues/issuecreator/project-issue-create-widget', ['require'], function(require) {
    var Backbone = require('backbone');

    var proto = {};

    proto.initialize = function(options) {
        options = options || {};
        this.projectName = options.projectName;
        this.projectId = options.projectId;
    };

    proto.events = {
        'click a[href=#]': function(e) {
            // Basically a patch for IIC's default trigger not preventing default when its <a> is clicked.
            // otherwise the URL may get an unnecessary #
            e.preventDefault();
        }
    };

    proto.init = function() {
        var IICWidget = require('jira/inline-issue-create/widget');
        var IICDefaultTrigger = require('jira/inline-issue-create/views/default-trigger');
        var InlineBootstrap = require('jira/inline-issue-create/entities/inline-bootstrap');
        this._kickoff(IICWidget, IICDefaultTrigger, InlineBootstrap);
    };

    proto._kickoff = function(IICWidget, IICDefaultTrigger, InlineBootstrap) {
        var instance = this;
        var bootstrap = new InlineBootstrap();
        bootstrap.setJQLs(['project = ' + instance.projectId]);

        var creator = new IICWidget({
            issueTypes: bootstrap.issueTypes,
            projects: bootstrap.projects
        });
        // The most important state is not initially pulled out of the bootstrap, unfortunately.
        // Doubly unfortunate is that there's a setJQLs method, but no getter, and it's not a Backbone Model attribute.
        creator.setCurrentJQLs(bootstrap.jqls);

        var trigger = new IICDefaultTrigger({
            widget: creator
        });
        trigger.on("selected", function () {
            creator.activate();
            creator.focus();
        });

        creator.bind("issueCreated", function(issue, prefilledFields, widget, args) {
            var issueType = bootstrap.issueTypes.get(issue.issueType || issue.createdIssueDetails.fields.issuetype);
            var issueTypeData = issueType.toJSON();
            var issueData = {
                id: issue.issueId,
                key: issue.issueKey,
                issueType: issueTypeData,
                summary: (prefilledFields.summary && prefilledFields.summary[0]) || (issue.createdIssueDetails && issue.createdIssueDetails.fields.summary)
            };
            instance.trigger("issueCreated", issueData);

            setTimeout(function() {
                creator.focus();
            }, 0);
        });

        creator.dropdownView.bind("render", function () {
            creator.dropdownView.ui.trigger.removeClass("aui-dropdown2-trigger");
            creator.dropdownView.ui.trigger.removeAttr("aria-owns");
            creator.dropdownView.ui.trigger.removeAttr("aria-haspopup");
        });

        var $el = instance.$el;
        $el.append(trigger.$el);
        $el.append(creator.view.$el);
        trigger.render();
        creator.render();
        creator.activate();
        creator.focus();
    };
    return Backbone.View.extend(proto);
});
