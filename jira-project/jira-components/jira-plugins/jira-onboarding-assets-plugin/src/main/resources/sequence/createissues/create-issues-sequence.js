/* global define, Promise */
define("jira/onboarding/create-issues-sequence", ["require"], function(require) {
    "use strict";

    var CreateIssuesSequenceView = require("jira/onboarding/createissues/views/create-issues-sequence-view");

    /**
     * Allows the user to quickly create issues for a project.
     *
     * @param options {Object}
     * @param options.id {Number} the ID of the project to set up
     * @constructor
     */
    var CreateIssuesSequence = function(options) {
        options = options || {};

        this.projectId = options.projectId;
        this.projectName = options.projectName;
        this.canCreateIssues = options.canCreateIssues || false;
    };

    /**
     * @param {Node} container
     * @param {OnboardingAnalytics} analytics
     * @returns {Promise}
     */
    CreateIssuesSequence.prototype.init = function(container, analytics) {
        var instance = this;

        return new Promise(function (resolve, reject) {
            if (!instance.canCreateIssues) {
                analytics.pushEvent(CreateIssuesSequence.EVENTS.NO_PERMISSION);
                reject(CreateIssuesSequence.EVENTS.NO_PERMISSION);
                return;
            }
            if (!instance.projectId) {
                analytics.pushEvent(CreateIssuesSequence.EVENTS.NO_PROJECT);
                reject(CreateIssuesSequence.EVENTS.NO_PROJECT);
                return;
            }

            var issues = [];
            var model = {
                name: instance.projectName,
                id: instance.projectId
            };

            var view = new CreateIssuesSequenceView({
                el: container,
                model: model
            });
            view.once("done", function(response) {
                var data = { issues: issues };
                analytics.pushEvent(CreateIssuesSequence.EVENTS.DONE);
                resolve(data);
            });
            view.once("skip", function() {
                analytics.pushEvent(CreateIssuesSequence.EVENTS.SKIPPED);
                reject(CreateIssuesSequence.EVENTS.SKIPPED);
            });
            view.on("issueCreated", function(issue) {
                issues.push(issue);
                analytics.pushEvent(CreateIssuesSequence.EVENTS.ISSUE_CREATED);
            });

            view.render();
        });
    };

    CreateIssuesSequence.EVENTS = {
        NO_PERMISSION: "noPermission",
        NO_PROJECT: "noProject",
        ISSUE_CREATED: "issueCreated",
        DONE: "done",
        SKIPPED: "skipped"
    };

    return CreateIssuesSequence;
});
