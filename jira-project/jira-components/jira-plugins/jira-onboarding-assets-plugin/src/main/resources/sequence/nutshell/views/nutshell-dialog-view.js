/* global AJS */
define("jira/onboarding/nutshell-sequence/views/nutshell-dialog-view", ["require"], function (require) {
    "use strict";

    var Backbone = require("backbone");
    var _ = require("underscore");
    var $ = require("jquery");

    return Backbone.View.extend({
        template: JIRA.Onboarding.sequence.nutshell.dialogContents,
        events: {
            "click .prev": function () {
                this.model.prev();
            },
            "click .next": function () {
                this.model.next();
            }
        },
        initialize: function () {
            this.listenTo(this.model, "step", this.render);
            if (_.isObject(this.model.current())) {
                this.render();
            }
        },
        render: function () {
            var currentStep = this.model.current();
            var dialogContents = this.template({
                text: [].concat(currentStep.text),
                primaryButtonText: this.model.hasNext() ? AJS.I18n.getText("common.forms.next") : AJS.I18n.getText("common.words.done"),
                backButtonText: AJS.I18n.getText("common.concepts.back"),
                hasBack: this.model.hasPrev(),
                step: this.model.currentIndex() + 1,
                totalSteps: this.model.get("steps").length
            });
            this._renderDialog(dialogContents, currentStep.trigger, {
                gravity: currentStep.gravity
            });

            return this;
        },
        _renderDialog: function (dialogContents, trigger, options) {
            var $window = $(window);
            // Remove a previously open InlineDialog from the DOM. This is a limitation of InlineDialog as there is no way to cleanly close it and remove it from the DOM.
            // AJS.InlineDialog.current is set only after the dialog completely fades in so we can't use it.
            if (this.dialog) {
                this.dialog.remove();
                $window.off('resize.nutshell');
            }
            var instance = this;
            this.setElement(AJS.InlineDialog($(trigger), this.id,
                function (content, trigger, showPopup) {
                    content.html(dialogContents);
                    $window.on('resize.nutshell', function windowScroll() {
                        instance.dialog.refresh();
                    });
                    showPopup();
                    return false;
                }, _.extend({
                    noBind: true,
                    hideDelay: null,
                    addActiveClass: false,
                    persistent: true
                }, options)
            ));
            this.dialog = this.$el;
            var cid = this.id;
            $(document).bind("showLayer",function(e,type,layer) {
                if (type === "inlineDialog" && layer.id === cid) {
                    AJS.InlineDialog.current = null; // nutshell chaperones shouldn't be considered InlineDialogs.
                    $(document.body).unbind("click."+cid+".inline-dialog-check");
                    layer._validateClickToClose = function() { return false; };
                    layer.hide = function() { return false };
                }
            });
            this.dialog.show();
            // MEGA HACK: add a css class to apply the orange border offset
            $("#arrow-" + this.id).addClass(options.gravity || AJS.InlineDialog.opts.gravity);
        },
        remove: function() {
            $(window).off('resize.nutshell');

            return Backbone.View.prototype.remove.apply(this, arguments);
        }
    });
});
