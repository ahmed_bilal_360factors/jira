define("jira/onboarding/nutshell-sequence/models/sequence-step", [], function () {
    function SequenceStep(props) {
        this.id = props.id;
        this.text = props.text;
        this.trigger = props.trigger;
        this.gravity = props.gravity;
    }

    var index = 0;

    function next() {
        return index++;
    }

    SequenceStep.PROJECTS = next();
    SequenceStep.PROJECTS_ISSUES = next();
    SequenceStep.ISSUE = next();
    SequenceStep.ISSUE_SUMMARY = next();
    SequenceStep.ISSUE_KEY = next();
    SequenceStep.ISSUE_ASSIGNEE = next();
    SequenceStep.ISSUE_STATUS = next();
    SequenceStep.WORKFLOW = next();
    SequenceStep.WORKFLOW_BUTTONS = next();
    SequenceStep.WORKFLOW_CLICK = next();
    SequenceStep.WORKFLOW_DONE = next();
    SequenceStep.ALL_DONE = next();

    return SequenceStep;
});
