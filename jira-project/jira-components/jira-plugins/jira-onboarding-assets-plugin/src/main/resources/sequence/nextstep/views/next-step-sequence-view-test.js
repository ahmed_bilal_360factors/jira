AJS.test.require('com.atlassian.jira.jira-onboarding-assets-plugin:next-step-component', function() {
    'use strict';


    var NextStepSequenceView = require('jira/onboarding/next-step-sequence-view');

    // Note: Using actual icon urls to stop a 404 error when trying to display them which could cause problems if QUnit
    // is changed to fail on javascript errors.
    var STUB_STEPS = [
        {
            key: 'test',
            url: 'testUrl',
            label: 'label',
            icon: AJS.contextPath() + '/download/resources/com.atlassian.jira.jira-onboarding-assets-plugin:next-step-component/create.svg'
        },
        {
            key: 'test2',
            url: 'testUrl2',
            label: 'label2',
            icon: AJS.contextPath() + '/download/resources/com.atlassian.jira.jira-onboarding-assets-plugin:next-step-component/create.svg'
        }
    ];


    module('onboarding/next-step-sequence-view', {
        setup: function () {
            this.element = AJS.$('#qunit-fixture');
        }
    });

    test('Creating a view with no steps throws an error', function() {
        try {
            new NextStepSequenceView({
                el: this.element,
                steps: []
            });
            ok(false, 'Should have thrown an error on creation with no steps');
        } catch (e) {
            ok(true);
        }

    });

    test('Clicking a step triggers an event', function() {
        var view = new NextStepSequenceView({
            el: this.element,
            steps: STUB_STEPS
        });

        view.render();

        var stepSelectedSpy = sinon.spy();
        view.bind('stepSelected', stepSelectedSpy);

        this.element.find('.next-step[data-step-key=' + STUB_STEPS[0].key + ']').click();

        ok(stepSelectedSpy.calledOnce, 'Should have triggered the step selected');
        ok(stepSelectedSpy.withArgs(STUB_STEPS[0]).calledOnce, 'Should have triggered step selected with the right object');
    });
});
