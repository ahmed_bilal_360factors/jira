/* global define, Promise */
define('jira/onboarding/assign-issues-sequence', ['require'], function(require) {
    var _ = require('underscore');
    var AssignIssuesSequenceView = require('jira/onboarding/assignissues/views/assign-issues-sequence-view');

    var AssignIssuesSequence = function(options) {
        options = options || {};
        this.issues = options.issues || [];
        this.totalUsers = options.totalUsers || 0;
        this.assigneeOptions = options.assigneeOptions || [];
        this.canAssignIssues = options.canAssignIssues || false;
    };

    AssignIssuesSequence.prototype._hasIssues = function() {
        return _.isArray(this.issues) && this.issues.length;
    };

    AssignIssuesSequence.prototype._hasUsers = function() {
        return this.totalUsers > 1;
    };

    AssignIssuesSequence.prototype.init = function(container, analytics) {
        var instance = this;

        return new Promise(function(resolve, reject) {
            if (!instance.canAssignIssues) {
                analytics.pushEvent(AssignIssuesSequence.EVENTS.NO_PERMISSION);
                reject(AssignIssuesSequence.EVENTS.NO_PERMISSION);
                return;
            }
            if (!instance._hasIssues()) {
                analytics.pushEvent(AssignIssuesSequence.EVENTS.NO_ISSUES);
                reject(AssignIssuesSequence.EVENTS.NO_ISSUES);
                return;
            }
            if (!instance._hasUsers()) {
                analytics.pushEvent(AssignIssuesSequence.EVENTS.INSUFFICIENT_USERS);
                reject(AssignIssuesSequence.EVENTS.INSUFFICIENT_USERS);
                return;
            }

            var view = new AssignIssuesSequenceView({
                issues: instance.issues,
                assigneeOptions: instance.assigneeOptions,
                el: container
            });
            view.once("done", function(response) {
                analytics.pushEvent(AssignIssuesSequence.EVENTS.DONE);
                resolve();
            });
            view.once("skip", function() {
                analytics.pushEvent(AssignIssuesSequence.EVENTS.SKIPPED);
                reject(AssignIssuesSequence.EVENTS.SKIPPED);
            });
            view.on("issueAssigned", function(data) {
                analytics.pushEvent(AssignIssuesSequence.EVENTS.ISSUE_ASSIGNED, data);
            });

            view.render();
        });
    };

    AssignIssuesSequence.EVENTS = {
        NO_PERMISSION: "noPermission",
        NO_ISSUES: "noIssues",
        INSUFFICIENT_USERS: "insufficientUsers",
        ISSUE_ASSIGNED: "issueAssigned",
        SKIPPED: "skip",
        DONE: "done"
    };

    return AssignIssuesSequence;
});
