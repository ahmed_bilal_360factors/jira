/* global define */

define('jira/onboarding/createissues/views/create-issues-sequence-view', ['require'], function(require) {
    var Backbone = require('backbone');
    var IICWidget = require('jira/onboarding/createissues/issuecreator/project-issue-create-widget');
    var Templates = JIRA.Onboarding.Sequence.CreateIssues;

    return Backbone.View.extend({
        initialize: function() {
            if (!(this.model instanceof Backbone.Model)) {
                this.model = new Backbone.Model(this.model);
            }
            this.createdIssues = new Backbone.Collection([]);
            this.listenTo(this.createdIssues, 'add', this._issueCreated);
        },

        events: {
            'click #skip': '_skip',
            'click #next': '_next'
        },

        render: function() {
            var issues = this.createdIssues.toJSON();
            this.$el.html(Templates.render({
                projectName: this.model.get('name'),
                issues: issues
            }));
            if (issues.length === 0) {
                this.$('.issue-table').hide();
            }
            var iicEl = this.$('.js-issue-creator');
            var creator = new IICWidget({
                el: iicEl,
                projectName: this.model.get('name'),
                projectId: this.model.get('id')
            });

            this.listenTo(creator, 'issueCreated', function(issue) {
                this.createdIssues.add(issue);
            });

            creator.init();

            return this;
        },

        _issueCreated: function(model) {
            var issue = model.toJSON();
            var issueTableEl = this.$('.issue-table');
            issueTableEl.show();
            var nextButtonEl = this.$('#next');
            var tableRowHtml = Templates.projectIssueTableRow(issue);
            issueTableEl.find('tbody').append(tableRowHtml);
            if (this.createdIssues.length) {
                nextButtonEl.removeAttr('disabled').removeAttr('aria-disabled');
            }
            this.trigger('issueCreated', issue);
        },

        _skip: function onSkip(e) {
            e.preventDefault();
            this.trigger('skip');
        },

        _next: function onNext(e) {
            e.preventDefault();
            this.trigger('done');
        }
    });
});
