/* global WRM */
/* global AJS */
define("jira/onboarding/jira-first-use-flow", ['require'], function (require) {
    "use strict";

    var _ = require('underscore');
    var $ = require('jquery');
    var SequenceBuilder = require('jira/onboarding/sequence-builder');
    var AvatarPickerSequence = require('jira/onboarding/avatar-picker-sequence');
    var NutshellSequence = require('jira/onboarding/nutshell-sequence');
    var NextStepSequence = require('jira/onboarding/next-step-sequence');
    var CreateProjectSequence = require('jira/onboarding/create-project-sequence');
    var CreateIssuesSequence = require('jira/onboarding/create-issues-sequence');
    var AssignIssuesSequence = require('jira/onboarding/assign-issues-sequence');
    var LoadingSequence = require('jira/onboarding/loading-sequence');
    var Browser = require('jira/util/browser');

    var LOADING_TIMEOUT = 5000;

    function goToDashboard() {
        Browser.reloadViaWindowLocation(AJS.contextPath() + "/secure/Dashboard.jspa");
    }

    function goToProject(projectKey) {
        Browser.reloadViaWindowLocation(AJS.contextPath() + '/projects/' + projectKey + '/issues');
    }

    function _verifyPermissions(projectKey, permissionKey) {
        return Promise.resolve($.get(AJS.contextPath() + '/rest/api/2/mypermissions', {
            projectKey: projectKey
        })).then(function(data) {
            var permission = data.permissions[permissionKey];
            return permission && permission.havePermission;
        });
    }
    function canCreateIssuesTo(projectKey) {
        return _verifyPermissions(projectKey, "CREATE_ISSUES");
    }

    function canAssignIssuesTo(projectKey) {
        return _verifyPermissions(projectKey, "ASSIGN_ISSUES");
    }

    /**
     * @name FirstUseFlowFactory
     *
     * @param {Function} complete function to tell the server that the flow is complete.
     * @return {FirstUseFlow}
     */
    return function (complete) {
        var avatarData = WRM.data.claim("com.atlassian.jira.jira-onboarding-assets-plugin:avatar-picker-sequence.data");
        var nutshellData = WRM.data.claim("com.atlassian.jira.jira-onboarding-assets-plugin:nutshell-sequence.data");
        var createProjectData = WRM.data.claim('com.atlassian.jira.jira-onboarding-assets-plugin:create-project-sequence.data');
        var assignIssuesData = WRM.data.claim('com.atlassian.jira.jira-onboarding-assets-plugin:assign-issues-sequence.data');
        var nextStepData = WRM.data.claim('com.atlassian.jira.jira-onboarding-assets-plugin:next-step-sequence.data');
        var loadingCreateIssuesSequenceBuilder = new SequenceBuilder(LoadingSequence);
        var createIssuesSequenceBuilder = new SequenceBuilder(CreateIssuesSequence);
        var loadingAssignIssuesSequenceBuilder = new SequenceBuilder(LoadingSequence);
        var assignIssuesSequenceBuilder = new SequenceBuilder(AssignIssuesSequence, assignIssuesData);

        function decideNextStep(sequenceBuilder) {
            return function () {
                if (sequenceBuilder.data.projectKey) {
                    var goToThatProject = _.partial(goToProject, sequenceBuilder.data.projectKey);
                    complete().then(goToThatProject, goToThatProject);
                } else {
                    // this should never be reached, unless something goes wrong in create project sequence
                    return "nextStep";
                }
            }
        }

        //Note: If the key or sequence keys are changed the onboarding-analytics.json file must be updated.
        return {
            key: "jiraFirstUseFlow",
            start: "avatar",
            sequences: {
                "avatar": {
                    instance: new AvatarPickerSequence(avatarData),
                    resolve: "nutshell",
                    reject: "nutshell"
                },
                "nutshell": {
                    instance: new NutshellSequence(nutshellData),
                    resolve: "createProject",
                    reject: "createProject"
                },
                "createProject": {
                    instance: new CreateProjectSequence(createProjectData),
                    resolve: function (projectData) {
                        createIssuesSequenceBuilder.addData(projectData);
                        loadingCreateIssuesSequenceBuilder.addData({
                            loading: canCreateIssuesTo(projectData.projectKey),
                            timeout: LOADING_TIMEOUT
                        });
                        assignIssuesSequenceBuilder.addData(projectData);
                        loadingAssignIssuesSequenceBuilder.addData({
                            loading: canAssignIssuesTo(projectData.projectKey),
                            timeout: LOADING_TIMEOUT
                        });
                        return "loading:createIssues";
                    },
                    reject: "nextStep"
                },
                "loading:createIssues": {
                    instance: loadingCreateIssuesSequenceBuilder,
                    resolve: function (canCreateIssues) {
                        createIssuesSequenceBuilder.addData({
                            canCreateIssues: canCreateIssues
                        });
                        return "createIssues";
                    },
                    reject: decideNextStep(createIssuesSequenceBuilder)
                },
                "createIssues": {
                    instance: createIssuesSequenceBuilder,
                    resolve: function (issueData) {
                        assignIssuesSequenceBuilder.addData(issueData);
                        return "loading:assignIssues";
                    },
                    reject: decideNextStep(createIssuesSequenceBuilder)
                },
                "loading:assignIssues": {
                    instance: loadingAssignIssuesSequenceBuilder,
                    resolve: function (canAssignIssues) {
                        assignIssuesSequenceBuilder.addData({
                            canAssignIssues: canAssignIssues
                        });
                        return "assignIssues";
                    },
                    reject: decideNextStep(assignIssuesSequenceBuilder)
                },
                "assignIssues": {
                    instance: assignIssuesSequenceBuilder,
                    resolve: function() {
                        var goToThatProject = _.partial(goToProject, assignIssuesSequenceBuilder.data.projectKey);
                        complete().then(goToThatProject, goToThatProject);
                    },
                    reject: decideNextStep(assignIssuesSequenceBuilder)
                },
                "nextStep": {
                    instance: new NextStepSequence(nextStepData),
                    resolve: complete,
                    reject: function () {
                        // if no permissions for the next step
                        complete().then(goToDashboard, goToDashboard);
                    }
                }
            }
        };
    };
});
