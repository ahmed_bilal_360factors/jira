/* global Promise */
define('jira/onboarding/analytics', function () {
    'use strict';

    /**
     * @class OnboardingAnalytics
     * @global
     *
     * @param {String} flowKey
     * @param {String} sequenceKey
     * @constructor
     */
    var OnboardingAnalytics = function(flowKey, sequenceKey) {
        this.flowKey = flowKey;
        this.sequenceKey = sequenceKey;
    };

    /**
     * Push an analytics event with a given name
     * @param {String} eventName for the event
     * @param {Object} [properties] to add to the analytics event
     */
    OnboardingAnalytics.prototype.pushEvent = function(eventName, properties) {
        if (this.canPushEvent()) {
            var analyticsEventName = OnboardingAnalytics.ONBOARDING_EVENT_KEY + '.' + this.flowKey + '.' + this.sequenceKey + '.' + eventName;
            properties = properties || {};
            AJS.trigger('analyticsEvent', {
                name: analyticsEventName,
                data: properties
            });
        }
    };

    /**
     * Whether this analytics object can push an event. This protects against undefined/null errors.
     * @returns {Boolean}
     */
    OnboardingAnalytics.prototype.canPushEvent = function() {
        return (AJS.trigger && this.flowKey && this.sequenceKey);
    };

    OnboardingAnalytics.ONBOARDING_EVENT_KEY = 'onboarding';


    //Note: You should not changed these string literals as it would break the whitelist for specific first use flows
    OnboardingAnalytics.EVENTS = {
        STARTED_SEQUENCE: 'started'
    };

    return OnboardingAnalytics;
});
