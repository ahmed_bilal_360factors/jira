(function() {
    var KeyboardShortcutToggle = require('jira/ajs/keyboardshortcut/keyboard-shortcut-toggle');
    var $ = require('jquery');

    $(document).ready(function() {
        KeyboardShortcutToggle.disable();

        disableLogoClick();
    });

    /**
     * We do not want them to be able to click the logo button to go to the Dashboard
     */
    function disableLogoClick() {
        $('a', '#logo').removeAttr('href');
    }

})();