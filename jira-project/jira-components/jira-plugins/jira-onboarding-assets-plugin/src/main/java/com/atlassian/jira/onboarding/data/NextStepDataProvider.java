package com.atlassian.jira.onboarding.data;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import com.google.common.collect.Maps;

@Scanned
public class NextStepDataProvider implements WebResourceDataProvider
{
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final PermissionManager permissionManager;

    public NextStepDataProvider(
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final PermissionManager permissionManager)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.permissionManager = permissionManager;
    }

    @Override
    public Jsonable get()
    {
        return new Jsonable()
        {
            @Override
            public void write(final Writer writer) throws IOException
            {
                try
                {
                    getJsonData().write(writer);
                }
                catch (JSONException e)
                {
                    throw new JsonMappingException(e);
                }
            }
        };
    }

    private JSONObject getJsonData()
    {
        final ApplicationUser user = jiraAuthenticationContext.getUser();

        final Map<String, Object> values = Maps.newHashMap();
        values.put("canBrowseProjects", canBrowseProjects(user));
        values.put("canCreateIssues", canCreateIssues(user));
        values.put("canSearchIssues", canSearchIssues(user));
        return new JSONObject(values);
    }

    private boolean canCreateIssues(final ApplicationUser user) {
        return permissionManager.hasProjects(ProjectPermissions.CREATE_ISSUES, user);
    }

    private boolean canBrowseProjects(final ApplicationUser user) {
        return permissionManager.hasProjects(ProjectPermissions.BROWSE_PROJECTS, user);
    }

    private boolean canSearchIssues(final ApplicationUser user) {
        return canBrowseProjects(user);
    }
}
