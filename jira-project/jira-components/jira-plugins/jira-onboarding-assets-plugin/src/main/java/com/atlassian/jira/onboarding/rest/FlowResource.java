package com.atlassian.jira.onboarding.rest;

import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.jira.onboarding.OnboardingService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

@Path ("/flow")
@Produces ({ MediaType.APPLICATION_JSON })
@Consumes ({ MediaType.APPLICATION_JSON })
@Scanned
public class FlowResource
{
    private final JiraAuthenticationContext authenticationContext;
    private final OnboardingService onboardingService;

    public FlowResource(
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final OnboardingService onboardingService)
    {
        this.authenticationContext = jiraAuthenticationContext;
        this.onboardingService = onboardingService;
    }

    @POST
    @Path ("/{flow}/complete")
    public Response completeFlow(@PathParam ("flow") final String flowKey)
    {
        if (isCompletingFlow(flowKey))
        {
            onboardingService.completeFirstUseFlow(getUser());
            return Response.ok().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).entity("Has not started a flow").build();
    }

    @POST
    @Path ("/{flow}/sequence/current/{sequenceKey}")
    public Response setCurrentSequence(
            @PathParam ("flow") final String flowKey,
            @PathParam ("sequenceKey") final String sequenceKey)
    {
        if (isCompletingFlow(flowKey))
        {
            onboardingService.setCurrentFirstUseFlowSequence(getUser(), sequenceKey);
            return Response.ok().build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity("Has not started a flow").build();
    }

    /**
     * @param flowKey we use to check if we have started
     * @return whether we are completing this flow
     */
    private boolean isCompletingFlow(@Nullable final String flowKey)
    {
        final String currentFlow = onboardingService.getStartedFirstUseFlowKey(getUser());
        return flowKey != null && currentFlow != null && currentFlow.equals(flowKey);
    }

    /**
     * @return user for the request
     */
    private ApplicationUser getUser()
    {
        return authenticationContext.getUser();
    }
}
