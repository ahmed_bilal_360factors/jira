package com.atlassian.jira.onboarding.data;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import com.atlassian.jira.onboarding.OnboardingService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.web.action.ActionViewData;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import com.google.common.collect.Maps;

@Scanned
public class SequenceControllerDataProvider implements WebResourceDataProvider
{
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final OnboardingService onboardingService;

    public SequenceControllerDataProvider(
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final OnboardingService onboardingService)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.onboardingService = onboardingService;
    }

    @Override
    public Jsonable get()
    {
        return new Jsonable()
        {
            @Override
            public void write(final Writer writer) throws IOException
            {
                try
                {
                    getJsonData().write(writer);
                }
                catch (JSONException e)
                {
                    throw new JsonMappingException(e);
                }
            }
        };
    }

    private JSONObject getJsonData()
    {
        final ApplicationUser user = jiraAuthenticationContext.getUser();

        final Map<String, Object> values = Maps.newHashMap();
        values.put("currentSequenceKey", getCurrentSequenceKey(user));
        values.put("currentFlow", getCurrentFlowKey(user));
        return new JSONObject(values);
    }


    @ActionViewData
    public String getCurrentSequenceKey(final ApplicationUser user)
    {
        return onboardingService.getCurrentFirstUseFlowSequence(user);
    }

    @ActionViewData
    public String getCurrentFlowKey(final ApplicationUser user)
    {
        return onboardingService.getStartedFirstUseFlowKey(user);
    }
}
