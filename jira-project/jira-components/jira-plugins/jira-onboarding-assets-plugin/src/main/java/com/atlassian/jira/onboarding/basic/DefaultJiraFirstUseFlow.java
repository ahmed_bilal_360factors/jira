package com.atlassian.jira.onboarding.basic;

import com.atlassian.jira.onboarding.FirstUseFlow;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;

public class DefaultJiraFirstUseFlow implements FirstUseFlow
{
    @Override
    public boolean isApplicable(final ApplicationUser user)
    {
        return true;
    }

    @Override
    @Nonnull
    public String getUrl()
    {
        return "/secure/WelcomeToJIRA.jspa";
    }
}
