package com.atlassian.jira.onboarding.data;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import com.google.common.collect.Maps;

@Scanned
public class CreateProjectSequenceData implements WebResourceDataProvider
{
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final GlobalPermissionManager globalPermissionManager;
    private final ProjectService projectService;

    public CreateProjectSequenceData(
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final GlobalPermissionManager globalPermissionManager,
            @ComponentImport final ProjectService projectService)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.globalPermissionManager = globalPermissionManager;
        this.projectService = projectService;
    }

    @Override
    public Jsonable get()
    {
        return new Jsonable()
        {
            @Override
            public void write(final Writer writer) throws IOException
            {
                try
                {
                    getJsonData().write(writer);
                }
                catch (JSONException e)
                {
                    throw new JsonMappingException(e);
                }
            }
        };
    }

    private JSONObject getJsonData()
    {
        final ApplicationUser user = jiraAuthenticationContext.getUser();

        final Map<String, Object> values = Maps.newHashMap();
        values.put("username", getUsername(user));
        values.put("canCompleteCreateProjectSequence", getCanCompleteCreateProjectSequence(user));
        values.put("maxKeyLength", projectService.getMaximumKeyLength());
        values.put("maxNameLength", projectService.getMaximumNameLength());
        return new JSONObject(values);
    }


    private String getUsername(final ApplicationUser user)
    {
        return user.getUsername();
    }

    public Boolean getCanCompleteCreateProjectSequence(final ApplicationUser user)
    {
        return globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user);
    }
}
