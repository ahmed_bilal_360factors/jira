package com.atlassian.jira.onboarding.data;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import com.google.common.collect.Maps;

@Scanned
public class AvatarPickerDataProvider implements WebResourceDataProvider
{
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final AvatarService avatarService;
    private final AvatarManager avatarManager;


    public AvatarPickerDataProvider(
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final AvatarService avatarService,
            @ComponentImport final AvatarManager avatarManager)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.avatarService = avatarService;
        this.avatarManager = avatarManager;
    }

    @Override
    public Jsonable get()
    {
        return new Jsonable()
        {
            @Override
            public void write(final Writer writer) throws IOException
            {
                try
                {
                    getJsonData().write(writer);
                }
                catch (JSONException e)
                {
                    throw new JsonMappingException(e);
                }
            }
        };
    }

    private JSONObject getJsonData()
    {
        final ApplicationUser user = jiraAuthenticationContext.getUser();

        final Map<String, Object> values = Maps.newHashMap();
        values.put("username", getUsername(user));
        values.put("userFullname", getUserFullName(user));
        values.put("userAvatarId", getUserAvatarId(user));
        values.put("userAvatarUrl", getUserAvatarUrl(user));
        values.put("defaultUserAvatarId", getDefaultUserAvatarId());
        return new JSONObject(values);
    }

    private Long getUserAvatarId(final ApplicationUser user)
    {
        return avatarService.getAvatar(user, user).getId();
    }

    private String getUserAvatarUrl(final ApplicationUser user)
    {
        return avatarService.getAvatarAbsoluteURL(user, user, Avatar.Size.defaultSize()).toString();
    }

    private String getDefaultUserAvatarId()
    {
        return String.valueOf(avatarManager.getDefaultAvatarId(Avatar.Type.USER));
    }

    private String getUsername(final ApplicationUser user)
    {
        return user.getUsername();
    }

    private String getUserFullName(final ApplicationUser user)
    {
        return user.getDisplayName();
    }

}
