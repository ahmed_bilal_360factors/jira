package com.atlassian.jira.onboarding.data;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.user.search.AssigneeService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.Assignees;
import com.atlassian.jira.issue.fields.option.AssigneeOption;
import com.atlassian.jira.issue.fields.option.AssigneeOptions;
import com.atlassian.jira.issue.fields.option.OptionGroup;
import com.atlassian.jira.issue.fields.option.SelectChild;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

@Scanned
public class AssignIssuesSequenceDataProvider implements WebResourceDataProvider
{
    private Assignees assignees;
    private final ApplicationProperties applicationProperties;
    private final AssigneeService assigneeService;
    private final AvatarService avatarService;
    private final UserManager userManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final EmailFormatter emailFormatter;

    public AssignIssuesSequenceDataProvider(
            @ComponentImport final ApplicationProperties applicationProperties,
            @ComponentImport final AssigneeService assigneeService,
            @ComponentImport final AvatarService avatarService,
            @ComponentImport final UserManager userManager,
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final EmailFormatter emailFormatter)
    {
        this.applicationProperties = applicationProperties;
        this.assigneeService = assigneeService;
        this.avatarService = avatarService;
        this.userManager = userManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.emailFormatter = emailFormatter;
    }

    @Override
    public Jsonable get()
    {
        return new Jsonable()
        {
            @Override
            public void write(final Writer writer) throws IOException
            {
                try
                {
                    getJsonData().write(writer);
                }
                catch (JSONException e)
                {
                    throw new JsonMappingException(e);
                }
            }
        };
    }

    private JSONObject getJsonData()
    {
        final ApplicationUser loggedInUser = jiraAuthenticationContext.getUser();
        final List<Issue> issues = Lists.newArrayList();
        final List<User> suggestedUsers = Lists.newArrayList(loggedInUser.getDirectoryUser());
        final AssigneeOptions options = getAssigneesObject().makeAssigneeOptions(issues, null, suggestedUsers, null, false);

        final List<JSONObject> optionsList = Lists.newArrayList();
        for (SelectChild thing : options.getOptions())
        {
            if (thing.isOptionGroup())
            {
                optionsList.add(makeAssigneeOptionGroupData((OptionGroup)thing));
            }
            else
            {
                optionsList.add(makeAssigneeOptionData((AssigneeOption)thing));
            }
        }

        final Map<String, Object> values = Maps.newHashMap();
        values.put("assigneeOptions", optionsList);
        values.put("loggedInUserAssignable", options.isLoggedInUserAssignable());
        values.put("totalUsers", userManager.getTotalUserCount());

        return new JSONObject(values);
    }

    private Assignees getAssigneesObject()
    {
        if (assignees == null)
        {
            assignees = new Assignees(jiraAuthenticationContext, applicationProperties, assigneeService, avatarService, userManager, emailFormatter);
        }
        return assignees;
    }

    /**
     * This method exists because our version of {@link JSONObject} (v2) fails
     * with IllegalArgumentException when attempting to reflect on Objects.
     * Essentially it's a bug that exists in our copy-pasta of JSON-java.
     */
    private JSONObject makeAssigneeOptionGroupData(final OptionGroup group)
    {
        final Map<String, Object> values = Maps.newHashMap();
        final List<JSONObject> options = Lists.newArrayList();
        for (AssigneeOption assigneeOption : group.getGroupOptions())
        {
            options.add(makeAssigneeOptionData(assigneeOption));
        }
        values.put("id", group.getId());
        values.put("display", group.getDisplay());
        values.put("footer", group.getFooter());
        values.put("groupOptions", options);
        values.put("optionGroup", group.isOptionGroup());
        values.put("weight", group.getWeight());
        return new JSONObject(values);
    }

    /**
     * This method exists because our version of {@link JSONObject} (v2) fails
     * with IllegalArgumentException when attempting to reflect on Objects.
     * Essentially it's a bug that exists in our copy-pasta of JSON-java.
     */
    private JSONObject makeAssigneeOptionData(final AssigneeOption option)
    {
        final Map<String, Object> values = Maps.newHashMap();
        values.put("id", option.getId());
        values.put("avatarURL", option.getAvatarURL());
        values.put("displayName", option.getDisplayName());
        values.put("emailAddress", option.getEmailAddress());
        values.put("loggedInUser", option.isLoggedInUser());
        values.put("optionEnabled", option.isOptionEnabled());
        values.put("optionGroup", option.isOptionGroup());
        values.put("optionName", option.getOptionName());
        values.put("selected", option.isSelected());
        return new JSONObject(values);
    }
}
