package com.atlassian.jira.onboarding.basic;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.jira.util.JiraUrlCodec;
import com.atlassian.jira.web.action.ActionViewData;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.assembler.PageBuilderService;

@Scanned
public class FirstUseFlowAction extends JiraWebActionSupport
{
    private final PageBuilderService pageBuilderService;
    private final PluginAccessor pluginAccessor;

    public FirstUseFlowAction(
            @ComponentImport final PluginAccessor pluginAccessor,
            @ComponentImport final PageBuilderService pageBuilderService)
    {
        this.pageBuilderService = pageBuilderService;
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    protected String doExecute() throws Exception
    {
        if (getLoggedInApplicationUser() == null)
        {
            return redirectToLogin();
        }

        if (pluginAccessor.isPluginEnabled("com.atlassian.analytics.analytics-client"))
        {
            pageBuilderService.assembler().resources().requireWebResource("com.atlassian.analytics.analytics-client:js-events");
        }
        pageBuilderService.assembler().resources().requireWebResource("com.atlassian.jira.jira-onboarding-assets-plugin:jira-first-use-flow");

        return super.doExecute();
    }

    /**
     * @return a login redirect URL so after logging in they return to this page
     */
    private String redirectToLogin()
    {
        HttpServletRequest request = getHttpRequest();
        String returnURL = request.getRequestURI().substring(request.getContextPath().length());
        if (request.getQueryString() != null)
        {
            returnURL += "?" + request.getQueryString();
        }

        return getRedirect("/login.jsp?os_destination=" + JiraUrlCodec.encode(returnURL), false);
    }

    @ActionViewData
    public String getPageTitle()
    {
        return getText("onboarding.welcome.to.jira");
    }

}
