package com.atlassian.jira.onboarding.data;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import com.google.common.collect.Maps;

@Scanned
public class NutshellDataProvider implements WebResourceDataProvider
{
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final AvatarService avatarService;

    public NutshellDataProvider(
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final AvatarService avatarService)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.avatarService = avatarService;
    }

    @Override
    public Jsonable get()
    {
        return new Jsonable()
        {
            @Override
            public void write(final Writer writer) throws IOException
            {
                try
                {
                    getJsonData().write(writer);
                }
                catch (JSONException e)
                {
                    throw new JsonMappingException(e);
                }
            }
        };
    }

    private JSONObject getJsonData()
    {
        final ApplicationUser user = jiraAuthenticationContext.getUser();

        final Map<String, Object> values = Maps.newHashMap();
        values.put("defaultProjectAvatarUrl", getDefaultProjectAvatarUrl());
        values.put("userFullname", user.getDisplayName());
        return new JSONObject(values);
    }

    private String getDefaultProjectAvatarUrl()
    {
        return String.valueOf(avatarService.getProjectDefaultAvatarAbsoluteURL(Avatar.Size.defaultSize()));
    }
}
