(function loadMarionette() {
    var jiraViewIssuePluginMarionette = Marionette;
    define('jira/view-issue-plugin/lib/marionette', [], function defineMarionette() {
        return jiraViewIssuePluginMarionette.noConflict();
    });
})();