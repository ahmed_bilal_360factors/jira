(function loadAttachmentContentsView() {
    define('jira/attachment/attachment-contents-view', [
        'jira/view-issue-plugin/lib/marionette'
    ], function defineAttachmentContentsView(Marionette) {
        'use strict';

        return Marionette.ItemView.extend({
            template: JIRA.Templates.ViewIssue.attachmentContents,

            initialize: function initialize(options) {
                this.attachment = options.attachment;
                this.issueId = options.issueId;
                this.baseUrl = options.baseUrl;
            },

            serializeData: function serializeData() {
                return {
                    attachment: this.attachment,
                    issueId: this.issueId,
                    baseUrl: this.baseUrl
                };
            }

        });
    });
})();