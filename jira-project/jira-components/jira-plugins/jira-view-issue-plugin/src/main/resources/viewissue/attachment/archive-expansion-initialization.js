AJS.$(function loadArchiveExpansion() {

    require([
        'jquery',
        'jira/view-issue-plugin/lib/marionette',
        'jira/attachment/attachment-contents-view'
    ], function initializeArchiveExpansion($, Marionette, AttachmentContentsView) {
        'use strict';

        JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function rewireArchiveExpansion(e, $element) {
            if (contains($element, '#file_attachments')) {
                wireArchiveExpansion($element);
            }
        });

        wireArchiveExpansion($('#file_attachments'));

        function contains($element, selector) {
            return $element.is(selector) || $element.find(selector).length > 0;
        }

        function wireArchiveExpansion($root) {
            var expanders = $root.find('.attachment-content .expander');
            expanders.each(function wireExpander(expanderIndex, expander) {
                $(expander).one('expandBlock', expandArchive);
            });
        }

        function expandArchive() {
            var $expander = $(this);
            var $attachmentRow = $expander.parents('.attachment-content').first();
            var attachmentId = $attachmentRow.data('attachment-id');
            var issueId = $attachmentRow.data('issue-id');
            var expansionResource = AJS.contextPath() + "/rest/api/2/attachment/" + attachmentId + "/expand/human";
            var promise = $.get(expansionResource);
            promise.done(function renderAttachmentContents(data) {
                var region = new Marionette.Region({
                    el: $expander.find('.zip-contents').first()
                });
                var view = new AttachmentContentsView({
                    attachment: data,
                    issueId: issueId,
                    baseUrl: window.location.protocol + "//" + window.location.host + AJS.contextPath()
                });
                region.show(view);
            });
            promise.error(function handleAttachmentExpansionError(data) {
                console.error(data);
            });
        }

    });
});