package com.atlassian.jira.my_home;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;

/**
 * Loads and stores the My JIRA Home for the given user.
 */
public interface MyJiraHomeStorage
{
    /**
     * Returns the home stored for the given user.
     *
     * @param user the user for which the home is requested
     * @return the home if found, else an empty string
     * @deprecated use load(ApplicationUser user) instead
     */
    @Nonnull
    @Deprecated
    String load(@Nonnull User user);

    /**
     * Returns the home stored for the given user.
     *
     * @param user the user for which the home is requested
     * @return the home if found, else an empty string
     * @since 6.4
     */
    @Nonnull
    String load(@Nonnull ApplicationUser user);

    /**
     * Stores the given home for the given user.
     *
     * @param user the user for which the home is stored
     * @param home the actual home to be stored
     *
     * @throws com.atlassian.jira.plugin.myjirahome.MyJiraHomeUpdateException if the update fails
     * @deprecated use store(ApplicationUser user, String home) instead
     */
    @Deprecated
    void store(@Nonnull User user, @Nonnull String home);

    /**
     * Stores the given home for the given user.
     *
     * @param user the user for which the home is stored
     * @param home the actual home to be stored
     *
     * @throws com.atlassian.jira.plugin.myjirahome.MyJiraHomeUpdateException if the update fails
     * @since 6.4
     */
    void store(@Nonnull ApplicationUser user, @Nonnull String home);
}
