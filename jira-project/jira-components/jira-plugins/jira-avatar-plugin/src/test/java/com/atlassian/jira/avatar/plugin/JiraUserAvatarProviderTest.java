package com.atlassian.jira.avatar.plugin;


import javax.annotation.Nullable;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.GravatarSettings;
import com.atlassian.jira.config.util.EncodingConfiguration;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.BaseUrl;
import com.atlassian.mock.propertyset.MockPropertySet;
import com.atlassian.plugins.avatar.AuiSize;
import com.atlassian.plugins.avatar.PluginAvatar;

import com.google.common.base.Function;
import com.opensymphony.module.propertyset.PropertySet;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.avatar.Avatar.Type.USER;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class JiraUserAvatarProviderTest
{
    private static final String FRED_HASH = "6255165076a5e31273cbda50bb9f9636";
    private static final String LIBRAVATAR_API = "http://cdn.libravatar.org/avatar/";
    private static final String SMALL_GRAVATAR_PARAMS = "?d=mm&s=16";
    private static final String FRED_LIBRAVATAR_URL = LIBRAVATAR_API + FRED_HASH + SMALL_GRAVATAR_PARAMS;
    private static final String FRED_SMALL_GRAVATAR_URL = "http://www.gravatar.com/avatar/" + FRED_HASH + SMALL_GRAVATAR_PARAMS;
    private static final String BASE_URL = "http://jira.atlassian.com";

    private final byte[] whateverAvatarBytes = new byte[] { 1, 3, 3, 7 };
    private final Avatar whateverAvatar = new PluggyMockAvatar(123L, null, true, "image/png", whateverAvatarBytes);

    @Mock private GravatarSettings gravatarSettings;
    @Mock private UserManager userManager;
    @Mock private AvatarManager avatarManager;
    @Mock private UserPropertyManager userPropertyManager;
    @Mock private BaseUrl baseUrl;
    private EncodingConfiguration encodingConfiguration = new EncodingConfiguration.Static("UTF-8");

    private JiraUserAvatarProvider avatarProvider;
    private final Function<String, ApplicationUser> userByEmail = new Function<String, ApplicationUser>()
    {
        @Override
        public ApplicationUser apply(@Nullable final String email)
        {
            return userManager.getUserByKey(email);
        }
    };

    @Before
    public void setUp() throws Exception
    {
        when(userManager.isUserExisting(null)).thenReturn(false);

    }

    @Test
    public void gravatarShouldUseCustomApiUrlIfAvailable() throws Exception
    {
        final ApplicationUser knownUser = new MockApplicationUser("Fred", "Wilson", "fred@example.com");
        final PropertySet propertySet = new MockPropertySet();

        when(userManager.getUserByKey("fred@example.com")).thenReturn(knownUser);
        when(userManager.isUserExisting(knownUser)).thenReturn(true);
        when(userPropertyManager.getPropertySet(knownUser)).thenReturn(propertySet);
        when(gravatarSettings.isAllowGravatars()).thenReturn(true);
        when(gravatarSettings.getCustomApiAddress()).thenReturn(LIBRAVATAR_API);
        when(avatarManager.getDefaultAvatarId(USER)).thenReturn(whateverAvatar.getId());

        avatarProvider = new JiraUserAvatarProvider(baseUrl, encodingConfiguration, gravatarSettings, avatarManager, userManager, userPropertyManager, userByEmail);

        String url = avatarProvider.getAvatar("fred@example.com", AuiSize.XSMALL.getSize()).getUrl();
        assertThat(url, equalTo(FRED_LIBRAVATAR_URL));
    }

    @Test
    public void gravatarShouldReturnCorrectUrlForFredAtExampleCom() throws Exception
    {
        final ApplicationUser knownUser = new MockApplicationUser("Fred", "Wilson", "fred@example.com");

        when(userManager.getUserByKey("fred@example.com")).thenReturn(knownUser);
        when(userManager.isUserExisting(knownUser)).thenReturn(true);
        when(userPropertyManager.getPropertySet(knownUser)).thenReturn(new MockPropertySet());
        when(gravatarSettings.isAllowGravatars()).thenReturn(true);
        when(avatarManager.getDefaultAvatarId(USER)).thenReturn(whateverAvatar.getId());

        avatarProvider = new JiraUserAvatarProvider(baseUrl, encodingConfiguration, gravatarSettings, avatarManager, userManager, userPropertyManager, userByEmail);

        String url = avatarProvider.getAvatar("fred@example.com", AuiSize.XSMALL.getSize()).getUrl();

        assertThat(url, equalTo(FRED_SMALL_GRAVATAR_URL));
    }

    @Test
    public void gravatarShouldReturnCorrectUrlForFredAtExampleComInMixedCase() throws Exception
    {
        final ApplicationUser knownUser = new MockApplicationUser("Fred", "Wilson", "FrEd@eXamPle.CoM");
        when(userManager.getUserByKey("fred@example.com")).thenReturn(knownUser);
        when(userManager.isUserExisting(knownUser)).thenReturn(true);
        when(userPropertyManager.getPropertySet(knownUser)).thenReturn(new MockPropertySet());
        when(gravatarSettings.isAllowGravatars()).thenReturn(true);
        when(avatarManager.getDefaultAvatarId(USER)).thenReturn(whateverAvatar.getId());

        avatarProvider = new JiraUserAvatarProvider(baseUrl, encodingConfiguration, gravatarSettings, avatarManager, userManager, userPropertyManager, userByEmail);

        String url = avatarProvider.getAvatar("fred@example.com", AuiSize.XSMALL.getSize()).getUrl();

        assertThat(url, equalTo(FRED_SMALL_GRAVATAR_URL));
    }

    @Test
    public void avatarUrlShouldWorkWithoutContextPath() throws Exception
    {
        final ApplicationUser knownUser = new MockApplicationUser("Fred", "Wilson", "fred@example.com");
        PropertySet propertySet = mock(PropertySet.class);

        when(baseUrl.getCanonicalBaseUrl()).thenReturn(BASE_URL);
        when(baseUrl.getBaseUrl()).thenReturn("");
        when(userManager.getUserByKey("fred@example.com")).thenReturn(knownUser);
        when(userManager.isUserExisting(knownUser)).thenReturn(true);
        when(userPropertyManager.getPropertySet(knownUser)).thenReturn(propertySet);
        when(propertySet.exists(AvatarManager.USER_AVATAR_ID_KEY)).thenReturn(true);

        avatarProvider = new JiraUserAvatarProvider(baseUrl, encodingConfiguration, gravatarSettings, avatarManager, userManager, userPropertyManager, userByEmail);

        String url = avatarProvider.getAvatar("fred@example.com", AuiSize.XLARGE.getSize()).getUrl();
        assertThat(url, equalTo("http://jira.atlassian.com/secure/useravatar?size=xlarge")); // TODO make JIRA return relative URLs for self

    }

    @Test
    public void avatarUrlShouldWorkWithContextPath() throws Exception
    {
        final ApplicationUser knownUser = new MockApplicationUser("Fred", "Wilson", "fred@example.com");
        PropertySet propertySet = mock(PropertySet.class);

        when(baseUrl.getCanonicalBaseUrl()).thenReturn(BASE_URL + "/jira");
        when(baseUrl.getBaseUrl()).thenReturn("/jira");
        when(userManager.getUserByKey("fred@example.com")).thenReturn(knownUser);
        when(userManager.isUserExisting(knownUser)).thenReturn(true);
        when(userPropertyManager.getPropertySet(knownUser)).thenReturn(propertySet);
        when(propertySet.exists(AvatarManager.USER_AVATAR_ID_KEY)).thenReturn(true);

        avatarProvider = new JiraUserAvatarProvider(baseUrl, encodingConfiguration, gravatarSettings, avatarManager, userManager, userPropertyManager, userByEmail);

        String url = avatarProvider.getAvatar("fred@example.com", AuiSize.XSMALL.getSize()).getUrl();
        assertThat(url, equalTo("http://jira.atlassian.com/jira/secure/useravatar?size=xsmall"));
    }

    @Test
    public void getAvatarShouldReturnDefaultAvatarForUserWithNoConfiguredAvatar() throws Exception
    {
        final ApplicationUser knownUser = new MockApplicationUser("Fred", "Wilson", "fred@example.com");

        when(userManager.getUserByKey("fred@example.com")).thenReturn(knownUser);
        when(userManager.isUserExisting(knownUser)).thenReturn(true);
        when(avatarManager.getDefaultAvatarId(USER)).thenReturn(whateverAvatar.getId());
        when(userPropertyManager.getPropertySet(knownUser)).thenReturn(new MockPropertySet());
        when(avatarManager.getById(whateverAvatar.getId())).thenReturn(whateverAvatar);

        avatarProvider = new JiraUserAvatarProvider(baseUrl, encodingConfiguration, gravatarSettings, avatarManager, userManager, userPropertyManager, userByEmail);

        final PluginAvatar pluginAvatar = avatarProvider.getAvatar("fred@example.com", AuiSize.XSMALL.getSize());
        final byte[] retrievedBytes = IOUtils.toByteArray(pluginAvatar.getBytes());
        assertThat(whateverAvatarBytes, equalTo(retrievedBytes));
    }

    @Test
    public void getAvatarShouldReturnAnonymousAvatarForUnknownUser() throws Exception
    {
        when(avatarManager.getAnonymousAvatarId()).thenReturn(whateverAvatar.getId());
        when(avatarManager.getById(whateverAvatar.getId())).thenReturn(whateverAvatar);

        avatarProvider = new JiraUserAvatarProvider(baseUrl, encodingConfiguration, gravatarSettings, avatarManager, userManager, userPropertyManager, userByEmail);

        final PluginAvatar pluginAvatar = avatarProvider.getAvatar("unknown@example.com", AuiSize.XSMALL.getSize());
        final byte[] retrievedBytes = IOUtils.toByteArray(pluginAvatar.getBytes());
        assertThat(whateverAvatarBytes, equalTo(retrievedBytes));
    }

}
