package com.atlassian.jira.avatar.plugin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.annotation.Nonnull;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.util.IOUtil;

import org.apache.commons.io.IOUtils;

/**
 * Quick and dirty mock Avatar for the pluginified avatar era.
 * NOT THREAD SAFE.
 */
public class PluggyMockAvatar implements Avatar
{
    private String tempFileName;
    private byte[] bytes;
    private Long id;
    private String owner;
    private boolean isSystem;
    private String contentType;

    public PluggyMockAvatar(Long id, String owner, boolean isSystem, String contentType, byte[] bytes)
    {
        this.id = id;
        this.owner = owner;
        this.isSystem = isSystem;
        this.contentType = contentType;
        this.bytes = bytes;
    }

    @Nonnull
    @Override
    public Type getAvatarType()
    {
        return Type.USER;
    }

    /**
     * Name of a file capable of returning our bytes.
     */
    @Nonnull
    @Override
    public String getFileName()
    {
        if (tempFileName == null) {
            FileOutputStream fileOutputStream = null;
            try
            {
                final File tempFile = File.createTempFile(this.getClass().getName(), ".bin");
                tempFile.deleteOnExit();
                fileOutputStream = new FileOutputStream(tempFile);
                IOUtil.copy(this.bytes, fileOutputStream);
                tempFileName = tempFile.getCanonicalPath();
            }
            catch (IOException e)
            {
                throw new RuntimeException("Can't create temp file!?", e);
            }
            finally {
                IOUtils.closeQuietly(fileOutputStream);
            }
        }
        return tempFileName;
    }

    @Nonnull
    @Override
    public String getContentType()
    {
        return this.contentType;
    }

    @Override
    public Long getId()
    {
        return id;
    }

    @Nonnull
    @Override
    public String getOwner()
    {
        return owner;
    }

    @Override
    public boolean isSystemAvatar()
    {
        return isSystem;
    }
}
