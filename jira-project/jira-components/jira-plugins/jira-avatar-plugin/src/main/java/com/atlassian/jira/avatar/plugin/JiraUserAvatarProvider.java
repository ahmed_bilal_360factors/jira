package com.atlassian.jira.avatar.plugin;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarServiceImpl;
import com.atlassian.jira.avatar.GravatarSettings;
import com.atlassian.jira.avatar.pluggable.UserWithAvatar;
import com.atlassian.jira.cluster.ClusterSafe;
import com.atlassian.jira.config.util.EncodingConfiguration;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.BaseUrl;
import com.atlassian.jira.util.DefaultBaseUrl;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.plugins.avatar.AvatarOwner;
import com.atlassian.plugins.avatar.AvatarProvider;
import com.atlassian.plugins.avatar.PluginAvatar;
import com.atlassian.util.concurrent.LazyReference;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.opensymphony.module.propertyset.PropertySet;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.jira.avatar.Avatar.Size.approx;
import static com.google.common.base.Objects.firstNonNull;
import static com.google.common.base.Strings.emptyToNull;

/**
 * A provider for JIRA's internal User Avatar hosting and optional Gravatar support.
 */
public class JiraUserAvatarProvider implements AvatarProvider<ApplicationUser, Long>
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AvatarServiceImpl.class);

    private final BaseUrl baseUrl;
    private final Function<String, ApplicationUser> userByEmail;
    private EncodingConfiguration encodingConfiguration;
    private final GravatarSettings gravatarSettings;
    private final AvatarManager avatarManager;
    private final UserManager userManager;
    private final UserPropertyManager userPropertyManager;

    @ClusterSafe
    private final LazyReference<Avatar.Size> defaultAvatarSize = new LazyReference<Avatar.Size>()
    {
        @Override
        protected Avatar.Size create() throws Exception
        {
            return Avatar.Size.defaultSize();
        }
    };

    /**
     * Injectable constructor, to be deleted when {@link BaseUrl} becomes an API component.
     */
    public JiraUserAvatarProvider(final VelocityRequestContextFactory velocityRequestContextFactory,
            final GravatarSettings gravatarSettings,
            final AvatarManager avatarManager,
            final UserManager userManager,
            final UserPropertyManager userPropertyManager,
            final EncodingConfiguration encodingConfiguration)
    {
        this.baseUrl = new DefaultBaseUrl(velocityRequestContextFactory);
        this.gravatarSettings = gravatarSettings;
        this.avatarManager = avatarManager;
        this.userManager = userManager;
        this.userPropertyManager = userPropertyManager;
        this.encodingConfiguration = encodingConfiguration;
        this.userByEmail = getApplicationUserByEmail(userManager);
    }

    public JiraUserAvatarProvider(
            final BaseUrl baseUrl,
            final EncodingConfiguration encodingConfiguration,
            final GravatarSettings gravatarSettings,
            final AvatarManager avatarManager,
            final UserManager userManager,
            final UserPropertyManager userPropertyManager)
    {
        this.baseUrl = baseUrl;
        this.encodingConfiguration = encodingConfiguration;
        this.gravatarSettings = gravatarSettings;
        this.avatarManager = avatarManager;
        this.userManager = userManager;
        this.userPropertyManager = userPropertyManager;
        this.userByEmail = getApplicationUserByEmail(userManager);
    }

    /**
     * Nondestable convenience constructor to avoids static utils for user by email lookup.
     */
    JiraUserAvatarProvider(
            final BaseUrl baseUrl,
            final EncodingConfiguration encodingConfiguration,
            final GravatarSettings gravatarSettings,
            final AvatarManager avatarManager,
            final UserManager userManager,
            final UserPropertyManager userPropertyManager,
            final Function<String,ApplicationUser> userByEmail)
    {
        this.baseUrl = baseUrl;
        this.encodingConfiguration = encodingConfiguration;
        this.gravatarSettings = gravatarSettings;
        this.avatarManager = avatarManager;
        this.userManager = userManager;
        this.userPropertyManager = userPropertyManager;
        this.userByEmail = userByEmail;
    }

    /**
     * Gets the avatar for the user with the given userKey.
     */
    @Override
    public PluginAvatar getAvatar(final String email, final int size)
    {
        final ApplicationUser theUser = userByEmail.apply(email);
        if (theUser == null)
        {
            return new UnownedAvatar(getAnonymousAvatar(), size);
        }
        else
        {
            return getAvatar(new UserWithAvatar(theUser), size);
        }
    }

    private static Function<String, ApplicationUser> getApplicationUserByEmail(final UserManager uManager)
    {
        return new Function<String, ApplicationUser>()
        {
            @Override
            public ApplicationUser apply(@Nullable final String email)
            {
                String emailAddress = StringUtils.trimToNull(email);
                if (emailAddress == null)
                {
                    throw new IllegalArgumentException("email was empty");
                }

                User user = null;
                for (User u : uManager.getUsers())
                {
                    if (emailAddress.equalsIgnoreCase(u.getEmailAddress()))
                    {
                        user = u;
                        break;

                    }
                }
                return ApplicationUsers.from(user);
            }

        };
    }

    @Override
    public PluginAvatar getAvatar(final AvatarOwner<ApplicationUser> applicationUserAvatarOwner, final int size)
    {
        return new JiraUserPluginAvatar(applicationUserAvatarOwner, size);
    }

    @Override
    public PluginAvatar getAvatarById(final Long id, final int size)
    {
        return new UnownedAvatar(avatarManager.getById(id), size);
    }

    @Override
    public PluginAvatar getAvatar(final AvatarOwner<ApplicationUser> avatarOwner, final Function<AvatarOwner<ApplicationUser>, PluginAvatar> fallbackFunction, final int size)
    {
        return getAvatar(avatarOwner, size);
    }

    /**
     * Returns true if Gravatar support is enabled.
     *
     * @return a boolean indicating whether Gravatar support is on
     */
    public boolean isGravatarEnabled()
    {
        return gravatarSettings.isAllowGravatars();
    }

    public boolean isUsingExternalAvatar(final ApplicationUser avatarUser)
    {
        if (avatarUser == null)
        {
            return false;
        }
        final Long avatarId = configuredAvatarIdFor(avatarUser);
        return isGravatarEnabled() && (avatarId == null || avatarId.equals(getDefaultAvatar().getId()));
    }

    private URI getAvatarURLImpl(ApplicationUser remoteUser, boolean skipPermissionCheck, ApplicationUser avatarUser, Avatar.Size size)
    {
        return getAvatarURLImpl(remoteUser, skipPermissionCheck, avatarUser, size, true);
    }

    private URI getAvatarURLImpl(ApplicationUser remoteUser, boolean skipPermissionCheck, ApplicationUser avatarUser, Avatar.Size size, boolean buildAbsoluteURL)
    {
        final boolean useGravatars = isUsingExternalAvatar(avatarUser);

        UrlStrategy urlStrategy = useGravatars ? new GravatarUrlStrategy() : new JiraUrlStrategy(skipPermissionCheck, buildAbsoluteURL);

        return urlStrategy.get(remoteUser, avatarUser, size != null ? size : defaultAvatarSize.get());
    }

    /**
     * Whether to use Gravatar instead of an internal Avatar.
     *
     * @param avatar an Avatar
     * @return true if JIRA should use a Gravatar instead of the given internal Avatar.
     */
    private boolean useGravatarFor(final Avatar avatar)
    {
        Avatar defaultAvatar = getDefaultAvatar();

        return isGravatarEnabled() && (avatar == null || Objects.equal(avatar, defaultAvatar));
    }

    private URI getAvatarNoPermCheck(ApplicationUser avatarUser, Avatar avatar, Avatar.Size size)
    {
        if (useGravatarFor(avatar))
        {
            return new GravatarUrlStrategy().get(avatarUser, avatarUser, size);
        }

        return buildUriForAvatar(avatar, size, true);
    }

    /**
     * Builds a URI to JIRA's avatar servlet for a given JIRA avatar, with the requested size.
     *
     * @param avatar the Avatar whose URI we want
     * @param size the size in which the avatar should be displayed
     * @param absoluteUrl a boolean indicating whether to biuld an absolute URL
     * @return a URI that can be used to display the avatar
     */
    private URI buildUriForAvatar(Avatar avatar, @Nonnull Avatar.Size size, boolean absoluteUrl)
    {
        String base = absoluteUrl ? baseUrl.getCanonicalBaseUrl() : baseUrl.getBaseUrl();
        UrlBuilder builder = new UrlBuilder(base + "/secure/useravatar", encodingConfiguration.getEncoding(), false);

        if (size != Avatar.Size.defaultSize())
        {
            builder.addParameter("size", size.getParam());
        }

        String ownerId = avatar != null ? avatar.getOwner() : null;
        if (ownerId != null)
        {
            builder.addParameter("ownerId", ownerId);
        }

        // optional avatarId
        Long avatarId = avatar != null ? avatar.getId() : null;
        if (avatarId != null)
        {
            builder.addParameter("avatarId", avatarId.toString());
        }

        return builder.asURI();
    }

    Avatar getAvatarImpl(ApplicationUser remoteUser, boolean skipPermissionCheck, ApplicationUser user, boolean tagged)
    {
        if (userManager.isUserExisting(user))
        {
            // try to use the configured avatar
            Long customAvatarId = configuredAvatarIdFor(user);
            if (customAvatarId != null)
            {
                Avatar avatar = tagged ? avatarManager.getByIdTagged(customAvatarId) : avatarManager.getById(customAvatarId);
                if (avatar != null && (skipPermissionCheck || canViewAvatar(remoteUser, avatar)))
                {
                    return avatar;
                }
            }

            // fall back to the default user avatar
            Avatar defaultAvatar = getDefaultAvatar();
            LOGGER.debug("Avatar not configured for user '{}', using default id {}", user.getUsername(), defaultAvatar != null ? defaultAvatar.getId() : null);

            return defaultAvatar;
        }

        Avatar anonymousAvatar = getAnonymousAvatar();
        LOGGER.debug("User is null, using anonymous avatar id {}", anonymousAvatar != null ? anonymousAvatar.getId() : null);

        return anonymousAvatar;
    }

    /**
     * Returns the avatar id that is configured for the given User. If the user has not configured an avatar, this
     * method returns null.
     *
     * @param user the user whose avatar we want
     * @return an avatar id, or null
     * @see AvatarManager#getDefaultAvatarId(com.atlassian.jira.avatar.Avatar.Type)
     * @see com.atlassian.jira.avatar.AvatarManager#getAnonymousAvatarId()
     */
    protected Long configuredAvatarIdFor(ApplicationUser user)
    {
        try
        {
            PropertySet userProperties = userPropertyManager.getPropertySet(user);
            if (userProperties.exists(AvatarManager.USER_AVATAR_ID_KEY))
            {
                long avatarId = userProperties.getLong(AvatarManager.USER_AVATAR_ID_KEY);
                LOGGER.debug("Avatar configured for user '{}' is {}", user.getUsername(), avatarId);

                return avatarId;
            }
        } catch (RuntimeException ignored) {
            // happily returning null here
        }

        return null;
    }

    private boolean canViewAvatar(ApplicationUser user, Avatar avatar)
    {
        return avatarManager.hasPermissionToView(user != null ? user.getDirectoryUser() : null, avatar.getAvatarType(), avatar.getOwner());

    }

    /**
     * Returns the default avatar, if configured. Otherwise returns null.
     *
     * @return the default Avatar, or null
     */
    Avatar getDefaultAvatar()
    {
        Long defaultAvatarId = avatarManager.getDefaultAvatarId(Avatar.Type.USER);

        return defaultAvatarId != null ? avatarManager.getById(defaultAvatarId) : null;
    }

    /**
     * Returns the anonymous avatar, if configured. Otherwise returns null.
     *
     * @return the anonymous avatar, or null
     */
    Avatar getAnonymousAvatar()
    {
        Long anonAvatarId = avatarManager.getAnonymousAvatarId();

        return anonAvatarId != null ? avatarManager.getById(anonAvatarId) : null;
    }

    private class UnownedAvatar implements PluginAvatar
    {
        private Avatar jiraAvatar;
        private int size;

        private UnownedAvatar(Avatar jiraAvatar, final int size)
        {
            this.jiraAvatar = jiraAvatar;
            this.size = size;
        }

        @Override
        public String getOwnerId()
        {
            return null;
        }

        @Override
        public String getUrl()
        {
            // skip using superfluous UrlStrategy here since avatar by id is only supported by internal JIRA avatars
            return buildUriForAvatar(jiraAvatar, approx(size), false).toString();
        }

        @Override
        public int getSize()
        {
            return size;
        }

        @Override
        public String getContentType()
        {
            return jiraAvatar.getContentType();
        }

        @Override
        public boolean isExternal()
        {
            return false;
        }

        @Override
        public InputStream getBytes() throws IOException
        {
            try
            {
                return new FileInputStream(jiraAvatar.getFileName());
            }
            catch (FileNotFoundException e)
            {
                throw new RuntimeException("Could not find the file for this Avatar", e);
            }
        }

        @Override
        public PluginAvatar atSize(final int size)
        {
            return new UnownedAvatar(this.jiraAvatar, size);
        }
    }

    private class JiraUserPluginAvatar implements PluginAvatar
    {
        private final boolean useGravatars;
        private final int avatarSize;
        private final ApplicationUser user;
        private final Avatar jiraAvatar;
        private final String ownerId;

        public JiraUserPluginAvatar(final AvatarOwner<ApplicationUser> owner, final int size)
        {
            this.ownerId = owner.getIdentifier();
            user = owner.get();
            useGravatars = isUsingExternalAvatar(user);
            avatarSize = size;
            jiraAvatar = user == null ? getAnonymousAvatar() : getAvatarImpl(user, true, user, false);
        }

        /**
         * Copy constructor for size variant.
         */
        private JiraUserPluginAvatar(JiraUserPluginAvatar template, int size)
        {
            this.useGravatars = template.useGravatars;
            this.user = template.user;
            this.ownerId = template.ownerId;
            this.jiraAvatar = template.jiraAvatar;
            this.avatarSize = size;
        }

        @Override
        public String getUrl()
        {
            final UrlStrategy urlStrategy = useGravatars ? new GravatarUrlStrategy() : new JiraUrlStrategy(true, true);
            return urlStrategy.get(user, user, approx(avatarSize)).toString();
        }

        @Override
        public boolean isExternal()
        {
            return useGravatars;
        }

        @Override
        public InputStream getBytes()
        {
            if (isExternal())
            {
                // This is not allowed
                throw new IllegalStateException("Request to get bytes of external Avatar");
            }
            try
            {
                return new FileInputStream(jiraAvatar.getFileName());
            }
            catch (FileNotFoundException e)
            {
                throw new RuntimeException("Could not find the file for this Avatar", e);
            }
        }

        @Override
        public PluginAvatar atSize(final int size)
        {
            return new JiraUserPluginAvatar(this, size);
        }

        @Override
        public String getOwnerId()
        {
            return ownerId;
        }

        @Override
        public int getSize()
        {
            return avatarSize;
        }

        @Override
        public String getContentType()
        {
            return jiraAvatar.getContentType();
        }
    }

    /**
     * Interface for avatar URL building strategy.
     */
    private interface UrlStrategy
    {
        URI get(ApplicationUser remoteUser, ApplicationUser user, @Nonnull Avatar.Size size);
    }

    /**
     * Build avatar URLs that point to JIRA avatars.
     */
    private class JiraUrlStrategy implements UrlStrategy
    {
        private final boolean skipPermissionCheck;
        private final boolean buildAbsoluteURL;

        public JiraUrlStrategy(boolean skipPermissionCheck, boolean buildAbsoluteURL)
        {
            this.skipPermissionCheck = skipPermissionCheck;
            this.buildAbsoluteURL = buildAbsoluteURL;
        }

        @Override
        public URI get(ApplicationUser remoteUser, ApplicationUser user, @Nonnull Avatar.Size size)
        {
            Avatar avatar = getAvatarImpl(remoteUser, skipPermissionCheck, user, false);

            return buildUriForAvatar(avatar, size, buildAbsoluteURL);
        }
    }

    /**
     * Build avatar URLs that point to Gravatar avatars. If the user does not exist, we serve the anonymous avatar
     * directly. If the user has not configured Gravatar, they will get the default avatar provided by Gravatar.
     */
    private class GravatarUrlStrategy implements UrlStrategy
    {
        private static final String HTTP_API = "http://www.gravatar.com/avatar/";
        private static final String HTTPS_API = "https://secure.gravatar.com/avatar/";

        @Override
        public URI get(ApplicationUser remoteUser, ApplicationUser user, @Nonnull Avatar.Size size)
        {
            // JRADEV-12195: email should not be null, but we have seen it in the wild (EAC/J connected to Crowd).
            if (user != null && user.getEmailAddress() != null)
            {
                // JRA-28913: Must use lower-case to get the correct hash
                final String hash = MD5Util.md5Hex(user.getEmailAddress().toLowerCase());
                final String apiAddress = firstNonNull(emptyToNull(gravatarSettings.getCustomApiAddress()), useSSL() ? HTTPS_API : HTTP_API);

                // JRA-29934: since September 2012, gravatar.com no longer handles the d= query parameter like it used
                // to, which breaks default avatars for non-publicly accessible JIRA instances. so we just use the
                // "mystery man" avatar provided by gravatar.com.
                return new UrlBuilder(apiAddress, encodingConfiguration.getEncoding(), false)
                        .addPath(hash)
                        .addParameter("d", "mm")
                        .addParameter("s", Integer.toString(size.getPixels()))
                        .asURI();
            }

            return buildUriForAvatar(getAnonymousAvatar(), size, true);
        }

        /**
         * @return whether we should use the Gravatar SSL servers or not
         */
        private boolean useSSL()
        {
            String baseURL = baseUrl.getCanonicalBaseUrl();
            try
            {
                return "https".equalsIgnoreCase(URI.create(baseURL).getScheme());
            }
            catch (Exception e)
            {
                // base URL is messed up, nothing we can do about it here...
                return false;
            }
        }
    }
}
