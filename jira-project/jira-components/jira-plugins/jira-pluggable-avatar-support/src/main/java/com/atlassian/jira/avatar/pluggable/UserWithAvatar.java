package com.atlassian.jira.avatar.pluggable;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugins.avatar.AvatarOwner;

/**
 * Implementation of the AvatarOwner domain object wrapper to enable plugin retrieval of avatars for JIRA users with
 * {@link com.atlassian.plugins.avatar.AvatarProvider#getAvatar(com.atlassian.plugins.avatar.AvatarOwner, int)}.
 */
public final class UserWithAvatar implements AvatarOwner<ApplicationUser>
{
    final ApplicationUser user;

    /**
     * Nominal user as an AvatarOwner. Supports the anonymous user.
     * @param user possibly null (anonymous user).
     */
    public UserWithAvatar(final ApplicationUser user)
    {
        this.user = user;
    }

    @Override
    public final String getIdentifier()
    {
        return user == null ? null : user.getEmailAddress();
    }

    @Override
    public final boolean useUnknownAvatar()
    {
        return user == null;
    }

    @Override
    public final ApplicationUser get()
    {
        return user;
    }
}
