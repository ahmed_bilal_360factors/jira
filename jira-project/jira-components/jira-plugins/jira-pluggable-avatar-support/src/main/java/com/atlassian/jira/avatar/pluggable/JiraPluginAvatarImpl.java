package com.atlassian.jira.avatar.pluggable;

import java.io.IOException;
import java.io.InputStream;

import com.atlassian.jira.avatar.JiraPluginAvatar;
import com.atlassian.plugins.avatar.PluginAvatar;

/**
 * Implements JIRA's {@link com.atlassian.jira.avatar.JiraPluginAvatar} using the externally defined
 * {@link com.atlassian.plugins.avatar.PluginAvatar}.
 */
class JiraPluginAvatarImpl implements JiraPluginAvatar
{
    private PluginAvatar pluginAvatar;

    public JiraPluginAvatarImpl(PluginAvatar pluginAvatar)
    {
        this.pluginAvatar = pluginAvatar;
    }

    @Override
    public String getOwnerId()
    {
        return pluginAvatar.getOwnerId();
    }

    @Override
    public String getUrl()
    {
        return pluginAvatar.getUrl();
    }

    @Override
    public int getSize()
    {
        return pluginAvatar.getSize();
    }

    @Override
    public String getContentType()
    {
        return pluginAvatar.getContentType();
    }

    @Override
    public boolean isExternal()
    {
        return pluginAvatar.isExternal();
    }

    @Override
    public InputStream getBytes() throws IOException
    {
        return pluginAvatar.getBytes();
    }
}
