package com.atlassian.jira.avatar.pluggable;

import java.util.Collections;
import java.util.List;

import com.atlassian.jira.avatar.JiraAvatarSupport;
import com.atlassian.jira.avatar.JiraPluginAvatar;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.avatar.AvatarProvider;
import com.atlassian.plugins.avatar.AvatarProviderModuleDescriptor;
import com.atlassian.plugins.avatar.PluginAvatar;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSortedSet;

import static com.atlassian.plugins.avatar.AuiSize.toPixels;
import static com.google.common.collect.ImmutableSortedSet.copyOf;
import static com.google.common.collect.Iterables.filter;

/**
 * Implmentation of JIRA core {@link com.atlassian.jira.avatar.JiraAvatarSupport} using the highest weighted, enabled
 * AvatarProvider plugin module available.
 */
public class AvatarProviderAdapter implements JiraAvatarSupport
{
    private static final Predicate<AvatarProviderModuleDescriptor> SHOULD_DISPLAY = new Predicate<AvatarProviderModuleDescriptor>()
    {
        @Override
        public boolean apply(AvatarProviderModuleDescriptor descriptor)
        {
            return descriptor.getCondition() == null || descriptor.getCondition().shouldDisplay(Collections.<String, Object>emptyMap());
        }
    };

    private PluginAccessor pluginAccessor;

    public AvatarProviderAdapter(final PluginAccessor pluginAccessor)
    {
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public JiraPluginAvatar getAvatar(final String email, final String size)
    {
        final PluginAvatar avatar = provider().getAvatar(email, toPixels(size));
        return new JiraPluginAvatarImpl(avatar);
    }

    @Override
    public JiraPluginAvatar getAvatar(final ApplicationUser user, final String size)
    {
        return new JiraPluginAvatarImpl(provider().getAvatar(new UserWithAvatar(user), toPixels(size)));
    }

    @Override
    public JiraPluginAvatar getAvatarById(final Long avatarId, final String size)
    {
        return new JiraPluginAvatarImpl(provider().getAvatarById(avatarId, toPixels(size)));
    }

    private AvatarProvider<ApplicationUser, Long> provider()
    {
        final List<AvatarProviderModuleDescriptor> enabled = pluginAccessor.getEnabledModuleDescriptorsByClass(AvatarProviderModuleDescriptor.class);
        final ImmutableSortedSet<AvatarProviderModuleDescriptor> mdSet = copyOf(filter(enabled, SHOULD_DISPLAY));

        if (mdSet.isEmpty())
        {
            throw new IllegalStateException("No AvatarProvider module found");
        }
        //noinspection unchecked
        return mdSet.first().getModule();
    }
}
