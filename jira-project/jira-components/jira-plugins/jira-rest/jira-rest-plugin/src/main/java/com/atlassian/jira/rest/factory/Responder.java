package com.atlassian.jira.rest.factory;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.jira.rest.api.util.ErrorCollection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static com.atlassian.jira.util.ErrorCollection.Reason.SERVER_ERROR;

/**
 * Provides ways standard ways of creating a {@link javax.ws.rs.core.Response}.
 *
 * @since 6.4
 */
@Component
public class Responder
{
    private final ErrorCollectionFactory errorCollectionFactory;

    @Autowired
    Responder(final ErrorCollectionFactory errorCollectionFactory)
    {
        this.errorCollectionFactory = errorCollectionFactory;
    }

    /**
     * Converts an {@link com.atlassian.jira.util.ErrorCollection} to a {@link javax.ws.rs.core.Response}.
     *
     * @param errors Errors to be converted
     * @return Response with the worst reason as the status and the errors as the entity
     */
    public Response errors(final com.atlassian.jira.util.ErrorCollection errors)
    {
        final ErrorCollection restfulErrors = errorCollectionFactory.of(errors);
        return restfulErrors(restfulErrors);
    }

    /**
     * Converts an {@link com.atlassian.jira.rest.api.util.ErrorCollection} to a {@link javax.ws.rs.core.Response}.
     *
     * @param errors Errors to be converted
     * @return Response with the worst reason as the status and the errors as the entity
     */
    public Response restfulErrors(final ErrorCollection errors)
    {
        final int status = errors.getStatus();
        return Response
                .status(status)
                .entity(errors)
                .cacheControl(never())
                .build();
    }

    /**
     * Standard response for a successful DELETE request.
     * @return Standard response.
     */
    public Response successfulDelete()
    {
        return builder(Status.NO_CONTENT).build();
    }

    /**
     * Starts a response builder for a given status.
     *
     * @param status Initial status
     * @return Started builder
     */
    public Response.ResponseBuilder builder(final Status status)
    {
        return Response.status(status);
    }

    /**
     * Creates a response about an exception.
     *
     * @param exception Exception included in the response
     * @return Response with the exception
     */
    public Response exception(final Exception exception)
    {
        final ErrorCollection errors = errorCollectionFactory.of(SERVER_ERROR, exception.getMessage());
        return restfulErrors(errors);
    }
}
