package com.atlassian.jira.rest.v2.issue.attachment.format;

import java.net.URI;

import javax.annotation.concurrent.Immutable;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;

import org.codehaus.jackson.annotate.JsonAutoDetect;

/**
 * Human-readable representation of an archive.
 *
 * @since v6.4
 */
@JsonAutoDetect
@Immutable
public class HumanReadableArchive
{

    public static final HumanReadableArchive DOC_EXAMPLE = new HumanReadableArchiveBuilder()
            .id(7237823)
            .mediaType("application/zip")
            .name("images.zip")
            .totalEntryCount(39)
            .entries(
                    ImmutableList.of(
                            new HumanReadableArchiveEntryBuilder()
                                    .path("MG00N067.JPG")
                                    .index(0)
                                    .size("119 kB")
                                    .mediaType("image/jpeg")
                                    .icon(URI.create("image.gif"))
                                    .alternativeText("JPEG file")
                                    .label("MG00N067.JPG")
                                    .build(),
                            new HumanReadableArchiveEntryBuilder()
                                    .path("Allegro from Duet in C Major.mp3")
                                    .index(1)
                                    .size("1.36 MB")
                                    .mediaType("audio/mpeg")
                                    .icon(URI.create("file.gif"))
                                    .alternativeText("File")
                                    .label("Allegro from Duet in C Major.mp3")
                                    .build(),
                            new HumanReadableArchiveEntryBuilder()
                                    .path(
                                            "long/path/thanks/to/lots/of/subdirectories/inside/making/it/quite/hard"
                                                    + "/to/reach/the/leaf.txt"
                                    )
                                    .index(2)
                                    .size("0.0 k")
                                    .mediaType("text/plain")
                                    .icon(URI.create("text.gif"))
                                    .alternativeText("Text File")
                                    .label("long/path/thanks/to/.../reach/the/leaf.txt")
                                    .build()
                    )
            )
            .build();

    private final long id;
    private final String name;
    private final Iterable<HumanReadableArchiveEntry> entries;
    private final long totalEntryCount;
    private final String mediaType;

    public HumanReadableArchive(
            final long id,
            final String name,
            final Iterable<HumanReadableArchiveEntry> entries,
            final long totalEntryCount,
            final String mediaType
    )
    {
        this.id = id;
        this.name = name;
        this.entries = ImmutableList.copyOf(entries);
        this.totalEntryCount = totalEntryCount;
        this.mediaType = mediaType;
    }

    public long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public Iterable<HumanReadableArchiveEntry> getEntries()
    {
        return entries;
    }

    public long getTotalEntryCount()
    {
        return totalEntryCount;
    }

    public String getMediaType()
    {
        return mediaType;
    }

    @Override
    public int hashCode() {return Objects.hashCode(id, name, entries, totalEntryCount, mediaType);}

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()) {return false;}
        final HumanReadableArchive other = (HumanReadableArchive) obj;
        return Objects.equal(this.id, other.id)
                && Objects.equal(this.name, other.name)
                && Objects.equal(this.entries, other.entries)
                && Objects.equal(this.totalEntryCount, other.totalEntryCount)
                && Objects.equal(this.mediaType, other.mediaType);
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("entries", entries)
                .add("totalEntryCount", totalEntryCount)
                .add("mediaType", mediaType)
                .toString();
    }
}
