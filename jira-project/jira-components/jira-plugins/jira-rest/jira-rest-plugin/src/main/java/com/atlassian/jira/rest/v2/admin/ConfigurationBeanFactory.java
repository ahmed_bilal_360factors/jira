package com.atlassian.jira.rest.v2.admin;

import com.atlassian.core.util.DateUtils;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.issue.vote.VoteService;
import com.atlassian.jira.bc.issue.watcher.WatcherService;
import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.link.IssueLinkManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.atlassian.jira.rest.v2.admin.TimeTrackingConfigurationBean.TimeTrackingUnit;
import static com.atlassian.jira.rest.v2.admin.TimeTrackingConfigurationBean.TimeTrackingUnit.day;
import static com.atlassian.jira.rest.v2.admin.TimeTrackingConfigurationBean.TimeTrackingUnit.hour;
import static com.atlassian.jira.rest.v2.admin.TimeTrackingConfigurationBean.TimeTrackingUnit.minute;
import static com.atlassian.jira.rest.v2.admin.TimeTrackingConfigurationBean.TimeTrackingUnit.week;

@Component
public class ConfigurationBeanFactory
{
    private final VoteService voteService;
    private final WatcherService watcherService;
    private final ApplicationProperties applicationProperties;
    private final SubTaskManager subTaskManager;
    private final AttachmentManager attachmentManager;
    private final IssueLinkManager issueLinkManager;
    private final TimeTrackingConfiguration timeTrackingConfiguration;

    @Autowired
    public ConfigurationBeanFactory(final VoteService voteService,
            final WatcherService watcherService,
            final ApplicationProperties applicationProperties,
            final SubTaskManager subTaskManager,
            final AttachmentManager attachmentManager,
            final IssueLinkManager issueLinkManager,
            final TimeTrackingConfiguration timeTrackingConfiguration)
    {
        this.voteService = voteService;
        this.watcherService = watcherService;
        this.applicationProperties = applicationProperties;
        this.subTaskManager = subTaskManager;
        this.attachmentManager = attachmentManager;
        this.issueLinkManager = issueLinkManager;
        this.timeTrackingConfiguration = timeTrackingConfiguration;
    }

    public ConfigurationBean createConfigurationBean()
    {
        final boolean timeTrackingEnabled = timeTrackingConfiguration.enabled();

        if (timeTrackingEnabled)
        {
            return createConfigurationBean(true, Option.some(createTimeTrackingConfiguration()));
        }
        else
        {
            return createConfigurationBean(false, Option.none(TimeTrackingConfigurationBean.class));
        }
    }

    private ConfigurationBean createConfigurationBean(boolean timeTrackingEnabled, Option<TimeTrackingConfigurationBean> timeTrackingConfigurationBean)
    {
        return ConfigurationBean.builer()
                .votingEnabled(voteService.isVotingEnabled())
                .watchingEnabled(watcherService.isWatchingEnabled())
                .unassignedIssuesAllowed(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED))
                .subTasksEnabled(subTaskManager.isSubTasksEnabled())
                .attachmentsEnabled(attachmentManager.attachmentsEnabled())
                .issueLinkingEnabled(issueLinkManager.isLinkingEnabled())
                .timeTrackingEnabled(timeTrackingEnabled)
                .timeTrackingConfiguration(timeTrackingConfigurationBean.getOrNull())
                .build();
    }

    private TimeTrackingConfigurationBean createTimeTrackingConfiguration()
    {
        final double hoursPerDay = timeTrackingConfiguration.getHoursPerDay().doubleValue();
        final double daysPerWeek = timeTrackingConfiguration.getDaysPerWeek().doubleValue();

        final TimeTrackingConfiguration.TimeFormat timeFormat = timeTrackingConfiguration.getTimeFormat();
        final TimeTrackingUnit timeTrackingUnit = getTimeTrackingUnit();

        return new TimeTrackingConfigurationBean(hoursPerDay, daysPerWeek, timeFormat, timeTrackingUnit);
    }

    private TimeTrackingUnit getTimeTrackingUnit()
    {
        final DateUtils.Duration unit = timeTrackingConfiguration.getDefaultUnit();
        switch (unit)
        {
            case HOUR:
                return hour;
            case DAY:
                return day;
            case WEEK:
                return week;
            case MINUTE:
            default:
                return minute;
        }
    }

}
