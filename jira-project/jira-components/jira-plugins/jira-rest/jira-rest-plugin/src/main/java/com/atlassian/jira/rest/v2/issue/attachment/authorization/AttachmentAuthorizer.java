package com.atlassian.jira.rest.v2.issue.attachment.authorization;

import javax.annotation.Nullable;

import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

/**
 * Decides if a user is authorized in context of an attachment.
 *
 * @since v6.4
 */
public interface AttachmentAuthorizer
{
    /**
     * @param attachment Attachment, which is the context of the authorization.
     * @param user Checked user. Null means an anonymous user.
     * @return True iff user is authorized.
     */
    boolean authorize(Attachment attachment, @Nullable ApplicationUser user);

    /**
     * @param i18n I18N source
     * @param attachmentId attachment id, to which permission is not granted
     * @return I18N'd message
     */
    String getNoPermissionMessage(I18nHelper i18n, Object attachmentId);
}
