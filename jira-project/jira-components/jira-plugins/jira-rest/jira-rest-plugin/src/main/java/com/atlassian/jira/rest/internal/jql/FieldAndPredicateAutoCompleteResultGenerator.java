package com.atlassian.jira.rest.internal.jql;

import java.util.Collection;
import java.util.List;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.jql.ClauseHandler;
import com.atlassian.jira.jql.ValueGeneratingClauseHandler;
import com.atlassian.jira.jql.operand.registry.PredicateRegistry;
import com.atlassian.jira.jql.util.JqlStringSupport;
import com.atlassian.jira.jql.values.ClauseValuesGenerator;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.DelimeterInserter;

import com.google.common.collect.ImmutableList;
import com.opensymphony.util.TextUtils;

import org.springframework.stereotype.Component;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Generates JQL autocomplete results basing on either field or predicate.
 */
public class FieldAndPredicateAutoCompleteResultGenerator
{
    public static final int MAX_RESULTS = 15;

    private final JiraAuthenticationContext authenticationContext;
    private final JqlStringSupport jqlStringSupport;
    private final SearchHandlerManager searchHandlerManager;
    private final PredicateRegistry predicateRegistry;

    public FieldAndPredicateAutoCompleteResultGenerator(final JiraAuthenticationContext authenticationContext,
            final JqlStringSupport jqlStringSupport,
            final SearchHandlerManager searchHandlerManager,
            final PredicateRegistry predicateRegistry)
    {
        this.authenticationContext = authenticationContext;
        this.jqlStringSupport = jqlStringSupport;
        this.searchHandlerManager = searchHandlerManager;
        this.predicateRegistry = predicateRegistry;
    }

    public Iterable<Result> getAutoCompleteResultsForField(final String fieldName, final String fieldValue)
    {
        checkNotNull(fieldName);
        checkNotNull(fieldValue);

        final User searcher = getSearcher();
        final Collection<ClauseHandler> clauseHandlers = searchHandlerManager.getClauseHandler(searcher, fieldName);
        if (clauseHandlers.size() == 1)
        {
            ClauseHandler clauseHandler = clauseHandlers.iterator().next();

            if (clauseHandler instanceof ValueGeneratingClauseHandler)
            {
                final ClauseValuesGenerator clauseValuesGenerator =  ((ValueGeneratingClauseHandler) (clauseHandler))
                        .getClauseValuesGenerator();
                return generateResults(clauseValuesGenerator, searcher, fieldName, fieldValue);
            }
        }
        return ImmutableList.of();
    }

    public Iterable<Result> getAutoCompleteResultsForPredicate(final String predicateName, final String predicateValue, final String fieldName)
    {
        checkNotNull(fieldName);
        checkNotNull(predicateName);
        checkNotNull(predicateValue);

        final User searcher = getSearcher();

        final ClauseValuesGenerator clauseValuesGenerator = predicateRegistry.getClauseValuesGenerator(predicateName, fieldName);
        if (clauseValuesGenerator != null)
        {
            return generateResults(clauseValuesGenerator, searcher, predicateName, predicateValue);
        }
        return ImmutableList.of();
    }

    private User getSearcher()
    {
        return authenticationContext.getLoggedInUser();
    }

    private Iterable<Result> generateResults(ClauseValuesGenerator clauseValuesGenerator, User searcher, String fieldName, String fieldValue)
    {
        DelimeterInserter delimeterInserter = new DelimeterInserter("<b>", "</b>");
        // Lets assume that anything that has one of the following one char before it, is a new word.
        delimeterInserter.setConsideredWhitespace("-_/\\,.+=&^%$#*@!~`'\":;<>(");

        final ClauseValuesGenerator.Results generatorResults = clauseValuesGenerator.getPossibleValues(searcher, fieldName, fieldValue, MAX_RESULTS);

        final List<ClauseValuesGenerator.Result> list = generatorResults.getResults();
        final ImmutableList.Builder<Result> results = ImmutableList.builder();

        for (ClauseValuesGenerator.Result result : list)
        {
            StringBuilder displayName = new StringBuilder();
            for (int i = 0; i < result.getDisplayNameParts().length; i++)
            {
                if (i != 0)
                {
                    displayName.append(" ");
                }
                String displayNamePart = result.getDisplayNameParts()[i];
                // Need to encode both BEFORE we let the delimeterInserter work so that we will correctly match the strings, even if they have
                // HTML escape characters in them.
                displayName.append(delimeterInserter.insert(TextUtils.htmlEncode(displayNamePart), new String[]{TextUtils.htmlEncode(fieldValue)}));
            }
            final String encodedValue = jqlStringSupport.encodeValue(result.getValue());
            // JRA-19142 - quote zero padded numbers for autocomplete suggestions
            results.add(new Result(quoteZeroPaddedNumbers(encodedValue), displayName.toString()));
        }

        return results.build();
    }

    private String quoteZeroPaddedNumbers(final String value)
    {
        if (value == null)
        {
            return value;
        }
        try
        {
            final long longVal = Long.parseLong(value);
            // Great, its a number, see if it starts with a 0
            if (value.startsWith("0"))
            {
                return "\"" + value + "\"";
            }
            else
            {
                return value;
            }
        }
        catch (NumberFormatException nfe)
        {
            return value;
        }
    }

    public final static class Result
    {
        private final String value;
        private final String displayName;

        public Result(final String value, final String displayName)
        {
            this.value = value;
            this.displayName = displayName;
        }

        public String getValue()
        {
            return value;
        }

        public String getDisplayName()
        {
            return displayName;
        }
    }

}
