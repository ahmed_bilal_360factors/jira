package com.atlassian.jira.rest.v2.issue;

import java.util.List;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.fugue.Option;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.config.IssueTypeService;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueTypeCreateBean;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueTypeJsonBean;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueTypeUpdateBean;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.rest.v2.issue.context.ContextUriInfo;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.xsrf.XsrfCheckResult;
import com.atlassian.jira.security.xsrf.XsrfInvocationChecker;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.plugins.rest.common.multipart.FilePart;
import com.atlassian.plugins.rest.common.multipart.MultipartFormParam;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;

import static com.atlassian.jira.config.IssueTypeService.CreateValidationResult;
import static com.atlassian.jira.config.IssueTypeService.DeleteValidationResult;
import static com.atlassian.jira.config.IssueTypeService.IssueTypeCreateInput;
import static com.atlassian.jira.config.IssueTypeService.IssueTypeDeleteInput;
import static com.atlassian.jira.config.IssueTypeService.IssueTypeUpdateInput;
import static com.atlassian.jira.config.IssueTypeService.UpdateValidationResult;
import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static com.atlassian.jira.util.ErrorCollection.Reason;
import static com.google.common.collect.Iterables.transform;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.status;

/**
 * @since 4.2
 */
@Path ("issuetype")
@AnonymousAllowed
@Consumes ({ MediaType.APPLICATION_JSON })
@Produces ({ MediaType.APPLICATION_JSON })
public class IssueTypeResource
{
    private final JiraAuthenticationContext authContext;
    private final ContextUriInfo contextUriInfo;
    private final JiraBaseUrls jiraBaseUrls;
    private final IssueTypeService issueTypeService;
    private final I18nHelper i18n;
    private final XsrfInvocationChecker xsrfChecker;
    private final AvatarResourceHelper avatarResourceHelper;
    private final GlobalPermissionManager globalPermissionManager;

    public IssueTypeResource(JiraAuthenticationContext authContext,
            ContextUriInfo contextUriInfo,
            JiraBaseUrls jiraBaseUrls,
            IssueTypeService issueTypeService,
            I18nHelper i18n,
            XsrfInvocationChecker xsrfChecker,
            AvatarResourceHelper avatarResourceHelper,
            GlobalPermissionManager globalPermissionManager)
    {
        this.authContext = authContext;
        this.contextUriInfo = contextUriInfo;
        this.jiraBaseUrls = jiraBaseUrls;
        this.issueTypeService = issueTypeService;
        this.i18n = i18n;
        this.xsrfChecker = xsrfChecker;
        this.avatarResourceHelper = avatarResourceHelper;
        this.globalPermissionManager = globalPermissionManager;
    }

    /**
     * Returns a list of all issue types visible to the user
     *
     * @return a list of issue types
     *
     * @response.representation.200.mediaType
     *      application/json
     *
     * @response.representation.200.doc
     *      Returned if the issue type exists and is visible by the calling user.
     *
     * @response.representation.200.example
     *      {@link IssueTypeBeanExample#ISSUE_TYPES_EXAMPLE}
     */
    @GET
    public Response getIssueAllTypes()
    {
        return ok(transformIssueTypeToBeans(issueTypeService.getIssueTypes(authContext.getUser())))
                .cacheControl(never())
                .build();
    }

    /**
     * Returns a full representation of the issue type that has the given id.
     *
     * @param issueTypeId a String containing an issue type id
     * @return a full representation of the issue type with the given id
     *
     * @response.representation.200.qname
     *      issueType
     *
     * @response.representation.200.mediaType
     *      application/json
     *
     * @response.representation.200.doc
     *      Returned if the issue type exists and is visible by the calling user.
     * @response.representation.200.example
     *      {@link IssueTypeBeanExample#DOC_EXAMPLE}
     *
     * @response.representation.404.doc
     *      Returned if the issue type does not exist, or is not visible to the calling user.
     */
    @GET
    @Path ("{id}")
    public Response getIssueType(@PathParam ("id") final String issueTypeId)
    {
        return issueTypeService.getIssueType(authContext.getUser(), issueTypeId).fold(new Supplier<Response>()
        {
            @Override
            public Response get()
            {
                return notFoundResponse();
            }
        }, new Function<IssueType, Response>()
        {
            @Override
            public Response apply(final IssueType issueType)
            {
                return ok(createIssueTypeBean(issueType))
                        .cacheControl(never())
                        .build();
            }
        });
    }

    /**
     * Returns a list of all alternative issue types for the given issue type id. The list will contain these issues types, to which
     * issues assigned to the given issue type can be migrated. The suitable alternatives are issue types which are assigned
     * to the same workflow, the same field configuration and the same screen scheme.
     *
     * @return a list of issue types
     *
     * @response.representation.200.mediaType
     *      application/json
     * @response.representation.200.doc
     *      Returned if the issue type exists and is visible by the calling user.
     * @response.representation.404.doc
     *      Returned if the issue type does not exist, or is not visible to the calling user.
     * @response.representation.200.example
     *      {@link IssueTypeBeanExample#ISSUE_TYPES_EXAMPLE}
     */
    @GET
    @Path("{id}/alternatives")
    public Response getAlternativeIssueTypes(@PathParam("id") final String issueTypeId)
    {
        final ApplicationUser user = authContext.getUser();
        final Option<IssueType> issueType = issueTypeService.getIssueType(user, issueTypeId);
        return issueType.fold(new Supplier<Response>()
        {
            @Override
            public Response get()
            {
                return notFoundResponse();
            }
        }, new Function<IssueType, Response>()
        {
            @Override
            public Response apply(@Nullable final IssueType issueType)
            {
                return ok(transformIssueTypeToBeans(issueTypeService.getAvailableAlternativesForIssueType(authContext.getUser(), issueTypeId)))
                        .cacheControl(never())
                        .build();
            }
        });
    }

    /**
     * Creates an issue type from a JSON representation and adds the issue newly created issue type to the default issue
     * type scheme.
     *
     * @request.representation.example
     *      {@link com.atlassian.jira.issue.fields.rest.json.beans.IssueTypeCreateBean#CREATE_EXAMPLE}
     *
     * @response.representation.201.qname
     *      issueType
     *
     * @response.representation.201.mediaType
     *      application/json
     *
     * @response.representation.201.doc
     *      Returned if the issue type was successfully created.
     * @response.representation.400.doc
     *      Returned if the request is invalid. This happens when the name is invalid or issue type is subtask on instance
     *      which has subtasks disabled.
     * @response.representation.401.doc
     *      Returned if the calling user is not authenticated.
     * @response.representation.403.doc
     *      Returned if the calling user does not have permission to administer JIRA.
     * @response.representation.409.doc
     *      Returned if there already exists an issue type with the specified name.
     *
     * @param issueTypeCreateBean
     * @since 6.4
     * @return
     */
    @POST
    public Response createIssueType(final IssueTypeCreateBean issueTypeCreateBean)
    {
        ApplicationUser user = authContext.getUser();

        CreateValidationResult validationResult = issueTypeService.validateCreateIssueType(user,
                issueTypeCreateInputFromBean(issueTypeCreateBean));

        if (validationResult.isValid())
        {
            IssueTypeService.IssueTypeResult issueTypeResult = issueTypeService.createIssueType(user, validationResult);

            return status(Response.Status.CREATED)
                    .entity(createIssueTypeBean(issueTypeResult.getIssueType()))
                    .cacheControl(never())
                    .build();
        }
        else
        {
            return error(validationResult.getErrorCollection());
        }
    }

    /**
     * Deletes the specified issue type. If the issue type has any associated issues, these issues will be migrated to
     * the alternative issue type specified in the parameter. You can determine the alternative issue types by calling
     * the <b>/rest/api/2/issuetype/{id}/alternatives</b> resource.
     *
     * @response.representation.204.mediaType
     *      application/json
     *
     * @response.representation.204.doc
     *      Returned if the issue type was successfully removed.
     * @response.representation.400.doc
     *      Returned if the request is invalid. It happens when there are associated issues with the issue type which is being removed,
     *      but it is impossible to migrate these issues to the alternative issue type.
     * @response.representation.401.doc
     *      Returned if the calling user is not authenticated.
     * @response.representation.403.doc
     *      Returned if the calling user does not have permission to administer JIRA.
     * @response.representation.404.doc
     *      Returned if the issue type which is supposed to be removed does not exist or the alternative issue type does not exist.
     *
     * @param issueTypeId the id of the issue type to remove.
     * @param alternativeIssueTypeId the id of an issue type to which issues associated with the removed issue type will be migrated.
     * @since 6.4
     */
    @DELETE
    @Path("{id}")
    public Response deleteIssueType(@PathParam ("id") final String issueTypeId,
            @QueryParam("alternativeIssueTypeId") final String alternativeIssueTypeId)
    {
        final ApplicationUser user = authContext.getUser();
        final IssueTypeDeleteInput issueTypeDeleteInput = new IssueTypeDeleteInput(issueTypeId, Option.option(alternativeIssueTypeId));

        final DeleteValidationResult validationResult = issueTypeService.validateDeleteIssueType(user, issueTypeDeleteInput);

        if (validationResult.isValid())
        {
            issueTypeService.deleteIssueType(user, validationResult);
            return status(Response.Status.NO_CONTENT)
                    .cacheControl(never())
                    .build();
        }
        else
        {
            return error(validationResult.getErrorCollection());
        }
    }

    /**
     * Updates the specified issue type from a JSON representation.
     *
     * @request.representation.example
     *      {@link com.atlassian.jira.issue.fields.rest.json.beans.IssueTypeUpdateBean#UPDATE_EXAMPLE}
     *
     * @response.representation.200.qname
     *      issueType
     *
     * @response.representation.200.mediaType
     *      application/json
     *
     * @response.representation.200.doc
     *      Returned if the issue type was successfully updated.
     * @response.representation.400.doc
     *      Returned if the request is invalid. This happens when the name is invalid or if the avatar with given id
     *      does not exist.
     * @response.representation.401.doc
     *      Returned if the calling user is not authenticated.
     * @response.representation.403.doc
     *      Returned if the calling user does not have permission to administer JIRA.
     * @response.representation.404.doc
     *      Returned if the issue type to update does not exist.
     * @response.representation.409.doc
     *      Returned if there already exists an issue type with the specified name.
     *
     * @param issueTypeId the id of the issue type to update.
     * @param issueTypeUpdateBean
     * @since 6.4
     * @return
     */
    @PUT
    @Path("{id}")
    public Response updateIssueType(@PathParam("id") final String issueTypeId,
            final IssueTypeUpdateBean issueTypeUpdateBean)
    {
        final ApplicationUser user = authContext.getUser();
        final IssueTypeUpdateInput updateInput = issueTypeUpdateInputFromBean(issueTypeUpdateBean);

        final UpdateValidationResult updateValidationResult = issueTypeService.validateUpdateIssueType(user, issueTypeId, updateInput);
        if (updateValidationResult.isValid())
        {
            final IssueTypeService.IssueTypeResult issueTypeResult = issueTypeService.updateIssueType(user, updateValidationResult);
            return ok(createIssueTypeBean(issueTypeResult.getIssueType()))
                    .cacheControl(never())
                    .build();
        }
        else
        {
            return error(updateValidationResult.getErrorCollection());
        }
    }

    /**
     * Creates temporary avatar. Creating a temporary avatar is part of a 3-step process in uploading a new
     * avatar for an issue type: upload, crop, confirm.
     *
     * <p>
     *     The following examples shows these three steps using curl.
     *     The cookies (session) need to be preserved between requests, hence the use of -b and -c.
     *     The id created in step 2 needs to be passed to step 3
     *     (you can simply pass the whole response of step 2 as the request of step 3).
     * </p>
     *
     * <pre>
     * curl -c cookiejar.txt -X POST -u admin:admin -H "X-Atlassian-Token: no-check" \
     *   -H "Content-Type: image/png" --data-binary @mynewavatar.png \
     *   'http://localhost:8090/jira/rest/api/2/issuetype/1/avatar/temporary?filename=mynewavatar.png'
     *
     * curl -b cookiejar.txt -X POST -u admin:admin -H "X-Atlassian-Token: no-check" \
     *   -H "Content-Type: application/json" --data '{"cropperWidth": "65","cropperOffsetX": "10","cropperOffsetY": "16"}' \
     *   -o tmpid.json \
     *   http://localhost:8090/jira/rest/api/2/issuetype/1/avatar
     *
     * curl -b cookiejar.txt -X PUT -u admin:admin -H "X-Atlassian-Token: no-check" \
     *   -H "Content-Type: application/json" --data-binary @tmpid.json \
     *   http://localhost:8090/jira/rest/api/2/issuetype/1/avatar
     * </pre>
     *
     * @since 6.4
     *
     * @param issueTypeId the id of the issue type, which avatar is updated.
     * @param filename name of file being uploaded
     * @param size size of file
     * @param request servlet request
     * @return temporary avatar cropping instructions
     *
     * @response.representation.201.qname
     *      avatar
     *
     * @response.representation.201.mediaType
     *      application/json
     *
     * @response.representation.201.doc
     *      temporary avatar cropping instructions
     *
     * @response.representation.201.example
     *      {@link com.atlassian.jira.rest.v2.issue.AvatarCroppingBean#DOC_EXAMPLE}
     *
     * @response.representation.401.doc
     *      Returned if the calling user is not authenticated.
     * @response.representation.403.doc
     *      Returned if the calling user does not have permission to administer JIRA.
     * @response.representation.404.doc
     *      Returned if the issue type to update does not exist or if the request does not contain valid XSRF token.
     */
    @POST
    @Consumes (MediaType.WILDCARD)
    @Path("{id}/avatar/temporary")
    public Response storeTemporaryAvatar(@PathParam ("id") final String issueTypeId,
            @QueryParam ("filename") final String filename,
            @QueryParam ("size") final Long size,
            @Context final HttpServletRequest request)
    {
        return validateUploadAttachment(authContext.getUser(), issueTypeId).getOrElse(new Supplier<Response>()
        {
            @Override
            public Response get()
            {
                return avatarResourceHelper.storeTemporaryAvatar(Avatar.Type.ISSUETYPE, issueTypeId, filename, size, request);
            }
        });
    }

    /**
     * Creates temporary avatar using multipart. The response is sent back as JSON stored in a textarea. This is because
     * the client uses remote iframing to submit avatars using multipart. So we must send them a valid HTML page back from
     * which the client parses the JSON from.
     *
     * <p>
     * Creating a temporary avatar is part of a 3-step process in uploading a new
     * avatar for an issue type: upload, crop, confirm. This endpoint allows you to use a multipart upload
     * instead of sending the image directly as the request body.
     * </p>
     *
     * <p>You *must* use "avatar" as the name of the upload parameter:</p>
     *
     * <pre>
     * curl -c cookiejar.txt -X POST -u admin:admin -H "X-Atlassian-Token: no-check" \
     *   -F "avatar=@mynewavatar.png;type=image/png" \
     *   'http://localhost:8090/jira/rest/api/2/issuetype/1/avatar/temporary'
     * </pre>
     *
     * @since 6.4
     *
     * @param issueTypeId the id of the issue type, which avatar is updated.
     * @param request servlet request
     *
     * @return temporary avatar cropping instructions
     *
     * @response.representation.201.qname
     *      avatar
     *
     * @response.representation.201.mediaType
     *      text/html
     *
     * @response.representation.201.doc
     *      temporary avatar cropping instructions embeded in HTML page. Error messages will also be embeded in the page.
     *
     * @response.representation.201.example
     *      {@link com.atlassian.jira.rest.v2.issue.AvatarCroppingBean#DOC_EXAMPLE}
     *
     * @response.representation.401.doc
     *      Returned if the calling user is not authenticated.
     * @response.representation.403.doc
     *      Returned if the calling user does not have permission to administer JIRA.
     * @response.representation.404.doc
     *      Returned if the issue type to update does not exist or if the request does not contain valid XSRF token.
     */
    @POST
    @Consumes (MediaType.MULTIPART_FORM_DATA)
    @Path("{id}/avatar/temporary")
    @Produces ({ MediaType.TEXT_HTML })
    public Response storeTemporaryAvatarUsingMultiPart(@PathParam("id") final String issueTypeId,
            @MultipartFormParam("avatar") final FilePart filePart,
            @Context final HttpServletRequest request)
    {
        return validateUploadAttachment(authContext.getUser(), issueTypeId).getOrElse(new Supplier<Response>()
        {
            @Override
            public Response get()
            {
                return avatarResourceHelper.storeTemporaryAvatarUsingMultiPart(Avatar.Type.ISSUETYPE, issueTypeId, filePart, request);
            }
        });
    }

    /**
     * Converts temporary avatar into a real avatar
     *
     * @since 6.4
     *
     * @param issueTypeId the id of the issue type, which avatar is updated.
     * @param croppingInstructions cropping instructions
     * @return created avatar
     *
     * @request.representation.example
     *      {@link com.atlassian.jira.rest.v2.issue.AvatarCroppingBean#DOC_EDIT_EXAMPLE}
     *
     * @response.representation.201.qname
     *      avatar
     *
     * @response.representation.201.mediaType
     *      application/json
     *
     * @response.representation.201.doc
     *      Returns created avatar
     *
     * @response.representation.201.example
     *      {@link AvatarBean#DOC_EXAMPLE}
     *
     * @response.representation.400.doc
     *      Returned if the cropping coordinates are invalid
     *
     * @response.representation.403.doc
     *      Returned if the currently authenticated user does not have permission to pick avatar
     *
     * @response.representation.404.doc
     *      Returned if the currently authenticated user does not have EDIT PROJECT permission.
     *
     * @response.representation.500.doc
     *      Returned if an error occurs while converting temporary avatar to real avatar
     */
    @POST
    @Path ("{id}/avatar")
    public Response createAvatarFromTemporary(@PathParam ("id") final String issueTypeId,
            final AvatarCroppingBean croppingInstructions)
    {
        return validateUploadAttachment(authContext.getUser(), issueTypeId).getOrElse(new Supplier<Response>()
        {
            @Override
            public Response get()
            {
                return avatarResourceHelper.createAvatarFromTemporary(Avatar.Type.ISSUETYPE, issueTypeId, croppingInstructions);
            }
        });
    }

    private Option<Response> validateUploadAttachment(final ApplicationUser user, final String issueTypeId)
    {
        final XsrfCheckResult xsrfCheckResult = xsrfChecker.checkWebRequestInvocation(ExecutingHttpRequest.get());

        if (user == null)
        {
            return Option.some(status(Response.Status.UNAUTHORIZED)
                    .entity(i18n.getText("rest.authentication.no.user.logged.in"))
                    .cacheControl(never())
                    .build());
        }
        else if (!globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user))
        {
            return Option.some(status(Response.Status.FORBIDDEN)
                    .entity(i18n.getText("rest.authorization.admin.required"))
                    .cacheControl(never())
                    .build());
        }
        else if (issueTypeService.getIssueType(user, issueTypeId).isEmpty())
        {
            return Option.some(status(Response.Status.NOT_FOUND)
                    .entity(i18n.getText("admin.error.issue.type.update.not.exist"))
                    .cacheControl(never())
                    .build());
        }
        else if (xsrfCheckResult.isRequired() && !xsrfCheckResult.isValid())
        {
            return Option.some(Response.status(Response.Status.NOT_FOUND).entity("XSRF check failed").build());
        }
        else
        {
            return Option.none(Response.class);
        }
    }

    private Response error(final com.atlassian.jira.util.ErrorCollection errorCollection)
    {
        Reason reason = Reason.getWorstReason(errorCollection.getReasons());
        return status(reason.getHttpStatusCode())
                .entity(ErrorCollection.of(errorCollection))
                .cacheControl(never())
                .build();
    }

    private List<IssueTypeJsonBean> transformIssueTypeToBeans(final Iterable<IssueType> issueTypes)
    {
        return ImmutableList.copyOf(transform(issueTypes, new Function<IssueType, IssueTypeJsonBean>()
        {
            @Override
            public IssueTypeJsonBean apply(final IssueType issueType)
            {
                return createIssueTypeBean(issueType);
            }
        }));
    }

    private IssueTypeJsonBean createIssueTypeBean(IssueType issueType)
    {
        return new IssueTypeBeanBuilder()
                .jiraBaseUrls(jiraBaseUrls)
                .context(contextUriInfo)
                .issueType(issueType)
                .build();
    }

    private IssueTypeCreateInput issueTypeCreateInputFromBean(final IssueTypeCreateBean issueTypeCreateBean)
    {
        return IssueTypeCreateInput.builder()
                .setDescription(issueTypeCreateBean.getDescription())
                .setName(issueTypeCreateBean.getName())
                .setType(issueTypeCreateBean.getType() == IssueTypeCreateBean.Type.standard ?
                        IssueTypeCreateInput.Type.STANDARD : IssueTypeCreateInput.Type.SUBTASK)
                .build();
    }

    private IssueTypeUpdateInput issueTypeUpdateInputFromBean(final IssueTypeUpdateBean issueTypeUpdateBean)
    {
        return IssueTypeUpdateInput.builder()
                .setDescription(issueTypeUpdateBean.getDescription())
                .setName(issueTypeUpdateBean.getName())
                .setAvatarId(issueTypeUpdateBean.getAvatarId())
                .build();
    }

    private Response notFoundResponse()
    {
        return status(Response.Status.NOT_FOUND)
                .cacheControl(never())
                .entity(ErrorCollection.of(i18n.getText("admin.error.issue.type.get.not.exist")))
                .build();
    }

}
