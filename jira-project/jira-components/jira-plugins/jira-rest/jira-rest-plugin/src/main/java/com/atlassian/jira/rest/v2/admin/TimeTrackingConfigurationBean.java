package com.atlassian.jira.rest.v2.admin;

import org.codehaus.jackson.annotate.JsonProperty;

import static com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration.TimeFormat;

public class TimeTrackingConfigurationBean
{
    public enum TimeTrackingUnit { minute, hour, day, week }

    @JsonProperty
    private double workingHoursPerDay;
    @JsonProperty
    private double workingDaysPerWeek;
    @JsonProperty
    private TimeFormat timeFormat;
    @JsonProperty
    private TimeTrackingUnit defaultUnit;

    public TimeTrackingConfigurationBean(double workingHoursPerDay,
            double workingDaysPerWeek,
            TimeFormat timeFormat,
            TimeTrackingUnit defaultUnit)
    {
        this.workingHoursPerDay = workingHoursPerDay;
        this.workingDaysPerWeek = workingDaysPerWeek;
        this.timeFormat = timeFormat;
        this.defaultUnit = defaultUnit;
    }

    public double getWorkingHoursPerDay()
    {
        return workingHoursPerDay;
    }

    public double getWorkingDaysPerWeek()
    {
        return workingDaysPerWeek;
    }

    public TimeFormat getTimeFormat()
    {
        return timeFormat;
    }

    public TimeTrackingUnit getDefaultUnit()
    {
        return defaultUnit;
    }
}
