package com.atlassian.jira.rest.v2.issue.attachment.authorization;

import com.atlassian.jira.bc.issue.attachment.AttachmentService;
import com.atlassian.jira.rest.factory.JiraServiceContextFactory;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Creates {@link com.atlassian.jira.rest.v2.issue.attachment.authorization.AttachmentAuthorizer} instances.
 *
 * @since v6.4
 */
@ExportAsService
@Component
public class AttachmentAuthorizerFactory
{
    private AttachmentService attachmentService;

    private JiraServiceContextFactory contextFactory;
    private PermissionManager permissionManager;

    @SuppressWarnings ("unused")
    private AttachmentAuthorizerFactory()
    {

    }

    @Autowired
    @SuppressWarnings ("unused")
    private AttachmentAuthorizerFactory(
            final AttachmentService attachmentService,
            final JiraServiceContextFactory contextFactory,
            final PermissionManager permissionManager
    )
    {
        this.attachmentService = attachmentService;
        this.contextFactory = contextFactory;
        this.permissionManager = permissionManager;

    }

    public AttachmentAuthorizer removal()
    {
        return new AttachmentRemovalAuthorizer(attachmentService, contextFactory);
    }

    public AttachmentAuthorizer view()
    {
        return new AttachmentViewAuthorizer(permissionManager);
    }
}
