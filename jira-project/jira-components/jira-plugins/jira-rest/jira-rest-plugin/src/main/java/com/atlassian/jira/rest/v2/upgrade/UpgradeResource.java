package com.atlassian.jira.rest.v2.upgrade;

import java.net.URI;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.upgrade.UpgradeManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.scheduler.status.RunDetails;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

/**
 * REST resource for executing and querying delayed upgrades.
 *
 * @since 6.4
 */
@Consumes ({ MediaType.APPLICATION_JSON })
@Produces ({ MediaType.APPLICATION_JSON })
@Path ("upgrade")
public class UpgradeResource
{
    private JiraAuthenticationContext jiraAuthenticationContext;
    private PermissionManager permissionManager;
    private UpgradeManager upgradeManager;
    private JiraBaseUrls jiraBaseUrls;

    public UpgradeResource(JiraAuthenticationContext jiraAuthenticationContext, PermissionManager permissionManager,
            UpgradeManager upgradeManager, JiraBaseUrls jiraBaseUrls)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.permissionManager = permissionManager;
        this.upgradeManager = upgradeManager;
        this.jiraBaseUrls = jiraBaseUrls;
    }

    /**
     * Runs any pending delayed upgrade tasks.  Need Admin permissions to do this.
     *
     * @return OK response if successful, array of error messages if schedule fails.
     */
    @POST
    public Response runUpgradesNow()
    {
        final ApplicationUser user = jiraAuthenticationContext.getUser();
        if (!permissionManager.hasPermission(Permissions.SYSTEM_ADMIN, user))
        {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        UpgradeManager.Status status = upgradeManager.scheduleDelayedUpgrades(0, true);
        if (status.successful())
        {
            URI location = UriBuilder.fromPath(jiraBaseUrls.restApi2BaseUrl()).path("upgrade").build();
            return Response.status(Response.Status.ACCEPTED).location(location).
                    header("Retry-After", 10).cacheControl(never()).build();
        }
        else
        {
            return Response.serverError().cacheControl(never()).entity(status.getErrors()).build();
        }
    }

    /**
     * Returns the result of the last upgrade task.
     *
     * Returns {@link javax.ws.rs.core.Response#seeOther(java.net.URI)} if still running.
     *
     * @return the result of the last upgrade.
     *
     * @response.representation.200.qname
     *
     *
     * @response.representation.200.mediaType
     *      application/json
     *
     * @response.representation.200.doc
     *      Returned if the upgrade is complete
     *
     * @response.representation.200.example
     *      {@link com.atlassian.jira.rest.v2.upgrade.UpgradeResultBean#DOC_EXAMPLE}

     * @response.representation.303.doc
     *      Returned if the upgrade task is still running.
     *
     * @response.representation.404.doc
     *      Returned if no prior upgrade task exists.
     *
     */
    @GET
    public Response getUpgradeResult()
    {
        final ApplicationUser user = jiraAuthenticationContext.getUser();
        if (!permissionManager.hasPermission(Permissions.ADMINISTER, user))
        {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        if ((upgradeManager.areDelayedUpgradesRunning()))
        {
            URI location = UriBuilder.fromPath(jiraBaseUrls.restApi2BaseUrl()).path("upgrade").build();
            return Response.status(Response.Status.SEE_OTHER).location(location).
                    header("Retry-After", 10).cacheControl(never()).build();
        }

        RunDetails result = upgradeManager.getLastUpgradeResult();
        if (result != null)
        {
            return Response.ok(upgradeManager.areDelayedUpgradesRunning()).entity(UpgradeResultBean.fromRunDetails(result))
                    .cacheControl(never())
                    .build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
