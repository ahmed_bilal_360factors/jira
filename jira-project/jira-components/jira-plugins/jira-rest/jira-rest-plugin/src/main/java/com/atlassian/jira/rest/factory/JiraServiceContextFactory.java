package com.atlassian.jira.rest.factory;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;

import org.springframework.stereotype.Component;

/**
 * Creates {@link com.atlassian.jira.bc.JiraServiceContext} instances.
 *
 * @since v6.4
 */
@Component
public class JiraServiceContextFactory
{
    JiraServiceContextFactory()
    {
    }

    /**
     * Instantiates JiraServiceContext with given user and new empty error collection. The I18nHelper will be created
     * from the User supplied.
     *
     * @param user user
     */
    public JiraServiceContext createContext(final ApplicationUser user)
    {
        return createContext(user, new SimpleErrorCollection());
    }

    /**
     * Instantiates JiraServiceContext with user and error collection. The I18nHelper will be created from the User
     * supplied.
     *
     * @param user user
     * @param errorCollection error collection; must not be null
     * @throws IllegalArgumentException if error collection is null
     */
    public JiraServiceContext createContext(final ApplicationUser user, final ErrorCollection errorCollection)
    {
        return createContext(user, errorCollection, null);
    }

    /**
     * Instantiates JiraServiceContext with user and error collection and I18nHelper.
     *
     * @param user user
     * @param errorCollection error collection; must not be null
     * @param i18nHelper optional I18nHelper to use
     * @throws IllegalArgumentException if error collection is null
     */
    public JiraServiceContext createContext(
            final ApplicationUser user,
            final ErrorCollection errorCollection,
            final I18nHelper i18nHelper
    )
    {
        return new JiraServiceContextImpl(user, errorCollection, i18nHelper);
    }

}
