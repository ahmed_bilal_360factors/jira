package com.atlassian.jira.rest.v1.jql;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.jira.rest.internal.jql.FieldAndPredicateAutoCompleteResultGenerator;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.CorsAllowed;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;

/**
 * Rest end point for JQL Autocomplete suggestions.
 *
 * @since v4.0
 */
@Path ("jql/autocomplete")
@AnonymousAllowed
@Produces ({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@CorsAllowed
public class AutoCompleteResource
{
    private final FieldAndPredicateAutoCompleteResultGenerator fieldAndPredicateAutoCompleteResultGenerator;

    public AutoCompleteResource(final FieldAndPredicateAutoCompleteResultGenerator fieldAndPredicateAutoCompleteResultGenerator)
    {
        this.fieldAndPredicateAutoCompleteResultGenerator = fieldAndPredicateAutoCompleteResultGenerator;
    }

    @GET
    /**
     * This is the AJAX entry point to find issues given a query string.  The particluar instance of this call can "grey out" issues
     * by providing extra filterer parameters.
     *
     * @param fieldName         what the user has typed in to search for the field name
     * @param fieldValue        the portion of the field value that has already been provided by the user.
     * @return a Response containing a list of {@link com.atlassian.jira.rest.v1.jql.AutoCompleteResource.AutoCompleteResultBean}'s containing the matching autocomplete values
     */
    public Response getIssuesResponse(@QueryParam ("fieldName") final String fieldName,
            @QueryParam ("fieldValue") final String fieldValue,
            @QueryParam ("predicateName") final String predicateName,
            @QueryParam ("predicateValue") final String predicateValue)
    {
        final ImmutableList.Builder<FieldAndPredicateAutoCompleteResultGenerator.Result> results = ImmutableList.builder();
        if (fieldValue != null)
        {
            results.addAll(fieldAndPredicateAutoCompleteResultGenerator.getAutoCompleteResultsForField(fieldName, fieldValue));
        }
        if (predicateName != null)
        {
            results.addAll(fieldAndPredicateAutoCompleteResultGenerator.getAutoCompleteResultsForPredicate(predicateName, predicateValue, fieldName));
        }

        return Response.ok(AutoCompleteResultsBean.wrap(results.build()))
                .cacheControl(NO_CACHE)
                .build();
    }

    @XmlRootElement (name = "autoCompleteResultWrapper")
    public static class AutoCompleteResultsBean
    {
        @XmlElement
        private List<AutoCompleteResultBean> results;

        public AutoCompleteResultsBean()
        {
            this(Lists.<AutoCompleteResultBean>newArrayList());
        }

        public AutoCompleteResultsBean(final Iterable<AutoCompleteResultBean> results)
        {
            this.results = ImmutableList.copyOf(results);
        }

        public static AutoCompleteResultsBean wrap(final ImmutableList<FieldAndPredicateAutoCompleteResultGenerator.Result> results)
        {
            return new AutoCompleteResultsBean(Iterables.transform(results, new Function<FieldAndPredicateAutoCompleteResultGenerator.Result, AutoCompleteResultBean>()
            {
                @Override
                public AutoCompleteResultBean apply(final FieldAndPredicateAutoCompleteResultGenerator.Result result)
                {
                    return new AutoCompleteResultBean(result.getValue(), result.getDisplayName());
                }
            }));
        }
    }

    @XmlRootElement
    public static class AutoCompleteResultBean
    {
        @XmlElement
        private String value;
        @XmlElement
        private String displayName;

        public AutoCompleteResultBean()
        {
        }

        public AutoCompleteResultBean(String value, String displayName)
        {
            this.value = value;
            this.displayName = displayName;
        }
    }

}
