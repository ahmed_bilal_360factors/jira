package com.atlassian.jira.rest.v2.search;

import java.util.List;
import java.util.Locale;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.rest.internal.jql.FieldAndPredicateAutoCompleteResultGenerator;
import com.atlassian.jira.rest.v2.issue.RESTException;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.web.component.jql.AutoCompleteJsonGenerator;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;

/**
 * Resource for auto complete data for searches.
 *
 * @since v5.1
 */
@Path ("jql/autocompletedata")
@AnonymousAllowed
@Consumes ( { MediaType.APPLICATION_JSON })
@Produces ( { MediaType.APPLICATION_JSON })
public class SearchAutoCompleteResource
{
    public static final String DOC_EXAMPLE =
            "{" +
                "\"visibleFieldNames\": [{\"value\":\"affectedVersion\",\"displayName\":\"affectedVersion\",\"auto\":\"true\",\"orderable\":\"true\",\"searchable\":\"true\",\"operators\":[\"=\",\"!=\",\"in\",\"not in\",\"is\",\"is not\",\"<\",\"<=\",\">\",\">=\"],\"types\":[\"com.atlassian.jira.project.version.Version\"]},{\"value\":\"assignee\",\"displayName\":\"assignee\",\"auto\":\"true\",\"orderable\":\"true\",\"searchable\":\"true\",\"operators\":[\"!=\",\"was not in\",\"not in\",\"was not\",\"is\",\"was in\",\"was\",\"=\",\"in\",\"changed\",\"is not\"],\"types\":[\"com.atlassian.crowd.embedded.api.User\"]}]," +
                "\"visibleFunctionNames\": {\"value\":\"currentLogin()\",\"displayName\":\"currentLogin()\",\"types\":[\"java.util.Date\"]},{\"value\":\"currentUser()\",\"displayName\":\"currentUser()\",\"types\":[\"com.atlassian.crowd.embedded.api.User\"]}]," +
                "\"jqlReservedWords\": \"empty\",\"and\",\"or\",\"in\",\"distinct\"]" +
            "}";

    public static final AutoCompleteResultWrapper SUGGESTIONS_DOC_EXAMPLE =
            AutoCompleteResultWrapper.wrap(ImmutableList.<FieldAndPredicateAutoCompleteResultGenerator.Result>of(
                    new FieldAndPredicateAutoCompleteResultGenerator.Result("ActiveObjects", "<b>Ac</b>tiveObjects (AO)"),
                    new FieldAndPredicateAutoCompleteResultGenerator.Result("Atlassian Connect", "Atlassian Connect (<b>AC</b>)"),
                    new FieldAndPredicateAutoCompleteResultGenerator.Result("Atlassian Connect in JIRA", "Atlassian Connect in JIRA (<b>AC</b>JIRA)")
            ));

    private final AutoCompleteJsonGenerator autoCompleteJsonGenerator;
    private final JiraAuthenticationContext authContext;
    private final I18nHelper i18n;
    private final FieldAndPredicateAutoCompleteResultGenerator fieldAndPredicateAutoCompleteResultGenerator;

    public SearchAutoCompleteResource(final AutoCompleteJsonGenerator autoCompleteJsonGenerator,
            final JiraAuthenticationContext authContext,
            final I18nHelper i18n,
            final FieldAndPredicateAutoCompleteResultGenerator fieldAndPredicateAutoCompleteResultGenerator)
    {
        this.autoCompleteJsonGenerator = autoCompleteJsonGenerator;
        this.authContext = authContext;
        this.i18n = i18n;
        this.fieldAndPredicateAutoCompleteResultGenerator = fieldAndPredicateAutoCompleteResultGenerator;
    }

    /**
     * Returns the auto complete data required for JQL searches.
     *
     * @return the auto complete data required for JQL searches.
     *
     * @response.representation.200.mediaType application/json
     *
     * @response.representation.200.doc
     *      The auto complete data required for JQL searches.
     *
     * @response.representation.200.example
     *      {@link #DOC_EXAMPLE}
     *
     * @response.representation.401.doc
     *      Returned if the calling user is not authenticated.
     *
     * @response.representation.500.doc
     *      Returned if an error occurs while generating the response.
     */
    @GET
    public Response getAutoComplete()
    {
        final User user = authContext.getLoggedInUser();
        final Locale locale = authContext.getLocale();

        try
        {
            final String entity =
                    "{" +
                        "\"visibleFieldNames\": " + autoCompleteJsonGenerator.getVisibleFieldNamesJson(user, locale) + "," +
                        "\"visibleFunctionNames\": " + autoCompleteJsonGenerator.getVisibleFunctionNamesJson(user, locale) + "," +
                        "\"jqlReservedWords\": " + autoCompleteJsonGenerator.getJqlReservedWordsJson() +
                    "}";

            return Response.ok(entity).cacheControl(never()).build();

        }
        catch (JSONException e)
        {
            throw new RESTException(ErrorCollection.of(i18n.getText("rest.error.generating.response"))
                    .reason(com.atlassian.jira.util.ErrorCollection.Reason.SERVER_ERROR));
        }
    }

    /**
     * Returns auto complete suggestions for JQL search.
     *
     * @param fieldName the field name for which the suggestions are generated.
     * @param fieldValue the portion of the field value that has already been provided by the user.
     * @param predicateName the predicate for which the suggestions are generated. Suggestions are generated only for: "by", "from" and "to".
     * @param predicateValue the portion of the predicate value that has already been provided by the user.
     *
     * @response.representation.200.mediaType application/json
     * @response.representation.200.doc The autocompletion suggestions for JQL search.
     * @response.representation.200.example {@link #SUGGESTIONS_DOC_EXAMPLE}
     *
     * @return a Response containing a list of {@link com.atlassian.jira.rest.v1.jql.AutoCompleteResource.AutoCompleteResultBean}'s containing the matching autocomplete values
     */
    @Path("suggestions")
    @GET
    public Response getFieldAutoCompleteForQueryString(@QueryParam ("fieldName") final String fieldName,
            @QueryParam("fieldValue") final String fieldValue,
            @QueryParam("predicateName") final String predicateName,
            @QueryParam("predicateValue") final String predicateValue)
    {
        final ImmutableList.Builder<FieldAndPredicateAutoCompleteResultGenerator.Result> results = ImmutableList.builder();
        if (fieldName != null)
        {
            results.addAll(fieldAndPredicateAutoCompleteResultGenerator.getAutoCompleteResultsForField(fieldName, StringUtils.defaultIfEmpty(fieldValue, StringUtils.EMPTY)));
        }
        if (predicateName != null)
        {
            results.addAll(fieldAndPredicateAutoCompleteResultGenerator.getAutoCompleteResultsForPredicate(predicateName, predicateValue, fieldName));
        }
        return Response.ok(AutoCompleteResultWrapper.wrap(results.build()))
                .cacheControl(NO_CACHE)
                .build();
    }


    public static class AutoCompleteResultWrapper
    {
        @XmlElement
        private List<AutoCompleteResultBean> results;

        public AutoCompleteResultWrapper(final Iterable<AutoCompleteResultBean> results)
        {
            this.results = ImmutableList.copyOf(results);
        }

        public static AutoCompleteResultWrapper wrap(final ImmutableList<FieldAndPredicateAutoCompleteResultGenerator.Result> results)
        {
            return new AutoCompleteResultWrapper(Iterables.transform(results, new Function<FieldAndPredicateAutoCompleteResultGenerator.Result, AutoCompleteResultBean>()
            {
                @Override
                public AutoCompleteResultBean apply(final FieldAndPredicateAutoCompleteResultGenerator.Result result)
                {
                    return new AutoCompleteResultBean(result.getValue(), result.getDisplayName());
                }
            }));
        }
    }

    @XmlRootElement
    public static class AutoCompleteResultBean
    {
        @XmlElement
        private String value;
        @XmlElement
        private String displayName;

        public AutoCompleteResultBean(String value, String displayName)
        {
            this.value = value;
            this.displayName = displayName;
        }
    }

}
