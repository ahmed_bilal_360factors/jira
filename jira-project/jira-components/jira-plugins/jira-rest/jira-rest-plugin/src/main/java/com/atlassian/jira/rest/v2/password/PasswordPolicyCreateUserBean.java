package com.atlassian.jira.rest.v2.password;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Represents a password policy check when creating a new user.
 *
 * @since v6.1
 */
public class PasswordPolicyCreateUserBean
{
    static final PasswordPolicyCreateUserBean DOC_EXAMPLE = new PasswordPolicyCreateUserBean()
    {
        {
            this.username = "fred";
            this.displayName = "Fred Normal";
            this.emailAddress = "fred@example.com";
            this.password = "secret";
        }
    };

    @JsonProperty
    String username;

    @JsonProperty
    String displayName;

    @JsonProperty
    String emailAddress;

    @JsonProperty
    String password;

    public String getUsername()
    {
        return username;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public String getPassword()
    {
        return password;
    }
}
