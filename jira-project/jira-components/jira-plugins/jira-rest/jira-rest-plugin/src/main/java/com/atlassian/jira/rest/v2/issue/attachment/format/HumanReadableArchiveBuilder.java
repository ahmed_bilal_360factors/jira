package com.atlassian.jira.rest.v2.issue.attachment.format;

/**
 * Builds a {@link com.atlassian.jira.rest.v2.issue.attachment.format.HumanReadableArchiveBuilder}
 *
 * @since v6.4
 */
public class HumanReadableArchiveBuilder
{
    private long id;
    private String name;
    private Iterable<HumanReadableArchiveEntry> entries;
    private long totalEntryCount;
    private String mediaType;

    public HumanReadableArchiveBuilder id(final long id)
    {
        this.id = id;
        return this;
    }

    public HumanReadableArchiveBuilder name(final String name)
    {
        this.name = name;
        return this;
    }

    public HumanReadableArchiveBuilder entries(final Iterable<HumanReadableArchiveEntry> entries)
    {
        this.entries = entries;
        return this;
    }

    public HumanReadableArchiveBuilder totalEntryCount(final long totalEntryCount)
    {
        this.totalEntryCount = totalEntryCount;
        return this;
    }

    public HumanReadableArchiveBuilder mediaType(final String mediaType)
    {
        this.mediaType = mediaType;
        return this;
    }

    public HumanReadableArchive build()
    {
        return new HumanReadableArchive(
                id,
                name,
                entries,
                totalEntryCount,
                mediaType
        );
    }
}
