package com.atlassian.jira.rest.v2.password;

import java.util.Collection;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.jira.plugin.user.PasswordPolicyManager;
import com.atlassian.jira.plugin.user.WebErrorMessage;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;

import com.google.common.collect.ImmutableList;

import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * REST resource for operations related to passwords and the password policy.
 *
 * @since v6.1
 */
@Path ("password")
@Consumes ({ MediaType.APPLICATION_JSON })
@Produces ({ MediaType.APPLICATION_JSON })
public class PasswordResource
{
    private final PasswordPolicyManager passwordPolicyManager;
    private final UserManager userManager;

    public PasswordResource(PasswordPolicyManager passwordPolicyManager, UserManager userManager)
    {
        this.passwordPolicyManager = passwordPolicyManager;
        this.userManager = userManager;
    }

    /**
     * Returns user-friendly statements governing the system's password policy.
     *
     * @param hasOldPassword whether or not the user will be required to enter their current password.  Use
     *      {@code false} (the default) if this is a new user or if an administrator is forcibly changing
     *      another user's password.
     * @return a response containing a JSON array of the user-facing messages.  If no policy is set, then
     *      this will be an empty list.
     *
     * @response.representation.200.mediaType
     *      application/json
     *
     * @response.representation.200.doc
     *      Returns an array of message strings.
     */
    @GET
    @Path("policy")
    public Response getPasswordPolicy(@QueryParam("hasOldPassword") @DefaultValue("false") boolean hasOldPassword)
    {
        return Response.ok(passwordPolicyManager.getPolicyDescription(hasOldPassword)).build();
    }

    /**
     * Returns user-friendly explanations of why the password policy would disallow a proposed user from being
     * created.
     * <p>
     * This is a "dry run" of the password policy validation that would be performed by the various user creation
     * methods in {@link com.atlassian.jira.bc.user.UserService}.  The intended use is for a user interface to
     * verify the password on the fly as the user enters it (or upon moving to another input field or delaying
     * for some time period, and so on).  At the very least, the username and password must be non-empty to run
     * these validations.  Note that this validation is only for the password policy itself; other validations,
     * such as whether or not a user with the same name already exists, are not checked by this request.
     * </p>
     *
     * @param bean a representation of the intended parameters for the user that would be created.
     * @return a response containing a JSON array of the user-facing messages.  If no policy is set, then
     *      this will be an empty list.
     *
     * @request.representation.mediaType
     *      application/json
     *
     * @request.representation.example
     *      {@link com.atlassian.jira.rest.v2.password.PasswordPolicyCreateUserBean#DOC_EXAMPLE}
     *      The username and new password must be specified.  The old password should be specified for
     *      updates where the user would be required to enter it and omitted for those like a password
     *      reset or forced change by the administrator where the old password would not be known.
     *
     * @response.representation.200.mediaType
     *      application/json
     *
     * @response.representation.200.doc
     *      Returns an array of message strings.
     *
     * @response.representation.400.doc
     *      Returned if the request is invalid, such as if the username or password is left unspecified.
     */
    @POST
    @Path("policy/createUser")
    public Response policyCheckCreateUser(PasswordPolicyCreateUserBean bean)
    {
        if (bean == null || isEmpty(bean.getUsername()) || isEmpty(bean.getPassword()))
        {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        return Response.ok(snippets(passwordPolicyManager.checkPolicy(bean.getUsername(),
                bean.getDisplayName(), bean.getEmailAddress(), bean.getPassword()))).build();
    }

    /**
     * Returns user-friendly explanations of why the password policy would disallow an existing user's password
     * from being updated.
     * <p>
     * This is a "dry run" of the password policy validation that would be performed by the various ways to
     * update a user's password, such as the {@link com.atlassian.jira.web.action.user.ChangePassword ChangePassword}
     * and {@link com.atlassian.jira.web.action.user.ResetPassword ResetPassword} web actions.
     * The intended use is for a user interface to verify the password on the fly as the user enters it (or upon
     * moving to another input field or delaying for some time period, and so on).  At the very least, the username
     * and new password must be non-empty to run these validations, and the user must actually exist.  Note that this
     * validation is only for the password policy itself; other validations that would be performed upon submitting
     * the request are not checked by this request.  In particular, the old password (if specified) is deliberately
     * not verified by this request, as doing so could cause security problems.
     * </p>
     *
     * @param bean a representation of the intended parameters for the update that would be performed
     * @return a response containing a JSON array of the user-facing messages.  If no policy is set, then
     *      this will be an empty list.
     *
     * @request.representation.mediaType
     *      application/json
     *
     * @request.representation.example
     *      {@link com.atlassian.jira.rest.v2.password.PasswordPolicyUpdateUserBean#DOC_EXAMPLE}
     *      The username and new password must be specified.  The old password should be specified for
     *      updates where the user would be required to enter it and omitted for those like a password
     *      reset or forced change by the administrator where the old password would not be known.
     *
     * @response.representation.200.mediaType
     *      application/json
     *
     * @response.representation.200.doc
     *      Returns an array of message strings.
     *
     * @response.representation.400.doc
     *      Returned if the request is invalid, such as if the username or new password is left unspecified.
     *
     * @response.representation.404.doc
     *      Returned if the username does not correspond to any existing user.
     */
    @POST
    @Path("policy/updateUser")
    public Response policyCheckUpdateUser(PasswordPolicyUpdateUserBean bean)
    {
        if (bean == null || isEmpty(bean.getUsername()) || isEmpty(bean.getNewPassword()))
        {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        final ApplicationUser existing = userManager.getUserByName(bean.getUsername());
        if (existing == null)
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        return Response.ok(snippets(passwordPolicyManager.checkPolicy(existing, bean.getOldPassword(),
                bean.getNewPassword()))).build();
    }

    private static List<String> snippets(Collection<WebErrorMessage> messages)
    {
        final ImmutableList.Builder<String> snippets = ImmutableList.builder();
        for (WebErrorMessage message : messages)
        {
            snippets.add(message.getSnippet());
        }
        return snippets.build();
    }
}
