package com.atlassian.jira.rest.v2.issue.attachment;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.rest.v2.issue.attachment.authorization.AttachmentAuthorizer;
import com.atlassian.jira.rest.v2.issue.attachment.format.ArchiveFormatter;
import com.atlassian.jira.rest.v2.issue.attachment.format.AttachmentMetaBean;
import com.atlassian.jira.rest.v2.issue.attachment.operation.AttachmentOperation;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import webwork.config.Configuration;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

/**
 * @since v4.2
 */
@Path ("attachment")
@AnonymousAllowed
@Consumes ({ MediaType.APPLICATION_JSON })
@Produces ({ MediaType.APPLICATION_JSON })
public class AttachmentResource
{
    private AttachmentStrategyFactory factory;
    private AttachmentManager attachmentManager;

    /**
     * This constructor used by tooling.
     */
    @SuppressWarnings ("unused")
    private AttachmentResource()
    {
    }

    @SuppressWarnings ("unused")
    public AttachmentResource(final AttachmentStrategyFactory factory, final AttachmentManager attachmentManager)
    {
        this.factory = factory;
        this.attachmentManager = attachmentManager;
    }

    /**
     * Returns the meta-data for an attachment, including the URI of the actual attached file.
     *
     * @param id id of the attachment to view
     * @return A response
     *
     * @response.representation.200.qname
     *      attachment
     *
     * @response.representation.200.mediaType
     *      application/json
     *
     * @response.representation.200.example
     *      {@link com.atlassian.jira.rest.v2.issue.attachment.format.AttachmentBean#DOC_EXAMPLE}
     *
     * @response.representation.200.doc
     *      JSON representation of the attachment meta-data. The representation does not contain the attachment itself,
     *      but contains a URI that can be used to download the actual attached file.
     *
     * @response.representation.403.doc
     *      The calling user is not permitted to view the requested attachment.
     *
     * @response.representation.404.doc
     *      Any of:
     *      <ul>
     *          <li>there is no attachment with the requested id</li>
     *          <li>attachments feature is disabled</li>
     *      </ul>
     */
    @GET
    @Path ("{id}")
    public Response getAttachment(@PathParam ("id") final String id)
    {
        final AttachmentAuthorizer authorizer = factory.authorizer().view();
        final AttachmentOperation operation = factory.operation().view();
        final AttachmentResourceStrategy strategy = factory.resourceStrategy(authorizer, operation);
        return strategy.processAttachment(id);
    }

    /**
     * Remove an attachment from an issue.
     *
     * @param id id of the attachment to remove
     * @return A response
     *
     * @response.representation.204.doc
     *      Removal was successful
     *
     * @response.representation.403.doc
     *      The calling user is not permitted to remove the requested attachment.
     *
     * @response.representation.404.doc
     *      Any of:
     *      <ul>
     *          <li>there is no attachment with the requested id</li>
     *          <li>attachments feature is disabled</li>
     *      </ul>
     */
    @DELETE
    @Path ("{id}")
    public Response removeAttachment(@PathParam ("id") final String id)
    {
        final AttachmentAuthorizer authorizer = factory.authorizer().removal();
        final AttachmentOperation operation = factory.operation().removal();
        final AttachmentResourceStrategy strategy = factory.resourceStrategy(authorizer, operation);
        return strategy.processAttachment(id);
    }

    /**
     * Returns the meta information for an attachments, specifically if they are enabled and the maximum upload size
     * allowed.
     *
     * @return a JSON representation of the enable attachment capabilities
     *
     * @response.representation.200.qname
     *      attachmentMeta
     *
     * @response.representation.200.mediaType
     *      application/json
     *
     * @response.representation.200.example
     *      {@link com.atlassian.jira.rest.v2.issue.attachment.format.AttachmentMetaBean#DOC_EXAMPLE}
     *
     * @response.representation.200.doc
     *      JSON representation of the attachment capabilities.
     *      Consumers of this resource may also need to check if the logged in user has permission to upload or
     *      otherwise manipulate attachments using the {@link com.atlassian.jira.rest.v2.permission.PermissionsResource}.
     */
    @GET
    @Path ("meta")
    public Response getAttachmentMeta()
    {
        final boolean enabled = attachmentManager.attachmentsEnabled();
        final long maxSize = Long.parseLong(Configuration.getString(APKeys.JIRA_ATTACHMENT_SIZE));
        final AttachmentMetaBean bean = new AttachmentMetaBean(enabled, maxSize);
        return Response.ok(bean).cacheControl(never()).build();
    }

    /**
     * Tries to expand an attachment. Output is raw and should be backwards-compatible through the course of time.
     *
     * @param id the id of the attachment to expand.
     * @return A response
     * @since v6.4
     *
     * @response.representation.200.qname
     *      raw attachment contents
     *
     * @response.representation.200.mediaType
     *      application/json
     *
     * @response.representation.200.example
     *      {@link com.atlassian.jira.plugin.attachment.AttachmentArchiveImpl#DOC_EXAMPLE}
     *
     * @response.representation.200.doc
     *      JSON representation of the attachment expanded contents. Empty entry list means that attachment cannot
     *      be expanded. It's either empty, corrupt or not an archive at all.
     *
     * @response.representation.403.doc
     *      The calling user is not permitted to view the requested attachment.
     *
     * @response.representation.404.doc
     *      Any of:
     *      <ul>
     *          <li>there is no attachment with the requested id</li>
     *          <li>attachments feature is disabled</li>
     *      </ul>
     *
     * @response.representation.409.doc
     *      The archive format is not supported.
     */
    @GET
    @Path ("{id}/expand/raw")
    @ExperimentalApi
    public Response expandForMachines(@PathParam ("id") final String id)
    {
        final AttachmentAuthorizer authorizer = factory.authorizer().view();
        final ArchiveFormatter formatter = factory.formatter().raw();
        final AttachmentOperation operation = factory.operation().expansion(formatter);
        final AttachmentResourceStrategy strategy = factory.resourceStrategy(authorizer, operation);
        return strategy.processAttachment(id);
    }

    /**
     * Tries to expand an attachment. Output is human-readable and subject to change.
     *
     * @param id the id of the attachment to expand.
     * @return A response
     * @since v6.4
     *
     * @response.representation.200.qname
     *      human readable attachment contents
     *
     * @response.representation.200.mediaType
     *      application/json
     *
     * @response.representation.200.example
     *      {@link com.atlassian.jira.rest.v2.issue.attachment.format.HumanReadableArchive#DOC_EXAMPLE}
     *
     * @response.representation.200.doc
     *      JSON representation of the attachment expanded contents. Empty entry list means that attachment cannot
     *      be expanded. It's either empty, corrupt or not an archive at all.
     *
     * @response.representation.403.doc
     *      The calling user is not permitted to view the requested attachment.
     *
     * @response.representation.404.doc
     *      Any of:
     *      <ul>
     *          <li>there is no attachment with the requested id</li>
     *          <li>attachments feature is disabled</li>
     *      </ul>
     *
     * @response.representation.409.doc
     *      The archive format is not supported.
     */
    @GET
    @Path ("{id}/expand/human")
    @ExperimentalApi
    public Response expandForHumans(@PathParam ("id") final String id)
    {
        final AttachmentAuthorizer authorizer = factory.authorizer().view();
        final ArchiveFormatter formatter = factory.formatter().human();
        final AttachmentOperation operation = factory.operation().expansion(formatter);
        final AttachmentResourceStrategy strategy = factory.resourceStrategy(authorizer, operation);
        return strategy.processAttachment(id);
    }
}
