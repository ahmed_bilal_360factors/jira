package com.atlassian.jira.rest.v2.issue.attachment.operation;

import javax.ws.rs.core.Response;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.issue.attachment.AttachmentService;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.rest.factory.Responder;
import com.atlassian.jira.util.ErrorCollection;

/**
 * Removes an attachment.
 *
 * @since v6.4
 */
final class AttachmentRemoval implements AttachmentOperation
{
    private final AttachmentService attachmentService;
    private final Responder responder;

    AttachmentRemoval(final AttachmentService attachmentService, final Responder responder)
    {
        this.attachmentService = attachmentService;
        this.responder = responder;
    }

    @Override
    public Response perform(final Attachment attachment, final JiraServiceContext context)
    {
        final Long attachmentId = attachment.getId();
        attachmentService.delete(context, attachmentId);
        final ErrorCollection errors = context.getErrorCollection();
        if (errors.hasAnyErrors())
        {
            return responder.errors(errors);
        }
        else
        {
            return responder.successfulDelete();
        }
    }
}
