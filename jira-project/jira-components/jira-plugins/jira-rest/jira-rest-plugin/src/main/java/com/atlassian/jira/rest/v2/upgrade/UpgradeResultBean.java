package com.atlassian.jira.rest.v2.upgrade;

import java.util.Date;

import javax.annotation.Nonnull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.atlassian.jira.rest.bind.DateTimeAdapter;
import com.atlassian.scheduler.status.RunDetails;

/**
* @since v6.4
*/
@XmlRootElement
public class UpgradeResultBean
{
    @XmlJavaTypeAdapter (DateTimeAdapter.class)
    private Date startTime;

    @XmlElement
    private Long duration;

    @XmlElement
    private String outcome;

    @XmlElement
    private String message;

    public UpgradeResultBean() {}

    UpgradeResultBean(final Date startTime, final Long duration, final String outcome, final String message)
    {
        this.startTime = startTime;
        this.duration = duration;
        this.outcome = outcome;
        this.message = message;
    }

    final static UpgradeResultBean DOC_EXAMPLE = new UpgradeResultBean();
    static {
        DOC_EXAMPLE.startTime = new Date();
        DOC_EXAMPLE.duration = 2001L;
        DOC_EXAMPLE.outcome = "SUCCESS";
        DOC_EXAMPLE.message = "";
    }

    public Date getStartTime()
    {
        return startTime;
    }

    public Long getDuration()
    {
        return duration;
    }

    public String getOutcome()
    {
        return outcome;
    }

    public String getMessage()
    {
        return message;
    }

    public static UpgradeResultBean fromRunDetails(@Nonnull final RunDetails runDetails)
    {
        return new UpgradeResultBean(runDetails.getStartTime(),
                runDetails.getDurationInMillis(),
                runDetails.getRunOutcome().toString(),
                runDetails.getMessage());
    }
}
