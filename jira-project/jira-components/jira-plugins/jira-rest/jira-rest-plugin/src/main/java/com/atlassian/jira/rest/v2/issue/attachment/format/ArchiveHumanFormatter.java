package com.atlassian.jira.rest.v2.issue.attachment.format;

import java.net.URI;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.core.UriBuilder;

import com.atlassian.core.util.FileSize;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.Path;
import com.atlassian.jira.plugin.attachment.AttachmentArchive;
import com.atlassian.jira.plugin.attachment.AttachmentArchiveEntry;
import com.atlassian.jira.web.util.FileIconBean;
import com.atlassian.jira.web.util.FileIconUtil;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import com.google.common.collect.ImmutableList;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Output is human-readable and subject to change.
 *
 * @since v6.4
 */
final class ArchiveHumanFormatter implements ArchiveFormatter<HumanReadableArchive>
{
    private static final int MAX_ENTRY_LABEL_LENGTH = 40;
    private final FileIconUtil iconPicker;

    @Autowired
    ArchiveHumanFormatter(@ComponentImport final FileIconUtil iconPicker)
    {
        this.iconPicker = iconPicker;
    }

    @Override
    public HumanReadableArchive format(final AttachmentArchive archive, final Attachment attachment)
    {
        final Long id = attachment.getId();
        final List<AttachmentArchiveEntry> entries = archive.getEntries();
        final Collection<HumanReadableArchiveEntry> convertedEntries = convertEntries(entries);
        final String name = attachment.getFilename();
        final int totalEntryCount = archive.getTotalEntryCount();
        final String mediaType = attachment.getMimetype();
        return new HumanReadableArchive(id, name, convertedEntries, totalEntryCount, mediaType);
    }

    private Collection<HumanReadableArchiveEntry> convertEntries(final Iterable<AttachmentArchiveEntry> entries)
    {
        final ImmutableList.Builder<HumanReadableArchiveEntry> convertedEntries = ImmutableList.builder();
        for (final AttachmentArchiveEntry entry : entries)
        {
            final HumanReadableArchiveEntry convertedEntry = convertEntry(entry);
            convertedEntries.add(convertedEntry);
        }
        return convertedEntries.build();
    }

    private HumanReadableArchiveEntry convertEntry(final AttachmentArchiveEntry archiveEntry)
    {
        final Path path = new Path(archiveEntry.getName());
        final long index = archiveEntry.getEntryIndex();
        final String size = FileSize.format(archiveEntry.getSize());
        final String mediaType = archiveEntry.getMediaType();
        final FileIconBean.FileIcon fileIcon = iconPicker.getFileIcon(path.toString(), mediaType);
        final String iconPath = fileIcon.getIcon();
        final URI icon = UriBuilder.fromPath(iconPath).build();
        final String alternativeText = fileIcon.getAltText();
        final String label = path.abbreviate(MAX_ENTRY_LABEL_LENGTH).toString();
        return new HumanReadableArchiveEntry(path.toString(), index, size, mediaType, icon, alternativeText, label);
    }
}
