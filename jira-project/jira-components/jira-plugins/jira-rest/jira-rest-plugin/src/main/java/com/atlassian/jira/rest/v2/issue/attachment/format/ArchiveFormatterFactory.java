package com.atlassian.jira.rest.v2.issue.attachment.format;

import com.atlassian.jira.plugin.attachment.AttachmentArchive;
import com.atlassian.jira.web.util.FileIconUtil;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Creates {@link com.atlassian.jira.rest.v2.issue.attachment.format.ArchiveFormatter} instances.
 *
 * @since v6.4
 */
@ExportAsService
@Component
public class ArchiveFormatterFactory
{
    private FileIconUtil iconPicker;

    @SuppressWarnings ("unused")
    private ArchiveFormatterFactory()
    {

    }

    @Autowired
    @SuppressWarnings ("unused")
    private ArchiveFormatterFactory(@ComponentImport final FileIconUtil iconPicker)
    {
        this.iconPicker = iconPicker;
    }

    public ArchiveFormatter<HumanReadableArchive> human()
    {
        return new ArchiveHumanFormatter(iconPicker);
    }

    public ArchiveFormatter<AttachmentArchive> raw()
    {
        return new ArchiveRawFormatter();
    }
}
