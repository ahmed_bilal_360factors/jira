package com.atlassian.jira.rest.v2.issue.attachment.format;

import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.plugin.attachment.AttachmentArchive;

/**
 * Output is raw and should be backwards-compatible through the course of time.
 *
 * @since v6.4
 */
final class ArchiveRawFormatter implements ArchiveFormatter<AttachmentArchive>
{
    @Override
    public AttachmentArchive format(final AttachmentArchive archive, final Attachment attachment)
    {
        return archive;
    }
}
