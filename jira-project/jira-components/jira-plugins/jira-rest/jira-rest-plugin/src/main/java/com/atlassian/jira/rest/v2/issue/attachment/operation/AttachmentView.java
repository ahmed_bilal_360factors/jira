package com.atlassian.jira.rest.v2.issue.attachment.operation;

import javax.ws.rs.core.Response;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.rest.factory.Responder;
import com.atlassian.jira.rest.v2.issue.attachment.format.AttachmentBean;
import com.atlassian.jira.rest.v2.issue.builder.BeanBuilderFactory;

import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static javax.ws.rs.core.Response.Status.OK;

/**
 * Views details of an attachment.
 *
 * @since v6.4
 */
final class AttachmentView implements AttachmentOperation
{
    private final Responder responder;
    private final BeanBuilderFactory beanBuilderFactory;

    AttachmentView(
            final Responder responder,
            final BeanBuilderFactory beanBuilderFactory
    )
    {
        this.responder = responder;
        this.beanBuilderFactory = beanBuilderFactory;
    }

    @Override
    public Response perform(final Attachment attachment, final JiraServiceContext context)
    {
        final AttachmentBean bean = beanBuilderFactory.newAttachmentBeanBuilder(attachment).build();
        return responder
                .builder(OK)
                .entity(bean)
                .cacheControl(never())
                .build();
    }
}
