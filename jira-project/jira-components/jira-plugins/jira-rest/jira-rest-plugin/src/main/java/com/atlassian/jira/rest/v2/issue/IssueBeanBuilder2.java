package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldException;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.ProjectSystemField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.rest.FieldJsonRepresentation;
import com.atlassian.jira.issue.fields.rest.RestAwareField;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueLinksBeanBuilderFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.IssueRefJsonBean;
import com.atlassian.jira.rest.v2.issue.builder.BeanBuilderFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.workflow.IssueWorkflowManager;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.ws.rs.core.UriBuilder;

/**
 * Builder for {@link com.atlassian.jira.rest.v2.issue.IssueBean} instances.
 *
 * @since v5.0
 */
public class IssueBeanBuilder2
{
    private static final Logger LOG = LoggerFactory.getLogger(IssueBeanBuilder2.class);

    private final FieldLayoutManager fieldLayoutManager;
    private final JiraAuthenticationContext authContext;
    private final FieldManager fieldManager;
    private final ResourceUriBuilder resourceUriBuilder;
    private final BeanBuilderFactory beanBuilderFactory;
    private final IssueLinksBeanBuilderFactory issueLinkBeanBuilderFactory;
    private final IssueWorkflowManager issueWorkflowManager;
    private final Predicate<Field> fieldIncluded;
    private final Cache<Long, IssueRefJsonBean> issueRefs = CacheBuilder.newBuilder().build();

    // do not use CacheManager/CacheFactory to create his cache - we want it to be private
    private final LoadingCache<Field, String> fieldNames = CacheBuilder.newBuilder().build(new CacheLoader<Field, String>()
    {
        @Override
        public String load(@Nonnull final Field field) throws Exception
        {
            return field.getName();
        }
    });

    private Supplier<String> parentLinkName = Suppliers.memoize(new Supplier<String>()
    {
        @Override
        public String get()
        {
            return authContext.getI18nHelper().getText("issue.field.parent");
        }
    });

    private Supplier<List<NavigableField>> includedNavigableFields = Suppliers.memoize(new Supplier<List<NavigableField>>()
    {
        @Override
        public List<NavigableField> get()
        {
            try
            {
                @SuppressWarnings ("deprecation") final Set<NavigableField> fields = fieldManager.getAvailableNavigableFields(authContext.getLoggedInUser());
                return ImmutableList.copyOf(Iterables.filter(fields, fieldIncluded));
            }
            catch (FieldException e)
            {
                // ignored...display as much as we can.
                return Collections.emptyList();
            }
        }
    });

    /**
     * The UriInfo to use when generating links.
     */
    private final UriBuilder uriBuilder;

    /**
     * The list of fields to include in the bean. If null, include all fields.
     */
    private IncludedFields fieldsToInclude;

    /**
     * the expand query string
     */
    private final String expand;

    public IssueBeanBuilder2(
            final FieldLayoutManager fieldLayoutManager,
            final JiraAuthenticationContext authContext,
            final FieldManager fieldManager,
            final ResourceUriBuilder resourceUriBuilder,
            final BeanBuilderFactory beanBuilderFactory,
            final IncludedFields fieldsToInclude,
            final IssueLinksBeanBuilderFactory issueLinkBeanBuilderFactory,
            final IssueWorkflowManager issueWorkflowManager,
            final UriBuilder uriBuilder, final String expand)
    {
        this.fieldLayoutManager = fieldLayoutManager;
        this.authContext = authContext;
        this.fieldManager = fieldManager;
        this.resourceUriBuilder = resourceUriBuilder;
        this.beanBuilderFactory = beanBuilderFactory;
        this.fieldsToInclude = fieldsToInclude;
        this.issueLinkBeanBuilderFactory = issueLinkBeanBuilderFactory;
        this.issueWorkflowManager = issueWorkflowManager;
        this.uriBuilder = uriBuilder;
        this.expand = expand;
        this.fieldIncluded = fieldsToInclude == null ? Predicates.<Field>alwaysTrue() : new Predicate<Field>()
        {
            @Override
            public boolean apply(final Field field)
            {
                return fieldsToInclude.included(field);
            }
        };

    }

    public IssueBean build(final Issue issue)
    {
        final IssueBean bean = new IssueBean(issue.getId(), issue.getKey(), resourceUriBuilder.build(uriBuilder.clone(), IssueResource.class, String.valueOf(issue.getId())));
        bean.fieldsToInclude(fieldsToInclude);

        addFields(issue, bean);
        addParentLink(issue, bean);
        addTransitions(issue, bean);
        addOpsbar(issue, bean);

        if (expand != null && expand.contains("editmeta"))
        {
            final EditMetaBean editmeta = beanBuilderFactory.newEditMetaBeanBuilder()
                    .issue(issue)
                    .fieldsToInclude(fieldsToInclude)
                    .build();
            bean.editmeta(editmeta);
        }

        if (expand != null && expand.contains("changelog"))
        {
            bean.changelog(beanBuilderFactory.newChangelogBeanBuilder().build(issue));
        }
        return bean;
    }

    private void addTransitions(Issue issue, IssueBean bean)
    {
        if (isIncludeTransitions())
        {
            final List<ActionDescriptor> sortedAvailableActions = issueWorkflowManager.getSortedAvailableActions(issue, authContext.getUser());
            final List<TransitionBean> transitionBeans = Lists.newArrayListWithCapacity(sortedAvailableActions.size());
            for (ActionDescriptor action : sortedAvailableActions)
            {
                TransitionBean transitionMetaBean = beanBuilderFactory.newTransitionMetaBeanBuilder()
                        .issue(issue)
                        .action(action)
                        .build();
                transitionBeans.add(transitionMetaBean);
            }
            bean.setTransitionBeans(transitionBeans);
        }
    }

    private void addOpsbar(Issue issue, IssueBean bean)
    {
        if (isIncludeOpsbar())
        {
            OpsbarBeanBuilder opsbarBeanBuilder = beanBuilderFactory.newOpsbarBeanBuilder(issue);
            bean.setOperations(opsbarBeanBuilder.build());
        }
    }

    private void addParentLink(Issue issue, IssueBean bean)
    {
        final Long parentId = issue.getParentId();

        if (parentId != null)
        {
            IssueRefJsonBean parentLink = issueRefs.getIfPresent(parentId);
            if (parentLink == null)
            {
                parentLink = issueLinkBeanBuilderFactory.newIssueLinksBeanBuilder(issue).buildParentLink();
                if (parentLink != null)
                {
                    issueRefs.put(parentId, parentLink);
                }
            }

            if (parentLink != null)
            {
                bean.addParentField(parentLink, parentLinkName.get());
            }
        }
    }

    private void addFields(final Issue issue, final IssueBean bean)
    {
        // iterate over all the visible layout items from the field layout for this issue and attempt to add them
        // to the result
        final boolean includeRenderableFields = isIncludeRenderableFields();
        final FieldLayout layout = fieldLayoutManager.getFieldLayout(issue);
        final List<FieldLayoutItem> fieldLayoutItems = layout.getVisibleLayoutItems(issue.getProjectObject(), CollectionBuilder.list(issue.getIssueTypeId()));

        final Predicate<FieldLayoutItem> fieldLayoutItemIncluded = fieldsToInclude == null ? Predicates.<FieldLayoutItem>alwaysTrue() : Predicates.compose(fieldIncluded, GET_ORDERABLE_FIELD);

        for (final FieldLayoutItem fieldLayoutItem : Iterables.filter(fieldLayoutItems, fieldLayoutItemIncluded))
        {
            final OrderableField field = fieldLayoutItem.getOrderableField();
            final FieldJsonRepresentation fieldValue = getFieldValue(fieldLayoutItem, issue);
            if (fieldValue != null && fieldValue.getStandardData() != null)
            {
                bean.addField(fieldNames.getUnchecked(field), field, fieldValue, includeRenderableFields);
            }
        }

        // Then we try to add "NavigableFields" which aren't "OrderableFields" unless they ae special ones.
        // These aren't included in the Field Layout.
        // This is a bit crap because "getAvailableNavigableFields" doesn't take the issue into account.
        // All it means is the field is not hidden in at least one project the user has BROWSE permission on.
        for (NavigableField field : includedNavigableFields.get())
        {
            if (!bean.hasField(field.getId()))
            {
                if (!(field instanceof OrderableField) || isSpecialField(field))
                {
                    if (field instanceof RestAwareField)
                    {
                        addRestAwareField(issue, bean, field, (RestAwareField) field);
                    }
                }
            }
        }
    }

    /**
     * Returns tru if this is a special field that cannot appear in the field config for some special reason
     *
     * @param field the field
     * @return true if the field is special
     */
    private boolean isSpecialField(NavigableField field)
    {
        // At the moment only the Project System Field is special in this respect
        return field instanceof ProjectSystemField;
    }

    private void addRestAwareField(Issue issue, IssueBean bean, Field field, RestAwareField restAware)
    {
        final boolean includeRenderableFields = isIncludeRenderableFields();
        FieldJsonRepresentation fieldJsonFromIssue = restAware.getJsonFromIssue(issue, includeRenderableFields, null);
        if (fieldJsonFromIssue != null && fieldJsonFromIssue.getStandardData() != null)
        {
            bean.addField(fieldNames.getUnchecked(field), field, fieldJsonFromIssue, includeRenderableFields);
        }
    }

    private boolean isIncludeRenderableFields()
    {
        return (expand != null && expand.contains("renderedFields"));
    }

    private boolean isIncludeTransitions()
    {
        return (expand != null && expand.contains("transitions"));
    }

    private boolean isIncludeOpsbar()
    {
        return (expand != null && expand.contains("operations"));
    }

    FieldJsonRepresentation getFieldValue(final FieldLayoutItem fieldLayoutItem, final Issue issue)
    {
        final OrderableField field = fieldLayoutItem.getOrderableField();
        if (field instanceof RestAwareField)
        {
            RestAwareField restAware = (RestAwareField) field;
            try
            {
                return restAware.getJsonFromIssue(issue, isIncludeRenderableFields(), fieldLayoutItem);
            }
            catch (RuntimeException e)
            {
                if(LOG.isDebugEnabled())
                {   LOG.debug(String.format("Cannot get value from RestAwareField %s, exception: '%s' ", field.getId(),e.getMessage()),e);

                }else{
                    LOG.info(String.format("Cannot get value from RestAwareField %s, exception: '%s' ", field.getId(),e.getMessage()));
                }
                return null;
            }
        }
        else
        {
            LOG.info(String.format("OrderableField %s not rendered in JSON", field.getId()));
            return null;
        }
    }

    private static final Function<FieldLayoutItem, Field> GET_ORDERABLE_FIELD = new Function<FieldLayoutItem, Field>()
    {
        @Override
        public Field apply(final FieldLayoutItem fieldLayoutItem)
        {
            return fieldLayoutItem.getOrderableField();
        }
    };
}
