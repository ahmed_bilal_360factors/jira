package com.atlassian.jira.rest.v2.issue.watcher;

import java.util.List;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.watcher.WatcherService;
import com.atlassian.jira.bc.issue.watcher.WatchingDisabledException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.UserJsonBean;
import com.atlassian.jira.rest.v2.issue.IssueResource;
import com.atlassian.jira.rest.v2.issue.WatchersBean;
import com.atlassian.jira.rest.v2.issue.context.ContextUriInfo;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.util.lang.Pair;

import com.google.common.base.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static com.atlassian.jira.user.ApplicationUsers.toDirectoryUser;
import static com.google.common.collect.Lists.transform;

/**
 * Component for finding watchers in the REST plugin.
 *
 * @since v4.2
 */
public class WatchersFinder
{
    private final Logger log = LoggerFactory.getLogger(WatchersFinder.class);

    private final WatcherService watcherService;
    private final ContextUriInfo uriInfo;
    private final UserBeanFactory userBeanFactory;

    WatchersFinder(WatcherService watcherService, ContextUriInfo uriInfo, UserBeanFactory userBeanFactory)
    {
        this.watcherService = watcherService;
        this.uriInfo = uriInfo;
        this.userBeanFactory = userBeanFactory;
    }

    /**
     * Retrieves a WatchersBean for the given Issue on behalf of a remote user.
     *
     * @param issue an issue from which watchers are returned.
     * @param callingUser a user
     * @return a WatchersBean
     */
    public WatchersBean getWatchers(Issue issue, ApplicationUser callingUser)
    {
        return buildBean(issue, callingUser);
    }

    private WatchersBean buildBean(final Issue issue, final ApplicationUser callingUser)
    {
        try
        {
            Pair<Integer, List<User>> watcherCountList = watcherService.getWatchers(issue, toDirectoryUser(callingUser)).get();

            List<UserJsonBean> watcherUserBeans = transform(watcherCountList.second(), toJson(callingUser));

            log.trace("Visible watchers on issue '{}': {}", issue.getKey(), watcherUserBeans);

            return WatchersBean.Builder.create()
                    .self(selfLink(issue))
                    .isWatching(watcherCountList.second().contains(toDirectoryUser(callingUser)))
                    .watchers(watcherUserBeans)
                    .watchCount(watcherCountList.first())
                    .build();
        }
        catch (WatchingDisabledException e)
        {
            return null; // don't report any watcher info
        }

    }

    private Function<User, UserJsonBean> toJson(final ApplicationUser callingUser)
    {
        return new Function<User, UserJsonBean>()
        {
            @Override
            public UserJsonBean apply(final User user)
            {
                return userBeanFactory.createBean(ApplicationUsers.from(user), callingUser);
            }
        };
    }

    private String selfLink(final Issue issue)
    {
        return uriInfo.getBaseUriBuilder().path(IssueResource.class).path(IssueResource.class, "getIssueWatchers").build(issue.getKey()).toString();
    }
}
