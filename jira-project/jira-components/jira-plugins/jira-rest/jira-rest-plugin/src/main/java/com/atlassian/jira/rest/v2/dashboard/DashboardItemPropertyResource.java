package com.atlassian.jira.rest.v2.dashboard;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.gadgets.GadgetId;
import com.atlassian.gadgets.dashboard.DashboardService;
import com.atlassian.gadgets.dashboard.DashboardState;
import com.atlassian.gadgets.dashboard.PermissionException;
import com.atlassian.jira.bc.dashboard.DashboardItem;
import com.atlassian.jira.bc.dashboard.DashboardItemPropertyService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.issue.fields.rest.json.beans.EntityPropertyBeanSelfFunctions;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.rest.v2.entity.property.BasePropertyResource;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.base.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import static com.atlassian.fugue.Either.left;
import static com.atlassian.fugue.Either.right;
import static com.atlassian.jira.issue.fields.rest.json.beans.EntityPropertyBeanSelfFunctions.dashboardItemPropertySelfFunction;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@AnonymousAllowed
@Path ("dashboard/{dashboardId}/items/{itemId}/properties")
@Consumes ({ APPLICATION_JSON })
@Produces ({ APPLICATION_JSON })
public final class DashboardItemPropertyResource
{
    private final static Logger log = LoggerFactory.getLogger(DashboardItemPropertyResource.class);

    private final BasePropertyResource<DashboardItem> delegate;
    private final DashboardService dashboardService;
    private final JiraAuthenticationContext authenticationContext;
    private final I18nHelper i18nHelper;

    public DashboardItemPropertyResource(
            DashboardItemPropertyService dashboardItemPropertyService,
            DashboardService dashboardService,
            JiraAuthenticationContext authenticationContext,
            JiraBaseUrls jiraBaseUrls,
            I18nHelper i18n,
            I18nHelper i18nHelper)
    {
        this.i18nHelper = i18nHelper;
        this.dashboardService = dashboardService;
        this.authenticationContext = authenticationContext;
        this.delegate = new BasePropertyResource<DashboardItem>(dashboardItemPropertyService, authenticationContext, jiraBaseUrls, i18n,
                EntityPropertyBeanSelfFunctions.EMPTY, EntityPropertyType.DASHBOARD_ITEM_PROPERTY);
    }

    /**
     * Returns the keys of all properties for the dashboard item identified by the id.
     *
     * @param itemId the dashboard item from which keys will be returned.
     * @return a response containing EntityPropertiesKeysBean.
     *
     * @response.representation.200.qname
     *      dashboard-item-properties-keys
     * @response.representation.200.doc
     *      Returned if the dashboard item was found.
     * @response.representation.200.mediaType
     *      application/json
     * @response.representation.200.example
     *      {@link com.atlassian.jira.rest.v2.entity.property.EntityPropertyResourceExamples#GET_PROPERTIES_KEYS_RESPONSE_200}
     * @response.representation.400.doc
     *      Returned if the dashboard item id is invalid.
     * @response.representation.404.doc
     *      Returned if the dashboard item with given id does not exist or user does not have permissions to view it.
     */
    @GET
    public Response getPropertiesKeys(@PathParam ("dashboardId") final String dashboardId, @PathParam ("itemId") final String itemId)
    {
        return validateDashboardContainsItem(dashboardId, itemId).left().on(new Function<String, Response>()
        {
            @Override
            public Response apply(final String input)
            {
                return delegate.withSelfFunction(dashboardItemPropertySelfFunction(dashboardId)).getPropertiesKeys(itemId);
            }
        });
    }

    /**
     * Sets the value of the specified dashboard item's property.
     * <p>
     *     You can use this resource to store a custom data against the dashboard item identified by the id.
     *     The user who stores the data is required to have permissions to administer the dashboard item.
     * </p>
     *
     * @param itemId the dashboard item on which the property will be set.
     * @param propertyKey the key of the dashboard item's property. The maximum length of the key is 255 bytes.
     * @param request the request containing value of the dashboard item's property. The value has to a valid, non-empty JSON conforming
     *  to http://tools.ietf.org/html/rfc4627. The maximum length of the property value is 32768 bytes.
     *
     * @response.representation.200.doc
     *      Returned if the dashboard item property is successfully updated.
     * @response.representation.201.doc
     *      Returned if the dashboard item property is successfully created.
     * @response.representation.400.doc
     *      Returned if the dashboard item id is invalid.
     * @response.representation.403.doc
     *      Returned if the calling user does not have permission to administer the dashboard item.
     * @response.representation.404.doc
     *      Returned if the dashboard item with given id does not exist or user does not have permissions to view it.
     */
    @PUT
    @Path ("/{propertyKey}")
    public Response setProperty(@PathParam ("dashboardId") String dashboardId, @PathParam ("itemId") final String itemId,
            @PathParam ("propertyKey") final String propertyKey, @Context final HttpServletRequest request)
    {
        return validateDashboardContainsItem(dashboardId, itemId).left().on(new Function<String, Response>()
        {
            @Override
            public Response apply(final String input)
            {
                return delegate.setProperty(itemId, propertyKey, request);
            }
        });
    }

    /**
     * Returns the value of the property with a given key from the dashboard item identified by the id. 
     * The user who retrieves the property is required to have permissions to read the dashboard item.
     *
     * @param itemId the dashboard item from which the property will be returned.
     * @param propertyKey the key of the property to return.
     * @return a response containing {@link com.atlassian.jira.issue.fields.rest.json.beans.EntityPropertyBean}.
     *
     * @response.representation.200.qname
     *      dashboard item-property
     * @response.representation.200.doc
     *      Returned if the dashboard item property was found.
     * @response.representation.200.mediaType
     *      application/json
     * @response.representation.200.example
     *      {@link com.atlassian.jira.rest.v2.entity.property.EntityPropertyResourceExamples#GET_PROPERTY_RESPONSE_200}
     * @response.representation.400.doc
     *      Returned if the dashboard item id is invalid.
     * @response.representation.404.doc
     *      Returned if the dashboard item with given id does not exist or user does not have permissions to view it.
     */
    @GET
    @Path ("/{propertyKey}")
    public Response getProperty(@PathParam ("dashboardId") final String dashboardId, @PathParam ("itemId") final String itemId,
            @PathParam ("propertyKey") final String propertyKey)
    {
        return validateDashboardContainsItem(dashboardId, itemId).left().on(new Function<String, Response>()
        {
            @Override
            public Response apply(final String input)
            {
                return delegate.withSelfFunction(dashboardItemPropertySelfFunction(dashboardId)).getProperty(itemId, propertyKey);
            }
        });
    }

    /**
     * Removes the property from the dashboard item identified by the key or by the id. Ths user removing the property is required
     * to have permissions to administer the dashboard item.
     *
     * @param itemId the dashboard item from which the property will be removed.
     * @param propertyKey the key of the property to remove.
     *
     * @return a 204 HTTP status if everything goes well.
     *
     * @response.representation.204.doc
     *      Returned if the dashboard item property was removed successfully.
     * @response.representation.400.doc
     *      Returned if the dashboard item id is invalid.
     * @response.representation.403.doc
     *      Returned if the calling user does not have permission to edit the dashboard item.
     * @response.representation.404.doc
     *      Returned if the dashboard item with given id does not exist or user does not have permissions to view it.
     */
    @DELETE
    @Path ("/{propertyKey}")
    public Response deleteProperty(@PathParam ("dashboardId") String dashboardId, @PathParam ("itemId") final String itemId,
            @PathParam ("propertyKey") final String propertyKey)
    {
        return validateDashboardContainsItem(dashboardId, itemId).left().on(new Function<String, Response>()
        {
            @Override
            public Response apply(final String input)
            {
                return delegate.deleteProperty(itemId, propertyKey);
            }
        });
    }

    private Either<Response, String> validateDashboardContainsItem(String dashboardId, String itemId)
    {
        String userName = authenticationContext.getUser() != null ? authenticationContext.getUser().getUsername() : null;
        Either<Response, String> notFoundResponse = left(notFoundResponse(dashboardId, itemId));
        try
        {
            Option<DashboardState> dashboardForGadget = dashboardService.getDashboardForGadget(GadgetId.valueOf(itemId), userName);
            if (dashboardForGadget.isEmpty() || !dashboardForGadget.get().getId().value().equals(dashboardId))
            {
                return notFoundResponse;
            }
            return right(itemId);
        }
        catch (PermissionException ex)
        {
            return notFoundResponse;
        }
        catch (RuntimeException ex)
        {
            log.warn(String.format("unexpected exception when retrieving dashboard item %s in dashboard %s", itemId, dashboardId), ex);
            return notFoundResponse;
        }
    }

    private Response notFoundResponse(final String dashboardId, final String itemId)
    {
        return Response
                .status(Response.Status.NOT_FOUND)
                .entity(ErrorCollection.of(i18nHelper.getText("rest.dashboard.item.not.found", dashboardId, itemId)))
                .build();
    }
}
