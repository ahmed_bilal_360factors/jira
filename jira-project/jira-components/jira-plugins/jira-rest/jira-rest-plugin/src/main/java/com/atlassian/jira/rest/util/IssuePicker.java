package com.atlassian.jira.rest.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.core.Response;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.issue.search.IssuePickerResults;
import com.atlassian.jira.bc.issue.search.IssuePickerSearchService;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.rest.v2.issue.project.ProjectFinder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.DelimeterInserter;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.opensymphony.util.TextUtils;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.codehaus.jackson.annotate.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.bc.issue.search.IssuePickerSearchService.IssuePickerParameters;
import static com.atlassian.jira.bc.project.ProjectService.GetProjectResult;
import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;
import static javax.ws.rs.core.Response.ok;

@Component
public class IssuePicker
{
    private static final Logger log = LoggerFactory.getLogger(IssuePicker.class);
    private static final int PICKER_LIMIT = 20;

    private final IssueManager issueManager;
    private final ProjectFinder projectFinder;
    private final JiraAuthenticationContext authContext;
    private final ApplicationProperties applicationProperties;
    private final IssuePickerSearchService issuePickerSearchService;
    private final I18nHelper i18nHelper;

    @Autowired
    public IssuePicker(final IssueManager issueManager,
            final ProjectFinder projectFinder,
            final JiraAuthenticationContext authContext,
            final ApplicationProperties applicationProperties,
            final IssuePickerSearchService issuePickerSearchService,
            final I18nHelper i18nHelper)
    {
        this.issueManager = issueManager;
        this.projectFinder = projectFinder;
        this.authContext = authContext;
        this.applicationProperties = applicationProperties;
        this.issuePickerSearchService = issuePickerSearchService;
        this.i18nHelper = i18nHelper;
    }

    public Response getIssuesResponse(final Option<String> query,
            final String currentJQL,
            final String currentIssueKey,
            final String currentProjectId,
            final boolean showSubTasks,
            final boolean showSubTaskParent)
    {
        final String jql = query.fold(new Supplier<String>()
        {
            @Override
            public String get()
            {
                return null;
            }
        }, new Function<String, String>()
        {
            @Override
            public String apply(final String query)
            {
                return StringUtils.isEmpty(query) ? null : currentJQL;
            }
        });

        return ok(getIssues(query.getOrNull(), jql, currentIssueKey, currentProjectId, showSubTasks, showSubTaskParent))
                .cacheControl(NO_CACHE)
                .build();
    }

    private IssuePickerResult getIssues(final String query,
            final String currentJQL,
            final String currentIssueKey,
            final String currentProjectId,
            final boolean showSubTasks,
            final boolean showSubTaskParent)
    {
        final JiraServiceContext context = getServiceContext();
        final IssuePickerResult results = new IssuePickerResult();
        final ApplicationUser user = authContext.getUser();

        Issue currentIssue = null;

        if (StringUtils.isNotBlank(currentIssueKey))
        {
            currentIssue = issueManager.getIssueObject(currentIssueKey);
        }

        int limit = getLimit();

        final IssuePickerParameters pickerParameters =
                new IssuePickerParameters(query, currentJQL, currentIssue, getProject(currentProjectId, user).getOrNull(), showSubTasks, showSubTaskParent, limit);

        final Collection<IssuePickerResults> pickerResults = issuePickerSearchService.getResults(context, pickerParameters);
        for (IssuePickerResults pickerResult : pickerResults)
        {
            final Collection<Issue> issues = pickerResult.getIssues();
            final String labelKey = pickerResult.getLabel();
            final String id = pickerResult.getId();
            final String label = i18nHelper.getText(labelKey);
            if (!issues.isEmpty())
            {
                final IssueSection section = new IssueSection(id, label, i18nHelper.getText("jira.ajax.autocomplete.showing.x.of.y", Integer.toString(issues.size()), Integer.toString(pickerResult.getTotalIssues())), null);
                results.addSection(section);

                for (Issue issue : issues)
                {
                    section.addIssue(getIssue(issue, pickerResult));
                }
            }
            else
            {
                final IssueSection section = new IssueSection(id, label, null, i18nHelper.getText("jira.ajax.autocomplete.no.matching.issues"));
                results.addSection(section);
            }
        }

        return results;
    }

    private Option<Project> getProject(final String currentProjectId, final ApplicationUser user)
    {
        if (StringUtils.isNotBlank(currentProjectId))
        {
            GetProjectResult projectResult = projectFinder.getGetProjectForActionByIdOrKey(user, currentProjectId, ProjectAction.VIEW_ISSUES);
            return projectResult.isValid() ?
                    some(projectResult.getProject()) : none(Project.class);
        }
        return none(Project.class);
    }

    private int getLimit()
    {
        //Default limit to 20
        int limit = PICKER_LIMIT;

        try
        {
            limit = Integer.valueOf(applicationProperties.getDefaultBackedString(APKeys.JIRA_AJAX_AUTOCOMPLETE_LIMIT));
        }
        catch (NumberFormatException nfe)
        {
            log.error("jira.ajax.autocomplete.limit does not exist or is an invalid number in jira-application.properties.", nfe);
        }

        return limit;
    }

    /*
    * We use direct html instead of velocity to ensure the AJAX lookup is as fast as possible
    */
    private IssuePickerIssue getIssue(Issue issue, IssuePickerResults result)
    {
        DelimeterInserter delimeterInserter = new DelimeterInserter("<b>", "</b>");
        // Lets assume that anything that has one of the following one char before it, is a new word.
        delimeterInserter.setConsideredWhitespace("-_/\\,.+=&^%$#*@!~`'\":;<>");

        final String[] keysTerms = result.getKeyTerms().toArray(new String[result.getKeyTerms().size()]);
        final String[] summaryTerms = result.getSummaryTerms().toArray(new String[result.getSummaryTerms().size()]);

        final String issueKey = delimeterInserter.insert(TextUtils.htmlEncode(issue.getKey()), keysTerms);
        final String issueSummary = delimeterInserter.insert(TextUtils.htmlEncode(issue.getSummary()), summaryTerms);

        return new IssuePickerIssue(issue.getKey(), issueKey, getIconURI(option(issue.getIssueTypeObject())), issueSummary, issue.getSummary());
    }

    private String getIconURI(final Option<IssueType> issueTypeOption)
    {
        return issueTypeOption.fold(new Supplier<String>()
        {
            @Override
            public String get()
            {
                return StringUtils.EMPTY;
            }
        }, new Function<IssueType, String>()
        {
            @Override
            public String apply(final IssueType issueType)
            {
                return issueType.getIconUrl();
            }
        });
    }

    private JiraServiceContext getServiceContext()
    {
        final ApplicationUser user = authContext.getUser();
        final ErrorCollection errorCollection = new SimpleErrorCollection();
        return new JiraServiceContextImpl(user, errorCollection);
    }

    public static class IssuePickerResult
    {
        @JsonProperty
        private final List<IssueSection> sections = new ArrayList<IssueSection>();

        public void addSection(IssueSection section)
        {
            sections.add(section);
        }

        public List<IssueSection> getSections()
        {
            return sections;
        }
    }

    public static class IssueSection
    {
        @JsonProperty
        private final String label;
        @JsonProperty
        private final String sub;
        @JsonProperty
        private final String id;
        @JsonProperty
        private final String msg;
        @JsonProperty
        private final List<IssuePickerIssue> issues = new ArrayList<IssuePickerIssue>();

        public IssueSection(String id, String label, String sub, String msg)
        {
            this.label = label;
            this.sub = sub;
            this.id = id;
            this.msg = msg;
        }

        public void addIssue(IssuePickerIssue issue)
        {
            issues.add(issue);
        }

        public String getLabel()
        {
            return label;
        }

        public String getSub()
        {
            return sub;
        }

        public String getId()
        {
            return id;
        }

        public String getMsg()
        {
            return msg;
        }

        public List<IssuePickerIssue> getIssues()
        {
            return issues;
        }
    }


    public static class IssuePickerIssue
    {
        @JsonProperty
        private final String key;
        @JsonProperty
        private final String keyHtml;
        @JsonProperty
        private final String img;
        @JsonProperty
        private final String summary;
        @JsonProperty
        private final String summaryText;

        public IssuePickerIssue(String key, String keyHtml, String img, String summary, String summaryText)
        {
            this.key = key;
            this.keyHtml = keyHtml;
            this.img = img;
            this.summary = summary;
            this.summaryText = summaryText;
        }

        public String getKey()
        {
            return key;
        }

        public String getKeyHtml()
        {
            return keyHtml;
        }

        public String getImg()
        {
            return img;
        }

        public String getSummary()
        {
            return summary;
        }

        public String getSummaryText()
        {
            return summaryText;
        }
    }
}
