package com.atlassian.jira.rest.v2.issue.project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.jira.bc.projectroles.ProjectRoleService;
import com.atlassian.jira.rest.util.ResponseUtils;
import com.atlassian.jira.security.roles.DefaultRoleActors;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.util.SimpleErrorCollection;

/**
 * @since 6.4
 */
@Path ("role")
@Consumes ({ MediaType.APPLICATION_JSON })
@Produces ({ MediaType.APPLICATION_JSON })
public class RoleResource
{

    private final ProjectRoleService projectRoleService;
    private final ProjectRoleBeanFactory projectRoleBeanFactory;

    public RoleResource(final ProjectRoleService projectRoleService, final ProjectRoleBeanFactory projectRoleBeanFactory)
    {
        this.projectRoleService = projectRoleService;
        this.projectRoleBeanFactory = projectRoleBeanFactory;
    }

    /**
     * Get all the ProjectRoles available in JIRA. Currently this list is global.
     *
     * @return Returns full details of the roles available in JIRA.
     *
     * @request.representation.200.mediaType
     *      application/json
     * @response.representation.200.doc
     *      Returned if the user is authenticated
     * @response.representation.200.example
     *      {@link com.atlassian.jira.rest.v2.issue.project.ProjectRoleBean#ROLE_SIMPLE_EXAMPLE}
     * @response.representation.401.doc
     *      Returned if you do not have permissions or you are not logged in.
     */
    @GET
    public Response getProjectRoles()
    {
        final List<ProjectRoleBean> roles = new ArrayList<ProjectRoleBean>();
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        final Collection<ProjectRole> projectRoles = projectRoleService.getProjectRoles(errorCollection);

        ResponseUtils.throwIfError(errorCollection);

        for (ProjectRole projectRole : projectRoles)
        {
            final DefaultRoleActors actors = projectRoleService.getDefaultRoleActors(projectRole, errorCollection);
            ResponseUtils.throwIfError(errorCollection);
            roles.add(projectRoleBeanFactory.projectRole(projectRole, actors));
        }

        return Response.ok(roles).build();

    }

    /**
     * Get a specific ProjectRole available in JIRA.
     *
     * @return Returns full details of the role available in JIRA.
     *
     * @request.representation.200.mediaType
     *      application/json
     * @response.representation.200.doc
     *      Returned if the user is authenticated
     * @response.representation.200.example
     *      {@link com.atlassian.jira.rest.v2.issue.project.ProjectRoleBean#ROLE_SIMPLE_EXAMPLE}
     * @response.representation.401.doc
     *      Returned if you do not have permissions or you are not logged in.
     */
    @GET
    @Path ("{id}")
    public Response getProjectRolesById(@PathParam ("id") final long roleId)
    {
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        final ProjectRole projectRole = projectRoleService.getProjectRole(roleId, errorCollection);
        ResponseUtils.throwIfError(errorCollection);

        if(projectRole == null)
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        final DefaultRoleActors actors = projectRoleService.getDefaultRoleActors(projectRole, errorCollection);
        ResponseUtils.throwIfError(errorCollection);

        return Response.ok(projectRoleBeanFactory.projectRole(projectRole, actors)).build();
    }



}
