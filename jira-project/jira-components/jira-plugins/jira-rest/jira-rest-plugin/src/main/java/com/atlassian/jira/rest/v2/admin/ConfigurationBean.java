package com.atlassian.jira.rest.v2.admin;

import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;

import org.codehaus.jackson.annotate.JsonProperty;

import static com.atlassian.jira.rest.v2.admin.TimeTrackingConfigurationBean.TimeTrackingUnit;

public class ConfigurationBean
{
    public static ConfigurationBean DOC_EXAMPLE;
    static
    {
        DOC_EXAMPLE = new ConfigurationBeanBuilder().votingEnabled(true).watchingEnabled(true).unassignedIssuesAllowed(false).subTasksEnabled(false).attachmentsEnabled(true).issueLinkingEnabled(true).timeTrackingEnabled(true).timeTrackingConfiguration(new TimeTrackingConfigurationBean(8.0, 5, TimeTrackingConfiguration.TimeFormat.pretty, TimeTrackingUnit.day)).build();
    }

    @JsonProperty
    private boolean votingEnabled;
    @JsonProperty
    private boolean watchingEnabled;
    @JsonProperty
    private boolean unassignedIssuesAllowed;
    @JsonProperty
    private boolean subTasksEnabled;
    @JsonProperty
    private final boolean issueLinkingEnabled;
    @JsonProperty
    private boolean timeTrackingEnabled;
    @JsonProperty
    private boolean attachmentsEnabled;
    @JsonProperty
    private TimeTrackingConfigurationBean timeTrackingConfiguration;

    private ConfigurationBean(boolean votingEnabled,
            boolean watchingEnabled,
            boolean unassignedIssuesAllowed,
            boolean subTasksEnabled,
            boolean attachmentsEnabled,
            boolean issueLinkingEnabled,
            boolean timeTrackingEnabled,
            TimeTrackingConfigurationBean timeTrackingConfiguration)
    {
        this.votingEnabled = votingEnabled;
        this.watchingEnabled = watchingEnabled;
        this.unassignedIssuesAllowed = unassignedIssuesAllowed;
        this.subTasksEnabled = subTasksEnabled;
        this.issueLinkingEnabled = issueLinkingEnabled;
        this.timeTrackingEnabled = timeTrackingEnabled;
        this.attachmentsEnabled = attachmentsEnabled;
        this.timeTrackingConfiguration = timeTrackingConfiguration;
    }

    public boolean isVotingEnabled()
    {
        return votingEnabled;
    }

    public boolean isWatchingEnabled()
    {
        return watchingEnabled;
    }

    public boolean isUnassignedIssuesAllowed()
    {
        return unassignedIssuesAllowed;
    }

    public boolean isSubTasksEnabled()
    {
        return subTasksEnabled;
    }

    public boolean isTimeTrackingEnabled()
    {
        return timeTrackingEnabled;
    }

    public boolean isIssueLinkingEnabled()
    {
        return issueLinkingEnabled;
    }

    public boolean isAttachmentsEnabled()
    {
        return attachmentsEnabled;
    }

    public TimeTrackingConfigurationBean getTimeTrackingConfiguration()
    {
        return timeTrackingConfiguration;
    }

    public static ConfigurationBeanBuilder builer()
    {
        return new ConfigurationBeanBuilder();
    }

    public static class ConfigurationBeanBuilder
    {
        private boolean votingEnabled;
        private boolean watchingEnabled;
        private boolean unassignedIssuesAllowed;
        private boolean subTasksEnabled;
        private boolean attachmentsEnabled;
        private boolean issueLinkingEnabled;
        private boolean timeTrackingEnabled;
        private TimeTrackingConfigurationBean timeTrackingConfiguration;

        public ConfigurationBeanBuilder votingEnabled(final boolean votingEnabled)
        {
            this.votingEnabled = votingEnabled;
            return this;
        }

        public ConfigurationBeanBuilder watchingEnabled(final boolean watchingEnabled)
        {
            this.watchingEnabled = watchingEnabled;
            return this;
        }

        public ConfigurationBeanBuilder unassignedIssuesAllowed(final boolean unassignedIssuesAllowed)
        {
            this.unassignedIssuesAllowed = unassignedIssuesAllowed;
            return this;
        }

        public ConfigurationBeanBuilder subTasksEnabled(final boolean subTasksEnabled)
        {
            this.subTasksEnabled = subTasksEnabled;
            return this;
        }

        public ConfigurationBeanBuilder attachmentsEnabled(final boolean attachmentsEnabled)
        {
            this.attachmentsEnabled = attachmentsEnabled;
            return this;
        }

        public ConfigurationBeanBuilder issueLinkingEnabled(final boolean issueLinkingEnabled)
        {
            this.issueLinkingEnabled = issueLinkingEnabled;
            return this;
        }

        public ConfigurationBeanBuilder timeTrackingEnabled(final boolean timeTrackingEnabled)
        {
            this.timeTrackingEnabled = timeTrackingEnabled;
            return this;
        }

        public ConfigurationBeanBuilder timeTrackingConfiguration(final TimeTrackingConfigurationBean timeTrackingConfiguration)
        {
            this.timeTrackingConfiguration = timeTrackingConfiguration;
            return this;
        }

        public ConfigurationBean build()
        {
            return new ConfigurationBean(votingEnabled, watchingEnabled, unassignedIssuesAllowed, subTasksEnabled, attachmentsEnabled, issueLinkingEnabled, timeTrackingEnabled, timeTrackingConfiguration);
        }
    }
}
