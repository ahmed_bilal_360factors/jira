package com.atlassian.jira.rest.v2.issue.attachment;

import javax.annotation.Nullable;
import javax.ws.rs.core.Response;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.issue.attachment.AttachmentService;
import com.atlassian.jira.exception.AttachmentNotFoundException;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.rest.factory.ErrorCollectionFactory;
import com.atlassian.jira.rest.factory.JiraServiceContextFactory;
import com.atlassian.jira.rest.factory.Responder;
import com.atlassian.jira.rest.v2.issue.attachment.authorization.AttachmentAuthorizer;
import com.atlassian.jira.rest.v2.issue.attachment.operation.AttachmentOperation;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.jira.util.ErrorCollection.Reason.FORBIDDEN;
import static com.atlassian.jira.util.ErrorCollection.Reason.NOT_FOUND;

/**
 * Strategy pattern for processing attachments in a RESTful manner.
 *
 * @since v6.4
 */
class AttachmentResourceStrategy
{
    private static final Logger log = LoggerFactory.getLogger(AttachmentResourceStrategy.class);

    private final AttachmentAuthorizer authorizer;
    private final AttachmentOperation operation;
    private final Responder responder;
    private final ErrorCollectionFactory errorCollectionFactory;
    private final AttachmentManager attachmentManager;
    private final AttachmentService attachmentService;
    private final JiraAuthenticationContext authContext;
    private final I18nHelper i18n;
    private final JiraServiceContextFactory contextFactory;

    AttachmentResourceStrategy(
            final AttachmentAuthorizer authorization,
            final AttachmentOperation operation,
            final Responder responder,
            final ErrorCollectionFactory errorCollectionFactory,
            final AttachmentManager attachmentManager,
            final AttachmentService attachmentService,
            final JiraAuthenticationContext authContext,
            final I18nHelper i18n,
            final JiraServiceContextFactory contextFactory
    )
    {
        this.authorizer = authorization;
        this.operation = operation;
        this.responder = responder;
        this.errorCollectionFactory = errorCollectionFactory;
        this.attachmentManager = attachmentManager;
        this.attachmentService = attachmentService;
        this.authContext = authContext;
        this.i18n = i18n;
        this.contextFactory = contextFactory;
    }

    public Response processAttachment(final String id)
    {
        try
        {
            return tryProcessingAttachment(id);
        }
        catch (final AttachmentNotFoundException e)
        {
            return getNotFoundResponse(id);
        }
        catch (final NumberFormatException e)
        {
            return getNotFoundResponse(id);
        }
        catch (final Exception e)
        {
            log.error("Failed to process attachment with id=" + id, e);
            return responder.exception(e);
        }
    }

    private Response tryProcessingAttachment(final String id)
    {
        if (!attachmentManager.attachmentsEnabled())
        {
            final String message = i18n.getText("attachment.service.error.disabled");
            final ErrorCollection errors = errorCollectionFactory.of(NOT_FOUND, message);
            return responder.restfulErrors(errors);
        }

        @Nullable final ApplicationUser user = authContext.getUser();
        final JiraServiceContext serviceContext = contextFactory.createContext(user);
        final long attachmentId = Long.parseLong(id);
        final Attachment attachment = attachmentService.getAttachment(serviceContext, attachmentId);
        final com.atlassian.jira.util.ErrorCollection serviceErrors = serviceContext.getErrorCollection();

        if (serviceErrors.hasAnyErrors())
        {
            final ErrorCollection errors = errorCollectionFactory.of(serviceErrors);
            errors.reason(NOT_FOUND);
            return responder.restfulErrors(errors);
        }
        else if (authorizer.authorize(attachment, user))
        {
            return operation.perform(attachment, serviceContext);
        }
        else
        {
            final String message = authorizer.getNoPermissionMessage(i18n, id);
            final ErrorCollection errors = errorCollectionFactory.of(FORBIDDEN, message);
            return responder.restfulErrors(errors);
        }
    }

    private Response getNotFoundResponse(final String id)
    {
        final String message = i18n.getText("rest.attachment.error.not.found", id);
        final ErrorCollection errors = errorCollectionFactory.of(NOT_FOUND, message);
        return responder.restfulErrors(errors);
    }
}
