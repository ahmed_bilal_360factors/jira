package com.atlassian.jira.rest.v2.issue.attachment.operation;

import javax.ws.rs.core.Response;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.issue.attachment.Attachment;

/**
 * Performs an operation on an attachment.
 *
 * @since v6.4
 */
public interface AttachmentOperation
{
    /**
     * Performs an operation on an attachment in a given context.
     * @param attachment The attachment operated upon.
     * @param context Context for the operation. Is NOT used to pass errors.
     * @return Response with the result of the operation.
     */
    Response perform(Attachment attachment, JiraServiceContext context);
}
