package com.atlassian.jira.rest.v1.issues;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.fugue.Option;
import com.atlassian.jira.rest.util.IssuePicker;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

/**
 * Rest end point for IssuePicker searching
 *
 * @since v4.0
 */
@AnonymousAllowed
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class IssuePickerResource
{
    private final IssuePicker issuePicker;

    public IssuePickerResource(final IssuePicker issuePicker)
    {
        this.issuePicker = issuePicker;
    }

    @GET
    /**
     * This is the AJAX entry point to find issues given a query string.  The particluar instance of this call can "grey out" issues
     * by providing extra filterer parameters.
     *
     * @param query             what the user type in to search on
     * @param currentJQL        the JQL of the current Search.
     * @param currentIssueKey   the current issue or null
     * @param currentProjectId  the current project id or null
     * @param showSubTasks      set to false if sub tasks should be greyed out
     * @param showSubTaskParent set to false to have parent issue greyed out
     * @return a Response containing a list of {@link com.atlassian.jira.rest.v1.issues.IssuePickerResource.IssueSection} containing matching issues
     */
    public Response getIssuesResponse(@QueryParam("query") final String query,
                                      @QueryParam("currentJQL") final String currentJQL,
                                      @QueryParam("currentIssueKey") final String currentIssueKey,
                                      @QueryParam("currentProjectId") final String currentProjectId,
                                      @QueryParam("showSubTasks") final boolean showSubTasks,
                                      @QueryParam("showSubTaskParent") final boolean showSubTaskParent)
    {
        return issuePicker.getIssuesResponse(Option.option(query), currentJQL, currentIssueKey,currentProjectId, showSubTasks, showSubTaskParent);
    }

}
