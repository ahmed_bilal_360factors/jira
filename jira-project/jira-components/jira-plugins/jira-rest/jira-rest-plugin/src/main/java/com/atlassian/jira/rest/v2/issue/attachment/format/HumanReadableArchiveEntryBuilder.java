package com.atlassian.jira.rest.v2.issue.attachment.format;

import java.net.URI;

/**
 * Builds a {@link com.atlassian.jira.rest.v2.issue.attachment.format.HumanReadableArchiveEntry}
 *
 * @since v6.4
 */
public class HumanReadableArchiveEntryBuilder
{
    private String path;
    private long index;
    private String size;
    private String mediaType;
    private URI icon;
    private String alternativeText;
    private String label;

    public HumanReadableArchiveEntryBuilder path(final String path)
    {
        this.path = path;
        return this;
    }

    public HumanReadableArchiveEntryBuilder index(final long index)
    {
        this.index = index;
        return this;
    }

    public HumanReadableArchiveEntryBuilder size(final String size)
    {
        this.size = size;
        return this;
    }

    public HumanReadableArchiveEntryBuilder mediaType(final String mediaType)
    {
        this.mediaType = mediaType;
        return this;
    }

    public HumanReadableArchiveEntryBuilder icon(final URI icon)
    {
        this.icon = icon;
        return this;
    }

    public HumanReadableArchiveEntryBuilder alternativeText(final String alternativeText)
    {
        this.alternativeText = alternativeText;
        return this;
    }

    public HumanReadableArchiveEntryBuilder label(final String label)
    {
        this.label = label;
        return this;
    }

    public HumanReadableArchiveEntry build()
    {
        return new HumanReadableArchiveEntry(
                path,
                index,
                size,
                mediaType,
                icon,
                alternativeText,
                label
        );
    }
}
