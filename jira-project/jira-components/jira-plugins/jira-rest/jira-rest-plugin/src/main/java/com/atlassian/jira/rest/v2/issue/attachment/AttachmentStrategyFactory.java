package com.atlassian.jira.rest.v2.issue.attachment;

import com.atlassian.jira.bc.issue.attachment.AttachmentService;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.rest.factory.ErrorCollectionFactory;
import com.atlassian.jira.rest.factory.JiraServiceContextFactory;
import com.atlassian.jira.rest.factory.Responder;
import com.atlassian.jira.rest.v2.issue.attachment.authorization.AttachmentAuthorizer;
import com.atlassian.jira.rest.v2.issue.attachment.authorization.AttachmentAuthorizerFactory;
import com.atlassian.jira.rest.v2.issue.attachment.format.ArchiveFormatterFactory;
import com.atlassian.jira.rest.v2.issue.attachment.operation.AttachmentOperation;
import com.atlassian.jira.rest.v2.issue.attachment.operation.AttachmentOperationFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Root factory for strategies related to attachments.
 *
 * @since v6.4
 */
@ExportAsService
@Component
public class AttachmentStrategyFactory
{
    private Responder responder;
    private ErrorCollectionFactory errorCollectionFactory;
    private AttachmentManager attachmentManager;
    private AttachmentService attachmentService;
    private JiraAuthenticationContext authContext;
    private I18nHelper i18n;
    private JiraServiceContextFactory contextFactory;
    private AttachmentAuthorizerFactory authorizerFactory;
    private ArchiveFormatterFactory formatterFactory;
    private AttachmentOperationFactory operationFactory;

    @SuppressWarnings ("unused")
    private AttachmentStrategyFactory()
    {

    }

    @Autowired
    @SuppressWarnings ("unused")
    private AttachmentStrategyFactory(
            final Responder responder,
            final ErrorCollectionFactory errorCollectionFactory,
            final AttachmentManager attachmentManager,
            final AttachmentService attachmentService,
            final JiraAuthenticationContext authContext,
            final I18nHelper i18n,
            final JiraServiceContextFactory contextFactory,
            final AttachmentAuthorizerFactory authorizerFactory,
            final ArchiveFormatterFactory formatterFactory,
            final AttachmentOperationFactory operationFactory
    )
    {
        this.responder = responder;
        this.errorCollectionFactory = errorCollectionFactory;
        this.attachmentManager = attachmentManager;
        this.attachmentService = attachmentService;
        this.authContext = authContext;
        this.i18n = i18n;
        this.contextFactory = contextFactory;
        this.authorizerFactory = authorizerFactory;
        this.formatterFactory = formatterFactory;
        this.operationFactory = operationFactory;
    }

    public AttachmentResourceStrategy resourceStrategy(
            final AttachmentAuthorizer authorizer,
            final AttachmentOperation operation
    )
    {
        return new AttachmentResourceStrategy(
                authorizer,
                operation,
                responder,
                errorCollectionFactory,
                attachmentManager,
                attachmentService,
                authContext,
                i18n,
                contextFactory
        );
    }

    public AttachmentOperationFactory operation()
    {
        return operationFactory;
    }

    public ArchiveFormatterFactory formatter()
    {
        return formatterFactory;
    }

    public AttachmentAuthorizerFactory authorizer()
    {
        return authorizerFactory;
    }
}
