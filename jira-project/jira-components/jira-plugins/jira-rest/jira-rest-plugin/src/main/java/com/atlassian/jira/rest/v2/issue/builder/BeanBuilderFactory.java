package com.atlassian.jira.rest.v2.issue.builder;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.link.RemoteIssueLink;
import com.atlassian.jira.rest.v2.issue.attachment.format.AttachmentBeanBuilder;
import com.atlassian.jira.rest.v2.issue.CreateMetaBeanBuilder;
import com.atlassian.jira.rest.v2.issue.EditMetaBeanBuilder;
import com.atlassian.jira.rest.v2.issue.IncludedFields;
import com.atlassian.jira.rest.v2.issue.IssueBeanBuilder;
import com.atlassian.jira.rest.v2.issue.IssueBeanBuilder2;
import com.atlassian.jira.rest.v2.issue.OpsbarBeanBuilder;
import com.atlassian.jira.rest.v2.issue.RemoteIssueLinkBeanBuilder;
import com.atlassian.jira.rest.v2.issue.TransitionMetaBeanBuilder;
import com.atlassian.jira.rest.v2.search.FilterBeanBuilder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.UriBuilder;

/**
 * Factory interface for getting instances of
 *
 * @since v4.2
 */
public interface BeanBuilderFactory
{
    /**
     * Returns a new instance of an AttachmentBeanBuilder.
     *
     * @return a AttachmentBeanBuilder
     * @param attachment
     */
    AttachmentBeanBuilder newAttachmentBeanBuilder(Attachment attachment);

    /**
     * Returns a new instance of an IssueBeanBuilder.
     *
     * @return an IssueBeanBuilder
     * @param include
     * @param expand
     */
    IssueBeanBuilder2 newIssueBeanBuilder2(@Nullable IncludedFields include, @Nullable String expand);

    /**
     * Returns a new instance of an IssueBeanBuilder.
     *
     * @return an IssueBeanBuilder
     * @param include
     * @param expand
     * @param baseUriBuilder
     */
    IssueBeanBuilder2 newIssueBeanBuilder2(@Nullable IncludedFields include, @Nullable String expand, @Nonnull UriBuilder baseUriBuilder);

    /**
     * Returns a new instance of an IssueBeanBuilder.
     *
     * @return an IssueBeanBuilder
     * @param include
     * @deprecated Use {@link #newIssueBeanBuilder2(com.atlassian.jira.rest.v2.issue.IncludedFields, java.lang.String)}. Since v6.4.
     */
    @Deprecated
    IssueBeanBuilder newIssueBeanBuilder(@Nonnull Issue issue, @Nullable IncludedFields include);

    /**
     * Returns a new instance of a CreateMetaBeanBuilder.
     *
     * @return a CreateMetaBeanBuilder
     */
    CreateMetaBeanBuilder newCreateMetaBeanBuilder();

    /**
     * Returns a new instance of a EditMetaBeanBuilder.
     *
     * @return a EditMetaBeanBuilder
     */
    EditMetaBeanBuilder newEditMetaBeanBuilder();

    /**
     * Returns a new instance of a TransitionMetaBeanBuilder.
     *
     * @return a TransitionMetaBeanBuilder
     */
    TransitionMetaBeanBuilder newTransitionMetaBeanBuilder();

    OpsbarBeanBuilder newOpsbarBeanBuilder(final Issue issue);

    /**
     * Returns a new instance of a RemoteIssueLinkBeanBuilder.
     *
     * @param remoteIssueLink
     * @return a RemoteIssueLinkBeanBuilder
     */
    RemoteIssueLinkBeanBuilder newRemoteIssueLinkBeanBuilder(RemoteIssueLink remoteIssueLink);

    ChangelogBeanBuilder newChangelogBeanBuilder();

    /**
     * Returns a new instance of a FilterBeanBuilder.
     *
     * @return a FilterBeanBuilder
     */
    FilterBeanBuilder newFilterBeanBuilder();
}
