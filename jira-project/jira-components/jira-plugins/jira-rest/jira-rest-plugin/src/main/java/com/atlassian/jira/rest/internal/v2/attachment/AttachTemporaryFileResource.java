package com.atlassian.jira.rest.internal.v2.attachment;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.core.util.FileSize;
import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.AttachmentError;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachment;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachmentManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.rest.internal.common.bean.AttachTemporaryFileBadResultBean;
import com.atlassian.jira.rest.internal.common.bean.AttachTemporaryFileGoodResultBean;
import com.atlassian.jira.rest.util.AttachmentHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.SecureUserTokenManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.CorsAllowed;

import com.google.common.base.Function;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import webwork.config.Configuration;

import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static com.google.common.collect.Iterables.getFirst;

@Path ("AttachTemporaryFile")
@Produces (MediaType.APPLICATION_JSON)
@AnonymousAllowed
@CorsAllowed
public class AttachTemporaryFileResource
{
    private static final Logger log = Logger.getLogger(AttachTemporaryFileResource.class);

    private final JiraAuthenticationContext authContext;
    private final TemporaryWebAttachmentManager temporaryWebAttachmentManager;
    private final IssueService issueService;
    private final ProjectService projectService;
    private final XsrfTokenGenerator xsrfGenerator;
    private final AttachmentHelper attachmentHelper;

    private final SecureUserTokenManager secureUserTokenManager;

    public AttachTemporaryFileResource(final JiraAuthenticationContext authContext,
            final TemporaryWebAttachmentManager temporaryWebAttachmentManager, final IssueService issueService,
            final ProjectService projectService, final XsrfTokenGenerator xsrfGenerator,
            final AttachmentHelper attachmentHelper, final SecureUserTokenManager secureUserTokenManager)
    {
        this.authContext = authContext;
        this.temporaryWebAttachmentManager = temporaryWebAttachmentManager;
        this.issueService = issueService;
        this.projectService = projectService;
        this.xsrfGenerator = xsrfGenerator;
        this.attachmentHelper = attachmentHelper;
        this.secureUserTokenManager = secureUserTokenManager;
    }

    @POST
    @Consumes (MediaType.WILDCARD)
    @Path ("/secure")
    public Response addTemporaryAttachment(@QueryParam ("filename") final String filename,
            @QueryParam ("projectId") final Long projectId, @QueryParam ("issueId") final Long issueId,
            @QueryParam ("size") final Long size, @QueryParam ("secureToken") final String secureToken,
            @QueryParam ("formToken") final String formToken, @Context final HttpServletRequest request)
    {
        try
        {
            if (secureToken == null)
            {
                return Response.status(Response.Status.BAD_REQUEST).cacheControl(never()).build();
            }

            final ApplicationUser secureUser = ApplicationUsers.from(
                    secureUserTokenManager.useToken(secureToken, SecureUserTokenManager.TokenType.SCREENSHOT));

            if (secureUser == null)
            {
                return Response.status(Response.Status.UNAUTHORIZED).cacheControl(never()).build();
            }

            authContext.setLoggedInUser(secureUser);
        }
        catch (final RuntimeException e)
        {

            quietlyCloseInputStream(request);
            throw e;
        }

        return addTemporaryAttachment(filename, projectId, issueId, size, formToken, request);
    }

    @POST
    @Consumes (MediaType.WILDCARD)
    public Response addTemporaryAttachment(@QueryParam ("filename") final String filename,
            @QueryParam ("projectId") final Long projectId, @QueryParam ("issueId") final Long issueId,
            @QueryParam ("size") final Long size, @QueryParam ("formToken") final String formToken,
            @Context final HttpServletRequest request)
    {
        try
        {
            if (formToken == null)
            {
                return createError(Response.Status.BAD_REQUEST, getI18n().getText(
                        "rest.common.missing.required.param", "formToken"));
            }

            final AttachmentHelper.ValidationResult validationResult = attachmentHelper.validate(request, filename, size);

            if (!validationResult.isValid())
            {
                switch (validationResult.getErrorType())
                {
                    case ATTACHMENT_TO_LARGE:
                    {
                        final String message = getI18n().getText("upload.too.big", filename,
                                FileSize.format(validationResult.getSize()),
                                FileSize.format(new Long(Configuration.getString(APKeys.JIRA_ATTACHMENT_SIZE))));
                        return createError(Response.Status.BAD_REQUEST, message);
                    }
                    case ATTACHMENT_IO_SIZE:
                    {
                        final String message = getI18n().getText("attachfile.error.io.size", filename);
                        return createError(Response.Status.BAD_REQUEST, message);
                    }
                    case ATTACHMENT_IO_UNKNOWN:
                    {
                        final String message = getI18n().getText(
                                "attachfile.error.io.error", filename, validationResult.getErrorMessage());

                        return createError(Response.Status.INTERNAL_SERVER_ERROR, message);
                    }
                    case FILENAME_BLANK:
                        return Response.status(Response.Status.BAD_REQUEST).cacheControl(never()).build();
                    case XSRF_TOKEN_INVALID:
                        return createTokenError(xsrfGenerator.generateToken(request));
                    default:
                        log.error("Got unknown validation error: " + validationResult.getErrorType());
                        return createError(Response.Status.INTERNAL_SERVER_ERROR,
                                getI18n().getText("attachment.error.unknown", filename));
                }
            }

            if (issueId == null && projectId == null)
            {
                return Response.status(Response.Status.BAD_REQUEST).cacheControl(never()).build();
            }

            final ApplicationUser user = authContext.getUser();
            final Either<Issue, Project> target = getAttachmentTarget(projectId, issueId, user);

            final Either<AttachmentError, TemporaryWebAttachment> createResult = temporaryWebAttachmentManager
                    .createTemporaryWebAttachment(validationResult.getInputStream(), filename,
                            validationResult.getContentType(), validationResult.getSize(), target, formToken, user);

            return createResult.fold(new Function<AttachmentError, Response>()
            {
                @Override
                public Response apply(final AttachmentError error)
                {
                    return createError(Response.Status.INTERNAL_SERVER_ERROR, error.getLocalizedMessage());
                }
            }, new Function<TemporaryWebAttachment, Response>()
            {
                @Override
                public Response apply(final TemporaryWebAttachment attachment)
                {
                    return Response
                            .status(Response.Status.CREATED)
                            .entity(new AttachTemporaryFileGoodResultBean(attachment.getStringId(), filename))
                            .cacheControl(never()).build();
                }
            });
        }
        finally
        {
            quietlyCloseInputStream(request);
        }
    }

    private I18nHelper getI18n()
    {
        return authContext.getI18nHelper();
    }

    private void quietlyCloseInputStream(final HttpServletRequest request)
    {
        try
        {
            IOUtils.closeQuietly(request.getInputStream());
        }
        catch (final Exception ignore)
        {
        }
    }

    private Either<Issue, Project> getAttachmentTarget(final Long projectId, final Long issueId, final ApplicationUser user)
    {
        if (issueId == null)
        {
            return Either.right(getProject(user, projectId));
        }
        else
        {
            return Either.left(getIssue(user, issueId));
        }
    }

    private Issue getIssue(final ApplicationUser user, final Long id)
    {
        return throw404WhenInvalid(issueService.getIssue(user, id)).getIssue();
    }

    private <T extends ServiceResult> T throw404WhenInvalid(final T serviceResult)
    {
        if (!serviceResult.isValid())
        {
            throw new WebApplicationException(createError(Response.Status.NOT_FOUND, serviceResult.getErrorCollection()));
        }
        return serviceResult;
    }

    private Project getProject(final ApplicationUser user, final Long id)
    {
        return throw404WhenInvalid(projectService.getProjectById(user, id)).getProject();
    }

    private static Response createError(final Response.Status status, final com.atlassian.jira.util.ErrorCollection collection)
    {
        String message = getFirst(collection.getErrorMessages(), null);
        if (message == null)
        {
            message = getFirst(collection.getErrors().values(), null);
        }
        return createError(status, message);
    }

    private static Response createError(final Response.Status status, final String message)
    {
        return Response.status(status).cacheControl(never()).entity(new AttachTemporaryFileBadResultBean(message)).build();
    }

    private Response createTokenError(final String newToken)
    {
        final String message = getI18n().getText("attachfile.xsrf.try.again");
        return Response.status(Response.Status.BAD_REQUEST)
                .cacheControl(never()).entity(new AttachTemporaryFileBadResultBean(message, newToken)).build();
    }
}
