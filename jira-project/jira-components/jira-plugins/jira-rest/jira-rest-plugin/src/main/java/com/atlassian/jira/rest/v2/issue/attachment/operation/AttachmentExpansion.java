package com.atlassian.jira.rest.v2.issue.attachment.operation;

import javax.ws.rs.core.Response;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.issue.AttachmentIndexManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.plugin.attachment.AttachmentArchive;
import com.atlassian.jira.rest.factory.Responder;
import com.atlassian.jira.rest.v2.issue.attachment.AttachmentConfiguration;
import com.atlassian.jira.rest.v2.issue.attachment.format.ArchiveFormatter;

import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static javax.ws.rs.core.Response.Status.OK;

/**
 * Tries to expand an attachment.
 *
 * @since v6.4
 */
final class AttachmentExpansion implements AttachmentOperation
{
    private final AttachmentIndexManager attachmentIndexManager;
    private final Responder responder;
    private final AttachmentConfiguration configuration;
    private final ArchiveFormatter entityFormatter;

    AttachmentExpansion(
            final AttachmentIndexManager attachmentIndexManager,
            final Responder responder,
            final AttachmentConfiguration configuration,
            final ArchiveFormatter entityFormatter
    )
    {
        this.attachmentIndexManager = attachmentIndexManager;
        this.responder = responder;
        this.configuration = configuration;
        this.entityFormatter = entityFormatter;
    }

    /**
     * {@inheritDoc}
     * @return 200 OK with expanded contents or 409 Conflict if the archive format is not supported
     */
    @Override
    public Response perform(final Attachment attachment, final JiraServiceContext context)
    {
        final Option<AttachmentArchive> contents = tryToExpand(attachment);
        if (contents.isDefined())
        {
            final AttachmentArchive archive = contents.get();
            final Object entity = entityFormatter.format(archive, attachment);
            return responder
                    .builder(OK)
                    .entity(entity)
                    .build();
        }
        else
        {
            return responder
                    .builder(CONFLICT)
                    .build();
        }
    }

    private Option<AttachmentArchive> tryToExpand(final Attachment attachment)
    {
        final Issue issue = attachment.getIssue();
        final int maxEntries = configuration.getMaxEntriesToExpand();
        return attachmentIndexManager.getAttachmentContents(attachment, issue, maxEntries);
    }
}
