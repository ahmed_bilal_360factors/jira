package com.atlassian.jira.rest.v2.index;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.atlassian.jira.index.request.ReindexRequest;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.ReindexStatus;
import com.atlassian.jira.rest.bind.DateTimeAdapter;

/**
* @since 6.4
*/
@XmlRootElement
public class ReindexRequestBean
{
    @XmlElement
    private long id;

    @XmlElement
    private ReindexStatus status;

    @XmlElement
    private ReindexRequestType type;

    @XmlJavaTypeAdapter (DateTimeAdapter.class)
    private Date requestTime;

    @XmlJavaTypeAdapter (DateTimeAdapter.class)
    private Date startTime;

    @XmlJavaTypeAdapter (DateTimeAdapter.class)
    private Date completionTime;

    public ReindexRequestBean()
    {
    }

    public ReindexRequestBean(ReindexRequest request)
    {
        this.id = request.getId();
        this.status = request.getStatus();
        this.requestTime = new Date(request.getRequestTime());
        this.startTime = (request.getStartTime() == null ? null : new Date(request.getStartTime()));
        this.completionTime = (request.getCompletionTime() == null ? null : new Date(request.getCompletionTime()));
        this.type = request.getType();
    }

    final static ReindexRequestBean DOC_EXAMPLE = new ReindexRequestBean();
    static
    {
        DOC_EXAMPLE.id = 10500;
        DOC_EXAMPLE.status = ReindexStatus.PENDING;
        DOC_EXAMPLE.type = ReindexRequestType.IMMEDIATE;
        DOC_EXAMPLE.requestTime = new Date();
        DOC_EXAMPLE.startTime = null;
        DOC_EXAMPLE.completionTime = null;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public ReindexStatus getStatus()
    {
        return status;
    }

    public void setStatus(ReindexStatus status)
    {
        this.status = status;
    }

    public ReindexRequestType getType()
    {
        return type;
    }

    public void setType(ReindexRequestType type)
    {
        this.type = type;
    }

    public Date getRequestTime()
    {
        return requestTime;
    }

    public void setRequestTime(Date requestTime)
    {
        this.requestTime = requestTime;
    }

    public Date getStartTime()
    {
        return startTime;
    }

    public void setStartTime(Date startTime)
    {
        this.startTime = startTime;
    }

    public Date getCompletionTime()
    {
        return completionTime;
    }

    public void setCompletionTime(Date completionTime)
    {
        this.completionTime = completionTime;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof ReindexRequestBean))
        {
            return false;
        }

        ReindexRequestBean that = (ReindexRequestBean) o;

        if (id != that.id)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        return (int) (id ^ (id >>> 32));
    }
}
