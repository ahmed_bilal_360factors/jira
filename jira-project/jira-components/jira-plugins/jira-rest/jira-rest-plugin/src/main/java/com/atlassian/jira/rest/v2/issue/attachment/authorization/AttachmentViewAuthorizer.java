package com.atlassian.jira.rest.v2.issue.attachment.authorization;

import javax.annotation.Nullable;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

/**
 * Decides if a user can view an attachment.
 *
 * @since v6.4
 */
final class AttachmentViewAuthorizer implements AttachmentAuthorizer
{

    private final PermissionManager permissionManager;

    AttachmentViewAuthorizer(final PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    @Override
    public boolean authorize(final Attachment attachment, @Nullable final ApplicationUser user)
    {
        final Issue issue = attachment.getIssue();
        return permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, issue, user);
    }

    @Override
    public String getNoPermissionMessage(final I18nHelper i18n, final Object attachmentId)
    {
        return i18n.getText("attachment.service.error.view.no.permission", attachmentId);
    }
}
