package com.atlassian.jira.rest.v2.issue.attachment.operation;

import com.atlassian.jira.bc.issue.attachment.AttachmentService;
import com.atlassian.jira.issue.AttachmentIndexManager;
import com.atlassian.jira.rest.factory.Responder;
import com.atlassian.jira.rest.v2.issue.attachment.AttachmentConfiguration;
import com.atlassian.jira.rest.v2.issue.attachment.format.ArchiveFormatter;
import com.atlassian.jira.rest.v2.issue.builder.BeanBuilderFactory;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Creates {@link com.atlassian.jira.rest.v2.issue.attachment.operation.AttachmentOperation} instances.
 *
 * @since v6.4
 */
@ExportAsService
@Component
public class AttachmentOperationFactory
{
    private Responder responder;
    private AttachmentService attachmentService;
    private BeanBuilderFactory beanBuilderFactory;
    private AttachmentIndexManager attachmentIndexManager;
    private AttachmentConfiguration configuration;

    @SuppressWarnings ("unused")
    private AttachmentOperationFactory()
    {

    }

    @Autowired
    @SuppressWarnings ("unused")
    private AttachmentOperationFactory(
            final Responder responder,
            final AttachmentService attachmentService,
            final BeanBuilderFactory beanBuilderFactory,
            @ComponentImport final AttachmentIndexManager attachmentIndexManager,
            final AttachmentConfiguration configuration
    )
    {
        this.responder = responder;
        this.attachmentService = attachmentService;
        this.beanBuilderFactory = beanBuilderFactory;
        this.attachmentIndexManager = attachmentIndexManager;
        this.configuration = configuration;
    }

    public AttachmentOperation removal()
    {
        return new AttachmentRemoval(attachmentService, responder);
    }

    public AttachmentOperation view()
    {
        return new AttachmentView(responder, beanBuilderFactory);
    }

    public AttachmentOperation expansion(final ArchiveFormatter<?> formatter)
    {
        return new AttachmentExpansion(
                attachmentIndexManager,
                responder,
                configuration,
                formatter
        );
    }

}
