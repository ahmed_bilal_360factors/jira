package com.atlassian.jira.rest.v2.issue.attachment.format;

import java.net.URI;

import javax.annotation.concurrent.Immutable;

import com.google.common.base.Objects;

import org.codehaus.jackson.annotate.JsonAutoDetect;

/**
 * Human-readable representation of an archive entry.
 *
 * @since v6.4
 */
@JsonAutoDetect
@Immutable
public class HumanReadableArchiveEntry
{
    private final String path;
    private final long index;
    private final String size;
    private final String mediaType;
    private final URI icon;
    private final String alternativeText;
    private final String label;

    public HumanReadableArchiveEntry(
            final String path,
            final long index,
            final String size,
            final String mediaType,
            final URI icon,
            final String alternativeText,
            final String label
    )
    {
        this.path = path;
        this.index = index;
        this.size = size;
        this.mediaType = mediaType;
        this.icon = icon;
        this.alternativeText = alternativeText;
        this.label = label;
    }

    public String getPath()
    {
        return path;
    }

    public long getIndex()
    {
        return index;
    }

    public String getSize()
    {
        return size;
    }

    public String getMediaType()
    {
        return mediaType;
    }

    public URI getIcon()
    {
        return icon;
    }

    public String getAlternativeText()
    {
        return alternativeText;
    }

    public String getLabel()
    {
        return label;
    }

    @Override
    public int hashCode() {return Objects.hashCode(path, index, size, mediaType, icon, alternativeText, label);}

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()) {return false;}
        final HumanReadableArchiveEntry other = (HumanReadableArchiveEntry) obj;
        return Objects.equal(this.path, other.path)
                && Objects.equal(this.index, other.index)
                && Objects.equal(this.size, other.size)
                && Objects.equal(this.mediaType, other.mediaType)
                && Objects.equal(this.icon, other.icon)
                && Objects.equal(this.alternativeText, other.alternativeText)
                && Objects.equal(this.label, other.label);
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this)
                .add("path", path)
                .add("index", index)
                .add("size", size)
                .add("mediaType", mediaType)
                .add("icon", icon)
                .add("alternativeText", alternativeText)
                .add("label", label)
                .toString();
    }
}
