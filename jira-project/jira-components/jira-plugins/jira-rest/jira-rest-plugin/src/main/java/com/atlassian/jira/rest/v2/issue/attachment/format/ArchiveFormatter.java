package com.atlassian.jira.rest.v2.issue.attachment.format;

import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.plugin.attachment.AttachmentArchive;

/**
 * Formats an attachment archive.
 *
 * @param <T> Output format
 * @since v6.4
 */
public interface ArchiveFormatter<T>
{
    /**
     * @param archive Archive to be formatted.
     * @param attachment Attachment which represents the archive.
     * @return Formatted archive.
     */
    T format(AttachmentArchive archive, Attachment attachment);
}
