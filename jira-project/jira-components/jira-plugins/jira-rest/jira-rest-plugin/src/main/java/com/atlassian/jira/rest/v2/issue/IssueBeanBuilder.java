package com.atlassian.jira.rest.v2.issue;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.rest.v2.issue.builder.BeanBuilderFactory;

import javax.ws.rs.core.UriBuilder;

/**
 * Builder for {@link IssueBean} instances.
 *
 * @since v5.0
 */
public class IssueBeanBuilder
{
    private final BeanBuilderFactory beanBuilderFactory;

    /**
     * The list of fields to include in the bean. If null, include all fields.
     */
    private IncludedFields fieldsToInclude;

    /**
     * the expand query string
     */
    private String expand;

    /**
     * The UriInfo to use when generating links. Will use ContextUriInfo unless one is explicitly specified via {@link
     * #uriBuilder(javax.ws.rs.core.UriBuilder)}
     */
    private UriBuilder uriBuilder;

    /**
     * The issue for which to build the bean.
     */
    private final Issue issue;

    private IssueBeanBuilder2 beanBuilder2;

    public IssueBeanBuilder(final BeanBuilderFactory beanBuilderFactory, final IncludedFields include, final Issue issue)
    {
        this.beanBuilderFactory = beanBuilderFactory;
        this.fieldsToInclude = include;
        this.issue = issue;
    }

    public IssueBeanBuilder expand(String expand)
    {
        this.expand = expand;
        return this;
    }

    public IssueBeanBuilder uriBuilder(final UriBuilder uriBuilder)
    {
        this.uriBuilder = uriBuilder;
        return this;
    }

    public IssueBean build()
    {
        if (beanBuilder2 == null)
        {
            if (uriBuilder != null)
            {
                beanBuilder2 = beanBuilderFactory.newIssueBeanBuilder2(fieldsToInclude, expand, uriBuilder);
            }
            else
            {
                beanBuilder2 = beanBuilderFactory.newIssueBeanBuilder2(fieldsToInclude, expand);
            }
        }
        return beanBuilder2.build(issue);
    }
}