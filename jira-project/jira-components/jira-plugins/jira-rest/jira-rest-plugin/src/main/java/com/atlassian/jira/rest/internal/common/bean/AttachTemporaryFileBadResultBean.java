package com.atlassian.jira.rest.internal.common.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.common.base.Objects;

/**
 * @since v6.4
 */
@SuppressWarnings ({ "UnusedDeclaration" })
@XmlRootElement
public class AttachTemporaryFileBadResultBean
{
    @XmlElement
    private String errorMessage;

    @XmlElement
    private String token;

    @SuppressWarnings ({ "UnusedDeclaration", "unused" })
    private AttachTemporaryFileBadResultBean()
    {}

    public AttachTemporaryFileBadResultBean(final String msg)
    {
        this(msg, null);
    }

    public AttachTemporaryFileBadResultBean(final String msg, final String token)
    {
        this.errorMessage = msg;
        this.token = token;
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(errorMessage, token);
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()) {return false;}
        final AttachTemporaryFileBadResultBean other = (AttachTemporaryFileBadResultBean) obj;

        return Objects.equal(this.errorMessage, other.errorMessage)
                && Objects.equal(this.token, other.token);
    }
}
