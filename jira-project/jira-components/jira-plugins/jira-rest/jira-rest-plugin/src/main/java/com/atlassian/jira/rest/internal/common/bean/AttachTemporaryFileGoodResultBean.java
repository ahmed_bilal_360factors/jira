package com.atlassian.jira.rest.internal.common.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.common.base.Objects;

/**
 * @since v6.4
 */
@SuppressWarnings ({ "UnusedDeclaration" })
@XmlRootElement
public class AttachTemporaryFileGoodResultBean
{
    @XmlElement
    private String name;

    @XmlElement
    private String id;

    @SuppressWarnings ({ "UnusedDeclaration", "unused" })
    private AttachTemporaryFileGoodResultBean()
    {}

    public AttachTemporaryFileGoodResultBean(final String id, final String name)
    {
        this.id = id;
        this.name = name;
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(name, id);
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()) {return false;}
        final AttachTemporaryFileGoodResultBean other = (AttachTemporaryFileGoodResultBean) obj;

        return Objects.equal(this.name, other.name)
                && Objects.equal(this.id, other.id);
    }
}
