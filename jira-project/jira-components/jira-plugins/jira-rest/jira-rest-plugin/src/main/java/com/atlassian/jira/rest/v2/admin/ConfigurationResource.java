package com.atlassian.jira.rest.v2.admin;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import static com.atlassian.jira.rest.api.http.CacheControl.never;

/**
 * @since v6.4
 */
@Path ("configuration")
@AnonymousAllowed
@Consumes ({ MediaType.APPLICATION_JSON })
@Produces ({ MediaType.APPLICATION_JSON })
public class ConfigurationResource
{
    private final JiraAuthenticationContext authenticationContext;
    private final GlobalPermissionManager globalPermissionManager;
    private final I18nHelper i18n;
    private final ConfigurationBeanFactory configurationBeanFactory;

    public ConfigurationResource(JiraAuthenticationContext authenticationContext,
            GlobalPermissionManager permissionManager,
            I18nHelper i18n,
            ConfigurationBeanFactory configurationBeanFactory)
    {
        this.authenticationContext = authenticationContext;
        this.globalPermissionManager = permissionManager;
        this.i18n = i18n;
        this.configurationBeanFactory = configurationBeanFactory;
    }

    /**
     * Returns the information if the optional features in JIRA are enabled or disabled. If the time tracking is enabled,
     * it also returns the detailed information about time tracking configuration.
     *
     * @response.representation.200.qname
     *      configuration
     *
     * @response.representation.200.mediaType
     *      application/json
     *
     * @response.representation.200.doc
     *      Returned the configuration of optional features in JIRA.
     * @response.representation.200.example
     *      {@link com.atlassian.jira.rest.v2.admin.ConfigurationBean#DOC_EXAMPLE}
     *
     * @response.representation.403.doc
     *      Returned if the user is not an admin.
     */
    @GET
    public Response getConfiguration()
    {
        final ApplicationUser user = authenticationContext.getUser();

        if (!globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user))
        {
            return Response
                    .status(Response.Status.FORBIDDEN)
                    .cacheControl(never())
                    .entity(ErrorCollection.of(i18n.getText("configuration.access.not.allowed")))
                    .build();
        }
        else
        {
            final ConfigurationBean configurationBean = configurationBeanFactory.createConfigurationBean();
            return Response
                .status(Response.Status.OK)
                .cacheControl(never())
                .entity(configurationBean)
                .build();
        }
    }

}
