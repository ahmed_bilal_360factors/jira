package com.atlassian.jira.rest.v2.index;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.index.request.ReindexRequest;
import com.atlassian.jira.index.request.ReindexRequestService;
import com.atlassian.jira.index.request.ReindexRequestType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * REST resource for querying and executing reindex requests.
 *
 * @since 6.4
 */
@Consumes ({ MediaType.APPLICATION_JSON })
@Produces ({ MediaType.APPLICATION_JSON })
@Path ("reindex/request")
public class ReindexRequestResource
{
    private static final Logger log = LoggerFactory.getLogger(ReindexRequestResource.class);

    private final ReindexRequestService reindexRequestService;

    public ReindexRequestResource(ReindexRequestService reindexRequestService)
    {
        this.reindexRequestService = reindexRequestService;
    }

    /**
     * Executes any pending reindex requests.  Returns a JSON array containing the IDs of the reindex requests
     * that are being processed.  Execution is asynchronous - progress of the returned tasks can be monitored through
     * other REST calls.
     *
     * @return JSON array containing the IDs of the reindex requests that are being processed.
     *
     * @response.representation.200.mediaType application/json
     *
     * @response.representation.200.doc
     *      Returns an array containing the reindex request IDs being processed.
     */
    @POST
    public Response processRequests()
    {
        try
        {
            Set<ReindexRequest> requests = reindexRequestService.processRequests(EnumSet.allOf(ReindexRequestType.class), true);
            List<Long> requestIds = new ArrayList<Long>();
            for (ReindexRequest request : requests)
            {
                requestIds.addAll(request.getAllRequestIds());
            }
            Collections.sort(requestIds);
            return Response.ok(requestIds).build();
        }
        catch (PermissionException e)
        {
            log.error("Permission error processing reindex requests: " + e, e);
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        catch (RuntimeException e)
        {
            log.error("Error processing reindex requests: " + e, e);
            return Response.serverError().build();
        }
    }

    /**
     * Retrieves the progress of a single reindex request.
     *
     * @param requestId the reindex request ID.
     *
     * @return details and status of the reindex request.
     *
     * @response.representation.200.mediaType application/json
     *
     * @response.representation.200.doc
     *      Details and status of the reindex request.
     *
     * @response.representation.200.example
     *      {@link ReindexRequestBean#DOC_EXAMPLE}
     *
     * @response.representation.404.doc
     *      Returned if no such reindex request exists.
     */
    @GET
    @Path("{requestId}")
    public Response getProgress(@PathParam("requestId") long requestId)
    {
        ReindexRequest request = reindexRequestService.getReindexProgress(requestId);
        if (request == null)
        {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        else
        {
            return Response.ok(new ReindexRequestBean(request)).build();
        }
    }

    /**
     * Retrieves the progress of a multiple reindex requests.  Only reindex requests that actually exist will be returned
     * in the results.
     *
     * @param requestIds the reindex request IDs.
     *
     * @return an array of results describing the progress of each of the found requests.
     *
     * @response.representation.200.mediaType application/json
     *
     * @response.representation.200.doc
     *      Details and status of the reindex request.
     *
     * @response.representation.200.example
     *      {@link ReindexRequestBean#DOC_EXAMPLE}
     */
    @GET
    @Path("bulk")
    public List<ReindexRequestBean> getProgressBulk(@QueryParam("requestId") Set<Long> requestIds)
    {
        Set<ReindexRequest> requests = reindexRequestService.getReindexProgress(requestIds);
        List<ReindexRequestBean> beans = new ArrayList<ReindexRequestBean>();
        for (ReindexRequest request : requests)
        {
            beans.add(new ReindexRequestBean(request));
        }
        return beans;
    }
}
