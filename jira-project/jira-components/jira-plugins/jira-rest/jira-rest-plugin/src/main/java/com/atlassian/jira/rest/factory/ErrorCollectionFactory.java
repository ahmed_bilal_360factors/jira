package com.atlassian.jira.rest.factory;

import java.util.Arrays;
import java.util.Collection;

import com.atlassian.jira.rest.api.util.ErrorCollection;

import org.springframework.stereotype.Component;

import static com.atlassian.jira.util.ErrorCollection.Reason;

/**
 * Creates {@link com.atlassian.jira.rest.api.util.ErrorCollection} instances.
 *
 * @since v6.4
 */
@Component
public class ErrorCollectionFactory
{

    ErrorCollectionFactory()
    {
    }

    /**
     * @param reason reason
     * @param message error messages
     * @return a new ErrorCollection
     */
    public ErrorCollection of(final Reason reason, final String message)
    {
        final ErrorCollection errors = of(message);
        errors.reason(reason);
        return errors;
    }

    /**
     * Returns a new ErrorCollection containing a list of error messages.
     *
     * @param messages an array of Strings containing error messages
     * @return a new ErrorCollection
     */
    public ErrorCollection of(final String... messages)
    {
        return of(Arrays.asList(messages));
    }

    /**
     * Returns a new ErrorCollection containing a list of error messages.
     *
     * @param messages an Iterable of Strings containing error messages
     * @return a new ErrorCollection
     */
    public ErrorCollection of(final Collection<String> messages)
    {
        return new ErrorCollection().addErrorMessages(messages);
    }

    /**
     * Returns a new ErrorCollection containing all the errors contained in the input error collection.
     *
     * @param errorCollection a com.atlassian.jira.util.ErrorCollection
     * @return a new ErrorCollection
     */
    public ErrorCollection of(final com.atlassian.jira.util.ErrorCollection errorCollection)
    {
        return new ErrorCollection().addErrorCollection(errorCollection);
    }
}