package com.atlassian.jira.rest.v2.avatar;

import com.atlassian.core.util.ReusableBufferedInputStream;
import com.atlassian.core.util.thumbnail.Thumber;
import com.atlassian.core.util.thumbnail.ThumbnailDimension;
import com.atlassian.jira.avatar.AvatarFormat;
import com.google.common.base.Predicates;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

@org.springframework.stereotype.Component
class ImageFileOperations
{
    private static final String AVATAR_IMAGE_FORMAT = "png";
    public static final AvatarFormat AVATAR_IMAGE_FORMAT_FULL = new AvatarFormat(AVATAR_IMAGE_FORMAT, "image/png");

    private final Thumber thumber;

    public ImageFileOperations()
    {
        this.thumber = new Thumber();
    }

    public Image getImageFromFile(final File sourceFile) throws IOException
    {
        Image sourceImage = thumber.getImage(sourceFile, Predicates.<ReusableBufferedInputStream>alwaysTrue());

        if (sourceImage == null)
        {
            throw new IOException("invalid image format: " + sourceFile.getName());
        }
        return sourceImage;
    }

    public UploadedAvatar scaleImageToTempFile(final Image sourceImage, final File targetFile, final int edgeSize)
            throws IOException
    {
        ThumbnailDimension targetDimension = thumber.determineScaleSize(edgeSize, edgeSize, sourceImage.getWidth(null), sourceImage.getHeight(null));
        BufferedImage scaledImage = thumber.scaleImage(sourceImage, targetDimension);

        ImageIO.write(scaledImage, AVATAR_IMAGE_FORMAT, targetFile);

        return new UploadedAvatar(targetFile, AVATAR_IMAGE_FORMAT_FULL.getContentType(), targetDimension.getWidth(), targetDimension.getHeight());
    }
}
