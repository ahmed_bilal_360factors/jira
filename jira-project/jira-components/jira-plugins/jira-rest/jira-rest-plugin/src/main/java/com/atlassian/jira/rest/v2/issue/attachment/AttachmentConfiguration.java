package com.atlassian.jira.rest.v2.issue.attachment;

import com.atlassian.gzipfilter.org.apache.commons.lang.math.NumberUtils;
import com.atlassian.jira.config.properties.ApplicationProperties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.atlassian.jira.config.properties.APKeys.JIRA_ATTACHMENT_NUMBER_OF_ZIP_ENTRIES_TO_SHOW;

/**
 * Attachment-related configuration.
 *
 * @since v6.4
 */
@Component
public class AttachmentConfiguration
{
    public static final int DEFAULT_MAX_ENTRIES = 30;

    private final ApplicationProperties properties;

    @Autowired
    AttachmentConfiguration(final ApplicationProperties properties)
    {
        this.properties = properties;
    }

    public int getMaxEntriesToExpand()
    {
        final String maxEntries = properties.getDefaultBackedString(JIRA_ATTACHMENT_NUMBER_OF_ZIP_ENTRIES_TO_SHOW);
        return NumberUtils.toInt(maxEntries, DEFAULT_MAX_ENTRIES);
    }
}
