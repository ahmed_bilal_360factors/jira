package com.atlassian.jira.rest.v2.issue.attachment.authorization;

import javax.annotation.Nullable;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.issue.attachment.AttachmentService;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.rest.factory.JiraServiceContextFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

/**
 * Decides whether a user can remove an attachment or not.
 *
 * @since v6.4
 */
final class AttachmentRemovalAuthorizer implements AttachmentAuthorizer
{
    private final AttachmentService attachmentService;
    private final JiraServiceContextFactory contextFactory;

    AttachmentRemovalAuthorizer(
            final AttachmentService attachmentService,
            final JiraServiceContextFactory contextFactory
    )
    {
        this.attachmentService = attachmentService;
        this.contextFactory = contextFactory;
    }

    @Override
    public boolean authorize(final Attachment attachment, @Nullable final ApplicationUser user)
    {
        final Long attachmentId = attachment.getId();
        final JiraServiceContext serviceContext = contextFactory.createContext(user);
        return attachmentService.canDeleteAttachment(serviceContext, attachmentId);
    }

    @Override
    public String getNoPermissionMessage(final I18nHelper i18n, final Object attachmentId)
    {
        return i18n.getText("attachment.service.error.delete.no.permission", attachmentId);
    }
}
