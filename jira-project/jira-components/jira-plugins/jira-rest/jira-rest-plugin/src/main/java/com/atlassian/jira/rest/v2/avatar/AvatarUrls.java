package com.atlassian.jira.rest.v2.avatar;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarImageResolver;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugins.avatar.AuiSize;

import org.springframework.stereotype.Component;

import static com.atlassian.jira.avatar.Avatar.Size.getSizeFromParam;
import static com.google.common.collect.Iterables.filter;
import static java.lang.String.format;
import static java.util.EnumSet.allOf;

/**
 * Helper class for building avatar URL maps.
 *
 * @since JIRA 6.3
 * @deprecated Use {@link com.atlassian.jira.avatar.AvatarUrls} if you really must. Since v6.1.
 */
@Component
@Deprecated
public class AvatarUrls
{

    /**
     * Returns the avatar URLs a user and a specific Avatar.
     *
     * @param avatarUser the user whose avatar this is (or null)
     * @param avatar the Avatar
     * @return avatar URLs mapped by size
     * @deprecated the notion of "all the avatar sizes" is broken. We plan to eventually scale to any reasonable pixel size.
     */
    @Deprecated
    public static Map<String, URI> getAvatarURLs(ApplicationUser avatarUser, Avatar avatar)
    {
        final AvatarService avatarService = ComponentAccessor.getAvatarService();
        final Map<String, URI> avatarUrls = new HashMap<String, URI>();

        // TODO JRADEV-20790 - Don't output higher res URLs in our REST endpoints until we start using them ourselves.
        final Iterable<Avatar.Size> lowResAvatars = filter(allOf(Avatar.Size.class), Avatar.Size.LOW_RES);

        for (Avatar.Size size : lowResAvatars)
        {
            final String sizeName = getAuiSizeName(size);

            avatarUrls.put(sizeName, avatarService.getAvatarUrlNoPermCheck(avatarUser, avatar, size));
        }

        return avatarUrls;
    }

    public Map<String, URI> getAvatarURLs(final ApplicationUser remoteUser, final Avatar avatar, final AvatarImageResolver avatarImageResolver)
    {
        Map<String, URI> uris = new HashMap<String, URI>();
        for (AuiSize size : AuiSize.values())
        {
            // TODO JRADEV-20790 - Don't output higher res URLs in our REST endpoints until we start using them ourselves.
            if (size.getSize() <= 48)
            {
                final Avatar.Size jiraSize = getSizeFromParam(size.name().toLowerCase());
                final URI uri = avatarImageResolver.getAvatarAbsoluteUri(remoteUser, avatar, jiraSize);
                uris.put(format("%dx%d", size.getSize(), size.getSize()), uri);
            }
        }
        return uris;
    }

    private static String getAuiSizeName(final Avatar.Size size)
    {
        final int px = size.getPixels();
        return format("%dx%d", px, px);
    }
}
