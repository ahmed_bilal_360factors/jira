package com.atlassian.jira.rest.v2.issue.attachment;

import javax.ws.rs.core.Response;

import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.plugin.attachment.AttachmentArchive;
import com.atlassian.jira.rest.v2.issue.attachment.authorization.AttachmentAuthorizer;
import com.atlassian.jira.rest.v2.issue.attachment.format.ArchiveFormatter;
import com.atlassian.jira.rest.v2.issue.attachment.format.HumanReadableArchive;
import com.atlassian.jira.rest.v2.issue.attachment.operation.AttachmentOperation;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class AttachmentResourceTest
{
    private static final String ATTACHMENT_ID = "682374";

    @Rule
    public final TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Mock (answer = Answers.RETURNS_DEEP_STUBS)
    private AttachmentStrategyFactory factory;
    @Mock
    private AttachmentManager attachmentManager;
    @InjectMocks
    private AttachmentResource sut;

    @Mock
    private AttachmentAuthorizer authorizer;
    @Mock
    private AttachmentOperation operation;
    @Mock
    private ArchiveFormatter<AttachmentArchive> rawFormatter;
    @Mock
    private ArchiveFormatter<HumanReadableArchive> humanFormatter;
    @Mock
    private AttachmentResourceStrategy strategy;
    @Mock
    private Response response;

    @Before
    public void setUp()
    {
        when(factory.resourceStrategy(authorizer, operation)).thenReturn(strategy);
        when(strategy.processAttachment(ATTACHMENT_ID)).thenReturn(response);
    }

    @Test
    public void shouldGetAttachment()
    {
        when(factory.authorizer().view()).thenReturn(authorizer);
        when(factory.operation().view()).thenReturn(operation);

        final Response actualResponse = sut.getAttachment(ATTACHMENT_ID);

        assertEquals(response, actualResponse);
    }

    @Test
    public void shouldRemoveAttachment()
    {
        when(factory.authorizer().removal()).thenReturn(authorizer);
        when(factory.operation().removal()).thenReturn(operation);

        final Response actualResponse = sut.removeAttachment(ATTACHMENT_ID);

        assertEquals(response, actualResponse);
    }

    @Test
    public void shouldExpandAttachmentForMachines()
    {
        when(factory.authorizer().view()).thenReturn(authorizer);
        when(factory.formatter().raw()).thenReturn(rawFormatter);
        when(factory.operation().expansion(rawFormatter)).thenReturn(operation);

        final Response actualResponse = sut.expandForMachines(ATTACHMENT_ID);

        assertEquals(response, actualResponse);
    }

    @Test
    public void shouldExpandAttachmentForHumans()
    {
        when(factory.authorizer().view()).thenReturn(authorizer);
        when(factory.formatter().human()).thenReturn(humanFormatter);
        when(factory.operation().expansion(humanFormatter)).thenReturn(operation);

        final Response actualResponse = sut.expandForHumans(ATTACHMENT_ID);

        assertEquals(response, actualResponse);
    }

}
