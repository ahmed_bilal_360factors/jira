package com.atlassian.jira.rest.v2.issue.attachment;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.junit.rules.InitMockitoMocks;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class AttachmentConfigurationTest
{
    @Rule
    public final TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private ApplicationProperties properties;
    @InjectMocks
    private AttachmentConfiguration sut;

    @Test
    public void shouldGetConfiguredMaxEntries() throws Exception
    {
        when(properties.getDefaultBackedString(APKeys.JIRA_ATTACHMENT_NUMBER_OF_ZIP_ENTRIES_TO_SHOW)).thenReturn("832");

        final int actualMaxEntries = sut.getMaxEntriesToExpand();

        assertEquals(832, actualMaxEntries);
    }

    @Test
    public void shouldGetDefaultMaxEntries() throws Exception
    {
        when(properties.getDefaultBackedString(APKeys.JIRA_ATTACHMENT_NUMBER_OF_ZIP_ENTRIES_TO_SHOW))
                .thenReturn("gibberish");

        final int actualMaxEntries = sut.getMaxEntriesToExpand();

        assertEquals(AttachmentConfiguration.DEFAULT_MAX_ENTRIES, actualMaxEntries);
    }
}