package com.atlassian.jira.rest.v2.issue.attachment.operation;

import javax.ws.rs.core.Response;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.issue.attachment.AttachmentService;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.rest.factory.Responder;
import com.atlassian.jira.util.ErrorCollection;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AttachmentRemovalTest
{
    private static final long ATTACHMENT_ID = 394658348L;

    @Rule
    public final TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private AttachmentService attachmentService;
    @Mock
    private Responder responder;
    @InjectMocks
    private AttachmentRemoval sut;

    @Mock
    private Attachment attachment;
    @Mock
    private JiraServiceContext jiraServiceContext;
    @Mock
    private Response response;
    @Mock
    private ErrorCollection errors;

    @Before
    public void setUp()
    {
        when(attachment.getId()).thenReturn(ATTACHMENT_ID);
        when(jiraServiceContext.getErrorCollection()).thenReturn(errors);
    }

    @Test
    public void shouldRemoveAttachment() throws Exception
    {
        when(errors.hasAnyErrors()).thenReturn(false);
        when(responder.successfulDelete()).thenReturn(response);

        final Response actualResponse = sut.perform(attachment, jiraServiceContext);

        verify(attachmentService).delete(jiraServiceContext, ATTACHMENT_ID);
        assertEquals(response, actualResponse);
    }

    @Test
    public void shouldFailToRemoveAttachment() throws Exception
    {
        when(errors.hasAnyErrors()).thenReturn(true);
        when(responder.errors(errors)).thenReturn(response);

        final Response actualResponse = sut.perform(attachment, jiraServiceContext);

        verify(attachmentService).delete(jiraServiceContext, ATTACHMENT_ID);
        assertEquals(response, actualResponse);
    }
}