package com.atlassian.jira.rest.v2.issue;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarPickerHelper;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.avatar.JiraAvatarSupport;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.bc.project.version.VersionService;
import com.atlassian.jira.bc.projectroles.ProjectRoleService;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.rest.exception.NotFoundWebException;
import com.atlassian.jira.rest.util.AttachmentHelper;
import com.atlassian.jira.rest.v2.issue.project.ProjectBean;
import com.atlassian.jira.rest.v2.issue.project.ProjectBeanFactory;
import com.atlassian.jira.rest.v2.issue.project.ProjectBeanFactoryImpl;
import com.atlassian.jira.rest.v2.issue.project.ProjectFinder;
import com.atlassian.jira.rest.v2.issue.project.ProjectRoleBeanFactory;
import com.atlassian.jira.rest.v2.issue.version.VersionBean;
import com.atlassian.jira.rest.v2.issue.version.VersionBeanFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.xsrf.XsrfInvocationChecker;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollection.Reason;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.jira.rest.assertions.ResponseAssertions.assertResponseBody;
import static com.atlassian.jira.rest.assertions.ResponseAssertions.assertResponseCacheNever;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import static org.mockito.Mockito.*;

/**
 * @since v4.4
 */
public class TestProjectResource
{
    @Rule
    public RuleChain chain = MockitoMocksInContainer.forTest(this);

    @Mock
    private JiraAuthenticationContext context;

    @Mock
    private ProjectService projectService;

    @Mock
    private VersionService versionService;

    @Mock
    private AvatarService avatarService;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private ProjectManager projectManager;

    @Mock
    private AvatarManager avatarManager;

    @Mock
    private AvatarPickerHelper avatarPickerHelper;

    @Mock
    private AttachmentHelper attachmentHelper;

    @Mock
    private VersionBeanFactory versionBeanFactory;

    @Mock
    private JiraBaseUrls jiraBaseUrls;

    @Mock
    private ProjectFinder projectFinder;

    @Mock
    private XsrfInvocationChecker xsrfChecker;

    @Mock
    private UriInfo uriInfo;

    @Mock
    private ResourceUriBuilder uriBuilder;

    @Mock
    private ProjectRoleService projectRoleService;

    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    private UserManager userManager;

    @Mock
    private ProjectRoleBeanFactory projectRoleBeanFactory;

    @Mock
    private JiraAvatarSupport jiraAvatarSupport;

    private ProjectBeanFactory projectBeanFactory;

    @Before
    public void setUp()
    {
        projectBeanFactory = new ProjectBeanFactoryImpl(versionBeanFactory, uriInfo, uriBuilder, projectRoleService,
                                jiraAuthenticationContext, userManager, jiraBaseUrls, projectManager, projectRoleBeanFactory, jiraAvatarSupport);
    }

    @Test
    public void testGetProjectVersionsAnnotations() throws NoSuchMethodException
    {
        Method method = ProjectResource.class.getMethod("getProjectVersions", String.class, String.class);
        Path path = method.getAnnotation(Path.class);
        assertNotNull(path);
        assertEquals("{projectIdOrKey}/versions", path.value());
    }

    @Test
    public void testGetProjectVersionsBadProject() throws Exception
    {
        ApplicationUser user = new MockApplicationUser("bbain");
        String key = "key";
        String error = "We have a problem";

        when(context.getUser()).thenReturn(user);
        when(projectFinder.getGetProjectForActionByIdOrKey(user, key, ProjectAction.VIEW_PROJECT)).thenReturn(
                new ProjectService.GetProjectResult(errors(error)));

        ProjectResource resource = new ProjectResource(projectService, context, null, versionService, null, avatarService, null, projectBeanFactory, versionBeanFactory, permissionManager, projectManager, avatarManager, avatarPickerHelper, attachmentHelper, jiraBaseUrls, null, null, null, null, projectFinder, xsrfChecker, jiraAvatarSupport);
        final Response response = resource.getProjectVersions(key, null);
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
        assertEquals(com.atlassian.jira.rest.api.util.ErrorCollection.of(error).reason(Reason.VALIDATION_FAILED), response.getEntity());
    }

    @Test
    public void testGetProjectVersionsBadVersion() throws Exception
    {
        ApplicationUser applicationUser = new MockApplicationUser("bbain");
        User user = new MockUser("bbain");
        String key = "key";
        String error = "We have a problem";
        MockProject project = new MockProject(10101L);

        when(context.getUser()).thenReturn(applicationUser);
        when(context.getLoggedInUser()).thenReturn(user);
        when(projectFinder.getGetProjectForActionByIdOrKey(applicationUser, key, ProjectAction.VIEW_PROJECT))
                .thenReturn(new ProjectService.GetProjectResult(ok(), project));

        when(versionService.getVersionsByProject(user, project))
                .thenReturn(new VersionService.VersionsResult(errors(error)));

        ProjectResource resource = new ProjectResource(projectService, context, null, versionService, null, avatarService, null, projectBeanFactory, versionBeanFactory, permissionManager, projectManager, avatarManager, avatarPickerHelper, attachmentHelper, jiraBaseUrls, null, null, null, null, projectFinder, xsrfChecker, jiraAvatarSupport);
        try
        {
            resource.getProjectVersions(key, null);
            fail("Should have thrown exceptions.");
        }
        catch (NotFoundWebException er)
        {
            assertEquals(com.atlassian.jira.rest.api.util.ErrorCollection.of(error).reason(Reason.VALIDATION_FAILED), er.getResponse().getEntity());
        }
    }

    @Test
    public void testGetProjectVersionsGood() throws Exception
    {
        User user = new MockUser("bbain");
        ApplicationUser applicationUser = new MockApplicationUser("bbain");
        String key = "key";
        MockProject project = new MockProject(10101L);
        MockVersion version = new MockVersion(181828L, "verion1");
        List<Version> versions = Lists.<Version>newArrayList(version);
        List<VersionBean> beans = Lists.newArrayList(new VersionBean());

        when(context.getUser()).thenReturn(applicationUser);
        when(context.getLoggedInUser()).thenReturn(user);
        when(projectFinder.getGetProjectForActionByIdOrKey(applicationUser, key, ProjectAction.VIEW_PROJECT))
                .thenReturn(new ProjectService.GetProjectResult(ok(), project));

        when(versionService.getVersionsByProject(user, project))
                .thenReturn(new VersionService.VersionsResult(ok(), versions));
        when(versionBeanFactory.createVersionBeans(versions, false)).thenReturn(beans);

        ProjectResource resource = new ProjectResource(projectService, context, null, versionService, null, avatarService, null, projectBeanFactory, versionBeanFactory, permissionManager, projectManager, avatarManager, avatarPickerHelper, attachmentHelper, jiraBaseUrls, null, null, null, null, projectFinder, xsrfChecker, jiraAvatarSupport);
        Response actualResponse = resource.getProjectVersions(key, null);

        assertResponseBody(beans, actualResponse);
        assertResponseCacheNever(actualResponse);
    }

    @Test
    public void testGetProjectsNoExpand() throws Exception
    {
        User user = new MockUser("bbain");
        ApplicationUser applicationUser = new MockApplicationUser("bbain");
        String key = "key";
        MockProject project = new MockProjectX(10101L);
        project.setDescription("My special project");
        MockVersion version = new MockVersion(181828L, "verion1");
        List<Version> versions = Lists.<Version>newArrayList(version);
        List<ProjectBean> beans = Lists.newArrayList(new ProjectBean());
        List<Project> projects = Lists.<Project>newArrayList(project);

        when(context.getUser()).thenReturn(applicationUser);
        when(context.getLoggedInUser()).thenReturn(user);
        when(projectService.getAllProjectsForAction(applicationUser, ProjectAction.VIEW_PROJECT))
                .thenReturn(new ServiceOutcomeImpl<List<Project>>(ok(), projects));

        ProjectResource resource = new ProjectResource(projectService, context, null, versionService, null, avatarService, null, projectBeanFactory, versionBeanFactory, permissionManager, projectManager, avatarManager, avatarPickerHelper, attachmentHelper, jiraBaseUrls, null, null, null, null, projectFinder, xsrfChecker, jiraAvatarSupport);
        Response actualResponse = resource.getAllProjects(null);

        List<?> responseBeans = (List<?>)actualResponse.getEntity();
        assertEquals("Wrong result count.", 1, responseBeans.size());
        ProjectBean responseBean = (ProjectBean)responseBeans.get(0);

        assertEquals("Wrong ID.", String.valueOf(project.getId()), responseBean.getId());
        assertNull("Should not have expanded description.", responseBean.getDescription());

        assertResponseCacheNever(actualResponse);
    }

    @Test
    public void testGetProjectsExpandDescription() throws Exception
    {
        User user = new MockUser("bbain");
        ApplicationUser applicationUser = new MockApplicationUser("bbain");
        String key = "key";
        MockProject project = new MockProjectX(10101L);
        project.setDescription("My special project");
        MockVersion version = new MockVersion(181828L, "verion1");
        List<Version> versions = Lists.<Version>newArrayList(version);
        List<ProjectBean> beans = Lists.newArrayList(new ProjectBean());
        List<Project> projects = Lists.<Project>newArrayList(project);

        when(context.getUser()).thenReturn(applicationUser);
        when(context.getLoggedInUser()).thenReturn(user);
        when(projectService.getAllProjectsForAction(applicationUser, ProjectAction.VIEW_PROJECT))
                .thenReturn(new ServiceOutcomeImpl<List<Project>>(ok(), projects));

        ProjectResource resource = new ProjectResource(projectService, context, null, versionService, null, avatarService, null, projectBeanFactory, versionBeanFactory, permissionManager, projectManager, avatarManager, avatarPickerHelper, attachmentHelper, jiraBaseUrls, null, null, null, null, projectFinder, xsrfChecker, jiraAvatarSupport);
        Response actualResponse = resource.getAllProjects("description");

        List<?> responseBeans = (List<?>)actualResponse.getEntity();
        assertEquals("Wrong result count.", 1, responseBeans.size());
        ProjectBean responseBean = (ProjectBean)responseBeans.get(0);

        assertEquals("Wrong ID.", String.valueOf(project.getId()), responseBean.getId());
        assertEquals("Wrong expanded description.", project.getDescription(), responseBean.getDescription());

        assertResponseCacheNever(actualResponse);
    }

    private static ErrorCollection errors(String... errors)
    {
        SimpleErrorCollection collection = new SimpleErrorCollection();
        collection.addErrorMessages(Arrays.asList(errors));
        return collection;
    }

    private static ErrorCollection ok()
    {
        return new SimpleErrorCollection();
    }

    private static class MockProjectX extends MockProject
    {
        public MockProjectX(long id)
        {
            super(id);
        }

        @Override
        public ProjectCategory getProjectCategoryObject()
        {
            return(null);
        }
    }
}
