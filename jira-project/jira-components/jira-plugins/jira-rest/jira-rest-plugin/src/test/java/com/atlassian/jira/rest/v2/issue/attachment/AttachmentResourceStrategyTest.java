package com.atlassian.jira.rest.v2.issue.attachment;

import javax.ws.rs.core.Response;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.issue.attachment.AttachmentService;
import com.atlassian.jira.exception.AttachmentNotFoundException;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.rest.api.util.ErrorCollection;
import com.atlassian.jira.rest.factory.ErrorCollectionFactory;
import com.atlassian.jira.rest.factory.JiraServiceContextFactory;
import com.atlassian.jira.rest.factory.Responder;
import com.atlassian.jira.rest.v2.issue.attachment.authorization.AttachmentAuthorizer;
import com.atlassian.jira.rest.v2.issue.attachment.operation.AttachmentOperation;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static com.atlassian.jira.util.ErrorCollection.Reason.FORBIDDEN;
import static com.atlassian.jira.util.ErrorCollection.Reason.NOT_FOUND;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class AttachmentResourceStrategyTest
{
    @Rule
    public final TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private AttachmentAuthorizer authorizer;
    @Mock
    private AttachmentOperation operation;
    @Mock
    private Responder responder;
    @Mock
    private ErrorCollectionFactory errorCollectionFactory;
    @Mock
    private AttachmentManager attachmentManager;
    @Mock
    private AttachmentService attachmentService;
    @Mock
    private JiraAuthenticationContext authContext;
    @Mock
    private I18nHelper i18n;
    @Mock
    private JiraServiceContextFactory contextFactory;
    @InjectMocks
    private AttachmentResourceStrategy sut;

    @Mock
    private ErrorCollection errors;
    @Mock
    private ApplicationUser user;
    @Mock
    private JiraServiceContext serviceContext;
    @Mock
    private Attachment attachment;
    @Mock
    private com.atlassian.jira.util.ErrorCollection serviceErrors;
    @Mock
    private Response response;

    @Before
    public void setUp()
    {
        when(attachmentManager.attachmentsEnabled()).thenReturn(true);
        when(authContext.getUser()).thenReturn(user);
        when(contextFactory.createContext(user)).thenReturn(serviceContext);
        when(serviceContext.getErrorCollection()).thenReturn(serviceErrors);
        when(serviceErrors.hasAnyErrors()).thenReturn(false);
    }

    @Test
    public void shouldPerformOperation()
    {
        when(attachmentService.getAttachment(serviceContext, 623785L)).thenReturn(attachment);
        when(authorizer.authorize(attachment, user)).thenReturn(true);
        when(operation.perform(attachment, serviceContext)).thenReturn(response);

        final Response actualResponse = sut.processAttachment("623785");

        assertEquals(response, actualResponse);
    }

    @Test
    public void shouldStopAtFailedAuthorization()
    {
        when(attachmentService.getAttachment(serviceContext, 7234L)).thenReturn(attachment);
        when(authorizer.authorize(attachment, user)).thenReturn(false);
        when(authorizer.getNoPermissionMessage(i18n, "7234")).thenReturn("noPermissionMsg");
        when(errorCollectionFactory.of(FORBIDDEN, "noPermissionMsg")).thenReturn(errors);
        when(responder.restfulErrors(errors)).thenReturn(response);

        final Response actualResponse = sut.processAttachment("7234");

        assertEquals(response, actualResponse);
        verifyZeroInteractions(operation);
    }

    @Test
    public void shouldStopAtDisabledFeature()
    {
        when(attachmentManager.attachmentsEnabled()).thenReturn(false);
        when(i18n.getText("attachment.service.error.disabled")).thenReturn("featureDisabledMsg");
        when(errorCollectionFactory.of(NOT_FOUND, "featureDisabledMsg")).thenReturn(errors);
        when(responder.restfulErrors(errors)).thenReturn(response);

        final Response actualResponse = sut.processAttachment("92347");

        assertEquals(response, actualResponse);
        verifyZeroInteractions(authorizer, operation);
    }

    @Test
    public void shouldStopAtServiceErrors()
    {
        when(serviceErrors.hasAnyErrors()).thenReturn(true);
        when(errorCollectionFactory.of(serviceErrors)).thenReturn(errors);
        when(responder.restfulErrors(errors)).thenReturn(response);

        final Response actualResponse = sut.processAttachment("92347");

        assertEquals(response, actualResponse);
        verifyZeroInteractions(authorizer, operation);
    }

    @Test
    public void shouldNotFindByNonNumericId()
    {
        when(i18n.getText("rest.attachment.error.not.found", "kebab")).thenReturn("notFoundMsg");
        when(errorCollectionFactory.of(NOT_FOUND, "notFoundMsg")).thenReturn(errors);
        when(responder.restfulErrors(errors)).thenReturn(response);

        final Response actualResponse = sut.processAttachment("kebab");

        assertEquals(response, actualResponse);
        verifyZeroInteractions(authorizer, operation);
    }

    @Test
    public void shouldNotFindByNumericId()
    {
        doThrow(AttachmentNotFoundException.class).when(attachmentService).getAttachment(serviceContext, 28573L);
        when(i18n.getText("rest.attachment.error.not.found", "28573")).thenReturn("notFoundMsg");
        when(errorCollectionFactory.of(NOT_FOUND, "notFoundMsg")).thenReturn(errors);
        when(responder.restfulErrors(errors)).thenReturn(response);

        final Response actualResponse = sut.processAttachment("28573");

        assertEquals(response, actualResponse);
        verifyZeroInteractions(authorizer, operation);
    }
}