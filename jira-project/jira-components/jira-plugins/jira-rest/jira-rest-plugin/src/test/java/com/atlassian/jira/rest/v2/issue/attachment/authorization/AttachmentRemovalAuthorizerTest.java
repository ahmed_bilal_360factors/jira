package com.atlassian.jira.rest.v2.issue.attachment.authorization;

import javax.ws.rs.core.Response;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.issue.attachment.AttachmentService;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.rest.factory.JiraServiceContextFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class AttachmentRemovalAuthorizerTest
{
    private static final long ATTACHMENT_ID = 85763948L;

    @Rule
    public final TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private AttachmentService attachmentService;
    @Mock
    private JiraServiceContextFactory contextFactory;
    @InjectMocks
    private AttachmentRemovalAuthorizer sut;

    @Mock
    private Attachment attachment;
    @Mock
    private ApplicationUser user;
    @Mock
    private JiraServiceContext serviceContext;
    @Mock
    private Response response;
    @Mock
    private I18nHelper i18n;

    @Test
    public void shouldAuthorize()
    {
        shouldCheckAuthorization(true);
    }

    @Test
    public void shouldNotAuthorize()
    {
        shouldCheckAuthorization(false);
    }

    private void shouldCheckAuthorization(final boolean expectedAuthorization)
    {
        when(attachment.getId()).thenReturn(ATTACHMENT_ID);
        when(contextFactory.createContext(user)).thenReturn(serviceContext);
        when(attachmentService.canDeleteAttachment(serviceContext, ATTACHMENT_ID)).thenReturn(expectedAuthorization);

        final boolean actualAuthorization = sut.authorize(attachment, user);

        assertEquals(expectedAuthorization, actualAuthorization);
    }

    @Test
    public void shouldGetNoPermissionMessage()
    {
        final String expectedMessage = "Dummy message";
        when(i18n.getText("attachment.service.error.delete.no.permission", ATTACHMENT_ID)).thenReturn(expectedMessage);

        final String actualMessage = sut.getNoPermissionMessage(i18n, ATTACHMENT_ID);

        assertEquals(expectedMessage, actualMessage);
    }
}
