package com.atlassian.jira.rest.v2.issue.attachment.operation;

import javax.ws.rs.core.Response;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.issue.AttachmentIndexManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.plugin.attachment.AttachmentArchive;
import com.atlassian.jira.rest.factory.Responder;
import com.atlassian.jira.rest.v2.issue.attachment.AttachmentConfiguration;
import com.atlassian.jira.rest.v2.issue.attachment.format.ArchiveFormatter;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.OK;
import static org.junit.Assert.assertEquals;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.when;

public class AttachmentExpansionTest
{
    private static final int MAX_ENTRIES = 273;

    @Rule
    public final TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private AttachmentIndexManager attachmentIndexManager;
    @Mock (answer = RETURNS_DEEP_STUBS)
    private Responder responder;
    @Mock
    private AttachmentConfiguration configuration;
    @Mock
    private ArchiveFormatter<?> entityFormatter;

    @InjectMocks
    private AttachmentExpansion sut;

    @Mock
    private Attachment attachment;
    @Mock
    private JiraServiceContext jiraServiceContext;
    @Mock
    private Issue issue;
    @Mock
    private AttachmentArchive archive;
    @Mock
    private Object entity;
    @Mock
    private Response response;

    @Before
    public void setUp() throws Exception
    {
        when(configuration.getMaxEntriesToExpand()).thenReturn(MAX_ENTRIES);
        when(attachment.getIssue()).thenReturn(issue);
    }

    @Test
    public void shouldExpandAttachment() throws Exception
    {
        when(attachmentIndexManager.getAttachmentContents(attachment, issue, MAX_ENTRIES))
                .thenReturn(Option.some(archive));
        when(entityFormatter.format(archive, attachment)).thenReturn(entity);
        when(
                responder
                        .builder(OK)
                        .entity(entity)
                        .build()
        ).thenReturn(response);

        final Response actualResponse = sut.perform(attachment, jiraServiceContext);

        assertEquals(response, actualResponse);
    }


    @Test
    public void shouldNotBeAbleToExpandAttachment() throws Exception
    {
        when(attachmentIndexManager.getAttachmentContents(attachment, issue, MAX_ENTRIES))
                .thenReturn(Option.<AttachmentArchive>none());
        when(
                responder
                        .builder(CONFLICT)
                        .build()
        ).thenReturn(response);

        final Response actualResponse = sut.perform(attachment, jiraServiceContext);

        assertEquals(response, actualResponse);
    }
}