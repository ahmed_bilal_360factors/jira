package com.atlassian.jira.rest.v2.admin;


import java.math.BigDecimal;

import com.atlassian.core.util.DateUtils;
import com.atlassian.jira.bc.issue.vote.VoteService;
import com.atlassian.jira.bc.issue.watcher.WatcherService;
import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.link.IssueLinkManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration.TimeFormat.pretty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfigurationBeanFactoryTest
{
    public static final boolean VOTE_ENABLED = true;
    public static final boolean WATCHING_DISABLED = false;
    public static final boolean SUBTASK_DISABLED = true;
    public static final boolean TIME_TRACKING_DISABLED = false;
    public static final boolean ATTACHMENT_DISABLED = false;
    public static final boolean LINKING_ENABLED = true;
    public static final boolean UNASSIGNED_ALLOWED = true;
    @Mock private VoteService voteService;
    @Mock private WatcherService watcherService;
    @Mock private ApplicationProperties applicationProperties;
    @Mock private SubTaskManager subTaskManager;
    @Mock private AttachmentManager attachmentManager;
    @Mock private IssueLinkManager issueLinkManager;
    @Mock private TimeTrackingConfiguration timeTrackingConfig;

    private ConfigurationBeanFactory configurationBeanFactory;

    @Before
    public void setUp()
    {
        when(voteService.isVotingEnabled()).thenReturn(VOTE_ENABLED);
        when(watcherService.isWatchingEnabled()).thenReturn(WATCHING_DISABLED);
        when(subTaskManager.isSubTasksEnabled()).thenReturn(SUBTASK_DISABLED);
        when(attachmentManager.attachmentsEnabled()).thenReturn(ATTACHMENT_DISABLED);
        when(issueLinkManager.isLinkingEnabled()).thenReturn(LINKING_ENABLED);
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(UNASSIGNED_ALLOWED);

        this.configurationBeanFactory = new ConfigurationBeanFactory(voteService, watcherService, applicationProperties, subTaskManager, attachmentManager, issueLinkManager, timeTrackingConfig);
    }

    @Test
    public void testConfigurationWithoutTimeTracking()
    {
        final ConfigurationBean bean = configurationBeanFactory.createConfigurationBean();

        assertEquals(bean.isAttachmentsEnabled(), ATTACHMENT_DISABLED);
        assertEquals(bean.isIssueLinkingEnabled(), LINKING_ENABLED);
        assertEquals(bean.isSubTasksEnabled(), SUBTASK_DISABLED);
        assertEquals(bean.isTimeTrackingEnabled(), TIME_TRACKING_DISABLED);
        assertEquals(bean.isUnassignedIssuesAllowed(), UNASSIGNED_ALLOWED);
        assertEquals(bean.isVotingEnabled(), VOTE_ENABLED);
        assertEquals(bean.isWatchingEnabled(), WATCHING_DISABLED);
    }

    @Test
    public void testConfigurationWithTimeTracking()
    {
        when(timeTrackingConfig.enabled()).thenReturn(true);
        when(timeTrackingConfig.getDaysPerWeek()).thenReturn(new BigDecimal(5.0d));
        when(timeTrackingConfig.getHoursPerDay()).thenReturn(new BigDecimal(8.0d));
        when(timeTrackingConfig.getTimeFormat()).thenReturn(pretty);
        when(timeTrackingConfig.getDefaultUnit()).thenReturn(DateUtils.Duration.MINUTE);

        final ConfigurationBean bean = configurationBeanFactory.createConfigurationBean();

        assertEquals(bean.isTimeTrackingEnabled(), true);
        assertEquals(bean.getTimeTrackingConfiguration().getDefaultUnit(), TimeTrackingConfigurationBean.TimeTrackingUnit.minute);
        assertEquals(bean.getTimeTrackingConfiguration().getTimeFormat(), pretty);
        assertEquals(bean.getTimeTrackingConfiguration().getWorkingDaysPerWeek(), 5.0d, Math.pow(10, -5));
        assertEquals(bean.getTimeTrackingConfiguration().getWorkingHoursPerDay(), 8.0d, Math.pow(10, -5));
    }

    @Test
    public void testConfigurationForWatchingEnabled()
    {
        when(watcherService.isWatchingEnabled()).thenReturn(true);

        assertTrue(configurationBeanFactory.createConfigurationBean().isWatchingEnabled());
    }

    @Test
    public void testConfigurationForWatchingDisabled()
    {
        when(watcherService.isWatchingEnabled()).thenReturn(false);

        assertFalse(configurationBeanFactory.createConfigurationBean().isWatchingEnabled());
    }

    @Test
    public void testConfigurationForAttachmentsDisabled()
    {
        when(attachmentManager.attachmentsEnabled()).thenReturn(false);

        assertFalse(configurationBeanFactory.createConfigurationBean().isAttachmentsEnabled());
    }

    @Test
    public void testConfigurationForAttachmentsEnabled()
    {
        when(attachmentManager.attachmentsEnabled()).thenReturn(true);

        assertTrue(configurationBeanFactory.createConfigurationBean().isAttachmentsEnabled());
    }

    @Test
    public void testConfigurationForIssueLinkingDisabled()
    {
        when(issueLinkManager.isLinkingEnabled()).thenReturn(false);

        assertFalse(configurationBeanFactory.createConfigurationBean().isIssueLinkingEnabled());
    }

    @Test
    public void testConfigurationForIssueLinkingEnabled()
    {
        when(issueLinkManager.isLinkingEnabled()).thenReturn(true);

        assertTrue(configurationBeanFactory.createConfigurationBean().isIssueLinkingEnabled());
    }

    @Test
    public void testConfigurationForSubtasksEnabled()
    {
        when(subTaskManager.isSubTasksEnabled()).thenReturn(true);

        assertTrue(configurationBeanFactory.createConfigurationBean().isSubTasksEnabled());
    }

    @Test
    public void testConfigurationForSubtasksDisabled()
    {
        when(subTaskManager.isSubTasksEnabled()).thenReturn(false);

        assertFalse(configurationBeanFactory.createConfigurationBean().isSubTasksEnabled());
    }

    @Test
    public void testConfigurationForUnassignedEnabled()
    {
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(true);

        assertTrue(configurationBeanFactory.createConfigurationBean().isUnassignedIssuesAllowed());
    }

    @Test
    public void testConfigurationForUnassignedDisabled()
    {
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(false);

        assertFalse(configurationBeanFactory.createConfigurationBean().isUnassignedIssuesAllowed());
    }

    @Test
    public void testConfigurationForVotingEnabled()
    {
        when(voteService.isVotingEnabled()).thenReturn(true);

        assertTrue(configurationBeanFactory.createConfigurationBean().isVotingEnabled());
    }

    @Test
    public void testConfigurationForVotingDisabled()
    {
        when(voteService.isVotingEnabled()).thenReturn(false);

        assertFalse(configurationBeanFactory.createConfigurationBean().isVotingEnabled());
    }
}