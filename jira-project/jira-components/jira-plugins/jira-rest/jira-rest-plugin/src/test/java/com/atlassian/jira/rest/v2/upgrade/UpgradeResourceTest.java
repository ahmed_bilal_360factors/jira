package com.atlassian.jira.rest.v2.upgrade;

import java.util.Collections;

import javax.ws.rs.core.Response;

import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.mock.MockPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.upgrade.UpgradeManager;
import com.atlassian.scheduler.status.RunDetails;
import com.atlassian.scheduler.status.RunOutcome;

import com.google.common.collect.ImmutableList;

import org.hamcrest.core.IsNull;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class UpgradeResourceTest
{
    @Mock private JiraAuthenticationContext mockAuthenticationContext;
    private PermissionManager mockPermissionManager = new MockPermissionManager(true);
    @Mock private UpgradeManager mockUpgradeManager;
    @Mock private JiraBaseUrls mockBaseUrls;

    private UpgradeResource upgradeResource;

    @Before
    public void setUp()
    {
        when(mockBaseUrls.restApi2BaseUrl()).thenReturn("http://127.0.0.1:8080/jira");
        upgradeResource = new UpgradeResource(mockAuthenticationContext, mockPermissionManager, mockUpgradeManager, mockBaseUrls);
    }

    @Test
    public void testUpgradeResultNoUpgrades()
    {
        Response response = upgradeResource.getUpgradeResult();
        assertEquals("Wrong status.", Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void testUpgradeResultWithPendingUpgradesRunning()
    {
        LocalDate now = new LocalDate();

        RunDetails rundetails = Mockito.mock(RunDetails.class);
        when(rundetails.getStartTime()).thenReturn(now.minus(Period.minutes(1)).toDate());
        when(rundetails.getDurationInMillis()).thenReturn(1234L);
        when(rundetails.getRunOutcome()).thenReturn(RunOutcome.SUCCESS);
        when(rundetails.getMessage()).thenReturn(null);

        when(mockUpgradeManager.areDelayedUpgradesRunning()).thenReturn(true);
        when(mockUpgradeManager.getLastUpgradeResult()).thenReturn(rundetails);

        Response response = upgradeResource.getUpgradeResult();
        assertThat("Wrong status.", Response.Status.SEE_OTHER.getStatusCode(), is(response.getStatus()));
        assertThat("Should be no content", response.getEntity(), IsNull.nullValue());
    }

    @Test
    public void testUpgradeResultDone()
    {
        LocalDate now = new LocalDate();

        RunDetails rundetails = Mockito.mock(RunDetails.class);
        when(rundetails.getStartTime()).thenReturn(now.minus(Period.minutes(1)).toDate());
        when(rundetails.getDurationInMillis()).thenReturn(1234L);
        when(rundetails.getRunOutcome()).thenReturn(RunOutcome.SUCCESS);
        when(rundetails.getMessage()).thenReturn(null);

        when(mockUpgradeManager.areDelayedUpgradesRunning()).thenReturn(false);
        when(mockUpgradeManager.getLastUpgradeResult()).thenReturn(rundetails);

        Response response = upgradeResource.getUpgradeResult();
        assertThat("Wrong status.", Response.Status.OK.getStatusCode(), is(response.getStatus()));
        UpgradeResultBean upgradeResultBean = (UpgradeResultBean) response.getEntity();
        assertThat("No Result", upgradeResultBean, IsNull.notNullValue());
        assertThat("No successful", upgradeResultBean.getOutcome(), is("SUCCESS"));
        assertThat("No successful", upgradeResultBean.getDuration(), is(1234L));
    }

    @Test
    public void testDoUpgradesSuccess()
    {
        when(mockUpgradeManager.scheduleDelayedUpgrades(0, true)).thenReturn(new UpgradeManager.Status(Collections.<String>emptyList()));
        Response response = upgradeResource.runUpgradesNow();
        assertEquals("Wrong status.", Response.Status.ACCEPTED.getStatusCode(), response.getStatus());
        verify(mockUpgradeManager).scheduleDelayedUpgrades(0, true);
    }

    @Test
    public void testDoUpgradesFail()
    {
        when(mockUpgradeManager.scheduleDelayedUpgrades(0, true)).thenReturn(new UpgradeManager.Status(ImmutableList.of("it failed")));
        Response response = upgradeResource.runUpgradesNow();
        assertEquals("Wrong status.", Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
        verify(mockUpgradeManager).scheduleDelayedUpgrades(0, true);
    }
}
