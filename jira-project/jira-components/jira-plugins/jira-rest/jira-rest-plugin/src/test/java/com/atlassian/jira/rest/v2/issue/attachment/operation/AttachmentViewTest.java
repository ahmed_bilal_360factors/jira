package com.atlassian.jira.rest.v2.issue.attachment.operation;

import javax.ws.rs.core.Response;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.rest.factory.Responder;
import com.atlassian.jira.rest.v2.issue.attachment.format.AttachmentBean;
import com.atlassian.jira.rest.v2.issue.builder.BeanBuilderFactory;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static com.atlassian.jira.rest.api.http.CacheControl.never;
import static javax.ws.rs.core.Response.Status.OK;
import static org.junit.Assert.assertEquals;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.when;

public class AttachmentViewTest
{
    @Rule
    public final TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Mock (answer = RETURNS_DEEP_STUBS)
    private Responder responder;
    @Mock (answer = RETURNS_DEEP_STUBS)
    private BeanBuilderFactory beanBuilderFactory;
    @InjectMocks
    private AttachmentView sut;

    @Mock
    private Attachment attachment;
    @Mock
    private JiraServiceContext jiraServiceContext;
    @Mock
    private AttachmentBean attachmentBean;
    @Mock
    private Response response;

    @Test
    public void shouldViewAttachment() throws Exception
    {
        when(
                beanBuilderFactory
                        .newAttachmentBeanBuilder(attachment)
                        .build()
        ).thenReturn(attachmentBean);
        when(
                responder
                        .builder(OK)
                        .entity(attachmentBean)
                        .cacheControl(never())
                        .build()
        ).thenReturn(response);

        final Response actualResponse = sut.perform(attachment, jiraServiceContext);

        assertEquals(response, actualResponse);
    }
}