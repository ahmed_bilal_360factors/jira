package com.atlassian.jira.rest.v2.issue;

import java.net.URI;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarPickerHelper;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.avatar.JiraAvatarSupport;
import com.atlassian.jira.avatar.JiraPluginAvatar;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockAvatar;
import com.atlassian.jira.rest.util.AttachmentHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static com.atlassian.jira.avatar.Avatar.Size;
import static com.atlassian.jira.avatar.Avatar.Size.LARGE;
import static com.atlassian.jira.avatar.Avatar.Size.MEDIUM;
import static com.atlassian.jira.avatar.Avatar.Size.NORMAL;
import static com.atlassian.jira.avatar.Avatar.Size.SMALL;
import static com.atlassian.jira.avatar.Avatar.Type.USER;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AvatarResourceHelperTest
{
    @Rule
    public final TestRule mockInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    JiraAuthenticationContext authContext;

    @Mock
    AvatarManager avatarManager;

    @Mock
    AvatarPickerHelper avatarPickerHelper;

    @Mock
    AttachmentHelper attachmentHelper;

    @Mock
    UserManager userManager;

    @Mock
    JiraAvatarSupport jiraAvatarSupport;

    @Mock
    @AvailableInContainer
    AvatarService avatarService;

    @InjectMocks
    AvatarResourceHelper avatarResourceHelper;

    @Test
    public void getAllAvatarsReturnsAvatarsForUser() throws Exception
    {
        ApplicationUser fredo = new MockApplicationUser("fredo", "Freddo Frog", "freddo@chocolateland.com");
        ApplicationUser admin = new MockApplicationUser("admin", "Admin Esta", "sysop@bofh.com");
        Avatar adminAvatar = new MockAvatar(1, "admin.jpg", "jpg", USER, "admin", false);
        Avatar fredoAvatar = new MockAvatar(2, "fredo.png", "png", USER, "fredo", false);

        when(authContext.getUser()).thenReturn(admin);

        when(avatarManager.getAllSystemAvatars(USER)).thenReturn(ImmutableList.<Avatar>of());
        when(avatarManager.getCustomAvatarsForOwner(USER, admin.getUsername())).thenReturn(ImmutableList.of(adminAvatar));
        when(avatarManager.getCustomAvatarsForOwner(USER, fredo.getUsername())).thenReturn(ImmutableList.of(fredoAvatar));
        when(avatarService.getAvatarUrlNoPermCheck(any(ApplicationUser.class), any(Avatar.class), any(Size.class))).thenAnswer(new UriFromFilename());

        when(userManager.getUserByKey(fredo.getKey())).thenReturn(fredo);
        when(userManager.getUserByKey(admin.getKey())).thenReturn(admin);

        final Map<String, URI> fredoAvatarUrls = ImmutableMap.of(
                "16x16", dummyUri(fredoAvatar, SMALL),
                "24x24", dummyUri(fredoAvatar, NORMAL),
                "32x32", dummyUri(fredoAvatar, MEDIUM),
                "48x48", dummyUri(fredoAvatar, LARGE)
        );

        expectAvatar(fredo, fredoAvatar, "xsmall", "http://fredo.png/16");
        expectAvatar(fredo, fredoAvatar, "medium", "http://fredo.png/32");
        expectAvatar(fredo, fredoAvatar, "small", "http://fredo.png/24");
        expectAvatar(fredo, fredoAvatar, "large", "http://fredo.png/48");

        final Map<String, List<AvatarBean>> avatarBeans = ImmutableMap.<String, List<AvatarBean>>of(
                "system", ImmutableList.<AvatarBean>of(),
                "custom", ImmutableList.of(new AvatarBean(String.valueOf(fredoAvatar.getId()), fredoAvatar.getOwner(), fredoAvatar.isSystemAvatar(), fredoAvatarUrls))
        );

        final Map<String, List<AvatarBean>> retrievedAvatars = avatarResourceHelper.getAllAvatars(USER, fredo.getUsername(), 10000L);
        assertThat("should return all of fredo's avatars", retrievedAvatars, equalTo(avatarBeans));
    }

    private void expectAvatar(final ApplicationUser user, final Avatar av, String size, String url) {
        final JiraPluginAvatar avatar = mock(JiraPluginAvatar.class);
        when(avatar.getUrl()).thenReturn(url);
        when(jiraAvatarSupport.getAvatar(user, size)).thenReturn(avatar);
        when(jiraAvatarSupport.getAvatarById(av.getId(), size)).thenReturn(avatar);
    }

    private static URI dummyUri(final Avatar avatar, final Size size)
    {
        return URI.create(String.format("http://%s/%s", avatar.getFileName(), size.getPixels()));
    }

    /**
     * Return a URI based on the Avatar filename and requested size.
     */
    private static class UriFromFilename implements Answer<URI>
    {
        @Override
        public URI answer(final InvocationOnMock invocation) throws Throwable
        {
            Avatar avatar = (Avatar) invocation.getArguments()[1];
            Size size = (Size) invocation.getArguments()[2];

            return dummyUri(avatar, size);
        }
    }

}
