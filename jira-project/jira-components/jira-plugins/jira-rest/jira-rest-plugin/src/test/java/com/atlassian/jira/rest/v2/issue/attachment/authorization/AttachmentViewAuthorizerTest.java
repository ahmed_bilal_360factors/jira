package com.atlassian.jira.rest.v2.issue.attachment.authorization;

import javax.ws.rs.core.Response;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class AttachmentViewAuthorizerTest
{
    private static final long ATTACHMENT_ID = 2963895L;

    @Rule
    public final TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private PermissionManager permissionManager;
    @InjectMocks
    private AttachmentViewAuthorizer sut;

    @Mock
    private Attachment attachment;
    @Mock
    private ApplicationUser user;
    @Mock
    private Issue issue;
    @Mock
    private Response response;
    @Mock
    private I18nHelper i18n;

    @Test
    public void shouldAuthorize()
    {
        shouldCheckAuthorization(true);
    }

    @Test
    public void shouldNotAuthorize() throws Exception
    {
        shouldCheckAuthorization(false);
    }

    private void shouldCheckAuthorization(final boolean expectedAuthorization)
    {
        when(attachment.getIssue()).thenReturn(issue);
        when(permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, issue, user))
                .thenReturn(expectedAuthorization);

        final boolean actualAuthorization = sut.authorize(attachment, user);

        assertEquals(expectedAuthorization, actualAuthorization);
    }

    @Test
    public void shouldGetNoPermissionMessage()
    {
        final String expectedMessage = "Dummy message";
        when(i18n.getText("attachment.service.error.view.no.permission", ATTACHMENT_ID)).thenReturn(expectedMessage);

        final String actualMessage = sut.getNoPermissionMessage(i18n, ATTACHMENT_ID);

        assertEquals(expectedMessage, actualMessage);
    }
}
