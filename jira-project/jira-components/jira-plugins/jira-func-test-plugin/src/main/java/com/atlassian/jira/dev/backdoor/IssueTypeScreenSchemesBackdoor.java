package com.atlassian.jira.dev.backdoor;

import com.atlassian.jira.issue.fields.screen.FieldScreenFactory;
import com.atlassian.jira.issue.fields.screen.FieldScreenSchemeManager;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenScheme;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeEntity;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Use this backdoor to manipulate Issue Type Screen Schemes as part of setup for tests.
 *
 * This class should only be called by the
 * {@link com.atlassian.jira.functest.framework.backdoor.IssueTypeScreenSchemesControl}.
 *
 * @since v5.0
 */
@Path ("issueTypeScreenSchemes")
@Produces ({ MediaType.APPLICATION_JSON })
public class IssueTypeScreenSchemesBackdoor
{
    private final FieldScreenFactory fieldScreenFactory;
    private final IssueTypeScreenSchemeManager schemeManager;
    private final FieldScreenSchemeManager fieldScreenSchemeManager;

    public IssueTypeScreenSchemesBackdoor(FieldScreenFactory fieldScreenFactory, IssueTypeScreenSchemeManager schemeManager,
            FieldScreenSchemeManager fieldScreenSchemeManager)
    {
        this.fieldScreenFactory = fieldScreenFactory;
        this.schemeManager = schemeManager;
        this.fieldScreenSchemeManager = fieldScreenSchemeManager;
    }

    @GET
    @AnonymousAllowed
    @Path("create")
    public Response create(@QueryParam ("name") String name, @QueryParam ("description") String description, @QueryParam ("fieldScreenSchemeId") Long fieldScreenSchemeId)
    {
        IssueTypeScreenScheme issueTypeScreenScheme = fieldScreenFactory.createIssueTypeScreenScheme();
        issueTypeScreenScheme.setName(name);
        issueTypeScreenScheme.setDescription(description);
        issueTypeScreenScheme.store();
        IssueTypeScreenSchemeEntity issueTypeScreenSchemeEntity = fieldScreenFactory.createIssueTypeScreenSchemeEntity();
        issueTypeScreenSchemeEntity.setIssueTypeId(null);
        issueTypeScreenSchemeEntity.setFieldScreenScheme(fieldScreenSchemeManager.getFieldScreenScheme(fieldScreenSchemeId));
        issueTypeScreenScheme.addEntity(issueTypeScreenSchemeEntity);

        return Response.ok(issueTypeScreenScheme.getId()).build();
    }

    @GET
    @AnonymousAllowed
    @Path("remove")
    public Response remove(@QueryParam ("id") long id)
    {
        schemeManager.getIssueTypeScreenScheme(id).remove();

        return Response.ok(null).build();
    }
}
