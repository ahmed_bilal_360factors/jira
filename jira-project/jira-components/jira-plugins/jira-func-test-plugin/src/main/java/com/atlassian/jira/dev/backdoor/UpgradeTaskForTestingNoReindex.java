package com.atlassian.jira.dev.backdoor;

import java.util.EnumSet;

import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestService;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;
import com.atlassian.jira.upgrade.AbstractUpgradeTask;

public class UpgradeTaskForTestingNoReindex extends AbstractUpgradeTask
{
    public static final String SYSTEM_PROPERTY_NAME = "UpgradeTaskForTesting.invocationCount";

    private final ReindexRequestService reindexRequestService;

    public UpgradeTaskForTestingNoReindex(ReindexRequestService reindexRequestService)
    {
        this.reindexRequestService = reindexRequestService;
    }

    @Override
    public String getShortDescription()
    {
        return "Upgrade Task for testing";
    }

    @Override
    public String getBuildNumber()
    {
        //This should be big enough to come after any build number
        return("2000000002");
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception
    {
        //Do something that can be detected later
        JiraSystemProperties.getInstance().setProperty(SYSTEM_PROPERTY_NAME, String.valueOf(Integer.getInteger(SYSTEM_PROPERTY_NAME, 0) + 1));

        //No reindex!
        //reindexRequestService.requestReindex(ReindexRequestType.IMMEDIATE, null, EnumSet.of(AffectedIndex.ALL), EnumSet.noneOf(SharedEntityType.class));
    }

    @Override
    public ScheduleOption getScheduleOption()
    {
        return ScheduleOption.BEFORE_JIRA_STARTED;
    }
}
