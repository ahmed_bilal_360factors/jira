package com.atlassian.jira.dev.backdoor;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.upgrade.UpgradeManager;
import com.atlassian.jira.upgrade.UpgradeManagerImpl;
import com.atlassian.jira.upgrade.UpgradeManagerParams;
import com.atlassian.jira.upgrade.UpgradeTask;
import com.atlassian.jira.util.JiraUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @since 6.4
 */
@Path("upgradeManager")
@Consumes ({ MediaType.APPLICATION_JSON })
@Produces ({ MediaType.APPLICATION_JSON })
public class UpgradeManagerBackdoor
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeManagerBackdoor.class);

    private final UpgradeManagerImpl upgradeManager;

    public UpgradeManagerBackdoor()
    {
        this.upgradeManager = ComponentAccessor.getComponent(UpgradeManagerImpl.class);
    }

    @POST
    @Path("addUpgrade")
    public Response addUpgrade(@QueryParam("task") String taskName)
    {
        try
        {
            final UpgradeTask task = JiraUtils.loadComponent(taskName, UpgradeManagerBackdoor.class);
            upgradeManager.addUpgrade(task);

            return Response.ok().build();
        }
        catch (ClassNotFoundException e)
        {
            log.error(e.toString(), e);
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
    @Path("doUpgrade")
    public Response doUpgrade()
    {
        UpgradeManagerParams params = UpgradeManagerParams.builder().setSetupMode(false).setAllowReindex(true).build();
        UpgradeManager.Status status = upgradeManager.doUpgrade(params);

        if (status.successful())
        {
            return Response.ok().build();
        }
        else
        {
            log.error("Error running upgrade: " + status.getErrors());
            return Response.serverError().entity(status.getErrors()).build();
        }
    }
}
