package com.atlassian.jira.dev.i18n.bundle;

import java.util.Locale;

import com.google.common.base.Objects;

/**
 * @since v6.4
 */
class ResourceBundleCacheEntry
{
    private final String name;
    private final Locale locale;
    private final ClassLoader classLoader;

    ResourceBundleCacheEntry(final String name, final Locale locale, final ClassLoader classLoader)
    {
        this.name = name;
        this.locale = locale;
        this.classLoader = classLoader;
    }

    public String getName()
    {
        return name;
    }

    public Locale getLocale()
    {
        return locale;
    }

    public ClassLoader getClassLoader()
    {
        return classLoader;
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this)
                .add("name", name)
                .add("locale", locale)
                .add("classLoader", classLoader)
                .toString();
    }
}
