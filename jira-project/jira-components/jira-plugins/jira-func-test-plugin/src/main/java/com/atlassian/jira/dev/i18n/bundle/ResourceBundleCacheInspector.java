package com.atlassian.jira.dev.i18n.bundle;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import com.atlassian.jira.component.ComponentAccessor;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;

import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;

/**
 * @since v6.4
 */
public class ResourceBundleCacheInspector
{

    public static final String PLUGIN_CLASS_LOADER_NAME =
            "com.atlassian.plugin.osgi.util.BundleClassLoaderAccessor$BundleClassLoader";

    /**
     * This method performs some nasty checks against internal private static cache
     * {@link java.util.ResourceBundle#cacheList}. The goal of this check is to
     * find if any plugin resources are still being cached internally by {@link java.util.ResourceBundle}. All plugin
     * bundles should be removed from {@link java.util.ResourceBundle#cacheList} to avoid duplication of bundles keys.
     * Since {@link java.util.ResourceBundle#cacheList} is private static cache we are doing some reflections magic to
     * get into it.
     *
     * @return Collection of {@link com.atlassian.jira.dev.i18n.bundle.CachedPluginBundle} - plugin bundles which are
     * still being cached in {@link java.util.ResourceBundle#cacheList}
     */
    public Collection<CachedPluginBundle> inspect()
    {
        final ClassLoader previousClassLoader = Thread.currentThread().getContextClassLoader();
        try
        {
            Thread.currentThread().setContextClassLoader(ComponentAccessor.class.getClassLoader());
            final Field cacheListField = ResourceBundle.class.getDeclaredField("cacheList");
            cacheListField.setAccessible(true);
            final Map<?, SoftReference<ResourceBundle>> cacheList = (Map<?, SoftReference<ResourceBundle>>) cacheListField.get(null);
            final Set<ResourceBundleCacheEntry> cache = transformToReadableForm(cacheList);
            return newArrayList(analyseResourceBundleCache(cache));
        }
        catch (Exception e)
        {
            throw new FailedToPerformResourceBundleCacheInspectionException(e);
        }
        finally
        {
            Thread.currentThread().setContextClassLoader(previousClassLoader);
        }
    }

    /**
     * This method performs transformation of entries from {@link java.util.ResourceBundle#cacheList} which consists
     * of {@link java.util.ResourceBundle.BundleReference} and {@link java.util.ResourceBundle.CacheKey} because both
     * of them are private static final classes.
     */
    private ImmutableSet<ResourceBundleCacheEntry> transformToReadableForm(final Map<?, SoftReference<ResourceBundle>> cache)
    {
        return ImmutableSet.copyOf(transform(cache.entrySet(), new Function<Map.Entry<?, SoftReference<ResourceBundle>>,
                ResourceBundleCacheEntry>()
        {
            @Override
            public ResourceBundleCacheEntry apply(final Map.Entry<?, SoftReference<ResourceBundle>> mapEntry)
            {
                final Object key = mapEntry.getKey();
                try
                {
                    final String name = getValueFromPrivateField(key, "name", String.class);
                    final Locale locale = getValueFromPrivateField(key, "locale", Locale.class);
                    final WeakReference<ClassLoader> loaderRef = getValueFromPrivateField(key, "loaderRef",
                            WeakReference.class);
                    return new ResourceBundleCacheEntry(name, locale, loaderRef.get());
                }
                catch (Exception e)
                {
                    throw new RuntimeException(e);
                }
            }
        }));
    }

    private <T> T getValueFromPrivateField(final Object object, final String name, final Class<T> clazz) throws IllegalAccessException, NoSuchFieldException
    {
        final Field field = object.getClass().getDeclaredField(name);
        field.setAccessible(true);
        final Object value = field.get(object);
        return (T) value;
    }

    private Iterable<CachedPluginBundle> analyseResourceBundleCache(final Set<ResourceBundleCacheEntry> cacheEntries)
    {
        final Iterable<ResourceBundleCacheEntry> failedCacheEntries = filter(cacheEntries, new Predicate<ResourceBundleCacheEntry>()
        {
            @Override
            public boolean apply(final ResourceBundleCacheEntry cacheEntry)
            {
                return isResourceBundleLoadedByPlugin(cacheEntry);
            }
        });

        return transform(failedCacheEntries, new Function<ResourceBundleCacheEntry, CachedPluginBundle>()
        {
            @Override
            public CachedPluginBundle apply(final ResourceBundleCacheEntry input)
            {
                return new CachedPluginBundle(input.getName(), input.getLocale(), input.getClassLoader().toString());
            }
        });
    }

    private boolean isResourceBundleLoadedByPlugin(final ResourceBundleCacheEntry cacheEntry)
    {
        final ClassLoader classLoader = cacheEntry.getClassLoader();
        if (classLoader != null)
        {
            return classLoader.getClass().getName().equals(PLUGIN_CLASS_LOADER_NAME);
        }
        return false;
    }
}

