package com.atlassian.jira.dev.backdoor;

import com.atlassian.jira.bc.license.LicenseRoleAuthorizationService;
import com.atlassian.jira.license.DefaultLicenseRoleManager;
import com.atlassian.jira.license.LicenseRoleId;
import com.atlassian.jira.license.LicenseRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Backdoor JSON interface to access the active users for a specific license role.
 * @since 6.4
 */
@Path("roleCount")
@AnonymousAllowed
@Consumes ({MediaType.APPLICATION_JSON})
@Produces ({MediaType.APPLICATION_JSON})
public class RoleCount
{
    private final LicenseRoleAuthorizationService licenseRoleAuthorizationService;
    public RoleCount(final LicenseRoleAuthorizationService licenseRoleAuthorizationService)
    {
        this.licenseRoleAuthorizationService = licenseRoleAuthorizationService;
    }

    @GET
    @Path("{roleId}")
    public Response getActiveUsers(@PathParam("roleId") String roleId)
    {
        return Response.ok(new UsersInLicense(roleId, licenseRoleAuthorizationService.getUserCount(new LicenseRoleId(roleId)))).build();
    }
}
