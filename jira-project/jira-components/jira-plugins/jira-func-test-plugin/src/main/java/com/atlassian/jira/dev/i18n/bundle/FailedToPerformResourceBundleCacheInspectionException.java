package com.atlassian.jira.dev.i18n.bundle;

/**
 * @since v6.4
 */
class FailedToPerformResourceBundleCacheInspectionException extends RuntimeException
{
    FailedToPerformResourceBundleCacheInspectionException(Throwable e)
    {
        super("Failed to perform ResourceBundle cache inspection. Possibly some internals of java.util.ResourceBundle" +
                " has changed and " + ResourceBundleCacheInspector.class.getName() + " should be adapted.", e);
    }
}
