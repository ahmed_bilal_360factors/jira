package com.atlassian.jira.dev.backdoor;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * A JSON serialised description of how many users are active in a role.
 *
 * @since v6.4
 */
@SuppressWarnings ({ "UnusedDeclaration" })
@XmlRootElement
@JsonSerialize (include = JsonSerialize.Inclusion.NON_NULL)
public class UsersInLicense
{
    @JsonProperty
    String licenseRoleId;

    @JsonProperty
    Integer activeUsers;

    @JsonCreator
    public UsersInLicense(@JsonProperty("licenseRoleId") String licenseRoleId, int activeUsers)
    {
        this.licenseRoleId=licenseRoleId;
        this.activeUsers=activeUsers;
    }

    public Integer getActiveUsers()
    {
        return activeUsers;
    }

    public void setActiveUsers(final Integer activeUsers)
    {
        this.activeUsers = activeUsers;
    }

    public String getLicenseRoleId()
    {
        return licenseRoleId;
    }

    public void setLicenseRoleId(final String licenseRoleId)
    {
        this.licenseRoleId = licenseRoleId;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final UsersInLicense that = (UsersInLicense) o;

        if (activeUsers != null ? !activeUsers.equals(that.activeUsers) : that.activeUsers != null)
        {
            return false;
        }
        if (licenseRoleId != null ? !licenseRoleId.equals(that.licenseRoleId) : that.licenseRoleId != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = licenseRoleId != null ? licenseRoleId.hashCode() : 0;
        result = 31 * result + (activeUsers != null ? activeUsers.hashCode() : 0);
        return result;
    }
}
