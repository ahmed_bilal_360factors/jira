package com.atlassian.jira.dev.backdoor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import com.google.common.collect.ImmutableList;

import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityExprList;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericValue;

/**
 * Use this backdoor to manipulate User Profiles as part of setup for tests.
 *
 * This class should only be called by the {@link com.atlassian.jira.functest.framework.backdoor.EntityEngine}.
 *
 * @since v5.2
 */
@Path ("entityEngine")
public class EntityEngineBackdoor
{
    private final OfBizDelegator genericDelegator;

    public EntityEngineBackdoor()
    {
        this.genericDelegator = ComponentAccessor.getOfBizDelegator();
    }

    @POST
    @AnonymousAllowed
    @Path("findByAnd")
    public Response findByAnd(@QueryParam ("entity") String entityname, Map<String, Object> fields)
    {
        List<GenericValue> values = null;
        values = genericDelegator.findByAnd(entityname, fields);

        return Response.ok(values).build();
    }

    @POST
    @AnonymousAllowed
    @Path("findByValueList")
    @Produces("application/json")
    @Consumes("application/json")
    public Response findByValueList(@QueryParam ("entity") String entityName, @QueryParam ("field") String field,
                    @QueryParam ("valueType")  @DefaultValue ("string") String valueType,
                    @QueryParam ("returnField") List<String> returnFields, List<String> values)
    {
        List<?> valueList = castValueList(values, valueType);
        EntityExpr expr = new EntityExpr(field, EntityOperator.IN, valueList);
        List<GenericValue> results = genericDelegator.findByCondition(entityName, expr, returnFields);

        return Response.ok(results).build();
    }

    @POST
    @AnonymousAllowed
    @Path("findByValueList2Fields")
    @Produces("application/json")
    @Consumes("application/json")
    public Response findByValueList2Fields(@QueryParam ("entity") String entityName, @QueryParam ("field") String field,
            @QueryParam ("field2") String field2,
            @QueryParam ("valueType")  @DefaultValue ("string") String valueType,
            @QueryParam ("returnField") List<String> returnFields, List<String> values)
    {
        List<?> valueList = castValueList(values, valueType);

        EntityExpr expr1 = new EntityExpr(field, EntityOperator.IN, valueList);
        EntityExpr expr2 = new EntityExpr(field2, EntityOperator.IN, valueList);
        List<EntityExpr> exprs = ImmutableList.of(expr1, expr2);
        EntityExprList condition = new EntityExprList(exprs, EntityOperator.OR);

        List<GenericValue> results = genericDelegator.findByCondition(entityName, condition, returnFields);

        return Response.ok(results).build();
    }

    private static List<?> castValueList(List<String> values, String valueType)
    {
        if ("number".equalsIgnoreCase(valueType))
        {
            return bulkParseLong(values);
        }
        else
        {
            return values;
        }
    }

    private static List<Long> bulkParseLong(List<String> values)
    {
        List<Long> longValues = new ArrayList<Long>(values.size());
        for (String value : values)
        {
            longValues.add(Long.parseLong(value.trim()));
        }
        return longValues;
    }
}
