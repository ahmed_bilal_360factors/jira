package com.atlassian.jira.dev.backdoor;

import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.jira.dev.i18n.bundle.CachedPluginBundle;
import com.atlassian.jira.dev.i18n.bundle.ResourceBundleCacheInspector;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

/**
 * @since v6.4
 */
@AnonymousAllowed
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@Path("/resourcebundle")
public class ResourceBundleCacheBackdoor
{

    @GET
    @Path ("cachedpluginbundles")
    @Produces (MediaType.APPLICATION_JSON)
    public Response getPluginBundlesCachedInternallyByResourceBundle()
    {
        final Collection<CachedPluginBundle> cachedPluginBundles = new ResourceBundleCacheInspector().inspect();
        return Response.ok(cachedPluginBundles).build();
    }

}
