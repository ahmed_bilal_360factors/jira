package com.atlassian.jira.dev.i18n.bundle;

import java.util.Locale;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since v6.4
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CachedPluginBundle
{
    @JsonProperty
    private String bundleName;
    @JsonProperty
    private Locale locale;
    @JsonProperty
    private String classLoaderName;

    public CachedPluginBundle(final String bundleName, final Locale locale, final String classLoaderName)
    {
        this.bundleName = bundleName;
        this.locale = locale;
        this.classLoaderName = classLoaderName;
    }

    public String getBundleName()
    {
        return bundleName;
    }

    public void setBundleName(final String bundleName)
    {
        this.bundleName = bundleName;
    }

    public Locale getLocale()
    {
        return locale;
    }

    public void setLocale(final Locale locale)
    {
        this.locale = locale;
    }

    public String getClassLoaderName()
    {
        return classLoaderName;
    }

    public void setClassLoaderName(final String classLoaderName)
    {
        this.classLoaderName = classLoaderName;
    }
}
