(function($, AJS) {
    var context = AJS.namespace("JIRA.Func.Rest");

    var messageFromErrorCollection = function (data) {
        if (data) {
            if (_.isArray(data.errorMessages) && !_.isEmpty(data.errorMessages)) {
                return data.errorMessages.join(' ');
            } else if (_.isObject(data.errors) && !_.isEmpty(data.errors)) {
                return _.values(data.errors).join(' ');
            }
        }
        return null;
    };

    var parseJson = function (xhr) {
        try {
            return xhr.responseText && $.parseJSON(xhr.responseText);
        } catch (e) {
            //fall through because its not JSON.
        }
        return null;
    };

    var handleAjax = function(xhr) {
        xhr.fail(function(xhr, text) {
            var header = "Fail: " + text + "(" + xhr.status + ")";
            var data = parseJson(xhr);
            if (data) {
                var errorMessage = messageFromErrorCollection(data);
                if (errorMessage) {
                    console.log(header + " - " + errorMessage);
                } else {
                    console.log(header + " - Data:");
                    console.log(data);
                }
            } else if (xhr.responseText) {
                console.log(header + " - ResponseText:");
                console.log(xhr.responseText);
            }
        });
        xhr.success(function(data, text, xhr) {
            var header = "Success: " + text + "(" + xhr.status + ")";
            if (data) {
                console.log(header + " - Data:");
                console.log(data);
            } else {
                console.log(header);
            }
        });
        return xhr;
    };

    context.post = function(url, data) {
        url = contextPath + url;
        return handleAjax($.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(data)
        }));
    };

    context.remove = function(url, data) {
        url = contextPath + url;
        return handleAjax($.ajax({
            url: url,
            type: "DELETE",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(data)
        }));
    };

    context.put = function(url, data) {
        url = contextPath + url;
        return handleAjax($.ajax({
            url: url,
            type: "PUT",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(data)
        }));
    };

    context.get = function(url, data) {
        url = contextPath + url;
        if (data) {
            if (url.indexOf("?") === -1) {
                url = url + '?';
            } else {
                url = url + '&';
            }
            url += $.param(data);
        }
        return handleAjax($.ajax({
            url: url,
            type: "GET",
            dataType: "json",
            contentType: "application/json"
        }));
    }

}(AJS.$, AJS));