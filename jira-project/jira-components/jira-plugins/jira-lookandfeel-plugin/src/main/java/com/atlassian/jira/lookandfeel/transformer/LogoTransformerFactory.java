package com.atlassian.jira.lookandfeel.transformer;

import com.atlassian.jira.lookandfeel.LookAndFeelProperties;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugin.webresource.transformer.CharSequenceDownloadableResource;
import com.atlassian.plugin.webresource.transformer.TransformableResource;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;
import com.atlassian.plugin.webresource.url.UrlBuilder;

/**
 *
 *
 * @since v6.3
 */
public class LogoTransformerFactory implements WebResourceTransformerFactory
{
    private final LookAndFeelProperties lookAndFeelProperties;
    private final WebResourceUrlProvider webResourceUrlProvider;

    public LogoTransformerFactory(LookAndFeelProperties lookAndFeelProperties, WebResourceUrlProvider provider)
    {
        this.lookAndFeelProperties = lookAndFeelProperties;
        this.webResourceUrlProvider = provider;
    }

    @Override
    public TransformerUrlBuilder makeUrlBuilder(final TransformerParameters transformerParameters)
    {
        return new TransformerUrlBuilder()
        {
            @Override
            public void addToUrl(final UrlBuilder urlBuilder)
            {
                urlBuilder.addToHash("defaultLogo", lookAndFeelProperties.getDefaultCssLogoUrl());
                urlBuilder.addToHash("defaultLogo", lookAndFeelProperties.getMaxLogoHeight());
            }
        };
    }

    @Override
    public UrlReadingWebResourceTransformer makeResourceTransformer(final TransformerParameters transformerParameters)
    {
        return new UrlReadingWebResourceTransformer()
        {
            @Override
            public DownloadableResource transform(final TransformableResource transformableResource, final QueryParams params)
            {
                return new CharSequenceDownloadableResource(transformableResource.nextResource())
                {
                    protected String transform(final CharSequence originalContent)
                    {
                        String replacedContent = originalContent.toString();
                        String prefix = webResourceUrlProvider.getStaticResourcePrefix(UrlMode.AUTO);
                        // JRADEV-6590 If this is the root app, then ignore the slash
                        if ("/".equals(prefix))
                        {
                            prefix = "";
                        }
                        String defaultLogoUrl = lookAndFeelProperties.getDefaultCssLogoUrl();
                        if (defaultLogoUrl != null && !defaultLogoUrl.startsWith("http://") && !defaultLogoUrl.startsWith("."))
                        {
                            defaultLogoUrl = prefix+defaultLogoUrl;
                        }
                        replacedContent = replacedContent.replace("@defaultLogoUrl", defaultLogoUrl);
                        replacedContent = replacedContent.replace("@maxLogoHeight", String.valueOf(lookAndFeelProperties.getMaxLogoHeight()));
                        return replacedContent;
                    }
                };
            }
        };
    }
}
