package com.atlassian.jira.plugin.flag;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.flag.FlagDismissalService;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import com.google.common.collect.ImmutableMap;

public class DismissedFlagsDataProvider implements WebResourceDataProvider
{
    private final FlagDismissalService flagDismissalService;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public DismissedFlagsDataProvider(
            FlagDismissalService flagDismissalService,
            JiraAuthenticationContext jiraAuthenticationContext)
    {
        this.flagDismissalService = flagDismissalService;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    public Jsonable get()
    {
        return new Jsonable()
        {
            @Override
            public void write(final Writer writer) throws IOException
            {
                try
                {
                    getFlags().write(writer);
                }
                catch (JSONException e)
                {
                    throw new JsonMappingException(e);
                }
            }
        };
    }

    private JSONObject getFlags()
    {
        Collection<String> dismissed = flagDismissalService.getDismissedFlagsForUser(jiraAuthenticationContext.getUser());
        return new JSONObject(ImmutableMap.<String, Object>of("dismissed", dismissed));
    }
}

