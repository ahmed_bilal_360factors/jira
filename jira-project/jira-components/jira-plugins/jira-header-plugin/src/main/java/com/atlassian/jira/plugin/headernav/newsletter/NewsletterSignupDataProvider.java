package com.atlassian.jira.plugin.headernav.newsletter;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import com.atlassian.fugue.Option;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import com.google.common.collect.Maps;

/**
 * Data provider to return if a newsletter signup tooltip should be shown the currently logged in user.
 *
 * @since v6.4
 */
public class NewsletterSignupDataProvider implements WebResourceDataProvider
{
    private final JiraAuthenticationContext authenticationContext;
    private final NewsletterUserPreferencesManager newsletterUserPreferencesManager;
    private final HelpUrls helpUrls;

    public NewsletterSignupDataProvider(
            final JiraAuthenticationContext authenticationContext,
            final NewsletterUserPreferencesManager newsletterUserPreferencesManager,
            final HelpUrls helpUrls)
    {
        this.authenticationContext = authenticationContext;
        this.newsletterUserPreferencesManager = newsletterUserPreferencesManager;
        this.helpUrls = helpUrls;
    }

    @Override
    public Jsonable get()
    {
        return new Jsonable()
        {
            @Override
            public void write(final Writer writer) throws IOException
            {
                try
                {
                    getJsonData().write(writer);
                }
                catch (JSONException e)
                {
                    throw new JsonMappingException(e);
                }
            }
        };
    }

    private JSONObject getJsonData()
    {
        final Map<String, Object> values = Maps.newHashMap();
        final Option<ApplicationUser> user = Option.option(authenticationContext.getUser());

        values.put("showNewsletterTip", newsletterUserPreferencesManager.shouldShowSignupTip(user));
        values.put("formUrl", helpUrls.getUrl("newsletter.signup.form").getUrl());
        if (user.isDefined())
        {
            values.put("userEmail", user.get().getEmailAddress());
        }
        return new JSONObject(values);
    }
}