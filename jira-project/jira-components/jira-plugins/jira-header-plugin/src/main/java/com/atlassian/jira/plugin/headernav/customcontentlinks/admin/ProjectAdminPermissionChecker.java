package com.atlassian.jira.plugin.headernav.customcontentlinks.admin;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;

import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;

public class ProjectAdminPermissionChecker
{
    private PermissionManager permissionManager;
    private ProjectManager projectManager;
    private UserManager userManager;

    public ProjectAdminPermissionChecker(PermissionManager permissionManager, ProjectManager projectManager, UserManager userManager) {
        this.permissionManager = permissionManager;
        this.projectManager = projectManager;
        this.userManager = userManager;
    }

    public boolean canAdminister(String projectKey, String userName) {
        Project jiraProject = projectManager.getProjectObjByKey(projectKey);
        if (jiraProject != null) {
            ApplicationUser applicationUser = userManager.getUserByName(userName);
            if (applicationUser != null) {
                return permissionManager.hasPermission(ADMINISTER_PROJECTS, jiraProject, applicationUser);
            }
        }
        return false;
    }
}
