package com.atlassian.jira.plugin.headernav.newsletter;

import com.atlassian.core.AtlassianCoreException;
import com.atlassian.core.util.Clock;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.preferences.ExtendedPreferences;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * Manager to consult user preferences to determine if a newsletter signup tip should be shown.
 */
public class NewsletterUserPreferencesManager
{
    private static final Logger log = LoggerFactory.getLogger(NewsletterUserPreferencesManager.class);

    private static final String NEWSLETTER_SIGNUP_FIRST_VIEW_TIMESTAMP = "newsletter.signup.first.view";
    private static final String OLD_WHATS_NEW_DIALOG_SHOWN_FLAG = "jira.user.whats.new.dont.show.version";
    private static final String NEWSLETTER_SIGNUP_DELAY = "jira.newsletter.tip.delay.days";
    private static final int NEWSLETTER_DEFAULT_DELAY_DAYS = 7;

    private final ApplicationProperties applicationProperties;
    private final UserPreferencesManager userPreferencesManager;
    private final Clock clock;

    public NewsletterUserPreferencesManager(
            final Clock clock,
            final ApplicationProperties applicationProperties,
            final UserPreferencesManager userPreferencesManager)
    {
        this.clock = clock;
        this.applicationProperties = applicationProperties;
        this.userPreferencesManager = userPreferencesManager;
    }

    /**
     * Returns true if it's been more than 7 days after the first time they logged in. The Newsletter signup tip should
     * only show after this duration.
     * <p/>
     * For anonymous users this method will always return false.
     * <p/>
     * One side effect of calling this method is that the current time will be stored against a
     * 'newsletter.signup.first.view' user preference for the provided user that will be used in subsequent calls to
     * determine if enough time has elapsed before the newsletter signup tip can be shown.
     *
     * @param user Current user. May be empty for anonymous users.
     * @return true if a user if it's been 7 days since the first time a user has accessed JIRA.
     */
    public boolean shouldShowSignupTip(final Option<ApplicationUser> user)
    {
        //Never show the newsletter signup tip to anonymous users.
        if (user.isEmpty())
        {
            return false;
        }

        final ExtendedPreferences userPreferences = userPreferencesManager.getExtendedPreferences(user.get());
        if (hasSeenWhatsNewDialog(userPreferences))
        {
            return false;
        }

        int delayInDays = getConfiguredNewsletterDelay();
        if (delayInDays < 0)
        {
            return false;
        }

        final long currentTime = clock.getCurrentDate().getTime();
        final long signupFirstViewTimestamp = storeFirstViewTimestampIfNeeded(userPreferences, currentTime);
        return (currentTime - signupFirstViewTimestamp) >= TimeUnit.DAYS.toMillis(delayInDays);
    }

    private long storeFirstViewTimestampIfNeeded(final ExtendedPreferences userPreferences, final long currentTime)
    {
        if (!userPreferences.containsValue(NEWSLETTER_SIGNUP_FIRST_VIEW_TIMESTAMP))
        {
            try
            {
                userPreferences.setLong(NEWSLETTER_SIGNUP_FIRST_VIEW_TIMESTAMP, currentTime);
            }
            catch (AtlassianCoreException e)
            {
                log.error("Unknown error storing newsletter signup timestamp user preference.", e);
            }
        }
        return userPreferences.getLong(NEWSLETTER_SIGNUP_FIRST_VIEW_TIMESTAMP);
    }

    private int getConfiguredNewsletterDelay()
    {
        final String newsletterSignupDelayString = applicationProperties.getDefaultBackedString(NEWSLETTER_SIGNUP_DELAY);
        return NumberUtils.toInt(newsletterSignupDelayString, NEWSLETTER_DEFAULT_DELAY_DAYS);
    }

    private boolean hasSeenWhatsNewDialog(final ExtendedPreferences userPreferences)
    {
        return userPreferences.containsValue(OLD_WHATS_NEW_DIALOG_SHOWN_FLAG);
    }
}
