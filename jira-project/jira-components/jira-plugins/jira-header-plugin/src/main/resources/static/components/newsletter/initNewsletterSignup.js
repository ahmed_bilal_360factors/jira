define('jira/newsletter/init-signup', [
    'jira/newsletter/signuptip'
], function(
    NewsletterSignup
    ) {
    return function() {
        var newsletterSignupData = WRM.data.claim("com.atlassian.jira.jira-header-plugin:newsletter-signup-tip.newsletterSignup");

        if (newsletterSignupData && newsletterSignupData.showNewsletterTip) {
            NewsletterSignup.render({
                userEmail: newsletterSignupData.userEmail,
                formUrl: newsletterSignupData.formUrl
            });
        }
    }
});

AJS.$(function () {
    require('jira/newsletter/init-signup')();
});
