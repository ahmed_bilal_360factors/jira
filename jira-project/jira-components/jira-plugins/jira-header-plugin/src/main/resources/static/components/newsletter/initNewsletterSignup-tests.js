AJS.test.require(["com.atlassian.jira.jira-header-plugin:newsletter-signup-tip"], function () {
    module('Newsletter initialisation code', {
        setup: function () {
            this.newsLetterSignup = {
                render:sinon.spy()
            };
            this.mockedContext = AJS.test.mockableModuleContext();
            this.mockedContext.mock('jira/newsletter/signuptip', this.newsLetterSignup);
            sinon.stub(WRM.data, 'claim');
        },

        teardown: function () {
            WRM.data.claim.restore();
        }
    });

    test("Render function is called when dataprovider says so", function () {
        WRM.data.claim.withArgs("com.atlassian.jira.jira-header-plugin:newsletter-signup-tip.newsletterSignup").returns({
            showNewsletterTip: true
        });

        this.mockedContext.require('jira/newsletter/init-signup')();

        sinon.assert.called(this.newsLetterSignup.render);
    });

    test("Render function is *not* called when dataprovider says so", function () {
        WRM.data.claim.withArgs("com.atlassian.jira.jira-header-plugin:newsletter-signup-tip.newsletterSignup").returns({
            showNewsletterTip: false
        });

        this.mockedContext.require('jira/newsletter/init-signup')();

        sinon.assert.notCalled(this.newsLetterSignup.render);
    });
});