AJS.test.require(["com.atlassian.jira.jira-header-plugin:newsletter-signup-tip"], function () {
    module('Newsletter render code', {
        setup: function () {
            this.newsLetterSignup = require('jira/newsletter/signuptip');
            this.container = AJS.$("<div>").attr("id", "content").appendTo("#qunit-fixture");
            this.trigger = AJS.$("<div id=\"user-options\"></div>").appendTo("#qunit-fixture");

            this.sandbox = sinon.sandbox.create();
            this.sandbox.useFakeServer();
            this.sandbox.useFakeTimers();
        },

        teardown: function () {
            AJS.$("body").off("submit", "form.aui.insiders-signup-form"); //unbind the body submit handler to ensure consecutive tests don't interfere with each other

            this.sandbox.clock.tick(0);
            this.sandbox.restore();
        },

        _initNewsletterSignupDialog: function(email, url) {
            this.newsLetterSignup.render({
                userEmail: email,
                formUrl: url
            });

            this.sandbox.clock.tick(0); //give the helptip some time to show
        }
    });

    test("Test e-mail validation", function () {
        this._initNewsletterSignupDialog("test@example.com", "https://www.atlassian.com/newsletter/signup");

        this.container.find("input[name=jira-newsletter-user-email]").val("invalidemail!!!");
        this.container.find("form.aui.insiders-signup-form").submit();

        equal(this.container.find("input[name=jira-newsletter-user-email]").next(".error").text(), "newsletter.signup.tip.error.email", "Should have rendered an error about e-mail");
    });

    test("Valid e-mail submit triggers cross-domain request to address entered", function () {
        this._initNewsletterSignupDialog("valid@email.com", "https://www.atlassian.com/newsletter/signup/{0}");

        this.container.find("input[name=jira-newsletter-user-email]").val("valid@email.com");
        this.container.find("form.aui.insiders-signup-form").submit();

        equal(this.sandbox.server.requests[0].method, "POST", "Correct request method used");
        equal(this.sandbox.server.requests[0].url, "https://www.atlassian.com/newsletter/signup/valid@email.com", "Correct url used");
    });
});