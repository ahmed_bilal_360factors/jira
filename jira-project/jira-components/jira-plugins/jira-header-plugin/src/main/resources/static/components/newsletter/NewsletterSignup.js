define('jira/newsletter/signuptip', [
    'jquery'
], function(
    $
    ) {

    // taken from setup-mac-util.js
    // http://www.w3.org/TR/html5/forms.html#valid-e-mail-address without &
    var emailRegex = /^[a-zA-Z0-9.!#$%'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

    function validateEmail(email) {
        if (email.length > 255) {
            return false;
        }
        return emailRegex.test(email);
    }

    return {
        render : function (newsletterFormDetails, helpTipOptions) {
            var defaults = {
                id: "newsletter-signup-tip",
                title: AJS.I18n.getText('newsletter.signup.tip.title'),
                bodyHtml: JIRA.Templates.newsletterSignupTip({
                    userEmail: newsletterFormDetails.userEmail
                }),
                anchor: "#user-options",
                isSequence: false,
                showCloseButton: false
            };
            var tip = new AJS.HelpTip($.extend(defaults, helpTipOptions));
            tip.show();

            AJS.trigger('analyticsEvent', { name: "jira.newsletter.signuptip.shown" });

            var $body = $("body");
            $body.on("submit", "form.aui.insiders-signup-form", function (e) {
                e.preventDefault();

                var $form = $(this);
                $form.find(".error").remove();

                var $emailInput = $form.find("#jira-newsletter-user-email");
                var email = $emailInput.val();
                if (validateEmail(email)) {
                    $.ajax({
                        type: 'POST',
                        url: AJS.format(newsletterFormDetails.formUrl, encodeURI(email)),
                        dataType: 'json'
                    }).success(function() {
                        AJS.trigger('analyticsEvent', { name: "jira.newsletter.signuptip.submitted" });
                    }).error(function(xhr) {
                        AJS.trigger('analyticsEvent', {
                            name: "jira.newsletter.signuptip.error",
                            data: {
                                statusCode: xhr.status
                            }
                        });
                    });

                    if(newsletterFormDetails.userEmail !== email) {
                        AJS.trigger('analyticsEvent', { name: "jira.newsletter.signuptip.email.changed" });
                    }

                    tip.dismiss("newslettersubscribed");
                }
                else {
                    AJS.trigger('analyticsEvent', { name: "jira.newsletter.signuptip.email.validationerror" });
                    $emailInput.after(aui.form.fieldError({message: AJS.I18n.getText('newsletter.signup.tip.error.email')}));
                }
            });

            $body.on("click", "form.aui.insiders-signup-form a.cancel", function (e) {
                e.preventDefault();
                AJS.trigger('analyticsEvent', { name: "jira.newsletter.signuptip.dismissed" });
                tip.dismiss("newslettercancelled");
            });
        }
    };
});
