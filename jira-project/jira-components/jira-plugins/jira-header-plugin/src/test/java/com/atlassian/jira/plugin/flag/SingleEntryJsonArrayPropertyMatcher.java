package com.atlassian.jira.plugin.flag;

import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

class SingleEntryJsonArrayPropertyMatcher extends BaseMatcher<String>
{
    private String propertyKey;
    private String entry;

    public SingleEntryJsonArrayPropertyMatcher(String propertyKey, String entry)
    {
        this.propertyKey = propertyKey;
        this.entry = entry;
    }

    @Override
    public boolean matches(Object item)
    {
        if (item instanceof String)
        {
            try
            {
                JSONObject jsonObject = new JSONObject((String) item);
                JSONArray entryArray = jsonObject.getJSONArray(propertyKey);
                //noinspection SimplifiableIfStatement
                if (entryArray.length() != 1)
                {
                    return false;
                }
                return String.valueOf(entry).equals(entryArray.getString(0));
            }

            catch (JSONException ignored)
            {
            }
        }

        return false;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText(
                "a JSON object containing the entry "
                        + propertyKey + " : [" + entry
                        + "] at the top level");
    }
}
