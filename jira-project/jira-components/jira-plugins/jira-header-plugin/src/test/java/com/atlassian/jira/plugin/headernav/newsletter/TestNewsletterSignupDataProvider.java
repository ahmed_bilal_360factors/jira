package com.atlassian.jira.plugin.headernav.newsletter;

import com.atlassian.fugue.Option;
import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.io.StringWriter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class TestNewsletterSignupDataProvider
{
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private NewsletterUserPreferencesManager newsletterUserPreferencesManager;
    @Mock
    private HelpUrls helpUrls;
    @Mock
    private HelpUrl helpUrl;

    private ApplicationUser fred = new MockApplicationUser("fred", "Fred", "fred@example.com");
    private NewsletterSignupDataProvider dataProvider;

    @Before
    public void setUp() throws Exception
    {
        initMocks(this);

        when(helpUrl.getUrl()).thenReturn("http://www.atlassian.com/newsletter/{0}/signup");
        when(helpUrls.getUrl("newsletter.signup.form")).thenReturn(helpUrl);

        dataProvider = new NewsletterSignupDataProvider(authenticationContext, newsletterUserPreferencesManager, helpUrls);
    }

    @Test
    public void anonymousUserDoesntIncludeEmail() throws Exception
    {
        when(authenticationContext.getUser()).thenReturn(null);
        when(newsletterUserPreferencesManager.shouldShowSignupTip(Option.<ApplicationUser>none())).thenReturn(false);

        final JSONObject json = getJsonObject();
        assertFalse(json.getBoolean("showNewsletterTip"));
        assertFalse(json.has("userEmail"));
        assertEquals("http://www.atlassian.com/newsletter/{0}/signup", json.getString("formUrl"));
    }

    @Test
    public void userPassedToPreferencesManager() throws Exception
    {
        when(authenticationContext.getUser()).thenReturn(fred);
        when(newsletterUserPreferencesManager.shouldShowSignupTip(Option.option(fred))).thenReturn(true);

        final JSONObject json = getJsonObject();
        assertTrue(json.getBoolean("showNewsletterTip"));
        assertEquals("fred@example.com", json.getString("userEmail"));
        assertEquals("http://www.atlassian.com/newsletter/{0}/signup", json.getString("formUrl"));
    }

    private JSONObject getJsonObject() throws IOException, JSONException
    {
        final StringWriter writer = new StringWriter();
        dataProvider.get().write(writer);
        return new JSONObject(writer.toString());
    }
}