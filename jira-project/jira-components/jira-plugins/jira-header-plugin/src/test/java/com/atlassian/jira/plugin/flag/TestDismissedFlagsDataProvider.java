package com.atlassian.jira.plugin.flag;

import java.io.StringWriter;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.flag.FlagDismissalService;
import com.atlassian.json.marshal.Jsonable;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class TestDismissedFlagsDataProvider
{
    private static final String FLAG_KEY = "flag.key";

    @Mock private FlagDismissalService flagDismissalService;
    @Mock private JiraAuthenticationContext authenticationContext;
    @Mock private ApplicationUser user;

    @Before
    public void setup()
    {
        initMocks(this);
        when(authenticationContext.getUser()).thenReturn(user);
    }

    @Test
    public void testGetWritesDismissedFlagsForCurrentUser() throws Exception
    {
        when(flagDismissalService.getDismissedFlagsForUser(user))
                .thenReturn(newHashSet(FLAG_KEY));

        StringWriter writer = new StringWriter();
        Jsonable dismissals = new DismissedFlagsDataProvider(
                flagDismissalService, authenticationContext).get();
        dismissals.write(writer);

        assertThat(
                writer.toString(),
                isJsonWithArrayEntryContainingOnly("dismissed", FLAG_KEY));
    }

    private Matcher<String> isJsonWithArrayEntryContainingOnly(final String propertyKey, final String entry)
    {
        return new SingleEntryJsonArrayPropertyMatcher(propertyKey, entry);
    }

}
