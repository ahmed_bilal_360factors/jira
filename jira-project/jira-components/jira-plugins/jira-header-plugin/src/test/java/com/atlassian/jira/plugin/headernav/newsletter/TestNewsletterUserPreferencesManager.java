package com.atlassian.jira.plugin.headernav.newsletter;

import com.atlassian.core.util.Clock;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.preferences.ExtendedPreferences;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import static com.atlassian.fugue.Option.option;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;


public class TestNewsletterUserPreferencesManager
{
    private static final long CURRENT_TIME = 10;

    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private UserPreferencesManager userPreferencesManager;
    @Mock
    private ExtendedPreferences extendedPreferences;
    @Mock
    private Clock clock;

    private ApplicationUser fred = new MockApplicationUser("fred");
    private NewsletterUserPreferencesManager manager;

    @Before
    public void setUp() throws Exception
    {
        initMocks(this);

        when(userPreferencesManager.getExtendedPreferences(fred)).thenReturn(extendedPreferences);
        when(clock.getCurrentDate()).thenReturn(new Date(CURRENT_TIME));

        manager = new NewsletterUserPreferencesManager(clock, applicationProperties, userPreferencesManager);
    }

    @Test
    public void anonymousUsersDontSeeNewsletterTip() throws Exception
    {
        assertFalse(manager.shouldShowSignupTip(Option.<ApplicationUser>none()));
    }

    @Test
    public void previousWhatsNewDialogUsageHidesNewsletterTip() throws Exception
    {
        when(extendedPreferences.containsValue("jira.user.whats.new.dont.show.version")).thenReturn(true);

        assertFalse(manager.shouldShowSignupTip(option(fred)));
    }

    @Test
    public void settingDelayToNegativeDaysDisablesNewsletterTip() throws Exception
    {
        when(extendedPreferences.containsValue("jira.user.whats.new.dont.show.version")).thenReturn(false);
        when(extendedPreferences.containsValue("newsletter.signup.first.view")).thenReturn(true);
        when(applicationProperties.getDefaultBackedString("jira.newsletter.tip.delay.days")).thenReturn("-1");

        assertFalse(manager.shouldShowSignupTip(option(fred)));
    }

    @Test
    public void newsletterTipNotShownOnFirstVisit() throws Exception
    {
        when(extendedPreferences.containsValue("jira.user.whats.new.dont.show.version")).thenReturn(false);
        when(extendedPreferences.containsValue("newsletter.signup.first.view")).thenReturn(false);

        assertFalse(manager.shouldShowSignupTip(option(fred)));
        verify(extendedPreferences).setLong(eq("newsletter.signup.first.view"), eq(CURRENT_TIME));
    }

    @Test
    public void settingDelayToZeroAlwaysShowsTip() throws Exception
    {
        when(extendedPreferences.containsValue("jira.user.whats.new.dont.show.version")).thenReturn(false);
        when(extendedPreferences.containsValue("newsletter.signup.first.view")).thenReturn(true);
        when(extendedPreferences.getLong("newsletter.signup.first.view")).thenReturn(CURRENT_TIME);
        when(applicationProperties.getDefaultBackedString("jira.newsletter.tip.delay.days")).thenReturn("0");

        assertTrue(manager.shouldShowSignupTip(option(fred)));
    }

    @Test
    public void settingDelayToInvalidFallsBackToSevenDays() throws Exception
    {
        when(extendedPreferences.containsValue("jira.user.whats.new.dont.show.version")).thenReturn(false);
        when(extendedPreferences.containsValue("newsletter.signup.first.view")).thenReturn(true);
        when(extendedPreferences.getLong("newsletter.signup.first.view")).thenReturn(CURRENT_TIME);
        when(applicationProperties.getDefaultBackedString("jira.newsletter.tip.delay.days")).thenReturn("invalid!!");

        assertFalse(manager.shouldShowSignupTip(option(fred)));

        //fast forward a day!
        when(clock.getCurrentDate()).thenReturn(new Date(TimeUnit.DAYS.toMillis(7) + CURRENT_TIME + 1L));
        assertTrue(manager.shouldShowSignupTip(option(fred)));
    }

    @Test
    public void settingDelayToPositiveShowsTipAfterTimeout() throws Exception
    {
        when(extendedPreferences.containsValue("jira.user.whats.new.dont.show.version")).thenReturn(false);
        when(extendedPreferences.containsValue("newsletter.signup.first.view")).thenReturn(true);
        when(extendedPreferences.getLong("newsletter.signup.first.view")).thenReturn(CURRENT_TIME);
        when(applicationProperties.getDefaultBackedString("jira.newsletter.tip.delay.days")).thenReturn("1");

        assertFalse(manager.shouldShowSignupTip(option(fred)));

        //fast forward a day!
        when(clock.getCurrentDate()).thenReturn(new Date(TimeUnit.DAYS.toMillis(1) + CURRENT_TIME + 1L));
        assertTrue(manager.shouldShowSignupTip(option(fred)));
    }
}