package com.atlassian.jira.gadgets.system.crossselling;

import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

import com.atlassian.core.util.Clock;
import com.atlassian.extras.api.ProductLicense;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class LicenseExpirationMessageFormatterTest
{
    public static final String EXPIRED = "expired";
    @Mock
    DateTimeFormatter dateTimeFormatter;

    @Mock
    Clock clock;

    @Mock
    I18nHelper.BeanFactory i18Factory;

    @Mock
    JiraAuthenticationContext authenticationContext;

    @Mock
    I18nHelper i18nHelper;

    @Mock
    ProductLicense productLicense;

    @InjectMocks
    LicenseExpirationMessageFormatter testObj;

    ResourceBundle resourceBundle = new ReturnEmptyStringBundle();

    @Before
    public void setUp() throws Exception
    {
        when(i18nHelper.getLocale()).thenReturn(null);
        when(i18nHelper.getText("admin.license.expired")).thenReturn(EXPIRED);
        when(i18nHelper.getDefaultResourceBundle()).thenReturn(resourceBundle);

        when(clock.getCurrentDate()).thenReturn(new Date());
    }

    @Test
    public void shouldReturnExpiredStringForExpired() throws Exception
    {
        // given
        when(productLicense.getExpiryDate()).thenReturn(new Date());
        when(productLicense.isExpired()).thenReturn(true);

        // when
        final String licenseExpiryStatusMessage = testObj.getLicenseExpiryStatusMessage(i18nHelper, productLicense);

        // then
        assertThat(licenseExpiryStatusMessage, is("(" + EXPIRED + ")"));
    }

    @Test
    public void shouldReturnEmptyStringForLicenseWithoutExpiryDate() throws Exception
    {
        // given
        when(productLicense.getExpiryDate()).thenReturn(null);

        // when
        final String licenseExpiryStatusMessage = testObj.getLicenseExpiryStatusMessage(i18nHelper, productLicense);

        // then
        assertThat(licenseExpiryStatusMessage, is(""));
    }

    @Test
    public void shouldReturnInformationOnValidLicense() throws Exception
    {
        // given
        Date someDate = new Date();
        when(productLicense.getExpiryDate()).thenReturn(someDate);
        when(productLicense.isExpired()).thenReturn(false);
        when(dateTimeFormatter.withLocale(any(Locale.class))).thenReturn(dateTimeFormatter);
        when(dateTimeFormatter.withStyle(any(DateTimeStyle.class))).thenReturn(dateTimeFormatter);
        final String dateFormat = "!@#$";
        when(dateTimeFormatter.format(someDate)).thenReturn(dateFormat);
        final String finalFormat = "EXPIRATION INFO";
        when(i18nHelper.getText(eq("admin.license.expiresin"), any(String.class), eq(dateFormat))).thenReturn(finalFormat);

        // when
        final String licenseExpiryStatusMessage = testObj.getLicenseExpiryStatusMessage(i18nHelper, productLicense);

        // then
        assertThat(licenseExpiryStatusMessage, is("(" + finalFormat + ")"));
    }

    public static class ReturnEmptyStringBundle extends ResourceBundle
    {
        @Override
        protected Object handleGetObject(final String key)
        {
            return "";
        }

        @Override
        public Enumeration<String> getKeys()
        {
            return null;
        }
    }
}