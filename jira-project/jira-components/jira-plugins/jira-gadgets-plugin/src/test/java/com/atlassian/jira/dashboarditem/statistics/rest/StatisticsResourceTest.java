package com.atlassian.jira.dashboarditem.statistics.rest;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.Response;

import com.atlassian.fugue.Either;
import com.atlassian.jira.dashboarditem.statistics.rest.StatisticsResource;
import com.atlassian.jira.dashboarditem.statistics.searcher.beans.StatisticsSearchResultBean;
import com.atlassian.jira.dashboarditem.statistics.searcher.beans.StatisticsSearchResultRowBean;
import com.atlassian.jira.dashboarditem.statistics.service.StatisticsResult;
import com.atlassian.jira.dashboarditem.statistics.service.StatisticsService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;

import org.apache.commons.httpclient.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class StatisticsResourceTest
{
    @Mock
    private StatisticsService statisticsService;

    @Mock
    private JiraAuthenticationContext authenticationContext;


    @Mock
    private ApplicationUser user;

    private StatisticsResource statisticsResource;

    @Before
    public void setUp()
    {
        statisticsResource = new StatisticsResource(authenticationContext, statisticsService);
        when(authenticationContext.getUser()).thenReturn(user);
    }


    @Test
    public void testSuccessfulSearchReturns200()
    {
        final List<StatisticsSearchResultRowBean> statisticRows = new ArrayList<StatisticsSearchResultRowBean>();
        statisticRows.add(new StatisticsSearchResultRowBean("key", 20, "url", new Long(5)));

        final StatisticsSearchResultBean statisticsSearchResultBean = new StatisticsSearchResultBean(10, statisticRows);
        final StatisticsResult statisticsResult = new StatisticsResult("Title", "url", "statType", statisticsSearchResultBean);

        Either<ErrorCollection, StatisticsResult> result = Either.right(statisticsResult);
        when(statisticsService.aggregateOneDimensionalStats(eq(user), anyString(), anyString())).thenReturn(result);

        Response response = statisticsResource.oneDimensionalSearch("jql", "statType");
        assertThat(response.getStatus(), is(200));
        assertThat(response.getEntity(), is((Object)statisticsResult));
    }

    @Test
    public void testUnsuccesfulSearchReturnsErrorCodeFromReason()
    {
        List<String> errorMessages = Arrays.asList("One error", "Second error");
        ErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessages(errorMessages);
        errors.addReason(ErrorCollection.Reason.FORBIDDEN);
        Either<ErrorCollection, StatisticsResult> result = Either.left(errors);
        when(statisticsService.aggregateOneDimensionalStats(eq(user), anyString(), anyString())).thenReturn(result);

        Response response = statisticsResource.oneDimensionalSearch("jql", "statType");
        assertThat(response.getStatus(), is(HttpStatus.SC_FORBIDDEN));
        assertThat(response.getEntity(), is((Object)errorMessages));
    }
}
