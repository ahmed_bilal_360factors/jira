package com.atlassian.jira.dashboarditem.statistics.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.dashboarditem.statistics.searcher.StatisticsSearcher;
import com.atlassian.jira.dashboarditem.statistics.searcher.beans.StatisticsSearchResultBean;
import com.atlassian.jira.dashboarditem.statistics.searcher.beans.StatisticsSearchResultRowBean;
import com.atlassian.jira.dashboarditem.statistics.service.StatisticsResult;
import com.atlassian.jira.dashboarditem.statistics.service.StatisticsServiceImpl;
import com.atlassian.jira.gadgets.system.StatisticTypesProvider;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.SingleValueOperand;

import org.apache.commons.httpclient.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertTrue;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class StatisticsServiceImplTest
{

    @Mock
    private SearchService searchService;

    @Mock
    private StatisticsSearcher statisticsSearcher;

    @Mock
    private StatisticTypesProvider statisticTypesProvider;

    @Mock
    private ProjectService projectService;

    @Mock
    private SearchRequestService searchRequestService;

    @Mock
    private VelocityRequestContextFactory velocityRequestContextFactory;

    @Mock
    private VelocityRequestContext velocityRequestContext;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    ApplicationUser user;

    @Mock
    private User directoryUser;

    @Mock
    private Query query;

    @Mock
    private Clause clause;

    @Mock
    private TerminalClause terminalClause;

    @Mock
    private SearchRequest searchRequest;

    @Mock
    private Project project;

    private StatisticsServiceImpl statisticsService;

    private static final String BASE_URL = "/base";

    private static final String ENCODED = "<encoded>";

    @Before
    public void setUp()
    {
        statisticsService = Mockito.spy(new StatisticsServiceImpl(searchService, statisticsSearcher, statisticTypesProvider,
                projectService, searchRequestService, velocityRequestContextFactory, authenticationContext));


        doReturn(ENCODED).when(statisticsService).urlEncode(anyString());

        when(user.getDirectoryUser()).thenReturn(directoryUser);

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getBaseUrl()).thenReturn(BASE_URL);

        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);

        when(i18nHelper.getText(anyString())).then(returnsFirstArg());
        when(i18nHelper.getText(anyString(), Matchers.<Serializable[]>anyVararg())).then(returnsFirstArg());

        when(statisticTypesProvider.getDisplayName(anyString())).then(returnsFirstArg());
    }

    @Test
    public void testUnparseableJql()
    {
        MessageSet errors = new MessageSetImpl();
        errors.addErrorMessage("Some jql error");
        final SearchService.ParseResult parseResult = new SearchService.ParseResult(null, errors);
        when(searchService.parseQuery(eq(directoryUser), anyString())).thenReturn(parseResult);

        Either<ErrorCollection, StatisticsResult> result = statisticsService.aggregateOneDimensionalStats(user, "jql", "statType");

        assertTrue(result.isLeft());

        ErrorCollection errorCollection = result.left().get();

        assertThat(errorCollection.getReasons().size(), is(1));

        ErrorCollection.Reason reason = ErrorCollection.Reason.getWorstReason(errorCollection.getReasons());
        assertThat(reason.getHttpStatusCode(), is(HttpStatus.SC_BAD_REQUEST));
    }

    public void testInvalidJql()
    {
        //Set up
        final SearchService.ParseResult parseResult = new SearchService.ParseResult(query, new MessageSetImpl());
        when(searchService.parseQuery(eq(directoryUser), anyString())).thenReturn(parseResult);

        MessageSet errors = new MessageSetImpl();
        errors.addErrorMessage("Some jql error");
        when(searchService.validateQuery(eq(directoryUser), eq(query))).thenReturn(errors);

        Either<ErrorCollection, StatisticsResult> result = statisticsService.aggregateOneDimensionalStats(user, "jql", "statType");

        assertTrue(result.isLeft());

        ErrorCollection errorCollection = result.left().get();

        assertThat(errorCollection.getReasons().size(), is(1));

        ErrorCollection.Reason reason = ErrorCollection.Reason.getWorstReason(errorCollection.getReasons());
        assertThat(reason.getHttpStatusCode(), is(HttpStatus.SC_BAD_REQUEST));
    }

    public void testInvalidStatType()
    {
        //Set up
        final SearchService.ParseResult parseResult = new SearchService.ParseResult(query, new MessageSetImpl());
        when(searchService.parseQuery(eq(directoryUser), anyString())).thenReturn(parseResult);

        MessageSet errors = new MessageSetImpl();
        errors.addErrorMessage("Some jql error");
        when(searchService.validateQuery(eq(directoryUser), eq(query))).thenReturn(errors);

        when(statisticTypesProvider.getDisplayName(anyString())).then(null);

        Either<ErrorCollection, StatisticsResult> result = statisticsService.aggregateOneDimensionalStats(user, "jql", "statType");

        assertTrue(result.isLeft());

        ErrorCollection errorCollection = result.left().get();

        assertThat(errorCollection.getReasons().size(), is(1));

        ErrorCollection.Reason reason = ErrorCollection.Reason.getWorstReason(errorCollection.getReasons());
        assertThat(reason.getHttpStatusCode(), is(HttpStatus.SC_BAD_REQUEST));
    }

    @Test
    public void testSearchException() throws SearchException
    {

        final Query query = new QueryImpl();
        final SearchService.ParseResult parseResult = new SearchService.ParseResult(query, new MessageSetImpl());
        when(searchService.parseQuery(eq(directoryUser), anyString())).thenReturn(parseResult);
        when(searchService.validateQuery(eq(directoryUser), eq(query))).thenReturn(new MessageSetImpl());

        final String errorMessage = "message";
        SearchException searchException  = new SearchException(errorMessage);
        when(statisticsSearcher.completeOneDimensionalSearch(eq(user), eq(query), anyString())).thenThrow(searchException);

        Either<ErrorCollection, StatisticsResult> result = statisticsService.aggregateOneDimensionalStats(user, "jql", "statType");

        assertTrue(result.isLeft());

        ErrorCollection errorCollection = result.left().get();

        assertThat(errorCollection.getErrorMessages().size(), is(1));
        assertTrue(errorCollection.getErrorMessages().contains(errorMessage));

        assertThat(errorCollection.getReasons().size(), is(1));

        ErrorCollection.Reason reason = ErrorCollection.Reason.getWorstReason(errorCollection.getReasons());
        assertThat(reason.getHttpStatusCode(), is(HttpStatus.SC_INTERNAL_SERVER_ERROR));
    }

    @Test
    public void testSuccessfulJqlSearch() throws SearchException
    {
        long RESULT_COUNT = 10;

        final SearchService.ParseResult parseResult = new SearchService.ParseResult(query, new MessageSetImpl());
        when(searchService.parseQuery(eq(directoryUser), anyString())).thenReturn(parseResult);
        when(searchService.validateQuery(eq(directoryUser), eq(query))).thenReturn(new MessageSetImpl());

        final List<StatisticsSearchResultRowBean> statisticRows = new ArrayList<StatisticsSearchResultRowBean>();
        statisticRows.add(new StatisticsSearchResultRowBean("key", 20, "url", new Long(5)));

        final StatisticsSearchResultBean statisticsSearchResultBean = new StatisticsSearchResultBean(RESULT_COUNT, statisticRows);

        when(query.getWhereClause()).thenReturn(clause);

        when(statisticsSearcher.completeOneDimensionalSearch(eq(user), eq(query), anyString())).thenReturn(statisticsSearchResultBean);

        Either<ErrorCollection, StatisticsResult> result = statisticsService.aggregateOneDimensionalStats(user, "jql", "statType");

        assertTrue(result.isRight());

        final StatisticsResult statisticsResult = result.right().get();

        assertThat(statisticsResult.getFilterUrl(), is(BASE_URL + "/issues/?jql=" + ENCODED));
        assertThat(statisticsResult.getFilterTitle(), is("jira.jql.query"));
        assertThat(statisticsResult.getIssueCount(), is((long)RESULT_COUNT));
        assertThat(statisticsResult.getResults(), is(statisticRows));
    }

    @Test
    public void testSuccessfulFilterSearch() throws SearchException
    {
        //Set up
        long RESULT_COUNT = 10;
        long FILTER_ID = 10;
        String filterName = "filterName";

        final SearchService.ParseResult parseResult = new SearchService.ParseResult(query, new MessageSetImpl());
        when(searchService.parseQuery(eq(directoryUser), anyString())).thenReturn(parseResult);
        when(searchService.validateQuery(eq(directoryUser), eq(query))).thenReturn(new MessageSetImpl());

        final List<StatisticsSearchResultRowBean> statisticRows = new ArrayList<StatisticsSearchResultRowBean>();
        statisticRows.add(new StatisticsSearchResultRowBean("key", 20, "url", new Long(5)));

        final StatisticsSearchResultBean statisticsSearchResultBean = new StatisticsSearchResultBean(RESULT_COUNT, statisticRows);

        when(query.getWhereClause()).thenReturn(clause);

        when(statisticsSearcher.completeOneDimensionalSearch(eq(user), eq(query), anyString())).thenReturn(statisticsSearchResultBean);

        doReturn(true).when(statisticsService).isFilterOnlyClause(eq(query));
        doReturn(terminalClause).when(statisticsService).getFilterClause(eq(query));

        final SingleValueOperand singleValueOperand = new SingleValueOperand(FILTER_ID);

        when(terminalClause.getOperand()).thenReturn(singleValueOperand);

        when (searchRequestService.getFilter(any(JiraServiceContextImpl.class), anyLong())).thenReturn(searchRequest);

        when(searchRequest.getName()).thenReturn(filterName);

        //Complete
        Either<ErrorCollection, StatisticsResult> result = statisticsService.aggregateOneDimensionalStats(user, "jql", "statType");


        //Test
        assertTrue(result.isRight());

        final StatisticsResult statisticsResult = result.right().get();

        assertThat(statisticsResult.getFilterUrl(), is(BASE_URL + "/issues/?filter=" + FILTER_ID));
        assertThat(statisticsResult.getFilterTitle(), is(filterName));
        assertThat(statisticsResult.getIssueCount(), is((long)RESULT_COUNT));
        assertThat(statisticsResult.getResults(), is(statisticRows));
    }

    @Test
    public void testSuccessfulProjectSearchWithId() throws SearchException
    {
        //Set up
        final long RESULT_COUNT = 10;
        final long PROJECT_ID = 10;
        final String PROJECT_NAME = "projectName";

        final SearchService.ParseResult parseResult = new SearchService.ParseResult(query, new MessageSetImpl());
        when(searchService.parseQuery(eq(directoryUser), anyString())).thenReturn(parseResult);
        when(searchService.validateQuery(eq(directoryUser), eq(query))).thenReturn(new MessageSetImpl());

        final List<StatisticsSearchResultRowBean> statisticRows = new ArrayList<StatisticsSearchResultRowBean>();
        statisticRows.add(new StatisticsSearchResultRowBean("key", 20, "url", new Long(5)));

        final StatisticsSearchResultBean statisticsSearchResultBean = new StatisticsSearchResultBean(RESULT_COUNT, statisticRows);

        when(query.getWhereClause()).thenReturn(clause);

        when(statisticsSearcher.completeOneDimensionalSearch(eq(user), eq(query), anyString())).thenReturn(statisticsSearchResultBean);

        doReturn(false).when(statisticsService).isFilterOnlyClause(eq(query));
        doReturn(true).when(statisticsService).isProjectOnlyClause(eq(query));
        doReturn(terminalClause).when(statisticsService).getProjectClause(eq(query));

        final SingleValueOperand singleValueOperand = new SingleValueOperand(PROJECT_ID);

        when(terminalClause.getOperand()).thenReturn(singleValueOperand);

        ProjectService.GetProjectResult projectResult = new ProjectService.GetProjectResult(project);

        when(project.getName()).thenReturn(PROJECT_NAME);

        when(projectService.getProjectById(eq(user), eq(PROJECT_ID))).thenReturn(projectResult);


        //Complete
        Either<ErrorCollection, StatisticsResult> result = statisticsService.aggregateOneDimensionalStats(user, "jql", "statType");




        //Test
        assertTrue(result.isRight());

        final StatisticsResult statisticsResult = result.right().get();

        assertThat(statisticsResult.getFilterUrl(), is(BASE_URL + "/issues/?jql=" + ENCODED));
        assertThat(statisticsResult.getFilterTitle(), is(PROJECT_NAME));
        assertThat(statisticsResult.getIssueCount(), is((long)RESULT_COUNT));
        assertThat(statisticsResult.getResults(), is(statisticRows));
    }


    @Test
    public void testSuccessfulProjectSearchWithKey() throws SearchException
    {
        //Set up
        final long RESULT_COUNT = 10;
        final String PROJECT_KEY = "QA";
        final String PROJECT_NAME = "projectName";

        final SearchService.ParseResult parseResult = new SearchService.ParseResult(query, new MessageSetImpl());
        when(searchService.parseQuery(eq(directoryUser), anyString())).thenReturn(parseResult);
        when(searchService.validateQuery(eq(directoryUser), eq(query))).thenReturn(new MessageSetImpl());

        final List<StatisticsSearchResultRowBean> statisticRows = new ArrayList<StatisticsSearchResultRowBean>();
        statisticRows.add(new StatisticsSearchResultRowBean("key", 20, "url", new Long(5)));

        final StatisticsSearchResultBean statisticsSearchResultBean = new StatisticsSearchResultBean(RESULT_COUNT, statisticRows);

        when(query.getWhereClause()).thenReturn(clause);

        when(statisticsSearcher.completeOneDimensionalSearch(eq(user), eq(query), anyString())).thenReturn(statisticsSearchResultBean);

        doReturn(false).when(statisticsService).isFilterOnlyClause(eq(query));
        doReturn(true).when(statisticsService).isProjectOnlyClause(eq(query));
        doReturn(terminalClause).when(statisticsService).getProjectClause(eq(query));

        final SingleValueOperand singleValueOperand = new SingleValueOperand(PROJECT_KEY);

        when(terminalClause.getOperand()).thenReturn(singleValueOperand);

        ProjectService.GetProjectResult projectResult = new ProjectService.GetProjectResult(project);

        when(project.getName()).thenReturn(PROJECT_NAME);

        when(projectService.getProjectByKey(eq(user), eq(PROJECT_KEY))).thenReturn(projectResult);


        //Complete
        Either<ErrorCollection, StatisticsResult> result = statisticsService.aggregateOneDimensionalStats(user, "jql", "statType");




        //Test
        assertTrue(result.isRight());

        final StatisticsResult statisticsResult = result.right().get();

        assertThat(statisticsResult.getFilterUrl(), is(BASE_URL + "/issues/?jql=" + ENCODED));
        assertThat(statisticsResult.getFilterTitle(), is(PROJECT_NAME));
        assertThat(statisticsResult.getIssueCount(), is((long)RESULT_COUNT));
        assertThat(statisticsResult.getResults(), is(statisticRows));
    }
}
