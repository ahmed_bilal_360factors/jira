package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import junit.framework.TestCase;
import org.easymock.classextension.EasyMock;

import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;
import static org.easymock.EasyMock.expect;
import static org.easymock.classextension.EasyMock.createMock;
import static org.easymock.classextension.EasyMock.createNiceMock;

/**
 * Tests QuickLinksResource
 *
 * @since v4.0
 */
public class TestQuickLinksResource extends TestCase
{
    private JiraAuthenticationContext ctx;
    private GlobalPermissionManager gprms;
    private ApplicationUser mockUser;
    private PermissionManager prms;
    private ApplicationProperties props;
    private SearchService search;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        mockUser = new MockApplicationUser("admin");
        ctx = createNiceMock(JiraAuthenticationContext.class);
        gprms = createNiceMock(GlobalPermissionManager.class);
        prms = createNiceMock(PermissionManager.class);
        props = createNiceMock(ApplicationProperties.class);
        search = createNiceMock(SearchService.class);
        TimeZoneManager timeZoneManager = createMock(TimeZoneManager.class);
        MockComponentWorker componentWorker = new MockComponentWorker();
        componentWorker.registerMock(TimeZoneManager.class, timeZoneManager);
        ComponentAccessor.initialiseWorker(componentWorker);
    }

    public void testAdmin() throws Exception
    {
        expect(ctx.getUser()).andReturn(mockUser);
        expect(prms.hasProjects(BROWSE_PROJECTS, mockUser)).andReturn(true);
        expect(prms.hasProjects(CREATE_ISSUES, mockUser)).andReturn(true);
        expect(prms.hasProjects(ADMINISTER_PROJECTS, mockUser)).andReturn(true);
        expect(gprms.hasPermission(ADMINISTER, mockUser)).andReturn(true);

        expect(props.getOption("jira.option.voting")).andReturn(true);
        expect(props.getOption("jira.option.watching")).andReturn(true);

        EasyMock.replay(ctx, gprms, prms, props, search);
        QuickLinksResource resource = new QuickLinksResource(ctx, gprms, prms, props, search);
        Response res = resource.getQuickLinks();
        QuickLinksResource.LinkLists lists = (QuickLinksResource.LinkLists) res.getEntity();
        assertNotNull(lists);

        assertLinksExistsWithTitles(lists.getCommonLinks(),
                "gadget.quicklinks.reported.issues",
                "gadget.quicklinks.voted.issues",
                "gadget.quicklinks.watched.issues");

        assertLinksExistsWithTitles(lists.getNavigationLinks(),
                "gadget.quicklinks.browse.projects",
                "gadget.quicklinks.find.issues",
                "gadget.quicklinks.create.issue",
                "gadget.quicklinks.adminstration");
    }

    public void testAdminNoVotingOrWatching() throws Exception
    {
        expect(ctx.getUser()).andReturn(mockUser);
        expect(prms.hasProjects(BROWSE_PROJECTS, mockUser)).andReturn(true);
        expect(prms.hasProjects(CREATE_ISSUES, mockUser)).andReturn(true);
        expect(prms.hasProjects(ADMINISTER_PROJECTS, mockUser)).andReturn(true);
        expect(gprms.hasPermission(ADMINISTER, mockUser)).andReturn(true);

        expect(props.getOption("jira.option.voting")).andReturn(false);
        expect(props.getOption("jira.option.watching")).andReturn(false);

        EasyMock.replay(ctx, gprms, prms, props, search);
        QuickLinksResource resource = new QuickLinksResource(ctx, gprms, prms, props, search);
        Response res = resource.getQuickLinks();
        QuickLinksResource.LinkLists lists = (QuickLinksResource.LinkLists) res.getEntity();
        assertNotNull(lists);

        assertLinksExistsWithTitles(lists.getCommonLinks(),
                "gadget.quicklinks.reported.issues");

        assertLinksExistsWithTitles(lists.getNavigationLinks(),
                "gadget.quicklinks.browse.projects",
                "gadget.quicklinks.find.issues",
                "gadget.quicklinks.create.issue",
                "gadget.quicklinks.adminstration");
    }

    public void testUserLinks() throws Exception
    {
        expect(ctx.getUser()).andReturn(mockUser);
        expect(prms.hasProjects(BROWSE_PROJECTS, mockUser)).andReturn(true);
        expect(prms.hasProjects(CREATE_ISSUES, mockUser)).andReturn(true);
        expect(prms.hasProjects(ADMINISTER_PROJECTS, mockUser)).andReturn(false);
        expect(gprms.hasPermission(ADMINISTER, mockUser)).andReturn(false);

        expect(props.getOption("jira.option.voting")).andReturn(true);
        expect(props.getOption("jira.option.watching")).andReturn(true);

        EasyMock.replay(ctx, gprms, prms, props, search);
        QuickLinksResource resource = new QuickLinksResource(ctx, gprms, prms, props, search);
        Response res = resource.getQuickLinks();
        QuickLinksResource.LinkLists lists = (QuickLinksResource.LinkLists) res.getEntity();
        assertNotNull(lists);

        assertLinksExistsWithTitles(lists.getCommonLinks(),
                "gadget.quicklinks.reported.issues",
                "gadget.quicklinks.voted.issues",
                "gadget.quicklinks.watched.issues");

        assertLinksExistsWithTitles(lists.getNavigationLinks(),
                "gadget.quicklinks.browse.projects",
                "gadget.quicklinks.find.issues",
                "gadget.quicklinks.create.issue");
    }

    public void testProjectAdmin() throws Exception
    {
        expect(ctx.getUser()).andReturn(mockUser);
        expect(prms.hasProjects(BROWSE_PROJECTS, mockUser)).andReturn(true);
        expect(prms.hasProjects(CREATE_ISSUES, mockUser)).andReturn(true);
        expect(prms.hasProjects(ADMINISTER_PROJECTS, mockUser)).andReturn(true);
        expect(gprms.hasPermission(ADMINISTER, mockUser)).andReturn(false);

        expect(props.getOption("jira.option.voting")).andReturn(true);
        expect(props.getOption("jira.option.watching")).andReturn(true);

        EasyMock.replay(ctx, gprms, prms, props, search);
        QuickLinksResource resource = new QuickLinksResource(ctx, gprms, prms, props, search);
        Response res = resource.getQuickLinks();
        QuickLinksResource.LinkLists lists = (QuickLinksResource.LinkLists) res.getEntity();
        assertNotNull(lists);

        assertLinksExistsWithTitles(lists.getCommonLinks(),
                "gadget.quicklinks.reported.issues",
                "gadget.quicklinks.voted.issues",
                "gadget.quicklinks.watched.issues");

        assertLinksExistsWithTitles(lists.getNavigationLinks(),
                "gadget.quicklinks.browse.projects",
                "gadget.quicklinks.find.issues",
                "gadget.quicklinks.create.issue");
    }

    public void testAdminNoProjects() throws Exception
    {
        expect(ctx.getUser()).andReturn(mockUser);
        expect(prms.hasProjects(BROWSE_PROJECTS, mockUser)).andReturn(true);
        expect(prms.hasProjects(CREATE_ISSUES, mockUser)).andReturn(true);
        expect(prms.hasProjects(ADMINISTER_PROJECTS, mockUser)).andReturn(false);
        expect(gprms.hasPermission(ADMINISTER, mockUser)).andReturn(true);

        expect(props.getOption("jira.option.voting")).andReturn(true);
        expect(props.getOption("jira.option.watching")).andReturn(true);

        EasyMock.replay(ctx, gprms, prms, props, search);
        QuickLinksResource resource = new QuickLinksResource(ctx, gprms, prms, props, search);
        Response res = resource.getQuickLinks();
        QuickLinksResource.LinkLists lists = (QuickLinksResource.LinkLists) res.getEntity();
        assertNotNull(lists);

        assertLinksExistsWithTitles(lists.getCommonLinks(),
                "gadget.quicklinks.reported.issues",
                "gadget.quicklinks.voted.issues",
                "gadget.quicklinks.watched.issues");

        assertLinksExistsWithTitles(lists.getNavigationLinks(),
                "gadget.quicklinks.browse.projects",
                "gadget.quicklinks.find.issues",
                "gadget.quicklinks.create.issue");
    }

    public void testAnonymous() throws Exception
    {
        expect(ctx.getUser()).andReturn(null);
        expect(prms.hasProjects(BROWSE_PROJECTS, mockUser)).andReturn(true);
        expect(prms.hasProjects(CREATE_ISSUES, mockUser)).andReturn(false);
        expect(prms.hasProjects(ADMINISTER_PROJECTS, mockUser)).andReturn(false);
        expect(gprms.hasPermission(ADMINISTER, mockUser)).andReturn(false);

        expect(props.getOption("jira.option.voting")).andReturn(true);
        expect(props.getOption("jira.option.watching")).andReturn(true);

        EasyMock.replay(ctx, gprms, prms, props, search);
        QuickLinksResource resource = new QuickLinksResource(ctx, gprms, prms, props, search);
        Response res = resource.getQuickLinks();
        assertTrue(res.getEntity() instanceof QuickLinksResource.Warning);
    }

    void assertLinksExistsWithTitles(Collection<QuickLinksResource.Link> links, String... titles)
    {
        assertEquals("Should be the same number of links as expected", titles.length, links.size());
        Set<String> expectedTitles = new HashSet<String>(Arrays.asList(titles));
        for (QuickLinksResource.Link link : links)
        {
            expectedTitles.remove(link.getText());
        }
        assertEquals("Expected titles not found: " + expectedTitles, 0, expectedTitles.size());
    }
}
