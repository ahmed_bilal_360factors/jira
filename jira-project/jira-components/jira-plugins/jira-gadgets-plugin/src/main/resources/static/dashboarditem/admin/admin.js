define("jira-dashboard-items/admin", [
    'jquery'
], function (
        $
) {
    function installGreenHopper(event) {
        event.preventDefault();
        AJS.gadget.plugin.installer("com.pyxis.greenhopper.jira-key", "/secure/SetupGreenHopper!default.jspa");
    }

    function installBonfire(event) {
        event.preventDefault();
        AJS.gadget.plugin.installer("com.atlassian.bonfire.plugin-key", "/secure/SetupBonfire!default.jspa");
    }

    function taskCheckboxClicked(element) {
        var $element = $(element);
        var $task = $element.parents(".admin-task");

        if ($task.hasClass("admin-task-disabled")) {
            return;
        }

        var $taskList = $element.parents(".admin-task-list");
        var $taskListContainer = $element.parents(".admin-task-list-container");
        var taskName = $task.find(".admin-task-content").data("name");
        var eventName;
        var restUrl;

        if ($task.hasClass("admin-task-completed")) {
            $task.removeClass("admin-task-completed");
            eventName = "xlane.admingadget.task." + taskName + ".unchecked";
            restUrl = AJS.contextPath() + "/rest/gadget/1.0/admin/task/undone";
        } else {
            $task.addClass("admin-task-completed");
            eventName = "xlane.admingadget.task." + taskName + ".checked";
            restUrl = AJS.contextPath() + "/rest/gadget/1.0/admin/task/done";
        }

        if ($taskList.find(".admin-task:not(.admin-task-completed)").length === 0) {
            // All tasks are completed
            $taskListContainer.find(".hide-admin-task-list-completed").show();
            $taskListContainer.find(".hide-admin-task-list-not-completed").hide();
        } else {
            // Not all tasks are completed
            $taskListContainer.find(".hide-admin-task-list-not-completed").show();
            $taskListContainer.find(".hide-admin-task-list-completed").hide();
        }

        $.ajax({
            url: restUrl + "?name=" + taskName,
            type: "PUT",
            global: false
        });

        AJS.trigger('analyticsEvent', {
            name: eventName
        });
    }

    function showTaskList(element, gadget, event, API) {
        event.preventDefault();

        var $element = $(element);

        var $gadget = $(gadget);
        var name = $element.parents(".admin-task-list-unhide").attr("aria-owns");
        var $taskList = $gadget.find(".admin-task-container").filter("[data-name=" + name + "]");
        var $separator = $gadget.find('.unhide-separator');
        $separator.hide();

        var $unhideContainer = $element.parents(".admin-task-list-unhide");
        $unhideContainer.hide();
        $taskList.slideDown("slow", function() { API.forceLayoutRefresh(); });

        $.ajax({
            url: AJS.contextPath() + "/rest/gadget/1.0/admin/tasklist/undone?name=" + name,
            type: "PUT",
            global: false
        });

        var eventName = "xlane.admingadget.tasklist." + name + ".unhide";

        AJS.trigger('analyticsEvent', {
            name: eventName
        });
    }

    function taskContentClicked(element, event) {
        var $task = $(element);
        if ($task.hasClass("admin-task-disabled")) {
            event.preventDefault();
            return;
        }

        var taskName = $(this).data("name");

        AJS.trigger('analyticsEvent', {
            name: "xlane.admingadget.task." + taskName + ".clicked"
        });
    }

    function createIssueClicked(event) {
        event.preventDefault();
        var $task = $(this);
        if ($task.hasClass("admin-task-disabled")) {
            return;
        }

        AJS.$('.create-issue').click();
    }

    function hideTaskList(element, gadget, event, API) {
        event.preventDefault();
        var $element = $(element);
        var $gadget = $(gadget);

        var $container = $element.parents(".admin-task-container");
        var name = $container.data("name");
        var $unhideContainer = $gadget.find('.admin-task-list-unhide').filter('[aria-owns=' + name + ']');
        var eventName = "xlane.admingadget.tasklist." + name + ".hide";
        var $separator = $gadget.find('.unhide-separator');

        if ($gadget.find('.admin-task-list-unhide :visible').length == 1) {
            $separator.show();
        }
        $container.slideUp("slow", function() { API.forceLayoutRefresh(); });
        $unhideContainer.show();

        $.ajax({
            url: AJS.contextPath() + "/rest/gadget/1.0/admin/tasklist/done?name=" + name,
            type: "PUT",
            global: false
        });

        var completed = Boolean($element.parents(".hide-admin-task-list-completed").length);
        AJS.trigger('analyticsEvent', {
            name: eventName,
            properties: {
                completed: completed
            }
        });
    }

    var DashboardItem = function(API) {
        this.API = API;
    };

    DashboardItem.prototype.render = function (element) {
        var $element = $(element);
        var API = this.API;

        $element.on("click", ".greenHopperInstall", installGreenHopper);
        $element.on("click", ".bonfireInstall", installBonfire);

        $element.on("click", ".greenHopperInstallOD", function (e) {
            taskCheckboxClicked(this, e);
        });

        $element.on("click", ".bonfireInstallOD", function () {
            taskCheckboxClicked(this);
        });

        $element.on("click", ".admin-task .admin-task-checkbox", function () {
            taskCheckboxClicked(this);
        });

        $element.on("click", ".hide-admin-task-list", function (e) {
            hideTaskList(this, element, e, API);
        });

        $element.on("click", ".show-admin-task-list", function (e) {
            showTaskList(this, element, e, API);
        });

        $element.on("click", ".admin-task-content", function (e) {
            taskContentClicked(this, e);
        });

        $element.on("click", ".create-issue-trigger", createIssueClicked);
    };

    return DashboardItem;
});
