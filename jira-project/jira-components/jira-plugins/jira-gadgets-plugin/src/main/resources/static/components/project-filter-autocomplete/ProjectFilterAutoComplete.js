define('jira-dashboard-items/components/autocomplete/project-filter-autocomplete', [
    'jira/autocomplete/rest-autocomplete',
    'jira/util/objects',
    'jquery'
], function(
    RESTAutoComplete,
    Objects,
    $
) {

    var Templates = JIRA.DashboardItem.ProjectFilterAutoComplete.Config.Templates;

    /**
     * Allows the user to open the advanced search window for the filter picker
     * @param options
     */
    var initialiseAdvancedSearch = function (options) {
        var parentElement = options.parentElement;
        var fieldID = options.fieldID;

        var updateSelect = function () {
            var id = $("#filter_" + fieldID + "_id").attr("value");
            var type = id.substr(0, id.indexOf('-'));
            id = id.substr(id.indexOf('-') + 1);
            var label = $("#filter_" + fieldID + "_name").text();

            parentElement.find("input[name=type]").val(type);
            parentElement.find("input[name=id]").val(id);
            parentElement.find("input[name=name]").val(label);
            parentElement.find(".filterpicker-value-name").text(label).addClass('success');

            JIRA.trigger("gadget-advancedprojectfilter-selected");
        };

        /*
         * Listening for DOMAttrModified works in all browsers except WEBKIT.  MutationObservers work for all
         * browsers except IE9 and IE10.  So I am using a combination of both to listen for the updates from the
         * advanced filter picker.  This does mean that some browsers will pick up the change twice, so we must
         * ensure that updateSelect can be executed twice without any negative side affects.
         */
        var advancedFilterResultIdField = parentElement.find("#filter_" + fieldID + "_id");
        advancedFilterResultIdField.on("DOMAttrModified", updateSelect);
        if (window.MutationObserver || window.WebkitMutationObserver || window.MozMutationObserver) {
            observer = new MutationObserver(updateSelect);
            observer.observe(advancedFilterResultIdField[0], {attributes: true});
        }

        parentElement.find("#filter_" + fieldID + "_advance").click(function (e) {
            var url = AJS.contextPath() + "/secure/FilterPickerPopup.jspa?showProjects=true&field=" + fieldID;
            var windowVal = "filter_" + fieldID.replace(new RegExp('-', 'g'), '') + "_window";
            var prefs = "width=800, height=500, resizable, scrollbars=yes";
            window.open(url, windowVal, prefs).focus();

            e.preventDefault();
        });
    };

    /**
     * @name ProjectFilterOptions
     * @global
     *
     * @property {function} completeField is function executed when selection is chosen
     */

    /**
     * @class ProjectFilterAutoComplete
     * @extends RESTAutoComplete
     * @abstract
     *
     * @param {ProjectFilterOptions} options for the ProjectFilterAutocomplete
     */
    var ProjectFilterAutoComplete = function(options) {
        options = options || {};

        // prototypial inheritance (http://javascript.crockford.com/prototypal.html)
        var that = Objects.begetObject(RESTAutoComplete);


        that.getAjaxParams = function(){
            return {
                url: AJS.contextPath() + "/rest/gadget/1.0/pickers/projectsAndFilters",
                data: options.ajaxData || {},
                dataType: "json",
                type: "GET"
            };
        };

        that.completeField =  function (selection) {
            if (selection) {
                options.parentElement.find("input[name=type]").val(selection.type);
                options.parentElement.find("input[name=id]").val(selection.id);
                options.parentElement.find("input[name=name]").val(selection.name);
                options.parentElement.find(".filterpicker-value-name").text(selection.name).addClass('success');
                this.field.val('');
                this.field.trigger("change");
            }
        };

        var createSuggestionBlock = function(element, sectionType, sectionName, data, renderer) {
            var suggestionNodes = [];
            var suggestionSectionElement = $(Templates.SuggestionSection({sectionTitle: sectionName}));
            var suggestionSectionList = suggestionSectionElement.find("ul.aui-list-section");


            $(data).each(function() {
                var suggestion = this;
                var suggestionElement = $(renderer({
                    suggestion: suggestion
                }));

                suggestion.type = sectionType;

                suggestionElement.mouseover(function() {
                    $(this).addClass("active");
                });
                suggestionNodes.push([suggestionElement, suggestion]);
                suggestionSectionList.append(suggestionElement);
            });

            element.append(suggestionSectionElement);

            return suggestionNodes;
        };

        /**
         * @method renderSuggestions
         * @param {Object} response
         */
        that.renderSuggestions = function(response) {
            // remove previous results
            this.clearResponseContainer();

            this.responseContainer.addClass('aui-list');
            var suggestionNodes = [];
            if (response && response.projects && response.projects.length > 0) {
                var projectSectionNodes = createSuggestionBlock(this.responseContainer, 'project', AJS.I18n.getText('common.concepts.projects'),
                    response.projects, Templates.SuggestionProject);
                suggestionNodes = suggestionNodes.concat(projectSectionNodes);
            }

            if (response && response.filters && response.filters.length > 0) {
                var filterSectionNodes = createSuggestionBlock(this.responseContainer, 'filter', AJS.I18n.getText('common.concepts.filters'),
                    response.filters, Templates.SuggestionFilter);
                suggestionNodes = suggestionNodes.concat(filterSectionNodes);
            }

            this.responseContainer.find(".aui-list-item-link").click(function(event) {
                event.preventDefault();
            });

            that.addSuggestionControls(suggestionNodes);

            return suggestionNodes;
        };

        options.minQueryLength = 1;
        options.queryDelay = 0.25;

        that.init(options);
        initialiseAdvancedSearch(options);

        return that;
    };

    return ProjectFilterAutoComplete;
});
