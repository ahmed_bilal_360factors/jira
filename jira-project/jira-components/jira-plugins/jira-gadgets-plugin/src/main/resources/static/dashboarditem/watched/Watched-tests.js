AJS.test.require("com.atlassian.jira.gadgets:common-test-resources");
AJS.test.require("com.atlassian.jira.gadgets:watched-dashboard-item-resources", function () {

    'use strict';

    module('jira-dashboard-items/watched', {
        setup: function () {
            this.mockedContext = AJS.test.context();

            this.API = AJS.$.extend({}, DashboardItem.Mocks.API);
            this.APIStub = sinon.stub(this.API);

            this.$el = AJS.$("<div/>");

            AJS.$("#qunit-fixture").append(this.$el);
        },

        renderResults: function(prefs, expectedPrefs) {
            var deferred = AJS.$.Deferred();
            this.mockedContext.mock('jira-dashboard-items/components/search-results', function (opts) {
                deepEqual(opts.preferences, expectedPrefs, "search result preferences match");

                return {
                    render: function () {
                        return deferred;
                    },
                    on: function(event, cb) {}
                };
            });

            var WatchedDashboardItem = this.mockedContext.require('jira-dashboard-items/watched');
            var dashboardItem = new WatchedDashboardItem(this.API);
            dashboardItem.render(this.$el, prefs);
        }
    });


    test("Render with show totals adds the 'watchers' column", function () {
        var itemInputPrefs = {columnNames:"summary|description", showTotalWatches:true}
        var expectedSearchResultPref = {
            "columnNames": "summary|description|watches",
            "jql": "issue in watchedIssues() AND resolution = EMPTY order by watchers desc",
            "showTotalWatches": true
        };

        this.renderResults(itemInputPrefs, expectedSearchResultPref);
    });

    test("Render without totals doesn't add the 'watchers' column", function () {
        var itemInputPrefs = {columnNames:"summary|description", showTotalWatches:false}
        var expectedSearchResultPref = {
            "columnNames": "summary|description",
            "jql": "issue in watchedIssues() AND resolution = EMPTY order by watchers desc",
            "showTotalWatches": false
        };

        this.renderResults(itemInputPrefs, expectedSearchResultPref);
    });

    test("Render with resolved issues modifies the JQL query", function () {
        var itemInputPrefs = {columnNames:"summary|description", showResolved:true}
        var expectedSearchResultPref = {
            "columnNames": "summary|description",
            "jql": "issue in watchedIssues() order by watchers desc",
            "showResolved": true
        };

        this.renderResults(itemInputPrefs, expectedSearchResultPref);
    });

    test("Render without resolved issues modifies the JQL query", function () {
        var itemInputPrefs = {columnNames:"summary|description", showResolved:false}
        var expectedSearchResultPref = {
            "columnNames": "summary|description",
            "jql": "issue in watchedIssues() AND resolution = EMPTY order by watchers desc",
            "showResolved": false
        };

        this.renderResults(itemInputPrefs, expectedSearchResultPref);
    });

    test("Render with default preferences", function () {
        var itemInputPrefs = {columnNames:"summary|description"}
        var expectedSearchResultPref = {
            "columnNames": "summary|description",
            "jql": "issue in watchedIssues() AND resolution = EMPTY order by watchers desc"
        };

        this.renderResults(itemInputPrefs, expectedSearchResultPref);
    });
});


