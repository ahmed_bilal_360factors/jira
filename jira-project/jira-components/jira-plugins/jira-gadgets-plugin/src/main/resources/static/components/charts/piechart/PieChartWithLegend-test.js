AJS.test.require("com.atlassian.jira.gadgets:piechart-with-legend", function() {

    var PieChartWithLegend = require('jira-dashboard-items/components/charts/piechart-with-legend');

    var DEFAULT_DATA = [5, 4, 3, 2, 1];
    var DEFAULT_RADIUS = 200;

    module('Charts.PieChartWithLegend', {
        setup: function() {
            this.$el = AJS.$("<div/>");
        },

        teardown: function() {
            this.$el.empty();
        }
    });

    test("Rendering legend renders all items in list", function() {
        var radius = DEFAULT_RADIUS;
        new PieChartWithLegend(DEFAULT_DATA, {
            piechart: {
                radius: radius
            }
        }).render(this.$el);

        equal(this.$el.find("ul.legend .legend-item").length, DEFAULT_DATA.length);
    });

    test("Passed in render function is called on all elements", function() {
        var legendCallCount = 0;
        var radius = DEFAULT_RADIUS;
        new PieChartWithLegend(DEFAULT_DATA, {
            piechart: {
                radius: radius
            },
            legend: {
                renderLegendItem: function() {
                    ++legendCallCount;
                }
            }
        }).render(this.$el);

        equal(legendCallCount, DEFAULT_DATA.length);
    });

    test("Passed in render function is rendered in list", function() {
        var radius = DEFAULT_RADIUS;
        new PieChartWithLegend(DEFAULT_DATA, {
            piechart: {
                radius: radius
            },
            legend: {
                renderLegendItem: function(element, piechart, data, index) {
                    element.html("<div class='rendered-item'>" + data + "</div>")
                }
            }
        }).render(this.$el);

        var renderedItems = this.$el.find("ul.legend .legend-item .rendered-item");
        equal(renderedItems.length, DEFAULT_DATA.length);

        for (var i = 0; i < DEFAULT_DATA.length; ++i) {
            equal(AJS.$(renderedItems[i]).text(), DEFAULT_DATA[i]);
        }
    });

    test("Hovering on pie chart legend highlights pie sector", function() {
        var radius = DEFAULT_RADIUS;
        new PieChartWithLegend(DEFAULT_DATA, {
            piechart: {
                radius: radius
            }
        }).render(this.$el);

        ok(this.$el.find(".piechart-arc-highlighted").length === 0, "No piechart sectors should be highlighted on render");

        this.$el.find("ul.legend .legend-item").first().mouseover();

        ok(this.$el.find(".piechart-arc-highlighted").length === 1, "One sector should be highlighted on legend hover");

        this.$el.find("ul.legend .legend-item").first().mouseleave();

        ok(this.$el.find(".piechart-arc-highlighted").length === 0, "On mouse leave no sectors should be highlighted");
    });
});