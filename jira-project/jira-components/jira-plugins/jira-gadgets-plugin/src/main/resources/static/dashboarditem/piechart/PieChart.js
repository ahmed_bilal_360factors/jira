define("jira-dashboard-items/piechart", [
   'jquery',
   'underscore',
   'jira-dashboard-items/components/charts/piechart-with-legend',
   'jira-dashboard-items/components/autocomplete/project-filter-autocomplete'
], function(
    $,
    _,
    PieChartWithLegend,
    ProjectFilterAutoComplete
) {

    /**
     *
     * @param {InlineGadgetAPI} API
     * @param {Object} [options] for gadget
     * @constructor
     */
    var DashboardItem = function(API, options) {
        this.API = API;
        this.options = options || {};
    };

    /**
     * Given a JSON string extract the errors into an array.
     * @param {JSON} errorString to parse
     * @returns {string[]} of errors
     */
    function parseErrorMessage(errorString) {
        var errorArray = [];
        var errorJSON = JSON.parse(errorString);
        for (var key in errorJSON){
            if (errorJSON.hasOwnProperty(key)) {
                errorArray.push({
                    key: key,
                    message: errorJSON[key]
                });
            }
        }
        return errorArray;
    }

    /**
     * @property {number} the max number of sectors allowed in a piechart gadget.
     */
    DashboardItem.prototype.SECTOR_LIMIT = 11;

    /**
     * Given a set of preferences it takes deprecated preference values and applies them to the new format.
     *
     * @param {Object} preferences of the gadget
     * @returns {Object}
     */
    function preferencesBackwardCompatible(preferences) {
        if (preferences.projectOrFilterId && !preferences.type && !preferences.id) {
            var projectOrFilterSplit = preferences.projectOrFilterId.split("-");
            preferences.type = projectOrFilterSplit[0];
            preferences.id = projectOrFilterSplit[1];
            delete preferences.projectOrFilterId;
        }
        return preferences;
    }

    DashboardItem.prototype._renderChart = function(gadgetElement, response) {
        var gadget = this;

        if (!response.results || response.results.length == 0) {
            gadgetElement.html(JIRA.DashboardItem.PieChart.Templates.NoPieChartContent());
        } else {

            //Group data
            var limit = gadget.SECTOR_LIMIT;
            if (response.results.length > limit) {
                var other = {
                    key: AJS.I18n.getText('common.words.other.no.dots'),
                    percentage: 0,
                    value: 0,
                    url: '#',
                    subResults: []
                };

                var overflow = response.results.length - limit + 1;
                other.subResults = response.results.splice(limit - 1, overflow);
                other.value = _.reduce(other.subResults, function (total, entry) { return total + entry.value; }, 0);
                other.percentage = Math.floor(other.value * 100 / response.issueCount);

                response.results.push(other);
            }

            for (var i = 0; i < response.results.length; ++i) {
                if (response.results[i].percentage === 0) {
                    response.results[i].percentage = "<1";
                }
            }

            var MAX_RADIUS = 200;
            var radius = Math.min(MAX_RADIUS, gadgetElement.width() / 2);

            var ellipsifyText = function(element, width) {
                var text = element.text();
                while (element[0].getBBox().width > width && text.length > 0) {
                    text = text.substr(0, text.length - 1);
                    element.text(AJS.I18n.getText('common.concepts.ellipsify', text));
                }
                return element.text()
            };

            //Calculates the font-size given the size of the inner radius.
            var MIN_INNER_RADIUS = 40;
            var MAX_INNER_RADIUS = 75;
            var INNER_RADIUS_RANGE = MAX_INNER_RADIUS - MIN_INNER_RADIUS;
            var innerRadius = Math.max(MIN_INNER_RADIUS, Math.min(radius / 2, MAX_INNER_RADIUS));

            var innerRadiusMinDifference = innerRadius - MIN_INNER_RADIUS;
            var percentageInner = innerRadiusMinDifference / INNER_RADIUS_RANGE;

            var MIN_PRIMARY_FONT_SIZE = 28;
            var MAX_PRIMARY_FONT_SIZE = 48;
            var PRIMARY_FONT_SIZE_RANGE = MAX_PRIMARY_FONT_SIZE - MIN_PRIMARY_FONT_SIZE;
            var primaryFontSize = MIN_PRIMARY_FONT_SIZE + (percentageInner * PRIMARY_FONT_SIZE_RANGE);

            var options = {
                piechart: {
                    radius: radius,
                    innerRadius: innerRadius,
                    hoverExpansion: 0,
                    centerChart: true,

                    clickable: true,

                    getValue: function (dataElement) {
                        return dataElement.value;
                    },

                    onEachArc: function (piechart, data, index, sectorElement) {
                        AJS.InlineDialog(sectorElement, "piechart-sector-" + gadget.API.getGadgetId() + "-" + index,
                            function (content, trigger, showPopup) {
                                if (data.subResults) {
                                    content.html(JIRA.DashboardItem.PieChart.Templates.GroupedSectorInlineDialog(data));
                                } else {
                                    content.html(JIRA.DashboardItem.PieChart.Templates.SectorInlineDialog(data));
                                }
                                showPopup();
                                return false;
                            },
                            {
                                gravity: 'w',
                                hideDelay: 1000,
                                closeOnTriggerClick: true
                            }
                        );
                    },

                    getPrimaryText: function(data) {
                        return data.percentage + "%";
                    },

                    getSecondaryText: function(data) {
                        return data.key;
                    },

                    onMouseOver: function(piechart, data, index, sectorElement) {
                         var primaryElement = $(sectorElement).find(".piechart-center-primary");
                         primaryElement.css("font-size", primaryFontSize + "px");
                         var secondaryElement = $(sectorElement).find(".piechart-center-secondary");
                         ellipsifyText(secondaryElement, secondaryElement.attr("width"));
                    }
                },

                legend: {
                    renderLegendTitle: function(element, data) {
                        element.html(JIRA.DashboardItem.PieChart.Templates.LegendTitle(response));
                    },

                    renderLegendItem: function(element, pieChart, data, index) {
                        element.html(JIRA.DashboardItem.PieChart.Templates.LegendSection());

                        data.colour = pieChart.colourPicker(pieChart, data, index);

                        var legendContent = $(".legend-content", element);
                        legendContent.html(JIRA.DashboardItem.PieChart.Templates.LegendItem(data));

                        if (data.subResults) {
                            var grouptoggle = legendContent.find("a");
                            grouptoggle.append("...");

                            var dialogId = "piechart-legend-" + gadget.API.getGadgetId() + "-" + index;

                            var dialog = AJS.InlineDialog(grouptoggle, dialogId,
                                function (content, trigger, showPopup) {
                                    content.css({"padding": "20px"}).html(JIRA.DashboardItem.PieChart.Templates.LegendMultiItem(data));
                                    showPopup();
                                    content.focus();
                                    return false;
                                },
                                {
                                    onHover: true,
                                    gravity: 'n',
                                    width: 175,
                                    hideDelay: 500,
                                    isRelativeToMouse: true
                                }
                            );
                            grouptoggle.click(function(event) {
                                event.preventDefault();
                            });

                            var isEnter = function(event) {
                                return event.keyCode === 13;
                            };

                            var isShiftTab = function(event) {
                                return event.keyCode === 9 && event.shiftKey === true;
                            };

                            var isTab = function(event) {
                                return event.keyCode === 9 && event.shiftKey === false
                            };

                            grouptoggle.keydown(function(event) {
                                if (isEnter(event)) {
                                    dialog.show();
                                    var dialogContainer = $("#inline-dialog-" + dialogId);

                                    //needed the timeout as the dialog is not shown yet.
                                    _.defer(function() {

                                        var otherDialogFocusable = dialogContainer.find(":focusable");

                                        var firstFocusable = otherDialogFocusable.first();
                                        firstFocusable.focus();
                                        firstFocusable.keydown(function(event) {
                                            if(isShiftTab(event)) {
                                                event.preventDefault();
                                                dialog.hide();
                                                grouptoggle.focus();
                                            }
                                        });

                                        var lastFocusable = otherDialogFocusable.last();
                                        lastFocusable.keydown(function(event) {
                                            if(isTab(event)) {
                                                dialog.hide();
                                                grouptoggle.focus();
                                            }
                                        });
                                    });
                                }
                            });
                        }
                    }
                }
            };


            var chart = new PieChartWithLegend(response.results, options);

            chart.render(gadgetElement);

            gadget.API.setTitle(AJS.I18n.getText('gadget.piechart.title.specific', response.filterTitle));
            gadget.API.forceLayoutRefresh();
        }
    };

    /**
     * Render the configured piechart gadget.
     * @param {node} element to apply gadget to
     * @param {Object} preferences for gadget that have been configured.
     */
    DashboardItem.prototype.render = function(element, preferences) {
        var gadgetElement = $(".piechart-gadget", element);
        var gadget = this;

        preferences = preferencesBackwardCompatible(preferences);

        gadget.API.initRefresh(preferences, _.bind(gadget.render, gadget, element, preferences));
        gadget.API.showLoadingBar();
        $.ajax({
            method: "GET",
            url: AJS.contextPath() + '/rest/gadget/1.0/statistics',
            dataType: "json",
            data: {
                jql: preferences.type + "=" + preferences.id,
                statType: preferences.statType
            }

        // The hide loading bar is in done/fail, not always, because d3 needs the element to be visible to render.  The
        // loading bar hides the element and therefore if the loading bar is still there when trying to render the chart
        // the piechart is rendered with size of 0.
        }).done(function(response) {
            gadget.API.hideLoadingBar();
            gadget.resizeHandler = _.throttle(_.bind(gadget._renderChart, gadget, gadgetElement, response), 100);
            $(element).resize(gadget.resizeHandler);
            gadget._renderChart(gadgetElement, response);
        }).fail(function(message) {
            gadget.API.hideLoadingBar();
            var errors = parseErrorMessage(message.responseText);

            gadgetElement.html(JIRA.DashboardItem.PieChart.Templates.Errors({
                errors: errors
            }));
        });
    };

    /**
     * Render the configuration screen for the piechart gadget
     * @param {Node} element to render into
     * @param {Object} preferences for object.
     */
    DashboardItem.prototype.renderEdit = function(element, preferences) {
        var gadget = this;
        var gadgetElement = $(".piechart-gadget", element);
        if (gadget.resizeHandler)
        {
            $(element).removeResize(gadget.resizeHandler);
            delete gadget.resizeHandler;
        }

        gadget.API.showLoadingBar();
        $.ajax({
            method: "GET",
            url: AJS.contextPath() + "/rest/gadget/1.0/statTypes"
        }).done(function(data) {

            // Random prefix to avoid collisions with other instances of this gadget
            var prefix = gadget.API.getGadgetId() + "-";

            preferences = preferencesBackwardCompatible(preferences);

            if (!preferences.name && preferences.type && preferences.id) {
                preferences.name = preferences.type + "-" + preferences.id;
            }

            gadget.API.setTitle( AJS.I18n.getText('gadget.piechart.title') );

            // mark preferences.name as already escaped so soy doesn't re-escape it
            if (preferences.name) {
                preferences.name = new soydata.SanitizedHtml(preferences.name);
            }

            gadgetElement.html(JIRA.DashboardItem.PieChart.Templates.Configuration({
                prefix: prefix,
                statTypes: data.stats,
                preferences: preferences
            }));

            var projectFilterOptions = {
                fieldID: prefix + "project-filter-picker",
                parentElement: gadgetElement,
                maxHeight: 140
            };

            //Mostly used for testing where we don't want a delay;
            if (gadget.options.delay) {
                projectFilterOptions.delay = gadget.options.delay;
            }

            ProjectFilterAutoComplete(projectFilterOptions);

            var form = $("form", gadgetElement);

            var cancelButton = $(".cancel", form);
            if (cancelButton) {
                cancelButton.click(function() {
                    gadget.API.closeEdit();
                });
            }

            function completeValidation(preferences, form) {
                if (!preferences.id || !preferences.type) {
                    $(".projectOrFilter-error", form).text(AJS.I18n.getText('gadget.common.required.query')).show();
                    return false;
                }

                return true;
            }

            form.on("submit",function(e){
                e.preventDefault();

                var form = $(e.target);

                var preferences = {
                    name: form.find("input[name=name]").val(),
                    type: form.find("input[name=type]").val(),
                    id: form.find("input[name=id]").val(),
                    statType: form.find("select[name=statType]").val(),
                    refresh: gadget.API.getRefreshFieldValue('refresh-interval')
                };

                if (completeValidation(preferences, form)) {
                    gadget.API.savePreferences(preferences);
                }
            });

            gadget.API.forceLayoutRefresh();
        }).fail(function() {
            gadgetElement.html(JIRA.DashboardItem.PieChart.Templates.ErrorRenderingConfiguration());
        }).always(function() {
            gadget.API.hideLoadingBar();
        });
    };

    return DashboardItem;
});