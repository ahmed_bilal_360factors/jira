define("jira-dashboard-items/components/search-results", [
    'jquery',
    'underscore',
    'backbone'
], function (
    $,
    _,
    Backbone
) {
    'use strict';

    // JC-303, JC-278: In ancient JIRA we used to store '--Default--' in the column config to get the default
    // columns in the gadgets. There's still some data out there that has this for the gadget config.
    // need to ensure we handle this correctly here before sending the request to theserver.
    var updateDataForLegacyDefaultColumns = function(data) {
        var legacyDefaultColsIndex = _.indexOf(data.columnNames, "--Default--");
        if(legacyDefaultColsIndex !== -1) {
            data.addDefault = true;
            data.columnNames.splice(legacyDefaultColsIndex, 1);
        }
    };

    return function (options) {
        var context = options.context;
        var preferences = options.preferences;
        var tableUrl;
        var dataDefaults = {
            num: preferences.num,
            tableContext: options.tableContext || "jira.table.cols.dashboard",
            addDefault: false,
            columnNames: preferences.columnNames.split("|"),
            enableSorting: true,
            paging: true,
            showActions: true
        };

        updateDataForLegacyDefaultColumns(dataDefaults);

        function onSuccess(data, textStatus, jqXHR) {
            var $resultCountLink;
            var $table;

            context.empty();

            // we need to append ids with a random prefix to avoid clashes between different instances of
            // the search results dashboard item
            data.table = "<div>" + data.table + "</div>";
            $table = $(data.table);
            $table.find('#issuetable').removeAttr('id').addClass('issue-table');
            $table.find('[id]').removeAttr('id');
            data.table = $table.html();

            if (data.displayed != 0) {
                //if all issues are showing on one page then pagination is not required
                var showPagination = data.displayed === data.total;
                context.append(JIRA.DashboardItem.Common.SearchResults.Templates.resultsTable({
                    pagination: showPagination,
                    table: data.table
                }));

                $resultCountLink = context.find(".results-count-link");
                var link;
                if(data.url) {
                    link = AJS.contextPath() + data.url;
                } else if (preferences.jql) {
                    link = AJS.contextPath() + "/issues/?jql=" + encodeURIComponent(preferences.jql);
                }

                if(link) {
                    $resultCountLink.replaceWith(JIRA.DashboardItem.Common.SearchResults.Templates.searchLink({
                        href: link,
                        title: data.title,
                        issueCount: $resultCountLink.html()
                    }));
                }

                $("th.sortable", context).each(function () {
                    this.onclick = null;
                }).click(function (e) {
                    e.preventDefault();
                    context.find(".search-results-dashboard-item-issue-table").addClass("loading");
                    preferences.sortBy = $(this).attr("rel");
                    publicApi.trigger("sorted", {
                       "sortBy": preferences.sortBy
                    });
                });

                $(".pagination a", context).click(function (event) {
                    event.preventDefault();
                    context.find(".search-results-dashboard-item-issue-table").addClass("loading");
                    preferences.startIndex = $(this).attr("rel");
                    renderTable();
                });

                AJS.Dropdown.create({
                    trigger: ".issue-actions-trigger",
                    autoScroll: false,
                    ajaxOptions: {
                        dataType: "json",
                        cache: false,
                        formatSuccess: JIRA.FRAGMENTS.issueActionsFragment
                    }
                });

                //TODO: This is so that the dashboard item resizes correctly if the content contains
                //      images. We should probably look at switching to something more advanced like ImageLoader.js
                //      in future. For now this is what the old gadget code did as well.
                if(options.onContentLoaded) {
                    setTimeout(options.onContentLoaded, 500);
                }
            }
            else {
                if(options.onEmptyResult) {
                    options.onEmptyResult(data, textStatus, jqXHR)
                } else {
                    context.empty().append(JIRA.DashboardItem.Common.SearchResults.Templates.noResults());
                }
            }
        }

        function renderTable() {
            return $.ajax({
                url: tableUrl,
                type: "GET",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: $.extend({}, dataDefaults, {
                    sortBy: (preferences.sortBy && preferences.sortBy !== "") ? preferences.sortBy : null,
                    startIndex: preferences.startIndex ? preferences.startIndex : 0
                }),
                error: function(jqXHR, textStatus, errorThrown) {
                    if(options.onError) {
                        options.onError(jqXHR, textStatus, errorThrown);
                    } else {
                        context.empty().append(JIRA.DashboardItem.Common.SearchResults.Templates.errorResult());
                    }
                },
                success: onSuccess,
                complete: options.onContentLoaded
            });
        }

        var publicApi = _.extend({
            render: function () {
                if (preferences.filterId) {
                    tableUrl = AJS.contextPath() + '/rest/gadget/1.0/issueTable/filter';
                    dataDefaults.filterId = preferences.filterId;
                    return renderTable();
                }
                else if (preferences.jql) {
                    tableUrl = AJS.contextPath() + '/rest/gadget/1.0/issueTable/jql';
                    dataDefaults.jql = preferences.jql;
                    return renderTable();
                }
                else {
                    throw new Error("Unable to render search results. filterId or jql are required.")
                }
            }
        }, Backbone.Events);

        return publicApi;
    };
});