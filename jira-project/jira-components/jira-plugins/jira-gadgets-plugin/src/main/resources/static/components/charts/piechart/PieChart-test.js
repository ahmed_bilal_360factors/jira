AJS.test.require("com.atlassian.jira.gadgets:piechart", function() {

    var PieChart = require('jira-dashboard-items/components/charts/piechart');

    var DEFAULT_DATA = [5, 4, 3, 2, 1];
    var DEFAULT_RADIUS = 200;
    var DEFAULT_HOVER_EXPANSION = 10;

    module('Charts.PieChart', {
        setup: function() {
            this.$el = AJS.$("<div/>");
        }
    });

    function getChart(element) {
        return element.find(".piechart");
    }

    function getArcs(element) {
        return element.find(".piechart-arc");
    }

    function getNumberArcs(element) {
        return getArcs(element).length;
    }

    function getArcText(arc) {
        return jQuery(arc).find("text").text();
    }

    function getArcColour(arc) {
        return jQuery(arc).find(".piechart-fill").attr('fill');
    }

    test('height/width calculation with no hover expansion', function() {
        var radius = DEFAULT_RADIUS;
        new PieChart(DEFAULT_DATA, {
            radius: radius
        }).render(this.$el);


        var chart = getChart(this.$el);
        equal(chart.attr('width'), radius * 2, "Should have twice the radius as width");
        equal(chart.attr('height'), radius * 2, "Should have twice the radius as height");
    });

    test('height/width calculation with  hover expansion', function() {
        var radius = DEFAULT_RADIUS;
        var hoverExpansion = DEFAULT_HOVER_EXPANSION;
        new PieChart(DEFAULT_DATA, {
            radius: radius,
            hoverExpansion: hoverExpansion
        }).render(this.$el);


        var chart = getChart(this.$el);
        equal(chart.attr('width'), (radius + hoverExpansion) * 2, "Should have twice the radius and hover expansion as width");
        equal(chart.attr('height'), (radius + hoverExpansion) * 2, "Should have twice the radius and hover expansion as height");
    });

    test('test data entries have equivalent sector', function() {
        var data = DEFAULT_DATA;

        new PieChart(data, {
           radius: DEFAULT_RADIUS
        }).render(this.$el);

        equal(getNumberArcs(this.$el), data.length, "Should have 5 sectors");
    });

    test("Colour assignment should be applied to sectors", function() {
        var colours = [
            "#ff0000",
            "#00ff00",
            "#0000ff"
        ];

        new PieChart(DEFAULT_DATA, {
            radius: DEFAULT_RADIUS,
            colours: colours
        }).render(this.$el);

        var arcs = getArcs(this.$el);
        for (var i = 0; i < arcs.length; ++i) {
            var arcColour = getArcColour(arcs[i]);
            equal(arcColour, colours[i % colours.length], "Should apply colours in order (run #" + i + ")")
        }
    });

    test("Default colour picker does not allow the last and first sector to have same colour", function() {
        var colours = [
            "#ff0000",
            "#00ff00",
            "#0000ff"
        ];

        var data = [4, 3, 2, 1];

        new PieChart(data, {
            radius: DEFAULT_RADIUS,
            colours: colours
        }).render(this.$el);

        var arcs = getArcs(this.$el);
        equal(getArcColour(arcs[0]), "#ff0000", "First sector should be first colour");
        equal(getArcColour(arcs[data.length - 1]), "#00ff00", "Last sector should skip first as data.length % colours.length == 0, therefore last colour is same as first");
    });


    //Hover does expansion
    test("Check on each arc is called on each arc when rendered", function() {
        var data = DEFAULT_DATA;
        expect(data.length);

        new PieChart(data, {
            radius: DEFAULT_RADIUS,
            onEachArc: function(pieChart, dataItem, index) {
                equal(dataItem, data[index], "Should pass in correct sector and index");
            }
        }).render(this.$el);
    });

    //Can't test hover and click because d3.on does not get triggered by jQuery.trigger
    //See: https://github.com/mbostock/d3/issues/100
});
