AJS.test.require("com.atlassian.jira.gadgets:stats-dashboard-item-resources", function() {

    var StatsDashboardItem = require('jira-dashboard-items/stats');
    var jquery = require('jquery');

    var STUB_STATS = {
        stats: [
            {value: "value1", label: "label1"},
            {value: "value2", label: "label2"},
            {value: "value3", label: "label3"}
        ]
    };

    var STUB_STATS_DATA = {
        filterOrProjectName: "name",
        filterOrProjectLink: "link",
        totalIssueCount: 8,
        statTypeDescription: "issuetypes",
        rows: [
            {
                html: "Feature",
                percentage: 50,
                url: "urlFeature",
                count: 4
            },
            {
                html: "Bug",
                percentage: 25,
                url: "urlBug",
                count: 2
            },
            {
                html: "Task",
                percentage: 12,
                url: "urlTask",
                count: 1
            },
            {
                html: "Other",
                percentage: 12,
                url: "urlOther",
                count: 1
            }
        ]
    };

    module('inline stats gadget test', {
        setup: function () {
            this.$el = jquery('<div class="stats-gadget" />');

            var API = {
                gadget: {
                    id: 1
                },
                forceLayoutRefresh: function(){},
                getGadgetId: function() {return 1},
                hideLoadingBar: function() {},
                initRefresh: function() {},
                savePreferences: function(){},
                setTitle: function(){},
                showLoadingBar: function() {}
            };

            var gadgetOptions = {
                delay: function(callback) { callback(); }
            };
            this.dashboardItem = new StatsDashboardItem(API, gadgetOptions);

            jquery("#qunit-fixture").append(this.$el);

            this.server = sinon.fakeServer.create();
        },

        teardown: function () {
            this.server.restore();
        }
    });

    test("Should have loading display when requesting resources and success", function() {
        var showLoadingSpy = sinon.spy();
        var hideLoadingSpy = sinon.spy();
        sinon.stub(this.dashboardItem.API, "showLoadingBar", showLoadingSpy);
        sinon.stub(this.dashboardItem.API, "hideLoadingBar", hideLoadingSpy);

        this.dashboardItem.renderEdit(this.$el, {});

        ok(showLoadingSpy.calledOnce);

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS));

        ok(hideLoadingSpy.calledOnce);
    });

    test("Should have loading display when requesting resources and failure", function() {
        var showLoadingSpy = sinon.spy();
        var hideLoadingSpy = sinon.spy();
        sinon.stub(this.dashboardItem.API, "showLoadingBar", showLoadingSpy);
        sinon.stub(this.dashboardItem.API, "hideLoadingBar", hideLoadingSpy);

        this.dashboardItem.renderEdit(this.$el, {});

        ok(showLoadingSpy.calledOnce);

        this.server.requests[0].respond(500, { "Content-Type": "application/json" }, JSON.stringify({}));

        ok(hideLoadingSpy.calledOnce);
    });

    test("Requested stat items should get displayed in dropdown", function() {

        this.dashboardItem.renderEdit(this.$el, {});

        equal(1, this.server.requests.length, "Should have a request for stat types");

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS));

        var statTypeSelect = jquery("#" + this.dashboardItem.API.gadget.id + "-stat-type", this.$el);

        var statTypeOptions = statTypeSelect.find("option");

        equal(statTypeOptions.length, STUB_STATS.stats.length, "Should have option for each stat");

        for (var i = 0; i < statTypeOptions.length; ++i) {
            equal(jquery(statTypeOptions[i]).attr('value'), STUB_STATS.stats[i].value);
            equal(jquery(statTypeOptions[i]).text(), STUB_STATS.stats[i].label);
        }
    });

    test("Shows error when stat items could not be retired", function() {
        this.dashboardItem.renderEdit(this.$el, {});
        this.server.requests[0].respond(500, { "Content-Type": "application/json" }, "{}");

        equal(this.$el.find(".aui-message.aui-message-error").length, 1);
    });

    test("Project/Filter autocomplete selections should be applied into the form", function() {
        this.dashboardItem.renderEdit(this.$el, {});
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS));

        var suggestions = {
            projects: [
                {
                    html: '<b>testProject</b>',
                    id: 10000,
                    key: 'xyz',
                    name: 'test'
                }
            ]
        };

        var projectFilterAutocomplete = jquery("#" + this.dashboardItem.API.gadget.id +"-project-filter-picker", this.$el);
        projectFilterAutocomplete.val("a");
        projectFilterAutocomplete.trigger(jquery.Event("keyup"));
        this.server.requests[1].respond(200, { "Content-Type": "application/json" }, JSON.stringify(suggestions));

        jquery(".suggestions .aui-list-item").first().click();

        equal(this.$el.find("input[name=id]").val(), 10000);
        equal(this.$el.find("input[name=type]").val(), "project");
        equal(projectFilterAutocomplete.val(), "");
    });

    test("Submitting the form sends a request with the correct values", function() {

        this.dashboardItem.renderEdit(this.$el, {});
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS));
        var suggestions = {
            projects: [
                {
                    html: '<b>testProject</b>',
                    id: 10000,
                    key: 'xyz',
                    name: 'test'
                }
            ]
        };

        var projectFilterAutocomplete = jquery("#" + this.dashboardItem.API.gadget.id +"-project-filter-picker", this.$el);
        projectFilterAutocomplete.val("a");
        projectFilterAutocomplete.trigger(jquery.Event("keyup"));
        this.server.requests[1].respond(200, { "Content-Type": "application/json" }, JSON.stringify(suggestions));
        jquery(".suggestions .aui-list-item").first().click();

        // set other option values
        jquery("#" + this.dashboardItem.API.gadget.id + "-stat-type option", this.$el).first().attr('selected', 'true');
        jquery("#" + this.dashboardItem.API.getGadgetId() + "-refresh-interval", this.$el).attr('checked', 'true');
        jquery('#' + this.dashboardItem.API.gadget.id + '-stats-sort-by', this.$el).first().attr('selected', 'true'); // natural
        jquery('#' + this.dashboardItem.API.gadget.id + '-stats-sort-direction', this.$el).first().attr('selected', 'true'); // ascending
        jquery('#' + this.dashboardItem.API.gadget.id + '-stats-show-resolved', this.$el).first().attr('selected', 'true'); // false

        var savePreferencesSpy = sinon.spy();
        sinon.stub(this.dashboardItem.API, "savePreferences", savePreferencesSpy);
        this.dashboardItem.API.getRefreshFieldValue = sinon.stub().returns('15');

        jquery("form", this.$el).submit();

        var calledArg = savePreferencesSpy.args[0][0];

        equal(calledArg.name, this.$el.find("input[name=name]").val(), "Should have gotten the name from the form");
        equal(calledArg.type, this.$el.find("input[name=type]").val(), "Should have gotten the type from the form");
        equal(calledArg.id, this.$el.find("input[name=id]").val(), "Should have gotten the id from the form");
        equal(calledArg.projectOrFilterId, calledArg.type + "-" + calledArg.id,"projectOrFilterId should be constructed from type and id values");
        equal(calledArg.statType, this.$el.find("select[name=statType]").val(), "Should have gotten the statType from the form");
        equal(calledArg.refresh, this.$el.find("input[name=refresh-interval]").val(), "Should have gotten the refresh interval from the form");
        equal(calledArg.sortBy, this.$el.find("select[name=sortBy]").val(), "Should have gotten the sortBy settings from the form");
        equal(calledArg.sortDirection, this.$el.find("select[name=sortDirection]").val(), "Should have gotten the sort direction from the form");
        equal(calledArg.includeResolvedIssues, this.$el.find("select[name=includeResolvedIssues]").val(), "Should have gotten the resolved issues settings from the form");
    });

    test("Saved gadget applies preferences to form", function() {
        this.dashboardItem.renderEdit(this.$el, {
            name: 'filter',
            type: 'type',
            id: 10000,
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15,
            sortBy: 'total',
            sortDirection: 'desc',
            includeResolvedIssues: 'true'
        });
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS));

        equal(this.$el.find("input[name=name]").val(), "filter", "Expected name to be saved");
        equal(this.$el.find("input[name=id]").val(), 10000, "Expected id to be saved");
        equal(this.$el.find("input[name=type]").val(), "type", "Expected type to be saved");
        equal(this.$el.find("select[name=statType]").val(), STUB_STATS.stats[0].value, "Expected stat type to be saved");
        equal(this.$el.find("input[name=refresh-interval]").val(), 15, "Expected refresh interval to be saved");
    });

    test("Rendering stats chart with no data displays message", function() {
        this.dashboardItem.render(this.$el, {
            type: 'type',
            id: 10000,
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });

        equal(this.server.requests.length, 1, "Should have request for stats chart data");

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify({results: []}));

        equal(this.$el.find(".aui-message.aui-message-info").length, 1);
    });

    test("Rendering stats gadget creates stats chart", function() {
        this.dashboardItem.render(this.$el, {
            type: 'type',
            id: 10000,
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS_DATA));

        equal(this.$el.find(".stats-gadget-table-header").length, 1, "Rendered a stats chart");
    });

    test("Error retrieving stats chart displays error", function() {
        this.spy(AJS,"format");
        this.dashboardItem.render(this.$el, {
            type: 'filter',
            id: 10000,
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });

        this.server.requests[0].respond(500, { "Content-Type": "application/json" }, JSON.stringify({}));


        equal(this.$el.find(".aui-message.aui-message-error").length, 1);
        var filterLink = "<a href=\"" + AJS.contextPath() + "/issues/?filter=10000\">";
        sinon.assert.calledWith(AJS.format, "dashboard.item.filter.results.invalid.filter", filterLink, "</a>");
        equal(this.$el.find(".aui-message.aui-message-error:contains('dashboard.item.filter.results.invalid.filter')").length, 1);

    });

    test("Error retrieving stats chart for project displays project specific error", function() {
        this.spy(AJS,"format");
        this.dashboardItem.render(this.$el, {
            type: 'project',
            id: 10000,
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });

        this.server.requests[0].respond(500, { "Content-Type": "application/json" }, JSON.stringify({}));


        equal(this.$el.find(".aui-message.aui-message-error").length, 1);
        //also verifies the url param is being encoded correctly.
        var projectFilterLink = "<a href=\"" + AJS.contextPath() + "/issues/?jql=project%20%3D%2010000\">";
        sinon.assert.calledWith(AJS.format, "dashboard.item.filter.results.invalid.project", projectFilterLink, "</a>");
        equal(this.$el.find(".aui-message.aui-message-error:contains('dashboard.item.filter.results.invalid.project')").length, 1);
    });

    test("Request url for getting stats chart data", function() {
        this.dashboardItem.render(this.$el, {
            type: 'project',
            id: 10000,
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS_DATA));

        var requestUrl = this.server.requests[0].url;
        //remove query parameters
        requestUrl = requestUrl.replace(/\?.*/, '');

        equal(requestUrl, AJS.contextPath() + "/rest/gadget/1.0/stats/generate");
    });

    test("sortBy field in request for new preference structure should be included in url", function() {
        this.dashboardItem.render(this.$el, {
            type: 'filter',
            id: 10000,
            sortBy: 'asc',
            isConfigured: true,
            refresh: 15
        });

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS_DATA));

        var requestUrl = this.server.requests[0].url;
        //remove _ query param as we do not care about what the value is.
        requestUrl = requestUrl.replace(/&_.*/, '');

        equal(requestUrl, AJS.contextPath() + "/rest/gadget/1.0/stats/generate?projectOrFilterId=filter-10000&sortBy=asc");
    });

    test("Init refresh was called on render", function() {
        var refreshInitSpy = sinon.spy();
        sinon.stub(this.dashboardItem.API, "initRefresh", refreshInitSpy);

        this.dashboardItem.render(this.$el, {
            id: 10000,
            type: 'project',
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS_DATA));

        ok(refreshInitSpy.calledOnce);
    });

    test("Old-style preferences are converted into new-style preferences", function() {
        sinon.stub(this.dashboardItem.API, "initRefresh");

        this.dashboardItem.render(this.$el, {
            projectOrFilterId: "project-10000",
            isConfigured: true,
            refresh: 15
        });

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS_DATA));

        var requestUrl = this.server.requests[0].url;
        //remove _ query param as we do not care about what the value is.
        requestUrl = requestUrl.replace(/&_.*/, '');

        equal(requestUrl, AJS.contextPath() + "/rest/gadget/1.0/stats/generate?projectOrFilterId=project-10000");
    });
});