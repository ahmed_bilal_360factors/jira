AJS.test.require("com.atlassian.jira.gadgets:common-test-resources");
AJS.test.require("com.atlassian.jira.gadgets:project-filter-autocomplete-component", function() {

    'use strict';

    var ProjectFilterAutocomplete = require('jira-dashboard-items/components/autocomplete/project-filter-autocomplete');


    var KEYDOWN_EVENT = AJS.$.Event("keydown");
    KEYDOWN_EVENT.keyCode = 40;

    var KEYUP_EVENT = AJS.$.Event("keydown");
    KEYUP_EVENT.keyCode = 38;

    var ENTER_EVENT = AJS.$.Event("keypress");
    ENTER_EVENT.keyCode = 13;

    module('jira-dashboard-items/components/autocomplete/project-filter-autocomplete', {
        setup: function() {
            this.$el = AJS.$("<div/>");
            this.$el.append(JIRA.DashboardItem.ProjectFilterAutoComplete.Config.Templates.projectFilterPicker({prefix: 'test', id: "autocomplete"}));
            AJS.$("#qunit-fixture").append(this.$el);

            this.autocompleteEl = this.$el.find("#testautocomplete");
            this.advancedSearchResultId = this.$el.find('#filter_testautocomplete_id');
            this.advancedSearchResultName = this.$el.find('#filter_testautocomplete_name');

            this.autocomplete = ProjectFilterAutocomplete({
                fieldID: "testautocomplete",
                parentElement: this.$el,

                //Turn off delay so that tests can be done synchronously
                delay: function(callback) {
                    callback();
                }
            });

            this.server = sinon.fakeServer.create();
        },

        teardown: function() {
            this.server.restore();
            this.$el.empty();
        }
    });

    test("Should make a request to get project and filter information", function() {
        this.autocompleteEl.val("a");
        this.autocompleteEl.trigger(AJS.$.Event("keyup"));
        equal(this.server.requests.length, 1, "Should have an autocomplete request");
    });

    function createNumberProjectSuggestions(number) {
        var suggestions = [];
        for (var i = 0; i < number; ++i) {
            suggestions.push({
                html: '<b>testProject</b>',
                id: 10000 + i,
                key: 'xyz' + i,
                name: 'test' + i
            });
        }
        return suggestions;
    }

    function createNumberFilterSuggestions(number) {
        var suggestions = [];
        for (var i = 0; i < number; ++i) {
            suggestions.push({
                descHtml: '<b>testFilter</b>',
                id: 20000 + i,
                name: 'filter' + i,
                nameHtml: '<b>filter</b>'
            });
        }
        return suggestions;
    }

    function createResponse(numberProjects, numberFilters) {
        return JSON.stringify({
            projects: createNumberProjectSuggestions(numberProjects),
            filters: createNumberFilterSuggestions(numberFilters)
        });
    }

    function createProjectResponse(numberProjects) {
        return createResponse(numberProjects, 0);
    }

    function createFilterResponse(numberFilters) {
        return createResponse(0, numberFilters);
    }

    function getNumberSuggestionBlocks(element) {
        var suggestionBlocks = element.find(".suggestionBlock");

        return suggestionBlocks.length;
    }

    function hasSuggestionBlock(element, sectionName) {
        var suggestionblock = getSuggestionBlock(element, sectionName);
        return suggestionblock !== undefined && suggestionblock.length > 0;
    }

    function hasProjectSuggestionBlock(element) {
        return hasSuggestionBlock(element, "common.concepts.projects");
    }

    function hasFilterSuggestionBlock(element) {
        return hasSuggestionBlock(element, "common.concepts.filters");
    }

    function getSuggestionBlock(element, sectionName) {
        return element.find(".suggestionBlock h5:contains('" + sectionName + "')").parent();
    }

    function getNumberSuggestions(element, sectionName) {
        var suggestionBlock = getSuggestionBlock(element, sectionName);

        return suggestionBlock.find("ul.aui-list-section").children().length;
    }

    function getNumberProjectSuggestions(element) {
        return getNumberSuggestions(element, "common.concepts.projects");
    }

    function getNumberFilterSuggestions(element) {
        return getNumberSuggestions(element, "common.concepts.filters");
    }

    function getSelectedSuggestion(element) {
        return element.find(".suggestionBlock .aui-list-item.active");
    }

    function getSelectedSuggestionTypeAndId(element) {
        var selectedSuggestion = getSelectedSuggestion(element);

        return {
            type: selectedSuggestion.attr('data-type'),
            id: selectedSuggestion.attr('data-id')
        }
    }

    function suggestionDropdownVisible(element) {
        return element.find(".suggestions").is(":visible");
    }


    test("Should populate suggestion box with content", function() {
        this.autocompleteEl.val("a");
        this.autocompleteEl.trigger(AJS.$.Event("keyup"));
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, createResponse(1, 1));

        equal(getNumberSuggestionBlocks(this.$el), 2, "Should have two suggestion blocks for project and filter");

        ok(hasProjectSuggestionBlock(this.$el), "Should have project block");
        ok(hasFilterSuggestionBlock(this.$el), "Should have filter block");

        equal(getNumberProjectSuggestions(this.$el), 1, "Should have one project suggestion");
        equal(getNumberFilterSuggestions(this.$el), 1, "Should have one filter suggestion");
    });


    test("Should not render sections if no suggestions available", function() {
        this.autocompleteEl.val("a");
        this.autocompleteEl.trigger(AJS.$.Event("keyup"));
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, createProjectResponse(1));

        equal(getNumberSuggestionBlocks(this.$el), 1, "Should only have project block");

        ok(hasProjectSuggestionBlock(this.$el), "Should have project block");
        ok(!hasFilterSuggestionBlock(this.$el), "Should not have filter block");

        this.autocompleteEl.val("b");
        this.autocompleteEl.trigger(AJS.$.Event("keyup"));
        this.server.requests[1].respond(200, { "Content-Type": "application/json" }, createFilterResponse(1));

        equal(getNumberSuggestionBlocks(this.$el), 1, "Should only have filter block");

        ok(!hasProjectSuggestionBlock(this.$el), "Should not have project block");
        ok(hasFilterSuggestionBlock(this.$el), "Should have have filter block");
    });


    test("Test autocomplete key navigation", function() {
        this.autocompleteEl.val("a");
        this.autocompleteEl.trigger(AJS.$.Event("keyup"));
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, createResponse(2, 2));

        var selectedSuggestion = getSelectedSuggestionTypeAndId(this.$el);
        equal(selectedSuggestion.type, "project", "Should have a project selected");
        equal(selectedSuggestion.id, "10000", "Should have first project selected");

        this.autocompleteEl.trigger(KEYDOWN_EVENT);
        var selectedSuggestion = getSelectedSuggestionTypeAndId(this.$el);
        equal(selectedSuggestion.type, "project", "Should have a project selected");
        equal(selectedSuggestion.id, "10001", "Should have second project selected");

        this.autocompleteEl.trigger(KEYDOWN_EVENT);
        var selectedSuggestion = getSelectedSuggestionTypeAndId(this.$el);
        equal(selectedSuggestion.type, "filter", "Should now have a filter selected");
        equal(selectedSuggestion.id, "20000", "Should have first filter selected");

        this.autocompleteEl.trigger(KEYDOWN_EVENT);
        var selectedSuggestion = getSelectedSuggestionTypeAndId(this.$el);
        equal(selectedSuggestion.type, "filter", "Should now have a filter selected");
        equal(selectedSuggestion.id, "20001", "Should have second filter selected");

        this.autocompleteEl.trigger(KEYUP_EVENT);
        var selectedSuggestion = getSelectedSuggestionTypeAndId(this.$el);
        equal(selectedSuggestion.type, "filter", "Should now have a filter selected");
        equal(selectedSuggestion.id, "20000", "Should have first filter selected");
    });

    test("Test autocomplete does not overflow suggestions on keypress", function() {

        this.autocompleteEl.val("a");
        this.autocompleteEl.trigger(AJS.$.Event("keyup"));
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, createResponse(1, 1));

        var selectedSuggestion = getSelectedSuggestionTypeAndId(this.$el);
        equal(selectedSuggestion.type, "project", "Should have a project selected");
        equal(selectedSuggestion.id, "10000", "Should have first project selected");

        this.autocompleteEl.trigger(KEYUP_EVENT);
        equal(selectedSuggestion.type, "project", "Should have a project selected");
        equal(selectedSuggestion.id, "10000", "Should have first project selected");


        this.autocompleteEl.trigger(KEYDOWN_EVENT);
        var selectedSuggestion = getSelectedSuggestionTypeAndId(this.$el);
        equal(selectedSuggestion.type, "filter", "Should now have a filter selected");
        equal(selectedSuggestion.id, "20000", "Should have first filter selected");


        this.autocompleteEl.trigger(KEYDOWN_EVENT);
        var selectedSuggestion = getSelectedSuggestionTypeAndId(this.$el);
        equal(selectedSuggestion.type, "filter", "Should now have a filter selected");
        equal(selectedSuggestion.id, "20000", "Should have first filter selected");
    });

    test("Selecting suggestion by enter closes suggestion dropdown", function() {
        this.autocompleteEl.val("a");
        this.autocompleteEl.trigger(AJS.$.Event("keyup"));
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, createResponse(1, 1));

        ok(suggestionDropdownVisible(this.$el), "Suggestion dropdown should be visible");
        this.autocompleteEl.trigger(ENTER_EVENT);
        ok(!suggestionDropdownVisible(this.$el), "Suggestion dropdown should not be visible on enter");
    });


    test("Selecting suggestion by mouse closes suggestion dropdown", function() {
        this.autocompleteEl.val("a");
        this.autocompleteEl.trigger(AJS.$.Event("keyup"));
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, createResponse(1, 1));

        ok(suggestionDropdownVisible(this.$el), "Suggestion dropdown should be visible");
        AJS.$(".suggestions .aui-list-item").first().click();
        ok(!suggestionDropdownVisible(this.$el), "Suggestion dropdown should not be visible on enter");
    });

    test("Selecting filter by mouse populates fields", function () {
        this.autocompleteEl.val("a");
        this.autocompleteEl.trigger(AJS.$.Event("keyup"));
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, createResponse(0, 1));

        ok(suggestionDropdownVisible(this.$el), "Suggestion dropdown should be visible");
        AJS.$(".suggestions .aui-list-item").first().click();
        ok(!suggestionDropdownVisible(this.$el), "Suggestion dropdown should not be visible on enter");

        equal(this.$el.find("input[name=type]").val(), "filter");
        equal(this.$el.find("input[name=id]").val(), "20000");
        equal(this.$el.find("input[name=name]").val(), "filter0");
        equal(this.$el.find(".filterpicker-value-name").text(), "filter0");
    });

    test("Selecting Project by mouse populates fields", function () {
        this.autocompleteEl.val("a");
        this.autocompleteEl.trigger(AJS.$.Event("keyup"));
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, createResponse(1, 0));

        ok(suggestionDropdownVisible(this.$el), "Suggestion dropdown should be visible");
        AJS.$(".suggestions .aui-list-item").first().click();
        ok(!suggestionDropdownVisible(this.$el), "Suggestion dropdown should not be visible on enter");

        equal(this.$el.find("input[name=type]").val(), "project");
        equal(this.$el.find("input[name=id]").val(), "10000");
        equal(this.$el.find("input[name=name]").val(), "test0");
        equal(this.$el.find(".filterpicker-value-name").text(), "test0");
    });


    asyncTest("Update filter from advanced sets select value", function () {
        // when running in firefox it will get triggered twice. so we used the called variable to make qunit behave
        var called = false;

        JIRA.bind("gadget-advancedprojectfilter-selected", _.bind(function () {
            if (!called) {
                called = true;
                equal(this.$el.find("input[name=type]").val(), "filter");
                equal(this.$el.find("input[name=id]").val(), "10234");
                equal(this.$el.find("input[name=name]").val(), "Sample Filter");
                equal(this.$el.find(".filterpicker-value-name").text(), "Sample Filter");

                QUnit.start();
}
        }, this));

        this.advancedSearchResultName.text("Sample Filter");
        this.advancedSearchResultId[0].setAttribute("value", "filter-10234");
    });

    asyncTest("Update project from advanced sets select value", function () {

        // when running in firefox it will get triggered twice. so we used the called variable to make qunit behave
        var called = false;

        JIRA.bind("gadget-advancedprojectfilter-selected", _.bind(function () {
            if (!called) {
                called = true;
                equal(this.$el.find("input[name=type]").val(), "project");
                equal(this.$el.find("input[name=id]").val(), "10234");
                equal(this.$el.find("input[name=name]").val(), "Sample Project");
                equal(this.$el.find(".filterpicker-value-name").text(), "Sample Project");

                QUnit.start();
            }
        }, this));

        this.advancedSearchResultName.text("Sample Project");
        this.advancedSearchResultId[0].setAttribute("value", "project-10234");
    });

});


