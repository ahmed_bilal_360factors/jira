define('jira-dashboard-items/components/charts/piechart-with-legend', [
    'jquery',
    'jira-dashboard-items/components/charts/piechart'
], function(
    $,
    PieChart
) {

    function noop() {}


    var Templates = JIRA.Charts.PieChart.Legend.Templates;

    /**
     *  @name LegendOptions
     *  @global
     *
     *  @property {RenderLegendTitle} renderLegendTitle renders the title for the legend.
     *  @property
     */

    /**
     * Function used to render the title of the legend.
     * @name RenderLegendTitle
     * @function
     * @global
     *
     * @param {JQuery} element to render onto
     * @param {Object[]} data for the piechart
     **/


    /**
     * Function used to render an item in the legend
     * @name RenderLegendItem
     * @function
     * @global
     *
     * @param {JQuery} element to render onto
     * @param {Object} data for this item.
     **/

    /**
     * @name PieChartWithLegendOptions
     * @global
     *
     * @property {PieChartOptions} piechart options
     * @property {LegendOptions} legend options
     *
     */

    /**
     * @constructor
     *
     * Generate a pie chart
     * @param data for the chart
     * @param {PieChartWithLegendOptions} options

     * @return {PieChartWithLegend}
     *      render{function} renders the PieChart
     *
     */
    var PieChartWithLegend = function(data, options) {
        options.legend = options.legend || {};

        var self = this;
        this.data = data;
        this.PieChart = new PieChart(data, options.piechart);
        this.renderLegendItem = options.legend.renderLegendItem || noop;
        this.renderLegendTitle = options.legend.renderLegendTitle || noop;

        function renderPieChart(element) {
            var chartEl = $('.piechart-wrapper', element);

            self.PieChart.render(chartEl);
        }

        function renderLegend(element) {
            var legendEl = $('.legend-wrapper', element);
            var legendTitle = $('.legend-title', legendEl);

            self.renderLegendTitle(legendTitle, self.data);

            var list = $("ul.legend", legendEl);
            for(var i = 0; i < self.data.length; ++i) {
                var dataItem = self.data[i];

                var listItem = $(Templates.ListItem({index: i}));
                self.renderLegendItem(listItem, self.PieChart, dataItem, i);

                list.append(listItem);
            }
        }

        this.render = function(element) {
            element = $(element);
            element.html(Templates.Container());

            renderPieChart(element);

            renderLegend(element);

            $(".legend .legend-item", element).each(function(index) {
                $(this).mouseover(function() {
                    self.PieChart.mouseOver(index)
                });
                $(this).mouseleave(function() {
                    self.PieChart.mouseOut(index)
                });
            });
        };
    };

    return PieChartWithLegend;
});