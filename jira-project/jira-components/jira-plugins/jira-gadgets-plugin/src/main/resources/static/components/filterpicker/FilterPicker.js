define("jira-dashboard-items/components/filter-picker", [
    'underscore',
    'jquery',
    'jira/ajs/ajax/smart-ajax',
    'jira/ajs/select/single-select'
], function (
    _,
    $,
    SmartAjax,
    SingleSelect
) {

    'use strict';

    return function () {
        var observer;

        var initialiseAdvancedSearch = function(parentElement, fieldId, select)
        {
            /*
             * Listening for DOMAttrModified works in all browsers except WEBKIT.  MutationObservers work for all
             * browsers except IE9 and IE10.  So I am using a combination of both to listen for the updates from the
             * advanced filter picker.  This does mean that some browsers will pick up the change twice, so we must
             * ensure that updateSelect can be executed twice without any negative side affects.
             */

            var updateSelect = function () {
                var id = $("#filter_" + fieldId + "_id").attr("value");
                if (id.indexOf("filter-") == 0) {
                    id = id.substr(7);
                }
                var label = $("#filter_" + fieldId + "_name").text();
                var descriptor = new AJS.ItemDescriptor({
                    value: id,
                    label: label
                });

                select.setSelection(descriptor, true);
            };

            var advancedFilterResultIdField = parentElement.find("#filter_" + fieldId + "_id");

            advancedFilterResultIdField.on("DOMAttrModified", updateSelect);
            if (window.MutationObserver || window.WebkitMutationObserver || window.MozMutationObserver) {
                observer = new MutationObserver(updateSelect);
                observer.observe(advancedFilterResultIdField[0], {attributes: true});
            }

            parentElement.find("#filter_" + fieldId + "_advance").click(function (e) {
                var url = AJS.contextPath() + "/secure/FilterPickerPopup.jspa?showProjects=false&field=" + fieldId;
                var windowVal = "filter_" + fieldId.replace(new RegExp('-', 'g'), '') + "_window";
                var prefs = "width=800, height=500, resizable, scrollbars=yes";
                window.open(url, windowVal, prefs).focus();

                e.preventDefault();
            });
        };


        return {
            init: function (options) {
                var element = options.element;
                var selectedValue = options.selectedValue;
                var self = this;

                this.select = new SingleSelect({
                    element: element,
                    itemAttrDisplayed: "label",
                    revertOnInvalid: true,
                    removeDuplicates: true,
                    submitInputVal: false,
                    showDropdownButton: true,
                    removeOnUnSelect: false,
                    // override the default matching strategy, allowing matches to start with any kind of prefix
                    matchingStrategy: '(^|(.*))({0})(.*)',
                    ajaxOptions: {
                        url: AJS.contextPath() + "/rest/gadget/1.0/pickers/filters",
                        query: true,
                        data: {
                            fieldName: "quickfind"
                        },
                        formatResponse: function (response) {
                            var ret = [];
                            if (response.filters && response.filters.length) {
                                var groupDescriptor = new AJS.GroupDescriptor();
                                _.each(response.filters, function (filter) {
                                    groupDescriptor.addItem(new AJS.ItemDescriptor({
                                        value: filter.id,
                                        label: filter.name
                                    }));
                                });
                                ret.push(groupDescriptor);
                            }
                            return ret;
                        }
                    }
                });

                if (selectedValue) {
                    self.select.disable();

                    SmartAjax.makeRequest({
                        method: "GET",
                        url: AJS.contextPath() + "/rest/api/2/filter/" + selectedValue,
                        success: function (data) {
                            self.select.setSelection(new AJS.ItemDescriptor({
                                value: selectedValue,
                                label: data.name
                            }));
                        },
                        complete: function() {
                            self.select.enable();
                        }
                    });
                }

                initialiseAdvancedSearch(options.parentElement, element.attr('id'), self.select);

                this.errorField = element.parents(".field-group").find(".error");
                self.select.hideErrorMessage = function () { }; //noop
                self.select.showErrorMessage = function () { }; //noop
                return this;
            },

            getValue: function () {
                if(this.select.lastSelection) {
                    return {
                        "id": this.select.lastSelection.value(),
                        "label": this.select.lastSelection.label()
                    };
                }
                return null;
            },

            validate: function () {
                if (this.select.lastSelection && this.select.lastSelection.value()) {
                    this.errorField.hide();
                    return true;
                }
                else {
                    this.errorField.show();
                    return false;
                }
            },

            disconnectObserver: function() {
                if (observer) {
                    observer.disconnect();
                }
            }
        }
    }
});