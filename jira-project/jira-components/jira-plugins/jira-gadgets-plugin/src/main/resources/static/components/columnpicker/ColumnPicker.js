define("jira-dashboard-items/components/column-picker", [
    'underscore',
    'jquery',
    'jira/ajs/ajax/smart-ajax',
    'jira/ajs/select/single-select'
], function (
    _,
    $,
    SmartAjax,
    SingleSelect
) {
    'use strict';

    var showAjaxError = function (container, result) {
        $(container).empty();
        AJS.messages.error(container, {
            title: AJS.I18n.getText('common.words.error'),
            body: '<p>' + SmartAjax.buildSimpleErrorContent(result) + '</p>',
            closeable: false
        });
        $(container).show();
    };

    var createTable = function (tableElement) {
        var LocalEntryModel = AJS.RestfulTable.EntryModel.extend({
            destroy: function () {
                this.collection.remove(this);
            }
        });

        var ReorderableRowView = AJS.RestfulTable.Row.extend({
            initialize: function (options) {
                options = _.extend({}, options, {allowReorder: true});
                AJS.RestfulTable.Row.prototype.initialize.call(this, options);
            },
            renderOperations: function () {
                return $(JIRA.DashboardItem.Common.Config.Templates.deleteColumnButton())
                        .click(_.bind(function (e) {
                            e.preventDefault();
                            this.destroy();
                        }, this));
            }
        });

        var table = new AJS.RestfulTable({
            el: tableElement,
            allowReorder: false,
            allowEdit: false,
            allowCreate: false,
            autoFocus: false,
            model: LocalEntryModel,
            resources: {
                all: function (populate) {
                    populate([]);
                }
            },
            columns: [
                {
                    id: "label",
                    header: ""
                }
            ],
            views: {
                row: ReorderableRowView
            }
        });
        table.$thead.detach();

        // Add allowance for another cell to the <thead>
        table.$theadRow.prepend("<th />");

        // Allow drag and drop reordering of rows
        table.$tbody.sortable({
            handle: "." + table.classNames.DRAG_HANDLE,
            helper: function (e, elt) {
                var helper = $("<div/>").addClass(table.classNames.MOVEABLE + " " + elt.attr("class"));
                elt.children().each(function (i) {
                    var $td = $(this);

                    // .offsetWidth/.outerWidth() is broken in webkit for tables, so we do .clientWidth + borders
                    // Need to coerce the border-left-width to an in because IE - http://bugs.jquery.com/ticket/10855
                    var borderLeft = parseInt(0 + $td.css("border-left-width"), 10);
                    var borderRight = parseInt(0 + $td.css("border-right-width"), 10);
                    var width = $td[0].clientWidth + borderLeft + borderRight;

                    helper.append($("<div/>").html($td.html()).addClass($td.attr("class")).width(width));
                });

                helper = $("<div class='aui-restfultable-readonly'/>").append(helper); // Basically just to get the styles.
                helper.css({left: elt.offset().left}); // To align with the other table rows, since we've locked scrolling on x.
                helper.appendTo(document.body);

                return helper;
            },
            start: function (event, ui) {
                var cachedHeight = ui.helper[0].clientHeight;
                var $this = ui.placeholder.find("td");

                // Make sure that when we start dragging widths do not change
                ui.item
                        .addClass(table.classNames.MOVEABLE)
                        .children().each(function (i) {
                            $(this).width($this.eq(i).width());
                        });

                // Create a <td> to add to the placeholder <tr> to inherit CSS styles.
                var td = '<td colspan="' + table.getColumnCount() + '">&nbsp;</td>';

                ui.placeholder.html(td).css({
                    height: cachedHeight,
                    visibility: 'visible'
                });

                // Stop hover effects etc from occuring as we move the mouse (while dragging) over other rows
                table.getRowFromElement(ui.item[0]).trigger(table._event.MODAL);
            },
            stop: function (event, ui) {
                if ($(ui.item[0]).is(":visible")) {
                    ui.item
                            .removeClass(table.classNames.MOVEABLE)
                            .children().attr("style", "");

                    ui.placeholder.removeClass(table.classNames.ROW);

                    // Return table to a normal state
                    table.getRowFromElement(ui.item[0]).trigger(table._event.MODELESS);
                }
            },
            axis: "y",
            delay: 0,
            containment: "document",
            cursor: "move",
            scroll: true,
            zIndex: 8000
        });

        // Prevent text selection while reordering.
        table.$tbody.bind("selectstart mousedown", function (event) {
            return !$(event.target).is("." + table.classNames.DRAG_HANDLE);
        });

        return table;
    };

    var createSelectField = function (options, table) {
        var errorContainer = options.errorContainer;
        var columnNames = options.columnNames;
        var availableColumns = [];

        // Get options for the select field
        SmartAjax.makeRequest({
            url: AJS.contextPath() + "/rest/gadget/1.0/availableColumns",
            method: "GET",
            success: function (response) {
                if (response.availableColumns && response.availableColumns.length) {
                    _.each(response.availableColumns, function (column) {
                        availableColumns.push(new AJS.ItemDescriptor(column));
                    });
                }

                if (columnNames == "--Default--") {
                    _.each(response.defaultColumns, function (column) {
                        table.addRow({
                            id: column.value,
                            label: column.label
                        });
                    });
                }
                else {
                    var columns = columnNames.split("|");
                    _.each(columns, function (columnId) {
                        var availableColumn = _.find(availableColumns, function (availableColumn) {
                            return (availableColumn.value() === columnId);
                        });
                        if (availableColumn) {
                            table.addRow({
                                id: availableColumn.value(),
                                label: availableColumn.label()
                            });
                        }
                    });
                }
                select.enable();
            },
            error: function (result) {
                showAjaxError(errorContainer, result);
            },
            complete: function () {
                if(options.onContentLoaded) {
                    options.onContentLoaded();
                }
            }
        });

        // Create the select field
        var select = new SingleSelect({
            element: options.fieldElement,
            itemAttrDisplayed: "label",
            revertOnInvalid: true,
            submitInputVal: false,
            showDropdownButton: true,
            suggestions: function (query) {
                return _.filter(availableColumns, function (columnDescriptor) {
                    var isAlreadyInTheTable = table._models.get(columnDescriptor.value());
                    var matchesQuery = columnDescriptor.label().toLocaleLowerCase().indexOf(query.toLocaleLowerCase()) > -1;
                    return !isAlreadyInTheTable && matchesQuery;
                });
            }
        });
        select.disable();
        select.model.$element.on('selected', function (e, descriptor) {
            select.clear();
            if (!descriptor.value()) {
                return;
            }
            if (table._models.get(descriptor.value())) {
                return;
            }
            table.addRow({
                id: descriptor.value(),
                label: descriptor.label()
            });
            if(options.onContentLoaded) {
                options.onContentLoaded();
            }
        });

        return select;
    };

    return function () {
        return {
            init: function (options) {
                var self = this;
                this.tableElement = options.tableElement;

                this.table = createTable(options.tableElement);
                this.select  = createSelectField(options, this.table);

                this.table.getColumnValues = function () {
                    return _.map(self.table.$table.find("tr"), function (tr) {
                        return $(tr).attr("data-id")
                    });
                };

                return this;
            },

            getValue: function () {
                return this.table.getColumnValues().join("|");
            },

            validate: function () {
                var errorField = this.tableElement.parents(".field-group").find(".error");
                if (this.table._models.length > 0) {
                    errorField.hide();
                    return true;
                }
                else {
                    errorField.show();
                    return false;
                }
            }
        }
    };
});