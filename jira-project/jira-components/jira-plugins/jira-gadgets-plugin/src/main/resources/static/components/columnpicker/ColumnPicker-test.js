AJS.test.require("com.atlassian.jira.gadgets:column-picker-component", function () {

    'use strict';

    var ColumnPicker = require('jira-dashboard-items/components/column-picker');

    var availableColumns = {"availableColumns": [
        {"label": "Description", "value": "description"},
        {"label": "cool names", "value": "customfield_10540"},
        {"label": "my urls", "value": "customfield_10740"},
        {"label": "Issue Type", "value": "issuetype"},
        {"label": "Key", "value": "issuekey"},
        {"label": "Summary", "value": "summary"},
        {"label": "Priority", "value": "priority"}
    ], "defaultColumns": [
        {"label": "Issue Type", "value": "issuetype"},
        {"label": "Key", "value": "issuekey"},
        {"label": "Summary", "value": "summary"},
        {"label": "Priority", "value": "priority"}
    ]};

    module('jira-dashboard-items/components/column-picker', {
        initPicker: function (initialColumns, error) {
            this.onContentLoadedSpy = sinon.spy();
            this.columnPicker = new ColumnPicker().init({
                errorContainer: this.errorContainer,
                tableElement: this.tableElement,
                fieldElement: this.fieldElement,
                columnNames: initialColumns,
                onContentLoaded: this.onContentLoadedSpy
            });
            if(error) {
                this.server.requests[0].respond(500, { "Content-Type": "application/json" }, "{}");
            } else {
                this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(availableColumns));
            }
        },

        setup: function () {
            this.$el = AJS.$("<div/>");

            this.tableElement = AJS.$('<table class="column-picker-order" id="fields-table"></table>');
            this.fieldElement = AJS.$('<select id="autocomplete"></select>');
            this.errorContainer = AJS.$("<div/>");

            this.$el.append(this.tableElement);
            this.$el.append(this.fieldElement);
            this.$el.append(this.errorContainer);

            this.server = sinon.fakeServer.create();
        },

        teardown: function () {
            this.server.restore();
            this.$el.empty();
            AJS.$("#autocomplete-suggestions").remove();
        }
    });

    function getColumnLabel(trElem) {
        return AJS.$(AJS.$(trElem).find("td")[1]).text();
    }

    test("Default columns rendered correctly", function () {
        this.initPicker("--Default--");

        var columnRows = this.tableElement.find("tr");
        equal(columnRows.length, 4, "Should have default columns loaded");
        equal(getColumnLabel(columnRows[0]), "Issue Type", "default columns match");
        equal(getColumnLabel(columnRows[1]), "Key", "default columns match");
        equal(getColumnLabel(columnRows[2]), "Summary", "default columns match");
        equal(getColumnLabel(columnRows[3]), "Priority", "default columns match");
    });

    test("Subset of columns rendered correctly", function () {
        this.initPicker("description|customfield_10740|summary");

        var columnRows = this.tableElement.find("tr");
        equal(columnRows.length, 3, "Should have columns rendered");
        equal(getColumnLabel(columnRows[0]), "Description", "columns match");
        equal(getColumnLabel(columnRows[1]), "my urls", "columns match");
        equal(getColumnLabel(columnRows[2]), "Summary", "columns match");
    });

    test("Missing columns get ignored", function () {
        this.initPicker("description|notavailable|summary");

        var columnRows = this.tableElement.find("tr");
        equal(columnRows.length, 2, "Should have columns rendered");
        equal(getColumnLabel(columnRows[0]), "Description", "columns match");
        equal(getColumnLabel(columnRows[1]), "Summary", "columns match");
    });

    test("Error retrieving columns shows error message", function () {
        equal("", this.errorContainer.text(), "No error shown before");
        this.initPicker("description|notavailable|summary", true);

        equal("common.words.errorcommon.forms.ajax.commserror", this.errorContainer.text(), "Error shown due to retrieving columns failure.");
        equal("disabled", this.columnPicker.select.$field.attr("disabled"), "Adding fields select should be disabled");
    });

    test("Adding a field via select works", function () {
        this.initPicker("description|customfield_10740|summary");

        this.columnPicker.select.setSelection(new AJS.ItemDescriptor({
            value: "issuetype",
            label: "Issue Type"
        }));

        var columnRows = this.tableElement.find("tr");
        equal(columnRows.length, 4, "Should have columns rendered");
        equal(getColumnLabel(columnRows[0]), "Description", "columns match");
        equal(getColumnLabel(columnRows[1]), "my urls", "columns match");
        equal(getColumnLabel(columnRows[2]), "Summary", "columns match");
        equal(getColumnLabel(columnRows[3]), "Issue Type", "columns match");

        equal("description|customfield_10740|summary|issuetype", this.columnPicker.getValue(), "getting the value is correct too");
    });

    test("Removing a field via works", function () {
        this.initPicker("description|customfield_10740|summary");

        var customFieldRow = AJS.$(this.tableElement.find("tr")[1]);
        customFieldRow.find(".aui-button").click();

        var columnRows = this.tableElement.find("tr");
        equal(columnRows.length, 2, "Should have columns rendered");
        equal(getColumnLabel(columnRows[0]), "Description", "columns match");
        equal(getColumnLabel(columnRows[1]), "Summary", "columns match");

        equal("description|summary", this.columnPicker.getValue(), "getting the value is correct too");
    });
});


