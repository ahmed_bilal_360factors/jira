AJS.test.require("com.atlassian.jira.gadgets:common-test-resources");
AJS.test.require("com.atlassian.jira.gadgets:piechart-dashboard-item-resources", function() {

    var PieChartDashboardItem = require('jira-dashboard-items/piechart');

    var STUB_STATS = {
        stats: [
            {value: "value1", label: "label1"},
            {value: "value2", label: "label2"},
            {value: "value3", label: "label3"}
        ]
    };

    var STUB_PIECHART_DATA = {
        filterTitle: "title",
        filterUrl: "url",
        issueCount: 8,
        statType: "issuetypes",
        results: [
            {
                key: "Feature",
                percentage: 50,
                url: "urlFeature",
                value: 4
            },
            {
                key: "Bug",
                percentage: 25,
                url: "urlBug",
                value: 2
            },
            {
                key: "Task",
                percentage: 12,
                url: "urlTask",
                value: 1
            },
            {
                key: "Other",
                percentage: 12,
                url: "urlOther",
                value: 1
            }
        ]
    };

    module('jira-dashboard-items/piechart', {
        setup: function () {
            this.$el = AJS.$("<div/>");
            this.$el.append(JIRA.DashboardItem.PieChart.Templates.PieChart());

            var API = AJS.$.extend({}, DashboardItem.Mocks.API);

            var gadgetOptions = {
                delay: function(callback) { callback(); }
            };
            this.dashboardItem = new PieChartDashboardItem(API, gadgetOptions);

            AJS.$("#qunit-fixture").append(this.$el);
            this.server = sinon.fakeServer.create();
        },

        teardown: function () {
            this.server.restore();
            this.$el.empty();
        }
    });

    /**
     * Underscore query parameter is annoying for testing url correctness so remove it.
     **/
    function removeUnderscoreQueryParam(url) {
        return url.replace(/&_=[^&?]*/, '');
    }

    test("Should have loading display when requesting resources and success", function() {
        var showLoadingSpy = sinon.spy();
        var hideLoadingSpy = sinon.spy();
        sinon.stub(this.dashboardItem.API, "showLoadingBar", showLoadingSpy);
        sinon.stub(this.dashboardItem.API, "hideLoadingBar", hideLoadingSpy);

        this.dashboardItem.renderEdit(this.$el, {});

        ok(showLoadingSpy.calledOnce);

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS));

        ok(hideLoadingSpy.calledOnce);

        equal(1, 1);
    });

    test("Should have loading display when requesting resources and failure", function() {
        var showLoadingSpy = sinon.spy();
        var hideLoadingSpy = sinon.spy();
        sinon.stub(this.dashboardItem.API, "showLoadingBar", showLoadingSpy);
        sinon.stub(this.dashboardItem.API, "hideLoadingBar", hideLoadingSpy);

        this.dashboardItem.renderEdit(this.$el, {});

        ok(showLoadingSpy.calledOnce);

        this.server.requests[0].respond(500, { "Content-Type": "application/json" }, JSON.stringify({}));

        ok(hideLoadingSpy.calledOnce);

        equal(1, 1);
    });

    test("Requested stat items should get displayed in dropdown", function() {

        this.dashboardItem.renderEdit(this.$el, {});

        equal(1, this.server.requests.length, "Should have a request for stat types");

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS));

        var statTypeSelect = AJS.$("#" + this.dashboardItem.API.getGadgetId() + "-stat-type", this.$el);

        var statTypeOptions = statTypeSelect.find("option");

        equal(statTypeOptions.length, STUB_STATS.stats.length, "Should have option for each stat");

        for (var i = 0; i < statTypeOptions.length; ++i) {
            equal(AJS.$(statTypeOptions[i]).attr('value'), STUB_STATS.stats[i].value);
            equal(AJS.$(statTypeOptions[i]).text(), STUB_STATS.stats[i].label);
        }
    });

    test("Shows error when stat items could not be retired", function() {
        this.dashboardItem.renderEdit(this.$el, {});
        this.server.requests[0].respond(500, { "Content-Type": "application/json" }, "{}");

        equal(this.$el.find(".aui-message.aui-message-error").length, 1);
    });

    test("Project/Filter autocomplete selections should be applied into the form", function() {
        this.dashboardItem.renderEdit(this.$el, {});
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS));

        var suggestions = {
            projects: [
                {
                    html: '<b>testProject</b>',
                    id: 10000,
                    key: 'xyz',
                    name: 'test'
                }
            ]
        };

        var projectFilterAutocomplete = AJS.$("#" + this.dashboardItem.API.getGadgetId() +"-project-filter-picker", this.$el);
        projectFilterAutocomplete.val("a");
        projectFilterAutocomplete.trigger(AJS.$.Event("keyup"));
        this.server.requests[1].respond(200, { "Content-Type": "application/json" }, JSON.stringify(suggestions));

        AJS.$(".suggestions .aui-list-item").first().click();

        equal(this.$el.find("input[name=id]").val(), 10000);
        equal(this.$el.find("input[name=type]").val(), "project");
        equal(projectFilterAutocomplete.val(), "");
    });


    test("Submitting the form sends a request with the correct values", function() {


        this.dashboardItem.renderEdit(this.$el, {});
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS));
        var suggestions = {
            projects: [
                {
                    html: '<b>testProject</b>',
                    id: 10000,
                    key: 'xyz',
                    name: 'test'
                }
            ]
        };

        var projectFilterAutocomplete = AJS.$("#" + this.dashboardItem.API.getGadgetId() +"-project-filter-picker", this.$el);
        projectFilterAutocomplete.val("a");
        projectFilterAutocomplete.trigger(AJS.$.Event("keyup"));
        this.server.requests[1].respond(200, { "Content-Type": "application/json" }, JSON.stringify(suggestions));
        AJS.$(".suggestions .aui-list-item").first().click();

        //Select stat type;
        AJS.$("#" + this.dashboardItem.API.getGadgetId() + "-stat-type option", this.$el).first().attr('selected', 'true');
        AJS.$("#" + this.dashboardItem.API.getGadgetId() + "-refresh-interval", this.$el).attr('checked', 'true');

        var savePreferencesSpy = sinon.spy();
        var savePreferencesStub = sinon.stub(this.dashboardItem.API, "savePreferences", savePreferencesSpy);
	    this.dashboardItem.API.getRefreshFieldValue = sinon.stub().returns('15');

        AJS.$("form", this.$el).submit();

        var calledArg = savePreferencesSpy.args[0][0];

        equal(calledArg.name, this.$el.find("input[name=name]").val(), "Should have gotten the name from the form");
        equal(calledArg.type, this.$el.find("input[name=type]").val(), "Should have gotten the type from the form");
        equal(calledArg.id, this.$el.find("input[name=id]").val(), "Should have gotten the id from the form");
        equal(calledArg.statType, this.$el.find("select[name=statType]").val(), "Should have gotten the statType from the form");
        equal(calledArg.refresh, this.$el.find("input[name=refresh-interval]").val(), "Should have gotten the refresh interval from the form");
    });


    test("Saved gadget applies preferences to form", function() {
        this.dashboardItem.renderEdit(this.$el, {
            name: 'filter',
            type: 'type',
            id: 10000,
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS));

        equal(this.$el.find("input[name=name]").val(), "filter", "Expected name to be saved");
        equal(this.$el.find("input[name=id]").val(), 10000, "Expected id to be saved");
        equal(this.$el.find("input[name=type]").val(), "type", "Expected type to be saved");
        equal(this.$el.find("select[name=statType]").val(), STUB_STATS.stats[0].value, "Expected stat type to be saved");
        equal(this.$el.find("input[name=refresh-interval]").val(), 15, "Expected refresh interval to be saved");
    });

    test("Rendering pie chart with no data displays message", function() {
        this.dashboardItem.render(this.$el, {
            type: 'type',
            id: 10000,
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });

        equal(this.server.requests.length, 1, "Should have request for pie chart data");

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify({results: []}));

        equal(this.$el.find(".aui-message.aui-message-info").length, 1);
    });

    test("Rendering pie chart gadget creates pie chart", function() {
        this.dashboardItem.render(this.$el, {
            type: 'type',
            id: 10000,
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_PIECHART_DATA));

        ok(this.$el.find(".piechart").length === 1, "Rendered a pie chart");

        //Doesn't test that a pie chart rendered correctly as that is tested in the PieChart tests.
    });

    test("Error retrieving pie chart displays error", function() {
        this.dashboardItem.render(this.$el, {
            type: 'type',
            id: 10000,
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });

        this.server.requests[0].respond(500, { "Content-Type": "application/json" }, JSON.stringify({}));


        ok(this.$el.find(".aui-message.aui-message-error").length === 1, "Should have rendered an error aui message");
    });

    test("Creating piechart generates a legend", function() {
        this.dashboardItem.render(this.$el, {
            type: 'type',
            id: 10000,
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_PIECHART_DATA));

        ok(this.$el.find("ul.legend").length === 1, "Should have found found the legend");
    });


    test("Lots of sectors are grouped into an other", function() {
        this.dashboardItem.render(this.$el, {
            type: 'type',
            id: 10000,
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });



        var DISPLAYABLE_DATA = [];
        for (var i = 0; i < this.dashboardItem.SECTOR_LIMIT - 1; ++i) {
            DISPLAYABLE_DATA.push({key: "Feature" + i, percentage: 50 ,url: "urlFeature",value: 4});
        }

        var OTHER_VALUE = 4;
        var OTHER_DATA = [];
        for (var j = 0; j < 5; ++j) {
            OTHER_DATA.push({key: "OtherFeature" + j, percentage: 50, url: "urlFeature", value: OTHER_VALUE});
        }

        var LARGE_DATA = {
            filterTitle: "title",
            filterUrl: "url",
            issueCount: 8,
            statType: "issuetypes",
            results: DISPLAYABLE_DATA.concat(OTHER_DATA)
        };

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(LARGE_DATA));

        equal(this.$el.find("ul.legend .legend-item").length, this.dashboardItem.SECTOR_LIMIT, "Should have created the limit of legend items in the legend");
        equal(this.$el.find(".piechart-arc").length, this.dashboardItem.SECTOR_LIMIT, "Should have created the limit of sectors in the piechart");

        var otherLegendItem = this.$el.find("ul.legend .legend-item").last();
        var otherLink = otherLegendItem.find(".legend-item-label");

        //test last one is an other
        equal(otherLink.text(), "common.words.other.no.dots...");
        equal(otherLegendItem.find(".legend-item-value").text(), OTHER_DATA.length * OTHER_VALUE);
    });

    test("Edit backward compatibility", function() {
        this.dashboardItem.renderEdit(this.$el, {
            projectOrFilterId: 'project-10000',
            statType: STUB_STATS.stats[0].value,
            refresh: 15
        });


        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS));

        equal(this.$el.find("input[name=name]").val(), "project-10000", "Project should be the join of id and type");
        equal(this.$el.find("input[name=id]").val(), 10000, "Id should have been extracted");
        equal(this.$el.find("input[name=type]").val(), "project", "project should have been extracted");
    });


    test("Request url for getting piechart data", function() {
        this.dashboardItem.render(this.$el, {
            type: 'project',
            id: 10000,
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_PIECHART_DATA));

        var requestUrl = this.server.requests[0].url;
        //remove query parameters
        requestUrl = requestUrl.replace(/\?.*/, '');

        equal(requestUrl, AJS.contextPath() + "/rest/gadget/1.0/statistics");
    });

    test("Generating jql for new preference structure", function() {
        this.dashboardItem.render(this.$el, {
            type: 'filter',
            id: 10000,
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_PIECHART_DATA));

        var requestUrl = this.server.requests[0].url;
        requestUrl = removeUnderscoreQueryParam(requestUrl);

        equal(requestUrl, AJS.contextPath() + "/rest/gadget/1.0/statistics?jql=filter%3D10000&statType=" + STUB_STATS.stats[0].value);
    });

    test("Test backward compatible preferences for query param arguments", function() {
        this.dashboardItem.render(this.$el, {
            projectOrFilterId: 'project-10000',
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_PIECHART_DATA));

        var requestUrl = this.server.requests[0].url;
        requestUrl = removeUnderscoreQueryParam(requestUrl);

        equal(requestUrl, AJS.contextPath() + "/rest/gadget/1.0/statistics?jql=project%3D10000&statType=" + STUB_STATS.stats[0].value);
    });

    test("Test does not use old project/filter field if new format is present", function() {
        this.dashboardItem.render(this.$el, {
            projectOrFilterId: 'project-20000',
            type: 'filter',
            id: '10000',
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_PIECHART_DATA));

        var requestUrl = this.server.requests[0].url;
        requestUrl = removeUnderscoreQueryParam(requestUrl);

        equal(requestUrl, AJS.contextPath() + "/rest/gadget/1.0/statistics?jql=filter%3D10000&statType=" + STUB_STATS.stats[0].value);
    });


    test("Init refresh was called on render", function() {
        var refreshInitSpy = sinon.spy();
        sinon.stub(this.dashboardItem.API, "initRefresh", refreshInitSpy);

        this.dashboardItem.render(this.$el, {
            id: 10000,
            type: 'project',
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_PIECHART_DATA));

        ok(refreshInitSpy.calledOnce);
    });


    test("Moving from rendered chart to edit actually shows edit screen", function() {
        this.dashboardItem.render(this.$el, {
            id: 10000,
            type: 'project',
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });

        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_PIECHART_DATA));

        ok(this.$el.find(".piechart-with-legend").length > 0, "Should have rendered the piechart");

        this.dashboardItem.renderEdit(this.$el, {
            id: 10000,
            type: 'project',
            statType: STUB_STATS.stats[0].value,
            isConfigured: true,
            refresh: 15
        });


        this.server.requests[1].respond(200, { "Content-Type": "application/json" }, JSON.stringify(STUB_STATS));
        ok(this.$el.find(".piechart-with-legend").length === 0, "Should not have a rendered piechart");
        ok(this.$el.find(".dashboard-item-preferences-config").length === 1, "Should have rendered piechart configuraton");
    });
});
