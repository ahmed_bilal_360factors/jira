define("jira-dashboard-items/filter-results", [
    'jquery',
    'underscore',
    'jira-dashboard-items/components/filter-picker',
    'jira-dashboard-items/components/search-results-config',
    'jira-dashboard-items/components/search-results'
], function (
    $,
    _,
    FilterPicker,
    SearchResultsConfig,
    SearchResults
) {

    'use strict';

    var DashboardItem = function (API) {
        this.API = API;
    };

    DashboardItem.prototype.render = function (context, preferences) {
        var self = this;

        self.API.showLoadingBar();

        var displayPrefs = $.extend({}, preferences);
        //fix up potentially old filterIds first that may still be stored.
        displayPrefs.filterId = displayPrefs.filterId !== undefined ? displayPrefs.filterId.replace('filter-', '') : displayPrefs.filterId;

        this.searchResults = new SearchResults({
            context: context,
            preferences: displayPrefs,
            onContentLoaded: this.API.forceLayoutRefresh.bind(this.API),
            onEmptyResult: function () {
                context.append(JIRA.DashboardItem.FilterResults.Templates.noResults({
                    href: AJS.contextPath() + "/issues/?filter=" + displayPrefs.filterId
                }));
                self.API.forceLayoutRefresh();
            },
            onError: function () {
                context.empty().append(JIRA.DashboardItem.FilterResults.Templates.filterErrorResult({
                    filterId: displayPrefs.filterId
                }));
                self.API.forceLayoutRefresh();
            }
        });

        this.searchResults.render().
                done(function(data) {
                    if(data && data.title) {
                        self.API.setTitle(AJS.I18n.getText('gadget.filter.results.specific.title', data.title));
                    }
                }).always(this.API.hideLoadingBar.bind(this.API));

        this.searchResults.on("sorted", function(eventData) {
            preferences.sortBy = eventData.sortBy;
            if(self.API.isEditable()) {
                self.API.savePreferences(preferences);
            } else {
                self.render(context, preferences);
            }
        });

        self.API.initRefresh(displayPrefs, _.bind(self.render, self, context, displayPrefs));
    };

    DashboardItem.prototype.renderEdit = function (element, preferences) {
        var self = this;

        var searchResultsConfig = new SearchResultsConfig({
            context: element,
            preferences: preferences,
            onContentLoaded: function () {
                self.API.hideLoadingBar();
                self.API.forceLayoutRefresh();
            },
            onCancel: this.API.closeEdit.bind(this.API),
            onSave: function (prefs) {
                var validFilter = self.filterPicker.validate();
                if (validFilter) {
                    var selectedFilter = self.filterPicker.getValue();
                    prefs.filterId = selectedFilter.id;
                    self.API.savePreferences(prefs);
                }
                else {
                    self.API.forceLayoutRefresh();
                }
            },
            gadgetAPI: self.API
        });

        var $form = searchResultsConfig.renderConfig();
        var prefix = self.API.getGadgetId() + "-";
        $form.find("fieldset:first").prepend(JIRA.DashboardItem.Common.Config.Templates.filterPicker({
            prefix:prefix,
            id:"saved-filter"
        }));
        // Instrument the special fields
        this.filterPicker = new FilterPicker().init({
            errorContainer: element.find(".dashboard-item-error"),
            element: $form.find("#" + prefix + "saved-filter"),
            selectedValue: preferences.filterId !== undefined ? preferences.filterId.replace('filter-', '') : preferences.filterId,
            parentElement: $form
        });

        this.API.once("afterRender", function () {
            if (element.width() < 350) {
                $form.addClass("top-label");
            }
            self.API.showLoadingBar();
        });
    };

    return DashboardItem;
});
