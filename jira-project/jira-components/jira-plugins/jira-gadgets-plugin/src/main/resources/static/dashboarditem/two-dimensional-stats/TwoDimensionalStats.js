define("jira-dashboard-items/two-dimensional-stats", [
    'jquery',
    'underscore',
    'jira-dashboard-items/components/filter-picker'
], function (
    $,
    _,
    FilterPicker
) {
    "use strict";

    /**
     *
     * @param {InlineGadgetAPI} API
     * @param {Object} [options] for gadget
     * @constructor
     */
    var DashboardItem = function (API, options) {
        this.API = API;
        this.options = options || {};

        // field configurations
        this.fields = createStaticSelectFieldsConfigurations();
        this.filterPicker = new FilterPicker();
    };

    /**
     * Render the configured piechart gadget.
     * @param {node} element to apply gadget to
     * @param {Object} preferences for gadget that have been configured.
     */
    DashboardItem.prototype.render = function (element, preferences) {
        var $element = $(element);
        var self = this;

        self.API.initRefresh(preferences, _.bind(self.render, self, element, preferences));
        self.API.showLoadingBar();

        var normalizedFilterId = preferences.filterId !== undefined ? preferences.filterId.replace('filter-', '') : preferences.filterId;

        $.ajax({
            method: "GET",
            url: AJS.contextPath() + '/rest/gadget/1.0/twodimensionalfilterstats/generate',
            dataType: "json",
            data: {
                filterId: 'filter-' + normalizedFilterId,
                xstattype: preferences.xstattype,
                ystattype: preferences.ystattype,
                sortDirection: preferences.sortDirection,
                sortBy: preferences.sortBy,
                numberToShow: ( preferences.more ? 1000 : preferences.numberToShow )
            }
        }).done(function (response) {
            self.API.setTitle( AJS.I18n.getText('gadget.twodimensionalfilterstats.title.with.filter', response.xHeading) );

            if (response.totalRows === 0) {
                $element.html(JIRA.DashboardItem.TwoDimensionalStats.Templates.EmptyResultMessage({
                    href: AJS.contextPath() + "/issues/?filter=" + normalizedFilterId
                }));
                return;
            }

            prepareCssClassesToBePrinted(response);
            $element.html(JIRA.DashboardItem.TwoDimensionalStats.Templates.Table({
                yAxisLabel: response.yHeading,
                xHeaderLabels: response.firstRow.cells,
                contentRows: response.rows,
                totalRows: response.totalRows,
                filterUrl: response.filter.filterUrl,
                filterTitle: response.filter.filterTitle,
                visibleContentRows: response.rows.length - 1, // - 1 because totals row is also included in response.rows
                showMore: preferences.more
            }));
            AJS.trigger('analyticsEvent', {name: 'jira.dashboard.gadgets.twodstats.rowscount', data: {value: response.rows.length}});
            AJS.trigger('analyticsEvent', {name: 'jira.dashboard.gadgets.twodstats.colscount', data: {value: response.firstRow.cells.length}});

            $element.find('.content-control-link').click( function(e) {
                preferences.more = $(e.target).data('more');
                if(self.API.isEditable()) {
                    self.API.savePreferences(preferences);
                } else {
                    self.render(element, preferences);
                }
            });
        }).fail(function () {
            $element.html(JIRA.DashboardItem.TwoDimensionalStats.Templates.ServerErrorMessage({
                filterId: normalizedFilterId
            }));
        }).always(function () {
            self.API.hideLoadingBar();

            self.API.forceLayoutRefresh();

            $element.find('.two-d-container').
                // check if horizontal scrollbar is present
                toggleClass('h-scrollbar', $element.find('.table-container').width() < $element.find('.table-container > .extra-container > table').width());
        });
    };

    /**
     * Render the configuration screen for the piechart gadget
     * @param {jQuery} element to render into
     * @param {Object} saved preferences for this gadget
     */
    DashboardItem.prototype.renderEdit = function (element, preferences) {
        var $element = $(element);
        var self = this;

        self.API.setTitle( AJS.I18n.getText('gadget.twodimensionalfilterstats.title') );
        self.API.showLoadingBar();

        $.ajax({
            method: "GET",
            url: AJS.contextPath() + '/rest/gadget/1.0/statTypes',
            dataType: "json"
        }).done(function (response) {
            self.fields.statTypeOptions = self._updateStatTypeFieldNames(response.stats);

            $element.html(JIRA.DashboardItem.TwoDimensionalStats.Templates.Form({
                prefix: self.API.getGadgetId() + "-",
                preferences: preferences,
                fields: self.fields
            }));

            self.filterPicker.init({
                gadgetErrorContainer: $element.find(".dashboard-item-error"),
                element: $element.find("#" + self.API.getGadgetId() + "-filterId"),
                selectedValue: preferences.filterId !== undefined ? preferences.filterId.replace('filter-', '') : preferences.filterId,
                parentElement: $element
            });
            $element.find('.buttons-container .cancel').click(self.API.closeEdit.bind(self.API));
            $element.find('form').on("submit", function(e) {
                e.preventDefault();
                var $form = $(e.target);

                var preferences = self._getPreferencesObject($form);
                preferences.filterId = 'filter-' + (self.filterPicker.validate() ? self.filterPicker.getValue().id : ''); // prefix "filter-" is necessary to make the stored value compatible with the old gadget
                preferences.refresh = self.API.getRefreshFieldValue();
                preferences.more = false;

                if (self._validPreferences($form, preferences)) {
                    self.API.savePreferences(preferences);
                }
                else {
                    self.API.forceLayoutRefresh();
                }
            });
        }).fail(function() {
            $element.html(JIRA.DashboardItem.TwoDimensionalStats.Templates.ServerErrorMessage());
        }).always(function () {
            self.API.hideLoadingBar();

            self.API.forceLayoutRefresh();
        });
    };

    /**
     * Prepare the attribute classes to be rendered via Soy templates
     * @param response with classes attributes converted from Array to String.join(' ')
     * @private
     */
    function prepareCssClassesToBePrinted(response) {
        _.each( response.firstRow.cells, function(cell) {
            if( cell['classes'] && cell['classes'].join )
                cell['classes'] = cell['classes'].join(' ');
        });
        _.each( response.rows, function(row) {
            _.each( row.cells, function(cell) {
                if( cell['classes'] && cell['classes'].join )
                    cell['classes'] = cell['classes'].join(' ');
            });
        });
    }

    /**
     * Extract data from a form using jQuery.serializeArray. An internal whitelist is used to control data read.
     * @param $form
     * @returns a map of preferences, where keys are the field names and values are the input values
     * @private
     */
    DashboardItem.prototype._getPreferencesObject = function($form) {
        var formData = $form.serializeArray();
        var paramsWhitelist = ['filterId', 'numberToShow', 'sortBy', 'sortDirection', 'xstattype', 'ystattype'];
        var preferences = {};

        _.each( formData, function(element) {
            if( _.indexOf(paramsWhitelist, element.name) == -1 )
                return;

            preferences[element.name] = element.value;
        });

        return preferences;
    };

    /**
     * Response from statType service should use .text instead of .label to be used by AUI templates
     * @param statTypes
     * @returns a map with label properties replaced by text properties
     * @private
     */
    DashboardItem.prototype._updateStatTypeFieldNames = function(statTypes) {
        var i = statTypes.length;
        while ( i-- ) {
            statTypes[i].text = statTypes[i].label;
            delete statTypes[i].label;
        }

        return statTypes;
    };

    /**
     * Verify given preferences. Fields verified: filterId and numberToShow
     * @param $form
     * @param preferences
     * @returns {boolean}
     * @private
     */
    DashboardItem.prototype._validPreferences = function($form, preferences) {
        $form.find('.error').remove();

        if ( preferences.filterId === '' || preferences.filterId === 'filter-' ) {
            var field = $form.find('[name=filterId]');
            field.parent().append('<div class="error">' + AJS.I18n.getText('gadget.common.filter.none.selected') + '</div>');
        }

        if ( !this._validNumberToShow( preferences.numberToShow ) ) {
            var field = $form.find('input[name=numberToShow]');
            field.after('<div class="error">' + AJS.I18n.getText('gadget.common.num.nan') + '</div>');
        }

        $form.find('.error').show();

        return $form.find('.error').size() == 0;
    };

    /**
     * Is this a validNumberToShow? (number AND 1 <= x <= 50)
     * @param number
     * @returns {boolean}
     * @private
     */
    DashboardItem.prototype._validNumberToShow = function(number) {
        return ( (/^\d+$/).test(number) === true && number >= 1 && number <= 50 );
    };

    /**
     * Create a configuration map to render static select fields in the edit form
     * @returns {Object} map of fields with two properties each: text and value
     */
    function createStaticSelectFieldsConfigurations() {
        var fields = {};

        fields.sortByOptions = [
            {
                text: AJS.I18n.getText('gadget.twodimensionalfilterstats.sort.by.natural.label'),
                value: 'natural'
            },
            {
                text: AJS.I18n.getText('gadget.twodimensionalfilterstats.sort.by.total.label'),
                value: 'total'
            }
        ];
        fields.sortDirectionOptions = [
            {
                text: AJS.I18n.getText('gadget.twodimensionalfilterstats.sort.direction.asc.label'),
                value: 'asc'
            },
            {
                text: AJS.I18n.getText('gadget.twodimensionalfilterstats.sort.direction.desc.label'),
                value: 'desc'
            }
        ];

        return fields;
    }

    return DashboardItem;
});