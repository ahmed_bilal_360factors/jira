AJS.test.require("com.atlassian.jira.gadgets:common-test-resources");
AJS.test.require("com.atlassian.jira.gadgets:filter-results-dashboard-item-resources", function () {

    'use strict';

    module('jira-dashboard-items/filter-results', {
        setup: function () {
            this.mockedContext = AJS.test.context();

            this.API = AJS.$.extend({}, DashboardItem.Mocks.API);
            this.APIStub = sinon.stub(this.API);

            this.$el = AJS.$("<div/>");

            AJS.$("#qunit-fixture").append(this.$el);
            this.server = sinon.fakeServer.create();
        },

        teardown: function () {
            this.server.restore();
            this.$el.empty();
        },

        assertCommonAPIMethodsCalled: function () {
            sinon.assert.calledOnce(this.APIStub.showLoadingBar, "Loading bar was shown");
            sinon.assert.calledOnce(this.APIStub.initRefresh, "Refresh of the dashboarditem was initialised");
            sinon.assert.calledOnce(this.APIStub.forceLayoutRefresh, "Layout refreshed after render");
        },

        submitConfigFormWithValues: function(numToShowValue, isFilterPickerValid, isColumnPickerValid) {
            this.mockedContext.mock('jira-dashboard-items/components/filter-picker', createMockPicker({id: "12345"}, isFilterPickerValid));
            this.mockedContext.mock('jira-dashboard-items/components/column-picker', createMockPicker("summary|description", isColumnPickerValid));

            var FilterResults = this.mockedContext.require('jira-dashboard-items/filter-results');
            var dashboardItem = new FilterResults(this.API);
            dashboardItem.renderEdit(this.$el, {});

            this.$el.find("input[name=number-results]").val(numToShowValue);
            this.$el.find("form").submit();
        },

        renderFilterResults: function(filterId, normalizedFilterId, callBackFunction) {
            var deferred = AJS.$.Deferred();
            this.mockedContext.mock('jira-dashboard-items/components/search-results', function (opts) {
                equal(normalizedFilterId, opts.preferences.filterId);

                return {
                    render: function () {
                        opts[callBackFunction]();
                        return deferred;
                    },
                    on: function(event, cb) {}
                };
            });

            var FilterResults = this.mockedContext.require('jira-dashboard-items/filter-results');
            var dashboardItem = new FilterResults(this.API);

            dashboardItem.render(this.$el, {
                filterId: filterId
            });

            return deferred;
        }
    });

    function createMockPicker(value, isValid) {
        return function () {
            return {
                init: function () {
                    return this;
                },
                getValue: function () {
                    return value;
                },
                validate: function () {
                    return isValid;
                }
            }
        }
    }

    test("Render with old filter id pref normalizes filter id and calls appropriate API methods", function () {
        var deferred = this.renderFilterResults("filter-12345", 12345, "onContentLoaded"); //filter-12345 should get normalised to 12345

        this.assertCommonAPIMethodsCalled();

        deferred.resolve({title: "New Filter"});
        sinon.assert.calledOnce(this.APIStub.hideLoadingBar, "Hide the loading bar");
        sinon.assert.calledOnce(this.APIStub.setTitle, "Title was set after load");
        sinon.assert.calledWith(this.APIStub.setTitle, "gadget.filter.results.specific.title");
        equal('', this.$el.text());
    });

    test("Render may error out", function () {
        var deferred = this.renderFilterResults("12345", 12345, "onError");

        this.assertCommonAPIMethodsCalled();

        deferred.resolve();
        sinon.assert.calledOnce(this.APIStub.hideLoadingBar, "Hide the loading bar");
        equal("dashboard.item.filter.results.invalid.filter", this.$el.text(), "Element contains error");
    });

    test("Render may return no results out", function () {
        var deferred = this.renderFilterResults("12345", 12345, "onEmptyResult");

        this.assertCommonAPIMethodsCalled();

        deferred.resolve();
        sinon.assert.calledOnce(this.APIStub.hideLoadingBar, "Hide the loading bar");
        equal("gadget.issuetable.common.empty", this.$el.text(), "Element contains error");
    });

    test("Validation of number to show field triggers error", function () {
        this.submitConfigFormWithValues("not a number!", true, true);

        sinon.assert.calledOnce(this.APIStub.forceLayoutRefresh, "Layout refreshed after render");
        sinon.assert.notCalled(this.APIStub.savePreferences, "savePrefs wasn't called");

        var numberResultField = this.$el.find("input[name=number-results]");
        ok(numberResultField.next(".error").is(":visible"), "Error shown");
        equal("gadget.common.num.nan", numberResultField.next(".error").text(), "Correct message shown");
    });

    test("Validation of filter picker triggers error", function () {
        this.submitConfigFormWithValues("30", false, true);

        sinon.assert.calledOnce(this.APIStub.forceLayoutRefresh, "Layout refreshed after render");
        sinon.assert.notCalled(this.APIStub.savePreferences, "savePrefs wasn't called");
    });

    test("Validation of column picker triggers error", function () {
        this.submitConfigFormWithValues("30", true, false);

        sinon.assert.calledOnce(this.APIStub.forceLayoutRefresh, "Layout refreshed after render");
        sinon.assert.notCalled(this.APIStub.savePreferences, "savePrefs wasn't called");
    });

    test("Save Prefs called once validation passes", function () {
        this.API.getRefreshFieldValue = sinon.stub().returns('false');
        this.submitConfigFormWithValues("30", true, true);

        sinon.assert.calledOnce(this.APIStub.savePreferences, "savePrefs was called");
        sinon.assert.calledWith(this.APIStub.savePreferences, {
            num: "30",
            filterId: "12345",
            refresh: "false",
            columnNames: "summary|description"
        });
    });
});


