define("jira-dashboard-items/components/search-results-config", [
    'jquery',
    'jira-dashboard-items/components/column-picker'
], function (
    $,
    ColumnPicker
) {
    'use strict';

    return function (options) {
        var context = $(options.context);
        var preferences = options.preferences;

        return {
            renderConfig: function () {
                // Random prefix to avoid collisions with other instances of this gadget
                var prefix = (Math.random() + 1).toString(36).substring(7) + "-";

                // Render the template
                context.empty().html(JIRA.DashboardItem.Common.SearchResults.Templates.config({
                    prefix: prefix,
                    preferences: {
                        num: preferences.num,
                        refresh: preferences.refresh,
                        isConfigured: preferences.isConfigured
                    }
                }));

                var form = context.find("form");

                var columnPicker = new ColumnPicker().init({
                    errorContainer: context.find(".dashboard-item-error"),
                    tableElement: form.find("#" + prefix + "fields-to-display-table"),
                    fieldElement: form.find("#" + prefix + "fields-to-display-select"),
                    columnNames: preferences.columnNames,
                    onContentLoaded: options.onContentLoaded
                });

                function validateNumberResults() {
                    var field = form.find("input[name=number-results]");
                    var value = field.val();

                    if (value > 0 && value <= 50) {
                        field.parents(".field-group").find(".error").hide();
                        return true;
                    } else {
                        field.parents(".field-group").find(".error").show();
                        return false;
                    }
                }

                function validateFields() {
                    var numberResultsIsValid = validateNumberResults();
                    var fieldsToDisplayIsValid = columnPicker.validate();

                    return numberResultsIsValid && fieldsToDisplayIsValid;
                }

                form.on("submit", _.bind(function (e) {
                    e.preventDefault();
                    if (!validateFields()) {
                        options.onContentLoaded();
                        return;
                    }

                    var form = $(e.target);
                    options.onSave({
                        "num": form.find("input[name=number-results]").val(),
                        "refresh": options.gadgetAPI.getRefreshFieldValue('refresh-interval'),
                        "columnNames": columnPicker.getValue()
                    });
                }, this));

                form.find(".buttons-container .cancel").on("click", _.bind(function () {
                    options.onCancel();
                }, this));
                
                return form;
            }
        }
    };
});