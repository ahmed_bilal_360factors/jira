define('jira-dashboard-items/components/charts/piechart', [
    'jquery'
], function(
    $
) {

    function DEFAULT_COLOUR_PICKER(colours, numberElements) {
        var LIME_GREEN = "#8eb021",
            BLUE = "#3b7fc4",
            RED = "#d04437",
            YELLOW = "#f6c342",
            VIOLET = "#654982",
            PINK = "#f691b2",
            GRAY = "#999999",
            ASH_GRAY = "#cccccc",
            BROWN = "#815b3a",
            CHEETO_ORANGE = "#f79232",
            TAN = "#f1a257",
            LIGHT_BROWN = "#d39c3f",
            CYAN = "#59afe1",
            SLATE = "#4a6785",
            COOL_BLUE = "#84bbc6",
            MID_GREEN = "#67ab49",
            MAUVE = "#ac707a",
            BRIGHT_PINK = "#f15c75";


        colours = colours || [
            LIME_GREEN,
            BLUE,
            RED,
            YELLOW,
            VIOLET,
            PINK,
            GRAY,
            BROWN,
            CHEETO_ORANGE,
            CYAN,
            BRIGHT_PINK
        ];

        /**
         *
         * @type {colourPickerFunction}
         */
        function getNextColour(piechart, data, index, sectorElement) {
            var colourIndex = index % colours.length;
            var isLast = (index + 1 === numberElements);
            var isFirstColour = (colourIndex === 0);
            var oneSector = (numberElements === 1);

            /**
             * This solves problems where if there is one more sector then the number of colours two sectors have the
             * same colour next to each other. E.g. colours (A B C) and 4 sectors would place colours: A B C A, which
             * is two A's together. This code will make it A B C B.
             */
            if (isLast && isFirstColour && !oneSector) {
                colourIndex = 1;
            }
            return colours[colourIndex];
        }

        return getNextColour;
    }

    function noop() {}

    /**
     * Class used to represent piechart options.
     * @name PieChartOptions
     * @global
     *
     * Required parameters
     * @property {number}               radius  of the pie chart
     * @property {getValueFunction}     getValue given a data element, return the value
     *
     * Design parameters
     * @property {boolean}              [centerChart]  whether the chart should be centered
     * @property {boolean}              [clickable]  whether the sectors are clickable and should have a click cursor on hover
     * @property {colourPickerFunction} [colourPicker]  given data and an index return what colour it should be
     * @property {string[]}             [colours] that should be displayed
     * @property {number}               [hoverExpansion] size of expansion when hovered over
     * @property {number}               [innerRadius]  used to create a donut pie chart
     * @property {number}               [sectorGap] gap size between sectors
     *
     * Functionality parameters
     * @property {getPrimaryTextFunction}[getPrimaryText] function called to populate the primary text within the piechart
     * @property {getSecondaryTextFunction}[getSecondaryText] function called to populate the secondary text within the piechart
     * @property {onClickFunction}      [onClick]  executed when a sector is clicked
     * @property {onEachSectorFunction} [onEachSector] executed on each sector where own content/changes can be applied
     * @property {onMouseOverFunction}  [onMouseOver] executed when hover over a sector
     * @property {onMouseOutFunction}   [onMouseOut]  executed when leaving the sector
     */

    /**
     * Functions that can be passed into PieChart
     **/
    /**
     * Function to get the value for a pie chart sector.
     * @name getValueFunction
     * @function
     * @global
     * @arg data {Object} - The search term to highlight.
     * @return {number} value for sector
     */

    /**
     * Function to get the value for a pie chart sector.
     * @name getPrimaryTextFunction
     * @function
     * @global
     * @arg data {Object} - The search term to highlight.
     * @arg index {number} - The index for the data item
     * @return {number} value for sector
     */

    /**
     * Function to get the value for a pie chart sector.
     * @name getSecondaryTextFunction
     * @function
     * @global
     * @arg data {Object} - The search term to highlight.
     * @arg index {number} - The index for the data item
     * @return {number} value for sector
     */

    /**
     * Function that takes pie chart information, manipulates or returns information.
     * @name colourPickerFunction
     * @function
     * @global
     *
     * @param {PieChart} piechart that the colour is being used on
     * @param {Object} data for sector
     * @param {number} index of sector
     * @param {node} sectorElement for sector
     * @return {string} colour for selection.
     */

    /**
     * Function that takes pie chart information and does some event on a click.
     * @name onClickFunction
     * @function
     * @global
     * @param {PieChart} piechart that the colour is being used on
     * @param {Object} data for sector
     * @param {number} index of sector
     * @param {node} sectorElement for sector
     */

    /**
     * Executed on each sector when a piechart is created.
     * @name onEachSectorFunction
     * @function
     * @global
     * @param {PieChart} piechart that the colour is being used on
     * @param {Object} data for sector
     * @param {number} index of sector
     * @param {node} sectorElement for sector
     */

    /**
     * Function that takes pie chart information and does some event on a mouseover on a sector.
     * @name onMouseOverFunction
     * @function
     * @global
     * @param {PieChart} piechart that the colour is being used on
     * @param {Object} data for sector
     * @param {number} index of sector
     * @param {node} sectorElement for sector
     */

    /**
     * Function that takes pie chart information and does some event on a mouseout on a sector.
     * @name onMouseOutFunction
     * @function
     * @global
     * @param {PieChart} piechart that the colour is being used on
     * @param {Object} data for sector
     * @param {number} index of sector
     * @param {node} sectorElement for sector
     */

    /**
     * Render function
     * @name renderFunction
     * @function
     * @arg {node} element to render piechart onto
     */

    /**
     * @constructor
     *
     * @name PieChart
     * @class
     *
     * Generate a pie chart
     * @param {Object[]} data  to pass into the chart. Note: Should be in sorted order otherwise some colours may appear
     *           next to each other.
     *
     * @param {PieChartOptions} options fo the PieChart
     *
     * @return {PieChart}
     *
     */
    var PieChart = function(data, options) {
        this.radius = options.radius;
        this.data = data;
        this.getValue = options.getValue || function(dataElement) { return dataElement; };


        this.centerChart = options.centerChart || false;
        this.clickable = options.clickable || false;
        this.colourPicker = options.colourPicker || DEFAULT_COLOUR_PICKER(options.colours, this.data.length);
        this.hoverExpansion = options.hoverExpansion || 0;
        this.innerRadius = options.innerRadius || 0;
        this.sectorGap = options.sectorGap || 2;
        this.innerCirclePadding = Math.min(10, Math.max(this.innerRadius, 0));

        this.getPrimaryText = options.getPrimaryText || noop;
        this.getSecondaryText = options.getSecondaryText || noop;
        this.onClick = options.onClick || noop;
        this.onEachArc = options.onEachArc || noop;
        this.onMouseOver = options.onMouseOver || noop;
        this.onMouseOut = options.onMouseOut || noop;

        this.pieCenter = this.radius + this.hoverExpansion;

        this.width = this.pieCenter * 2;
        this.height = this.pieCenter * 2;

        this.render = function(element) {
            var self = this;

            var chart = d3.select(element[0])
                .append("svg")
                .data([this.data])
                .classed({'piechart': true, 'centered': this.centerChart})
                .attr("width", this.width)
                .attr("height", this.height);

            chart
                .append("g")
                .classed({'piechart-graph': true})
                .attr("transform", "translate(" + this.pieCenter  + "," + this.pieCenter + ")");

            var centerTextBlockOptions = {
                width: 2 * (this.innerRadius - this.innerCirclePadding),
                height: Math.sqrt(2 * this.innerRadius * this.innerRadius),
                dx: (this.pieCenter - this.innerRadius + this.innerCirclePadding),
                dy: (this.pieCenter - this.innerRadius + 2 * this.innerCirclePadding)
            };

            var arc = d3.svg.arc()
                .outerRadius(this.radius)
                .innerRadius(this.innerRadius);

            var arcLarger = d3.svg.arc()
                .outerRadius(this.radius + this.hoverExpansion)
                .innerRadius(this.innerRadius);

            var pie = d3.layout.pie()
                //Given a sector what is the value for it.
                .value(this.getValue);

            this.arcs = chart.select("g.piechart-graph").selectAll("g")
                .data(pie)
                .enter()
                .append("g")
                .classed({'piechart-arc': true, 'clickable': this.clickable})

                .each(function(sector, index) {
                    self.onEachArc(self, sector.data, index, $(d3.select(this).node()));
                });



            //Add primary text

            this.arcs
                .append("text")
                .attr("width", centerTextBlockOptions.width)
                .attr("text-anchor", "middle")
                .classed("piechart-center-primary", true)
                .attr("y", 5)
                .attr("fill", function(sector, index) {
                    return self.colourPicker(self, sector.data, index, d3.select(this).node());
                })
                .text(function(sector, index) {
                    return self.getPrimaryText(sector.data, index);
                });

            //Add secondary text
            this.arcs
                .append("text")
                .attr("width", centerTextBlockOptions.width)
                .attr("text-anchor", "middle")
                .classed("piechart-center-secondary", true)
                .attr("fill", "#333333")
                .attr("y", 25)
                .text(function(sector, index) {
                    return self.getSecondaryText(sector.data, index);
                });


            this.arcs.append("path")
                .attr('stroke', '#fff')
                .attr('stroke-width', this.sectorGap)
                .attr("fill", function(sector, index) {
                    return self.colourPicker(self, sector.data, index, d3.select(this).node());
                })
                .classed({'piechart-fill': true})
                .attr("d", arc);

            this.arcs.on("click", function(sector, index) {
                self.onClick(self, sector.data, index, d3.select(this).node());
            });

            this.mouseOver = function(dataIndex) {
                _mouseOver.call(this.arcs[0][dataIndex], this.arcs.data()[dataIndex], dataIndex);
            };

            this.mouseOut = function(dataIndex) {
                _mouseOut.call(this.arcs[0][dataIndex], this.arcs.data()[dataIndex], dataIndex);
            };

            var _mouseOver = function(sector, index) {
                d3.select(this).classed({'piechart-arc-highlighted': true});
                d3.select(this).select("path").transition().duration(100).attr("d", arcLarger);
                self.onMouseOver(self, sector.data, index, d3.select(this).node());
            };

            var _mouseOut = function(sector, index) {
                d3.select(this).classed({'piechart-arc-highlighted': false});
                d3.select(this).select("path").transition().duration(100).attr("d", arc);
                self.onMouseOut(self, sector.data, index, d3.select(this).node());
            };

            this.arcs.on("mouseover", function(sector, index) {
                _mouseOver.call(this, sector, index);
            });

            this.arcs.on("mouseout", function(sector, index) {
                _mouseOut.call(this, sector, index);
            });
        };
    };

    return PieChart;
});