define("jira-dashboard-items/watched", [
    'jquery',
    'underscore',
    'jira-dashboard-items/components/search-results-config',
    'jira-dashboard-items/components/search-results'
], function (
    $,
    _,
    SearchResultsConfig,
    SearchResults
) {
    'use strict';

    var DashboardItem = function (API) {
        this.API = API;
    };

    DashboardItem.prototype.render = function (context, preferences) {
        var self = this;

        self.API.showLoadingBar();
        self.API.setTitle(AJS.I18n.getText("gadget.watched.title"));

        var displayPrefs = $.extend({}, preferences);
        displayPrefs.jql = displayPrefs.showResolved ? "issue in watchedIssues() order by watchers desc" : "issue in watchedIssues() AND resolution = EMPTY order by watchers desc";

        if(displayPrefs.showTotalWatches) {
            displayPrefs.columnNames += "|watches";
        }

        this.searchResults = new SearchResults({
            context: context,
            tableContext: "jira.table.cols.watched",
            preferences: displayPrefs,
            onContentLoaded: this.API.forceLayoutRefresh.bind(this.API),
            onEmptyResult: function () {
                context.append(JIRA.DashboardItem.Watched.Templates.noResults({
                    href: AJS.contextPath() + "/issues/?jql=" + encodeURIComponent(displayPrefs.jql)
                }));
                self.API.forceLayoutRefresh();
            },
            onError: function () {
                context.empty().append(JIRA.DashboardItem.Watched.Templates.errorResult());
                self.API.forceLayoutRefresh();
            }
        });

        this.searchResults.render().always(this.API.hideLoadingBar.bind(this.API));
        this.searchResults.on("sorted", function(eventData) {
            preferences.sortBy = eventData.sortBy;
            if(self.API.isEditable()) {
                self.API.savePreferences(preferences);
            } else {
                self.render(context, preferences);
            }
        });
        self.API.initRefresh(displayPrefs, this.searchResults.render);
    };

    DashboardItem.prototype.renderEdit = function (element, preferences) {
        var self = this;

        var searchResultsConfig = new SearchResultsConfig({
            context: element,
            preferences: preferences,
            onContentLoaded: function () {
                self.API.hideLoadingBar();
                self.API.forceLayoutRefresh();
            },
            onCancel: this.API.closeEdit.bind(this.API),
            onSave: function (prefs) {
                prefs.showResolved = element.find("input[name=showResolved]").is(":checked");
                prefs.showTotalWatches = element.find("input[name=showTotalWatches]").is(":checked");
                self.API.savePreferences(prefs);
            },
            gadgetAPI: self.API
        });

        var $form = searchResultsConfig.renderConfig();
        var prefix = self.API.getGadgetId() + "-";
        $form.find(".field-group:last").before(JIRA.DashboardItem.Watched.Templates.extraConfig({
            checkboxes:[
                {
                    id: prefix + "show-resolved",
                    name: "showResolved",
                    labelText: AJS.I18n.getText("gadget.voted.field.showresolved.name"),
                    isChecked: !!preferences.showResolved
                },
                {
                    id: prefix + "show-totals",
                    name: "showTotalWatches",
                    labelText: AJS.I18n.getText("gadget.watched.field.showtotal.name"),
                    isChecked: !!preferences.showTotalWatches
                }
            ]
        }));

        this.API.once("afterRender", function () {
            if (element.width() < 350) {
                $form.addClass("top-label");
            }
            self.API.showLoadingBar();
        });
    };

    return DashboardItem;
});
