define("jira-dashboard-items/stats", [
    'jquery',
    'jira-dashboard-items/components/autocomplete/project-filter-autocomplete',
    'underscore'
], function(
    $,
    ProjectFilterAutoComplete,
    _
) {
    'use strict';

    /**
     *
     * @param {InlineGadgetAPI} API
     * @param {Object} [options] for gadget
     * @constructor
     */
    var DashboardItem = function(API, options) {
        this.API = API;
        this.options = options || {};
    };

    /**
     * Extract type and ID fields from projectOrFilterId and add them to the preferences object.
     * @param {Object} [preferences] user prefs for this gadget
     */
    function extractFieldsFromProjectOrFilterId(preferences) {
        if (preferences.projectOrFilterId && !(preferences.type && preferences.id)) {
            var projectOrFilterSplit = preferences.projectOrFilterId.split("-");
            preferences.type = projectOrFilterSplit[0];
            preferences.id = projectOrFilterSplit[1];
        }
    }

    /**
     * Extract data from a form using jQuery.serializeArray. An internal whitelist is used to control data read.
     * Note: 'refresh-interval' is mapped to 'refresh' here
     * @param {Object} [$form]
     * @returns {Object} a map of preferences, where keys are the field names and values are the input values
     * @private
     */
    function getPreferencesObject($form) {
        var formData = $form.serializeArray(),
                paramsWhitelist = ['name', 'type', 'id', 'statType', 'refresh-interval', 'sortBy', 'sortDirection', 'includeResolvedIssues'],
                preferences = {};

        _.each( formData, function(element) {
            if( _.indexOf(paramsWhitelist, element.name) === -1 )
            {
                return;
            }
            if (element.name === 'refresh-interval') {
                preferences['refresh'] = element.value;
            }
            else {
                preferences[element.name] = element.value;
            }
        });

        return preferences;
    }

    /**
     * Construct a new projectOfFilterId and add it to the preferences object
     * @param {Object} [preferences] the preferences object
     */
    function updateProjectOrFilterId(preferences) {
        preferences.projectOrFilterId = preferences.type + "-" + preferences.id;
    }

    function getTitle(response) {
        var title = '';
        title += AJS.I18n.getText('gadget.stats.title') + ': ';
        title += response.filterOrProjectName + ' (' + response.statTypeDescription + ')';
        return title;
    }

    /**
     * Render the configured stats gadget.
     * @param {node} element to apply gadget to
     * @param {Object} preferences for gadget that have been configured.
     */
    DashboardItem.prototype.render = function(element, preferences) {

        var gadget = this;
        var $gadgetElement = $(element);
        $gadgetElement.addClass('stats-gadget');

        gadget.API.showLoadingBar();

        extractFieldsFromProjectOrFilterId(preferences);

        gadget.API.initRefresh(preferences, _.bind(gadget.render, gadget, element, preferences));

        $.ajax({
            method: "GET",
            url: AJS.contextPath() + '/rest/gadget/1.0/stats/generate', // use the old opensocial gadget's rest endpoint
            dataType: "json",
            data: {
                includeResolvedIssues: preferences.includeResolvedIssues,
                projectOrFilterId: preferences.type + "-" + preferences.id,
                sortBy: preferences.sortBy,
                sortDirection: preferences.sortDirection,
                statType: preferences.statType
            }
        }).done(function(response) {

            if (!response.totalIssueCount || response.totalIssueCount === 0) {
                $gadgetElement.html(JIRA.DashboardItem.Stats.Templates.NoStatsContent());
            } else {

                // Random prefix to avoid collisions with other instances of this gadget
                var prefix = gadget.API.getGadgetId() + "-";

                $gadgetElement.html(JIRA.DashboardItem.Stats.Templates.render({
                    data: response,
                    prefix: prefix
                }));
            }
            gadget.API.setTitle(getTitle(response));

        }).fail(function(message) {
            var filterUrl = AJS.contextPath() + "/issues/";
            if(preferences.type === "project") {
                filterUrl += "?jql=" + encodeURIComponent("project = "+ preferences.id);
            } else {
                filterUrl += "?filter=" + encodeURIComponent(preferences.id);
            }

            $gadgetElement.html(JIRA.DashboardItem.Stats.Templates.Errors({
                queryType: preferences.type,
                filterUrl: filterUrl
            }));
        }).always(function() {
            gadget.API.hideLoadingBar();
            gadget.API.forceLayoutRefresh();
        });
    };

    /**
     * Render the configuration screen for the stats gadget
     * @param {JQuery} element to render into
     * @param {Object} preferences for object.
     */
    DashboardItem.prototype.renderEdit = function(element, preferences) {
        var gadget = this;
        var $gadgetElement = $(element);
        $gadgetElement.addClass('stats-gadget');

        gadget.API.showLoadingBar();
        $.ajax({
            method: "GET",
            url: AJS.contextPath() + "/rest/gadget/1.0/statTypes" // use the old opensocial gadget's rest endpoint
        }).done(function(data) {

            // Random prefix to avoid collisions with other instances of this gadget
            var prefix = gadget.API.getGadgetId()+ "-";

            extractFieldsFromProjectOrFilterId(preferences);

            if (!preferences.name && preferences.type && preferences.id) {
                preferences.name = preferences.type + "-" + preferences.id;
            }

            // mark preferences.name as already escaped so soy doesn't re-escape it
            if (preferences.name) {
                preferences.name = new soydata.SanitizedHtml(preferences.name);
            }

            $gadgetElement.html(JIRA.DashboardItem.Stats.Templates.Configuration({
                prefix: prefix,
                statTypes: data.stats,
                preferences: preferences
            }));

            var projectFilterOptions = {
                fieldID: prefix + "project-filter-picker",
                parentElement: $gadgetElement,
                maxHeight: 140
            };

            //Mostly used for testing where we don't want a delay;
            if (gadget.options.delay) {
                projectFilterOptions.delay = gadget.options.delay;
            }

            ProjectFilterAutoComplete(projectFilterOptions);

            var form = $("form", $gadgetElement);

            var cancelButton = $(".cancel", form);
            if (cancelButton) {
                cancelButton.click(function() {
                    gadget.API.closeEdit();
                });
            }

            function completeValidation(preferences, form) {
                if (!preferences.id || !preferences.type) {
                    $(".projectOrFilter-error", form).text(AJS.I18n.getText('gadget.common.required.query')).show();
                    return false;
                }

                return true;
            }

            form.on("submit",function(e){
                e.preventDefault();

                var $form = $(e.target);

                var preferences = getPreferencesObject($form);
                updateProjectOrFilterId(preferences);

                if (completeValidation(preferences, $form)) {
                    gadget.API.savePreferences(preferences);
                }
            });

        }).fail(function() {
            $gadgetElement.html(JIRA.DashboardItem.Stats.Templates.ErrorRenderingConfiguration());
        }).always(function() {
            gadget.API.hideLoadingBar();
            gadget.API.forceLayoutRefresh();
        });
    };

    return DashboardItem;
});