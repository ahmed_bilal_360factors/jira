AJS.test.require("com.atlassian.jira.gadgets:common-test-resources");
AJS.test.require("com.atlassian.jira.gadgets:filter-picker-component", function () {

    'use strict';

    var FilterPicker = require('jira-dashboard-items/components/filter-picker');

    var sampleResponse = {"filters": [
        {"id": "11023", "name": "\"><IMG SRC=\"javascript: alert('XSS')\"", "nameHtml": "&quot;&gt;&lt;IMG SRC=&quot;javascript:<strong>a</strong>lert(&#39;XSS&#39;)&quot;", "descHtml": ""},
        {"id": "10329", "name": "5.0: Issues Pushed forward to iteration 11 AND Escalated in Priority", "nameHtml": "5.0: Issues Pushed forward to iteration 11 <strong>A</strong>ND Escalated in Priority", "descHtml": "<strong>A</strong>ll those issues which have been pused to iteration 11 <strong>a</strong>nd then priority Escalated :)."}
    ]};
    var sampleFilterResponse = {
        "self": "http://localhost:8090/jira/rest/api/2/filter/10347",
        "id": "10347",
        "name": "@everybody #will# get acce$$ to thi$ fi*lte`_everyone",
        "description": "",
        "owner": {
            "self": "http://localhost:8090/jira/rest/api/2/user?username=fred",
            "key": "fred",
            "name": "fred",
            "avatarUrls": {"16x16": "http://localhost:8090/jira/secure/useravatar?size=xsmall&avatarId=10092", "24x24": "http://localhost:8090/jira/secure/useravatar?size=small&avatarId=10092", "32x32": "http://localhost:8090/jira/secure/useravatar?size=medium&avatarId=10092", "48x48": "http://localhost:8090/jira/secure/useravatar?avatarId=10092"},
            "displayName": "Frederick Frederalburger",
            "active": true
        },
        "jql": "issuetype = bug",
        "viewUrl": "http://localhost:8090/jira/secure/IssueNavigator.jspa?mode=hide&requestId=10347",
        "searchUrl": "http://localhost:8090/jira/rest/api/2/search?jql=issuetype+%3D+bug",
        "favourite": true,
        "sharePermissions": [
            {"id": 10458, "type": "global"}
        ],
        "sharedUsers": {"size": 47, "items": [], "max-results": 1000, "start-index": 0, "end-index": 0},
        "subscriptions": {"size": 0, "items": [], "max-results": 1000, "start-index": 0, "end-index": 0}
    };
    var emptyResponse = {};


    module('jira-dashboard-items/components/filter-picker', {
        initPicker: function (filterId) {
            this.autocomplete = new FilterPicker().init({
                errorContainer: this.errorContainer,
                element: this.autocompleteEl,
                selectedValue: filterId,
                parentElement: this.$el
            });
        },

        setup: function () {
            this.$el = AJS.$("<div/>");

            this.autocompleteEl = AJS.$('<select id="autocomplete"></select>');
            this.errorContainer = AJS.$("<div/>");
            this.advancedSearchResultId = AJS.$('<input id="filter_autocomplete_id"/>');
            this.advancedSearchResultName = AJS.$('<input id="filter_autocomplete_name"/>');

            this.$el.html(this.autocompleteEl);
            this.$el.append(this.errorContainer);
            this.$el.append(this.advancedSearchResultId);
            this.$el.append(this.advancedSearchResultName);

            AJS.$("#qunit-fixture").append(this.$el);

            this.server = sinon.fakeServer.create();
        },

        teardown: function () {
            this.server.restore();
            this.$el.empty();
            this.autocomplete.disconnectObserver();

            AJS.$("#autocomplete-suggestions").remove();
        }
    });

    test("Should make a request to get filter information", function () {
        this.initPicker();
        this.autocomplete.select.$field.val("a");
        this.autocomplete.select.onEdit();

        equal(this.server.requests.length, 1, "Should have an autocomplete request");
    });

    test("Selecting a value works", function () {
        this.initPicker();
        var selection = this.autocomplete.getValue();
        ok(selection === null, "No value selected initially");

        this.autocomplete.select.setSelection(new AJS.ItemDescriptor({
            value: 10234,
            label: "Sample Filter"
        }));

        deepEqual(this.autocomplete.getValue(), { "id": 10234, "label": "Sample Filter" }, "Value should now be selected");
    });

    test("Should populate suggestion box with content", function () {
        this.initPicker();
        this.autocomplete.select.$field.val("a");
        this.autocomplete.select.onEdit();
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(sampleResponse));

        var suggestions = AJS.$("#autocomplete-suggestions .aui-list-item");

        equal(suggestions.length, 2, "Should have 2 matching suggestions");
        equal("\"><IMG SRC=\"javascript: alert('XSS')\"", AJS.$(suggestions[0]).text());
        equal("5.0: Issues Pushed forward to iteration 11 AND Escalated in Priority", AJS.$(suggestions[1]).text());
    });

    test("Filter picker can handle error when fetchign suggestions", function () {
        this.initPicker();

        var alertStub = this.stub(window, "alert", function(msg) { return false; } );

        this.autocomplete.select.$field.val("a");
        this.autocomplete.select.onEdit();
        this.server.requests[0].respond(500, { "Content-Type": "application/json" }, "{}");

        equal(1, alertStub.callCount, "Should have invoked alert one time");
        equal("common.forms.ajax.servererror",alertStub.getCall(0).args[0], "Should have displayed an alert" );
    });

    test("Should show only matching suggestion box with content", function () {
        this.initPicker();
        this.autocomplete.select.$field.val("alert");
        this.autocomplete.select.onEdit();
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(sampleResponse));

        var suggestions = AJS.$("#autocomplete-suggestions .aui-list-item");

        equal(suggestions.length, 1, "Should have 1 matching suggestions");
        equal("\"><IMG SRC=\"javascript: alert('XSS')\"", AJS.$(suggestions[0]).text());
    });

    test("Should show no results message if no response", function () {
        this.initPicker();
        this.autocomplete.select.$field.val("alert");
        this.autocomplete.select.onEdit();
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(emptyResponse));

        var suggestions = AJS.$("#autocomplete-suggestions .aui-list-item");

        equal(suggestions.length, 0, "Should have no suggestions");
        equal(AJS.$("#autocomplete-suggestions .no-suggestions").length, 1, "Should show an empty results message");
    });

    test("Should not have selection if we pass in no filter id", function () {
        this.initPicker();
        var noSelection = this.autocomplete.select.getSelectedDescriptor();
        ok(noSelection === undefined, "By default nothing is selected");

    });

    test("Should have selection if we pass in filter id", function () {
        this.initPicker(10347);
        this.server.requests[0].respond(200, { "Content-Type": "application/json" }, JSON.stringify(sampleFilterResponse));

        var selection = this.autocomplete.getValue();
        deepEqual({id:10347, label:"@everybody #will# get acce$$ to thi$ fi*lte`_everyone"}, selection, "We have a selected descriptor");
    });

    test("If a filters been deleted, should still be able to render the config form and enter a new filter name", function () {
        equal("", this.errorContainer.text(), "No error is shown yet.");
        this.initPicker(10347);
        this.server.requests[0].respond(400, { "Content-Type": "application/json" }, "{}");

        ok(this.autocomplete.select.$field.attr("disabled") === undefined, "Should not be disabled");

        this.autocomplete.select.$field.val("a");
        this.autocomplete.select.onEdit();
        this.server.requests[1].respond(200, { "Content-Type": "application/json" }, JSON.stringify(sampleResponse));

        var suggestions = AJS.$("#autocomplete-suggestions .aui-list-item");

        equal(suggestions.length, 2, "Should have 2 matching suggestions");
        equal("\"><IMG SRC=\"javascript: alert('XSS')\"", AJS.$(suggestions[0]).text());
        equal("5.0: Issues Pushed forward to iteration 11 AND Escalated in Priority", AJS.$(suggestions[1]).text());
    });

    test("Update from advanced filter sets select value", function () {
        this.initPicker();

        this.advancedSearchResultName.text("Sample Filter");
        this.advancedSearchResultId[0].setAttribute("value", "filter-10234");

        deepEqual(this.autocomplete.getValue(), { "id": "10234", "label": "Sample Filter" }, "Value should now be selected");
    });

    test("Two triggers still result in filter being selected", function () {
        this.initPicker();

        this.advancedSearchResultName.text("Sample Filter");
        this.advancedSearchResultId[0].setAttribute("value", "filter-10234");
        this.advancedSearchResultId[0].setAttribute("value", "filter-10234");

        deepEqual(this.autocomplete.getValue(), { "id": "10234", "label": "Sample Filter" }, "Value should now be selected");
    });
});


