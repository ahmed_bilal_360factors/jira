(function() {
    AJS.namespace("DashboardItem.Mocks");

    /**
     * Defines the same interface as AG.InlineGadgetAPI.js in atlassian-gadgets.
     */
    DashboardItem.Mocks.API = _.extend({}, Backbone.Events, {
        isEditable: function() {},
        savePreferences: function () {},
        setTitle: function () {},
        initRefresh: function () {},
        showLoadingBar: function () {},
        hideLoadingBar: function () {},
        closeEdit: function () {},
        forceLayoutRefresh: function () {},
        getGadgetId: function () {},
        getRefreshFieldValue: function () {}
    });
})();