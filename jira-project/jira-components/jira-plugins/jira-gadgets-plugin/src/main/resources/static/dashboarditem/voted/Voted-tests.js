AJS.test.require("com.atlassian.jira.gadgets:common-test-resources");
AJS.test.require("com.atlassian.jira.gadgets:voted-dashboard-item-resources", function () {

    'use strict';

    module('jira-dashboard-items/voted', {
        setup: function () {
            this.mockedContext = AJS.test.context();

            this.API = AJS.$.extend({}, DashboardItem.Mocks.API);
            this.APIStub = sinon.stub(this.API);

            this.$el = AJS.$("<div/>");

            AJS.$("#qunit-fixture").append(this.$el);
        },

        renderResults: function(prefs, expectedPrefs) {
            var deferred = AJS.$.Deferred();
            this.mockedContext.mock('jira-dashboard-items/components/search-results', function (opts) {
                deepEqual(opts.preferences, expectedPrefs, "search result preferences match");

                return {
                    render: function () {
                        return deferred;
                    },
                    on: function(event, cb) {}
                };
            });

            var VotedDashboardItem = this.mockedContext.require('jira-dashboard-items/voted');
            var dashboardItem = new VotedDashboardItem(this.API);
            dashboardItem.render(this.$el, prefs);
        }
    });


    test("Render with show totals adds the 'votes' column", function () {
        var votedItemInputPrefs = {columnNames:"summary|description", showTotalVotes:true}
        var expectedSearchResultPref = {
            "columnNames": "summary|description|votes",
            "jql": "issue in votedIssues() AND resolution = EMPTY order by votes desc",
            "showTotalVotes": true
        };

        this.renderResults(votedItemInputPrefs, expectedSearchResultPref);
    });

    test("Render without totals doesn't add the 'votes' column", function () {
        var votedItemInputPrefs = {columnNames:"summary|description", showTotalVotes:false}
        var expectedSearchResultPref = {
            "columnNames": "summary|description",
            "jql": "issue in votedIssues() AND resolution = EMPTY order by votes desc",
            "showTotalVotes": false
        };

        this.renderResults(votedItemInputPrefs, expectedSearchResultPref);
    });

    test("Render with resolved issues modifies the JQL query", function () {
        var votedItemInputPrefs = {columnNames:"summary|description", showResolved:true}
        var expectedSearchResultPref = {
            "columnNames": "summary|description",
            "jql": "issue in votedIssues() order by votes desc",
            "showResolved": true
        };

        this.renderResults(votedItemInputPrefs, expectedSearchResultPref);
    });

    test("Render without resolved issues modifies the JQL query", function () {
        var votedItemInputPrefs = {columnNames:"summary|description", showResolved:false}
        var expectedSearchResultPref = {
            "columnNames": "summary|description",
            "jql": "issue in votedIssues() AND resolution = EMPTY order by votes desc",
            "showResolved": false
        };

        this.renderResults(votedItemInputPrefs, expectedSearchResultPref);
    });

    test("Render with default preferences", function () {
        var votedItemInputPrefs = {columnNames:"summary|description"}
        var expectedSearchResultPref = {
            "columnNames": "summary|description",
            "jql": "issue in votedIssues() AND resolution = EMPTY order by votes desc"
        };

        this.renderResults(votedItemInputPrefs, expectedSearchResultPref);
    });
});


