package com.atlassian.jira.gadgets.system.crossselling;

import java.util.Properties;

import javax.inject.Inject;

import com.atlassian.extras.api.ProductLicense;
import com.atlassian.extras.common.util.LicenseProperties;
import com.atlassian.extras.core.plugins.DefaultPluginLicense;
import com.atlassian.extras.decoder.v2.Version2LicenseDecoder;
import com.atlassian.fugue.Option;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import com.google.common.base.Function;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

@Component
public class ProductLicenseFactory
{
    public static final String LICENSE_PROPERTY_PREFIX = "com.atlassian.upm.license.internal.impl.PluginSettingsPluginLicenseRepository:licenses:";
    static final Integer MAX_KEY_LENGTH = 100;

    private final PluginSettingsFactory pluginSettingsFactory;
    private final PluginAccessor pluginAccessor;

    @Inject
    public ProductLicenseFactory(@ComponentImport final PluginSettingsFactory pluginSettingsFactory,
            @ComponentImport final PluginAccessor pluginAccessor)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.pluginAccessor = pluginAccessor;
    }

    public Option<ProductLicense> getPluginLicense(final String pluginNamespace, final String pluginKey)
    {
        if (null == pluginAccessor.getEnabledPlugin(pluginKey))
        {
            return Option.none();
        }

        final Option<String> licenseString = Option.option(getPluginLicenseString(pluginKey));

        Option<ProductLicense> license = licenseString.map(new Function<String, ProductLicense>()
        {
            @Override
            public ProductLicense apply(final String input)
            {
                final Properties licenseDescription = new Version2LicenseDecoder().doDecode(input);
                final DefaultPluginLicense pluginLicense = new DefaultPluginLicense(null, new ProductLicenseProperties(pluginNamespace, licenseDescription));
                return pluginLicense;
            }
        });

        return license;
    }

    private String getPluginLicenseString(final String pluginKey)
    {
        final PluginSettings settingsForKey = pluginSettingsFactory.createGlobalSettings();

        return (String) settingsForKey.get(hashKeyIfTooLong(LICENSE_PROPERTY_PREFIX + pluginKey));
    }

    public static String hashKeyIfTooLong(final String key)
    {
        if (key.length() > MAX_KEY_LENGTH)
        {
            final String keyHash = DigestUtils.md5Hex(key);
            final String keptOriginalKey = key.substring(0, MAX_KEY_LENGTH - keyHash.length());
            final String hashedKey = keptOriginalKey + keyHash;
            return hashedKey;
        }
        return key;
    }


    private
    static class ProductLicenseProperties
            implements LicenseProperties
    {
        private final String namespace;
        private final Properties properties;

        public ProductLicenseProperties(String namespace, Properties properties)
        {
            this.namespace = namespace;
            this.properties = properties;
        }

        public String getProperty(String propertyName)
        {
            return getProperty(propertyName, null);
        }

        public String getProperty(String propertyName, String defaultValue)
        {
            final String valueOfNSProperty = this.properties.getProperty(namespacedKey(propertyName));
            return valueOfNSProperty != null ? valueOfNSProperty : this.properties.getProperty(propertyName, defaultValue);
        }

        public int getInt(String propertyName, int defaultValue)
        {
            String stringValue = getProperty(propertyName);
            if ((stringValue == null) || (stringValue.length() == 0))
            {
                return defaultValue;
            }
            try
            {
                return Integer.parseInt(stringValue);
            }
            catch (NumberFormatException e) {}
            return defaultValue;
        }

        private String namespacedKey(final String propertyName)
        {
            return namespace + "." + propertyName;
        }
    }
}