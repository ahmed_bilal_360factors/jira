package com.atlassian.jira.dashboarditem.admin;

import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.gadgets.system.AdminTaskManager;
import com.atlassian.jira.gadgets.system.crossselling.CrossSellingLicenseCheck;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.system.SystemInfoUtils;
import com.atlassian.jira.util.system.check.SystemEnvironmentChecklist;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.util.ExternalLinkUtil;
import com.atlassian.jira.web.util.ExternalLinkUtilImpl;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.ContextProvider;

import static com.atlassian.jira.config.properties.APKeys.JIRA_OPTION_USER_EXTERNALMGT;
import static com.atlassian.jira.util.collect.CollectionUtil.first;
import static com.google.common.collect.Maps.newHashMap;
import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.substringAfter;

@Scanned
public class AdminContextProvider implements ContextProvider
{
    public static final String ADD_PRODUCT_URL = "https://my.atlassian.com/ondemand/configure/%s?modifyAction=activate&productKey=%s";

    private final JiraAuthenticationContext authenticationContext;
    private final ApplicationProperties applicationProperties;
    private final SystemInfoUtils systemInfoUtils;
    private final GlobalPermissionManager globalPermissionManager;
    private final UserUtil userUtil;
    private final JiraLicenseService jiraLicenseService;
    private final AdminTaskManager adminTaskManager;
    private final VelocityRequestContextFactory velocityRequestContextFactory;
    private final HelpUrls helpUrls;
    private final CrossSellingLicenseCheck crossSellingLicenseCheck;

    public AdminContextProvider(
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final ApplicationProperties applicationProperties,
            @ComponentImport final SystemInfoUtils systemInfoUtils,
            @ComponentImport final GlobalPermissionManager globalPermissionManager,
            @ComponentImport final UserUtil userUtil,
            @ComponentImport final JiraLicenseService jiraLicenseService,
            final AdminTaskManager adminTaskManager,
            @ComponentImport final VelocityRequestContextFactory velocityRequestContextFactory,
            @ComponentImport final HelpUrls helpUrls,
            final CrossSellingLicenseCheck crossSellingLicenseCheck )
    {
        this.authenticationContext = authenticationContext;
        this.applicationProperties = applicationProperties;
        this.systemInfoUtils = systemInfoUtils;
        this.globalPermissionManager = globalPermissionManager;
        this.userUtil = userUtil;
        this.jiraLicenseService = jiraLicenseService;
        this.adminTaskManager = adminTaskManager;
        this.velocityRequestContextFactory = velocityRequestContextFactory;
        this.helpUrls = helpUrls;
        this.crossSellingLicenseCheck = crossSellingLicenseCheck;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException
    { }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context)
    {
        Map<String, Object> newContext = newHashMap();
        newContext.putAll(context);
        newContext.put("baseUrl", velocityRequestContextFactory.getJiraVelocityRequestContext().getBaseUrl());
       newContext.put("admin", adminProperties());
        return newContext;
    }

    private Map<String, Object> adminProperties()
    {
        Map<String, Object> properties = newHashMap();
        LicenseDetails licenseDetails = jiraLicenseService.getLicense();
        ExternalLinkUtil externalLinkUtil = ExternalLinkUtilImpl.getInstance();

        properties.put("warningMessages", SystemEnvironmentChecklist.getWarningMessages(authenticationContext.getLocale(), true));
        properties.put("notExternalUserManagement", !applicationProperties.getOption(JIRA_OPTION_USER_EXTERNALMGT));
        if(licenseDetails.getMaximumNumberOfUsers() == 0)
        {
            properties.put("hasZeroUserLicense", true);
        }
        properties.put("hasExceededUserLimit", userUtil.hasExceededUserLimit());
        properties.put("hasReachedUserLimit", !userUtil.canActivateNumberOfUsers(1));
        properties.put("isEvaluation", licenseDetails.isEvaluation());
        properties.put("systemAdministrator", isSystemAdministrator());
        properties.put("licenseStatusMessage", licenseDetails.getLicenseStatusMessage(authenticationContext.getLoggedInUser(), "<br/><br/>"));
        properties.put("licenseTypeNiceName", licenseDetails.getDescription());
        properties.put("partnerName", licenseDetails.getPartnerName());
        properties.put("usingHsql", systemInfoUtils.getDatabaseType().equalsIgnoreCase("hsql"));
        properties.put("licenseExpiryStatusMessageHtml",
                licenseDetails.getLicenseExpiryStatusMessage(authenticationContext.getLoggedInUser()));
        properties.put("externalLinkMyAccount", externalLinkUtil.getProperty("external.link.atlassian.my.account"));
        properties.put("externalLinkPersonalSite", externalLinkUtil.getProperty("external.link.jira.personal.site"));
        properties.put("tasks", adminTaskManager.getAdminTaskLists(authenticationContext.getLoggedInUser()));
        properties.put("dbConfigDocsUrl", getHelpUrl("dbconfig.generic"));
        properties.put("browseDocsUrl", getHelpUrl("gadget.admin.documentation.browse"));
        properties.put("defineWorkflowsDocsUrl", getHelpUrl("gadget.admin.documentation.workflows"));
        properties.put("customizeFieldsDocsUrl", getHelpUrl("gadget.admin.documentation.fields"));
        properties.put("customizeScreensDocsUrl", getHelpUrl("gadget.admin.documentation.screens"));
        properties.put("manageUsersDocsUrl", getHelpUrl("gadget.admin.documentation.manage.users"));
        properties.put("timeTrackingDocsUrl", getHelpUrl("gadget.admin.documentation.time.tracking"));
        properties.put("migrationDocsUrl", getHelpUrl("gadget.admin.documentation.migration"));
        properties.put("addBonfireToODUrl", String.format(ADD_PRODUCT_URL, getSen(licenseDetails), "bonfire.jira.ondemand"));
        properties.put("addGreenhopperToODUrl", String.format(ADD_PRODUCT_URL, getSen(licenseDetails), "greenhopper.jira.ondemand"));
        properties.put("crossSellingLicenses", crossSellingLicenseCheck );
        properties.put("lcenseHolder", userIsLicenseHolder(
                authenticationContext.getUser(),
                licenseDetails));

        return properties;
    }

    private boolean isSystemAdministrator()
    {
        User user = authenticationContext.getLoggedInUser();
        return ((user != null) && globalPermissionManager.hasPermission(Permissions.SYSTEM_ADMIN, user));
    }

    private String getHelpUrl(String helpKey) {
        return helpUrls.getUrl(helpKey).getUrl();
    }

    private String getSen(LicenseDetails licenseDetails)
    {
        return substringAfter(licenseDetails.getSupportEntitlementNumber(), "SEN-");
    }

    private boolean userIsLicenseHolder(
            ApplicationUser user,
            LicenseDetails license)
    {
        if (license == null || license.getContacts().size() < 1)
        {
            return false;
        }
        String licenseeEmail = first(license.getContacts()).getEmail();
        if (isBlank(licenseeEmail))
        {
            return false;
        }
        // The suggested simplification is too complicated!
        //noinspection SimplifiableIfStatement
        if (user == null || isBlank(user.getEmailAddress()))
        {
            return false;
        }
        return user.getEmailAddress().equals(licenseeEmail);
    }

}
