package com.atlassian.jira.dashboarditem.statistics.searcher.beans;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Represents a row in the statistic search results.
 */
@XmlRootElement
public class StatisticsSearchResultRowBean
{
    @XmlElement
    private final String key;

    @XmlElement
    private final int percentage;

    @XmlElement
    private final String url;

    @XmlElement
    private final Long value;

    public StatisticsSearchResultRowBean(@Nonnull final String key, @Nonnull final int percentage, @Nullable final String url, @Nonnull final Long value)
    {
        this.key = key;
        this.percentage = percentage;
        this.url = url;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public int getPercentage() {
        return percentage;
    }

    public String getUrl() {
        return url;
    }

    public Long getValue() {
        return value;
    }
}
