package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.statistics.CustomFieldStattable;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.statistics.AssigneeStatisticsMapper;
import com.atlassian.jira.issue.statistics.ComponentStatisticsMapper;
import com.atlassian.jira.issue.statistics.CreatorStatisticsMapper;
import com.atlassian.jira.issue.statistics.FixForVersionStatisticsMapper;
import com.atlassian.jira.issue.statistics.IssueTypeStatisticsMapper;
import com.atlassian.jira.issue.statistics.LabelsStatisticsMapper;
import com.atlassian.jira.issue.statistics.PriorityStatisticsMapper;
import com.atlassian.jira.issue.statistics.ProjectStatisticsMapper;
import com.atlassian.jira.issue.statistics.RaisedInVersionStatisticsMapper;
import com.atlassian.jira.issue.statistics.ReporterStatisticsMapper;
import com.atlassian.jira.issue.statistics.ResolutionStatisticsMapper;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.issue.statistics.StatusStatisticsMapper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Provides available values for stat types used by various gadgets
 */
@Component
public class StatisticTypesProvider
{
    public static final String COMPONENTS = getString("components");
    public static final String FIXFOR = getString("fixfor");
    public static final String ALLFIXFOR = getString("allFixfor");
    public static final String VERSION = getString("version");
    public static final String ALLVERSION = getString("allVersion");
    public static final String ASSIGNEES = getString("assignees");
    public static final String ISSUETYPE = getString("issuetype");
    public static final String PRIORITIES = getString("priorities");
    public static final String PROJECT = getString("project");
    public static final String REPORTER = getString("reporter");
    public static final String CREATOR = getString("creator");
    public static final String RESOLUTION = getString("resolution");
    public static final String STATUSES = getString("statuses");
    public static final String LABELS = getString("labels");

    private static final Map<String, String> systemValues = new LinkedHashMap<String, String>();

    static
    {
        systemValues.put(ASSIGNEES, "gadget.filterstats.field.statistictype.assignees");
        systemValues.put(COMPONENTS, "gadget.filterstats.field.statistictype.components");
        systemValues.put(ISSUETYPE, "gadget.filterstats.field.statistictype.issuetype");
        systemValues.put(FIXFOR, "gadget.filterstats.field.statistictype.fixfor");
        systemValues.put(ALLFIXFOR, "gadget.filterstats.field.statistictype.allfixfor");
        systemValues.put(PRIORITIES, "gadget.filterstats.field.statistictype.priorities");
        systemValues.put(PROJECT, "gadget.filterstats.field.statistictype.project");
        systemValues.put(VERSION, "gadget.filterstats.field.statistictype.version");
        systemValues.put(ALLVERSION, "gadget.filterstats.field.statistictype.allversion");
        systemValues.put(REPORTER, "gadget.filterstats.field.statistictype.reporter");
        systemValues.put(CREATOR, "gadget.filterstats.field.statistictype.creator");
        systemValues.put(RESOLUTION, "gadget.filterstats.field.statistictype.resolution");
        systemValues.put(STATUSES, "gadget.filterstats.field.statistictype.statuses");
        systemValues.put(LABELS, "gadget.filterstats.field.statistictype.labels");
    }

    private final JiraAuthenticationContext authenticationContext;
    private final CustomFieldManager customFieldManager;

    @Autowired
    public StatisticTypesProvider(
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final CustomFieldManager customFieldManager)
    {
        this.authenticationContext = authenticationContext;
        this.customFieldManager = customFieldManager;
    }

    /**
     * Get the {@link com.atlassian.jira.issue.statistics.StatisticsMapper} associated with passed key
     *
     * @param statsMapperKey The key for the statsmapper.  Usually a customfield id, or one of the constants in this
     * class
     * @return The StatisticsMapper associated with the passed in key.
     */
    public StatisticsMapper getStatsMapper(String statsMapperKey)
    {
        final StatisticsMapper systemMapper = getSystemMapper(statsMapperKey);
        if (systemMapper != null)
        {
            return systemMapper;
        }

        final CustomField customField = customFieldManager.getCustomFieldObject(statsMapperKey);
        if (customField == null)
        {
            throw new RuntimeException("No custom field with id " + statsMapperKey);
        }
        if (customField.getCustomFieldSearcher() instanceof CustomFieldStattable)
        {
            final CustomFieldStattable customFieldStattable = (CustomFieldStattable) customField.getCustomFieldSearcher();
            return customFieldStattable.getStatisticsMapper(customField);
        }
        else
        {
            return null;
        }
    }

    /**
     * @return returns a map of all system stat mappers
     */
    public Map<String, String> getSystemStatisticTypes()
    {
        return Collections.unmodifiableMap(systemValues);
    }

    /**
     * Returns the display name for a field
     *
     * @param field The field to get the displayable name for
     * @return A human consumable name for the field
     */
    public String getDisplayName(String field)
    {
        final I18nHelper i18n = authenticationContext.getI18nHelper();

        String fieldName = "";
        final Map<String, String> systemValues = getSystemStatisticTypes();
        if (systemValues.containsKey(field))
        {
            fieldName = i18n.getText(systemValues.get(field));
        }
        else
        {
            final CustomField customField = customFieldManager.getCustomFieldObject(field);
            if (customField != null)
            {
                fieldName = customField.getName();
            }
        }
        return fieldName;
    }

    private StatisticsMapper getSystemMapper(String statsMapperKey)
    {
        if (COMPONENTS.equals(statsMapperKey))
        {
            return new ComponentStatisticsMapper();
        }
        else if (ASSIGNEES.equals(statsMapperKey))
        {
            return new AssigneeStatisticsMapper(ComponentAccessor.getUserManager(), ComponentAccessor.getJiraAuthenticationContext());
        }
        else if (ISSUETYPE.equals(statsMapperKey))
        {
            return new IssueTypeStatisticsMapper(ComponentAccessor.getConstantsManager());
        }
        else if (FIXFOR.equals(statsMapperKey))
        {
            return new FixForVersionStatisticsMapper(ComponentAccessor.getVersionManager(), false);
        }
        else if (ALLFIXFOR.equals(statsMapperKey))
        {
            return new FixForVersionStatisticsMapper(ComponentAccessor.getVersionManager(), true);
        }
        else if (PRIORITIES.equals(statsMapperKey))
        {
            return new PriorityStatisticsMapper(ComponentAccessor.getConstantsManager());
        }
        else if (PROJECT.equals(statsMapperKey))
        {
            return new ProjectStatisticsMapper(ComponentAccessor.getProjectManager());
        }
        else if (VERSION.equals(statsMapperKey))
        {
            return new RaisedInVersionStatisticsMapper(ComponentAccessor.getVersionManager(), false);
        }
        else if (ALLVERSION.equals(statsMapperKey))
        {
            return new RaisedInVersionStatisticsMapper(ComponentAccessor.getVersionManager(), true);
        }
        else if (REPORTER.equals(statsMapperKey))
        {
            return new ReporterStatisticsMapper(ComponentAccessor.getUserManager(), ComponentAccessor.getJiraAuthenticationContext());
        }
        else if (CREATOR.equals(statsMapperKey))
        {
            return new CreatorStatisticsMapper(ComponentAccessor.getUserManager(), ComponentAccessor.getJiraAuthenticationContext());
        }
        else if (RESOLUTION.equals(statsMapperKey))
        {
            return new ResolutionStatisticsMapper(ComponentAccessor.getConstantsManager());
        }
        else if (STATUSES.equals(statsMapperKey))
        {
            return new StatusStatisticsMapper(ComponentAccessor.getConstantsManager());
        }
        else if (LABELS.equals(statsMapperKey))
        {
            return new LabelsStatisticsMapper(false);
        }


        return null; // custom field maybe?
    }

    private static String getString(final String str)
    {
        return str;
    }
}
