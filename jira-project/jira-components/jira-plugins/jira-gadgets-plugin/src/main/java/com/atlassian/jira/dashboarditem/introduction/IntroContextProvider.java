package com.atlassian.jira.dashboarditem.introduction;

import com.atlassian.jira.admin.IntroductionProperty;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

@Scanned
public class IntroContextProvider implements ContextProvider
{
    private final JiraAuthenticationContext authenticationContext;
    private final PermissionManager permissionManager;
    private final IntroductionProperty introductionProperty;
    private final ApplicationProperties applicationProperties;
    private final HelpUrls helpUrls;

    public IntroContextProvider(
            @ComponentImport JiraAuthenticationContext authenticationContext,
            @ComponentImport PermissionManager permissionManager,
            @ComponentImport IntroductionProperty introductionProperty,
            @ComponentImport ApplicationProperties applicationProperties,
            @ComponentImport HelpUrls helpUrls)
    {
        this.authenticationContext = authenticationContext;
        this.permissionManager = permissionManager;
        this.introductionProperty = introductionProperty;
        this.applicationProperties = applicationProperties;
        this.helpUrls = helpUrls;
    }

    @Override
    public void init(final Map<String, String> params) throws PluginParseException { }

    @Override
    public Map<String, Object> getContextMap(final Map<String, Object> context)
    {
        final Map<String, Object> newContext = Maps.newHashMap(context);
        final String html = introductionProperty.getViewHtml();

        if (StringUtils.isBlank(html))
        {
            newContext.put("isAdmin", permissionManager.hasPermission(Permissions.ADMINISTER, authenticationContext.getUser()));
            newContext.put("jira101HelpPath", helpUrls.getUrl("jira101"));
            newContext.put("introHelpPath", helpUrls.getUrl("introduction"));
            newContext.put("instanceTitle", applicationProperties.getDefaultBackedString(APKeys.JIRA_TITLE));
        }
        else
        {
            newContext.put("introHtml", html);
        }
        return newContext;
    }
}
