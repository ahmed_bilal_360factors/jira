package com.atlassian.jira.gadgets.system.crossselling;

import java.util.Collections;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import com.atlassian.extras.api.ProductLicense;
import com.atlassian.fugue.Option;
import com.atlassian.fugue.Suppliers;
import com.atlassian.jira.gadgets.system.util.DefaultGreenhopperLicenseChecker;
import com.atlassian.ozymandias.SafePluginPointAccess;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.springframework.stereotype.Component;

/**
 * @since v6.4
 */
@Component
public class CrossSellingLicenseCheck
{
    public static final String SERVICEDESK_PLUGIN_ID = "com.atlassian.servicedesk";
    public static final String UPM_MANAGE_PLUGIN = "plugins/servlet/upm#manage/";
    public static final String GREENHOPPER_NAMESPACE = "greenhopper";
    private final LicenseExpirationMessageFormatter formatter;
    private final ProductLicenseFactory productLicenseFactory;

    @Inject
    public CrossSellingLicenseCheck(
            final LicenseExpirationMessageFormatter formatter,
            final ProductLicenseFactory productLicenseFactory)
    {
        this.formatter = formatter;
        this.productLicenseFactory = productLicenseFactory;
    }

    public Iterable<LicenseDescriptionDetails> getLicenses()
    {
        // load current licenses
        final Option<Iterable<LicenseDescriptionDetails>> licensesSafeOption = SafePluginPointAccess.call(new Callable<Iterable<LicenseDescriptionDetails>>()
        {
            @Override
            public Iterable<LicenseDescriptionDetails> call() throws Exception
            {
                final ImmutableList<LicenseDescriptionDetails> crossSellingPlugins = ImmutableList.of(
                        new LicenseDescriptionDetails(getGreenHoperLicense(), UPM_MANAGE_PLUGIN + DefaultGreenhopperLicenseChecker.JIRA_AGILE_PLUGIN_ID),
                        new LicenseDescriptionDetails(getServiceDeskLicense(), UPM_MANAGE_PLUGIN + SERVICEDESK_PLUGIN_ID));

                return Iterables.filter(crossSellingPlugins, new Predicate<LicenseDescriptionDetails>()
                {
                    @Override
                    public boolean apply(final LicenseDescriptionDetails licenseDescription)
                    {
                        return licenseDescription.getShowLicenseMessage();
                    }
                });
            }
        });

        return licensesSafeOption.getOrElse(Collections.<LicenseDescriptionDetails>emptyList());
    }

    private Option<ProductLicense> getGreenHoperLicense()
    {
        final String pluginNamespace = GREENHOPPER_NAMESPACE;
        final String pluginKey = DefaultGreenhopperLicenseChecker.JIRA_AGILE_PLUGIN_ID;

        return productLicenseFactory.getPluginLicense(pluginNamespace, pluginKey);
    }

    private Option<ProductLicense> getServiceDeskLicense()
    {
        final String pluginKey = SERVICEDESK_PLUGIN_ID;

        return productLicenseFactory.getPluginLicense(pluginKey, pluginKey);
    }

    public class LicenseDescriptionDetails
    {
        private final Option<ProductLicense> pluginLicense;

        private String pluginRelativePath;

        private LicenseDescriptionDetails(final Option<ProductLicense> pluginLicense, final String pluginRelativePath)
        {
            this.pluginLicense = pluginLicense;
            this.pluginRelativePath = pluginRelativePath;
        }

        public String getPluginRelativePath()
        {
            return pluginRelativePath;
        }

        public boolean getShowLicenseMessage()
        {
            return pluginLicense.isDefined();
        }

        public String getLicenseDescription()
        {
            return pluginLicense.fold(
                    Suppliers.<String>alwaysNull(),
                    SafePluginPointAccess.safe(new Function<ProductLicense, String>()
                    {
                        @Override
                        public String apply(final ProductLicense input)
                        {
                            return input.getDescription();
                        }
                    }));
        }

        public String getExpiryDescription()
        {
            return pluginLicense.fold(
                    Suppliers.<String>alwaysNull(),
                    SafePluginPointAccess.safe(new Function<ProductLicense, String>()
                    {
                        @Override
                        public String apply(final ProductLicense input)
                        {
                            return formatter.getLicenseExpiryStatusMessage(input);
                        }
                    }));
        }
    }

}
