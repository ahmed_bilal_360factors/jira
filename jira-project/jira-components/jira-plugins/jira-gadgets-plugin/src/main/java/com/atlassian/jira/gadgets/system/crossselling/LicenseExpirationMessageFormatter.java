package com.atlassian.jira.gadgets.system.crossselling;

import java.util.Date;

import javax.inject.Inject;

import com.atlassian.core.util.Clock;
import com.atlassian.core.util.DateUtils;
import com.atlassian.extras.api.ProductLicense;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import org.springframework.stereotype.Component;

/**
 * @since v6.4
 */
@Component
public class LicenseExpirationMessageFormatter
{
    private final DateTimeFormatter dateTimeFormatter;
    private final Clock clock;
    private final I18nHelper.BeanFactory i18nFactory;
    private final JiraAuthenticationContext authenticationContext;

    @Inject
    public LicenseExpirationMessageFormatter(@ComponentImport final DateTimeFormatter dateTimeFormatter, @ComponentImport final Clock clock, @ComponentImport final I18nHelper.BeanFactory i18nFactory, final JiraAuthenticationContext authenticationContext)
    {
        this.dateTimeFormatter = dateTimeFormatter;
        this.clock = clock;
        this.i18nFactory = i18nFactory;
        this.authenticationContext = authenticationContext;
    }

    public String getLicenseExpiryStatusMessage(ProductLicense license)
    {
        return getLicenseExpiryStatusMessage(i18nFactory.getInstance(authenticationContext.getUser()), license);
    }

    public String getLicenseExpiryStatusMessage(final I18nHelper i18n, ProductLicense license)
    {
        final Date expiryDate = license.getExpiryDate();
        if (expiryDate == null)
        {
            return "";
        }

        final String msg;
        if (license.isExpired())
        {
            msg = i18n.getText("admin.license.expired");
        }
        else
        {
            final DateTimeFormatter dmyFormatter = dateTimeFormatter.withLocale(i18n.getLocale()).withStyle(DateTimeStyle.DATE);
            msg = i18n.getText("admin.license.expiresin", getTimeUntilExpiry(i18n, expiryDate), dmyFormatter.format(expiryDate));
        }

        return "(" + msg + ")";
    }

    private String getTimeUntilExpiry(final I18nHelper i18n, Date expiryDate)
    {
        return DateUtils.dateDifference(getCurrentTime(), expiryDate.getTime(), 2, i18n.getDefaultResourceBundle());
    }


    final long getCurrentTime()
    {
        return clock.getCurrentDate().getTime();
    }
}
