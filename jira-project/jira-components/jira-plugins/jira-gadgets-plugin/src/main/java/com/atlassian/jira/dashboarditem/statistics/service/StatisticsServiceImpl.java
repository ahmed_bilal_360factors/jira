package com.atlassian.jira.dashboarditem.statistics.service;

import javax.annotation.Nonnull;

import com.atlassian.fugue.Either;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.dashboarditem.statistics.searcher.StatisticsSearcher;
import com.atlassian.jira.dashboarditem.statistics.searcher.beans.StatisticsSearchResultBean;
import com.atlassian.jira.gadgets.system.StatisticTypesProvider;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.constants.SavedFilterSearchConstants;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.issue.search.searchers.impl.NamedTerminalClauseCollectingVisitor;
import com.atlassian.jira.issue.search.searchers.util.TerminalClauseCollectingVisitor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.JiraUrlCodec;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.query.Query;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.SingleValueOperand;

import com.google.common.annotations.VisibleForTesting;
import com.sun.istack.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @since v6.4
 */
@Component
public class StatisticsServiceImpl implements StatisticsService
{
    private final SearchService searchService;
    private final StatisticsSearcher statisticsSearcher;
    private final StatisticTypesProvider statisticTypesProvider;
    private final ProjectService projectService;
    private final SearchRequestService searchRequestService;
    private final VelocityRequestContextFactory velocityRequestContextFactory;
    private final JiraAuthenticationContext authenticationContext;

    public static final String PROJECT_CLAUSE_NAME = SystemSearchConstants.forProject().getJqlClauseNames().getPrimaryName();
    public static final String FILTER_CLAUSE_NAME = SavedFilterSearchConstants.getInstance().getJqlClauseNames().getPrimaryName();

    @Autowired
    public StatisticsServiceImpl(
            @ComponentImport final SearchService searchService,
            final StatisticsSearcher statisticsSearcher,
            final StatisticTypesProvider statisticTypesProvider,
            @ComponentImport final ProjectService projectService,
            @ComponentImport final SearchRequestService searchRequestService,
            @ComponentImport final VelocityRequestContextFactory velocityRequestContextFactory,
            @ComponentImport final JiraAuthenticationContext authenticationContext)
    {
        this.searchService = searchService;
        this.statisticsSearcher = statisticsSearcher;
        this.statisticTypesProvider = statisticTypesProvider;
        this.projectService = projectService;
        this.searchRequestService = searchRequestService;
        this.velocityRequestContextFactory = velocityRequestContextFactory;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public Either<ErrorCollection, StatisticsResult> aggregateOneDimensionalStats(final ApplicationUser user, @Nonnull final String jql, @Nonnull final String statType)
    {
        Either<ErrorCollection, Query> errorCollectionQueryEither = getQuery(user, jql);
        if (errorCollectionQueryEither.isLeft()) {
            return Either.left(errorCollectionQueryEither.left().get());
        }

        final Query jqlQuery = errorCollectionQueryEither.right().get();


        Either<ErrorCollection, String> statOrError = getStatDisplayName(statType);
        if (statOrError.isLeft())
        {
            return Either.left(statOrError.left().get());
        }

        final String statTypeDisplayName = statOrError.right().get();

        ErrorCollection errors = new SimpleErrorCollection();

        StatisticsSearchResultBean statBean;
        try
        {
            statBean = statisticsSearcher.completeOneDimensionalSearch(user, jqlQuery, statType);
        }
        catch (SearchException e)
        {
            errors.addErrorMessage(e.getMessage());
            errors.addReason(ErrorCollection.Reason.SERVER_ERROR);
            return Either.left(errors);
        }

        return Either.right(getStatisticsResult(user, statTypeDisplayName, jqlQuery, statBean));
    }


    /**
     * Get the statistic display name for a statistic.
     * @param statType name for the statistic
     *
     * @return either an error or the display name of the stat
     */
    @Nonnull
    private Either<ErrorCollection, String> getStatDisplayName(@Nonnull final String statType)
    {

        final ErrorCollection errors = new SimpleErrorCollection();
        final String statTypeDisplayName = statisticTypesProvider.getDisplayName(statType);
        if (StringUtils.isBlank(statTypeDisplayName)) {
            errors.addErrorMessage(authenticationContext.getI18nHelper().getText("gadget.common.invalid.stat.type", statType));
            errors.addReason(ErrorCollection.Reason.VALIDATION_FAILED);
            return Either.left(errors);
        }

        return Either.right(statTypeDisplayName);
    }


    /**
     * Get the query for a given jql, otherwise an error collection if error.
     *
     * @param user completing the search
     * @param jql for the query
     *
     * @return either a query or errors.
     */
    @Nonnull
    private Either<ErrorCollection, Query> getQuery(@Nonnull final ApplicationUser user, @Nonnull final String jql)
    {
        final ErrorCollection errors = new SimpleErrorCollection();
        final SearchService.ParseResult parseResult = searchService.parseQuery(ApplicationUsers.toDirectoryUser(user), jql);
        if (!parseResult.isValid())
        {
            errors.addErrorMessages(parseResult.getErrors().getErrorMessages());
            errors.addReason(ErrorCollection.Reason.VALIDATION_FAILED);
            return Either.left(errors);
        }

        final Query jqlQuery = parseResult.getQuery();
        MessageSet messages = searchService.validateQuery(ApplicationUsers.toDirectoryUser(user), jqlQuery);
        if (messages.hasAnyErrors())
        {
            errors.addErrorMessages(messages.getErrorMessages());
            errors.addReason(ErrorCollection.Reason.VALIDATION_FAILED);
            return Either.left(errors);
        }

        return Either.right(jqlQuery);
    }

    /**
     * Generates statistics results from the search completed.
     *
     * @param user completing the search
     * @param statTypeDisplayName being grouped by
     * @param jqlQuery that was searched
     * @param statBean containing the search results
     * @return statistics for the search
     */
    private StatisticsResult getStatisticsResult(final ApplicationUser user, final String statTypeDisplayName, final Query jqlQuery, final StatisticsSearchResultBean statBean)
    {
        final String baseUrl = velocityRequestContextFactory.getJiraVelocityRequestContext().getBaseUrl();
        String filterName = authenticationContext.getI18nHelper().getText("jira.jql.query");
        String filterUrl = baseUrl + "/issues/?jql=" + urlEncode(jqlQuery.getQueryString());

        if (isFilterOnlyClause(jqlQuery))
        {
            TerminalClause filterClause = getFilterClause(jqlQuery);

            final Long filterId = ((SingleValueOperand) filterClause.getOperand()).getLongValue();
            final SearchRequest filter = searchRequestService.getFilter(new JiraServiceContextImpl(user, new SimpleErrorCollection()), filterId);
            if (filter != null)
            {
                filterName = filter.getName();
                filterUrl = baseUrl + "/issues/?filter=" + filterId;
            }
        }
        else if (isProjectOnlyClause(jqlQuery))
        {
            TerminalClause projectClause = getProjectClause(jqlQuery);
            SingleValueOperand operand = (SingleValueOperand) projectClause.getOperand();
            ProjectService.GetProjectResult projectResult;
            if (operand.getStringValue() != null)
            {
                projectResult = projectService.getProjectByKey(user, operand.getStringValue());
            }
            else
            {
                projectResult = projectService.getProjectById(user, operand.getLongValue());
            }
            if (projectResult.isValid())
            {
                filterName = projectResult.getProject().getName();
            }
        }

        return new StatisticsResult(filterName, filterUrl, statTypeDisplayName, statBean);
    }

    /**
     * Get the number of clauses in a query
     * @param query to check
     * @return number of clauses in this query
     */
    @VisibleForTesting
    int getNumberClauses(@Nonnull final Query query)
    {
        final TerminalClauseCollectingVisitor clauseVisitor = new TerminalClauseCollectingVisitor();
        query.getWhereClause().accept(clauseVisitor);
        return clauseVisitor.getClauses().size();
    }

    /**
     * Checks if there is only a project clause in the query
     * @param query to check
     * @return if only project clause
     */
    @VisibleForTesting
    boolean isProjectOnlyClause(@Nonnull final Query query)
    {
        return (getNumberClauses(query) == 1 && getProjectClause(query) != null);
    }

    /**
     * Checks if there is only a filter clause in the query
     * @param query to check
     * @return if only filter clause
     */
    @VisibleForTesting
    boolean isFilterOnlyClause(@Nonnull final Query query)
    {
        return  (getNumberClauses(query) == 1 && getFilterClause(query) != null);
    }

    /**
     * Get the filter clause
     * precondition query has only one clause
     * @param query to get clause from
     * @return filter clause in query or null
     */
    @VisibleForTesting
    @Nullable
    TerminalClause getFilterClause(@Nonnull final Query query)
    {
        return getClause(query, FILTER_CLAUSE_NAME);
    }

    /**
     * Get the project clause
     * precondition query has only one clause
     * @param query to get clause from
     * @return project clause in query or null
     */
    @VisibleForTesting
    @Nullable
    TerminalClause getProjectClause(@Nonnull final Query query)
    {
        return getClause(query, PROJECT_CLAUSE_NAME);
    }
    /**
     * Get the project clause
     * precondition query has only one clause
     * @param query to get clause from
     * @return project clause in query or null
     */
    @VisibleForTesting
    @Nullable
    TerminalClause getClause(@Nonnull final Query query, @Nonnull final String clauseName)
    {
        final NamedTerminalClauseCollectingVisitor projectVisitor = new NamedTerminalClauseCollectingVisitor(clauseName);

        query.getWhereClause().accept(projectVisitor);

        if (projectVisitor.getNamedClauses().size() != 1)
        {
            return null;
        }

        TerminalClause clause = projectVisitor.getNamedClauses().get(0);

        if (!(clause.getOperand() instanceof SingleValueOperand))
        {
            return null;
        }

        return clause;
    }


    /**
     * URL Encode a string
     * @param string to be encoded
     * @return encoded string
     */
    @VisibleForTesting
    @Nonnull
    String urlEncode(@Nonnull final String string) {
        return JiraUrlCodec.encode(string);
    }
}
