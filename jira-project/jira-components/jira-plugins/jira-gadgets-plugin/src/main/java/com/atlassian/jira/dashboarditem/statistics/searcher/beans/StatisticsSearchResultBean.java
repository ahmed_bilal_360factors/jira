package com.atlassian.jira.dashboarditem.statistics.searcher.beans;

import java.util.List;

/**
 * Represents the results for a given search.
 */
public class StatisticsSearchResultBean
{
    private final long total;
    private final List<StatisticsSearchResultRowBean> results;

    public StatisticsSearchResultBean(final long total, final List<StatisticsSearchResultRowBean> results)
    {
        this.total = total;
        this.results = results;
    }

    public long getTotal()
    {
        return total;
    }

    public List<StatisticsSearchResultRowBean> getResults()
    {
        return results;
    }
}
