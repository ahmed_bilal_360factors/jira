package com.atlassian.jira.dashboarditem.statistics.searcher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.dashboarditem.statistics.searcher.beans.StatisticsSearchResultBean;
import com.atlassian.jira.dashboarditem.statistics.searcher.beans.StatisticsSearchResultRowBean;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.search.ReaderCache;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestAppender;
import com.atlassian.jira.issue.statistics.FilterStatisticsValuesGenerator;
import com.atlassian.jira.issue.statistics.StatisticsMapper;
import com.atlassian.jira.issue.statistics.util.DefaultFieldValueToDisplayTransformer;
import com.atlassian.jira.issue.statistics.util.FieldValueToDisplayTransformer;
import com.atlassian.jira.issue.statistics.util.ObjectToFieldValueMapper;
import com.atlassian.jira.issue.statistics.util.OneDimensionalTermHitCollector;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.JiraUrlCodec;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.query.Query;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StatisticsSearcher
{
    public enum OrderBy
    {
        TOTAL, NATURAL
    }

    public enum Direction
    {
        ASC, DESC
    }

    private final ProjectManager projectManager;
    private final I18nHelper i18nHelper;
    private final CustomFieldManager customFieldManager;
    private final SearchService searchService;
    private final VelocityRequestContextFactory velocityRequestContextFactory;
    private final SearchProvider searchProvider;
    private final FieldVisibilityManager fieldVisibilityManager;
    private final ReaderCache readerCache;
    private final FieldManager fieldManager;


    @Autowired
    public StatisticsSearcher(
            @ComponentImport final ProjectManager projectManager,
            @ComponentImport final I18nHelper i18nHelper,
            @ComponentImport final CustomFieldManager customFieldManager,
            @ComponentImport final SearchService searchService,
            @ComponentImport final VelocityRequestContextFactory velocityRequestContextFactory,
            @ComponentImport final SearchProvider searchProvider,
            @ComponentImport final FieldVisibilityManager fieldVisibilityManager,
            @ComponentImport final ReaderCache readerCache,
            @ComponentImport final FieldManager fieldManager)
    {
        this.projectManager = projectManager;
        this.i18nHelper = i18nHelper;
        this.customFieldManager = customFieldManager;
        this.searchService = searchService;
        this.velocityRequestContextFactory = velocityRequestContextFactory;
        this.searchProvider = searchProvider;
        this.fieldVisibilityManager = fieldVisibilityManager;
        this.readerCache = readerCache;
        this.fieldManager = fieldManager;
    }

    public StatisticsSearchResultBean completeOneDimensionalSearch(ApplicationUser user, Query query, String statType)
            throws SearchException
    {
        return completeOneDimensionalSearch(user, query, statType, OrderBy.TOTAL, Direction.DESC);
    }

    public StatisticsSearchResultBean completeOneDimensionalSearch(ApplicationUser user, Query query, String statType, OrderBy orderBy, Direction direction)
            throws SearchException
    {
        StatisticsMapper mapper = new FilterStatisticsValuesGenerator().getStatsMapper(statType);
        OneDimensionalTermHitCollector hitCollector = new OneDimensionalTermHitCollector(mapper.getDocumentConstant(),
                fieldVisibilityManager, readerCache, fieldManager, projectManager);

        searchProvider.search(query, user, hitCollector);

        Map<Object, Integer> map = new HashMap<Object, Integer>();
        for (Map.Entry<String, Integer> entry : hitCollector.getResult().entrySet())
        {
            String fieldValue = entry.getKey();
            // Convert the indexed value into the actual object. For example, convert an indexed
            // version id into the Version object.
            Object objectValue = mapper.getValueFromLuceneField(fieldValue);

            if (mapper.isValidValue(objectValue)) //testing for valid values fixes JRA-5211
            {
                // Put the actual object and its count into the map
                // The map will sort itself if it has a comparator

                // There can be multiple indexed values that all have the same object value. Select Options are
                // one case. We index the Option ID (10000, 10001) but they might have the same value (Red, Red).
                // We need to accumulate them all together instead of just picking one at random to store.
                if (map.containsKey(objectValue))
                {
                    final Integer currentValue = map.get(objectValue);
                    map.put(objectValue, entry.getValue() + currentValue);
                }
                else
                {
                    map.put(objectValue, entry.getValue());
                }
            }
        }

        final FieldValueToDisplayTransformer<String> fieldValueToDisplayTransformer =
                new DefaultFieldValueToDisplayTransformer(i18nHelper, customFieldManager);
        SearchRequestAppender<Object> searchRequestAppender;
        if (mapper instanceof SearchRequestAppender.Factory)
        {
            searchRequestAppender = ((SearchRequestAppender.Factory) mapper).getSearchRequestAppender();
        }
        else
        {
            searchRequestAppender = new StatisticMapperWrappingSearchRequestAppender(mapper);
        }

        final Long total = hitCollector.getHitCount();

        List<StatisticsSearchResultRowBean> results = new ArrayList<StatisticsSearchResultRowBean>();
        for (Map.Entry<Object, Integer> entry : map.entrySet())
        {
            final String key = StringEscapeUtils.unescapeHtml(ObjectToFieldValueMapper.transform(statType, entry.getKey(), null, fieldValueToDisplayTransformer));
            final Long value = new Long(entry.getValue());
            final Integer percentage = (int) (100 * value / total);

            final SearchRequest urlSearchRequest = searchRequestAppender.appendInclusiveSingleValueClause(entry.getKey(), new SearchRequest(query));
            final String url = velocityRequestContextFactory.getJiraVelocityRequestContext().getBaseUrl() +
                    "/issues/?jql=" + JiraUrlCodec.encode(searchService.getJqlString(urlSearchRequest.getQuery()), false);

            StatisticsSearchResultRowBean row = new StatisticsSearchResultRowBean(key, percentage, url, value);
            results.add(row);
        }

        if (hitCollector.getIrrelevantCount() > 0)
        {
            final String key = i18nHelper.getText("common.concepts.irrelevant");
            final Long value = hitCollector.getIrrelevantCount();
            final Integer percentage = (int) (100 * value / total);

            StatisticsSearchResultRowBean row = new StatisticsSearchResultRowBean(key, percentage, null, value);
            results.add(row);
        }

        Collections.sort(results, new Comparator<StatisticsSearchResultRowBean>()
        {
            //Decreasing values, increasing keys.
            @Override
            public int compare(final StatisticsSearchResultRowBean first, final StatisticsSearchResultRowBean second)
            {
                int valueComparison = second.getValue().compareTo(first.getValue());
                if (valueComparison == 0)
                {
                    return first.getKey().compareTo(second.getKey());
                }
                else
                {
                    return valueComparison;
                }
            }
        });

        return new StatisticsSearchResultBean(total, results);
    }

    private static class StatisticMapperWrappingSearchRequestAppender<T> implements SearchRequestAppender<T>
    {
        private final StatisticsMapper<T> statisticsMapper;

        public StatisticMapperWrappingSearchRequestAppender(StatisticsMapper<T> statisticsMapper)
        {
            this.statisticsMapper = Assertions.notNull(statisticsMapper);
        }

        @Override
        public SearchRequest appendInclusiveSingleValueClause(T value, SearchRequest searchRequest)
        {
            return statisticsMapper.getSearchUrlSuffix(value, searchRequest);
        }

        @Override
        public SearchRequest appendExclusiveMultiValueClause(Iterable<? extends T> values, SearchRequest searchRequest)
        {
            return null;
        }
    }
}
