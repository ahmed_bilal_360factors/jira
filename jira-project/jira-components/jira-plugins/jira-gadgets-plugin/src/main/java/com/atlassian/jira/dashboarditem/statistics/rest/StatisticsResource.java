package com.atlassian.jira.dashboarditem.statistics.rest;

import javax.annotation.Nullable;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.fugue.Either;
import com.atlassian.jira.dashboarditem.statistics.service.StatisticsResult;
import com.atlassian.jira.dashboarditem.statistics.service.StatisticsService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import org.apache.commons.lang.StringUtils;


/**
 * Statistic searching REST resource.
 */
@Path ("/statistics")
@AnonymousAllowed
public class StatisticsResource
{
    private static final String SEARCH_JQL = "jql";
    private static final String STAT_TYPE = "statType";

    private final JiraAuthenticationContext authenticationContext;
    private final StatisticsService statisticsService;

    public StatisticsResource(
            final JiraAuthenticationContext authenticationContext,
            final StatisticsService statisticsService)
    {
        this.authenticationContext = authenticationContext;
        this.statisticsService = statisticsService;
    }

    /**
     * Complete a one dimensional search on a jql search query.
     * @param jql to search
     * @param statType to group search results by
     * @return http response.
     */
    @GET
    @Produces ({ MediaType.APPLICATION_JSON })
    public Response oneDimensionalSearch(
            @QueryParam (SEARCH_JQL) @Nullable final String jql,
            @QueryParam (STAT_TYPE) @Nullable final String statType)
    {
        ErrorCollection parameterErrors = validParameters(jql, statType);
        if (parameterErrors.hasAnyErrors())
        {
            return createErrorResponse(parameterErrors);
        }

        final Either<ErrorCollection, StatisticsResult> result =
                statisticsService.aggregateOneDimensionalStats(authenticationContext.getUser(), jql, statType);
        if (result.isLeft())
        {
            return createErrorResponse(result.left().get());
        }
        else
        {
            return Response.ok(result.right().get()).build();
        }
    }

    /**
     * Validate jql and statType so they are not blank or missing.
     *
     * @param jql parameter to check
     * @param statType parameter to check
     *
     * @return any errors in parameter format
     */
    private ErrorCollection validParameters(@Nullable final String jql, @Nullable final String statType)
    {
        ErrorCollection errors = new SimpleErrorCollection();
        if (StringUtils.isBlank(jql)) {
            errors.addErrorMessage(authenticationContext.getI18nHelper().getText("rest.missing.field", SEARCH_JQL));
            errors.addReason(ErrorCollection.Reason.VALIDATION_FAILED);
        }

        if (StringUtils.isBlank(statType)) {
            errors.addErrorMessage(authenticationContext.getI18nHelper().getText("rest.missing.field", STAT_TYPE));
            errors.addReason(ErrorCollection.Reason.VALIDATION_FAILED);
        }
        return errors;
    }

    /**
     * Generate a generic error response given errors.
     * @param errors to generate response with
     * @return http response.
     */
    private Response createErrorResponse(final ErrorCollection errors)
    {
        ErrorCollection.Reason worstReason = ErrorCollection.Reason.getWorstReason(errors.getReasons());
        int errorCode = 500;
        if (worstReason != null) {
            errorCode = worstReason.getHttpStatusCode();
        }
        return createErrorResponse(errors, errorCode);
    }

    /**
     * Generate an error response given errors and an error code.
     * @param errors to generate response with
     * @param errorCode of the http response
     * @return http response.
     */
    private Response createErrorResponse(final ErrorCollection errors, int errorCode)
    {
        return Response.status(errorCode).entity(errors.getErrorMessages()).build();
    }
}