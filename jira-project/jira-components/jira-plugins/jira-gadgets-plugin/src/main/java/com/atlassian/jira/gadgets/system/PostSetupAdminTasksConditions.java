package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.mail.settings.MailSettings;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * @since v6.4
 */
@Component
public class PostSetupAdminTasksConditions
{
    private final AdminTaskManager.Condition instantSetupPathTaken;
    private final MailSettings mailSettings;
    private final MailServerManager mailServerManager;

    @Inject
    public PostSetupAdminTasksConditions(
            final ApplicationProperties applicationProperties,
            final @ComponentImport MailSettings mailSettings,
            final @ComponentImport MailServerManager mailServerManager)
    {
        this.mailSettings = mailSettings;
        this.mailServerManager = mailServerManager;
        instantSetupPathTaken = new AdminTaskManager.Condition()
        {
            @Override
            public boolean isDone()
            {
                return false;
            }

            @Override
            public boolean isEnabled()
            {
                return applicationProperties.getOption(APKeys.JIRA_SETUP_IS_INSTANT);
            }
        };
    }

    public AdminTaskManager.Condition getAdminAccountDetailsCondition()
    {
        return instantSetupPathTaken;
    }

    public AdminTaskManager.Condition getAppPropertiesCondition()
    {
        return instantSetupPathTaken;
    }

    public AdminTaskManager.Condition getMailPropertiesCondition()
    {
        return new AdminTaskManager.Condition()
        {
            @Override
            public boolean isDone()
            {
                return mailServerManager.isDefaultSMTPMailServerDefined() && mailSettings.send().isEnabled();
            }

            @Override
            public boolean isEnabled()
            {
                return mailSettings.send().isModifiable();
            }
        };
    }

}
