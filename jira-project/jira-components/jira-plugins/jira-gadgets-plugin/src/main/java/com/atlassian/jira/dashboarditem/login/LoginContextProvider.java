package com.atlassian.jira.dashboarditem.login;

import com.atlassian.jira.bc.security.login.LoginProperties;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.collect.Maps;

import java.util.Map;

@Scanned
public class LoginContextProvider implements ContextProvider
{
    private final ApplicationProperties applicationProperties;
    private JiraAuthenticationContext authenticationContext;
    private LoginService loginService;

    public LoginContextProvider(
            @ComponentImport final ApplicationProperties applicationProperties,
            @ComponentImport final LoginService loginService,
            @ComponentImport final JiraAuthenticationContext authenticationContext
    )
    {
        this.loginService = loginService;
        this.applicationProperties = applicationProperties;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public void init(final Map<String, String> params) throws PluginParseException
    {
    }

    @Override
    public Map<String, Object> getContextMap(final Map<String, Object> context)
    {
        LoginProperties loginProperties = loginService.getLoginProperties(authenticationContext.getLoggedInUser(), ExecutingHttpRequest.get());

        final Map<String, Object> newContext = Maps.newHashMap(context);
        newContext.put("showCaptcha", loginProperties.isElevatedSecurityCheckShown());
        newContext.put("isPublicMode", loginProperties.isPublicMode());
        newContext.put("adminFormOn", String.valueOf(applicationProperties.getOption(APKeys.JIRA_SHOW_CONTACT_ADMINISTRATORS_FORM)));
        newContext.put("showForgotPassword", !loginProperties.isExternalPasswordManagement() && !loginProperties.isExternalUserManagement());
        newContext.put("showRememberMe", loginProperties.isAllowCookies());
        newContext.put("errorMessage", "");
        newContext.put("captchaTimestamp", System.currentTimeMillis());
        return newContext;
    }
}
