package com.atlassian.jira.gadgets.system;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.statistics.CustomFieldStattable;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.rest.v1.util.CacheControl.NO_CACHE;

/**
 * REST resource for retreiving and validating Statistic Types.
 *
 * @since v4.0
 */
@Path ("/statTypes")
@AnonymousAllowed
@Produces ({ MediaType.APPLICATION_JSON })
@Scanned
public class StatisticTypesResource
{
    private final CustomFieldManager customFieldManager;
    private final JiraAuthenticationContext authenticationContext;
    private final PermissionManager permissionManager;
    private final StatisticTypesProvider statisticTypesProvider;

    public StatisticTypesResource(
            @ComponentImport final CustomFieldManager customFieldManager,
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final PermissionManager permissionManager,
            final StatisticTypesProvider statisticTypesProvider)
    {

        this.customFieldManager = customFieldManager;
        this.authenticationContext = authenticationContext;
        this.permissionManager = permissionManager;
        this.statisticTypesProvider = statisticTypesProvider;
    }

    /**
     * Retreive all available Statistic Types in JIRA.  Both System and Custom Fields.
     *
     * @return A collection key/value pairs {@link MapEntry}
     */
    @GET
    public Response getAxies()
    {
        final Map<String, String> values = getValues();
        return Response.ok(new StatTypeCollection(values)).cacheControl(NO_CACHE).build();
    }

    private Map<String, String> getValues()
    {
        final I18nHelper i18n = authenticationContext.getI18nHelper();

        final Map<String, String> allValues = new LinkedHashMap<String, String>();

        for (Map.Entry entry : statisticTypesProvider.getSystemStatisticTypes().entrySet())
        {
            allValues.put((String) entry.getKey(), i18n.getText((String) entry.getValue()));
        }
        final List<CustomField> customFieldObjects = customFieldManager.getCustomFieldObjects();

        for (CustomField customField : customFieldObjects)
        {
            if (hasCustomFieldAccess(customField) && (customField.getCustomFieldSearcher() instanceof CustomFieldStattable))
            {
                allValues.put(customField.getId(), i18n.getText(customField.getName()));
            }
        }

        return allValues;
    }

    private boolean hasCustomFieldAccess(final CustomField customField)
    {
        final ApplicationUser user = authenticationContext.getUser();

        if (customField.isAllProjects())
        {
            // if custom field is attached to all projects
            // check if current user can access any projects
            return permissionManager.hasProjects(BROWSE_PROJECTS, user);
        }

        // from all projects associated with this CF
        // find any that may be browsed by current user
        return Iterables.any(customField.getAssociatedProjectObjects(), new Predicate<Project>()
        {
            @Override
            public boolean apply(Project project)
            {
                return permissionManager.hasPermission(BROWSE_PROJECTS, project, user);
            }
        });
    }


    ///CLOVER:OFF
    @XmlRootElement
    public static class StatTypeCollection
    {
        @XmlElement
        private Collection<MapEntry> stats;

        @SuppressWarnings ({ "UnusedDeclaration", "unused" })
        private StatTypeCollection()
        { }

        public StatTypeCollection(final Map<String, String> values)
        {

            this.stats = convertToMapEntryCollection(values);
        }

        public Collection<MapEntry> getValues()
        {
            return stats;
        }

        private Collection<MapEntry> convertToMapEntryCollection(final Map<String, String> values)
        {
            final Set<Map.Entry<String,String>> mapEntries = values.entrySet();
            final Collection<MapEntry> entries = new ArrayList<MapEntry>(mapEntries.size());
            for (Map.Entry<String, String> mapEntry : mapEntries)
            {
                entries.add(new MapEntry(mapEntry.getKey(), mapEntry.getValue()));
            }
            return entries;
        }
    }

    @XmlRootElement
    public static class MapEntry
    {
        @XmlElement
        private final String value;

        @XmlElement
        private final String label;

        @SuppressWarnings ({ "UnusedDeclaration", "unused" })
        private MapEntry()
        {
            value = null;
            label = null;
        }

        public MapEntry(final String value, final String label)
        {
            this.value = value;
            this.label = label;
        }

        public String getKey()
        {
            return value;
        }

        public String getValue()
        {
            return label;
        }
    }
    ///CLOVER:ON
}
