package com.atlassian.jira.dashboarditem.statistics.service;

import com.atlassian.fugue.Either;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;

/**
 * Used to generate statistical results for given searches and groupings.
 *
 * @since v6.4
 */
public interface StatisticsService
{
    /**
     * Generates statistics for a given search grouped by a statistic type
     * @param user completing the search
     * @param jql to search
     * @param statType to group by
     * @return Either error if search was invalid or a set of results
     */
    Either<ErrorCollection, StatisticsResult> aggregateOneDimensionalStats(final ApplicationUser user, final String jql, final String statType);
}
