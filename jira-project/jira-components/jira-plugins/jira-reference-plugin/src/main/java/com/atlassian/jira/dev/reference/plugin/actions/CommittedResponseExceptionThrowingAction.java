package com.atlassian.jira.dev.reference.plugin.actions;

import java.io.PrintWriter;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.web.pagebuilder.GeneralJspDecorator;
import com.atlassian.jira.web.pagebuilder.JiraPageBuilderService;


/**
 * Action that commits the HTTP response (by triggering flush-head-early) and then throws a RuntimeException.
 * Primarily used by JIRA's com.atlassian.jira.webtests.ztests.sendheadearly.TestCommittedResponseHtmlErrorRecovery func test.
 *
 * @since 6.4
 */
public class CommittedResponseExceptionThrowingAction extends JiraWebActionSupport
{
    private static final String POST_FLUSH_CONTENT = "POST_FLUSH_CONTENT";
    private static final String SERVLET_EXCEPTION_MESSAGE = "This exception was thrown on purpose.";

    private final JiraPageBuilderService jiraPageBuilderService;

    public CommittedResponseExceptionThrowingAction(JiraPageBuilderService jiraPageBuilderService)
    {
        this.jiraPageBuilderService = jiraPageBuilderService;
    }

    @Override
    protected String doExecute() throws Exception
    {
        jiraPageBuilderService.get().setDecorator(new GeneralJspDecorator(jiraPageBuilderService.assembler()));
        jiraPageBuilderService.get().flush();

        // Call to getWriter() is here on purpose, but does nothing. It has the /potential/ to change the
        // behaviour of the PageBuilderFilter. It causes responseWrapper.isBuffering() to be true, which makes
        // the PageBuilderFilter want to decorate the page -- it still shouldn't though, because we've chucked
        // an exception after flushing.
        PrintWriter writer = getHttpResponse().getWriter();
        writer.println(POST_FLUSH_CONTENT);

        throw new RuntimeException(SERVLET_EXCEPTION_MESSAGE);
    }
}
