package com.atlassian.jira.dev.reference.plugin.components;

import com.atlassian.event.api.EventListener;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReferenceEventListener
{
    private static final Logger log = LoggerFactory.getLogger(ReferenceEventListener.class);
    
    @EventListener
    public void listenForIssueEvent(IssueEvent issueEvent)
    {
        if (issueEvent.getEventTypeId().equals(EventType.ISSUE_CREATED_ID))
        {
            log.info("Reference Event listener got Issue Created Event");
        }
    }
}
