package com.atlassian.jira.index.request;

import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.annotations.PublicApi;

/**
 * Used for persistence and control of reindex requests.
 *
 * @since 6.4
 */
@PublicApi
public interface ReindexRequestManager
{
    /**
     * Checks if any ReindexRequests of any type are waiting to be processed.
     *
     * @return true if unprocessed requests exist
     */
    public boolean isReindexRequested();

    /**
     * Checks if any ReindexRequests are waiting to be processed.
     *
     * @param reindexRequestTypes Types of requests to check for.
     * @return true if unprocessed requests exist
     */
    public boolean isReindexRequested(final Set<ReindexRequestType> reindexRequestTypes);

    /**
     * Retrieves all {@linkplain com.atlassian.jira.index.request.ReindexStatus#PENDING pending} reindex requests, in order
     * from oldest to newest.
     *
     * @param reindexRequestTypes Types of requests to check for.
     * @return a list of reindex requests.
     */
    @Nonnull
    public Iterable<ReindexRequest> getPendingReindexRequests(final Set<ReindexRequestType> reindexRequestTypes);

    /**
     * @return true if a reindex is in progress, false if not.
     */
    public boolean isReindexInProgress();

    /**
     * Requests a reindex.
     *
     * @param type whether reindex is immediate or delayed.
     * @param affectedIndexes the indexes to regenerate.
     * @param entityTypes the entity types whose indexes are to be regenerated.
     *
     * @return the request that was created.
     */
    @Nonnull
    public ReindexRequest requestReindex(@Nonnull ReindexRequestType type,
                    @Nonnull Set<AffectedIndex> affectedIndexes, @Nonnull Set<SharedEntityType> entityTypes);

    /**
     * Transitions the status of reindex requests.
     *
     * @param requests the requests.
     * @param newStatus the new status to transition to.
     *
     * @return the updated requests.
     */
    @Nonnull
    public List<ReindexRequest> transitionStatus(@Nonnull Iterable<ReindexRequest> requests, @Nonnull ReindexStatus newStatus);

    /**
     * Transitions the status of a reindex request.
     *
     * @param request the request.
     * @param newStatus the new status to transition to.
     *
     * @return the updated request.
     */
    @Nonnull
    public ReindexRequest transitionStatus(@Nonnull ReindexRequest request, @Nonnull ReindexStatus newStatus);

    /**
     * Retrieves the progress of some requests.
     *
     * @param requestIds the set of {@linkplain ReindexRequest#getId() reindex request IDs} to look up.
     *
     * @return set of requests indicating progress.  Any request IDs that were not found will not have a corresponding entry
     *          in this result set.
     */
    @Nonnull
    public Set<ReindexRequest> getReindexProgress(@Nonnull Set<Long> requestIds);

    /**
     * Process any outstanding reindex requests.
     * @param waitForCompletion If true this method will not return until the reindexing is complete, otherwise it will return
     *                          when the reindex task is submitted,
     * @param reindexRequestTypes Types of requests to process either {@link ReindexRequestType}
     * @param runInBackground If true the reindex should be a background reindex otherwise a foreground stop the world reindex
     * @return The updated processed requests.
     */
    Set<ReindexRequest> processPendingRequests(boolean waitForCompletion, Set<ReindexRequestType> reindexRequestTypes, boolean runInBackground);

    /**
     * Finds any active or running requests from inactive nodes in the cluster and transitions them
     * to failed.  Use this to clean up any tasks from nodes that were running on nodes that were killed.
     *
     * @return the list of requests that were transitioned.
     */
    @Nonnull
    public List<ReindexRequest> failRunningRequestsFromDeadNodes();

    /**
     * Finds any active or running requests from the specified node in the cluster and transitions them to failed.
     *
     * @param nodeId the ID of the cluster node, or null if not clustered.
     *
     * @return the list of requests that were transitioned.
     */
    @Nonnull
    public List<ReindexRequest> failRunningRequestsFromNode(@Nullable String nodeId);

    /**
     * Clear any pending requests.
     */
    void clearAll();
}
