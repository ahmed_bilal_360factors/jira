package com.atlassian.jira.issue.attachment;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;

/**
 * An object representing identity of temporary attachment.
 *
 * @since v6.4
 */
@Immutable
@ParametersAreNonnullByDefault
public final class TemporaryAttachmentId
{
    private final String temporaryId;

    private TemporaryAttachmentId(final String temporaryId)
    {
        this.temporaryId = temporaryId;
    }

    public static TemporaryAttachmentId fromString(final String temporaryId)
    {
        return new TemporaryAttachmentId(temporaryId);
    }

    public String toStringId()
    {
        return temporaryId;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final TemporaryAttachmentId that = (TemporaryAttachmentId) o;

        if (!temporaryId.equals(that.temporaryId)) { return false; }

        return true;
    }

    @Override
    public int hashCode()
    {
        return temporaryId.hashCode();
    }
}
