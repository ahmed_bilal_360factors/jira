package com.atlassian.jira.tenancy;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.tenancy.api.Tenant;
import com.atlassian.tenancy.api.TenantContext;

import javax.annotation.Nullable;

/**
 * The tenant context is used to track tenants
 *
 * @since v6.4
 */
@ExperimentalApi
public interface JiraTenantContext extends TenantContext
{

    @Nullable
    Tenant getCurrentTenant();

    void setCurrentTenant(Tenant tenant);

    void clearTenant();
}
