package com.atlassian.jira.event.property;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.event.AbstractEvent;

@ExperimentalApi
public abstract class AbstractApplicationPropertySetEvent<T> extends AbstractEvent
{
    private final String applicationPropertyKey;
    private final T value;

    public AbstractApplicationPropertySetEvent(final String applicationPropertyKey, final T value)
    {
        this.applicationPropertyKey = applicationPropertyKey;
        this.value = value;
    }

    public String getPropertyKey()
    {
        return applicationPropertyKey;
    }

    public T getPropertyValue()
    {
        return value;
    }
}
