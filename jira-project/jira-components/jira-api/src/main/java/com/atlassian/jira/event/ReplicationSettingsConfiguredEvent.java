package com.atlassian.jira.event;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.annotations.ExperimentalApi;

/**
 * Published when an attachment stores configuration is changed
 */
@ExperimentalApi
@EventName ("ha.storage.replication.enabled")
public final class ReplicationSettingsConfiguredEvent extends AbstractEvent
{
    private boolean attachments;
    private boolean plugins;
    private boolean avatars;
    private boolean indexSnaphots;

    /**
     * Event to publish when changing the replication settings.
     * @param attachments True if attachments will be replicated to secondary storage.
     * @param plugins True if installed plugins will be replicated to secondary storage.
     * @param avatars True if avatars will be replicated to secondary storage.
     * @param indexSnaphots True if indexSnapshots will be replicated to secondary storage.
     */
    public ReplicationSettingsConfiguredEvent(final boolean attachments, final boolean plugins, final boolean avatars,
            final boolean indexSnaphots)
    {
        this.attachments = attachments;
        this.plugins = plugins;
        this.avatars = avatars;
        this.indexSnaphots = indexSnaphots;
    }

    @SuppressWarnings("unused") //called during event firing
    public boolean isAttachments()
    {
        return attachments;
    }

    @SuppressWarnings("unused") //called during event firing
    public boolean isPlugins()
    {
        return plugins;
    }

    @SuppressWarnings("unused") //called during event firing
    public boolean isAvatars()
    {
        return avatars;
    }

    @SuppressWarnings("unused") //called during event firing
    public boolean isIndexSnaphots()
    {
        return indexSnaphots;
    }
}
