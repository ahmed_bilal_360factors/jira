package com.atlassian.jira.issue.fields.rest.json.beans;

import com.google.common.base.Objects;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since 6.4
 */
public class IssueTypeCreateBean
{
    static final IssueTypeCreateBean CREATE_EXAMPLE;
    static
    {
        final IssueTypeCreateBean issueTypeCreateBean = new IssueTypeCreateBean();
        issueTypeCreateBean.setDescription("description");
        issueTypeCreateBean.setName("name");
        issueTypeCreateBean.setType(Type.standard);
        CREATE_EXAMPLE = issueTypeCreateBean;
    }

    public enum Type { subtask, standard }

    @JsonProperty
    private String name;
    @JsonProperty
    private String description;
    @JsonProperty
    private Type type = Type.standard;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public Type getType()
    {
        return type;
    }

    public void setType(final Type type)
    {
        this.type = Objects.firstNonNull(type, this.type);
    }
}
