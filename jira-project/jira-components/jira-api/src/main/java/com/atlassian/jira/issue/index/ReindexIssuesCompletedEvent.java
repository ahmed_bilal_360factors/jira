package com.atlassian.jira.issue.index;

import com.atlassian.annotations.PublicApi;

/**
* Event raised when indexing a set of issues has completed.
* @since v5.0
*/
@PublicApi
public class ReindexIssuesCompletedEvent implements IndexEvent
{
    /**
     * The amount of time it took to reindex the issues.
     */
    final public long time;

    public ReindexIssuesCompletedEvent(long time)
    {
        this.time = time;
    }
}
