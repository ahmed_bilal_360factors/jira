package com.atlassian.jira.bc.workflow;

import com.atlassian.annotations.Internal;
import com.opensymphony.workflow.loader.FunctionDescriptor;

/**
 * A container-injectable factory that provides methods to create instance of
 * {@link FunctionDescriptor} to be used in {@link WorkflowTransitionService}.
 *
 * @since v6.4
 */
@Internal
public interface WorkflowFunctionDescriptorFactory
{
    /**
     * Creates an instance of {@link FunctionDescriptor} that updates an issue field.
     *
     * @param name of the field to be updated.
     * @param value to be set for the issue field with {@code name}.
     *
     * @return A {@link FunctionDescriptor} that set an issue field named {@code name} with {@code value} when invoked.
     */
    FunctionDescriptor updateIssueField(String name, String value);
}
