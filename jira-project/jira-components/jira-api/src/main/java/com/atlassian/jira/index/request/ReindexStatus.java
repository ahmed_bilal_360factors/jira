package com.atlassian.jira.index.request;

import com.atlassian.annotations.PublicApi;

/**
 * Status for a reindex operation request.
 *
 * @since 6.4
 */
@PublicApi
public enum ReindexStatus
{
    /**
     * Not started yet.
     */
    PENDING,

    /**
     * A reindex task is processing the request as part of a group, and this request is waiting to be executed.
     */
    ACTIVE,

    /**
     * Reindex is being performed.
     */
    RUNNING,

    /**
     * Reindex was attempted and failed.
     */
    FAILED,

    /**
     * Completed successfully.
     */
    COMPLETE
}
