package com.atlassian.jira.issue;

import java.util.List;

import javax.annotation.concurrent.Immutable;

import com.atlassian.fugue.Pair;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;

/**
 * Holds results and errors from bulk operation on attachments.
 *
 * @since v6.4
 */
@Immutable
public class AttachmentsBulkOperationResult<T>
{
    private final List<AttachmentError> errors;
    private final List<T> results;

    public AttachmentsBulkOperationResult(final List<AttachmentError> errors, final List<T> results)
    {
        this.errors = ImmutableList.copyOf(errors);
        this.results = ImmutableList.copyOf(results);
    }

    public AttachmentsBulkOperationResult(final Pair<List<AttachmentError>, List<T>> errorAndResult)
    {
        this(errorAndResult.left(), errorAndResult.right());
    }

    public List<AttachmentError> getErrors()
    {
        return errors;
    }

    public List<T> getResults()
    {
        return results;
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(errors, results);
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()) {return false;}
        final AttachmentsBulkOperationResult other = (AttachmentsBulkOperationResult) obj;
        return Objects.equal(this.errors, other.errors) && Objects.equal(this.results, other.results);
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this)
                .add("errors", errors)
                .add("results", results)
                .toString();
    }
}
