package com.atlassian.jira.index.request;

import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.exception.PermissionException;

/**
 * @since 6.4
 */
@PublicApi
public interface ReindexRequestService
{
    /**
     * Processes any outstanding reindex requests asynchronously.
     * @param reindexRequestTypes The request types to process.
     *
     * @param runInBackground
     * @return the reindex requests that are being processed.
     */
    @Nonnull
    public Set<ReindexRequest> processRequests(Set<ReindexRequestType> reindexRequestTypes, final boolean runInBackground) throws PermissionException;

    /**
     * Processes any outstanding reindex requests and waits for them to finish.
     * @param reindexRequestTypes The request types to process.
     *
     * @param runInBackground
     * @return the reindex requests that were processed.
     */
    @Nonnull
    public Set<ReindexRequest> processRequestsAndWait(Set<ReindexRequestType> reindexRequestTypes, final boolean runInBackground) throws PermissionException;

    /**
     * Requests a reindex, adding a reindex request to the queue.  There is no guarantee that this request will be
     * performed immediately.
     *
     * @param type whether reindex is immediate or delayed.
     * @param affectedIndexes the indexes to regenerate.
     * @param entityTypes the entity types whose indexes are to be regenerated.
     * @return the request that was created.
     */
    @Nonnull
    public ReindexRequest requestReindex(@Nonnull ReindexRequestType type,
            @Nonnull Set<AffectedIndex> affectedIndexes, @Nonnull Set<SharedEntityType> entityTypes);

    /**
     * Reads progress of a reindex request.
     *
     * @param requestId the ID of the reindex request.
     *
     * @return reindex progress, or <code>null</code> if no request with the specified ID exists.
     */
    @Nullable
    public ReindexRequest getReindexProgress(long requestId);

    /**
     * Reads progress of multiple reindex requests.
     *
     * @param requestIds a set of reindex request IDs.
     *
     * @return the reindex request progress.  Any reindex request specified in <code>requestIds</code> that was not
     *          found will not have a corresponding element in the return set.
     */
    @Nonnull
    public Set<ReindexRequest> getReindexProgress(@Nonnull Set<Long> requestIds);

    public boolean isReindexRequested();

}
