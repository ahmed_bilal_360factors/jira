package com.atlassian.jira.task.context;

import com.atlassian.annotations.Internal;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.annotation.Nonnull;

import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static java.text.MessageFormat.format;

/**
 * Simple sink that writes output to a Logger.
 *
 * @since v3.13
 */
@Internal
class LoggingContextSink implements Sink
{
    private final Logger log;
    private String name = "";
    private final String msg;
    private final Level level;

    LoggingContextSink(@Nonnull final Logger log, @Nonnull final String msg, @Nonnull final Level level)
    {
        this.log = notNull("log", log);
        this.msg = notNull("msg", msg);
        this.level = notNull("level", level);
    }

    public void setName(final String name)
    {
        this.name = notNull("name", name);
    }

    public void updateProgress(final int progress)
    {
        log.log(level, format(msg, progress, name));
    }
}
