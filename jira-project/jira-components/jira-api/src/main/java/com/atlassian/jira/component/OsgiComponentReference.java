package com.atlassian.jira.component;

import javax.annotation.Nonnull;

import com.atlassian.util.concurrent.Supplier;

import static com.atlassian.jira.component.ComponentAccessor.getOSGiComponentInstanceOfType;

/**
 * Represents a reference like {@link com.atlassian.jira.component.ComponentReference} but for OSGi Components.
 *
 * @since 6.4
 */
public class OsgiComponentReference<T> extends ComponentReference<T> implements Supplier<T>, com.atlassian.jira.util.Supplier<T>
{
    private final Class<T> componentClass;

    public OsgiComponentReference(@Nonnull final Class<T> componentClass)
    {
        super(componentClass);
        this.componentClass = componentClass;
    }

    @Override
    public T get()
    {
        return getOSGiComponentInstanceOfType(componentClass);
    }
}
