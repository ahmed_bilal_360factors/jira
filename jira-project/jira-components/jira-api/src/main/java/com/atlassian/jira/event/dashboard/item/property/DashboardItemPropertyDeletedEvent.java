package com.atlassian.jira.event.dashboard.item.property;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.event.entity.AbstractPropertyEvent;
import com.atlassian.jira.event.entity.EntityPropertyDeletedEvent;
import com.atlassian.jira.user.ApplicationUser;

@PublicApi
public final class DashboardItemPropertyDeletedEvent  extends AbstractPropertyEvent implements EntityPropertyDeletedEvent
{
    public DashboardItemPropertyDeletedEvent(final EntityProperty entityProperty, final ApplicationUser user)
    {
        super(entityProperty, user);
    }
}
