/*
 * Created by IntelliJ IDEA.
 * User: Mike
 * Date: Jul 28, 2004
 * Time: 11:01:00 AM
 */
package com.atlassian.jira.plugin.report;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.fugue.Option;
import com.atlassian.jira.plugin.ConfigurableModuleDescriptor;
import com.atlassian.jira.plugin.JiraResourcedModuleDescriptor;
import com.atlassian.jira.project.Project;
import com.atlassian.plugin.web.descriptors.WeightedDescriptor;

import java.util.Map;

/**
 * The report plugin allows end users to write pluggable reports for JIRA.
 *
 * @see Report
 */

public interface ReportModuleDescriptor extends JiraResourcedModuleDescriptor<Report>, ConfigurableModuleDescriptor, WeightedDescriptor
{
    public Report getModule();

    public String getLabel();

    public String getLabelKey();

    /**
     * Returns url for first page of this report. Project parameter is provided to insert context into the URL
     *
     * @param project project in which context this report is opened
     * @return url for the first page of this report
     * @since  6.2
     */
    public String getUrl(Project project);

    /**
     * Returns optional URL for the first page of this report within specified context.
     * {@code none()} means the report should not show in this particular context.
     *
     * @param context context in which this report is opened
     * @return Report URL or {@link com.atlassian.fugue.Option#none()} when report should not show in this context
     * @since 6.4
     */
    @ExperimentalApi
    public Option<String> getUrl(Map<String, Object> context);

    /**
     * Returns the report category. Should not be null.
     *
     * @return the report category
     * @since 6.4
     */
    public ReportCategory getCategory();

    /**
     * Returns the class which will be used to decorate report thumbnail
     * @return css class
     * @since 6.4
     */
    public String getThumbnailCssClass();

}
