package com.atlassian.jira.permission;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.plugin.web.Condition;

/**
 * Represents a project permission.
 *
 * @since v6.3
 */
@PublicApi
public interface ProjectPermission
{
    /**
     * @return unique key of this project permission.
     * @since v6.3
     */
    String getKey();

    /**
     * @return ProjectPermissionKey object of this project permission.
     * @since v6.4
     */
    ProjectPermissionKey getProjectPermissionKey();

    /**
     * @return i18n key of this permission's name. Cannot be null.
     * @since v6.3
     */
    String getNameI18nKey();

    /**
     * @return i18n key of this permission's description. Can be null.
     * @since v6.3
     */
    String getDescriptionI18nKey();

    /**
     * @return category of this permission. Cannot be null.
     * @since v6.3
     */
    ProjectPermissionCategory getCategory();

    /**
     * @return condition to determine the availability of this permission. Cannot be null.
     * @since v6.4
     */
    Condition getCondition();
}
