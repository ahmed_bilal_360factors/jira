package com.atlassian.jira.index.request;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.annotations.PublicApi;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

/**
 * A reindex request with components.
 *
 * @since 6.4
 */
@PublicApi
public class ReindexRequest extends ReindexRequestBase
{
    private final @Nonnull Set<AffectedIndex> affectedIndexes;
    private final @Nonnull Set<SharedEntityType> sharedEntities;

    private final @Nonnull List<ReindexRequest> sources;

    protected ReindexRequest(@Nullable Long id, @Nonnull ReindexRequestType type,
            long requestTime, @Nullable Long startTime, @Nullable Long completionTime, @Nullable String executionNodeId,
            @Nonnull ReindexStatus status, @Nonnull Set<AffectedIndex> affectedIndexes,
            @Nonnull Set<SharedEntityType> sharedEntities,
            @Nonnull List<ReindexRequest> sources)
    {
        super(id, type, requestTime, startTime, completionTime, executionNodeId, status);
        this.affectedIndexes = ImmutableSet.copyOf(affectedIndexes);
        this.sharedEntities = ImmutableSet.copyOf(sharedEntities);
        this.sources = ImmutableList.copyOf(sources);
    }

    public ReindexRequest(@Nullable Long id, @Nonnull ReindexRequestType type,
            long requestTime, @Nullable Long startTime, @Nullable Long completionTime, @Nullable String executionNodeId,
            @Nonnull ReindexStatus status, @Nonnull Set<AffectedIndex> affectedIndexes,
            @Nonnull Set<SharedEntityType> sharedEntities)
    {
        this(id, type, requestTime, startTime, completionTime, executionNodeId, status, affectedIndexes, sharedEntities, ImmutableList.<ReindexRequest>of());
    }

    public ReindexRequest(@Nonnull ReindexRequestType type,
            long requestTime, @Nullable Long startTime, @Nullable Long completionTime, @Nullable String executionNodeId,
            @Nonnull ReindexStatus status, @Nonnull Set<AffectedIndex> affectedIndexes,
            @Nonnull Set<SharedEntityType> sharedEntities,
            @Nonnull List<ReindexRequest> sources)
    {
        this(null, type, requestTime, startTime, completionTime, executionNodeId, status, affectedIndexes, sharedEntities, ImmutableList.copyOf(sources));
    }

    public ReindexRequest(@Nonnull ReindexRequestBase base, @Nonnull Set<AffectedIndex> affectedIndexes,
                        @Nonnull Set<SharedEntityType> sharedEntities)
    {
        this(base.getId(), base.getType(), base.getRequestTime(), base.getStartTime(),
                base.getCompletionTime(), base.getExecutionNodeId(), base.getStatus(), affectedIndexes, sharedEntities, ImmutableList.<ReindexRequest>of());
    }

    @Nonnull
    public ReindexRequest withSources(@Nonnull Collection<ReindexRequest> additionalSourcces)
    {
        return new ReindexRequest(getId(), getType(), getRequestTime(), getStartTime(), getCompletionTime(), getExecutionNodeId(), getStatus(), getAffectedIndexes(), getSharedEntities(),
                ImmutableList.<ReindexRequest>builder().addAll(getSources()).addAll(additionalSourcces).build());
    }

    @Nonnull
    public Set<AffectedIndex> getAffectedIndexes()
    {
        return affectedIndexes;
    }

    @Nonnull
    public Set<SharedEntityType> getSharedEntities()
    {
        return sharedEntities;
    }

    /**
     * @return the sources of this request if this reindex request was generated from other requests, such as via
     *          a coalescer.
     */
    @Nonnull
    public List<ReindexRequest> getSources()
    {
        return sources;
    }

    /**
     * Returns the request ID from this request and all source children recursively.  Null IDs are not included.
     *
     * @return a set of request IDs.
     */
    @Nonnull
    public Set<Long> getAllRequestIds()
    {
        Set<Long> requestIds = new LinkedHashSet<Long>();
        readRequestIds(requestIds);
        return requestIds;
    }

    private void readRequestIds(@Nonnull Collection<? super Long> requestIds)
    {
        if (getId() != null)
        {
            requestIds.add(getId());
        }

        for (ReindexRequest source : sources)
        {
            source.readRequestIds(requestIds);
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof ReindexRequest))
        {
            return false;
        }

        ReindexRequest that = (ReindexRequest) o;

        if (!Objects.equal(getId(), that.getId()))
        {
            return false;
        }
        if (!Objects.equal(getType(), that.getType()))
        {
            return false;
        }
        if (!Objects.equal(getRequestTime(), that.getRequestTime()))
        {
            return false;
        }
        if (!Objects.equal(getStartTime(), that.getStartTime()))
        {
            return false;
        }
        if (!Objects.equal(getCompletionTime(), that.getCompletionTime()))
        {
            return false;
        }
        if (!Objects.equal(getExecutionNodeId(), that.getExecutionNodeId()))
        {
            return false;
        }
        if (!Objects.equal(getStatus(), that.getStatus()))
        {
            return false;
        }
        if (!Objects.equal(getAffectedIndexes(), that.getAffectedIndexes()))
        {
            return false;
        }
        if (!Objects.equal(getSharedEntities(), that.getSharedEntities()))
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(getId(), getType(), getRequestTime(), getStartTime(), getCompletionTime(), getExecutionNodeId(), getStatus(), getAffectedIndexes(), getSharedEntities());
    }
}
