package com.atlassian.jira.license;


import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.fugue.Option;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Event fired when a new license is entered into JIRA. This can allow, e.g. plugins that cache license-related
 * information to flush their cache.
 *
 * @since v6.4
 */
@ExperimentalApi
public class LicenseChangedEvent
{
    /**
     * The details of the previous license.
     */
    final public Option<? extends LicenseDetails> previousLicenseDetails;

    /**
     * The details of the new license.
     */
    final public Option<? extends LicenseDetails> newLicenseDetails;

    public LicenseChangedEvent(@Nonnull Option<? extends LicenseDetails> previousLicenseDetails, @Nonnull Option<? extends LicenseDetails> newLicenseDetails)
    {
        this.previousLicenseDetails = checkNotNull(previousLicenseDetails, "previousLicenseDetails");
        this.newLicenseDetails = checkNotNull(newLicenseDetails, "newLicenseDetails");
    }

    /**
     * Get new license details
     *
     * @return the new LicenseDetails or null if license has been removed
     * @since v6.4
     */
    public final Option<? extends LicenseDetails> getNewLicenseDetails()
    {
        return this.newLicenseDetails;
    }

    /**
     * Get previous license details
     *
     * @return the previous LicenseDetails or null if new license
     * @since v6.4
     */
    public final Option<? extends LicenseDetails> getPreviousLicenseDetails()
    {
        return this.previousLicenseDetails;
    }

    /**
     * Determines whether this event is a new license event (new license added)
     *
     * @return true if this is a new license event
     * @since v6.4
     */
    public boolean isNewLicense()
    {
        return !previousLicenseDetails.isDefined() && newLicenseDetails.isDefined();
    }

    /**
     * Determines whether this event is a update license event (license update)
     *
     * @return true if this is a update license event
     * @since v6.4
     */
    public boolean isLicenseUpdated()
    {
        return previousLicenseDetails.isDefined() && newLicenseDetails.isDefined();
    }

    /**
     * Determines whether this event is a remove license event (license removed)
     *
     * @return true if this is a remove license event
     * @since v6.4
     */
    public boolean isLicenseRemoved()
    {
        return previousLicenseDetails.isDefined() && !newLicenseDetails.isDefined();
    }
}