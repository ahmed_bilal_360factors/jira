package com.atlassian.jira.bc.dashboard;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.entity.property.EntityPropertyService;

@PublicApi
public interface DashboardItemPropertyService extends EntityPropertyService<DashboardItem> {}
