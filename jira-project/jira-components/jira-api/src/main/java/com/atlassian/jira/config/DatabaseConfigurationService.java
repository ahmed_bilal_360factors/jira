package com.atlassian.jira.config;

import com.atlassian.annotations.PublicApi;

import javax.annotation.Nullable;

/**
 * Service for reading database configuration properties.
 *
 * @since v6.4.4
 */
@PublicApi
public interface DatabaseConfigurationService
{
    /**
     * Returns the database schema name that is currently used by JIRA.
     * <p>
     * This is an optional field in the database configuration, and therefore it may return null.
     * </p>
     *
     * @return the schema name.
     */
    @Nullable
    String getSchemaName();
}
