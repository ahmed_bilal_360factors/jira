package com.atlassian.jira.issue.attachment;

import com.google.common.base.Objects;
import org.joda.time.DateTime;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.Immutable;

/**
 * Represents temporary attachment uploaded by user which may be later converted into a real attachment.
 *
 * @since v6.4
 */
@Immutable
@ParametersAreNonnullByDefault
public class TemporaryWebAttachment
{
    private final TemporaryAttachmentId temporaryAttachmentId;
    private final String filename;
    private final String contentType;
    private final String formToken;
    private final DateTime created;
    private final long size;

    public TemporaryWebAttachment(final TemporaryAttachmentId temporaryAttachmentId, final String filename,
            final String contentType, final String formToken, final long size, final DateTime created)
    {
        this.temporaryAttachmentId = temporaryAttachmentId;
        this.filename = filename;
        this.contentType = contentType;
        this.formToken = formToken;
        this.size = size;
        this.created = created;
    }

    public TemporaryAttachmentId getTemporaryAttachmentId()
    {
        return temporaryAttachmentId;
    }

    public String getStringId()
    {
        return temporaryAttachmentId.toStringId();
    }

    public String getFilename()
    {
        return filename;
    }

    public String getContentType()
    {
        return contentType;
    }

    public String getFormToken()
    {
        return formToken;
    }

    public long getSize()
    {
        return size;
    }

    public DateTime getCreated()
    {
        return created;
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(temporaryAttachmentId, filename, contentType, formToken, created, size);
    }

    @Override
    public boolean equals(@Nullable final Object obj)
    {
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()) {return false;}
        final TemporaryWebAttachment other = (TemporaryWebAttachment) obj;

        return Objects.equal(this.temporaryAttachmentId, other.temporaryAttachmentId)
                && Objects.equal(this.filename, other.filename)
                && Objects.equal(this.contentType, other.contentType)
                && Objects.equal(this.formToken, other.formToken)
                && Objects.equal(this.created, other.created)
                && Objects.equal(this.size, other.size);
    }


    @Override
    public String toString()
    {
        return Objects.toStringHelper(this)
                .add("temporaryAttachmentId", temporaryAttachmentId)
                .add("filename", filename)
                .add("contentType", contentType)
                .add("formToken", formToken)
                .add("created", created)
                .add("size", size)
                .toString();
    }
}
