package com.atlassian.jira.config;

import javax.annotation.Nonnull;

import com.atlassian.annotations.PublicApi;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.event.config.IssueTypeCreatedEvent;
import com.atlassian.jira.event.config.IssueTypeDeletedEvent;
import com.atlassian.jira.event.config.IssueTypeUpdatedEvent;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.google.common.base.Preconditions.checkArgument;

/**
 * Service for managing issue types.
 *
 * @since 6.4
 */
@PublicApi
public interface IssueTypeService
{
    /**
     * Returns an issue type for the given issue type id, if it is visible for the user.
     *
     * @param applicationUser user which requests access to the issue type.
     * @param id of an issue type to return.
     * @return issue type for id.
     */
    Option<IssueType> getIssueType(ApplicationUser applicationUser, String id);

    /**
     * @param applicationUser user which requests access to issue types.
     * @return all issue types visible for the given user.
     */
    Iterable<IssueType> getIssueTypes(ApplicationUser applicationUser);

    /**
     * Returns a collection of suitable alternative {@link IssueType}s to which issues with the supplied issue type can be moved to.
     * The suitable alternative {@link IssueType}s will have to use the same workflow, the same field configuration and the same screen scheme.
     *
     * @param applicationUser user which requests access to issue types.
     * @param id of an issue type to find alternative for.
     * @return issue type alternative for issue type with given id.
     */
    Iterable<IssueType> getAvailableAlternativesForIssueType(ApplicationUser applicationUser, String id);

    /**
     * This method validates the parameters for creation of an issue type and execute permission checks. If the checks
     * pass the method constructs a new instance of {@link CreateValidationResult} which can be passed
     * to {@link #createIssueType(ApplicationUser, CreateValidationResult)}.
     *
     * @param user against whom the permission checks will be run.
     * @param issueTypeCreateInput this represents the issue type values.
     * @return an instance of {@link CreateValidationResult}.
     */
    CreateValidationResult validateCreateIssueType(ApplicationUser user, @Nonnull IssueTypeCreateInput issueTypeCreateInput);

    /**
     * This method will store a new issue type in JIRA DB.
     * This method will trigger a new {@link IssueTypeCreatedEvent}.
     * @param user against whom the permission checks will be run.
     * @param validationResult validation result returned by {@link #validateCreateIssueType(ApplicationUser, com.atlassian.jira.config.IssueTypeService.IssueTypeCreateInput)}.
     * @return the result of issue type creation.
     */
    IssueTypeResult createIssueType(ApplicationUser user, @Nonnull CreateValidationResult validationResult);

    /**
     * This method validates the parameters for update of an issue type and execute permission checks. If checks
     * pass the method constructs a new instance of {@link UpdateValidationResult} which can be passed to
     * {@link #updateIssueType(ApplicationUser, UpdateValidationResult)}.
     *
     * @param user against whom the permission checks will be run.
     * @param issueTypeId issue type to update.
     * @param issueTypeUpdateInput represents the issue type values.
     * @return an instance of {@link UpdateValidationResult}.
     */
    UpdateValidationResult validateUpdateIssueType(ApplicationUser user, @Nonnull String issueTypeId, @Nonnull IssueTypeUpdateInput issueTypeUpdateInput);

    /**
     * This method will store the update issue type value in JIRA database and trigger a new {@link IssueTypeUpdatedEvent}.
     * @param user against whom the permission checks will be run.
     * @param validationResult validation result returned by {@link #validateUpdateIssueType(ApplicationUser, String, com.atlassian.jira.config.IssueTypeService.IssueTypeUpdateInput)}.
     * @return the result of issue type creation.
     */
    IssueTypeResult updateIssueType(ApplicationUser user, @Nonnull UpdateValidationResult validationResult);

    /**
     * This method validates if it is possible to remove issue type with given id and executes permission checks.
     * @param user against whom the permission checks will be run.
     * @param issueTypeDeleteInput issue type delete input.
     * @return an instance of {@link DeleteValidationResult}.
     */
    DeleteValidationResult validateDeleteIssueType(ApplicationUser user, @Nonnull IssueTypeDeleteInput issueTypeDeleteInput);

    /**
     * Removes the issue from JIRA database and triggers {@link IssueTypeDeletedEvent}.
     * All issues which are associated to the removed issue type are going to be migrated to an alternative issue type.
     * @param user against whom the permission checks will be run.
     * @param validationResult validation result returned by {@link #validateDeleteIssueType(ApplicationUser, IssueTypeDeleteInput)}.
     */
    void deleteIssueType(ApplicationUser user, @Nonnull DeleteValidationResult validationResult);

    class IssueTypeUpdateInput
    {
        private final Long issueTypeToUpdate;
        private final Option<String> name;
        private final Option<String> description;
        private final Option<Long> avatarId;

        protected IssueTypeUpdateInput(final Long issueTypeToUpdate, final Option<String> name, final Option<String> description, final Option<Long> avatarId)
        {
            this.issueTypeToUpdate = issueTypeToUpdate;
            this.name = name;
            this.description = description;
            this.avatarId = avatarId;
        }

        public Option<Long> getAvatarId()
        {
            return avatarId;
        }

        public Long getIssueTypeToUpdate()
        {
            return issueTypeToUpdate;
        }

        public Option<String> getName()
        {
            return name;
        }

        public Option<String> getDescription()
        {
            return description;
        }

        public static Builder builder()
        {
            return new Builder();
        }

        public static class Builder
        {
            private String description;
            private String name;
            private Long avatarId;
            private Long issueTypeToUpdateId;

            public Builder setDescription(final String description)
            {
                this.description = description;
                return this;
            }

            public Builder setName(final String name)
            {
                this.name = name;
                return this;
            }

            public Builder setAvatarId(final Long avatarId)
            {
                this.avatarId = avatarId;
                return this;
            }

            public void setIssueTypeToUpdateId(final Long issueTypeToUpdateId)
            {
                this.issueTypeToUpdateId = issueTypeToUpdateId;
            }

            public IssueTypeUpdateInput build()
            {
                return new IssueTypeUpdateInput(
                        issueTypeToUpdateId,
                        Option.option(name),
                        Option.option(description),
                        Option.option(avatarId));
            }
        }

        @Override
        public boolean equals(final Object o)
        {
            if (this == o) { return true; }
            if (o == null || getClass() != o.getClass()) { return false; }

            final IssueTypeUpdateInput that = (IssueTypeUpdateInput) o;

            if (avatarId != null ? !avatarId.equals(that.avatarId) : that.avatarId != null) { return false; }
            if (description != null ? !description.equals(that.description) : that.description != null)
            {
                return false;
            }
            if (issueTypeToUpdate != null ? !issueTypeToUpdate.equals(that.issueTypeToUpdate) : that.issueTypeToUpdate != null)
            { return false; }
            if (name != null ? !name.equals(that.name) : that.name != null) { return false; }

            return true;
        }

        @Override
        public int hashCode()
        {
            int result = issueTypeToUpdate != null ? issueTypeToUpdate.hashCode() : 0;
            result = 31 * result + (name != null ? name.hashCode() : 0);
            result = 31 * result + (description != null ? description.hashCode() : 0);
            result = 31 * result + (avatarId != null ? avatarId.hashCode() : 0);
            return result;
        }
    }

    class IssueTypeCreateInput
    {
        public enum Type { STANDARD, SUBTASK }

        private final String name;
        private final Type type;
        private final Option<String> description;

        private IssueTypeCreateInput(final String name, final Type type, final Option<String> description)
        {
            this.name = name;
            this.type = type;
            this.description = description;
        }

        public Type getType()
        {
            return type;
        }

        public String getName()
        {
            return name;
        }

        public Option<String> getDescription()
        {
            return description;
        }

        public static Builder builder()
        {
            return new Builder();
        }

        public static class Builder
        {
            private String description;
            private String name;
            private Type type;

            public Builder setDescription(final String description)
            {
                this.description = description;
                return this;
            }

            public Builder setName(final String name)
            {
                this.name = name;
                return this;
            }

            public Builder setType(final Type type)
            {
                this.type = type;
                return this;
            }

            public IssueTypeCreateInput build()
            {
                checkArgument(type != null);
                checkArgument(name != null);

                return new IssueTypeCreateInput(name,
                        type, Option.option(description));
            }
        }

        @Override
        public boolean equals(final Object o)
        {
            if (this == o) { return true; }
            if (o == null || getClass() != o.getClass()) { return false; }

            final IssueTypeCreateInput that = (IssueTypeCreateInput) o;

            if (description != null ? !description.equals(that.description) : that.description != null)
            {
                return false;
            }
            if (name != null ? !name.equals(that.name) : that.name != null) { return false; }
            if (type != that.type) { return false; }

            return true;
        }

        @Override
        public int hashCode()
        {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (type != null ? type.hashCode() : 0);
            result = 31 * result + (description != null ? description.hashCode() : 0);
            return result;
        }
    }

    class IssueTypeDeleteInput
    {
        private final String issueTypeToDeleteId;
        private final Option<String> alternativeIssueTypeId;

        public IssueTypeDeleteInput(final String issueTypeToDeleteId, final Option<String> alternativeIssueTypeId)
        {
            this.issueTypeToDeleteId = issueTypeToDeleteId;
            this.alternativeIssueTypeId = alternativeIssueTypeId;
        }

        public String getIssueTypeToDeleteId()
        {
            return issueTypeToDeleteId;
        }

        public Option<String> getAlternativeIssueTypeId()
        {
            return alternativeIssueTypeId;
        }
    }

    class CreateValidationResult extends ServiceResultImpl
    {
        private final Option<IssueTypeCreateInput> issueTypeInput;

        private CreateValidationResult(final ErrorCollection errorCollection, final Option<IssueTypeCreateInput> issueTypeInput)
        {
            super(errorCollection);
            this.issueTypeInput = issueTypeInput;
        }

        public Option<IssueTypeCreateInput> getIssueTypeInput()
        {
            return issueTypeInput;
        }

        public static CreateValidationResult ok(final IssueTypeCreateInput issueTypeCreateInput)
        {
            return new CreateValidationResult(new SimpleErrorCollection(), some(issueTypeCreateInput));
        }

        public static CreateValidationResult error(final ErrorCollection errorCollection)
        {
            return new CreateValidationResult(errorCollection, none(IssueTypeCreateInput.class));
        }
    }

    class UpdateValidationResult extends ServiceResultImpl
    {
        private final Option<IssueTypeUpdateInput> issueTypeInput;
        private final Option<IssueType> issueType;

        private UpdateValidationResult(final ErrorCollection errorCollection, Option<IssueTypeUpdateInput> issueTypeInput, final Option<IssueType> issueType)
        {
            super(errorCollection);
            this.issueTypeInput = issueTypeInput;
            this.issueType = issueType;
        }

        public Option<IssueTypeUpdateInput> getIssueTypeInput()
        {
            return issueTypeInput;
        }

        public Option<IssueType> getIssueType()
        {
            return issueType;
        }

        public static UpdateValidationResult error(final ErrorCollection errorCollection)
        {
            return new UpdateValidationResult(errorCollection, none(IssueTypeUpdateInput.class), none(IssueType.class));
        }

        public static UpdateValidationResult ok(final IssueTypeUpdateInput issueTypeUpdateInput, final IssueType issueType)
        {
            return new UpdateValidationResult(new SimpleErrorCollection(), some(issueTypeUpdateInput), some(issueType));
        }
    }

    class DeleteValidationResult extends ServiceResultImpl
    {
        private final Option<IssueTypeDeleteInput> issueTypeDeleteInput;

        public DeleteValidationResult(final ErrorCollection errorCollection, final Option<IssueTypeDeleteInput> issueTypeId)
        {
            super(errorCollection);
            this.issueTypeDeleteInput = issueTypeId;
        }

        public Option<IssueTypeDeleteInput> getDeleteIssueTypeInput()
        {
            return issueTypeDeleteInput;
        }
    }

    class IssueTypeResult
    {
        private final IssueType issueType;

        public IssueTypeResult(IssueType issueType)
        {
            this.issueType = issueType;
        }

        public IssueType getIssueType()
        {
            return issueType;
        }
    }
}
