package com.atlassian.jira.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

public class SimpleErrorCollection implements ErrorCollection, Serializable
{
    private static final long serialVersionUID = 3974433688611511656L;

    private Map<String, String> errors;
    private List<String> errorMessages;
    private Set<Reason> reasons;

    public SimpleErrorCollection()
    {
        this(Lists.<String>newLinkedList(), Sets.<Reason>newHashSet());
    }

    public SimpleErrorCollection(final String errorMsg, final Reason reason)
    {
        this(Lists.newLinkedList(Lists.newArrayList(errorMsg)), Sets.newHashSet(reason));
    }

    public SimpleErrorCollection(final List<String> errorMessages, final Set<Reason> reasons)
    {
        this(Maps.<String, String>newHashMap(), errorMessages, reasons);
    }

    public SimpleErrorCollection(ErrorCollection errorCollection)
    {
        this(Maps.newHashMap(errorCollection.getErrors()),
                Lists.newLinkedList(errorCollection.getErrorMessages()),
                Sets.newHashSet(errorCollection.getReasons()));
    }

    private SimpleErrorCollection(final Map<String, String> errors, final List<String> errorMessages, final Set<Reason> reasons)
    {
        this.errors = errors;
        this.errorMessages = errorMessages;
        this.reasons = reasons;
    }

    @Override
    public void addError(String field, String message)
    {
        errors.put(field, message);
    }

    @Override
    public void addErrorMessage(String message)
    {
        errorMessages.add(message);
    }

    @Override
    public Collection<String> getErrorMessages()
    {
        return errorMessages;
    }

    @Override
    public void setErrorMessages(Collection<String> errorMessages)
    {
        this.errorMessages = new ArrayList<String>(errorMessages);
    }

    @Override
    public Collection<String> getFlushedErrorMessages()
    {
        Collection<String> errors = getErrorMessages();
        this.errorMessages = new ArrayList<String>();
        return errors;
    }

    @Override
    public Map<String, String> getErrors()
    {
        return errors;
    }

    @Override
    public void addErrorCollection(ErrorCollection errors)
    {
        addErrorMessages(errors.getErrorMessages());
        addErrors(errors.getErrors());
        addReasons(errors.getReasons());
    }

    @Override
    public void addErrorMessages(Collection<String> incomingMessages)
    {
        if (incomingMessages != null && !incomingMessages.isEmpty())
        {
            for (final String incomingMessage : incomingMessages)
            {
                addErrorMessage(incomingMessage);
            }
        }
    }

    @Override
    public void addErrors(Map<String, String> incomingErrors)
    {
        if (incomingErrors == null)
        {
            return;
        }
        for (final Map.Entry<String, String> mapEntry : incomingErrors.entrySet())
        {
            addError(mapEntry.getKey(), mapEntry.getValue());
        }
    }

    @Override
    public boolean hasAnyErrors()
    {
        return (errors != null && !errors.isEmpty()) || (errorMessages != null && !errorMessages.isEmpty()); 
    }

    @Override
    public void addError(String field, String message, Reason reason)
    {
        addError(field, message);
        addReason(reason);
    }

    @Override
    public void addErrorMessage(String message, Reason reason)
    {
        addErrorMessage(message);
        addReason(reason);
    }

    @Override
    public void addReason(Reason reason)
    {
        this.reasons.add(reason);
    }

    @Override
    public void addReasons(Set<Reason> reasons)
    {
        this.reasons.addAll(reasons);
    }

    @Override
    public void setReasons(Set<Reason> reasons)
    {
        this.reasons = reasons;
    }

    @Override
    public Set<Reason> getReasons()
    {
        return reasons;
    }

    public String toString()
    {
        return "Errors: " + getErrors() + "\n" + "Error Messages: " + getErrorMessages();
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        SimpleErrorCollection that = (SimpleErrorCollection) o;

        if (!errorMessages.equals(that.errorMessages)) { return false; }
        if (!errors.equals(that.errors)) { return false; }
        if (!reasons.equals(that.reasons)) { return false; }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = errors.hashCode();
        result = 31 * result + errorMessages.hashCode();
        result = 31 * result + reasons.hashCode();
        return result;
    }
}