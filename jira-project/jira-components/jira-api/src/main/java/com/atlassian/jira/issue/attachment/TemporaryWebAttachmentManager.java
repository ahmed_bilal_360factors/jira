package com.atlassian.jira.issue.attachment;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.annotations.PublicApi;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.AttachmentError;
import com.atlassian.jira.issue.AttachmentsBulkOperationResult;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Manager for temporary web attachments.
 *
 * @since v6.4
 */
@PublicApi
@ParametersAreNonnullByDefault
public interface TemporaryWebAttachmentManager
{
    /**
     * Creates new temporary attachment that may be later converted into real attachment. This attachment will be put to
     * internal store and can be later retrieved using {@link TemporaryWebAttachmentManager#getTemporaryWebAttachment}.
     *
     * @param stream attachment content
     * @param fileName name of attachment file
     * @param contentType content type
     * @param size size of attachment
     * @param target target for attachment - issue or project (in case if temporary attachment will be converted into
     * real attachment during issue creation)
     * @param formToken token representing attachment group (usually a token for a web form)
     * @param user the author of attachment
     * @return created temporary attachment or error if creation failed
     */
    Either<AttachmentError, TemporaryWebAttachment> createTemporaryWebAttachment(final InputStream stream,
            final String fileName, final String contentType, long size, final Either<Issue, Project> target,
            final String formToken, @Nullable final ApplicationUser user);

    /**
     * Gets temporary web attachment by string ID, which was produced by {@link com.atlassian.jira.issue.attachment.TemporaryWebAttachment#getStringId}.
     *
     * @param temporaryAttachmentId string attachment ID
     * @return an option to temporary attachment - it will be empty if attachment was not found
     */
    Option<TemporaryWebAttachment> getTemporaryWebAttachment(String temporaryAttachmentId);

    /**
     * Gets all temporary attachments associated with given form token.
     *
     * @param formToken temporary attachments group token used during temporary attachments creation
     * @return collection of matching {@link com.atlassian.jira.issue.attachment.TemporaryWebAttachment} objects
     */
    Collection<TemporaryWebAttachment> getTemporaryWebAttachmentsByFormToken(String formToken);

    /**
     * Updates existing temporary attachment object to new state. This may be useful to change attachment file name (for
     * example if user may provide custom name).
     *
     * @param temporaryAttachmentId string id of temporary attachment to update
     * @param updated object that holds new values for the temporary attachment
     * @throws java.util.NoSuchElementException
     */
    void updateTemporaryWebAttachment(String temporaryAttachmentId, TemporaryWebAttachment updated);


    /**
     * Converts temporary attachments created by {@link TemporaryWebAttachmentManager#createTemporaryWebAttachment}
     * into real attachments.
     *
     * @param user user which is performing conversion
     * @param issue target issue for new attachments
     * @param temporaryAttachmentsIds temporary attachments string IDs.
     * @return bulk operation result
     */
    AttachmentsBulkOperationResult<ChangeItemBean> convertTemporaryAttachments(
            @Nullable ApplicationUser user, Issue issue, List<String> temporaryAttachmentsIds);

    /**
     * Removes all remaining temporary attachments which were created with given form token.
     *
     * @param formToken form toke used to identify temporary attachments to remove
     */
    void clearTemporaryAttachmentsByFormToken(String formToken);
}
