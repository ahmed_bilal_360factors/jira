package com.atlassian.jira.task.context;

import javax.annotation.Nonnull;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.task.TaskProgressSink;
import com.atlassian.jira.task.context.Context.Task;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.Sized;
import com.atlassian.johnson.event.Event;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Provides static methods for creating {@link Context} instances easily.
 * <p>
 * Contexts provide background tasks with information about the work to be performed, including
 * how the task can report on the task's progress.
 * </p>
 *
 * @since v4.0
 */
@PublicApi
public class Contexts
{
    // Message keys for deprecated reindexing-specific methods
    private static final String REINDEX_PERCENTAGE = "admin.indexing.percent.complete";
    private static final String REINDEX_CURRENT = "admin.indexing.current.index";

    private static final Task NULL_TASK = new Task()
    {
        public void complete()
        {
        }
    };

    private static final Context NULL_CONTEXT = new Context()
    {

        public void setName(final String arg0)
        {
        }

        public Task start(final Object input)
        {
            return NULL_TASK;
        }

        @Override
        public int getNumberOfTasksToCompletion()
        {
            return 0;
        }
    };



    /** A context which ignores all progress reported to it and has no side-effects. */
    public static Context nullContext()
    {
        return NULL_CONTEXT;
    }



    /**
     * @deprecated Since 6.3.6 use {@link #percentageReporter(com.atlassian.jira.util.collect.Sized,
     * TaskProgressSink, I18nHelper, Logger, String,
     * String, String)} for 6.3.6+ or {@link #builder()} for 6.4+
     */
    @Deprecated
    public static Context percentageReporter(@Nonnull final Sized sized, @Nonnull final TaskProgressSink sink,
                                             @Nonnull final I18nHelper i18n, @Nonnull final Logger logger,
                                             @Nonnull final String msg)
    {
        return builder()
                .sized(sized)
                .progress(sink, i18n, REINDEX_PERCENTAGE, REINDEX_CURRENT)
                .log(logger, msg)
                .build();
    }

    /**
     * @deprecated Use {@link #builder()} instead. Since v6.4.
     */
    public static Context percentageReporter(@Nonnull final Sized sized, @Nonnull final TaskProgressSink sink,
                                             @Nonnull final I18nHelper i18n, @Nonnull final Logger logger,
                                             @Nonnull final String msg, @Nonnull final String uiMessageKeyPercentage,
                                             final String uiMessageKeyCurrent)
    {
        return builder()
                .sized(sized)
                .progress(sink, i18n, uiMessageKeyPercentage, uiMessageKeyCurrent)
                .log(logger, msg)
                .build();
    }

    /**
     * @deprecated Use {@link #builder()} instead. Since v6.4.
     */
    public static Context percentageReporter(@Nonnull final Sized sized, @Nonnull final TaskProgressSink sink,
                                             @Nonnull final I18nHelper i18n, @Nonnull final Logger logger,
                                             @Nonnull final String msg, @Nonnull final String uiMessageKeyPercentage,
                                             final String uiMessageKeyCurrent, @Nonnull Level level)
    {
        return builder()
                .sized(sized)
                .progress(sink, i18n, uiMessageKeyPercentage, uiMessageKeyCurrent)
                .log(logger, msg, level)
                .build();
    }

    /**
     * @deprecated Since 6.3.6 use {@link #percentageReporter(com.atlassian.jira.util.collect.Sized,
     * TaskProgressSink, I18nHelper, Logger, String,
     * String, String, com.atlassian.johnson.event.Event)} in 6.3.6+ or {@link #builder()} in 6.4+
     */
    @Deprecated
    public static Context percentageReporter(@Nonnull final Sized sized, @Nonnull final TaskProgressSink sink,
                                             @Nonnull final I18nHelper i18n, @Nonnull final Logger logger,
                                             @Nonnull final String msg, @Nonnull final Event event)
    {
        return percentageReporter(sized, sink, i18n, logger, msg, "admin.indexing.percent.complete",
                "admin.indexing.current.index", event);
    }

   /**
     * @deprecated Since 6.3.6 use {@link #percentageReporter(com.atlassian.jira.util.collect.Sized,
     * TaskProgressSink, I18nHelper, Logger, String,
     * String, String, com.atlassian.johnson.event.Event), Level} in 6.3.6+ or {@link #builder()} in 6.4+
     */
    @Deprecated
    public static Context percentageReporter(@Nonnull final Sized sized, @Nonnull final TaskProgressSink sink,
                                             @Nonnull final I18nHelper i18n, @Nonnull final Logger logger,
                                             @Nonnull final String msg, @Nonnull final Event event, Level level)
    {
        return builder()
                .sized(sized)
                .event(event)
                .progress(sink, i18n, REINDEX_PERCENTAGE, REINDEX_CURRENT)
                .log(logger, msg, level)
                .build();
    }

    public static Context percentageReporter(@Nonnull final Sized sized, @Nonnull final TaskProgressSink sink,
                                             @Nonnull final I18nHelper i18n, @Nonnull final Logger logger,
                                             @Nonnull final String msg, @Nonnull final String uiMessageKeyPercentage,
                                             final String uiMessageKeyCurrent, @Nonnull final Event event)
    {
        return builder()
                .sized(sized)
                .event(event)
                .progress(sink, i18n, uiMessageKeyPercentage, uiMessageKeyCurrent)
                .log(logger, msg)
                .build();
    }

    /**
     * @deprecated Use {@link #builder()} instead. Since v6.4.
     */
    @Deprecated
    public static Context percentageReporter(@Nonnull final Sized sized, @Nonnull final TaskProgressSink sink,
                                             @Nonnull final I18nHelper i18n, @Nonnull final Logger logger,
                                             @Nonnull final String msg, @Nonnull final String uiMessageKeyPercentage,
                                             final String uiMessageKeyCurrent, @Nonnull final Event event, Level level)
    {
        return builder()
                .sized(sized)
                .event(event)
                .progress(sink, i18n, uiMessageKeyPercentage, uiMessageKeyCurrent)
                .log(logger, msg, level)
                .build();
    }

    /**
     * @deprecated Use {@link #builder()} instead. Since v6.4.
     */
    @Deprecated
    public static Context percentageLogger(@Nonnull final Sized sized, @Nonnull final Logger logger,
                                           @Nonnull final String msg)
    {
        return builder()
                .sized(sized)
                .log(logger, msg)
                .build();
    }

    /**
     * @deprecated Use {@link #builder()} instead. Since v6.4.
     */
    @Deprecated
    public static Context percentageLogger(@Nonnull final Sized sized, @Nonnull final Logger logger,
                                           @Nonnull final String msg, Level level)
    {
        return builder()
                .sized(sized)
                .log(logger, msg, level)
                .build();
    }

    private Contexts()
    {
        throw new AssertionError("cannot instantiate!");
    }

    /**
     * Returns a new {@link com.atlassian.jira.task.context.Context.Builder} for constructing a background task {@code Context}.
     *
     * @return a new {@link com.atlassian.jira.task.context.Context.Builder} for constructing a background task {@code Context}.
     * @since v6.4
     */
    public static Context.Builder builder()
    {
        return new Context.Builder();
    }

}
