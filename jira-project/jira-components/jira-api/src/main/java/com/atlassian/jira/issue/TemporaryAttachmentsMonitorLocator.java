package com.atlassian.jira.issue;

import javax.annotation.Nullable;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.web.action.issue.TemporaryAttachmentsMonitor;

/**
 * Locator to abstract how we obtain the TemporaryAttachmentsMonitor.  Implementations should store one of these
 * monitors per user.
 *
 * @since v4.2
 * @deprecated Use {@link com.atlassian.jira.issue.attachment.TemporaryWebAttachmentManager} instead. Since v6.4
 */
@PublicApi
@Deprecated
public interface TemporaryAttachmentsMonitorLocator
{
    /**
     * Returns the current temporary attachmentsMonitor.  Creates a new one if specified when none exists yet.
     *
     * @param create Should this call create a new monitor if none exists yet
     * @return The current monitor or null.
     */
    @Nullable
    TemporaryAttachmentsMonitor get(boolean create);
}
