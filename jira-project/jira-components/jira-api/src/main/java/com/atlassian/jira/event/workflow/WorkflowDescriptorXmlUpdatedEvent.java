package com.atlassian.jira.event.workflow;

import javax.annotation.Nonnull;

import com.atlassian.annotations.Internal;

/**
 * Event indicating that a workflow descriptor xml was updated.  This may be used in rare cases during upgrade tasks to
 * indicate that descriptor caches may need refreshing for example.
 *
 * @since v6.4
 */
@Internal
public class WorkflowDescriptorXmlUpdatedEvent
{
    private final String workflowName;

    public WorkflowDescriptorXmlUpdatedEvent(@Nonnull String workflowName)
    {
        this.workflowName = workflowName;
    }

    public String getWorkflowName()
    {
        return workflowName;
    }
}
