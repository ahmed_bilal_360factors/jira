package com.atlassian.jira.issue.index.indexers.impl;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.DoubleConverter;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.web.FieldVisibilityManager;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.NumericField;
import org.apache.lucene.util.NumericUtils;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * A simple custom field indexer for the number custom fields
 *
 * @since v4.0
 */
public class NumberCustomFieldIndexer extends AbstractCustomFieldIndexer
{
    private final CustomField customField;
    private final DoubleConverter doubleConverter;

    public NumberCustomFieldIndexer(final FieldVisibilityManager fieldVisibilityManager, final CustomField customField, final DoubleConverter doubleConverter)
    {
        super(fieldVisibilityManager, notNull("customField", customField));
        this.doubleConverter = notNull("doubleConverter", doubleConverter);
        this.customField = customField;
    }

    public void addDocumentFieldsSearchable(final Document doc, final Issue issue)
    {
        addDocumentFields(doc, issue, true);
    }

    public void addDocumentFieldsNotSearchable(final Document doc, final Issue issue)
    {
        addDocumentFields(doc, issue, false);
    }

    private void addDocumentFields(final Document doc, final Issue issue, final boolean searchable)
    {
        Object value = customField.getValue(issue);
        if (value != null)
        {
            final String string = doubleConverter.getStringForLucene((Double) value);
            doc.add(new Field(getDocumentFieldId(), string, Field.Store.YES,
                    searchable ? Field.Index.NOT_ANALYZED_NO_NORMS : Field.Index.NO));
        }
        if (searchable)
        {
            // We store this field twice in the index, because historically JIRA indexed the number in a format that did not sort negative numbers correctly.
            // We now index the field using the properly encoded format to allow sorting. To keep backward compatibility with third party plugins we retain
            // the old stored value and also index the old value, although these plugins should migrate to using NumberCustomFieldClauseQueryFactory.
            final String encoded = NumericUtils.doubleToPrefixCoded(value != null ? (Double) value : Double.MAX_VALUE);
            doc.add(new Field(DocumentConstants.LUCENE_SORTFIELD_PREFIX + getDocumentFieldId(), encoded, Field.Store.NO, Field.Index.NOT_ANALYZED_NO_NORMS));
        }
    }
}