package com.atlassian.jira.issue.customfields.impl;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.customfields.TextCustomFieldType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.FieldJsonRepresentation;
import com.atlassian.jira.issue.fields.rest.FieldTypeInfo;
import com.atlassian.jira.issue.fields.rest.FieldTypeInfoContext;
import com.atlassian.jira.issue.fields.rest.RestAwareCustomFieldType;
import com.atlassian.jira.issue.fields.rest.json.JsonData;
import com.atlassian.jira.issue.fields.rest.json.JsonType;
import com.atlassian.jira.issue.fields.rest.json.JsonTypeBuilder;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;

import java.awt.*;
import javax.annotation.Nonnull;

import javax.annotation.Nullable;

/**
 * @deprecated Use {@link GenericTextCFType} instead. Since v5.0.
 */
@Deprecated
@PublicSpi
public abstract class StringCFType extends AbstractSingleFieldType implements RestAwareCustomFieldType, TextCustomFieldType
{
    private final TextFieldCharacterLengthValidator textFieldCharacterLengthValidator;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    /**
     * Constructor.
     * @deprecated Use {@link #StringCFType(com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister, com.atlassian.jira.issue.customfields.manager.GenericConfigManager, com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator, com.atlassian.jira.security.JiraAuthenticationContext)} instead. Since v6.4.1
     */
    @Deprecated
    public StringCFType(final CustomFieldValuePersister customFieldValuePersister, final GenericConfigManager genericConfigManager)
    {
        this(customFieldValuePersister, genericConfigManager,
                ComponentAccessor.getComponent(TextFieldCharacterLengthValidator.class), ComponentAccessor.getJiraAuthenticationContext());
    }

    public StringCFType(final CustomFieldValuePersister customFieldValuePersister, final GenericConfigManager genericConfigManager,
            final TextFieldCharacterLengthValidator textFieldCharacterLengthValidator, JiraAuthenticationContext jiraAuthenticationContext)
    {
        super(customFieldValuePersister, genericConfigManager);
        this.textFieldCharacterLengthValidator = textFieldCharacterLengthValidator;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    protected Object getDbValueFromObject(final Object customFieldObject)
    {
        return getStringFromSingularObject(customFieldObject);
    }

    @Override
    protected Object getObjectFromDbValue(@Nonnull final Object databaseValue) throws FieldValidationException
    {
        return getSingularObjectFromString((String) databaseValue);
    }

    @Override
    public String getValueFromCustomFieldParams(final CustomFieldParams relevantParams) throws FieldValidationException
    {
        final String value = (String) super.getValueFromCustomFieldParams(relevantParams);
        if (textFieldCharacterLengthValidator.isTextTooLong(value))
        {
            final I18nHelper i18nBean = jiraAuthenticationContext.getI18nHelper();
            final long maximumNumberOfCharacters = textFieldCharacterLengthValidator.getMaximumNumberOfCharacters();
            throw new FieldValidationException(i18nBean.getText("field.error.text.toolong", maximumNumberOfCharacters));
        }
        else
        {
            return value;
        }
    }

    @Override
    public Object accept(VisitorBase visitor)
    {
        if (visitor instanceof Visitor)
        {
            return ((Visitor) visitor).visitString(this);
        }

        return super.accept(visitor);
    }

    public interface Visitor<T> extends VisitorBase<T>
    {
        T visitString(StringCFType stringCustomFieldType);
    }

    @Override
    public FieldTypeInfo getFieldTypeInfo(FieldTypeInfoContext fieldTypeInfoContext)
    {
        return new FieldTypeInfo(null, null);
    }

    @Override
    public JsonType getJsonSchema(CustomField customField)
    {
        return JsonTypeBuilder.custom(JsonType.STRING_TYPE, getKey(), customField.getIdAsLong());
    }

    @Override
    public FieldJsonRepresentation getJsonFromIssue(CustomField field, Issue issue, boolean renderedVersionRequested, @Nullable FieldLayoutItem fieldLayoutItem)
    {
        Object value = getValueFromIssue(field, issue);
        FieldJsonRepresentation bean = new FieldJsonRepresentation(new JsonData(value));

        if (field.isRenderable() && renderedVersionRequested && fieldLayoutItem != null)
        {
            final String content = ComponentAccessor.getComponent(RendererManager.class).getRenderedContent(fieldLayoutItem, issue);
            bean.setRenderedData(new JsonData(content));
        }

        return bean;
    }
}
