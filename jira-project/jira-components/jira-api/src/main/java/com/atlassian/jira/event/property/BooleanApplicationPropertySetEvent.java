package com.atlassian.jira.event.property;

import com.atlassian.annotations.ExperimentalApi;

@ExperimentalApi
public class BooleanApplicationPropertySetEvent extends AbstractApplicationPropertySetEvent<Boolean>
{
    public BooleanApplicationPropertySetEvent(final String applicationPropertyKey, final Boolean value)
    {
        super(applicationPropertyKey, value);
    }
}
