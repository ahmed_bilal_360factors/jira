package com.atlassian.jira.issue.index;

import javax.annotation.Nullable;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.config.ReindexMessage;

/**
 * Event raised when a "reindex all" event is beginning. This is the event triggered when e.g. the
 * admin clicks reindex or new data is imported.
 *
 * This Class should not be constructed by consumers of the API.
 * @since v5.0
 */
@PublicApi
@EventName ("reindex.all.started")
public class ReindexAllStartedEvent implements IndexEvent
{
    private final boolean useBackgroundIndexing;
    private final boolean updateReplicatedIndex;
    private final IssueIndexingParams issueIndexingParams;
    private final String reason;

    // added to keep the APICheck happy
    @SuppressWarnings("unused")
    @Deprecated
    public ReindexAllStartedEvent()
    {
        this(false, false);
    }

    @Deprecated
    public ReindexAllStartedEvent(final boolean useBackgroundIndexing, final boolean updateReplicatedIndex)
    {
        this(useBackgroundIndexing, updateReplicatedIndex, IssueIndexingParams.INDEX_ALL, null);
    }

    @Internal
    public ReindexAllStartedEvent(final boolean useBackgroundIndexing, final boolean updateReplicatedIndex, IssueIndexingParams issueIndexingParams, @Nullable ReindexMessage message)
    {
        this.useBackgroundIndexing = useBackgroundIndexing;
        this.updateReplicatedIndex = updateReplicatedIndex;
        this.issueIndexingParams = issueIndexingParams;
        this.reason = message != null ? message.getI18nTaskKey() : null;
    }

    public boolean isUsingBackgroundIndexing()
    {
        return useBackgroundIndexing;
    }

    public boolean shouldUpdateReplicatedIndex()
    {
        return updateReplicatedIndex;
    }

    public IssueIndexingParams getIssueIndexingParams()
    {
        return issueIndexingParams;
    }

    @SuppressWarnings("unused")
    public boolean isIndexIssues() {return issueIndexingParams.isIndexIssues();}

    @SuppressWarnings("unused")
    public boolean isIndexChangeHistory() {return issueIndexingParams.isIndexChangeHistory();}

    @SuppressWarnings("unused")
    public boolean isIndexComments() {return issueIndexingParams.isIndexComments();}

    @SuppressWarnings("unused")
    public boolean isIndexWorklogs() {return issueIndexingParams.isIndexWorklogs();}

    public String getReason()
    {
        return reason;
    }
}
