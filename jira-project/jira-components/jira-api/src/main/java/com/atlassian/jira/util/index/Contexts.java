package com.atlassian.jira.util.index;

import javax.annotation.Nonnull;

import com.atlassian.jira.task.TaskProgressSink;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.Sized;
import com.atlassian.johnson.event.Event;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import static com.atlassian.jira.task.context.Context.Builder;

/**
 * Provides static methods for creating {@link Context} instances easily.
 * <p>
 * These contexts are always specific to reindexing tasks.  See {@link com.atlassian.jira.task.context.Contexts}
 * for a more general-purpose context factory.
 * </p>
 *
 * @since v3.13
 * @see com.atlassian.jira.task.context.Contexts
 */
public class Contexts
{
    private static final String REINDEX_PERCENTAGE = "admin.indexing.percent.complete";
    private static final String REINDEX_CURRENT = "admin.indexing.current.index";
    private static final String REINDEXING = "Re-indexing is {0}% complete. Current index: {1}";

    public static Context percentageLogger(final Sized sized, final Logger logger)
    {
        return builder()
                .sized(sized)
                .log(logger, REINDEXING, Level.INFO)
                .build();
    }

    public static Context percentageReporter(@Nonnull final Sized sized, @Nonnull final TaskProgressSink sink, @Nonnull final I18nHelper i18n, @Nonnull final Logger logger)
    {
        return builder()
                .sized(sized)
                .progress(sink, i18n, REINDEX_PERCENTAGE, REINDEX_CURRENT)
                .log(logger, REINDEXING, Level.INFO)
                .build();
    }

    public static Context percentageReporter(@Nonnull final Sized sized, @Nonnull final TaskProgressSink sink, @Nonnull final I18nHelper i18n, @Nonnull final Logger logger, @Nonnull final Event event)
    {
        return builder()
                .sized(sized)
                .event(event)
                .progress(sink, i18n, REINDEX_PERCENTAGE, REINDEX_CURRENT)
                .log(logger, REINDEXING, Level.INFO)
                .build();
    }

    private static Builder builder()
    {
        return com.atlassian.jira.task.context.Contexts.builder();
    }

    /**
     * @return a do nothing context
     * @deprecated use {@link com.atlassian.jira.task.context.Contexts#nullContext()} instead
     */
    @Deprecated
    public static Context nullContext()
    {
        return com.atlassian.jira.task.context.Contexts.nullContext();
    }
}
