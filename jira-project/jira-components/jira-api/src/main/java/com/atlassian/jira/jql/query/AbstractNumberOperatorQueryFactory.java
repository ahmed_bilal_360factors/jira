package com.atlassian.jira.jql.query;

import java.util.List;

import com.atlassian.jira.issue.customfields.converters.DoubleConverter;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.util.IndexValueConverter;

import com.google.common.collect.Lists;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Base class for Indexed Value clauses query factories
 *
 * @since v6.4
 */
abstract class AbstractNumberOperatorQueryFactory
{
    private final DoubleConverter doubleConverter;

    public AbstractNumberOperatorQueryFactory(DoubleConverter doubleConverter)
    {
        this.doubleConverter = notNull("doubleConverter", doubleConverter);
    }

    /**
     * @param rawValues the raw values to convert
     * @return a list of index values in String form; never null, but may contain null values if empty literals were passed in.
     */
    List<Double> getIndexValues(List<QueryLiteral> rawValues)
    {
        final List<Double> values = Lists.newArrayListWithCapacity(rawValues.size());
        for (QueryLiteral rawValue : rawValues)
        {
            if (rawValue.isEmpty())
            {
                values.add(null);
            }
            else
            {
                final Double indexValue = doubleConverter.getDouble(rawValue.asString());
                if (indexValue != null)
                {
                    values.add(indexValue);
                }
            }
        }
        return values;
    }
}