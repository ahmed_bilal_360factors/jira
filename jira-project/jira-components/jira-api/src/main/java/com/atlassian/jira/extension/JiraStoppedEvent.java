package com.atlassian.jira.extension;

/**
 * An event published when JIRA is shutting down.
 *
 * @since v6.4
 */
public class JiraStoppedEvent
{
}
