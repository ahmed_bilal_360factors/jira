package com.atlassian.jira.event;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.event.AbstractEvent;

/**
 * Published when the index recovery setting is saved indicating if index recovery was enabled or disabled
 */
@ExperimentalApi
@EventName ("ha.index.recovery.enabled")
public final class IndexRecoveryEnabledEvent extends AbstractEvent
{
    /**
     * The state of the event true for enabled and false for disabled
     */
    private final boolean recoveryEnabled;

    public IndexRecoveryEnabledEvent(final boolean recoveryEnabled)
    {
        this.recoveryEnabled = recoveryEnabled;
    }

    public boolean isRecoveryEnabled()
    {
        return recoveryEnabled;
    }
}
