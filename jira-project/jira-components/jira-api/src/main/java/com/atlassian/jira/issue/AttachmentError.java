package com.atlassian.jira.issue;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.fugue.Option;
import com.atlassian.jira.util.ErrorCollection;

import com.google.common.base.Objects;

/**
 * Holds error details for attachment action.
 *
 * @since v6.4
 */
@ParametersAreNonnullByDefault
public class AttachmentError
{
    private final String logMessage;
    private final String localizedMessage;
    private final String filename;
    private final ErrorCollection.Reason reason;
    private final Option<Exception> exception;

    public AttachmentError(final String logMessage, final String localizedMessage, final String filename,
            final Option<Exception> exception, final ErrorCollection.Reason reason)
    {
        this.logMessage = logMessage;
        this.localizedMessage = localizedMessage;
        this.filename = filename;
        this.reason = reason;
        this.exception = exception;
    }

    public AttachmentError(final String logMessage, final String localizedMessage, final String filename,
            final ErrorCollection.Reason reason)
    {
        this(logMessage, localizedMessage, filename, Option.<Exception>none(), reason);
    }

    public String getFilename()
    {
        return filename;
    }

    public String getLogMessage()
    {
        return logMessage;
    }

    public String getLocalizedMessage()
    {
        return localizedMessage;
    }

    public Option<Exception> getException()
    {
        return exception;
    }

    public ErrorCollection.Reason getReason() { return reason; }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(logMessage, localizedMessage, filename, reason, exception);
    }

    @Override
    public boolean equals(@Nullable final Object obj)
    {
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()) {return false;}

        final AttachmentError other = (AttachmentError) obj;
        return Objects.equal(this.logMessage, other.logMessage)
                && Objects.equal(this.localizedMessage, other.localizedMessage)
                && Objects.equal(this.filename, other.filename)
                && Objects.equal(this.reason, other.reason)
                && Objects.equal(this.exception, other.exception);
    }

    @Override
    public String toString()
    {
        return "AttachmentError{" +
                "logMessage='" + logMessage + '\'' +
                ", localizedMessage='" + localizedMessage + '\'' +
                ", filename='" + filename + '\'' +
                ", reason=" + reason +
                ", exception=" + exception +
                '}';
    }
}
