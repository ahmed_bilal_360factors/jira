package com.atlassian.jira.cluster;

import javax.annotation.Nullable;

import com.atlassian.annotations.PublicApi;

/**
 * Provides basic information about whether or not JIRA is clustered, the identification of
 * the current node, and so on.
 *
 * @since v6.4.5
 */
@PublicApi
public interface ClusterInfo
{
    /**
     * Provides the node ID for this particular cluster node.
     *
     * @return the current node's unique identifier, or {@code null} if this is not a clustered environment
     *         ({@link #isClustered()} will return false).
     */
    @Nullable
    String getNodeId();

    /**
     * Returns whether or not this is JIRA instance is configured as a node in a JIRA Data Center cluster.
     *
     * @return {@code true} if this is a clustered JIRA instance; {@code false} if this JIRA instance is
     *          not part of a JIRA Data Center cluster.
     */
    boolean isClustered();
}
