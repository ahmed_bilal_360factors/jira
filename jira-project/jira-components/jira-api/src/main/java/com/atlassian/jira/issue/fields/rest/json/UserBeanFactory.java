package com.atlassian.jira.issue.fields.rest.json;

import java.util.Collection;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.UserJsonBean;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.EmailFormatter;

/**
 * This provides a simple, dependency-free, straight forward API to generating the JSON corresponding to a User.
 * @since v5.2
 */
@ExperimentalApi
public interface UserBeanFactory
{
    /**
     * Generate a bean suitable for serialisation by Jackon into JSON.
     * @param createdUser Create UserJsonBean for this user
     * @deprecated Use {@link #createBean(com.atlassian.jira.user.ApplicationUser, com.atlassian.jira.user.ApplicationUser)}
     */
    @Deprecated
    UserJsonBean createBean(User createdUser);

    /**
     * Generate a bean suitable for serialisation by Jackon into JSON for given user in the context of loggedInUser.
     * @param createdUser Create UserJsonBean for createdUser
     * @param loggedInUser UserJsonBean will be created in the context of loggedInUser (i.e. escape/hide email address if necessary)
     * @deprecated use {@link #createBean(com.atlassian.jira.user.ApplicationUser, com.atlassian.jira.user.ApplicationUser)}
     */
    @Deprecated
    UserJsonBean createBean(User createdUser, ApplicationUser loggedInUser);

    /**
     * Generate a bean suitable for serialisation by Jackon into JSON for given user in the context of loggedInUser.
     * @param createdUser Create UserJsonBean for createdUser
     * @param loggedInUser UserJsonBean will be created in the context of loggedInUser (i.e. escape/hide email address if necessary)
     * @param jiraBaseUrls JiraBaseUrls
     * @param emailFormatter EmailFormatter
     * @param timeZoneManager TimeZoneManager
     * @deprecated use {@link #createBean(com.atlassian.jira.user.ApplicationUser, com.atlassian.jira.user.ApplicationUser, com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls, com.atlassian.jira.util.EmailFormatter, com.atlassian.jira.timezone.TimeZoneManager)}
     */
    @Deprecated
    UserJsonBean createBean(final User createdUser, final ApplicationUser loggedInUser, final JiraBaseUrls jiraBaseUrls, final EmailFormatter emailFormatter, final TimeZoneManager timeZoneManager);

    /**
     * Generate a bean suitable for serialisation by Jackon into JSON for given user in the context of loggedInUser.
     * @param createdUser Create UserJsonBean for createdUser
     * @param loggedInUser UserJsonBean will be created in the context of loggedInUser (i.e. escape/hide email address if necessary)
     */
    UserJsonBean createBean(ApplicationUser createdUser, ApplicationUser loggedInUser);

    /**
     * Generate a bean suitable for serialisation by Jackon into JSON for given user in the context of loggedInUser.
     * @param createdUser Create UserJsonBean for createdUser
     * @param loggedInUser UserJsonBean will be created in the context of loggedInUser (i.e. escape/hide email address if necessary)
     * @param jiraBaseUrls JiraBaseUrls
     * @param emailFormatter EmailFormatter
     * @param timeZoneManager TimeZoneManager
     */
    UserJsonBean createBean(final ApplicationUser createdUser, final ApplicationUser loggedInUser, final JiraBaseUrls jiraBaseUrls, final EmailFormatter emailFormatter, final TimeZoneManager timeZoneManager);

    /**
     * Generate a bean suitable for serialisation by Jackon into JSON for given user in the context of loggedInUser.
     * @param createdUsers Create UserJsonBeans for createdUsers
     * @param loggedInUser UserJsonBean will be created in the context of loggedInUser (i.e. escape/hide email address if necessary)
     */
    Collection<UserJsonBean> createBeanCollection(final Collection<ApplicationUser> createdUsers, final ApplicationUser loggedInUser);

    /**
     * Generate a bean suitable for serialisation by Jackon into JSON for given user in the context of loggedInUser.
     * @param createdUsers Create UserJsonBeans for createdUsers
     * @param loggedInUser UserJsonBean will be created in the context of loggedInUser (i.e. escape/hide email address if necessary)
     * @param jiraBaseUrls JiraBaseUrls
     * @param emailFormatter EmailFormatter
     * @param timeZoneManager TimeZoneManager
     */
    Collection<UserJsonBean> createBeanCollection(final Collection<ApplicationUser> createdUsers, final ApplicationUser loggedInUser, final JiraBaseUrls jiraBaseUrls, final EmailFormatter emailFormatter, final TimeZoneManager timeZoneManager);

}
