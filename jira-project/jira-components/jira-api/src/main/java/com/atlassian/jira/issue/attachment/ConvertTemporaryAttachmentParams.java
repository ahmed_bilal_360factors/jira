package com.atlassian.jira.issue.attachment;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;

import com.google.common.base.Objects;

import org.joda.time.DateTime;

/**
 * Values holder for new attachment (created from the temporary attachment).
 * <p/>
 * Use {@link com.atlassian.jira.issue.attachment.ConvertTemporaryAttachmentParamsBuilder} to
 * instantiate this class.
 *
 * @since v6.4
 */
@ParametersAreNonnullByDefault
public class ConvertTemporaryAttachmentParams
{
    public static ConvertTemporaryAttachmentParamsBuilder builder()
    {
        return new ConvertTemporaryAttachmentParamsBuilder();
    }

    @Nullable
    private final ApplicationUser author;
    private final Issue issue;
    private final TemporaryAttachmentId temporaryAttachmentId;
    private final String filename;
    private final String contentType;
    private final long fileSize;
    private final DateTime createdTime;

    ConvertTemporaryAttachmentParams(@Nullable final ApplicationUser author, final Issue issue,
            final TemporaryAttachmentId temporaryAttachmentId, final String filename, final String contentType,
            final long fileSize, final DateTime createdTime)
    {
        this.author = author;
        this.issue = issue;
        this.temporaryAttachmentId = temporaryAttachmentId;
        this.filename = filename;
        this.contentType = contentType;
        this.fileSize = fileSize;
        this.createdTime = createdTime;
    }

    @Nullable
    public ApplicationUser getAuthor()
    {
        return author;
    }

    public Issue getIssue()
    {
        return issue;
    }

    public TemporaryAttachmentId getTemporaryAttachmentId()
    {
        return temporaryAttachmentId;
    }

    public String getFilename()
    {
        return filename;
    }

    public String getContentType()
    {
        return contentType;
    }

    public long getFileSize()
    {
        return fileSize;
    }

    public DateTime getCreatedTime()
    {
        return createdTime;
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(author, issue, temporaryAttachmentId, filename, contentType, fileSize, createdTime);
    }

    @Override
    public boolean equals(@Nullable final Object obj)
    {
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()) {return false;}
        final ConvertTemporaryAttachmentParams other = (ConvertTemporaryAttachmentParams) obj;
        return Objects.equal(this.author, other.author)
                && Objects.equal(this.issue, other.issue)
                && Objects.equal(this.temporaryAttachmentId, other.temporaryAttachmentId)
                && Objects.equal(this.filename, other.filename)
                && Objects.equal(this.contentType, other.contentType)
                && Objects.equal(this.fileSize, other.fileSize)
                && Objects.equal(this.createdTime, other.createdTime);
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this)
                .add("author", author)
                .add("issue", issue)
                .add("temporaryAttachmentId", temporaryAttachmentId)
                .add("filename", filename)
                .add("contentType", contentType)
                .add("fileSize", fileSize)
                .add("createdTime", createdTime)
                .toString();
    }
}
