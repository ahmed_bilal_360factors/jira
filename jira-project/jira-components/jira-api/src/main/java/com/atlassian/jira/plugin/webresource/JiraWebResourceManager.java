package com.atlassian.jira.plugin.webresource;

import com.atlassian.annotations.PublicApi;
import com.atlassian.plugin.webresource.WebResourceManager;

import java.util.Map;

/**
 * Add some additional methods to the stock WebResourceManager that we can use in JIRA.
 *
 * @since v5.0
 */
@PublicApi
public interface JiraWebResourceManager extends WebResourceManager
{
    /**
     * Adds key-value String pairs to a map to be rendered later.
     *
     * @param key a unique key to store the value against
     * @param value an HTML-safe string
     *
     * @return true if metadata added to map successfully
     * @since v5.0
     * @deprecated Use {@link com.atlassian.webresource.api.data.WebResourceDataProvider} instead. Since v6.4.
     */
    boolean putMetadata(String key, String value);

    /**
     * Returns the map of key-value pairs added via {@link #putMetadata(String, String)}.
     *
     * Prior to v6.4, multiple invocations weren't supported, and only the first call would return metadata. Subsequent
     * calls would return an empty Map. Since v6.4, multiple invocations of this method are supported. Each invocation
     * drains and returns the metadata added since the last invocation.
     *
     * @return the map of key-value pairs added via {@link #putMetadata(String, String)}.
     * @since v5.0
     * @deprecated You should generally never need to use this API, instead you should rely on the page builder service
     *     delivering data to the client side and exposing it via the {@code WRM.data.claim} JavaScript API.
     */
    Map<String, String> getMetadata();
}
