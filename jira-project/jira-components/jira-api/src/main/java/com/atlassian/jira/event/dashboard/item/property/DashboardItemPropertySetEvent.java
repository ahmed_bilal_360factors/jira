package com.atlassian.jira.event.dashboard.item.property;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.event.entity.AbstractPropertyEvent;
import com.atlassian.jira.event.entity.EntityPropertySetEvent;
import com.atlassian.jira.user.ApplicationUser;

@PublicApi
public final class DashboardItemPropertySetEvent extends AbstractPropertyEvent implements EntityPropertySetEvent
{
    public DashboardItemPropertySetEvent(final EntityProperty entityProperty, final ApplicationUser user)
    {
        super(entityProperty, user);
    }
}
