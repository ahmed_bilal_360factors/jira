package com.atlassian.jira.util;

public class PathTraversalRuntimeException extends RuntimeException
{
    public PathTraversalRuntimeException(final String message)
    {
        super(message);
    }

    public PathTraversalRuntimeException() {}
}
