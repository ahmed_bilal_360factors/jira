package com.atlassian.jira.issue.fields.rest.json;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.Nullable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.UserJsonBean;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.util.JiraUrlCodec;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;

/**
 * @since v5.1
 */
public class DefaultUserBeanFactory implements UserBeanFactory
{
    private final JiraBaseUrls jiraBaseUrls;
    private final EmailFormatter emailFormatter;
    private final TimeZoneManager timeZoneManager;

    /**
     * @deprecated Use {@link #DefaultUserBeanFactory(com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls, com.atlassian.jira.util.EmailFormatter, com.atlassian.jira.timezone.TimeZoneManager))}
     */
    @Deprecated
    public DefaultUserBeanFactory(JiraBaseUrls jiraBaseUrls)
    {
        this(jiraBaseUrls, ComponentAccessor.getComponent(EmailFormatter.class), ComponentAccessor.getComponent(TimeZoneManager.class));
    }

    /**
     * @deprecated Use {@link #DefaultUserBeanFactory(com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls, com.atlassian.jira.util.EmailFormatter, com.atlassian.jira.timezone.TimeZoneManager))}
     */
    @Deprecated
    public DefaultUserBeanFactory(JiraBaseUrls jiraBaseUrls, final EmailFormatter emailFormatter)
    {
        this(jiraBaseUrls, emailFormatter, ComponentAccessor.getComponent(TimeZoneManager.class));
    }

    public DefaultUserBeanFactory(JiraBaseUrls jiraBaseUrls, final EmailFormatter emailFormatter, final TimeZoneManager timeZoneManager)
    {
        this.jiraBaseUrls = jiraBaseUrls;
        this.emailFormatter = emailFormatter;
        this.timeZoneManager = timeZoneManager;
    }

    private static Map<String, String> getAvatarURLs(ApplicationUser user)
    {
        final AvatarService avatarService = ComponentAccessor.getAvatarService();

        final Map<String, String> avatarUrls = new HashMap<String, String>();
        for (Avatar.Size size : Avatar.Size.values())
        {
            final int px = size.getPixels();
            if (px > 48) continue; // TODO JRADEV-20790 - Don't output higher res URLs in our REST endpoints until we start using them ourselves.
            final String sizeName = String.format("%dx%d",px,px);
            // TODO this should be a URI reference and not an absolute URL
            avatarUrls.put(sizeName, avatarService.getAvatarAbsoluteURL(user, user, size).toString());
        }
        return avatarUrls;
    }

    /**
     * @deprecated Use {@link #createBean(com.atlassian.jira.user.ApplicationUser, com.atlassian.jira.user.ApplicationUser)}
     */
    @Override
    @Deprecated
    public UserJsonBean createBean(User createdUser)
    {
        final ApplicationUser applicationUser = ApplicationUsers.from(createdUser);
        return createBean(applicationUser, ComponentAccessor.getComponent(JiraAuthenticationContext.class).getUser());
    }

    /**
     * @deprecated Use {@link #createBean(com.atlassian.jira.user.ApplicationUser, com.atlassian.jira.user.ApplicationUser)}
     */
    @Override
    @Deprecated
    public UserJsonBean createBean(User createdUser, final ApplicationUser loggedInUser)
    {
        final ApplicationUser applicationUser = ApplicationUsers.from(createdUser);
        return createBean(applicationUser, loggedInUser);
    }

    @Override
    public UserJsonBean createBean(final User createdUser, final ApplicationUser loggedInUser, final JiraBaseUrls jiraBaseUrls, final EmailFormatter emailFormatter, final TimeZoneManager timeZoneManager)
    {
        final ApplicationUser applicationUser = ApplicationUsers.from(createdUser);
        return createBean(applicationUser, loggedInUser, jiraBaseUrls, emailFormatter, timeZoneManager);
    }

    @Override
    public UserJsonBean createBean(final ApplicationUser createdUser, final ApplicationUser loggedInUser)
    {
        return createBean(createdUser, loggedInUser, jiraBaseUrls, emailFormatter, timeZoneManager);
    }

    @Override
    public UserJsonBean createBean(final ApplicationUser createdUser, final ApplicationUser loggedInUser, final JiraBaseUrls jiraBaseUrls, final EmailFormatter emailFormatter, final TimeZoneManager timeZoneManager)
    {
        if (createdUser == null)
        {
            return null;
        }
        TimeZone timeZone = timeZoneManager.getTimeZoneforUser(createdUser.getDirectoryUser());

        final UserJsonBean bean = new UserJsonBean();
        bean.setSelf(jiraBaseUrls.restApi2BaseUrl() + "user?username=" + JiraUrlCodec.encode(createdUser.getUsername()));
        bean.setName(createdUser.getUsername());
        bean.setDisplayName(createdUser.getDisplayName());
        bean.setEmailAddress(createdUser.getEmailAddress(), loggedInUser, emailFormatter);
        bean.setActive(createdUser.isActive());
        bean.setAvatarUrls(getAvatarURLs(createdUser));
        bean.setTimeZone(timeZone != null ? timeZone.getID() : null);
        bean.setKey(createdUser.getKey());
        return bean;
    }

    @Override
    public Collection<UserJsonBean> createBeanCollection(final Collection<ApplicationUser> createdUsers, final ApplicationUser loggedInUser)
    {
        return this.createBeanCollection(createdUsers, loggedInUser, jiraBaseUrls, emailFormatter, timeZoneManager);
    }

    @Override
    public Collection<UserJsonBean> createBeanCollection(final Collection<ApplicationUser> createdUsers, final ApplicationUser loggedInUser, final JiraBaseUrls jiraBaseUrls, final EmailFormatter emailFormatter, final TimeZoneManager timeZoneManager)
    {
        if (createdUsers == null)
        {
            return null;
        }

        return copyOf(transform(createdUsers, new Function<ApplicationUser, UserJsonBean>()
        {
            @Override
            public UserJsonBean apply(@Nullable final ApplicationUser from)
            {
                return createBean(from, loggedInUser, jiraBaseUrls, emailFormatter, timeZoneManager);
            }
        }));
    }
}
