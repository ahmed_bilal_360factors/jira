package com.atlassian.jira.plugin.report;

/**
 * The report category.
 * @since v6.4
 */
public interface ReportCategory
{
    /**
     * Unique {@link String} identifier of given category. Should not contain any spaces and non-standard ASCII
     * characters.
     *
     * @return the key
     */
    public String getKey();

    /**
     * Existing i18n key of given report category.
     *
     * @return the i18n key
     */
    public String getI18nKey();

}
