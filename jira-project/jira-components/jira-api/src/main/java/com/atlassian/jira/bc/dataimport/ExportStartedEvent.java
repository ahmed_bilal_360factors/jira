package com.atlassian.jira.bc.dataimport;

import com.atlassian.annotations.PublicApi;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;

/**
 * Event raised when a data export begins.
 *
 * @since v5.0
 */
@PublicApi
public class ExportStartedEvent implements DataExportEvent
{
    /**
     * The user that instigated the export. May be null if, for instance, it is triggered by a scheduled job and not a
     * user.
     *
     * @deprecated since 6.4 use {@link #loggedInApplicationUser}
     */
    @Deprecated
    public final User loggedInUser;
    /**
     * The user that instigated the export. May be null if, for instance, it is triggered by a scheduled job and not a
     * user.
     */
    public final ApplicationUser loggedInApplicationUser;

    /**
     * The filename the data is being saved to.
     */
    public final String filename;

    /**
     * The time in milliseconds when the export was started..
     */
    public final Long xmlExportTime;

    @Deprecated
    public ExportStartedEvent(final User user, final String filename)
    {
        this(ApplicationUsers.from(user), filename, null);
    }

    /**
     * @deprecated since 6.4 use {@link #ExportStartedEvent(com.atlassian.jira.user.ApplicationUser, String, Long)}
     */
    @Deprecated
    public ExportStartedEvent(final User user, final String filename, final Long xmlExportTime)
    {
        this(ApplicationUsers.from(user), filename, xmlExportTime);
    }

    public ExportStartedEvent(final ApplicationUser user, final String filename, final Long xmlExportTime)
    {
        this.loggedInUser = ApplicationUsers.toDirectoryUser(user);
        this.loggedInApplicationUser = user;
        this.filename = filename;
        this.xmlExportTime = xmlExportTime;
    }

    @Override
    public Long getXmlExportTime()
    {
        return xmlExportTime;
    }
}
