package com.atlassian.jira.jql.query;

import java.util.List;

import javax.annotation.Nullable;

import com.atlassian.jira.datetime.LocalDate;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.operator.OperatorClasses;
import com.atlassian.jira.jql.util.JqlLocalDateSupport;
import com.atlassian.jira.util.Function;
import com.atlassian.query.operator.Operator;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A query factory that can handle relational operators for dates.
 *
 * @since v4.4
 */
public class LocalDateRelationalQueryFactory implements OperatorSpecificQueryFactory
{
    private static final Logger log = LoggerFactory.getLogger(LocalDateRelationalQueryFactory.class);

    private final RangeQueryFactory<LocalDate> rangeQueryFactory;
    private final JqlLocalDateSupport jqlLocalDateSupport;

    public LocalDateRelationalQueryFactory(final JqlLocalDateSupport jqlLocalDateSupport)
    {
        this.jqlLocalDateSupport = jqlLocalDateSupport;
        this.rangeQueryFactory = new RangeQueryFactory<LocalDate>(new Function<LocalDate, String>()
        {
            public String get(final LocalDate date)
            {
                return jqlLocalDateSupport.getIndexedValue(date);
            }
        });
    }

    public QueryFactoryResult createQueryForSingleValue(final String fieldName, final Operator operator, final List<QueryLiteral> rawValues)
    {
        if (!handlesSingleValueOperator(operator))
        {
            log.debug(String.format("LocalDate operands do not support operator '%s'.", operator.getDisplayString()));
            return QueryFactoryResult.createFalseResult();
        }

        final List<LocalDate> dates = jqlLocalDateSupport.getLocalDatesFromQueryLiterals(rawValues);

        // if there were no parsable dates in the literals, the resultant list will be empty
        if (dates.isEmpty())
        {
            return QueryFactoryResult.createFalseResult();
        }

        // most operators only expect one value
        final LocalDate value = dates.get(0);

        // if we somehow got null as the value, don't error out but just return a false query
        if (value == null)
        {
            return QueryFactoryResult.createFalseResult();
        }

        return new QueryFactoryResult(rangeQueryFactory.get(operator, fieldName, value));
    }

    public QueryFactoryResult createQueryForMultipleValues(final String fieldName, final Operator operator, final List<QueryLiteral> rawValues)
    {
        if (!handlesMultiValueOperator(operator))
        {
            log.debug(String.format("LocalDate operands do not support operator '%s'.", operator.getDisplayString()));
            return QueryFactoryResult.createFalseResult();
        }

        final List<LocalDate> dates = jqlLocalDateSupport.getLocalDatesFromQueryLiterals(rawValues);

        // if we somehow got null as the value, don't error out but filter it
        final List<LocalDate> filteredDates = ImmutableList.copyOf(Iterables.filter(dates, new Predicate<LocalDate>()
        {
            @Override
            public boolean apply(@Nullable final LocalDate localDate)
            {
                return (localDate != null);
            }
        }));

        // if there were no parsable dates in the literals, the resultant list will be empty
        if (filteredDates.isEmpty())
        {
            return QueryFactoryResult.createFalseResult();
        }

        return new QueryFactoryResult(rangeQueryFactory.get(operator, fieldName, filteredDates));
    }

    public QueryFactoryResult createQueryForEmptyOperand(final String fieldName, final Operator operator)
    {
        log.debug("Empty operands are not supported by this query factory.");
        return QueryFactoryResult.createFalseResult();
    }

    private boolean handlesSingleValueOperator(final Operator operator)
    {
        return OperatorClasses.RELATIONAL_ONLY_OPERATORS.contains(operator);
    }

    private boolean handlesMultiValueOperator(final Operator operator)
    {
        return operator == Operator.DURING;
    }

    public boolean handlesOperator(final Operator operator)
    {
        return handlesSingleValueOperator(operator) || handlesMultiValueOperator(operator);
    }
}
