package com.atlassian.jira.bc.license;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseRoleId;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Provides authorisation-related methods around the provision and consumption of license roles.
 *
 * @see {@link JiraLicenseManager}
 * @since 6.3
 * @deprecated use {@link com.atlassian.jira.bc.license.LicenseRoleAdminService} or
 * {@link com.atlassian.jira.bc.license.LicenseRoleAuthorizationService} instead
 */
@ExperimentalApi
@Deprecated
public interface LicenseRoleService extends LicenseRoleAdminService
{
    /**
     * Returns true if the given user has the given license role regardless of whether a license for that role is
     * present.
     *
     * @param user the user to check - if this is null, this method returns false
     * @param licenseRoleId the license role to check - if this is null, you'll get an exception :)
     * @deprecated use {@link LicenseRoleAuthorizationService#canUseRole(com.atlassian.jira.user.ApplicationUser, com.atlassian.jira.license.LicenseRoleId)} instead
     */
    @Deprecated
    boolean userHasRole(@Nullable ApplicationUser user, @Nonnull LicenseRoleId licenseRoleId);
}
