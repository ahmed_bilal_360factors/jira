package com.atlassian.jira.issue.context;

import com.atlassian.annotations.PublicApi;
import com.atlassian.bandana.BandanaContext;
import com.atlassian.jira.issue.context.manager.JiraContextTreeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.collect.MapBuilder;
import org.ofbiz.core.entity.GenericValue;

import java.util.Map;

@PublicApi
/**
 * @deprecated ProjectCategoryContext is not actually used in core JIRA and will be removed. Since v6.4.
 */
public class ProjectCategoryContext extends AbstractJiraContext
{
    // ------------------------------------------------------------------------------------------------- Type Properties
    protected ProjectCategory projectCategory;

    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final ProjectManager projectManager;

    // ---------------------------------------------------------------------------------------------------- Constructors

    /**
     * @deprecated Use {@link #ProjectCategoryContext(com.atlassian.jira.project.ProjectCategory, com.atlassian.jira.issue.context.manager.JiraContextTreeManager)} instead. Since v5.2.
     * @param projectCategoryGV
     * @param treeManager
     */
    public ProjectCategoryContext(final GenericValue projectCategoryGV, final JiraContextTreeManager treeManager)
    {
        this.projectCategory = projectCategoryGV == null ? null : treeManager.getProjectManager().getProjectCategoryObject(projectCategoryGV.getLong("id"));
        this.projectManager = treeManager.getProjectManager();
    }

    /**
     * @deprecated Use {@link #ProjectCategoryContext(com.atlassian.jira.project.ProjectCategory, com.atlassian.jira.project.ProjectManager)} instead. Since v6.4.
     */
    public ProjectCategoryContext(ProjectCategory projectCategory, JiraContextTreeManager treeManager)
    {
        this.projectCategory = projectCategory;
        this.projectManager = treeManager.getProjectManager();
    }

    public ProjectCategoryContext(ProjectCategory projectCategory, ProjectManager projectManager)
    {
        this.projectCategory = projectCategory;
        this.projectManager = projectManager;
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public JiraContextNode getParent()
    {
        return GlobalIssueContext.getInstance();
    }

    // Redundant, but here to make the API checker happy
    @Override
    public BandanaContext getParentContext()
    {
        return getParent();
    }

    @Override
    public boolean hasParentContext()
    {
        return true;
    }


    // -------------------------------------------------------------------------------------------------- Helper Methods
    @Override
    public Map<String, Object> appendToParamsMap(final Map<String, Object> input)
    {
        return MapBuilder.newBuilder(input).add(FIELD_PROJECT_CATEGORY, getProjectCategoryObject() != null ? getProjectCategoryObject().getId() : null).add(
            FIELD_PROJECT, null).toMap();
        //        props.put(FIELD_ISSUE_TYPE, null);
    }

    // -------------------------------------------------------------------------------------- Basic accessors & mutators
    @Override
    public Project getProjectObject()
    {
        return null;
    }

    @Override
    public GenericValue getProject()
    {
        return null;
    }

    public Long getProjectId()
    {
        return null;
    }

    @Override
    public GenericValue getProjectCategory()
    {
        return projectCategory == null ? null : projectManager.getProjectCategory(projectCategory.getId());
    }

    @Override
    public ProjectCategory getProjectCategoryObject()
    {
        return projectCategory;
    }

    @Override
    public IssueType getIssueTypeObject()
    {
        return null;
    }

    @Override
    public GenericValue getIssueType()
    {
        return null;
    }

    public String getIssueTypeId()
    {
        return null;
    }

    @Override
    public String toString()
    {
        return "ProjectCategoryContext[projectCategoryId=" + projectCategory + ']';
    }
}
