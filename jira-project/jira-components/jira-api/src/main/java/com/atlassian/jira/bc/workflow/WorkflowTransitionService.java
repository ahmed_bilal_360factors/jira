package com.atlassian.jira.bc.workflow;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.opensymphony.workflow.loader.ConditionDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * A container-injectable service that provides the ability to add conditions (via {@link ConditionDescriptor})
 * and post functions (via {@link FunctionDescriptor}) to a JIRA workflow.
 * <p>
 * Instances of {@link ConditionDescriptor} and {@link FunctionDescriptor} can be created using
 * {@link WorkflowConditionDescriptorFactory} and {@link WorkflowFunctionDescriptorFactory} respectively.
 *
 * @since v6.4
 */
@Internal
public interface WorkflowTransitionService
{
    /**
     * Adds {@code condition} to all transitions whose name match the given {@code transitionName} in {@code workflow}.
     *
     * @param transitionName Name of the transitions to be updated with the {@code condition}. This is case-insensitive.
     * @param condition The condition to be applied to all matched transitions.
     * @param workflow The JIRA workflow to be updated.
     *
     * @return A resulting {@link ErrorCollection} for the operation. Use {@link ErrorCollection#hasAnyErrors()} to check whether
     * there are any errors.
     */
    ErrorCollection addConditionToWorkflow(@Nonnull String transitionName, @Nonnull ConditionDescriptor condition, @Nonnull JiraWorkflow workflow);

    /**
     * Adds {@code function} to all transitions whose name match the given {@code transitionName} in {@code workflow}.
     *
     * @param transitionName Name of the transitions to be updated with the {@code function}. This is case-insensitive.
     * @param function The function to be applied to all matched transitions.
     * @param workflow The JIRA workflow to be updated.
     *
     * @return A resulting {@link ErrorCollection} for the operation. Use {@link ErrorCollection#hasAnyErrors()} to check whether
     * there are any errors.
     */
    ErrorCollection addPostFunctionToWorkflow(@Nonnull String transitionName, @Nonnull FunctionDescriptor function, @Nonnull JiraWorkflow workflow);

    /**
     * Sets the given {@code screen} to all transitions whose name match the given {@code transitionName} in {@code workflow}.
     *
     * @param transitionName Name of the transitions to be updated with the {@code condition}. This is case-insensitive.
     * @param screen The screen to be set for all matched transitions.
     * @param workflow The JIRA workflow to be updated.
     *
     * @return A resulting {@link ErrorCollection} for the operation. Use {@link ErrorCollection#hasAnyErrors()} to check whether
     * there are any errors.
     */
    ErrorCollection setScreen(@Nonnull String transitionName, @Nullable FieldScreen screen, @Nonnull JiraWorkflow workflow);
}
