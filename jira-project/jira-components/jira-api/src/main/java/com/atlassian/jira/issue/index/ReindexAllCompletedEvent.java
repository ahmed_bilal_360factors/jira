package com.atlassian.jira.issue.index;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;

/**
 * Raised when "reindex all" has completed.

 * This Class should not be constructed by consumers of the API.
 * @since v5.0
 */
@PublicApi
@EventName("reindex.all.completed")
public class ReindexAllCompletedEvent implements IndexEvent
{
    /** How long it took for the reindex to occur, in milliseconds */
    private final long duration;

    /** The time when the reindex started (millis since epoch) */
    private final long startTime;

    private final boolean useBackgroundIndexing;
    private final boolean updateReplicatedIndex;

    private final IssueIndexingParams issueIndexingParams;
    private final Long issueCount;

    @Deprecated
    public ReindexAllCompletedEvent(long time)
    {
        this(time, false, false);
    }

    @Deprecated
    public ReindexAllCompletedEvent(final long duration, final boolean useBackgroundIndexing, final boolean updateReplicatedIndex)
    {
        this(-1, duration, useBackgroundIndexing, updateReplicatedIndex, IssueIndexingParams.INDEX_ALL, null);
    }

    @Internal
    public ReindexAllCompletedEvent(final long startTime, final long duration, final boolean useBackgroundIndexing, final boolean updateReplicatedIndex,
            final IssueIndexingParams issueIndexingParams, final Long issueCount)
    {
        this.startTime = startTime;
        this.duration = duration;
        this.useBackgroundIndexing = useBackgroundIndexing;
        this.updateReplicatedIndex = updateReplicatedIndex;
        this.issueIndexingParams = issueIndexingParams;
        this.issueCount = issueCount;
    }

    /**
     * Get the time when this re-index was started, in milliseconds since epoch
     * @return the start time
     */
    public long getIndexStartTime() {
        return startTime;
    }

    public long getTotalTime()
    {
        return duration;
    }

    public boolean isUsingBackgroundIndexing()
    {
        return useBackgroundIndexing;
    }

    @SuppressWarnings("unused")
    public boolean shouldUpdateReplicatedIndex()
    {
        return updateReplicatedIndex;
    }

    public IssueIndexingParams getIssueIndexingParams()
    {
        return issueIndexingParams;
    }

    @SuppressWarnings("unused")
    public boolean isIndexIssues()
    {
        return issueIndexingParams.isIndexIssues();
    }

    @SuppressWarnings("unused")
    public boolean isIndexChangeHistory()
    {
        return issueIndexingParams.isIndexChangeHistory();
    }

    @SuppressWarnings("unused")
    public boolean isIndexComments()
    {
        return issueIndexingParams.isIndexComments();
    }

    @SuppressWarnings("unused")
    public boolean isIndexWorklogs()
    {
        return issueIndexingParams.isIndexWorklogs();
    }

    public Long getIssueCount()
    {
        return issueCount;
    }
}
