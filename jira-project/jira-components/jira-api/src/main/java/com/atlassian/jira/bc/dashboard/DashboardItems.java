package com.atlassian.jira.bc.dashboard;

import com.atlassian.annotations.PublicApi;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.dashboard.DashboardId;

/**
 * Static factory methods to create {@link com.atlassian.jira.bc.dashboard.DashboardItem} instances
 */
@PublicApi
public final class DashboardItems
{
    private DashboardItems() {}

    public static DashboardItem fromGadgetState(GadgetState state, DashboardId containedInDashboard)
    {
        return new DashboardItemImpl(DashboardItemLocator.itemLocator(containedInDashboard, state.getId()));
    }
}
