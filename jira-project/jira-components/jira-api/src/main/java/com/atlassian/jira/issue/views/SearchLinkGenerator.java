package com.atlassian.jira.issue.views;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.project.version.Version;

/**
 * Generates issue navigator search link for the given field value.
 *
 * @since 6.4
 */
@ExperimentalApi
public interface SearchLinkGenerator
{
    /**
     * Get search link for component field scoped within a project of the passed component.
     *
     * @param component The component object to generate the link from.
     * @return String with search URL that includes base url, empty String if null parameter was passed.
     */
    String getComponentSearchLink(ProjectComponent component);

    /**
     * Get search link for fixVersion field scoped within a project of the passed version.
     *
     * @param version The version object to generate the link from.
     * @return String with search URL that includes base url, empty String if null parameter was passed.
     */
    String getFixVersionSearchLink(Version version);
}
