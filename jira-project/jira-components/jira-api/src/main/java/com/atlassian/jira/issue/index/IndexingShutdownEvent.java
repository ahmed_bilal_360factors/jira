package com.atlassian.jira.issue.index;

import com.atlassian.annotations.PublicApi;

/**
* Event triggered when indexing gets shutdown entirely. This generally only
* happens during JIRA's startup/initialization.
* @since v5.0
*/
@PublicApi
public class IndexingShutdownEvent implements IndexEvent
{}
