package com.atlassian.jira.task.context;

import com.atlassian.annotations.Internal;

/**
 * Used to notify interested parties of the task's progress.
 *
 * @since v6.4
 */
@Internal
interface Sink
{
    /**
     * What sub-task is currently being processed.
     *
     * @param subTask an identifying name for the background task
     */
    void setName(final String subTask);

    /**
     * Progress has been made.
     * Should only be called when the value changes, not after every task.
     *
     * @param progress the progress percentage, expressed as an integer from {@code 0} to {@code 100}, inclusive.
     */
    void updateProgress(final int progress);
}
