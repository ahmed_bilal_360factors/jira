package com.atlassian.jira.permission;

public class PermissionSchemeEntry
{
    private final Long id;
    private final Long schemeId;
    private final String permissionKey;
    private final String type;
    private final String parameter;

    public PermissionSchemeEntry(final Long id, final Long schemeId, final String permissionKey, final String type, final String parameter)
    {
        this.id = id;
        this.schemeId = schemeId;
        this.permissionKey = permissionKey;
        this.type = type;
        this.parameter = parameter;
    }

    public Long getId()
    {
        return id;
    }

    public Long getSchemeId()
    {
        return schemeId;
    }

    public String getPermissionKey()
    {
        return permissionKey;
    }

    public String getType()
    {
        return type;
    }

    public String getParameter()
    {
        return parameter;
    }
}
