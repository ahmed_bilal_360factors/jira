package com.atlassian.jira.issue.index;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;

/**
 * Raised when "reindex all" has completed.

 * This Class should not be constructed by consumers of the API.
 *
 * @since v5.2
 */
@PublicApi
@EventName ("reindex.all.cancelled")
public final class ReindexAllCancelledEvent implements IndexEvent
{
    @Internal
    public ReindexAllCancelledEvent()
    {
    }
}
