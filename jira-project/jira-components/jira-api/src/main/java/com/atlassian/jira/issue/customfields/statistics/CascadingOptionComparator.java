package com.atlassian.jira.issue.customfields.statistics;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.issue.customfields.option.Option;

import java.util.Comparator;

import javax.annotation.Nullable;

/**
 * Compares two CascadingSelect options by their sequence numbers.
 *
 * @since v6.1
 */
@Internal
public class CascadingOptionComparator implements Comparator<CascadingOption>
{
    private final Comparator<Option> optionComparator;

    public CascadingOptionComparator()
    {
        optionComparator = getComparator();
    }

    public static Comparator<Option> getComparator()
    {
        return new Comparator<Option>()
        {
            @Override
            public int compare(@Nullable final Option option1, @Nullable final Option option2)
            {
                if (option1 == null && option2 == null)
                {
                    return 0;
                }
                else if (option1 == null)
                {
                    return -1;
                }
                else if (option2 == null)
                {
                    return 1;
                }
                else
                {
                    return option1.getSequence().compareTo(option2.getSequence());
                }
            }
        };
    }

    public int compare(final CascadingOption o1, final CascadingOption o2)
    {
        if (o1 == null && o2 == null)
        {
            return 0;
        }
        else if (o1 == null)
        {
            return -1;
        }
        else if (o2 == null)
        {
            return 1;
        }

        // Compare the parents first.
        final Option parent1 = o1.getParent();
        final Option parent2 = o2.getParent();

        final int parentResult = optionComparator.compare(parent1, parent2);
        if (parentResult != 0)
        {
            return parentResult;
        }

        // If the parents are identical, compare the children instead.
        final Option child1 = o1.getChild();
        final Option child2 = o2.getChild();
        return optionComparator.compare(child1, child2);
    }
}
