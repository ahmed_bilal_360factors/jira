package com.atlassian.jira.avatar;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.annotation.Nonnull;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

/**
 * Represents an icon for a project or some other entity in JIRA.
 *
 * @since v4.0
 */
@PublicApi
public interface Avatar
{
    /**
     * The type of Avatar.
     *
     * @return a non null Avatar.Type.
     */
    @Nonnull
    Avatar.Type getAvatarType();

    /**
     * The base filename to the avatar image file. The actual file name will be modified with the id etc.
     *
     * @return the non null file name.
     */
    @Nonnull
    String getFileName();

    /**
     * The MIME type of the avatar image file.
     *
     * @return the non null file name.
     */
    @Nonnull
    String getContentType();

    /**
     * The database identifier for the Avatar, may be null if it hasn't yet been stored or if database identifiers are
     * not supported. This will always return null for user avatars in some deployment configurations. Avatars for
     * other purposes (e.g. Projects) may follow this in future versions and this method will be deprecated entirely.
     * The id should not be used to construct URLs to the currently-configured avatar for anything. This method should
     * only be used when it is necessary to refer to an avatar that is not currently the configured avatar for the
     * domain object. The only use cases where this is needed are those to do with modifying or viewing detailed avatar
     * configuration.
     *
     * @return the database id or null.
     */
    Long getId();

    /**
     * A String representation of the identity of the domain object that this avatar is an avatar for!
     * For example, if it is a user avatar, it would be the username (since that is the primary key), for a Project
     * it is the project ID as a String. The meaning of this should be determined by the
     * {@link com.atlassian.jira.avatar.Avatar.Type}.
     *
     * @return the owner id must not be null.
     */
    @Nonnull
    String getOwner();

    /**
     * Indicates whether the Avatar is a system-provided one or if users have defined it.
     *
     * @return true only if the Avatar is a system-provided one.
     */
    boolean isSystemAvatar();

    /**
     * An indicator of the owner type of the avatar. E.g. project, user, group, role etc.
     */
    public static enum Type
    {

        PROJECT("project", APKeys.JIRA_DEFAULT_AVATAR_ID),
        USER("user", APKeys.JIRA_DEFAULT_USER_AVATAR_ID),
        /**
         * @since v6.3
         */
        ISSUETYPE("issuetype", APKeys.JIRA_DEFAULT_ISSUETYPE_AVATAR_ID);

        private String name;
        private String defaultIdKey;
        private final static Map<String, Type> typesByName;

        static {
            typesByName = createNameToTypeMap();
        }

        private static ImmutableMap<String, Type> createNameToTypeMap()
        {
            final ImmutableMap.Builder<String, Type> typesByNameBuilder = ImmutableMap.builder();
            for( Type type: Type.values()) {
                typesByNameBuilder.put( type.getName(), type );
            }

            return typesByNameBuilder.build();
        }

        private Type(final String name, final String defaultIdKey)
        {
            this.name = name;
            this.defaultIdKey = defaultIdKey;
        }

        /**
         * The canonical String representation of the type.
         *
         * @return the name.
         */
        public String getName()
        {
            return name;
        }

        public static Type getByName(final String name)
        {
            return name==null ?
                    null :
                    typesByName.get(name);
        }

        public Long getDefaultId(ApplicationProperties applicationProperties)
        {
            String defaultAvatarId = applicationProperties.getString(this.defaultIdKey);
            return defaultAvatarId != null ? Long.valueOf(defaultAvatarId) : null;
        }
    }

    /**
     * The standard sizes for avatars.
     */
    public static enum Size
    {
        /**
         * A small avatar (24x24 pixels). Use when outputting user's names.
         */
        NORMAL("small", 24),

        /**
         * An extra-small avatar (16x16 pixels).
         */
        SMALL("xsmall", 16),

        /**
         * A medium avatar (32x32 pixels). Use in comments and other activity streams.
         */
        MEDIUM("medium", 32),

        /**
         * A large avatar (48x48 pixels).
         */
        LARGE("large", 48, true),

        XLARGE("xlarge", 64),
        XXLARGE("xxlarge", 96),
        XXXLARGE("xxxlarge", 128),
        RETINA_XXLARGE("xxlarge@2x", 192),
        RETINA_XXXLARGE("xxxlarge@2x", 256);

        public static final Predicate<Size> LOW_RES = new Predicate<Avatar.Size>()
        {
            // TODO JRADEV-20790 - Don't output higher res URLs in our REST endpoints until system avatars have more pixels.
            @Override
            public boolean apply( final Avatar.Size input)
            {
                return input.getPixels() <= 48;
            }
        };

        /**
         * The value to pass back to the server for the size parameter.
         */
        final String param;

        /**
         * The number of pixels.
         */
        final Integer pixels;

        /**
         * Whether this is the default size.
         */
        final boolean isDefault;

        private static final Size largest;
        private static final Size defaultSize;
        private static final List<Size> orderedSizes;
        private static final Map<String, Size> paramToSize;

        static
        {
            Size maxValue = SMALL;
            Size defaultValue = SMALL;
            for (Size imageSize : values())
            {
                if (imageSize.isDefault)
                {
                    defaultValue = imageSize;
                }
                if (imageSize.pixels > maxValue.pixels)
                {
                    maxValue = imageSize;
                }
            }
            largest = maxValue;
            defaultSize = defaultValue;
            orderedSizes = Size.inPixelOrder();
            paramToSize = createParamToSizeMap();
        }

        private static Map<String, Size> createParamToSizeMap()
        {
            final ImmutableMap.Builder<String, Size> paramToSize = ImmutableMap.builder();
            for( Size size : Size.values()) {
                paramToSize.put(size.getParam(), size);
            }

            return paramToSize.build();
        }

        Size(String param, int pixels, boolean isDefault)
        {
            this.param = param;
            this.isDefault = isDefault;
            this.pixels = pixels;
        }

        private Size(String param, int pixels)
        {
            this(param, pixels, false);
        }

        /**
         * In order to cater for future addition of larger sizes this method finds the largest image size.
         * @return The largest Size
         */
        public static Size largest()
        {
            return largest;
        }

        /**
         * @return the default size for avatars.
         */
        public static Size defaultSize()
        {
            return defaultSize;
        }

        /**
         * @param pixelValue minimum number of pixels tall+wide the avatar size should be
         * @return an avatar {@link Size} that's equal to or larger than the pixelValue,
         *         or null if there's no size that could cater the value.
         */
        public static Size biggerThan(int pixelValue)
        {
            Size theSize = null;
            for (Size aSize : Size.inPixelOrder())
            {
                if (aSize.pixels >= pixelValue)
                {
                    theSize = aSize;
                    break;
                }
            }
            return theSize;
        }

        /**
         * @param pixelValue minimum number of pixels tall+wide the avatar size should be
         * @return an avatar {@link Size} that's equal to or larger than the {@link pixelValue},
         *         or null if there's no size that could cater the value.
         */
        public static Size smallerThan(int pixelValue)
        {
            Size theSize = null;
            for (Size aSize : Lists.reverse(Size.inPixelOrder()))
            {
                if (aSize.pixels <= pixelValue)
                {
                    theSize = aSize;
                    break;
                }
            }
            return theSize;
        }

        static List<Size> inPixelOrder()
        {
            if (null != orderedSizes) return orderedSizes;
            List<Size> orderedSizes = Arrays.asList(Size.values());
            Collections.sort(orderedSizes, new Comparator<Size>()
            {
                @Override
                public int compare(final Size o1, final Size o2)
                {
                    if (o1.getPixels() == o2.getPixels()) return 0;
                    return (o1.getPixels() < o2.getPixels()) ? -1 : 1;
                }
            });
            return orderedSizes;
        }

        public int getPixels()
        {
            return pixels;
        }

        public String getParam()
        {
            return param;
        }

        @Override
        public String toString()
        {
            return String.format("<Size [%s], %dx%dpx>", param, pixels, pixels);
        }

        /**
         * Returns the {@link com.atlassian.jira.avatar.Avatar.Size} for the given AUI standard size name.
         * @param param the standard name of the size (e.g. small, medium, xlarge)
         * @return the corresponding {@link com.atlassian.jira.avatar.Avatar.Size}
         * @throws java.util.NoSuchElementException if there is no known size by that name.
         */
        public static Size getSizeFromParam(final String param)
        {
            if ( null == param || !paramToSize.containsKey(param) )
                throw new NoSuchElementException(param);

            return paramToSize.get(param);
        }

        /**
         * Returns the Size which is exactly the same size as the given pixel edge value, rounding upward if no
         * exact match is available, except if the given value is greater than the maximum available size in which
         * case the maximum size will be returned.
         *
         * @param size
         * @return
         */
        public static Size approx(final int size)
        {
            if (size >= largest.getPixels()) {
                return largest;
            }
            if (size <= orderedSizes.get(0).getPixels()) {
                return orderedSizes.get(0);
            }
            for (Size s : orderedSizes)
            {
                if (s.getPixels() >= size) {
                    return s;
                }
            }
            // this shouldn't happen because we've exhausted the options
            return Size.defaultSize;
        }
    }

    /**
     * These are the filenames of avatars that used to be available as system avatars, but were
     * to be removed from the list of avatar options available to new projects.
     */
    static final List<String> demotedSystemProjectAvatars = Lists.newArrayList(
            "codegeist.png",
            "jm_black.png",
            "jm_brown.png",
            "jm_orange.png",
            "jm_red.png",
            "jm_white.png",
            "jm_yellow.png",
            "monster.png"
    );
}
