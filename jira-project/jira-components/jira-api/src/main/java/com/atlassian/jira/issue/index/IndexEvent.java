package com.atlassian.jira.issue.index;

/**
 * Interface for all events raised during indexing.
 * @since v6.4
 */
public interface IndexEvent
{
}
