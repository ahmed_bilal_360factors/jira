package com.atlassian.jira.plugin.myjirahome;

import javax.annotation.Nullable;

import com.atlassian.annotations.PublicApi;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;

/**
 * Fired when a user changes his My JIRA Home location. The event should not contain the location itself
 * but the complete plugin module keys instead.
 *
 * @since 5.1
 */
@PublicApi
public class MyJiraHomeChangedEvent
{
    private final ApplicationUser user;
    private final String fromHomePluginModuleKey;
    private final String toHomePluginModuleKey;

    /**
     * This method will be removed in JIRA 7.0
     * @deprecated use the other constructor instead
     */
    @Deprecated
    public MyJiraHomeChangedEvent(final User user, final String fromHomePluginModuleKey, final String toHomePluginModuleKey)
    {
        this.user = ApplicationUsers.from(user);
        this.fromHomePluginModuleKey = fromHomePluginModuleKey;
        this.toHomePluginModuleKey = toHomePluginModuleKey;
    }

    /**
     * @since 6.4
     */
    public MyJiraHomeChangedEvent(final ApplicationUser user, final String fromHomePluginModuleKey,
            final String toHomePluginModuleKey)
    {
        this.user = user;
        this.fromHomePluginModuleKey = fromHomePluginModuleKey;
        this.toHomePluginModuleKey = toHomePluginModuleKey;
    }

    /**
     * This method will be removed in JIRA 7.0
     * @deprecated use getApplicationUser instead
     */
    @Deprecated
    @Nullable
    public User getUser()
    {
        return ApplicationUsers.toDirectoryUser(user);
    }

    /**
     * @since 6.4
     */
    public ApplicationUser getApplicationUser()
    {
        return user;
    }

    public String getFromHomePluginModuleKey()
    {
        return fromHomePluginModuleKey;
    }

    public String getToHomePluginModuleKey()
    {
        return toHomePluginModuleKey;
    }
}
