package com.atlassian.jira.issue;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.annotations.PublicApi;
import com.atlassian.fugue.Either;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;

/**
 * Provides validation methods for attachments.
 *
 * @since v6.4
 */
@PublicApi
@ParametersAreNonnullByDefault
public interface AttachmentValidator
{

    /**
     * Verifies if user may create attachment for given issue.
     *
     * @param user user who creates attachment
     * @param issue target issue
     * @param errorCollection error collections that will be filled in case of error
     * @return true if user can create attachments
     */
    boolean canCreateAttachments(@Nullable ApplicationUser user, Issue issue, ErrorCollection errorCollection);

    /**
     * Verifies if user may create temporary attachment for given issue or project (in case if the issue is being
     * created).
     *
     * @param user user who creates temporary attachment
     * @param issueOrProject target issue or project (in case if the issue is being created)
     * @param errorCollection error collections that will be filled in case of error
     * @return true if user can create temporary attachments
     */
    boolean canCreateTemporaryAttachments(@Nullable ApplicationUser user, Either<Issue, Project> issueOrProject,
            ErrorCollection errorCollection);
}
