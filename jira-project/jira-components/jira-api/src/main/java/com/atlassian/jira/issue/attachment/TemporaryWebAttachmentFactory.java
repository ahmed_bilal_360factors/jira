package com.atlassian.jira.issue.attachment;

import org.joda.time.DateTime;

/**
 * Factory for {@link com.atlassian.jira.issue.attachment.TemporaryWebAttachment}.
 *
 * @since v6.4
 */
public class TemporaryWebAttachmentFactory
{
    public TemporaryWebAttachment create(final TemporaryAttachmentId temporaryAttachmentId, final String filename,
            final String contentType, final String formToken, final long size)
    {
        return new TemporaryWebAttachment(temporaryAttachmentId, filename, contentType, formToken, size, DateTime.now());
    }
}
