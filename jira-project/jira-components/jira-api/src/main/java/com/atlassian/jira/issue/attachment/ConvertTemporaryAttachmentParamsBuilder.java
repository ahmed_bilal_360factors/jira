package com.atlassian.jira.issue.attachment;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;

import com.google.common.base.Preconditions;

import org.joda.time.DateTime;

/**
 * A builder class for {@link ConvertTemporaryAttachmentParams}.
 *
 * @since v6.4
 */
@ParametersAreNonnullByDefault
public class ConvertTemporaryAttachmentParamsBuilder
{
    @Nullable
    private ApplicationUser author;
    private Issue issue;
    private TemporaryAttachmentId temporaryAttachmentId;
    private String filename;
    private String contentType;
    private long fileSize;
    private DateTime createdTime = DateTime.now();

    public ConvertTemporaryAttachmentParamsBuilder setAuthor(@Nullable final ApplicationUser author)
    {
        this.author = author;
        return this;
    }

    public ConvertTemporaryAttachmentParamsBuilder setIssue(final Issue issue)
    {
        this.issue = issue;
        return this;
    }

    public ConvertTemporaryAttachmentParamsBuilder setTemporaryAttachmentId(final TemporaryAttachmentId temporaryAttachmentId)
    {
        this.temporaryAttachmentId = temporaryAttachmentId;
        return this;
    }

    public ConvertTemporaryAttachmentParamsBuilder setFilename(final String filename)
    {
        this.filename = filename;
        return this;
    }

    public ConvertTemporaryAttachmentParamsBuilder setContentType(final String contentType)
    {
        this.contentType = contentType;
        return this;
    }

    public ConvertTemporaryAttachmentParamsBuilder setFileSize(final long fileSize)
    {
        this.fileSize = fileSize;
        return this;
    }

    public ConvertTemporaryAttachmentParamsBuilder setCreatedTime(final DateTime createdTime)
    {
        this.createdTime = createdTime;
        return this;
    }

    public ConvertTemporaryAttachmentParams build()
    {
        Preconditions.checkNotNull(issue);
        Preconditions.checkNotNull(temporaryAttachmentId);
        Preconditions.checkNotNull(filename);
        Preconditions.checkNotNull(contentType);
        Preconditions.checkNotNull(fileSize);
        Preconditions.checkNotNull(createdTime);
        return new ConvertTemporaryAttachmentParams(author, issue, temporaryAttachmentId, filename, contentType, fileSize, createdTime);
    }
}