package com.atlassian.jira.task.context;

import com.atlassian.annotations.Internal;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.ImmutableList;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Composite version of the Sink. Takes multiple Sink implementations and calls them in order.
 *
 * @since v3.13
 */
@Internal
class CompositeSink implements Sink
{
    private final List<Sink> delegates;

    CompositeSink(final Collection<? extends Sink> delegates)
    {
        notNull("delegates", delegates);
        this.delegates = ImmutableList.copyOf(delegates);
    }

    public void setName(final String name)
    {
        notNull("name", name);
        for (final Object element : delegates)
        {
            final Sink delegate = (Sink) element;
            delegate.setName(name);
        }
    }

    public void updateProgress(final int progress)
    {
        for (final Object element : delegates)
        {
            final Sink delegate = (Sink) element;
            delegate.updateProgress(progress);
        }
    }
}
