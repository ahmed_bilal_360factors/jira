package com.atlassian.jira.issue.customfields.statistics;

import java.util.Comparator;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.issue.customfields.converters.SelectConverter;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.LuceneFieldSorter;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * {@link com.atlassian.jira.issue.search.LuceneFieldSorter} implementation for {@link com.atlassian.jira.issue.customfields.statistics.CascadingOption}.
 *
 * @since v6.4
 */
@Internal
public class CascadingOptionFieldSorter implements LuceneFieldSorter<CascadingOption>
{
    protected final CustomField customField;
    protected final SelectConverter selectConverter;

    public CascadingOptionFieldSorter(final CustomField customField, final SelectConverter selectConverter)
    {
        this.selectConverter = notNull("selectConverter", selectConverter);
        this.customField = notNull("customField", customField);
    }

    @Override
    public String getDocumentConstant()
    {
        return customField.getId() + CascadingSelectStatisticsMapper.SUB_VALUE_SUFFIX;
    }

    @Override
    public CascadingOption getValueFromLuceneField(final String documentValue)
    {
        if (documentValue == null || "".equals(documentValue))
        {
            return null;
        }
        // The separator is | so we need to escape it when turning it into a regex
        String[] valueSections = documentValue.split("\\" + CascadingSelectStatisticsMapper.PARENT_AND_CHILD_INDEX_SEPARATOR);
        switch (valueSections.length)
        {
            case 0:
                return null;
            case 1:
                Option onlyParent = selectConverter.getObject(valueSections[0]);
                return new CascadingOption(onlyParent, null);
            case 2:
                Option parent = selectConverter.getObject(valueSections[0]);
                Option child = selectConverter.getObject(valueSections[1]);
                return new CascadingOption(parent, child);
            default:
                throw new FieldValidationException("Option Id '" + documentValue + "' is not a valid cascading select pair.");
        }
    }

    @Override
    public Comparator<CascadingOption> getComparator()
    {
        return new CascadingOptionComparator();
    }
}
