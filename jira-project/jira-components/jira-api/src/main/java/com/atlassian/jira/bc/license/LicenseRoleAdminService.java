package com.atlassian.jira.bc.license;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.license.LicenseRole;
import com.atlassian.jira.license.LicenseRoleId;

import java.util.Set;
import javax.annotation.Nonnull;

/**
 * Provides access to license role management functionality.
 *
 * @since 6.4
 */
public interface LicenseRoleAdminService
{
    /**
     * The key for any group related errors reported.
     */
    final String ERROR_GROUPS = "groups";

    /**
     * The key for any primaryGroup related errors reported.
     */
    final String ERROR_PRIMARY = "primaryGroup";

    /**
     * Return the {@link Set} of all the roles {@link LicenseRoleAuthorizationService#isDefined defined}
     * in the system or an error.
     *
     * @return the collection of all the roles defined in the system or an error.
     */
    @Nonnull
    ServiceOutcome<Set<LicenseRole>> getRoles();

    /**
     * Return the {@link com.atlassian.jira.license.LicenseRole} identified by the passed
     * {@link com.atlassian.jira.license.LicenseRoleId}.
     *
     * @param licenseRoleId the id of the role to find.
     *
     * @return the {@code LicenseRole} with the passed {@code LicenseRoleId} or an error if such a {@code LicenseRole}
     * is not currently installed in JIRA.
     */
    @Nonnull
    ServiceOutcome<LicenseRole> getRole(@Nonnull LicenseRoleId licenseRoleId);

    /**
     * Save the passed {@link com.atlassian.jira.license.LicenseRole} information to the database. This method will
     * only accept the passed role if:
     *
     * <ol>
     *  <li>The role is authorised.</li>
     *  <li>The role only contains currently valid groups.</li>
     *  <li>The primary group is contained within the role.</li>
     * </ol>
     *
     * @param role the role to save.
     * @return the role as persisted to the database or an error.
     */
    @Nonnull
    ServiceOutcome<LicenseRole> setRole(@Nonnull LicenseRole role);

    /**
     * Set the groups associated with the passed {@link LicenseRoleId}. The primary group for the role will be unset.
     *
     * @param licenseRoleId the id of the license role to update.
     * @param groups the groups to associated with the passed license role.
     * @return the updated {@link com.atlassian.jira.license.LicenseRole} or an error.
     * @deprecated Use {@link #setRole(com.atlassian.jira.license.LicenseRole)} instead.
     */
    @Nonnull
    @Deprecated
    ServiceOutcome<LicenseRole> setGroups(@Nonnull LicenseRoleId licenseRoleId, @Nonnull Iterable<String> groups);
}
