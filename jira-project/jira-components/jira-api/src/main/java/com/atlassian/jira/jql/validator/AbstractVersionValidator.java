package com.atlassian.jira.jql.validator;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operator.OperatorClasses;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.jql.resolver.VersionIndexInfoResolver;
import com.atlassian.jira.jql.resolver.VersionResolver;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;

import com.google.common.annotations.VisibleForTesting;

/**
 * Abstract version clause validator that pretty much does all the work for version validation.
 *
 * @since v4.0
 */
abstract class AbstractVersionValidator implements ClauseValidator
{
    ///CLOVER:OFF

    private final ValuesExistValidator versionValuesExistValidator;
    private final SupportedOperatorsValidator supportedOperatorsValidator;

    AbstractVersionValidator(final VersionResolver versionResolver, final JqlOperandResolver operandResolver,
            final PermissionManager permissionManager, final VersionManager versionManager, final I18nHelper.BeanFactory beanFactory)
    {
        this.supportedOperatorsValidator = getSupportedOperatorsValidator();
        this.versionValuesExistValidator = getVersionValuesExistValidator(versionResolver, operandResolver, permissionManager, versionManager, beanFactory);
    }

    public MessageSet validate(final User searcher, final TerminalClause terminalClause)
    {
        final MessageSet errors = supportedOperatorsValidator.validate(searcher, terminalClause);
        if (errors.hasAnyErrors())
        {
            return errors;
        }
        return versionValuesExistValidator.validate(searcher, terminalClause);
    }

    public MessageSet validate(final TerminalClause terminalClause, final QueryCreationContext queryCreationContext)
    {
        final MessageSet errors = supportedOperatorsValidator.validate(queryCreationContext.getApplicationUser(), terminalClause);
        if (errors.hasAnyErrors())
        {
            return errors;
        }
        return versionValuesExistValidator.validate(terminalClause, queryCreationContext);
    }

    @VisibleForTesting
    SupportedOperatorsValidator getSupportedOperatorsValidator()
    {
        return new SupportedOperatorsValidator(OperatorClasses.EQUALITY_OPERATORS_WITH_EMPTY, OperatorClasses.RELATIONAL_ONLY_OPERATORS);
    }

    @VisibleForTesting
    VersionValuesExistValidator getVersionValuesExistValidator(final VersionResolver versionResolver,
            final JqlOperandResolver operandResolver, final PermissionManager permissionManager,
            final VersionManager versionManager, final I18nHelper.BeanFactory beanFactory)
    {
        return new VersionValuesExistValidator(operandResolver, new VersionIndexInfoResolver(versionResolver),
                permissionManager, versionManager, beanFactory);
    }

    ///CLOVER:ON
}
