package com.atlassian.jira.security.roles;

import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.jira.user.ApplicationUser;

/**
 * This interface represents an extended RoleActorFactory which is capable of creating all role actors for a given user.
 * It was introduced to avoid breaking the SPI of RoleActorFactory.
 * In further JIRA versions the additional method will belong to RoleActorFactory.
 *
 * @since v6.4
 */

public interface OptimizedRoleActorFactory extends RoleActorFactory
{
    /**
     * Find and return all role actors for the given user.
     *
     * @param user for which actors will be obtained
     *
     * @return a set of actors being associated with the given user or empty set if none found.
     */
    @Nonnull
    Set<ProjectRoleActor> getAllRoleActorsForUser(@Nullable ApplicationUser user);
}
