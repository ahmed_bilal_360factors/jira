package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.PublicSpi;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginParseException;

import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.join;

/**
 * Convenient abstraction to initialise conditions that require the {@link PermissionManager} and accept "permission"
 * param.
 * <p/>
 * The permission param is converted using {@link Permissions#getType(String)} and its value is set in {@link
 * #permission}
 *
 * @since v6.0
 */
@PublicSpi
public abstract class AbstractPermissionCondition extends AbstractWebCondition
{
    private static final String PREFIX = "atl.jira.permission.request.cache";
    private static final String HAS_PROJECTS_PREFIX = "atl.jira.hasProjects.request.cache";

    protected final PermissionManager permissionManager;
    protected int permission;

    public AbstractPermissionCondition(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public void init(Map<String,String> params) throws PluginParseException
    {
        permission = Permissions.getType(params.get("permission"));
        if (permission == -1)
        {
            throw new PluginParseException("Could not determine permission type for: " + params.get("permission"));
        }
        super.init(params);
    }

    @Nonnull
    @ExperimentalApi
    public static String getHasPermissionKey(int permission, @Nullable ApplicationUser user, Object... args)
    {
        return format("%s:%s:%s:%s", PREFIX, permission, user, (args != null ? join(args, ":") : ""));
    }

    @Nonnull
    @ExperimentalApi
    public static String getHasPermissionKey(ProjectPermissionKey permission, @Nullable ApplicationUser user, Object... args)
    {
        return format("%s:%s:%s:%s", PREFIX, permission, user, (args != null ? join(args, ":") : ""));
    }

    @Nonnull
    @ExperimentalApi
    public static String getHasProjectsKey(int permission, @Nullable ApplicationUser user, Object... args)
    {
        return format("%s:%s:%s:%s", HAS_PROJECTS_PREFIX, permission, user, (args != null ? join(args, ":") : ""));
    }
}
