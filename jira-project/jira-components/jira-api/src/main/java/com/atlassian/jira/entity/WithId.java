package com.atlassian.jira.entity;

import javax.annotation.Nullable;

import com.atlassian.annotations.ExperimentalApi;

import com.google.common.base.Function;

/**
 * Entities implementing this interface are supposed to be uniquely identifiable by id.
 * @since v6.2
 */
@ExperimentalApi
public interface WithId
{
    /**
     * @return the unique id of the entity.
     */
    Long getId();
}
