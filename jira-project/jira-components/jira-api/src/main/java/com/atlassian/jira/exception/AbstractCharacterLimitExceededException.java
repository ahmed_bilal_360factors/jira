package com.atlassian.jira.exception;

import com.atlassian.annotations.Internal;

/**
 * Base class for jira character limit exceeded exceptions.
 *
 * @since: 6.4.1
 */
public abstract class AbstractCharacterLimitExceededException extends IllegalArgumentException
{
    protected final long maxNumberOfCharacters;

    @Internal
    public AbstractCharacterLimitExceededException(final long maxNumberOfCharacters)
    {
        this.maxNumberOfCharacters = maxNumberOfCharacters;
    }

    /**
     * Returns maximum number of characters allowed.
     *
     * @return max number of characters, 0 means unlimited
     */
    public long getMaxNumberOfCharacters()
    {
        return maxNumberOfCharacters;
    }
}
