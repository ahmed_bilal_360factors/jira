package com.atlassian.jira.issue.fields.rest.json.beans;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.base.Function;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Collection;
import java.util.Map;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;

/**
 * @since v5.0
 */
public class UserJsonBean
{
    @JsonProperty
    private String self;

    @JsonProperty
    private String name;

    @JsonProperty
    private String key;

    @JsonProperty
    private String emailAddress;

    @JsonProperty
    private Map<String, String> avatarUrls;

    @JsonProperty
    private String displayName;

    @JsonProperty
    private boolean active;

    @JsonProperty
    private String timeZone;

    public String getSelf()
    {
        return self;
    }

    public void setSelf(String self)
    {
        this.self = self;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    /**
     * @deprecated Use {@link #setEmailAddress(String, com.atlassian.jira.user.ApplicationUser,
     * com.atlassian.jira.util.EmailFormatter)}
     */
    @Deprecated
    public void setEmailAddress(String emailAddress)
    {
        setEmailAddress(emailAddress, ComponentAccessor.getComponent(JiraAuthenticationContext.class).getUser(), ComponentAccessor.getComponent(EmailFormatter.class));
    }

    public void setEmailAddress(String emailAddress, ApplicationUser loggedInUser, EmailFormatter emailFormatter)
    {
        this.emailAddress = emailFormatter.formatEmail(emailAddress, loggedInUser != null ? loggedInUser.getDirectoryUser() : null);
    }

    public Map<String, String> getAvatarUrls()
    {
        return avatarUrls;
    }

    public void setAvatarUrls(Map<String, String> avatarUrls)
    {
        this.avatarUrls = avatarUrls;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getTimeZone()
    {
        return timeZone;
    }

    public void setTimeZone(final String timeZone)
    {
        this.timeZone = timeZone;
    }

    /**
     * @deprecated Use {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBeanCollection(java.util.Collection,
     * com.atlassian.jira.user.ApplicationUser)} or {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBeanCollection(java.util.Collection,
     * com.atlassian.jira.user.ApplicationUser, JiraBaseUrls, com.atlassian.jira.util.EmailFormatter,
     * com.atlassian.jira.timezone.TimeZoneManager)}
     */
    @Deprecated
    public static Collection<UserJsonBean> shortBeans(final Collection<User> users, final JiraBaseUrls urls)
    {
        return shortBeans(users, urls, ComponentAccessor.getComponent(JiraAuthenticationContext.class).getUser(), ComponentAccessor.getComponent(EmailFormatter.class), ComponentAccessor.getComponent(TimeZoneManager.class));
    }

    /**
     * @deprecated Use {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBeanCollection(java.util.Collection,
     * com.atlassian.jira.user.ApplicationUser)} or {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBeanCollection(java.util.Collection,
     * com.atlassian.jira.user.ApplicationUser, JiraBaseUrls, com.atlassian.jira.util.EmailFormatter,
     * com.atlassian.jira.timezone.TimeZoneManager)}
     */
    @Deprecated
    public static Collection<UserJsonBean> shortBeans(final Collection<User> users, final JiraBaseUrls urls, final ApplicationUser loggedInUser, final EmailFormatter emailFormatter)
    {
        return shortBeans(users, urls, loggedInUser, emailFormatter, ComponentAccessor.getComponent(TimeZoneManager.class));
    }

    /**
     * @deprecated Use {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBeanCollection(java.util.Collection,
     * com.atlassian.jira.user.ApplicationUser)} or {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBeanCollection(java.util.Collection,
     * com.atlassian.jira.user.ApplicationUser, JiraBaseUrls, com.atlassian.jira.util.EmailFormatter,
     * com.atlassian.jira.timezone.TimeZoneManager)}
     */
    @Deprecated
    public static Collection<UserJsonBean> shortBeans(final Collection<User> users, final JiraBaseUrls urls, final ApplicationUser loggedInUser, final EmailFormatter emailFormatter, final TimeZoneManager timeZoneManager)
    {
        if (users == null)
        {
            return null;
        }
        final Collection<ApplicationUser> applicationUsers = copyOf(transform(users, new Function<User, ApplicationUser>()
        {
            @Override
            public ApplicationUser apply(final User user)
            {
                return ApplicationUsers.from(user);
            }
        }));
        return ComponentAccessor.getComponent(UserBeanFactory.class).createBeanCollection(applicationUsers, loggedInUser, urls, emailFormatter, timeZoneManager);
    }

    /**
     * @deprecated Use {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBeanCollection(java.util.Collection,
     * com.atlassian.jira.user.ApplicationUser)} or {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBeanCollection(java.util.Collection,
     * com.atlassian.jira.user.ApplicationUser, JiraBaseUrls, com.atlassian.jira.util.EmailFormatter,
     * com.atlassian.jira.timezone.TimeZoneManager)}
     */
    @Deprecated
    public static Collection<UserJsonBean> shortBeanCollection(final Collection<ApplicationUser> users, final JiraBaseUrls urls)
    {
        return ComponentAccessor.getComponent(UserBeanFactory.class).createBeanCollection(users, ComponentAccessor.getComponent(JiraAuthenticationContext.class).getUser(), urls, ComponentAccessor.getComponent(EmailFormatter.class), ComponentAccessor.getComponent(TimeZoneManager.class));
    }

    /**
     * @deprecated Use {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBeanCollection(java.util.Collection,
     * com.atlassian.jira.user.ApplicationUser)} or {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBeanCollection(java.util.Collection,
     * com.atlassian.jira.user.ApplicationUser, JiraBaseUrls, com.atlassian.jira.util.EmailFormatter,
     * com.atlassian.jira.timezone.TimeZoneManager)}
     */
    @Deprecated
    public static Collection<UserJsonBean> shortBeanCollection(final Collection<ApplicationUser> users, final JiraBaseUrls urls, final ApplicationUser loggedInUser, final EmailFormatter emailFormatter)
    {
        return ComponentAccessor.getComponent(UserBeanFactory.class).createBeanCollection(users, loggedInUser, urls, emailFormatter, ComponentAccessor.getComponent(TimeZoneManager.class));
    }

    /**
     * @deprecated Use {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBeanCollection(java.util.Collection,
     * com.atlassian.jira.user.ApplicationUser)} or {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBeanCollection(java.util.Collection,
     * com.atlassian.jira.user.ApplicationUser, JiraBaseUrls, com.atlassian.jira.util.EmailFormatter,
     * com.atlassian.jira.timezone.TimeZoneManager)}
     */
    @Deprecated
    public static Collection<UserJsonBean> shortBeanCollection(final Collection<ApplicationUser> users, final JiraBaseUrls urls, final ApplicationUser loggedInUser, final EmailFormatter emailFormatter, final TimeZoneManager timeZoneManager)
    {
        return ComponentAccessor.getComponent(UserBeanFactory.class).createBeanCollection(users, loggedInUser, urls, emailFormatter, timeZoneManager);
    }

    /**
     * @deprecated Use {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBean(com.atlassian.jira.user.ApplicationUser,
     * com.atlassian.jira.user.ApplicationUser)} or {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBean(com.atlassian.jira.user.ApplicationUser,
     * com.atlassian.jira.user.ApplicationUser, JiraBaseUrls, com.atlassian.jira.util.EmailFormatter,
     * com.atlassian.jira.timezone.TimeZoneManager)}
     */
    @Deprecated
    public static UserJsonBean shortBean(final User user, final JiraBaseUrls urls)
    {
        final ApplicationUser applicationUser = ApplicationUsers.from(user);
        return ComponentAccessor.getComponent(UserBeanFactory.class).createBean(applicationUser, ComponentAccessor.getComponent(JiraAuthenticationContext.class).getUser(), urls, ComponentAccessor.getComponent(EmailFormatter.class), ComponentAccessor.getComponent(TimeZoneManager.class));
    }

    /**
     * @deprecated Use {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBean(com.atlassian.jira.user.ApplicationUser,
     * com.atlassian.jira.user.ApplicationUser)} or {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBean(com.atlassian.jira.user.ApplicationUser,
     * com.atlassian.jira.user.ApplicationUser, JiraBaseUrls, com.atlassian.jira.util.EmailFormatter,
     * com.atlassian.jira.timezone.TimeZoneManager)}
     */
    @Deprecated
    public static UserJsonBean shortBean(final User user, final JiraBaseUrls urls, final ApplicationUser loggedInUser, final EmailFormatter emailFormatter)
    {
        final ApplicationUser applicationUser = ApplicationUsers.from(user);
        return ComponentAccessor.getComponent(UserBeanFactory.class).createBean(applicationUser, loggedInUser, urls, emailFormatter, ComponentAccessor.getComponent(TimeZoneManager.class));
    }

    /**
     * @deprecated Use {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBean(com.atlassian.jira.user.ApplicationUser,
     * com.atlassian.jira.user.ApplicationUser)} or {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBean(com.atlassian.jira.user.ApplicationUser,
     * com.atlassian.jira.user.ApplicationUser, JiraBaseUrls, com.atlassian.jira.util.EmailFormatter,
     * com.atlassian.jira.timezone.TimeZoneManager)}
     */
    @Deprecated
    public static UserJsonBean shortBean(final User user, final JiraBaseUrls urls, final ApplicationUser loggedInUser, final EmailFormatter emailFormatter, final TimeZoneManager timeZoneManager)
    {
        final ApplicationUser applicationUser = ApplicationUsers.from(user);
        return ComponentAccessor.getComponent(UserBeanFactory.class).createBean(applicationUser, loggedInUser, urls, emailFormatter, timeZoneManager);
    }

    /**
     * @deprecated Use {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBean(com.atlassian.jira.user.ApplicationUser,
     * com.atlassian.jira.user.ApplicationUser)} or {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBean(com.atlassian.jira.user.ApplicationUser,
     * com.atlassian.jira.user.ApplicationUser, JiraBaseUrls, com.atlassian.jira.util.EmailFormatter,
     * com.atlassian.jira.timezone.TimeZoneManager)}
     */
    @Deprecated
    public static UserJsonBean shortBean(final ApplicationUser user, final JiraBaseUrls urls)
    {
        return ComponentAccessor.getComponent(UserBeanFactory.class).createBean(user, ComponentAccessor.getComponent(JiraAuthenticationContext.class).getUser(), urls, ComponentAccessor.getComponent(EmailFormatter.class), ComponentAccessor.getComponent(TimeZoneManager.class));
    }

    /**
     * @deprecated Use {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBean(com.atlassian.jira.user.ApplicationUser,
     * com.atlassian.jira.user.ApplicationUser)} or {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBean(com.atlassian.jira.user.ApplicationUser,
     * com.atlassian.jira.user.ApplicationUser, JiraBaseUrls, com.atlassian.jira.util.EmailFormatter,
     * com.atlassian.jira.timezone.TimeZoneManager)}
     */
    @Deprecated
    public static UserJsonBean shortBean(final ApplicationUser user, final JiraBaseUrls urls, final ApplicationUser currentUser, final EmailFormatter emailFormatter)
    {
        return ComponentAccessor.getComponent(UserBeanFactory.class).createBean(user, currentUser, urls, emailFormatter, ComponentAccessor.getComponent(TimeZoneManager.class));
    }

    /**
     * @deprecated Use {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBean(com.atlassian.jira.user.ApplicationUser,
     * com.atlassian.jira.user.ApplicationUser)} or {@link com.atlassian.jira.issue.fields.rest.json.DefaultUserBeanFactory#createBean(com.atlassian.jira.user.ApplicationUser,
     * com.atlassian.jira.user.ApplicationUser, JiraBaseUrls, com.atlassian.jira.util.EmailFormatter,
     * com.atlassian.jira.timezone.TimeZoneManager)}
     */
    @Deprecated
    public static UserJsonBean shortBean(final ApplicationUser user, final JiraBaseUrls urls, final ApplicationUser currentUser, final EmailFormatter emailFormatter, final TimeZoneManager timeZoneManager)
    {
        return ComponentAccessor.getComponent(UserBeanFactory.class).createBean(user, currentUser, urls, emailFormatter, ComponentAccessor.getComponent(TimeZoneManager.class));
    }

    public static final UserJsonBean USER_DOC_EXAMPLE = new UserJsonBean();
    public static final UserJsonBean USER_SHORT_DOC_EXAMPLE = new UserJsonBean();

    static
    {
        USER_DOC_EXAMPLE.setSelf("http://www.example.com/jira/rest/api/2/user?username=fred");
        USER_DOC_EXAMPLE.setName("fred");
        USER_DOC_EXAMPLE.emailAddress = "fred@example.com";
        USER_DOC_EXAMPLE.setDisplayName("Fred F. User");
        USER_DOC_EXAMPLE.setActive(true);
        USER_DOC_EXAMPLE.setAvatarUrls(MapBuilder.<String, String>newBuilder()
                .add("16x16", "http://www.example.com/jira/secure/useravatar?size=xsmall&ownerId=fred")
                .add("24x24", "http://www.example.com/jira/secure/useravatar?size=small&ownerId=fred")
                .add("32x32", "http://www.example.com/jira/secure/useravatar?size=medium&ownerId=fred")
                .add("48x48", "http://www.example.com/jira/secure/useravatar?size=large&ownerId=fred")
                .toMap());
        USER_DOC_EXAMPLE.setTimeZone("Australia/Sydney");

        USER_SHORT_DOC_EXAMPLE.setSelf(USER_DOC_EXAMPLE.getSelf());
        USER_SHORT_DOC_EXAMPLE.setName(USER_DOC_EXAMPLE.getName());
        USER_SHORT_DOC_EXAMPLE.setDisplayName(USER_DOC_EXAMPLE.getDisplayName());
    }
}

