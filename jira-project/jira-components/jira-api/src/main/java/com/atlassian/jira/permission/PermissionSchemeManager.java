/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

package com.atlassian.jira.permission;

import com.atlassian.annotations.Internal;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;

import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * This class is used to handle Permission Schemes. Permission Schemes are created, removed and edited through this class
 */
public interface PermissionSchemeManager extends SchemeManager
{
    public String getSchemeEntityName();

    public String getEntityName();

    public String getAssociationType();

    public String getSchemeDesc();

    /**
     * Get all Scheme entity records for a particular scheme.
     * Inherited from SchemeManager.
     *
     * @param scheme The scheme that the entities belong to
     * @return List of (GenericValue) entities
     * @throws GenericEntityException If a DB error occurs
     */
    List<GenericValue> getEntities(GenericValue scheme) throws GenericEntityException;

    /**
     * Inherited from SchemeManager.
     *
     * @deprecated Use {@link #getPermissionSchemeEntries(com.atlassian.jira.scheme.Scheme, com.atlassian.jira.security.plugin.ProjectPermissionKey)} instead. Since v6.4.
     */
    List<GenericValue> getEntities(GenericValue scheme, Long entityTypeId) throws GenericEntityException;

    /**
     * Inherited from SchemeManager.
     *
     * @deprecated Use {@link #getPermissionSchemeEntries(com.atlassian.jira.scheme.Scheme, com.atlassian.jira.security.plugin.ProjectPermissionKey)} instead. Since v6.4.
     */
    List<GenericValue> getEntities(GenericValue scheme, String permissionKey) throws GenericEntityException;

    /**
     * Inherited from SchemeManager.
     *
     * @deprecated Use {@link #getPermissionSchemeEntries(com.atlassian.jira.scheme.Scheme, com.atlassian.jira.security.plugin.ProjectPermissionKey)} instead. Since v6.4.
     */
    List<GenericValue> getEntities(GenericValue scheme, Long entityTypeId, String parameter) throws GenericEntityException;

    /**
     * Inherited from SchemeManager.
     *
     * @deprecated Use {@link #getPermissionSchemeEntries(long, com.atlassian.jira.security.plugin.ProjectPermissionKey, String)} instead. Since v6.4.
     */
    List<GenericValue> getEntities(GenericValue scheme, String type, Long entityTypeId) throws GenericEntityException;

    /**
     * Returns the PermissionSchemeEntries for the given Permission Scheme and ProjectPermissionKey.
     * @param scheme The permission scheme
     * @param permissionKey The permission type
     * @return the PermissionSchemeEntries for the given Permission Scheme and ProjectPermissionKey.
     *
     * @since 6.4
     * @see #getPermissionSchemeEntries(long, ProjectPermissionKey)
     */
    Collection<PermissionSchemeEntry> getPermissionSchemeEntries(@Nonnull Scheme scheme, @Nonnull ProjectPermissionKey permissionKey);

    /**
     * Returns the PermissionSchemeEntries for the given Permission Scheme and ProjectPermissionKey.
     * @param schemeId The permission scheme
     * @param permissionKey The permission type
     * @return the PermissionSchemeEntries for the given Permission Scheme and ProjectPermissionKey.
     *
     * @since 6.4
     * @see #getPermissionSchemeEntries(Scheme, ProjectPermissionKey)
     */
    Collection<PermissionSchemeEntry> getPermissionSchemeEntries(long schemeId, @Nonnull ProjectPermissionKey permissionKey);

    /**
     * Returns the PermissionSchemeEntries for the given Permission Scheme and ProjectPermissionKey.
     * @param schemeId The permission scheme
     * @param permissionKey The permission type
     * @return the PermissionSchemeEntries for the given Permission Scheme and ProjectPermissionKey.
     *
     * @since 6.4
     * @see #getPermissionSchemeEntries(long, ProjectPermissionKey)
     */
    Collection<PermissionSchemeEntry> getPermissionSchemeEntries(long schemeId, @Nonnull ProjectPermissionKey permissionKey, @Nonnull String type);

    /**
     * @since v6.3
     * @deprecated Use {@link #getPermissionSchemeEntries(com.atlassian.jira.scheme.Scheme, com.atlassian.jira.security.plugin.ProjectPermissionKey)} instead. Since v6.4.
     */
    List<GenericValue> getEntities(@Nonnull GenericValue scheme, @Nonnull ProjectPermissionKey permissionKey) throws GenericEntityException;

    /**
     * Get all Generic Value permission records for a particular scheme and permission Id
     * @param scheme The scheme that the permissions belong to
     * @param permissionId The Id of the permission
     * @param parameter The permission parameter (group name etc)
     * @param type The type of the permission(Group, Current Reporter etc)
     * @return List of (GenericValue) permissions
     * @throws GenericEntityException
     * @deprecated Use {@link #getEntities(GenericValue, ProjectPermissionKey, String, String)}. Since v6.3.
     */
    @Deprecated
    public List<GenericValue> getEntities(GenericValue scheme, Long permissionId, String type, String parameter) throws GenericEntityException;

    /**
     * Get all Generic Value permission records for a particular scheme and permission Id
     * @param scheme The scheme that the permissions belong to
     * @param permissionKey The key of the permission
     * @param parameter The permission parameter (group name etc)
     * @param type The type of the permission(Group, Current Reporter etc)
     * @return List of (GenericValue) permissions
     * @throws GenericEntityException
     * @since v6.3
     *
     * @deprecated Use {@link #getPermissionSchemeEntries(long, com.atlassian.jira.security.plugin.ProjectPermissionKey, String)} instead. Since v6.4.
     */
    public List<GenericValue> getEntities(@Nonnull GenericValue scheme, @Nonnull ProjectPermissionKey permissionKey, @Nonnull String type, @Nonnull String parameter) throws GenericEntityException;

    /**
     * @since v6.3
     *
     * @deprecated Use {@link #getPermissionSchemeEntries(com.atlassian.jira.scheme.Scheme, com.atlassian.jira.security.plugin.ProjectPermissionKey)} instead. Since v6.4.
     */
    List<GenericValue> getEntities(@Nonnull GenericValue scheme, @Nonnull ProjectPermissionKey permissionKey, @Nonnull String parameter) throws GenericEntityException;

    /**
     * @since v6.3
     *
     * @deprecated Use {@link #getPermissionSchemeEntries(long, com.atlassian.jira.security.plugin.ProjectPermissionKey, String)} instead. Since v6.4.
     */
    List<GenericValue> getEntitiesByType(@Nonnull GenericValue scheme, @Nonnull ProjectPermissionKey permissionKey, @Nonnull String type) throws GenericEntityException;

    public void flushSchemeEntities();

    /**
     * This is a method that is meant to quickly get you all the schemes that contain an entity of the
     * specified type and parameter.
     * @param type is the entity type
     * @param parameter is the scheme entries parameter value
     * @return Collection of GenericValues that represents a scheme
     */
    public Collection<GenericValue> getSchemesContainingEntity(String type, String parameter);

    /**
     * @deprecated Use {@link #hasSchemeAuthority(ProjectPermissionKey, GenericValue)}. Since v6.3.
     */
    @Deprecated
    boolean hasSchemeAuthority(Long entityType, GenericValue entity);

    /**
     * Checks anonymous permission of the given permission type for the given entity.
     *
     * @param permissionKey permission key.
     * @param entity     the entity to which permission is being checked.
     * @return true only if the anonymous user is permitted.
     * @since v6.3
     *
     * @deprecated This should only be used internally - use the PermissionManager interface. Since v6.4.
     */
    boolean hasSchemeAuthority(@Nonnull ProjectPermissionKey permissionKey, @Nonnull GenericValue entity);

    /**
     * @deprecated Use {@link #hasSchemeAuthority(ProjectPermissionKey, GenericValue, User, boolean)}. Since v6.3.
     */
    @Deprecated
    boolean hasSchemeAuthority(Long entityType, GenericValue entity, User user, boolean issueCreation);

    /**
     * Checks anonymous permission of the given permission type for the given project.
     *
     * @param permissionKey permission key.
     * @param project       the project to which permission is being checked.
     * @return true only if the anonymous user is permitted.
     * @since v6.4
     */
    @Internal
    boolean hasSchemePermission(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Project project);

    /**
     * Checks anonymous permission of the given permission type for the given issue.
     *
     * @param permissionKey permission key.
     * @param issue       the issue to which permission is being checked.
     * @return true only if the anonymous user is permitted.
     * @since v6.4
     */
    @Internal
    boolean hasSchemePermission(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Issue issue);

    /**
     * Checks the given user's permission of the given permission type for the given entity.
     *
     * @param permissionKey    permission key.
     * @param entity        the entity to which permission is being checked.
     * @param user          the user.
     * @param issueCreation whether the permission is for creating an issue.
     * @return true only if the user is permitted.
     * @since v6.3
     *
     * @deprecated This should only be used internally - use the PermissionManager interface. Since v6.4.
     */
    boolean hasSchemeAuthority(@Nonnull ProjectPermissionKey permissionKey, @Nonnull GenericValue entity, @Nonnull User user, boolean issueCreation);

    /**
     * Checks the given user's permission of the given permission type for the given project.
     *
     * @param permissionKey permission key.
     * @param project       the project to which permission is being checked.
     * @param user          the user.
     * @param issueCreation whether the permission is for creating an issue.
     * @return true only if the user is permitted.
     * @since v6.4
     */
    @Internal
    boolean hasSchemePermission(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Project project, @Nullable User user, boolean issueCreation);

    /**
     * Checks the given user's permission of the given permission type for the given issue.
     *
     * @param permissionKey    permission key.
     * @param issue         the issue to check permission against
     * @param user          the user.
     * @param issueCreation whether the permission is for creating an issue.
     * @return true only if the user is permitted.
     * @since v6.4
     */
    @Internal
    boolean hasSchemePermission(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Issue issue, @Nullable User user, boolean issueCreation);

    /**
     * @deprecated Use {@link #getGroups(ProjectPermissionKey, Project)}. Since v6.3.
     */
    @Deprecated
    Collection<Group> getGroups(Long permissionId, Project project);

    /**
     * @since v6.3
     */
    Collection<Group> getGroups(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Project project);

    /**
     * @deprecated Use {@link #getUsers(ProjectPermissionKey, PermissionContext)}. Since v6.3.
     */
    @Deprecated
    Collection<User> getUsers(Long permissionId, PermissionContext ctx);

    /**
     * @since v6.3
     */
    Collection<User> getUsers(@Nonnull ProjectPermissionKey permissionKey, @Nonnull PermissionContext ctx);
}
