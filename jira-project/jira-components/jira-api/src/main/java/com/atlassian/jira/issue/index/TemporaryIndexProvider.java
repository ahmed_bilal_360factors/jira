package com.atlassian.jira.issue.index;

import java.util.Collection;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;

/**
 * Provides a personal, temporary lucene index that you can query against. Each call creates a new temporary index that is stored on disk.
 * It's is automatically removed when the call ends.
 * @since v6.4
 */
@ExperimentalApi
public interface TemporaryIndexProvider
{
    /**
     * Create a temporary index, run the callback and return the computed value
     *
     * @param issues issues that will be indexed into the lucene index
     * @param indexSearcher callback for querying the index
     * @return result of the callback
     * @throws SearchException if unable to compute a result of the search
     */
    @Nullable
    <T> T indexIssuesAndSearch(@Nonnull Collection<? extends Issue> issues, @Nonnull IndexSearcher<T> indexSearcher)
            throws SearchException;

    public interface IndexSearcher<T>
    {
        /**
         * Computes a result, or throws an exception if unable to do so.
         *
         * @return computed result
         * @throws SearchException if unable to compute a result of the search
         */
        T search(SearchProvider searchProvider) throws SearchException;
    }
}
