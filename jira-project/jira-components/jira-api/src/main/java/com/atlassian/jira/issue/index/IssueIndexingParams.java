package com.atlassian.jira.issue.index;

import javax.annotation.concurrent.Immutable;

import com.atlassian.annotations.PublicApi;

/**
 * Provides parameters required to conduct indexing or re-indexing JIRA issues. <br/>
 * All parameters except indexIssues are by default set to false.
 * Use {@link IssueIndexingParams#INDEX_NONE} to set all values to false.
 * Use {@link IssueIndexingParams#INDEX_ISSUE_ONLY} to set all values to false except {@link com.atlassian.jira.issue.index.IssueIndexingParams#indexIssues}.
 * Use {@link IssueIndexingParams#INDEX_ALL} to set all values to true.
 * <ul>
 * <li>indexIssues - true if issues should be indexed</li>
 * <li>indexComments - true if comments should be indexed</li>
 * <li>indexChangeHistory - true if change history should be indexed</li>
 * <li>indexWorklogs true if issue worklogs should be indexed</li>
 * </ul>
 * Clients should use the provided
 * {@link IssueIndexingParams.Builder} to construct an instance of this class.
 *
 * @since v6.4
 */
@Immutable
@PublicApi
public class IssueIndexingParams
{
    public static IssueIndexingParams INDEX_NONE = new IssueIndexingParams(false, false, false, false);
    public static IssueIndexingParams INDEX_ISSUE_ONLY = new IssueIndexingParams(true, false, false, false);
    public static IssueIndexingParams INDEX_ALL = new IssueIndexingParams(true, true, true, true);

    private final boolean indexIssues;
    private final boolean indexChangeHistory;
    private final boolean indexComments;
    private final boolean indexWorklogs;

    private IssueIndexingParams(final boolean indexIssues, final boolean indexChangeHistory, final boolean indexComments, final boolean indexWorklogs)
    {
        this.indexIssues = indexIssues;
        this.indexChangeHistory = indexChangeHistory;
        this.indexComments = indexComments;
        this.indexWorklogs = indexWorklogs;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public boolean isIndexIssues()
    {
        return indexIssues;
    }

    public boolean isIndexChangeHistory()
    {
        return indexChangeHistory;
    }

    public boolean isIndexComments()
    {
        return indexComments;
    }

    public boolean isIndexWorklogs()
    {
        return indexWorklogs;
    }

    public boolean isIndex()
    {
        return indexIssues || indexChangeHistory || indexComments || indexWorklogs;
    }

    public boolean isIndexAll()
    {
        return indexIssues && indexChangeHistory && indexComments && indexWorklogs;
    }

    @Override
    public String toString()
    {
        return "{" +
                "indexIssues=" + indexIssues +
                ", indexChangeHistory=" + indexChangeHistory +
                ", indexComments=" + indexComments +
                ", indexWorklogs=" + indexWorklogs +
                '}';
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final IssueIndexingParams that = (IssueIndexingParams) o;

        if (indexChangeHistory != that.indexChangeHistory) { return false; }
        if (indexComments != that.indexComments) { return false; }
        if (indexIssues != that.indexIssues) { return false; }
        //noinspection RedundantIfStatement
        if (indexWorklogs != that.indexWorklogs) { return false; }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = (indexIssues ? 1 : 0);
        result = 31 * result + (indexChangeHistory ? 1 : 0);
        result = 31 * result + (indexComments ? 1 : 0);
        result = 31 * result + (indexWorklogs ? 1 : 0);
        return result;
    }

    public static class Builder
    {
        private boolean indexIssues = true;
        private boolean indexChangeHistory = false;
        private boolean indexComments = false;
        private boolean indexWorklogs = false;

        /**
         * Creates builder with ORs on each param
         */
        public Builder addIndexingParams(final IssueIndexingParams issueIndexingParams)
        {
            indexIssues |= issueIndexingParams.indexIssues;
            indexChangeHistory |= issueIndexingParams.indexChangeHistory;
            indexComments |= issueIndexingParams.indexComments;
            indexWorklogs |= issueIndexingParams.indexWorklogs;
            return this;
        }

        public Builder setIssues(final boolean indexIssues)
        {
            this.indexIssues = indexIssues;
            return this;
        }

        public Builder setChangeHistory(final boolean indexChangeHistory)
        {
            this.indexChangeHistory = indexChangeHistory;
            return this;
        }

        public Builder setComments(final boolean indexComments)
        {
            this.indexComments = indexComments;
            return this;
        }

        public Builder setWorklogs(final boolean indexWorklogs)
        {
            this.indexWorklogs = indexWorklogs;
            return this;
        }

        public Builder withChangeHistory()
        {
            this.indexChangeHistory = true;
            return this;
        }

        public Builder withComments()
        {
            this.indexComments = true;
            return this;
        }

        public Builder withWorklogs()
        {
            this.indexWorklogs = true;
            return this;
        }

        public Builder withoutIssues()
        {
            this.indexIssues = false;
            return this;
        }

        public IssueIndexingParams build()
        {
            return new IssueIndexingParams(indexIssues, indexChangeHistory, indexComments, indexWorklogs);
        }
    }
}
