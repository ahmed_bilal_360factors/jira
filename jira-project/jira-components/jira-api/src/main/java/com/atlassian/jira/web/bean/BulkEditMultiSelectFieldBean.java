package com.atlassian.jira.web.bean;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption;

import com.atlassian.jira.issue.fields.OrderableField;

/**
 * Used in the BulkEdit Wizard.
 * Stores the change mode options' values selected for the multi-select/multiple values system fields (Labels, Affects Versions, Fix Versions, Components).
 * The change mode options are available in the {@link com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption}
 *
 * @since v6.4
 */
@ExperimentalApi
public interface BulkEditMultiSelectFieldBean
{
    /**
     *  Checks if the change mode selection is allowed for the field.
     *  Returns true if the field is multi-select/multiple values system field (defined in {@link com.atlassian.jira.web.bean.BulkEditMultiSelectFieldBean}). Returns false in all other cases.
     *
     *  @param field The field for which change mode selection allowance is retrieved.
     */
    public boolean isChangeModeSelectionAllowed(OrderableField field);

    /**
     *  Returns the field name to be used in template for the change mode selection for the multi-select/multiple values system field.
     *
     *  @param field The field for which field name is retrieved.
     */
    public String getChangeModeFieldName(OrderableField field);

    /**
     *  Sets the change mode for multi-select/multiple values system field.
     *
     *  @param field The field for which change mode is set.
     *  @param changeMode option to be set for field. The options defined by {@link com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption}
     *  @throws java.lang.IllegalArgumentException in case field is not multi-select/multiple values system field
     */
    public void setChangeModeForField(OrderableField field, BulkEditMultiSelectFieldOption changeMode) throws IllegalArgumentException;

    /**
     *  Returns the {@link com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption} for the multi-select/multiple values system field
     *  or {@code null} in case the field is not multi-select/multiple values system field
     *
     *  @param field The field for which change mode option is retrieved.
     *  @return {@link com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption} set for the field or {@code null}
     */
    public Option<BulkEditMultiSelectFieldOption> getChangeModeForField(OrderableField field);

    /**
     *  Sets the change mode for the multi-select/multiple values system field from the Map of parameters.
     *  The parameters contain key-value pairs where the key is fieldId and the value is change mode option name.
     *
     *  @param field The field for which change mode option is set.
     *  @param params The map of parameters where the key is fieldId and the value is change mode option name
     *  @throws java.lang.IllegalArgumentException in case the field is not multi-select/multiple values system field
     */
    public void setChangeModeFromParams(OrderableField field, Map<String, String[]> params) throws IllegalArgumentException;

    /**
     *  Returns the list of change mode options which are available for the multi-select/multiple values system field.
     *
     *  @return The list of {@link com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption} to be visible in the Bulk Edit Wizard
     */
    public Collection<BulkEditMultiSelectFieldOption> getChangeModeOptions();

    /**
     *  Returns the default change mode option for multi-select/multiple values system field.
     *
     *  @return {@link com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption} which is the default for the Bulk Edit Wizard
     */
    public BulkEditMultiSelectFieldOption getDefaultChangeModeOption();

    /**
     *  Returns the change mode option for option id.
     *
     *  @param id The option id for which {@link com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption} is retrieved.
     *  @return {@link com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption} which is the default for the Bulk Edit Wizard
     */
    public BulkEditMultiSelectFieldOption getChangeModeOptionById(String id);

    /**
     *  Returns the action description for multi-select/multiple values system field based on set change mode for Bulk Edit confirmation screen.
     *
     *  @param field The field for which action description is retrieved.
     *  @return The action description for multi-select/multiple values system field based on set change mode option.
     */
    public String getMultiSelectFieldActionDescription(OrderableField field);

}
