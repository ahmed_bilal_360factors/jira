package com.atlassian.jira.bc.dashboard;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.entity.WithId;

/**
 * Represents a dashboard item.
 */
@PublicApi
public interface DashboardItem extends WithId
{
    /**
     * Returns an object that can tell where is this dashboard item located.
     *
     * @return dashboard item locator
     */
    public DashboardItemLocator getLocator();
}
