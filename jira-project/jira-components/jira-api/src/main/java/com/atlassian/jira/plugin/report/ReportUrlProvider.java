package com.atlassian.jira.plugin.report;

import com.atlassian.annotations.ExperimentalSpi;
import com.atlassian.fugue.Option;

import java.util.Map;

/**
 * Provides context-dependent URL for given report.
 *
 * @since 6.4
 */
@ExperimentalSpi
public interface ReportUrlProvider
{
    /**
     * <p>Generate Report URL for given {@link com.atlassian.jira.plugin.report.ReportModuleDescriptor}, within given
     * {@code context}.<br/> When given {@code context} does not satisfy report requirements, or Report should not
     * show in particular context, it should return {@link com.atlassian.fugue.Option#none()}</p>
     *
     * @param reportModule Descriptor of Report Module which URL has to be generated
     * @param context Context in which report will be displayed
     * @return Report URL or {@link com.atlassian.fugue.Option#none()} when report should not show in this context
     */
    public Option<String> getUrl(ReportModuleDescriptor reportModule, Map<String, Object> context);
}
