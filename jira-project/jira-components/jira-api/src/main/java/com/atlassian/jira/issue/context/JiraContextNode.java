package com.atlassian.jira.issue.context;

import com.atlassian.annotations.PublicApi;
import com.atlassian.bandana.BandanaContext;
import com.atlassian.jira.project.ProjectCategory;
import org.ofbiz.core.entity.GenericValue;

import java.util.Map;

import javax.annotation.Nullable;

/**
 * A constructed {@link IssueContext} with the ability to climb nodes
 */
@PublicApi
public interface JiraContextNode extends BandanaContext, IssueContext, Comparable<JiraContextNode>
{
    /**
     * @deprecated Project Category contexts are not supported in JIRA and will be removed from the API. Since v6.4.
     */
    String FIELD_PROJECT_CATEGORY = "projectcategory";
    String FIELD_PROJECT = "project";

    /**
     * @return Whether or not this context has a parent context.
     */
    boolean hasParentContext();

    /**
     * @return This context's parent, or null if it is the root context.
     *
     * @since 6.4
     */
    @Nullable
    JiraContextNode getParent();

    /**
     * @return This context's parent, or null if it is a root context.
     *
     * @deprecated Use {@link #getParent()} instead. Since v6.4.
     */
    BandanaContext getParentContext();

    /**
     * @deprecated Use {@link #getProjectCategoryObject()} instead. Since v5.2.
     *
     * @return ProjectCategory
     */
    public GenericValue getProjectCategory();

    /**
     * @deprecated Project Category contexts are not supported in JIRA and will be removed from the API. Since v6.4.
     */
    public ProjectCategory getProjectCategoryObject();

    public boolean isInContext(IssueContext issueContext);

    /**
     * Copy the supplied parameters and add new ones.
     * @param props to copy from
     * @return the copied map
     */
    Map<String, Object> appendToParamsMap(Map<String, Object> props);

}
