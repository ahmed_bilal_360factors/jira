package com.atlassian.jira.jql.query;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.jira.issue.customfields.converters.DoubleConverter;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.util.IndexValueConverter;
import com.atlassian.query.clause.TerminalClause;

import org.apache.lucene.util.NumericUtils;

/**
 * Factory for producing clauses for the custom fields that have a raw index value
 *
 * @since v6.4
 */
public class NumberCustomFieldClauseQueryFactory implements ClauseQueryFactory
{
    private final ClauseQueryFactory delegateClauseQueryFactory;
    //CLOVER:OFF
    public NumberCustomFieldClauseQueryFactory(String luceneField, JqlOperandResolver operandResolver, DoubleConverter doubleConverter, boolean supportsRelational)
    {
        final List<OperatorSpecificQueryFactory> operatorFactories = new ArrayList<OperatorSpecificQueryFactory>();
        operatorFactories.add(new NumberEqualityQueryFactory(doubleConverter, Double.MAX_VALUE));
        if (supportsRelational)
        {
            operatorFactories.add(new NumberRelationalQueryFactory(doubleConverter, Double.MAX_VALUE));
        }
        this.delegateClauseQueryFactory = new GenericClauseQueryFactory(luceneField, operatorFactories, operandResolver);
    }

    public QueryFactoryResult getQuery(final QueryCreationContext queryCreationContext, final TerminalClause terminalClause)
    {
        return delegateClauseQueryFactory.getQuery(queryCreationContext, terminalClause);
    }
    //CLOVER:ON
}
