package com.atlassian.jira.event.property;

import com.atlassian.annotations.ExperimentalApi;


@ExperimentalApi
public class StringApplicationPropertySetEvent extends AbstractApplicationPropertySetEvent<String>
{
    public StringApplicationPropertySetEvent(final String applicationPropertyKey, final String value)
    {
        super(applicationPropertyKey, value);
    }
}
