package com.atlassian.jira.bc.license;

import com.atlassian.jira.license.LicenseRole;
import com.atlassian.jira.license.LicenseRoleId;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;


/**
 * A utility service to determine if a user has access to a specific {@link LicenseRole} (as identified by its
 * {@link LicenseRoleId}), or if a license role is available.
 *
 * @see {@link com.atlassian.jira.license.JiraLicenseManager}
 * @see {@link com.atlassian.jira.bc.license.LicenseRoleAdminService}
 * @see com.atlassian.jira.license.LicenseRoleId
 * @see com.atlassian.jira.license.LicenseRole
 * @since 6.4
 */
public interface LicenseRoleAuthorizationService
{
    /**
     * Returns true if the given user has been assigned to the given role and the role is
     * backed by a license that grants that role. Note that this method still returns true for
     * expired or exceeded as well as current licenses.
     *
     * @param user the user to check for access rights - if this is null, this method returns false
     * @param role the license role to check
     * @return true if the user has the specified role and the role is correctly licensed
     * @see {@link LicenseRoleService#userHasRole(ApplicationUser, LicenseRoleId)} for a method which does not check the
     * license
     * @since 6.4
     */
    boolean canUseRole(@Nullable ApplicationUser user, @Nonnull LicenseRoleId role);

    /**
     * Returns true if the given license role has been defined by a product, plugin or the platform,
     * and there is a license present for the role. Note that this method still returns true for
     * expired or exceeded as well as current licenses.
     *
     * @param role a license role identifier
     * @return true if the given license role is defined and has a (not necessarily current) license.
     * @see {@link com.atlassian.jira.license.JiraLicenseManager#isLicensed(LicenseRoleId)} which
     * provides this functionality from 6.3
     * @since 6.4
     */
    @Deprecated
    boolean isLicenseRoleInstalled(@Nonnull LicenseRoleId role);


    /** @deprecated use {@link #canUseRole}. */
    @Deprecated
    boolean isAuthenticated(@Nullable ApplicationUser user, @Nonnull LicenseRoleId licenseRoleId);

    /** @deprecated use {@link #isLicenseRoleInstalled}. */
    @Deprecated
    boolean isAuthenticated(@Nonnull LicenseRoleId role);

    /**
     * Retrieve the number of active users for a specific license role (product). It will uniquely count all users who
     * are found in the groups associated with the product.
     *
     * @param roleId is the license role ID used to identify the product.
     * @return the number of active users for the product identified by roleId
     * @since 6.4
     */
    int getUserCount(@Nonnull LicenseRoleId roleId);
}
