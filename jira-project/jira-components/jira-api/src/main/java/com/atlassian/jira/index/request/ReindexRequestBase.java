package com.atlassian.jira.index.request;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.annotations.PublicApi;

/**
 * A request for JIRA to perform a reindex without its components.
 *
 * @since 6.4
 *
 * @see com.atlassian.jira.index.request.ReindexRequest
 */
@PublicApi
public class ReindexRequestBase
{
    public static final String ID = "id";
    public static final String TYPE = "type";
    public static final String REQUEST_TIME = "requestTime";
    public static final String START_TIME = "startTime";
    public static final String COMPLETION_TIME = "completionTime";
    public static final String STATUS = "status";
    public static final String EXECUTION_NODE_ID = "executionNodeId";

    private final @Nullable Long id;
    private final @Nonnull ReindexRequestType type;
    private final long requestTime;
    private final @Nullable Long startTime;
    private final @Nullable Long completionTime;
    private final @Nullable String executionNodeId;
    private final @Nonnull ReindexStatus status;

    public ReindexRequestBase(@Nullable Long id, @Nonnull ReindexRequestType type,
            long requestTime, @Nullable Long startTime, @Nullable Long completionTime, @Nullable String executionNodeId,
            @Nonnull ReindexStatus status)
    {
        this.id = id;
        this.type = type;
        this.requestTime = requestTime;
        this.startTime = startTime;
        this.completionTime = completionTime;
        this.executionNodeId = executionNodeId;
        this.status = status;
    }

    @Nullable
    public Long getId()
    {
        return id;
    }

    @Nonnull
    public ReindexRequestType getType()
    {
        return type;
    }

    public long getRequestTime()
    {
        return requestTime;
    }

    @Nullable
    public Long getStartTime()
    {
        return startTime;
    }

    @Nullable
    public Long getCompletionTime()
    {
        return completionTime;
    }

    @Nonnull
    public ReindexStatus getStatus()
    {
        return status;
    }

    @Nullable
    public String getExecutionNodeId()
    {
        return executionNodeId;
    }
}
