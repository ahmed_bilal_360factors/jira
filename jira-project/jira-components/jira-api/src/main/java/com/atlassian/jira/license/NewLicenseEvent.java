package com.atlassian.jira.license;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.fugue.Option;

/**
 * Event fired when a new license is entered into JIRA. This can allow, e.g. plugins that cache license-related
 * information to flush their cache.
 *
 * @since v5.0
 * @deprecated As of v6.4 please use {@link LicenseChangedEvent}
 */
@Deprecated
@ExperimentalApi
public final class NewLicenseEvent extends LicenseChangedEvent
{
    public NewLicenseEvent(LicenseDetails licenseDetails)
    {
        super(Option.<LicenseDetails>none(), Option.<LicenseDetails>option(licenseDetails));
    }

    /**
     * Getter for the license details of the new license
     *
     * @return the License Details
     * @since v6.4
     */
    public final LicenseDetails getLicenseDetails()
    {
        return getNewLicenseDetails().getOrNull();
    }
}
