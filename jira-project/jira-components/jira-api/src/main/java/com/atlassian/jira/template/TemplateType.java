package com.atlassian.jira.template;

/**
 *
 * @since v6.4
 */
public enum TemplateType
{
    VELOCITY("velocity"),
    SOY("soy");

    private final String key;

    public String getKey()
    {
        return key;
    }

    TemplateType(final String key) {this.key = key;}

    public boolean equalsIgnoreCase(String otherKey)
    {
        return getKey().equalsIgnoreCase(otherKey);
    }
}
