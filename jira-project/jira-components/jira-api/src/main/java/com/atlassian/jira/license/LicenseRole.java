package com.atlassian.jira.license;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.fugue.Option;

import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

/**
 * Represents a License Role in JIRA.
 *
 * <p>
 * {@link LicenseRole}s exist in one of 3 states: defined, installed, and active:
 * <ul>
 *     <li><em>defined</em> - the license role has been defined by a product, plugin or the platform itself.</li>
 *     <li><em>installed</em> - license role is defined and the role has an accompanying license (which may or
 *     may not be currently valid).</li>
 *     <li><em>active</em> - an installed license role that has an accompanying license that is within all
 *     license terms (ie: within its expiry date and the number of users of that role is under the
 *     user count granted by the license).</li>
 * </ul>
 *
 * <pre>
 *                                DEFINED
 *                               --------
 *                                 |  |\
 *                                 |  |
 *                   installation  |  |  uninstallation
 *                  (add license)  |  |  (remove license)
 *                                 |  |
 *                                \|  |
 *                              ----------
 *                              INSTALLED
 *                              ----------
 *                                 |  |\
 *                                 |  |
 *                     activation  |  |  inactivation
 *           (license is current)  |  |  (license expires or is exceeded)
 *                                 |  |
 *                                \|  |
 *                               --------
 *                                ACTIVE
 * </pre>
 *<p>
 * Note that an active {@code LicenseRole} is necessarily also defined and installed, whereas a
 * a defined {@code LicenseRole} may or may not be installed or active.
 *</p>
 *
 * @since v6.3
 */
@ExperimentalApi
@Immutable
public interface LicenseRole
{
    /**
     * Returns the canonical {@link LicenseRoleId} that uniquely identifies this {@link LicenseRole}.
     *
     * @return the canonical {@link LicenseRoleId} that uniquely identifies this {@link LicenseRole}.
     */
    @Nonnull
    LicenseRoleId getId();

    /**
     * Return the name of the license role. The name is i18ned for the calling user if possible.
     * @return the name of the license role. The name is i18ned for the calling user if possible.
     */
    @Nonnull
    String getName();

    /**
     * Return the set of Group Ids associated with the role.
     *
     * @return the groups ids associated with the role.
     */
    @Nonnull
    Set<String> getGroups();

    /**
     * Return the primary group configured for the role. {@link com.atlassian.fugue.Option#none()} may be returned
     * if no primary is configured.
     *
     * @return the primary group associated with the role.
     */
    @Nonnull
    Option<String> getPrimaryGroup();

    /**
     * Return a <strong>new</strong> LicenseRole with its groups and primaryGroup set to the passed arguments.
     *
     * @param groups the groups associated with the role.
     * @param primaryGroup primary group for the role. A value of {@link com.atlassian.fugue.Option#none()} can be
     * passed indicate that there is no primaryGroup for the role.
     * @throws java.lang.IllegalArgumentException if groups contains null or the primaryGroup is not within groups.
     *
     * @return a new LicenseRole with its groups and primaryGroup set to the passed arguments.
     */
    @Nonnull
    LicenseRole withGroups(@Nonnull Iterable<String> groups, @Nonnull Option<String> primaryGroup);
}
