package com.atlassian.jira.index.request;

import com.atlassian.annotations.PublicApi;

/**
 * An index type.
 *
 * @since 6.4
 */
@PublicApi
public enum AffectedIndex
{
    ISSUE, COMMENT, CHANGEHISTORY, WORKLOG, SHAREDENTITY, ALL
}