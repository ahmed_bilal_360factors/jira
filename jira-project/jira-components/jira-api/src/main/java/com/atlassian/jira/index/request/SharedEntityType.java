package com.atlassian.jira.index.request;

import java.util.Map;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.sharing.SharedEntity;

import com.google.common.collect.Maps;

/**
 * @since 6.4
 */
@PublicApi
public enum SharedEntityType
{
    SEARCH_REQUEST(SearchRequest.ENTITY_TYPE), PORTAL_PAGE(PortalPage.ENTITY_TYPE), NONE(null);

    private SharedEntity.TypeDescriptor typeDescriptor;

    private static Map<SharedEntity.TypeDescriptor, SharedEntityType> typeDescriptorSharedEntityTypeMap;

    private SharedEntityType(SharedEntity.TypeDescriptor typeDescriptor)
    {
        this.typeDescriptor = typeDescriptor;
    }

    public SharedEntity.TypeDescriptor getTypeDescriptor()
    {
        return typeDescriptor;
    }

    public static SharedEntityType  fromTypeDescriptor(SharedEntity.TypeDescriptor typeDescriptor)
    {
        if (typeDescriptorSharedEntityTypeMap == null)
        {
            initialiseTypeDescriptorMap();
        }
        if (typeDescriptor == null)
        {
            return SharedEntityType.NONE;
        }
        else
        {
            return typeDescriptorSharedEntityTypeMap.get(typeDescriptor);
        }
    }

    private static void initialiseTypeDescriptorMap()
    {
        typeDescriptorSharedEntityTypeMap = Maps.newHashMap();
        for (SharedEntityType type : values())
        {
            if (type.getTypeDescriptor() != null)
            {
                typeDescriptorSharedEntityTypeMap.put(type.getTypeDescriptor(), type);
            }
        }
    }
}