package com.atlassian.jira.issue.fields.rest.json.beans;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since 6.4
 */
public class IssueTypeUpdateBean
{
    static final IssueTypeUpdateBean UPDATE_EXAMPLE;
    static
    {
        final IssueTypeUpdateBean issueTypeUpdateBean = new IssueTypeUpdateBean();
        issueTypeUpdateBean.setDescription("description");
        issueTypeUpdateBean.setName("name");
        issueTypeUpdateBean.setAvatarId(1l);
        UPDATE_EXAMPLE = issueTypeUpdateBean;
    }

    @JsonProperty
    private String name;
    @JsonProperty
    private String description;
    @JsonProperty
    private Long avatarId;

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public Long getAvatarId()
    {
        return avatarId;
    }

    public void setAvatarId(final Long avatarId)
    {
        this.avatarId = avatarId;
    }
}
