package com.atlassian.jira.event.project;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.annotations.Internal;
import com.atlassian.fugue.Option;
import com.atlassian.jira.project.version.Version;
import com.google.common.base.Objects;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Event indicating a version has been deleted
 *
 * @since v4.4
 */
public class VersionDeleteEvent extends AbstractVersionEvent
{
    private final Option<Version> mergedTo;

    /**
     * @deprecated Use static factory methods defined in this class instead. Since v6.4.
     */
    @Internal
    @Deprecated
    public VersionDeleteEvent(@Nonnull Version version)
    {
        this(version, null);
    }

    private VersionDeleteEvent(@Nonnull Version version, @Nullable Version mergedTo)
    {
        super(version);
        this.mergedTo = option(mergedTo);
    }

    public static VersionDeleteEvent deletedAndMerged(@Nonnull Version deletedVersion, @Nonnull Version mergedTo)
    {
        return new VersionDeleteEvent(checkNotNull(deletedVersion), checkNotNull(mergedTo));
    }

    public static VersionDeleteEvent deleted(@Nonnull Version deletedVersion)
    {
        return new VersionDeleteEvent(checkNotNull(deletedVersion), null);
    }

    public Option<Version> getMergedTo()
    {
        return mergedTo;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VersionDeleteEvent that = (VersionDeleteEvent) o;

        return Objects.equal(this.mergedTo, that.mergedTo) &&
                Objects.equal(this.getVersion(), that.getVersion());
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(mergedTo, getVersion());
    }
}
