package com.atlassian.jira.issue.customfields;

/**
 * Marker interface for custom field types that holds a text (single or multiline).
 * Field marked with this interface will be validated in {@code IssueManager} against JIRA character limit
 * when an issue is created/updated.
 *
 * @see com.atlassian.jira.issue.IssueManager
 */
public interface TextCustomFieldType
{
}
