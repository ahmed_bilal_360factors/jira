package com.atlassian.jira.issue.customfields;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.PublicSpi;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.LuceneFieldSorter;

/**
 * A custom field searcher will implement this interface if the custom field can be sorted but sorting requires multiple Lucene {@link org.apache.lucene.search.SortField}.
 *
 * It is useful for hierarchical custom fields like CascadingSelectCFType that store parent-child relationship and wish to be sorted by both fields.
 *
 * If a custom field wishes to sort itself, it can use the much slower method {@link SortableCustomField}.
 *
 * @see SortableCustomField
 * @see com.atlassian.jira.issue.customfields.SortableCustomFieldSearcher
 * @see com.atlassian.jira.issue.customfields.NaturallyOrderedCustomFieldSearcher
 */
@ExperimentalApi
@PublicSpi
public interface MultiSortableCustomFieldSearcher
{
    public Iterable<LuceneFieldSorter> getSorters(CustomField customField);
}
