package com.atlassian.jira.bc.workflow;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.opensymphony.workflow.loader.ConditionDescriptor;

/**
 * A container-injectable factory that provides methods to create instance of
 * {@link ConditionDescriptor} to be used in {@link WorkflowTransitionService}.
 *
 * @since v6.4
 */
@Internal
public interface WorkflowConditionDescriptorFactory
{
    /**
     * Creates an instance of {@link ConditionDescriptor} that checks for a project permission.
     *
     * @param key of the project permission to check.
     * @return A {@link ConditionDescriptor} that checks for a project permission.
     */
    ConditionDescriptor permission(ProjectPermissionKey key);
}
