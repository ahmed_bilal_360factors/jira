package com.atlassian.jira.jql.query;

import java.util.List;

import com.atlassian.jira.datetime.LocalDate;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.util.JqlLocalDateSupport;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Provides common date operator specific query factory methods.
 *
 * @since v4.4
 * @deprecated since 6.4 use {@link com.atlassian.jira.jql.util.JqlLocalDateSupport#getLocalDatesFromQueryLiterals(java.util.List)}
 */
@Deprecated
public abstract class AbstractLocalDateOperatorQueryFactory
{
    private final JqlLocalDateSupport jqlLocalDateSupport;

    protected AbstractLocalDateOperatorQueryFactory(JqlLocalDateSupport jqlLocalDateSupport)
    {
        this.jqlLocalDateSupport = notNull("jqlLocalDateSupport", jqlLocalDateSupport);
    }

    /**
     * @deprecated since 6.4 use {@link com.atlassian.jira.jql.util.JqlLocalDateSupport#getLocalDatesFromQueryLiterals(java.util.List)}
     */
    @Deprecated
    List<LocalDate> getLocalDateValues(List<QueryLiteral> rawValues)
    {
        return jqlLocalDateSupport.getLocalDatesFromQueryLiterals(rawValues);
    }
}
