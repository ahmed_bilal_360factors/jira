package com.atlassian.jira.issue.index;

import com.atlassian.annotations.PublicApi;

/**
* Event fired when indexes are deactivated, e.g. during setup or data import
* @since v5.0
*/
@PublicApi
public class IndexDeactivatedEvent implements IndexEvent
{}
