package com.atlassian.jira.issue.fields.screen;

import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenScheme;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeEntity;

/**
 * @since v6.4
 */
public interface FieldScreenFactory
{
    FieldScreen createScreen();

    FieldScreenScheme createFieldScreenScheme();

    FieldScreenSchemeItem createFieldScreenSchemeItem();

    IssueTypeScreenScheme createIssueTypeScreenScheme();

    IssueTypeScreenSchemeEntity createIssueTypeScreenSchemeEntity();
}
