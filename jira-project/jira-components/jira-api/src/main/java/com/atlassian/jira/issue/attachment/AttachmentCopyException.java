package com.atlassian.jira.issue.attachment;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Represents a case where there was an error moving the attachment.
 *
 * @since v6.3
 */
@ExperimentalApi
public class AttachmentCopyException extends AttachmentRuntimeException
{
    public AttachmentCopyException(final String message)
    {
        super(message);
    }

    public AttachmentCopyException(final Throwable cause)
    {
        super(cause);
    }

    public AttachmentCopyException(final String message, final Throwable cause)
    {
        super(message, cause);
    }
}
