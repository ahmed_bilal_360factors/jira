package com.atlassian.jira.exception;

import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;

/**
 * Indicates that comment body exceeded the jira character limit.
 *
 * @since: 6.4.1
 */
@PublicApi
public class CommentBodyCharacterLimitExceededException extends AbstractCharacterLimitExceededException
{

    @Internal
    public CommentBodyCharacterLimitExceededException(final long maxNumberOfCharacters)
    {
        super(maxNumberOfCharacters);
    }


}
