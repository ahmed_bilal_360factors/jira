package com.atlassian.jira.bc.dashboard;

import com.atlassian.annotations.PublicApi;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import static com.google.common.base.Preconditions.checkNotNull;

@Immutable
@PublicApi
final class DashboardItemImpl implements DashboardItem
{
    private final DashboardItemLocator locator;

    public DashboardItemImpl(@Nonnull final DashboardItemLocator locator)
    {
        this.locator = checkNotNull(locator);
    }

    @Override
    public DashboardItemLocator getLocator()
    {
        return locator;
    }

    @Override
    public Long getId()
    {
        return gadgetIdAsLong(locator);
    }

    private Long gadgetIdAsLong(final DashboardItemLocator locator)
    {
        return Long.valueOf(locator.getGadgetId().value());
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final DashboardItemImpl that = (DashboardItemImpl) o;

        if (!locator.equals(that.locator)) { return false; }

        return true;
    }

    @Override
    public int hashCode()
    {
        return locator.hashCode();
    }

    @Override
    public String toString()
    {
        return "DashboardItem (" + locator + ")";
    }
}
