package com.atlassian.jira.tenancy;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Allows you to check if the instance is tenant enabled or not.
 *
 * @since v6.4
 */
@ExperimentalApi
public interface TenancyCondition
{
    boolean isEnabled();
}
