package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.PublicSpi;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginParseException;
import com.google.common.base.Supplier;

import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.jira.plugin.webfragment.conditions.AbstractPermissionCondition.getHasPermissionKey;
import static com.atlassian.jira.plugin.webfragment.conditions.RequestCachingConditionHelper.cacheConditionResultInRequest;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.join;

/**
 * Convenient abstraction to initialise conditions that require the {@link com.atlassian.jira.security.PermissionManager}.
 *
 * @since v6.4
 */
@PublicSpi
@ExperimentalApi
public abstract class AbstractFixedPermissionCondition extends AbstractWebCondition
{
    protected final PermissionManager permissionManager;
    protected final int permission;

    public AbstractFixedPermissionCondition(PermissionManager permissionManager, int permission)
    {
        this.permissionManager = permissionManager;
        this.permission = permission;
    }

    public void init(Map<String,String> params) throws PluginParseException
    {
        super.init(params);
    }

    public boolean shouldDisplay(final ApplicationUser user, JiraHelper jiraHelper)
    {
        return cacheConditionResultInRequest(getHasPermissionKey(permission, user), new Supplier<Boolean>()
        {
            @Override
            public Boolean get()
            {
                return permissionManager.hasPermission(permission, user);
            }
        });
    }

}
