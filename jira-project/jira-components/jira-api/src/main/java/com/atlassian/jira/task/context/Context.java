package com.atlassian.jira.task.context;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.task.TaskProgressSink;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.FixedSized;
import com.atlassian.jira.util.collect.Sized;
import com.atlassian.johnson.event.Event;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Task context. Can be used to provide status updates to clients.
 * <p/>
 * Null is not allowed for any method arguments or return values.
 * <p/>
 * Instances should internally protect any state from concurrent updates and so should
 * require no external synchronisation if shared by multiple threads.
 *
 * @since v3.13
 */
@PublicApi
public interface Context
{
    /**
     * Set the name of the current step.
     *
     * @param name the name
     */
    void setName(String name);

    /**
     * Start a new sub-task. These should be completed in a <pre>finally</pre> block.
     *
     * @param input the object of the task, can be used for tracking currently executing tasks.
     * @return the new Task.
     */
    Task start(Object input);

    /**
     * Returns number of tasks not started yet, after finishing which progress is complete.
     */
    public int getNumberOfTasksToCompletion();

    /**
     * A Task is a unit of work. They should be completed in a <pre>finally</pre> block.
     */
    interface Task
    {
        void complete();
    }

    /**
     * A builder for constructing a background task {@code Context}.
     *
     * @since v6.4
     */
    class Builder
    {
        private final Collection<Sink> sinks = new ArrayList<Sink>(4);
        private Sized sized = new FixedSized(0);

        /**
         * Something that can provide an estimate of the amount of work that the background task will perform.
         * <p>
         * If this method is invoked multiple times, only the last value supplied will be used.
         * </p>
         *
         * @param sized whatever is providing an estimate for the amount of work
         * @return {@code this}
         * @see com.atlassian.jira.util.collect.FixedSized
         * @see com.atlassian.jira.util.collect.CollectionEnclosedIterable
         */
        public Builder sized(@Nonnull Sized sized)
        {
            this.sized = notNull("sized", sized);
            return this;
        }

        /**
         * Specifies a custom sink for the background task's progress.
         *
         * @param sink where to report the task's progress
         * @return {@code this}
         */
        private Builder sink(@Nonnull Sink sink)
        {
            sinks.add(notNull("sink", sink));
            return this;
        }

        /**
         * A global {@code atlassian-johnson} event that will be used to block access to the application.
         * <p>
         * Some activities, like restoring from a backup file, require blocking all access to JIRA.  This passes
         * progress information on to this event when it is supplied to the builder.
         * </p>
         *
         * @param event the global {@code atlassian-johnson} event
         * @return {@code this}
         */
        public Builder event(@Nonnull Event event)
        {
            return sink(new JohnsonEventSink(event));
        }

        /**
         * A convenience method that calls assumes {@link org.apache.log4j.Level#DEBUG DEBUG} logging.
         * It is exactly equivalent to {@link #log(org.apache.log4j.Logger, String, org.apache.log4j.Level) log(log, format, Level.DEBUG)}.
         *
         * @param log as for {@link #log(org.apache.log4j.Logger, String, org.apache.log4j.Level)}
         * @param format as for {@link #log(org.apache.log4j.Logger, String, org.apache.log4j.Level)}
         * @return {@code this}
         */
        public Builder log(@Nonnull Logger log, @Nonnull String format)
        {
            return sink(new LoggingContextSink(log, format, Level.DEBUG));
        }

        /**
         * Provides a logger to which progress will be logged at the specified level.
         *
         * @param log the logger to which progress messages will be logged
         * @param format a message format string, such as for {@link String#format(String, Object...)}.  It will
         *               be called with a single argument that represents the progress percentage; a number between
         *               {@code 0} and {@code 100}, inclusive.
         * @param level the logging level to use
         * @return {@code this}
         */
        public Builder log(@Nonnull Logger log, @Nonnull String format, @Nonnull Level level)
        {
            return sink(new LoggingContextSink(log, format, level));
        }

        /**
         * Uses the specified {@code TaskProgressSink}, using the provided {@code I18nHelper} to supply translated
         * messages about the task.
         *
         * @param progressSink the delegate {@code TaskProgressSink} to which translated messages are relayed
         * @param i18n the I18N helper for formatting the progress messages
         * @param uiMessageKeyPercentage the message key for messages about the progress
         * @param uiMessageKeyCurrent the message key for messages about the current subtask
         * @return {@code this}
         */
        public Builder progress(@Nonnull TaskProgressSink progressSink, @Nonnull I18nHelper i18n,
                @Nonnull String uiMessageKeyPercentage, @Nullable String uiMessageKeyCurrent)
        {
            return sink(new TaskProgressPercentageContextSink(i18n, progressSink, uiMessageKeyPercentage,
                    uiMessageKeyCurrent));
        }

        /**
         * Returns the completed {@code Context}.
         * @return the completed {@code Context}.
         */
        public Context build()
        {
            final CompositeSink sink = new CompositeSink(sinks);
            if (sized != null)
            {
                final int size = sized.size();
                if (size > 0)
                {
                    return new PercentageContext(size, sink);
                }
            }
            return new UnboundContext(sink);
        }
    }
}
