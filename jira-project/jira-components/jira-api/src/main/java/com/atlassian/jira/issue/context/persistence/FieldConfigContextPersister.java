package com.atlassian.jira.issue.context.persistence;

import com.atlassian.annotations.Internal;
import com.atlassian.bandana.BandanaContext;
import com.atlassian.bandana.BandanaPersister;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collection;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

// Should not be in the API - its leaked through FieldConfigScheme.Builder and FieldConfigSchemeImpl
@Internal
public interface FieldConfigContextPersister extends BandanaPersister
{
    /**
     * Retrieve a value by it's context/key tuple.
     * <p>
     * Method definition pulled up from BandanaPersister
     *
     * @deprecated Use {@link #getRelevantConfigSchemeId(com.atlassian.jira.issue.context.IssueContext, String)} instead. Since v6.4.
     */
    Object retrieve(BandanaContext context, String key);

    /**
     * Store a context/key/value triplet.
     * <p>
     * Method definition pulled up from BandanaPersister
     *
     * @deprecated Use {@link #store(String, com.atlassian.jira.issue.context.JiraContextNode, com.atlassian.jira.issue.fields.config.FieldConfigScheme)} instead. Since v6.4.
     */
    void store(BandanaContext context, String key, Object value);

    /**
     * Store a fieldId/contextNode/scheme triplet.
     *
     * @since 6.4
     */
    void store(String fieldId, JiraContextNode contextNode, FieldConfigScheme fieldConfigScheme);

    /**
     * Flush all caches of this persister.
     * <p>
     * Method definition pulled up from BandanaPersister
     */
    void flushCaches();

    /**
     * Remove the storage of data associated with this context
     * <p>
     * Method definition pulled up from BandanaPersister
     *
     * @deprecated Use {@link #removeContextsForConfigScheme(com.atlassian.jira.issue.fields.config.FieldConfigScheme)} instead. Since v6.4.
     */
    void remove(BandanaContext context);

    /**
     * Remove the storage of data associated with this context, under the key
     * <p>
     * Method definition pulled up from BandanaPersister
     *
     * @deprecated Use {@link #removeContextsForConfigScheme(com.atlassian.jira.issue.fields.config.FieldConfigScheme)} instead. Since v6.4.
     */
    void remove(BandanaContext context, String key);

    List<JiraContextNode> getAllContextsForCustomField(String key);

    List<JiraContextNode> getAllContextsForConfigScheme(FieldConfigScheme fieldConfigScheme);

    /**
     * Remove contexts for a scheme
     * @param fieldConfigSchemeId ID of scheme to remove.
     * @deprecated since v6.3 Use {@link #removeContextsForConfigScheme(FieldConfigScheme fieldConfigScheme)}
     */
    @Deprecated
    void removeContextsForConfigScheme(Long fieldConfigSchemeId);

    void removeContextsForConfigScheme(@Nonnull FieldConfigScheme fieldConfigScheme);

    /**
     * Bulk store context/key/value triplets.
     * @since 6.0.7
     *
     * @deprecated Use {@link #store(String, java.util.Collection, com.atlassian.jira.issue.fields.config.FieldConfigScheme)} instead. Since v6.4.
     */
    void store(Collection<? extends BandanaContext> contexts, String key, Object value);

    /**
     * Bulk store fieldId/contextNode/scheme triplets.
     *
     * @since 6.4
     */
    void store(String fieldId, Collection<? extends JiraContextNode> contextNodes, FieldConfigScheme fieldConfigScheme);

    /**
     * @deprecated Use {@link #removeContextsForProject(com.atlassian.jira.project.Project)} instead. Since v5.1.
     * @param project the project
     */
    void removeContextsForProject(GenericValue project);

    void removeContextsForProject(Project project);

    /**
     * @deprecated Project Category contexts are not supported in JIRA and will be removed from the API. Since v6.4.
     */
    void removeContextsForProjectCategory(ProjectCategory projectCategory);

    @Nullable
    Long getRelevantConfigSchemeId(final Project project, @Nonnull final String fieldId, final boolean lookUpParentContexts);

    @Nullable
    Long getRelevantConfigSchemeId(@Nonnull final IssueContext issueContext, @Nonnull final String fieldId);
}
