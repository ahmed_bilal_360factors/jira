package com.atlassian.jira.bulkedit;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.OrderableField;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;

/**
 * Represents the change modes for the multi-select/multiple values system fields (Labels, Versions, Components) to be used in the Bulk Edit Wizard and operation.
 *
 * @since v6.4
 */
@ExperimentalApi
public interface BulkEditMultiSelectFieldOption
{
    /**
     *  Returns the id of the change mode option
     */
    public String getId();

    /**
     *  Returns the i18n key for the name of the change mode option
     */
    public String getNameI18nKey();

    /**
     *  Returns the i18n key for the description of the change mode option
     */
    public String getDescriptionI18nKey();

    /**
     *  Returns the Collection representing the result of the Bulk Edit operation for the option
     *
     *  @param issue
     *  @param field
     *  @param fieldValuesHolder
     *  @return Collection representing the result of the Bulk Edit operation for the option
     */
    public Map<String,Object> getFieldValuesMap(Issue issue, OrderableField field, Map<String, Object> fieldValuesHolder);

    /**
     *  Returns true if the requirement for minimum number of elements for option is fulfilled
     *
     *  @param field
     *  @param fieldValuesHolder
     *  @return true if the requirement for minimum number of elements for option is fulfilled
     */
    public boolean validateOperation(OrderableField field, Map<String,Object> fieldValuesHolder);

    /**
     *  Returns String representing new Components/Versions field values for Project which will be added during bulk operation
     *
     *  @param field
     *  @param fieldValuesHolder
     *  @return String representing new Components/Versions field values for Project which will be added during bulk operation
     *
     */
    public String getFieldValuesToAdd(OrderableField field, Map<String,Object> fieldValuesHolder);
}

