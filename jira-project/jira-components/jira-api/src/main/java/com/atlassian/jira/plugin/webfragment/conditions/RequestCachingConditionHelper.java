package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.google.common.base.Supplier;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

/**
 * Use this class to easily cache result of the condition evaluation for the current request. All context
 * independent condition should use this class to speed up JIRA. Results will be cached using {@link javax.servlet.http.HttpServletRequest#setAttribute(String, Object)}.
 *
 * @since v6.4
 */
@PublicApi
@ExperimentalApi
public class RequestCachingConditionHelper
{
    public static boolean cacheConditionResultInRequest(@Nonnull String cacheKey, @Nonnull Supplier<Boolean> supplier)
    {
        // do not cache during tests
        final HttpServletRequest request = ExecutingHttpRequest.get();
        if (request == null)
        {
            return supplier.get();
        }

        Object cachedConditionResult = request.getAttribute(cacheKey);
        if (cachedConditionResult == null || !(cachedConditionResult instanceof Boolean))
        {
            try
            {
                boolean conditionResult = supplier.get();
                request.setAttribute(cacheKey, conditionResult);
                cachedConditionResult = conditionResult;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        return (Boolean) cachedConditionResult;
    }
}
