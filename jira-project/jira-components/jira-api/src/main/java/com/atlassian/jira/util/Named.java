package com.atlassian.jira.util;

import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since v5.2
 */
public interface Named
{
    String getName();
}
