package com.atlassian.jira.bc.dashboard;

import com.atlassian.annotations.PublicApi;
import com.atlassian.gadgets.GadgetId;
import com.atlassian.gadgets.dashboard.DashboardId;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Objects of this class are used to locate the dashboard items.
 * They contain the dashboard item id and the dashboard id which
 * contains the item.
 */
@Immutable
@PublicApi
public final class DashboardItemLocator
{
    private final DashboardId dashboardId;
    private final GadgetId gadgetId;

    private DashboardItemLocator(@Nonnull final DashboardId dashboardId, @Nonnull final GadgetId gadgetId)
    {
        this.dashboardId = checkNotNull(dashboardId);
        this.gadgetId = checkNotNull(gadgetId);
    }

    public static DashboardItemLocator itemLocator(@Nonnull String dashboardId, @Nonnull String itemId)
    {
        return new DashboardItemLocator(
                DashboardId.valueOf(checkNotNull(dashboardId)),
                GadgetId.valueOf(checkNotNull(itemId)));
    }

    public static DashboardItemLocator itemLocator(@Nonnull final DashboardId dashboardId, @Nonnull final GadgetId itemId)
    {
        return new DashboardItemLocator(dashboardId, itemId);
    }

    public DashboardId getDashboardId()
    {
        return dashboardId;
    }

    public GadgetId getGadgetId()
    {
        return gadgetId;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final DashboardItemLocator that = (DashboardItemLocator) o;

        if (!dashboardId.equals(that.dashboardId)) { return false; }
        if (!gadgetId.equals(that.gadgetId)) { return false; }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = dashboardId.hashCode();
        result = 31 * result + gadgetId.hashCode();
        return result;
    }

    @Override
    public String toString()
    {
        return dashboardId + "/" + gadgetId;
    }
}
