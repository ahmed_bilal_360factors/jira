package com.atlassian.jira.index.request;

import com.atlassian.annotations.PublicApi;

/**
 * Controls when a reindex operation is performed.
 *
 * @since 6.4
 */
@PublicApi
public enum ReindexRequestType
{
    //Note: order of these is important - it affects logic of the coalescer

    /**
     * Performed immediately.
     */
    IMMEDIATE,

    /**
     * Performed some time in the future.
     */
    DELAYED
}
