package com.atlassian.jira.cluster.disasterrecovery;

import java.io.File;
import java.util.Arrays;

import javax.annotation.Nonnull;

import com.atlassian.jira.cluster.disasterrecovery.JiraHomeReplicatorService.ReplicationResult;

/**
 * This event will say that something changed in the filesystem
 * and we can track it or do something about it
 *
 * @since v6.4
 */
public class JiraHomeChangeEvent
{

    public enum Action
    {
        FILE_ADD,
        FILE_DELETED
    }

    public enum FileType
    {
        ATTACHMENT("jira.secondary.store.attachments.enabled")
        {
            @Override
            ReplicationResult replicate(final JiraHomeReplicatorService jiraHomeReplicatorService)
            {
                return jiraHomeReplicatorService.replicateAttachments();
            }
        },
        INDEX_SNAPSHOT("jira.secondary.store.index.snapshots.enabled")
        {
            @Override
            ReplicationResult replicate(final JiraHomeReplicatorService jiraHomeReplicatorService)
            {
                return jiraHomeReplicatorService.replicateIndexSnapshots();
            }
        },

        AVATAR("jira.secondary.store.avatars.enabled")
        {
            @Override
            ReplicationResult replicate(final JiraHomeReplicatorService jiraHomeReplicatorService)
            {
                return jiraHomeReplicatorService.replicateAvatars();
            }

        },

        PLUGIN("jira.secondary.store.plugins.enabled")
        {
            @Override
            ReplicationResult replicate(final JiraHomeReplicatorService jiraHomeReplicatorService)
            {
                return jiraHomeReplicatorService.replicatePlugins();
            }
        };

        private String key;

        FileType(String key)
        {
            this.key = key;
        }

        public String getKey()
        {
            return key;
        }

        abstract ReplicationResult replicate(JiraHomeReplicatorService jiraHomeReplicatorService);
    }

    // The action that occur
    @Nonnull private Action action;

    // The files affected
    @Nonnull private File[] files;

    // The object that changed
    @Nonnull private FileType fileType;

    /**
     * We dispatch a change in the home and the amount of files
     * that changed
     * @param action the action
     * @param files the files (this are absolute paths, we are going to alter the path later)
     */
    public JiraHomeChangeEvent(@Nonnull final Action action, @Nonnull final FileType fileType, @Nonnull final File... files)
    {
        this.action = action;
        this.files = files;
        this.fileType = fileType;
    }

    @Nonnull
    public Action getAction()
    {
        return action;
    }

    @Nonnull
    public File[] getFiles()
    {
        return files;
    }

    @Nonnull
    public FileType getFileType()
    {
        return fileType;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (!(o instanceof JiraHomeChangeEvent)) { return false; }

        final JiraHomeChangeEvent that = (JiraHomeChangeEvent) o;

        if (action != that.action) { return false; }
        if (fileType != that.fileType) { return false; }
        if (!Arrays.equals(files, that.files)) { return false; }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = action.hashCode();
        result = 31 * result + fileType.hashCode();
        return result;
    }
}
