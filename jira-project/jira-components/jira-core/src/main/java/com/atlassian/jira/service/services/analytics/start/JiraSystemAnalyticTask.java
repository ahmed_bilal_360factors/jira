package com.atlassian.jira.service.services.analytics.start;

import java.util.Map;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.service.services.analytics.JiraAnalyticTask;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.system.SystemInfoUtils;

import com.google.common.annotations.VisibleForTesting;

import static com.atlassian.jira.service.services.analytics.start.JiraStartAnalyticEvent.UNKNOWN;


/**
 * Tracks information of the system like : DB, Java Version, variables, etc
 */
public class JiraSystemAnalyticTask implements JiraAnalyticTask
{

    private BuildUtilsInfo info;
    private SystemInfoUtils systemInfoUtils;
    private ClusterManager clusterManager;
    private JiraProperties jiraProperties;

    public JiraSystemAnalyticTask()
    {
    }

    @VisibleForTesting
    JiraSystemAnalyticTask(final BuildUtilsInfo info, final SystemInfoUtils systemInfoUtils, final ClusterManager clusterManager, final JiraProperties jiraProperties)
    {
        this.info = info;
        this.systemInfoUtils = systemInfoUtils;
        this.clusterManager = clusterManager;
        this.jiraProperties = jiraProperties;
    }

    @Override
    public void init()
    {
        info = ComponentAccessor.getComponent(BuildUtilsInfo.class);
        systemInfoUtils = ComponentAccessor.getComponent(SystemInfoUtils.class);
        clusterManager = ComponentAccessor.getComponent(ClusterManager.class);
        jiraProperties = ComponentAccessor.getComponent(JiraProperties.class);
    }

    @Override
    public Map<String, Object> getAnalytics()
    {
        final MapBuilder<String, Object> builder = MapBuilder.newBuilder();

        // Jira Config
        final int[] versionNumbers = info.getVersionNumbers();
        builder.add("release.version", versionNumbers [0]);
        builder.add("major.version", versionNumbers [1]);
        builder.add("minor.version", versionNumbers [2]);
        builder.add("build.number", info.getCurrentBuildNumber());
        builder.add("build.date", String.valueOf(info.getCurrentBuildDate()));

        // DC License
        builder.add("license.dc" , clusterManager.isClusterLicensed());

        // JVM
        builder.add("java.specification.version", jiraProperties.getProperty("java.specification.version"));
        builder.add("java.version", jiraProperties.getProperty("java.version"));
        builder.add("java.vendor", jiraProperties.getProperty("java.vendor"));

        // OS
        builder.add("os.name", jiraProperties.getProperty("os.name", UNKNOWN));
        builder.add("os.version", jiraProperties.getProperty("os.version", UNKNOWN));
        builder.add("os.arch", jiraProperties.getProperty("os.arch", UNKNOWN));


        // DB
        builder.add("database.name", systemInfoUtils.getDatabaseType());

        try
        {
            final SystemInfoUtils.DatabaseMetaData databaseMetaData = systemInfoUtils.getDatabaseMetaData();
            builder.add("database.version", databaseMetaData.getDatabaseProductVersion());
            builder.add("database.driver.version", databaseMetaData.getDriverVersion());
        }
        catch (Exception e)
        {
            // Something went wrong, we don't care
            builder.add("database.version", UNKNOWN);
            builder.add("database.driver.version", UNKNOWN);
        }

        return builder.toMap();
    }

    @Override
    public boolean isReportingDataShape()
    {
        return false;
    }

}
