package com.atlassian.jira.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InternalServerErrorServlet extends HttpServlet
{
    private static final Logger log = LoggerFactory.getLogger(InternalServerErrorServlet.class);

    private void doRequest(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException
    {
        resp.setContentType("text/html");

        InternalServerErrorHelper.render500ResponsePage(req, resp.getWriter());
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException
    {
        doRequest(req, resp);
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException
    {
        doRequest(req, resp);
    }

}
