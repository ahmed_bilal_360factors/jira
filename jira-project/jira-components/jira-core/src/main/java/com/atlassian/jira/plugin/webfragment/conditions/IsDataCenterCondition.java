package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Checks to see if JIRA is DataCenter edition
 *
 * @since v6.4
 */
@SuppressWarnings("unused")
public class IsDataCenterCondition extends AbstractWebCondition
{
    private final ClusterManager clusterManager;

    public IsDataCenterCondition(final ClusterManager clusterManager)
    {
        this.clusterManager = clusterManager;
    }

    @Override
    public boolean shouldDisplay(final ApplicationUser user, final JiraHelper jiraHelper)
    {
        return clusterManager.isClusterLicensed();
    }
}
