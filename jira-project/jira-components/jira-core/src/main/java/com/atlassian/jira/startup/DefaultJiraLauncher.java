package com.atlassian.jira.startup;

import com.atlassian.instrumentation.operations.OpTimerFactory;
import com.atlassian.jdk.utilities.runtimeinformation.RuntimeInformationFactory;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.index.ha.DisasterRecoveryLauncher;
import com.atlassian.jira.instrumentation.Instrumentation;
import com.atlassian.jira.issue.index.DefaultIndexManager;
import com.atlassian.jira.upgrade.ConsistencyCheckImpl;
import com.atlassian.jira.upgrade.PluginSystemLauncher;
import com.atlassian.jira.util.devspeed.JiraDevSpeedTimer;
import com.atlassian.jira.web.ServletContextProvider;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.threadlocal.BruteForceThreadLocalCleanup;
import com.atlassian.threadlocal.RegisteredThreadLocals;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import javax.servlet.ServletContext;

/**
 * This implementation of JiraLauncher contains all of the smarts of what to start in which order to ensure that JIRA
 * starts properly.
 *
 * @since v4.3
 */
public class DefaultJiraLauncher implements JiraLauncher
{
    private static final Logger log = LoggerFactory.getLogger(DefaultJiraLauncher.class);

    private final ChecklistLauncher startupChecklist;
    private final BootstrapContainerLauncher bootstrapContainerLauncher;
    private final ComponentContainerLauncher componentContainerLauncher;
    private final NotificationInstanceKiller notificationInstanceKiller;
    private final DatabaseLauncher databaseLauncher;
    private final PluginSystemLauncher pluginSystemLauncher;
    private final ConsistencyCheckImpl consistencyChecker;
    private final SystemInfoLauncher systemInfoLauncher;
    private final FailedPluginsLauncher preDbFailedPluginsLauncher;
    private final FailedPluginsLauncher postDbFailedPluginsLauncher;
    private final ClusteringLauncher clusteringLauncher;
    private final ClusteringChecklistLauncher clusteringChecklistLauncher;
    private final ActiveServicesLauncher activeServicesLauncher;
    private final JiraProperties jiraSystemProperties;
    private final DisasterRecoveryLauncher disasterRecoveryLauncher;
    private final ReindexRequestCleaner reindexRequestCleaner;
    private final IndexRecoveryLauncher indexRecoveryLauncher;
    private final ImageIOProviderScannerLauncher imageIOProviderScannerLauncher;

    public DefaultJiraLauncher()
    {
        this.jiraSystemProperties = JiraSystemProperties.getInstance();
        this.startupChecklist = new ChecklistLauncher(jiraSystemProperties);
        this.bootstrapContainerLauncher = new BootstrapContainerLauncher();
        this.componentContainerLauncher = new ComponentContainerLauncher();
        this.notificationInstanceKiller = new NotificationInstanceKiller();
        this.databaseLauncher = new DatabaseLauncher(jiraSystemProperties);
        this.pluginSystemLauncher = new PluginSystemLauncher();
        this.consistencyChecker = new ConsistencyCheckImpl();
        this.systemInfoLauncher = new SystemInfoLauncher();
        this.preDbFailedPluginsLauncher = new FailedPluginsLauncher();
        this.postDbFailedPluginsLauncher = new FailedPluginsLauncher();
        this.clusteringLauncher = new ClusteringLauncher();
        this.clusteringChecklistLauncher = new ClusteringChecklistLauncher();
        this.activeServicesLauncher = new ActiveServicesLauncher();
        this.disasterRecoveryLauncher = new DisasterRecoveryLauncher();
        this.reindexRequestCleaner = new ReindexRequestCleaner();
        this.indexRecoveryLauncher = new IndexRecoveryLauncher();
        this.imageIOProviderScannerLauncher = new ImageIOProviderScannerLauncher();
    }

    @Override
    public void start()
    {
        JiraDevSpeedTimer.run(getStartupName(), new Runnable()
        {
            public void run()
            {
                preDbLaunch();
                postDbLaunch();
            }
        });
        DefaultIndexManager.flushThreadLocalSearchers();  // JRA-29587
    }

    /**
     * Things that can or must be done before the database is configured.
     */
    private void preDbLaunch()
    {
        systemInfoLauncher.start();
        bootstrapContainerLauncher.start();
        //JRADEV-17174: We must have a bootstrap container before we do the startup checks
        startupChecklist.start();
        imageIOProviderScannerLauncher.start();
        preDbFailedPluginsLauncher.start();
    }

    /**
     * Those things that need the database to have been configured.
     */
    private void postDbLaunch()
    {
        if (JiraStartupChecklist.startupOK())
        {
            final DatabaseConfigurationManager dbcm = ComponentAccessor.getComponentOfType(DatabaseConfigurationManager.class);
            final ServletContext servletContext = ServletContextProvider.getServletContext();

            dbcm.doNowOrWhenDatabaseConfigured(new Runnable()
            {
                @Override
                public void run()
                {
                    new DatabaseChecklistLauncher(dbcm, servletContext, jiraSystemProperties).start();
                }
            }, "Database Checklist Launcher");

            dbcm.doNowOrWhenDatabaseActivated(new Runnable()
            {
                @Override
                public void run()
                {
                    //JRADEV-20303 If the build number fails get out of Dodge
                    if (!JohnsonEventContainer.get(servletContext).hasEvents())
                    {
                        componentContainerLauncher.start(); //start pico
                        databaseLauncher.start();
                        disasterRecoveryLauncher.earlyStart();
                        clusteringChecklistLauncher.start();
                        clusteringLauncher.start();
                        pluginSystemLauncher.start(); //start Plugin System
                        consistencyChecker.initialise(ServletContextProvider.getServletContext());
                        reindexRequestCleaner.start();
                        activeServicesLauncher.start();
                        notificationInstanceKiller.deleteAfterDelay();
                        disasterRecoveryLauncher.start();
                        indexRecoveryLauncher.start();
                    }
                }

            }, "Post database-configuration launchers");

            postDbFailedPluginsLauncher.start();
        }
    }

    @Override
    public void stop()
    {
        log.info("Stopping launchers");

        ThreadDumper threadDumper = null;

        OpTimerFactory opTimerFactory = null;
        try
        {
            threadDumper = startDumper();
            // We moved the clustering launcher here so the other node receives the signal immediately
            // and the other nodes don't fail while this node is shutting down.
            clusteringLauncher.stop();
            activeServicesLauncher.stop();
            reindexRequestCleaner.stop();
            consistencyChecker.destroy(ServletContextProvider.getServletContext());
            pluginSystemLauncher.stop();
            databaseLauncher.stop();
            imageIOProviderScannerLauncher.stop();
            startupChecklist.stop();
            //after next command PICO dies so we have to cache interesting components
            opTimerFactory = ComponentAccessor.getComponent(OpTimerFactory.class);
            componentContainerLauncher.stop();
            bootstrapContainerLauncher.stop();
            systemInfoLauncher.stop();
            preDbFailedPluginsLauncher.stop();
            postDbFailedPluginsLauncher.stop();
        }
        finally
        {
            if (threadDumper != null)
            {
                threadDumper.cancelDump();
            }
        }

        cleanupAfterOurselves(opTimerFactory);
    }

    private ThreadDumper startDumper()
    {
        ThreadDumper threadDumper = new ThreadDumper(TimeUnit.MINUTES.toMillis(4));

        if(jiraSystemProperties.isDevMode() || jiraSystemProperties.getBoolean("jira.dump"))
        {
            // don't use a threadpool; we want independence from the rest of JIRA's threads.
            Thread thread = new Thread(threadDumper, "Thread-dumping thread from " + this.getClass().getSimpleName());

            thread.setDaemon(true);
            thread.start();
        }
        return threadDumper;
    }

    private void cleanupAfterOurselves(OpTimerFactory opTimerFactory)
    {
        cleanupThreadLocals(opTimerFactory);

        // and shutdown log4j for good measure
        LogManager.shutdown();
    }

    private void cleanupThreadLocals(OpTimerFactory opTimerFactory)
    {
        // we do instrument on the main servlet context thread so we need to clean it up
        Instrumentation.snapshotThreadLocalOperationsAndClear(opTimerFactory);

        // clean up any TL that have done the right thing and registered themselves
        RegisteredThreadLocals.reset();

        // now brute force any other thread locals for this class loader
        BruteForceThreadLocalCleanup.cleanUp(getClass().getClassLoader());
    }

    private String getStartupName()
    {
        final String jvmInputArguments = StringUtils.defaultString(RuntimeInformationFactory.getRuntimeInformation().getJvmInputArguments());
        final boolean rebel = jvmInputArguments.contains("jrebel.jar");
        final boolean debugMode = jvmInputArguments.contains("-Xdebug");

        return "jira.startup" + (debugMode ? ".debug" : ".run") + (rebel ? ".jrebel" : "");
    }
}
