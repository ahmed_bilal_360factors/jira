package com.atlassian.jira.mail;

import java.util.Map;

import javax.annotation.Nullable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.mail.builder.EmailBuilder;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.mail.queue.MailQueueItem;

import com.google.common.collect.Maps;

public class MailServiceQueueItemBuilder
{
    public static final String ISSUE = "issue";
    private final NotificationRecipient recipient;
    private final Map<String, Object> context;
    private final User replyTo;
    private final String subjectTemplatePath;
    private final String bodyTemplatePath;

    public MailServiceQueueItemBuilder(User replyTo, NotificationRecipient recipient, String subjectTemplatePath,
            String bodyTemplatePath, Map<String, Object> context)
    {
        this.replyTo = replyTo;
        this.recipient = recipient;
        this.subjectTemplatePath = subjectTemplatePath;
        this.bodyTemplatePath = bodyTemplatePath;
        this.context = context;
    }

    public MailQueueItem buildQueueItem()
    {
        return createEmailBuilder(replyTo.getEmailAddress()).renderLater();
    }

    public MailQueueItem buildQueueItemUsingProjectEmailAsReplyTo()
    {
        return createEmailBuilder(getProjectEmail()).renderLater();
    }

    protected EmailBuilder createEmailBuilder(String replyToEmail)
    {
        EmailBuilder emailBuilder;
        Email email = new Email(recipient.getEmail());
        email.setFromName(JiraMailUtils.getFromNameForUser(replyTo));
        //when replyTo or From email is null,atlassian mail plugin (EmailMessageCreator) will set it to default outgoing email address before the email is sent.
        email.setReplyTo(replyToEmail);
        if (context.containsKey(ISSUE))
        {
            final Issue issue = (Issue) context.get(ISSUE);
            email.setFrom(JiraMailUtils.getProjectEmailFromIssue(issue));
            IssueEvent issueEvent = new IssueEvent(issue, Maps.newHashMap(), replyTo, 0L);
            emailBuilder = new EmailBuilder(email, recipient, issueEvent);
        }
        else
        {
            email.setFrom(replyTo.getEmailAddress());
            emailBuilder = new EmailBuilder(email, recipient);
        }
        return emailBuilder.withSubjectFromFile(subjectTemplatePath)
                .withBodyFromFile(bodyTemplatePath)
                .addParameters(context);
    }

    @Nullable
    private String getProjectEmail()
    {
        if (!context.containsKey(ISSUE)) {return null;}
        final Issue issue = (Issue) context.get(ISSUE);
        return JiraMailUtils.getProjectEmailFromIssue(issue);

    }
}
