package com.atlassian.jira.index;

import org.apache.lucene.index.Term;


/**
 * Converts provided entity to lucene documents for indexing
 *
 * @since v6.4
 */
public interface RelatedEntityDocumentFactory<T> extends EntityDocumentFactory<T>
{
    Term getIdentifyingTerm(final T entity);
}
