package com.atlassian.jira.service.services.analytics.start;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenSchemeManager;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.service.services.analytics.JiraAnalyticTask;
import com.atlassian.jira.util.collect.MapBuilder;

import com.google.common.annotations.VisibleForTesting;

import org.ofbiz.core.entity.GenericValue;


/**
 * This task will calculate basic counts over objects and will return its values
 */
public class JiraBasicStatsAnalyticTask implements JiraAnalyticTask
{

    private OfBizDelegator ofBizDelegator;
    private ConstantsManager constantsManager;
    private VersionManager versionManager;
    private ProjectComponentManager componentManager;
    private IssueSecurityLevelManager levelManager;
    private FieldScreenManager screenManager;
    private FieldScreenSchemeManager screenSchemeManager;


    public JiraBasicStatsAnalyticTask()
    {
    }

    @VisibleForTesting
    JiraBasicStatsAnalyticTask(final OfBizDelegator ofBizDelegator, final ConstantsManager constantsManager, final VersionManager versionManager,
                               final ProjectComponentManager componentManager, final IssueSecurityLevelManager levelManager, final FieldScreenManager screenManager,
                               final FieldScreenSchemeManager screenSchemeManager)
    {
        this.ofBizDelegator = ofBizDelegator;
        this.constantsManager = constantsManager;
        this.versionManager = versionManager;
        this.componentManager = componentManager;
        this.levelManager = levelManager;
        this.screenManager = screenManager;
        this.screenSchemeManager = screenSchemeManager;
    }

    @Override
    public void init()
    {
        this.ofBizDelegator = ComponentAccessor.getOfBizDelegator();
        this.constantsManager = ComponentAccessor.getConstantsManager();
        this.versionManager = ComponentAccessor.getVersionManager();
        this.componentManager = ComponentAccessor.getProjectComponentManager();
        this.levelManager = ComponentAccessor.getIssueSecurityLevelManager();
        this.screenManager = ComponentAccessor.getFieldScreenManager();
        this.screenSchemeManager = ComponentAccessor.getComponent(FieldScreenSchemeManager.class);
    }

    @Override
    public Map<String, Object> getAnalytics()
    {
        final MapBuilder<String, Object> builder = MapBuilder.newBuilder();

        builder.add("issues", ofBizDelegator.getCount(Entity.Name.ISSUE));
        builder.add("projects", ofBizDelegator.getCount(Entity.Name.PROJECT));
        builder.add("comments", ofBizDelegator.getCount(Entity.Name.COMMENT));
        builder.add("customfields", ofBizDelegator.getCount(CustomField.ENTITY_TABLE_NAME));

        builder.add("issuetypes", constantsManager.getAllIssueTypeIds().size());
        builder.add("status", size(constantsManager.getStatusObjects()));
        builder.add("resolutions", constantsManager.getResolutionObjects().size());
        builder.add("priorities", constantsManager.getPriorityObjects().size());

        builder.add("versions", size(versionManager.getAllVersions()));
        builder.add("components", size(componentManager.findAll()));

        builder.add("issuesecuritylevels", size(levelManager.getAllIssueSecurityLevels()));

        builder.add("screens", size(screenManager.getFieldScreens()));
        builder.add("screensschemes", size(screenSchemeManager.getFieldScreenSchemes()));

        buildPermissionSchemeInfo(builder);

        return builder.toMap();
    }

    @Override
    public boolean isReportingDataShape()
    {
        return true;
    }

    /**
     * We retrieve the PermissionSchemeCount and get the average
     *
     * @param builder the builder
     */
    private void buildPermissionSchemeInfo(final MapBuilder<String, Object> builder)
    {
        final List<GenericValue> values = Select.columns("scheme", "count").from("PermissionSchemeCount").
                runWith(ofBizDelegator).asList();

        int permissions = values.size();
        long total = 0;

        for (GenericValue value : values)
        {
            // We have some scheme as "null" that we don't want that info.
            if (value.getString("scheme") == null)
            {
                permissions--;
                continue;
            }

            total += value.getLong("count");
        }

        builder.add("permission.scheme.avg", (int) total / permissions);
        builder.add("permissionschemes", permissions);
    }

    /**
     * Simple wrapper for safe NullPointerException
     *
     * @param collection the collection
     * @return 0 if null , or the value if exists
     */
    private int size(final Collection collection)
    {
        return collection != null ? collection.size() : 0;
    }
}
