package com.atlassian.jira.log;

import org.apache.log4j.Appender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

/**
 * @since v6.4.2
 */
public class LoggingConfigurationImpl implements LoggingConfiguration
{
    private static final String DEFAULT_PATTERN = "%d %t %p %X{jira.username} %X{jira.request.id} %X{jira.request.assession.id} %X{jira.request.ipaddr} %X{jira.request.url} [%c{4}] %m%n";

    public PatternLayout getSystemPatternLayout()
    {
        Appender filelog = Logger.getRootLogger().getAppender("filelog");
        if (filelog != null && filelog instanceof PatternLayout)
        {
            return (PatternLayout) filelog.getLayout();
        }
        return new PatternLayout(DEFAULT_PATTERN);
    }
}
