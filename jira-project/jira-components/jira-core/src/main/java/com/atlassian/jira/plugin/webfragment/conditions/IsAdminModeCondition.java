package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Checks if we're in admin mode
 */
public class IsAdminModeCondition extends AbstractWebCondition
{
    public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper)
    {
        return jiraHelper.getRequest().getAttribute("jira.admin.mode") != null;
    }
}
