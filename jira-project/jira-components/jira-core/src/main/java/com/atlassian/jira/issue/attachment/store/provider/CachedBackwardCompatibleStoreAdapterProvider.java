package com.atlassian.jira.issue.attachment.store.provider;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.FeatureEvent;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.issue.attachment.store.BackwardCompatibleStoreAdapter;
import com.atlassian.jira.web.action.admin.EditAttachmentSettingsEvent;
import com.atlassian.util.concurrent.ResettableLazyReference;

public class CachedBackwardCompatibleStoreAdapterProvider implements Startable
{
    private final EventPublisher eventPublisher;
    private final ResettableLazyReference<BackwardCompatibleStoreAdapter> adapter;

    public CachedBackwardCompatibleStoreAdapterProvider(
            final BackwardCompatibleStoreAdapterProvider backwardCompatibleStoreAdapterProvider,
            final EventPublisher eventPublisher)
    {
        this.eventPublisher = eventPublisher;
        adapter = new ResettableLazyReference<BackwardCompatibleStoreAdapter>()
        {
            @Override
            protected BackwardCompatibleStoreAdapter create() throws Exception
            {
                return backwardCompatibleStoreAdapterProvider.provide();
            }
        };
    }

    public BackwardCompatibleStoreAdapter provide()
    {
        return adapter.get();
    }

    @Override
    public void start() throws Exception
    {
        eventPublisher.register(this);
    }

    @EventListener
    public void onDarkFeatureEvent(final FeatureEvent featureEvent)
    {
        resetReference();
    }
    @EventListener
    public void onEditAttachmentSettings(final EditAttachmentSettingsEvent editAttachmentSettingsEvent)
    {
        resetReference();
    }

    private void resetReference()
    {
        adapter.reset();
    }
}
