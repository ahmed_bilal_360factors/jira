package com.atlassian.jira.issue.index;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.AbstractJiraHome;
import com.atlassian.jira.config.util.IndexWriterConfiguration;
import com.atlassian.jira.config.util.IndexingConfiguration;
import com.atlassian.jira.index.Configuration;
import com.atlassian.jira.index.DefaultConfiguration;
import com.atlassian.jira.index.Index;
import com.atlassian.jira.index.Indexes;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.SearchProviderFactoryImpl;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.issue.search.parameters.lucene.PermissionsFilterGenerator;
import com.atlassian.jira.issue.search.providers.LuceneSearchProvider;
import com.atlassian.jira.issue.search.util.SearchSortUtil;
import com.atlassian.jira.issue.util.IssueObjectIssuesIterable;
import com.atlassian.jira.jql.query.LuceneQueryBuilder;
import com.atlassian.jira.task.context.Contexts;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.apache.lucene.store.RAMDirectory;

/**
 *
 *
 * @since v6.4
 */
public class DefaultTemporaryIndexProvider implements TemporaryIndexProvider
{
    private final IndexWriterConfiguration writerConfiguration;
    private final ApplicationProperties applicationProperties;
    private final SearchHandlerManager searchHandlerManager;
    private final IssueFactory issueFactory;
    private final PermissionsFilterGenerator permissionsFilterGenerator;
    private final SearchSortUtil searchSortUtil;
    private final LuceneQueryBuilder luceneQueryBuilder;
    private final DefaultIssueIndexer.CommentRetriever commentRetriever;
    private final DefaultIssueIndexer.ChangeHistoryRetriever changeHistoryRetriever;
    private final DefaultIssueIndexer.WorklogRetriever worklogRetriever;
    private final IssueDocumentFactory issueDocumentFactory;
    private final CommentDocumentFactory commentDocumentFactory;
    private final ChangeHistoryDocumentFactory changeHistoryDocumentFactory;
    private final WorklogDocumentFactory worklogDocumentFactory;

    public DefaultTemporaryIndexProvider(final IndexWriterConfiguration writerConfiguration, final ApplicationProperties applicationProperties,
            final SearchHandlerManager searchHandlerManager,
            final IssueFactory issueFactory,
            final PermissionsFilterGenerator permissionsFilterGenerator,
            final SearchSortUtil searchSortUtil, final LuceneQueryBuilder luceneQueryBuilder,
            final DefaultIssueIndexer.CommentRetriever commentRetriever,
            final DefaultIssueIndexer.ChangeHistoryRetriever changeHistoryRetriever,
            final DefaultIssueIndexer.WorklogRetriever worklogRetriever,
            final IssueDocumentFactory issueDocumentFactory,
            final CommentDocumentFactory commentDocumentFactory,
            final ChangeHistoryDocumentFactory changeHistoryDocumentFactory,
            final WorklogDocumentFactory worklogDocumentFactory)
    {
        this.writerConfiguration = writerConfiguration;
        this.applicationProperties = applicationProperties;
        this.searchHandlerManager = searchHandlerManager;
        this.issueFactory = issueFactory;
        this.permissionsFilterGenerator = permissionsFilterGenerator;
        this.searchSortUtil = searchSortUtil;
        this.luceneQueryBuilder = luceneQueryBuilder;
        this.commentRetriever = commentRetriever;
        this.changeHistoryRetriever = changeHistoryRetriever;
        this.worklogRetriever = worklogRetriever;
        this.issueDocumentFactory = issueDocumentFactory;
        this.commentDocumentFactory = commentDocumentFactory;
        this.changeHistoryDocumentFactory = changeHistoryDocumentFactory;
        this.worklogDocumentFactory = worklogDocumentFactory;
    }

    @Override
    @Nullable
    public <T> T indexIssuesAndSearch(@Nonnull Collection<? extends Issue> issues, @Nonnull IndexSearcher<T> indexSearcher)
            throws SearchException
    {
        final RamDirectoryFactory indexDirectoryFactory = new RamDirectoryFactory();
        try
        {

            final DefaultIssueIndexer issueIndexer = new DefaultIssueIndexer(indexDirectoryFactory, commentRetriever, changeHistoryRetriever, worklogRetriever,
                    applicationProperties, issueDocumentFactory, commentDocumentFactory, changeHistoryDocumentFactory, worklogDocumentFactory);
            try
            {
                final IndexingConfiguration indexingConfiguration = new IndexingConfiguration.PropertiesAdapter(applicationProperties);
                final SearcherCache searcherCache = new SearcherCache();
                try
                {
                    final SearchProviderFactory searchProviderFactory = new SearchProviderFactoryImpl(new IssueSearcherFactory()
                    {
                        @Nonnull
                        @Override
                        public org.apache.lucene.search.IndexSearcher getEntitySearcher(final IndexDirectoryFactory.Name index)
                        {
                            return searcherCache.retrieveEntitySearcher(issueIndexer, indexingConfiguration, index);
                        }
                    });

                    issueIndexer.reindexIssues(
                            new IssueObjectIssuesIterable(issues), Contexts.nullContext(), IssueIndexingParams.INDEX_ALL, false)
                            .await();

                    final LuceneSearchProvider searchProvider = new LuceneSearchProvider(issueFactory, searchProviderFactory,
                            permissionsFilterGenerator, searchHandlerManager, searchSortUtil, luceneQueryBuilder);

                    return indexSearcher.search(searchProvider);
                }
                finally
                {
                    // you need to make sure all searchers are closed
                    searcherCache.closeSearchers();
                }
            }
            finally
            {
                issueIndexer.shutdown();
            }
        }
        finally
        {
            for (RAMDirectory dir : indexDirectoryFactory.getDirectories())
            {
                dir.close();
            }
        }
    }

    protected IndexingMode getDirectFlushingIndexingMode()
    {
        return new IndexingMode() {
            @Nonnull
            @Override
            public Index.Manager createIndexManager(final String name, final Configuration configuration, final ApplicationProperties applicationProperties)
            {
                // We never need to flush this index.
                // As we use an NRT reader the issues are all visible from the very start.
                return Indexes.createSimpleIndexManager(configuration);
            }
        };
    }

    @Nonnull
    protected AbstractJiraHome getJiraHome(@Nonnull final File homeDir)
    {
        return new AbstractJiraHome()
        {
            @Nonnull
            @Override
            public File getHome()
            {
                return getLocalHome();
            }

            @Nonnull
            @Override
            public File getLocalHome()
            {
                return homeDir;
            }
        };
    }

    private class RamDirectoryFactory implements IndexDirectoryFactory
    {
        private IndexingMode mode = getDirectFlushingIndexingMode();
        private  List<RAMDirectory> directories = ImmutableList.of();

        @Override
        public String getIndexRootPath()
        {
            return null;
        }

        @Override
        public List<String> getIndexPaths()
        {
            return ImmutableList.of();
        }

        public List<RAMDirectory> getDirectories()
        {
            return directories;
        }

        @Override
        public void setIndexingMode(@Nonnull final IndexingMode mode)
        {
            this.mode = mode;
        }

        @Override
        public Map<Name, Index.Manager> get()
        {
            final ImmutableList.Builder<RAMDirectory> directoriesBuilder = new ImmutableList.Builder<RAMDirectory>();

            final ImmutableMap.Builder<Name, Index.Manager> indexes = new ImmutableMap.Builder<Name, Index.Manager>();
            for (final Name type : Name.values())
            {
                RAMDirectory directory = new RAMDirectory();
                indexes.put(type, mode.createIndexManager(type.name(), new DefaultConfiguration(directory,
                        IssueIndexer.Analyzers.INDEXING, writerConfiguration), applicationProperties));
                directoriesBuilder.add(directory);
            }
            directories = directoriesBuilder.build();
            return indexes.build();
        }
    }
}
