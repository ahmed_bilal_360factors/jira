package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;

/**
 * Will return shouldDisplay of true if the user has the {@link com.atlassian.jira.security.Permissions#SYSTEM_ADMIN}
 * global permission.
 *
 * @since v3.12
 */
public class UserIsSysAdminCondition extends AbstractFixedPermissionCondition
{
    public UserIsSysAdminCondition(PermissionManager permissionManager)
    {
        super(permissionManager, Permissions.SYSTEM_ADMIN);
    }
}
