package com.atlassian.jira.license;

import com.atlassian.fugue.Option;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import org.ofbiz.core.entity.GenericValue;

import java.util.List;
import java.util.Set;
import javax.annotation.Nonnull;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since v6.4
 */
public class OfBizLicenseRoleStore implements LicenseRoleStore
{
    private static final String ENTITY = "LicenseRoleGroup";

    private static class Columns
    {
        private static final String LICENSE_ROLE_NAME = "licenseRoleName";
        private static final String GROUP_ID = "groupId";
        private static final String PRIMARY = "primaryGroup";
    }

    private final OfBizDelegator delegator;

    public OfBizLicenseRoleStore(@Nonnull final OfBizDelegator delegator)
    {
        this.delegator = notNull("delegator", delegator);
    }

    @Nonnull
    @Override
    public LicenseRoleData get(@Nonnull final LicenseRoleId id)
    {
        notNull("id", id);

        final List<GenericValue> entries = getById(id);
        final Set<String> groups = Sets.newHashSetWithExpectedSize(entries.size());

        String primaryGroup = null;
        for (GenericValue entry : entries)
        {
            final String groupName = entry.getString(Columns.GROUP_ID);
            groups.add(groupName);

            if (isPrimary(entry))
            {
                if (primaryGroup == null || primaryGroup.compareTo(groupName) > 0)
                {
                    primaryGroup = groupName;
                }
            }
        }

        return new LicenseRoleData(id, groups, Option.option(primaryGroup));
    }

    @Nonnull
    @Override
    public LicenseRoleData save(@Nonnull final LicenseRoleData data)
    {
        notNull("data", data);

        //Delete all the current entries.
        final String wantedDefault = data.getPrimaryGroup().getOrNull();
        final List<GenericValue> entries = getById(data.getId());
        final Set<String> groups = Sets.newHashSet(data.getGroups());

        for (GenericValue entry : entries)
        {
            final String groupName = entry.getString(Columns.GROUP_ID);
            if (groups.remove(groupName))
            {
                //Group is already in the database. Just check the primary value.
                if (isPrimary(entry))
                {
                    //Unset as primary if it is no longer the primary.
                    if (wantedDefault == null || !wantedDefault.equals(groupName))
                    {
                        entry.set(Columns.PRIMARY, false);
                        save(entry);
                    }
                }
                else
                {
                    //Set as primary if it is now the primary.
                    if (wantedDefault != null && wantedDefault.equals(groupName))
                    {
                        entry.set(Columns.PRIMARY, true);
                        save(entry);
                    }
                }
            }
            else
            {
                //Group not in the wanted values so delete it.
                remove(entry);
            }
        }

        //Any groups left over not currently stored in the database.
        final String name = data.getId().getName();
        for (String group : groups)
        {
            boolean primary = wantedDefault != null && wantedDefault.equals(group);
            delegator.createValue(ENTITY, ImmutableMap.<String, Object>of(
                    Columns.LICENSE_ROLE_NAME, name,
                    Columns.GROUP_ID, group,
                    Columns.PRIMARY, primary));
        }

        return get(data.getId());
    }

    @Override
    public void removeGroup(@Nonnull String groupName)
    {
        // note: this implementation makes no effort to reassign the primary group of the role if
        // the deleted group was that role's primary group, as the choice would necessarily be arbitrary.
        delegator.removeByAnd(ENTITY, ImmutableMap.of(Columns.GROUP_ID, groupName));
    }

    private void save(final GenericValue entry)
    {
        delegator.store(entry);
    }

    private void remove(final GenericValue entry)
    {
        delegator.removeValue(entry);
    }

    private List<GenericValue> getById(final LicenseRoleId id)
    {
        return delegator.findByAnd(ENTITY,
                ImmutableMap.of(Columns.LICENSE_ROLE_NAME, id.getName()));
    }

    //The primary can be null on upgrade from 6.3 to 7.0.
    private boolean isPrimary(GenericValue value)
    {
        final Boolean primary = value.getBoolean(Columns.PRIMARY);
        return primary != null && primary;
    }
}
