package com.atlassian.jira.index.request;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.ServletContext;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.core.util.Clock;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.Node;
import com.atlassian.jira.config.BackgroundIndexTaskContext;
import com.atlassian.jira.config.ForegroundIndexTaskContext;
import com.atlassian.jira.config.IndexTaskContext;
import com.atlassian.jira.issue.index.IssueIndexingParams;
import com.atlassian.jira.task.ProvidesTaskProgress;
import com.atlassian.jira.task.TaskDescriptor;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.task.TaskProgressSink;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.index.IndexLifecycleManager;
import com.atlassian.jira.web.ServletContextProvider;
import com.atlassian.jira.web.action.admin.index.ActivateAsyncIndexerCommand;
import com.atlassian.jira.web.action.admin.index.IndexCommandResult;
import com.atlassian.jira.web.action.admin.index.ReIndexBackgroundIndexerCommand;
import com.atlassian.johnson.JohnsonEventContainer;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Objects;
import com.google.common.base.Predicate;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import org.apache.log4j.Logger;

/**
 * @since 6.4
 */
public class DefaultReindexRequestManager implements ReindexRequestManager
{
    private static final Logger log = Logger.getLogger(DefaultReindexRequestManager.class);
    private static final String PROCESS_REQUESTS_LOCK = DefaultReindexRequestManager.class.getName() + ".processRequests";

    private final ReindexRequestDao reindexRequestDao;
    private final Clock clock;
    private final ReindexRequestCoalescer requestCoalescer;
    private final ClusterManager clusterManager;
    private final ClusterLockService clusterLockService;
    private final TaskManager taskManager;
    private final TaskDescriptorHelper taskDescriptorHelper;
    private final IndexLifecycleManager indexLifecycleManager;
    private final I18nHelper.BeanFactory i18nFactory;
    private final I18nHelper i18nHelper;

    public DefaultReindexRequestManager(ReindexRequestDao reindexRequestDao, Clock clock, ReindexRequestCoalescer requestCoalescer,
            ClusterManager clusterManager, final ClusterLockService clusterLockService, final TaskManager taskManager,
            final IndexLifecycleManager indexLifecycleManager, final I18nHelper.BeanFactory i18nFactory, final I18nHelper i18nHelper)
    {
        this.reindexRequestDao = reindexRequestDao;
        this.clock = clock;
        this.requestCoalescer = requestCoalescer;
        this.clusterManager = clusterManager;
        this.clusterLockService = clusterLockService;
        this.taskManager = taskManager;
        this.indexLifecycleManager = indexLifecycleManager;
        this.i18nFactory = i18nFactory;
        this.i18nHelper = i18nHelper;
        this.taskDescriptorHelper = new TaskDescriptorHelper(taskManager);
    }

    private ReindexRequest readFullRequest(ReindexRequestBase baseRequest)
    {
        final List<ReindexComponent> components = reindexRequestDao.getComponentsForRequest(baseRequest.getId());

        final EnumSet<AffectedIndex> affectedIndexes = EnumSet.noneOf(AffectedIndex.class);
        final EnumSet<SharedEntityType> entityTypes = EnumSet.noneOf(SharedEntityType.class);
        for (ReindexComponent component : components)
        {
            if (component.getAffectedIndex() == AffectedIndex.SHAREDENTITY)
            {
                entityTypes.add(component.getEntityType());
            }
            affectedIndexes.add(component.getAffectedIndex());
        }
        return new ReindexRequest(baseRequest, affectedIndexes, entityTypes);
    }

    @Override
    public boolean isReindexRequested()
    {
        return isReindexRequested(EnumSet.allOf(ReindexRequestType.class));
    }

    @Override
    public boolean isReindexRequested(final Set<ReindexRequestType> reindexRequestTypes)
    {
        return getPendingReindexRequests(reindexRequestTypes).iterator().hasNext();
    }

    @Nonnull
    @Override
    public Iterable<ReindexRequest> getPendingReindexRequests(@Nonnull final Set<ReindexRequestType> reindexRequestTypes)
    {
        return readPendingReindexRequests(reindexRequestTypes, false);
    }

    @Nonnull
    public Iterable<ReindexRequest> readPendingReindexRequests(@Nonnull final Set<ReindexRequestType> reindexRequestTypes, boolean updateCoalescedRequests)
    {
        final List<ReindexRequestBase> baseRequests = reindexRequestDao.getRequestsWithStatus(ReindexStatus.PENDING);
        final ImmutableList.Builder<ReindexRequest> requestListBuilder = ImmutableList.builder();
        for (ReindexRequestBase baseRequest : baseRequests)
        {
            ReindexRequest fullRequest = readFullRequest(baseRequest);
            requestListBuilder.add(fullRequest);
        }

        List<ReindexRequest> requests = requestListBuilder.build();
        List<ReindexRequest> coalescedRequests = requestCoalescer.coalesce(requests);

        //The coalescer might have made changes to existing tasks, need to persist changes to modified tasks
        if (updateCoalescedRequests)
        {
            //Detect changes and persist them
            Map<Long, ReindexRequest> coalescedRequestsById = new HashMap<Long, ReindexRequest>();
            buildRequestMap(coalescedRequests, coalescedRequestsById);

            for (ReindexRequest originalRequest : requests)
            {
                ReindexRequest requestAfterCoalesce = coalescedRequestsById.get(originalRequest.getId());
                if (!originalRequest.getAffectedIndexes().equals(requestAfterCoalesce.getAffectedIndexes()) ||
                    !originalRequest.getSharedEntities().equals(requestAfterCoalesce.getSharedEntities()))
                {
                    reindexRequestDao.removeComponents(requestAfterCoalesce.getId());

                    for (AffectedIndex affectedIndex : requestAfterCoalesce.getAffectedIndexes())
                    {
                        reindexRequestDao.writeComponent(new ReindexComponent(null, requestAfterCoalesce.getId(), affectedIndex, SharedEntityType.NONE));
                    }
                    for (SharedEntityType entityType : requestAfterCoalesce.getSharedEntities())
                    {
                        reindexRequestDao.writeComponent(new ReindexComponent(null, requestAfterCoalesce.getId(), AffectedIndex.SHAREDENTITY, entityType));
                    }
                }
            }
        }

        return Iterables.filter(coalescedRequests, new Predicate<ReindexRequest>()
        {
            @Override
            public boolean apply(final ReindexRequest input)
            {
                return reindexRequestTypes.contains(input.getType());
            }
        });
    }

    private void buildRequestMap(Iterable<ReindexRequest> requests, Map<? super Long, ? super ReindexRequest> requestMap)
    {
        for (ReindexRequest request : requests)
        {
            if (request.getId() != null)
            {
                requestMap.put(request.getId(), request);
            }

            buildRequestMap(request.getSources(), requestMap);
        }
    }

    @Override
    public Set<ReindexRequest> processPendingRequests(boolean waitForCompletion, Set<ReindexRequestType> reindexRequestTypes,
            boolean runInBackground)
    {
        Iterable<ReindexRequest> requests;

        //What happens if both read pending requests and each run them?  Cluster locks stops this from happening.
        ClusterLock lock = clusterLockService.getLockForName(PROCESS_REQUESTS_LOCK);
        lock.lock();
        try
        {
            if (isReindexInProgress())
            {
                throw new IllegalStateException("Indexing already in progress.");
            }

            requests = readPendingReindexRequests(reindexRequestTypes, true);

            //Do this inside the lock - needed so that a race condition for two requests checking for pending cannot occur
            requests = transitionStatus(requests, ReindexStatus.ACTIVE);
        }
        finally
        {
            lock.unlock();
        }

        try
        {
            //Requests have been coalesced already, now run reindex on each one
            requests = performReindex(requests, waitForCompletion, runInBackground);

            //If we get here, we have a normal run and the requests will have their statuses set to complete in the DB
        }
        catch (Throwable e)
        {
            log.error("Error occured performing reindex: " + e, e);

            //Transition requests from running to failed, which also clears up state so that the next request can come in and try again
            //This only occurs for serious errors, not errors in the reindexer itself (which is caught inside performReindex())
            //But we want to be absolutely sure that the state is cleared or else no one will be able to reindex at all any more
            //(stuck reindex tasks)
            requests = transitionStatus(requests, ReindexStatus.FAILED);

            Throwables.propagate(e);
        }

        return ImmutableSet.copyOf(requests);
    }

    @Nonnull
    private List<ReindexRequest> performReindex(@Nonnull Iterable<ReindexRequest> requests, boolean waitForCompletion, boolean runInBackground)
            throws InterruptedException
    {
        final List<ReindexRequest> resultList = new ArrayList<ReindexRequest>();

        for (ReindexRequest request : requests)
        {
            resultList.add(performReindex(request, waitForCompletion, runInBackground));
        }

        return resultList;
    }

    @Nonnull
    @VisibleForTesting
    protected ReindexRequest performReindex(@Nonnull ReindexRequest request, boolean waitForCompletion, boolean runInBackground)
            throws InterruptedException
    {
        return reindex(request, waitForCompletion, runInBackground);
    }

    private ReindexRequest reindex(final ReindexRequest request, boolean waitForTaskCompletion, boolean runInBackground)
            throws InterruptedException
    {
        TaskDescriptor<IndexCommandResult> task;

        //Don't allow a global reindex (from other reindex endpoint) and one from here to run at the same time
        task = taskDescriptorHelper.getActiveIndexTask();
        if (task != null)
        {
            //If the user kicked off another reindex through, say, the REST interface
            //we don't want to fail but rather just wait for it to finish before we run our own one
            log.warn("Indexing task already in progress, waiting for current task to complete.");
            try
            {
                taskDescriptorHelper.waitForTaskCompletion(task);
            }
            catch (ExecutionException e)
            {
                throw new RuntimeException(e);
            }
        }
        if (runInBackground)
        {
            task = triggerBackgroundIndexing(request);
        }
        else
        {
            task = triggerForegroundIndexing(request);
        }

        if (waitForTaskCompletion)
        {
            try
            {
                taskDescriptorHelper.waitForTaskCompletion(task);
            }
            catch (ExecutionException e)
            {
                throw new RuntimeException(e);
            }
        }

        return getReindexProgress(request);
    }

    private TaskDescriptor<IndexCommandResult> triggerBackgroundIndexing(ReindexRequest request)
    {
        IssueIndexingParams issueIndexingParams = IssueIndexingParams.builder()
                .setChangeHistory(request.getAffectedIndexes().contains(AffectedIndex.CHANGEHISTORY))
                .setComments(request.getAffectedIndexes().contains(AffectedIndex.COMMENT))
                .setIssues(request.getAffectedIndexes().contains(AffectedIndex.ISSUE))
                .setWorklogs(request.getAffectedIndexes().contains(AffectedIndex.WORKLOG))
                .build();

        return submitIndexingTask(new ReIndexBackgroundIndexerCommand(indexLifecycleManager, issueIndexingParams, log, i18nHelper, i18nFactory),
                new BackgroundIndexTaskContext(), true, request);
    }

    private TaskDescriptor<IndexCommandResult> triggerForegroundIndexing(ReindexRequest request)
    {
        return submitIndexingTask(new ActivateAsyncIndexerCommand(true, getJohnsonEventContainer(), indexLifecycleManager, log,
                        i18nHelper, i18nFactory),
                new ForegroundIndexTaskContext(), false, request);
    }

    private <T extends Callable<IndexCommandResult> & ProvidesTaskProgress> TaskDescriptor<IndexCommandResult> submitIndexingTask(T cmd, final IndexTaskContext indexTaskContext, boolean cancellable,
            ReindexRequest request)
    {
        return taskManager.submitTask(new RunReindexAndUpdateRequestTask<T>(cmd, request), i18nHelper.getText("admin.indexing.jira.indexing"), indexTaskContext, cancellable);
    }

    private JohnsonEventContainer getJohnsonEventContainer()
    {
        final ServletContext ctx = ServletContextProvider.getServletContext();
        if (ctx != null)
        {
            return JohnsonEventContainer.get(ctx);
        }
        return null;
    }


    private List<ReindexRequestBase> getActiveOrRunningRequests()
    {
        List<ReindexRequestBase> requestsToCheck = new ArrayList<ReindexRequestBase>();
        requestsToCheck.addAll(reindexRequestDao.getRequestsWithStatus(ReindexStatus.RUNNING));
        requestsToCheck.addAll(reindexRequestDao.getRequestsWithStatus(ReindexStatus.ACTIVE));
        return requestsToCheck;
    }

    @Nonnull
    @Override
    public List<ReindexRequest> failRunningRequestsFromDeadNodes()
    {
        if (!clusterManager.isClustered())
        {
            return Collections.emptyList();
        }

        List<ReindexRequestBase> requestsToCheck = getActiveOrRunningRequests();
        if (requestsToCheck.isEmpty())
        {
            return Collections.emptyList();
        }

        Set<String> liveNodeIds = new HashSet<String>();
        for (Node liveNode : clusterManager.findLiveNodes())
        {
            liveNodeIds.add(liveNode.getNodeId());
        }

        List<ReindexRequest> requestsFromDeadNodes = new ArrayList<ReindexRequest>();
        for (ReindexRequestBase request : requestsToCheck)
        {
            final String executionNodeId = request.getExecutionNodeId();
            if (!liveNodeIds.contains(executionNodeId))
            {
                log.warn("Detected active reindex request " + request.getId() + " on inactive node " + executionNodeId + ", marking as failed.  This reindex task should be re-run.");
                requestsFromDeadNodes.add(readFullRequest(request));
            }
        }

        if (!requestsFromDeadNodes.isEmpty())
        {
            return transitionStatus(requestsFromDeadNodes, ReindexStatus.FAILED);
        }
        else
        {
            return Collections.emptyList();
        }
    }

    @Nonnull
    @Override
    public List<ReindexRequest> failRunningRequestsFromNode(@Nullable String nodeId)
    {
        List<ReindexRequestBase> requestsToCheck = getActiveOrRunningRequests();
        if (requestsToCheck.isEmpty())
        {
            return Collections.emptyList();
        }

        List<ReindexRequest> requestsFromDeadNodes = new ArrayList<ReindexRequest>();
        for (ReindexRequestBase request : requestsToCheck)
        {
            final String executionNodeId = request.getExecutionNodeId();
            if (Objects.equal(nodeId, executionNodeId))
            {
                log.warn("Detected active reindex request " + request.getId() + " on node " + executionNodeId + ", marking as failed.  This reindex task should be re-run.");
                requestsFromDeadNodes.add(readFullRequest(request));
            }
        }

        if (!requestsFromDeadNodes.isEmpty())
        {
            return transitionStatus(requestsFromDeadNodes, ReindexStatus.FAILED);
        }
        else
        {
            return Collections.emptyList();
        }
    }


    @Override
    public boolean isReindexInProgress()
    {
        //There may be a case where there are tasks from other nodes that did not finish and the nodes were taken
        //down uncleanly.  At this point, detect these and filter them out
        failRunningRequestsFromDeadNodes();

        return !reindexRequestDao.getRequestsWithStatus(ReindexStatus.RUNNING).isEmpty() || !reindexRequestDao.getRequestsWithStatus(ReindexStatus.ACTIVE).isEmpty();
    }

    @Override
    @Nonnull
    public ReindexRequest requestReindex(@Nonnull ReindexRequestType type, @Nonnull Set<AffectedIndex> affectedIndexes, @Nonnull Set<SharedEntityType> entityTypes)
    {
        final long requestTime = clock.getCurrentDate().getTime();

        ReindexRequestBase request = new ReindexRequestBase(null, type, requestTime, null, null, null, ReindexStatus.PENDING);
        request = reindexRequestDao.writeRequest(request);
        final long requestId = request.getId();

        for (AffectedIndex affectedIndex : affectedIndexes)
        {
            reindexRequestDao.writeComponent(new ReindexComponent(null, requestId, affectedIndex, SharedEntityType.NONE));
        }
        for (SharedEntityType entityType : entityTypes)
        {
            reindexRequestDao.writeComponent(new ReindexComponent(null, requestId, AffectedIndex.SHAREDENTITY, entityType));
        }

        ReindexRequest fullRequest = new ReindexRequest(request, affectedIndexes, entityTypes);

        return fullRequest;
    }

    @Nonnull
    private ReindexRequest transitionStatus(@Nonnull ReindexRequest request, @Nonnull ReindexStatus newStatus, long transitionTime)
    {
        Long startTime = request.getStartTime();
        Long completionTime;

        //As well as changing the status, we might need to update timestamps
        String nodeId = null;

        switch (newStatus)
        {
            case COMPLETE:
                completionTime = transitionTime;
                break;
            case ACTIVE:
                startTime = transitionTime;
                completionTime = null;
                nodeId = clusterManager.getNodeId();
                break;
            case RUNNING:
                startTime = transitionTime;
                completionTime = null;
                nodeId = clusterManager.getNodeId();
                break;
            case FAILED:
                completionTime = transitionTime;
                break;
            case PENDING:
                startTime = null;
                completionTime = null;
                break;
            default:
                throw new Error("Unsupported status: " + newStatus);
        }

        //Process children, translating them
        final ImmutableList.Builder<ReindexRequest> sourceReplacementBuilder = ImmutableList.builder();
        for (ReindexRequest sourceRequest : request.getSources())
        {
            ReindexRequest replacementRequest = transitionStatus(sourceRequest, newStatus, transitionTime);
            sourceReplacementBuilder.add(replacementRequest);
        }

        request = new ReindexRequest(request.getId(), request.getType(), request.getRequestTime(),
                startTime, completionTime, nodeId, newStatus, request.getAffectedIndexes(), request.getSharedEntities(), sourceReplacementBuilder.build());

        //If this request is in the database (and not a coalesced request) then update in the database
        if (request.getId() != null)
        {
            reindexRequestDao.writeRequest(request);
        }

        return request;
    }

    @Override
    @Nonnull
    public List<ReindexRequest> transitionStatus(@Nonnull Iterable<ReindexRequest> requests, @Nonnull ReindexStatus newStatus)
    {
        final long transitionTime = clock.getCurrentDate().getTime();
        final ImmutableList.Builder<ReindexRequest> transitionedRequestBuilder = ImmutableList.builder();
        for (ReindexRequest request : requests)
        {
            transitionedRequestBuilder.add(transitionStatus(request, newStatus, transitionTime));
        }
        return transitionedRequestBuilder.build();
    }

    @Nonnull
    @Override
    public ReindexRequest transitionStatus(@Nonnull ReindexRequest request, @Nonnull ReindexStatus newStatus)
    {
        return transitionStatus(ImmutableList.of(request), newStatus).get(0);
    }

    @Nonnull
    @Override
    public Set<ReindexRequest> getReindexProgress(@Nonnull Set<Long> requestIds)
    {
        ImmutableSet.Builder<ReindexRequest> builder = ImmutableSet.builder();
        for (long requestId : requestIds)
        {
            ReindexRequest request = getReindexProgress(requestId);
            if (request != null)
            {
                builder.add(request);
            }
        }
        return builder.build();
    }

    @Override
    public void clearAll()
    {
        reindexRequestDao.removeAllPendingRequests();
    }

    /**
     * Reads progress for an existing request.
     *
     * @param request the request.
     *
     * @return the updated request including progress information.
     */
    private ReindexRequest getReindexProgress(ReindexRequest request)
    {
        //If this is a real request, just read by ID from the database
        if (request.getId() != null)
        {
            return getReindexProgress(request.getId());
        }

        //Otherwise we have composite request, update sources
        long earliestStartTime = Long.MAX_VALUE;
        long latestCompletionTime = Long.MIN_VALUE;
        String executionNodeId = null;
        ReindexStatus status = ReindexStatus.COMPLETE;
        List<ReindexRequest> newSources = new ArrayList<ReindexRequest>();
        for (ReindexRequest source : request.getSources())
        {
            newSources.add(getReindexProgress(source));

            if (source.getStartTime() != null)
            {
                earliestStartTime = Math.min(earliestStartTime, source.getStartTime());
            }
            if (source.getCompletionTime() != null)
            {
                latestCompletionTime = Math.max(latestCompletionTime, source.getCompletionTime());
            }
            if (source.getExecutionNodeId() != null)
            {
                executionNodeId = source.getExecutionNodeId();
            }
            if (source.getStatus() == ReindexStatus.FAILED)
            {
                status = ReindexStatus.FAILED;
            }

            //Complete status passes through

            else if (source.getStatus() == ReindexStatus.RUNNING && status != ReindexStatus.FAILED)
            {
                status = ReindexStatus.RUNNING;
            }
            else if (source.getStatus() == ReindexStatus.ACTIVE && (status != ReindexStatus.FAILED && status != ReindexStatus.RUNNING))
            {
                status = ReindexStatus.ACTIVE;
            }
            else if (source.getStatus() == ReindexStatus.PENDING && (status != ReindexStatus.FAILED && status != ReindexStatus.RUNNING && status != ReindexStatus.PENDING))
            {
                status = ReindexStatus.PENDING;
            }
        }

        Long actualStartTime;
        if (earliestStartTime == Long.MAX_VALUE)
        {
            actualStartTime = null;
        }
        else
        {
            actualStartTime = earliestStartTime;
        }

        Long actualCompletionTime;
        if (latestCompletionTime == Long.MIN_VALUE)
        {
            actualCompletionTime = null;
        }
        else
        {
            actualCompletionTime = latestCompletionTime;
        }

        //Update the composite request with new sources and status
        ReindexRequest updated = new ReindexRequest(request.getId(), request.getType(),
                request.getRequestTime(), actualStartTime, actualCompletionTime,
                executionNodeId, status, request.getAffectedIndexes(),
                request.getSharedEntities(), newSources);

        return updated;
    }

    public ReindexRequest getReindexProgress(@Nonnull final Long requestId)
    {
        ReindexRequestBase foundBase = reindexRequestDao.findRequestById(requestId);
        if (foundBase != null)
        {
            return readFullRequest(foundBase);
        }
        return null;
    }

    private class RunReindexAndUpdateRequestTask<T extends Callable<IndexCommandResult> & ProvidesTaskProgress> implements Callable<IndexCommandResult>, ProvidesTaskProgress
    {
        private ReindexRequest request;
        private final T wrappedTask;

        public RunReindexAndUpdateRequestTask(T wrappedTask, ReindexRequest request)
        {
            this.wrappedTask = wrappedTask;
            this.request = request;
        }

        @Override
        public IndexCommandResult call() throws Exception
        {
            IndexCommandResult result;

            request = transitionStatus(request, ReindexStatus.RUNNING);
            try
            {
                result = wrappedTask.call();
                if (result.isSuccessful())
                {
                    request = transitionStatus(request, ReindexStatus.COMPLETE);
                }
                else
                {
                    log.error("Reindex failed: " + result.getErrorCollection().getErrorMessages());
                    request = transitionStatus(request, ReindexStatus.FAILED);
                }
            }
            catch (Exception e)
            {
                log.error("Reindex failed: " + e, e);
                request = transitionStatus(request, ReindexStatus.FAILED);
                throw e;
            }

            return result;
        }

        @Override
        public void setTaskProgressSink(TaskProgressSink taskProgressSink)
        {
            wrappedTask.setTaskProgressSink(taskProgressSink);
        }
    }

}
