package com.atlassian.jira.util;

import com.atlassian.jira.issue.search.SearchException;
import com.opensymphony.util.TextUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;

import javax.annotation.Nonnull;

public class ExceptionUtil
{

    /**
     * Extracts the closest thing to an end-user-comprehensible message from an exception.
     *
     * @param ex exception to get message for.
     * @return string representing exception without stacktrace.
     */
    public static String getMessage(final Throwable ex)
    {
        final StringBuilder sb = new StringBuilder();
        appendExceptionMessageOrClassName(ex, sb);
        if (ex.getCause() != null)
        {
            sb.append(" caused by: ");
            appendExceptionMessageOrClassName(ex.getCause(), sb);
        }

        return sb.toString();
    }

    /**
     * Logs the given exception.
     *
     * @param log the target logger
     * @param message the message to log
     * @param ex the exception to log
     * @return the logged message (not the given one)
     */
    public static String logExceptionWithWarn(
            @Nonnull final Logger log, final String message, @Nonnull final Throwable ex)
    {
        final String logMessage = message + " Exception: " + getMessage(ex);
        log.warn(logMessage);
        if (log.isDebugEnabled()) {
            log.debug("", ex);
        }
        return logMessage;
    }

    /**
     * Logs the given exception.
     *
     * @param log the target logger
     * @param message the message to log
     * @param ex the exception to log
     * @return the logged message (not the given one)
     */
    public static String logExceptionWithWarn(
            @Nonnull final org.apache.log4j.Logger log, final String message, @Nonnull final Throwable ex)
    {
        final String logMessage = message + " Exception: " + getMessage(ex);
        log.warn(logMessage);
        if (log.isDebugEnabled()) {
            log.debug(ex);
        }
        return logMessage;
    }

    private static void appendExceptionMessageOrClassName(final Throwable ex, final StringBuilder sb)
    {
        if (ex.getMessage() != null)
        {
            sb.append(ex.getMessage());
        }
        else
        {
            sb.append(ex.getClass().getSimpleName());
        }
    }

    public static String getExceptionAsHtml(final SearchException e)
    {
        return TextUtils.plainTextToHtml(ExceptionUtils.getFullStackTrace(e));
    }
}
