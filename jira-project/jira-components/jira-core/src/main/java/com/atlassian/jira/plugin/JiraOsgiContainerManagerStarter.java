package com.atlassian.jira.plugin;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.extension.Startable;

/**
 * Acts as a starter for JiraOsgiContainerManager so that the original component is not stored in listeners.
 * This is a workaround for memory leak that prevented plugin system from unloading on export.
 *
 * @since v6.4
 */
public class JiraOsgiContainerManagerStarter implements Startable
{
    @Override
    public void start() throws Exception
    {
        ComponentAccessor.getComponentOfType(JiraOsgiContainerManager.class).start();
    }
}
