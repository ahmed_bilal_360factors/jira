package com.atlassian.jira.appconsistency.db;

import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.properties.JiraPropertiesImpl;
import com.atlassian.jira.config.properties.SystemPropertiesAccessor;
import com.atlassian.jira.database.DatabaseCollationReader;
import com.atlassian.jira.ofbiz.DefaultOfBizConnectionFactory;

public class CollationCheckFactory
{
    public static CollationCheck createCollationCheck(DatabaseConfigurationManager databaseConfigurationManager) {
        return new CollationCheck(
                databaseConfigurationManager.getDatabaseConfiguration(),
                new DatabaseCollationReader(
                        DefaultOfBizConnectionFactory.getInstance(),
                        databaseConfigurationManager.getDatabaseConfiguration()
                ),
                new JiraPropertiesImpl(new SystemPropertiesAccessor())
        );
    }
}
