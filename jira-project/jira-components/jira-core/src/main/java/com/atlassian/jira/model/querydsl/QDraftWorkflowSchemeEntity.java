package com.atlassian.jira.model.querydsl;

import com.mysema.query.sql.ColumnMetadata;
import com.mysema.query.types.path.*;

import java.sql.Types;
import javax.annotation.Generated;

/**
 * QDraftWorkflowSchemeEntity is a Querydsl query object.
 *
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QDraftWorkflowSchemeEntity extends JiraRelationalPathBase<DraftWorkflowSchemeEntityDTO>
{
    public static final QDraftWorkflowSchemeEntity DRAFT_WORKFLOW_SCHEME_ENTITY = new QDraftWorkflowSchemeEntity("DRAFT_WORKFLOW_SCHEME_ENTITY");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final NumberPath<Long> scheme = createNumber("scheme", Long.class);
    public final StringPath workflow = createString("workflow");
    public final StringPath issuetype = createString("issuetype");

    public QDraftWorkflowSchemeEntity(String alias)
    {
        super(DraftWorkflowSchemeEntityDTO.class, alias, "draftworkflowschemeentity");
        addMetadata();
    }

    private void addMetadata()
    {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(scheme, ColumnMetadata.named("scheme").withIndex(2).ofType(Types.NUMERIC).withSize(18));
        addMetadata(workflow, ColumnMetadata.named("workflow").withIndex(3).ofType(Types.VARCHAR).withSize(255));
        addMetadata(issuetype, ColumnMetadata.named("issuetype").withIndex(4).ofType(Types.VARCHAR).withSize(255));
    }
}

