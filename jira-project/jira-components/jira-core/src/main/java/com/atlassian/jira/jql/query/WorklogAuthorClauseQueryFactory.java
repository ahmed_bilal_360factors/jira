package com.atlassian.jira.jql.query;

import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.resolver.UserResolver;
import com.atlassian.query.clause.TerminalClause;
import org.apache.lucene.search.Query;

public final class WorklogAuthorClauseQueryFactory extends WorklogClauseQueryFactory
{
    private final UserCustomFieldClauseQueryFactory userClauseQueryFactory;

    public WorklogAuthorClauseQueryFactory(
            final IssueIdFilterQueryFactory issueIdFilterQueryFactory,
            final QueryProjectRoleAndGroupPermissionsDecorator queryPermissionsDecorator,
            final UserResolver userResolver,
            final JqlOperandResolver operandResolver)
    {
        super(issueIdFilterQueryFactory, queryPermissionsDecorator);
        this.userClauseQueryFactory = new UserCustomFieldClauseQueryFactory(DocumentConstants.WORKLOG_AUTHOR, userResolver, operandResolver);
    }

    @Override
    public Query getWorklogQuery(final QueryCreationContext queryCreationContext, final TerminalClause terminalClause)
    {
        return userClauseQueryFactory.getQuery(queryCreationContext, terminalClause).getLuceneQuery();
    }
}
