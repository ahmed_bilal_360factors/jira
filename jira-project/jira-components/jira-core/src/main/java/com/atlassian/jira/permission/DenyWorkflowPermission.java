package com.atlassian.jira.permission;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Collections;
import java.util.Set;

/**
 * Denies the permission for everyone.
 */
public class DenyWorkflowPermission implements WorkflowPermission
{
    private ProjectPermissionKey denyPermission;

    public DenyWorkflowPermission(ProjectPermissionKey projectPermissionKey)
    {
        this.denyPermission = projectPermissionKey;
    }

    public Set getUsers(PermissionContext ctx)
    {
        return Collections.EMPTY_SET;
    }

    @Override
    public boolean allows(ProjectPermissionKey permission, Issue issue, ApplicationUser user)
    {
        return !permission.equals(denyPermission);
    }

    public String toString()
    {
        return "'denied' workflow permission";
    }
}
