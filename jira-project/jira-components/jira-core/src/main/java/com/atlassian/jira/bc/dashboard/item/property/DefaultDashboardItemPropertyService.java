package com.atlassian.jira.bc.dashboard.item.property;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.dashboard.DashboardItem;
import com.atlassian.jira.bc.dashboard.DashboardItemPropertyService;
import com.atlassian.jira.entity.property.BaseEntityPropertyService;
import com.atlassian.jira.entity.property.JsonEntityPropertyManager;
import com.atlassian.jira.util.I18nHelper;

public final class DefaultDashboardItemPropertyService extends BaseEntityPropertyService<DashboardItem> implements DashboardItemPropertyService
{
    public DefaultDashboardItemPropertyService(
            final JsonEntityPropertyManager jsonEntityPropertyManager,
            final I18nHelper i18n,
            final EventPublisher eventPublisher,
            final DashboardItemPropertyHelper entityPropertyHelper)
    {
        super(jsonEntityPropertyManager, i18n, eventPublisher, entityPropertyHelper);
    }
}
