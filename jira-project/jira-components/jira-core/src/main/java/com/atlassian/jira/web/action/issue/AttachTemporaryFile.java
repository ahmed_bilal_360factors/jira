package com.atlassian.jira.web.action.issue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Iterator;

import javax.annotation.Nullable;

import com.atlassian.adapter.jackson.ObjectMapper;
import com.atlassian.fugue.Either;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.AttachmentError;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachment;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachmentManager;
import com.atlassian.jira.issue.util.IssueUpdater;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;

import org.apache.commons.compress.utils.IOUtils;

import webwork.action.ServletActionContext;
import webwork.multipart.MultiPartRequestWrapper;

/**
 * Used by inline-file-attach.js to upload temporary attachments that can then be converted to real attachments
 * lateron.
 *
 * @since 4.2
 */
public class AttachTemporaryFile extends AbstractIssueSelectAction
{
    public static final String TEMP_FILENAME = "tempFilename";

    private final transient TemporaryWebAttachmentManager temporaryWebAttachmentManager;
    private final transient ObjectMapper objectMapper;
    protected final transient IssueUpdater issueUpdater;
    protected final transient ApplicationProperties applicationProperties;

    private boolean create = false;
    private Long projectId;

    private String formToken;

    private TemporaryWebAttachment temporaryWebAttachment;

    public AttachTemporaryFile(final SubTaskManager subTaskManager,
            final TemporaryWebAttachmentManager temporaryWebAttachmentManager,
            final ObjectMapper objectMapper, final IssueUpdater issueUpdater,
            final ApplicationProperties applicationProperties)
    {
        super(subTaskManager);
        this.temporaryWebAttachmentManager = temporaryWebAttachmentManager;
        this.objectMapper = objectMapper;
        this.issueUpdater = issueUpdater;
        this.applicationProperties = applicationProperties;
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception
    {
        final MultiPartRequestWrapper multipart = getMultipart();
        final String filename = multipart.getFilesystemName(TEMP_FILENAME);
        final String contentType = multipart.getContentType(TEMP_FILENAME);
        @Nullable
        final File file = multipart.getFile(TEMP_FILENAME);
        if (file == null)
        {
            addErrorMessage(getText("attachfile.error.file.zero", filename), Reason.VALIDATION_FAILED);
        }
        else
        {
            try
            {
                createTemporaryAttachment(filename, contentType, file);
            }
            finally
            {
                if (!file.delete())
                {
                    log.warn(String.format("Unable to delete temporary attachment file %s", file.getName()));
                }
            }
        }
        return "temp_file_json";
    }

    private void createTemporaryAttachment(final String filename, final String contentType, final File file)
            throws FileNotFoundException
    {
        final FileInputStream inputStream = new FileInputStream(file);
        try
        {
            final Either<Issue, Project> target = isCreate()
                    ? Either.<Issue, Project>right(getProjectObject())
                    : Either.<Issue, Project>left(getIssueObject());

            final Either<AttachmentError, TemporaryWebAttachment> result = temporaryWebAttachmentManager
                    .createTemporaryWebAttachment(
                            inputStream, filename, contentType, file.length(), target,
                            getFormToken(), getLoggedInApplicationUser()
                    );

            temporaryWebAttachment = result.right().on(new Function<AttachmentError, TemporaryWebAttachment>()
            {
                @Override
                public TemporaryWebAttachment apply(final AttachmentError error)
                {
                    addErrorMessage(error.getLocalizedMessage(), error.getReason());
                    return null;
                }
            });
        }
        finally
        {
            IOUtils.closeQuietly(inputStream);
        }
    }

    public boolean isCreate()
    {
        return create;
    }

    public void setCreate(final boolean create)
    {
        this.create = create;
    }

    public String getFormToken()
    {
        return formToken;
    }

    public void setFormToken(final String formToken)
    {
        this.formToken = formToken;
    }

    protected MultiPartRequestWrapper getMultipart()
    {
        return ServletActionContext.getMultiPartRequest();
    }

    @SuppressWarnings ("UnusedDeclaration") // used in tempfilejson.jsp
    public String getResponseAsJson()
    {
        if (hasAnyErrors())
        {
            return objectMapper.writeValueAsString(ImmutableMap.of(
                    "errorMsg", getErrorMessage()));
        }
        else
        {
            return objectMapper.writeValueAsString(ImmutableMap.of(
                    "id", temporaryWebAttachment.getStringId(),
                    "name", temporaryWebAttachment.getFilename()
            ));
        }
    }

    public Project getProjectObject()
    {
        if (projectId != null)
        {
            return getProjectManager().getProjectObj(projectId);
        }
        return null;
    }

    public Long getProjectId()
    {
        return projectId;
    }

    public void setProjectId(final Long projectId)
    {
        this.projectId = projectId;
    }

    public String getErrorMessage()
    {
        if (!getErrorMessages().isEmpty())
        {
            final StringBuilder errorMsgs = new StringBuilder();
            final Iterator errorIterator = getErrorMessages().iterator();
            while (errorIterator.hasNext())
            {
                final String error = (String) errorIterator.next();
                errorMsgs.append(error);
                if (errorIterator.hasNext())
                {
                    errorMsgs.append(", ");
                }
            }

            return errorMsgs.toString();
        }
        return "";
    }
}
