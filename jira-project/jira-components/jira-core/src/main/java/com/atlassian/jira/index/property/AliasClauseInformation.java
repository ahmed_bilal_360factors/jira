package com.atlassian.jira.index.property;

import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypeImpl;
import com.atlassian.jira.index.IndexDocumentConfiguration;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.jql.ClauseInformation;
import com.atlassian.query.operator.Operator;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Objects of this class holds all information about the issue property which they alias.
 */
@Immutable
public class AliasClauseInformation implements ClauseInformation
{
    private final JiraDataTypeImpl jiraDataType;
    private final Set<Operator> operators;
    private final String alias;
    private final String luceneFieldName;
    private final ClauseNames clauseNames;
    private final String pluginName;
    private final IssuePropertyJql issuePropertyJql;
    private final boolean autoComplete;
    private final boolean orderable;

    public AliasClauseInformation(final IndexDocumentConfiguration.Type type,
            final String alias,
            final String luceneFieldName,
            final ClauseNames clauseNames,
            final IssuePropertyJql issuePropertyJql,
            final String pluginName)
    {
        this.issuePropertyJql = checkNotNull(issuePropertyJql);
        this.jiraDataType = checkNotNull(type).getJiraDataType();
        this.operators = checkNotNull(type).getOperators();
        this.alias = checkNotNull(alias);
        this.luceneFieldName = checkNotNull(luceneFieldName);
        this.clauseNames = checkNotNull(clauseNames);
        this.pluginName = checkNotNull(pluginName);
        this.autoComplete = checkNotNull(type).isAutoCompleteEnabled();
        this.orderable = checkNotNull(type).isOrderable();
    }

    @Override
    public ClauseNames getJqlClauseNames()
    {
        return clauseNames;
    }

    @Nullable
    @Override
    public String getIndexField()
    {
        return luceneFieldName;
    }

    @Nullable
    @Override
    public String getFieldId()
    {
        return alias;
    }

    @Override
    public Set<Operator> getSupportedOperators()
    {
        return operators;
    }

    @Override
    public JiraDataType getDataType()
    {
        return jiraDataType;
    }

    @Nonnull
    public String getPluginName()
    {
        return pluginName;
    }

    public IssuePropertyJql getIssuePropertyJql()
    {
        return issuePropertyJql;
    }

    public String getAlias()
    {
        return alias;
    }

    public boolean isAutoComplete()
    {
        return autoComplete;
    }

    public boolean isOrderable()
    {
        return orderable;
    }
}
