package com.atlassian.jira.tenancy;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.tenancy.api.Tenant;
import com.atlassian.tenancy.api.TenantAccessor;

/**
 * Extension to the TenantAccessor allowing us to add tenants.
 *
 * @since v6.4
 */
@ExperimentalApi
public interface JiraTenantAccessor extends TenantAccessor
{
    void addTenant(Tenant tenant);
}
