package com.atlassian.jira.cluster.disasterrecovery;

import java.io.File;
import java.util.concurrent.ExecutorService;

import com.atlassian.fugue.Option;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.config.util.SecondaryJiraHome;
import com.atlassian.jira.util.PathUtils;
import com.atlassian.jira.util.log.RateLimitingLogger;

import com.google.common.io.Files;

/**
 * Task that runs to replicate one file from the primary location
 * to the secondary location.
 *
 * We pass the ExecutorService to be able to replicate the action several times
 *
 * @since v6.4
 */
class CopyTask extends ReplicatorTask
{
    private static final RateLimitingLogger log = new RateLimitingLogger(CopyTask.class);

    // The amount of times we are going to retry an operation over the secondary
    public static final int RETRIES = 3;

    private final ExecutorService executorService;

    private int retries = 0;

    public CopyTask(final File file, final JiraHome jiraHome, final SecondaryJiraHome secondaryJiraHome, final ExecutorService executorService)
    {
        super(file, jiraHome, secondaryJiraHome);
        this.executorService = executorService;
    }

    @Override
    public void run()
    {
        try
        {
            final Option<String> relativePath = getRelativePathOf(jiraHome, file);

            if (relativePath.isEmpty())
            {
                log.warn(" Attempting to replicate [" + file.getAbsolutePath() + "] failed because it is not in JIRA home");
                return;
            }

            final String secondaryFilePath = PathUtils.joinPaths(secondaryJiraHome.getHomePath(), relativePath.get());

            createDirectoriesIfNecessary(secondaryFilePath);
            final File to = new File(secondaryFilePath);

            Files.copy(file, to);
        }
        catch (Exception e)
        {
            // We retry 3 times
            retries++;

            if (retries < RETRIES)
            {
                log.warn(" Error occured while trying to write [" + file.getAbsolutePath()
                        + "] to the secondary location, re-adding the task . Retries " + retries + " . Error :  " + e.getMessage(), e);

                executorService.submit(this);
            }
            else
            {
                log.warn(" Giving up trying to write [" + file.getAbsolutePath()
                        + "] after " + retries + " retries. Error :  " + e.getMessage(), e);
            }
        }
    }

}
