package com.atlassian.jira.model.querydsl;

import com.mysema.query.sql.ColumnMetadata;
import com.mysema.query.types.path.*;

import java.sql.Types;
import javax.annotation.Generated;

/**
 * QOAuthConsumerToken is a Querydsl query object.
 *
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QOAuthConsumerToken extends JiraRelationalPathBase<OAuthConsumerTokenDTO>
{
    public static final QOAuthConsumerToken O_AUTH_CONSUMER_TOKEN = new QOAuthConsumerToken("O_AUTH_CONSUMER_TOKEN");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final DateTimePath<java.sql.Timestamp> created = createDateTime("created", java.sql.Timestamp.class);
    public final StringPath tokenKey = createString("tokenKey");
    public final StringPath token = createString("token");
    public final StringPath tokenSecret = createString("tokenSecret");
    public final StringPath tokenType = createString("tokenType");
    public final StringPath consumerKey = createString("consumerKey");

    public QOAuthConsumerToken(String alias)
    {
        super(OAuthConsumerTokenDTO.class, alias, "oauthconsumertoken");
        addMetadata();
    }

    private void addMetadata()
    {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(created, ColumnMetadata.named("created").withIndex(2).ofType(Types.TIMESTAMP).withSize(35));
        addMetadata(tokenKey, ColumnMetadata.named("token_key").withIndex(3).ofType(Types.VARCHAR).withSize(255));
        addMetadata(token, ColumnMetadata.named("token").withIndex(4).ofType(Types.VARCHAR).withSize(255));
        addMetadata(tokenSecret, ColumnMetadata.named("token_secret").withIndex(5).ofType(Types.VARCHAR).withSize(255));
        addMetadata(tokenType, ColumnMetadata.named("token_type").withIndex(6).ofType(Types.VARCHAR).withSize(60));
        addMetadata(consumerKey, ColumnMetadata.named("consumer_key").withIndex(7).ofType(Types.VARCHAR).withSize(255));
    }
}

