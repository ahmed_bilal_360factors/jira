package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Checks if this user is logged in
 */
public class UserLoggedInCondition extends AbstractWebCondition
{
    public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper)
    {
        return user != null;
    }
}
