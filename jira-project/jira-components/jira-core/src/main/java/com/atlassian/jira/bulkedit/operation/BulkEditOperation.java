package com.atlassian.jira.bulkedit.operation;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.exception.IssueFieldsCharacterLimitExceededException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.LongIdsValueHolder;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.SearchContextImpl;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.BulkEditBean;

import com.atlassian.jira.web.bean.BulkEditMultiSelectFieldBean;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import com.google.common.collect.Sets;
import org.apache.commons.collections.map.ListOrderedMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;

import static com.atlassian.jira.permission.ProjectPermissions.EDIT_ISSUES;

public class BulkEditOperation implements ProgressAwareBulkOperation
{
    public static final String NAME_KEY = "bulk.edit.operation.name";
    public static final String NAME = "BulkEdit";
    private static final String DESCRIPTION_KEY = "bulk.edit.operation.description";
    private static final String CANNOT_PERFORM_MESSAGE_KEY = "bulk.edit.cannotperform";
    private static final List<String> ALL_SYSTEM_FIELDS = ImmutableList.of(
            IssueFieldConstants.ISSUE_TYPE, IssueFieldConstants.SECURITY, IssueFieldConstants.PRIORITY,
            IssueFieldConstants.FIX_FOR_VERSIONS, IssueFieldConstants.AFFECTED_VERSIONS,
            IssueFieldConstants.COMPONENTS, IssueFieldConstants.ASSIGNEE, IssueFieldConstants.REPORTER,
            IssueFieldConstants.ENVIRONMENT, IssueFieldConstants.DUE_DATE, IssueFieldConstants.COMMENT,
            IssueFieldConstants.LABELS);
    private final IssueManager issueManager;
    private final PermissionManager permissionManager;
    private final ProjectManager projectManager;
    private final FieldManager fieldManager;
    private final JiraAuthenticationContext authenticationContext;

    public BulkEditOperation(final IssueManager issueManager, final PermissionManager permissionManager,
                             final ProjectManager projectManager, final FieldManager fieldManager,
                             final JiraAuthenticationContext authenticationContext)
    {
        this.issueManager = issueManager;
        this.permissionManager = permissionManager;
        this.projectManager = projectManager;
        this.fieldManager = fieldManager;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public boolean canPerform(final BulkEditBean bulkEditBean, final ApplicationUser remoteUser)
    {
        // Ensure that all selected issues can be edited
        for (final Issue issue : bulkEditBean.getSelectedIssues())
        {
            if (!issue.isEditable())
            {
                return false;
            }
        }

        // Check if any of the actions are available
        final Collection actions = getActions(bulkEditBean, remoteUser).values();
        for (final Object action : actions)
        {
            final BulkEditAction bulkEditAction = (BulkEditAction) action;
            if (bulkEditAction.isAvailable(bulkEditBean))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Initialises all the bulk edit actions and returns them.
     *
     * @param bulkEditBean    bean used for actions retrieval
     * @param applicationUser remote user
     * @return bulk edit actions
     */
    public Map getActions(final BulkEditBean bulkEditBean, final ApplicationUser applicationUser)
    {
        final Map actions = new ListOrderedMap();
        // Go through all system fields
        for (final String actionName : ALL_SYSTEM_FIELDS)
        {
            actions.put(actionName, buildBulkEditAction(actionName));
        }

        // Add all custom field actions
        actions.putAll(getCustomFieldActions(bulkEditBean, applicationUser));

        return actions;
    }

    private BulkEditAction buildBulkEditAction(final String fieldId)
    {
        return new BulkEditActionImpl(fieldId, fieldManager, authenticationContext);
    }

    public Map getCustomFieldActions(final BulkEditBean bulkEditBean, final ApplicationUser applicationUser)
    {
        Long projectId;
        if (!bulkEditBean.isMultipleProjects())
        {
            projectId = (Long) bulkEditBean.getProjectIds().iterator().next();
        }

        final SearchContext searchContext = new SearchContextImpl(null, new ArrayList(bulkEditBean.getProjectIds()),
                new ArrayList(bulkEditBean.getIssueTypes()));

        final List<CustomField> customFields =
                ComponentAccessor.getCustomFieldManager().getCustomFieldObjects(searchContext);
        final List<CustomField> availableCustomFields = Lists.newArrayListWithCapacity(customFields.size());
        for (final CustomField customField : customFields)
        {
            // Need to check if the field is NOT hidden in ALL field layouts of selected projects
            for (final FieldLayout fieldLayout : bulkEditBean.getFieldLayouts())
            {
                if (!fieldLayout.isFieldHidden(customField.getId()))
                {
                    availableCustomFields.add(customField);
                }
            }
        }

        if (!availableCustomFields.isEmpty())
        {
            // If we got here then the field is visible in all field layouts
            // So check for permission in all projects of the selected issues
            for (final Long aLong : bulkEditBean.getProjectIds())
            {
                projectId = aLong;
                // Need to check for EDIT permission here rather than in the BulkEdit itself, as a user does not need the EDIT permission to edit the ASSIGNEE field,
                // just the ASSIGNEE permission, so the permissions to check depend on the field
                if (!hasPermission(EDIT_ISSUES, projectManager.getProjectObj(projectId), applicationUser))
                {
                    return EasyMap.build(null, new UnavailableBulkEditAction("common.concepts.customfields",
                            "bulk.edit.unavailable.customfields", authenticationContext));
                }
            }

            final Map bulkEditActions = new ListOrderedMap();
            // Create BulkEditActions to represent each bulk-editable custom field
            for (final CustomField customField : availableCustomFields)
            {
                bulkEditActions.put(customField.getId(), buildBulkEditAction(customField.getId()));
            }

            return bulkEditActions;
        }
        else
        {
            return EasyMap.build(null,
                    new UnavailableBulkEditAction("common.concepts.customfields", "bulk.edit.unavailable.customfields",
                            authenticationContext));
        }
    }

    private boolean hasPermission(final ProjectPermissionKey permission, final Project project,
                                  final ApplicationUser remoteUser)
    {
        return permissionManager.hasPermission(permission, project, remoteUser);
    }

    @Override
    public void perform(final BulkEditBean bulkEditBean, final ApplicationUser applicationUser, Context taskContext)
            throws BulkOperationException
    {
        final boolean sendMail = bulkEditBean.isSendBulkNotification();
        final BulkEditMultiSelectFieldBean bulkEditMultiSelectFieldBean = bulkEditBean.getBulkEditMultiSelectFieldBean();

        for (final Issue issue1 : bulkEditBean.getSelectedIssues())
        {
            Context.Task task = taskContext.start(issue1);
            final MutableIssue issue = (MutableIssue) issue1;
            for (final BulkEditAction bulkEditAction : bulkEditBean.getActions().values())
            {
                final OrderableField field = bulkEditAction.getField();
                final FieldLayoutItem fieldLayoutItem = ComponentAccessor.getFieldLayoutManager()
                        .getFieldLayout(issue.getProjectObject(), issue.getIssueTypeId())
                        .getFieldLayoutItem(field);
           
                // For multi-select system fields check selected change mode option
                if (bulkEditMultiSelectFieldBean.isChangeModeSelectionAllowed(field))
                {
                    final Option<BulkEditMultiSelectFieldOption> changeMode = bulkEditMultiSelectFieldBean.getChangeModeForField(field);

                    if (!changeMode.isDefined())
                    {
                        throw new RuntimeException("Change mode not set for MultiSelect System Field: " +  field.getId());
                    }

                    Map<String,Object> valuesHolder = changeMode.get().getFieldValuesMap(issue, field, bulkEditBean.getFieldValuesHolder());
                    // This variable stores initial values to check later if values have changed inside updateIssue
                    Collection<Object> initialFieldValues = (Collection<Object>) valuesHolder.get(field.getId());
                    field.updateIssue(fieldLayoutItem, issue, valuesHolder);

                    // In case new Components or Versions were added during updateIssue we need to
                    // add them to fieldValuesHolder of bulkEditBean as well to be included in next issues
                    if (!valuesHolder.get(field.getId()).toString().equals(initialFieldValues.toString()))
                    {
                        Collection<Object> fieldValuesAfterOperation = (Collection<Object>) valuesHolder.get(field.getId());
                        Collection<Object> recentlyAddedFieldValues = subtractValues(fieldValuesAfterOperation, initialFieldValues);
                        updateFieldValuesHolder(field, recentlyAddedFieldValues, bulkEditBean.getFieldValuesHolder(), true);
                    }
                }
                // For other fields than multi-select system fields - replace all existing values
                else
                {
                    field.updateIssue(fieldLayoutItem, issue, bulkEditBean.getFieldValuesHolder());
                }
            }
            try
            {
                issueManager.updateIssue(applicationUser.getDirectoryUser(), issue, EventDispatchOption.ISSUE_UPDATED, sendMail);
            }
            catch (IssueFieldsCharacterLimitExceededException ex)
            {
                throw new BulkOperationException("Character limit exceeded, limit: " + ex.getMaxNumberOfCharacters() + ", fields: " + ex.getInvalidFieldIds(), ex);
            }
            task.complete();
        }
    }

    @VisibleForTesting
    Collection<Object> subtractValues(final Collection<Object> fieldValues, final Collection<Object> fieldValuesToRemove)
    {
        List<Object> fieldValuesToRemoveList = Lists.newArrayList(fieldValuesToRemove);

        final List<String> fieldValuesToRemoveListString = Lists.newArrayList(Iterables.transform(fieldValuesToRemoveList, new Function<Object, String>()
        {
            @Override
            public String apply(@Nullable final Object value)
            {
                return value.toString();
            }
        }));

        final Predicate<Object> notFieldValuesFromIssue = Predicates.not(new Predicate()
        {
            public boolean apply(final Object a) { return fieldValuesToRemoveListString.contains(a.toString()); }
        });
        Iterable<Object> fieldValuesIterable = Iterables.filter(fieldValues, notFieldValuesFromIssue);

        if (fieldValues instanceof Set)
        {
            return Sets.newHashSet(fieldValuesIterable);
        }
        else
        {
            return Lists.newArrayList(fieldValuesIterable);
        }
    }

    @VisibleForTesting
    void updateFieldValuesHolder(final OrderableField field, final Collection<Object> fieldValuesToAdd, final Map<String, Object> fieldValuesHolder, final Boolean clearValuesToAdd)
    {
        if (fieldValuesHolder.get(field.getId()) instanceof LongIdsValueHolder)
        {
            LongIdsValueHolder newFieldValues = new LongIdsValueHolder(new ArrayList<Long>((Collection) fieldValuesToAdd));
            LongIdsValueHolder fieldValues = LongIdsValueHolder.fromFieldValuesHolder(field.getId(), fieldValuesHolder);
            fieldValues.addAll(newFieldValues);
            if (clearValuesToAdd)
            {
                fieldValues.setValuesToAdd(Collections.<String>emptySet());
            }
            fieldValuesHolder.put(field.getId(),fieldValues);
        }
    }

    @Override
    public int getNumberOfTasks(final BulkEditBean bulkEditBean)
    {
        return bulkEditBean.getSelectedIssues().size();
    }

    public String getNameKey()
    {
        return NAME_KEY;
    }

    public String getDescriptionKey()
    {
        return DESCRIPTION_KEY;
    }

    public boolean equals(final Object o)
    {
        return this == o || o instanceof BulkEditOperation;
    }

    public String getOperationName()
    {
        return NAME;
    }

    public String getCannotPerformMessageKey()
    {
        return CANNOT_PERFORM_MESSAGE_KEY;
    }
}
