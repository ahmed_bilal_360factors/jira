package com.atlassian.jira.issue.attachment.store;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.AttachmentStreamGetData;
import com.atlassian.jira.issue.attachment.StoreAttachmentBean;
import com.atlassian.jira.issue.attachment.StoreAttachmentResult;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

import static org.apache.commons.lang3.StringUtils.EMPTY;

/**
 * Null attachment store
 * @since 6.4
 */
@ParametersAreNonnullByDefault
public class NullAttachmentStore implements StreamAttachmentStore
{
    @Nonnull
    @Override
    public Option<ErrorCollection> errors()
    {
        return Option.none();
    }

    @Override
    public Promise<StoreAttachmentResult> putAttachment(final StoreAttachmentBean storeAttachmentBean)
    {
        return Promises.promise(StoreAttachmentResult.created());
    }

    @Override
    public <A> Promise<A> getAttachment(final AttachmentKey attachmentKey, final Function<InputStream, A> inputStreamProcessor)
    {
        //This was previously calling provided stream processor with null, changed it to provide empty stream to avoid NPE
        return Promises.promise(inputStreamProcessor.get(new ByteArrayInputStream(new byte[0])));
    }

    @Override
    public <A> Promise<A> getAttachmentData(final AttachmentKey attachmentKey, final Function<AttachmentGetData, A> attachmentGetDataProcessor)
    {
        final AttachmentGetData attachmentGetData = new AttachmentStreamGetData(new ByteArrayInputStream(new byte[0]), 0);
        return Promises.promise(attachmentGetDataProcessor.get(attachmentGetData));
    }

    @Override
    public Promise<Unit> moveAttachment(final AttachmentKey oldAttachmentKey, final AttachmentKey newAttachmentKey)
    {
        return getUnitPromise();
    }

    @Override
    public Promise<Unit> copyAttachment(final AttachmentKey sourceAttachmentKey, final AttachmentKey newAttachmentKey)
    {
        return getUnitPromise();
    }

    @Override
    public Promise<Unit> deleteAttachment(final AttachmentKey attachmentKey)
    {
        return getUnitPromise();
    }

    @Override
    public Promise<TemporaryAttachmentId> putTemporaryAttachment(final InputStream inputStream, final long size)
    {
        return Promises.promise(TemporaryAttachmentId.fromString(EMPTY));
    }

    @Override
    public Promise<Unit> moveTemporaryToAttachment(final TemporaryAttachmentId temporaryAttachmentId, final AttachmentKey destinationKey)
    {
        return getUnitPromise();
    }

    @Override
    public Promise<Unit> deleteTemporaryAttachment(final TemporaryAttachmentId temporaryAttachmentId)
    {
        return getUnitPromise();
    }

    @Override
    public Promise<Boolean> exists(final AttachmentKey attachmentKey)
    {
        return Promises.promise(false);
    }

    private Promise<Unit> getUnitPromise() {return Promises.promise(Unit.VALUE);}
}
