package com.atlassian.jira.issue.attachment.store.provider;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.attachment.FileSystemAttachmentStore;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.store.BackwardCompatibleStoreAdapter;

/**
 * Provider which will create instances of {@link com.atlassian.jira.issue.attachment.store.BackwardCompatibleStoreAdapter}
 *
 * @since v6.4
 */
public class BackwardCompatibleStoreAdapterProvider
{
    private final StreamAttachmentStoreProvider streamAttachmentStoreProvider;

    public BackwardCompatibleStoreAdapterProvider(final StreamAttachmentStoreProvider streamAttachmentStoreProvider)
    {
        this.streamAttachmentStoreProvider = streamAttachmentStoreProvider;
    }

    public BackwardCompatibleStoreAdapter provide()
    {

        final Option<FileSystemAttachmentStore> optionalFileBasedStore = streamAttachmentStoreProvider.getFileSystemStore();
        final StreamAttachmentStore streamAttachmentStore = streamAttachmentStoreProvider.getStreamAttachmentStore();

        return new BackwardCompatibleStoreAdapter(streamAttachmentStore, optionalFileBasedStore);
    }
}
