package com.atlassian.jira.issue.index;

import com.atlassian.jira.index.RelatedEntityDocumentFactory;
import com.atlassian.jira.issue.worklog.Worklog;

/**
 * Abstracts the means to create a {@link org.apache.lucene.document.Document} for a {@link Worklog} and its {@link
 * com.atlassian.jira.issue.Issue}.
 *
 * @since v6.4
 */
public interface WorklogDocumentFactory extends RelatedEntityDocumentFactory<Worklog>
{
}
