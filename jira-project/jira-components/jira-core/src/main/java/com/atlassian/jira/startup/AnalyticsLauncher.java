package com.atlassian.jira.startup;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.extension.JiraStoppedEvent;
import com.atlassian.plugin.event.PluginEventManager;

/**
 * Launcher created in order to publish JiraShutdownEvent
 *
 * @since v6.4
 */
public class AnalyticsLauncher implements JiraLauncher
{
    @Override
    public void start()
    {
        //do not publish another JiraStartedEvent
    }

    @Override
    public void stop()
    {
        getEventPublisher().publish(new JiraStoppedEvent());
    }
    public EventPublisher getEventPublisher()
    {
        return ComponentAccessor.getComponentOfType(EventPublisher.class);
    }

}
