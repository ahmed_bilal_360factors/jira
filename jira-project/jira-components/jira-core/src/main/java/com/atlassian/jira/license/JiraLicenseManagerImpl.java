package com.atlassian.jira.license;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.jira.auditing.AuditingManager;
import com.atlassian.jira.auditing.handlers.SystemAuditEventHandler;
import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.license.SIDManager;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.jira.component.ComponentAccessor.getComponent;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.collect.Lists.newArrayList;

/**
 * @since v4.0
 */
public class JiraLicenseManagerImpl implements JiraLicenseManager
{
    private static final Logger log = LoggerFactory.getLogger(JiraLicenseManagerImpl.class);

    private final BuildUtilsInfo buildUtilsInfo;
    private final SIDManager sidManager;
    private final EventPublisher eventPublisher;
    private final LicenseDetailsFactory licenseDetailsFactory;

    private final FeatureManager featureManager;
    private final MultiLicenseStore multiLicenseStore;

    public JiraLicenseManagerImpl(BuildUtilsInfo buildUtilsInfo, SIDManager sidManager,
            EventPublisher eventPublisher, MultiLicenseStore multiLicenseStore, FeatureManager featureManager,
            LicenseDetailsFactory licenseDetailsFactory)
    {
        this.eventPublisher = eventPublisher;
        this.licenseDetailsFactory = notNull("licenseDetailsFactory", licenseDetailsFactory);
        this.featureManager = notNull("featureManager", featureManager);
        this.multiLicenseStore = notNull("multiLicenseStore", multiLicenseStore);
        this.buildUtilsInfo = notNull("buildUtilsInfo", buildUtilsInfo);
        this.sidManager = notNull("sidManager", sidManager);
    }

    @Override
    public String getServerId()
    {
        String serverId = multiLicenseStore.retrieveServerId();
        if (StringUtils.isBlank(serverId))
        {
            serverId = sidManager.generateSID();
            multiLicenseStore.storeServerId(serverId);
        }
        return serverId;
    }

    @Override
    public LicenseDetails getLicense()
    {
        Iterator<String> iterator = multiLicenseStore.retrieve().iterator();
        if (iterator.hasNext())
        {
            String next = iterator.next();
            if (iterator.hasNext())
            {
                log.debug("There are multiple licenses installed, but something is using the getLicense method.", new Exception());
            }
            return getLicense(next);
        }
        return NullLicenseDetails.NULL_LICENSE_DETAILS;
    }

    @Override
    public LicenseDetails getLicense(String licenseString)
    {
        return licenseDetailsFactory.getLicense(licenseString);
    }

    @Override
    @Nonnull
    public Iterable<LicenseDetails> getLicenses()
    {
        Iterable<String> licenseStrings = multiLicenseStore.retrieve();

        if (Iterables.isEmpty(licenseStrings))
        {
            return ImmutableList.of(NullLicenseDetails.NULL_LICENSE_DETAILS);
        }

        return Iterables.transform(licenseStrings, new Function<String, LicenseDetails>()
        {
            public LicenseDetails apply(@Nullable final String s)
            {
                return getLicense(s);
            }
        });
    }

    @Override
    public boolean isLicensed(@Nonnull LicenseRoleId role)
    {
        for (LicenseDetails licenseDetails : getLicenses())
        {
            LicenseRoleDetails licenseRoles = licenseDetails.getLicenseRoles();
            if (!licenseRoles.getIds().contains(role))
            {
                continue;
            }

            return true;
        }

        // no valid license
        return false;
    }

    @Override
    public boolean isDecodeable(String licenseString)
    {
        return this.licenseDetailsFactory.isDecodeable(licenseString);
    }

    @Override
    public LicenseDetails setLicense(String licenseString)
    {
        return setLicense(licenseString, true);
    }

    @Override
    public LicenseDetails setLicenseNoEvent(String licenseString)
    {
        return setLicense(licenseString, false);
    }

    @Override
    public void clearAndSetLicense(String licenseString)
    {
        LicenseDetails licenseDetails = clearAndSetLicenseNoEvent(licenseString);
        eventPublisher.publish(new NewLicenseEvent(licenseDetails));
    }

    @Override
    public LicenseDetails clearAndSetLicenseNoEvent(String licenseString)
    {
        verifyEncodable(licenseString);
        multiLicenseStore.store(newArrayList(licenseString));

        return getLicense(licenseString);
    }

    private void verifyEncodable(final String licenseString)
    {
        if (!isDecodeable(licenseString))
        {
            throw new IllegalArgumentException("The licenseString is invalid and will not be stored.");
        }
    }

    private LicenseDetails setLicense(String licenseString, boolean fireEvent)
    {
        verifyEncodable(licenseString);
        replaceLicense(licenseString);

        final LicenseDetails licenseDetails = getLicense(licenseString);

        // if the license maintenance is valid, then we should reset any app properties
        // if the license is not too old and the confirmation of installation with old license was made, remove the
        // confirmation
        if (licenseDetails.isMaintenanceValidForBuildDate(buildUtilsInfo.getCurrentBuildDate())
                && licenseDetails.hasLicenseTooOldForBuildConfirmationBeenDone())
        {
            multiLicenseStore.resetOldBuildConfirmation();
        }

        final NewLicenseEvent event = new NewLicenseEvent(licenseDetails);
        if (fireEvent)
        {
            eventPublisher.publish(event);
        }
        else
        {
            if (!featureManager.isOnDemand())
            {
                // HIROL-86 Adding audit log of new licenses. We added this here because it was the only way to get the info
                // when the system was restore and the license was changed, cause PICO was not initialized at that point.
                // Please check JDEV-11469 for more info about PICO container not initialized.
                notNull("auditingManager", getComponent(AuditingManager.class)).store(
                        notNull("systemAuditEventHandler", getComponent(SystemAuditEventHandler.class)).onNewLicenseEvent(event));
            }
        }

        return licenseDetails;
    }

    /**
     * Removes all licenses that share a role with this license, and then stores this license.
     */
    private void replaceLicense(String licenseString)
    {
        if (featureManager.isEnabled(CoreFeatures.LICENSE_ROLES_ENABLED))
        {
            LicenseDetails license = getLicense(licenseString);
            final Set<LicenseRoleId> newLicenseRoles = license.getLicenseRoles().getIds();

            Predicate<LicenseDetails> notContained = new NotContainedPredicate(newLicenseRoles);
            Iterable<LicenseDetails> withoutNewRoles = Iterables.filter(getLicenses(), notContained);

            ArrayList<String> toStore = newArrayList(licenseString);
            for (LicenseDetails pair : withoutNewRoles)
            {
                toStore.add(pair.getLicenseString());
            }

            multiLicenseStore.store(toStore);
        }
        else
        {
            multiLicenseStore.store(newArrayList(licenseString));
        }
    }

    @Override
    public void confirmProceedUnderEvaluationTerms(String userName)
    {
        multiLicenseStore.confirmProceedUnderEvaluationTerms(userName);
        final Iterable<String> licenses = multiLicenseStore.retrieve();
        List<LicenseDetails> expiredEvaluations = new ArrayList<LicenseDetails>();
        for (String license : licenses)
        {
            LicenseDetails licenseDetails = getLicense(license);
            if (licenseDetails.isMaintenanceValidForBuildDate(buildUtilsInfo.getCurrentBuildDate()))
            {
                expiredEvaluations.add(licenseDetails);
            }
        }
        eventPublisher.publish(new ConfirmEvaluationLicenseEvent(userName, expiredEvaluations));
    }

    private static class NotContainedPredicate implements Predicate<LicenseDetails>
    {
        private final Set<LicenseRoleId> newLicenseRoles;
        private final Predicate<LicenseRoleId> predicate;

        public NotContainedPredicate(Set<LicenseRoleId> newLicenseRoles)
        {
            this.newLicenseRoles = newLicenseRoles;
            this.predicate = new ContainsRolePredicate(newLicenseRoles);
        }

        public boolean apply(LicenseDetails licenseDetails)
        {
            if (licenseDetails == NullLicenseDetails.NULL_LICENSE_DETAILS)
            {
                //discard null-type licenses - don't store them.
                return false;
            }
            return !containsAlready(licenseDetails);
        }

        private boolean containsAlready(LicenseDetails licenseDetails)
        {
            Set<LicenseRoleId> localLicenseRoles = licenseDetails.getLicenseRoles().getIds();

            if (newLicenseRoles.isEmpty())
            {
                return localLicenseRoles.isEmpty();
            }
            return Iterables.any(localLicenseRoles, predicate);
        }
    }

    private static class ContainsRolePredicate implements Predicate<LicenseRoleId>
    {
        private final Set<LicenseRoleId> newRoles;

        public ContainsRolePredicate(Set<LicenseRoleId> newRoles)
        {
            this.newRoles = newRoles;
        }

        public boolean apply(LicenseRoleId existingRole)
        {
            return newRoles.contains(existingRole);
        }
    }
}
