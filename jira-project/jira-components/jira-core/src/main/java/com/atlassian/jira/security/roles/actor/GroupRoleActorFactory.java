package com.atlassian.jira.security.roles.actor;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.annotations.Internal;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.database.SqlCallback;
import com.atlassian.jira.model.querydsl.QProjectRoleActor;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.OptimizedRoleActorFactory;
import com.atlassian.jira.security.roles.ProjectRoleActor;
import com.atlassian.jira.security.roles.RoleActor;
import com.atlassian.jira.security.roles.RoleActorDoesNotExistException;
import com.atlassian.jira.user.ApplicationUser;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.mysema.commons.lang.CloseableIterator;
import com.mysema.query.Tuple;
import com.mysema.query.types.Projections;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static com.google.common.collect.Iterables.transform;

@Internal
public class GroupRoleActorFactory implements OptimizedRoleActorFactory
{
    /**
     * @deprecated Use {@link com.atlassian.jira.security.roles.ProjectRoleActor#GROUP_ROLE_ACTOR_TYPE} instead. Since v6.4.
     */
    public static final String TYPE = ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE;

    private final GroupManager groupManager;
    private final DbConnectionManager dbConnectionManager;

    public GroupRoleActorFactory(final GroupManager groupManager, final DbConnectionManager dbConnectionManager)
    {
        this.groupManager = groupManager;
        this.dbConnectionManager = dbConnectionManager;
    }

    public ProjectRoleActor createRoleActor(final Long id, final Long projectRoleId, final Long projectId, final String type, final String groupName)
            throws RoleActorDoesNotExistException
    {
        if (!ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE.equals(type))
        {
            throw new IllegalArgumentException(this.getClass().getName() + " cannot create RoleActors of type: " + type);
        }

        return new GroupRoleActor(id, projectRoleId, projectId, groupName, groupManager.groupExists(groupName));
    }

    public Set<RoleActor> optimizeRoleActorSet(final Set<RoleActor> roleActors)
    {
        // no optimise for groups
        return roleActors;
    }

    public class GroupRoleActor extends AbstractRoleActor
    {
        private final boolean active;

        GroupRoleActor(final Long id, final Long projectRoleId, final Long projectId, final String groupName, final boolean active)
        {
            super(id, projectRoleId, projectId, groupName);
            this.active = active;
        }

        @Override
        public boolean isActive()
        {
            return active;
        }

        public String getType()
        {
            return GROUP_ROLE_ACTOR_TYPE;
        }

        public String getDescriptor()
        {
            return getParameter();
        }

        public Set<User> getUsers()
        {
            final Set<User> users = new HashSet<User>();

            for (User user : groupManager.getUsersInGroup(getParameter()))
            {
                 users.add(user);
            }
            return users;
        }

        public boolean contains(final ApplicationUser user)
        {
            return user != null && groupManager.isUserInGroup(user.getUsername(), getParameter());
        }

        @Override
        public boolean contains(final User user)
        {
            return user != null && groupManager.isUserInGroup(user, getParameter());
        }

        /**
         * Returns a Group object that represents a valid (existing) group or throws an IllegalArgumentException
         * if the group does not exist
         *
         * @return group
         * @throws IllegalArgumentException if group does not exist
         */
        public Group getGroup() throws IllegalArgumentException
        {
            return groupManager.getGroupEvenWhenUnknown(getParameter());
        }

    }

    @Override
    @Nonnull
    public Set<ProjectRoleActor> getAllRoleActorsForUser(@Nullable final ApplicationUser user)
    {
        if (user == null)
        {
            return ImmutableSet.of();
        }

        final Collection<String> groupNames = ImmutableList.copyOf(transform(groupManager.getGroupNamesForUser(user), new Function<String, String>()
        {
            @Override
            public String apply(@Nullable final String s)
            {
                return toLowerCase(s);
            }
        }));

        final ImmutableSet.Builder<ProjectRoleActor> resultsBuilder = ImmutableSet.builder();

        dbConnectionManager.execute(new SqlCallback()
        {
            @Override
            public void run(final DbConnection dbConnection)
            {
                final QProjectRoleActor pra = new QProjectRoleActor("pra");

                //We are not going to filter group names here because we need to compare them using IdentifierUtils.toLowerCase
                final CloseableIterator<Tuple> roleActorTuples = dbConnection.newSqlQuery().
                        from(pra).
                        where(pra.roletype.eq(ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE).and(pra.pid.isNotNull())).
                        iterate(Projections.tuple(pra.id, pra.pid, pra.projectroleid, pra.roletype, pra.roletypeparameter));
                try
                {
                    addMatchingActors(roleActorTuples, pra, groupNames, resultsBuilder);
                }
                finally
                {
                    roleActorTuples.close();
                }
            }
        });

        return resultsBuilder.build();
    }

    private void addMatchingActors(final Iterator<Tuple> roleActorTuples, final QProjectRoleActor pra,
            final Collection<String> groupNames, final ImmutableSet.Builder<ProjectRoleActor> resultsBuilder)
    {
        while (roleActorTuples.hasNext())
        {
            final Tuple tuple = roleActorTuples.next();
            final String groupName = toLowerCase(tuple.get(pra.roletypeparameter));

            if (groupNames.contains(groupName))
            {
                final GroupRoleActor actor = new GroupRoleActor(tuple.get(pra.id), tuple.get(pra.projectroleid), tuple.get(pra.pid), groupName, true);
                resultsBuilder.add(actor);
            }
        }
    }
}
