package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.subtask.conversion.IssueConversionService;
import com.atlassian.jira.bc.subtask.conversion.IssueToSubTaskConversionService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.SimpleErrorCollection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Condition that determines whether the current user can convert the current issue to a sub task.
 * <p/>
 * An issue must be in the JiraHelper context params.
 *
 * @since v4.1
 */
public class CanConvertToSubTaskCondition extends AbstractIssueWebCondition
{
    private static final Logger log = LoggerFactory.getLogger(CanConvertToSubTaskCondition.class);
    private final IssueConversionService conversionService;


    public CanConvertToSubTaskCondition(IssueToSubTaskConversionService conversionService)
    {
        this.conversionService = conversionService;
    }

    public boolean shouldDisplay(ApplicationUser user, Issue issue, JiraHelper jiraHelper)
    {
        JiraServiceContext context = new JiraServiceContextImpl(user, new SimpleErrorCollection());
        return conversionService.canConvertIssue(context, issue);

    }

}
