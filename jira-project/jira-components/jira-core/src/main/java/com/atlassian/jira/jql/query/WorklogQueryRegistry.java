package com.atlassian.jira.jql.query;

import com.atlassian.fugue.Option;
import com.atlassian.query.clause.TerminalClause;

/**
 * Used to map a {@link com.atlassian.query.clause.TerminalClause} to its associated
 * {@link WorklogClauseQueryFactory}s.
 *
 * @since v6.4
 */
public interface WorklogQueryRegistry
{
    /**
     * Fetches an associated AbstractWorklogClauseQueryFactory objects for the provided Clause.
     * The returned value is based on the clauses name.
     *
     * @param clause that defines the name for which we want to find the query factory, must not be null.
     * @param context query context
     * @return the query factory associated with this clause, or none() if there isn't any
     */
    Option<WorklogClauseQueryFactory> getClauseQueryFactory(QueryCreationContext context, TerminalClause clause);
}
