package com.atlassian.jira.issue.fields.screen;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenScheme;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeEntity;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeEntityImpl;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeImpl;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;

public class DefaultFieldScreenFactory implements FieldScreenFactory
{
    private final ConstantsManager constantsManager;
    private final FieldScreenManager fieldScreenManager;
    private final FieldScreenSchemeManager fieldScreenSchemeManager;
    private final IssueTypeScreenSchemeManager issueTypeScreenSchemeManager;

    public DefaultFieldScreenFactory(ConstantsManager constantsManager,
            FieldScreenManager fieldScreenManager,
            FieldScreenSchemeManager fieldScreenSchemeManager,
            IssueTypeScreenSchemeManager issueTypeScreenSchemeManager)
    {
        this.constantsManager = constantsManager;
        this.fieldScreenManager = fieldScreenManager;
        this.fieldScreenSchemeManager = fieldScreenSchemeManager;
        this.issueTypeScreenSchemeManager = issueTypeScreenSchemeManager;
    }

    @Override
    public FieldScreen createScreen()
    {
        return new FieldScreenImpl(fieldScreenManager);
    }

    @Override
    public FieldScreenScheme createFieldScreenScheme()
    {
        return new FieldScreenSchemeImpl(fieldScreenSchemeManager);
    }

    @Override
    public FieldScreenSchemeItem createFieldScreenSchemeItem()
    {
        return new FieldScreenSchemeItemImpl(fieldScreenSchemeManager, fieldScreenManager);
    }

    @Override
    public IssueTypeScreenScheme createIssueTypeScreenScheme()
    {
        return new IssueTypeScreenSchemeImpl(issueTypeScreenSchemeManager);
    }

    @Override
    public IssueTypeScreenSchemeEntity createIssueTypeScreenSchemeEntity()
    {
        return new IssueTypeScreenSchemeEntityImpl(issueTypeScreenSchemeManager, fieldScreenSchemeManager, constantsManager);
    }
}
