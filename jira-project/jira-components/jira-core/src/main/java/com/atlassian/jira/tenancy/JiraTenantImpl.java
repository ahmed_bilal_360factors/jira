package com.atlassian.jira.tenancy;

import com.atlassian.tenancy.api.Tenant;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Simple implementation of {@code Tenant} interface, with a simple ID property.
 *
 * @since v6.4.
 */
public class JiraTenantImpl implements Tenant
{
    private final String id;

    public JiraTenantImpl(@Nonnull final String id)
    {
        checkNotNull(id, "id should not be null");
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        final JiraTenantImpl tenant = (JiraTenantImpl) o;
        return id.equals(tenant.id);
    }

    @Override
    public int hashCode()
    {
        return id.hashCode();
    }

    @Override
    public String toString()
    {
        return "JiraTenantImpl{" +
                "id='" + id + '\'' +
                '}';
    }
}
