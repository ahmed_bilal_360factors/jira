package com.atlassian.jira.user.directory.loader;

import com.atlassian.cache.CacheManager;
import com.atlassian.crowd.directory.loader.CacheableDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.DelegatedAuthenticationDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.DelegatingDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.DelegatingDirectoryInstanceLoaderImpl;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.InternalDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.InternalHybridDirectoryInstanceLoader;
import com.atlassian.event.api.EventPublisher;

import com.google.common.collect.ImmutableList;

/**
 * This is the super-loader that's responsible for delegating to the others.
 * <p>
 * Historically, JIRA has used {@code DelegatingDirectoryInstanceLoaderImpl} directly, but that means
 * that Crowd has to keep a Pico-friendly constructor around just for us.  That really shouldn't be
 * their responsibility to maintain.
 * </p>
 *
 * @since v6.4
 */
public class JiraDelegatingDirectoryInstanceLoader extends CacheableDirectoryInstanceLoader
{
    // Note: InternalHybridDirectoryInstanceLoader is just a marker interface for DbCachingRemoteDirectoryInstanceLoader
    public JiraDelegatingDirectoryInstanceLoader(final InternalDirectoryInstanceLoader internalDirectoryInstanceLoader,
            final InternalHybridDirectoryInstanceLoader dbCachingDirectoryInstanceLoader,
            final DelegatedAuthenticationDirectoryInstanceLoader delegatedAuthenticationDirectoryInstanceLoader,
            final EventPublisher eventPublisher,
            final CacheManager cacheManager)
    {
        super(
                delegatingDirectoryInstanceLoader(
                        internalDirectoryInstanceLoader,
                        dbCachingDirectoryInstanceLoader,
                        delegatedAuthenticationDirectoryInstanceLoader ),
                eventPublisher,
                cacheManager);
    }

    static DelegatingDirectoryInstanceLoader delegatingDirectoryInstanceLoader(DirectoryInstanceLoader... loaders)
    {
        return new DelegatingDirectoryInstanceLoaderImpl(ImmutableList.copyOf(loaders));
    }
}
