package com.atlassian.jira.issue.attachment.store;

import java.io.InputStream;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Suppliers;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.StoreAttachmentBean;
import com.atlassian.jira.issue.attachment.StoreAttachmentResult;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.jira.issue.attachment.store.strategy.StoreOperationStrategy;
import com.atlassian.jira.issue.attachment.store.strategy.get.AttachmentGetStrategy;
import com.atlassian.jira.issue.attachment.store.strategy.move.MoveTemporaryAttachmentStrategy;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;

import com.google.common.base.Functions;

/**
 * Performs dual sending operation to two streams. It will keep consistency on given primary store and not necessarily
 * on secondary. Sending to secondary store actions will get performed in background thread and any error will be
 * ignored. All actions when executed without exceptions are guarantee to be successfully executed against primary
 * store.
 *
 * @since v6.4
 */
@ParametersAreNonnullByDefault
public class DualSendingAttachmentStore implements StreamAttachmentStore
{

    private final StreamAttachmentStore primaryAttachmentStore;
    private final StreamAttachmentStore secondaryAttachmentStore;
    private final AttachmentGetStrategy attachmentGetStrategy;
    private final MoveTemporaryAttachmentStrategy moveTemporaryAttachmentStrategy;
    private final StoreOperationStrategy attachmentOperationStrategy;

    public DualSendingAttachmentStore(
            final StreamAttachmentStore primaryAttachmentStore,
            final StreamAttachmentStore secondaryAttachmentStore,
            final AttachmentGetStrategy attachmentGetStrategy,
            final MoveTemporaryAttachmentStrategy moveTemporaryAttachmentStrategy, final StoreOperationStrategy attachmentOperationStrategy)
    {
        this.primaryAttachmentStore = primaryAttachmentStore;
        this.secondaryAttachmentStore = secondaryAttachmentStore;
        this.attachmentGetStrategy = attachmentGetStrategy;
        this.moveTemporaryAttachmentStrategy = moveTemporaryAttachmentStrategy;
        this.attachmentOperationStrategy = attachmentOperationStrategy;
    }

    @Override
    public Promise<StoreAttachmentResult> putAttachment(final StoreAttachmentBean storeAttachmentBean)
    {
        //noinspection NullableProblems
        return primaryAttachmentStore.putTemporaryAttachment(storeAttachmentBean.getStream(), storeAttachmentBean.getSize())
                .flatMap(new com.google.common.base.Function<TemporaryAttachmentId, Promise<Unit>>()
                {
                    @Override
                    public Promise<Unit> apply(final TemporaryAttachmentId temporaryAttachmentId)
                    {
                        return moveTemporaryToAttachment(temporaryAttachmentId, storeAttachmentBean.getAttachmentKey());
                    }
                })
                .map(Functions.constant(StoreAttachmentResult.created()));
    }

    @Override
    public <A> Promise<A> getAttachment(final AttachmentKey attachmentKey, final Function<InputStream, A> inputStreamProcessor)
    {
        return getAttachmentData(attachmentKey, new Function<AttachmentGetData, A>()
        {
            @Override
            public A get(final AttachmentGetData store)
            {
                return inputStreamProcessor.get(store.getInputStream());
            }
        });
    }

    @Override
    public <A> Promise<A> getAttachmentData(final AttachmentKey attachmentKey, final Function<AttachmentGetData, A> attachmentGetDataProcessor)
    {
        return attachmentGetStrategy.getAttachmentData(attachmentKey, attachmentGetDataProcessor);
    }

    @Override
    public Promise<Unit> moveAttachment(final AttachmentKey oldAttachmentKey, final AttachmentKey newAttachmentKey)
    {
        return attachmentOperationStrategy.perform(new Function<StreamAttachmentStore, Promise<Unit>>()
        {
            @Override
            public Promise<Unit> get(final StreamAttachmentStore store)
            {
                return store.moveAttachment(oldAttachmentKey, newAttachmentKey);
            }
        });
    }

    @Override
    public Promise<Unit> copyAttachment(final AttachmentKey sourceAttachmentKey, final AttachmentKey newAttachmentKey)
    {
        return attachmentOperationStrategy.perform(new Function<StreamAttachmentStore, Promise<Unit>>()
        {
            @Override
            public Promise<Unit> get(final StreamAttachmentStore store)
            {
                return store.copyAttachment(sourceAttachmentKey, newAttachmentKey);
            }
        });
    }

    @Override
    public Promise<Unit> deleteAttachment(final AttachmentKey attachmentKey)
    {
        return attachmentOperationStrategy.perform(new Function<StreamAttachmentStore, Promise<Unit>>()
        {
            @Override
            public Promise<Unit> get(final StreamAttachmentStore store)
            {
                return store.deleteAttachment(attachmentKey);
            }
        });
    }

    @Override
    public Promise<TemporaryAttachmentId> putTemporaryAttachment(final InputStream inputStream, final long size)
    {
        return primaryAttachmentStore.putTemporaryAttachment(inputStream, size);
    }

    @Override
    public Promise<Unit> moveTemporaryToAttachment(final TemporaryAttachmentId temporaryAttachmentId, final AttachmentKey destinationKey)
    {
        return moveTemporaryAttachmentStrategy.moveTemporaryToAttachment(temporaryAttachmentId, destinationKey);
    }

    @Override
    public Promise<Unit> deleteTemporaryAttachment(final TemporaryAttachmentId temporaryAttachmentId)
    {
        return primaryAttachmentStore.deleteTemporaryAttachment(temporaryAttachmentId);
    }

    @Override
    public Promise<Boolean> exists(final AttachmentKey attachmentKey)
    {
        return attachmentGetStrategy.exists(attachmentKey);
    }

    @Override
    @Nonnull
    public Option<ErrorCollection> errors()
    {
        final Option<ErrorCollection> secondaryStoreErrors = secondaryAttachmentStore.errors();
        //noinspection NullableProblems
        return primaryAttachmentStore.errors().fold(
                Suppliers.ofInstance(secondaryStoreErrors),
                new com.google.common.base.Function<ErrorCollection, Option<ErrorCollection>>()
                {
                    @Override
                    public Option<ErrorCollection> apply(final ErrorCollection input)
                    {
                        if (secondaryStoreErrors.isDefined())
                        {
                            final ErrorCollection errorCollection = secondaryStoreErrors.get();
                            input.addErrorCollection(errorCollection);
                        }
                        return Option.some(input);
                    }
                });
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final DualSendingAttachmentStore that = (DualSendingAttachmentStore) o;

        return attachmentGetStrategy.equals(that.attachmentGetStrategy)
                && moveTemporaryAttachmentStrategy.equals(that.moveTemporaryAttachmentStrategy)
                && primaryAttachmentStore.equals(that.primaryAttachmentStore)
                && secondaryAttachmentStore.equals(that.secondaryAttachmentStore)
                && attachmentOperationStrategy.equals(that.attachmentOperationStrategy);
    }

    @Override
    public int hashCode()
    {
        int result = primaryAttachmentStore.hashCode();
        result = 31 * result + secondaryAttachmentStore.hashCode();
        result = 31 * result + attachmentGetStrategy.hashCode();
        result = 31 * result + moveTemporaryAttachmentStrategy.hashCode();
        result = 31 * result + attachmentOperationStrategy.hashCode();
        return result;
    }
}
