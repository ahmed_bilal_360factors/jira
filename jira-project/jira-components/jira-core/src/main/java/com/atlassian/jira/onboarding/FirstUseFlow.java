package com.atlassian.jira.onboarding;

import javax.annotation.Nonnull;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Base class for implementing an introductory flow through a product on the JIRA platform.
 * This type of flow is only ever triggered on a user's first use of JIRA.
 * A user will not be put through more than one first-use flow.
 */
@Internal
public interface FirstUseFlow
{
    /**
     * A hook point to allow a flow to check whether it would be appropriate to put the user through it.
     * For example, if you have a flow that only people with permission to create projects should go through,
     * you could do that permission check here.
     *
     * @return true if the first use flow should be activated for this user.
     */
    public boolean isApplicable(ApplicationUser user);

    /**
     * The location (of the web action) at which the first-use flow begins.
     * Any user that is eligible to be run through the flow will be directed to this URL upon logging in.
     *
     * @return the URL of the flow's starting action.
     */
    @Nonnull
    public String getUrl();
}
