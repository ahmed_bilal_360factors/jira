package com.atlassian.jira.index.property;

import javax.annotation.Nonnull;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.index.IndexDocumentConfiguration;
import com.atlassian.jira.issue.customfields.converters.DoubleConverter;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.jql.ClauseHandler;
import com.atlassian.jira.jql.DefaultClauseHandler;
import com.atlassian.jira.jql.DefaultValuesGeneratingClauseHandler;
import com.atlassian.jira.jql.context.SimpleClauseContextFactory;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.permission.DefaultClausePermissionHandler;
import com.atlassian.jira.jql.permission.IssuePropertyClausePermissionChecker;
import com.atlassian.jira.jql.query.AliasedIssuePropertyClauseQueryFactory;
import com.atlassian.jira.jql.util.JqlDateSupport;
import com.atlassian.jira.jql.validator.ClauseValidator;
import com.atlassian.jira.jql.validator.SupportedOperatorsValidator;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.query.clause.Property;
import com.atlassian.query.clause.TerminalClause;

import com.google.common.collect.ImmutableList;

import static com.atlassian.jira.index.IndexDocumentConfiguration.ExtractConfiguration;

public class DefaultJqlAliasFactory implements JqlAliasFactory
{
    private final JiraAuthenticationContext authenticationContext;
    private final SimpleClauseContextFactory clauseContextFactory;
    private final DoubleConverter doubleConverter;
    private final JqlDateSupport jqlDateSupport;
    private final JqlOperandResolver operandResolver;
    private final PluginAccessor pluginAccessor;
    private final IssuePropertyClauseValueGeneratorFactory valuesGeneratorFactory;

    public DefaultJqlAliasFactory(final SimpleClauseContextFactory clauseContextFactory,
            final DoubleConverter doubleConverter,
            final JqlDateSupport jqlDateSupport,
            final JqlOperandResolver operandResolver,
            final JiraAuthenticationContext authenticationContext,
            final IssuePropertyClauseValueGeneratorFactory valuesGeneratorFactory,
            final PluginAccessor pluginAccessor)
    {
        this.clauseContextFactory = clauseContextFactory;
        this.doubleConverter = doubleConverter;
        this.jqlDateSupport = jqlDateSupport;
        this.operandResolver = operandResolver;
        this.authenticationContext = authenticationContext;
        this.valuesGeneratorFactory = valuesGeneratorFactory;
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public JqlAlias createAlias(@Nonnull final String pluginKey,
            @Nonnull final String propertyKey,
            @Nonnull final ExtractConfiguration extractConfiguration,
            @Nonnull final String alias)
    {
        final Property property = new Property(ImmutableList.of(propertyKey), ImmutableList.of(extractConfiguration.getPath()));
        final IssuePropertyJql issuePropertyJql = IssuePropertyJql.getIssuePropertyJql(property);
        final Plugin plugin = pluginAccessor.getPlugin(pluginKey);

        final AliasClauseInformation clauseInformation = new AliasClauseInformation(extractConfiguration.getType(),
                alias,
                AliasedIssuePropertyClauseQueryFactory.getLuceneFieldName(property),
                getClauseNames(issuePropertyJql, alias),
                issuePropertyJql,
                plugin != null ? plugin.getName() : pluginKey);

        final AliasedIssuePropertyClauseQueryFactory queryFactory = new AliasedIssuePropertyClauseQueryFactory(doubleConverter,
                jqlDateSupport,
                operandResolver,
                authenticationContext,
                property,
                extractConfiguration.getType(),
                extractConfiguration.getType().getOperators());

        final AliasedPropertyClauseValidator validator =
                new AliasedPropertyClauseValidator(extractConfiguration);

        final DefaultClausePermissionHandler permissionHandler =
                new DefaultClausePermissionHandler(new IssuePropertyClausePermissionChecker());

        final ClauseHandler clauseHandler =
                getClauseHandler(clauseInformation, queryFactory, validator, permissionHandler, extractConfiguration, property);

        return new JqlAlias(clauseHandler);
    }

    private ClauseHandler getClauseHandler(final AliasClauseInformation clauseInformation,
            final AliasedIssuePropertyClauseQueryFactory queryFactory,
            final AliasedPropertyClauseValidator validator,
            final DefaultClausePermissionHandler permissionHandler,
            final ExtractConfiguration extractConfiguration,
            final Property property)
    {
        if (extractConfiguration.getType().equals(IndexDocumentConfiguration.Type.STRING))
        {
            IssuePropertyClauseValueGenerator valueGenerator = valuesGeneratorFactory.create(property);
            return new DefaultValuesGeneratingClauseHandler(clauseInformation,
                    queryFactory,
                    validator,
                    permissionHandler,
                    clauseContextFactory,
                    valueGenerator);
        }
        else
        {
            return new DefaultClauseHandler(clauseInformation,
                    queryFactory,
                    validator,
                    permissionHandler,
                    clauseContextFactory);
        }
    }

    private ClauseNames getClauseNames(final IssuePropertyJql issuePropertyJql, final String alias)
    {
        return new ClauseNames(issuePropertyJql.getCanonicalJql(), alias);
    }

    private static class AliasedPropertyClauseValidator implements ClauseValidator
    {
        private final SupportedOperatorsValidator supportedOperatorsValidator;

        public AliasedPropertyClauseValidator(final ExtractConfiguration extractConfiguration)
        {
            supportedOperatorsValidator = new SupportedOperatorsValidator(extractConfiguration.getType().getOperators());
        }

        @Nonnull
        @Override
        public MessageSet validate(final User searcher, @Nonnull final TerminalClause terminalClause)
        {
            return supportedOperatorsValidator.validate(searcher, terminalClause);
        }
    }

}
