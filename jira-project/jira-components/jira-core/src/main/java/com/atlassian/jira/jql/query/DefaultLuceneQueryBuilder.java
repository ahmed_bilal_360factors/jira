package com.atlassian.jira.jql.query;

import com.atlassian.jira.issue.search.ClauseTooComplexSearchException;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.util.LuceneQueryModifier;
import com.atlassian.jira.util.InjectableComponent;
import com.atlassian.query.clause.Clause;

import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;

/**
 * Creates a Lucene Query from a JQL clause.
 *
 * @since v4.0
 */
@InjectableComponent
public class DefaultLuceneQueryBuilder implements LuceneQueryBuilder
{
    private final QueryVisitor queryVisitor;
    private final LuceneQueryModifier luceneQueryModifier;

    public DefaultLuceneQueryBuilder(final QueryVisitor queryVisitor, final LuceneQueryModifier luceneQueryModifier)
    {
        this.queryVisitor = queryVisitor;
        this.luceneQueryModifier = luceneQueryModifier;
    }

    public Query createLuceneQuery(final QueryCreationContext queryCreationContext, final Clause clause) throws SearchException
    {
        final Query luceneQuery;
        try
        {
            luceneQuery = queryVisitor.createQuery(clause, queryCreationContext);
        }
        catch (ContextAwareQueryVisitor.JqlTooComplex jqlTooComplex)
        {
            throw new ClauseTooComplexSearchException(jqlTooComplex.getClause());
        }

        //we need to process the returned query so that it will run in lucene correctly. For instance, we
        //will add positive queries where necessary so that negations work.
        try
        {
            return luceneQueryModifier.getModifiedQuery(luceneQuery);
        }
        catch (BooleanQuery.TooManyClauses tooManyClauses)
        {
            throw new ClauseTooComplexSearchException(clause);
        }
    }
}
