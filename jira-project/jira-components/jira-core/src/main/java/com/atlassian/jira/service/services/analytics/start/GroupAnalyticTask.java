package com.atlassian.jira.service.services.analytics.start;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.service.services.analytics.JiraAnalyticTask;
import com.atlassian.jira.util.collect.MapBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Gets the information about groups
 *
 * @since v6.4
 */
public class GroupAnalyticTask implements JiraAnalyticTask
{

    private static final Logger LOG = LoggerFactory.getLogger(GroupAnalyticTask.class);

    private GroupManager groupManager;

    @Override
    public void init()
    {
        groupManager = ComponentAccessor.getGroupManager();
    }

    @Override
    public Map<String, Object> getAnalytics()
    {
        LOG.debug("Running GroupAnalyticTask");

        final MapBuilder<String, Object> builder = MapBuilder.newBuilder();
        builder.add("groups.total", groupManager.getAllGroups().size());

        return builder.toMap();
    }

    @Override
    public boolean isReportingDataShape()
    {
        return true;
    }
}
