package com.atlassian.jira.bc.workflow;

import com.atlassian.jira.workflow.function.issue.UpdateIssueFieldFunction;
import com.google.common.annotations.VisibleForTesting;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.loader.DescriptorFactory;
import com.opensymphony.workflow.loader.FunctionDescriptor;

import java.util.Map;

import static com.atlassian.jira.util.collect.MapBuilder.build;

public class DefaultWorkflowFunctionDescriptorFactory implements WorkflowFunctionDescriptorFactory
{
    @VisibleForTesting static final String CLASS_NAME_KEY = "class.name";
    @VisibleForTesting static final String FIELD_NAME_KEY = "field.name";
    @VisibleForTesting static final String FIELD_VALUE_KEY = "field.value";

    @SuppressWarnings("unchecked")
    private FunctionDescriptor makeFunctionDescriptor(Class<? extends FunctionProvider> clazz, Map<String, String> params)
    {
        FunctionDescriptor functionDescriptor = DescriptorFactory.getFactory().createFunctionDescriptor();
        functionDescriptor.setType("class");
        final Map conditionArgs = functionDescriptor.getArgs();

        conditionArgs.put(CLASS_NAME_KEY, clazz.getName());
        conditionArgs.putAll(params);
        return functionDescriptor;
    }

    @Override
    public FunctionDescriptor updateIssueField(String name, String value)
    {
        return makeFunctionDescriptor(UpdateIssueFieldFunction.class, build(
                FIELD_NAME_KEY, name,
                FIELD_VALUE_KEY, value
        ));
    }
}
