package com.atlassian.jira.web.bean;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption;
import com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOptionAdd;
import com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOptionRemove;
import com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOptionRemoveAll;
import com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOptionReplace;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.fields.OrderableField;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

/**
 * Used in the BulkEdit Wizard.
 * Stores the change mode options' values selected for the multi-select/multiple values system fields (Labels, Affects Versions, Fix Versions, Components).
 * The change mode options are available in the {@link com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption}
 *
 * @since v6.4
 */
public class BulkEditMultiSelectFieldBeanImpl implements BulkEditMultiSelectFieldBean
{

    public static final String CHANGE_MODE_FIELD_PREFIX = "change";

    /**
     * List representing Multi-Select System Fields for which user should be able to select change mode.
     */
    public static final List<String> BULK_EDIT_FIELDS_WITH_CHANGE_MODE_SELECTION = ImmutableList.of(
            IssueFieldConstants.LABELS,
            IssueFieldConstants.AFFECTED_VERSIONS,
            IssueFieldConstants.COMPONENTS,
            IssueFieldConstants.FIX_FOR_VERSIONS);

    public static final String ADD_ID = "add";
    public static final String REMOVE_ID = "remove";
    public static final String REPLACE_ID = "replace";
    public static final String REMOVEALL_ID = "removeall";
    public static final BulkEditMultiSelectFieldOption addOption = new BulkEditMultiSelectFieldOptionAdd();
    public static final BulkEditMultiSelectFieldOption replaceOption = new BulkEditMultiSelectFieldOptionReplace();
    public static final BulkEditMultiSelectFieldOption removeOption = new BulkEditMultiSelectFieldOptionRemove();
    public static final BulkEditMultiSelectFieldOption removeAllOption = new BulkEditMultiSelectFieldOptionRemoveAll();
    public static final Map<String, BulkEditMultiSelectFieldOption> multiSelectChangeModeOptions = ImmutableMap.of(
            ADD_ID, addOption,
            REPLACE_ID, replaceOption,
            REMOVE_ID, removeOption,
            REMOVEALL_ID, removeAllOption);

    public Map<String, BulkEditMultiSelectFieldOption> changeModeOptions = Maps.newHashMap();


    /**
     *  Checks if the change mode selection is allowed for the field.
     *  Returns true if the field is multi-select/multiple values system field (defined in {@link com.atlassian.jira.web.bean.BulkEditMultiSelectFieldBean}). Returns false in all other cases.
     *
     *  @param field
     */
    public boolean isChangeModeSelectionAllowed(final OrderableField field)
    {
        return BULK_EDIT_FIELDS_WITH_CHANGE_MODE_SELECTION.contains(field.getId());
    }

    /**
     *  Returns the field name to be used in template for the change mode selection for the multi-select/multiple values system field.
     *
     *  @param field
     */
    public String getChangeModeFieldName(final OrderableField field)
    {
        return CHANGE_MODE_FIELD_PREFIX + field.getId();
    }


    /**
     *  Sets the change mode for multi-select/multiple values system field.
     *
     *  @param field
     *  @param changeMode option to be set for field. The options defined by {@link com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption}
     *  @throws java.lang.IllegalArgumentException in case field is not multi-select/multiple values system field
     */
    public void setChangeModeForField(final OrderableField field, final BulkEditMultiSelectFieldOption changeMode) throws IllegalArgumentException
    {
        if (isChangeModeSelectionAllowed(field))
        {
            changeModeOptions.put(field.getId(), changeMode);
        }
        else
        {
            throw new IllegalArgumentException("Field not supported for BulkEditMultiSelect change: " + field.getId());
        }
    }

    /**
     *  Returns the {@link com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption} for the multi-select/multiple values system field
     *  or {@code null} in case the field is not multi-select/multiple values system field
     *
     *  @param field
     *  @return {@link com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption} set for the field or {@code null}
     */
    public Option<BulkEditMultiSelectFieldOption> getChangeModeForField(final OrderableField field)
    {
        if (isChangeModeSelectionAllowed(field))
        {
            if (changeModeOptions.get(field.getId()) != null)
            {
                return Option.some(changeModeOptions.get(field.getId()));
            }
            else return Option.some(getDefaultChangeModeOption());
        }
        else return Option.none();
    }

    /**
     *  Sets the change mode for the multi-select/multiple values system field from the Map of parameters.
     *  The parameters contain key-value pairs where the key is fieldId and the value is change mode option name.
     *
     *  @param field
     *  @param params The map of parameters where the key is fieldId and the value is change mode option name
     *  @throws java.lang.IllegalArgumentException in case the field is not multi-select/multiple values system field
     */
    public void setChangeModeFromParams(final OrderableField field, final Map<String, String[]> params) throws IllegalArgumentException
    {
        if (isChangeModeSelectionAllowed(field))
        {
            final String fieldName = getChangeModeFieldName(field);
            final String[] selectedOptions = params.get(fieldName);
            String selectedOption = null;

            if (selectedOptions != null)
            {
                for (String value : selectedOptions)
                {
                    if (getChangeModeOptionById(value) != null)
                    {
                        selectedOption = value;
                    }
                }
                if (selectedOption == null)
                {
                    selectedOption = getDefaultChangeModeOption().getId();
                }
            }
            else
            {
                selectedOption = getDefaultChangeModeOption().getId();
            }
            changeModeOptions.put(field.getId(), getChangeModeOptionById(selectedOption));
        }
        else
        {
            throw new IllegalArgumentException("Field not supported for BulkEditMultiSelect change: " + field.getId());
        }
    }

    /**
     *  Returns list of change mode options which are available for Multi-Select System Fields.
     *
     *  @return List of {@link com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption} to be visible in Bulk Edit Wizard
     */
    public Collection<BulkEditMultiSelectFieldOption> getChangeModeOptions()
    {
        return multiSelectChangeModeOptions.values();
    }

    /**
     *  Returns the default change mode option for multi-select/multiple values system field.
     *
     *  @return {@link com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption} which is the default for the Bulk Edit Wizard
     */
    public BulkEditMultiSelectFieldOption getDefaultChangeModeOption()
    {
        return multiSelectChangeModeOptions.get(ADD_ID);
    }

    /**
     *  Returns change mode option for given option id.
     *
     *  @param id
     *  @return {@link com.atlassian.jira.bulkedit.BulkEditMultiSelectFieldOption} for given option id
     */
    public BulkEditMultiSelectFieldOption getChangeModeOptionById(final String id)
    {
        return multiSelectChangeModeOptions.get(id);
    }

    /**
     *  Returns I18n key representing action description for field for Bulk Edit Wizard Confirmation screen.
     *
     * @param field The field for which action description is retrieved.
     * @return I18n key representing action description
     */
    public String getMultiSelectFieldActionDescription(final OrderableField field)
    {
        Option<BulkEditMultiSelectFieldOption> option = getChangeModeForField(field);
        return option.isDefined() ? option.get().getDescriptionI18nKey() : StringUtils.EMPTY;
    }

}
