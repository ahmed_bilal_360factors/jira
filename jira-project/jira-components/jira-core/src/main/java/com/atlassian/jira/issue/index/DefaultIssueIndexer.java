package com.atlassian.jira.issue.index;

import com.atlassian.fugue.Effect;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.entity.WithId;
import com.atlassian.jira.index.AccumulatingResultBuilder;
import com.atlassian.jira.index.DefaultIndex;
import com.atlassian.jira.index.EntityDocumentFactory;
import com.atlassian.jira.index.Index;
import com.atlassian.jira.index.Index.Operation;
import com.atlassian.jira.index.Index.Result;
import com.atlassian.jira.index.Index.UpdateMode;
import com.atlassian.jira.index.IndexingStrategy;
import com.atlassian.jira.index.MultiThreadedIndexingConfiguration;
import com.atlassian.jira.index.MultiThreadedIndexingStrategy;
import com.atlassian.jira.index.Operations;
import com.atlassian.jira.index.RelatedEntityDocumentFactory;
import com.atlassian.jira.index.SimpleIndexingStrategy;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.changehistory.ChangeHistoryGroup;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.index.IndexDirectoryFactory.Name;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import com.atlassian.jira.security.RequestCacheKeys;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.util.Consumer;
import com.atlassian.jira.util.Supplier;
import com.atlassian.jira.util.collect.EnclosedIterable;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import net.jcip.annotations.GuardedBy;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.jira.config.properties.PropertiesUtil.getIntProperty;
import static com.atlassian.jira.index.Operations.newCompletionDelegate;
import static com.atlassian.jira.index.Operations.newConditionalUpdate;
import static com.atlassian.jira.index.Operations.newCreate;
import static com.atlassian.jira.index.Operations.newDelete;
import static com.atlassian.jira.index.Operations.newUpdate;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;

public class DefaultIssueIndexer implements IssueIndexer
{
    private final CommentRetriever commentRetriever;
    private final ChangeHistoryRetriever changeHistoryRetriever;
    private final WorklogRetriever worklogRetriever;
    private final MultiThreadedIndexingConfiguration multiThreadedIndexingConfiguration;

    private final LuceneIssueIndexProvider lifecycle;

    private final IssueDocumentFactory issueDocumentFactory;

    private final CommentDocumentFactory commentDocumentFactory;
    private final ChangeHistoryDocumentFactory changeHistoryDocumentFactory;
    private final WorklogDocumentFactory worklogDocumentFactory;

    /**
     * simple indexing strategy just asks the operation for its result, and it is stateless, so we can reuse it.
     */
    private final IndexingStrategy simpleIndexingStrategy = new SimpleIndexingStrategy();

    private final DocumentCreationStrategy documentCreationStrategy = new DefaultDocumentCreationStrategy();

    public DefaultIssueIndexer(
            @Nonnull final IndexDirectoryFactory indexDirectoryFactory,
            @Nonnull final CommentRetriever commentRetriever,
            @Nonnull final ChangeHistoryRetriever changeHistoryRetriever,
            @Nonnull final WorklogRetriever worklogRetriever,
            @Nonnull final ApplicationProperties applicationProperties,
            @Nonnull final IssueDocumentFactory issueDocumentFactory,
            @Nonnull final CommentDocumentFactory commentDocumentFactory,
            @Nonnull final ChangeHistoryDocumentFactory changeHistoryDocumentFactory,
            @Nonnull final WorklogDocumentFactory worklogDocumentFactory)
    {
        this.lifecycle = new LuceneIssueIndexProvider(indexDirectoryFactory);
        this.commentRetriever = notNull("commentRetriever", commentRetriever);
        this.changeHistoryRetriever = notNull("changeHistoryReriever", changeHistoryRetriever);
        this.worklogRetriever = notNull("worklogRetriever", worklogRetriever);
        this.issueDocumentFactory = notNull("issueDocumentFactory", issueDocumentFactory);
        this.commentDocumentFactory = notNull("commentDocumentFactory", commentDocumentFactory);
        this.changeHistoryDocumentFactory = notNull("changeHistoryDocumentFactory", changeHistoryDocumentFactory);
        this.worklogDocumentFactory = notNull("worklogDocumentFactory", worklogDocumentFactory);
        this.multiThreadedIndexingConfiguration = new PropertiesAdapter(applicationProperties);
    }

    @GuardedBy ("external index read lock")
    public Index.Result deindexIssues(@Nonnull final EnclosedIterable<Issue> issues, @Nonnull final Context context)
    {
        return perform(issues, simpleIndexingStrategy, context, new IndexOperation()
        {
            public Index.Result perform(final Issue issue, final Context.Task task)
            {
                try
                {
                    final Term issueTerm = issueDocumentFactory.getIdentifyingTerm(issue);
                    final Operation delete = newDelete(issueTerm, UpdateMode.INTERACTIVE);
                    final Operation onCompletion = newCompletionDelegate(delete, new TaskCompleter(task));
                    final AccumulatingResultBuilder results = new AccumulatingResultBuilder();
                    results.add("Issue", issue.getId(), lifecycle.getIssueIndex().perform(onCompletion));
                    results.add("Comment For Issue", issue.getId(), lifecycle.getCommentIndex().perform(delete));
                    results.add("Change History For Issue", issue.getId(), lifecycle.getChangeHistoryIndex().perform(delete));
                    results.add("Worklog For Issue", issue.getId(), lifecycle.getWorklogIndex().perform(delete));
                    return results.toResult();
                }
                catch (final Exception ex)
                {
                    return new DefaultIndex.Failure(ex);
                }
            }
        });
    }

    @GuardedBy ("external index read lock")
    public Index.Result indexIssues(@Nonnull final EnclosedIterable<Issue> issues, @Nonnull final Context context)
    {
        return indexIssues(issues, context, IssueIndexingParams.INDEX_ALL);
    }

    @GuardedBy ("external index read lock")
    public Index.Result indexIssues(@Nonnull final EnclosedIterable<Issue> issues, @Nonnull final Context context, @Nonnull final IssueIndexingParams issueIndexingParams)
    {
        return perform(issues, simpleIndexingStrategy, context, new IndexIssuesOperation(UpdateMode.INTERACTIVE, issueIndexingParams));
    }

    /**
     * No other index operations should be called while this method is being called
     */
    @GuardedBy ("external index write lock")
    public Index.Result indexIssuesBatchMode(@Nonnull final EnclosedIterable<Issue> issues, @Nonnull final Context context)
    {
        return indexIssuesBatchMode(issues, context, IssueIndexingParams.INDEX_ALL);
    }

    @GuardedBy ("external index write lock")
    public Index.Result indexIssuesBatchMode(@Nonnull final EnclosedIterable<Issue> issues, @Nonnull final Context context, @Nonnull final IssueIndexingParams issueIndexingParams)
    {
        try
        {
            lifecycle.close();
            lifecycle.setMode(IndexingMode.DIRECT);
            return perform(issues, new MultiThreadedIndexingStrategy(simpleIndexingStrategy, multiThreadedIndexingConfiguration, "IssueIndexer"),
                    context, new IndexIssuesOperation(UpdateMode.BATCH, issueIndexingParams));
        }
        finally
        {
            lifecycle.close();
            lifecycle.setMode(IndexingMode.QUEUED);
        }
    }

    @Override
    @GuardedBy ("external index read lock")
    public Result reindexIssues(@Nonnull final EnclosedIterable<Issue> issues, @Nonnull final Context context, final IssueIndexingParams issueIndexingParams, final boolean conditionalUpdate)
    {
        return perform(issues, simpleIndexingStrategy, context, new IndexOperation()
        {
            public Index.Result perform(final Issue issue, final Context.Task task)
            {
                try
                {
                    final AccumulatingResultBuilder results = new AccumulatingResultBuilder();
                    final UpdateMode mode = UpdateMode.INTERACTIVE;
                    final Documents documents = documentCreationStrategy.get(issue, issueIndexingParams);
                    final Term issueTerm = documents.getIdentifyingTerm();
                    final Operation update;
                    if (conditionalUpdate)
                    {
                        // do a conditional update using "updated" as the optimistic lock
                        update = newConditionalUpdate(issueTerm, documents.getIssue(), mode, IssueFieldConstants.UPDATED);
                    }
                    else
                    {
                        update = newUpdate(issueTerm, documents.getIssue(), mode);
                    }
                    final Operation onCompletion = newCompletionDelegate(update, new TaskCompleter(task));

                    if (issueIndexingParams.isIndexIssues())
                    {
                        results.add("Issue", issue.getId(), lifecycle.getIssueIndex().perform(onCompletion));
                    }
                    if (issueIndexingParams.isIndexComments())
                    {
                        results.add("Comment For Issue", issue.getId(), lifecycle.getCommentIndex().perform(newUpdate(issueTerm, documents.getComments(), mode)));
                    }
                    if (issueIndexingParams.isIndexChangeHistory())
                    {
                        results.add("Change History For Issue", issue.getId(), lifecycle.getChangeHistoryIndex().perform(newUpdate(issueTerm, documents.getChanges(), mode)));
                    }
                    if (issueIndexingParams.isIndexWorklogs())
                    {
                        results.add("Worklog For Issue", issue.getId(), lifecycle.getWorklogIndex().perform(newUpdate(issueTerm, documents.getWorklogs(), mode)));
                    }
                    flushCustomFieldValueCache();
                    return results.toResult();
                }
                catch (final Exception ex)
                {
                    return new DefaultIndex.Failure(ex);
                }
            }
        });
    }

    @GuardedBy ("external index read lock")
    public Index.Result reindexIssues(@Nonnull final EnclosedIterable<Issue> issues, @Nonnull final Context context,
            final boolean reIndexComments, final boolean reIndexChangeHistory, final boolean conditionalUpdate)
    {
        final IssueIndexingParams issueIndexingParams = IssueIndexingParams.builder()
                .setComments(reIndexComments)
                .setChangeHistory(reIndexChangeHistory)
                .build();
        return reindexIssues(issues, context, issueIndexingParams, conditionalUpdate);
    }

    @GuardedBy ("external index read lock")
    public Index.Result reindexComments(@Nonnull final Collection<Comment> comments, @Nonnull final Context context)
    {
        return perform(comments, simpleIndexingStrategy, context, new RelatedEntityOperation<Comment>(commentDocumentFactory, Name.COMMENT));
    }

    @GuardedBy ("external index read lock")
    public Index.Result reindexWorklogs(@Nonnull final Collection<Worklog> worklogs, @Nonnull final Context context)
    {
        return perform(worklogs, simpleIndexingStrategy, context, new RelatedEntityOperation<Worklog>(worklogDocumentFactory, Name.WORKLOG));
    }

    @Override
    public void deleteIndexes()
    {
        for (final Index.Manager manager : lifecycle)
        {
            manager.deleteIndexDirectory();
        }
    }

    @Override
    public void deleteIndexes(final IssueIndexingParams issueIndexingParams)
    {
        final Set<Name> indexes = transformIndexingParamsToIndexesEnumSet(issueIndexingParams);
        for (final Name indexName : indexes)
        {
            lifecycle.get(indexName).deleteIndexDirectory();
        }
    }

    public IndexSearcher openEntitySearcher(final Name index)
    {
        return lifecycle.get(index).openSearcher();
    }

    public Index.Result optimize()
    {
        final AccumulatingResultBuilder builder = new AccumulatingResultBuilder();
        for (final Index.Manager manager : lifecycle)
        {
            builder.add(manager.getIndex().perform(Operations.newOptimize()));
        }
        return builder.toResult();
    }

    @Override
    public void shutdown()
    {
        lifecycle.close();
    }

    @Override
    public List<String> getIndexPaths()
    {
        return lifecycle.getIndexPaths();
    }

    @Override
    public String getIndexRootPath()
    {
        return lifecycle.getIndexRootPath();
    }

    /**
     * Perform an {@link IndexOperation} on some {@link EnclosedIterable issues} using a particular {@link
     * IndexingStrategy strategy}. There is a {@link Context task context} that must be updated to provide feedback to
     * the user.
     * <p/>
     * The implementation needs to be thread-safe, as it may be run in parallel and maintain a composite result to
     * return to the caller.
     *
     * @param issues the issues to index/deindex/reindex
     * @param strategy single or multi-threaded
     * @param context task context for status feedback
     * @param operation deindex/reindex/index etc.
     * @return the {@link Result} may waited on or not.
     */
    private static Index.Result perform(final EnclosedIterable<Issue> issues, final IndexingStrategy strategy, final Context context, final IndexOperation operation)
    {
        try
        {
            notNull("issues", issues);
            // thread-safe handler for the asynchronous Result
            final AccumulatingResultBuilder builder = new AccumulatingResultBuilder();
            // perform the operation for every issue in the collection
            issues.foreach(new Consumer<Issue>()
            {
                public void consume(@Nonnull final Issue issue)
                {
                    // wrap the updater task in a Job and give it a Context.Task so we can tell the user what's happening
                    final Context.Task task = context.start(issue);
                    // ask the Strategy for the Result, this may be performed on a thread-pool
                    // the result may be a future if asynchronous

                    final Result result = strategy.get(new Supplier<Index.Result>()
                    {
                        public Index.Result get()
                        {
                            // the actual index operation
                            return operation.perform(issue, task);
                        }
                    });
                    builder.add("Issue", issue.getId(), result);
                }
            });
            return builder.toResult();
        }
        finally
        {
            strategy.close();
        }
    }

    /**
     * Perform an {@link IndexOperation} on some {@link Collection entities} using a particular {@link IndexingStrategy
     * strategy}. There is a {@link Context task context} that must be updated to provide feedback to the user.
     * <p/>
     * The implementation needs to be thread-safe, as it may be run in parallel and maintain a composite result to
     * return to the caller.
     *
     * @param entities the entities to index/deindex/reindex
     * @param strategy single or multi-threaded
     * @param context task context for status feedback
     * @param operation deindex/reindex/index etc.
     * @return the {@link Result} may waited on or not.
     */
    private static <T extends WithId> Index.Result perform(final Iterable<T> entities, final IndexingStrategy strategy, final Context context, final EntityOperation<T> operation)
    {
        try
        {
            notNull("entities", entities);
            // thread-safe handler for the asynchronous Result
            final AccumulatingResultBuilder builder = new AccumulatingResultBuilder();
            // perform the operation for every entity in the collection
            for (final T entity : entities)
            {
                // wrap the updater task in a Job and give it a Context.Task so we can tell the user what's happening
                final Context.Task task = context.start(entity);
                // ask the Strategy for the Result, this may be performed on a thread-pool
                // the result may be a future if asynchronous
                final Result result = strategy.get(new Supplier<Index.Result>()
                {
                    public Index.Result get()
                    {
                        // the actual index operation
                        return operation.perform(entity, task);
                    }
                });
                builder.add("Entity", entity.getId(), result);
            }
            return builder.toResult();
        }
        finally
        {
            strategy.close();
        }
    }

    public interface EntityRetriever<T> extends Function<Issue, List<T>>
    {
    }

    public interface CommentRetriever extends EntityRetriever<Comment>
    {
    }

    public interface ChangeHistoryRetriever extends EntityRetriever<ChangeHistoryGroup>
    {
    }

    public interface WorklogRetriever extends EntityRetriever<Worklog>
    {
    }

    /**
     * Used when indexing to do the actual indexing of an issue.
     */
    private class IndexIssuesOperation implements IndexOperation
    {
        final UpdateMode mode;
        final IssueIndexingParams issueIndexingParams;

        IndexIssuesOperation(final UpdateMode mode, final IssueIndexingParams issueIndexingParams)
        {
            this.mode = mode;
            this.issueIndexingParams = issueIndexingParams;
        }

        public Index.Result perform(final Issue issue, final Context.Task task)
        {
            try
            {
                final Documents documents = documentCreationStrategy.get(issue, issueIndexingParams);
                final Operation issueCreate = newCreate(documents.getIssue(), mode);
                final Operation onCompletion = newCompletionDelegate(issueCreate, new TaskCompleter(task));
                final AccumulatingResultBuilder results = new AccumulatingResultBuilder();
                results.add("Issue", issue.getId(), lifecycle.getIssueIndex().perform(onCompletion));
                if (!documents.getComments().isEmpty())
                {
                    final Operation commentsCreate = newCreate(documents.getComments(), mode);
                    results.add("Comment For Issue", issue.getId(), lifecycle.getCommentIndex().perform(commentsCreate));
                }
                if (!documents.getChanges().isEmpty())
                {
                    final Operation changeHistoryCreate = newCreate(documents.getChanges(), mode);
                    results.add("Change History For Issue", issue.getId(), lifecycle.getChangeHistoryIndex().perform(changeHistoryCreate));
                }
                if (!documents.getWorklogs().isEmpty())
                {
                    final Operation worklogsCreate = newCreate(documents.getWorklogs(), mode);
                    results.add("Worklog For Issue", issue.getId(), lifecycle.getWorklogIndex().perform(worklogsCreate));
                }
                flushCustomFieldValueCache();
                return results.toResult();
            }
            catch (final Exception ex)
            {
                return new DefaultIndex.Failure(ex);
            }
        }
    }

    private void flushCustomFieldValueCache()
    {
        // TODO This is a horrible hack to stop the field values cache growing out of control during a reindex.  Find a better way!
        final Map<?, ?> customFieldValueCache = (Map<?, ?>) JiraAuthenticationContextImpl.getRequestCache().get(RequestCacheKeys.CUSTOMFIELD_VALUES_CACHE);
        if (customFieldValueCache != null)
        {
            customFieldValueCache.clear();
        }
        // TODO:end
    }

    /**
     * An {@link IndexOperation} performs the actual update to the index for a specific {@link Issue}.
     */
    private interface IndexOperation extends EntityOperation<Issue>
    {
    }

    private interface EntityOperation<T>
    {
        Index.Result perform(T entity, Context.Task task);
    }

    private Set<Name> transformIndexingParamsToIndexesEnumSet(final IssueIndexingParams issueIndexingParams)
    {
        Set<Name> indexes = EnumSet.noneOf(Name.class);
        if (issueIndexingParams.isIndexIssues())
        {
            indexes.add(Name.ISSUE);
        }
        if (issueIndexingParams.isIndexComments())
        {
            indexes.add(Name.COMMENT);
        }
        if (issueIndexingParams.isIndexChangeHistory())
        {
            indexes.add(Name.CHANGE_HISTORY);
        }
        if (issueIndexingParams.isIndexWorklogs())
        {
            indexes.add(Name.WORKLOG);
        }
        return indexes;
    }

    private class RelatedEntityOperation<T> implements EntityOperation<T>
    {
        private final RelatedEntityDocumentFactory<T> factory;
        private final Name index;


        RelatedEntityOperation(final RelatedEntityDocumentFactory<T> factory, final Name index)
        {
            this.factory = factory;
            this.index = index;
        }

        public Index.Result perform(T entity, Context.Task task)
        {
            try
            {
                final UpdateMode mode = UpdateMode.INTERACTIVE;
                final Option<Document> document = factory.apply(entity);
                final Term identifyingTerm = factory.getIdentifyingTerm(entity);
                if (document.isEmpty())
                {
                    return new DefaultIndex.Failure(new RuntimeException("Entity undefined"));
                }
                else
                {
                    return lifecycle.getIndex(index).perform(newUpdate(identifyingTerm, document.get(), mode));
                }
            }
            catch (final Exception ex)
            {
                return new DefaultIndex.Failure(ex);
            }
        }
    }

    private static class TaskCompleter implements Runnable
    {
        private final Context.Task task;

        public TaskCompleter(final Context.Task task)
        {
            this.task = task;
        }

        public void run()
        {
            task.complete();
        }
    }

    interface DocumentCreationStrategy
    {
        Documents get(Issue input, IssueIndexingParams issueIndexingParams);
    }

    class Documents
    {
        private final Document issueDocument;
        private final List<Document> comments;
        private final List<Document> changes;
        private final List<Document> worklogs;
        private final Term term;

        Documents(final Issue issue, final Option<Document> issueDocument, final Collection<Option<Document>> comments, final Collection<Option<Document>> changes, final Collection<Option<Document>> worklogs)
        {
            Preconditions.checkArgument(issueDocument.isDefined(), "Issue document bust be defined");
            this.issueDocument = issueDocument.get();
            this.comments = LuceneDocumentsBuilder.foreach(comments);
            this.changes = LuceneDocumentsBuilder.foreach(changes);
            this.worklogs = LuceneDocumentsBuilder.foreach(worklogs);
            term = issueDocumentFactory.getIdentifyingTerm(issue);
        }

        Document getIssue()
        {
            return issueDocument;
        }

        List<Document> getComments()
        {
            return comments;
        }

        List<Document> getWorklogs()
        {
            return worklogs;
        }

        List<Document> getChanges()
        {
            return changes;
        }

        Term getIdentifyingTerm()
        {
            return term;
        }
    }

    private static class LuceneDocumentsBuilder implements Effect<Document>
    {
        private final ImmutableList.Builder<Document> builder = ImmutableList.builder();

        public static List<Document> foreach(final Collection<Option<Document>> documents)
        {
            final LuceneDocumentsBuilder luceneDocumentsBuilder = new LuceneDocumentsBuilder();
            for (final Option<Document> document : documents)
            {
                document.foreach(luceneDocumentsBuilder);
            }
            return luceneDocumentsBuilder.builder.build();
        }

        @Override
        public void apply(final Document luceneDocument)
        {
            builder.add(luceneDocument);
        }

    }

    /**
     * Get the list of change documents for indexing
     */
    interface DocumentBuilder extends Function<Issue, Collection<Option<Document>>>
    {
    }

    /**
     * Get the documents (issue and issue related entities) for the issue.
     */
    class DefaultDocumentCreationStrategy implements DocumentCreationStrategy
    {
        public Documents get(final Issue issue, final IssueIndexingParams issueIndexingParams)
        {
            final DocumentBuilder commentDocumentBuilder = getDocumentBuilder(commentRetriever, commentDocumentFactory);
            final DocumentBuilder changeHistoryDocumentBuilder = getDocumentBuilder(changeHistoryRetriever, changeHistoryDocumentFactory);
            final DocumentBuilder worklogDocumentBuilder = getDocumentBuilder(worklogRetriever, worklogDocumentFactory);

            final Collection<Option<Document>> comments = issueIndexingParams.isIndexComments() ? commentDocumentBuilder.apply(issue) : Collections.<Option<Document>>emptyList();
            final Collection<Option<Document>> changes = issueIndexingParams.isIndexChangeHistory() ? changeHistoryDocumentBuilder.apply(issue) : Collections.<Option<Document>>emptyList();
            final Collection<Option<Document>> worklogs = issueIndexingParams.isIndexWorklogs() ? worklogDocumentBuilder.apply(issue) : Collections.<Option<Document>>emptyList();
            final Option<Document> issues = issueDocumentFactory.apply(issue);
            return new Documents(issue, issues, comments, changes, worklogs);
        }

        private <T> DocumentBuilder getDocumentBuilder(final EntityRetriever<T> retriever, final EntityDocumentFactory<T> documentFactory)
        {
            return new DocumentBuilder()
            {
                @Override
                public Collection<Option<Document>> apply(@Nullable final Issue issue)
                {
                    return copyOf(transform(retriever.apply(issue), documentFactory));
                }
            };
        }
    }

    static class PropertiesAdapter implements MultiThreadedIndexingConfiguration
    {
        private final ApplicationProperties applicationProperties;

        PropertiesAdapter(ApplicationProperties applicationProperties)
        {
            this.applicationProperties = notNull("applicationProperties", applicationProperties);
        }

        public int minimumBatchSize()
        {
            return getIntProperty(applicationProperties, APKeys.JiraIndexConfiguration.Issue.MIN_BATCH_SIZE, 50);
        }

        public int maximumQueueSize()
        {
            return getIntProperty(applicationProperties, APKeys.JiraIndexConfiguration.Issue.MAX_QUEUE_SIZE, 1000);
        }

        public int noOfThreads()
        {
            return getIntProperty(applicationProperties, APKeys.JiraIndexConfiguration.Issue.THREADS, 20);
        }
    }
}