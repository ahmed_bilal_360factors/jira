package com.atlassian.jira.issue.attachment.store.strategy;

import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;

/**
 * Strategy for performing operation on dual attachments store
 *
 * @since v6.4
 */
public interface StoreOperationStrategy
{
    <V> Promise<V> perform(Function<StreamAttachmentStore, Promise<V>> operation);
}
