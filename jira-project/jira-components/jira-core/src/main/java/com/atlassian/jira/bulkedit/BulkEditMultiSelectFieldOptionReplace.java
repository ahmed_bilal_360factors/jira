package com.atlassian.jira.bulkedit;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.LongIdsValueHolder;
import com.atlassian.jira.issue.fields.OrderableField;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Represents the Bulk Edit multi select field option for which the field values provided in Bulk Edit will replace the already set field values
 *
 * @since v6.4
 */
public class BulkEditMultiSelectFieldOptionReplace extends AbstractBulkEditMultiSelectFieldOption implements BulkEditMultiSelectFieldOption
{
    private static final String ID = "replace";
    private static final String NAME_I18N_KEY = "bulkedit.actions.multiselect.field.option.REPLACE";
    private static final String DESCRIPTION_I18N_KEY = "bulkedit.actions.multiselect.field.option.REPLACE";

    public String getId()
    {
        return ID;
    }

    public String getNameI18nKey()
    {
        return NAME_I18N_KEY;
    }

    public String getDescriptionI18nKey()
    {
        return DESCRIPTION_I18N_KEY;
    }
}
