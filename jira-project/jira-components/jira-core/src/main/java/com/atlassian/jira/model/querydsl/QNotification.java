package com.atlassian.jira.model.querydsl;

import com.mysema.query.sql.ColumnMetadata;
import com.mysema.query.types.path.*;

import java.sql.Types;
import javax.annotation.Generated;

/**
 * QNotification is a Querydsl query object.
 *
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QNotification extends JiraRelationalPathBase<NotificationDTO>
{
    public static final QNotification NOTIFICATION = new QNotification("NOTIFICATION");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final NumberPath<Long> scheme = createNumber("scheme", Long.class);
    public final StringPath event = createString("event");
    public final NumberPath<Long> eventTypeId = createNumber("eventTypeId", Long.class);
    public final NumberPath<Long> templateId = createNumber("templateId", Long.class);
    public final StringPath type = createString("type");
    public final StringPath parameter = createString("parameter");

    public QNotification(String alias)
    {
        super(NotificationDTO.class, alias, "notification");
        addMetadata();
    }

    private void addMetadata()
    {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(scheme, ColumnMetadata.named("scheme").withIndex(2).ofType(Types.NUMERIC).withSize(18));
        addMetadata(event, ColumnMetadata.named("event").withIndex(3).ofType(Types.VARCHAR).withSize(60));
        addMetadata(eventTypeId, ColumnMetadata.named("event_type_id").withIndex(4).ofType(Types.NUMERIC).withSize(18));
        addMetadata(templateId, ColumnMetadata.named("template_id").withIndex(5).ofType(Types.NUMERIC).withSize(18));
        addMetadata(type, ColumnMetadata.named("notif_type").withIndex(6).ofType(Types.VARCHAR).withSize(60));
        addMetadata(parameter, ColumnMetadata.named("notif_parameter").withIndex(7).ofType(Types.VARCHAR).withSize(60));
    }
}

