package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Only display a web-fragment if a User has permission to browse/pick users.
 *
 * @since v5.0
 */
public class BrowseUsersPermissionCondition extends AbstractWebCondition
{
    private PermissionManager permissionManager;

    public BrowseUsersPermissionCondition(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    @Override
    public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper)
    {
        return permissionManager.hasPermission(Permissions.USER_PICKER, user);
    }
}
