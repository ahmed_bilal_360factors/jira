package com.atlassian.jira.security.xsrf;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.jira.util.velocity.VelocityRequestContext;

/**
 * A no-op XsrfTokenGenerator that is added to the Bootstrap container because it is required in Servlet Filters.
 *
 * @since v6.4.5
 */
public class BootstrapXsrfTokenGenerator implements XsrfTokenGenerator
{
    @Override
    public String generateToken()
    {
        return generateToken(null, true);
    }

    @Override
    public String generateToken(final boolean create)
    {
        return generateToken(null, create);
    }

    @Override
    public String generateToken(final HttpServletRequest request)
    {
        return generateToken(request, true);
    }

    @Override
    public String generateToken(final VelocityRequestContext request)
    {
        return generateToken(null, true);
    }

    @Override
    public String generateToken(final HttpServletRequest request, final boolean create)
    {
        // no-op in Bootstrap.
        // Implemented only so that the XsrfTokenAdditionRequestFilter servlet filter stops throwing NPE
        return null;
    }

    @Override
    public String getToken(final HttpServletRequest request)
    {
        throw new IllegalStateException("Not implemented in Bootstrap Container");
    }

    @Override
    public String getXsrfTokenName()
    {
        throw new IllegalStateException("Not implemented in Bootstrap Container");
    }

    @Override
    public boolean validateToken(final HttpServletRequest request, final String token)
    {
        throw new IllegalStateException("Not implemented in Bootstrap Container");
    }

    @Override
    public boolean generatedByAuthenticatedUser(final String token)
    {
        throw new IllegalStateException("Not implemented in Bootstrap Container");
    }
}
