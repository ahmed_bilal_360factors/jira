package com.atlassian.jira.web.action.admin.cluster;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.disasterrecovery.JiraHomeReplicatorService;
import com.atlassian.jira.config.util.SecondaryJiraHome;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;

/**
 * Configure the replication settings for a cluster environment
 *
 * @since v6.4
 */
@WebSudoRequired
public class ReplicationSettings extends JiraWebActionSupport
{

    protected final SecondaryJiraHome secondaryJiraHome;
    private final ClusterManager clusterManager;
    private final JiraHomeReplicatorService jiraHomeReplicatorService;

    private Boolean replicating;

    public ReplicationSettings(SecondaryJiraHome secondaryJiraHome, ClusterManager clusterManager,
            JiraHomeReplicatorService jiraHomeReplicatorService)
    {
        this.secondaryJiraHome = secondaryJiraHome;
        this.clusterManager = clusterManager;
        this.jiraHomeReplicatorService = jiraHomeReplicatorService;
    }

    protected String doExecute() throws Exception
    {
        if (isReplicateRequested())
        {
            jiraHomeReplicatorService.replicateJiraHome();
        }
        return SUCCESS;
    }

    public boolean isReplicationEnabled()
    {
        return secondaryJiraHome.isEnabled();
    }

    public String getReplicationPath()
    {
        if (!isReplicationEnabled())
        {
            return "";
        }
        return secondaryJiraHome.getHomePath();
    }

    public boolean isClusterLicensed()
    {
        return clusterManager.isClusterLicensed();
    }

    public boolean isReplicating()
    {
        if (replicating == null)
        {
            replicating = isReplicateRequested() || jiraHomeReplicatorService.isReplicating();
        }
        return replicating;
    }

    public boolean isReplicateRequested()
    {
        return getHttpRequest().getParameter("ReplicateRequested") != null;
    }
}
