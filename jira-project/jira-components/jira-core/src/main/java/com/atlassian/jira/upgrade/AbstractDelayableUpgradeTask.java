package com.atlassian.jira.upgrade;

import com.atlassian.jira.issue.index.IssueIndexingParams;

public abstract class AbstractDelayableUpgradeTask extends AbstractUpgradeTask
{

    protected AbstractDelayableUpgradeTask()
    {
        super();
    }

    @Override
    public ScheduleOption getScheduleOption()
    {
        return ScheduleOption.AFTER_JIRA_STARTED;
    }

}
