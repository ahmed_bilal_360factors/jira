package com.atlassian.jira.issue.attachment.store;

import java.io.File;
import java.io.InputStream;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Suppliers;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.AttachmentStore;
import com.atlassian.jira.issue.attachment.FileBasedAttachmentStore;
import com.atlassian.jira.issue.attachment.FileSystemAttachmentStore;
import com.atlassian.jira.issue.attachment.StoreAttachmentBean;
import com.atlassian.jira.issue.attachment.StoreAttachmentResult;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Supplier;

/**
 * Simple adapter which delegates to {@link com.atlassian.jira.issue.attachment.StreamAttachmentStore} and
 * optionally to {@link com.atlassian.jira.issue.attachment.FileBasedAttachmentStore}.
 *
 * @since v6.4
 */
@ParametersAreNonnullByDefault
public class BackwardCompatibleStoreAdapter implements StreamAttachmentStore, FileBasedAttachmentStore
{

    private final StreamAttachmentStore streamAttachmentStore;
    private final Option<FileSystemAttachmentStore> fileSystemAttachmentStore;

    public BackwardCompatibleStoreAdapter(final StreamAttachmentStore streamAttachmentStore,
            final Option<FileSystemAttachmentStore> fileSystemAttachmentStore)
    {
        this.streamAttachmentStore = streamAttachmentStore;
        this.fileSystemAttachmentStore = fileSystemAttachmentStore;
    }

    @Override
    public File getAttachmentFile(final AttachmentKey attachmentKey) throws DataAccessException
    {
        //noinspection NullableProblems
        return delegateToFileStoreOrThrowException(new Function<FileBasedAttachmentStore, File>()
        {
            @Override
            public File apply(final FileBasedAttachmentStore fileBasedAttachmentStore)
            {
                return fileBasedAttachmentStore.getAttachmentFile(attachmentKey);
            }
        });
    }

    @Override
    public File getAttachmentFile(final AttachmentStore.AttachmentAdapter attachment, final File attachmentDir)
    {
        //noinspection NullableProblems
        return delegateToFileStoreOrThrowException(new Function<FileBasedAttachmentStore, File>()
        {
            @Override
            public File apply(final FileBasedAttachmentStore fileBasedAttachmentStore)
            {
                return fileBasedAttachmentStore.getAttachmentFile(attachment, attachmentDir);
            }
        });
    }

    private File delegateToFileStoreOrThrowException(final Function<FileBasedAttachmentStore, File> delegateToFileStore)
    {
        return fileSystemAttachmentStore.fold(new Supplier<File>()
        {
            @Override
            public File get()
            {
                throw new UnsupportedOperationException("Direct operations on file system are not supported.");
            }
        }, delegateToFileStore);
    }

    @Override
    public Promise<Unit> deleteAttachmentContainerForIssue(@Nonnull final Issue issue)
    {
        //noinspection NullableProblems
        return fileSystemAttachmentStore.fold(Suppliers.ofInstance(Promises.promise(Unit.VALUE)),
                new Function<FileBasedAttachmentStore, Promise<Unit>>()
        {
            @Override
            public Promise<Unit> apply(final FileBasedAttachmentStore fileBasedAttachmentStore)
            {
                return fileBasedAttachmentStore.deleteAttachmentContainerForIssue(issue);
            }
        });
    }

    @Override
    public Promise<StoreAttachmentResult> putAttachment(final StoreAttachmentBean storeAttachmentBean)
    {
        return streamAttachmentStore.putAttachment(storeAttachmentBean);
    }

    @Override
    public <A> Promise<A> getAttachment(final AttachmentKey attachmentKey, final com.atlassian.util.concurrent.Function<InputStream, A> inputStreamProcessor)
    {
        return streamAttachmentStore.getAttachment(attachmentKey, inputStreamProcessor);
    }

    @Override
    public <A> Promise<A> getAttachmentData(final AttachmentKey attachmentKey, final com.atlassian.util.concurrent.Function<AttachmentGetData, A> attachmentGetDataProcessor)
    {
        return streamAttachmentStore.getAttachmentData(attachmentKey, attachmentGetDataProcessor);
    }

    @Override
    public Promise<Unit> moveAttachment(final AttachmentKey oldAttachmentKey, final AttachmentKey newAttachmentKey)
    {
        return streamAttachmentStore.moveAttachment(oldAttachmentKey, newAttachmentKey);
    }

    @Override
    public Promise<Unit> copyAttachment(final AttachmentKey sourceAttachmentKey, final AttachmentKey newAttachmentKey)
    {
        return streamAttachmentStore.copyAttachment(sourceAttachmentKey, newAttachmentKey);
    }

    @Override
    public Promise<Unit> deleteAttachment(final AttachmentKey attachmentKey)
    {
        return streamAttachmentStore.deleteAttachment(attachmentKey);
    }

    @Override
    public Promise<TemporaryAttachmentId> putTemporaryAttachment(final InputStream inputStream, final long size)
    {
        return streamAttachmentStore.putTemporaryAttachment(inputStream, size);
    }

    @Override
    public Promise<Unit> moveTemporaryToAttachment(final TemporaryAttachmentId temporaryAttachmentId, final AttachmentKey destinationKey)
    {
        return streamAttachmentStore.moveTemporaryToAttachment(temporaryAttachmentId, destinationKey);
    }

    @Override
    public Promise<Unit> deleteTemporaryAttachment(final TemporaryAttachmentId temporaryAttachmentId)
    {
        return streamAttachmentStore.deleteTemporaryAttachment(temporaryAttachmentId);
    }

    @Override
    public Promise<Boolean> exists(final AttachmentKey attachmentKey)
    {
        return streamAttachmentStore.exists(attachmentKey);
    }

    @Override
    @Nonnull
    public Option<ErrorCollection> errors()
    {
        return streamAttachmentStore.errors();
    }

    public boolean hasFileSystemAvailable()
    {
        return fileSystemAttachmentStore.isDefined();
    }

    @VisibleForTesting
    StreamAttachmentStore getStreamAttachmentStore()
    {
        return streamAttachmentStore;
    }
}
