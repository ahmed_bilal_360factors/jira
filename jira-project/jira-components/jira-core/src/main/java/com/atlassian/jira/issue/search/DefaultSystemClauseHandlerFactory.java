package com.atlassian.jira.issue.search;

import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.config.StatusCategoryManager;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.index.indexers.FieldIndexer;
import com.atlassian.jira.issue.index.indexers.impl.AttachmentIndexer;
import com.atlassian.jira.issue.index.indexers.impl.CurrentEstimateIndexer;
import com.atlassian.jira.issue.index.indexers.impl.IssueIdIndexer;
import com.atlassian.jira.issue.index.indexers.impl.IssueKeyIndexer;
import com.atlassian.jira.issue.index.indexers.impl.OriginalEstimateIndexer;
import com.atlassian.jira.issue.index.indexers.impl.ParentIssueIndexer;
import com.atlassian.jira.issue.index.indexers.impl.SecurityIndexer;
import com.atlassian.jira.issue.index.indexers.impl.TimeSpentIndexer;
import com.atlassian.jira.issue.index.indexers.impl.VoterIndexer;
import com.atlassian.jira.issue.index.indexers.impl.WatcherIndexer;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.issue.vote.VoteManager;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.jql.ClauseHandler;
import com.atlassian.jira.jql.NoOpClauseHandler;
import com.atlassian.jira.jql.context.ClauseContextFactory;
import com.atlassian.jira.jql.context.IssueIdClauseContextFactory;
import com.atlassian.jira.jql.context.IssueParentClauseContextFactory;
import com.atlassian.jira.jql.context.IssueSecurityLevelClauseContextFactory;
import com.atlassian.jira.jql.context.MultiClauseDecoratorContextFactory;
import com.atlassian.jira.jql.context.ProjectCategoryClauseContextFactory;
import com.atlassian.jira.jql.context.SavedFilterClauseContextFactory;
import com.atlassian.jira.jql.context.ValidatingDecoratorContextFactory;
import com.atlassian.jira.jql.permission.ClausePermissionHandler;
import com.atlassian.jira.jql.permission.DefaultClausePermissionHandler;
import com.atlassian.jira.jql.permission.FieldClausePermissionChecker;
import com.atlassian.jira.jql.permission.IssueClauseValueSanitiser;
import com.atlassian.jira.jql.permission.IssueParentPermissionChecker;
import com.atlassian.jira.jql.permission.IssuePropertyClausePermissionChecker;
import com.atlassian.jira.jql.permission.StatusCategoryPermissionChecker;
import com.atlassian.jira.jql.permission.TimeTrackingPermissionChecker;
import com.atlassian.jira.jql.permission.VotePermissionChecker;
import com.atlassian.jira.jql.permission.WatchPermissionChecker;
import com.atlassian.jira.jql.query.AttachmentClauseQueryFactory;
import com.atlassian.jira.jql.query.CurrentEstimateClauseQueryFactory;
import com.atlassian.jira.jql.query.IssueIdClauseQueryFactory;
import com.atlassian.jira.jql.query.IssueParentClauseQueryFactory;
import com.atlassian.jira.jql.query.IssuePropertyClauseQueryFactory;
import com.atlassian.jira.jql.query.IssueSecurityLevelClauseQueryFactory;
import com.atlassian.jira.jql.query.LastViewedDateClauseQueryFactory;
import com.atlassian.jira.jql.query.OriginalEstimateClauseQueryFactory;
import com.atlassian.jira.jql.query.ProjectCategoryClauseQueryFactory;
import com.atlassian.jira.jql.query.SavedFilterClauseQueryFactory;
import com.atlassian.jira.jql.query.StatusCategoryClauseQueryFactory;
import com.atlassian.jira.jql.query.TimeSpentClauseQueryFactory;
import com.atlassian.jira.jql.query.VoterClauseQueryFactory;
import com.atlassian.jira.jql.query.VotesClauseQueryFactory;
import com.atlassian.jira.jql.query.WatcherClauseQueryFactory;
import com.atlassian.jira.jql.query.WatchesClauseQueryFactory;
import com.atlassian.jira.jql.query.WorklogAuthorClauseQueryFactory;
import com.atlassian.jira.jql.query.WorklogDateClauseQueryFactory;
import com.atlassian.jira.jql.validator.AttachmentsClauseValidator;
import com.atlassian.jira.jql.validator.CurrentEstimateValidator;
import com.atlassian.jira.jql.validator.IssueIdValidator;
import com.atlassian.jira.jql.validator.IssueParentValidator;
import com.atlassian.jira.jql.validator.IssuePropertyClauseValidator;
import com.atlassian.jira.jql.validator.IssueSecurityLevelClauseValidator;
import com.atlassian.jira.jql.validator.LastViewedDateValidator;
import com.atlassian.jira.jql.validator.LocalDateValidator;
import com.atlassian.jira.jql.validator.OperatorUsageValidator;
import com.atlassian.jira.jql.validator.OriginalEstimateValidator;
import com.atlassian.jira.jql.validator.ProjectCategoryValidator;
import com.atlassian.jira.jql.validator.SavedFilterClauseValidator;
import com.atlassian.jira.jql.validator.StatusCategoryValidator;
import com.atlassian.jira.jql.validator.TimeSpentValidator;
import com.atlassian.jira.jql.validator.UserCustomFieldValidator;
import com.atlassian.jira.jql.validator.VotesValidator;
import com.atlassian.jira.jql.validator.WatchesValidator;
import com.atlassian.jira.jql.values.ProjectCategoryClauseValuesGenerator;
import com.atlassian.jira.jql.values.SavedFilterValuesGenerator;
import com.atlassian.jira.jql.values.SecurityLevelClauseValuesGenerator;
import com.atlassian.jira.jql.values.StatusCategoryClauseValuesGenerator;
import com.atlassian.jira.jql.values.UserClauseValuesGenerator;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.ComponentFactory;
import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.util.concurrent.LazyReference;
import com.google.common.collect.ImmutableList;

import java.util.Collection;
import java.util.Collections;

import static com.atlassian.jira.jql.permission.DefaultClausePermissionHandler.NOOP_CLAUSE_PERMISSION_HANDLER;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since v4.0
 */
public class DefaultSystemClauseHandlerFactory implements SystemClauseHandlerFactory
{
    private final ComponentLocator locator;
    private final ComponentFactory factory;
    private final FieldClausePermissionChecker.Factory fieldClausePermissionHandlerFactory;
    private final SearchHandlerBuilderFactory searchHandlers;

    private final LazyReference<Collection<SearchHandler>> systemClauseSearchHandlers = new LazyReference<Collection<SearchHandler>>()
    {
        @Override
        protected Collection<SearchHandler> create() throws Exception
        {
            return ImmutableList.of(createSavedFilterSearchHandler(), createIssueKeySearchHandler(),
                    createIssueParentSearchHandler(), createCurrentEstimateSearchHandler(), createOriginalEstimateSearchHandler(),
                    createTimeSpentSearchHandler(), createSecurityLevelSearchHandler(), createVotesSearchHandler(), createVoterSearchHandler(),
                    createWatchesSearchHandler(), createWatcherSearchHandler(), createProjectCategoryHandler(),
                    createSubTaskSearchHandler(), createProgressSearchHandler(), createLastViewedHandler(), createAttachmentsSearchHandler(),
                    createIssuePropertySearchHandler(), createStatusCategorySearchHandler(),
                    createWorklogDateSearchHandler(), createWorklogAuthorSearchHandler());
        }
    };

    public DefaultSystemClauseHandlerFactory(final ComponentLocator locator, final ComponentFactory factory, final FieldClausePermissionChecker.Factory fieldClausePermissionHandlerFactory, final SearchHandlerBuilderFactory searchHandlers)
    {
        this.factory = notNull("factory", factory);
        this.locator = notNull("locator", locator);
        this.fieldClausePermissionHandlerFactory = notNull("fieldClausePermissionHandlerFactory", fieldClausePermissionHandlerFactory);
        this.searchHandlers = searchHandlers;
    }

    public Collection<SearchHandler> getSystemClauseSearchHandlers()
    {
        return systemClauseSearchHandlers.get();
    }

    private SearchHandler createSavedFilterSearchHandler()
    {
        // We are returning a SearchHandler not for this implementation but because others may want to include an indexer
        return searchHandlers
                .builder(SystemSearchConstants.forSavedFilter())
                .setClauseQueryFactoryType(SavedFilterClauseQueryFactory.class)
                .setClauseValidatorType(SavedFilterClauseValidator.class)
                .setContextFactory(decorateWithValidatingContextFactory(locator.getComponentInstanceOfType(SavedFilterClauseContextFactory.class)))
                .setPermissionHandler(NOOP_CLAUSE_PERMISSION_HANDLER)
                .buildWithValuesGenerator(new SavedFilterValuesGenerator(locator.getComponentInstanceOfType(SearchRequestService.class)));
    }

    private SearchHandler createIssueKeySearchHandler()
    {
        return searchHandlers
                .builder(SystemSearchConstants.forIssueKey())
                .setClauseQueryFactoryType(IssueIdClauseQueryFactory.class)
                .setClauseValidatorType(IssueIdValidator.class)
                .setContextFactory(decorateWithMultiContextFactory(
                        locator.getComponentInstanceOfType(IssueIdClauseContextFactory.Factory.class).create(
                                SystemSearchConstants.forIssueKey().getSupportedOperators())))
                .setPermissionHandler(new DefaultClausePermissionHandler(factory.createObject(IssueClauseValueSanitiser.class)))
                .addIndexer(factory.createObject(IssueKeyIndexer.class))
                .addIndexer(factory.createObject(IssueIdIndexer.class))
                .build();
    }

    private SearchHandler createSubTaskSearchHandler()
    {
        final String fieldId = IssueFieldConstants.SUBTASKS;
        final SubTaskManager subTaskManager = locator.getComponentInstanceOfType(SubTaskManager.class);
        final ClausePermissionHandler clausePermissionHandler = new DefaultClausePermissionHandler(new IssueParentPermissionChecker(subTaskManager));
        final ClauseHandler noOpClauseHandler = new NoOpClauseHandler(clausePermissionHandler, fieldId, new ClauseNames(fieldId),
                "jira.jql.validation.field.not.searchable");
        final SearchHandler.ClauseRegistration clauseRegistration = new SearchHandler.ClauseRegistration(noOpClauseHandler);
        return new SearchHandler(Collections.<FieldIndexer>emptyList(), null, Collections.singletonList(clauseRegistration));
    }

    private SearchHandler createProgressSearchHandler()
    {
        final String fieldId = IssueFieldConstants.PROGRESS;
        final ClauseHandler noOpClauseHandler = new NoOpClauseHandler(createTimeTrackingPermissionHandler(), fieldId, new ClauseNames(fieldId),
                "jira.jql.validation.field.not.searchable");
        final SearchHandler.ClauseRegistration clauseRegistration = new SearchHandler.ClauseRegistration(noOpClauseHandler);
        return new SearchHandler(Collections.<FieldIndexer>emptyList(), null, Collections.singletonList(clauseRegistration));
    }

    private SearchHandler createIssueParentSearchHandler()
    {
        return searchHandlers
                .builder(SystemSearchConstants.forIssueParent())
                .setClauseQueryFactoryType(IssueParentClauseQueryFactory.class)
                .setClauseValidatorType(IssueParentValidator.class)
                .setContextFactory(locator.getComponentInstanceOfType(IssueParentClauseContextFactory.class))
                .setPermissionHandler(
                        new DefaultClausePermissionHandler(
                                new IssueParentPermissionChecker(locator.getComponentInstanceOfType(SubTaskManager.class)), factory.createObject(IssueClauseValueSanitiser.class)))
                .addIndexer(factory.createObject(ParentIssueIndexer.class))
                .build();
    }

    private SearchHandler createCurrentEstimateSearchHandler()
    {
        return searchHandlers
                .builder(SystemSearchConstants.forCurrentEstimate())
                .setClauseQueryFactoryType(CurrentEstimateClauseQueryFactory.class)
                .setClauseValidatorType(CurrentEstimateValidator.class)
                .setPermissionHandler(createTimeTrackingPermissionHandler())
                .addIndexer(factory.createObject(CurrentEstimateIndexer.class))
                .build();
    }

    private SearchHandler createOriginalEstimateSearchHandler()
    {
        return searchHandlers
                .builder(SystemSearchConstants.forOriginalEstimate())
                .setClauseQueryFactoryType(OriginalEstimateClauseQueryFactory.class)
                .setClauseValidatorType(OriginalEstimateValidator.class)
                .setPermissionHandler(createTimeTrackingPermissionHandler())
                .addIndexer(factory.createObject(OriginalEstimateIndexer.class))
                .build();
    }

    private SearchHandler createTimeSpentSearchHandler()
    {
        return searchHandlers
                .builder(SystemSearchConstants.forTimeSpent())
                .setClauseQueryFactoryType(TimeSpentClauseQueryFactory.class)
                .setClauseValidatorType(TimeSpentValidator.class)
                .setPermissionHandler(createTimeTrackingPermissionHandler())
                .addIndexer(factory.createObject(TimeSpentIndexer.class))
                .build();
    }

    private SearchHandler createSecurityLevelSearchHandler()
    {
        return searchHandlers
                .builder(SystemSearchConstants.forSecurityLevel())
                .setClauseQueryFactoryType(IssueSecurityLevelClauseQueryFactory.class)
                .setClauseValidatorType(IssueSecurityLevelClauseValidator.class)
                .setContextFactory(decorateWithMultiContextFactory(locator.getComponentInstanceOfType(IssueSecurityLevelClauseContextFactory.Creator.class).create()))
                .setPermissionHandler(createClausePermissionHandler(SystemSearchConstants.forSecurityLevel().getFieldId()))
                .addIndexer(factory.createObject(SecurityIndexer.class))
                .buildWithValuesGenerator(new SecurityLevelClauseValuesGenerator(locator.getComponentInstanceOfType(IssueSecurityLevelManager.class)));
    }

    private SearchHandler createVotesSearchHandler()
    {
        return searchHandlers.builder(SystemSearchConstants.forVotes())
                .setClauseQueryFactoryType(VotesClauseQueryFactory.class)
                .setClauseValidatorType(VotesValidator.class)
                .setPermissionChecker(new VotePermissionChecker(locator.getComponentInstanceOfType(VoteManager.class)))
                .build();
    }

    private SearchHandler createVoterSearchHandler()
    {
        return searchHandlers
                .builder(SystemSearchConstants.forVoters())
                .setClauseQueryFactoryType(VoterClauseQueryFactory.class)
                .setClauseValidatorType(UserCustomFieldValidator.class)
                .setPermissionChecker(new VotePermissionChecker(locator.getComponentInstanceOfType(VoteManager.class)))
                .addIndexer(factory.createObject(VoterIndexer.class))
                .build();
    }

    private SearchHandler createWatchesSearchHandler()
    {
        return searchHandlers
                .builder(SystemSearchConstants.forWatches())
                .setClauseQueryFactoryType(WatchesClauseQueryFactory.class)
                .setClauseValidatorType(WatchesValidator.class)
                .setPermissionChecker(new WatchPermissionChecker(locator.getComponentInstanceOfType(WatcherManager.class)))
                .addIndexer(factory.createObject(WatcherIndexer.class))
                .build();
    }

    private SearchHandler createWatcherSearchHandler()
    {
        return searchHandlers
                .builder(SystemSearchConstants.forWatchers())
                .setClauseQueryFactoryType(WatcherClauseQueryFactory.class)
                .setClauseValidatorType(UserCustomFieldValidator.class)
                .setPermissionChecker(new WatchPermissionChecker(locator.getComponentInstanceOfType(WatcherManager.class)))
                .addIndexer(factory.createObject(WatcherIndexer.class))
                .build();
    }

    private SearchHandler createAttachmentsSearchHandler()
    {
        return searchHandlers
                .builder(SystemSearchConstants.forAttachments())
                .setClauseQueryFactoryType(AttachmentClauseQueryFactory.class)
                .setClauseValidatorType(AttachmentsClauseValidator.class)
                .setPermissionChecker(createClausePermissionHandler(IssueFieldConstants.ATTACHMENT))
                .addIndexer(factory.createObject(AttachmentIndexer.class))
                .build();
    }

    private SearchHandler createIssuePropertySearchHandler()
    {
        return searchHandlers
                .builder(SystemSearchConstants.forIssueProperty())
                .setClauseQueryFactoryType(IssuePropertyClauseQueryFactory.class)
                .setClauseValidatorType(IssuePropertyClauseValidator.class)
                .setPermissionChecker(new IssuePropertyClausePermissionChecker())
                .build();
    }

    private SearchHandler createProjectCategoryHandler()
    {
        return searchHandlers
                .builder(SystemSearchConstants.forProjectCategory())
                .setClauseQueryFactoryType(ProjectCategoryClauseQueryFactory.class)
                .setClauseValidatorType(ProjectCategoryValidator.class)
                .setContextFactory(decorateWithMultiContextFactory(locator.getComponent(ProjectCategoryClauseContextFactory.class)))
                .setPermissionHandler(NOOP_CLAUSE_PERMISSION_HANDLER)
                .buildWithValuesGenerator(new ProjectCategoryClauseValuesGenerator(locator.getComponentInstanceOfType(ProjectManager.class)));
    }

    private SearchHandler createLastViewedHandler()
    {
        return searchHandlers
                .builder(SystemSearchConstants.forLastViewedDate())
                .setClauseQueryFactoryType(LastViewedDateClauseQueryFactory.class)
                .setClauseValidatorType(LastViewedDateValidator.class)
                .setPermissionHandler(NOOP_CLAUSE_PERMISSION_HANDLER)
                .build();
    }

    private SearchHandler createStatusCategorySearchHandler()
    {
        final StatusCategoryManager statusCategoryManager = locator.getComponentInstanceOfType(StatusCategoryManager.class);

        return searchHandlers
                .builder(SystemSearchConstants.forStatusCategory())
                .setClauseQueryFactoryType(StatusCategoryClauseQueryFactory.class)
                .setClauseValidatorType(StatusCategoryValidator.class)
                .setPermissionChecker(new StatusCategoryPermissionChecker(statusCategoryManager))
                .buildWithValuesGenerator(new StatusCategoryClauseValuesGenerator(statusCategoryManager));
    }

    private SearchHandler createWorklogDateSearchHandler()
    {
        return searchHandlers
                .builder(SystemSearchConstants.forWorklogDate())
                .setClauseQueryFactoryType(WorklogDateClauseQueryFactory.class)
                .setClauseValidatorType(LocalDateValidator.class)
                .setPermissionHandler(createTimeTrackingPermissionHandler())
                .build();
    }

    private SearchHandler createWorklogAuthorSearchHandler()
    {
        return searchHandlers
                .builder(SystemSearchConstants.forWorklogAuthor())
                .setClauseQueryFactoryType(WorklogAuthorClauseQueryFactory.class)
                .setClauseValidatorType(UserCustomFieldValidator.class)
                .setPermissionHandler(createTimeTrackingPermissionHandler())
                .buildWithValuesGenerator(factory.createObject(UserClauseValuesGenerator.class));
    }

    private ClausePermissionHandler createTimeTrackingPermissionHandler()
    {
        final ApplicationProperties applicationProperties = locator.getComponentInstanceOfType(ApplicationProperties.class);
        return new DefaultClausePermissionHandler(new TimeTrackingPermissionChecker(fieldClausePermissionHandlerFactory, applicationProperties));
    }

    private ClausePermissionHandler createClausePermissionHandler(final String fieldId)
    {
        return new DefaultClausePermissionHandler(fieldClausePermissionHandlerFactory.createPermissionChecker(fieldId));
    }

    private ClauseContextFactory decorateWithMultiContextFactory(final ClauseContextFactory factory)
    {
        final MultiClauseDecoratorContextFactory.Factory multiFactory = locator.getComponentInstanceOfType(MultiClauseDecoratorContextFactory.Factory.class);
        return multiFactory.create(factory);
    }

    private ClauseContextFactory decorateWithValidatingContextFactory(final ClauseContextFactory factory)
    {
        return new ValidatingDecoratorContextFactory(locator.getComponentInstanceOfType(OperatorUsageValidator.class), factory);
    }
}
