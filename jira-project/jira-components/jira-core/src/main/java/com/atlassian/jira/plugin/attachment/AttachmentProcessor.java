package com.atlassian.jira.plugin.attachment;

import java.io.File;
import java.util.List;

import javax.annotation.Nonnull;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Provides ability to process archive files.
 *
 * @since v6.3
 */
@ExperimentalApi
public interface AttachmentProcessor
{
    /**
     * Maximum number of entries that should be returned by processAttachment method.
     */
    public static int MAX_ENTRIES = 100;

    /**
     * Extracts attachment archive file and returns its contents (only files, no directories).
     * Note that the argument has to be File not an InputStream in order to fully list archive file entries.
     *
     * @param file attachment.
     * @return List of attachment entries trimmed to MAX_ENTRIES elements.
     */
    List<AttachmentArchiveEntry> processAttachment(@Nonnull File file);
}
