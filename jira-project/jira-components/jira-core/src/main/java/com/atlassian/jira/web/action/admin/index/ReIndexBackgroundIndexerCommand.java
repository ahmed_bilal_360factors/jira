package com.atlassian.jira.web.action.admin.index;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.issue.index.IssueIndexingParams;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.index.IndexLifecycleManager;
import com.google.common.annotations.VisibleForTesting;
import org.apache.log4j.Logger;

/**
 * Reindex just the issues in the background
 *
 * @since v5.2
 */
@Internal
@VisibleForTesting
public class ReIndexBackgroundIndexerCommand extends AbstractAsyncIndexerCommand
{
    private final IssueIndexingParams issueIndexingParams;

    public ReIndexBackgroundIndexerCommand(final IndexLifecycleManager indexManager, final Logger log, final I18nHelper i18nHelper, final I18nHelper.BeanFactory i18nBeanFactory)
    {
        super(null, indexManager, log, i18nHelper, i18nBeanFactory);
        issueIndexingParams = IssueIndexingParams.INDEX_ISSUE_ONLY;
    }

    public ReIndexBackgroundIndexerCommand(final IndexLifecycleManager indexManager, IssueIndexingParams issueIndexingParams, final Logger log, final I18nHelper i18nHelper, final I18nHelper.BeanFactory i18nBeanFactory)
    {
        super(null, indexManager, log, i18nHelper, i18nBeanFactory);
        this.issueIndexingParams = issueIndexingParams;
    }

    @Override
    public IndexCommandResult doReindex(final Context context, final IndexLifecycleManager indexManager)
    {
        final long reindexTime = indexManager.reIndexIssuesInBackground(context, issueIndexingParams);
        return new IndexCommandResult(reindexTime);
    }
}
