package com.atlassian.jira.action.admin;

import com.atlassian.jira.ofbiz.OfBizDelegator;

/**
 * Serves one purpose in life: to downgrade upgrade tasks that are destructive.
 * <p>
 * The general rule is that {@code downgrade()} should always be a no-op on {@code master}.  If an upgrade task
 * is introduced on {@code master} that requires special treatment in a downgrade, put that logic in this class
 * on the branch, and delete it as part of the merge to master.  You will also need to change the minimum downgrade
 * version that master has in {@code pom.xml} and {@code jira-distribution/pom.xml} to specify the stable release
 * that the downgrade step is first going into.  Please also give the versions for which the downgrade matters,
 * what is getting downgraded, and why.
 * </p>
 *
 * @since v5.2.8
 */
public class OfBizDowngradeHandler
{
    private final OfBizDelegator ofBizDelegator;

    public OfBizDowngradeHandler(OfBizDelegator ofBizDelegator)
    {
        this.ofBizDelegator = ofBizDelegator;
    }

    /**
     * Called to perform special downgrade tasks.
     * <p>
     * This method is only invoked when we have identified that you are importing data from a newer version of
     * JIRA, presumably because an OnDemand customer has exported JIRA data to import into their on premise
     * instance.
     * </p>
     */
    public void downgrade()
    {
        // WARNING: This should *always* be empty on master!
    }
}
