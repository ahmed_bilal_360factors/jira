package com.atlassian.jira.jql.query;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.util.LuceneQueryModifier;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operator.Operator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.lucene.search.Query;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Generates a query for the comment system field.
 *
 * @since v4.0
 */
public class CommentClauseQueryFactory implements ClauseQueryFactory
{
    private static final Logger log = LoggerFactory.getLogger(CommentClauseQueryFactory.class);

    private final IssueIdFilterQueryFactory issueIdFilterQueryFactory;
    private final QueryProjectRoleAndGroupPermissionsDecorator queryPermissionsDecorator;
    private final LuceneQueryModifier luceneQueryModifier;
    private final ClauseQueryFactory delegateClauseQueryFactory;

    public CommentClauseQueryFactory(final IssueIdFilterQueryFactory issueIdFilterQueryFactory,
            final QueryProjectRoleAndGroupPermissionsDecorator queryPermissionsDecorator,
            final LuceneQueryModifier luceneQueryModifier,
            final JqlOperandResolver operandResolver)
    {
        this.issueIdFilterQueryFactory = issueIdFilterQueryFactory;
        this.queryPermissionsDecorator = queryPermissionsDecorator;
        this.luceneQueryModifier = luceneQueryModifier;
        this.delegateClauseQueryFactory = getDelegate(operandResolver);
    }

    @Nonnull
    @Override
    public QueryFactoryResult getQuery(@Nonnull final QueryCreationContext queryCreationContext, @Nonnull final TerminalClause terminalClause)
    {
        if (!isClauseValid(terminalClause))
        {
            return QueryFactoryResult.createFalseResult();
        }

        final Query query = luceneQueryModifier.getModifiedQuery(delegateClauseQueryFactory.getQuery(queryCreationContext, terminalClause).getLuceneQuery());
        final Query queryWithPermissionFilter = queryPermissionsDecorator.appendPermissionFilterQuery(query, queryCreationContext, DocumentConstants.COMMENT_LEVEL, DocumentConstants.COMMENT_LEVEL_ROLE);

        final Query issueIdFilterQuery = issueIdFilterQueryFactory.createIssueIdFilterQuery(queryWithPermissionFilter, SearchProviderFactory.COMMENT_INDEX);
        return new QueryFactoryResult(issueIdFilterQuery);
    }

    ClauseQueryFactory getDelegate(final JqlOperandResolver jqlOperandResolver)
    {
        final List<OperatorSpecificQueryFactory> operatorFactories = new ArrayList<OperatorSpecificQueryFactory>();
        operatorFactories.add(new LikeQueryFactory(false));
        return new GenericClauseQueryFactory(DocumentConstants.COMMENT_BODY, operatorFactories, jqlOperandResolver);
    }

    boolean isClauseValid(final TerminalClause terminalClause)
    {
        final Operator operator = terminalClause.getOperator();
        if (!Operator.LIKE.equals(operator) && !Operator.NOT_LIKE.equals(operator))
        {
            log.debug("Can not generate a comment clause query for a clause with operator '" + operator.getDisplayString() + "'.");
            return false;
        }
        return true;
    }
}
