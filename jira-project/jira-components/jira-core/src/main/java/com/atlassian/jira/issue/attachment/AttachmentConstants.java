package com.atlassian.jira.issue.attachment;

/**
 * Constants useful for dealing with attachments
 *
 * @since v6.3
 */
public final class AttachmentConstants
{
    public static final String ATTACHMENT_ENTITY_NAME = "FileAttachment";
    public static final String FILESYSTEM = "filesystem";
    public static final String NONE = "none";
    public static final String ATTACHMENT_STORE_PROVIDER_MODE = "attachment.store.provider.mode";
    public static final String DEFAULT_ATTACHMENT_STORE_MODE = "default";
    public static final String DUAL_ATTACHMENT_STORE_MODE = "dual";

    private AttachmentConstants() {}


}
