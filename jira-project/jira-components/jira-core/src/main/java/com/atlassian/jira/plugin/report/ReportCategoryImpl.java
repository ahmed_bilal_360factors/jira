package com.atlassian.jira.plugin.report;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;

import java.util.Map;
import javax.annotation.Nullable;

/**
 * @since v6.4
 */
public class ReportCategoryImpl implements ReportCategory
{
    public static final ReportCategoryImpl AGILE = new ReportCategoryImpl("agile");
    public static final ReportCategoryImpl ISSUE_ANALYSIS = new ReportCategoryImpl("issue.analysis");
    public static final ReportCategoryImpl FORECAST_MANAGEMENT = new ReportCategoryImpl("forecast.management");
    public static final ReportCategoryImpl OTHER = new ReportCategoryImpl("other");

    private static final Map<String, ReportCategoryImpl> BY_KEY;

    private final String key;

    ReportCategoryImpl(final String key)
    {
        this.key = key;
    }

    public String getKey()
    {
        return key;
    }

    public String getI18nKey()
    {
        return "report.category." + key;
    }

    @Nullable
    public static ReportCategory byKey(String key)
    {
        return BY_KEY.get(key);
    }

    static
    {
        ImmutableList<ReportCategoryImpl> categories = ImmutableList.of(AGILE, ISSUE_ANALYSIS, FORECAST_MANAGEMENT, OTHER);
        BY_KEY = Maps.uniqueIndex(categories, new Function<ReportCategoryImpl, String>()
        {
            @Override
            public String apply(final ReportCategoryImpl input)
            {
                return input.getKey();
            }
        });
    }

}
