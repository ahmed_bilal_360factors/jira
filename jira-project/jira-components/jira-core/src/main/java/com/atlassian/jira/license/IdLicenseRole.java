package com.atlassian.jira.license;

import com.atlassian.fugue.Option;

import java.util.Collections;
import javax.annotation.Nonnull;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * A {@link com.atlassian.jira.license.LicenseRole} uses the data
 * in a {@link com.atlassian.jira.license.LicenseRoleId} to implement {@link #getId()} and {@link #getName()}.
 *
 * @since 6.4
 */
final class IdLicenseRole extends AbstractLicenseRole
{
    private final LicenseRoleId id;

    static IdLicenseRole empty(LicenseRoleId id)
    {
        return new IdLicenseRole(id, Collections.<String>emptyList(), Option.none(String.class));
    }

    IdLicenseRole(final LicenseRoleId id, final Iterable<String> groups, final Option<String> primaryGroup)
    {
        super(groups, primaryGroup);
        this.id = notNull("id", id);
    }

    @Nonnull
    @Override
    public LicenseRoleId getId()
    {
        return id;
    }

    @Nonnull
    @Override
    public String getName()
    {
        return id.getName();
    }

    @Nonnull
    @Override
    public LicenseRole withGroups(@Nonnull final Iterable<String> groups, @Nonnull final Option<String> primaryGroup)
    {
        return new IdLicenseRole(id, groups, primaryGroup);
    }
}
