/**
 * Copyright 2008 Atlassian Pty Ltd 
 */
package com.atlassian.jira.sharing.index;

import java.util.Set;

import javax.annotation.Nonnull;

import com.atlassian.jira.issue.index.IssueIndexingParams;
import com.atlassian.jira.sharing.SharedEntity;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.util.index.IndexLifecycleManager;

/**
 * Responsible for managing indexing of all shared entities
 * 
 * @since v3.13
 */
public interface SharedEntityIndexManager extends IndexLifecycleManager
{
    /**
     * Reindex the specified shared entity types.
     *
     * @param context used to report progress back to the user or to the logs.
     * @param issueIndexingParams indexes to reindex.
     * @param sharedEntityTypes the shared entity types to reindex.
     *
     * @return Reindex time in ms.
     *
     * @since 6.4
     */
    long reIndexSharedEntities(@Nonnull Context context, @Nonnull IssueIndexingParams issueIndexingParams, @Nonnull Set<SharedEntity.TypeDescriptor> sharedEntityTypes);
}
