package com.atlassian.jira.issue.attachment.store.strategy.move;

import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.util.concurrent.Promise;

/**
 * Moves temporary attachment to real attachment in single store.
 *
 * @since v6.4
 */
@ParametersAreNonnullByDefault
public class SingleStoreMoveStrategy implements MoveTemporaryAttachmentStrategy
{
    private final StreamAttachmentStore primaryStore;

    public SingleStoreMoveStrategy(final StreamAttachmentStore primaryStore)
    {
        this.primaryStore = primaryStore;
    }

    @Override
    public Promise<Unit> moveTemporaryToAttachment(final TemporaryAttachmentId temporaryAttachmentId, final AttachmentKey destinationKey)
    {
        return primaryStore.moveTemporaryToAttachment(temporaryAttachmentId, destinationKey);
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final SingleStoreMoveStrategy that = (SingleStoreMoveStrategy) o;

        return primaryStore.equals(that.primaryStore);
    }

    @Override
    public int hashCode()
    {
        return primaryStore.hashCode();
    }
}
