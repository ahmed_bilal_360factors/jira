package com.atlassian.jira.security;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.seraph.SecurityService;
import com.atlassian.seraph.config.SecurityConfig;

import com.google.common.collect.ImmutableSet;

/**
 * A security service that requires the {@code "admin"} role for any URL that starts with {@code "/secure/admin/"}.
 * <p>
 * JIRA used to use Seraph's own {@code PathService} to enforce this, but that is a general purpose service that
 * is really better suited to managing thousands of path-based rules.  We only have one rule, so it is overkill
 * and inefficient for our purposes.
 * </p>
 *
 * @since v6.4
 */
public class JiraPathService implements SecurityService
{
    private static final String SECURE_ADMIN_PREFIX = "/secure/admin/";

    private static final Set<String> ADMIN = ImmutableSet.of("admin");

    @Override
    public void init(Map<String, String> stringStringMap, SecurityConfig securityConfig)
    {
        // We are stateless
    }

    @Override
    public void destroy()
    {
        // We are stateless
    }

    @Override
    public Set<String> getRequiredRoles(HttpServletRequest httpServletRequest)
    {
        final String servletPath = httpServletRequest.getServletPath();
        if (servletPath != null && servletPath.startsWith(SECURE_ADMIN_PREFIX))
        {
            return ADMIN;
        }
        return ImmutableSet.of();
    }
}
