package com.atlassian.jira.index.property;

import javax.annotation.Nonnull;

import static com.atlassian.jira.index.IndexDocumentConfiguration.ExtractConfiguration;

/**
 * Builds JQL alias for the given issue property.
 */
public interface JqlAliasFactory
{
    JqlAlias createAlias(@Nonnull String pluginKey, @Nonnull String propertyKey, @Nonnull ExtractConfiguration extractConfiguration, @Nonnull String alias);
}
