package com.atlassian.jira.plugin;

import com.atlassian.jira.InitializingComponent;
import com.atlassian.jira.cache.GoogleCacheInstruments;
import com.atlassian.jira.cluster.ClusterSafe;
import com.atlassian.jira.util.map.CacheObject;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.container.OsgiContainerStartedEvent;
import com.atlassian.plugin.osgi.container.OsgiContainerStoppedEvent;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.concurrent.TimeUnit.MINUTES;

public class OsgiServiceTrackerCacheImpl implements OsgiServiceTrackerCache, InitializingComponent
{

    private static final Logger log = LoggerFactory.getLogger(OsgiServiceTrackerCacheImpl.class);

    private final OsgiContainerManager osgiContainerManager;

    private final PluginEventManager pluginEventManager;

    /**
     * A cache of service trackers.
     */
    @ClusterSafe
    private final Cache<String, CacheObject<ServiceTracker>> serviceTrackerCache;

    public OsgiServiceTrackerCacheImpl(OsgiContainerManager osgiContainerManager, PluginEventManager pluginEventManager)
    {
        this.osgiContainerManager = osgiContainerManager;
        this.pluginEventManager = pluginEventManager;
        serviceTrackerCache = CacheBuilder.newBuilder()
                .expireAfterAccess(15, MINUTES)
                .removalListener(new ServiceTrackerRemovalListener())
                .build(new ServiceTrackerLoader());
    }

    @Override
    public void afterInstantiation() throws Exception
    {
        pluginEventManager.register(this);
    }

    /**
     * Retrieves and returns a public component from OSGi land via its class.  This method can be used to retrieve
     * a component provided via a plugins OSGi bundle.  Please note that components returned via this method <b>should
     * NEVER be cached</b> (e.g. in a field) as they may be refreshed at any time as a plugin is enabled/disabled or the
     * ComponentManager is reinitialised (after an XML import).
     *
     * @param clazz The interface class
     * @return The component, or null if not found
     * @since v6.4
     */
    public <T> T getOsgiComponentOfType(final Class<T> clazz)
    {
        if (osgiContainerManager.isRunning())
        {
            ServiceTracker serviceTracker = getServiceTrackerFromCache(clazz.getName());
            if (serviceTracker != null)
            {
                return clazz.cast(serviceTracker.getService());
            }
        }
        else
        {
            log.debug("An attempt was made to retrieve an OSGi component of type:{} while the OSGi container was not "
                    + "running.", clazz);
        }

        return null;
    }

    /**
     * Registers the {@code serviceTrackers} cache in JIRA instrumentation.
     */
    @SuppressWarnings ({ "UnusedDeclaration" })
    @PluginEventListener
    public void onOsgiContainerStartedEvent(final OsgiContainerStartedEvent event)
    {
        new GoogleCacheInstruments(JiraOsgiContainerManager.class.getSimpleName()).addCache(serviceTrackerCache).install();
    }

    @SuppressWarnings ({ "UnusedDeclaration" })
    @PluginEventListener
    public void onPluginFrameworkShutdownEvent(OsgiContainerStoppedEvent event)
    {
        serviceTrackerCache.invalidateAll();
    }

    /**
     * Returns a cached service tracker. Note that the {@code ServiceTracker} instance may be closed at any time after
     * it has been returned, so <b>it is not advisable to hold a reference to it</b> (e.g. in a field).
     *
     * @param className a String containing the class name
     * @return a ServiceTracker, or null
     */
    private ServiceTracker getServiceTrackerFromCache(String className)
    {
        return serviceTrackerCache.getUnchecked(className).getValue();
    }

    /**
     * Loads service trackers from the OsgiContainerManager.
     */
    private class ServiceTrackerLoader extends CacheLoader<String, CacheObject<ServiceTracker>>
    {
        @Override
        public CacheObject<ServiceTracker> load(String className) throws Exception
        {
            ServiceTracker serviceTracker = osgiContainerManager.getServiceTracker(className);
            log.trace("Created service tracker for '{}': {}", className, serviceTracker);

            return CacheObject.wrap(serviceTracker);
        }
    }

    /**
     * Closes service trackers when they are evicted from the cache.
     */
    private static class ServiceTrackerRemovalListener implements RemovalListener<String, CacheObject<ServiceTracker>>
    {
        @Override
        public void onRemoval(RemovalNotification<String, CacheObject<ServiceTracker>> notification)
        {
            CacheObject<ServiceTracker> cacheObject = notification.getValue();
            if (cacheObject != null)
            {
                ServiceTracker serviceTracker = cacheObject.getValue();
                if (serviceTracker != null)
                {
                    log.trace("Closing service tracker: {}", serviceTracker);
                    serviceTracker.close();
                }
            }
        }
    }
}
