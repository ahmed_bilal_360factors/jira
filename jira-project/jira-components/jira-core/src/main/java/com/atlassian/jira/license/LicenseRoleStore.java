package com.atlassian.jira.license;

import com.atlassian.fugue.Option;

import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import static com.atlassian.jira.util.dbc.Assertions.containsNoNulls;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.collect.ImmutableSet.copyOf;

/**
 * Store for storing and retrieving {@link com.atlassian.jira.license.LicenseRoleStore.LicenseRoleData}.
 *
 * @since v6.4
 */
public interface LicenseRoleStore
{
    /**
     * Get the {@link com.atlassian.jira.license.LicenseRoleStore.LicenseRoleData} for the passed
     * {@link com.atlassian.jira.license.LicenseRoleId}.
     *
     * @param id the ID to search for.
     *
     * The {@link com.atlassian.jira.license.LicenseRoleStore.LicenseRoleData} for the passed
     * {@link com.atlassian.jira.license.LicenseRoleId}.
     */
    @Nonnull
    LicenseRoleData get(@Nonnull LicenseRoleId id);

    /**
     * Save the passed {@link com.atlassian.jira.license.LicenseRoleStore.LicenseRoleData} to the database.
     *
     * @param data the data to save to the database.
     * @return the {@link com.atlassian.jira.license.LicenseRoleStore.LicenseRoleData} as saved in the database.
     */
    @Nonnull
    LicenseRoleData save(@Nonnull LicenseRoleData data);

    /**
     * Removes the association of the given group from all license roles in the database.
     *
     * @param groupName the name of the group to remove.
     * @return true if any group-role associations were removed by this operation.
     */
    void removeGroup(@Nonnull String groupName);

    @Immutable
    final class LicenseRoleData
    {
        private final Set<String> groups;
        private final Option<String> primaryGroup;
        private final LicenseRoleId id;

        public LicenseRoleData(final LicenseRoleId id, final Iterable<String> groups, Option<String> groupName)
        {
            this.id = notNull("id", id);
            this.groups = copyOf(containsNoNulls("groups", groups));
            this.primaryGroup = notNull("groupName", groupName);
        }

        public Set<String> getGroups()
        {
            return groups;
        }

        public Option<String> getPrimaryGroup()
        {
            return primaryGroup;
        }

        public LicenseRoleId getId()
        {
            return id;
        }

        @Override
        public boolean equals(final Object o)
        {
            if (this == o) { return true; }
            if (o == null || getClass() != o.getClass()) { return false; }

            final LicenseRoleData that = (LicenseRoleData) o;

            if (!groups.equals(that.groups)) { return false; }
            if (!id.equals(that.id)) { return false; }
            if (!primaryGroup.equals(that.primaryGroup)) { return false; }

            return true;
        }

        @Override
        public int hashCode()
        {
            int result = groups.hashCode();
            result = 31 * result + primaryGroup.hashCode();
            result = 31 * result + id.hashCode();
            return result;
        }
    }
}
