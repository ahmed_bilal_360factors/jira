package com.atlassian.jira.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Class name abbreviator. This will abbreviate the package name of a class name
 * from, for example, com.atlassian.jira.util.ClassNameAbbreviator to c.a.j.u.ClassNameAbbreviator
 *
 * @since v6.4
 */
public class ClassNameAbbreviator
{
    public final static ClassNameAbbreviator DEFAULT = new ClassNameAbbreviator(5);
    private final int minChunks;

    /**
     * Create a new abbreviator.
     * @param minChunks if the name is not actually a class name pattern, we will still compress but leave this many chunks
     */
    public ClassNameAbbreviator(final int minChunks)
    {
        this.minChunks = minChunks;
    }

    public String getAbbreviatedName(final String name)
    {
        if (name == null || name.length() <= 2)
        {
            return name;
        }

        final StringBuilder buf = new StringBuilder();
        // compress all characters up until we find a '.' followed by an uppercase character
        Stack<Integer> chunkStart = new Stack<Integer>();
        chunkStart.push(0);
        int pos = 0;
        int leadingDot = 0;
        // If the first char is a dot add it into the buffer
        if (name.charAt(pos) == '.')
        {
            buf.append(name.charAt(pos));
            pos++;
            leadingDot = 1;
        }
        while (pos < name.length())
        {
            // Add everything after first upper case character
            if (Character.isUpperCase(name.charAt(pos)))
            {
                return buf.append(name, pos, name.length()).toString();
            }
            buf.append(name.charAt(pos));
            pos++;
            while (pos < name.length() )
            {
                if (name.charAt(pos) == '.')
                {
                    buf.append(name.charAt(pos));
                    chunkStart.push(pos);
                    pos++;
                    break;
                }
                pos++;
            }
        }
        // We didn't find a class so return the first abbreviations + minChunks whole bits
        int totalChunks = chunkStart.size();
        int wordsToCompress = Math.max(0, totalChunks - minChunks);
        int remainingChunks =  Math.min(totalChunks, minChunks);

        String prefix = "";
        if (wordsToCompress > 0)
        {
            int end = Math.min(2 * wordsToCompress - 1 + leadingDot, buf.length());
            prefix = buf.substring(0, end);
        }
        if (remainingChunks == 0)
        {
            return prefix;
        }
        // Find the beginning of the remaining chunks from the stack
        int suffixStart = 0;
        for (int i = 0; i < remainingChunks; i++)
        {
            suffixStart = chunkStart.pop();
        }
        return prefix + name.substring(suffixStart);
    }
}
