package com.atlassian.jira.plugin.webfragment.conditions;

import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.plugin.PluginParseException;

/**
 * Checks if the user can see issue type {@link AbstractJiraPermissionCondition#permission}
 */
public class UserHasProjectCondition extends AbstractJiraCondition
{
	private String projectKey;


    public boolean shouldDisplay(User user, JiraHelper jiraHelper)
    {
    	return (jiraHelper.getProjectObject().getKey().equals(projectKey));
    }
    

    public void init(Map params) throws PluginParseException
    {
    	projectKey = (String) params.get("projectKey");
        if (projectKey == null)
        {
            throw new PluginParseException("Could not determine project key for  for: " + params.get("projectKey"));
        }
    	super.init(params);
    }
}
