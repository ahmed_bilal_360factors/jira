package com.atlassian.jira.auditing;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.jira.license.LicenseDetails;

/**
 * This class carries information about license related entries in the Audit Log. This information appears on the title
 * of the entry.
 *
 * @since v6.4
 */
public class AffectedLicense implements AssociatedItem
{
    public static final String TYPE_LICENSE = "License SEN";
    public static final String TYPE_EVALUATION_LICENSE = "Accepted Evaluation";

    /** DUMMY_ID is used for parent and object ID as there is no concept for licenses nor a parent object. */
    private static final String DUMMY_ID = "0";
    private final String objectName;
    private final String licenseType;

    public AffectedLicense(@Nonnull final LicenseDetails license)
    {
        this(license.getSupportEntitlementNumber(), false);
    }

    public AffectedLicense(final String objectName, @Nonnull final Boolean isEvaluation)
    {
        this.objectName = objectName == null ? "<SEN N/A>" : objectName;
        this.licenseType = isEvaluation ? AffectedLicense.TYPE_EVALUATION_LICENSE : AffectedLicense.TYPE_LICENSE;
    }

    @Override
    @Nonnull
    public String getObjectName()
    {
        return objectName;
    }

    @Override
    public String getObjectId()
    {
        return DUMMY_ID;
    }

    @Nullable
    @Override
    public String getParentName()
    {
        return licenseType;
    }

    @Nullable
    @Override
    public String getParentId()
    {
        return DUMMY_ID;
    }

    @Override
    @Nonnull
    public Type getObjectType()
    {
        return Type.LICENSE;
    }
}
