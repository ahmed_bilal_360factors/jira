package com.atlassian.jira.upgrade.tasks;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Removes old indexes from jiraaction table that are rarely if ever helpful and may even be harmful to performance.
 *
 * @since v6.3.6 (as 6336)
 */
public class UpgradeTask_Build64005 extends DropIndexTask
{
    private static final Logger LOG = LoggerFactory.getLogger(UpgradeTask_Build64005.class);

    public UpgradeTask_Build64005()
    {
        super();
    }

    @Override
    public String getBuildNumber()
    {
        return "64005";
    }

    private void dropIndex(final Connection connection, final String tableName, final String indexName)
    {
        try
        {
            final String sql = buildDropIndexSql(tableName, indexName);
            final Statement update = connection.createStatement();
            try
            {
                update.execute(sql);
            }
            finally
            {
                update.close();
            }
        }
        catch (SQLException sqle)
        {
            LOG.debug("Ignoring error dropping old index", sqle);
        }
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception
    {
        if (setupMode)
        {
            // There is no need to do this on a clean install
            return;
        }

        final Connection connection = getDatabaseConnection();
        try
        {
            dropIndex(connection, "jiraaction", "action_authorcreated");
            dropIndex(connection, "jiraaction", "action_authorupdated");
        }
        finally
        {
            connection.close();
        }
    }
    @Override
    public String getShortDescription()
    {
        return "Remove old indexes from jiraaction table";
    }
}
