package com.atlassian.jira.jql.query;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.operator.OperatorClasses;
import com.atlassian.jira.jql.resolver.SavedFilterResolver;
import com.atlassian.jira.jql.validator.SavedFilterCycleDetector;
import com.atlassian.jira.util.InjectableComponent;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operator.Operator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;

import java.util.List;

import com.google.common.collect.Lists;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * A query factory that will generate a query for a saved filter.
 *
 * @since v4.0
 */
@InjectableComponent
public class SavedFilterClauseQueryFactory implements ClauseQueryFactory
{
    private static final Logger log = LoggerFactory.getLogger(SavedFilterClauseQueryFactory.class);

    private final SavedFilterResolver savedFilterResolver;
    private final JqlOperandResolver jqlOperandResolver;
    private final SavedFilterCycleDetector savedFilterCycleDetector;
    private final QueryVisitor queryVisitor;

    public SavedFilterClauseQueryFactory(final SavedFilterResolver savedFilterResolver, final JqlOperandResolver jqlOperandResolver,
            final SavedFilterCycleDetector savedFilterCycleDetector, final QueryVisitor queryVisitor)
    {
        this.savedFilterResolver = savedFilterResolver;
        this.jqlOperandResolver = jqlOperandResolver;
        this.savedFilterCycleDetector = savedFilterCycleDetector;
        this.queryVisitor = queryVisitor;
    }

    public QueryFactoryResult getQuery(final QueryCreationContext queryCreationContext, final TerminalClause terminalClause)
    {
        notNull("queryCreationContext", queryCreationContext);

        if (!OperatorClasses.EQUALITY_OPERATORS.contains(terminalClause.getOperator()))
        {
            return QueryFactoryResult.createFalseResult();
        }

        final List<QueryLiteral> rawValues = jqlOperandResolver.getValues(queryCreationContext, terminalClause.getOperand(), terminalClause);
        final List<SearchRequest> matchingFilters = queryCreationContext.isSecurityOverriden() ?
                savedFilterResolver.getSearchRequestOverrideSecurity(rawValues)
                : savedFilterResolver.getSearchRequest(queryCreationContext.getUser(), rawValues);
        final List<Query> queries = Lists.newArrayListWithCapacity(matchingFilters.size());

        for (SearchRequest filter : matchingFilters)
        {
            if (savedFilterCycleDetector.containsSavedFilterReference(queryCreationContext.getQueryUser(), queryCreationContext.isSecurityOverriden(), filter, null))
            {
                log.debug(String.format("Saved filter with id '%d' contains a reference to itself; a query cannot be generated from this.", filter.getId()));
                return QueryFactoryResult.createFalseResult();
            }
            queries.add(getQueryFromSavedFilter(queryCreationContext, filter));
        }

        final Operator operator = terminalClause.getOperator();
        final boolean mustNotOccur = Operator.NOT_EQUALS == operator || Operator.NOT_IN == operator;

        if (queries.isEmpty())
        {
            // Did not find any matching filters so we will return false
            return QueryFactoryResult.createFalseResult();
        }
        else if(queries.size() == 1)
        {
            // Only return one clause and let the negation be handled by the visitor
            return new QueryFactoryResult(queries.get(0), mustNotOccur);
        }
        else
        {
            // OR all the queries together and let the negation be handled by the visitor
            final BooleanQuery boolQuery = new BooleanQuery();
            for (Query query : queries)
            {
                boolQuery.add(query, BooleanClause.Occur.SHOULD);
            }
            return new QueryFactoryResult(boolQuery, mustNotOccur);
        }
    }

    ///CLOVER:OFF
    Query getQueryFromSavedFilter(final QueryCreationContext queryCreationContext, final SearchRequest savedFilter)
    {
        if (savedFilter.getQuery().getWhereClause() == null)
        {
            return new MatchAllDocsQuery();
        }
        return queryVisitor.createQuery(savedFilter.getQuery().getWhereClause(), queryCreationContext);
    }
    ///CLOVER:ON

}
