package com.atlassian.jira.issue.index;

import java.util.List;

import javax.annotation.Nonnull;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.index.DefaultIssueIndexer.WorklogRetriever;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogManager;
import com.atlassian.jira.issue.worklog.WorklogStore;

public class DefaultWorklogRetriever implements WorklogRetriever
{
    @Override
    public List<Worklog> apply(final Issue issue)
    {
        return ComponentAccessor.getWorklogManager().getByIssue(issue);
    }

}
