package com.atlassian.jira.issue.attachment.store.provider;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.attachment.FileSystemAttachmentStore;
import com.atlassian.jira.issue.attachment.RemoteAttachmentStore;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.store.DualSendingAttachmentStore;
import com.atlassian.jira.issue.attachment.store.strategy.StoreOperationStrategy;
import com.atlassian.jira.issue.attachment.store.strategy.StoreOperationStrategyFactory;
import com.atlassian.jira.issue.attachment.store.strategy.get.AttachmentGetStrategiesFactory;
import com.atlassian.jira.issue.attachment.store.strategy.get.DualAttachmentGetStrategy;
import com.atlassian.jira.issue.attachment.store.strategy.move.BackgroundResendingMoveStrategy;
import com.atlassian.jira.issue.attachment.store.strategy.move.MoveTemporaryAttachmentStrategy;
import com.atlassian.jira.issue.attachment.store.strategy.move.MoveTemporaryStrategiesFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider.Mode.FS_ONLY;
import static com.atlassian.jira.issue.attachment.store.provider.BlobStoreAttachmentStoreModeProvider.Mode.FS_PRIMARY;

/**
 * Provides instances of {@link com.atlassian.jira.issue.attachment.StreamAttachmentStore} configured for OD
 * environments.
 *
 * @since v6.4
 */
public class StreamAttachmentStoreProviderImpl implements StreamAttachmentStoreProvider
{
    private static final Logger log = LoggerFactory.getLogger(StreamAttachmentStoreProviderImpl.class);
    private final BlobStoreAttachmentStoreModeProvider blobStoreAttachmentStoreModeProvider;
    private final FileSystemAttachmentStore fileSystemAttachmentStore;
    private final RemoteAttachmentStore remoteAttachmentStore;
    private final MoveTemporaryStrategiesFactory moveTemporaryStrategiesFactory;
    private final AttachmentGetStrategiesFactory attachmentGetStrategiesFactory;
    private final StoreOperationStrategyFactory storeOperationStrategyFactory;

    public StreamAttachmentStoreProviderImpl(
            final BlobStoreAttachmentStoreModeProvider blobStoreAttachmentStoreModeProvider,
            final FileSystemAttachmentStore fileSystemAttachmentStore,
            final RemoteAttachmentStore remoteAttachmentStore,
            final MoveTemporaryStrategiesFactory moveTemporaryStrategiesFactory,
            final AttachmentGetStrategiesFactory attachmentGetStrategiesFactory,
            final StoreOperationStrategyFactory storeOperationStrategyFactory)
    {
        this.blobStoreAttachmentStoreModeProvider = blobStoreAttachmentStoreModeProvider;
        this.fileSystemAttachmentStore = fileSystemAttachmentStore;
        this.remoteAttachmentStore = remoteAttachmentStore;
        this.moveTemporaryStrategiesFactory = moveTemporaryStrategiesFactory;
        this.attachmentGetStrategiesFactory = attachmentGetStrategiesFactory;
        this.storeOperationStrategyFactory = storeOperationStrategyFactory;
    }

    @Override
    public StreamAttachmentStore getStreamAttachmentStore()
    {
        final BlobStoreAttachmentStoreModeProvider.Mode mode = blobStoreAttachmentStoreModeProvider.mode();
        switch (mode)
        {
            case FS_ONLY:
                return fileSystemAttachmentStore;
            case FS_PRIMARY:
                return createStoreForFileSystemPrimary();
            case REMOTE_PRIMARY:
                return createStoreForRemotePrimary();
            case REMOTE_ONLY:
                return remoteAttachmentStore;
            default:
                log.warn("Unknown attachmentStore mode: {}, default File System store will be used to store and retrieve "
                        + "attachments", mode);
                return fileSystemAttachmentStore;
        }
    }

    private StreamAttachmentStore createStoreForFileSystemPrimary()
    {
        final BackgroundResendingMoveStrategy backgroundResendingStrategy = moveTemporaryStrategiesFactory.createBackgroundResendingStrategy(
                fileSystemAttachmentStore, remoteAttachmentStore);

        final DualAttachmentGetStrategy dualAttachmentGetStrategy = attachmentGetStrategiesFactory.createDualGetAttachmentStrategy(
                fileSystemAttachmentStore, remoteAttachmentStore);
        final StoreOperationStrategy operationStrategy = storeOperationStrategyFactory.createFailoverOperationStrategy(
                fileSystemAttachmentStore, remoteAttachmentStore);

        return new DualSendingAttachmentStore(
                fileSystemAttachmentStore,
                remoteAttachmentStore,
                dualAttachmentGetStrategy,
                backgroundResendingStrategy, operationStrategy);
    }

    private StreamAttachmentStore createStoreForRemotePrimary()
    {
        final MoveTemporaryAttachmentStrategy moveSingleStrategy = moveTemporaryStrategiesFactory.createMoveOnlyPrimaryStrategy(
                remoteAttachmentStore);
        //for remote primary we also use fs as primary store for get operations
        final DualAttachmentGetStrategy dualAttachmentGetStrategy = attachmentGetStrategiesFactory.createDualGetAttachmentStrategy(
                fileSystemAttachmentStore, remoteAttachmentStore);
        final StoreOperationStrategy operationStrategy = storeOperationStrategyFactory.createFailoverOperationStrategy(
                remoteAttachmentStore, fileSystemAttachmentStore);
        return new DualSendingAttachmentStore(
                remoteAttachmentStore,
                fileSystemAttachmentStore,
                dualAttachmentGetStrategy,
                moveSingleStrategy, operationStrategy);
    }

    @Override
    public Option<FileSystemAttachmentStore> getFileSystemStore()
    {
        final BlobStoreAttachmentStoreModeProvider.Mode mode = blobStoreAttachmentStoreModeProvider.mode();
        if (mode == FS_ONLY || mode == FS_PRIMARY)
        {
            return Option.some(fileSystemAttachmentStore);
        }
        else
        {
            return Option.none();
        }
    }
}
