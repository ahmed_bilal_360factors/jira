package com.atlassian.jira.web.filters;

import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.web.servlet.InternalServerErrorHelper;
import com.atlassian.jira.web.util.InternalServerErrorDataSource;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import com.google.common.collect.ImmutableMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A filter that attempts to handle errors that occur after the HTTP response has already been committed (eg. due to
 * Flush Head Early.)
 * For use where the default error handling (an HTTP 500 response rendered by InternalServerErrorServlet) won't suffice.
 *
 * Our current strategy is to:
 * 1. pre-render the standard error page HTML
 * 2. wrap pre-rendered page in some magic javascript
 * 3. dump the result into the middle of the response outputstream
 *
 * We hope that the browser will execute the aforementioned magic javascript as soon as it sees it, which will destroy
 * and re-render the page client-side with the HTML we generated in (1.) above.
 *
 * We must hope the user is a human that pays attention to what we're rendering, not a machine that pays attention to
 * the HTTP status code we've already transmitted (most likely 200 OK.)
 */
public class CommittedResponseHtmlErrorRecoveryFilter implements Filter
{
    public static final String ENABLE_COMMITTED_RESPONSE_HTML_ERROR_RECOVERY = "ENABLE_COMMITTED_RESPONSE_HTML_ERROR_RECOVERY".toString();

    private static final Logger log = LoggerFactory.getLogger(CommittedResponseHtmlErrorRecoveryFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
    }

    @Override
    public void destroy()
    {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException
    {
        try
        {
            filterChain.doFilter(servletRequest, servletResponse);
        }
        catch (ServletException servletException)
        {
            boolean success = attemptToRecoverFromExceptionWhenResponseAlreadyCommitted(servletRequest, servletResponse, servletException);
            if (!success)
            {
                throw servletException;
            }
        }
        catch (RuntimeException runtimeException)
        {
            boolean success = attemptToRecoverFromExceptionWhenResponseAlreadyCommitted(servletRequest, servletResponse, runtimeException);
            if (!success)
            {
                throw runtimeException;
            }
        }
        catch (IOException ioException)
        {
            boolean success = attemptToRecoverFromExceptionWhenResponseAlreadyCommitted(servletRequest, servletResponse, ioException);
            if (!success)
            {
                throw ioException;
            }
        }
    }

    private boolean attemptToRecoverFromExceptionWhenResponseAlreadyCommitted(ServletRequest servletRequest, ServletResponse servletResponse, Exception exception)
            throws IOException
    {
        if (Boolean.TRUE.equals(servletRequest.getAttribute(ENABLE_COMMITTED_RESPONSE_HTML_ERROR_RECOVERY))
                && servletResponse.isCommitted() && servletRequest instanceof HttpServletRequest)
        {
            // Ah crap. We've already committed the response - how can we communicate to the user than an exception
            // occurred at this point? We're already in the middle of sending an HTTP 200 OK response :(
            // Let's attempt to inject something into the response that will just render an error page client-side
            // (and hope the user is a human, not a machine!)

            log.error("Exception occurred after HTTP response was already committed: " + exception.getMessage(), exception);

            // our standard error page expects to find the exception on the request...
            servletRequest.setAttribute(InternalServerErrorDataSource.JAVAX_SERVLET_ERROR_EXCEPTION, exception);
            servletRequest.setAttribute(InternalServerErrorDataSource.JAVAX_SERVLET_ERROR_MESSAGE, exception.getMessage());

            // pre-render our standard error page
            StringWriter stringWriter = new StringWriter();
            InternalServerErrorHelper.render500ResponsePage((HttpServletRequest) servletRequest, stringWriter);
            String errorPageHtml = stringWriter.toString();

            // wrap our pre-rendered error page in @flaves' magic javascript that performs a client-side redraw;
            // dump that straight into the response outputstream, regardless of where we're up to. Fingers crossed!
            ImmutableMap.Builder<String, Object> map = ImmutableMap.builder();
            map.put("contextPath", ((HttpServletRequest) servletRequest).getContextPath());
            map.put("errorPageHtml", errorPageHtml);
            SoyTemplateRenderer renderer = ComponentAccessor.getOSGiComponentInstanceOfType(SoyTemplateRenderer.class);
            try
            {
                renderer.render(servletResponse.getWriter(), "jira.webresources:committed-response-html-error-recovery",
                        "JIRA.Templates.errors.Misc.committedResponseHtmlErrorRecovery", map.build());
            }
            catch (SoyException soyException)
            {
                log.error("Cannot render soy template", soyException);
            }

            // We're justified in closing the response writer here; nobody else should be writing to it.
            // THE WORLD HAS ENDED AND WE'RE BAILING OUT.
            // Also: despite our handling the exception here, tomcat still knows about it via the response.isErrorAfterCommit()
            // flag - tomcat_7.0.55 actually tries to handle this condition (org.apache.catalina.valves.ErrorReportValve:106)
            // by simply flushing the buffer and giving up. In doing so it fails to complete the chunked-encoding protocol
            // correctly, making the entire response invalid HTTP. We consider this behaviour a bug in tomcat. By calling
            // close() here we trick tomcat into finishing the response early, correctly completing the chunked-encoding
            // protocol /before it notices response.isErrorAfterCommit() is set/.
            servletResponse.getWriter().close();

            return true;  // success
        }
        else
        {
            // If we haven't committed the response yet then a default mechanism exists that can handle
            // this exception appropriately somewhere else - let's just report failure so that it can be rethrown.
            return false;
        }
    }
}
