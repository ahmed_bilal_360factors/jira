package com.atlassian.jira.bc.workflow;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.action.util.workflow.WorkflowEditorTransitionConditionUtil;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.google.common.annotations.VisibleForTesting;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.ConditionDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import com.opensymphony.workflow.loader.ResultDescriptor;

import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.fugue.Either.left;
import static com.atlassian.fugue.Either.right;
import static com.atlassian.fugue.Option.option;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.workflow.WorkflowUtil.setActionScreen;

public class DefaultWorkflowTransitionService implements WorkflowTransitionService
{
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final WorkflowEditorTransitionConditionUtil transitionConditionUtil;
    private final WorkflowService workflowService;

    public DefaultWorkflowTransitionService(
            JiraAuthenticationContext jiraAuthenticationContext,
            WorkflowService workflowService)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.transitionConditionUtil = new WorkflowEditorTransitionConditionUtil();
        this.workflowService = workflowService;
    }

    @Override
    public ErrorCollection addConditionToWorkflow(@Nonnull String transitionName, @Nonnull ConditionDescriptor condition, @Nonnull JiraWorkflow workflow)
    {
        ErrorCollection errorCollection = checkArguments(transitionName, workflow, condition,
                "admin.workflowtransitions.service.error.null.condition");
        if (errorCollection.hasAnyErrors())
        {
            return errorCollection;
        }

        Option<JiraWorkflow> getDraft = draftOf(errorCollection, workflow);
        if (getDraft.isEmpty())
        {
            //Draft creation might fail, if so, return the resulting error.
            return errorCollection;
        }

        JiraWorkflow draftWorkflow = getDraft.get();
        Either<Collection<ActionDescriptor>, ErrorCollection> getActions = getActionsByName(transitionName, draftWorkflow);
        if (getActions.isRight())
        {
            return getActions.right().get();
        }

        for (ActionDescriptor action : getActions.left().get())
        {
            transitionConditionUtil.addCondition(action, "", condition);
        }

        workflowService.updateWorkflow(getServiceContext(errorCollection), draftWorkflow);

        return errorCollection;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ErrorCollection addPostFunctionToWorkflow(@Nonnull String transitionName, @Nonnull FunctionDescriptor function, @Nonnull JiraWorkflow workflow)
    {
        ErrorCollection errorCollection = checkArguments(transitionName, workflow, function,
                "admin.workflowtransitions.service.error.null.function");
        if (errorCollection.hasAnyErrors())
        {
            return errorCollection;
        }

        Option<JiraWorkflow> getDraft = draftOf(errorCollection, workflow);
        if (getDraft.isEmpty())
        {
            //Draft creation might fail, if so, return the resulting error.
            return errorCollection;
        }

        JiraWorkflow draftWorkflow = getDraft.get();
        Either<Collection<ActionDescriptor>, ErrorCollection> getActions = getActionsByName(transitionName, draftWorkflow);
        if (getActions.isRight())
        {
            return getActions.right().get();
        }

        for (ActionDescriptor action : getActions.left().get())
        {
            ResultDescriptor unconditionalResult = action.getUnconditionalResult();
            List postFunctions = unconditionalResult.getPostFunctions();
            postFunctions.add(0, function);
        }

        workflowService.updateWorkflow(getServiceContext(errorCollection), draftWorkflow);

        return errorCollection;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ErrorCollection setScreen(@Nonnull String transitionName, @Nullable FieldScreen screen, @Nonnull JiraWorkflow workflow)
    {
        ErrorCollection errorCollection = checkArguments(transitionName, workflow);
        if (errorCollection.hasAnyErrors())
        {
            return errorCollection;
        }

        Option<JiraWorkflow> getDraft = draftOf(errorCollection, workflow);
        if (getDraft.isEmpty())
        {
            return errorCollection;
        }

        JiraWorkflow draftWorkflow = getDraft.get();
        Either<Collection<ActionDescriptor>, ErrorCollection> getActions = getActionsByName(transitionName, draftWorkflow);
        if (getActions.isRight())
        {
            return getActions.right().get();
        }

        for (ActionDescriptor action : getActions.left().get())
        {
            setActionScreen(action, screen);
        }

        workflowService.updateWorkflow(getServiceContext(errorCollection), draftWorkflow);

        return errorCollection;
    }

    private ErrorCollection checkArguments(String transitionName, JiraWorkflow jiraWorkflow)
    {
        ErrorCollection errorCollection = new SimpleErrorCollection();
        I18nHelper i18n = jiraAuthenticationContext.getI18nHelper();
        checkActionAndWorkflow(transitionName, jiraWorkflow, errorCollection, i18n);
        return errorCollection;
    }

    private ErrorCollection checkArguments(String transitionName, JiraWorkflow jiraWorkflow, Object object, String i18nKeyForObject)
    {
        ErrorCollection errorCollection = new SimpleErrorCollection();
        I18nHelper i18n = jiraAuthenticationContext.getI18nHelper();
        checkActionAndWorkflow(transitionName, jiraWorkflow, errorCollection, i18n);
        if (object == null)
        {
            errorCollection.addErrorMessage(i18n.getText(i18nKeyForObject));
        }
        return errorCollection;
    }

    private void checkActionAndWorkflow(String transitionName, JiraWorkflow jiraWorkflow, ErrorCollection errorCollection, I18nHelper i18n)
    {
        if (transitionName == null)
        {
            errorCollection.addErrorMessage(i18n.getText("admin.workflowtransitions.service.error.null.action.name"));
        }
        if (jiraWorkflow == null)
        {
            errorCollection.addErrorMessage(i18n.getText("admin.workflowtransitions.service.error.null.workflow"));
        }
    }

    @VisibleForTesting
    Option<JiraWorkflow> draftOf(ErrorCollection errorCollection, JiraWorkflow jiraWorkflow)
    {
        JiraServiceContext jiraServiceContext = getServiceContext(errorCollection);
        JiraWorkflow existingDraftWorkflow = workflowService.getDraftWorkflow(jiraServiceContext, jiraWorkflow.getName());
        return existingDraftWorkflow == null
                ? option(workflowService.createDraftWorkflow(jiraServiceContext, jiraWorkflow.getName()))
                : some(existingDraftWorkflow);
    }

    private Either<Collection<ActionDescriptor>, ErrorCollection> getActionsByName(String transitionName, JiraWorkflow draftWorkflow)
    {
        //Retrieves all actions that match the name and return them.
        Collection<ActionDescriptor> actions = draftWorkflow.getActionsByName(transitionName);
        if (actions.isEmpty())
        {
            I18nHelper i18n = jiraAuthenticationContext.getI18nHelper();
            ErrorCollection errorCollection = new SimpleErrorCollection();
            errorCollection.addErrorMessage(i18n.getText("admin.workflowtransitions.service.error.no.action", transitionName));
            return right(errorCollection);
        }
        return left(actions);
    }

    private JiraServiceContext getServiceContext(ErrorCollection errorCollection)
    {
        return new JiraServiceContextImpl(jiraAuthenticationContext.getUser(), errorCollection);
    }
}
