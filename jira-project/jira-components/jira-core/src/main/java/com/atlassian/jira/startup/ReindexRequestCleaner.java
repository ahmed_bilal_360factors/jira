package com.atlassian.jira.startup;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.index.request.ReindexRequestManager;

/**
 * Cleans up reindex requests that were running on this node and then the node was killed.
 *
 * @since 6.4
 */
public class ReindexRequestCleaner implements JiraLauncher
{

    @Override
    public void start()
    {
        //Any running tasks from previous instance of this node should be failed
        final ReindexRequestManager reindexRequestManager = ComponentAccessor.getComponent(ReindexRequestManager.class);
        final ClusterManager clusterManager = ComponentAccessor.getComponent(ClusterManager.class);
        reindexRequestManager.failRunningRequestsFromNode(clusterManager.getNodeId());
    }

    @Override
    public void stop()
    {
        //Node stopped gracefully, any active reindex requests now fail
        final ReindexRequestManager reindexRequestManager = ComponentAccessor.getComponent(ReindexRequestManager.class);
        final ClusterManager clusterManager = ComponentAccessor.getComponent(ClusterManager.class);
        reindexRequestManager.failRunningRequestsFromNode(clusterManager.getNodeId());
    }
}
