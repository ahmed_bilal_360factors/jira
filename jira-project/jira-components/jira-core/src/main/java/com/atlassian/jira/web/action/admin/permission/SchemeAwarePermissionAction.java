/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

package com.atlassian.jira.web.action.admin.permission;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.PermissionTypeManager;
import com.atlassian.jira.scheme.AbstractSchemeAwareAction;
import com.atlassian.jira.scheme.SchemeType;

/**
 * This class is used as a super class for any classes that perform actions on permission schemes.
 * It contains the schemeId and the actual (GenericValue) scheme object
 */
public class SchemeAwarePermissionAction extends AbstractSchemeAwareAction
{
    public SchemeType getType(String id)
    {
        return ComponentAccessor.getComponent(PermissionTypeManager.class).getSchemeType(id);
    }

    @Override
    public PermissionSchemeManager getSchemeManager()
    {
        return ComponentAccessor.getPermissionSchemeManager();
    }

    @Override
    public String getRedirectURL()
    {
        return null;
    }
}
