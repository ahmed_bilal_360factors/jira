package com.atlassian.jira.issue.attachment;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.fugue.Either;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.AttachmentValidator;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.bean.I18nBean;

import com.google.common.base.Function;

import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ATTACHMENTS;

/**
 * Default implementations for {@link com.atlassian.jira.issue.AttachmentValidator}
 *
 * @since v6.4
 */
@ParametersAreNonnullByDefault
public class DefaultAttachmentValidator implements AttachmentValidator
{
    private final I18nBean.BeanFactory i18nBeanFactory;
    private final PermissionManager permissionManager;
    private final IssueManager issueManager;
    private final AttachmentManager attachmentManager;

    public DefaultAttachmentValidator(
            final I18nHelper.BeanFactory i18nBeanFactory,
            final PermissionManager permissionManager,
            final IssueManager issueManager,
            final AttachmentManager attachmentManager)
    {
        this.i18nBeanFactory = i18nBeanFactory;
        this.permissionManager = permissionManager;
        this.issueManager = issueManager;
        this.attachmentManager = attachmentManager;
    }

    @Override
    public boolean canCreateAttachments(@Nullable final ApplicationUser user, final Issue issue,
            final ErrorCollection errorCollection)
    {
        return validateAttachmentsEnabled(user, errorCollection)
                && validateCreateAttachmentsWithoutWorkflow(user, issue, errorCollection)
                && validateIssueEditable(user, issue, errorCollection);

    }

    @Override
    public boolean canCreateTemporaryAttachments(@Nullable final ApplicationUser user,
            final Either<Issue, Project> issueOrProject, final ErrorCollection errorCollection)
    {
        return validateAttachmentsEnabled(user, errorCollection)
                && issueOrProject.fold(
                new Function<Issue, Boolean>()
                {
                    @Override
                    public Boolean apply(@SuppressWarnings ("NullableProblems") final Issue issue)
                    {
                        return validateCreateAttachmentsWithoutWorkflow(user, issue, errorCollection);
                    }
                }, new Function<Project, Boolean>()
                {
                    @Override
                    public Boolean apply(@SuppressWarnings ("NullableProblems") final Project project)
                    {
                        return validateCreateTemporaryAttachmentsInProject(user, project, errorCollection);
                    }
                });
    }

    private I18nHelper getI18n(@Nullable final ApplicationUser user)
    {
        return i18nBeanFactory.getInstance(user);
    }

    private boolean validateCreateTemporaryAttachmentsInProject(@Nullable final ApplicationUser user,
            final Project project, final ErrorCollection errorCollection)
    {
        return processValidationResult(permissionManager.hasPermission(CREATE_ATTACHMENTS, project, user),
                "attachment.service.error.create.no.permission.project", user, errorCollection);
    }

    private boolean validateCreateAttachmentsWithoutWorkflow(@Nullable final ApplicationUser user, final Issue issue,
            final ErrorCollection errorCollection)
    {
        return processValidationResult(permissionManager.hasPermission(CREATE_ATTACHMENTS, issue, user),
                "attachment.service.error.create.no.permission", user, errorCollection);
    }

    private boolean validateAttachmentsEnabled(@Nullable final ApplicationUser user, final ErrorCollection errorCollection)
    {
        return processValidationResult(attachmentManager.attachmentsEnabled(),
                "attachment.service.error.attachments.disabled", user, errorCollection);
    }

    private boolean validateIssueEditable(@Nullable final ApplicationUser user, final Issue issue,
            final ErrorCollection errorCollection)
    {
        return processValidationResult(issueManager.isEditable(issue),
                "attachment.service.error.create.issue.non.editable", user, errorCollection);
    }

    private boolean processValidationResult(final boolean result, final String errorMessageKey,
            @Nullable final ApplicationUser user, final ErrorCollection errorCollection)
    {
        if (!result)
        {
            errorCollection.addErrorMessage(getI18n(user).getText(errorMessageKey));
            return false;
        }
        else
        {
            return true;
        }
    }
}
