package com.atlassian.jira.index.property;

import java.util.List;

import com.atlassian.fugue.Option;
import com.atlassian.jira.entity.property.EntityPropertyType;

import com.google.common.base.Function;

import static com.atlassian.fugue.Options.flatten;
import static com.atlassian.jira.entity.property.EntityPropertyType.ISSUE_PROPERTY;
import static com.atlassian.jira.index.IndexDocumentConfiguration.ExtractConfiguration;
import static com.atlassian.jira.index.IndexDocumentConfiguration.KeyConfiguration;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;

public class DefaultJqlAliasManager implements JqlAliasManager
{
    private final PluginIndexConfigurationManager pluginIndexConfigurationManager;
    private final JqlAliasFactory jqlAliasFactory;

    public DefaultJqlAliasManager(final PluginIndexConfigurationManager pluginIndexConfigurationManager, final JqlAliasFactory jqlAliasFactory)
    {
        this.pluginIndexConfigurationManager = pluginIndexConfigurationManager;
        this.jqlAliasFactory = jqlAliasFactory;
    }

    @Override
    public Iterable<JqlAlias> getJqlAliases()
    {
        final Iterable<PluginIndexConfiguration> indexConfigurations =
                pluginIndexConfigurationManager.getDocumentsForEntity(ISSUE_PROPERTY.getDbEntityName());

        Iterable<Iterable<KeyConfigurationWrapper>> allKeyConfigurations = transform(indexConfigurations, new Function<PluginIndexConfiguration, Iterable<KeyConfigurationWrapper>>()
        {
            @Override
            public Iterable<KeyConfigurationWrapper> apply(final PluginIndexConfiguration pluginIndexConfiguration)
            {
                final List<KeyConfiguration> keyConfigurations = pluginIndexConfiguration.getIndexDocumentConfiguration().getKeyConfigurations();
                return transform(keyConfigurations, KeyConfigurationWrapper.build(pluginIndexConfiguration));
            }
        });

        return createAliases(concat(allKeyConfigurations));
    }

    private Iterable<JqlAlias> createAliases(final Iterable<KeyConfigurationWrapper> indexConfigurations)
    {
        final Iterable<Iterable<Option<JqlAlias>>> aliases = transform(indexConfigurations, new Function<KeyConfigurationWrapper, Iterable<Option<JqlAlias>>>()
        {
            @Override
            public Iterable<Option<JqlAlias>> apply(final KeyConfigurationWrapper keyConfigurationWrapper)
            {
                return transform(keyConfigurationWrapper.getKeyConfiguration().getExtractorConfigurations(), new Function<ExtractConfiguration, Option<JqlAlias>>()
                {
                    @Override
                    public Option<JqlAlias> apply(final ExtractConfiguration extractConfiguration)
                    {
                        return extractConfiguration.getAlias().map(new Function<String, JqlAlias>()
                        {
                            @Override
                            public JqlAlias apply(final String alias)
                            {
                                return jqlAliasFactory.createAlias(keyConfigurationWrapper.pluginKey,
                                        keyConfigurationWrapper.getKeyConfiguration().getPropertyKey(),
                                        extractConfiguration,
                                        alias);
                            }
                        });
                    }
                });
            }
        });

        return flatten(concat(aliases));
    }

    private static class KeyConfigurationWrapper
    {
        private final String pluginKey;
        private final String moduleKey;
        private final KeyConfiguration keyConfiguration;

        private KeyConfigurationWrapper(final String pluginKey, final String moduleKey, final KeyConfiguration keyConfiguration)
        {
            this.pluginKey = pluginKey;
            this.moduleKey = moduleKey;
            this.keyConfiguration = keyConfiguration;
        }

        public KeyConfiguration getKeyConfiguration()
        {
            return keyConfiguration;
        }

        public static Function<KeyConfiguration, KeyConfigurationWrapper> build(final PluginIndexConfiguration pluginIndexConfiguration)
        {
            return new Function<KeyConfiguration, KeyConfigurationWrapper>()
            {
                @Override
                public KeyConfigurationWrapper apply(final KeyConfiguration keyConfiguration)
                {
                    return new KeyConfigurationWrapper(pluginIndexConfiguration.getPluginKey(), pluginIndexConfiguration.getModuleKey(), keyConfiguration);
                }
            };
        }
    }

}
