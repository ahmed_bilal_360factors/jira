package com.atlassian.jira.security.roles;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.database.SqlCallback;
import com.atlassian.jira.model.querydsl.QProjectRoleActor;
import com.atlassian.jira.plugin.roles.ProjectRoleActorModuleDescriptor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.ozymandias.PluginPointFunction;
import com.atlassian.ozymandias.PluginPointVisitor;
import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.util.profiling.UtilTimerStack;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.mysema.query.Tuple;
import com.mysema.query.types.Projections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Creator of RoleActor objects that have been registered dynamically which delegates the calls to plugin points.
 */
public class PluginDelegatingRoleActorFactory implements OptimizedRoleActorFactory
{
    private static final Logger log = LoggerFactory.getLogger(PluginDelegatingRoleActorFactory.class);

    private final PluginAccessor pluginAccessor;
    private final DbConnectionManager dbConnectionManager;

    public PluginDelegatingRoleActorFactory(final PluginAccessor pluginAccessor, final DbConnectionManager dbConnectionManager)
    {
        this.pluginAccessor = pluginAccessor;
        this.dbConnectionManager = dbConnectionManager;
    }

    @Override
    public ProjectRoleActor createRoleActor(final Long id, final Long projectRoleId, final Long projectId, final String type, final String parameter)
            throws RoleActorDoesNotExistException
    {
        final ProjectRoleActorModuleDescriptor roleActorModuleDescriptor = getRoleActorModuleDescriptor(type, getImplementationsMap());
        if (roleActorModuleDescriptor == null)
        {
            throw new IllegalArgumentException("Type " + type + " is not a registered RoleActor implementation");
        }

        //We are not wrapping this in SafePluginPointAccess because there is no easy way to propagate checked exception correctly.
        final RoleActorFactory roleActorFactory = roleActorModuleDescriptor.getModule();
        return roleActorFactory.createRoleActor(id, projectRoleId, projectId, type, parameter);
    }

    @Override
    public Set<RoleActor> optimizeRoleActorSet(final Set<RoleActor> roleActors)
    {
        final OptimizeRoleActorSetVisitor visitor = new OptimizeRoleActorSetVisitor(roleActors);
        SafePluginPointAccess.to().descriptors(getImplementations(), visitor);
        return visitor.getOptimizedActors();
    }

    @Override
    @Nonnull
    public Set<ProjectRoleActor> getAllRoleActorsForUser(@Nullable final ApplicationUser user)
    {
        final List<Set<ProjectRoleActor>> actorsFromPluginPoints =
                SafePluginPointAccess.to().descriptors(getImplementations(), new GetAllRoleActorsFunction(user));

        return ImmutableSet.copyOf(Iterables.concat(actorsFromPluginPoints));
    }

    private ProjectRoleActorModuleDescriptor getRoleActorModuleDescriptor(final String type, final Map<String, ProjectRoleActorModuleDescriptor> implementations)
    {
        return implementations == null ? null : implementations.get(type);
    }

    private Map<String, ProjectRoleActorModuleDescriptor> getImplementationsMap()
    {
        UtilTimerStack.push("DefaultRoleActorFactory.getImplementations");
        UtilTimerStack.push("DefaultRoleActorFactory.getImplementations-getEnabledModuleDescriptorByClass");
        final List<ProjectRoleActorModuleDescriptor> descriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(ProjectRoleActorModuleDescriptor.class);
        UtilTimerStack.pop("DefaultRoleActorFactory.getImplementations-getEnabledModuleDescriptorByClass");

        final Map<String, ProjectRoleActorModuleDescriptor> actorsByType = Maps.newHashMapWithExpectedSize(descriptors.size());

        for (final ProjectRoleActorModuleDescriptor projectRoleModuleDescriptor : descriptors)
        {
            actorsByType.put(projectRoleModuleDescriptor.getKey(), projectRoleModuleDescriptor);
        }
        UtilTimerStack.pop("DefaultRoleActorFactory.getImplementations");

        return actorsByType;
    }

    private Collection<ProjectRoleActorModuleDescriptor> getImplementations()
    {
        return getImplementationsMap().values();
    }

    /**
     * A visitor for optimizing RoleActor sets. Used to delegate the optimization to the plugin points.
     */
    private static class OptimizeRoleActorSetVisitor implements PluginPointVisitor<ProjectRoleActorModuleDescriptor, RoleActorFactory>
    {
        private Set<RoleActor> optimizedActors;

        public OptimizeRoleActorSetVisitor(final Set<RoleActor> optimizedActors)
        {
            this.optimizedActors = optimizedActors;
        }

        @Override
        public void visit(final ProjectRoleActorModuleDescriptor projectRoleActorModuleDescriptor, final RoleActorFactory roleActorFactory)
        {
            optimizedActors = roleActorFactory.optimizeRoleActorSet(optimizedActors);
        }

        public Set<RoleActor> getOptimizedActors()
        {
            return optimizedActors;
        }
    }

    /**
     * Function which obtains all ProjectRoleActors from given plugin point.
     * If a plugin point implements optimized version of the factory getAllRoleActorsForUser method is called.
     * If not the suitable data for actors is read from DB and then for each row createRoleActor method is called.
     */
    private class GetAllRoleActorsFunction implements PluginPointFunction<ProjectRoleActorModuleDescriptor, RoleActorFactory, Set<ProjectRoleActor>>
    {
        private final ApplicationUser user;

        public GetAllRoleActorsFunction(final ApplicationUser user)
        {
            this.user = user;
        }

        @Override
        public Set<ProjectRoleActor> onModule(final ProjectRoleActorModuleDescriptor projectRoleActorModuleDescriptor, final RoleActorFactory roleActorFactory)
        {
            if (roleActorFactory instanceof OptimizedRoleActorFactory)
            {
                return ((OptimizedRoleActorFactory) roleActorFactory).getAllRoleActorsForUser(user);
            }

            final String type = projectRoleActorModuleDescriptor.getType();

            log.warn("Using non optimized RoleActorFactory of type: " + type);

            final ImmutableSet.Builder<ProjectRoleActor> resultsBuilder = ImmutableSet.builder();
            dbConnectionManager.execute(new SqlCallback()
            {
                @Override
                public void run(final DbConnection dbConnection)
                {
                    final QProjectRoleActor pra = new QProjectRoleActor("pra");

                    final List<Tuple> roleActorTuples = dbConnection.newSqlQuery().
                        from(pra).
                        where(pra.roletype.eq(type)).
                        list(Projections.tuple(pra.id, pra.pid, pra.projectroleid, pra.roletype, pra.roletypeparameter));

                    for (Tuple t : roleActorTuples)
                    {
                        try
                        {
                            resultsBuilder.add(roleActorFactory.createRoleActor(t.get(pra.id), t.get(pra.projectroleid), t.get(pra.pid), t.get(pra.roletype), t.get(pra.roletypeparameter)));
                        }
                        catch (RoleActorDoesNotExistException e)
                        {
                            log.warn("Unable to create a project role actor. " + e.getMessage());
                        }
                    }
                }
            });

            return resultsBuilder.build();
        }
    }
}