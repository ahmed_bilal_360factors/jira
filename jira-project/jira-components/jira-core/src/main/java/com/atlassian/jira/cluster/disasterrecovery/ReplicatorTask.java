package com.atlassian.jira.cluster.disasterrecovery;

import java.io.File;

import com.atlassian.fugue.Option;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.config.util.SecondaryJiraHome;

import org.apache.commons.lang3.StringUtils;

/**
 * Base Methods for replication tasks
 *
 * @since v6.4
 */
public abstract class ReplicatorTask implements Runnable
{
    final File file;
    final JiraHome jiraHome;
    final SecondaryJiraHome secondaryJiraHome;

    ReplicatorTask(final File file, final JiraHome jiraHome, final SecondaryJiraHome secondaryJiraHome)
    {

        this.file = file;
        this.jiraHome = jiraHome;
        this.secondaryJiraHome = secondaryJiraHome;
    }

    boolean createDirectoriesIfNecessary(final String secondaryFilePath)
    {
        File file1 = new File(secondaryFilePath);
        File directory = file1.getParentFile();
        return directory.mkdirs() || directory.exists();
    }

    /**
     * We get the relative path for this file
     * removing the JIRA Home Path
     */
    Option<String> getRelativePathOf(final JiraHome jiraHome, final File file)
    {
        if(file.getAbsolutePath().startsWith(jiraHome.getHomePath()))
        {
            return Option.some(StringUtils.replaceOnce(file.getAbsolutePath(), jiraHome.getHomePath(), ""));
        }

        return Option.none();
    }
}
