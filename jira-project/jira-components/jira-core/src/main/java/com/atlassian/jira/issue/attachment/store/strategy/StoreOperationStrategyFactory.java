package com.atlassian.jira.issue.attachment.store.strategy;

import com.atlassian.jira.issue.attachment.StreamAttachmentStore;

/**
 * @since v6.4
 */
public class StoreOperationStrategyFactory
{

    public StoreOperationStrategy createConsistentDualOperationStrategy(final StreamAttachmentStore primaryStore,
            final StreamAttachmentStore secondaryStore)
    {
        return new DualStoreConsistentOperationStrategy(primaryStore, secondaryStore);
    }

    public StoreOperationStrategy createFailoverOperationStrategy(final StreamAttachmentStore primaryStore,
            final StreamAttachmentStore secondaryStore)
    {
        return new FailoverOnMissingOperationStrategy(primaryStore, secondaryStore);
    }
}
