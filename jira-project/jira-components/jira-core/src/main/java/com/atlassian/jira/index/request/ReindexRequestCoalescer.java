package com.atlassian.jira.index.request;

import java.util.List;

import javax.annotation.Nonnull;

/**
 * Combines reindex requests into a smaller number that do the same job.
 *
 * @since 6.4
 */
public interface ReindexRequestCoalescer
{
    /**
     * Attempts to combine a list of reindex requests into a fewer number.
     *
     * @param requests the requests to combine.
     *
     * @return a new list of possibly fewer reindex requests.
     */
    @Nonnull
    public List<ReindexRequest> coalesce(@Nonnull List<ReindexRequest> requests);
}
