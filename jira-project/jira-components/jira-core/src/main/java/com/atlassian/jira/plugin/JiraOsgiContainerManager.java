package com.atlassian.jira.plugin;

import java.io.File;

import javax.servlet.ServletContext;

import com.atlassian.jira.extension.Startable;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.osgi.container.PackageScannerConfiguration;
import com.atlassian.plugin.osgi.container.felix.FelixOsgiContainerManager;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import com.atlassian.plugin.servlet.ServletContextFactory;

/**
 * OSGi container manager that sets up the bundle directory from the servlet context
 */
public class JiraOsgiContainerManager extends FelixOsgiContainerManager
{

    /**
     * Path to load framework bundles from the webapp, see jira-components/jira-webapp/pom.xml.
     */
    private static final String FRAMEWORK_BUNDLES_LOCATION = "/WEB-INF/osgi-framework-bundles";

    public JiraOsgiContainerManager(
            final PluginPath pluginPath,
            final PackageScannerConfiguration packageScannerConfig,
            final HostComponentProvider provider,
            final PluginEventManager eventManager,
            final ServletContextFactory servletContextFactory)
    {
        super(
                populatedFrameworkBundles(servletContextFactory),
                pluginPath.getOsgiPersistentCache(),
                packageScannerConfig,
                provider,
                eventManager);
    }

    private static File populatedFrameworkBundles(final ServletContextFactory servletContextFactory)
    {
        final ServletContext servletContext = servletContextFactory.getServletContext();
        final String osgiJarPath = servletContext.getRealPath(FRAMEWORK_BUNDLES_LOCATION);
        // JiraWebappStartupCheck ensures that getRealPath does not return null for /

        return new File(osgiJarPath);
    }

}
