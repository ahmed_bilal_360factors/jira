package com.atlassian.jira;

import com.atlassian.configurable.XMLObjectConfigurationFactory;
import com.atlassian.core.ofbiz.CoreFactory;
import com.atlassian.jira.cluster.ClusterServicesManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.LocaleManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.ListenerManager;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.SearchRequestManager;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.issue.subscription.SubscriptionManager;
import com.atlassian.jira.mail.MailThreadManager;
import com.atlassian.jira.memoryinspector.MemoryInspector;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.notification.NotificationTypeManager;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.PermissionTypeManager;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.SecurityTypeManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.service.ServiceManager;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.upgrade.UpgradeManager;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.index.IndexLifecycleManager;
import com.atlassian.jira.web.action.admin.customfields.CustomFieldValidator;
import com.atlassian.jira.web.servlet.rpc.AxisServletProvider;
import com.atlassian.jira.web.util.JiraLocaleUtils;
import com.atlassian.jira.web.util.OutlookDateManager;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.mail.queue.MailQueue;
import com.atlassian.velocity.VelocityManager;

import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.picocontainer.PicoContainer;
import org.quartz.Scheduler;

import webwork.util.ValueStack;

/**
 * Provides static methods for obtaining 'Manager' classes, though which much of JIRA's functionality is exposed.
 *
 * @deprecated Use {@link ComponentAccessor} instead. Since v4.4.
 */
@SuppressWarnings ( { "JavaDoc" })
public class ManagerFactory
{
    private static final Logger log = LoggerFactory.getLogger(ManagerFactory.class);

    /**
     * This should *never* be called, except in tests, or if you are importing or seting up for the first time. The
     * reason this is called is to ensure that all the managers are reinitialised after the license has changed.
     * <p/>
     * Note: Make sure the scheduler is shutdown
     */
    public static synchronized void globalRefresh()
    {
        log.debug("ManagerFactory.globalRefresh");

        getIndexLifecycleManager().shutdown();

        // shutdown task manager
        final TaskManager taskManager = ComponentManager.getInstance().getTaskManager();
        taskManager.shutdownAndWait(0);

        ComponentAccessor.getComponent(ClusterServicesManager.class).stopServices();
        ComponentManager.getInstance().stop();
        //TODO [PICO] permgen-prev? - deregister events?
        ComponentManager.getInstance().dispose();

        // JDEV-29269 Cleanup resources kept by MultiThreadedHttpConnectionManager - the only
        // (civilized) way to get rid of the MultiThreadedHttpConnectionManager cleanup thread.
        MultiThreadedHttpConnectionManager.shutdownAll();

        // Look for possible memory leaks
        new MemoryInspector().inspectMemoryAfterJiraShutdown();

        // Rebuild all the registered objects
        ComponentManager.getInstance().initialise();

        // clear the method cache for JRA-16750
        ValueStack.clearMethods();

        // Need to bootstrap all the components that need initialising
        ComponentManager.getInstance().start();
        CoreFactory.globalRefresh();
        ComponentManager.getComponentInstanceOfType(AxisServletProvider.class).reset();

        getConstantsManager().refresh();
    }

    private ManagerFactory()
    {
    }

    @Deprecated
    /**
     * @deprecated Use {@link ComponentAccessor#getApplicationProperties()} instead. Since v5.0.
     */
    public static ApplicationProperties getApplicationProperties()
    {
        return getContainer().getComponent(ApplicationProperties.class);
    }

    @Deprecated
    /**
     * @deprecated Use {@link ComponentAccessor#getAttachmentManager()} instead. Since v5.0.
     */
    public static AttachmentManager getAttachmentManager()
    {
        return ComponentManager.getInstance().getAttachmentManager();
    }

    @Deprecated
    /**
     * @deprecated Use {@link ComponentAccessor#getConstantsManager()} instead. Since v5.0.
     */
    public static ConstantsManager getConstantsManager()
    {
        return getContainer().getComponent(ConstantsManager.class);
    }

    @Deprecated
    /**
     * @deprecated Use {@link ComponentAccessor#getCustomFieldManager()} instead. Since v5.0.
     */
    public static CustomFieldManager getCustomFieldManager()
    {
        return getContainer().getComponent(CustomFieldManager.class);
    }

    @Deprecated
    /**
     * @deprecated Use {@link ComponentAccessor#getFieldManager()} instead. Since v5.0.
     */
    public static FieldManager getFieldManager()
    {
        return getContainer().getComponent(FieldManager.class);
    }

    @Deprecated
    public static IndexLifecycleManager getIndexLifecycleManager()
    {
        return ComponentManager.getInstance().getIndexLifecycleManager();
    }

    @Deprecated
    /**
     * @deprecated Use {@link ComponentAccessor#getIssueIndexManager()} instead. Since v5.0.
     */
    public static IssueIndexManager getIndexManager()
    {
        return ComponentManager.getInstance().getIndexManager();
    }

    @Deprecated
    /**
     * @deprecated Use {@link ComponentAccessor#getIssueManager()} instead. Since v4.4.
     */
    public static IssueManager getIssueManager()
    {
        return getContainer().getComponent(IssueManager.class);
    }

    @Deprecated
    public static IssueSecuritySchemeManager getIssueSecuritySchemeManager()
    {
        return getContainer().getComponent(IssueSecuritySchemeManager.class);
    }

    @Deprecated
    public static SecurityTypeManager getIssueSecurityTypeManager()
    {
        return getContainer().getComponent(SecurityTypeManager.class);
    }

    @Deprecated
    /**
     * @deprecated Use {@link ComponentAccessor#getListenerManager()} instead. Since v5.0.
     */
    public static ListenerManager getListenerManager()
    {
        return getContainer().getComponent(ListenerManager.class);
    }

    @Deprecated
    /**
     * @deprecated Use {@link ComponentAccessor#getMailQueue()} instead. Since v5.0.
     */
    public static MailQueue getMailQueue()
    {
        return getContainer().getComponent(MailQueue.class);
    }

    @Deprecated
    /**
     * @deprecated Use {@link ComponentAccessor#getNotificationSchemeManager()} instead. Since v5.0.
     */
    public static NotificationSchemeManager getNotificationSchemeManager()
    {
        return getContainer().getComponent(NotificationSchemeManager.class);
    }

    @Deprecated
    public static NotificationTypeManager getNotificationTypeManager()
    {
        return getContainer().getComponent(NotificationTypeManager.class);
    }

    @Deprecated
    public static XMLObjectConfigurationFactory getObjectConfigurationFactory()
    {
        return getContainer().getComponent(XMLObjectConfigurationFactory.class);
    }

    @Deprecated
    /**
     * @deprecated Use {@link ComponentAccessor#getPermissionManager()} instead. Since v5.0.
     */
    public static PermissionManager getPermissionManager()
    {
        return ComponentManager.getInstance().getPermissionManager();
    }

    @Deprecated
    /**
     * @deprecated Use {@link ComponentAccessor#getPermissionSchemeManager()} instead. Since v5.0.
     */
    public static PermissionSchemeManager getPermissionSchemeManager()
    {
        return getContainer().getComponent(PermissionSchemeManager.class);
    }

    @Deprecated
    public static PermissionTypeManager getPermissionTypeManager()
    {
        return getContainer().getComponent(PermissionTypeManager.class);
    }

    /**
     * @deprecated Use {@link ComponentAccessor#getProjectManager()} instead. Since v5.0.
     */
    @Deprecated
    public static ProjectManager getProjectManager()
    {
        return getContainer().getComponent(ProjectManager.class);
    }

    /**
     * @return a Quartz scheduler
     * @deprecated Use {@link ComponentAccessor#getScheduler()} instead. Since v5.0.  Since v6.3, you should
     *          use the {@code SchedulerService} instead.
     */
    @Deprecated
    public static Scheduler getScheduler()
    {
        return getContainer().getComponent(Scheduler.class);
    }

    /**
     * @deprecated Use {@link ComponentAccessor#getIssueSecurityLevelManager()} instead. Since v5.0.
     */
    @Deprecated
    public static IssueSecurityLevelManager getIssueSecurityLevelManager()
    {
        return getContainer().getComponent(IssueSecurityLevelManager.class);
    }

    @Deprecated
    public static SearchRequestManager getSearchRequestManager()
    {
        return getContainer().getComponent(SearchRequestManager.class);
    }

    /**
     * @deprecated Use {@link ComponentAccessor#getServiceManager()} instead. Since v5.0.
     */
    @Deprecated
    public static ServiceManager getServiceManager()
    {
        return getContainer().getComponent(ServiceManager.class);
    }

    /**
     * @deprecated Use {@link ComponentAccessor#getSubscriptionManager()} instead. Since v5.0.
     */
    @Deprecated
    public static SubscriptionManager getSubscriptionManager()
    {
        return getContainer().getComponent(SubscriptionManager.class);
    }

    /**
     * @deprecated Use {@link com.atlassian.jira.ComponentManager#getUpgradeManager()} instead. Since v5.0.
     */
    @Deprecated
    public static UpgradeManager getUpgradeManager()
    {
        return getContainer().getComponent(UpgradeManager.class);
    }

    /**
     * @deprecated Use {@link ComponentAccessor#getGlobalPermissionManager()} instead. Since v5.0.
     */
    @Deprecated
    public static GlobalPermissionManager getGlobalPermissionManager()
    {
        return getContainer().getComponent(GlobalPermissionManager.class);
    }

    /**
     * @deprecated Use {@link ComponentAccessor#getVelocityManager()} instead. Since v5.0.
     */
    @Deprecated
    public static VelocityManager getVelocityManager()
    {
        return getContainer().getComponent(VelocityManager.class);
    }

    @Deprecated
    public static OutlookDateManager getOutlookDateManager()
    {
        return getContainer().getComponent(OutlookDateManager.class);
    }

    /**
     * @deprecated Use {@link ComponentAccessor#getWorkflowManager()} instead. Since v5.0.
     */
    @Deprecated
    public static WorkflowManager getWorkflowManager()
    {
        return ComponentManager.getInstance().getWorkflowManager();
    }

    /**
     * @deprecated Use {@link ComponentAccessor#getLocaleManager()} instead. Since v5.0.
     */
    @Deprecated
    public static LocaleManager getLocaleManager()
    {
        return getContainer().getComponent(LocaleManager.class);
    }

    @Deprecated
    public static JiraLocaleUtils getJiraLocaleUtils()
    {
        return getContainer().getComponent(JiraLocaleUtils.class);
    }

    /**
     * @return MailThreadManager
     *
     * @deprecated Use {@link ComponentAccessor#getMailThreadManager()} instead. Since v4.4.
     */
    @Deprecated
    public static MailThreadManager getMailThreadManager()
    {
        return getContainer().getComponent(MailThreadManager.class);
    }

    /**
     * @deprecated Use {@link ComponentAccessor#getWorkflowSchemeManager()} instead. Since v5.0.
     */
    @Deprecated
    public static WorkflowSchemeManager getWorkflowSchemeManager()
    {
        return getContainer().getComponent(WorkflowSchemeManager.class);
    }

    /**
     * @deprecated Use {@link ComponentAccessor#getOptionsManager()} instead. Since v5.0.
     */
    @Deprecated
    public static OptionsManager getOptionsManager()
    {
        return getContainer().getComponent(OptionsManager.class);
    }

    @Deprecated
    public static CustomFieldValidator getCustomFieldValidator()
    {
        return getContainer().getComponent(CustomFieldValidator.class);
    }

    /**
     * @deprecated Use {@link ComponentAccessor#getUserManager()} instead. Since v5.0.
     */
    @Deprecated
    public static UserManager getUserManager()
    {
        return getContainer().getComponent(UserManager.class);
    }

    /**
     * @deprecated Use {@link ComponentAccessor#getGroupManager()} instead. Since v5.0.
     */
    @Deprecated
    public static GroupManager getGroupManager()
    {
        return getContainer().getComponent(GroupManager.class);
    }

    /**
     * @deprecated Use {@link ComponentAccessor#getUserPropertyManager()} instead. Since v5.0.
     */
    @Deprecated
    public static UserPropertyManager getUserPropertyManager()
    {
        return getContainer().getComponent(UserPropertyManager.class);
    }

    private static PicoContainer getContainer()
    {
        return ComponentManager.getInstance().getContainer();
    }
}
