package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Condition to determine whether linking is enabled
 *
 * @since v4.1
 */
public class LinkingEnabledCondition extends AbstractWebCondition
{
    private static final Logger log = LoggerFactory.getLogger(LinkingEnabledCondition.class);
    private final ApplicationProperties applicationProperties;

    public LinkingEnabledCondition(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
    }

    public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper)
    {
        return applicationProperties.getOption(APKeys.JIRA_OPTION_ISSUELINKING);
    }
}
