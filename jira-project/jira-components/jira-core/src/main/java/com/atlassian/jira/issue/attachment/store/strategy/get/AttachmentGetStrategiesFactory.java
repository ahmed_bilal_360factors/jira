package com.atlassian.jira.issue.attachment.store.strategy.get;

import com.atlassian.jira.issue.attachment.FileSystemAttachmentStore;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;

public class AttachmentGetStrategiesFactory
{
    public DualAttachmentGetStrategy createDualGetAttachmentStrategy(
            final FileSystemAttachmentStore primaryStore,
            final StreamAttachmentStore secondaryStore)
    {
        return new DualAttachmentGetStrategy(primaryStore, secondaryStore);
    }

    public SingleStoreAttachmentGetStrategy createSingleStoreGetStrategy(final StreamAttachmentStore singleStore)
    {
        return new SingleStoreAttachmentGetStrategy(singleStore);
    }
}
