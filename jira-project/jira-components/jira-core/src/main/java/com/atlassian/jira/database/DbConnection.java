package com.atlassian.jira.database;

import java.sql.Connection;

import com.mysema.query.sql.RelationalPath;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.dml.SQLUpdateClause;

/**
 * A handle to a Database Connection obtained from JIRA's connection pool.
 *
 * @see com.atlassian.jira.database.DbConnectionManager
 * @since v6.4
 */
public interface DbConnection
{
    /**
     * Returns the JDBC connection wrapped by this object.
     * @return the JDBC connection wrapped by this object.
     */
    Connection getJdbcConnection();

    /**
     * Starts a SELECT statement on this connection.
     * <p>
     * Example usage:
     * <pre>
     *     QVersion v = new QVersion("v");
     *
     *     final List<VersionDTO> versions =
     *             dbConnection.newSqlQuery()
     *                     .from(v)
     *                     .where(v.project.eq(projectId))
     *                     .orderBy(v.sequence.asc())
     *                     .list(v);
     * </pre>
     *
     * @return the new Query builder.
     */
    SQLQuery newSqlQuery();

    /**
     * Starts an update statement on the given DB Table.
     * <p>
     * Example usage:
     * <pre>
     *        dbConnection.update(QIssueLink.ISSUE_LINK)
     *                .set(QIssueLink.ISSUE_LINK.linktype, newIssueLinkTypeId)
     *                .where(QIssueLink.ISSUE_LINK.id.eq(issueLink.getId()))
     *                .execute();
     * </pre>
     *
     * @param entity The DB entity you want to update eg {@link com.atlassian.jira.model.querydsl.QIssue#ISSUE}
     * @return a builder to create your update statement.
     */
    SQLUpdateClause update(RelationalPath<?> entity);

    void setAutoCommit(boolean autoCommit);

    void commit();

    void rollback();
}
