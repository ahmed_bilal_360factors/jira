package com.atlassian.jira.issue.attachment;

import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.core.util.FileSize;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.AttachmentError;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.AttachmentValidator;
import com.atlassian.jira.issue.AttachmentsBulkOperationResult;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ExceptionUtil;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.jira.web.action.issue.TemporaryWebAttachmentsMonitor;
import com.atlassian.jira.web.util.DefaultWebAttachmentManager;
import com.atlassian.jira.web.util.FileNameCharacterCheckerUtil;
import com.atlassian.jira.web.util.PreValidatedActionExecutor;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import static com.atlassian.jira.util.EitherUtils.splitEithers;
import static com.atlassian.jira.util.ErrorCollection.Reason.SERVER_ERROR;
import static com.atlassian.jira.util.ErrorCollection.Reason.VALIDATION_FAILED;
import static com.google.common.collect.Iterables.transform;

/**
 * Default implementation of TemporaryWebAttachmentManager.
 *
 * @since v6.4
 */
@ParametersAreNonnullByDefault
public class DefaultTemporaryWebAttachmentManager implements TemporaryWebAttachmentManager
{
    private static final Logger log = LoggerFactory.getLogger(DefaultWebAttachmentManager.class);

    private final FileNameCharacterCheckerUtil fileNameCharacterCheckerUtil;
    private final AttachmentManager attachmentManager;
    private final AttachmentValidator attachmentValidator;
    private final I18nHelper.BeanFactory beanFactory;
    private final TemporaryWebAttachmentsMonitorLocator temporaryWebAttachmentsMonitorLocator;
    private final ApplicationProperties applicationProperties;
    private final TemporaryWebAttachmentFactory temporaryWebAttachmentsFactory;

    public DefaultTemporaryWebAttachmentManager(
            final AttachmentManager attachmentManager,
            final AttachmentValidator attachmentValidator,
            final I18nHelper.BeanFactory beanFactory,
            final TemporaryWebAttachmentsMonitorLocator temporaryWebAttachmentsMonitorLocator,
            final ApplicationProperties applicationProperties,
            final TemporaryWebAttachmentFactory temporaryWebAttachmentsFactory)
    {
        this.attachmentManager = attachmentManager;
        this.attachmentValidator = attachmentValidator;
        this.beanFactory = beanFactory;
        this.temporaryWebAttachmentsMonitorLocator = temporaryWebAttachmentsMonitorLocator;
        this.applicationProperties = applicationProperties;
        this.temporaryWebAttachmentsFactory = temporaryWebAttachmentsFactory;
        this.fileNameCharacterCheckerUtil = new FileNameCharacterCheckerUtil();
    }

    @Override
    @ParametersAreNonnullByDefault
    public Either<AttachmentError, TemporaryWebAttachment> createTemporaryWebAttachment(final InputStream stream,
            final String fileName, final String contentType, final long size, final Either<Issue, Project> target,
            final String formToken, @Nullable final ApplicationUser user)
    {
        Assertions.notBlank("contentType", contentType);
        Assertions.notNull("stream", stream);
        Assertions.notNull("target", target);
        Assertions.nonNegative("size", size);

        return new PreValidatedActionExecutor<AttachmentError>()
                .withValidator(createCanCreateTemporaryAttachmentsValidator(fileName, target, user))
                .withValidator(createFileSizeValidator(fileName, size, user))
                .withValidator(createFileNameValidator(fileName, user))
                .executeFailOnFirstError(new Supplier<Either<AttachmentError, TemporaryWebAttachment>>()
                {
                    @Override
                    public Either<AttachmentError, TemporaryWebAttachment> get()
                    {
                        return createTemporaryWebAttachmentWithoutValidation(user, stream, size, fileName, contentType, formToken);
                    }
                });
    }

    @Override
    public Option<TemporaryWebAttachment> getTemporaryWebAttachment(final String temporaryAttachmentId)
    {
        return temporaryWebAttachmentsMonitorLocator.getOrCreate().getById(temporaryAttachmentId);
    }

    @Override
    public Collection<TemporaryWebAttachment> getTemporaryWebAttachmentsByFormToken(final String formToken)
    {
        return temporaryWebAttachmentsMonitorLocator.getOrCreate().getByFormToken(formToken);
    }

    @Override
    public void updateTemporaryWebAttachment(final String temporaryAttachmentId, final TemporaryWebAttachment updated)
    {
        final TemporaryWebAttachmentsMonitor monitor = temporaryWebAttachmentsMonitorLocator.getOrCreate();
        monitor.getById(temporaryAttachmentId).fold(
                new Supplier<Unit>()
                {
                    @Override
                    public Unit get()
                    {
                        throw new NoSuchElementException(
                                "Temporary Web Attachment with id " + temporaryAttachmentId + " does not exist.");
                    }
                },
                new Function<TemporaryWebAttachment, Unit>()
                {
                    @Override
                    public Unit apply(@Nullable final TemporaryWebAttachment original)
                    {
                        monitor.removeById(temporaryAttachmentId);
                        monitor.add(updated);
                        return Unit.VALUE;
                    }
                });
    }

    private Either<AttachmentError, TemporaryWebAttachment> createTemporaryWebAttachmentWithoutValidation(
            @Nullable final ApplicationUser user, final InputStream stream, final long size, final String fileName, final String contentType, final String formToken)
    {
        try
        {
            final TemporaryAttachmentId attachmentId = attachmentManager.createTemporaryAttachment(stream, size);
            return addToMonitorCleanUpOnFailure(
                    temporaryWebAttachmentsFactory.create(attachmentId, fileName, contentType, formToken, size), user);
        }
        catch (final Exception e)
        {
            log.debug("Exception occurred while attaching file.", e);
            return Either.left(new AttachmentError(
                    "Exception occurred while attaching file: " + e.toString(),
                    getI18n(user).getText("attachfile.error.io.error", fileName, e.getMessage()),
                    fileName,
                    Option.some(e),
                    SERVER_ERROR
            ));
        }
    }

    private Supplier<Option<AttachmentError>> createCanCreateTemporaryAttachmentsValidator(
            final String fileName, final Either<Issue, Project> target, @Nullable final ApplicationUser user)
    {
        return new Supplier<Option<AttachmentError>>()
        {
            @Override
            public Option<AttachmentError> get()
            {
                final ErrorCollection errorCollection = new SimpleErrorCollection();
                attachmentValidator.canCreateTemporaryAttachments(user, target, errorCollection);
                return getFirstError(fileName, errorCollection);
            }
        };
    }

    private Supplier<Option<AttachmentError>> createCanConvertTemporaryAttachmentsValidator(final Issue issue,
            @Nullable final ApplicationUser user)
    {
        return new Supplier<Option<AttachmentError>>()
        {
            @Override
            public Option<AttachmentError> get()
            {
                final ErrorCollection errorCollection = new SimpleErrorCollection();
                attachmentValidator.canCreateAttachments(user, issue, errorCollection);
                return getFirstError("", errorCollection);
            }
        };
    }

    private Supplier<Option<AttachmentError>> createFileNameValidator(final String fileName,
            @Nullable final ApplicationUser user)
    {
        return new Supplier<Option<AttachmentError>>()
        {
            @Override
            public Option<AttachmentError> get()
            {
                if (StringUtils.isBlank(fileName))
                {
                    return createAttachmentError(fileName, "Attachment file name is blank",
                            getI18n(user).getText("attachfile.error.no.name"), VALIDATION_FAILED);
                }

                final String invalidChar = fileNameCharacterCheckerUtil.assertFileNameDoesNotContainInvalidChars(fileName);
                if (invalidChar != null)
                {
                    return createAttachmentError(
                            fileName,
                            String.format("Attachment file name contains invalid character %s", invalidChar),
                            getI18n(user).getText("attachfile.error.invalidcharacter", invalidChar),
                            VALIDATION_FAILED
                    );
                }

                // all passed
                return Option.none();
            }
        };
    }

    private Supplier<Option<AttachmentError>> createFileSizeValidator(final String fileName, final long size,
            @Nullable final ApplicationUser user)
    {
        return new Supplier<Option<AttachmentError>>()
        {
            @Override
            public Option<AttachmentError> get()
            {
                final long maxAttachmentSize = getMaxAttachmentSize();
                if (size == 0)
                {
                    return createAttachmentError(
                            fileName,
                            "Bad attachment size: 0",
                            getI18n(user).getText("attachfile.error.io.bad.size.zero", fileName),
                            VALIDATION_FAILED
                    );
                }
                else if (size > maxAttachmentSize)
                {
                    return createAttachmentError(
                            fileName,
                            String.format("Attachment is too large. %s > %s", size, maxAttachmentSize),
                            getI18n(user).getText("attachfile.error.file.large", fileName, FileSize.format(maxAttachmentSize)),
                            VALIDATION_FAILED
                    );
                }
                else
                {
                    return Option.none();
                }
            }
        };
    }

    private static Option<AttachmentError> createAttachmentError(final String fileName, final String logMessage,
            final String localizedMessage, final ErrorCollection.Reason reason)
    {
        return Option.some(new AttachmentError(logMessage, localizedMessage, fileName, reason));
    }

    @Override
    public AttachmentsBulkOperationResult<ChangeItemBean> convertTemporaryAttachments(
            @Nullable final ApplicationUser user, final Issue issue,
            final List<String> temporaryAttachmentsIds)
    {
        return new PreValidatedActionExecutor<AttachmentError>()
                .withValidator(createCanConvertTemporaryAttachmentsValidator(issue, user))
                .executeFailOnFirstError(new Supplier<Either<AttachmentError, AttachmentsBulkOperationResult<ChangeItemBean>>>()
                {
                    @Override
                    public Either<AttachmentError, AttachmentsBulkOperationResult<ChangeItemBean>> get()
                    {
                        return Either.right(convertTemporaryAttachmentsWithoutValidation(user, issue, temporaryAttachmentsIds));
                    }
                })
                .right().on(new Function<AttachmentError, AttachmentsBulkOperationResult<ChangeItemBean>>()
                {
                    @Override
                    public AttachmentsBulkOperationResult<ChangeItemBean> apply(
                            @SuppressWarnings ("NullableProblems") final AttachmentError input)
                    {
                        return new AttachmentsBulkOperationResult<ChangeItemBean>(
                                ImmutableList.of(input), Collections.<ChangeItemBean>emptyList());
                    }
                });
    }

    private AttachmentsBulkOperationResult<ChangeItemBean> convertTemporaryAttachmentsWithoutValidation(
            @Nullable final ApplicationUser user, final Issue issue, final List<String> temporaryAttachmentsIds)
    {
        final TemporaryWebAttachmentsMonitor monitor = temporaryWebAttachmentsMonitorLocator.getOrCreate();
        return new AttachmentsBulkOperationResult<ChangeItemBean>(splitEithers(
                ImmutableList.copyOf(transform(temporaryAttachmentsIds,
                        new Function<String, Either<AttachmentError, ChangeItemBean>>()
                        {
                            @Override
                            public Either<AttachmentError, ChangeItemBean> apply(
                                    @SuppressWarnings ("NullableProblems") final String selectedAttachmentId)
                            {
                                return transformTemporaryAttachment(selectedAttachmentId, user, issue, monitor);
                            }
                        }
                ))
        ));
    }

    @Override
    public void clearTemporaryAttachmentsByFormToken(final String formToken)
    {
        temporaryWebAttachmentsMonitorLocator.get().map(new Function<TemporaryWebAttachmentsMonitor, Unit>()
        {
            @Override
            public Unit apply(@SuppressWarnings ("NullableProblems") final TemporaryWebAttachmentsMonitor monitor)
            {
                monitor.cleanByFormToken(formToken);
                return Unit.VALUE;
            }
        });
    }

    private Either<AttachmentError, ChangeItemBean> transformTemporaryAttachment(final String selectedAttachmentId,
            @Nullable final ApplicationUser user, final Issue issue, final TemporaryWebAttachmentsMonitor monitor)
    {
        return monitor.getById(selectedAttachmentId)
                .map(transformTemporaryAttachmentFunction(selectedAttachmentId, user, issue, monitor))
                .getOrElse(new Supplier<Either<AttachmentError, ChangeItemBean>>()
                {
                    @Override
                    public Either<AttachmentError, ChangeItemBean> get()
                    {
                        final String errorMessage = getI18n(user).getText("attachfile.error.temp.file.not.exists");
                        return attachmentError("Temporary attachment missing: " + selectedAttachmentId, errorMessage,
                                "#" + selectedAttachmentId, SERVER_ERROR);
                    }
                });
    }

    private Function<TemporaryWebAttachment, Either<AttachmentError, ChangeItemBean>> transformTemporaryAttachmentFunction(
            final String selectedAttachmentId, @Nullable final ApplicationUser user, final Issue issue,
            final TemporaryWebAttachmentsMonitor monitor)
    {
        return new Function<TemporaryWebAttachment, Either<AttachmentError, ChangeItemBean>>()
        {
            @Override
            public Either<AttachmentError, ChangeItemBean> apply(
                    @SuppressWarnings ("NullableProblems") final TemporaryWebAttachment tempAttachment)
            {
                final Either<AttachmentError, ChangeItemBean> conversionResult = attachmentManager.convertTemporaryAttachment(
                        ConvertTemporaryAttachmentParams.builder()
                                .setAuthor(user)
                                .setIssue(issue)
                                .setTemporaryAttachmentId(tempAttachment.getTemporaryAttachmentId())
                                .setFilename(tempAttachment.getFilename())
                                .setContentType(tempAttachment.getContentType())
                                .setFileSize(tempAttachment.getSize())
                                .setCreatedTime(tempAttachment.getCreated())
                                .build()
                );

                return conversionResult.map(new Function<ChangeItemBean, ChangeItemBean>()
                {
                    @Override
                    public ChangeItemBean apply(@SuppressWarnings ("NullableProblems") final ChangeItemBean changeItemBean)
                    {
                        monitor.removeById(selectedAttachmentId);
                        return changeItemBean;
                    }
                });
            }
        };
    }

    private <T> Either<AttachmentError, T> attachmentError(final String logMessage, final String localizedMessage,
            final String filename, final ErrorCollection.Reason reason)
    {
        log.warn(logMessage);
        return Either.left(new AttachmentError(logMessage, localizedMessage, filename, Option.<Exception>none(), reason));
    }

    private I18nHelper getI18n(@Nullable final ApplicationUser user)
    {
        return beanFactory.getInstance(user);
    }

    private Either<AttachmentError, TemporaryWebAttachment> addToMonitorCleanUpOnFailure(
            final TemporaryWebAttachment temporaryAttachment, @Nullable final ApplicationUser user)
    {
        final Either<AttachmentError, TemporaryWebAttachment> result = tryAddToMonitor(temporaryAttachment, user);
        if (result.isLeft())
        {
            attachmentManager.deleteTemporaryAttachment(temporaryAttachment.getTemporaryAttachmentId());
        }
        return result;
    }

    private Either<AttachmentError, TemporaryWebAttachment> tryAddToMonitor(
            final TemporaryWebAttachment temporaryAttachment, @Nullable final ApplicationUser user)
    {
        try
        {
            temporaryWebAttachmentsMonitorLocator.getOrCreate().add(temporaryAttachment);
            return Either.right(temporaryAttachment);
        }
        catch (final Exception e)
        {
            ExceptionUtil.logExceptionWithWarn(log, "Got exception while adding temporary attachment to attachment monitor.", e);
            return Either.left(new AttachmentError(
                    String.format("Got exception while adding temporary attachment '%s' to attachment monitor: %s",
                            temporaryAttachment.getFilename(), e.toString()),
                    getI18n(user).getText("attachfile.error.session.error", temporaryAttachment.getFilename()),
                    temporaryAttachment.getFilename(),
                    Option.some(e), SERVER_ERROR
            ));
        }
    }

    private long getMaxAttachmentSize()
    {
        return Long.parseLong(applicationProperties.getDefaultBackedString(APKeys.JIRA_ATTACHMENT_SIZE));
    }

    private static Option<AttachmentError> getFirstError(final String fileName, final ErrorCollection collection)
    {
        if (collection.hasAnyErrors())
        {
            final String message = getFirstErrorMessage(collection);
            return createAttachmentError(fileName, "An error occurred: " + message, message, VALIDATION_FAILED);
        }
        else
        {
            return Option.none();
        }
    }

    private static String getFirstErrorMessage(final ErrorCollection collection)
    {
        final Iterable<String> allErrors = Iterables.concat(
                collection.getErrorMessages(), mapValues(collection.getErrors()));

        final String firstErrorMessage = Iterables.getFirst(allErrors, null);
        return Preconditions.checkNotNull(firstErrorMessage, "Expected to find error message but cannot find one!");
    }

    private static Collection<String> mapValues(@Nullable final Map<String, String> map)
    {
        return map != null ? map.values() : Collections.<String>emptyList();
    }
}
