package com.atlassian.jira.model.querydsl;

import javax.annotation.Generated;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;

import org.ofbiz.core.entity.GenericValue;

/**
 * Data Transfer Object for the ColumnLayout entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 *
 * @see QColumnLayout
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class ColumnLayoutDTO
{
    private Long id;
    private String username;
    private Long searchrequest;

    public Long getId()
    {
        return id;
    }

    public String getUsername()
    {
        return username;
    }

    public Long getSearchrequest()
    {
        return searchrequest;
    }

    public ColumnLayoutDTO(Long id, String username, Long searchrequest)
    {
        this.id = id;
        this.username = username;
        this.searchrequest = searchrequest;
    }
    /**
    * Creates a GenericValue object from the values in this Data Transfer Object.
    * <p>
    *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
    * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
    * @return a GenericValue object constructed from the values in this Data Transfer Object.
    */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator)
    {
        return ofBizDelegator.makeValue("ColumnLayout", new FieldMap()
                        .add("id", id)
                        .add("username", username)
                        .add("searchrequest", searchrequest)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */

    public static ColumnLayoutDTO fromGenericValue(GenericValue gv)
    {
        return new ColumnLayoutDTO(
            gv.getLong("id"),
            gv.getString("username"),
            gv.getLong("searchrequest")
        );
    }
}

