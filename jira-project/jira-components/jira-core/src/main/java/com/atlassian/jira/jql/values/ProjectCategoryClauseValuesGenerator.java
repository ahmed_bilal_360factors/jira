package com.atlassian.jira.jql.values;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.comparator.LocaleSensitiveNamedComparator;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.web.bean.I18nBean;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import com.google.common.collect.Lists;

/**
 * Generates values for project categories.
 *
 * @since v4.0
 */
public class ProjectCategoryClauseValuesGenerator implements ClauseValuesGenerator
{
    private final ProjectManager projectManager;

    public ProjectCategoryClauseValuesGenerator(final ProjectManager projectManager)
    {
        this.projectManager = projectManager;
    }

    public Results getPossibleValues(final User searcher, final String jqlClauseName, final String valuePrefix, final int maxNumResults)
    {
        final List<ProjectCategory> projectCategories = new ArrayList<ProjectCategory>(projectManager.getAllProjectCategories());

        Collections.sort(projectCategories, new LocaleSensitiveNamedComparator(getLocale(searcher)));

        final List<Result> results = Lists.newArrayListWithCapacity(projectCategories.size());
        for (ProjectCategory projectCategory : projectCategories)
        {
            if (results.size() == maxNumResults)
            {
                break;
            }
            final String categoryName = projectCategory.getName();
            final String lowerCaseCategoryName = categoryName.toLowerCase();
            if (StringUtils.isBlank(valuePrefix) || lowerCaseCategoryName.startsWith(valuePrefix.toLowerCase()))
            {
                results.add(new Result(categoryName));
            }
        }

        return new Results(results);
    }

    ///CLOVER:OFF
    Locale getLocale(final User searcher)
    {
        return new I18nBean(searcher).getLocale();
    }
    ///CLOVER:ON

}
