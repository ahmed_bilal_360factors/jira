package com.atlassian.jira.tenancy;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.annotation.Nullable;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.timezone.TimeZoneInfo;
import com.atlassian.jira.timezone.TimeZoneService;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ImportUtils;
import com.atlassian.plugins.landlord.spi.LandlordRequestException;
import com.atlassian.plugins.landlord.spi.LandlordRequests;
import com.atlassian.tenancy.api.Tenant;
import com.atlassian.tenancy.api.event.TenantArrivedEvent;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is responsible for accepting Tenants - Currently in BTF mode we always have a tenant after setup -
 *
 * @since v6.4
 */
public class DefaultLandlordRequests implements LandlordRequests, TenantSetupRegistrar
{
    private final EventPublisher eventPublisher;
    private final JiraTenantAccessor jiraTenantAccessor;
    private final TenantPluginBridge tenantPluginBridge;
    private final TimeZoneService timeZoneService;
    private final Set<TenantInitialDataLoader> tenantInitialDataLoaders;
    private final JiraProperties jiraProperties;

    private final static Logger log = LoggerFactory.getLogger(DefaultLandlordRequests.class);

    public DefaultLandlordRequests(final EventPublisher eventPublisher, final JiraTenantAccessor jiraTenantAccessor,
            final TenantPluginBridge tenantPluginBridge, final TimeZoneService timeZoneService, final JiraProperties jiraProperties)
    {
        this.eventPublisher = eventPublisher;
        this.jiraTenantAccessor = jiraTenantAccessor;
        this.tenantPluginBridge = tenantPluginBridge;
        this.timeZoneService = timeZoneService;
        this.jiraProperties = jiraProperties;
        tenantInitialDataLoaders = Sets.newHashSet();
    }

    @Override
    public void acceptTenant(final String tenantId) throws LandlordRequestException
    {
        acceptTenant(tenantId, Collections.<String, String>emptyMap());
    }

    @Override
    public void acceptTenant(final String tenantId, final Map<String, String> tenantProperties)
            throws LandlordRequestException
    {
        final Tenant tenant = new JiraTenantImpl(tenantId);
        jiraTenantAccessor.addTenant(tenant);

        updateTimezoneIfProvided(tenantProperties);
        tenantPluginBridge.trigger();
        for (final TenantInitialDataLoader loader : tenantInitialDataLoaders)
        {
            loader.start(tenant);
        }
        eventPublisher.publish(new TenantArrivedEvent(tenant));
        log.debug("Tenanted - AcceptTenantCalled");
    }

    @Override
    public void removeTenant(final String tenantId) throws LandlordRequestException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public List<String> getTenants()
    {
        return Lists.newArrayList(Iterables.transform(jiraTenantAccessor.getAvailableTenants(), new Function<Tenant, String>()
        {
            @Override
            public String apply(@Nullable final Tenant tenant)
            {
                if (tenant instanceof JiraTenantImpl)
                {
                    return ((JiraTenantImpl) tenant).getId();
                }
                else
                {
                    throw new IllegalArgumentException("tenant should be an instance of JiraTenant");
                }
            }
        }));
    }

    @Override
    public void registerTenantInitialDataLoader(final TenantInitialDataLoader tenantInitialDataLoader)
    {
        synchronized(tenantInitialDataLoaders)
        {
            tenantInitialDataLoaders.add(tenantInitialDataLoader);
        }
        initialiseTenantIfAlreadyTenanted(tenantInitialDataLoader);
    }

    @Override
    public void unregisterTenantInitialDataLoader(final TenantInitialDataLoader tenantInitialDataLoader)
    {
        synchronized(tenantInitialDataLoaders)
        {
            tenantInitialDataLoaders.remove(tenantInitialDataLoader);
        }
    }

    @Override
    public void unregisterAll()
    {
        synchronized(tenantInitialDataLoaders)
        {
            tenantInitialDataLoaders.clear();
        }
    }

    private void initialiseTenantIfAlreadyTenanted(final TenantInitialDataLoader tenantInitialDataLoader)
    {
        for (final Tenant tenant: jiraTenantAccessor.getAvailableTenants())
        {
            tenantInitialDataLoader.start(tenant);
        }
    }

    private void updateTimezoneIfProvided(final Map<String, String> tenantProperties)
    {
        final String timezone = tenantProperties.get("timezone");
        if (null != timezone)
        {
            final boolean wasSubvertSecurityScheme = ImportUtils.isSubvertSecurityScheme();
            try
            {
                ImportUtils.setSubvertSecurityScheme(true);
                final ApplicationUser noUser = null;
                //noinspection ConstantConditions
                final JiraServiceContextImpl serviceContext = new JiraServiceContextImpl(noUser);
                timeZoneService.setDefaultTimeZone(timezone, serviceContext);
                TimeZoneInfo tzInfo = timeZoneService.getDefaultTimeZoneInfo(serviceContext);
                setJvmAndJodaDefaultTimezone(tzInfo);
            }
            finally
            {
                ImportUtils.setSubvertSecurityScheme(wasSubvertSecurityScheme);
            }
        }
    }

    private void setJvmAndJodaDefaultTimezone(TimeZoneInfo tzInfo)
    {
        jiraProperties.setProperty("user.timezone", tzInfo.getTimeZoneId());
        TimeZone.setDefault(tzInfo.toTimeZone());
        DateTimeZone.setDefault(DateTimeZone.forTimeZone(tzInfo.toTimeZone()));
    }
}
