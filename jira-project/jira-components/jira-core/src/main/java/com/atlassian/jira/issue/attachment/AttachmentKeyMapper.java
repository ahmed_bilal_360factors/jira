package com.atlassian.jira.issue.attachment;

public class AttachmentKeyMapper
{
    public AttachmentKey fromAttachment(final Attachment attachment)
    {
        return AttachmentKeys.from(attachment);
    }

}
