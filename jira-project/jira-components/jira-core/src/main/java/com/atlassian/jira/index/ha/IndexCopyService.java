package com.atlassian.jira.index.ha;

import javax.annotation.Nullable;

/**
 *
 * @since v6.1
 */
public interface IndexCopyService
{
    /**
     * Back up an index for replicating to another node
     * @param requestingNode Node requesting the index
     * @return the name of the backup file, or null if the backup did not occur
     */
    String backupIndex(String requestingNode);

    /**
     * Backup an index for replicating to another node
     * @param requestingNode Node requesting the index
     * @param contribution an additional contribution to be added to the backup
     * @return the name of the backup file, or null if the backup did not occur
     */
    String backupIndex(String requestingNode, @Nullable IndexSnapshotContribution contribution);

    /**
     * Restore an index.
     * @param filePath path of the zip file containing the index backup.
     */
    void restoreIndex(String filePath);
}
