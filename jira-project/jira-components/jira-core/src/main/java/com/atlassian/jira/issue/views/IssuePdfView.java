/**
 * 
 */
package com.atlassian.jira.issue.views;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.views.util.WordViewUtils;
import com.atlassian.jira.plugin.issueview.IssueViewRequestParams;
import com.atlassian.jira.plugin.searchrequestview.RequestHeaders;
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.Pipeline;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

/**
 * @author khurram.ahmed
 *
 */
public class IssuePdfView  {

	/**
	 * 
	 * @param issue
	 * @param requestHeaders
	 * @param issueViewRequestParams
	 */
	public void writeHeaders(final Issue issue, final RequestHeaders requestHeaders,
	            final IssueViewRequestParams issueViewRequestParams)
	    {
	        WordViewUtils.writeGenericNoCacheHeaders(requestHeaders);
	        requestHeaders.addHeader("content-disposition", "attachment;filename=\"" + issue.getKey() + ".PDF\";");
	    }
	
	/**
	 * 
	 * @param html
	 * @return
	 */
	public InputStream convertHTML(String html){
		try {
			ByteArrayOutputStream oByteArrayOutputStream = new 
					ByteArrayOutputStream(); 
			this.convertFromHTMLToPDF(html, oByteArrayOutputStream);
			
			ByteArrayInputStream oByteArrayInputStream = new ByteArrayInputStream 
					(oByteArrayOutputStream.toByteArray()); 
			return oByteArrayInputStream;
			
		} catch (Exception e) {
		}
		
		return null;
	}
	/**
	 * 
	 * @param html
	 * @param outputStream
	 */
	private void convertFromHTMLToPDF(String html,OutputStream  outputStream){
		Document document = null;
		try{
		
		HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
		htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
		CSSResolver cssResolver = XMLWorkerHelper.getInstance().getDefaultCssResolver(false);
		String cssFile = Thread.currentThread().getContextClassLoader().getResource("export.css").getPath();
		cssResolver.addCssFile(cssFile, true);
	    document = new Document();
	    document.setPageSize(PageSize.A4);
	    PdfWriter writer = PdfWriter.getInstance(document, outputStream);
	    document.open();
	    Pipeline<?> pipeline = new CssResolverPipeline(cssResolver,
				new HtmlPipeline(htmlContext, new PdfWriterPipeline(
						document, writer)));
	    XMLWorker worker = new XMLWorker(pipeline, true);
	    InputStream is = new ByteArrayInputStream(html.getBytes());
		XMLParser p = new XMLParser(worker);
		p.parse(is);
	   
	    
	} catch (Exception e) {
	 
	}
		finally{
			if (document!=null && document.isOpen()){ 
				document.close();
			}
		}
		
}
	

}
