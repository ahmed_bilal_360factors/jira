package com.atlassian.jira.upgrade.tasks;

import java.util.EnumSet;

import javax.annotation.Nullable;

import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;

/**
 * Reindexes JIRA due to a bug in the way {@link com.atlassian.jira.issue.customfields.impl.MultiSelectCFType} fields
 * were being indexed.
 *
 * @since v5.1
 */
public class UpgradeTask_Build751 extends AbstractReindexUpgradeTask
{

    @Override
    public String getBuildNumber()
    {
        return "751";
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception
    {
        getReindexRequestService().requestReindex(ReindexRequestType.IMMEDIATE, EnumSet.of(AffectedIndex.ISSUE), EnumSet.noneOf(SharedEntityType.class));
    }

    @Override
    public String getShortDescription()
    {
        return super.getShortDescription() + " due to changes to way select values are indexed.";
    }

    @Nullable
    @Override
    public String dependsUpon()
    {
        return "708";
    }

}
