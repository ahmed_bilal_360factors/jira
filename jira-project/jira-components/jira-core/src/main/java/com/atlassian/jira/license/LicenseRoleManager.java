package com.atlassian.jira.license;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.fugue.Option;

import java.util.Set;
import javax.annotation.Nonnull;

/**
 * Provides read and write capabilities regarding {@link LicenseRole}s. Unless otherwise stated,
 * the methods on this class only deal with <em>installed</em> {@code LicenseRole}s. An <em>installed</em>
 * {@code LicenseRole} has been defined by a plugin/product <strong>AND</strong> has an accompanying
 * license that grants that role.
 *
 * @since 6.3
 */
@ExperimentalApi
public interface LicenseRoleManager
{
    /**
     * Return the installed {@link LicenseRole} associated with the passed ID or
     * {@link Option#none} if no such role exists.
     *
     * An <em>installed</em> {@code LicenseRole} has been defined by a plugin/product
     * <strong>AND</strong> has an accompanying license that grants that role.
     *
     * @param licenseRoleId the ID to search for.
     * @return the {@link LicenseRole} associated with the passed ID.
     * @since 6.4
     */
    @Nonnull
    Option<LicenseRole> getLicenseRole(@Nonnull LicenseRoleId licenseRoleId);

    /**
     * Returns the {@link Set} of all currently installed {@link LicenseRole}s.
     *
     * An <em>installed</em> {@code LicenseRole} has been defined by a plugin/product
     * <strong>AND</strong> has an accompanying license that grants that role.
     *
     * @return the {@link Set} of installed {@link com.atlassian.jira.license.LicenseRole}s.
     * @since 6.4
     */
    @Nonnull
    Set<LicenseRole> getLicenseRoles();

    /**
     * Get the {@link Set} of group names that have been associated with one or more
     * installed license roles.
     *
     * An <em>installed</em> {@code LicenseRole} has been defined by a plugin/product
     * <strong>AND</strong> has an accompanying license that grants that role.
     *
     * @return groups associated with all installed license roles
     * @see com.atlassian.jira.bc.license.LicenseRoleAdminService
     * @since 6.4
     */
    @Nonnull
    Set<String> getGroupsForInstalledLicenseRoles();

    /**
     * Removes any/all associations of the given group from all license roles.
     *
     * @param groupName the name of the group to remove.
     * @since 6.4
     */
    void removeGroupFromLicenseRoles(@Nonnull String groupName);

    /**
     * Return {@code true} when the passed {@link com.atlassian.jira.license.LicenseRoleId} is installed.
     *
     * An <em>installed</em> {@code LicenseRole} has been defined by a plugin/product
     * <strong>AND</strong> has an accompanying license that grants that role.
     *
     * @param licenseRoleId the ID to check.
     * @return {@code true} when the passed {@link com.atlassian.jira.license.LicenseRoleId} is installed.
     */
    boolean isLicenseRoleInstalled(@Nonnull LicenseRoleId licenseRoleId);

    /**
     * Save the passed {@link com.atlassian.jira.license.LicenseRole} information to the database.
     * This method will only accept the passed role if:
     * <ol>
     *  <li>The role is <em>active</em>, that is, it is defined by a product/plugin and has an
     *  accompanying license that is current.</li>
     *  <li>The role only contains currently valid groups.</li>
     *  <li>The primary group is contained within the role.</li>
     * </ol>
     *
     * @param role the role to save.
     * @throws java.lang.IllegalArgumentException if passed role does not contain valid groups, if the primary group
     * is invalid or if the role is not installed.
     *
     * @return the role as persisted to the database.
     * @since 6.4
     */
    @Nonnull
    LicenseRole setLicenseRole(@Nonnull LicenseRole role);

    /**
     * Retrieve the number of active users for a specific license role (product). It will uniquely count all users who
     * are found in the groups associated with the product.
     *
     * @param roleId is the license role ID used to identify the product.
     * @return the number of active users for the product identified by roleId
     * @since 6.4
     */
    int getUserCount(@Nonnull LicenseRoleId roleId);
}
