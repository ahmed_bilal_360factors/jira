package com.atlassian.jira.issue.attachment.store.strategy.move;

import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.util.concurrent.Effect;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;

/**
 * Moves temporary attachment to real attachment in primary store and starts background task to create temporary and
 * convert it to real attachment in secondary store.
 *
 * @since v6.4
 */
@ParametersAreNonnullByDefault
public class BackgroundResendingMoveStrategy implements MoveTemporaryAttachmentStrategy
{
    private final StreamAttachmentStore primaryStore;
    private final StreamAttachmentStore secondaryStore;
    private final SendToStoreFunctionFactory sendToStoreFunctionFactory;

    public BackgroundResendingMoveStrategy(
            final StreamAttachmentStore primaryStore,
            final StreamAttachmentStore secondaryStore, final SendToStoreFunctionFactory sendToStoreFunctionFactory)
    {
        this.primaryStore = primaryStore;
        this.secondaryStore = secondaryStore;
        this.sendToStoreFunctionFactory = sendToStoreFunctionFactory;
    }

    @Override
    public Promise<Unit> moveTemporaryToAttachment(final TemporaryAttachmentId temporaryAttachmentId, final AttachmentKey destinationKey)
    {
        return primaryStore.moveTemporaryToAttachment(temporaryAttachmentId, destinationKey)
                .done(sendAttachmentToSecondaryStore(destinationKey));
    }

    private Effect<Unit> sendAttachmentToSecondaryStore(final AttachmentKey destinationKey)
    {
        return new Effect<Unit>()
        {
            @Override
            public void apply(final Unit unit)
            {
                primaryStore.getAttachmentData(destinationKey, sendToSecondaryStore(destinationKey));
            }
        };
    }

    private Function<AttachmentGetData, Unit> sendToSecondaryStore(final AttachmentKey destinationKey)
    {
        return sendToStoreFunctionFactory.createFunction(destinationKey, secondaryStore);
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final BackgroundResendingMoveStrategy that = (BackgroundResendingMoveStrategy) o;

        return primaryStore.equals(that.primaryStore) && secondaryStore.equals(that.secondaryStore);
    }

    @Override
    public int hashCode()
    {
        int result = primaryStore.hashCode();
        result = 31 * result + secondaryStore.hashCode();
        return result;
    }
}
