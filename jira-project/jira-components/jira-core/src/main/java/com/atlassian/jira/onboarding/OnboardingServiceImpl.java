package com.atlassian.jira.onboarding;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginAccessor;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

public class OnboardingServiceImpl implements OnboardingService
{
    private final OnboardingStore store;
    private final PluginAccessor pluginAccessor;
    private final GlobalPermissionManager permissionManager;
    private final FeatureManager featureManager;
    private final LoginService loginService;


    public OnboardingServiceImpl(final PluginAccessor pluginAccessor,
            final OnboardingStore store,
            final GlobalPermissionManager permissionManager,
            final FeatureManager featureManager,
            final LoginService loginService)
    {
        this.pluginAccessor = pluginAccessor;
        this.store = store;
        this.permissionManager = permissionManager;
        this.featureManager = featureManager;
        this.loginService = loginService;
    }

    @Override
    public boolean hasCompletedFirstUseFlow(@Nonnull final ApplicationUser user)
    {
        return store.getBoolean(user, OnboardingStore.FIRST_USE_FLOW_COMPLETED);
    }

    @Override
    public void completeFirstUseFlow(@Nonnull final ApplicationUser user)
    {
        store.setBoolean(user, OnboardingStore.FIRST_USE_FLOW_COMPLETED, true);
    }

    @Override
    public void setCurrentFirstUseFlowSequence(@Nonnull final ApplicationUser user, @Nonnull final String sequenceKey)
    {
        store.setString(user, OnboardingStore.FIRST_USE_FLOW_CURRENT_SEQUENCE, sequenceKey);
    }

    @Nullable
    @Override
    public String getCurrentFirstUseFlowSequence(@Nonnull final ApplicationUser user)
    {
        return store.getString(user, OnboardingStore.FIRST_USE_FLOW_CURRENT_SEQUENCE);
    }

    @Nullable
    @Override
    public FirstUseFlow getFirstUseFlow(@Nullable final ApplicationUser user)
    {
        // if onboarding is disabled via dark feature there is nothing else that needs to be tested
        if (featureManager.isEnabled(OnboardingService.DARK_FEATURE_DISABLE_ONBOARDING_FLAG))
        {
            return null;
        }

        if (user == null || hasResolvedFirstUseFlow(user))
        {
            return null;
        }

        FirstUseFlow firstUseFlow = evaluateFirstUseFlowChoice(user);
        // when firstUseFlow is evaluated as null it means the onboarding must not be shown to this user, so let's store
        // this status to avoid re-evaluating the onboarding flow again in any future requests from this user. It is also
        // useful when the loginCount is reset (e.g., when user directories are changed)
        if (firstUseFlow == null)
        {
            resolveFirstUseFlow(user);
        }

        return firstUseFlow;
    }

    /**
     * Checks whether the user should do an onboarding flow for the product. Returns the appropriate onboarding flow to
     * put the user through (if any).
     *
     * @return a {@link FirstUseFlow} if the user is eligible for one (and hasn't done one before), null otherwise.
     */
    @Nullable
    private FirstUseFlow evaluateFirstUseFlowChoice(@Nonnull final ApplicationUser user)
    {
        if (isOnDemandSysAdmin(user) || hasCompletedFirstUseFlow(user))
        {
            return null;
        }

        else if (hasStartedFirstUseFlow(user))
        {
            final String startedFlowKey = getStartedFirstUseFlowKey(user);
            if (startedFlowKey == null)
            {
                return null;
            }

            FirstUseFlowModuleDescriptor chosenFlowDescriptor = getFirstUseFlowModuleDescriptorByKey(startedFlowKey);
            if (chosenFlowDescriptor == null)
            {
                return null;
            }

            return chosenFlowDescriptor.getModule();
        }


        else if (featureManager.isEnabled(OnboardingService.DARK_FEATURE_DISABLE_USER_CHECKS_FLAG))
        {
            return startHighestWeightFlow(user);
        }

        else if (firstTimeLoggingIn(user))
        {
            return startHighestWeightFlow(user);
        }

        return null;
    }


    /**
     * Start the highest weight flow in the system
     *
     * @param user to find flow for
     * @return the flow that was marked as being started.
     */
    @Nullable
    private FirstUseFlow startHighestWeightFlow(@Nonnull final ApplicationUser user)
    {
        List<FirstUseFlowModuleDescriptor> flows = getOrderedFirstUseFlowDescriptors();
        for (FirstUseFlowModuleDescriptor flow : flows)
        {
            if (flow.getModule().isApplicable(user))
            {
                markFirstUseFlowStarted(user, flow);
                return flow.getModule();
            }
        }
        return null;
    }

    /**
     * Store that we have started a first use flow.
     *
     * @param user that the first use flow is being completed by
     * @param firstUseFlowModuleDescriptor of the first use flow to complete
     */
    private void markFirstUseFlowStarted(@Nonnull final ApplicationUser user, @Nonnull final FirstUseFlowModuleDescriptor firstUseFlowModuleDescriptor)
    {
        store.setString(user, OnboardingStore.STARTED_FLOW_KEY, firstUseFlowModuleDescriptor.getKey());
    }

    /**
     * Returns if a user has already started a first use flow.
     *
     * @param user to check against.
     * @return true if already started a first use flow
     */
    private boolean hasStartedFirstUseFlow(@Nonnull final ApplicationUser user)
    {
        return store.isSet(user, OnboardingStore.STARTED_FLOW_KEY);
    }

    /**
     * @param user to check against
     * @return whether the first use flow was resolved (and skipped) for this user before.
     */
    private boolean hasResolvedFirstUseFlow(@Nonnull final ApplicationUser user)
    {
        return store.getBoolean(user, OnboardingStore.FIRST_USE_FLOW_RESOLVED);
    }


    @Override
    @Nullable
    public String getStartedFirstUseFlowKey(@Nonnull final ApplicationUser user)
    {
        return store.getString(user, OnboardingStore.STARTED_FLOW_KEY);
    }

    /**
     * Checks whether a given user is a sysadmin.
     *
     * @param user logged in
     * @return whether the user is a sysadmin
     */
    private boolean isOnDemandSysAdmin(@Nonnull final ApplicationUser user)
    {
        return (
                featureManager.isOnDemand()
                        && permissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, user)
        );
    }

    /**
     * Tests whether this is the first time a user has logged in.
     *
     * @param user logging in
     * @return whether they have logged in before
     */
    private boolean firstTimeLoggingIn(@Nonnull final ApplicationUser user)
    {
        final Long loginCount = loginService.getLoginInfo(user.getUsername()).getLoginCount();
        /**
         * There are some circumstances under which we may not get the login count information for a user.
         * They include scenarios where JIRA is not the authenticator, such as SSO (Atlassian ID).
         */
        return loginCount != null && loginCount == 1;
    }

    /**
     * Mark the user as having the onboarding flow resolved
     *
     * @param user to mark onboarding as resolved
     */
    private void resolveFirstUseFlow(@Nonnull final ApplicationUser user)
    {
        store.setBoolean(user, OnboardingStore.FIRST_USE_FLOW_RESOLVED, true);
    }

    /**
     * @param key of the first flow module that we want to retrieve.
     * @return the firstUseFlowModuleDescriptor for the specific key.
     */
    @Nullable
    private FirstUseFlowModuleDescriptor getFirstUseFlowModuleDescriptorByKey(@Nonnull final String key)
    {
        Iterable<FirstUseFlowModuleDescriptor> unfilteredDescriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class);
        FirstUseFlowModuleDescriptor matchingDescriptor = null;

        Iterable<FirstUseFlowModuleDescriptor> filteredDescriptors = Iterables.filter(unfilteredDescriptors, new Predicate<FirstUseFlowModuleDescriptor>()
        {
            @Override
            public boolean apply(@Nullable final FirstUseFlowModuleDescriptor input)
            {
                return (null != input) && key.equals(input.getKey());
            }
        });

        Iterator<FirstUseFlowModuleDescriptor> iterator = filteredDescriptors.iterator();
        if (iterator.hasNext())
        {
            matchingDescriptor = iterator.next();
        }

        return matchingDescriptor;
    }

    /**
     * Finds and sorts through all the registered FirstUseFlows, returning a list where the most important flow is
     * first.
     *
     * @return a list of flows ordered by importance (first to last).
     */
    @Nonnull
    private List<FirstUseFlowModuleDescriptor> getOrderedFirstUseFlowDescriptors()
    {
        List<FirstUseFlowModuleDescriptor> descriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class);
        List<FirstUseFlowModuleDescriptor> x = Lists.newArrayList(descriptors);
        Collections.sort(x);
        return x;
    }
}
