package com.atlassian.jira.jql.query;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import com.atlassian.jira.bc.issue.search.QueryContextConverter;
import com.atlassian.jira.issue.search.optimizers.DeterminedProjectsExtractor;
import com.atlassian.jira.issue.search.optimizers.DeterminedProjectsInQueryVisitor;
import com.atlassian.jira.util.InjectableComponent;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.ChangedClause;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.ClauseVisitor;
import com.atlassian.query.clause.NotClause;
import com.atlassian.query.clause.OrClause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.WasClause;

import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;

import static com.atlassian.jira.jql.query.QueryVisitorUtil.makeQuery;

/**
 * @since v6.4
 */
public class ContextAwareQueryVisitor implements ClauseVisitor<QueryFactoryResult>
{
    private final QueryRegistry queryRegistry;
    private final WasClauseQueryFactory wasClauseQueryFactory;
    private final ChangedClauseQueryFactory changedClauseQueryFactory;
    private final DeterminedProjectsExtractor determinedProjectsExtractor;
    private final QueryCreationContext queryCreationContext;
    private final Clause parentClause;

    ContextAwareQueryVisitor(final QueryRegistry queryRegistry, final WasClauseQueryFactory wasClauseQueryFactory,
            final ChangedClauseQueryFactory changedClauseQueryFactory, final QueryCreationContext queryCreationContext,
            final Clause parentClause)
    {
        this(queryRegistry, wasClauseQueryFactory, changedClauseQueryFactory, new DeterminedProjectsExtractor(), queryCreationContext, parentClause);
    }

    ContextAwareQueryVisitor(final QueryRegistry queryRegistry, final WasClauseQueryFactory wasClauseQueryFactory,
            final ChangedClauseQueryFactory changedClauseQueryFactory, final DeterminedProjectsExtractor determinedProjectsExtractor,
            final QueryCreationContext queryCreationContext, final Clause parentClause)
    {
        this.queryRegistry = queryRegistry;
        this.wasClauseQueryFactory = wasClauseQueryFactory;
        this.changedClauseQueryFactory = changedClauseQueryFactory;
        this.determinedProjectsExtractor = determinedProjectsExtractor;
        this.queryCreationContext = queryCreationContext;
        this.parentClause = parentClause;
    }

    public QueryFactoryResult visit(final AndClause andClause)
    {
        assertClauseNotTooComplex(andClause);

        final BooleanQuery booleanQuery = new BooleanQuery();
        for (Clause clause : andClause.getClauses())
        {
            final QueryFactoryResult queryFactoryResult = clause.accept(createContextAwareQueryVisitor(andClause));
            if (queryFactoryResult.mustNotOccur())
            {
                booleanQuery.add(queryFactoryResult.getLuceneQuery(), BooleanClause.Occur.MUST_NOT);
            }
            else
            {
                booleanQuery.add(queryFactoryResult.getLuceneQuery(), BooleanClause.Occur.MUST);
            }
        }

        return new QueryFactoryResult(booleanQuery);
    }

    public QueryFactoryResult visit(final OrClause orClause)
    {
        assertClauseNotTooComplex(orClause);

        final BooleanQuery booleanQuery = new BooleanQuery();
        for (Clause clause : orClause.getClauses())
        {
            final QueryFactoryResult queryFactoryResult = clause.accept(createContextAwareQueryVisitor(orClause));
            booleanQuery.add(makeQuery(queryFactoryResult), BooleanClause.Occur.SHOULD);
        }

        return new QueryFactoryResult(booleanQuery);
    }

    public QueryFactoryResult visit(final NotClause notClause)
    {
        throw new IllegalStateException("We have removed all the NOT clauses from the query, this should never occur.");
    }

    public QueryFactoryResult visit(final TerminalClause terminalClause)
    {
        final Collection<ClauseQueryFactory> clauseQueryFactories = this.queryRegistry.getClauseQueryFactory(queryCreationContext, terminalClause);

        if (clauseQueryFactories.isEmpty())
        {
            // This time we really mean it! When we are asked to run a query for things that no longer exist we will
            // run the query the best we can, adding false to the tree for bits that we can not resolve.
            return QueryFactoryResult.createFalseResult();
        }

        final Set<String> determinedProjects = determinedProjectsExtractor.extractDeterminedProjectsFromClause(parentClause);
        final QueryCreationContext contextWithClause = new QueryCreationContextImpl(queryCreationContext, determinedProjects);

        try
        {
            if (clauseQueryFactories.size() == 1)
            {
                final ClauseQueryFactory factory = clauseQueryFactories.iterator().next();
                return factory.getQuery(contextWithClause, terminalClause);
            }
            else
            {
                final BooleanQuery query = new BooleanQuery();
                for (ClauseQueryFactory factory : clauseQueryFactories)
                {
                    query.add(makeQuery(factory.getQuery(contextWithClause, terminalClause)), BooleanClause.Occur.SHOULD);
                }
                return new QueryFactoryResult(query);
            }
        }
        catch (BooleanQuery.TooManyClauses tooManyClauses)
        {
            throw new JqlTooComplex(terminalClause);
        }
        // This could happen for Factories that use their own QueryVisitors, e.g. SavedFilterCQF
        catch (JqlTooComplex jqlTooComplex)
        {
            throw new JqlTooComplex(terminalClause);
        }
    }

    @Override
    public QueryFactoryResult visit(final WasClause clause)
    {
        return wasClauseQueryFactory.create(queryCreationContext.getUser(), clause);
    }

    @Override
    public QueryFactoryResult visit(final ChangedClause clause)
    {
        return changedClauseQueryFactory.create(queryCreationContext.getUser(), clause);
    }

    private ContextAwareQueryVisitor createContextAwareQueryVisitor(final Clause clause)
    {
        return new ContextAwareQueryVisitor(queryRegistry, wasClauseQueryFactory, changedClauseQueryFactory, queryCreationContext, clause);
    }

    private static void assertClauseNotTooComplex(final Clause clause)
    {
        if (clause.getClauses().size() > BooleanQuery.getMaxClauseCount())
        {
            throw new JqlTooComplex(clause);
        }
    }

    /**
     * This exception is thrown when the JQL Query is to complex to be generated into an Lucene Query.
     * It is caught in {@link LuceneQueryBuilder} and converted into a {@link com.atlassian.jira.issue.search.ClauseTooComplexSearchException}.
     */
    static class JqlTooComplex extends RuntimeException
    {
        private final Clause clause;

        JqlTooComplex(final Clause clause)
        {
            this.clause = clause;
        }

        public Clause getClause()
        {
            return clause;
        }
    }

    @InjectableComponent
    public static class ContextAwareQueryVisitorFactory
    {
        private final QueryRegistry queryRegistry;
        private final WasClauseQueryFactory wasClauseQueryFactory;
        private final ChangedClauseQueryFactory changedClauseQueryFactory;

        public ContextAwareQueryVisitorFactory(final QueryRegistry queryRegistry,
            final WasClauseQueryFactory wasClauseQueryFactory, final ChangedClauseQueryFactory changedClauseQueryFactory)
        {
            this.queryRegistry = Assertions.notNull("queryRegistry", queryRegistry);
            this.wasClauseQueryFactory = wasClauseQueryFactory;
            this.changedClauseQueryFactory = changedClauseQueryFactory;
        }

        public ContextAwareQueryVisitor createVisitor(final QueryCreationContext queryCreationContext, final Clause parentClause)
        {
            return new ContextAwareQueryVisitor(queryRegistry, wasClauseQueryFactory, changedClauseQueryFactory, queryCreationContext, parentClause);
        }

        public ContextAwareQueryVisitor createVisitorWithCustomQueryRegistry
                (final QueryCreationContext queryCreationContext, final Clause parentClause, final QueryRegistry customQueryRegistry)
        {
            return new ContextAwareQueryVisitor(customQueryRegistry, wasClauseQueryFactory, changedClauseQueryFactory, queryCreationContext, parentClause);
        }
    }
}
