package com.atlassian.jira.upgrade.tasks;

import java.util.EnumSet;
import java.util.List;

import javax.annotation.Nullable;

import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.impl.CascadingSelectCFType;
import com.atlassian.jira.issue.fields.CustomField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Requires a JIRA re-index to add lucene indexes to support Cascading Select fields.
 *
 * @since v6.1
 */
public class UpgradeTask_Build6142 extends AbstractReindexUpgradeTask
{
    private final Logger log = LoggerFactory.getLogger(getClass());
    private final CustomFieldManager customFieldManager;

    public UpgradeTask_Build6142(final CustomFieldManager customFieldManager)
    {
        super();
        this.customFieldManager = customFieldManager;
    }
    @Override
    public String getBuildNumber()
    {
        return "6142";
    }

    @Override
    public String getShortDescription()
    {
        return "Run a reindex if the instance has Cascading Select fields since their values will now be added to the Lucene index.";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception
    {
        // Only perform a reindex if the instance has cascading fields
        List<CustomField> customFields = customFieldManager.getCustomFieldObjects();
        for (CustomField customField : customFields)
        {
            if (customField.getCustomFieldType() instanceof CascadingSelectCFType)
            {
                log.info("A Reindex will be performed because at least 1 Cascade Select Custom Field is present");
                getReindexRequestService().requestReindex(ReindexRequestType.DELAYED, EnumSet.of(AffectedIndex.ISSUE), EnumSet.noneOf(SharedEntityType.class));
            }
        }

        log.info("No reindex will be performed.");
    }

    @Nullable
    @Override
    public String dependsUpon()
    {
        return "6141";
    }
}
