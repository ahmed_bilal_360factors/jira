package com.atlassian.jira.service.util;

import java.text.ParseException;
import java.util.Date;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.FilterCronValidationErrorMappingUtil;
import com.atlassian.jira.util.SimpleErrorCollection;

import org.quartz.CronTrigger;

/**
 * This is a temporary hack/copy to use until we get a proper one.
 *
 * @since v6.4
 */
public class CronValidator
{
    private final JiraAuthenticationContext authenticationContext;

    public CronValidator(final JiraAuthenticationContext authenticationContext)
    {
        this.authenticationContext = authenticationContext;
    }

    /**
     * Validate a Cron expresion.
     *
     * @param context jira service context
     * @param expr    the Cron Expression to validate and turn into trigger
     * @return ErrorCollection
     */
    public void validateCron(JiraServiceContext context, String expr, String field)
    {
        CronTrigger trigger = null;
        ErrorCollection errorCollection = context.getErrorCollection();
        try
        {

            trigger = new CronTrigger("temp", "temp", expr);
            //Test the trigger by calculating next fire time.  This will catch some extra errors
            Date nextFireTime = trigger.getFireTimeAfter(null);
            if (nextFireTime == null)
            {
                String str = authenticationContext.getI18nBean().getText("filter.subsription.cron.errormessage.filter.never.run", expr);
                errorCollection.addError(field, str);
            }
        }
        catch (ParseException e) // Generally known validations problems
        {
            final FilterCronValidationErrorMappingUtil errorMapper = new FilterCronValidationErrorMappingUtil(authenticationContext);
            errorMapper.mapError(context, e, field);
        }
        catch (IllegalArgumentException e) // Null expression
        {
            context.getErrorCollection().addError(field, authenticationContext.getI18nHelper().getText("filter.subsription.cron.errormessage.mode.error"));
        }
        catch (Exception e) // Unknown validation problems.
        {
            context.getErrorCollection().addError(field, authenticationContext.getI18nHelper().getText("filter.subsription.cron.errormessage.general.error", expr));
        }
    }

}
