package com.atlassian.jira.web.action.setup;

import com.atlassian.jira.setup.AsynchronousJiraSetupFactory;
import com.atlassian.jira.setup.InstantSetupStrategy;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.SneakyAutoLoginUtil;
import com.atlassian.jira.util.FileFactory;
import com.atlassian.jira.util.http.JiraUrl;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.web.action.ActionViewData;
import webwork.action.ServletActionContext;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

public class SetupFinishing extends AbstractSetupAction
{

    private String email;
    private String bundleLicense;
    private String jiraLicense;
    private String password;

    private final SetupSharedVariables sharedVariables;
    private final SetupProductBundleHelper productBundleHelper;

    public SetupFinishing(final FileFactory fileFactory,
            final JiraProperties jiraProperties,
            final SetupSharedVariables sharedVariables)
    {
        super(fileFactory, jiraProperties);
        this.sharedVariables = sharedVariables;
        productBundleHelper = new SetupProductBundleHelper(sharedVariables);
    }

    public String doDefault() throws Exception
    {
        if (setupAlready())
        {
            return SETUP_ALREADY;
        }

        return INPUT;
    }

    private String getRedirectPathAfterSetup()
    {
        return getRedirectPathAfterSetup(null);
    }

    private String getRedirectPathAfterSetup(ApplicationUser user)
    {
        if (!setupAlready())
        {
            return null;
        }

        return productBundleHelper.getSetupCompleteRedirectUrl(user != null ? user : getLoggedInApplicationUser());
    }

    public String doExecute()
    {
        if (setupAlready())
        {
            return getRedirect(getRedirectPathAfterSetup());
        }

        sharedVariables.setUserCredentials(email, password);
        sharedVariables.setBundleLicenseKey(bundleLicense);
        sharedVariables.setJiraLicenseKey(jiraLicense);
        sharedVariables.setLocale(getLocale().toString());

        InstantSetupStrategy.setupJiraBaseUrl(JiraUrl.constructBaseUrl(getHttpRequest()));

        return getRedirect("SetupFinishing!default.jspa");
    }

    public String doTriggerSetup() throws JSONException, IOException
    {
        if (setupAlready())
        {
            throw new RuntimeException("Can not be done after setup finished");
        }

        final SetupProductBundleHelper productBundleHelper = new SetupProductBundleHelper(sharedVariables);

        AsynchronousJiraSetupFactory.getInstance().setupJIRA(getSetupSessionId(),
                InstantSetupStrategy.SetupParameters.builder()
                        .setJiraLicenseKey(sharedVariables.getJiraLicenseKey())
                        .setBaseUrl(JiraUrl.constructBaseUrl(getHttpRequest()))
                        .setLocalBaseUrl(sharedVariables.getBaseUrl())
                        .setServerId(getServerId())
                        .setCredentials(sharedVariables.getUserCredentials())
                        .setLocale(sharedVariables.getLocale())
                        .setIsBundleSelected(!productBundleHelper.isNoBundleSelected())
                        .setBundleLicenseKey(sharedVariables.getBundleLicenseKey())
                        .setBundleLicenseUrl(productBundleHelper.getLicenseUrl())
                        .build());

        final JSONObject json = new JSONObject();
        json.put("result", "OK");
        writeJSONToHttpResponse(json);
        return NONE;
    }

    public String doSetupFinished() throws IOException, JSONException
    {
        final JSONObject json = new JSONObject();
        if (setupAlready() && AsynchronousJiraSetupFactory.getInstance().isSetupFinished(getSetupSessionId()))
        {
            final ApplicationUser user = SneakyAutoLoginUtil.logUserInByName(sharedVariables.getUserCredentials().get("email"), getHttpRequest());
            json.put("redirectUrl", getApplicationProperties().getString(APKeys.JIRA_BASEURL) + getRedirectPathAfterSetup(user));
            final JiraLicenseService jiraLicenseService = ComponentAccessor.getComponent(JiraLicenseService.class);
            json.put("SEN", jiraLicenseService.getLicense().getSupportEntitlementNumber());
        }
        else
        {
            throw new RuntimeException("Setup is not yet finished");
        }
        writeJSONToHttpResponse(json);
        return NONE;
    }

    private void writeJSONToHttpResponse(final JSONObject json) throws IOException
    {
        final HttpServletResponse response = getHttpResponse();
        response.setContentType("application/json");
        final String result = json.toString();
        response.getWriter().write(result);
        response.getWriter().flush();
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public void setBundleLicense(String bundleLicense)
    {
        this.bundleLicense = bundleLicense;
    }

    public void setJiraLicense(String jiraLicense)
    {
        this.jiraLicense = jiraLicense;
    }

    @ActionViewData
    public String getSelectedBundle()
    {
        return sharedVariables.getSelectedBundle();
    }
}

