package com.atlassian.jira.upgrade.tasks;

import javax.annotation.Nullable;

import java.util.EnumSet;

import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;

import static java.lang.String.format;

/**
 * Triggers a reindex necessary to rebuild the exact-text field document fields so stop words are not ignored
 * by exact phrase searches.
 *
 * @since v6.3
 */
public class UpgradeTask_Build6319 extends AbstractReindexUpgradeTask
{
    @Override
    public String getBuildNumber()
    {
        return "6319";
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception
    {
        getReindexRequestService().requestReindex(ReindexRequestType.DELAYED, EnumSet.of(AffectedIndex.ISSUE, AffectedIndex.COMMENT), EnumSet.noneOf(SharedEntityType.class));

    }

    @Override
    public String getShortDescription()
    {
        return format
                (
                        "%s Necessary so stop words are not ignored in exact phrase searches.",
                        super.getShortDescription()
                );
    }
}
