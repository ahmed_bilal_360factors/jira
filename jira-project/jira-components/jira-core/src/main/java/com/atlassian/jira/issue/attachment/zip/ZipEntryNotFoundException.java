package com.atlassian.jira.issue.attachment.zip;

public class ZipEntryNotFoundException extends RuntimeException
{
    public ZipEntryNotFoundException(final String message)
    {
        super(message);
    }
}
