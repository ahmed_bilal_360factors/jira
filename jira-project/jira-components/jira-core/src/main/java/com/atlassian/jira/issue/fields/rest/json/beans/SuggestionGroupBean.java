package com.atlassian.jira.issue.fields.rest.json.beans;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Collection;

/**
 * Represents group of possible values for single select field.
 *
 * @since 6.4
 */
@JsonIgnoreProperties (ignoreUnknown = true)
public class SuggestionGroupBean
{
    @JsonProperty
    private String label;

    @JsonProperty
    private Collection<SuggestionBean> items;

    public String getLabel()
    {
        return label;
    }

    public Collection<SuggestionBean> getItems()
    {
        return items;
    }

    public SuggestionGroupBean(final String label, final Collection<SuggestionBean> items)
    {
        this.label = label;
        this.items = items;
    }
}
