package com.atlassian.jira.web.action.issue.util;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.AttachmentError;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.AttachmentsBulkOperationResult;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.TemporaryAttachmentsMonitorLocator;
import com.atlassian.jira.issue.attachment.TemporaryAttachment;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachment;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachmentManager;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.jira.web.action.issue.TemporaryAttachmentsMonitor;
import com.atlassian.jira.web.bean.I18nBean;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.jira.util.ErrorCollection.Reason.SERVER_ERROR;
import static com.google.common.collect.Iterables.all;
import static com.google.common.collect.Iterables.transform;

/**
 * This class gives ability to handle temporary attachment conversion with backward compatible way. It is meant to be
 * used in places where we cannot introduce breaking changes. Hopefully it can go to void really quick and we can
 * re-introduce peace into JIRA code base...
 * <p/>
 * Be warned: this class pretends to be smart, but in fact it keeps the fingers crossed on any action it performs.
 *
 * @since v6.4
 */
public class BackwardCompatibleTemporaryAttachmentUtil
{
    private static final Logger log = LoggerFactory.getLogger(BackwardCompatibleTemporaryAttachmentUtil.class);

    @SuppressWarnings ("deprecation")
    private final TemporaryAttachmentsMonitorLocator temporaryAttachmentsMonitorLocator;
    private final TemporaryWebAttachmentManager temporaryWebAttachmentManager;
    private final AttachmentManager attachmentManager;
    private final I18nBean.BeanFactory i18nFactory;

    public BackwardCompatibleTemporaryAttachmentUtil(
            @SuppressWarnings ("deprecation") final TemporaryAttachmentsMonitorLocator temporaryAttachmentsMonitorLocator,
            final TemporaryWebAttachmentManager temporaryWebAttachmentManager, final AttachmentManager attachmentManager,
            final I18nHelper.BeanFactory i18nFactory)
    {
        this.temporaryAttachmentsMonitorLocator = temporaryAttachmentsMonitorLocator;
        this.temporaryWebAttachmentManager = temporaryWebAttachmentManager;
        this.attachmentManager = attachmentManager;
        this.i18nFactory = i18nFactory;
    }

    public boolean allAttachmentsExists(final List<String> temporaryAttachmentEntryIds)
    {
        return allAttachmentExists(temporaryAttachmentEntryIds) || allAttachmentExistsInOldMonitor(temporaryAttachmentEntryIds);
    }

    /**
     * To support old temporary attachments we will use TemporaryAttachmentsMonitor
     */
    private boolean allAttachmentExistsInOldMonitor(final List<String> temporaryAttachmentEntryIds)
    {
        //noinspection deprecation
        return Option.option(temporaryAttachmentsMonitorLocator.get(false)).flatMap(new Function<TemporaryAttachmentsMonitor, Option<Boolean>>()
        {
            @Override
            public Option<Boolean> apply(@Nullable final TemporaryAttachmentsMonitor monitor)
            {
                return convertToLongIds(temporaryAttachmentEntryIds).map(new Function<List<Long>, Boolean>()
                {
                    @Override
                    public Boolean apply(@Nullable final List<Long> longIds)
                    {
                        return allAttachmentExistsInOldMonitor(longIds, monitor);
                    }
                });
            }
        }).getOrElse(false);
    }

    private boolean allAttachmentExistsInOldMonitor(final List<Long> temporaryAttachmentEntryIds,
            @SuppressWarnings ("deprecation") final TemporaryAttachmentsMonitor monitor)
    {
        return all(lazyGetAllTemporaryAttachmentsFromOldMonitor(temporaryAttachmentEntryIds, monitor), new Predicate<TemporaryAttachment>()
        {
            @Override
            public boolean apply(@Nullable final TemporaryAttachment input)
            {
                return input != null && input.getFile() != null && input.getFile().exists();
            }
        });
    }

    private boolean allAttachmentExists(final List<String> temporaryAttachmentEntryIds)
    {
        return all(temporaryAttachmentEntryIds, new Predicate<String>()
        {
            @Override
            public boolean apply(final String temporaryAttachmentEntryId)
            {
                return temporaryWebAttachmentManager.getTemporaryWebAttachment(temporaryAttachmentEntryId).isDefined();
            }
        });
    }

    private Iterable<TemporaryAttachment> lazyGetAllTemporaryAttachmentsFromOldMonitor(
            final List<Long> temporaryAttachmentEntryIds,
            @SuppressWarnings ("deprecation") final TemporaryAttachmentsMonitor monitor)
    {
        return transform(temporaryAttachmentEntryIds, new Function<Long, TemporaryAttachment>()
        {
            @Override
            public TemporaryAttachment apply(final Long temporaryAttachmentEntryId)
            {
                return monitor.getById(temporaryAttachmentEntryId);
            }
        });
    }

    private Option<List<Long>> convertToLongIds(final Collection<String> stringIds)
    {
        try
        {
            // Remember to not be lazy here :)
            return Option.some((List<Long>) ImmutableList.copyOf(transform(stringIds, new Function<String, Long>()
            {
                @Override
                public Long apply(final String input)
                {
                    return Long.parseLong(input);
                }
            })));
        }
        catch (final NumberFormatException e)
        {
            // this doesn't look like the old long-id
            return Option.none();
        }
    }

    public AttachmentsBulkOperationResult<ChangeItemBean> convertTemporaryAttachments(
            final ApplicationUser user, final Issue issue,
            final List<String> temporaryAttachmentsIds)
    {
        return convertToLongIds(temporaryAttachmentsIds).fold(new Supplier<AttachmentsBulkOperationResult<ChangeItemBean>>()
        {
            @Override
            public AttachmentsBulkOperationResult<ChangeItemBean> get()
            {
                return temporaryWebAttachmentManager.convertTemporaryAttachments(user, issue, temporaryAttachmentsIds);
            }
        }, new Function<List<Long>, AttachmentsBulkOperationResult<ChangeItemBean>>()
        {
            @Override
            public AttachmentsBulkOperationResult<ChangeItemBean> apply(final List<Long> longIds)
            {
                return convertTemporaryAttachmentsInOldWay(user, issue, longIds);
            }
        });
    }

    @SuppressWarnings ("deprecation")
    private AttachmentsBulkOperationResult<ChangeItemBean> convertTemporaryAttachmentsInOldWay(
            final ApplicationUser user, final Issue issue, final List<Long> longIds)
    {
        log.warn("Using deprecated method for adding attachments to issue! Someone still uses "
                + "TemporaryAttachmentsMonitor - you can probably track him by looking at the referrer "
                + "or previous requests to JIRA.");

        final TemporaryAttachmentsMonitor monitor = temporaryAttachmentsMonitorLocator.get(false);
        if (monitor == null)
        {
            final I18nHelper i18n = i18nFactory.getInstance(user);
            return new AttachmentsBulkOperationResult<ChangeItemBean>(ImmutableList.of(new AttachmentError(
                    "Couldn't get temporary attachments monitor - session probably expired!",
                    i18n.getText("attachment.temporary.id.session.time.out"), "", SERVER_ERROR
            )), Collections.<ChangeItemBean>emptyList());
        }
        else
        {
            return attachmentManager.tryConvertTemporaryAttachments(user, issue, longIds, monitor);
        }
    }

    public Collection<?> getTemporaryWebAttachmentsByFormToken(final String formToken)
    {
        final Collection<TemporaryWebAttachment> attachments =
                temporaryWebAttachmentManager.getTemporaryWebAttachmentsByFormToken(formToken);

        if (attachments.isEmpty())
        {
            // try fallback
            //noinspection deprecation
            final TemporaryAttachmentsMonitor monitor = temporaryAttachmentsMonitorLocator.get(false);
            return monitor != null ? monitor.getByFormToken(formToken) : Collections.emptyList();
        }
        else
        {
            return attachments;
        }
    }

    public void clearTemporaryAttachmentsByFormToken(final String formToken)
    {
        Assertions.notNull("formToken", formToken);

        temporaryWebAttachmentManager.clearTemporaryAttachmentsByFormToken(formToken);

        // remove also from old monitor
        //noinspection deprecation
        final TemporaryAttachmentsMonitor monitor = temporaryAttachmentsMonitorLocator.get(false);
        if (monitor != null)
        {
            monitor.clearEntriesForFormToken(formToken);
        }
    }
}
