package com.atlassian.jira.issue.index;

import com.atlassian.fugue.Option;
import com.atlassian.jira.datetime.LocalDateFactory;
import com.atlassian.jira.index.SearchExtractorRegistrationManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.index.indexers.impl.BaseFieldIndexer;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.util.LuceneUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.Term;

public class DefaultWorklogDocumentFactory implements WorklogDocumentFactory
{
    private final SearchExtractorRegistrationManager searchExtractorManager;

    public DefaultWorklogDocumentFactory(final SearchExtractorRegistrationManager searchExtractorManager)
    {
        this.searchExtractorManager = searchExtractorManager;
    }

    public Option<Document> apply(final Worklog worklog)
    {
        if (worklog == null)
        {
            return Option.none();
        }
        final Issue issue = worklog.getIssue();
        final Builder builder = new Builder(worklog)
                .addField(DocumentConstants.PROJECT_ID, String.valueOf(issue.getProjectId()), Field.Store.NO, Field.Index.NOT_ANALYZED_NO_NORMS)
                .addField(DocumentConstants.ISSUE_ID, String.valueOf(issue.getId()), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS)
                .addField(DocumentConstants.WORKLOG_ID, String.valueOf(worklog.getId()), Field.Store.NO, Field.Index.NOT_ANALYZED_NO_NORMS)
                .addField(DocumentConstants.WORKLOG_DATE, LuceneUtils.localDateToString(LocalDateFactory.from(worklog.getStartDate())), Field.Store.NO, Field.Index.NOT_ANALYZED_NO_NORMS)
                .addKeywordWithDefault(DocumentConstants.WORKLOG_LEVEL, worklog.getGroupLevel(), BaseFieldIndexer.NO_VALUE_INDEX_VALUE)
                .addKeywordWithDefault(DocumentConstants.WORKLOG_LEVEL_ROLE, worklog.getRoleLevel() != null ? worklog.getRoleLevel().getId() : null, BaseFieldIndexer.NO_VALUE_INDEX_VALUE)
                .addAllExtractors(searchExtractorManager.findExtractorsForEntity(Worklog.class));

        if (worklog.getAuthorKey() != null)
        {
            builder.addField(DocumentConstants.WORKLOG_AUTHOR, worklog.getAuthorKey(), Field.Store.NO, Field.Index.NOT_ANALYZED_NO_NORMS)
                    .addField(DocumentConstants.ISSUE_VISIBLE_FIELD_IDS, DocumentConstants.WORKLOG_AUTHOR, Field.Store.NO, Field.Index.NOT_ANALYZED_NO_NORMS) // we add this here so that standard handler of != and "NOT IN" operators works
                    .addField(DocumentConstants.ISSUE_NON_EMPTY_FIELD_IDS, DocumentConstants.WORKLOG_AUTHOR, Field.Store.NO, Field.Index.NOT_ANALYZED_NO_NORMS); // we add this here so that standard handler of != and "NOT IN" operators works
        }

        return builder.build();
    }

    private static class Builder extends EntityDocumentBuilder<Worklog, Builder>
    {
        private Builder(final Worklog entity)
        {
            super(entity, SearchProviderFactory.WORKLOG_INDEX);
        }
    }

    public Term getIdentifyingTerm(final Worklog worklog)
    {
        return new Term(DocumentConstants.WORKLOG_ID, worklog.getId().toString());
    }
}
