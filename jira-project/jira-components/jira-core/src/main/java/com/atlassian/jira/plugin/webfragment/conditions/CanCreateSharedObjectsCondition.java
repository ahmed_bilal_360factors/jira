package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;

/**
 * Checks if the logged in user has the rights to create shared objects.
 *
 * @since v4.0
 */
public class CanCreateSharedObjectsCondition extends AbstractFixedPermissionCondition
{
    public CanCreateSharedObjectsCondition(PermissionManager permissionManager)
    {
        super(permissionManager, Permissions.CREATE_SHARED_OBJECTS);
    }
}
