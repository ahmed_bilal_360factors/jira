package com.atlassian.jira.license;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Common class for events that happen to a {@link com.atlassian.jira.license.LicenseRole}.
 *
 * @since v6.4
 */
public abstract class LicenseRoleEvent
{
    private final LicenseRoleId id;

    LicenseRoleEvent(final LicenseRoleId id)
    {
        this.id = notNull("id", id);
    }

    public LicenseRoleId getLicenseRoleId()
    {
        return id;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final LicenseRoleEvent that = (LicenseRoleEvent) o;
        return id.equals(that.id);

    }

    @Override
    public int hashCode()
    {
        return id.hashCode();
    }
}
