package com.atlassian.jira.service.services.analytics;

import java.util.List;
import java.util.Map;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.extension.JiraStartedEvent;
import com.atlassian.jira.extension.JiraStoppedEvent;
import com.atlassian.jira.service.services.analytics.start.GroupAnalyticTask;
import com.atlassian.jira.service.services.analytics.start.JiraBasicStatsAnalyticTask;
import com.atlassian.jira.service.services.analytics.start.JiraStartAnalyticEvent;
import com.atlassian.jira.service.services.analytics.start.JiraSystemAnalyticTask;
import com.atlassian.jira.service.services.analytics.start.UserAnalyticTask;
import com.atlassian.jira.service.services.analytics.start.WorkflowAnalyticTask;
import com.atlassian.jira.service.services.analytics.stop.JiraStopAnalyticEvent;
import com.atlassian.jira.service.services.analytics.stop.ReportUptimeAnalyticsTask;

import com.google.common.collect.ImmutableList;

import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

/**
 * Service that grabs the configuration of the user and sends analytics.
 * <p/>
 * This analytic is extensible by adding a new class that implements JiraAnalyticTask and registering here
 *
 * @since 6.4
 */
@EventComponent
public class JiraLifecycleAnalyticsService
{

    private final static Logger log = LoggerFactory.getLogger(JiraLifecycleAnalyticsService.class);

    private final EventPublisher eventPublisher;

    public JiraLifecycleAnalyticsService(final EventPublisher eventPublisher)
    {
        this.eventPublisher = eventPublisher;
    }

    /**
     * When Jira starts we calculate the metrics. We do it with an event so we don't mess with the launchers.
     */
    @EventListener
    public void onJiraStart(final JiraStartedEvent event)
    {
        eventPublisher.publish(new JiraStartAnalyticEvent(getUsageStats(false)));
    }

    @Nonnull
    public Map<String, Object> getUsageStats(boolean onlyDataShapeStatistics)
    {
        final List<? extends JiraAnalyticTask> tasks = getStartupTasks();
        final ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();

        for (final JiraAnalyticTask task : tasks)
        {
            if (onlyDataShapeStatistics && !task.isReportingDataShape())
            {
                continue;
            }

            try
            {
                task.init();
                builder.putAll(task.getAnalytics());
            }
            catch (Exception e)
            {
                log.error(" Error trying to grab analytics from [{}]", task.getClass().getName(), e);
            }
        }
        return builder.build();
    }

    @EventListener
    public void onJiraStop(final JiraStoppedEvent jiraStoppedEvent)
    {
        final List<? extends JiraAnalyticTask> tasks = getShutdownTasks();
        final ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();

        for (final JiraAnalyticTask task : tasks)
        {
            try
            {
                task.init();
                builder.putAll(task.getAnalytics());
            }
            catch (Exception e)
            {
                log.error(" Error trying to grab analytics from [{}]", task.getClass().getName(), e);
            }
        }

        eventPublisher.publish(new JiraStopAnalyticEvent(builder.build()));
    }

    /**
     * Returns all the tasks to run on startup
     *
     * @return the list of task to run
     */
    @Nonnull
    public List<? extends JiraAnalyticTask> getStartupTasks()
    {
        return ImmutableList.of(
                new JiraSystemAnalyticTask(),
                new JiraBasicStatsAnalyticTask(),
                new UserAnalyticTask(),
                new WorkflowAnalyticTask(),
                new GroupAnalyticTask());
    }

    /**
     * Returns all the tasks to run on shutdown
     *
     * @return the list of task to run
     */
    @Nonnull
    public List<? extends JiraAnalyticTask> getShutdownTasks()
    {
        return ImmutableList.of(
                new ReportUptimeAnalyticsTask());
    }
}
