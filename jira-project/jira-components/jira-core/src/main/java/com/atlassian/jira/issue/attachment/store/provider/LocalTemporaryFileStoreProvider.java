package com.atlassian.jira.issue.attachment.store.provider;

import java.io.File;

import com.atlassian.jira.issue.attachment.AttachmentDirectoryAccessor;
import com.atlassian.jira.issue.attachment.store.LocalTemporaryFileStore;
import com.atlassian.jira.issue.attachment.store.PathTraversalChecker;

import org.picocontainer.injectors.ProviderAdapter;

public class LocalTemporaryFileStoreProvider extends ProviderAdapter
{

    public LocalTemporaryFileStore provide(final AttachmentDirectoryAccessor attachmentDirectoryAccessor)
    {
        final File temporaryAttachmentDirectory = attachmentDirectoryAccessor.getTemporaryAttachmentDirectory();
        return  new LocalTemporaryFileStore(temporaryAttachmentDirectory, new PathTraversalChecker());
    }

}
