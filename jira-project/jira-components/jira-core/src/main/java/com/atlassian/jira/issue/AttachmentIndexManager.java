package com.atlassian.jira.issue;

import java.io.File;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.plugin.attachment.AttachmentArchive;

/**
 * Provides possibility to save cache contents of archive files to database.
 * @since v6.3
 */
public interface AttachmentIndexManager
{
    /**
     * This methods generates the index of and archive file.
     * It looks up for plugins that can handle processing file with certain extension.
     * Then it stores given attachment index to database.
     *
     * Note that after processing file other than archive its index will still be saved as an empty JSON array ("[]")
     * in order to store information that this file was already processed so that getAttachmentContents will not call
     * this method unnecessarily on each issue display.
     *
     * @param file attachment.
     * @param attachment for which index provided.
     * @param issue to which attachment belongs.
     */
    void processAttachmentAndCreateIndex(File file, Attachment attachment, Issue issue);

    /**
     * Retrieves index of given attachment file. If the index does not exist tries to obtain Attachment file
     * from AttachmentStore and if obtained saves its index to
     * the database.
     * @param attachment for which index will be retrieved.
     * @param issue to which attachment belongs.
     * @param maxEntries maximum number of entries that should be returned in
     * @return Option.none if no index was found or contents that were retrieved from database.
     */
    Option<AttachmentArchive> getAttachmentContents(Attachment attachment, Issue issue, int maxEntries);

    /**
     * Removes the index data of the specified attachment.
     * @param attachment for which index file will be removed.
     * @param issue to which attachment belongs.
     */
    void removeAttachmentIndex(Attachment attachment, Issue issue);

    /**
     * Checks if provided attachment is currently expandable.
     * @param attachment to be tested.
     * @return true only if attachment can be expanded by JIRA.
     */
    boolean isExpandable(Attachment attachment);

}
