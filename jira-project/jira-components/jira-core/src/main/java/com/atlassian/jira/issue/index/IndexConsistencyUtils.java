package com.atlassian.jira.issue.index;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.lucene.search.IndexSearcher;

/**
 * Utility methods related to performing consistency checks on indexes.
 *
 * @since v5.2
 */
public class IndexConsistencyUtils
{
    private static final Logger log = LoggerFactory.getLogger(IndexConsistencyUtils.class);

    // static-only class
    private IndexConsistencyUtils()
    {}

    /**
     * Performs a simple consistency check on an index by opening it, comparing the document count to an expected value
     * supplied by the caller, and closing it.  If the expected document count can not be determined reliably and
     * efficiently, then {@code -1} may be specified to skip that part of the check.  If an expected count is given, the
     * actual count must be within {@code 10%} of the expected value or {@code 10} documents, whichever value is
     * larger.
     * <p/>
     * Note: Implicitly closes the searcher
     *
     * @param name a name to identify the index
     * @param expectedCount the expected count of documents in the index, or {@code -1} to skip this check
     * @param searcher index searcher
     * @return {@code true} if the index is present and contains reasonably close to the expected number of documents;
     * {@code false} if any exception is thrown or if the index document count is excessively different from the
     * expected count
     */
    public static boolean isIndexConsistent(final String name, final int expectedCount, final IndexSearcher searcher)
            throws IOException
    {
        try
        {
            final int actualCount = searcher.getIndexReader().numDocs();
            if (log.isDebugEnabled())
            {
                log.debug("isIndexConsistent: " + name + ": expectedCount=" + expectedCount + "; actualCount=" + actualCount);
            }
            if (expectedCount >= 0)
            {
                final int delta = Math.abs(expectedCount - actualCount);
                final int tolerance = Math.max(10, expectedCount / 10);
                if (delta > tolerance)
                {
                    log.warn("Index consistency check failed for index '" + name + "': expectedCount=" + expectedCount + "; actualCount=" + actualCount);
                    return false;
                }
            }
        }
        finally
        {
            searcher.close();
        }
        return true;
    }
}
