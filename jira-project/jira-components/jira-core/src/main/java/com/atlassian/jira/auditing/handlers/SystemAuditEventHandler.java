package com.atlassian.jira.auditing.handlers;

import com.atlassian.jira.auditing.RecordRequest;
import com.atlassian.jira.license.ConfirmEvaluationLicenseEvent;
import com.atlassian.jira.license.NewLicenseEvent;
import javax.annotation.Nonnull;

/**
 * Handles various audit events that are raised to be at a 'System' level. System events are those that are not
 * generated by a user. Event handlers will typically create a RecordRequest. A RecordRequest is used to store it in the
 * audit logs.
 *
 * @since 6.4
 */
public interface SystemAuditEventHandler
{
    /**
     * Event handler for events raised when a new license is added in the system.
     *
     * @param event the new license event
     * @return the RecordRequest created as part of the event handler
     */
    @Nonnull
    RecordRequest onNewLicenseEvent(@Nonnull final NewLicenseEvent event);

    /**
     * Event handler for events raised when the admin has requested to continue with evaluation license after
     * one or more licenses expired.
     *
     * @param event The event confirming that the evaluation should continue
     * @return a record of all expired licenses and user (name) who approved to continue with the evaluation
     */
    @Nonnull
    RecordRequest onExtendTrialLicense(@Nonnull final ConfirmEvaluationLicenseEvent event);
}