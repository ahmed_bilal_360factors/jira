package com.atlassian.jira.auditing.handlers;

import com.atlassian.jira.auditing.AffectedLicense;
import com.atlassian.jira.auditing.AuditingCategory;
import com.atlassian.jira.auditing.ChangedValue;
import com.atlassian.jira.auditing.RecordRequest;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.license.ConfirmEvaluationLicenseEvent;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.LicenseRoleDetails;
import com.atlassian.jira.license.LicenseRoleId;
import com.atlassian.jira.license.NewLicenseEvent;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.util.OutlookDate;
import com.atlassian.jira.web.util.OutlookDateManager;

import java.util.List;
import java.util.Set;
import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @since 6.4
 */
public class SystemAuditEventHandlerImpl implements SystemAuditEventHandler
{
    private static final String ADMIN_LICENSE_ORGANISATION = "admin.license.organisation";
    private static final String ADMIN_LICENSE_DATE_PURCHASED = "admin.license.date.purchased";
    private static final String ADMIN_LICENSE_TYPE = "admin.license.type";
    private static final String ADMIN_SERVER_ID = "admin.server.id";
    private static final String ADMIN_LICENSE_SEN = "admin.license.sen";
    private final I18nHelper i18nHelper;
    private final OutlookDateManager outlookDateManager;
    private final DateTimeFormatterFactory dateTimeFormatterFactory;

    public SystemAuditEventHandlerImpl(I18nHelper i18nHelper, OutlookDateManager outlookDateManager,
            DateTimeFormatterFactory dateTimeFormatterFactory)
    {

        this.i18nHelper = i18nHelper;
        this.outlookDateManager = outlookDateManager;
        this.dateTimeFormatterFactory = dateTimeFormatterFactory;
    }

    @Override
    @Nonnull
    public RecordRequest onNewLicenseEvent(@Nonnull final NewLicenseEvent event)
    {
        checkNotNull(event, "event");


        final LicenseDetails licenseDetails = event.getLicenseDetails();

        checkNotNull(licenseDetails);
        checkNotNull(licenseDetails.getJiraLicense());

        return new RecordRequest(AuditingCategory.SYSTEM, "jira.auditing.system.license.added")
                .forObject(new AffectedLicense(licenseDetails))
                .withChangedValues(buildChangedValuesForNewLicense(licenseDetails));
    }

    @Override
    @Nonnull
    public RecordRequest onExtendTrialLicense(@Nonnull final ConfirmEvaluationLicenseEvent event)
    {
        checkNotNull(event, "event");

        return new RecordRequest(AuditingCategory.SYSTEM, "jira.auditing.system.license.extend.evaluation")
                .forObject(new AffectedLicense(event.getUserWhoConfirmed(), true))
                .withChangedValues(buildChangedValuesForEvaluationExtended(event.getExpiredLicenses()));
    }

    private List<ChangedValue> buildChangedValuesForNewLicense(final LicenseDetails details)
    {
        final ChangedValuesBuilder changedValues = new ChangedValuesBuilder();

        changedValues.add(ADMIN_LICENSE_ORGANISATION, null, details.getOrganisation());
        changedValues.add(ADMIN_LICENSE_DATE_PURCHASED, null, details.getPurchaseDate(getOutlookDate()));
        changedValues.add(ADMIN_LICENSE_TYPE, null, details.getDescription());
        changedValues.add(ADMIN_SERVER_ID, null, details.getJiraLicense().getServerId());
        changedValues.add(ADMIN_LICENSE_SEN, null, details.getSupportEntitlementNumber());

        buildUserLimitForNewLicenseRoles(details, changedValues);
        buildChangedValuesForNewLicenseRoles(details, changedValues);

        return changedValues.build();
    }

    private List<ChangedValue> buildChangedValuesForEvaluationExtended(final Iterable<LicenseDetails> detailsBag)
    {
        DateTimeFormatter dateTimeFormatter = dateTimeFormatterFactory.formatter().withStyle(DateTimeStyle.ISO_8601_DATE);
        final ChangedValuesBuilder changedValues = new ChangedValuesBuilder();

        for (LicenseDetails details : detailsBag)
        {
            changedValues.add(ADMIN_LICENSE_ORGANISATION, null, details.getOrganisation());
            changedValues.add(ADMIN_LICENSE_DATE_PURCHASED, null, details.getPurchaseDate(getOutlookDate()));
            changedValues.add("admin.license.date.expired", null,
                    dateTimeFormatter.format(details.getJiraLicense().getMaintenanceExpiryDate()));
            changedValues.add(ADMIN_LICENSE_TYPE, null, details.getDescription());
            changedValues.add(ADMIN_SERVER_ID, null, details.getJiraLicense().getServerId());
            changedValues.add(ADMIN_LICENSE_SEN, null, details.getSupportEntitlementNumber());
            buildUserLimitForNewLicenseRoles(details, changedValues);
        }
        return changedValues.build();
    }

    /**
     * Adding LicenseRoles information in the Audit Log
     *
     * @param details the details of the license
     * @param changedValues the builder with all the information to audit
     * @see http://jdog.jira-dev.com/browse/HIROL-60
     */
    private void buildChangedValuesForNewLicenseRoles(final LicenseDetails details, final ChangedValuesBuilder changedValues)
    {
        final LicenseRoleDetails licenseRoles = details.getLicenseRoles();
        final Set<LicenseRoleId> roles = licenseRoles.getIds();

        if (roles.isEmpty())
        {
            return;
        }

        for (LicenseRoleId roleId : roles)
        {
            changedValues.add(roleId.getName(), null, String.valueOf(licenseRoles.getUserLimit(roleId)));
        }
    }

    /**
     * Evaluates the amount of users the license can have and logs it in the builder.
     *
     * @param details the license details
     * @param changedValues the builder with all the information to audit
     */
    private void buildUserLimitForNewLicenseRoles(final LicenseDetails details, final ChangedValuesBuilder changedValues)
    {

        final String userLimit = !details.isUnlimitedNumberOfUsers()
                ? String.valueOf(details.getJiraLicense().getMaximumNumberOfUsers())
                : i18nHelper.getText("common.words.unlimited");

        changedValues.add("admin.license.user.limit", null, userLimit);
    }

    private OutlookDate getOutlookDate()
    {
        return outlookDateManager.getOutlookDate(i18nHelper.getLocale());
    }
}
