package com.atlassian.jira.issue.index;

import javax.annotation.Nonnull;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.PropertiesUtil;
import com.atlassian.jira.index.Configuration;
import com.atlassian.jira.index.Index;
import com.atlassian.jira.index.Indexes;

/**
 * Used by {@link com.atlassian.jira.issue.index.IndexDirectoryFactory.IndexPathAdapter} to create {@link com.atlassian.jira.index.Index.Manager}. There are two base
 * implementations {@link #DIRECT} and {@link #QUEUED} which create an {@link com.atlassian.jira.index.Index.Manager} that is either blocking and non-threaded or non-blocking and threaded.
 *
 * @since v6.4
 */
public interface IndexingMode
{
    IndexingMode DIRECT = new DirectIndexingMode();
    IndexingMode QUEUED = new QueuedIndexingMode();

    @Nonnull
    Index.Manager createIndexManager(final String name, final Configuration configuration, final ApplicationProperties applicationProperties);

    class DirectIndexingMode implements IndexingMode
    {
        @Nonnull
        @Override
        public Index.Manager createIndexManager(final String name, final Configuration configuration, final ApplicationProperties applicationProperties)
        {
            return Indexes.createSimpleIndexManager(configuration);
        }
    }

    class QueuedIndexingMode implements IndexingMode
    {
        @Nonnull
        @Override
        public Index.Manager createIndexManager(final String name, final Configuration configuration, final ApplicationProperties applicationProperties)
        {
            int maxQueueSize = PropertiesUtil.getIntProperty(applicationProperties, APKeys.JiraIndexConfiguration.Issue.MAX_QUEUE_SIZE, 1000);
            return Indexes.createQueuedIndexManager(name, configuration, maxQueueSize);
        }
    }
}
