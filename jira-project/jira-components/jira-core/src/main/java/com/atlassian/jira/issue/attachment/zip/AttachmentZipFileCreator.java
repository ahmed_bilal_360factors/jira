package com.atlassian.jira.issue.attachment.zip;

import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.BulkAttachmentOperations;
import com.atlassian.jira.util.Consumer;
import com.atlassian.jira.util.IOUtil;
import com.atlassian.jira.util.collect.EnclosedIterable;
import com.atlassian.jira.util.io.InputStreamConsumer;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nonnull;

import io.atlassian.blobstore.client.api.Unit;

/**
 * This class can create temporary zip files containing all the attachments on an issue
 *
 * @since v4.1
 */
public class AttachmentZipFileCreator
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AttachmentZipFileCreator.class);
    private final BulkAttachmentOperations bulkAttachmentOperations;
    private final AttachmentManager attachmentManager;

    public AttachmentZipFileCreator(final BulkAttachmentOperations bulkAttachmentOperations,
            final AttachmentManager attachmentManager)
    {
        this.bulkAttachmentOperations = bulkAttachmentOperations;
        this.attachmentManager = attachmentManager;
    }

    /**
     * This will return a ZIP file that contains all the attachments of an issue.  The file will be created in the
     * temporary directory and end in .zip
     * <p/>
     * you should delete the file when you are done, otherwise, well this is JIRA not Bamboo!
     *
     * @return a zip file containing all the attachments of an issue
     * @throws IOException if stuff goes wrong
     */
    public File toZipFile(final Issue issue) throws IOException
    {
        final File zipFile = File.createTempFile(issue.getKey() + "-", ".zip");
        final UniqueFileNameGenerator uniqueFileNameGenerator = new UniqueFileNameGenerator();

        ZipArchiveOutputStream out = null;
        try
        {
            out = new ZipArchiveOutputStream(new FileOutputStream(zipFile));
            copyAttachmentsToZipFile(issue, uniqueFileNameGenerator, out);
        }
        finally
        {
            IOUtil.shutdownStream(out);
        }

        return zipFile;
    }

    private void copyAttachmentsToZipFile(final Issue issue, final UniqueFileNameGenerator uniqueFileNameGenerator,
            final ZipArchiveOutputStream out)
    {
        final EnclosedIterable<Attachment> attachmentsOfIssue = bulkAttachmentOperations.getAttachmentOfIssue(issue);
        attachmentsOfIssue.foreach(copyAttachmentToOutput(uniqueFileNameGenerator, out));
    }

    private Consumer<Attachment> copyAttachmentToOutput(final UniqueFileNameGenerator uniqueFileNameGenerator,
            final ZipArchiveOutputStream out)
    {
        return new Consumer<Attachment>()
        {
            @Override
            public void consume(@Nonnull final Attachment attachment)
            {
                final InputStreamConsumer<Unit> streamConsumer = new InputStreamConsumer<Unit>()
                {
                    @Override
                    public Unit withInputStream(final InputStream inputStream) throws IOException
                    {
                        final String attachmentFileName = attachment.getFilename();
                        final ZipArchiveEntry zipEntry = new ZipArchiveEntry(uniqueFileNameGenerator.getUniqueFileName(
                                attachmentFileName));
                        out.putArchiveEntry(zipEntry);
                        IOUtil.copy(inputStream, out);
                        out.closeArchiveEntry();
                        return Unit.UNIT;
                    }
                };

                try
                {
                    attachmentManager.streamAttachmentContent(attachment, streamConsumer);
                }
                catch (final IOException e)
                {
                    LOGGER.info("Streaming operation failed for attachment with Id " + attachment.getId());
                }
            }
        };
    }

    /**
     * A Set based thingy that remembers names that have been seen by it before and returns them munged into new unique
     * names.  brad.js will become brad.js.1 and brad.js.2 each time it is presented.
     */
    static class UniqueFileNameGenerator
    {
        private static final Pattern pattern = Pattern.compile("\\.([0-9]+)$");
        private Set<String> fileNamesSet = new HashSet<String>();

        public String getUniqueFileName(final String fileName)
        {
            String safeFileName = fileName;
            while (fileNamesSet.contains(safeFileName))
            {
                safeFileName = mungeFileName(safeFileName);
            }
            fileNamesSet.add(safeFileName);
            return safeFileName;
        }

        private String mungeFileName(final String fileName)
        {
            Matcher m = pattern.matcher(fileName);
            if (m.find())
            {
                // ok lets replace the number at the end
                String numberStr = m.group(m.groupCount());
                int number = Integer.parseInt(numberStr, 10) + 1;

                return m.replaceFirst("." + number);
            }
            else
            {
                return fileName + ".1";
            }
        }
    }

}
