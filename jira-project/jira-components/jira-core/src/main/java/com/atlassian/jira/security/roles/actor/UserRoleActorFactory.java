package com.atlassian.jira.security.roles.actor;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.annotations.Internal;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.database.SqlCallback;
import com.atlassian.jira.model.querydsl.QProjectRoleActor;
import com.atlassian.jira.security.roles.OptimizedRoleActorFactory;
import com.atlassian.jira.security.roles.ProjectRoleActor;
import com.atlassian.jira.security.roles.RoleActor;
import com.atlassian.jira.security.roles.RoleActorDoesNotExistException;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.dbc.Assertions;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.mysema.query.Tuple;
import com.mysema.query.types.Projections;

import static com.atlassian.jira.security.roles.ProjectRoleActor.USER_ROLE_ACTOR_TYPE;
import static com.google.common.collect.Iterables.all;

/**
 * Responsible for construction of UserRoleActor instances. Also optimises the
 * lookup where we have many users in a particular role for a project by doing
 * a map lookup based on the username.
 * <p>
 * Access to the actual User instance is via a UserFactory so we can unit-test.
 * The production dependency is set in the default ctor.
 */
@Internal
public class UserRoleActorFactory implements OptimizedRoleActorFactory
{
    /**
     * @deprecated Use {@link com.atlassian.jira.security.roles.ProjectRoleActor#USER_ROLE_ACTOR_TYPE} instead. Since v6.4.
     */
    public static final String TYPE = "atlassian-user-role-actor";

    private final UserManager userManager;
    private final DbConnectionManager dbConnectionManager;

    public UserRoleActorFactory(final UserManager userManager, final DbConnectionManager dbConnectionManager)
    {
        this.userManager = userManager;
        this.dbConnectionManager = dbConnectionManager;
    }

    @Override
    public ProjectRoleActor createRoleActor(final Long id, final Long projectRoleId, final Long projectId, final String type, final String parameter)
            throws RoleActorDoesNotExistException
    {
        if (!USER_ROLE_ACTOR_TYPE.equals(type))
        {
            throw new IllegalArgumentException(this.getClass().getName() + " cannot create RoleActors of type: " + type);
        }
        Assertions.notNull("parameter", parameter);
        final ApplicationUser user = userManager.getUserByKeyEvenWhenUnknown(parameter);
        if (user == null)
        {
            throw new RoleActorDoesNotExistException("User '" + parameter + "' does not exist.");
        }
        return new UserRoleActor(id, projectRoleId, projectId, user);
    }

    @Override
    public Set<RoleActor> optimizeRoleActorSet(final Set<RoleActor> roleActors)
    {
        final Set<RoleActor> originals = new HashSet<RoleActor>(roleActors);
        final Set<UserRoleActor> userRoleActors = new HashSet<UserRoleActor>(roleActors.size());
        for (final Iterator<RoleActor> it = originals.iterator(); it.hasNext();)
        {
            final RoleActor roleActor = it.next();
            if (roleActor instanceof UserRoleActor)
            {
                userRoleActors.add((UserRoleActor) roleActor);
                it.remove();
            }
        }
        if (!userRoleActors.isEmpty())
        {
            // no point aggregating if there's only one
            if (userRoleActors.size() > 1)
            {
                final UserRoleActor prototype = userRoleActors.iterator().next();
                originals.add(new AggregateRoleActor(prototype, userRoleActors));
            }
            else
            {
                // just one? throw it back...
                originals.addAll(userRoleActors);
            }
        }
        return Collections.unmodifiableSet(originals);
    }

    @Override
    @Nonnull
    public Set<ProjectRoleActor> getAllRoleActorsForUser(@Nullable final ApplicationUser user)
    {
        if (user == null)
        {
            return ImmutableSet.of();
        }

        final String userKey = user.getKey();
        final ImmutableSet.Builder<ProjectRoleActor> resultsBuilder = ImmutableSet.builder();

        dbConnectionManager.execute(new SqlCallback()
        {
            @Override
            public void run(final DbConnection dbConnection)
            {
                final QProjectRoleActor pra = new QProjectRoleActor("pra");

                final List<Tuple> roleActorTuples = dbConnection.newSqlQuery().
                        from(pra).
                        where(pra.roletype.eq(USER_ROLE_ACTOR_TYPE).
                                and(pra.roletypeparameter.eq(user.getKey())).
                                and(pra.pid.isNotNull())).
                        list(Projections.tuple(pra.id, pra.pid, pra.projectroleid, pra.roletype, pra.roletypeparameter));

                for (Tuple tuple : roleActorTuples)
                {
                    resultsBuilder.add(new UserRoleActor(tuple, pra, userKey));
                }
            }
        });

        return resultsBuilder.build();
    }

    class UserRoleActor extends AbstractRoleActor
    {
        private UserRoleActor(final Long id, final Long projectRoleId, final Long projectId, final ApplicationUser user)
        {
            super(id, projectRoleId, projectId, ApplicationUsers.getKeyFor(user));
        }

        private UserRoleActor(final Tuple tuple, final QProjectRoleActor projectRoleActor, final String userKey)
        {
            super(tuple.get(projectRoleActor.id), tuple.get(projectRoleActor.projectroleid), tuple.get(projectRoleActor.pid), userKey);
        }

        public String getType()
        {
            return USER_ROLE_ACTOR_TYPE;
        }

        @Override
        public boolean isActive()
        {
            return getUser().isActive();
        }

        public String getDescriptor()
        {
            return getAppUser().getDisplayName();
        }

        public Set<User> getUsers()
        {
            return CollectionBuilder.newBuilder(getUser()).asSet();
        }

        public boolean contains(final ApplicationUser user)
        {
            return user != null && user.getKey().equals(getParameter());
        }

        @Override
        public boolean contains(final User user)
        {
            return this.contains(ApplicationUsers.from(user));
        }

        private ApplicationUser getAppUser()
        {
            return userManager.getUserByKeyEvenWhenUnknown(getParameter());
        }

        private User getUser()
        {
            return ApplicationUsers.toDirectoryUser(userManager.getUserByKeyEvenWhenUnknown(getParameter()));
        }

    }

    /**
     * Aggregate UserRoleActors and look them up based on the hashcode
     */
    static class AggregateRoleActor extends AbstractRoleActor
    {
        private final Map <String, UserRoleActor> userRoleActorMap;

        private AggregateRoleActor(final ProjectRoleActor prototype, final Set<UserRoleActor> roleActors)
        {
            super(null, prototype.getProjectRoleId(), prototype.getProjectId(), null);

            final Map<String, UserRoleActor> map = new HashMap<String, UserRoleActor>(roleActors.size());

            for (final UserRoleActor userRoleActor : roleActors)
            {
                map.put(userRoleActor.getParameter(), userRoleActor);
            }
            this.userRoleActorMap = Collections.unmodifiableMap(map);
        }

        @Override
        public boolean isActive()
        {
            return all(userRoleActorMap.values(), new Predicate<UserRoleActor>()
            {
                @Override
                public boolean apply(@Nullable final UserRoleActor user)
                {
                    return user != null && user.isActive();
                }
            });
        }

        @Override
        public boolean contains(final ApplicationUser user)
        {
            return user != null && userRoleActorMap.containsKey(user.getKey())
                    && userRoleActorMap.get(user.getKey()).contains(user);
        }

        @Override
        public boolean contains(final User user)
        {
            return contains(ApplicationUsers.from(user));
        }

        /*
         * not enormously efficient, could cache users maybe, we want contains to be fast...
         *
         * @see com.atlassian.jira.security.roles.RoleActor#getUsers()
         */
        public Set<User> getUsers()
        {
            final Set<User> result = new HashSet<User>(userRoleActorMap.size());
            for (UserRoleActor roleActor : userRoleActorMap.values())
            {
                // not the most efficient, but generally called in UI etc.
                result.addAll(roleActor.getUsers());
            }
            return Collections.unmodifiableSet(result);
        }

        public String getType()
        {
            return USER_ROLE_ACTOR_TYPE;
        }
    }
}
