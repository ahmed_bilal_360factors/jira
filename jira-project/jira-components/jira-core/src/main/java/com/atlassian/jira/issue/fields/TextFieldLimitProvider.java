package com.atlassian.jira.issue.fields;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides limits for text fields.
 *
 * @since v5.0.3
 */
public class TextFieldLimitProvider
{
    private static final long DEFAULT_TEXT_FIELD_LIMIT = 30000;

    private final ApplicationProperties applicationProperties;
    private final LogOnceLogger propertyMissingLog = new LogOnceLogger(LoggerFactory.getLogger(getClass()));
    private final LogOnceLogger invalidFormatLog = new LogOnceLogger(LoggerFactory.getLogger(getClass()));

    public TextFieldLimitProvider(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
    }
    
    public long getTextFieldLimit()
    {
        return retrieveTextLimitFromApplicationProperties();
    }

    private long retrieveTextLimitFromApplicationProperties()
    {
        final String textFieldLimitString = applicationProperties.getDefaultBackedString(APKeys.JIRA_TEXT_FIELD_CHARACTER_LIMIT);

        if (textFieldLimitString == null || "".equals(textFieldLimitString))
        {
            propertyMissingLog.warnOnlyOnce("Property '" + APKeys.JIRA_TEXT_FIELD_CHARACTER_LIMIT + "' is missing. Using default value of " + DEFAULT_TEXT_FIELD_LIMIT + " characters instead.");
            return DEFAULT_TEXT_FIELD_LIMIT;
        }

        try
        {
            return Long.parseLong(textFieldLimitString);
        }
        catch (NumberFormatException e)
        {
            invalidFormatLog.warnOnlyOnce("Invalid format of '" + APKeys.JIRA_TEXT_FIELD_CHARACTER_LIMIT + "' property: '" + textFieldLimitString + "'. Expected value to be a long. Using default value of " + DEFAULT_TEXT_FIELD_LIMIT + " characters instead.");
            return DEFAULT_TEXT_FIELD_LIMIT;
        }
    }

    private static class LogOnceLogger
    {
        private boolean alreadyLogged = false;
        private final Logger delegatee;

        private LogOnceLogger(final Logger delegatee)
        {
            this.delegatee = delegatee;
        }

        public void warnOnlyOnce(final String message)
        {
            if (!alreadyLogged)
            {
                alreadyLogged = true;
                delegatee.warn(message);
            }
        }
    }
}
