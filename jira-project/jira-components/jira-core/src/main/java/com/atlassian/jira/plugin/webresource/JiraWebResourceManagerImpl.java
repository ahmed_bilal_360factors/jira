package com.atlassian.jira.plugin.webresource;

import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceManagerImpl;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A simple subclass of WebResourceManagerImpl that allows us to override
 * some of its behaviour
 *
 * @since v4.4
 */
public class JiraWebResourceManagerImpl extends WebResourceManagerImpl implements JiraWebResourceManager
{
    private static final String REQUEST_CACHE_METADATA_KEY = "jira.metadata.map";

    public JiraWebResourceManagerImpl(
            PluginResourceLocator pluginResourceLocator,
            WebResourceIntegration webResourceIntegration,
            WebResourceUrlProvider webResourceUrlProvider,
            ResourceBatchingConfiguration batchingConfiguration)
    {
        super(pluginResourceLocator, webResourceIntegration, webResourceUrlProvider, batchingConfiguration);
    }

    public boolean putMetadata(String key, String value)
    {
        @SuppressWarnings("unchecked")
        Map<String, String> metadataMap = (LinkedHashMap<String, String>) cache().get(REQUEST_CACHE_METADATA_KEY);

        if (metadataMap == null)
        {
            metadataMap = new LinkedHashMap<String, String>();
            cache().put(REQUEST_CACHE_METADATA_KEY, metadataMap);
        }

        metadataMap.put(key, value);
        return true;
    }

    public Map<String, String> getMetadata()
    {
        @SuppressWarnings("unchecked")
        Map<String, String> metadataMap = (Map<String, String>) cache().remove(REQUEST_CACHE_METADATA_KEY);

        // We never want to return null, _always_ return a Map.
        if (metadataMap == null)
        {
            metadataMap = MapBuilder.emptyMap();
        }

        return metadataMap;
    }

    private Map<String, Object> cache()
    {
        return webResourceIntegration.getRequestCache();
    }
}
