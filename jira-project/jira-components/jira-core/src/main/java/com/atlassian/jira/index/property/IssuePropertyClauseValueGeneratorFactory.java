package com.atlassian.jira.index.property;

import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.jql.query.AliasedIssuePropertyClauseQueryFactory;
import com.atlassian.query.clause.Property;

/**
 * Factory for aliased issue properties auto-completion values generation.
 */
public class IssuePropertyClauseValueGeneratorFactory
{
    private final IssueIndexManager indexManager;

    public IssuePropertyClauseValueGeneratorFactory(final IssueIndexManager indexManager)
    {
        this.indexManager = indexManager;
    }

    public IssuePropertyClauseValueGenerator create(final Property property)
    {
        final String luceneFieldName = AliasedIssuePropertyClauseQueryFactory.getLuceneFieldName(property);
        return new IssuePropertyClauseValueGenerator(luceneFieldName, indexManager);
    }
}
