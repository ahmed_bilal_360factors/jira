package com.atlassian.jira.issue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachmentsMonitorLocator;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.jira.web.SessionKeys;
import com.atlassian.jira.web.action.issue.DefaultTemporaryWebAttachmentsMonitor;
import com.atlassian.jira.web.action.issue.TemporaryWebAttachmentsMonitor;

import com.google.common.base.Function;
import com.google.common.base.Supplier;

/**
 * Default implementation that uses the http session for storage.
 *
 * @since v6.4
 */
public class DefaultTemporaryWebAttachmentsMonitorLocator implements TemporaryWebAttachmentsMonitorLocator
{
    private final StreamAttachmentStore attachmentStore;

    public DefaultTemporaryWebAttachmentsMonitorLocator(final StreamAttachmentStore attachmentStore)
    {
        this.attachmentStore = attachmentStore;
    }

    private Option<TemporaryWebAttachmentsMonitor> getMonitorFromSession(final Option<HttpSession> session)
    {
        return session.flatMap(new Function<HttpSession, Option<TemporaryWebAttachmentsMonitor>>()
        {
            @Override
            public Option<TemporaryWebAttachmentsMonitor> apply(final HttpSession session)
            {
                return Option.option((TemporaryWebAttachmentsMonitor) session.getAttribute(SessionKeys.TEMP_WEB_ATTACHMENTS));
            }
        });
    }

    private TemporaryWebAttachmentsMonitor createNewMonitor(final HttpSession session)
    {
        final TemporaryWebAttachmentsMonitor monitor = new DefaultTemporaryWebAttachmentsMonitor(attachmentStore);
        session.setAttribute(SessionKeys.TEMP_WEB_ATTACHMENTS, monitor);
        return monitor;
    }

    private Option<HttpSession> getCurrentSession()
    {
        final HttpServletRequest request = ExecutingHttpRequest.get();
        return request == null ? Option.<HttpSession>none() : Option.some(request.getSession());
    }

    @Override
    public Option<TemporaryWebAttachmentsMonitor> get()
    {
        final Option<HttpSession> currentSession = getCurrentSession();
        return getMonitorFromSession(currentSession);
    }

    @Override
    public TemporaryWebAttachmentsMonitor getOrCreate()
    {
        //noinspection ConstantConditions
        final Option<HttpSession> sessionOption = getCurrentSession();
        final HttpSession session = sessionOption.getOrThrow(new Supplier<IllegalStateException>()
        {
            @Override
            public IllegalStateException get()
            {
                throw new IllegalStateException("Session doesn't exists, this method cannot work outside HTTP session!");
            }
        });

        return getMonitorFromSession(sessionOption).getOrElse(new Supplier<TemporaryWebAttachmentsMonitor>()
        {
            @Override
            public TemporaryWebAttachmentsMonitor get()
            {
                return createNewMonitor(session);
            }
        });

    }
}
