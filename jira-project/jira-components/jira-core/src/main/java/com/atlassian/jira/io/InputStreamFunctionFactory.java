package com.atlassian.jira.io;

import java.io.File;
import java.io.InputStream;

import com.atlassian.core.util.FileUtils;
import com.atlassian.fugue.Unit;
import com.atlassian.util.concurrent.Function;

import org.springframework.stereotype.Component;

/**
 * Creates functions accepting an {@link java.io.InputStream}.
 *
 * @since v6.4
 */
public class InputStreamFunctionFactory
{
    public Function<InputStream, Unit> saveStream(final File targetFile)
    {
        return new Function<InputStream, Unit>()
        {
            @Override
            public Unit get(final InputStream inputStream)
            {
                try
                {
                    FileUtils.copyFile(inputStream, targetFile, true);
                    return Unit.VALUE;
                }
                catch (final Exception e)
                {
                    throw new RuntimeException(e);
                }
            }
        };
    }
}
