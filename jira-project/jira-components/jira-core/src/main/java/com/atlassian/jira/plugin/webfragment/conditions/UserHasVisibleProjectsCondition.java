package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.base.Supplier;

import static com.atlassian.jira.plugin.webfragment.conditions.RequestCachingConditionHelper.cacheConditionResultInRequest;
import static java.lang.String.format;

/**
 * Checks if the user can see at least one project with this {@link AbstractPermissionCondition#permission}
 */
public class UserHasVisibleProjectsCondition extends AbstractPermissionCondition
{
    public UserHasVisibleProjectsCondition(PermissionManager permissionManager)
    {
        super(permissionManager);
    }

    public boolean shouldDisplay(final ApplicationUser user, JiraHelper jiraHelper)
    {
        return cacheConditionResultInRequest(getHasProjectsKey(permission, user), new Supplier<Boolean>()
        {
            @Override
            public Boolean get()
            {
                return permissionManager.hasProjects(permission, user);
            }
        });
    }
}
