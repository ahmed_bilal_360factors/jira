package com.atlassian.jira.index.request;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nonnull;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.SetMultimap;

/**
 * @since 6.4
 */
public class DefaultReindexRequestCoalescer implements ReindexRequestCoalescer
{
    @Nonnull
    @Override
    public List<ReindexRequest> coalesce(@Nonnull List<ReindexRequest> requests)
    {
        //Normalize all the requests to make comparison easier
        requests = normalizeRequests(requests);

        //Group requests and their components
        SetMultimap<ReindexRequestType, AffectedIndex> groupedAffectedIndexes = HashMultimap.create();
        SetMultimap<ReindexRequestType, SharedEntityType> groupedSharedEntityTypes = HashMultimap.create();
        ListMultimap<ReindexRequestType, ReindexRequest> groupedRequests = ArrayListMultimap.create();

        for (ReindexRequest request : requests)
        {
            ReindexRequestType type = request.getType();
            groupedRequests.put(type, request);
            groupedAffectedIndexes.putAll(type, request.getAffectedIndexes());
            groupedSharedEntityTypes.putAll(type, request.getSharedEntities());
        }

        //Generate new requests for each request type, combining their components
        Map<ReindexRequestType, ReindexRequest> combinedRequests = new EnumMap<ReindexRequestType, ReindexRequest>(ReindexRequestType.class);

        for (ReindexRequestType type : groupedRequests.keySet())
        {
            List<ReindexRequest> sourcesForType = groupedRequests.get(type);

            //Special case, if there is only one request of this type, then just use that
            //instead of creating another one with this as its source
            if (sourcesForType.size() == 1)
            {
                combinedRequests.put(type, sourcesForType.get(0));
            }
            else
            {
                ReindexRequest combinedRequest = new ReindexRequest(null, type,
                        lowestRequestTime(sourcesForType), null, null, null, combinedStatus(sourcesForType),
                        groupedAffectedIndexes.get(type), groupedSharedEntityTypes.get(type),
                        sourcesForType);

                combinedRequests.put(type, combinedRequest);
            }
        }

        //We now have combined requests, one for each type

        //Go through all the request types, if any lower ones have operations that are also performed in higher ones
        //then eliminate them (e.g. IMMEDIATE task reindexes issues, so remove this from the DELAYED tasks)

        List<ReindexRequest> furtherCombinedRequests = eliminateAndCombineTree(combinedRequests.values()); //In enum natural order, IMMEDIATE comes before delayed

        return ImmutableList.copyOf(furtherCombinedRequests);

    }

    private List<ReindexRequest> eliminateAndCombineTree(Collection<ReindexRequest> oneRequestPerType)
    {
        List<ReindexRequest> combinedRequests = new ArrayList<ReindexRequest>();

        Set<AffectedIndex> affectedIndexesProcessed = EnumSet.noneOf(AffectedIndex.class);
        Set<SharedEntityType> sharedEntityTypesProcessed = EnumSet.noneOf(SharedEntityType.class);
        ReindexRequest requestFromPreviousType = null;
        for (ReindexRequest request : oneRequestPerType)
        {
            Set<AffectedIndex> usefulAffectedIndexesForRequest = safeCopyOf(request.getAffectedIndexes(), AffectedIndex.class);
            usefulAffectedIndexesForRequest.removeAll(affectedIndexesProcessed);
            Set<SharedEntityType> usefulSharedEntityTypesForRequest = safeCopyOf(request.getSharedEntities(), SharedEntityType.class);
            usefulSharedEntityTypesForRequest.removeAll(sharedEntityTypesProcessed);

            if (!usefulAffectedIndexesForRequest.isEmpty() || !usefulSharedEntityTypesForRequest.isEmpty())
            {
                //If the request is synthetic, then don't double-wrap sources
                if (request.getId() == null)
                {
                    //There might be sources that have too much work, so trim them down if needed
                    List<ReindexRequest> newSources = new ArrayList<ReindexRequest>(request.getSources().size());
                    for (ReindexRequest oldSource : request.getSources())
                    {
                        Set<AffectedIndex> usefulAffectedIndexesInOldSource = safeCopyOf(oldSource.getAffectedIndexes(), AffectedIndex.class);
                        usefulAffectedIndexesInOldSource.retainAll(usefulAffectedIndexesForRequest);
                        Set<SharedEntityType> usefulSharedEntityTypesInOldSource = safeCopyOf(oldSource.getSharedEntities(), SharedEntityType.class);
                        usefulSharedEntityTypesInOldSource.retainAll(usefulSharedEntityTypesForRequest);

                        ReindexRequest newSource = new ReindexRequest(oldSource.getId(), oldSource.getType(), oldSource.getRequestTime(), oldSource.getStartTime(), oldSource.getCompletionTime(), oldSource.getExecutionNodeId(), oldSource.getStatus(), usefulAffectedIndexesInOldSource, usefulSharedEntityTypesInOldSource, oldSource.getSources());
                        newSources.add(newSource);
                    }

                    //Request is already synthetic, don't re-wrap
                    request = new ReindexRequest(null, request.getType(), request.getRequestTime(), request.getStartTime(), request.getCompletionTime(), request.getExecutionNodeId(), request.getStatus(), usefulAffectedIndexesForRequest, usefulSharedEntityTypesForRequest, newSources);
                }
                //Special case - if there are no changes to the request at all then leave it be
                else if (request.getSources().isEmpty() && usefulAffectedIndexesForRequest.equals(request.getAffectedIndexes()) && usefulSharedEntityTypesForRequest.equals(request.getSharedEntities()))
                {
                    //Nothing needed to do here, just use the original request
                }
                //Non-synthetic request with changes - wrap original request with synthetic one
                else
                {
                    //No. update original request with changes
                    request = new ReindexRequest(request.getId(), request.getType(), request.getRequestTime(), request.getStartTime(), request.getCompletionTime(), request.getExecutionNodeId(), request.getStatus(), usefulAffectedIndexesForRequest, usefulSharedEntityTypesForRequest, request.getSources());

                }
                combinedRequests.add(request);
                requestFromPreviousType = request;
            }
            else
            {
                List<ReindexRequest> sources = request.getSources();
                if (sources.isEmpty())
                {
                    sources = Collections.singletonList(request);
                }

                //If requestFromPreviousType is not synthetic, turn it into synthetic with one source
                if (requestFromPreviousType.getId() != null)
                {
                    List<ReindexRequest> newSources = ImmutableList.<ReindexRequest>builder().add(requestFromPreviousType).addAll(sources).build();
                    ReindexRequest updatedRequestFromPreviousType = new ReindexRequest(null, requestFromPreviousType.getType(), requestFromPreviousType.getRequestTime(), requestFromPreviousType.getStartTime(), requestFromPreviousType.getCompletionTime(), requestFromPreviousType.getExecutionNodeId(), requestFromPreviousType.getStatus(), requestFromPreviousType.getAffectedIndexes(), requestFromPreviousType.getSharedEntities(), newSources);
                    combinedRequests.set(combinedRequests.indexOf(requestFromPreviousType), updatedRequestFromPreviousType);
                }
                else
                {
                    //Otherwise just update the sources
                    combinedRequests.set(combinedRequests.indexOf(requestFromPreviousType), requestFromPreviousType.withSources(sources));
                }
            }
            affectedIndexesProcessed.addAll(usefulAffectedIndexesForRequest);
            sharedEntityTypesProcessed.addAll(usefulSharedEntityTypesForRequest);
        }

        return combinedRequests;
    }

    /**
     * Create a safe copy of an enum set, even if it is empty.
     * <p>
     *
     * Blindly using {@link java.util.EnumSet#copyOf(java.util.Collection)} can be problematic if the input set is
     * empty (it throws IllegalArgumentException).  This method checks this first and does an alternative copy so that
     * you always get a copy no matter if the set is empty or not.
     *
     * @param set the set to copy.
     * @param type the type of enum in the set.  Required due to generics erasure.
     * @param <E> the type of enum in the set.
     *
     * @return a copy of the set.
     */
    @Nonnull
    private static <E extends Enum<E>> EnumSet<E> safeCopyOf(@Nonnull Set<E> set, @Nonnull Class<E> type)
    {
        //If the set is a real enumset it is always safe to copy, even if empty.
        if (set instanceof EnumSet)
        {
            return EnumSet.copyOf((EnumSet<E>) set);
        }
        //Otherwise if it's empty, make a new empty enumset
        else if (set.isEmpty())
        {
            return EnumSet.noneOf(type);
        }
        //Not empty, so it's safe to use EnumSet.copyOf()
        else
        {
            return EnumSet.copyOf(set);
        }
    }

    private long lowestRequestTime(@Nonnull Iterable<ReindexRequest> requests)
    {
        long time = Long.MAX_VALUE;
        for (ReindexRequest request : requests)
        {
            time = Math.min(time, request.getRequestTime());
        }
        return time;
    }

    /**
     * Pick the lowest status from all the requests.
     */
    @Nonnull
    private ReindexStatus combinedStatus(@Nonnull Iterable<ReindexRequest> requests)
    {
        //Pick the last status as the default
        ReindexStatus combinedStatus = ReindexStatus.values()[ReindexStatus.values().length - 1];

        for (ReindexRequest request : requests)
        {
            ReindexStatus curStatus = request.getStatus();
            if (curStatus.compareTo(combinedStatus) < 0)
            {
                combinedStatus = curStatus;
            }
        }

        return combinedStatus;
    }

    @Nonnull
    private List<ReindexRequest> normalizeRequests(@Nonnull List<ReindexRequest> requests)
    {
        //Don't modify original list passed in
        final List<ReindexRequest> requestList = new ArrayList<ReindexRequest>(requests.size());

        for (ReindexRequest request : requests)
        {
            requestList.add(normalizeRequest(request));
        }

        return requestList;
    }

    @Nonnull
    private ReindexRequest normalizeRequest(@Nonnull ReindexRequest request)
    {
        Set<SharedEntityType> sharedEntityTypes = normalizeSharedEntityTypes(request.getSharedEntities());
        Set<AffectedIndex> affectedIndexes = normalizeAffectedIndexes(request.getAffectedIndexes(), sharedEntityTypes);

        return new ReindexRequest(request.getId(), request.getType(), request.getRequestTime(),
                        request.getStartTime(), request.getCompletionTime(), request.getExecutionNodeId(), request.getStatus(),
                        affectedIndexes, sharedEntityTypes, request.getSources());
    }

    @Nonnull
    private Set<AffectedIndex> normalizeAffectedIndexes(@Nonnull Set<AffectedIndex> affectedIndexes, @Nonnull Set<SharedEntityType> sharedEntityTypes)
    {
        if (affectedIndexes.contains(AffectedIndex.ALL))
        {
            affectedIndexes = EnumSet.complementOf(EnumSet.of(AffectedIndex.ALL));
        }
        else if (!affectedIndexes.isEmpty())
        {
            affectedIndexes = EnumSet.copyOf(affectedIndexes);
        }
        else //Can't use EnumSet.copyOf() with empty set
        {
            affectedIndexes = EnumSet.noneOf(AffectedIndex.class);
        }

        //Ensure shared entity element exists appropriately depending on shared entity set
        if (sharedEntityTypes.isEmpty())
        {
            affectedIndexes.remove(AffectedIndex.SHAREDENTITY);
        }
        else
        {
            affectedIndexes.add(AffectedIndex.SHAREDENTITY);
        }

        return affectedIndexes;
    }

    @Nonnull
    private Set<SharedEntityType> normalizeSharedEntityTypes(@Nonnull Set<SharedEntityType> sharedEntityTypes)
    {
        if (sharedEntityTypes.contains(SharedEntityType.NONE))
        {
            return EnumSet.noneOf(SharedEntityType.class);
        }

        return sharedEntityTypes;
    }
}
