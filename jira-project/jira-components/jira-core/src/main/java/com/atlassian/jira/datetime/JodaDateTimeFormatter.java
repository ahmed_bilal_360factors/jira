package com.atlassian.jira.datetime;

import org.joda.time.*;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormatter;

import java.util.Date;
import java.util.Locale;

/**
 * Base class for formatter strategies that just delegate to JodaTime's formatter.
 *
 * @since 4.4
 */
class JodaDateTimeFormatter implements DateTimeFormatStrategy
{
    private final DateTimeFormatterServiceProvider serviceProvider;
    private final JodaFormatterSupplier cache;
    private final String patternPropertyName;
    private final DateTimeStyle style;

    protected JodaDateTimeFormatter(DateTimeFormatterServiceProvider serviceProvider, JodaFormatterSupplier cache, String patternPropertyName, DateTimeStyle style)
    {
        this.serviceProvider = serviceProvider;
        this.cache = cache;
        this.patternPropertyName = patternPropertyName;
        this.style = style;
    }

    @Override
    public String format(DateTime dateTime, Locale locale)
    {
        DateTimeFormatter formatter = cache.get(new JodaFormatterSupplier.Key(pattern(), locale));

        return formatter.print(dateTime);
    }

    @Override
    public Date parse(String text, DateTimeZone timeZone, Locale locale)
    {
        DateTimeFormatter formatter = cache.get(new JodaFormatterSupplier.Key(pattern(), locale)).withZone(timeZone);

        // We convert to a local time first to avoid weirdness around Daylight Savings cut over times, esp
        // in Time Zones where the DST cut over is at midnight.  See https://jira.atlassian.com/browse/JRA-22582
        LocalDateTime localDateTime = formatter.parseLocalDateTime(text);
        return localDateTime.toDate(timeZone.toTimeZone());
    }

    @Override
    public DateTimeStyle style()
    {
        return style;
    }

    @Override
    public String pattern()
    {
        return serviceProvider.getDefaultBackedString(patternPropertyName);
    }
}
