/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

package com.atlassian.jira.upgrade;

import java.util.Collection;

import javax.annotation.Nullable;

import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.issue.index.IssueIndexingParams;

public interface UpgradeTask
{
    /**
     * @return  The build number that this upgrade is applicable to
     */
    public String getBuildNumber();

    /**
     * A short (<50 chars) description of the upgrade action
     */
    public String getShortDescription();

    /**
     * Perform the upgrade.
     * @param setupMode Indicating this upgrade task is running during set up.
     */
    public void doUpgrade(boolean setupMode) throws Exception;

    /**
     * Return the Upgrade task id of another upgrade task that must be run prior to this task.
     * @since v6.4
     */
    @Nullable
    public String dependsUpon();

    /**
     * Return when this upgrade task can be scheduled.
     * @since v6.4
     */
    public ScheduleOption getScheduleOption();

    /**
     * Return any errors that occur.  Each entry is a string.
     */
    public Collection<String> getErrors();

    /**
     * Track status of a task this session, if isTaskDone(String) returns true you don't need to do it again. 
     */
    public class Status
    {
        private static JiraProperties jiraSystemProperties = JiraSystemProperties.getInstance();

        public static void setTaskDone(final String taskId)
        {
            jiraSystemProperties.setProperty(asPropertyName(taskId), "true");
        }
        
        public static boolean isTaskDone(final String taskId)
        {
            return jiraSystemProperties.getProperty(asPropertyName(taskId)) != null;
        }

        private static String asPropertyName(final String taskId)
        {
            return "jira.task." + taskId + ".complete";
        }
    }

    enum ScheduleOption
    {
        /** The upgrade task must be run during the JIRA startup process and prior to JIRA being available for use. */
        BEFORE_JIRA_STARTED,
        /** The upgrade task may be run after the JIRA startup process is complete and while JIRA is available for use. */
        AFTER_JIRA_STARTED
    }
}