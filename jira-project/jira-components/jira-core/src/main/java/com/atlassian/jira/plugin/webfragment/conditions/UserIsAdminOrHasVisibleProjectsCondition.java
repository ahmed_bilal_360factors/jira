package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.base.Supplier;

import static com.atlassian.jira.plugin.webfragment.conditions.RequestCachingConditionHelper.cacheConditionResultInRequest;

/**
 * Checks if the user is an admin or can see atleast one project with this {@link AbstractPermissionCondition#permission}
 */
public class UserIsAdminOrHasVisibleProjectsCondition extends AbstractPermissionCondition
{
    private final UserIsAdminCondition isAdminCondition;

    public UserIsAdminOrHasVisibleProjectsCondition(PermissionManager permissionManager)
    {
        super(permissionManager);
        isAdminCondition = new UserIsAdminCondition(permissionManager);
    }

    public boolean shouldDisplay(final ApplicationUser user, JiraHelper jiraHelper)
    {
        return isAdminCondition.shouldDisplay(user, jiraHelper)
                || cacheConditionResultInRequest(getHasProjectsKey(permission, user), new Supplier<Boolean>()
        {
            @Override
            public Boolean get()
            {
                return permissionManager.hasProjects(new ProjectPermissionKey(permission), user);
            }
        });
    }
}