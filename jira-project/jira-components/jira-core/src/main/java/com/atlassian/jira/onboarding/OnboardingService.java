package com.atlassian.jira.onboarding;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.jira.user.ApplicationUser;

public interface OnboardingService
{
    static final String DARK_FEATURE_DISABLE_ONBOARDING_FLAG = "jira.onboarding.feature.disabled";
    static final String DARK_FEATURE_DISABLE_USER_CHECKS_FLAG = "jira.onboarding.user.check.disabled";

    /**
     * Checks whether the user should do an onboarding flow for the product. Returns the appropriate onboarding flow to
     * put the user through (if any). Additionally stores a resolved user property when the onboarding was evaluated and
     * skipped for the current user
     *
     * @return a {@link FirstUseFlow} if the user is eligible for one (and hasn't done one before), null otherwise.
     */
    @Nullable
    FirstUseFlow getFirstUseFlow(@Nullable final ApplicationUser user);

    /**
     * Get the key of the flow that the user is completing.
     *
     * @param user to check against
     * @return key or null if not doing a flow
     */
    @Nullable
    String getStartedFirstUseFlowKey(@Nonnull final ApplicationUser user);

    /**
     * @param user to check against
     * @return whether a user has already completed a first user flow.
     */
    boolean hasCompletedFirstUseFlow(@Nonnull final ApplicationUser user);

    /**
     * Mark the user as completing their first user flow
     *
     * @param user that the first flow has been completed for
     */
    void completeFirstUseFlow(@Nonnull final ApplicationUser user);

    /**
     * Assign the user as beginning a current sequence in the flow
     *
     * @param user that is completing the flow
     * @param sequenceKey of the sequence being started
     */
    void setCurrentFirstUseFlowSequence(@Nonnull final ApplicationUser user, @Nonnull final String sequenceKey);

    /**
     * Get the current sequence key for the flow that the user is completing.
     *
     * @param user that is completing flow
     * @return sequence key of flow
     */
    @Nullable
    String getCurrentFirstUseFlowSequence(@Nonnull final ApplicationUser user);

}
