package com.atlassian.jira.web.action.setup;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.util.FileFactory;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.web.HttpServletVariables;
import com.atlassian.jira.web.action.ActionViewData;
import com.atlassian.license.SIDManager;

import org.apache.commons.lang.StringUtils;

public class SetupAccount extends AbstractSetupAction
{
    private static final String SETUP_INSTANT = "instant";

    private String setupOption = "classic";

    private final SetupSharedVariables sharedVariables;
    private final SIDManager sidManager;

    public SetupAccount(final FileFactory fileFactory, final JiraProperties jiraProperties,
            final SetupSharedVariables sharedVariables, final SIDManager sidManager)
    {
        super(fileFactory, jiraProperties);
        this.sidManager = sidManager;
        this.sharedVariables = sharedVariables;
    }

    @Override
    public String doDefault() throws Exception
    {
        if (setupAlready())
        {
            return SETUP_ALREADY;
        }

        return INPUT;
    }

    @Override
    public String doExecute() throws Exception
    {
        if (setupAlready())
        {
            return SETUP_ALREADY;
        }

        if (setupOption.equals(SETUP_INSTANT))
        {
            return forceRedirect("SetupFinishing!default.jspa");
        }
        else //classic
        {
            return forceRedirect("SetupDatabase!default.jspa");
        }
    }

    public String getSetupOption()
    {
        return setupOption;
    }

    public void setSetupOption(final String setupOption)
    {
        this.setupOption = setupOption;
    }

    @Override
    public String getServerId()
    {
        String serverId = getApplicationProperties().getString(APKeys.JIRA_SID);

        if (StringUtils.isBlank(serverId))
        {
            serverId = sidManager.generateSID();
            getApplicationProperties().setString(APKeys.JIRA_SID, serverId);
        }

        return serverId;
    }

    @ActionViewData
    public String getSelectedBundle()
    {
        return sharedVariables.getSelectedBundle();
    }

    @ActionViewData
    public String getSelectedBundleName()
    {
        return sharedVariables.getSelectedBundleName();
    }

    @ActionViewData
    public String getErrorTextsJson() throws JSONException
    {
        final I18nHelper i18nHelper = ComponentAccessor.getI18nHelperFactory().getInstance(getLocale());
        final JSONObject json = new JSONObject();

        json.put("invalidEmail", i18nHelper.getText("setup.account.form.email.invalid"));
        json.put("emailRequired", i18nHelper.getText("setup.account.form.email.required"));
        json.put("invalidCredentials", i18nHelper.getText("setup.account.invalid.credentials"));
        json.put("passwordRequired", i18nHelper.getText("setup.account.form.password.required"));
        json.put("agreementRequired", i18nHelper.getText("setup.account.form.agreement.required"));
        json.put("fullnameRequired", i18nHelper.getText("setup.account.form.fullname.required"));

        return json.toString();
    }
}
