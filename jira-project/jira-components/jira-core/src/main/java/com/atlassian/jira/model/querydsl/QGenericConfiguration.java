package com.atlassian.jira.model.querydsl;

import com.mysema.query.sql.ColumnMetadata;
import com.mysema.query.types.path.*;

import java.sql.Types;
import javax.annotation.Generated;

/**
 * QGenericConfiguration is a Querydsl query object.
 *
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QGenericConfiguration extends JiraRelationalPathBase<GenericConfigurationDTO>
{
    public static final QGenericConfiguration GENERIC_CONFIGURATION = new QGenericConfiguration("GENERIC_CONFIGURATION");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final StringPath datatype = createString("datatype");
    public final StringPath datakey = createString("datakey");
    public final StringPath xmlvalue = createString("xmlvalue");

    public QGenericConfiguration(String alias)
    {
        super(GenericConfigurationDTO.class, alias, "genericconfiguration");
        addMetadata();
    }

    private void addMetadata()
    {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(datatype, ColumnMetadata.named("datatype").withIndex(2).ofType(Types.VARCHAR).withSize(60));
        addMetadata(datakey, ColumnMetadata.named("datakey").withIndex(3).ofType(Types.VARCHAR).withSize(60));
        addMetadata(xmlvalue, ColumnMetadata.named("xmlvalue").withIndex(4).ofType(Types.VARCHAR).withSize(4000));
    }
}

