package com.atlassian.jira.plugin.webresource;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.gadgets.event.ClearSpecCacheEvent;
import com.atlassian.jira.InitializingComponent;
import com.atlassian.jira.config.FeatureEvent;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.i18n.CachingI18nFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Locale;
import java.util.Map;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * The implementation of the {@link com.atlassian.plugin.webresource.WebResourceIntegration} for JIRA.
 */
public class JiraWebResourceIntegration implements WebResourceIntegration, InitializingComponent
{
    static final String CDN_DISABLED_FEATURE_KEY = "jira.cdn.disabled";

    private final PluginAccessor pluginAccessor;
    private final ApplicationProperties applicationProperties;
    private final VelocityRequestContextFactory requestContextFactory;
    private final BuildUtilsInfo buildUtilsInfo;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final CachingI18nFactory i18nFactory;
    private final JiraHome jiraHome;
    private final EventPublisher eventPublisher;
    private final FeatureManager featureManager;

    public JiraWebResourceIntegration(
            final PluginAccessor pluginAccessor, final ApplicationProperties applicationProperties,
            final VelocityRequestContextFactory requestContextFactory, final BuildUtilsInfo buildUtilsInfo,
            final JiraAuthenticationContext jiraAuthenticationContext, CachingI18nFactory i18nFactory, JiraHome jiraHome,
            final EventPublisher eventPublisher, final FeatureManager featureManager)
    {
        this.i18nFactory = i18nFactory;
        this.pluginAccessor = notNull("pluginAccessor", pluginAccessor);
        this.applicationProperties = notNull("applicationProperties", applicationProperties);
        this.requestContextFactory = notNull("requestContextFactory", requestContextFactory);
        this.buildUtilsInfo = notNull("buildUtilsInfo", buildUtilsInfo);
        this.jiraAuthenticationContext = notNull("jiraAuthenticationContext", jiraAuthenticationContext);
        this.jiraHome = notNull("jiraHome", jiraHome);
        this.eventPublisher = eventPublisher;
        this.featureManager = featureManager;
    }

    @SuppressWarnings ( { "UnusedDeclaration" })
    @EventListener
    // If the superbatch is retrieved during plugin system startup / bootstrap, an incomplete set of dependencies may be cached (JRA-32692).
    // Increment the version of the superbatch once the plugin framework is done starting up to invalidate the cached superbatch.
    public void pluginFrameworkStarted(final PluginFrameworkStartedEvent event)
    {
        incrementSuperbatchVersion();
    }

    @EventListener
    public void onCdnDisabled(final FeatureEvent featureEvent)
    {
        if (featureEvent.feature().equals(CDN_DISABLED_FEATURE_KEY)
                || featureEvent.feature().equals(JiraPrefixCDNStrategy.ENABLED_FEATURE_KEY)
                || featureEvent.feature().equals(JiraBaseUrlCDNStrategy.ENABLED_FEATURE_KEY))
        {
            eventPublisher.publish(new ClearSpecCacheEvent());
        }
    }


    private void incrementSuperbatchVersion()
    {
        String versionString = getSuperBatchVersion();
        Long superbatchVersion = StringUtils.isNotEmpty(versionString) ? Long.parseLong(versionString) : 1L;
        applicationProperties.setString(APKeys.WEB_RESOURCE_SUPER_BATCH_FLUSH_COUNTER, Long.toString(superbatchVersion + 1));
    }

    public PluginAccessor getPluginAccessor()
    {
        return pluginAccessor;
    }

    public Map<String, Object> getRequestCache()
    {
        return JiraAuthenticationContextImpl.getRequestCache();
    }

    public String getSystemCounter()
    {
        return applicationProperties.getDefaultBackedString(APKeys.WEB_RESOURCE_FLUSH_COUNTER);
    }

    public String getSystemBuildNumber()
    {
        return buildUtilsInfo.getCurrentBuildNumber();
    }

    public String getBaseUrl()
    {
        return getBaseUrl(UrlMode.AUTO);
    }

    public String getBaseUrl(final UrlMode urlMode)
    {
        switch (urlMode)
        {
            case RELATIVE:
                // It's possible to want the base URL from outside an executing HTTP request (e.g. in a quartz scheduled
                // job). In this case we should fall through to UrlMode.AUTO
                HttpServletRequest httpServletRequest = ExecutingHttpRequest.get();
                if (httpServletRequest != null) {
                    return httpServletRequest.getContextPath();
                }
            case AUTO:
                // This may return a canonical URL, depending on the type of request being handled.
                return requestContextFactory.getJiraVelocityRequestContext().getBaseUrl();
            case ABSOLUTE:
                return requestContextFactory.getJiraVelocityRequestContext().getCanonicalBaseUrl();
            default:
                throw new AssertionError("Unsupported URLMode: " + urlMode);
        }
    }

    public String getSuperBatchVersion()
    {
        return applicationProperties.getDefaultBackedString(APKeys.WEB_RESOURCE_SUPER_BATCH_FLUSH_COUNTER);
    }

    @Override
    public String getStaticResourceLocale()
    {
        final I18nHelper i18n = jiraAuthenticationContext.getI18nHelper();
        return i18n.getLocale().toString() + i18nFactory.getStateHashCode();
    }

    @Nonnull
    @Override
    public File getTemporaryDirectory()
    {
        return new File(jiraHome.getLocalHome(), "tmp" + File.separator + "webresources");
    }

    @Override
    public void afterInstantiation() throws Exception
    {
        eventPublisher.register(this);
    }

    @Override
    public CDNStrategy getCDNStrategy()
    {
        boolean disabled = featureManager.isEnabled(CDN_DISABLED_FEATURE_KEY);
        if (!disabled)
        {
            if (featureManager.isEnabled(JiraBaseUrlCDNStrategy.ENABLED_FEATURE_KEY))
            {
                return new JiraBaseUrlCDNStrategy(applicationProperties);
            }
            if (featureManager.isEnabled(JiraPrefixCDNStrategy.ENABLED_FEATURE_KEY))
            {
                return new JiraPrefixCDNStrategy();
            }
        }
        return null;
    }

    @Override
    public Locale getLocale()
    {
        return jiraAuthenticationContext.getLocale();
    }

    @Override
    public String getI18nRawText(Locale locale, String key)
    {
        return i18nFactory.getInstance(locale).getUnescapedText(key);
    }

    @Override
    public String getI18nText(Locale locale, String key)
    {
        return i18nFactory.getInstance(locale).getText(key);
    }
}
