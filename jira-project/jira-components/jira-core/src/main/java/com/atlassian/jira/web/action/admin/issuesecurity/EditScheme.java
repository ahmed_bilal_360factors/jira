/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

package com.atlassian.jira.web.action.admin.issuesecurity;

import java.util.Map;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.scheme.AbstractEditScheme;
import com.atlassian.jira.scheme.SchemeManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.util.JiraEntityUtils;
import com.atlassian.sal.api.websudo.WebSudoRequired;

import org.ofbiz.core.entity.GenericValue;

@WebSudoRequired
public class EditScheme extends AbstractEditScheme
{
    private Long defaultLevel;

    public SchemeManager getSchemeManager()
    {
        return ComponentAccessor.getComponent(IssueSecuritySchemeManager.class);
    }

    public String getRedirectURL()
    {
        return "ViewIssueSecuritySchemes.jspa";
    }

    public String doDefault() throws Exception
    {
        setDefaultLevel(getScheme().getLong("defaultlevel"));
        return super.doDefault();
    }

    @RequiresXsrfCheck
    protected String doExecute() throws Exception
    {
        GenericValue updatedScheme = getScheme();
        updatedScheme.setString("name", getName());
        updatedScheme.setString("description", getDescription());
        updatedScheme.set("defaultlevel", getDefaultLevel());

        ComponentAccessor.getComponent(IssueSecuritySchemeManager.class).updateScheme(updatedScheme);

        return getRedirect(getRedirectURL());
    }

    public Long getDefaultLevel()
    {
        return defaultLevel;
    }

    public void setDefaultLevel(Long defaultLevel)
    {
        if (defaultLevel == null || defaultLevel.equals(new Long(-1)))
            this.defaultLevel = null;
        else
            this.defaultLevel = defaultLevel;
    }

    public Map getSecurityLevels()
    {
        return JiraEntityUtils.createEntityMap(ComponentAccessor.getIssueSecurityLevelManager().getSchemeIssueSecurityLevels(getSchemeId()), "id", "name");
    }
}
