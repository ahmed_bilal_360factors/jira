package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserIssueHistoryManager;

/**
 * Checks if there are any history issue's
 */
public class UserHasIssueHistoryCondition extends AbstractWebCondition
{
    final private UserIssueHistoryManager userHistoryManager;

    public UserHasIssueHistoryCondition(UserIssueHistoryManager userHistoryManager)
    {
        this.userHistoryManager = userHistoryManager;
    }

    public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper)
    {
        return userHistoryManager.hasIssueHistory(user);
    }
}
