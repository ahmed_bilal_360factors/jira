package com.atlassian.jira.bc.workflow;

import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.workflow.condition.PermissionCondition;
import com.google.common.annotations.VisibleForTesting;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.loader.ConditionDescriptor;
import com.opensymphony.workflow.loader.DescriptorFactory;

import java.util.Map;

import static com.atlassian.jira.util.collect.MapBuilder.build;

public class DefaultWorkflowConditionDescriptorFactory implements WorkflowConditionDescriptorFactory
{
    @VisibleForTesting static final String CLASS_NAME_KEY = "class.name";
    @VisibleForTesting static final String PERMISSION_KEY = "permission";

    @SuppressWarnings("unchecked")
    private ConditionDescriptor makeConditionDescriptor(Class<? extends Condition> clazz, Map<String, String> params)
    {
        ConditionDescriptor conditionDescriptor = DescriptorFactory.getFactory().createConditionDescriptor();
        conditionDescriptor.setType("class");
        final Map conditionArgs = conditionDescriptor.getArgs();

        conditionArgs.put(CLASS_NAME_KEY, clazz.getName());
        conditionArgs.putAll(params);
        return conditionDescriptor;
    }

    @Override
    public ConditionDescriptor permission(ProjectPermissionKey key)
    {
        return makeConditionDescriptor(PermissionCondition.class, build(
                PERMISSION_KEY, key.permissionKey()
        ));
    }
}
