package com.atlassian.jira.issue.index;

import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;

import com.atlassian.jira.config.util.IndexingConfiguration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.lucene.search.IndexSearcher;

import static com.atlassian.jira.issue.index.IndexDirectoryFactory.Name;

/**
 * This class manages the searcher thread local cache.  The actual searchers themselves are
 * stored in this object, which is stored in a {@link ThreadLocal}.
 */
class SearcherCache
{
    private static final Logger log = LoggerFactory.getLogger(DefaultIndexManager.class);

    private static final ThreadLocal<SearcherCache> THREAD_LOCAL = new ThreadLocal<SearcherCache>();

    static SearcherCache getThreadLocalCache()
    {
        SearcherCache threadLocalSearcherCache = THREAD_LOCAL.get();
        if (threadLocalSearcherCache == null)
        {
            threadLocalSearcherCache = new SearcherCache();
            THREAD_LOCAL.set(threadLocalSearcherCache);
        }
        return threadLocalSearcherCache;
    }


    private Map<Name, IndexSearcher> entitySearchers = new EnumMap<Name, IndexSearcher>(Name.class);

    IndexSearcher retrieveEntitySearcher(final IssueIndexer issueIndexer, final IndexingConfiguration indexingConfiguration, final Name index)
    {
        if (!entitySearchers.containsKey(index))
        {
            try
            {
                entitySearchers.put(index, issueIndexer.openEntitySearcher(index));
            }
            catch (final RuntimeException e)
            {
                throw new SearchUnavailableException(e, indexingConfiguration.isIndexAvailable());
            }
        }

        return entitySearchers.get(index);
    }

    /**
     * Close the issues and comments searchers.
     */
    void closeSearchers()
    {
        for (Map.Entry<Name, IndexSearcher> searcher : entitySearchers.entrySet())
        {
            try
            {
                closeSearcher(searcher.getValue());
            }
            catch (Exception e)
            {
                log.error("Error while resetting searcher: " + searcher.getKey() + " message: " + e.getMessage(), e);
            }
        }
        entitySearchers = new EnumMap<Name, IndexSearcher>(Name.class);
    }

    private void closeSearcher(final IndexSearcher searcher) throws IOException
    {
        if (searcher != null)
        {
            searcher.close();
        }
    }
}
