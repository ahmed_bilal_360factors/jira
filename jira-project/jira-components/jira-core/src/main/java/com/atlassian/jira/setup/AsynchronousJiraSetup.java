package com.atlassian.jira.setup;

import com.atlassian.fugue.Option;
import com.atlassian.jira.util.thread.JiraThreadLocalUtils;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.Multimaps;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

import static com.atlassian.fugue.Iterables.first;

/**
 * This class allows running final setup steps during instant setup in separate thread. Also it allows to check progress
 * across setup steps from other thread in a thread safe way using AsynchronousJiraSetup.waitForStepToEnd() Actual tasks
 * that has to be performed in order to setup JIRA are delegated to SetupStrategy.
 *
 * @since v6.4
 */
public class AsynchronousJiraSetup<ParametersT, StepT>
{
    private static final Logger log = Logger.getLogger(AsynchronousJiraSetup.class);

    private volatile boolean bundleHasLicenseError = false;

    public boolean isBundleHasLicenseError()
    {
        return bundleHasLicenseError;
    }

    private final SetupStrategy<ParametersT, StepT> setupStrategy;

    private final Object lock = new Object();

    // access protected with lock
    private String setupSessionId;
    // access protected with lock
    private Option<StepT> currentStep;
    // access protected with lock
    private final Map<StepT, SetupStrategy.Status> setupStatus;
    // access protected with lock
    private String setupError;

    AsynchronousJiraSetup(final SetupStrategy<ParametersT, StepT> setupStrategy)
    {
        this.setupStrategy = setupStrategy;
        setupStatus = new HashMap<StepT, SetupStrategy.Status>(setupStrategy.getInitialSteps());
        currentStep = getPendingStep(setupStatus);
        if (!currentStep.isDefined())
        {
            throw new RuntimeException("Strategy " + setupStrategy.getClass().getName() + " does not define pending step");
        }
    }

    private static <Step> Option<Step> getPendingStep(final Map<Step, SetupStrategy.Status> steps)
    {
        final ImmutableSetMultimap<SetupStrategy.Status, Step> stepsForStatuses = ImmutableSetMultimap.copyOf(Multimaps.forMap(steps)).inverse();
        return first(stepsForStatuses.get(SetupStrategy.Status.PENDING));
    }


    public boolean isSetupFinished(final String setupSessionId)
    {
        synchronized (lock)
        {
            if (setupSessionId != null && setupSessionId.equals(this.setupSessionId))
            {
                return !currentStep.isDefined();
            }
            else
            {
                throw new RuntimeException("This can be executed only from the session that initiated setup");
            }
        }
    }

    public SetupStatus<StepT> getStatusOnceStepIsDone(final String sessionId, final StepT step)
    {
        try
        {
            synchronized (lock)
            {
                if (sessionId == null || !sessionId.equals(setupSessionId))
                {
                    throw new RuntimeException("This can be executed only from the session that initiated setup");
                }

                while (currentStep.getOrNull() == step)
                {
                    lock.wait();
                }
                return new SetupStatus<StepT>(ImmutableMap.copyOf(setupStatus), setupError);
            }
        }
        catch (final InterruptedException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void setupJIRA(final String setupSessionId, final ParametersT parameters)
    {
        synchronized (lock)
        {
            if (this.setupSessionId != null)
            {
                throw new RuntimeException("Can be done only once");
            }
            this.setupSessionId = setupSessionId;
        }

        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                JiraThreadLocalUtils.preCall();
                try
                {
                    doSetupJIRA(parameters);
                }
                finally
                {
                    JiraThreadLocalUtils.postCall(log, null);
                }
            }
        }, "AsynchronousJiraSetup").start();
    }

    private void doSetupJIRA(final ParametersT parameters)
    {
        try
        {
            setupStrategy.setup(parameters, new StepSwitcher());
            synchronized (lock)
            {
                setupStatus.put(currentStep.get(), SetupStrategy.Status.SUCCESS);
                currentStep = Option.none();
                lock.notifyAll();
            }
        }
        catch (final Exception e)
        {
            log.error("Error during SetupFinishing", e);
            synchronized (lock)
            {
                setupStatus.put(currentStep.get(), SetupStrategy.Status.FAILURE);
                setupError = e.getMessage();
                currentStep = Option.none();
                lock.notifyAll();
            }
        }
    }

    private class StepSwitcher implements SetupStrategy.StepSwitcher<StepT>
    {

        @Override
        public void setBundleHasLicenseError(final boolean bundleHasLicenseError)
        {
            AsynchronousJiraSetup.this.bundleHasLicenseError = bundleHasLicenseError;
        }

        @Override
        public void withStep(final StepT step, final SetupStrategy.StepTask task) throws Exception
        {
            synchronized (lock)
            {
                setupStatus.put(currentStep.get(), SetupStrategy.Status.SUCCESS);
                currentStep = Option.some(step);
                setupStatus.put(step, SetupStrategy.Status.PENDING);
                lock.notifyAll();
            }
            task.run();
        }

        @Override
        public void setError(final String error)
        {
            synchronized (lock)
            {
                setupError = error;
            }
        }
    }

    public static final class SetupStatus<T>
    {
        private final ImmutableMap<T, SetupStrategy.Status> steps;
        private final String error;

        private SetupStatus(final ImmutableMap<T, SetupStrategy.Status> steps, final String error)
        {
            this.steps = steps;
            this.error = error;
        }

        public ImmutableMap<T, SetupStrategy.Status> getSteps()
        {
            return steps;
        }

        public String getError()
        {
            return error;
        }
    }
}
