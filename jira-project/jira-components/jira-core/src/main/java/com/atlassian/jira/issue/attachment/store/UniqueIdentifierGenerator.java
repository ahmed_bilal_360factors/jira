package com.atlassian.jira.issue.attachment.store;

import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.Nonnull;

import com.google.common.base.Joiner;

/**
 * Provides unique identifiers without persisting state, after application restart will start providing same set of identifiers
 * which are unique in single application run.
 *
 * @since v6.4
 */
public class UniqueIdentifierGenerator
{
    private final String prefix;
    private final String clusterNodeId;
    private final AtomicLong counter = new AtomicLong(0);

    public UniqueIdentifierGenerator(@Nonnull final String prefix, @Nonnull final String clusterNodeId)
    {
        this.prefix = prefix;
        this.clusterNodeId = clusterNodeId;
    }

    public String getNextId()
    {
        final long number = counter.getAndIncrement();
        return Joiner.on("-").join(prefix, clusterNodeId, number);
    }
}
