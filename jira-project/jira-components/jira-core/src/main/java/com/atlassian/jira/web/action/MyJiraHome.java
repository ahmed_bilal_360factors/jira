package com.atlassian.jira.web.action;

import com.atlassian.jira.onboarding.FirstUseFlow;
import com.atlassian.jira.onboarding.OnboardingService;
import com.atlassian.jira.plugin.myjirahome.MyJiraHomeLinker;
import com.atlassian.jira.security.login.LoginManagerImpl;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

/**
 * Redirects to the current My JIRA Home.
 *
 * @since 5.1
 */
public class MyJiraHome extends JiraWebActionSupport
{
    private final MyJiraHomeLinker myJiraHomeLinker;
    private final UserManager userManager;
    private final OnboardingService onboardingService;

    public MyJiraHome(@Nonnull final MyJiraHomeLinker myJiraHomeLinker, @Nonnull final UserManager userManager, @Nonnull final OnboardingService onboardingService)
    {
        this.myJiraHomeLinker = myJiraHomeLinker;
        this.userManager = userManager;
        this.onboardingService = onboardingService;
    }

    @Override
    protected String doExecute() throws Exception
    {
        return getRedirect(findMyHome());
    }

    private String findMyHome()
    {
        final ApplicationUser loggedInUser = getLoggedInApplicationUser();

        FirstUseFlow firstUseFlow = onboardingService.getFirstUseFlow(loggedInUser);
        if (firstUseFlow != null)
        {
            return firstUseFlow.getUrl();
        }

        if (loggedInUser != null)
        {
            return myJiraHomeLinker.getHomeLink(loggedInUser);
        }
        else if (isKnownButUnauthorised())
        {
            //If the user is authenticated but not authorised to access JIRA, send them to the default home
            return myJiraHomeLinker.getDefaultUserHome();
        }
        else
        {
            return myJiraHomeLinker.getHomeLink((ApplicationUser) null);
        }
    }

    private boolean isKnownButUnauthorised()
    {
        final HttpServletRequest httpRequest = getHttpRequest();
        if (Boolean.TRUE.equals(httpRequest.getAttribute(LoginManagerImpl.AUTHORISED_FAILURE)))
        {
            Object userKey = httpRequest.getAttribute(LoginManagerImpl.AUTHORISING_USER_KEY);
            if (userKey instanceof String)
            {
                String userKeyStr = (String) userKey;
                final ApplicationUser user = userManager.getUserByKey(userKeyStr);

                if (user != null)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
