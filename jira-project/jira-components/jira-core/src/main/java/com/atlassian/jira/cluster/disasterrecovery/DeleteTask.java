package com.atlassian.jira.cluster.disasterrecovery;

import java.io.File;

import com.atlassian.fugue.Option;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.config.util.SecondaryJiraHome;
import com.atlassian.jira.util.PathUtils;
import com.atlassian.jira.util.log.RateLimitingLogger;

/**
 * Deletes a file from the secondary location
 * @since v6.4
 */
class DeleteTask extends ReplicatorTask
{
    private static final RateLimitingLogger log = new RateLimitingLogger(DeleteTask.class);

    DeleteTask(final File file, final JiraHome jiraHome, final SecondaryJiraHome secondaryJiraHome)
    {
        super(file, jiraHome, secondaryJiraHome);
    }

    @Override
    public void run()
    {
        final Option<String> relativePath = getRelativePathOf(jiraHome, file);

        if (relativePath.isEmpty())
        {
            log.warn(" Attempting to replicate [" + file.getAbsolutePath() + "] failed because it is not in JIRA home");
            return;
        }

        final String secondaryFilePath = PathUtils.joinPaths(secondaryJiraHome.getHomePath(), relativePath.get());

        final File fileToDelete = new File(secondaryFilePath);

        if (fileToDelete.exists() && !fileToDelete.delete())
        {
            log.warn(" Unable to delete file [" + secondaryFilePath + "]");
        }
    }
}