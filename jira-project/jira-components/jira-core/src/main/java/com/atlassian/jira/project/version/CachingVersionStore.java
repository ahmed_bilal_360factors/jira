/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */
package com.atlassian.jira.project.version;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.cache.Supplier;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.entity.WithIdFunctions;
import com.atlassian.jira.event.ClearCacheEvent;
import com.google.common.base.Optional;
import com.google.common.primitives.Longs;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;

/**
 * A caching implementation of the VersionStore that relies on a delegate to do the DB operations.
 */
@EventComponent
public class CachingVersionStore implements VersionStore
{
    private final VersionStore delegate;

    private final Cache<Long, Optional<Version>> versionById;
    private final CachedReference<List<Version>> allVersions;
    private final Cache<String, List<Version>> versionsByName;
    private final Cache<Long, List<Version>> versionsByProjectId;

    public CachingVersionStore(final VersionStore delegate, final CacheManager cacheManager)
    {
        this.delegate = delegate;
        versionById = cacheManager.getCache(getCacheReferenceName("versionById"), new CacheLoader<Long, Optional<Version>>()
        {

            @Nonnull
            @Override
            public Optional<Version> load(@Nonnull final Long id)
            {
                return Optional.fromNullable(delegate.getVersion(id));
            }

        });
        allVersions = cacheManager.getCachedReference(getCacheReferenceName("allVersions"), new Supplier<List<Version>>()
        {

            @Override
            public List<Version> get()
            {
                return copyOf(delegate.getAllVersions());
            }

        });
        versionsByName = cacheManager.getCache(getCacheReferenceName("versionsByName"), new CacheLoader<String, List<Version>>()
        {
            @Nonnull
            @Override
            public List<Version> load(@Nonnull final String name)
            {
                return copyOf(delegate.getVersionsByName(name));
            }

        });
        versionsByProjectId = cacheManager.getCache(getCacheReferenceName("versionsByProjectId"), new CacheLoader<Long, List<Version>>()
        {
            @Nonnull
            @Override
            public List<Version> load(@Nonnull final Long projectId)
            {
                return copyOf(delegate.getVersionsByProject(projectId));
            }

        });
    }

    private String getCacheReferenceName(String name)
    {
        return CachingVersionStore.class.getCanonicalName() + ".cache." + name;
    }

    @EventListener
    @SuppressWarnings("unused")
    public void onClearCache(final ClearCacheEvent event)
    {
        refreshCache();
    }

    @Nonnull
    public Iterable<Version> getAllVersions()
    {
        return allVersions.get();
    }

    public Version getVersion(final Long id)
    {
        return id != null ? versionById.get(id).orNull() : null;
    }

    @Nonnull
    public Version createVersion(@Nonnull final Map<String, Object> versionParams)
    {
        // We can call through to the delegate to make the DB changes in multi-threaded mode.
        final Version version = delegate.createVersion(versionParams);
        // Now we force a cache refresh - this is synchronised with other writers, but readers can still get the old
        // cache up until the new cache is fully populated, and swapped into memory.
        refreshCache(version.getId());
        return version;
    }

    public void storeVersion(@Nonnull final Version version)
    {
        delegate.storeVersion(version);
        refreshCache(version.getId());
    }

    public void storeVersions(@Nonnull final Collection<Version> versions)
    {
        delegate.storeVersions(versions);
        refreshCache(Longs.toArray(copyOf(transform(versions, WithIdFunctions.getId()))));
    }

    public void deleteVersion(@Nonnull final Version version)
    {
        long versionId = version.getId();
        delegate.deleteVersion(version);
        refreshCache(versionId);
    }


    @Nonnull
    @Override
    public Iterable<Version> getVersionsByName(final String name)
    {
        List<Version> result = null;
        if (name != null)
        {
            result = versionsByName.get(name);
        }
        return result != null ?  result : Collections.<Version> emptyList();
    }

    @Nonnull
    @Override
    public Iterable<Version> getVersionsByProject(final Long projectId)
    {
        List<Version> result = null;
        if (projectId != null)
        {
            result = versionsByProjectId.get(projectId);
        }
        return result != null ?  result : Collections.<Version> emptyList();
    }

    private void refreshCache(long... versionIds)
    {
        if (versionIds.length > 0)
        {
            for (long versionId : versionIds)
            {
                versionById.remove(versionId);
            }
        }
        else
        {
            versionById.removeAll();
        }
        allVersions.reset();
        versionsByName.removeAll();
        versionsByProjectId.removeAll();
    }
}
