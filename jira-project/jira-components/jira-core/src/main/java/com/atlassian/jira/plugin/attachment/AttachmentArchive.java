package com.atlassian.jira.plugin.attachment;

import java.util.List;

/**
 * This interface represents entries of an attachment archive. Empty archives are indistinguishable from corrupt ones.
 * <p/>
 * The interface was previously inside {@link com.atlassian.jira.issue.attachment.AttachmentZipKit}
 *
 * @since v6.4
 */
public interface AttachmentArchive
{

    /**
     * @return total number of entries available (can be larger that what was asked for)
     * @deprecated {@link #getTotalEntryCount() getTotalEntryCount} instead. Since v6.4.
     */
    @Deprecated
    public int getTotalNumberOfEntriesAvailable();

    /**
     * @return total number of entries available (can be larger that what was asked for)
     */
    public int getTotalEntryCount();

    /**
     * @return the list of {@link AttachmentArchiveEntry}
     */
    public List<AttachmentArchiveEntry> getEntries();

    /**
     * @return true if there are more entries available in the zip than asked for.
     * @deprecated Compare {@link #getTotalEntryCount()} with size of {@link #getEntries()} instead. Since v6.4.
     */
    @Deprecated
    public boolean isMoreAvailable();

}
