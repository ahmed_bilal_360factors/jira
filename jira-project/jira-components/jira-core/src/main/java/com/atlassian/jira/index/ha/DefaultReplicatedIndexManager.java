package com.atlassian.jira.index.ha;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import javax.annotation.Nonnull;

import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.cluster.ClusterNodeProperties;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.IndexTaskContext;
import com.atlassian.jira.entity.WithId;
import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.SharedEntityType;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.index.ReindexAllCompletedEvent;
import com.atlassian.jira.issue.index.ReindexAllStartedEvent;
import com.atlassian.jira.issue.util.IssuesIterable;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.sharing.SharedEntity;
import com.atlassian.jira.task.TaskDescriptor;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.util.Consumer;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Manages the replicated index.
 *
 * @since v6.1
 */
@EventComponent
public class DefaultReplicatedIndexManager implements ReplicatedIndexManager
{
    private static final Logger log = LoggerFactory.getLogger(DefaultReplicatedIndexManager.class);

    private final OfBizReplicatedIndexOperationStore ofBizReplicatedIndexOperationStore;
    private final TaskManager taskManager;

    public DefaultReplicatedIndexManager(final OfBizReplicatedIndexOperationStore ofBizReplicatedIndexOperationStore, final TaskManager taskManager)
    {
        this.ofBizReplicatedIndexOperationStore = ofBizReplicatedIndexOperationStore;
        this.taskManager = taskManager;
    }

    /**
     *  Reindexes the set of provided issues in the replicated index.
     *
     * @param issuesIterable  an {@link IssuesIterable} that iterates obver the issues to be reindexed
     */
    @Override
    public void reindexIssues(@Nonnull final IssuesIterable issuesIterable)
    {
        try
        {
            final Set<Long> issueIds = Sets.newHashSet();
            issuesIterable.foreach(new Consumer<Issue>()
            {
                @Override
                public void consume(@Nonnull Issue issue)
                {
                    issueIds.add(issue.getId());
                }
            });
            updateReplicatedIndex(issueIds, AffectedIndex.ISSUE,
                    SharedEntityType.NONE, ReplicatedIndexOperation.Operation.UPDATE);
        }
        catch (Exception e)
        {
            log.error(buildErrorMessage("reindexed issues"), e);
        }
    }

    /**
     *  Reindexes the set of provided comments in the replicated index.
     *
     * @param comments  A collection of {@link Comment} to reindex
     */
    @Override
    public void reindexComments(final Collection<Comment> comments)
    {
        reindexEntity(comments, AffectedIndex.COMMENT);
    }

    /**
     *  Reindexes the set of provided worklogs in the replicated index.
     *
     *  @param worklogs  A collection of {@link Worklog} to reindex
     */
    @Override
    public void reindexWorklogs(final Collection<Worklog> worklogs)
    {
        reindexEntity(worklogs, AffectedIndex.WORKLOG);
    }

    /**
     * Reindexes the set of provided entities in the replicated index.
     *
     * @param entities  A collection of entities implementing {@link WithId} to reindex
     */
    @Override
    public <T extends WithId> void reindexEntity(final Collection<T> entities, AffectedIndex index)
    {
        try
        {
            final Set<Long> entityIds = Sets.newHashSet();
            for(WithId entity : entities)
            {
                entityIds.add(entity.getId());
            }
            updateReplicatedIndex(entityIds, index,
                    SharedEntityType.NONE, ReplicatedIndexOperation.Operation.UPDATE);
        }
        catch (Exception e)
        {
            log.error(buildErrorMessage("reindexed " + index));
        }
    }

    /**
     * Removes the issues from the replicated index.
     *
     * @param issuesToDelete  the {@link Issue}s to mark as deleted.
     */
    @Override
    public void deIndexIssues(final Set<Issue> issuesToDelete)
    {
        try
        {
            final Set<Long> issueIds = Sets.newHashSet();
            if (issuesToDelete != null)
            {
                for (Issue issue : issuesToDelete)
                {
                    issueIds.add(issue.getId());
                }
            }
            updateReplicatedIndex(issueIds, AffectedIndex.ISSUE,
                    SharedEntityType.NONE, ReplicatedIndexOperation.Operation.DELETE);
        }
        catch (Exception e)
        {
            log.error(buildErrorMessage("deindexed issues"), e);
        }
    }

    @Override
    public void reindexProject(final Project project)
    {
        updateReplicatedIndex(ImmutableSet.of(project.getId()), AffectedIndex.ISSUE,
                SharedEntityType.NONE, ReplicatedIndexOperation.Operation.PROJECT_REINDEX);
    }

    /**
     * Reindexes the shared entity in the replicated index.
     *
     * @param entity  the {@link SharedEntity} to be reindexed
     */
    @Override
    public void indexSharedEntity(SharedEntity entity)
    {
        try
        {
            updateReplicatedIndex(ImmutableSet.of(entity.getId()), AffectedIndex.SHAREDENTITY,
                    SharedEntityType.fromTypeDescriptor(entity.getEntityType()),
                    ReplicatedIndexOperation.Operation.UPDATE);
        }
        catch (Exception e)
        {
            log.error(buildErrorMessage("reindexed shared entity"), e);
        }
    }

    /**
     * Deindexes the shared entity in the replicated index.
     *
     * @param entity  the {@link SharedEntity} to be removed
     */
    @Override
    public void deIndexSharedEntity(SharedEntity entity)
    {
        try
        {
            updateReplicatedIndex(ImmutableSet.of(entity.getId()), AffectedIndex.SHAREDENTITY,
                    SharedEntityType.fromTypeDescriptor(entity.getEntityType()),
                    ReplicatedIndexOperation.Operation.DELETE);
        }
        catch (Exception e)
        {
            log.error(buildErrorMessage("deindexed shared entity"), e);
        }
    }

    @EventListener
    public void onReindexAllStarted(ReindexAllStartedEvent reindexAllStartedEvent)
    {
        if (reindexAllStartedEvent.shouldUpdateReplicatedIndex()) {
            ReplicatedIndexOperation.Operation  reindex;
            if (reindexAllStartedEvent.isUsingBackgroundIndexing())
            {
                reindex = ReplicatedIndexOperation.Operation.BACKGROUND_REINDEX_START;
            }
            else
            {
                reindex = ReplicatedIndexOperation.Operation.FULL_REINDEX_START;
            }
            ofBizReplicatedIndexOperationStore.createIndexOperation(new Timestamp(System.currentTimeMillis()),
                    AffectedIndex.ALL,
                    SharedEntityType.NONE,
                    reindex, Collections.<Long>emptySet(), "");
        }
    }

    @EventListener
    public void onReindexAllCompleted(ReindexAllCompletedEvent reindexAllCompletedEvent)
    {
        if (reindexAllCompletedEvent.shouldUpdateReplicatedIndex()) {
            ReplicatedIndexOperation.Operation  reindex;
            if (reindexAllCompletedEvent.isUsingBackgroundIndexing())
            {
                reindex = ReplicatedIndexOperation.Operation.BACKGROUND_REINDEX_END;
            }
            else
            {
                reindex = ReplicatedIndexOperation.Operation.FULL_REINDEX_END;
            }
            // Circular dependencies force us to access via ComponentAccessor. Potentially we could replace direct calls
            // to the ReplicatedIndexManager with events & event handlers.
            final ClusterNodeProperties clusterNodeProperties = ComponentAccessor.getComponent(ClusterNodeProperties.class);
            final IndexCopyService indexCopyService = ComponentAccessor.getComponent(IndexCopyService.class);
            final String backupFilename = indexCopyService.backupIndex(clusterNodeProperties.getNodeId(), new ReindexMetadataContribution(reindexAllCompletedEvent));
            ofBizReplicatedIndexOperationStore.createIndexOperation(new Timestamp(System.currentTimeMillis()),
                    AffectedIndex.ALL,
                    SharedEntityType.NONE,
                    reindex, Collections.<Long>emptySet(), backupFilename);
        }
        else
        {
            TaskDescriptor taskDescriptor = taskManager.getLiveTask(new IndexTaskContext());
            if (taskDescriptor != null && taskDescriptor.isFinished())
            {
                taskManager.removeTask(taskDescriptor.getTaskId());
            }
        }
    }


    private void updateReplicatedIndex(final Set<Long> ids, final AffectedIndex affectedIndex,
            final SharedEntityType entityType, final ReplicatedIndexOperation.Operation operation)
    {
        ofBizReplicatedIndexOperationStore.createIndexOperation(new Timestamp(System.currentTimeMillis()),
                affectedIndex, entityType, operation, ids, "");
    }

    private String buildErrorMessage(String indexOperationInError)
    {
        return String.format("Caught an exception trying to replicate %s.  The replicated index may or may not be correct", indexOperationInError);
    }

    /**
     * A contribution to the index backup, which writes metadata about the re-index
     */
    private class ReindexMetadataContribution implements IndexSnapshotContribution
    {
        /** The reindex completion event */
        private ReindexAllCompletedEvent reindexAllCompletedEvent;

        /**
         * @param reindexAllCompletedEvent the re-index completion event
         */
        public ReindexMetadataContribution(final ReindexAllCompletedEvent reindexAllCompletedEvent) {
            this.reindexAllCompletedEvent = reindexAllCompletedEvent;
        }

        @Override
        public void writeContribution(final File indexDir) throws IOException
        {
            ReindexMetadata metadata = new ReindexMetadata();
            if (reindexAllCompletedEvent.getIndexStartTime() >= 0)
            {
                metadata.setIndexStartTime(reindexAllCompletedEvent.getIndexStartTime());
            }
            metadata.storeTo(indexDir);
        }
    }
}
