package com.atlassian.jira.plugin.webfragment.conditions;

import java.util.Map;

import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.condition.UrlReadingCondition;
import com.atlassian.plugin.webresource.url.UrlBuilder;

/**
 * An url reading condition which is true iff the configured system property is set and has case insensitive value "true".
 *
 * @since v6.3
 */
public class UrlReadingBooleanSystemPropertyCondition implements UrlReadingCondition
{
    private String property;

    @Override
    public void init(final Map<String, String> params) throws PluginParseException
    {
        property = params.get("property");
    }

    @Override
    public void addToUrl(final UrlBuilder urlBuilder)
    {
        if (JiraSystemProperties.getInstance().getBoolean(property))
        {
            urlBuilder.addToQueryString(property, "true");
        }
    }

    @Override
    public boolean shouldDisplay(final QueryParams queryParams)
    {
        final String property = queryParams.get(this.property);
        return Boolean.parseBoolean(property);
    }
}
