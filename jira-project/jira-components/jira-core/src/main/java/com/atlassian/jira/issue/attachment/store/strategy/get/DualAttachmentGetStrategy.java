package com.atlassian.jira.issue.attachment.store.strategy.get;

import java.io.IOException;
import java.io.InputStream;

import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.util.RuntimeIOException;
import com.atlassian.util.concurrent.Effect;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

import com.google.common.base.Functions;
import com.google.common.io.NullOutputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An implementation of {@link com.atlassian.jira.issue.attachment.store.strategy.get.AttachmentGetStrategy} which get
 * two stores and will try to first load attachment from primary store, in case of absence will try to get from
 * secondary store. When Attachment will be found in primaryStore a background task will be started which will load
 * attachment from secondary store and transfer content to NullOutputStream to generate a real load on secondary store.
 *
 * @since v6.4
 */
@ParametersAreNonnullByDefault
public class DualAttachmentGetStrategy implements AttachmentGetStrategy
{
    private static final Logger log = LoggerFactory.getLogger(DualAttachmentGetStrategy.class);
    private static final Function<InputStream, Integer> DISCARD_STREAM_FUNCTION =
            new Function<InputStream, Integer>()
            {
                @Override
                public Integer get(final InputStream input)
                {
                    try
                    {
                        return IOUtils.copy(input, new NullOutputStream());
                    }
                    catch (final IOException e)
                    {
                        throw new RuntimeIOException(e);
                    }
                }
            };
    private final StreamAttachmentStore primaryStore;
    private final StreamAttachmentStore secondaryStore;

    public DualAttachmentGetStrategy(final StreamAttachmentStore primaryStore, final StreamAttachmentStore secondaryStore)
    {
        this.primaryStore = primaryStore;
        this.secondaryStore = secondaryStore;
    }

    @Override
    public <A> Promise<A> getAttachmentData(final AttachmentKey attachmentKey, final Function<AttachmentGetData, A> attachmentGetDataProcessor)
    {
        final Effect<Throwable> handleBackgroundFailure =
                new Effect<Throwable>()
                {
                    @Override
                    public void apply(final Throwable throwable)
                    {
                        if (log.isDebugEnabled())
                        {
                            log.debug("Error reading attachment from secondary store for attachment Id: {}",
                                    attachmentKey.getAttachmentId().toString(), throwable);
                        }
                    }
                };
        //The logic is as follows:
        //1. try to get attachment from primary store
        //1.1 on success mock fetching attachment from secondary store (this is used to trigger fake load on secondary store)
        //1.2 on error go to 2
        //2. try to get attachment from secondary store (this requires creating second level of Promises this is why there is flatMap in play
        return primaryStore.getAttachmentData(attachmentKey, attachmentGetDataProcessor)
                .done(new Effect<A>()
                      {
                          @Override
                          public void apply(final A ignore)
                          {
                              secondaryStore.getAttachment(attachmentKey, DISCARD_STREAM_FUNCTION).fail(handleBackgroundFailure);
                          }
                      }
                ).fold(new com.google.common.base.Function<Throwable, Promise<A>>()
                {
                    @Override
                    public Promise<A> apply(final Throwable throwableFromPrimary)
                    {
                        return secondaryStore.getAttachmentData(attachmentKey, attachmentGetDataProcessor);
                    }
                },new com.google.common.base.Function<A, Promise<A>>()
                {
                    @Override
                    public Promise<A> apply(final A input)
                    {
                        return Promises.promise(input);
                    }
                }).flatMap(Functions.<Promise<A>>identity());
    }

    @Override
    public Promise<Boolean> exists(final AttachmentKey attachmentKey)
    {
        return primaryStore.exists(attachmentKey).fold(new com.google.common.base.Function<Throwable, Promise<Boolean>>()
        {
            @Override
            public Promise<Boolean> apply(final Throwable ignored)
            {
                return secondaryStore.exists(attachmentKey);
            }
        }, new com.google.common.base.Function<Boolean, Promise<Boolean>>()
        {
            @Override
            public Promise<Boolean> apply(final Boolean exists)
            {
                if (exists)
                {
                    return Promises.promise(true);
                }
                else
                {
                    return secondaryStore.exists(attachmentKey);
                }
            }
        }).flatMap(Functions.<Promise<Boolean>>identity());
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final DualAttachmentGetStrategy that = (DualAttachmentGetStrategy) o;

        return primaryStore.equals(that.primaryStore) && secondaryStore.equals(that.secondaryStore);
    }

    @Override
    public int hashCode()
    {
        int result = primaryStore.hashCode();
        result = 31 * result + secondaryStore.hashCode();
        return result;
    }
}
