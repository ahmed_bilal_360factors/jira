/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

package com.atlassian.jira.web.action.setup;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.LocaleManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.util.BrowserUtils;
import com.atlassian.jira.util.FileFactory;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.action.ActionViewData;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.opensymphony.util.TextUtils;

public abstract class AbstractSetupAction extends JiraWebActionSupport
{
    protected static final String SETUP_ALREADY = "setupalready";
    private static final String SETUP_SESSION_ID_KEY = "setup-session-id";

    public static final String DEFAULT_GROUP_ADMINS = "jira-administrators";
    public static final String DEFAULT_GROUP_DEVELOPERS = "jira-developers";
    public static final String DEFAULT_GROUP_USERS = "jira-users";

    protected final FileFactory fileFactory;
    protected final JiraProperties jiraProperties;

    public AbstractSetupAction(FileFactory fileFactory, JiraProperties jiraProperties)
    {
        this.fileFactory = fileFactory;
        this.jiraProperties = jiraProperties;
    }

    public boolean setupAlready()
    {
        return (getApplicationProperties().getString(APKeys.JIRA_SETUP) != null);
    }

    protected void validateFormPathParam(final String formElement, final String blankErrorMessage, final String nonUniqueErrorMessage, final String myPath, final Collection<String> otherPaths)
    {
        if (!TextUtils.stringSet(myPath))
        {
            addError(formElement, getText(blankErrorMessage));
        }
        else
        {
            for (String otherPath : otherPaths)
            {
                if (myPath.equals(otherPath))
                {
                    addError(formElement, getText(nonUniqueErrorMessage));
                }
            }
            validateSetupPath(myPath, formElement);
        }
    }

    protected void validateSetupPath(final String paramPath, final String formElement)
    {
        File attachmentDir = fileFactory.getFile(paramPath);

        if (!attachmentDir.isAbsolute())
        {
            addError(formElement, getText("setup.error.filepath.notabsolute"));
        }
        else if (!attachmentDir.exists())
        {
            try
            {
                if (!attachmentDir.mkdirs())
                {
                    addError(formElement, getText("setup.error.filepath.notexist"));
                }
            }
            catch (Exception e)
            {
                addError(formElement, getText("setup.error.filepath.notexist"));
            }
        }
        else if (!attachmentDir.isDirectory())
        {
            addError(formElement, getText("setup.error.filepath.notdir"));
        }
        else if (!attachmentDir.canWrite())
        {
            addError(formElement, getText("setup.error.filepath.notwriteable"));
        }
    }

    protected String getCurrentTrackerStepId()
    {
        return "";
    }

    @ActionViewData
    public List<Map<String, Object>> getTrackerSteps()
    {
        ImmutableList.Builder<Map<String, Object>> defaultsBuilder = ImmutableList.builder();

        defaultsBuilder.add(ImmutableMap.<String, Object>of(
                "href", "#",
                "id", "setup-tracker-database",
                "text", "Database setup"
        ));

        defaultsBuilder.add(ImmutableMap.<String, Object>of(
                "href", "#",
                "id", "setup-tracker-properties-and-licensing",
                "text", "Properties & Licensing"
        ));

        defaultsBuilder.add(ImmutableMap.<String, Object>of(
                "href", "#",
                "id", "setup-tracker-admin-account",
                "text", "Admin account"
        ));

        defaultsBuilder.add(ImmutableMap.<String, Object>of(
                "href", "#",
                "id", "setup-tracker-email",
                "text", "Email setup"
        ));

        ImmutableList.Builder<Map<String, Object>> builder = ImmutableList.builder();
        for (Map<String, Object> item : defaultsBuilder.build())
        {
            builder.add(MapBuilder.<String, Object>newBuilder()
                    .addAll(item)
                    .add("isCurrent", item.get("id").equals(getCurrentTrackerStepId()) ? true : false)
                    .toMap());
        }

        return builder.build();
    }

    @ActionViewData
    public String getModifierKey()
    {
        return BrowserUtils.getModifierKey();
    }

    public String getSetupSessionId()
    {
        String sessionId = (String) getHttpSession().getAttribute(SETUP_SESSION_ID_KEY);

        if (sessionId == null)
        {
            sessionId = UUID.randomUUID().toString();
            getHttpSession().setAttribute(SETUP_SESSION_ID_KEY, sessionId);
        }

        return sessionId;
    }

    @ActionViewData
    public String getProductName()
    {
        return jiraProperties.getProductName();
    }

    @Override
    public String getServerId()
    {
        return getApplicationProperties().getString(APKeys.JIRA_SID);
    }

    /**
     * Check if given locale is valid for this JIRA instance, ie. if it is installed.
     *
     * @param locale to validate
     * @return whether given local is valid
     */
    public boolean isLocaleValid(final String locale)
    {
        final LocaleManager localeManager = ComponentAccessor.getComponentOfType(LocaleManager.class);
        final Set<Locale> installedLocales = localeManager.getInstalledLocales();

        return installedLocales.contains(localeManager.getLocale(locale));
    }

    public void setJiraLocale(final String locale)
    {
        // Always get a fresh copy of the application properties because during the short life of some actions
        // (eg. SetupDatabase) the Pico container can be swapped out from underneath us.
        final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();

        if (isLocaleValid(locale))
        {
            applicationProperties.setString(APKeys.JIRA_I18N_DEFAULT_LOCALE, locale);
        }
    }

    /**
     * Set the indexing language for the selected (or default) locale.
     */
    public void setIndexingLanguageForDefaultServerLocale()
    {
        // Always get a fresh copy of the application properties because during the short life of some actions
        // (eg. SetupDatabase) the Pico container can be swapped out from underneath us.
        final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();
        final IndexLanguageToLocaleMapper languageToLocaleMapper = ComponentAccessor.getComponent(IndexLanguageToLocaleMapper.class);

        applicationProperties.setString(APKeys.JIRA_I18N_LANGUAGE_INPUT, languageToLocaleMapper.getLanguageForLocale(getLocale().toString()));
    }

    public String isInstantSetup()
    {
        final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();

        return String.valueOf(applicationProperties.getOption(APKeys.JIRA_SETUP_IS_INSTANT));
    }

    public String getAnalyticsIframeUrl() {
        final String defaultUrl = jiraProperties.isDevMode()
                ? "https://qa-wac.internal.atlassian.com/pingback"
                : "https://www.atlassian.com/pingback";
        return jiraProperties.getProperty("jira.setup.analytics.iframe.url", defaultUrl);
    }
}
