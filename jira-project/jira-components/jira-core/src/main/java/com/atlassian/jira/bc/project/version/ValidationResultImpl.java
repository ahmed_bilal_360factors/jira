package com.atlassian.jira.bc.project.version;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.util.ErrorCollection;

import java.util.Set;

import static com.atlassian.fugue.Option.some;

/**
 * Encapsulates the result of validating a delete version service call
 *
 * @since v3.13
 */
final class ValidationResultImpl extends ServiceResultImpl implements VersionService.ValidationResult
{
    private final Version versionToDelete;
    private final Version fixSwapVersion;
    private final Version affectsSwapVersion;
    private final boolean isValid;
    private final Set<VersionService.ValidationResult.Reason> reasons;

    ValidationResultImpl(ErrorCollection errorCollection, final Version versionToDelete, final Version affectsSwapVersion, final Version fixSwapVersion, final boolean valid, Set<Reason> reasons)
    {
        super(errorCollection);
        this.versionToDelete = versionToDelete;
        this.fixSwapVersion = fixSwapVersion;
        this.affectsSwapVersion = affectsSwapVersion;
        this.isValid = valid;
        this.reasons = reasons;
    }

    public boolean isValid()
    {
        return isValid;
    }

    public Version getVersionToDelete()
    {
        return versionToDelete;
    }

    public Version getFixSwapVersion()
    {
        return fixSwapVersion;
    }

    public Version getAffectsSwapVersion()
    {
        return affectsSwapVersion;
    }

    @Override
    public Option<Version> getVersionToMergeTo()
    {
        return isMerge() ? some(affectsSwapVersion) : Option.<Version>none();
    }

    @Override
    public Action getAction()
    {
        return isMerge() ? Action.MERGE : (isDelete() ? Action.DELETE : Action.DELETE_AND_REPLACE);
    }

    public boolean isMerge()
    {
        return affectsSwapVersion != null && affectsSwapVersion.equals(fixSwapVersion);
    }

    private boolean isDelete()
    {
        return affectsSwapVersion == null && fixSwapVersion == null;
    }

    @Override
    public Set<VersionService.ValidationResult.Reason> getReasons()
    {
        return reasons;
    }
}
