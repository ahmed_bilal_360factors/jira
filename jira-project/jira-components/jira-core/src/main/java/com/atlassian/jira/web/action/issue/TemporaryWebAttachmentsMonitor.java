package com.atlassian.jira.web.action.issue;

import java.util.Collection;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.annotations.Internal;
import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachment;

/**
 * Temporary web attachments monitor is responsible for keeping temporary attachments uploaded by user. It also should
 * clean up those attachments if they expire (for example when user session expires).
 *
 * Please do not use this class directly outside {com.atlassian.jira.web.util.TemporaryWebAttachmentManager} implementation.
 *
 * @since v6.4
 */
@Internal
@ParametersAreNonnullByDefault
public interface TemporaryWebAttachmentsMonitor
{
    /**
     * Retrieves temporary attachment by id.
     * @param temporaryAttachmentId temporary attachment id.
     * @return selected temporary attachment if exists
     */
    Option<TemporaryWebAttachment> getById(String temporaryAttachmentId);

    /**
     * Removes temporary attachment with given id from monitor. Does not remove it from attachment store.
     * @param temporaryAttachmentId Temporary Attachment Id which will be removed.
     * @return Removed temporary attachment if exists
     */
    Option<TemporaryWebAttachment> removeById(String temporaryAttachmentId);

    /**
     * Adds new temporary attachment.
     * @param temporaryAttachment temporary attachment.
     */
    void add(TemporaryWebAttachment temporaryAttachment);

    /**
     * Gets all temporary attachments with given from token.
     * @param formToken form token
     * @return Collection of matched temporary attachments
     */
    Collection<TemporaryWebAttachment> getByFormToken(String formToken);

    /**
     * Removes all attachments from monitor and from attachment store, matched by form token.
     */
    void cleanByFormToken(String formToken);
}
