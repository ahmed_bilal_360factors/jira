package com.atlassian.jira.index;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.fugue.Effect;
import com.atlassian.fugue.Option;
import com.atlassian.instrumentation.Counter;
import com.atlassian.jira.cluster.ClusterSafe;
import com.atlassian.jira.concurrent.ResettableLazyReference;
import com.atlassian.jira.index.DelayCloseable.AlreadyClosedException;
import com.atlassian.jira.index.Index.Operation;
import com.atlassian.jira.instrumentation.Instrumentation;
import com.atlassian.jira.instrumentation.InstrumentationName;
import com.atlassian.jira.util.Closeable;
import com.atlassian.jira.util.Function;
import com.atlassian.jira.util.RuntimeIOException;
import com.atlassian.jira.util.Supplier;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;

import net.jcip.annotations.ThreadSafe;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Thread-safe container that manages our current {@link IndexSearcher} and {@link Writer}.
 * <p>
 * Gets passed searcher and writer factories that create new instances of these when required.
 */
@ThreadSafe
public class DefaultIndexEngine implements DefaultIndex.Engine
{
    private static final Logger log = LoggerFactory.getLogger(DefaultIndexEngine.class);

    /**
     * How to perform an actual write to the writer.
     */
    public static enum FlushPolicy
    {
        /**
         * Do not flush or close.
         */
        NONE()
        {
            @Override
            void commit(final WriterReference writer)
            {}
        },
        /**
         * Commit the writer's pending updates, do not close.
         */
        FLUSH()
        {
            @Override
            void commit(final WriterReference writer)
            {
                writer.commit();
            }
        },
        /**
         * Commit the writer's pending updates, from time to time
         */
        PERIODIC()
        {
            // We schedule a one off task in a resettable to run a little while in the future.
            // This is cached and persists in the cache until it is run, when it will invalidate itself and
            // a new instance will be created for a later commit().

            ScheduledExecutorService ex = Executors.newScheduledThreadPool(10, new IndexEngineThreadFactory());

            LoadingCache<WriterReference, ScheduledFuture<?>> executorCache = CacheBuilder.newBuilder()
                    .build(new CacheLoader<WriterReference, ScheduledFuture<?>>()
            {
                @Override
                public ScheduledFuture<?> load(@Nonnull final WriterReference writer) throws Exception
                {
                    return ex.schedule(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            executorCache.invalidate(writer);
                            writer.commit();
                            Counter LuceneIndexCommitInstrument = Instrumentation.pullCounter(InstrumentationName.WRITER_LUCENE_COMMIT);
                            LuceneIndexCommitInstrument.incrementAndGet();

                        }
                    }, writer.get(Index.UpdateMode.INTERACTIVE).getCommitFrequency(), TimeUnit.MILLISECONDS);
                }
            });

            @Override
            void commit(final WriterReference writer)
            {
                try
                {
                    executorCache.get(writer);
                }
                catch (ExecutionException e)
                {
                    throw new RuntimeException(e);
                }
            }
        },

        /**
         * Close the writer after performing the write.
         */
        CLOSE()
        {
            @Override
            @ClusterSafe ("Indexing")
            synchronized void commit(final WriterReference writer)
            {
                writer.close();
            }
        };

        void perform(final Operation operation, final WriterReference writer) throws IOException
        {
            try
            {
                operation.perform(writer.get(operation.mode()));
            }
            finally
            {
                commit(writer);
            }
        }

        abstract void commit(final WriterReference writer);
    }

    private final WriterReference writerReference;
    private final SearcherFactory searcherFactory;
    private final SearcherReference searcherReference;
    private final Configuration configuration;

    /**
     * Production ctor.
     *
     * @param configuration the {@link Directory} and {@link Analyzer}
     */
    DefaultIndexEngine(final @Nonnull Configuration configuration)
    {
        this.configuration = notNull("configuration", configuration);
        this.searcherFactory = new SearcherFactoryImpl(configuration);
        this.searcherReference = new SearcherReference(searcherFactory);
        this.writerReference = new WriterReference(new DefaultWriterFactory());
    }

    /**
     * For testing ctor.
     *
     * @param searcherFactory for creating {@link IndexSearcher searchers}
     * @param writerFactory for creating Writer instances of the correct mode
     * @param configuration the {@link Directory} and {@link Analyzer}
     */
    DefaultIndexEngine(final @Nonnull SearcherFactory searcherFactory, @Nullable final Function<Index.UpdateMode, Writer> writerFactory, final @Nonnull Configuration configuration)
    {
        this.configuration = notNull("configuration", configuration);
        this.searcherFactory = notNull("searcherFactory", searcherFactory);
        this.searcherReference = new SearcherReference(searcherFactory);
        this.writerReference = new WriterReference(writerFactory == null ? new DefaultWriterFactory() : writerFactory);
    }

    /**
     * leak a {@link IndexSearcher}. Must get closed after usage.
     */
    @Nonnull
    public IndexSearcher getSearcher()
    {
        // mode is irrelevant to a Searcher
        return searcherReference.get(Index.UpdateMode.INTERACTIVE);
    }

    public void clean()
    {
        close();
        try
        {
            IndexWriterConfig luceneConfig = new IndexWriterConfig(LuceneVersion.get(), configuration.getAnalyzer());
            luceneConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
            new IndexWriter(configuration.getDirectory(), luceneConfig).close();
        }
        catch (final IOException e)
        {
            throw new RuntimeIOException(e);
        }
    }

    public void write(@Nonnull final Operation operation) throws IOException
    {
        try
        {
            writerReference.get(operation.mode()).getFlushPolicy().perform(operation, writerReference);
        }
        finally
        {
            searcherReference.close();
        }
    }

    public void close()
    {
        writerReference.close();
        searcherReference.close();
        searcherFactory.release();  // JRA-29587
    }

    /**
     * Thread-safe holder of the current Searcher
     */
    @ThreadSafe
    private class SearcherReference extends ReferenceHolder<DelayCloseSearcher>
    {
        private final SearcherFactory searcherSupplier;

        SearcherReference(@Nonnull final SearcherFactory searcherSupplier)
        {
            this.searcherSupplier = notNull("searcherSupplier", searcherSupplier);
        }

        @Override
        DelayCloseSearcher doCreate(final Index.UpdateMode mode)
        {
            return new DelayCloseSearcher(searcherSupplier.get());
        }

        @Override
        DelayCloseSearcher open(final DelayCloseSearcher searcher)
        {
            searcher.open();
            return searcher;
        }

        @Override
        void doClose(final DelayCloseSearcher searcher)
        {
            searcher.closeWhenDone();
        }
    }

    /**
     * Thread-safe holder of the current Writer
     */
    @ThreadSafe
    private static class WriterReference extends ReferenceHolder<Writer>
    {
        private final Function<Index.UpdateMode, Writer> writerFactory;

        WriterReference(@Nonnull final Function<Index.UpdateMode, Writer> writerFactory)
        {
            this.writerFactory = notNull("writerFactory", writerFactory);
        }

        public void commit()
        {
            final Option<Writer> writerOption = get();
            if (writerOption.isDefined())
            {
                try
                {
                    writerOption.get().commit();
                }
                catch (IllegalStateException ise)
                {
                    log.error("Hit an exception committing writes to the index; discarding the current writer!", ise);
                    safeClose(writerOption.get());
                    throw ise;
                }
            }
        }

        @Override
        Writer doCreate(final Index.UpdateMode mode)
        {
            return writerFactory.get(mode);
        }

        @Override
        void doClose(final Writer writer)
        {
            writer.close();
        }

        @Override
        Writer open(final Writer writer)
        {
            return writer;
        }

    }

    private class DefaultWriterFactory implements Function<Index.UpdateMode, Writer>
    {
        @Override
        public Writer get(Index.UpdateMode mode)
        {
            // be default, create a writer wrapper that has access to this engine's searcher
            return new WriterWrapper(configuration, mode, new Supplier<IndexSearcher>() {
                @Override
                public IndexSearcher get()
                {
                    return getSearcher();
                }
            });
        }
    }

    static abstract class ReferenceHolder<T> implements Function<Index.UpdateMode, T>, Closeable
    {
        private final ResettableLazyReference<T> reference = new ResettableLazyReference<T>();
        private final Effect<T> resetEffect = new Effect<T>()
        {
            @Override
            public void apply(final T localReference)
            {
                try
                {
                    doClose(localReference);
                }
                catch (final RuntimeException re)
                {
                    log.debug("Error closing reference", re);
                }
            }
        };

        /**
         * Close if and only if the specified expected value is the currently held reference.
         */
        final void safeClose(final T expected)
        {
            reference.safeReset(expected).foreach(resetEffect);
        }

        public final void close()
        {
            reference.reset().foreach(resetEffect);
        }

        abstract void doClose(T element);

        @Override
        public final T get(final Index.UpdateMode mode)
        {
            while (true)
            {
                try
                {
                    return open(reference.getOrCreate(new Supplier<T>()
                    {
                        @Override
                        public T get()
                        {
                            return doCreate(mode);
                        }
                    }));
                }
                catch (final AlreadyClosedException ace)
                {
                    log.debug("Already closed", ace);
                }
                // in the rare case of a race condition, try again
            }
        }

        abstract T doCreate(Index.UpdateMode mode);

        abstract T open(T element);

        final Option<T> get()
        {
            return reference.get();
        }
    }

    static interface SearcherFactory extends Supplier<IndexSearcher>
    {
        void release();
    }

    private class SearcherFactoryImpl implements SearcherFactory
    {
        private final Configuration configuration;

        /* This is already held in the thread safe SearcherReference. */
        private volatile IndexReader oldReader = null;

        SearcherFactoryImpl(final Configuration configuration)
        {
            this.configuration = notNull("configuration", configuration);
        }

        public IndexSearcher get()
        {
            try
            {
                IndexReader reader;
                if (oldReader != null)
                {
                    try
                    {
                        reader = reopenIndexReader(oldReader);
                        // If we actually get a new reader, we must close the old one
                        //noinspection ObjectEquality
                        if (reader != oldReader)
                        {
                            // This will really close only when the ref count goes to zero.
                            try
                            {
                                oldReader.close();
                            }
                            catch (org.apache.lucene.store.AlreadyClosedException ace)
                            {
                                log.debug("Tried to close an already closed reader.", ace);
                            }
                        }
                    }
                    catch (org.apache.lucene.store.AlreadyClosedException ignore)
                    {
                        // JRADEV-7825: Really this shouldn't happen unless someone closes the reader from outside all
                        // the inscrutable code in this class (and its friends) but
                        // don't worry, we will just open a new one in that case.
                        log.warn("Tried to reopen the IndexReader, but it threw AlreadyClosedException. Opening a fresh IndexReader.");
                        reader = openIndexReader();
                    }
                }
                else
                {
                    reader = openIndexReader();
                }
                oldReader = reader;
                return new IndexSearcher(reader);
            }
            catch (final IOException e)
            {
                ///CLOVER:OFF
                throw new RuntimeIOException(e);
                ///CLOVER:ON
            }
        }

        private IndexReader openIndexReader() throws IOException
        {
            if (useNRT())
            {
                return IndexReader.open(writerReference.get(Index.UpdateMode.INTERACTIVE).getLuceneWriter(), true);
            }
            // Ensure all writes have been committed
            writerReference.get(Index.UpdateMode.INTERACTIVE).getLuceneWriter().commit();
            return IndexReader.open(configuration.getDirectory(), true);
        }

        private IndexReader reopenIndexReader(IndexReader reader) throws IOException
        {
            if (useNRT())
            {
                return reader.reopen(writerReference.get(Index.UpdateMode.INTERACTIVE).getLuceneWriter(), true);
            }
            return reader.reopen();
        }

        private boolean useNRT()
        {
            // Use NRT reader if FlushPolicy is PERIODIC.
            return FlushPolicy.PERIODIC.equals(writerReference.get(Index.UpdateMode.INTERACTIVE).getFlushPolicy());
        }

        public void release()
        {
            final IndexReader reader = oldReader;
            if (reader != null)
            {
                try
                {
                    reader.close();
                    oldReader = null;
                }
                catch (org.apache.lucene.store.AlreadyClosedException ignore)
                {
                    // Ignore
                }
                catch (IOException e)
                {
                    ///CLOVER:OFF
                    throw new RuntimeException(e);
                    ///CLOVER:ON
                }
            }
        }
    }

    /**
     * Internal thread factory class for threads created to commit the indexing.
     */
    private static class IndexEngineThreadFactory implements ThreadFactory
    {
        private final AtomicLong threadId = new AtomicLong(0);

        @Nonnull
        public Thread newThread(@Nonnull final Runnable runnable)
        {
            final Thread t = new Thread(runnable, "JiraIndexCommitThread-" + threadId.incrementAndGet());
            t.setDaemon(true);
            return t;
        }
    }



}
