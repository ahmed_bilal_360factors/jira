package com.atlassian.jira.issue.attachment.store.strategy;

import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.util.concurrent.Effect;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;

/**
 * Perform operation on dual store ensuring that consistency exists in primary store. This means that only result from
 * primary store is relevant and operation on secondary store is performed only if operation on primary store is
 * successful;
 *
 * @since v6.4
 */
public class DualStoreConsistentOperationStrategy implements StoreOperationStrategy
{
    private final StreamAttachmentStore primaryAttachmentStore;
    private final StreamAttachmentStore secondaryAttachmentStore;

    public DualStoreConsistentOperationStrategy(final StreamAttachmentStore primaryAttachmentStore, final StreamAttachmentStore secondaryAttachmentStore)
    {
        this.primaryAttachmentStore = primaryAttachmentStore;
        this.secondaryAttachmentStore = secondaryAttachmentStore;
    }

    @Override
    public <V> Promise<V> perform(final Function<StreamAttachmentStore, Promise<V>> operation)
    {
        return operation.get(primaryAttachmentStore).done(
                new Effect<V>()
                {
                    @Override
                    public void apply(final V v)
                    {
                        operation.get(secondaryAttachmentStore);
                    }
                }
        );
    }
}
