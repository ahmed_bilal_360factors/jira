package com.atlassian.jira.concurrent;

import java.util.concurrent.TimeUnit;

import com.atlassian.jira.util.concurrent.BlockingCounter;
import com.atlassian.util.concurrent.TimedOutException;

import com.google.common.annotations.VisibleForTesting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An in-memory barrier.
 *
 * @since v5.2
 */
class BarrierImpl implements Barrier
{
    private static final Logger log = LoggerFactory.getLogger(BarrierImpl.class);

    private final String name;

    /** The counter used to block threads. */
    @VisibleForTesting
    final BlockingCounter counter = new BlockingCounter();

    /**
     * An airlock exit door.
     * This allows us to raise the barrier again before releasing the threads
     */
    private final BlockingCounter airlockExit = new BlockingCounter();

    public BarrierImpl(String name)
    {
        this.name = name;
    }

    @Override
    public String name()
    {
        return name;
    }

    @Override
    public void await()
    {
        if (counter.wouldBlock()) {
            log.debug("Barrier '{}' is up. Waiting for it to be lowered", name());
            counter.awaitUninterruptibly();

            // If the airlockExit is active, wait for that too.
            airlockExit.awaitUninterruptibly();
        }
        log.debug("Barrier '{}' is down", name());
    }

    @Override
    public void raise()
    {
        log.debug("Raising barrier '{}'", name());
        counter.up();
    }

    @Override
    public void lower()
    {
        log.debug("Lowering barrier '{}'", name());
        counter.down();
    }

    @Override
    public void lowerThenRaise()
    {
        log.debug("Lowering barrier '{}' for waiting threads, then raising again", name());

        // Lower the barrier, allowing waiting threads into an airlock.
        // Once they are in, raise the barrier and release the airlock.

        // raise the airlock exit door and allow all waiting threads into the airlock
        airlockExit.up();
        counter.down();
        waitForZeroWaitingThreads();

        // Raise barrier again and release the airlock
        counter.up();
        airlockExit.down();
    }

    /**
     * Wait until there are no threads waiting at the barrier
     */
    private void waitForZeroWaitingThreads()
    {
        // It shouldn't take long for all the threads to move on once the barrier is lowered
        final int timeoutInSeconds = 2;
        final long deadline = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(timeoutInSeconds);
        boolean deadlineReached = false;
        while (!deadlineReached && counter.getWaitingThreadCount() > 0)
        {
            deadlineReached = System.currentTimeMillis() > deadline;
        }
        if (deadlineReached)
        {
            log.warn("Timed out after " + timeoutInSeconds + " seconds waiting for threads to move past the barrier. Continuing.");
        }
    }
}
