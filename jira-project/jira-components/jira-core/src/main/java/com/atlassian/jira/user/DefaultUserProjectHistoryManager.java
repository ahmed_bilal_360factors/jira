package com.atlassian.jira.user;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;

import java.util.Collections;
import java.util.List;

import com.google.common.collect.Lists;

import static com.atlassian.jira.user.ApplicationUsers.from;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Convienience wrapper for the {@link com.atlassian.jira.user.UserHistoryManager} that deals directly with Projects.
 *
 * @since v4.0
 */
public class DefaultUserProjectHistoryManager implements UserProjectHistoryManager
{
    private final PermissionManager permissionManager;
    private final ProjectManager projectManager;
    private final UserHistoryManager userHistoryManager;

    public DefaultUserProjectHistoryManager(UserHistoryManager userHistoryManager, ProjectManager projectManager,
                                            PermissionManager permissionManager)
    {
        this.userHistoryManager = userHistoryManager;
        this.projectManager = projectManager;
        this.permissionManager = permissionManager;
    }

    public void addProjectToHistory(User user, Project project)
    {
        notNull("project", project);
        userHistoryManager.addItemToHistory(UserHistoryItem.PROJECT, user, project.getId().toString());
    }

    public boolean hasProjectHistory(int permission, User user)
    {
        final List<UserHistoryItem> history = userHistoryManager.getHistory(UserHistoryItem.PROJECT, user);
        final ApplicationUser applicationUser = from(user);
        if (history != null)
        {
            for (final UserHistoryItem historyItem : history)
            {
                final Project project = projectManager.getProjectObj(Long.valueOf(historyItem.getEntityId()));
                if (project != null && permissionManager.hasPermission(new ProjectPermissionKey(permission), project, applicationUser))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public Project getCurrentProject(int permission, User user)
    {
        final List<UserHistoryItem> history = userHistoryManager.getHistory(UserHistoryItem.PROJECT, user);
        final ApplicationUser applicationUser = from(user);
        if (history != null)
        {
            for (final UserHistoryItem historyItem : history)
            {
                final Project project = projectManager.getProjectObj(Long.valueOf(historyItem.getEntityId()));
                if (project != null && permissionManager.hasPermission(new ProjectPermissionKey(permission), project, applicationUser))
                {
                    return project;
                }
            }
        }
        return null;
    }

    public List<UserHistoryItem> getProjectHistoryWithoutPermissionChecks(User user)
    {
        return userHistoryManager.getHistory(UserHistoryItem.PROJECT, user);
    }

    public List<Project> getProjectHistoryWithPermissionChecks(int permission, User user)
    {
        final List<UserHistoryItem> history = getProjectHistoryWithoutPermissionChecks(user);
        final ApplicationUser applicationUser = from(user);

        if (history != null)
        {
            final List<Project> returnList = Lists.newArrayListWithCapacity(history.size());
            for (UserHistoryItem userHistoryItem : history)
            {
                final Project project = projectManager.getProjectObj(Long.valueOf(userHistoryItem.getEntityId()));
                if (project != null && permissionManager.hasPermission(new ProjectPermissionKey(permission), project, applicationUser))
                {
                    returnList.add(project);
                }
            }
            return returnList;
        }
        else
        {
            return Collections.emptyList();
        }
    }

    public List<Project> getProjectHistoryWithPermissionChecks(ProjectAction projectAction, User user)
    {
        final List<UserHistoryItem> history = getProjectHistoryWithoutPermissionChecks(user);

        if (history != null)
        {
            final List<Project> returnList = Lists.newArrayListWithCapacity(history.size());
            for (UserHistoryItem userHistoryItem : history)
            {
                final Project project = projectManager.getProjectObj(Long.valueOf(userHistoryItem.getEntityId()));

                if (project != null && projectAction.hasPermission(permissionManager, user, project))
                {
                    returnList.add(project);
                }
            }
            return returnList;
        }
        else
        {
            return Collections.emptyList();
        }
    }
}
