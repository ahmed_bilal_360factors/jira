package com.atlassian.jira.crowd.embedded;

import java.util.Date;
import java.util.List;

import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.authentication.ApplicationAuthenticationContext;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.token.TokenLifetime;
import com.atlassian.crowd.model.user.User;

/**
 * We don't provide any of this functionality.  Just need to provide this guy because
 * Crowd Rest Plugin needs an implementation.
 *
 * @since v4.3
 */
public class NoopTokenAuthenticationManager implements TokenAuthenticationManager
{
    @Override
    public Token authenticateApplication(ApplicationAuthenticationContext authenticationContext, TokenLifetime tokenLifetime)
    {
        return null;
    }

    @Override
    public Token authenticateUser(UserAuthenticationContext userAuthenticationContext, TokenLifetime tokenLifetime)
    {
        return null;
    }

    @Override
    public Token authenticateUser(UserAuthenticationContext userAuthenticationContext)
    {
        return null;
    }

    @Override
    public Token authenticateUserWithoutValidatingPassword(UserAuthenticationContext userAuthenticationContext)
    {
        return null;
    }

    @Override
    public Token validateApplicationToken(String tokenKey, ValidationFactor[] validationFactors)
    {
        return null;
    }

    @Override
    public Token validateUserToken(String userTokenKey, ValidationFactor[] validationFactors, String application)
    {
        return null;
    }

    @Override
    public void invalidateToken(String token)
    {
    }

    @Override
    public void removeExpiredTokens()
    {
    }

    @Override
    public User findUserByToken(String tokenKey, String applicationName)
    {
        return null;
    }

    @Override
    public Token findUserTokenByKey(String tokenKey, String applicationName)
    {
        return null;
    }

    @Override
    public List<Application> findAuthorisedApplications(User user, String applicationName)
    {
        return null;
    }

    @Override
    public void invalidateTokensForUser(String username, String exclusionToken, String applicationName)
    {
    }

    @Override
    public Date getTokenExpiryTime(Token token)
    {
        return null;
    }
}
