package com.atlassian.velocity;

import java.lang.reflect.Method;

import javax.annotation.Nonnull;

import org.apache.velocity.util.introspection.ClassMap;
import org.apache.velocity.util.introspection.MethodMap;

/**
 * Implementation of ClassMap that uses JiraMethodMap for methods resolution.
 *
 * @since v6.4
 */
public class JiraClassMap implements ClassMap
{

    private final JiraMethodMap methodMap;

    public JiraClassMap(@Nonnull final Class<?> clazz)
    {
        this.methodMap = new JiraMethodMap(clazz);
    }

    @Override
    public Method findMethod(final String name, final Object[] params) throws MethodMap.AmbiguousException
    {
        return methodMap.find(name, params);
    }
}
