package com.atlassian.jira.plugin.webfragment.conditions;

import java.util.Collection;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.option.Option;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Condition to check whether the issue has any sub task types available.
 * <p/>
 * An issue must be in the JiraHelper context params.
 *
 * @since v4.1
 */
public class HasSubTasksAvailableCondition extends AbstractIssueWebCondition
{
    private static final Logger log = LoggerFactory.getLogger(SubTasksEnabledCondition.class);
    private final FieldManager fieldManager;

    public HasSubTasksAvailableCondition(FieldManager fieldManager)
    {
        this.fieldManager = fieldManager;
    }

    public boolean shouldDisplay(ApplicationUser user, Issue issue, JiraHelper jiraHelper)
    {
        final Collection<Option> availableSubTaskIssueTypes = fieldManager.getIssueTypeField().getOptionsForIssue(issue, true);
        return (availableSubTaskIssueTypes != null) && !availableSubTaskIssueTypes.isEmpty();
    }

}
