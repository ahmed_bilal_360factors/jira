package com.atlassian.jira.model.querydsl;

import com.mysema.query.sql.ColumnMetadata;
import com.mysema.query.types.path.*;

import java.sql.Types;
import javax.annotation.Generated;

/**
 * QGadgetUserPreference is a Querydsl query object.
 *
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QGadgetUserPreference extends JiraRelationalPathBase<GadgetUserPreferenceDTO>
{
    public static final QGadgetUserPreference GADGET_USER_PREFERENCE = new QGadgetUserPreference("GADGET_USER_PREFERENCE");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final NumberPath<Long> portletconfiguration = createNumber("portletconfiguration", Long.class);
    public final StringPath userprefkey = createString("userprefkey");
    public final StringPath userprefvalue = createString("userprefvalue");

    public QGadgetUserPreference(String alias)
    {
        super(GadgetUserPreferenceDTO.class, alias, "gadgetuserpreference");
        addMetadata();
    }

    private void addMetadata()
    {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(portletconfiguration, ColumnMetadata.named("portletconfiguration").withIndex(2).ofType(Types.NUMERIC).withSize(18));
        addMetadata(userprefkey, ColumnMetadata.named("userprefkey").withIndex(3).ofType(Types.VARCHAR).withSize(255));
        addMetadata(userprefvalue, ColumnMetadata.named("userprefvalue").withIndex(4).ofType(Types.VARCHAR).withSize(2147483647));
    }
}

