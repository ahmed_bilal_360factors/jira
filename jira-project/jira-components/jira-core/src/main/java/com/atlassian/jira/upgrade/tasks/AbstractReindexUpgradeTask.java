/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

package com.atlassian.jira.upgrade.tasks;

import java.util.EnumSet;
import java.util.Set;

import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;

public abstract class AbstractReindexUpgradeTask extends AbstractImmediateUpgradeTask
{
    protected static final Set<AffectedIndex> ALL_INDEXES = EnumSet.allOf(AffectedIndex.class);
    protected static final Set<AffectedIndex> ALL_ISSUE_INDEXES =
            EnumSet.of(AffectedIndex.ISSUE, AffectedIndex.COMMENT, AffectedIndex.CHANGEHISTORY, AffectedIndex.WORKLOG);
    protected static final Set<SharedEntityType> ALL_SHARED_ENTITY_TYPES = EnumSet.allOf(SharedEntityType.class);
    protected static final Set<AffectedIndex> WORKLOG_ONLY = EnumSet.of(AffectedIndex.WORKLOG);
    protected static final Set<SharedEntityType> NO_SHARED_ENTITIES = EnumSet.noneOf(SharedEntityType.class);

    public void doUpgrade(boolean setupMode) throws Exception
    {
        getReindexRequestService().requestReindex(ReindexRequestType.IMMEDIATE, ALL_INDEXES, ALL_SHARED_ENTITY_TYPES);
    }

    public void reIndexAllIssues() throws Exception
    {
        getReindexRequestService().requestReindex(ReindexRequestType.IMMEDIATE, ALL_ISSUE_INDEXES, NO_SHARED_ENTITIES);
    }

    public String getShortDescription()
    {
        return "Signalling all data in JIRA should be reindexed.";
    }
}
