package com.atlassian.jira.plugin.attachment;

import java.util.List;

import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;

@JsonAutoDetect
public class AttachmentArchiveImpl implements AttachmentArchive
{
    private static final Logger log = LoggerFactory.getLogger(AttachmentArchiveImpl.class);
    public static final AttachmentArchive DOC_EXAMPLE;

    static
    {
        final ImmutableList<AttachmentArchiveEntry> entries = ImmutableList.of(
                new AttachmentArchiveEntryBuilder()
                        .entryIndex(0)
                        .name("Allegro from Duet in C Major.mp3")
                        .size(1430174)
                        .mediaType("audio/mpeg")
                        .build(),
                new AttachmentArchiveEntryBuilder()
                        .entryIndex(1)
                        .name("lrm.rtf")
                        .size(331)
                        .mediaType("text/rtf")
                        .build()
        );
        DOC_EXAMPLE = new AttachmentArchiveImpl(entries, 24);
    }

    public static Iterable<JSONObject> serialize(final List<AttachmentArchiveEntry> entries)
    {
        return Iterables.transform(
                entries,
                new Function<AttachmentArchiveEntry, JSONObject>()
                {
                    @Override
                    public JSONObject apply(final AttachmentArchiveEntry entry)
                    {
                        try
                        {
                            final JSONObject json = new JSONObject();
                            json.put("entryIndex", entry.getEntryIndex());
                            json.put("name", entry.getName());
                            json.put("size", entry.getSize());
                            json.put("mediaType", entry.getMediaType());
                            return json;
                        }
                        catch (final JSONException e)
                        {
                            throw new RuntimeException(e);
                        }
                    }
                }
        );
    }

    public static AttachmentArchive fromJSONArrayQuiet(final String jsonArrayString, final int maxSize)
    {
        try
        {
            final JSONArray jsonArray = new JSONArray(jsonArrayString);
            final ImmutableList.Builder<AttachmentArchiveEntry> builder = ImmutableList.builder();

            final int maxIndex = Math.min(jsonArray.length(), maxSize);
            for (int i = 0; i < maxIndex; i++)
            {
                builder.add(FROM_JSON.apply(jsonArray.getJSONObject(i)));
            }

            final List<AttachmentArchiveEntry> entries = builder.build();
            return new AttachmentArchiveImpl(entries, jsonArray.length());
        }
        catch (final JSONException ignore)
        {
            log.error(String.format("Invalid jsonArray supplied: %s", jsonArrayString));
            return null;
        }
    }

    private static final Function<JSONObject, AttachmentArchiveEntry> FROM_JSON = new Function<JSONObject, AttachmentArchiveEntry>()
    {
        @Override
        public AttachmentArchiveEntry apply(final JSONObject jsonObject)
        {
            try
            {
                return new AttachmentArchiveEntryBuilder()
                        .entryIndex(jsonObject.getInt("entryIndex"))
                        .name(jsonObject.getString("name"))
                        .size(jsonObject.getLong("size"))
                        .mediaType(jsonObject.getString("mediaType"))
                        .build();
            }
            catch (final JSONException e)
            {
                throw new RuntimeException(e);
            }
        }
    };
    private final List<AttachmentArchiveEntry> entries;

    private final int totalEntryCount;

    public AttachmentArchiveImpl(final List<AttachmentArchiveEntry> entries, final int totalEntryCount)
    {
        this.entries = entries;
        this.totalEntryCount = totalEntryCount;
    }

    @Override
    public List<AttachmentArchiveEntry> getEntries()
    {
        return entries;
    }

    @Override
    @JsonIgnore
    public boolean isMoreAvailable()
    {
        return totalEntryCount > entries.size();
    }

    @Override
    @JsonIgnore
    public int getTotalNumberOfEntriesAvailable()
    {
        return getTotalEntryCount();
    }

    @Override
    public int getTotalEntryCount()
    {
        return totalEntryCount;
    }

    @Override
    public int hashCode() {return Objects.hashCode(entries, totalEntryCount);}

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()) {return false;}
        final AttachmentArchiveImpl other = (AttachmentArchiveImpl) obj;
        return Objects.equal(this.entries, other.entries) && Objects.equal(this.totalEntryCount, other.totalEntryCount);
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this)
                .add("entries", entries)
                .add("totalEntryCount", totalEntryCount)
                .toString();
    }
}
