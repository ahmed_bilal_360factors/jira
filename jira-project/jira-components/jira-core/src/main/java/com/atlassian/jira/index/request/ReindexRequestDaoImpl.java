package com.atlassian.jira.index.request;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.jira.entity.Delete;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.Select;

/**
 * @since 6.4
 */
public class ReindexRequestDaoImpl implements ReindexRequestDao
{
    private final EntityEngine entityEngine;

    public ReindexRequestDaoImpl(EntityEngine entityEngine)
    {
        this.entityEngine = entityEngine;
    }

    @Nonnull
    @Override
    public List<ReindexRequestBase> getRequestsWithStatus(@Nonnull ReindexStatus status)
    {
        return Select.from(Entity.REINDEX_REQUEST)
                .whereEqual(ReindexRequestBase.STATUS, status.name())
                .orderBy(ReindexRequestBase.REQUEST_TIME)
                .runWith(entityEngine)
                .asList();
    }

    @Nonnull
    @Override
    public ReindexRequestBase writeRequest(@Nonnull ReindexRequestBase request)
    {
        //Check if we need to create or update
        if (request.getId() == null)
        {
            return entityEngine.createValue(Entity.REINDEX_REQUEST, request);
        }
        else
        {
            entityEngine.updateValue(Entity.REINDEX_REQUEST, request);
            return request;
        }
    }

    @Nonnull
    @Override
    public List<ReindexComponent> getComponentsForRequest(long requestId)
    {
        return Select.from(Entity.REINDEX_COMPONENT)
                .whereEqual(ReindexComponent.REQUEST_ID, requestId)
                .orderBy(ReindexComponent.ID)
                .runWith(entityEngine)
                .asList();
    }

    @Nonnull
    @Override
    public ReindexComponent writeComponent(@Nonnull ReindexComponent component)
    {
        return entityEngine.createValue(Entity.REINDEX_COMPONENT, component);
    }

    @Override
    public void removeComponents(long requestId)
    {
        Delete.from(Entity.REINDEX_COMPONENT)
                .whereEqual(ReindexComponent.REQUEST_ID, requestId)
                .execute(entityEngine);
    }

    @Nullable
    @Override
    public ReindexRequestBase findRequestById(long requestId)
    {
        return Select.from(Entity.REINDEX_REQUEST)
                .whereEqual(ReindexRequest.ID, requestId)
                .runWith(entityEngine)
                .singleValue();
    }

    @Override
    public void removeAllPendingRequests()
    {
        Delete.from(Entity.REINDEX_REQUEST)
                .whereEqual(ReindexRequestBase.STATUS, ReindexStatus.PENDING.name()).execute(entityEngine);
    }
}
