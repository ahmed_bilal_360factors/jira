package com.atlassian.jira.model.querydsl;

import javax.annotation.Generated;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;

import org.ofbiz.core.entity.GenericValue;

/**
 * Data Transfer Object for the IssueLink entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 *
 * @see QIssueLink
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class IssueLinkDTO
{
    private Long id;
    private Long linktype;
    private Long source;
    private Long destination;
    private Long sequence;

    public Long getId()
    {
        return id;
    }

    public Long getLinktype()
    {
        return linktype;
    }

    public Long getSource()
    {
        return source;
    }

    public Long getDestination()
    {
        return destination;
    }

    public Long getSequence()
    {
        return sequence;
    }

    public IssueLinkDTO(Long id, Long linktype, Long source, Long destination, Long sequence)
    {
        this.id = id;
        this.linktype = linktype;
        this.source = source;
        this.destination = destination;
        this.sequence = sequence;
    }
    /**
    * Creates a GenericValue object from the values in this Data Transfer Object.
    * <p>
    *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
    * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
    * @return a GenericValue object constructed from the values in this Data Transfer Object.
    */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator)
    {
        return ofBizDelegator.makeValue("IssueLink", new FieldMap()
                        .add("id", id)
                        .add("linktype", linktype)
                        .add("source", source)
                        .add("destination", destination)
                        .add("sequence", sequence)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */

    public static IssueLinkDTO fromGenericValue(GenericValue gv)
    {
        return new IssueLinkDTO(
            gv.getLong("id"),
            gv.getLong("linktype"),
            gv.getLong("source"),
            gv.getLong("destination"),
            gv.getLong("sequence")
        );
    }
}

