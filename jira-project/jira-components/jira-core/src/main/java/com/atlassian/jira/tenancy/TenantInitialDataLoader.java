package com.atlassian.jira.tenancy;

import javax.servlet.ServletContext;

import com.atlassian.tenancy.api.Tenant;

/**
 * If a tenant needs to load some initial data it should provide a loader.  You should avoid tenant specific initial data
 * where possible, but if you cannot avoid having to tenant initialisation data then you should provide an implementation of this
 * interface and register it using {@code}TenantSetupRegistrar.registerTenantInitialDataLoader}
 *
 * You should also make your loader idempotent as the start will be invoked every time a tenant arrives
 *
 * @since v6.4
 */
public interface TenantInitialDataLoader
{
    void start(Tenant tenant);
}
