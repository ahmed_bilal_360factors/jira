package com.atlassian.jira.jql.query;

import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.util.JqlLocalDateSupport;
import com.atlassian.query.clause.TerminalClause;
import org.apache.lucene.search.Query;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Creates clauses for queries on the due date field.
 *
 * @since v6.4
 */
public class WorklogDateClauseQueryFactory extends WorklogClauseQueryFactory
{
    private final LocalDateClauseQueryFactory localDateClauseQueryFactory;

    public WorklogDateClauseQueryFactory(final JqlLocalDateSupport jqlLocalDateSupport,
            final JqlOperandResolver jqlOperandResolver,
            final IssueIdFilterQueryFactory issueIdFilterQueryFactory,
            final QueryProjectRoleAndGroupPermissionsDecorator queryPermissionsDecorator)
    {
        super(issueIdFilterQueryFactory, queryPermissionsDecorator);
        this.localDateClauseQueryFactory = new LocalDateClauseQueryFactory(SystemSearchConstants.forWorklogDate(), notNull("jqlLocalDateSupport", jqlLocalDateSupport), notNull("jqlOperandResolver", jqlOperandResolver));
    }

    @Override
    public Query getWorklogQuery(final QueryCreationContext queryCreationContext, final TerminalClause terminalClause)
    {
        return localDateClauseQueryFactory.getQuery(queryCreationContext, terminalClause).getLuceneQuery();
    }
}
