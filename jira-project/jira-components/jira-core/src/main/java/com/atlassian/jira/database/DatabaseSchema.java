package com.atlassian.jira.database;

import com.atlassian.fugue.Option;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.DatabaseConfigurationService;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;

/**
 * Exposes the Database schema as configured in dbconfig.xml
 *
 * @since v6.4.4
 */
public class DatabaseSchema
{
    private static Option<String> schemaName;

    public static String getSchemaName()
    {
        // DatabaseConfigurationManager.getDatabaseConfiguration() will throw a RuntimeException if you call it before
        // we have a configured DB config (i.e on a fresh install before DB Setup).
        // Once it actually returns successfully, the schema name will not change.
        if (schemaName == null)
        {
            schemaName = Option.option(ComponentAccessor.getComponent(DatabaseConfigurationService.class).getSchemaName());
        }
        return schemaName.getOrElse("");
    }
}
