package com.atlassian.jira.bc.dashboard.item.property;

import com.atlassian.fugue.Function2;
import com.atlassian.fugue.Iterables;
import com.atlassian.fugue.Option;
import com.atlassian.gadgets.GadgetId;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.dashboard.DashboardNotFoundException;
import com.atlassian.gadgets.dashboard.DashboardState;
import com.atlassian.gadgets.dashboard.spi.DashboardPermissionService;
import com.atlassian.gadgets.dashboard.spi.DashboardStateStore;
import com.atlassian.gadgets.dashboard.spi.DashboardStateStoreException;
import com.atlassian.jira.bc.dashboard.DashboardItem;
import com.atlassian.jira.bc.dashboard.DashboardItems;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.entity.property.EntityPropertyHelper;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.event.dashboard.item.property.DashboardItemPropertyDeletedEvent;
import com.atlassian.jira.event.dashboard.item.property.DashboardItemPropertySetEvent;
import com.atlassian.jira.event.entity.EntityPropertyDeletedEvent;
import com.atlassian.jira.event.entity.EntityPropertySetEvent;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.InjectableComponent;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.base.Function;
import com.google.common.base.Predicate;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;

@InjectableComponent
public final class DashboardItemPropertyHelper implements EntityPropertyHelper<DashboardItem>
{
    private final DashboardPermissionService dashboardPermissionService;
    private final DashboardStateStore dashboardStateStore;

    private final CheckPermissionFunction<DashboardItem> hasEditPermissionFunction = new CheckPermissionFunction<DashboardItem>()
    {
        @Override
        public ErrorCollection apply(final ApplicationUser applicationUser, final DashboardItem dashboardItem)
        {
            return errorCollectionIfFalse(
                    dashboardPermissionService.isWritableBy(dashboardItem.getLocator().getDashboardId(), username(applicationUser)),
                    "user " + name(applicationUser) + " doesn't have permission to edit dashboard item " + dashboardItem.getId());
        }
    };

    private final CheckPermissionFunction<DashboardItem> hasReadPermissionFunction = new CheckPermissionFunction<DashboardItem>()
    {
        @Override
        public ErrorCollection apply(final ApplicationUser applicationUser, final DashboardItem dashboardItem)
        {
            return errorCollectionIfFalse(
                    dashboardPermissionService.isReadableBy(dashboardItem.getLocator().getDashboardId(), username(applicationUser)),
                    "user " + name(applicationUser) + " doesn't have permission to access dashboard item " + dashboardItem.getId());
        }
    };

    private String username(final ApplicationUser applicationUser) { return applicationUser != null ? applicationUser.getUsername() : null; }

    private String name(final ApplicationUser applicationUser) { return applicationUser != null ? applicationUser.getName() : null; }

    private final Function<Long, Option<DashboardItem>> getEntityByIdFunction = new Function<Long, Option<DashboardItem>>()
    {
        @Override
        public Option<DashboardItem> apply(final Long itemId)
        {
            try
            {
                return option(dashboardStateStore.findDashboardWithGadget(GadgetId.valueOf(String.valueOf(itemId)))).flatMap(new Function<DashboardState, Option<DashboardItem>>()
                {
                    @Override
                    public Option<DashboardItem> apply(final DashboardState dashboard)
                    {
                        Iterable<DashboardItem> allItemsInDashboard = transform(concat(dashboard.getColumns()), new Function<GadgetState, DashboardItem>()
                        {
                            @Override
                            public DashboardItem apply(final GadgetState gadgetState)
                            {
                                return DashboardItems.fromGadgetState(gadgetState, dashboard.getId());
                            }
                        });
                        return Iterables.findFirst(allItemsInDashboard, new Predicate<DashboardItem>()
                        {
                            @Override
                            public boolean apply(final DashboardItem dashboardItem)
                            {
                                return dashboardItem.getId().equals(itemId);
                            }
                        });
                    }
                });
            }
            catch (DashboardStateStoreException ex)
            {
                return none();
            }
            catch (DashboardNotFoundException ex)
            {
                return none();
            }
        }
    };

    private final Function2<ApplicationUser, EntityProperty, EntityPropertySetEvent> createSetPropertyEventFunction = new Function2<ApplicationUser, EntityProperty, EntityPropertySetEvent>()
    {
        @Override
        public EntityPropertySetEvent apply(final ApplicationUser applicationUser, final EntityProperty entityProperty)
        {
            return new DashboardItemPropertySetEvent(entityProperty, applicationUser);
        }
    };

    private final Function2<ApplicationUser, EntityProperty, EntityPropertyDeletedEvent> createDeletePropertyEventFunction = new Function2<ApplicationUser, EntityProperty, EntityPropertyDeletedEvent>()
    {
        @Override
        public EntityPropertyDeletedEvent apply(final ApplicationUser applicationUser, final EntityProperty entityProperty)
        {
            return new DashboardItemPropertyDeletedEvent(entityProperty, applicationUser);
        }
    };

    public DashboardItemPropertyHelper(final DashboardPermissionService dashboardPermissionService, final DashboardStateStore dashboardStateStore)
    {
        this.dashboardPermissionService = dashboardPermissionService;
        this.dashboardStateStore = dashboardStateStore;
    }

    @Override
    public CheckPermissionFunction<DashboardItem> hasEditPermissionFunction()
    {
        return hasEditPermissionFunction;
    }

    @Override
    public CheckPermissionFunction<DashboardItem> hasReadPermissionFunction()
    {
        return hasReadPermissionFunction;
    }

    @Override
    public Function<Long, Option<DashboardItem>> getEntityByIdFunction()
    {

        return getEntityByIdFunction;
    }

    @Override
    public Function2<ApplicationUser, EntityProperty, ? extends EntityPropertySetEvent> createSetPropertyEventFunction()
    {
        return createSetPropertyEventFunction;
    }

    @Override
    public Function2<ApplicationUser, EntityProperty, ? extends EntityPropertyDeletedEvent> createDeletePropertyEventFunction()
    {
        return createDeletePropertyEventFunction;
    }

    @Override
    public EntityPropertyType getEntityPropertyType()
    {
        return EntityPropertyType.DASHBOARD_ITEM_PROPERTY;
    }

    private ErrorCollection errorCollectionIfFalse(final boolean hasPermission, String message)
    {
        return hasPermission ? new SimpleErrorCollection() : new SimpleErrorCollection(message, ErrorCollection.Reason.FORBIDDEN);
    }
}
