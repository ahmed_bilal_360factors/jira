package com.atlassian.jira.dashboard.permission;

import com.atlassian.fugue.Option;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.Vote;
import com.atlassian.gadgets.dashboard.DashboardState;
import com.atlassian.gadgets.dashboard.spi.DashboardPermissionService;
import com.atlassian.gadgets.plugins.GadgetLocationTranslator;
import com.atlassian.gadgets.plugins.PluginGadgetSpec;
import com.atlassian.jira.plugin.webfragment.DefaultWebFragmentContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.gadgets.Vote.ALLOW;
import static com.atlassian.gadgets.Vote.DENY;
import static com.atlassian.jira.component.ComponentAccessor.getOSGiComponentInstanceOfType;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

public class JiraGadgetPermissionManager implements GadgetPermissionManager
{
    private static final Logger log = LoggerFactory.getLogger(JiraGadgetPermissionManager.class);

    private static final String LOGIN_GADGET_KEY = "login-gadget";
    private static final String LOGIN_GADGET_PLUGIN_KEY = "com.atlassian.jira.gadgets";

    //match two groups in a string starting with anything, then 'rest/gadgets/' then anything then '/g/' then match all except '/' then ':' then match all except '/' then anything.
    private static final Pattern PLUGIN_KEY_PATTERN = Pattern.compile(".*rest\\/gadgets\\/.*\\/g\\/([^\\/]+):([^\\/]+).*", Pattern.CASE_INSENSITIVE);

    private final PermissionManager permissionManager;
    private final PluginAccessor pluginAccessor;
    private final DashboardPermissionService permissionService;

    public JiraGadgetPermissionManager(
            final PermissionManager permissionManager,
            final PluginAccessor pluginAccessor,
            final DashboardPermissionService permissionService)
    {
        this.permissionManager = permissionManager;
        this.pluginAccessor = pluginAccessor;
        this.permissionService = permissionService;
    }

    @Override
    public Vote voteOn(final PluginGadgetSpec pluginGadgetSpec, final ApplicationUser remoteUser)
    {
        notNull("pluginGadgetSpec", pluginGadgetSpec);

        //special case for the login gadget.  It should be hidden for logged in users and shown for logged out user.
        if (pluginGadgetSpec.getModuleKey().equals(LOGIN_GADGET_KEY) && pluginGadgetSpec.getPluginKey().equals(LOGIN_GADGET_PLUGIN_KEY))
        {
            return remoteUser == null ? ALLOW : DENY;
        }

        if (!evaluateConditions(pluginGadgetSpec))
        {
            return DENY;
        }

        final String roleString = pluginGadgetSpec.getParameter("roles-required");
        if (StringUtils.isBlank(roleString))
        {
            return ALLOW;
        }

        //admins get to see all gadgets, so that they'll show up in the 'Default Dashboard' section in the admin section
        if (permissionManager.hasPermission(Permissions.ADMINISTER, remoteUser))
        {
            return ALLOW;
        }

        final String[] roles = StringUtils.split(roleString);
        for (String role : roles)
        {
            final int permission = Permissions.getType(role);
            if (permission == -1)
            {
                log.warn("Invalid role-required specified for gadget '" + pluginGadgetSpec.getKey() + "': '" + role + "'");
                return Vote.PASS;
            }
            if (Permissions.isGlobalPermission(permission))
            {
                if (!permissionManager.hasPermission(permission, remoteUser))
                {
                    return DENY;
                }
            }
            else
            {
                if (!hasProjectsPermission(permission, remoteUser))
                {
                    return DENY;
                }
            }
        }
        return ALLOW;
    }

    @Override
    public DashboardState filterGadgets(final DashboardState dashboardState, final ApplicationUser remoteUser)
    {
        notNull("dashboardState", dashboardState);

        boolean isWritable = permissionService.isWritableBy(dashboardState.getId(), remoteUser == null ? null : remoteUser.getName());
        if (isWritable)
        {
            return dashboardState;
        }

        //read only dashboard.  Remove any gadgets user doesn't have permission to see.
        final List<List<GadgetState>> columns = new ArrayList<List<GadgetState>>();
        for (Iterable<GadgetState> columnIterable : dashboardState.getColumns())
        {
            final List<GadgetState> column = new ArrayList<GadgetState>();
            for (GadgetState state : columnIterable)
            {
                Option<PluginGadgetSpec> gadgetSpecResult = getPluginGadgetSpec(state.getGadgetSpecUri());
                if (!gadgetSpecResult.isEmpty())
                {
                    if (DENY.equals(voteOn(gadgetSpecResult.get(), remoteUser)))
                    {
                        //skip this gadget if local conditions evaluate to false or other permission provider voted deny.
                        continue;
                    }
                }
                column.add(state);
            }
            columns.add(column);
        }
        return DashboardState.dashboard(dashboardState).columns(columns).build();
    }

    @Override
    public Option<PluginGadgetSpec> getPluginGadgetSpec(URI gadgetUri)
    {
        if (gadgetUri == null)
        {
            return Option.none();
        }

        URI gadgetSpecUri = getGadgetLocationTranslator().translate(gadgetUri);
        String moduleKey = extractModuleKey(gadgetSpecUri.toASCIIString());
        return moduleKey == null ? Option.<PluginGadgetSpec>none() : getPluginGadgetSpecFromModuleKey(moduleKey);
    }

    @VisibleForTesting
    String extractModuleKey(final String gadgetUri)
    {
        final Matcher matcher = PLUGIN_KEY_PATTERN.matcher(gadgetUri);
        if (matcher.matches() && matcher.groupCount() == 2)
        {
            return matcher.group(1) + ":" + matcher.group(2);
        }
        return null;
    }

    private Option<PluginGadgetSpec> getPluginGadgetSpecFromModuleKey(String completeGadgetModuleKey)
    {
        final ModuleDescriptor<?> moduleDescriptor = pluginAccessor.getEnabledPluginModule(completeGadgetModuleKey);
        if (moduleDescriptor == null)
        {
            return Option.none();
        }

        Object module = moduleDescriptor.getModule();
        if (module instanceof PluginGadgetSpec)
        {
            return Option.some((PluginGadgetSpec) module);
        }
        return Option.none();
    }

    private boolean evaluateConditions(PluginGadgetSpec gadgetSpec)
    {
        Map<String, Object> context = DefaultWebFragmentContext.get();
        return gadgetSpec.getEnabledCondition().shouldDisplay(context) &&
               gadgetSpec.getLocalCondition().shouldDisplay(context);
    }

    private boolean hasProjectsPermission(int permission, ApplicationUser user)
    {
        try
        {
            return permissionManager.hasProjects(new ProjectPermissionKey(permission), user);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    private GadgetLocationTranslator getGadgetLocationTranslator()
    {
        return getOSGiComponentInstanceOfType(GadgetLocationTranslator.class);
    }
}
