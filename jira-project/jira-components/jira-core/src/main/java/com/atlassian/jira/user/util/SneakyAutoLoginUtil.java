package com.atlassian.jira.user.util;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.login.LoginStore;
import com.atlassian.jira.security.websudo.InternalWebSudoManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.seraph.auth.DefaultAuthenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Principal;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;

/**
 * Try to log a user in automatically behind-the-scenes without knowing their password.
 *
 * @since v5.0.4
 */
public class SneakyAutoLoginUtil
{
    private static final Logger log = LoggerFactory.getLogger(SneakyAutoLoginUtil.class);

    /**
     * Attempt to authenticate the user with JIRA and return their details if successful.
     *
     * @param username The name of the user to log in.
     * @param request The request where the user will attempt to add authentication to
     * @return the {@link com.atlassian.jira.user.ApplicationUser} that was authenticated, or null if
     * something went wrong.
     */
    @Nullable
    public static ApplicationUser logUserInByName(final String username, final HttpServletRequest request)
    {
        final UserManager userManager = ComponentAccessor.getUserManager();
        final ApplicationUser user = userManager.getUserByName(username);
        final User directoryUser = user == null ? null : user.getDirectoryUser();
        final boolean autoLoginWorked = logUserIn(directoryUser, request);
        if (!autoLoginWorked)
        {
            // The user was not automatically logged in, so return no user object.
            return null;
        }
        /**
         * We want to note that the user has been logged in, so that things like
         * the {@link com.atlassian.jira.onboarding.OnboardingService} can inspect
         * the login count and make appropriate decisions.
         * @see <a href="https://jdog.jira-dev.com/browse/JDEV-32063">JDEV-32063</a>
         */
        try
        {
            LoginStore loginStore = ComponentAccessor.getComponent(LoginStore.class);
            Long loginCount = loginStore.getLoginInfo(directoryUser).getLoginCount();
            if (loginCount == null || loginCount == 0)
            {
                loginStore.recordLoginAttempt(directoryUser, true);
            }
        }
        catch (Exception e)
        {
            log.error("Could not record the login attempt for user '" + username + "'.", e);
        }
        return user;
    }

    public static boolean logUserIn(final String username, final HttpServletRequest request)
    {
        return logUserInByName(username, request) == null;
    }

    public static boolean logUserIn(final User user, final HttpServletRequest request)
    {
        if (user == null)
        {
            log.warn("Unable to automatically log in: user is null");
        }
        else
        {
            try
            {
                final CrowdService crowdService = ComponentAccessor.getCrowdService();
                if (crowdService == null)
                {
                    log.warn("Unable to automatically log in: crowdService is null");
                }
                else
                {
                    // Log the user in by populating the appropriate session attributes.
                    final Principal principal = ComponentAccessor.getUserManager().getUserByName(user.getName());
                    request.getSession().setAttribute(DefaultAuthenticator.LOGGED_IN_KEY, principal);
                    request.getSession().setAttribute(DefaultAuthenticator.LOGGED_OUT_KEY, null);

                    InternalWebSudoManager webSudoManager = ComponentAccessor.getComponent(InternalWebSudoManager.class);
                    webSudoManager.markWebSudoRequest(request);

                    return true;
                }
            }
            catch (final Exception e)
            {
                log.warn("Error with automatic log in. The user will need to log in manually.", e);
            }
        }

        return false;
    }
}
