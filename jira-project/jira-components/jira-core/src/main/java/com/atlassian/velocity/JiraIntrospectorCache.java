package com.atlassian.velocity;

import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.jira.component.ComponentAccessor;

import org.apache.velocity.runtime.log.Log;
import org.apache.velocity.util.introspection.ClassMap;
import org.apache.velocity.util.introspection.IntrospectorCache;

/**
 * JIRA velocity introspector cache that uses atlassian cache to store loaded classes.
 *
 * @since v6.4
 */
public class JiraIntrospectorCache implements IntrospectorCache
{
    private final Cache<Class<?>, ClassMap> classMapCache;

    @SuppressWarnings ("UnusedParameters")
    public JiraIntrospectorCache(final Log log)
    {
        //this is created by velocity so we cannot inject cache manager directly
        final CacheManager cacheManager = ComponentAccessor.getComponentOfType(CacheManager.class);

        classMapCache = cacheManager.getCache(JiraIntrospectorCache.class.getName() + ".cache", new CacheLoader<Class<?>, ClassMap>()
        {
            @Override
            @Nonnull
            public ClassMap load(@Nonnull final Class<?> key)
            {
                return new CachingJiraClassMap(new JiraClassMap(key));
            }
        }, new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).build());
    }

    @Override
    public void clear()
    {
        classMapCache.removeAll();
    }

    @Override
    public ClassMap get(final Class clazz)
    {
        return classMapCache.get(clazz);
    }

    @Override
    public ClassMap put(final Class clazz)
    {
        return classMapCache.get(clazz);
    }

}
