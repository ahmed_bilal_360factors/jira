package com.atlassian.jira.issue.attachment;

import org.ofbiz.core.entity.DelegatorInterface;

import static com.atlassian.jira.issue.attachment.AttachmentConstants.ATTACHMENT_ENTITY_NAME;

/**
 * @since v6.4
 */
public class AttachmentIdSequencer
{
    private final DelegatorInterface delegatorInterface;

    public AttachmentIdSequencer(final DelegatorInterface delegatorInterface)
    {
        this.delegatorInterface = delegatorInterface;
    }

    public Long getNextId()
    {
        return delegatorInterface.getNextSeqId(ATTACHMENT_ENTITY_NAME);
    }
}
