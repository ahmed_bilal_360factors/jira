package com.atlassian.jira.service.services.analytics;

import java.util.Map;

/**
 * Interface to add information in the Jira Start Analytic event
 */
public interface JiraAnalyticTask
{

    /**
     * Method where the task will grab all the classes
     * from the context
     */
    void init();

    /**
     * Calculates the analytics and return a map with
     * all the properties.
     *
     * @return a map with the properties to add to the start event
     */
    public Map<String,Object> getAnalytics();

    /**
     * Return true if this task reports JIRA database size any usage statistics.
     * If true it will be included into <database-statistics> section of Support ZIP
     * by code in {@link com.atlassian.jira.util.system.ExtendedSystemInfoUtilsImpl#getUsageStats()}.
     */
    boolean isReportingDataShape();
}
