package com.atlassian.jira.service.services.analytics.start;

import java.util.Map;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.service.services.analytics.JiraAnalyticTask;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.collect.MapBuilder;

import com.google.common.annotations.VisibleForTesting;

/**
 * Gets the information of active users / total users
 */
public class UserAnalyticTask implements JiraAnalyticTask
{

    private UserUtil userUtil;


    public UserAnalyticTask()
    {
    }

    @VisibleForTesting
    UserAnalyticTask(final UserUtil userUtil)
    {
        this.userUtil = userUtil;
    }

    @Override
    public void init()
    {
        this.userUtil = ComponentAccessor.getUserUtil();
    }

    @Override
    public Map<String, Object> getAnalytics()
    {
        final MapBuilder<String, Object> builder = MapBuilder.newBuilder();

        builder.add("users.total", userUtil.getTotalUserCount());
        builder.add("users.active", userUtil.getActiveUserCount());

        return builder.toMap();
    }

    @Override
    public boolean isReportingDataShape()
    {
        return true;
    }
}
