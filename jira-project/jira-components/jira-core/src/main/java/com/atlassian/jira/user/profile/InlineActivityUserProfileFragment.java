package com.atlassian.jira.user.profile;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.util.JiraUrlCodec;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.jira.web.bean.I18nBean;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Uses the new inline activity stream to render a user's activity.
 *
 * @since v6.4
 */
public class InlineActivityUserProfileFragment implements UserProfileFragment
{
    public static final String INLINE_ACTIVITY_STREAM_FEATURE_KEY = "com.atlassian.streams.InlineActivityStream";
    private static final String INLINE_ACTIVITY_STREAM_WEB_RESOURCE = "com.atlassian.streams:inline-activity-stream-resources";
    private static final Logger log = LoggerFactory.getLogger(InlineActivityUserProfileFragment.class);

    private final PluginAccessor pluginAccessor;
    private final I18nBean.BeanFactory i18nFactory;
    private final SoyTemplateRenderer soyTemplateRenderer;

    public InlineActivityUserProfileFragment(PluginAccessor pluginAccessor, I18nBean.BeanFactory i18nFactory, SoyTemplateRenderer soyTemplateRenderer)
    {
        this.pluginAccessor = pluginAccessor;
        this.i18nFactory = i18nFactory;
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    @Override
    public boolean showFragment(final User profileUser, final User currentUser)
    {
        return pluginAccessor.isPluginModuleEnabled(INLINE_ACTIVITY_STREAM_WEB_RESOURCE);
    }

    @Override
    public String getFragmentHtml(final User profileUser, final User currentUser)
    {
        ImmutableMap.Builder<String, Object> context = ImmutableMap.builder();

        String activityUrl = getRequestContextPath() + "/activity?maxResults=10&streams=user+IS+" + getJiraUrlEncoded(profileUser.getName());
        context.put("activityStreamUrl", activityUrl);
        try
        {
            return soyTemplateRenderer.render("jira.webresources:inline-activity-stream-soy-templates", "JIRA.Templates.InlineActivityStream.renderGadget", context.build());
        }
        catch (SoyException e)
        {
            log.error("Could not render soy template for inline activity stream");
            log.debug("Exception: ", e);
        }
        return "";
    }

    @Override
    public String getId()
    {
        return "inline-activity-profile-fragment";
    }

    @VisibleForTesting
    String getRequestContextPath()
    {
        return ExecutingHttpRequest.get().getContextPath();
    }

    @VisibleForTesting
    String getJiraUrlEncoded(String user)
    {
        return JiraUrlCodec.encode(user);
    }
}
