package com.atlassian.jira.upgrade;

/**
 * Provides parameters required to perform JIRA upgrade.
 * <ul>
 * <li>setupMode - {@code true} if this is the initial setup. {@code false} if it is a normal upgrade</li>
 * <li>allowReindex - {@code true} if after all upgrade tasks reindexing is allowed. {@code false} if after all upgrade tasks shouldn't be any reindexing</li>
 * </ul>
 * Clients should use the provided {@link UpgradeManagerParams.Builder} to construct an instance of this class.
 *
 * @since v6.4
 */
public class UpgradeManagerParams
{
    private final boolean setupMode;
    private final boolean allowReindex;
    private final boolean withoutDelay;

    private UpgradeManagerParams(final boolean setupMode, final boolean allowReindex, final boolean withoutDelay)
    {
        this.setupMode = setupMode;
        this.allowReindex = allowReindex;
        this.withoutDelay = withoutDelay;
    }

    public boolean isSetupMode()
    {
        return setupMode;
    }

    public boolean isAllowReindex()
    {
        return allowReindex;
    }

    public boolean isWithoutDelay()
    {
        return withoutDelay;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final UpgradeManagerParams that = (UpgradeManagerParams) o;

        if (allowReindex != that.allowReindex) { return false; }
        //noinspection RedundantIfStatement
        if (setupMode != that.setupMode) { return false; }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = (setupMode ? 1 : 0);
        result = 31 * result + (allowReindex ? 1 : 0);
        return result;
    }

    public static class Builder
    {
        private boolean setupMode = false;
        private boolean allowReindex = true;
        private boolean withoutDelay = false;

        public Builder setSetupMode(boolean setupMode)
        {
            this.setupMode = setupMode;
            return this;
        }

        public Builder setAllowReindex(boolean allowReindex)
        {
            this.allowReindex = allowReindex;
            return this;
        }

        public Builder setWithoutDelay(boolean withoutDelay)
        {
            this.withoutDelay = withoutDelay;
            return this;
        }

        public Builder withSetupMode()
        {
            this.setupMode = true;
            return this;
        }

        public Builder withoutReindexAllowed()
        {
            this.allowReindex = false;
            return this;
        }

        public Builder withoutDelay()
        {
            this.withoutDelay = true;
            return this;
        }

        public UpgradeManagerParams build()
        {
            return new UpgradeManagerParams(setupMode, allowReindex, withoutDelay);
        }
    }
}
