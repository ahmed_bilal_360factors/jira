package com.atlassian.jira.issue.fields.config;

/**
 * Represents the "ConfigurationContext" Entity in the DB.
 * <p>
 * This entity is a mapping from a "FieldConfigScheme" to the list of Projects that are relevant to that FieldConfigScheme.
 * A single entity with a null Project represents the "Global" context - any projects not explicitly listed in another context.
 * <p>
 * Note that there is also room here for a "ProjectCategory" context, but we don't actually expose this through the UI,
 * and we should deprecate this ability in the API just in case some crazy plugin is using it.
 */
public class ConfigurationContext
{
    //    <field name="id" type="numeric"/>
    //    <field name="projectcategory" type="numeric"/>
    //    <field name="project" type="numeric"/>
    //    <field name="key" col-name="customfield" type="long-varchar"/>
    //    <field name="fieldconfigscheme" type="numeric"/>
    public static final String ID = "id";
    public static final String PROJECT_CATEGORY_ID = "projectcategory";
    public static final String PROJECT_ID = "project";
    /** This field is called "customfield" in the DB and "key" in OfBiz Entity, but "Field ID" is what it actually stores. */
    public static final String FIELD_ID = "key";
    public static final String FIELD_CONFIG_SCHEME_ID = "fieldconfigscheme";

    private final Long id;
    private final Long projectCategoryId;
    private final Long projectId;
    private final String fieldId;
    private final Long fieldConfigSchemeId;

    public ConfigurationContext(final Long id, final Long projectCategoryId, final Long projectId, final String fieldId, final Long fieldConfigSchemeId) {
        this.id = id;
        this.projectCategoryId = projectCategoryId;
        this.projectId = projectId;
        this.fieldId = fieldId;
        this.fieldConfigSchemeId = fieldConfigSchemeId;
    }

    public Long getId()
    {
        return id;
    }

    public Long getProjectCategoryId()
    {
        return projectCategoryId;
    }

    public Long getProjectId()
    {
        return projectId;
    }

    /**
     * Returns the "fieldId" field which is actually called "customfield" in the DB and "key" in OfBiz Entity.
     * <p>
     *     It is in fact the Configurable Field ID eg "customfield_11002" or "issuetype", so we have given a more correct name here.
     *
     * @return the "fieldId" field which is actually called "customfield" in the DB and "key" in OfBiz Entity.
     */
    public String getFieldId()
    {
        return fieldId;
    }


    public Long getFieldConfigSchemeId()
    {
        return fieldConfigSchemeId;
    }
}
