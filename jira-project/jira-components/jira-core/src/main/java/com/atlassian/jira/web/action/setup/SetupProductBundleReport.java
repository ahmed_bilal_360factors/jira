package com.atlassian.jira.web.action.setup;

import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.util.FileFactory;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;

/**
 * Display report page if there was an error during attempt to save license for chosen bundle
 *
 * @since v6.3
 */
public class SetupProductBundleReport extends AbstractSetupAction
{
    private final String REPORT = "report";

    private final SetupProductBundleHelper productBundleHelper;
    private final VelocityRequestContextFactory velocityRequestContextFactory;

    private String src;

    public SetupProductBundleReport(FileFactory fileFactory,
            SetupProductBundleHelper setupProductBundleHelper,
            VelocityRequestContextFactory velocityRequestContextFactory,
            JiraProperties jiraProperties)
    {
        super(fileFactory, jiraProperties);
        this.velocityRequestContextFactory = velocityRequestContextFactory;
        this.productBundleHelper = setupProductBundleHelper;
    }

    @Override
    public String doDefault() throws Exception
    {
        if (!productBundleHelper.hasLicenseError())
        {
            return getRedirect(productBundleHelper.getSetupCompleteRedirectUrl(getLoggedInApplicationUser()));
        }

        return REPORT;
    }

    @Override
    protected String doExecute() throws Exception
    {
        // forget the error
        productBundleHelper.cleanLicenseError();
        // forget the bundle
        productBundleHelper.cleanSelectedBundle();

        return getRedirect(productBundleHelper.getSetupCompleteRedirectUrl(getLoggedInApplicationUser()));
    }

    public void setSrc(final String src)
    {
        this.src = src;
    }

    public String getSrc()
    {
        return src;
    }

    private String getRedirectToDashboard()
    {
        return getRedirect("Dashboard.jspa" + (src == null ? "" : "?src=" + src));
    }

    public String getUpmUrlForPlugin()
    {
        return velocityRequestContextFactory.getJiraVelocityRequestContext().getCanonicalBaseUrl() + "/plugins/servlet/upm/manage/user-installed#manage";
    }
}