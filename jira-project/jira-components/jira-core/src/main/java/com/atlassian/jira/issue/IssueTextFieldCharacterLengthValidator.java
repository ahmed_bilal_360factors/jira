package com.atlassian.jira.issue;

import java.util.Set;
import javax.annotation.Nonnull;

/**
 * Text length issue validator to be used by issue objects to be created/edited with IssueManager.
 * Checks if issue text fields are within the jira character limit.
 * Summary field is not validated (hardlimited to 255 chars).
 *
 * @see com.atlassian.jira.issue.IssueManager
 * @see com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator
 */
public interface IssueTextFieldCharacterLengthValidator
{

    /**
     * Validates length of text field against the "jira character limit". Checks values of text system fields and custom
     * field types marked with {@link com.atlassian.jira.issue.customfields.TextCustomFieldType} interface.
     *
     * @param issue the issue to be validated
     * @return validation result
     */
    @Nonnull
    ValidationResult validateAllFields(@Nonnull final Issue issue);

    /**
     * Validates length of modified text field against the "jira character limit". Checks values of text system fields and custom
     * field types marked with {@link com.atlassian.jira.issue.customfields.TextCustomFieldType} interface.
     *
     * @param issue the issue to be validated
     * @return validation result
     */
    @Nonnull
    ValidationResult validateModifiedFields(@Nonnull final MutableIssue issue);

    /**
     * Result of the validation.
     */
    public interface ValidationResult
    {

        /**
         * Returns whether the validation was successful or not.
         * If this methods returns {@code true} then the {@link #getInvalidFieldIds()} returns an empty set.
         *
         * @return true
         */
        boolean isValid();

        /**
         * Return the character limit value that was used to validate the issue
         * @return maximum number of characters allowed, 0 means unlimited
         */
        long getMaximumNumberOfCharacters();

        /**
         * Returns ids of fields that didn't pass the validation
         *
         * @return set containing ids of fields that exceed the limit or an empty set if no field exceeds the limit
         */
        @Nonnull
        public Set<String> getInvalidFieldIds();

    }

}


