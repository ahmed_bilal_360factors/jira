package com.atlassian.jira.upgrade.tasks;

import javax.annotation.Nullable;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Turn soap on for new installations
 *
 * @since v4.4
 */
public class UpgradeTask_Build642 extends AbstractImmediateUpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask_Build642.class);

    private final ApplicationProperties applicationProperties;

    public UpgradeTask_Build642(ApplicationProperties applicationProperties)
    {
        super();
        this.applicationProperties = applicationProperties;
    }

    public String getBuildNumber()
    {
    return "642";
    }

    public String getShortDescription()
    {
        return "Turn AllowRPC On for new installations only.";
    }

    public void doUpgrade(boolean setupMode) throws Exception
    {
        boolean allowRpc;
        if (setupMode)
        {
            allowRpc = true;
        }
        else
        {
            allowRpc = applicationProperties.getOption(APKeys.JIRA_OPTION_RPC_ALLOW);
        }
        applicationProperties.setOption(APKeys.JIRA_OPTION_RPC_ALLOW, allowRpc);
    }


    @Nullable
    @Override
    public String dependsUpon()
    {
        return "640";
    }

}
