package com.atlassian.jira.issue.thumbnail;

import com.atlassian.core.util.thumbnail.Thumber;
import com.atlassian.core.util.thumbnail.Thumbnail;
import com.atlassian.core.util.thumbnail.ThumbnailDimension;
import com.atlassian.core.util.thumbnail.ThumbnailUtil;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.ThumbnailConfiguration;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.AttachmentStore;
import com.atlassian.jira.issue.attachment.ThumbnailAccessor;
import com.atlassian.jira.util.io.InputStreamConsumer;
import com.atlassian.jira.util.mime.MimeManager;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Toolkit;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Collection;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static org.apache.commons.io.FileUtils.deleteQuietly;
import static org.apache.commons.io.FileUtils.touch;
import static org.apache.commons.io.IOUtils.closeQuietly;

/**
 * ThumbnailManager is a utility used to create and retrieve thumbnails for attachments.
 */
public class DefaultThumbnailManager implements ThumbnailManager
{
    private static final Logger log = LoggerFactory.getLogger(DefaultThumbnailManager.class);

    private static final String TEMP_FILE_SUFFIX = ".tmp";

    private final AttachmentManager attachmentManager;
    private final AttachmentStore attachmentStore;
    private final ThumbnailAccessor thumbnailAccessor;
    private final MimeManager mimeManager;
    private final ThumbnailConfiguration thumbnailConfig;
    private final VelocityRequestContextFactory velocityRequestContextFactory;
    private final ApplicationProperties applicationProperties;
    private final boolean toolkitAvailable;

    public DefaultThumbnailManager(final ThumbnailConfiguration thumbnailConfiguration,
            final AttachmentManager attachmentManager, final MimeManager mimeManager,
            final VelocityRequestContextFactory velocityRequestContextFactory, final AttachmentStore attachmentStore,
            final ThumbnailAccessor thumbnailAccessor, final ApplicationProperties applicationProperties)
    {
        this.attachmentManager = attachmentManager;
        this.attachmentStore = attachmentStore;
        this.thumbnailAccessor = thumbnailAccessor;
        this.mimeManager = mimeManager;
        this.thumbnailConfig = thumbnailConfiguration;
        this.velocityRequestContextFactory = velocityRequestContextFactory;
        this.applicationProperties = applicationProperties;
        //An <code>java.awt.AWTError</code> would get thrown if AWT is not available
        this.toolkitAvailable = Toolkit.getDefaultToolkit() != null;
    }

    @Override
    public Collection<Thumbnail> getThumbnails(final Issue issue, final User user)
    {
        final List<Attachment> attachments = attachmentManager.getAttachments(issue);
        final List<Thumbnail> thumbnails = Lists.newArrayListWithCapacity(attachments.size());
        for (final Attachment attachment : attachments)
        {
            if (isThumbnailable(issue, attachment))
            {
                thumbnails.add(doGetThumbnail(issue, attachment));
            }
        }
        return thumbnails;
    }

    @Override
    public boolean isThumbnailable(final Issue issue, final Attachment attachment)
    {
        // The attachment object may know this already, as once we have worked it out we store it in the database.
        if (attachment.isThumbnailable() != null)
        {
            return attachment.isThumbnailable();
        }

        // if toolkit is unavailable, we can't thumbnail the image.
        if (!checkToolkit())
        {
            return false;
        }

        // Check that file is of a valid image mime type
        // All thumbnails are saved in JPEG format on disk.
        final Promise<Boolean> isSupportedImage = attachmentStore.getAttachment(attachment, new Function<InputStream, Boolean>()
        {
            @Override
            public Boolean get(final InputStream input)
            {
                return new Thumber(MIME_TYPE).isFileSupportedImage(input);
            }
        }).recover(new com.google.common.base.Function<Throwable, Boolean>()
        {
            @Override
            public Boolean apply(final Throwable t)
            {
                return false;
            }
        });

        boolean thumbnailable = false;
        if (isSupportedImage.claim())
        {
            final String mimeType = mimeManager.getSuggestedMimeType(attachment.getFilename());
            thumbnailable = ThumbnailUtil.isMimeTypeSupported(mimeType);
        }
        attachmentManager.setThumbnailable(attachment, thumbnailable);
        return thumbnailable;
    }

    @Override
    public boolean isThumbnailable(final Attachment attachment)
    {
        return isThumbnailable(attachment.getIssueObject(), attachment);
    }

    @Override
    public Thumbnail getThumbnail(final Attachment attachment)
    {
        return getThumbnail(attachment.getIssueObject(), attachment);
    }

    @Override
    public Thumbnail getThumbnail(final Issue issue, final Attachment attachment)
    {
        if (!isThumbnailable(issue, attachment))
        {
            return null;
        }
        return doGetThumbnail(issue, attachment);
    }

    @Override
    public boolean checkToolkit()
    {
        return toolkitAvailable;
    }

    @Override
    public ThumbnailedImage toThumbnailedImage(@Nullable final Thumbnail thumbnail)
    {
        if (thumbnail == null)
        {
            return null;
        }
        return new AtlassianCoreThumbnail(applicationProperties,
                velocityRequestContextFactory.getJiraVelocityRequestContext(), thumbnail);
    }

    @Override
    public <T> T streamThumbnailContent(final Attachment attachment, final InputStreamConsumer<T> consumer)
            throws IOException
    {
        final File thumbnailFile = thumbnailAccessor.getThumbnailFile(attachment);
        InputStream inputStream = new FileInputStream(thumbnailFile);
        try
        {
            inputStream = new BufferedInputStream(inputStream);
            return consumer.withInputStream(inputStream);
        }
        finally
        {
            closeQuietly(inputStream);
        }
    }

    private Thumbnail doGetThumbnail(final Issue issue, final Attachment attachment)
    {
        final File thumbnailFile = getThumbnailFile(issue, attachment);

        final Thumbnail existingThumbnail = readThumbnail(attachment, thumbnailFile);
        if (existingThumbnail != null)
        {
            return existingThumbnail;
        }

        /*
            It's unlikely two threads will try to create the same thumbnail at once; it's sufficiently cluster-safe
            for them each to write to a temp file in the destination directory and rename it when finished.
         */
        final File tempThumbnailFile = getTempFile(thumbnailFile);
        final Thumbnail thumbnail = createThumbnail(attachment, tempThumbnailFile,
                thumbnailConfig.getMaxWidth(), thumbnailConfig.getMaxHeight());

        if (thumbnail instanceof BrokenThumbnail)
        {
            // We couldn't generate the thumbnail - no need to rename or delete any files
            return thumbnail;
        }

        if (thumbnailFile.exists())
        {
            // Another thread got there first
            deleteQuietly(tempThumbnailFile);
        }
        else
        {
            // We won the race to create the thumbnail; did we also win the race to rename the temp file?
            final boolean renamed = tempThumbnailFile.renameTo(thumbnailFile);
            if (!renamed || !thumbnailFile.exists())
            {
                log.debug("Could not rename '{}' to '{}'", tempThumbnailFile.getAbsolutePath(), thumbnailFile.getAbsolutePath());
                return new BrokenThumbnail(attachment.getId());
            }
        }
        return new Thumbnail(thumbnail.getHeight(), thumbnail.getWidth(), thumbnailFile.getName(), attachment.getId(),
                thumbnail.getMimeType());
    }

    private File getThumbnailFile(@Nullable final Issue issue, final Attachment attachment)
    {
        return issue == null ?
                thumbnailAccessor.getThumbnailFile(attachment) : thumbnailAccessor.getThumbnailFile(issue, attachment);
    }

    private Thumbnail createThumbnail(final Attachment attachment, final File thumbnailFile, final int maxWidth,
            final int maxHeight)
    {
        try
        {
            // check that the thumbnail file can be created, otherwise ImageIO will get NullPointerExceptions
            // see also http://bugs.sun.com/view_bug.do?bug_id=5034864
            touch(thumbnailFile);
            deleteQuietly(thumbnailFile);

            return attachmentManager.streamAttachmentContent(attachment, new InputStreamConsumer<Thumbnail>()
            {
                @Override
                public Thumbnail withInputStream(final InputStream in) throws MalformedURLException
                {
                    final Thumbnail thumbnail = new Thumber(MIME_TYPE).retrieveOrCreateThumbNail(in,
                            attachment.getFilename(), thumbnailFile, maxWidth, maxHeight, attachment.getId());
                    if (thumbnail == null)
                    {
                        return null;
                    }
                    if (thumbnail.getFilename() != null && thumbnailFile.getName().equals(thumbnail.getFilename()))
                    {
                        return thumbnail;
                    }
                    // The core thumber code has a behaviour that causes it to name the thumbnail differently
                    // depending on whether the file exists or not, so we ensure it's always the file name we expect.
                    return new Thumbnail(thumbnail.getHeight(), thumbnail.getWidth(), thumbnailFile.getName(),
                            thumbnail.getAttachmentId(), thumbnail.getMimeType());
                }
            });
        }
        catch (Exception e)
        {
            log.warn("Error writing to thumbnail file: {}", thumbnailFile, e);
            return new BrokenThumbnail(attachment.getId());
        }
    }

    private Thumbnail readThumbnail(@Nonnull final Attachment attachment, @Nonnull final File thumbnailFile)
    {
        if (thumbnailFile.exists())
        {
            log.debug("Thumbnail file '{}' already exists. Returning existing thumbnail.", thumbnailFile);
            InputStream inputStream = null;
            try
            {
                inputStream = new FileInputStream(thumbnailFile);
                final ThumbnailDimension thumbnailDimension = ThumbnailUtil.dimensionsForImage(inputStream);
                return new Thumbnail(thumbnailDimension.getHeight(), thumbnailDimension.getWidth(),
                        thumbnailFile.getName(), attachment.getId(), MIME_TYPE);
            }
            catch (IOException asd)
            {
                log.debug("Unable to read image data from existing thumbnail file '{}'. Deleting this thumbnail.", thumbnailFile);
                deleteQuietly(thumbnailFile);
            }
            finally
            {
                closeQuietly(inputStream);
            }
        }
        return null;
    }

    @Override
    public void deleteThumbnailForAttachment(@Nullable final Issue issue, final Attachment attachment)
    {
        if (isThumbnailable(issue, attachment))
        {
            final File thumbnailFile = getThumbnailFile(issue, attachment);
            if (thumbnailFile.exists() && !thumbnailFile.delete())
            {
                log.debug("Unable to delete thumbnail for attachment {}", attachment.getId());
            }
        }
    }

    @Override
    public void deleteThumbnailsForIssue(@Nonnull final Issue issue)
    {
        for (final Attachment attachment : attachmentManager.getAttachments(issue))
        {
            deleteThumbnailForAttachment(issue, attachment);
        }
        final File thumbnailDirectory = thumbnailAccessor.getThumbnailDirectory(issue);
        if (thumbnailDirectory.exists() && !thumbnailDirectory.delete())
        {
            log.debug("Unable to delete thumbnail directory of issue {}", issue.getKey());
        }
    }

    private static File getTempFile(final File thumbnailFile)
    {
        try
        {
            return File.createTempFile(thumbnailFile.getName(), TEMP_FILE_SUFFIX, thumbnailFile.getParentFile());
        }
        catch (final IOException unableToCreateFile)
        {
            throw new IllegalStateException(unableToCreateFile);
        }
    }
}