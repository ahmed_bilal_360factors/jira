package com.atlassian.jira.license;

import com.atlassian.fugue.Option;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import java.util.Set;
import javax.annotation.Nonnull;

import static com.atlassian.jira.util.dbc.Assertions.containsNoNulls;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.collect.ImmutableSet.copyOf;

/**
 * Common ancestor for {@link LicenseRole} implementations.
 *
 * @since v6.4
 */
abstract class AbstractLicenseRole implements LicenseRole
{
    private final ImmutableSet<String> groups;
    private final Option<String> primaryGroup;

    AbstractLicenseRole(final Iterable<String> groups, final Option<String> primaryGroup)
    {
        notNull("primaryGroup", primaryGroup);
        containsNoNulls("groups", groups);

        if (primaryGroup.isDefined() && !Iterables.contains(groups, primaryGroup.get()))
        {
            throw new IllegalArgumentException(String.format("primaryGroup '%s' is not listed in the groups '%s'.", primaryGroup, groups));
        }

        this.primaryGroup = primaryGroup;
        this.groups = copyOf(groups);
    }

    @Nonnull
    @Override
    public final Set<String> getGroups()
    {
        return groups;
    }

    @Nonnull
    @Override
    public final Option<String> getPrimaryGroup()
    {
        return primaryGroup;
    }

    @Override
    public final boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o instanceof LicenseRole)
        {
            final LicenseRole that = (LicenseRole) o;
            //It is intentional that we only look at the id.
            return getId().equals(that.getId());
        }
        else
        {
            return false;
        }
    }

    @Override
    public final int hashCode()
    {
        //It is intentional that we only look at the id.
        return getId().hashCode();
    }

    @Override
    public final String toString()
    {
        return String.format("[name: %s, groups: %s, id: %s, primary: %s]", getName(), getGroups(), getId(), getPrimaryGroup());
    }
}
