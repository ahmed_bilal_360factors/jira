package com.atlassian.jira.issue.index;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Nonnull;

import com.atlassian.jira.index.Index;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Provides {@link com.atlassian.jira.index.Index} for all issue indexes.
 * Additionally manages the lifecycle of {@link com.atlassian.jira.index.Index.Manager} associated with them.
 *
 * @since v6.4
 */
public class LuceneIssueIndexProvider implements Iterable<Index.Manager>
{
    private final AtomicReference<Map<IndexDirectoryFactory.Name, Index.Manager>> ref = new AtomicReference<Map<IndexDirectoryFactory.Name, Index.Manager>>();
    private final IndexDirectoryFactory factory;

    public LuceneIssueIndexProvider(@Nonnull final IndexDirectoryFactory factory)
    {
        this.factory = notNull("factory", factory);
    }

    public Iterator<Index.Manager> iterator()
    {
        return open().values().iterator();
    }

    void close()
    {
        final Map<IndexDirectoryFactory.Name, Index.Manager> indexes = ref.getAndSet(null);
        if (indexes == null)
        {
            return;
        }
        for (final Index.Manager manager : indexes.values())
        {
            manager.close();
        }
    }

    Map<IndexDirectoryFactory.Name, Index.Manager> open()
    {
        Map<IndexDirectoryFactory.Name, Index.Manager> result = ref.get();
        while (result == null)
        {
            ref.compareAndSet(null, factory.get());
            result = ref.get();
        }
        return result;
    }

    public Index getIssueIndex()
    {
        return get(IndexDirectoryFactory.Name.ISSUE).getIndex();
    }

    public Index getCommentIndex()
    {
        return get(IndexDirectoryFactory.Name.COMMENT).getIndex();
    }

    public Index getChangeHistoryIndex()
    {
        return get(IndexDirectoryFactory.Name.CHANGE_HISTORY).getIndex();
    }

    public Index getWorklogIndex()
    {
        return get(IndexDirectoryFactory.Name.WORKLOG).getIndex();
    }

    public Index getIndex(final IndexDirectoryFactory.Name indexName)
    {
        return get(indexName).getIndex();
    }

    public Index.Manager get(final IndexDirectoryFactory.Name key)
    {
        return open().get(key);
    }

    List<String> getIndexPaths()
    {
        return factory.getIndexPaths();
    }

    String getIndexRootPath()
    {
        return factory.getIndexRootPath();
    }

    void setMode(final IndexingMode type)
    {
        factory.setIndexingMode(type);
    }


}
