package com.atlassian.jira.license;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.plugin.license.LicenseRoleModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;
import javax.annotation.Nonnull;

@EventComponent
public class PluginLicenseRoleDefinitions implements LicenseRoleDefinitions
{
    private static final Logger log = LoggerFactory.getLogger(PluginLicenseRoleDefinitions.class);

    private final PluginAccessor pluginAccessor;
    private final JiraLicenseManager licenseManager;
    private final EventPublisher publisher;

    public PluginLicenseRoleDefinitions(final PluginAccessor pluginAccessor,
            final JiraLicenseManager licenseManager, final EventPublisher publisher)
    {
        this.pluginAccessor = pluginAccessor;
        this.licenseManager = licenseManager;
        this.publisher = publisher;
    }

    @Nonnull
    @Override
    public Set<LicenseRoleDefinition> getDefinedRoleDefinitions()
    {
        return getLicenseRoles(Predicates.<LicenseRoleDefinition>alwaysTrue());
    }

    @Nonnull
    @Override
    public Set<LicenseRoleDefinition> getInstalledRoleDefinitions()
    {
        return getLicenseRoles(isLicensed());
    }

    @Override
    public boolean isLicenseRoleDefined(@Nonnull final LicenseRoleId licenseRoleId)
    {
        return getDefinedRoleDefinition(licenseRoleId).isDefined();
    }

    @Override
    public boolean isLicenseRoleInstalled(@Nonnull final LicenseRoleId licenseRoleId)
    {
        return getInstalledRoleDefinition(licenseRoleId).isDefined();
    }

    @Nonnull
    @Override
    public Option<LicenseRoleDefinition> getDefinedRoleDefinition(@Nonnull final LicenseRoleId licenseRoleId)
    {
        return Option.option(Iterables.find(getDefinedRoleDefinitions(), findId(licenseRoleId), null));
    }

    @Nonnull
    @Override
    public Option<LicenseRoleDefinition> getInstalledRoleDefinition(@Nonnull final LicenseRoleId licenseRoleId)
    {
        return Option.option(Iterables.find(getInstalledRoleDefinitions(), findId(licenseRoleId), null));
    }

    private Predicate<LicenseRoleDefinition> isLicensed()
    {
        return new Predicate<LicenseRoleDefinition>()
        {
            @Override
            public boolean apply(final LicenseRoleDefinition input)
            {
                return licenseManager.isLicensed(input.getLicenseRoleId());
            }
        };
    }

    private static Predicate<LicenseRoleDefinition> findId(final LicenseRoleId licenseRoleId)
    {
        return new Predicate<LicenseRoleDefinition>()
        {
            @Override
            public boolean apply(final LicenseRoleDefinition licenseRoleModuleDescriptor)
            {
                return licenseRoleModuleDescriptor.getLicenseRoleId().equals(licenseRoleId);
            }
        };
    }

    private Set<LicenseRoleDefinition> getLicenseRoles(Predicate<LicenseRoleDefinition> predicate)
    {
        final List<LicenseRoleModuleDescriptor> moduleDescriptors =
                pluginAccessor.getEnabledModuleDescriptorsByClass(LicenseRoleModuleDescriptor.class);

        final Set<LicenseRoleDefinition> uniqueModuleDescriptors = Sets.newHashSet();
        for (LicenseRoleModuleDescriptor moduleDescriptor: moduleDescriptors)
        {
            final LicenseRoleDefinition definition = moduleDescriptor.getModule();
            if (predicate.apply(definition))
            {
                if (!uniqueModuleDescriptors.add(definition))
                {
                    log.debug(String.format("The license role module descriptor with id '%s' has a duplicate definition.",
                            definition.getLicenseRoleId()));
                }
            }
        }
        return ImmutableSet.copyOf(uniqueModuleDescriptors);
    }

    @EventListener
    public void pluginModuleDisabled(final PluginModuleDisabledEvent event)
    {
        for (LicenseRoleModuleDescriptor licenseRoleModuleDescriptor : asRole(event.getModule()))
        {
            publisher.publish(new LicenseRoleUndefinedEvent(licenseRoleModuleDescriptor.getLicenseRoleId()));
        }
    }

    @EventListener
    public void pluginModuleEnabled(final PluginModuleEnabledEvent event)
    {
        for (LicenseRoleModuleDescriptor licenseRoleModuleDescriptor : asRole(event.getModule()))
        {
            publisher.publish(new LicenseRoleDefinedEvent(licenseRoleModuleDescriptor.getLicenseRoleId()));
        }
    }

    private Option<LicenseRoleModuleDescriptor> asRole(ModuleDescriptor module)
    {
        if (module instanceof LicenseRoleModuleDescriptor)
        {
            return Option.some((LicenseRoleModuleDescriptor) module);
        }
        else
        {
            return Option.none(LicenseRoleModuleDescriptor.class);
        }
    }
}
