package com.atlassian.jira.entity;

import javax.annotation.Nullable;

import com.google.common.base.Function;

public class WithIdFunctions
{
    public static Function<WithId, Long> getId()
    {
        return new Function<WithId, Long>()
        {
            @Override
            public Long apply(@Nullable final WithId input)
            {
                return input != null ? input.getId() : null;
            }
        };
    }
}