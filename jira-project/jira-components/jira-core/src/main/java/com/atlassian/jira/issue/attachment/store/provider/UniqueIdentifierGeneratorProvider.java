package com.atlassian.jira.issue.attachment.store.provider;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.issue.attachment.store.UniqueIdentifierGenerator;

import org.picocontainer.injectors.ProviderAdapter;

public class UniqueIdentifierGeneratorProvider extends ProviderAdapter
{
    public static final String IDENTIFIER_PREFIX = "id";
    public static final String NON_CLUSTER_NODE_ID = "noncluster";

    public UniqueIdentifierGenerator provide(final ClusterManager  clusterManager)
    {
        final String nodeId = getNodeId(clusterManager);
        return new UniqueIdentifierGenerator(IDENTIFIER_PREFIX, nodeId);
    }

    private String getNodeId(final ClusterManager clusterManager)
    {
        final String clusterNodeId = clusterManager.getNodeId();
        if(clusterNodeId == null)
        {
            return NON_CLUSTER_NODE_ID;
        }
        else
        {
            return clusterNodeId;
        }
    }
}
