package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

/**
 * Condition to check whether there are any link types available.
 *
 * @since v4.1
 */
public class HasLinkTypesAvailableCondition extends AbstractWebCondition
{
    private static final Logger log = LoggerFactory.getLogger(HasLinkTypesAvailableCondition.class);
    private final IssueLinkTypeManager issueLinkTypeManager;

    public HasLinkTypesAvailableCondition(IssueLinkTypeManager issueLinkTypeManager)
    {
        this.issueLinkTypeManager = issueLinkTypeManager;
    }

    public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper)
    {
        final Collection<IssueLinkType> linkTypes = issueLinkTypeManager.getIssueLinkTypes();
        return linkTypes != null && !linkTypes.isEmpty();
    }

}
