package com.atlassian.jira.service;

import java.util.Collection;
import java.util.Map;

/**
 * A store interface for the ServiceConfigs
 */
public interface ServiceConfigStore
{
    /**
     * Adds a new Service of the given class with the the given configuration.
     *
     * @param serviceName The service name.
     * @param serviceClass The JiraService class that we wish to add as a service.
     * @param serviceDelay the service delay.
     *
     * @return JiraServiceContainer for this service.
     *
     * @throws ServiceException If there is any errors trying to add this Service.
     */
    @Deprecated
    public JiraServiceContainer addServiceConfig(String serviceName, Class<? extends JiraService> serviceClass, long serviceDelay)
            throws ServiceException;

    /**
     * Adds a new Service of the given class with the the given configuration.
     * @param name Service name
     * @param serviceClass Class implementing the service
     * @param cronExpression Cron expression
     * @param serviceDelay the service delay.  Only used if the instance is downgraded to a JIRA version prior to 6.4
     * @return JiraServiceContainer for this service.
     */
    public JiraServiceContainer addServiceConfig(String name, Class<? extends JiraService> serviceClass, String cronExpression, Long serviceDelay)
            throws ServiceException;

    @Deprecated
    public void editServiceConfig(JiraServiceContainer config, long delay, Map<String, String[]> params)
            throws ServiceException;

    /**
     * Edit the schedule and parameters for a service.
     * @param serviceContainer The service container
     * @param cronExpression Cron expression
     * @param params Service parameter map.
     */
    public void editServiceConfig(JiraServiceContainer serviceContainer, String cronExpression, Map<String, String[]> params)
            throws ServiceException;

    public void removeServiceConfig(JiraServiceContainer config);

    public JiraServiceContainer getServiceConfigForId(Long id);

    public JiraServiceContainer getServiceConfigForName(String name);

    public Collection<JiraServiceContainer> getAllServiceConfigs();
}
