package com.atlassian.jira.issue.attachment.store.strategy.get;

import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;

/**
 * An interface representing a strategy for retrieving attachments. It's up to implementing object how attachment stream
 * will be retrieved and passed to streamProcessor.
 *
 * @since v6.4
 */
@ParametersAreNonnullByDefault
public interface AttachmentGetStrategy
{
    <A> Promise<A> getAttachmentData(AttachmentKey attachmentKey, Function<AttachmentGetData, A> attachmentGetDataProcessor);

    Promise<Boolean> exists(AttachmentKey attachmentKey);
}
