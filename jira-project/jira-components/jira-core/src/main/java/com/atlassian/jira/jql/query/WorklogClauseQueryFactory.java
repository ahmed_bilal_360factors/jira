package com.atlassian.jira.jql.query;

import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.query.clause.TerminalClause;
import org.apache.lucene.search.Query;

import javax.annotation.Nonnull;

/**
 * This is a clause query factory for worklog clauses.
 *
 * <p>
 *     The implementation of method {@link com.atlassian.jira.jql.query.ClauseQueryFactory#getQuery(com.atlassian.jira.jql.query.QueryCreationContext, com.atlassian.query.clause.TerminalClause)}
 *     works in the standard way, i.e. it returns a query against the issue index, so you can use this class as any other {@link com.atlassian.jira.jql.query.ClauseQueryFactory}.
 * </p>
 *
 * <p>
 *     One additional method is present, though. It is useful if you want to query worklog index directly and then process the results on your own.
 * </p>
 */
public abstract class WorklogClauseQueryFactory implements ClauseQueryFactory
{
    private final IssueIdFilterQueryFactory issueIdFilterQueryFactory;
    private final QueryProjectRoleAndGroupPermissionsDecorator queryPermissionsDecorator;

    WorklogClauseQueryFactory(final IssueIdFilterQueryFactory issueIdFilterQueryFactory, final QueryProjectRoleAndGroupPermissionsDecorator queryPermissionsDecorator)
    {
        this.issueIdFilterQueryFactory = issueIdFilterQueryFactory;
        this.queryPermissionsDecorator = queryPermissionsDecorator;
    }

    @Nonnull
    @Override
    public final QueryFactoryResult getQuery(@Nonnull final QueryCreationContext queryCreationContext, @Nonnull final TerminalClause terminalClause)
    {
        final Query query = getWorklogQuery(queryCreationContext, terminalClause);
        final Query queryWithPermissionFilter = queryPermissionsDecorator.decorateWorklogQueryWithPermissionChecks(query, queryCreationContext);

        final Query issueIdFilterQuery = issueIdFilterQueryFactory.createIssueIdFilterQuery(queryWithPermissionFilter, SearchProviderFactory.WORKLOG_INDEX);
        return new QueryFactoryResult(issueIdFilterQuery);
    }

    /**
     * Returns a lucene query that can be run against the worklog index.
     *
     * @param queryCreationContext query context
     * @param terminalClause clause to transform into query
     * @return lucene query against worklog index
     */
    public abstract Query getWorklogQuery(final QueryCreationContext queryCreationContext, final TerminalClause terminalClause);
}
