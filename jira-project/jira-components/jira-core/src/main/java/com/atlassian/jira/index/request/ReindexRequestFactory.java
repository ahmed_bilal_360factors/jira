package com.atlassian.jira.index.request;

import java.sql.Timestamp;
import java.util.Map;

import com.atlassian.jira.entity.AbstractEntityFactory;
import com.atlassian.jira.ofbiz.FieldMap;

import org.ofbiz.core.entity.GenericValue;

/**
 * @since 6.4
 */
public class ReindexRequestFactory extends AbstractEntityFactory<ReindexRequestBase>
{
    @Override
    public Map<String, Object> fieldMapFrom(ReindexRequestBase value)
    {
        return FieldMap.build(ReindexRequestBase.ID, value.getId())
                .add(ReindexRequestBase.TYPE, value.getType().name())
                .add(ReindexRequestBase.REQUEST_TIME, new Timestamp(value.getRequestTime()))
                .add(ReindexRequestBase.START_TIME, (value.getStartTime() == null ? null : new Timestamp(value.getStartTime())))
                .add(ReindexRequestBase.COMPLETION_TIME, (value.getCompletionTime() == null ? null : new Timestamp(value.getCompletionTime())))
                .add(ReindexRequestBase.EXECUTION_NODE_ID, value.getExecutionNodeId())
                .add(ReindexRequestBase.STATUS, value.getStatus().name());
    }

    @Override
    public String getEntityName()
    {
        return "ReindexRequest";
    }

    @Override
    public ReindexRequestBase build(GenericValue genericValue)
    {
        return new ReindexRequestBase(genericValue.getLong(ReindexRequestBase.ID),
                                    ReindexRequestType.valueOf(genericValue.getString(ReindexRequestBase.TYPE)),
                                    genericValue.getTimestamp(ReindexRequestBase.REQUEST_TIME).getTime(),
                                    timestampToLong(genericValue.getTimestamp(ReindexRequestBase.START_TIME)),
                                    timestampToLong(genericValue.getTimestamp(ReindexRequestBase.COMPLETION_TIME)),
                                    genericValue.getString(ReindexRequestBase.EXECUTION_NODE_ID),
                                    ReindexStatus.valueOf(genericValue.getString(ReindexRequestBase.STATUS)));
    }

    private static Long timestampToLong(Timestamp timestamp)
    {
        if (timestamp == null)
        {
            return null;
        }
        else
        {
            return timestamp.getTime();
        }
    }

}
