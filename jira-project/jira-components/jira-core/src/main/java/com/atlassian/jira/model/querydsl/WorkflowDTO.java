package com.atlassian.jira.model.querydsl;

import javax.annotation.Generated;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;

import org.ofbiz.core.entity.GenericValue;

/**
 * Data Transfer Object for the Workflow entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 *
 * @see QWorkflow
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class WorkflowDTO
{
    private Long id;
    private String name;
    private String creator;
    private String descriptor;
    private String islocked;

    public Long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getCreator()
    {
        return creator;
    }

    public String getDescriptor()
    {
        return descriptor;
    }

    public String getIslocked()
    {
        return islocked;
    }

    public WorkflowDTO(Long id, String name, String creator, String descriptor, String islocked)
    {
        this.id = id;
        this.name = name;
        this.creator = creator;
        this.descriptor = descriptor;
        this.islocked = islocked;
    }
    /**
    * Creates a GenericValue object from the values in this Data Transfer Object.
    * <p>
    *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
    * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
    * @return a GenericValue object constructed from the values in this Data Transfer Object.
    */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator)
    {
        return ofBizDelegator.makeValue("Workflow", new FieldMap()
                        .add("id", id)
                        .add("name", name)
                        .add("creator", creator)
                        .add("descriptor", descriptor)
                        .add("islocked", islocked)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */

    public static WorkflowDTO fromGenericValue(GenericValue gv)
    {
        return new WorkflowDTO(
            gv.getLong("id"),
            gv.getString("name"),
            gv.getString("creator"),
            gv.getString("descriptor"),
            gv.getString("islocked")
        );
    }
}

