package com.atlassian.jira.index.request;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @since 6.4
 */
public class ReindexComponent
{
    public static final String ID = "id";
    public static final String REQUEST_ID = "requestId";
    public static final String AFFECTED_INDEX = "affectedIndex";
    public static final String ENTITY_TYPE = "entityType";

    private final @Nullable Long id;
    private final long requestId;
    private final @Nonnull AffectedIndex affectedIndex;
    private final @Nonnull SharedEntityType entityType;

    public ReindexComponent(@Nullable Long id, long requestId, @Nonnull AffectedIndex affectedIndex, @Nonnull SharedEntityType entityType)
    {
        this.id = id;
        this.requestId = requestId;
        this.affectedIndex = affectedIndex;
        this.entityType = entityType;
    }

    @Nullable
    public Long getId()
    {
        return id;
    }

    public long getRequestId()
    {
        return requestId;
    }

    @Nonnull
    public AffectedIndex getAffectedIndex()
    {
        return affectedIndex;
    }

    @Nonnull
    public SharedEntityType getEntityType()
    {
        return entityType;
    }
}
