package com.atlassian.jira.model.querydsl;

import javax.annotation.Generated;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;

import org.ofbiz.core.entity.GenericValue;

/**
 * Data Transfer Object for the UserAssociation entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 *
 * @see QUserAssociation
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class UserAssociationDTO
{
    private String sourceName;
    private Long sinkNodeId;
    private String sinkNodeEntity;
    private String associationType;
    private Integer sequence;
    private java.sql.Timestamp created;

    public String getSourceName()
    {
        return sourceName;
    }

    public Long getSinkNodeId()
    {
        return sinkNodeId;
    }

    public String getSinkNodeEntity()
    {
        return sinkNodeEntity;
    }

    public String getAssociationType()
    {
        return associationType;
    }

    public Integer getSequence()
    {
        return sequence;
    }

    public java.sql.Timestamp getCreated()
    {
        return created;
    }

    public UserAssociationDTO(String sourceName, Long sinkNodeId, String sinkNodeEntity, String associationType, Integer sequence, java.sql.Timestamp created)
    {
        this.sourceName = sourceName;
        this.sinkNodeId = sinkNodeId;
        this.sinkNodeEntity = sinkNodeEntity;
        this.associationType = associationType;
        this.sequence = sequence;
        this.created = created;
    }
    /**
    * Creates a GenericValue object from the values in this Data Transfer Object.
    * <p>
    *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
    * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
    * @return a GenericValue object constructed from the values in this Data Transfer Object.
    */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator)
    {
        return ofBizDelegator.makeValue("UserAssociation", new FieldMap()
                        .add("sourceName", sourceName)
                        .add("sinkNodeId", sinkNodeId)
                        .add("sinkNodeEntity", sinkNodeEntity)
                        .add("associationType", associationType)
                        .add("sequence", sequence)
                        .add("created", created)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */

    public static UserAssociationDTO fromGenericValue(GenericValue gv)
    {
        return new UserAssociationDTO(
            gv.getString("sourceName"),
            gv.getLong("sinkNodeId"),
            gv.getString("sinkNodeEntity"),
            gv.getString("associationType"),
            gv.getInteger("sequence"),
            gv.getTimestamp("created")
        );
    }
}

