package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Checks if a project is selected
 * <p/>
 * The project must be set within {@link com.atlassian.jira.plugin.webfragment.model.JiraHelper}
 */
public class HasSelectedProjectCondition extends AbstractWebCondition
{
    public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper)
    {
        return jiraHelper.getProject() != null;
    }
}
