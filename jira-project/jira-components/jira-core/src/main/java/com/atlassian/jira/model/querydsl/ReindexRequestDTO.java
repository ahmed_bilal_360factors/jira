package com.atlassian.jira.model.querydsl;

import javax.annotation.Generated;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;

import org.ofbiz.core.entity.GenericValue;

/**
 * Data Transfer Object for the ReindexRequest entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 *
 * @see QReindexRequest
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class ReindexRequestDTO
{
    private Long id;
    private String type;
    private java.sql.Timestamp requestTime;
    private java.sql.Timestamp startTime;
    private java.sql.Timestamp completionTime;
    private String status;
    private String executionNodeId;

    public Long getId()
    {
        return id;
    }

    public String getType()
    {
        return type;
    }

    public java.sql.Timestamp getRequestTime()
    {
        return requestTime;
    }

    public java.sql.Timestamp getStartTime()
    {
        return startTime;
    }

    public java.sql.Timestamp getCompletionTime()
    {
        return completionTime;
    }

    public String getStatus()
    {
        return status;
    }

    public String getExecutionNodeId()
    {
        return executionNodeId;
    }

    public ReindexRequestDTO(Long id, String type, java.sql.Timestamp requestTime, java.sql.Timestamp startTime, java.sql.Timestamp completionTime, String status, String executionNodeId)
    {
        this.id = id;
        this.type = type;
        this.requestTime = requestTime;
        this.startTime = startTime;
        this.completionTime = completionTime;
        this.status = status;
        this.executionNodeId = executionNodeId;
    }
    /**
    * Creates a GenericValue object from the values in this Data Transfer Object.
    * <p>
    *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
    * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
    * @return a GenericValue object constructed from the values in this Data Transfer Object.
    */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator)
    {
        return ofBizDelegator.makeValue("ReindexRequest", new FieldMap()
                        .add("id", id)
                        .add("type", type)
                        .add("requestTime", requestTime)
                        .add("startTime", startTime)
                        .add("completionTime", completionTime)
                        .add("status", status)
                        .add("executionNodeId", executionNodeId)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */

    public static ReindexRequestDTO fromGenericValue(GenericValue gv)
    {
        return new ReindexRequestDTO(
            gv.getLong("id"),
            gv.getString("type"),
            gv.getTimestamp("requestTime"),
            gv.getTimestamp("startTime"),
            gv.getTimestamp("completionTime"),
            gv.getString("status"),
            gv.getString("executionNodeId")
        );
    }
}

