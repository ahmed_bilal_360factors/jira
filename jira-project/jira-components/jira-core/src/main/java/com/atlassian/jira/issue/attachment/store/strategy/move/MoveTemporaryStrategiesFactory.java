package com.atlassian.jira.issue.attachment.store.strategy.move;

import com.atlassian.jira.issue.attachment.StreamAttachmentStore;

public class MoveTemporaryStrategiesFactory
{
    private final SendToStoreFunctionFactory sendToStoreFunctionFactory;

    public MoveTemporaryStrategiesFactory(final SendToStoreFunctionFactory sendToStoreFunctionFactory)
    {
        this.sendToStoreFunctionFactory = sendToStoreFunctionFactory;
    }

    public SingleStoreMoveStrategy createMoveOnlyPrimaryStrategy(final StreamAttachmentStore singleStore)
    {
        return new SingleStoreMoveStrategy(singleStore);
    }

    public BackgroundResendingMoveStrategy createBackgroundResendingStrategy(
            final StreamAttachmentStore primaryStore,
            final StreamAttachmentStore secondaryStore)
    {
        return new BackgroundResendingMoveStrategy(primaryStore, secondaryStore, sendToStoreFunctionFactory);
    }
}
