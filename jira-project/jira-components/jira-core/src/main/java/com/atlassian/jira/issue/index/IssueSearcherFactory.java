package com.atlassian.jira.issue.index;

import javax.annotation.Nonnull;

import org.apache.lucene.search.IndexSearcher;

/**
 * Provides {@link org.apache.lucene.search.IndexSearcher} for all issue indexes.
 *
 * @since v6.4
 */
public interface IssueSearcherFactory
{
    /**
     * Get an {@link IndexSearcher} that can be used to search the chosen index.
     * <p />
     * Note: This is an unmanaged IndexSearcher. You MUST call {@link IndexSearcher#close()} when you are done with it. Alternatively you should
     * really call {@link com.atlassian.jira.issue.search.SearchProviderFactory#getSearcher(String))} passing in {@link com.atlassian.jira.issue.search.SearchProviderFactory#CHANGE_HISTORY_INDEX} as it is a managed
     * searcher and all the closing semantics are handled for you.
     */
    @Nonnull
    IndexSearcher getEntitySearcher(IndexDirectoryFactory.Name index);
}
