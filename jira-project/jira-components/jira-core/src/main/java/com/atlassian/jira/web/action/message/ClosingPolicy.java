package com.atlassian.jira.web.action.message;

/**
 * Describes UI element closing behaviour.
 *
 * @see <a href="https://docs.atlassian.com/aui/latest/docs/flag.html">Flag parameters</a>
 * @since v6.4
 */
public enum ClosingPolicy
{
    AUTO("auto"), MANUAL("manual"), NEVER("never");

    private final String auiValue;

    /**
     * @param auiValue closing policy compatible with AUI flags
     */
    ClosingPolicy(final String auiValue)
    {
        this.auiValue = auiValue;
    }

    /**
     * @return closing policy compatible with AUI flags
     */
    public String getAuiValue()
    {
        return auiValue;
    }
}
