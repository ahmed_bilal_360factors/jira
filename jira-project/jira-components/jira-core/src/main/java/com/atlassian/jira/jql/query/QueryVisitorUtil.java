package com.atlassian.jira.jql.query;

import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;

/**
 *
 * @since v6.4
 */
class QueryVisitorUtil
{
    static Query makeQuery(final QueryFactoryResult queryFactoryResult)
    {
        if (queryFactoryResult.mustNotOccur())
        {
            //we must wrap the thing in a boolean query so that the "mustNot" in the result is represented.
            final BooleanQuery query = new BooleanQuery();
            query.add(queryFactoryResult.getLuceneQuery(), BooleanClause.Occur.MUST_NOT);
            return query;
        }
        else
        {
            return queryFactoryResult.getLuceneQuery();
        }
    }
}
