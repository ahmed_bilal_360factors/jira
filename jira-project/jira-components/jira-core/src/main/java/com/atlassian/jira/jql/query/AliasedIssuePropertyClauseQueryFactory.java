package com.atlassian.jira.jql.query;


import java.util.Set;

import com.atlassian.fugue.Option;
import com.atlassian.jira.index.IndexDocumentConfiguration;
import com.atlassian.jira.issue.customfields.converters.DoubleConverter;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.util.JqlDateSupport;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.query.clause.Property;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operator.Operator;

import com.google.common.collect.ImmutableSet;

import static com.atlassian.fugue.Option.some;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Factory for producing Lucene queries from aliased issue properties.
 *
 * @since v6.4
 */
public class AliasedIssuePropertyClauseQueryFactory extends AbstractPropertyClauseQueryFactory
{
    private final Property property;
    private final IndexDocumentConfiguration.Type type;
    private final Set<Operator> operators;

    public AliasedIssuePropertyClauseQueryFactory(final DoubleConverter doubleConverter,
            final JqlDateSupport jqlDateSupport,
            final JqlOperandResolver operandResolver,
            final JiraAuthenticationContext authenticationContext,
            final Property property,
            final IndexDocumentConfiguration.Type type,
            final Set<Operator> operators)
    {
        super(doubleConverter, jqlDateSupport, operandResolver, authenticationContext);
        this.property = checkNotNull(property);
        this.type = checkNotNull(type);
        this.operators = checkNotNull(operators);
    }

    @Override
    protected boolean isSupportedOperator(final TerminalClause terminalClause)
    {
        return operators.contains(terminalClause.getOperator());
    }

    @Override
    protected Option<Property> getProperty(final TerminalClause terminalClause)
    {
        return some(property);
    }

    @Override
    protected Iterable<IndexDocumentConfiguration.Type> getPropertyTypes(final Property property)
    {
        return ImmutableSet.of(type);
    }
}
