package com.atlassian.jira.bulkedit;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.LongIdsValueHolder;
import com.atlassian.jira.issue.fields.OrderableField;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;
/**
 * Represents the Bulk Edit multi select field option for which the field values provided in Bulk Edit will be removed from the already set field values (if they exist)
 *
 * @since v6.4
 */
public class BulkEditMultiSelectFieldOptionRemove extends AbstractBulkEditMultiSelectFieldOption implements BulkEditMultiSelectFieldOption
{
    private static final String ID = "remove";
    private static final String NAME_I18N_KEY = "bulkedit.actions.multiselect.field.option.REMOVE";
    private static final String DESCRIPTION_I18N_KEY = "bulkedit.actions.multiselect.field.option.REMOVE";

    public String getId()
    {
        return ID;
    }

    public String getNameI18nKey()
    {
        return NAME_I18N_KEY;
    }

    public String getDescriptionI18nKey()
    {
        return DESCRIPTION_I18N_KEY;
    }

    @Override
    public Map<String,Object> getFieldValuesMap(final Issue issue, final OrderableField field, final Map<String, Object> fieldValuesHolder)
    {
        final Map<String, Object> issueFieldValuesHolder = Maps.newHashMap();
        field.populateFromIssue(issueFieldValuesHolder, issue);
        final Collection<Object> fieldValuesFromIssue = (Collection<Object>) issueFieldValuesHolder.get(field.getId());
        final Collection<Object> fieldValuesBulkChange = (Collection<Object>) fieldValuesHolder.get(field.getId());
        final List<Object> fieldValuesBulkChangeList = Lists.newArrayList(fieldValuesBulkChange);

        final List<String> fieldValuesBulkChangeListString = Lists.newArrayList(Iterables.transform(fieldValuesBulkChangeList, new Function<Object, String>()
        {
            @Override
            public String apply(final Object value)
            {
                return value.toString();
            }
        }));

        final Predicate<Object> notFieldValuesFromIssue = Predicates.not(new Predicate<Object>()
        {
            public boolean apply(final Object a) { return fieldValuesBulkChangeListString.contains(a.toString()); }
        });
        Iterable<Object> fieldValuesIterable = Iterables.filter(fieldValuesFromIssue, notFieldValuesFromIssue);
        if (fieldValuesBulkChange instanceof Set)
        {
            return ImmutableMap.of(field.getId(), (Object) Sets.newHashSet(fieldValuesIterable));
        }
        else
        {
            return ImmutableMap.of(field.getId(), (Object) Lists.newArrayList(fieldValuesIterable));
        }
    }

    @Override
    public boolean validateOperation(final OrderableField field, final Map<String,Object> fieldValuesHolder)
    {
            return (((Collection) fieldValuesHolder.get(field.getId())).size() > 0);
    }

    @Override
    public String getFieldValuesToAdd(final OrderableField field, final Map<String,Object> fieldValuesHolder)
    {
        return StringUtils.EMPTY;
    }
}
