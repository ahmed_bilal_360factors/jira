package com.atlassian.jira.plugin.webfragment.conditions;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.plugin.PluginParseException;

/**
 * Checks if the user can see issue type {@link AbstractJiraPermissionCondition#permission}
 */
public class UserHasIssueTypeCondition extends AbstractJiraCondition
{
	private Integer issueType;
	private PermissionManager permissionManager;
    public UserHasIssueTypeCondition(PermissionManager permissionManager)
    {
    	this.permissionManager = permissionManager;
    }

    public boolean shouldDisplay(User user, JiraHelper jiraHelper)
    {
    	//Get projects for current user with browse permission. There must be one project only
    	List<Project> userProjects =  (List<Project>) permissionManager.getProjectObjects(10, user);
    	
    	Project project = userProjects.get(0);
    	Collection<IssueType> projectIssueTypes =  project.getIssueTypes();
    	for(IssueType projectIssueType : projectIssueTypes ){
    		Integer issueTypeId = Integer.parseInt(projectIssueType.getId());
    		if(issueTypeId.equals(issueType)){
    			return true;
    		}	
    	}
    		return false;
    }
    

    public void init(Map params) throws PluginParseException
    {
    	issueType = Integer.parseInt(((String) params.get("issueType")));
        if (issueType == null)
        {
            throw new PluginParseException("Could not determine issue type for: " + params.get("issueType"));
        }
    	super.init(params);
    }
}
