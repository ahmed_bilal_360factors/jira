package com.atlassian.jira.issue.fields.config;

/**
 * Represents the FieldConfigSchemeIssueType DB table.
 * <p>
 * FieldConfigSchemeIssueType shows which Issue Types are relevant to a given CustomField FieldConfigScheme.
 * <p>
 * Strangely, it also maps to the fieldconfiguration DB table (which in turn links the CF options and default value).
 * This is pretty fucked up because fieldconfiguration should actually link to FieldConfigScheme :(
 */
public class FieldConfigSchemeIssueType
{
    //    <field name="id" type="numeric"/>
    //    <field name="issuetype" type="long-varchar"/>
    //    <field name="fieldconfigscheme" type="numeric"/>
    //    <field name="fieldconfiguration" type="numeric"/>
    public static final String ID = "id";
    public static final String ISSUE_TYPE_ID = "issuetype";
    public static final String FIELD_CONFIG_SCHEME_ID = "fieldconfigscheme";
    public static final String FIELD_CONFIGURATION_ID = "fieldconfiguration";

    private final Long id;
    private final String issueTypeId;
    private final Long fieldConfigSchemeId;
    private final Long fieldConfigurationId;

    public FieldConfigSchemeIssueType(final Long id, final String issueTypeId, final Long fieldConfigSchemeId, final Long fieldConfigurationId) {
        this.id = id;
        this.issueTypeId = issueTypeId;
        this.fieldConfigSchemeId = fieldConfigSchemeId;
        this.fieldConfigurationId = fieldConfigurationId;
    }

    public Long getId()
    {
        return id;
    }

    public String getIssueTypeId()
    {
        return issueTypeId;
    }

    public Long getFieldConfigSchemeId()
    {
        return fieldConfigSchemeId;
    }

    public Long getFieldConfigurationId()
    {
        return fieldConfigurationId;
    }
}
