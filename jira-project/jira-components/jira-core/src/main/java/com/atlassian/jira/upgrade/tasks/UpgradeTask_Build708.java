package com.atlassian.jira.upgrade.tasks;

import javax.annotation.Nullable;

import com.atlassian.jira.config.properties.PropertiesManager;

/**
 * Cleans up left over configuration from JIRA issues cache.
 * Need to run this again as all its good work was undone by the consistency checker.
 */
public class UpgradeTask_Build708 extends UpgradeTask_Build605
{

    public UpgradeTask_Build708(PropertiesManager propertiesManager)
    {
        super(propertiesManager);
    }

    @Override
    public String getBuildNumber()
    {
        return "708";
    }

    @Nullable
    @Override
    public String dependsUpon()
    {
        return "707";
    }

}
