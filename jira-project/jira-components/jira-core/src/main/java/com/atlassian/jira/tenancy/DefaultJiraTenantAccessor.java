package com.atlassian.jira.tenancy;

import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.InitializingComponent;
import com.atlassian.jira.bc.dataimport.ImportCompletedEvent;
import com.atlassian.jira.bc.dataimport.ImportStartedEvent;
import com.atlassian.jira.extension.JiraStartedEvent;
import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.jira.web.ServletContextProvider;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;
import com.atlassian.tenancy.api.Tenant;
import com.atlassian.tenancy.api.TenantContext;
import com.atlassian.tenancy.api.TenantUnavailableException;
import com.atlassian.tenancy.api.UnexpectedTenantChangeException;

import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.concurrent.Callable;

import javax.servlet.ServletContext;
/**
 * Implementation of TenantAccessor for InstantOn - only has a system tenant
 *
 * @since v6.4
 */
@EventComponent
public class DefaultJiraTenantAccessor implements JiraTenantAccessor, InitializingComponent
{
    private final static Logger log = LoggerFactory.getLogger(DefaultJiraTenantAccessor.class);
    private final static Event UNTENANTED_EVENT = new Event(
        EventType.get("untenanted"), "JIRA is not yet tenanted", EventLevel.get(EventLevel.WARNING));

    private final TenantContext tenantContext;
    private final ComponentLocator componentLocator;
    private final ServletContext servletContext;

    private Tenant systemTenant;
    private volatile boolean doingImport;

    public DefaultJiraTenantAccessor(final TenantContext tenantContext, final ComponentLocator componentLocator)
    {
        this(tenantContext, componentLocator, ServletContextProvider.getServletContext());
    }

    @VisibleForTesting
    DefaultJiraTenantAccessor(final TenantContext tenantContext, final ComponentLocator componentLocator, final ServletContext servletContext)
    {
        this.tenantContext = tenantContext;
        this.componentLocator = componentLocator;
        this.servletContext = servletContext;
    }

    @Override
    public synchronized Iterable<Tenant> getAvailableTenants()
    {
        if (systemTenant == null)
        {
            return Collections.emptySet();
        }
        else
        {
            return Collections.singleton(systemTenant);
        }
    }

    @Override
    public <T> T asTenant(Tenant tenant, Callable<T> call) throws TenantUnavailableException, InvocationTargetException
    {
        Tenant currentTenant = tenantContext.getCurrentTenant();
        if (currentTenant == null)
        {
            log.warn("You are not associated with a tenant, so cannot call tenant specific code");
            throw new TenantUnavailableException();
        }
        if (tenant != currentTenant)
        {
            log.warn("You cannot invoke a runnable in another tenant's context");
            throw new UnexpectedTenantChangeException();
        }
        try
        {
            return call.call();
        }
        catch (Exception e)
        {
            throw new InvocationTargetException(e);
        }
    }

    @Override
    public synchronized void addTenant(final Tenant tenant)
    {
        if (doingImport)
        {
            throw new IllegalStateException("Cannot add tenant while doing an import");
        }
        if (systemTenant == null)
        {
            systemTenant = tenant;
        }
        else
        {
            throw new IllegalArgumentException("The system is already tenanted");
        }
    }

    @EventListener
    public void onImportStarted(@SuppressWarnings ("UnusedParameters") ImportStartedEvent importStartedEvent)
    {
        doingImport = true;
    }

    @EventListener
    public void onImportCompleted(@SuppressWarnings ("UnusedParameters") ImportCompletedEvent importCompletedEvent)
    {
        doingImport = false;
    }

    @EventListener
    public void onJiraStarted(@SuppressWarnings ("UnusedParameters") JiraStartedEvent jiraStartedEvent)
    {
        JohnsonEventContainer.get(servletContext).removeEvent(UNTENANTED_EVENT);

    }

    @Override
    public void afterInstantiation() throws Exception
    {
        final TenancyCondition tenancyCondition = componentLocator.getComponent(TenancyCondition.class);
        if (tenancyCondition != null && tenancyCondition.isEnabled() && !JiraUtils.isSetup())
        {
            JohnsonEventContainer.get(servletContext).addEvent(UNTENANTED_EVENT);
        }
    }
}
