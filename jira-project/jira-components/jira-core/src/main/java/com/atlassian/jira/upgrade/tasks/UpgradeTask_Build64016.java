package com.atlassian.jira.upgrade.tasks;

import java.sql.Connection;
import java.sql.Statement;

/**
 * This upgrade task comes with a little bit of history. We started with two indexes on `entity_property` table, these
 * indexes were entityproperty_entity(entityName, entityId) and entityproperty_key(propertyKey). When developing entity
 * properties during JIRA 6.2 we created entity_property_identiti(entityName, entityId, propertyKey) to assure that the
 * search on these three columns is performing well. However, we didn't drop the entityproperty_entity and
 * entityproperty_key until late phases of 6.4 developments. The drop run on OD and for clients who upgraded to 6.4.0
 * (the drop was called UpgradeTask_Build64013). Which is when we discovered that this was actually wrong and we have to
 * restore entityproperty_entity(entityName, entityId) entityproperty_key(propertyKey) and drop the
 * entity_property_identiti. UpgradeTask_Build64013 is removed so it doesn't run for clients who didn't do the upgrade.
 * This upgrade task completes the cycle and restore the order two indexes on (entityName, entityId) and (propertyKey).
 */
public class UpgradeTask_Build64016 extends DropIndexTask
{
    @Override
    public String getBuildNumber()
    {
        return "64016";
    }

    @Override
    public String getShortDescription()
    {
        return "Drops the entityproperty_identity index in entity_property table";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception
    {
        final Connection connection = getDatabaseConnection();
        final String tableName = "entity_property";

        final String index = "entityproperty_identiti";

        try
        {
            connection.setAutoCommit(false);
            final String dropIndex = buildDropIndexSql(tableName, index);
            Statement update = connection.createStatement();
            update.execute(dropIndex);
            connection.commit();
        }
        catch (Exception ignore)
        {
            // In case the index doesn't exist. This happens, when customer runs this upgrade task between JIRA >= 6.1
            // and JIRA < 6.4, where the index was not created yet.
        }
        finally
        {
            connection.close();
        }
    }
}
