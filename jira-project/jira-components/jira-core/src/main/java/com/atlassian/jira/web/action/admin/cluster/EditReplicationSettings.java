package com.atlassian.jira.web.action.admin.cluster;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent;
import com.atlassian.jira.cluster.disasterrecovery.JiraHomeReplicatorService;
import com.atlassian.jira.config.util.SecondaryJiraHome;

/**
 * Replication Settings Edit page
 * @since v6.4
 */
public class EditReplicationSettings extends ReplicationSettings
{

    private boolean attachmentsEnabled;
    private boolean pluginsEnabled;
    private boolean indexSnapshotsEnabled;
    private boolean avatarsEnabled;

    public EditReplicationSettings(SecondaryJiraHome secondaryJiraHome, ClusterManager clusterManager,
            JiraHomeReplicatorService jiraHomeReplicatorService)
    {
        super(secondaryJiraHome, clusterManager, jiraHomeReplicatorService);
    }

    @Override
    public String doDefault() throws Exception
    {
        attachmentsEnabled = getApplicationProperties().getOption(JiraHomeChangeEvent.FileType.ATTACHMENT.getKey());
        pluginsEnabled = getApplicationProperties().getOption(JiraHomeChangeEvent.FileType.PLUGIN.getKey());
        indexSnapshotsEnabled = getApplicationProperties().getOption(JiraHomeChangeEvent.FileType.INDEX_SNAPSHOT.getKey());
        avatarsEnabled = getApplicationProperties().getOption(JiraHomeChangeEvent.FileType.AVATAR.getKey());
        return INPUT;
    }

    @Override
    protected String doExecute() throws Exception
    {
        secondaryJiraHome.applySettings(attachmentsEnabled, pluginsEnabled, indexSnapshotsEnabled, avatarsEnabled);
        return returnComplete("ReplicationSettings.jspa");
    }

    public boolean isAttachmentsEnabled()
    {
        return attachmentsEnabled;
    }

    public void setAttachmentsEnabled(final boolean attachmentsEnabled)
    {
        this.attachmentsEnabled = attachmentsEnabled;
    }

    public boolean isPluginsEnabled()
    {
        return pluginsEnabled;
    }

    public void setPluginsEnabled(final boolean pluginsEnabled)
    {
        this.pluginsEnabled = pluginsEnabled;
    }

    public boolean isIndexSnapshotsEnabled()
    {
        return indexSnapshotsEnabled;
    }

    public void setIndexSnapshotsEnabled(final boolean indexSnapshotsEnabled)
    {
        this.indexSnapshotsEnabled = indexSnapshotsEnabled;
    }

    public boolean isAvatarsEnabled()
    {
        return avatarsEnabled;
    }

    public void setAvatarsEnabled(final boolean avatarsEnabled)
    {
        this.avatarsEnabled = avatarsEnabled;
    }
}
