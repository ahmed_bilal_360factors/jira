package com.atlassian.jira.license;

import com.atlassian.fugue.Option;

import java.util.Set;
import javax.annotation.Nonnull;

/**
 * Methods for accessing and querying the {@link com.atlassian.jira.license.LicenseRoleDefinition}s
 * that provide the definition for {@link LicenseRole}s.
 *
 * @since v6.4
 */
public interface LicenseRoleDefinitions
{
    /**
     * Return all the defined {@code LicenseRoleDefinition}s. A {@code LicenseRoleDefinition} is
     * considered <em>defined</em> when a plugin provides it. A defined {@code LicenseRoleDefinition}
     * does not necessarily have an accompanying license.
     *
     * @return all the defined {@code LicenseRoleDefinition}s.
     */
    @Nonnull
    Set<LicenseRoleDefinition> getDefinedRoleDefinitions();

    /**
     * Return all the installed {@code LicenseRoleDefinition}s. A {@code LicenseRoleDefinition}
     * is considered <em>installed</em> when a plugin defines it and it has an accompanying license.
     *
     * An <em>installed</em> {@code LicenseRole} has been defined by a plugin/product
     * <strong>AND</strong> has an accompanying license that grants that role.
     *
     * @return all the installed {@code LicenseRoleDefinition}s.
     */
    @Nonnull
    Set<LicenseRoleDefinition> getInstalledRoleDefinitions();

    /**
     * Return {@code true} if the {@link LicenseRole} identified by the given {@link LicenseRoleId}
     * is defined.
     *
     * A <em>defined</em> {@code LicenseRole} has been defined by a plugin/product
     * but does not necessarily have an accompanying license that grants that role.
     *
     * @param licenseRoleId the ID to check.
     * @return {@code true} when the passed {@link LicenseRoleId} is defined.
     */
    boolean isLicenseRoleDefined(@Nonnull final LicenseRoleId licenseRoleId);

    /**
     * Return {@code true} when the passed {@link com.atlassian.jira.license.LicenseRoleId} is installed.
     *
     * An <em>installed</em> {@code LicenseRole} has been defined by a plugin/product
     * <strong>AND</strong> has an accompanying license that grants that role.
     *
     * @param licenseRoleId the ID to check.
     * @return {@code true} when the passed {@link com.atlassian.jira.license.LicenseRoleId} is installed.
     */
    boolean isLicenseRoleInstalled(@Nonnull final LicenseRoleId licenseRoleId);

    /**
     * Return the {@link LicenseRoleDefinition} associated with the passed {@link LicenseRoleId}
     * provided it is defined. A value of {@link com.atlassian.fugue.Option#none()} is returned if the passed
     * {@code LicenseRoleId} is undefined.
     *
     * A <em>defined</em> {@code LicenseRole} has been defined by a plugin/product
     * but does not necessarily have an accompanying license that grants that role.
     *
     * @param licenseRoleId the ID to check.
     * @return Return the {@link LicenseRoleDefinition} associated with the passed
     * {@link LicenseRoleId} if it is defined or {@link com.atlassian.fugue.Option#none()}
     * otherwise.
     */
    @Nonnull
    Option<LicenseRoleDefinition> getDefinedRoleDefinition(@Nonnull final LicenseRoleId licenseRoleId);

    /**
     * Return the {@link LicenseRoleDefinition} associated with the passed {@link LicenseRoleId}
     * provided it is installed. A value of {@link com.atlassian.fugue.Option#none()} is returned if the passed
     * {@code LicenseRoleId} is not installed.
     *
     * An <em>installed</em> {@code LicenseRole} has been defined by a plugin/product
     * <strong>AND</strong> has an accompanying license that grants that role.
     *
     * @param licenseRoleId the ID to check.
     * @return Return the {@link LicenseRoleDefinition} associated with the passed
     * {@link LicenseRoleId} if it is installed or {@link com.atlassian.fugue.Option#none()}
     * otherwise.
     */
    @Nonnull
    Option<LicenseRoleDefinition> getInstalledRoleDefinition(@Nonnull final LicenseRoleId licenseRoleId);
}
