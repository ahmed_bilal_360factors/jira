package com.atlassian.jira.database;

/**
 * A callback mechanism that delivers a managed DB connection.
 *
 * @see com.atlassian.jira.database.DbConnectionManager#execute(SqlCallback)
 * @since v6.4
 */
public interface SqlCallback
{
    void run(DbConnection dbConnection);
}
