package com.atlassian.jira.jql.query;

import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.util.InjectableComponent;
import com.google.common.base.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;

import java.io.IOException;
import java.util.Collections;
import javax.annotation.Nullable;

import static com.google.common.collect.Iterables.transform;

/**
 * Component that can execute queries against the worklog index
 * and return issue ids associated with the returned worklog documents.
 */
@InjectableComponent
public class WorklogQueryExecutor
{
    private static final Logger log = LoggerFactory.getLogger(WorklogQueryExecutor.class);

    private final SearchProviderFactory searchProviderFactory;
    private final QueryProjectRoleAndGroupPermissionsDecorator queryPermissionsDecorator;

    public WorklogQueryExecutor(final SearchProviderFactory searchProviderFactory, final QueryProjectRoleAndGroupPermissionsDecorator queryPermissionsDecorator)
    {
        this.searchProviderFactory = searchProviderFactory;
        this.queryPermissionsDecorator = queryPermissionsDecorator;
    }

    /**
     * Executes a worklog query and returns an iterable of unique issue ids
     * associated with the returned worklogs.
     *
     * <p>
     *     Before execution the query is modified so that it automatically
     *     checks all the required permissions for user currently
     *     executing it, so you needn't worry about such stuff, just
     *     provide the query in it's simplest possible form.
     * </p>
     *
     * <p>
     *     In case the index is unreachable or any other IO exceptions an empty set is returned.
     * </p>
     *
     * @param worklogQuery lucene query over the worklog index
     * @param context query context
     * @return issue ids
     */
    public Iterable<Long> executeWorklogQuery(final Query worklogQuery, QueryCreationContext context)
    {
        final IndexSearcher searcher = searchProviderFactory.getSearcher(SearchProviderFactory.WORKLOG_INDEX);
        final IssueIdCollector collector = new IssueIdCollector(searcher.getIndexReader());

        try
        {
            final Query permissionChecksDecoratedQuery = queryPermissionsDecorator.decorateWorklogQueryWithPermissionChecks(worklogQuery, context);
            searcher.search(permissionChecksDecoratedQuery, collector);
            return transform(collector.getIssueIds(), new Function<String, Long>()
            {
                @Override
                public Long apply(@Nullable final String input)
                {
                    return Long.valueOf(input);
                }
            });
        }
        catch (final IOException e)
        {
            log.error("Unable to search the " + SearchProviderFactory.WORKLOG_INDEX + " index.", e);
            return Collections.emptySet();
        }
    }
}
