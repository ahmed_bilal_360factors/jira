package com.atlassian.jira.project.version;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.association.NodeAssocationType;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.project.VersionArchiveEvent;
import com.atlassian.jira.event.project.VersionCreateEvent;
import com.atlassian.jira.event.project.VersionDeleteEvent;
import com.atlassian.jira.event.project.VersionMergeEvent;
import com.atlassian.jira.event.project.VersionMoveEvent;
import com.atlassian.jira.event.project.VersionReleaseEvent;
import com.atlassian.jira.event.project.VersionUnarchiveEvent;
import com.atlassian.jira.event.project.VersionUnreleaseEvent;
import com.atlassian.jira.event.project.VersionUpdatedEvent;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.IssueRelationConstants;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.UpdateIssueRequest;
import com.atlassian.jira.issue.fields.AffectedVersionsSystemField;
import com.atlassian.jira.issue.fields.FixVersionsSystemField;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.CollectionReorderer;
import com.atlassian.jira.util.Function;
import com.atlassian.jira.util.NamedPredicates;
import com.atlassian.jira.util.collect.CollectionUtil;
import com.atlassian.jira.util.dbc.Assertions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.opensymphony.util.TextUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nonnull;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.project.version.VersionPredicates.isArchived;
import static com.atlassian.jira.project.version.VersionPredicates.isReleased;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Lists.newArrayList;

public class DefaultVersionManager implements VersionManager
{
    private static final Logger log = LoggerFactory.getLogger(DefaultVersionManager.class);

    private static final Function<Version, Long> VERSION_TO_ID_TRANSFORMER = new Function<Version, Long>()
    {
        @Override
        public Long get(final Version input)
        {
            return input.getId();
        }
    };

    private final IssueManager issueManager;
    private final NodeAssociationStore nodeAssociationStore;
    private final IssueIndexManager issueIndexManager;
    private final ProjectManager projectManager;
    private final VersionStore versionStore;
    private final EventPublisher eventPublisher;

    public DefaultVersionManager(
            final IssueManager issueManager,
            final NodeAssociationStore nodeAssociationStore,
            final IssueIndexManager issueIndexManager,
            final ProjectManager projectManager,
            final VersionStore versionStore,
            final EventPublisher eventPublisher)
    {
        this.issueManager = issueManager;
        this.nodeAssociationStore = nodeAssociationStore;
        this.issueIndexManager = issueIndexManager;
        this.projectManager = projectManager;
        this.versionStore = versionStore;
        this.eventPublisher = eventPublisher;
    }

    /**
     * @deprecated since version 6.0
     */
    @Deprecated
    public Version createVersion(final String name, final Date releaseDate, final String description, final GenericValue project, final Long scheduleAfterVersion)
            throws CreateException
    {
        if (project == null)
        {
            throw new CreateException("You cannot create a version without a project.");
        }
        return createVersion(name, releaseDate, description, project.getLong("id"), scheduleAfterVersion);
    }

    @Override
    public Version createVersion(final String name, final Date releaseDate, final String description, final Long projectId, final Long scheduleAfterVersion)
            throws CreateException
    {
        return createVersion(name, null, releaseDate, description, projectId, scheduleAfterVersion);
    }

    @Override
    public Version createVersion(final String name, final Date startDate, final Date releaseDate, final String description, final Long projectId, final Long scheduleAfterVersion)
            throws CreateException
    {
        if (!TextUtils.stringSet(name))
        {
            throw new CreateException("You cannot create a version without a name.");
        }

        if (projectId == null)
        {
            throw new CreateException("You cannot create a version without a project.");
        }

        final Map<String, Object> versionParams = new HashMap<String, Object>();
        versionParams.put("project", projectId);
        versionParams.put("name", name);
        versionParams.put("description", description);

        if (startDate != null)
        {
            versionParams.put("startdate", new Timestamp(startDate.getTime()));
        }

        if (releaseDate != null)
        {
            versionParams.put("releasedate", new Timestamp(releaseDate.getTime()));
        }

        // This determines where in the scheduling order the new version should be placed.
        if (scheduleAfterVersion != null)
        {
            if (scheduleAfterVersion == -1L)
            {
                // Decrease all version sequences in order to place new version in first position
                moveAllVersionSequences(projectId);
                // New version sequence will be first position
                versionParams.put("sequence", 1L);
            }
            else
            {
                // Decrease version sequences which follow this version in order to slot in new version
                moveVersionSequences(scheduleAfterVersion);
                // New version sequence will follow this version
                final Long newSequence = getVersion(scheduleAfterVersion).getSequence() + 1L;
                versionParams.put("sequence", newSequence);
            }
        }
        else
        {
            versionParams.put("sequence", getMaxVersionSequence(projectId));
        }

        Version newVersion = versionStore.createVersion(versionParams);
        eventPublisher.publish(new VersionCreateEvent(newVersion));
        return newVersion;
    }

    // ---- Scheduling Methods ----
    public void moveToStartVersionSequence(final Version version)
    {
        final List<Version> versions = new ArrayList<Version>(getAllVersions(version));
        CollectionReorderer.moveToStart(versions, version);
        storeReorderedVersionList(versions);
        eventPublisher.publish(new VersionMoveEvent(version));
    }

    public void increaseVersionSequence(final Version version)
    {
        final List<Version> versions = new ArrayList<Version>(getAllVersions(version));
        CollectionReorderer.increasePosition(versions, version);
        storeReorderedVersionList(versions);
        eventPublisher.publish(new VersionMoveEvent(version));
    }

    public void decreaseVersionSequence(final Version version)
    {
        final List<Version> versions = new ArrayList<Version>(getAllVersions(version));
        CollectionReorderer.decreasePosition(versions, version);
        storeReorderedVersionList(versions);
        eventPublisher.publish(new VersionMoveEvent(version));
    }

    public void moveToEndVersionSequence(final Version version)
    {
        final List<Version> versions = new ArrayList<Version>(getAllVersions(version));
        CollectionReorderer.moveToEnd(versions, version);
        storeReorderedVersionList(versions);
        eventPublisher.publish(new VersionMoveEvent(version));
    }

    public void moveVersionAfter(final Version version, final Long scheduleAfterVersionId)
    {
        if (version == null)
        {
            throw new IllegalArgumentException("You cannot move a null version");
        }

        //Don't re-schedule if the scheduleAfterVersion id is of itself (Note: scheduleAfterVersion can be null)
        if ((version.getId() != null) && !version.getId().equals(scheduleAfterVersionId))
        {
            Version targetVersion;
            if (scheduleAfterVersionId == null)
            {
                targetVersion = getLastVersion(version.getProjectId());//move to last version
            }
            else if (scheduleAfterVersionId == -1L)
            {
                targetVersion = null;//move to start
            }
            else
            //move to the target version
            {
                targetVersion = getVersion(scheduleAfterVersionId);
            }

            final List<Version> versions = new ArrayList<Version>(getAllVersions(version));
            CollectionReorderer.moveToPositionAfter(versions, version, targetVersion);
            storeReorderedVersionList(versions);
            eventPublisher.publish(new VersionMoveEvent(version));
        }
    }

    // Increases sequence numbers for versions (make them later) to make space for new version
    private void moveVersionSequences(final Long scheduleAfterVersion)
    {
        final Version startVersion = getVersion(scheduleAfterVersion);
        final Collection<Version> versions = getVersions(startVersion.getProjectId());
        final List<Version> versionsChanged = Lists.newArrayListWithCapacity(versions.size());

        for (final Version version : versions)
        {
            if (version.getSequence() > startVersion.getSequence())
            {
                final Long newSequence = version.getSequence() + 1L;
                version.setSequence(newSequence);
                versionsChanged.add(version);
            }
        }
        versionStore.storeVersions(versionsChanged);
    }

    // Increases sequence numbers for all versions (make them later) to make space for new version
    private void moveAllVersionSequences(final Long project)
    {
        final Collection<Version> versions = getVersions(project);
        final List<Version> versionsChanged = Lists.newArrayListWithCapacity(versions.size());

        for (final Version version : versions)
        {
            final Long newSequence = version.getSequence() + 1L;
            version.setSequence(newSequence);
            versionsChanged.add(version);
        }

        versionStore.storeVersions(versionsChanged);
    }

    public void deleteVersion(final Version version)
    {
        deleteVersionWithoutPublishingAnEvent(version);
        eventPublisher.publish(VersionDeleteEvent.deleted(version));
    }

    private void deleteVersionWithoutPublishingAnEvent(final Version version)
    {
        versionStore.deleteVersion(version);

        //JRA-13766: We need to get all the versions, and re-store the versions where sequence numbers are not
        //correct.
        reorderVersionsInProject(version);
    }

    @Override
    public Version update(Version version)
    {
        final Version originalVersion = getVersion(version.getId());
        versionStore.storeVersion(version);
        eventPublisher.publish(new VersionUpdatedEvent(version, originalVersion));
        return version;
    }

    @Override
    public void editVersionDetails(Version version, String name, String description)
    {
        //There must be a name for the entity
        if (!TextUtils.stringSet(name))
        {
            throw new IllegalArgumentException("You must specify a valid version name.");
        }
        else
        {
            //if the name already exists then add an Error message
            if (isDuplicateName(version, name))
            {
                throw new IllegalArgumentException("A version with this name already exists in this project.");
            }
        }

        version.setName(name);
        version.setDescription(description);

        versionStore.storeVersion(version);
    }

    public void editVersionDetails(final Version version, final String versionName, final String description, final GenericValue project)
    {
        editVersionDetails(version, versionName, description);
    }

    // ---- Release Version methods ----
    public void releaseVersion(Version version, boolean release)
    {
        releaseVersions(Collections.singleton(version), release);
        final Version updatedVersion = getVersion(version.getId());
        if (release)
        {
            eventPublisher.publish(new VersionReleaseEvent(updatedVersion));
        }
        else
        {
            eventPublisher.publish(new VersionUnreleaseEvent(updatedVersion));
        }
    }

    public void releaseVersions(final Collection<Version> versions, final boolean release)
    {
        final List<Version> versionsChanged = Lists.newArrayListWithCapacity(versions.size());

        for (final Version version : versions)
        {
            validateReleaseParams(version, release);
            version.setReleased(release);

            versionsChanged.add(version);
        }

        if (!versionsChanged.isEmpty())
        {
            versionStore.storeVersions(versionsChanged);
        }
    }

    public void moveIssuesToNewVersion(final List<GenericValue> issues, final Version currentVersion, final Version swapToVersion)
            throws IndexException
    {
        if (currentVersion != null && swapToVersion != null && !issues.isEmpty())
        {
            nodeAssociationStore.swapAssociation(issues, IssueRelationConstants.FIX_VERSION, currentVersion.getGenericValue(), swapToVersion.getGenericValue());
            issueIndexManager.reIndexIssues(issues);
        }
    }

    // ---- Archive Version methods ----
    public void archiveVersions(final String[] idsToArchive, final String[] idsToUnarchive)
    {
        final List<Version> versionsChanged = Lists.newArrayListWithCapacity(idsToArchive.length);

        for (String anIdsToArchive : idsToArchive)
        {
            final Long archiveId = new Long(anIdsToArchive);
            final Version version = getVersion(archiveId);
            if ((version != null) && !version.isArchived())
            {
                version.setArchived(true);
                versionsChanged.add(version);
            }
        }

        for (String anIdsToUnarchive : idsToUnarchive)
        {
            final Long unArchiveId = new Long(anIdsToUnarchive);
            final Version version = getVersion(unArchiveId);
            if ((version != null) && version.isArchived())
            {
                version.setArchived(false);
                versionsChanged.add(version);
            }
        }

        if (!versionsChanged.isEmpty())
        {
            versionStore.storeVersions(versionsChanged);
        }
    }

    public void archiveVersion(final Version version, final boolean archive)
    {
        version.setArchived(archive);
        versionStore.storeVersion(version);

        final Version updatedVersion = getVersion(version.getId());
        if (archive)
        {
            eventPublisher.publish(new VersionArchiveEvent(updatedVersion));
        }
        else
        {
            eventPublisher.publish(new VersionUnarchiveEvent(updatedVersion));
        }
    }

    // ---- Version Due Date Mthods ----
    public void editVersionReleaseDate(final Version version, final Date duedate)
    {
        //Theversion must be specified
        if (version == null)
        {
            throw new IllegalArgumentException("You must specify a valid version.");
        }

        if (version.getStartDate() != null && duedate != null)
        {
            if (version.getStartDate().after(duedate))
            {
                throw new IllegalArgumentException("Release date must be after the version start date");
            }
        }

        version.setReleaseDate(duedate);

        versionStore.storeVersion(version);
    }

    @Override
    public void editVersionStartDate(Version version, Date startDate)
    {
        if (version == null)
        {
            throw new IllegalArgumentException("You must specify a valid version.");
        }

        if (startDate != null && version.getReleaseDate() != null)
        {
            if (startDate.after(version.getReleaseDate()))
            {
                throw new IllegalArgumentException("Start date must be before the version release date");
            }
        }

        version.setStartDate(startDate);

        versionStore.storeVersion(version);
    }

    @Override
    public void editVersionStartReleaseDate(Version version, Date startDate, Date releaseDate)
    {
        if (version == null)
        {
            throw new IllegalArgumentException("You must specify a valid version.");
        }

        if (startDate != null && releaseDate != null)
        {
            if (startDate.after(releaseDate))
            {
                throw new IllegalArgumentException("Start date must be before the version release date");
            }
        }

        version.setStartDate(startDate);
        version.setReleaseDate(releaseDate);

        versionStore.storeVersion(version);
    }

    public boolean isVersionOverDue(final Version version)
    {
        if (version.getReleaseDate() == null || version.isArchived() || version.isReleased())
        {
            return false;
        }
        final Calendar releaseDate = Calendar.getInstance();
        releaseDate.setTime(version.getReleaseDate());

        final Calendar lastMidnight = Calendar.getInstance();
        lastMidnight.set(Calendar.HOUR_OF_DAY, 0);
        lastMidnight.set(Calendar.MINUTE, 0);
        lastMidnight.set(Calendar.SECOND, 0);
        lastMidnight.set(Calendar.MILLISECOND, 0);

        return releaseDate.before(lastMidnight);
    }

    // ---- Get Version(s) methods ----
    public Collection<Version> getVersionsUnarchived(final Long projectId)
    {
        final Iterable<Version> versions = versionStore.getVersionsByProject(projectId);
        return filterVersions(versions, Predicates.not(isArchived()));
    }

    @Nonnull
    private List<Version> filterVersions(@Nonnull final Iterable<Version> versions, @Nonnull final Predicate<Version> predicate)
    {
        return newArrayList(Iterables.filter(versions, predicate));
    }

    @Override
    public Collection<Version> getVersionsArchived(final GenericValue projectGV)
    {
        final Iterable<Version> versions = versionStore.getVersionsByProject(projectGV.getLong("id"));
        return filterVersions(versions, isArchived());
    }

    @Override
    public Collection<Version> getVersionsArchived(final Project project)
    {
        final Iterable<Version> versions = versionStore.getVersionsByProject(project.getId());
        return filterVersions(versions, isArchived());
    }

    @Override
    public List<Version> getVersions(final GenericValue project)
    {
        return getVersions(project.getLong("id"));
    }

    @Override
    public List<Version> getVersions(final Long projectId)
    {
        Assertions.notNull("projectId", projectId);
        return copyOf(versionStore.getVersionsByProject(projectId));
    }

    @Override
    public List<Version> getVersions(Long projectId, boolean includeArchived)
    {
        Assertions.notNull("projectId", projectId);
        Iterable<Version> versions = versionStore.getVersionsByProject(projectId);
        Predicate<Version> predicate = Predicates.alwaysTrue();

        if (!includeArchived)
        {
            predicate = Predicates.not(isArchived());
        }

        return filterVersions(versions, predicate);
    }

    @Override
    public List<Version> getVersions(Project project)
    {
        return getVersions(project.getId());
    }

    @Override
    public Collection<Version> getVersionsByName(final String versionName)
    {
        Assertions.notNull("versionName", versionName);
        return copyOf(versionStore.getVersionsByName(versionName));
    }

    public Collection<Version> getAffectedVersionsByIssue(final GenericValue issue)
    {
        return getVersionsByIssue(issue, IssueRelationConstants.VERSION);
    }

    @Override
    public Collection<Version> getAffectedVersionsFor(Issue issue)
    {
        return getVersionsByIssue(issue.getGenericValue(), IssueRelationConstants.VERSION);
    }

    @Override
    public List<ChangeItemBean> updateIssueAffectsVersions(final Issue issue, Collection<Version> newVersions)
    {
        return updateIssueValue(issue, newVersions, getAffectedVersionsFor(issue), NodeAssocationType.ISSUE_TO_AFFECTS_VERISON, AffectedVersionsSystemField.CHANGE_ITEM_FIELD);
    }

    @Override
    public List<ChangeItemBean> updateIssueFixVersions(final Issue issue, final Collection<Version> newValue)
    {
        return updateIssueValue(issue, newValue, getFixVersionsFor(issue), NodeAssocationType.ISSUE_TO_FIX_VERISON, FixVersionsSystemField.CHANGE_ITEM_FIELD);
    }

    private List<ChangeItemBean> updateIssueValue(Issue issue, Collection<Version> newVersions, Collection<Version> oldVersions, final NodeAssocationType nodeAssocationType, final String changeItemField)
    {
        if (newVersions == null)
        {
            newVersions = Collections.emptyList();
        }

        List<ChangeItemBean> changes = new ArrayList<ChangeItemBean>(newVersions.size());

        // compare the ids not the object values - JRA-12130
        Collection<Long> oldVersionIds = CollectionUtil.transform(oldVersions, VERSION_TO_ID_TRANSFORMER);
        Collection<Long> newVersionIds = CollectionUtil.transform(newVersions, VERSION_TO_ID_TRANSFORMER);

        for (final Version newVersion : newVersions)
        {
            if (!oldVersionIds.contains(newVersion.getId()))
            {
                nodeAssociationStore.createAssociation(nodeAssocationType, issue.getId(), newVersion.getId());
                changes.add(new ChangeItemBean(ChangeItemBean.STATIC_FIELD, changeItemField, null, null, newVersion.getId().toString(), newVersion.getName()));
            }
        }

        // loop through existing entities and remove any that aren't in new entities
        for (final Version oldVersion : oldVersions)
        {
            if (!newVersionIds.contains(oldVersion.getId()))
            {
                nodeAssociationStore.removeAssociation(nodeAssocationType, issue.getId(), oldVersion.getId());
                changes.add(new ChangeItemBean(ChangeItemBean.STATIC_FIELD, changeItemField, oldVersion.getId().toString(), oldVersion.getName(), null, null));
            }
        }
        return changes;
    }

    public Collection<Version> getFixVersionsByIssue(final GenericValue issue)
    {
        return getVersionsByIssue(issue, IssueRelationConstants.FIX_VERSION);
    }

    @Override
    public Collection<Version> getFixVersionsFor(Issue issue)
    {
        return getVersionsByIssue(issue.getGenericValue(), IssueRelationConstants.FIX_VERSION);
    }

    public Collection<Version> getAllVersions()
    {
        return copyOf(versionStore.getAllVersions());
    }

    @Override
    public Collection<Version> getAllVersionsForProjects(Collection<Project> projects, boolean includeArchived)
    {
        if (projects.isEmpty())
        {
            return Collections.emptyList();
        }

        Iterable<Version> allVersionGvs = Collections.emptyList();
        for (Project project : projects)
        {
            Iterable<Version> projectVersionGvs = versionStore.getVersionsByProject(project.getId());
            allVersionGvs = Iterables.concat(allVersionGvs, projectVersionGvs);
        }

        Predicate<Version> predicate = Predicates.alwaysTrue();
        if (!includeArchived)
        {
            predicate = Predicates.not(isArchived());
        }

        return filterVersions(allVersionGvs, predicate);
    }

    public Collection<Version> getAllVersionsReleased(final boolean includeArchived)
    {
        Predicate<Version> predicate = isReleased();
        if (!includeArchived)
        {
            predicate = Predicates.and(predicate, Predicates.not(isArchived()));
        }
        return filterVersions(versionStore.getAllVersions(), predicate);
    }

    public Collection<Version> getAllVersionsUnreleased(final boolean includeArchived)
    {
        Predicate<Version> predicate = Predicates.not(isReleased());
        if (!includeArchived)
        {
            predicate = Predicates.and(predicate, Predicates.not(isArchived()));
        }
        return filterVersions(versionStore.getAllVersions(), predicate);
    }

    @Override
    public void merge(final ApplicationUser user, @Nonnull final Version versionToDelete, @Nonnull final Version versionToMergeTo)
    {
        boolean affectedIssues = swapOrRemoveVersionsFromIssues(user, versionToDelete, some(versionToMergeTo), some(versionToMergeTo));
        if (affectedIssues) // this seems a bit pointless but at the moment of writing we are unsure if this behaviour can be safely changed (so that the event will be always thrown)
        {
            eventPublisher.publish(new VersionMergeEvent(versionToMergeTo, versionToDelete));
        }
        deleteVersionWithoutPublishingAnEvent(versionToDelete);
        eventPublisher.publish(VersionDeleteEvent.deletedAndMerged(versionToDelete, versionToMergeTo));
    }

    @Override
    public void deleteAndRemoveFromIssues(final ApplicationUser user, @Nonnull final Version versionToRemove)
    {
        deleteVersion(versionToRemove);
        swapOrRemoveVersionsFromIssues(user, versionToRemove, Option.<Version>none(), Option.<Version>none());
    }

    @Override
    public void swapVersionForRelatedIssues(final ApplicationUser user, final Version version, final Option<Version> affectsSwapVersion, final Option<Version> fixSwapVersion)
    {
        boolean affectedIssues = swapOrRemoveVersionsFromIssues(user, version, affectsSwapVersion, fixSwapVersion);

        if (affectedIssues && fixSwapVersion.isDefined())
        {
            eventPublisher.publish(new VersionMergeEvent(fixSwapVersion.get(), version));
        }
    }

    /**
     * @return true if there were any issues affected
     */
    private boolean swapOrRemoveVersionsFromIssues(final ApplicationUser user, final Version version, final Option<Version> affectsSwapVersion, final Option<Version> fixSwapVersion)
    {
        final Set<Long> issuesIds = getAllAssociatedIssueIds(version);
        // swap the versions on the affected issues
        for (final Long issueId : issuesIds)
        {
            final MutableIssue newIssue = issueManager.getIssueObject(issueId);
            newIssue.setAffectedVersions(getNewVersions(version, newIssue.getAffectedVersions(), affectsSwapVersion));
            newIssue.setFixVersions(getNewVersions(version, newIssue.getFixVersions(), fixSwapVersion));

            final Issue updated = issueManager.updateIssue(user, newIssue, UpdateIssueRequest.builder().
                    eventDispatchOption(EventDispatchOption.ISSUE_UPDATED).
                    sendMail(false).build());
            try
            {
                // reindex all the affected issues
                issueIndexManager.reIndex(updated);
            }
            catch (final IndexException e)
            {
                log.warn("Could not reindex issue with id '" + issueId + "' after swapping versions", e);
            }
        }

        return issuesIds.size() > 0;
    }

    private Set<Long> getAllAssociatedIssueIds(final Version version)
    {
        final Set<Long> issueIds = Sets.newHashSet();
        issueIds.addAll(getIssueIdsWithAffectsVersion(version));
        issueIds.addAll(getIssueIdsWithFixVersion(version));
        return issueIds;
    }

    /**
     * Gets a set of new versions.
     *
     * @param versionToRemove The version to remove.
     * @param versions The current {@link Version}s for an issue.
     * @param versionToSwap The version being swapped in.  May be null (in which case nothing gets swapped in).
     * @return A set of versions to save back to the issue.
     */
    private Set<Version> getNewVersions(final Version versionToRemove, final Collection<Version> versions, final Option<Version> versionToSwap)
    {
        if (versionToSwap.isDefined())
        {
            return Sets.newHashSet(Iterables.transform(versions, new com.google.common.base.Function<Version, Version>()
            {
                @Override
                public Version apply(final Version input)
                {
                    return input.getId().equals(versionToRemove.getId()) ? versionToSwap.get() : input;
                }
            }));
        }
        else
        {
            return Sets.newHashSet(Iterables.filter(versions, new Predicate<Version>()
            {
                @Override
                public boolean apply(final Version input)
                {
                    return !input.getId().equals(versionToRemove.getId());
                }
            }));
        }
    }

    /**
     * @param issue the issue
     * @param relationName {@link IssueRelationConstants#VERSION} or {@link IssueRelationConstants#FIX_VERSION}.
     * @return A collection of {@link Version}s for this issue.
     */
    protected Collection<Version> getVersionsByIssue(final GenericValue issue, final String relationName)
    {
        final List<GenericValue> versionGVs = nodeAssociationStore.getSinksFromSource(issue, "Version", relationName);
        final List<Version> versions = new ArrayList<Version>();

        for (final GenericValue versionGV : versionGVs)
        {
            versions.add(new VersionImpl(projectManager, versionGV));
        }
        return versions;
    }

    public Collection<Version> getVersions(final List<Long> ids)
    {
        final List<Version> versions = Lists.newArrayListWithCapacity(ids.size());
        for (final Long id : ids)
        {
            versions.add(getVersion(id));
        }

        return versions;
    }

    public Collection<Version> getVersionsUnreleased(final Long projectId, final boolean includeArchived)
    {
        final Iterable<Version> versions = versionStore.getVersionsByProject(projectId);

        Predicate<Version> predicate = Predicates.not(isReleased());
        if (!includeArchived)
        {
            predicate = Predicates.and(predicate, Predicates.not(isArchived()));
        }
        return filterVersions(versions, predicate);
    }

    public Collection<Version> getVersionsReleased(final Long projectId, final boolean includeArchived)
    {
        final Iterable<Version> versions = versionStore.getVersionsByProject(projectId);

        Predicate<Version> predicate = isReleased();
        if (!includeArchived)
        {
            predicate = Predicates.and(predicate, Predicates.not(isArchived()));
        }
        return filterVersions(versions, predicate);
    }

    public Collection<Version> getVersionsReleasedDesc(final Long projectId, final boolean includeArchived)
    {
        final List<Version> released = new ArrayList<Version>(getVersionsReleased(projectId, includeArchived));
        Collections.reverse(released);
        return released;
    }

    public Version getVersion(final Long id)
    {
        return versionStore.getVersion(id);
    }

    /**
     * Retrieve a specific Version in a project given the project id, or <code>null</code> if no such version exists in
     * that project.
     */
    public Version getVersion(final Long projectId, final String versionName)
    {
        final Iterable<Version> versions = Iterables.filter(versionStore.getVersionsByProject(projectId), NamedPredicates.equalsIgnoreCase(versionName));
        return Iterables.getFirst(versions, null);
    }

    @Override
    public Collection<GenericValue> getAllAffectedIssues(final Collection<Version> versions)
    {
        try
        {
            final Collection<GenericValue> affectedIssues = new HashSet<GenericValue>();
            for (final Version version : versions)
            {
                affectedIssues.addAll(issueManager.getIssuesByEntity(IssueRelationConstants.VERSION, version.getGenericValue()));
                affectedIssues.addAll(issueManager.getIssuesByEntity(IssueRelationConstants.FIX_VERSION, version.getGenericValue()));
            }

            return affectedIssues;
        }
        catch (final GenericEntityException e)
        {
            throw new DataAccessException("Error getting issue for versions " + ToStringBuilder.reflectionToString(versions), e);
        }
    }

    /**
     * NB: This is done because we can't inject a {@link IssueFactory}, this would cause circular dependency.
     */
    protected IssueFactory getIssueFactory()
    {
        return ComponentAccessor.getIssueFactory();
    }

    /**
     * Return all other versions in the project except this one
     */
    public Collection<Version> getOtherVersions(final Version version)
    {
        final Collection<Version> otherVersions = new ArrayList<Version>(getAllVersions(version));
        otherVersions.remove(version);
        return otherVersions;
    }

    /**
     * Return all unarchived versions except this one
     */
    public Collection<Version> getOtherUnarchivedVersions(final Version version)
    {
        final Collection<Version> otherUnarchivedVersions = new ArrayList<Version>(getVersionsUnarchived(version.getProjectId()));
        otherUnarchivedVersions.remove(version);
        return otherUnarchivedVersions;
    }

    public Collection<GenericValue> getFixIssues(final Version version)
    {
        try
        {
            return issueManager.getIssuesByEntity(IssueRelationConstants.FIX_VERSION, version.getGenericValue());
        }
        catch (final GenericEntityException e)
        {
            throw new DataAccessException("Unabled to get fix issues for version " + version, e);
        }
    }

    @Override
    public Collection<Issue> getIssuesWithFixVersion(Version version)
    {
        Collection<Long> issueIds = getIssueIdsWithFixVersion(version);
        final Collection<Issue> issues = new ArrayList<Issue>(issueIds.size());
        for (Long issueId : issueIds)
        {
            issues.add(issueManager.getIssueObject(issueId));
        }
        return issues;
    }

    public Collection<GenericValue> getAffectsIssues(final Version version)
    {
        try
        {
            return issueManager.getIssuesByEntity(IssueRelationConstants.VERSION, version.getGenericValue());
        }
        catch (final GenericEntityException e)
        {
            throw new DataAccessException("Unabled to get affected issues for version " + version, e);
        }
    }

    @Override
    public Collection<Issue> getIssuesWithAffectsVersion(Version version)
    {
        return getIssueFactory().getIssues(getAffectsIssues(version));
    }

    @Override
    public Collection<Long> getIssueIdsWithAffectsVersion(@Nonnull final Version version)
    {
        return nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_AFFECTS_VERISON, version.getId());
    }

    @Override
    public Collection<Long> getIssueIdsWithFixVersion(@Nonnull final Version version)
    {
        return nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_FIX_VERISON, version.getId());
    }

    /**
     * Returns a sorted list of all versions in the project that this version is in.
     *
     * @param version The version used to determine the project.
     * @return a sorted list of all versions in the project that this version is in.
     */
    private List<Version> getAllVersions(final Version version)
    {
        return getVersions(version.getProjectId());
    }

    public boolean isDuplicateName(final Version currentVersion, final String name)
    {
        //Chek to see if there is already a version with that name for the project
        for (final Version version : currentVersion.getProjectObject().getVersions())
        {
            if (!currentVersion.getId().equals(version.getId()) && name.trim().equalsIgnoreCase(version.getName()))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a version in the current project already exists with the same name
     */
    public boolean isDuplicateName(final Version currentVersion, final String name, final GenericValue project)
    {
        return isDuplicateName(currentVersion, name);
    }

    private void validateReleaseParams(final Version version, final boolean release)
    {
        if ((version == null) && release)
        {
            throw new IllegalArgumentException("Please select a version to release");
        }
        else if ((version == null) && !release)
        {
            throw new IllegalArgumentException("Please select a version to unrelease.");
        }
    }

    private Version getLastVersion(final Long projectId)
    {
        long maxSequence = 0L;
        Version lastVersion = null;

        for (final Version version : getVersions(projectId))
        {
            if ((version.getSequence() != null) && (version.getSequence() >= maxSequence))
            {
                maxSequence = version.getSequence();
                lastVersion = version;
            }
        }
        return lastVersion;
    }

    private long getMaxVersionSequence(final Long projectId)
    {
        long maxSequence = 1L;

        for (final Version version : getVersions(projectId))
        {
            if ((version.getSequence() != null) && (version.getSequence() >= maxSequence))
            {
                maxSequence = version.getSequence() + 1L;
            }
        }
        return maxSequence;
    }

    /**
     * Given a re-ordered list of versions, commit the changes to the backend datastore.
     */
    public void storeReorderedVersionList(final List<Version> versions)
    {
        final List<Version> versionsChanged = Lists.newArrayListWithCapacity(versions.size());

        for (int i = 0; i < versions.size(); i++)
        {
            final Version version = versions.get(i);
            final long expectedSequenceNumber = i + 1L;
            if (expectedSequenceNumber != version.getSequence())
            {
                version.setSequence(expectedSequenceNumber);
                versionsChanged.add(version);
            }
        }

        versionStore.storeVersions(versionsChanged);
    }

    /**
     * For testing purposes.
     *
     * @param version The version used to determine the project.
     */
    void reorderVersionsInProject(final Version version)
    {
        storeReorderedVersionList(getAllVersions(version));
    }
}
