package com.atlassian.jira.issue.attachment;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.annotation.Nullable;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class will help when working with zip files
 *
 * @since 4.1
 * @deprecated Use {@link com.atlassian.jira.issue.AttachmentIndexManager} instead. Since v6.4.
 */
@Deprecated
public class AttachmentZipKit
{
    private static final Logger log = LoggerFactory.getLogger(AttachmentZipKit.class);

    public static class ZipEntryInputStream extends InputStream
    {
        private final InputStream inputStream;
        private final ZipArchiveEntry zipEntry;

        private ZipEntryInputStream(final InputStream inputStream, final ZipArchiveEntry zipEntry)
        {
            this.inputStream = inputStream;
            this.zipEntry = zipEntry;
        }

        public ZipArchiveEntry getZipEntry()
        {
            return zipEntry;
        }


        public int read() throws IOException
        {
            return inputStream.read();
        }

        public int read(final byte[] b) throws IOException
        {
            return inputStream.read(b);
        }

        public int read(final byte[] b, final int off, final int len) throws IOException
        {
            return inputStream.read(b, off, len);
        }

        public long skip(final long n) throws IOException
        {
            return inputStream.skip(n);
        }

        public int available() throws IOException
        {
            return inputStream.available();
        }

        public void close() throws IOException
        {
            inputStream.close();
        }

        public void mark(final int readlimit)
        {
            inputStream.mark(readlimit);
        }

        public void reset() throws IOException
        {
            inputStream.reset();
        }

        public boolean markSupported()
        {
            return inputStream.markSupported();
        }

    }

    /**
     * This will return true if the file is in fact a valid zip file.
     *
     * @param file the file in play.
     * @return true if its actually a readable zip file.
     * @deprecated Use {@link com.atlassian.jira.issue.AttachmentIndexManager#isExpandable(Attachment)} instead. Since v6.4.
     */
    @Deprecated
    public boolean isZip(@Nullable final File file)
    {
        if (file == null)
        {
            return false;
        }
        try
        {
            final ZipFile zipFile = new ZipFile(file);
            final boolean hasAtLeastOneEntry = zipFile.getEntries().hasMoreElements();
            zipFile.close();
            return hasAtLeastOneEntry;
        }
        catch (IOException e)
        {
            return false;
        }
    }

    /**
     * This will extract an {@link java.io.OutputStream} of the contents of the file at entryIndex within the zip file.
     * If no file can be found be at that entryIndex or the entry is a directory then null will be returned.
     *
     * @param zipFile the zip file in play
     * @param entryIndex the entryIndex of the entry to return
     * @return an {@link com.atlassian.jira.issue.attachment.AttachmentZipKit.ZipEntryInputStream} that can be read from
     *         for data and has extra {@link org.apache.commons.compress.archivers.zip.ZipArchiveEntry} information.
     * @throws IOException if things go wrong during reading
     * @deprecated Use {@link com.atlassian.jira.issue.AttachmentIndexManager#getAttachmentContents(Attachment, com.atlassian.jira.issue.Issue, int)} instead. Since v6.4.
     */
    @Deprecated
    public ZipEntryInputStream extractFile(@Nullable final File zipFile, final long entryIndex) throws IOException
    {
        if (!isZip(zipFile))
        {
            throw new IOException("This is not a zipFile" + zipFile);
        }
        final ZipArchiveInputStream zipInputStream = new ZipArchiveInputStream(new FileInputStream(zipFile));
        ZipArchiveEntry entry = zipInputStream.getNextZipEntry();

        int currentEntry = 0;
        while (entry != null && currentEntry < entryIndex)
        {
            entry = zipInputStream.getNextZipEntry();
            currentEntry++;
        }

        if (entry != null && currentEntry == entryIndex)
        {
            return new ZipEntryInputStream(zipInputStream, entry);
        }
        return null;
    }

    // The code below is used by JIRA Agile and if it will be changed/removed the old Agile (<6.4.14) will break.

    /**
     * @deprecated Use {@link com.atlassian.jira.issue.AttachmentIndexManager} instead. Since v6.4.
     */
    @Deprecated
    public enum FileCriteria
    {
        ONLY_FILES
                {
                    boolean matches(final ZipArchiveEntry zipEntry)
                    {
                        return !zipEntry.isDirectory();
                    }
                },
        ONLY_DIRECTORIES
                {
                    boolean matches(final ZipArchiveEntry zipEntry)
                    {
                        return zipEntry.isDirectory();
                    }
                },
        ALL
                {
                    boolean matches(final ZipArchiveEntry zipEntry)
                    {
                        return true;
                    }
                };

        abstract boolean matches(final ZipArchiveEntry zipEntry);
    }

    /**
     * This will return a list of {@link AttachmentZipKit.AttachmentZipEntry}s for a given zip file
     *
     * @param zipFile the ZIP file in play
     * @param criteria the criteria of which entries to return
     * @param maxEntries the maximum number of entries to put in th list.  If this is -negative then not maximum is
     * used.
     * @return a list of entries
     * @throws IOException if the file cant be read or is not a zip. file.
     * @deprecated Use {@link com.atlassian.jira.issue.AttachmentIndexManager} instead. Since v6.4.
     */
    @Deprecated
    public AttachmentZipEntries listEntries(File zipFile, int maxEntries, final FileCriteria criteria)
            throws IOException
    {
        ZipFile zf = new ZipFile(zipFile);

        try
        {
            List<AttachmentZipEntry> list = new ArrayList<AttachmentZipEntry>();
            int currentEntry = 0;

            for (Enumeration<ZipArchiveEntry> enumeration = zf.getEntries(); enumeration.hasMoreElements(); )
            {
                final ZipArchiveEntry zipEntry = enumeration.nextElement();
                if (criteria.matches(zipEntry))
                {
                    list.add(new AttachmentZipEntryImpl(currentEntry, zipEntry));
                }
                currentEntry++;
            }

            final int totalNumberOfEntriesAvailable = list.size();
            boolean hasMore = false;

            if (maxEntries >= 0 && list.size() > maxEntries)
            {
                hasMore = true;
                list = list.subList(0, maxEntries);
            }
            return new AttachmentZipEntriesImpl(list, totalNumberOfEntriesAvailable, hasMore);
        }
        finally
        {
            try
            {
                zf.close();
            }
            catch (IOException e)
            {
                log.error(String.format("JIRA was not able to close the zip file: '%s' while / after listing its "
                        + "entries. An IOException was thrown.", zipFile.getPath()));

            }
        }
    }

    /**
     * @deprecated Use {@link com.atlassian.jira.issue.AttachmentIndexManager} instead. Since v6.4.
     */
    @Deprecated
    public interface AttachmentZipEntries
    {
        /**
         * @return true if there are more entries available in the zip than asked for.
         */
        public boolean isMoreAvailable();

        /**
         * @return total number of entries available (can be larger that what was asked for)
         */
        public int getTotalNumberOfEntriesAvailable();

        /**
         * @return the list of {@link com.atlassian.jira.issue.attachment.AttachmentZipKit.AttachmentZipEntry}
         */
        public List<AttachmentZipEntry> getList();
    }

    /**
     * These are the data returned about entries in a zip file
     * @deprecated Use {@link com.atlassian.jira.issue.AttachmentIndexManager} instead. Since v6.4.
     */
    @Deprecated
    public static interface AttachmentZipEntry
    {
        /**
         * @return the entryIndex within the zip file
         */
        long getEntryIndex();

        /**
         * @return the file name of the zip entry
         */
        String getName();

        /**
         * @return the abbreviated file name
         */
        String getAbbreviatedName();

        /**
         * @return the extension of the file name of the zip entry
         */
        String getExtension();

        /**
         * @return the size of the file uncompressed
         */
        long getSize();

        /**
         * @return true if the entry is a directory instead of a file
         */
        boolean isDirectory();

        /**
         * @return the number of directories deep this entry is
         */
        int getDirectoryDepth();

        /**
         * @return The date the entry was last modified
         */
        Date getModifiedDate();

        /**
         * @return The mimetype of the entry
         */
        String getMimetype();
    }

    /**
     * @deprecated Use {@link com.atlassian.jira.issue.AttachmentIndexManager} instead. Since v6.4.
     */
    @Deprecated
    private static class AttachmentZipEntriesImpl implements AttachmentZipEntries
    {
        private final boolean moreAvailable;
        private final int totalNumberOfEntriesAvailable;
        private final List<AttachmentZipEntry> list;

        private AttachmentZipEntriesImpl(final List<AttachmentZipEntry> list, final int totalNumberOfEntriesAvailable, final boolean moreAvailable)
        {
            this.moreAvailable = moreAvailable;
            this.totalNumberOfEntriesAvailable = totalNumberOfEntriesAvailable;
            this.list = list;
        }

        public boolean isMoreAvailable()
        {
            return moreAvailable;
        }

        public int getTotalNumberOfEntriesAvailable()
        {
            return totalNumberOfEntriesAvailable;
        }

        public List<AttachmentZipEntry> getList()
        {
            return Collections.unmodifiableList(list);
        }
    }

    /**
     * @deprecated Use {@link com.atlassian.jira.issue.AttachmentIndexManager} instead. Since v6.4.
     */
    @Deprecated
    private static class AttachmentZipEntryImpl implements AttachmentZipEntry
    {
        private final int entryIndex;
        private final int directoryDepth;
        private final ZipArchiveEntry zipEntry;

        public AttachmentZipEntryImpl(final int entryIndex, final ZipArchiveEntry zipEntry)
        {
            this.entryIndex = entryIndex;
            this.zipEntry = zipEntry;
            this.directoryDepth = calcDepth(zipEntry.getName());
        }

        private int calcDepth(final String name)
        {
            File f = new File(name);
            int i = 0;
            while ((f = f.getParentFile()) != null)
            {
                i++;
            }
            return i;
        }

        @Override
        public long getEntryIndex()
        {
            return entryIndex;
        }

        @Override
        public String getName()
        {
            return zipEntry.getName();
        }

        @Override
        public String getAbbreviatedName()
        {
            return new Path(getName()).abbreviate(40).getPath();
        }

        @Override
        public String getExtension()
        {
            String name = new File(getName()).getName();
            int index = name.lastIndexOf(".");
            if (index > 0)
            {
                return name.substring(index);
            }
            return "";
        }

        @Override
        public long getSize()
        {
            return zipEntry.getSize();
        }

        @Override
        public boolean isDirectory()
        {
            return zipEntry.isDirectory();
        }

        @Override
        public int getDirectoryDepth()
        {
            return directoryDepth;
        }

        @Override
        public Date getModifiedDate()
        {
            return new Date(zipEntry.getTime());
        }

        @Override
        public String getMimetype()
        {
            return MimetypesFileTypeMap.getContentType(getName());
        }

        @Override
        public String toString()
        {
            return new StringBuilder(super.toString())
                    .append("-idx:").append(getEntryIndex())
                    .append("-name:").append(getName())
                    .append("-dir:").append(isDirectory())
                    .append("-size:").append(getSize())
                    .toString();
        }
    }

}
