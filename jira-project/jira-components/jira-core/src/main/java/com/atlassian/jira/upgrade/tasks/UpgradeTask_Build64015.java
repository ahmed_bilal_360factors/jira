package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.entity.Delete;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.portal.CachingPortletConfigurationStore;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Removes the JIRA Bugzilla ID search gadget.
 *
 * @since v6.4
 */
public class UpgradeTask_Build64015 extends AbstractImmediateUpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask_Build64015.class);

    private static final String BUGZILLA_GADGET_URI = "rest/gadgets/1.0/g/com.atlassian.jira.gadgets:bugzilla/gadgets/bugzilla-id-search.xml";
    private static final String GADGET_TABLE = "PortletConfiguration";
    private static final String GADGET_URI_COLUMN = "gadgetXml";

    private final OfBizDelegator ofBizDelegator;
    private final CachingPortletConfigurationStore cachingPortletConfigurationStore;

    public UpgradeTask_Build64015(final OfBizDelegator ofBizDelegator, CachingPortletConfigurationStore cachingPortletConfigurationStore)
    {
        super();
        this.ofBizDelegator = ofBizDelegator;
        this.cachingPortletConfigurationStore = cachingPortletConfigurationStore;
    }

    @Override
    public String getBuildNumber()
    {
        return "64015";
    }

    @Override
    public String getShortDescription()
    {
        return "Removing the JIRA Bugzilla ID search gadget.";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception
    {
        try
        {
            final DateTime startedAt = new DateTime();
            int deletedGadgets = Delete.from(GADGET_TABLE).whereLike(GADGET_URI_COLUMN, BUGZILLA_GADGET_URI).execute(ofBizDelegator);
            log.debug(String.format("Upgrade task took %d seconds to remove %d bugzilla gadgets.", Seconds.secondsBetween(startedAt, new DateTime()).getSeconds(), deletedGadgets));
        }
        finally
        {
            // making sure the portlet cache is cleared before a user renders an existing dashboard
            cachingPortletConfigurationStore.flush();
        }
    }
}
