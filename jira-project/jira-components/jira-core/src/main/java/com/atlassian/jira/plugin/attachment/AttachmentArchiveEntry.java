package com.atlassian.jira.plugin.attachment;

/**
 * An archive entry DTO.
 */
public interface AttachmentArchiveEntry
{
    /**
     * @return the entryIndex within the zip file
     */
    long getEntryIndex();

    /**
     * @return the file name of the zip entry
     */
    String getName();

    /**
     * @return the abbreviated file name
     * @deprecated Abbreviate the path (name) in views. It is view-specific operation. Since v6.4.
     */
    @Deprecated
    String getAbbreviatedName();

    /**
     * @return the size of the file uncompressed
     */
    long getSize();

    /**
     * @return The mimetype of the entry
     */
    String getMediaType();

}
