package com.atlassian.jira.model.querydsl;

import javax.annotation.Generated;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;

import org.ofbiz.core.entity.GenericValue;

/**
 * Data Transfer Object for the MovedIssueKey entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 *
 * @see QMovedIssueKey
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class MovedIssueKeyDTO
{
    private Long id;
    private String oldIssueKey;
    private Long issueId;

    public Long getId()
    {
        return id;
    }

    public String getOldIssueKey()
    {
        return oldIssueKey;
    }

    public Long getIssueId()
    {
        return issueId;
    }

    public MovedIssueKeyDTO(Long id, String oldIssueKey, Long issueId)
    {
        this.id = id;
        this.oldIssueKey = oldIssueKey;
        this.issueId = issueId;
    }
    /**
    * Creates a GenericValue object from the values in this Data Transfer Object.
    * <p>
    *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
    * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
    * @return a GenericValue object constructed from the values in this Data Transfer Object.
    */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator)
    {
        return ofBizDelegator.makeValue("MovedIssueKey", new FieldMap()
                        .add("id", id)
                        .add("oldIssueKey", oldIssueKey)
                        .add("issueId", issueId)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */

    public static MovedIssueKeyDTO fromGenericValue(GenericValue gv)
    {
        return new MovedIssueKeyDTO(
            gv.getLong("id"),
            gv.getString("oldIssueKey"),
            gv.getLong("issueId")
        );
    }
}

