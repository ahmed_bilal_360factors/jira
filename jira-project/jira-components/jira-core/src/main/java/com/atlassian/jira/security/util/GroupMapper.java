package com.atlassian.jira.security.util;

import java.util.Collection;

import com.atlassian.jira.scheme.Scheme;

public interface GroupMapper
{
    public Collection<Scheme> getMappedValues(String groupName);
}
