package com.atlassian.jira.issue.attachment.store;

import java.io.InputStream;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.fugue.Options;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.attachment.AttachmentDeleteException;
import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.AttachmentMoveException;
import com.atlassian.jira.issue.attachment.AttachmentReadException;
import com.atlassian.jira.issue.attachment.AttachmentRuntimeException;
import com.atlassian.jira.issue.attachment.AttachmentStreamGetData;
import com.atlassian.jira.issue.attachment.AttachmentWriteException;
import com.atlassian.jira.issue.attachment.NoAttachmentDataException;
import com.atlassian.jira.issue.attachment.RemoteAttachmentStore;
import com.atlassian.jira.issue.attachment.StoreAttachmentBean;
import com.atlassian.jira.issue.attachment.StoreAttachmentResult;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Supplier;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.atlassian.blobstore.client.api.Access;
import io.atlassian.blobstore.client.api.BlobStoreService;
import io.atlassian.blobstore.client.api.Failure;
import io.atlassian.blobstore.client.api.GetResult;
import io.atlassian.blobstore.client.api.HeadResult;
import io.atlassian.blobstore.client.api.PutResult;

import static com.google.common.base.Functions.compose;

/**
 * Implementation of an AttachmentStore that communicates with BlobStore.
 *
 * @since v6.3
 */
@ParametersAreNonnullByDefault
public final class BlobStoreAttachmentStore implements RemoteAttachmentStore
{
    private static final Access FOREVER_CACHE = Access.builder().setCacheControl(Access.CacheControl.FOREVER).build();

    private static final Logger log = LoggerFactory.getLogger(BlobStoreAttachmentStore.class);

    private final UniqueIdentifierGenerator uniqueIdentifierGenerator;

    public BlobStoreAttachmentStore(final UniqueIdentifierGenerator uniqueIdentifierGenerator)
    {
        this.uniqueIdentifierGenerator = uniqueIdentifierGenerator;
    }

    private BlobStoreService getBlobStore()
    {
        final BlobStoreService bss = ComponentAccessor.getOSGiComponentInstanceOfType(BlobStoreService.class);
        return bss != null ? bss : FailingBlobStoreService.INSTANCE;
    }

    @Nonnull
    @Override
    public <A> Promise<A> getAttachment(final AttachmentKey attachmentKey, final com.atlassian.util.concurrent.Function<InputStream, A> inputStreamProcessor)
    {
        return getAttachmentData(attachmentKey, new com.atlassian.jira.util.Function<AttachmentGetData, A>()
        {
            @Override
            public A get(final AttachmentGetData attachmentGetData)
            {
                return inputStreamProcessor.get(attachmentGetData.getInputStream());
            }
        });
    }

    @Override
    public <A> Promise<A> getAttachmentData(final AttachmentKey attachmentKey, final com.atlassian.util.concurrent.Function<AttachmentGetData, A> attachmentGetDataProcessor)
    {
        final String attachmentId = attachmentKey.getAttachmentId().toString();

        final Function<GetResult, AttachmentGetData> dataFromResult = new Function<GetResult, AttachmentGetData>()
        {
            @Override
            public AttachmentGetData apply(final GetResult getResult)
            {
                return new AttachmentStreamGetData(getResult.data(), getResult.head().contentLength());
            }
        };
        final Function<Failure, Promise<A>> failure = reject(new Function<Failure, Throwable>()
        {
            @Override
            public Throwable apply(final Failure failure)
            {
                return new AttachmentReadException(
                        "Remote blobstore couldn't provide an input stream for attachment " + attachmentId
                                + ": " + failure.message());
            }
        });
        final Supplier<Promise<A>> notFound = new Supplier<Promise<A>>()
        {
            @Override
            public Promise<A> get()
            {
                return Promises.rejected(new NoAttachmentDataException(
                        "Remote blobstore couldn't find an entry for attachment " + attachmentId));
            }
        };
        final Function<Option<A>, Promise<A>> result = fold(
                notFound,
                promise(Functions.<A>identity())

        );
        final Function<Either<Failure, Option<A>>, Promise<A>> fold = fold(failure, result);
        return getBlobStore()
                .get(attachmentId, FOREVER_CACHE, Options.lift(compose(toGoogleFunction(attachmentGetDataProcessor), dataFromResult)))
                .flatMap(fold);
    }

    private <T, R> Function<T, R> toGoogleFunction(final com.atlassian.util.concurrent.Function<T, R> inputStreamProcessor)
    {
        return com.atlassian.util.concurrent.Functions.toGoogleFunction(inputStreamProcessor);
    }

    @Nonnull
    @Override
    public Promise<StoreAttachmentResult> putAttachment(@Nonnull final StoreAttachmentBean storeAttachmentBean)
    {
        final InputStream dataStream = storeAttachmentBean.getStream();
        final String id = String.valueOf(storeAttachmentBean.getId());
        final Long size = storeAttachmentBean.getSize();

        return putBlobToStore(dataStream, id, size);
    }

    private Promise<StoreAttachmentResult> putBlobToStore(final InputStream dataStream, final String id, final Long size)
    {
        final Function<Failure, Promise<StoreAttachmentResult>> failureFunction = reject(new Function<Failure, Throwable>()
        {
            @Override
            public Throwable apply(final Failure failure)
            {
                IOUtils.closeQuietly(dataStream);
                return new AttachmentWriteException(failure.message());
            }
        });

        final Function<PutResult, Promise<StoreAttachmentResult>> result = promise(new Function<PutResult, StoreAttachmentResult>()
        {
            @Override
            public StoreAttachmentResult apply(final PutResult result)
            {
                IOUtils.closeQuietly(dataStream);
                final PutResult.Status status = result.status();
                if (status == PutResult.Status.CREATED)
                {
                    return StoreAttachmentResult.created();
                }
                else
                {
                    return StoreAttachmentResult.updated();
                }
            }
        });

        final Function<Either<Failure, PutResult>, Promise<StoreAttachmentResult>> fold = fold(failureFunction, result);
        return getBlobStore()
                .put(id, dataStream, size)
                .flatMap(fold);
    }

    @Nonnull
    @Override
    public Promise<Boolean> exists(final AttachmentKey attachmentKey)
    {
        final String attachmentId = attachmentKey.getAttachmentId().toString();

        final Function<Failure, Promise<Boolean>> failure = reject(new Function<Failure, Throwable>()
        {
            @Override
            public Throwable apply(final Failure failure)
            {
                return new AttachmentReadException(
                        "Remote blobstore couldn't provide an input stream for attachment " + attachmentId + ": " + failure.message());
            }
        });
        final Function<Option<HeadResult>, Promise<Boolean>> result = promise(new Function<Option<HeadResult>, Boolean>()
        {
            @Override
            public Boolean apply(final Option<HeadResult> headResult)
            {
                return headResult.isDefined();
            }
        });

        final Function<Either<Failure, Option<HeadResult>>, Promise<Boolean>> fold = fold(failure, result);
        return getBlobStore()
                .head(attachmentId, FOREVER_CACHE)
                .flatMap(fold);
    }

    @Nonnull
    @Override
    public Promise<Unit> deleteAttachment(final AttachmentKey attachmentKey)
    {
        return deleteBlob(attachmentKey.getAttachmentId().toString());
    }

    public Promise<Unit> deleteBlob(final String id)
    {
        final Function<Failure, Throwable> failure = new Function<Failure, Throwable>()
        {
            @Override
            public Throwable apply(final Failure failure)
            {
                return new AttachmentDeleteException(
                        "Remote blobstore couldn't delete attachment " + id + ": " + failure.message());
            }
        };
        final Function<Throwable, Unit> logFailure = new Function<Throwable, Unit>()
        {
            @Override
            public Unit apply(final Throwable t)
            {
                log.info("Logging and ignoring failure", t);
                return Unit.VALUE;
            }
        };
        final Function<Boolean, Unit> success = new Function<Boolean, Unit>()
        {
            @Override
            public Unit apply(final Boolean wasDeleted)
            {
                if (!wasDeleted) log.debug("Nothing to delete at id " + id);
                return Unit.VALUE;
            }
        };
        final Function<Either<Failure, Boolean>, Promise<Unit>> fold = fold(promise(compose(logFailure, failure)), promise(success));
        return getBlobStore().delete(id).flatMap(fold);
    }

    @Nonnull
    @Override
    public Option<ErrorCollection> errors()
    {
        if (FailingBlobStoreService.INSTANCE.equals(getBlobStore()))
        {
            final ErrorCollection errors = new SimpleErrorCollection();
            errors.addErrorMessage("BlobStore client plugin is not available. Cannot access BlobStore attachment storage.");
            return Option.some(errors);
        }
        else
        {
            return Option.none();
        }
    }

    @Override
    public Promise<Unit> moveAttachment(final AttachmentKey oldAttachmentKey, final AttachmentKey newAttachmentKey)
    {
        final String oldId = oldAttachmentKey.getAttachmentId().toString();
        final String newId = newAttachmentKey.getAttachmentId().toString();

        if(!oldId.equals(newId))
        {
            return moveBlob(oldId, newId);
        }
        else
        {
            return Promises.promise(Unit.VALUE);
        }
    }

    private Promise<Unit> moveBlob(final String oldId, final String newId)
    {
        final Function<Failure, Promise<Unit>> failure = reject(new Function<Failure, AttachmentMoveException>()
        {
            @Override
            public AttachmentMoveException apply(final Failure failure)
            {
                return new AttachmentMoveException(failure.message());
            }
        });

        final Function<Either<Failure, PutResult>, Promise<Unit>> fold = fold(failure, toUnitIgnoreResult());
        return getBlobStore()
                .move(oldId, newId)
                .flatMap(fold);
    }

    @Override
    public Promise<Unit> copyAttachment(final AttachmentKey sourceAttachmentKey, final AttachmentKey newAttachmentKey)
    {
        final String sourceId = sourceAttachmentKey.getAttachmentId().toString();
        final String newId = newAttachmentKey.getAttachmentId().toString();

        final Function<Failure, Promise<Unit>> reject = reject(new Function<Failure, Throwable>()
        {
            @Override
            public Throwable apply(final Failure failure)
            {
                return new AttachmentRuntimeException(failure.message());
            }
        });

        final Function<Either<Failure, PutResult>, Promise<Unit>> fold =
                fold(reject, toUnitIgnoreResult());

        return getBlobStore().copy(sourceId, newId).flatMap(fold);
    }

    @Nonnull
    @Override
    public Promise<TemporaryAttachmentId> putTemporaryAttachment(final InputStream inputStream, final long size)
    {
        final String tempId = uniqueIdentifierGenerator.getNextId();
        final TemporaryAttachmentId temporaryAttachmentId = TemporaryAttachmentId.fromString(tempId);

        return putBlobToStore(inputStream, tempId, size).map(Functions.constant(temporaryAttachmentId));
    }

    @Override
    public Promise<Unit> moveTemporaryToAttachment(final TemporaryAttachmentId temporaryAttachmentId, final AttachmentKey destinationKey)
    {
        final String destinationId = destinationKey.getAttachmentId().toString();
        return moveBlob(temporaryAttachmentId.toStringId(), destinationId);
    }

    @Override
    public Promise<Unit> deleteTemporaryAttachment(final TemporaryAttachmentId temporaryAttachmentId)
    {
        return deleteBlob(temporaryAttachmentId.toStringId());
    }

    private static <A> Function<A, Promise<Unit>> toUnitIgnoreResult()
    {
        return new Function<A, Promise<Unit>>()
        {
            @Override
            public Promise<Unit> apply(final Object ignore)
            {
                return Promises.promise(Unit.VALUE);
            }
        };
    }

    /**
     * @see Either#fold
     */
    private static <L, R, O> Function<Either<L, R>, O> fold(final Function<L, O> left, final Function<? super R, O> right)
    {
        return new Function<Either<L, R>, O>()
        {
            @Override
            public O apply(final Either<L, R> either)
            {
                return either.fold(left, right);
            }
        };
    }

    /**
     * @see Option#fold
     */
    private static <A, B> Function<Option<A>, B> fold(final Supplier<B> none, final Function<A, B> some)
    {
        return new Function<Option<A>, B>()
        {
            @Override
            public B apply(final Option<A> op)
            {
                return op.fold(none, some);
            }
        };
    }

    /**
     * Transform a function that returns a value to one that returns a fulfilled promise.
     */
    private static <A, B> Function<A, Promise<B>> promise(final Function<A, B> f)
    {
        return compose(BlobStoreAttachmentStore.<B>promise(), f);
    }

    /**
     * Transform a value to a fulfilled promise.
     */
    private static <A> Function<A, Promise<A>> promise()
    {
        return new Function<A, Promise<A>>()
        {
            @Override
            public Promise<A> apply(final A a)
            {
                return Promises.promise(a);
            }
        };
    }

    /**
     * Transform a function that returns a Throwable to one that returns a failed promise.
     */
    private static <A, B> Function<A, Promise<B>> reject(final Function<A, ? extends Throwable> f)
    {
        return compose(BlobStoreAttachmentStore.<B>reject(), f);
    }

    /**
     * Transform a Throwable to a failed promise.
     */
    private static <A> Function<Throwable, Promise<A>> reject()
    {
        return new Function<Throwable, Promise<A>>()
        {
            @Override
            public Promise<A> apply(final Throwable t)
            {
                return Promises.rejected(t);
            }
        };
    }

    private static final class FailingBlobStoreService implements BlobStoreService
    {
        private static final BlobStoreService INSTANCE = new FailingBlobStoreService();

        private static final String MESSAGE = "BlobStore Client Plugin not loaded";

        private <A> Promise<A> fail()
        {
            return Promises.rejected(new UnsupportedOperationException(MESSAGE));
        }

        @Override
        public <A> Promise<Either<Failure, A>> get(final String key, final Access options, final Function<Option<GetResult>, A> f)
        {
            return fail();
        }

        @Override
        public Promise<Either<Failure, PutResult>> put(final String key, final InputStream stream, final Long contentLength)
        {
            return fail();
        }

        @Override
        public Promise<Either<Failure, Boolean>> delete(final String key)
        {
            return fail();
        }

        @Override
        public Promise<Either<Failure, Boolean>> delete(final String key, final String hash)
        {
            return fail();
        }

        @Override
        public Promise<Either<Failure, PutResult>> move(final String sourceKey, final String destinationKey)
        {
            return fail();
        }

        @Override
        public Promise<Either<Failure, PutResult>> copy(final String sourceKey, final String destinationKey)
        {
            return fail();
        }

        @Override
        public Promise<Either<Failure, Option<HeadResult>>> head(final String key, final Access options)
        {
            return fail();
        }

        @Override
        public void reset()
        {
        }
    }
}
