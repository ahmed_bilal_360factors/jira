package com.atlassian.jira.web.action.admin.constants;

import java.util.Collection;

import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.web.action.JiraWebActionSupport;

public abstract class AbstractConstantAction<T extends IssueConstant> extends JiraWebActionSupport
{
    protected abstract String getConstantEntityName();

    protected abstract String getNiceConstantName();

    protected abstract String getIssueConstantField();

    protected abstract String getRedirectPage();

    protected abstract T getConstant(String id);

    /**
     * Get a collection of this constant
     */
    protected abstract Collection<T> getConstants();

    /**
     * Clear caches related to this constant
     */
    protected abstract void clearCaches();

    /**
     * Is the constant passed in the default constant
     */
}
