package com.atlassian.jira.model.querydsl;

import com.mysema.query.sql.ColumnMetadata;
import com.mysema.query.types.path.*;

import java.sql.Types;
import javax.annotation.Generated;

/**
 * QDraftWorkflow is a Querydsl query object.
 *
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QDraftWorkflow extends JiraRelationalPathBase<DraftWorkflowDTO>
{
    public static final QDraftWorkflow DRAFT_WORKFLOW = new QDraftWorkflow("DRAFT_WORKFLOW");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final StringPath parentname = createString("parentname");
    public final StringPath descriptor = createString("descriptor");

    public QDraftWorkflow(String alias)
    {
        super(DraftWorkflowDTO.class, alias, "jiradraftworkflows");
        addMetadata();
    }

    private void addMetadata()
    {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(parentname, ColumnMetadata.named("parentname").withIndex(2).ofType(Types.VARCHAR).withSize(255));
        addMetadata(descriptor, ColumnMetadata.named("descriptor").withIndex(3).ofType(Types.VARCHAR).withSize(2147483647));
    }
}

