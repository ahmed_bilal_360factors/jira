package com.atlassian.jira.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseType;
import com.atlassian.jira.config.database.jdbcurlparser.JdbcUrlParser;
import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.ofbiz.OfBizConnectionFactory;

import com.google.common.annotations.VisibleForTesting;

public class DatabaseCollationReader
{
    private final OfBizConnectionFactory connectionFactory;
    private final DatabaseConfig databaseConfig;

    private static final String ORACLE_GET_COLLATION_SQL = "SELECT VALUE from NLS_DATABASE_PARAMETERS WHERE PARAMETER='NLS_CHARACTERSET'";
    private static final String MYSQL_GET_COLLATION_SQL = "SELECT DEFAULT_COLLATION_NAME from information_schema.SCHEMATA S where SCHEMA_NAME = ?";
    private static final String POSTGRES_GET_COLLATION_SQL = "SELECT datcollate FROM pg_database WHERE datname = ?";
    private static final String SQL_SERVER_GET_COLLATION_SQL = "SELECT collation_name FROM sys.databases WHERE name=?";

    public DatabaseCollationReader(OfBizConnectionFactory connectionFactory, DatabaseConfig databaseConfig)
    {
        this.connectionFactory = connectionFactory;
        this.databaseConfig = databaseConfig;
    }

    public String findCollation() throws SQLException, ParseException
    {
        Connection connection = null;

        try
        {
            connection = connectionFactory.getConnection();
            PreparedStatement ps = generateCollationQuery(databaseConfig, connection);
            if (ps != null)
            {
                try
                {
                    ResultSet rs = ps.executeQuery();
                    rs.next();
                    return rs.getString(1);
                }
                finally
                {
                    ps.close();
                }
            }
            else
            {
                return null;
            }
        }
        finally
        {
            if (connection != null)
            {
                connection.close();
            }
        }
    }

    private PreparedStatement generateCollationQuery(final DatabaseConfig databaseConfig, Connection connection)
            throws SQLException, ParseException
    {
        PreparedStatement ps = null;

        if (databaseConfig.isOracle())
        {
            ps = connection.prepareStatement(ORACLE_GET_COLLATION_SQL);
        }
        else if (databaseConfig.isMySql())
        {
            ps = connection.prepareStatement(MYSQL_GET_COLLATION_SQL);
            ps.setString(1, getDatabaseName(databaseConfig, connection));
        }
        else if (databaseConfig.isPostgres())
        {
            ps = connection.prepareStatement(POSTGRES_GET_COLLATION_SQL);
            ps.setString(1, getDatabaseName(databaseConfig, connection));
        }
        else if (databaseConfig.isSqlServer())
        {
            ps = connection.prepareStatement(SQL_SERVER_GET_COLLATION_SQL);
            ps.setString(1, getDatabaseName(databaseConfig, connection));
        }

        return ps;
    }

    /**
     * Returns the database name from the JDBC url
     */
    private String getDatabaseName(final DatabaseConfig databaseConfig, Connection connection)
            throws ParseException, SQLException
    {
        JdbcUrlParser urlParser = DatabaseType.forDatabaseTypeName(databaseConfig.getDatabaseType()).getJdbcUrlParser();
        String url = connection.getMetaData().getURL();

        return urlParser.parseUrl(url).getInstance();
    }

    @VisibleForTesting
    static String[] getCollationQueries()
    {
        return new String[] {
                ORACLE_GET_COLLATION_SQL,
                MYSQL_GET_COLLATION_SQL,
                POSTGRES_GET_COLLATION_SQL,
                SQL_SERVER_GET_COLLATION_SQL
        };
    }
}