package com.atlassian.jira.config.properties;

import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.event.property.BooleanApplicationPropertySetEvent;
import com.atlassian.jira.event.property.StringApplicationPropertySetEvent;
import com.atlassian.jira.util.LocaleParser;

import com.google.common.annotations.VisibleForTesting;
import com.opensymphony.util.TextUtils;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class to manage the interface with a single property set, used for application properties
 */
@EventComponent
public class ApplicationPropertiesImpl implements ApplicationProperties
{
    private static final Logger log = LoggerFactory.getLogger(ApplicationPropertiesImpl.class);

    @VisibleForTesting
    public static final String DEFAULT_ENCODING = "UTF-8";

    private final ApplicationPropertiesStore applicationPropertiesStore;
    private final Locale defaultLocale = Locale.getDefault();

    public ApplicationPropertiesImpl(ApplicationPropertiesStore applicationPropertiesStore)
    {
        this.applicationPropertiesStore = applicationPropertiesStore;
    }

    @SuppressWarnings ({ "UnusedDeclaration" })
    @EventListener
    public void onClearCache(final ClearCacheEvent event)
    {
        refresh();
    }

    @Override
    public String getText(final String name)
    {
        return this.applicationPropertiesStore.getTextFromDb(name);
    }
    @Override
    public String getString(final String name)
    {
        return applicationPropertiesStore.getStringFromDb(name);
    }

    @Override
    public Collection<String> getDefaultKeys()
    {
        return getDefaultProperties().keySet();
    }

    @Override
    public String getDefaultBackedString(final String name)
    {
        return applicationPropertiesStore.getString(name);
    }

    @Override
    public String getDefaultBackedText(final String name)
    {
        String value = null;
        try
        {
            value = getText(name);
        }
        catch (final Exception e)
        {
            log.warn("Exception getting property '" + name + "' from database. Using default");
        }
        if (value == null)
        {
            value = getDefaultString(name);
        }
        return value;
    }

    @Override
    public String getDefaultString(final String name)
    {
        return getDefaultProperties().get(name);
    }

    @Override
    public boolean getOption(final String key)
    {
        return applicationPropertiesStore.getOption(key);
    }

    @Override
    public Collection<String> getKeys()
    {
        return applicationPropertiesStore.getKeysStoredInDb();
    }

    @Override
    public Map<String, Object> asMap()
    {
        return applicationPropertiesStore.getPropertiesAsMap();
    }

    @Override
    public void setString(final String key, final String value)
    {
        applicationPropertiesStore.setString(key, value);
        ComponentAccessor.getComponent(EventPublisher.class).publish(new StringApplicationPropertySetEvent(key, value));
    }

    @Override
    public void setText(final String key, final String value)
    {
        applicationPropertiesStore.setText(key, value);
        ComponentAccessor.getComponent(EventPublisher.class).publish(new StringApplicationPropertySetEvent(key, value));
    }
    @Override
    public void setOption(final String key, final boolean value)
    {
        applicationPropertiesStore.setOption(key, value);
        ComponentAccessor.getComponent(EventPublisher.class).publish(new BooleanApplicationPropertySetEvent(key, value));
    }

    @Override
    public String getEncoding()
    {
        String encoding = getString(APKeys.JIRA_WEBWORK_ENCODING);
        if (!TextUtils.stringSet(encoding))
        {
            encoding = DEFAULT_ENCODING;
            setString(APKeys.JIRA_WEBWORK_ENCODING, encoding);
        }
        return encoding;
    }

    @Override
    public String getMailEncoding()
    {
        String encoding = getDefaultBackedString(APKeys.JIRA_MAIL_ENCODING);
        if (!TextUtils.stringSet(encoding))
        {
            encoding = getEncoding();

        }
        return encoding;
    }

    @Override
    public String getContentType()
    {
        return "text/html; charset=" + getEncoding();
    }

    @Override
    public void refresh()
    {
        applicationPropertiesStore.refreshDbProperties();
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this).append("propertiesManager", applicationPropertiesStore).toString();
    }

    private Map<String, String> getDefaultProperties()
    {
        return applicationPropertiesStore.getDefaultsWithOverlays();
    }

    @Override
    public Locale getDefaultLocale()
    {
        // The default locale almost never changes, but we must handle it correctly when it does.
        // We store the Locale for the defaultLocale string, which is expensive to create, in a very small cache (map).
        final String localeString = getDefaultBackedString(APKeys.JIRA_I18N_DEFAULT_LOCALE);
        if (localeString != null)
        {
            return LocaleParser.parseLocale(localeString);
        }
        return defaultLocale;
    }

    @Override
    public Collection<String> getStringsWithPrefix(final String prefix)
    {
        return applicationPropertiesStore.getStringsWithPrefixFromDb(prefix);
    }
}
