package com.atlassian.jira.index.ha;

import java.io.File;
import java.io.IOException;

/**
 * A contribution to the index snapshot.
 * This allows a client to add additional arbitrary information into the index.
 */
public interface IndexSnapshotContribution
{
    /**
     * Write a contribution into the index snapshot
     * @param indexDir the index directory
     * @throws IOException if the contribution cannot be written
     */
    public void writeContribution(File indexDir) throws IOException;
}
