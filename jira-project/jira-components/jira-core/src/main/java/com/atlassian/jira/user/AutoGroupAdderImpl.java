package com.atlassian.jira.user;

import com.atlassian.crowd.core.event.listener.AutoGroupAdderListener;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.util.ComponentLocator;

/**
 * Registers an {@link AutoGroupAdderListener}.
 *
 * @since v4.4
 */
public class AutoGroupAdderImpl implements AutoGroupAdder, Startable
{
    private final DirectoryManager directoryManager;
    private final EventPublisher eventPublisher;

    public AutoGroupAdderImpl(final DirectoryManager directoryManager, final EventPublisher eventPublisher)
    {
        this.directoryManager = directoryManager;
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void start() throws Exception
    {
        eventPublisher.register(new AutoGroupAdderListener(directoryManager));
    }
}
