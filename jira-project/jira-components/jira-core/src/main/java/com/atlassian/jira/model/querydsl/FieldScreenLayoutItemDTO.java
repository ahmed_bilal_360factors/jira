package com.atlassian.jira.model.querydsl;

import javax.annotation.Generated;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;

import org.ofbiz.core.entity.GenericValue;

/**
 * Data Transfer Object for the FieldScreenLayoutItem entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 *
 * @see QFieldScreenLayoutItem
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class FieldScreenLayoutItemDTO
{
    private Long id;
    private String fieldidentifier;
    private Long sequence;
    private Long fieldscreentab;

    public Long getId()
    {
        return id;
    }

    public String getFieldidentifier()
    {
        return fieldidentifier;
    }

    public Long getSequence()
    {
        return sequence;
    }

    public Long getFieldscreentab()
    {
        return fieldscreentab;
    }

    public FieldScreenLayoutItemDTO(Long id, String fieldidentifier, Long sequence, Long fieldscreentab)
    {
        this.id = id;
        this.fieldidentifier = fieldidentifier;
        this.sequence = sequence;
        this.fieldscreentab = fieldscreentab;
    }
    /**
    * Creates a GenericValue object from the values in this Data Transfer Object.
    * <p>
    *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
    * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
    * @return a GenericValue object constructed from the values in this Data Transfer Object.
    */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator)
    {
        return ofBizDelegator.makeValue("FieldScreenLayoutItem", new FieldMap()
                        .add("id", id)
                        .add("fieldidentifier", fieldidentifier)
                        .add("sequence", sequence)
                        .add("fieldscreentab", fieldscreentab)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */

    public static FieldScreenLayoutItemDTO fromGenericValue(GenericValue gv)
    {
        return new FieldScreenLayoutItemDTO(
            gv.getLong("id"),
            gv.getString("fieldidentifier"),
            gv.getLong("sequence"),
            gv.getLong("fieldscreentab")
        );
    }
}

