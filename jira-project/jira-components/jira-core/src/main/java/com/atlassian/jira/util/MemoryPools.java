package com.atlassian.jira.util;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryType;
import java.lang.management.MemoryUsage;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import static com.atlassian.jira.util.TextUtil.lpad;
import static com.atlassian.jira.util.TextUtil.rpad;

/**
 * Gathers data about the JVM memory pools
 */
public class MemoryPools
{
    public static List<MemoryPool> getMemoryPools()
    {
        final List<MemoryPoolMXBean> memoryPoolMXBeans = ManagementFactory.getMemoryPoolMXBeans();
        final List<MemoryPool> memoryPools = new ArrayList<MemoryPool>(memoryPoolMXBeans.size());
        for (MemoryPoolMXBean memoryPoolMXBean : memoryPoolMXBeans)
        {
            final MemoryUsage memoryUsage = memoryPoolMXBean.getUsage();

            final MemoryPool memoryPool = new MemoryPool(
                    memoryPoolMXBean.getName(),
                    memoryUsage.getUsed(),
                    memoryUsage.getCommitted(),
                    memoryUsage.getMax(),
                    memoryPoolMXBean.getType()
            );
            memoryPools.add(memoryPool);
        }
        return memoryPools;
    }

    /**
     * Gathers all the MemoryPool data in a String to be dumped to log file or similar.
     * @param detailByPool if true, we list each pool individually, else we summarise by type - that is Heap vs non-heap
     * @return dump of memory
     */
    public static String memoryPoolsDump(boolean detailByPool)
    {
        List<MemoryPool> pools = getMemoryPools();
        EnumMap<MemoryType, PoolTotals> totalsByType = new EnumMap<MemoryType, PoolTotals>(MemoryType.class);
        for (MemoryType memoryType : MemoryType.values())
        {
            totalsByType.put(memoryType, new PoolTotals());
        }
        PoolTotals totalMemory = new PoolTotals();

        String poolsInfo = "";
        poolsInfo += "---------------------------------------------------------------------------------";
        for (MemoryPool pool : pools)
        {
            PoolTotals byType = totalsByType.get(pool.getType());
            byType.add(pool);
            totalMemory.add(pool);
            if (detailByPool)
            {
                poolsInfo += "\n  " +
                        rpad(pool.getName(), 24) +
                        ":  Used: " + inMiB(pool.getUsed()) +
                        ".  Committed: " + inMiB(pool.getCommitted()) +
                        ".  Max: " + inMiB(pool.getMax());
            }
        }
        if (detailByPool)
        {
            poolsInfo += "\n---------------------------------------------------------------------------------";
        }
        for (MemoryType memoryType : MemoryType.values())
        {
            PoolTotals byType = totalsByType.get(memoryType);
            poolsInfo += "\n  " +
                    rpad(memoryType.toString(), 16) +
                    ":  Used: " + inMiB(byType.used) +
                    ".  Committed: " + inMiB(byType.committed) +
                    ".  Max: " + inMiB(byType.max);
        }
        poolsInfo += "\n---------------------------------------------------------------------------------";
        poolsInfo += "\n  " +
                rpad("TOTAL", 16) +
                ":  Used: " + inMiB(totalMemory.used) +
                ".  Committed: " + inMiB(totalMemory.committed) +
                ".  Max: " + inMiB(totalMemory.max);
        poolsInfo += "\n---------------------------------------------------------------------------------";

        return poolsInfo;
    }

    public static String inMiB(final long bytes)
    {
        if (bytes == -1)
        {
            // Undefined
            return "    -    ";
        }
        return lpad(((bytes + 512 * 1024) / (1024 * 1024)) + " MiB", 8);
    }

    static class PoolTotals
    {
        private long used = 0;
        private long committed = 0;
        private long max = 0;

        public void add(final MemoryPool pool)
        {
            used += pool.getUsed();
            committed += pool.getCommitted();
            // getMax will return -1 for "undefined" max
            if (pool.getMax() > 0)
            {
                max += pool.getMax();
            }
        }
    }
}
