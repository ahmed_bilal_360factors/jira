package com.atlassian.jira.startup;

import javax.servlet.ServletContext;

import com.atlassian.jira.web.ServletContextProvider;

import com.opensymphony.util.TextUtils;

/**
 * A StartupCheck that checks the Webapp is configured correctly.
 * <p/>
 * This currently just checks that {@link ServletContext#getRealPath} works for "/", and thus that we're not running
 * from a packed WAR.
 *
 * @since v6.4
 */
public class JiraWebappStartupCheck implements StartupCheck
{
    private static final String NAME = "JIRA Webapp Configuration Check";
    // Note that this string is known to Hercules/Sisyphus and Support need to know if it changes
    // The corresponding CAC page in the knowledge base is https://confluence.atlassian.com/x/FBLVGQ
    private static final String FAULT_DESCRIPTION = "Running JIRA from a packed WAR is not supported. Configure your"
            + " Servlet container to unpack the WAR before running it.";

    @Override
    public String getName()
    {
        return NAME;
    }

    @Override
    public boolean isOk()
    {
        final ServletContext servletContext = ServletContextProvider.getServletContext();
        final String realPath = servletContext.getRealPath("/");
        return realPath != null;
    }

    @Override
    public String getFaultDescription()
    {
        return FAULT_DESCRIPTION;
    }

    @Override
    public String getHTMLFaultDescription()
    {
        return TextUtils.htmlEncode(getFaultDescription());
    }

    @Override
    public void stop()
    {
        // Nothing to check on shutdown
    }
}
