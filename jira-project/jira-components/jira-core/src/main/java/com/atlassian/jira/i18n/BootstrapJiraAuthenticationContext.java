package com.atlassian.jira.i18n;

import java.util.Collections;
import java.util.Locale;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.plugin.language.TranslationTransform;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.i18n.I18nTranslationModeImpl;
import com.atlassian.jira.web.util.OutlookDate;

/**
 * Simple JiraAuthenticationContext to be used in the Bootstrap container, because it is needed to show start up errors.
 * <p>
 *     Basically it says all access is anonymous, and it always returns an I18nHelper in English.
 * </p>
 *
 * @since v6.4.5
 */
public class BootstrapJiraAuthenticationContext implements JiraAuthenticationContext
{
    private I18nHelper i18nHelper;

    public BootstrapJiraAuthenticationContext()
    {
        // Builds a hard-coded I18nHelper that will always display in English
        this.i18nHelper = new BackingI18n(getLocale(), new I18nTranslationModeImpl(), Collections.<TranslationTransform>emptyList(), new BootstrapTranslationStore(getLocale()));
    }

    @Override
    public ApplicationUser getUser()
    {
        return null;
    }

    @Override
    public User getLoggedInUser()
    {
        return null;
    }

    @Override
    public boolean isLoggedInUser()
    {
        return false;
    }

    @Override
    public Locale getLocale()
    {
        return Locale.UK;
    }

    @Override
    public OutlookDate getOutlookDate()
    {
        throw new IllegalStateException("Not implemented in the Bootstrap Container");
    }

    @Override
    public String getText(final String key)
    {
        return getI18nHelper().getText(key);
    }

    @Override
    public I18nHelper getI18nHelper()
    {
        return i18nHelper;
    }

    @Override
    public I18nHelper getI18nBean()
    {
        return getI18nHelper();
    }

    @Override
    public void setLoggedInUser(final User user)
    {
        throw new IllegalStateException("Not implemented in the Bootstrap Container");
    }

    @Override
    public void setLoggedInUser(final ApplicationUser user)
    {
        throw new IllegalStateException("Not implemented in the Bootstrap Container");
    }

    @Override
    public void clearLoggedInUser()
    {
        // no-op
    }
}
