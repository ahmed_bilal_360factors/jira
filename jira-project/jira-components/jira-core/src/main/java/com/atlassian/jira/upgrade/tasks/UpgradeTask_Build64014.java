package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.config.DefaultFeatureManager;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;
import com.google.common.collect.ImmutableMap;
import org.ofbiz.core.entity.GenericValue;

import java.util.List;

/**
 * Turns on the dark feature for project centric nav switch (to allow the user to turn it on and off)
 * and the embedded issue navigator to be on by default.
 *
 * N.B. The project centric nav itself will enabled by default (no dark feature needed for this)
 */
public class UpgradeTask_Build64014 extends AbstractImmediateUpgradeTask
{
    private static final String FEATURE_TYPE_SITE = "site";

    private static final String SWITCH_FEATURE_KEY = "com.atlassian.jira.projects.ProjectCentricNavigation.Switch";
    private static final String ISSUENAV_FEATURE_KEY = "com.atlassian.jira.projects.issuenavigator";

    private static final class Field
    {
        private static final String FEATURE_NAME = "featureName";
        private static final String FEATURE_TYPE = "featureType";
        private static final String USERKEY = "userKey";
    }

    private final OfBizDelegator ofBizDelegator;
    private final DefaultFeatureManager defaultFeatureManager;

    public UpgradeTask_Build64014(OfBizDelegator ofBizDelegator, DefaultFeatureManager defaultFeatureManager)
    {
        this.ofBizDelegator = ofBizDelegator;
        this.defaultFeatureManager = defaultFeatureManager;
    }

    @Override
    public String getBuildNumber()
    {
        return "64014";
    }

    @Override
    public String getShortDescription()
    {
        return "Enabling Project Centric Navigation Switch and new Project Issue Navigator by default";
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception
    {
        try
        {
            // we want to add this for both new instances and upgraded instances so ignoring setupMode param

            // Add the dark feature for users to be able to turn on and off the project centric nav
            List<GenericValue> existingSwitchFeature = ofBizDelegator.findByField(Entity.Name.FEATURE, Field.FEATURE_NAME, SWITCH_FEATURE_KEY);
            if (existingSwitchFeature.isEmpty())
            {
                ofBizDelegator.createValue(Entity.Name.FEATURE, ImmutableMap.<String, Object>of(
                        Field.FEATURE_NAME, SWITCH_FEATURE_KEY,
                        Field.FEATURE_TYPE, FEATURE_TYPE_SITE,
                        Field.USERKEY, ""));
            }

            // Add the feature flag so that the "All issues" link in the project sidebar takes you to a custom project centric
            // issue navigator instead of the full issue navigator.
            List<GenericValue> existingIssueFeature = ofBizDelegator.findByField(Entity.Name.FEATURE, Field.FEATURE_NAME, ISSUENAV_FEATURE_KEY);
            if (existingIssueFeature.isEmpty())
            {
                ofBizDelegator.createValue(Entity.Name.FEATURE, ImmutableMap.<String, Object>of(
                        Field.FEATURE_NAME, ISSUENAV_FEATURE_KEY,
                        Field.FEATURE_TYPE, FEATURE_TYPE_SITE,
                        Field.USERKEY, ""));
            }
        }
        finally
        {
            //clear the feature manager cache to ensure we-reread the latest state from the database.
            defaultFeatureManager.onClearCache(null);
        }
    }
}
