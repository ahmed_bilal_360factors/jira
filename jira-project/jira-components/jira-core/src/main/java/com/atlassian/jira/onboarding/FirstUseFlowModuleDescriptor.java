package com.atlassian.jira.onboarding;

import javax.annotation.Nullable;

import com.atlassian.jira.plugin.AbstractJiraModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.ModuleFactory;

import com.google.common.annotations.VisibleForTesting;

import org.dom4j.Element;

public class FirstUseFlowModuleDescriptor extends AbstractJiraModuleDescriptor<FirstUseFlow> implements Comparable<FirstUseFlowModuleDescriptor>
{
    @VisibleForTesting
    static final int UNWEIGHTED = -1;

    private int weight;

    public FirstUseFlowModuleDescriptor(final JiraAuthenticationContext authenticationContext, final ModuleFactory moduleFactory)
    {
        super(authenticationContext, moduleFactory);
    }

    @Override
    public void init(final Plugin plugin, final Element element) throws PluginParseException
    {
        super.init(plugin, element);

        final String weightValue = element.attributeValue("weight");
        try
        {
            this.weight = Integer.parseInt(weightValue);
        }
        catch (NumberFormatException e)
        {
            this.weight = UNWEIGHTED;
        }
    }

    public int getWeight()
    {
        return weight;
    }

    @Override
    public int compareTo(@Nullable final FirstUseFlowModuleDescriptor o)
    {
        if (o == null)
        {
            // Nulls are pushed to the "right" (or the end) of a sort
            return -1;
        }
        return Integer.valueOf(o.getWeight()).compareTo(getWeight());
    }
}
