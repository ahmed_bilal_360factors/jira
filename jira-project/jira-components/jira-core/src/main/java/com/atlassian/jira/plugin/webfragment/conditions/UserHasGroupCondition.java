package com.atlassian.jira.plugin.webfragment.conditions;

import java.util.Map;
import java.util.Set;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.plugin.PluginParseException;

public class UserHasGroupCondition extends AbstractJiraCondition {

	public String groupName = "manager";
	
	public void init(Map params) throws PluginParseException {
		String value = (String) params.get("group");
		if (value == null) {
			throw new PluginParseException("Could not determine group for: " + params.get("group"));
		}
		groupName = value;
		super.init(params);
	}
	
	public boolean shouldDisplay(User user, JiraHelper jiraHelper) {
		UserUtil userUtil = ComponentAccessor.getComponentOfType(UserUtil.class);
		Set<Group> groups = userUtil.getGroupsForUser(user.getName());
		for (Group group : groups) {
			if(group.getName().equalsIgnoreCase(groupName)) {
				return true;
			}
		}
		return false;
	}
	
}
