package com.atlassian.jira.web.action.setup;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.SystemPropertyKeys;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.HttpServletVariables;

import com.google.common.collect.ImmutableMap;

import org.apache.commons.httpclient.Cookie;

/**
 * Stores items shared across setup steps. Future work should make this class vanish.
 */
public class SetupSharedVariables
{
    public static final String SETUP_CHOOSEN_BUNDLE = "setup-chosen-bundle";
    private static final String SETUP_BUNDLE_LICENSE_KEY = "setup-bundle-license-key";
    private static final String SETUP_BUNDLE_LICENSE_COOKIES = "setup-bundle-license-cookies";
    private static final String SETUP_BUNDLE_HAS_LICENSE_ERROR = "setup-bundle-has-license-error";
    private static final String SETUP_INSTANT_JIRA_LICENSE_KEY = "setup-instant-jira-license-key";
    private static final String SETUP_INSTANT_MAC_EMAIL = "setup-instant-mac-email";
    private static final String SETUP_INSTANT_MAC_PASSWORD = "setup-instant-mac-password";
    private static final String SETUP_JIRA_LOCAL = "setup-jira-local";

    private final ApplicationProperties applicationProperties;
    private final HttpServletVariables servletVariables;
    private final JiraProperties jiraProperties;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public SetupSharedVariables(final HttpServletVariables servletVariables,
            final ApplicationProperties applicationProperties,
            final JiraProperties jiraProperties,
            final JiraAuthenticationContext jiraAuthenticationContext)
    {
        this.servletVariables = servletVariables;
        this.applicationProperties = applicationProperties;
        this.jiraProperties = jiraProperties;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    public String getSelectedBundle()
    {
        final String selectedBundle = (String) servletVariables.getHttpSession().getAttribute(SETUP_CHOOSEN_BUNDLE);

        if (selectedBundle == null)
        {
            return applicationProperties.getString(APKeys.JIRA_SETUP_CHOSEN_BUNDLE);
        }

        return selectedBundle;
    }

    public String getSelectedBundleName()
    {
        String bundleName = null;
        String selection = getSelectedBundle();

        if (SetupProductBundle.BUNDLE_DEVELOPMENT.equals(selection))
        {
            bundleName = "JIRA Agile";
        }
        else if (SetupProductBundle.BUNDLE_SERVICEDESK.equals(selection))
        {
            bundleName = "Service Desk";
        }

        return bundleName;
    }

    public void setSelectedBundle(String selection)
    {
        servletVariables.getHttpSession().setAttribute(SETUP_CHOOSEN_BUNDLE, selection);
        applicationProperties.setString(APKeys.JIRA_SETUP_CHOSEN_BUNDLE, selection);

        final String jiraProductName = "JIRA";
        final String bundleName = getSelectedBundleName();
        final String productName;

        if (bundleName != null)
        {
            final I18nHelper i18nHelper = jiraAuthenticationContext.getI18nHelper();
            productName = i18nHelper.getText("setup.bundle.product.name", jiraProductName, bundleName);
        }
        else
        {
            productName = jiraProductName;
        }

        jiraProperties.setProperty(SystemPropertyKeys.PRODUCT_NAME, productName);
    }

    public String getBundleLicenseKey()
    {
        return (String) servletVariables.getHttpSession().getAttribute(SETUP_BUNDLE_LICENSE_KEY);
    }

    public void setBundleLicenseKey(String licenseKey)
    {
        servletVariables.getHttpSession().setAttribute(SETUP_BUNDLE_LICENSE_KEY, licenseKey);
    }

    /**
     * Get previously saved JIRA evaluation license key. This method is intended to be used
     * only within Instant Setup path.
     *
     * @return JIRA license
     */
    public String getJiraLicenseKey()
    {
        return (String) servletVariables.getHttpSession().getAttribute(SETUP_INSTANT_JIRA_LICENSE_KEY);
    }

    /**
     * Save JIRA evaluation license key. This method is intended to be used only within Instant Setup path.
     *
     * @param licenseKey JIRA license
     */
    public void setJiraLicenseKey(String licenseKey)
    {
        servletVariables.getHttpSession().setAttribute(SETUP_INSTANT_JIRA_LICENSE_KEY, licenseKey);
    }

    public Set<Cookie> getBundleLicenseCookies()
    {
        final Object cookies = servletVariables.getHttpSession().getAttribute(SETUP_BUNDLE_LICENSE_COOKIES);

        if (cookies != null)
        {
            return (Set<Cookie>) cookies;
        }

        return Collections.emptySet();
    }

    public void setBundleLicenseCookies(final Set<Cookie> cookies)
    {
        servletVariables.getHttpSession().setAttribute(SETUP_BUNDLE_LICENSE_COOKIES, cookies);
    }

    public void removeBundleLicenseCookies()
    {
        servletVariables.getHttpSession().removeAttribute(SETUP_BUNDLE_LICENSE_COOKIES);
    }

    public String getBaseUrl()
    {
        final HttpServletRequest request = servletVariables.getHttpRequest();

        return request.getScheme() + "://localhost:" + request.getLocalPort() + request.getContextPath();
    }

    public boolean getBundleHasLicenseError()
    {
        Boolean hasError = (Boolean) servletVariables.getHttpSession().getAttribute(SETUP_BUNDLE_HAS_LICENSE_ERROR);

        return hasError == null ? false : hasError;
    }

    public void setBundleHasLicenseError(boolean hasError)
    {
        servletVariables.getHttpSession().setAttribute(SETUP_BUNDLE_HAS_LICENSE_ERROR, hasError);
    }

    public String getWebSudoToken()
    {
        return applicationProperties.getString(APKeys.JIRA_SETUP_WEB_SUDO_TOKEN);
    }

    public void setWebSudoToken(final String token)
    {
        applicationProperties.setString(APKeys.JIRA_SETUP_WEB_SUDO_TOKEN, token);
    }

    /**
     * Save entered by user MAC credentials, which will be used to create an admin account.
     * Should be used by Instant Setup only.
     *
     * @param email the email associated with MAC account
     * @param password the password for MAC account
     */
    public void setUserCredentials(final String email, final String password)
    {
        servletVariables.getHttpSession().setAttribute(SETUP_INSTANT_MAC_EMAIL, email);
        servletVariables.getHttpSession().setAttribute(SETUP_INSTANT_MAC_PASSWORD, password);
    }

    /**
     * Return entered by user MAC credentials. Should be used by Instant Setup only.
     *
     * @return a map containing email and password
     */
    public Map<String, String> getUserCredentials()
    {
        return ImmutableMap.of(
                "email", (String) servletVariables.getHttpSession().getAttribute(SETUP_INSTANT_MAC_EMAIL),
                "password", (String) servletVariables.getHttpSession().getAttribute(SETUP_INSTANT_MAC_PASSWORD)
        );
    }

    /**
     * Save whether current setup process run in instant mode.
     *
     * @param isInstant whether setup run in instant mode
     */
    public void setIsInstantSetup(boolean isInstant)
    {
        applicationProperties.setOption(APKeys.JIRA_SETUP_IS_INSTANT, isInstant);
        applicationProperties.setOption(APKeys.JIRA_SETUP_MODE_DECIDED, true);
    }

    /**
     * Return the information whether current setup process run in instant mode.
     *
     * @return boolean saying if in instant mode
     */
    public boolean getIsInstantSetup()
    {
        return applicationProperties.getOption(APKeys.JIRA_SETUP_IS_INSTANT);
    }

    /**
     * Return the information whether it has been decided already in which mode setup should be running.
     *
     * @return boolean if decided
     */
    public boolean isSetupModeDecided()
    {
        return applicationProperties.getOption(APKeys.JIRA_SETUP_MODE_DECIDED);
    }

    /**
     * Save the given locale in session, so that in can be utilized later by Instant Setup after initial data import.
     *
     * @param locale locale to save
     */
    public void setLocale(final String locale)
    {
        servletVariables.getHttpSession().setAttribute(SETUP_JIRA_LOCAL, locale);
    }

    /**
     * Retrieve the locale saved in session.
     *
     * @return saved previously locale
     */
    public String getLocale()
    {
        return (String) servletVariables.getHttpSession().getAttribute(SETUP_JIRA_LOCAL);
    }
}