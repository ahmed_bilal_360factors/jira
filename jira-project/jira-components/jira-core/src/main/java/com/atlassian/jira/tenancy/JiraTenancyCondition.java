package com.atlassian.jira.tenancy;

import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;


/**
 * Helper class to test if tenancy is enabled.  Can be controlled by either system property or dark feature
 *
 * @since v6.4
 */
public class JiraTenancyCondition implements TenancyCondition
{

    private final FeatureManager featureManager;
    private final JiraProperties jiraProperties;

    public JiraTenancyCondition(final FeatureManager featureManager, final JiraProperties jiraProperties)
    {
        this.featureManager = featureManager;
        this.jiraProperties = jiraProperties;
    }

    public boolean isEnabled()
    {
        return jiraProperties.getBoolean("atlassian.tenancy.enabled") || featureManager.isEnabled(CoreFeatures.TENANCY_ENABLED);
    }
}
