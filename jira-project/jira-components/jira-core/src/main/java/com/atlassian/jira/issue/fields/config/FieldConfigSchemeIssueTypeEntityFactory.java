package com.atlassian.jira.issue.fields.config;

import com.atlassian.jira.entity.AbstractEntityFactory;
import com.atlassian.jira.ofbiz.FieldMap;
import org.ofbiz.core.entity.GenericValue;

import java.util.Map;

public class FieldConfigSchemeIssueTypeEntityFactory extends AbstractEntityFactory<FieldConfigSchemeIssueType>
{
    @Override
    public String getEntityName()
    {
        return "FieldConfigSchemeIssueType";
    }

    @Override
    public Map<String, Object> fieldMapFrom(final FieldConfigSchemeIssueType value)
    {
        return new FieldMap(FieldConfigSchemeIssueType.ID, value.getId())
                .add(FieldConfigSchemeIssueType.ISSUE_TYPE_ID, value.getIssueTypeId())
                .add(FieldConfigSchemeIssueType.FIELD_CONFIG_SCHEME_ID, value.getFieldConfigSchemeId())
                .add(FieldConfigSchemeIssueType.FIELD_CONFIGURATION_ID, value.getFieldConfigurationId());
    }

    @Override
    public FieldConfigSchemeIssueType build(final GenericValue gv)
    {
        return new FieldConfigSchemeIssueType(gv.getLong(FieldConfigSchemeIssueType.ID),
                gv.getString(FieldConfigSchemeIssueType.ISSUE_TYPE_ID),
                gv.getLong(FieldConfigSchemeIssueType.FIELD_CONFIG_SCHEME_ID),
                gv.getLong(FieldConfigSchemeIssueType.FIELD_CONFIGURATION_ID)
        );
    }
}
