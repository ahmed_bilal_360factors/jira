package com.atlassian.jira.avatar;

import java.net.URI;
import java.util.Collections;

import javax.annotation.Nonnull;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.fugue.Option;
import com.atlassian.jira.avatar.Avatar.Size;
import com.atlassian.jira.cluster.ClusterSafe;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.Supplier;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.descriptors.AbstractWebFragmentModuleDescriptor;
import com.atlassian.util.concurrent.LazyReference;

import com.opensymphony.module.propertyset.PropertySet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.jira.component.ComponentAccessor.getOsgiComponentReference;

/**
 * Implementation of the AvatarService. Uses AvatarPlugin module for user Avatars.
 *
 * @since v4.3
 */
@SuppressWarnings ("UnusedParameters")
public class AvatarServiceImpl implements AvatarService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AvatarServiceImpl.class);

    private final UserManager userManager;
    private final AvatarManager avatarManager;
    private final UserPropertyManager userPropertyManager;
    private final VelocityRequestContextFactory velocityRequestContextFactory;
    private final ApplicationProperties applicationProperties;
    private final GravatarSettings gravatarSettings;
    private final Supplier<JiraAvatarSupport> jiraAvatarSupport;
    private final PluginAccessor pluginAccessor;

    @ClusterSafe
    private final LazyReference<Avatar.Size> defaultAvatarSize = new LazyReference<Avatar.Size>()
    {
        @Override
        protected Avatar.Size create() throws Exception
        {
            return Avatar.Size.defaultSize();
        }
    };

    AvatarServiceImpl(
            UserManager userManager,
            AvatarManager avatarManager,
            UserPropertyManager userPropertyManager,
            VelocityRequestContextFactory velocityRequestContextFactory,
            ApplicationProperties applicationProperties,
            GravatarSettings gravatarSettings,
            Supplier<JiraAvatarSupport> jiraAvatarSupport,
            PluginAccessor pluginAccessor)
    {
        this.userManager = userManager;
        this.avatarManager = avatarManager;
        this.userPropertyManager = userPropertyManager;
        this.velocityRequestContextFactory = velocityRequestContextFactory;
        this.applicationProperties = applicationProperties;
        this.gravatarSettings = gravatarSettings;
        this.jiraAvatarSupport = jiraAvatarSupport;
        this.pluginAccessor = pluginAccessor;
    }

    /**
     * Injectable constructor.
     */
    @SuppressWarnings ("UnusedDeclaration")
    public AvatarServiceImpl(
            UserManager userManager,
            AvatarManager avatarManager,
            UserPropertyManager userPropertyManager,
            VelocityRequestContextFactory velocityRequestContextFactory,
            ApplicationProperties applicationProperties,
            GravatarSettings gravatarSettings,
            PluginAccessor pluginAccessor)
    {
        this(userManager, avatarManager, userPropertyManager, velocityRequestContextFactory, applicationProperties,
                gravatarSettings, getOsgiComponentReference(JiraAvatarSupport.class), pluginAccessor);
    }

    protected static ApplicationUser fromStaleUser(User user)
    {
        try
        {
            return ApplicationUsers.from(user);
        }
        catch (IllegalStateException e)
        {
            return null;
        }
    }

    @Override
    public Avatar getAvatar(User remoteUser, String username) throws AvatarsDisabledException
    {
        ApplicationUser user = userManager.getUserByName(username);
        if (user == null)
        {
            Avatar anonymousAvatar = getAnonymousAvatar();
            LOGGER.debug("User with key '{}' does not exist, using anonymous avatar id {}", username, anonymousAvatar != null ? anonymousAvatar.getId() : null);
            return anonymousAvatar;
        }
        return getAvatarImpl(fromStaleUser(remoteUser), false, user, false);
    }

    private Avatar getAvatarImpl(final ApplicationUser applicationUser, final boolean skipPermissionCheck, @Nonnull final ApplicationUser user, final boolean absoluteUrl)
    {
        final JiraPluginAvatar jiraPluginAvatar = jiraAvatarSupport.get().getAvatar(user, "small");
        return new PluginAvatarAdapter(jiraPluginAvatar, Avatar.Type.USER)
        {
            // HACK for JIRA 6.4 - delete this whole override for JIRA 7.0
            // we need to know if we definitely do not have the atlassian id avatar provider plugin
            // (atlassian account plugin) it has a dark feature flag so we have to make sure we don't assume that
            // it is active just because it is present. We can't just check for evidence of a BTF deployment config
            // either because JAC, SAC etc. will have this. Also we can't touch any of the actual classes because
            // this is core and we don't depend on these classes. https://jdog.jira-dev.com/browse/JDEV-31971
            @Override
            public Long getId()
            {
                final Option<ModuleDescriptor<?>> pluginModule =
                        Option.<ModuleDescriptor<?>>option(pluginAccessor.getEnabledPluginModule("com.atlassian.id.atlassian-account-plugin:atlassianIdAvatarProvider"));
                boolean atlassianIdActive = false;
                if (pluginModule.isDefined())
                {
                    try
                    {
                        final Condition condition = ((AbstractWebFragmentModuleDescriptor) pluginModule.get()).getCondition();
                        atlassianIdActive = condition.shouldDisplay(Collections.<String, Object>emptyMap());
                    }
                    catch (RuntimeException ignore)
                    {
                        // if this happens it implies that the plugin has been changed, in this case we want to fall
                        // back to the default logic above.
                    }
                }

                if (atlassianIdActive)
                {
                    return super.getId();
                }
                else
                {
                    LOGGER.debug("getId() for user avatar should no longer be called. It'll always return null in 7.0");
                    return userPropertyManager.getPropertySet(user).getLong("user.avatar.id");
                }
            }
        };
    }

    @Override
    public Avatar getAvatar(ApplicationUser remoteUser, ApplicationUser avatarUser) throws AvatarsDisabledException
    {
        if (avatarUser == null)
        {
            return getAnonymousAvatar();
        }
        return getAvatarImpl(remoteUser, false, avatarUser, false);
    }

    @Override
    public Avatar getAvatarTagged(ApplicationUser remoteUser, ApplicationUser avatarUser)
            throws AvatarsDisabledException
    {
        if (avatarUser == null)
        {
            return getAnonymousAvatar();
        }
        return getAvatarImpl(remoteUser, false, avatarUser, true);
    }

    @Override
    public URI getAvatarURL(User remoteUser, String username) throws AvatarsDisabledException
    {
        return getAvatarURL(fromStaleUser(remoteUser), userManager.getUserByName(username));
    }

    @Override
    public URI getAvatarURL(ApplicationUser remoteUser, ApplicationUser avatarUser) throws AvatarsDisabledException
    {
        return getAvatarURLImpl(remoteUser, false, avatarUser, defaultAvatarSize.get());
    }

    private URI getAvatarURLImpl(ApplicationUser remoteUser, boolean skipPermissionCheck, ApplicationUser avatarUser, Size size)
    {
        return URI.create(jiraAvatarSupport.get().getAvatar(avatarUser, size.getParam()).getUrl());
    }

    @Override
    public URI getAvatarURL(User remoteUser, String username, Avatar.Size size) throws AvatarsDisabledException
    {
        return getAvatarURLImpl(fromStaleUser(remoteUser), false, userManager.getUserByName(username), size);
    }

    @Override
    public URI getAvatarURL(ApplicationUser remoteUser, ApplicationUser avatarUser, Avatar.Size size)
            throws AvatarsDisabledException
    {
        return getAvatarURLImpl(remoteUser, false, avatarUser, size);
    }

    @Override
    public URI getAvatarUrlNoPermCheck(String username, Avatar.Size size) throws AvatarsDisabledException
    {
        return getAvatarURLImpl(null, true, userManager.getUserByName(username), size);
    }

    @Override
    public URI getAvatarUrlNoPermCheck(ApplicationUser avatarUser, Avatar.Size size) throws AvatarsDisabledException
    {
        return getAvatarURLImpl(null, true, avatarUser, size);
    }

    /**
     * Builds a URI for a JIRA avatar with the requested size.
     *
     * @param avatarUser the ApplicationUser whose avatar we'd like to display
     * @param avatar the Avatar whose URI we want
     * @param size the size in which the avatar should be displayed
     * @return a URI that can be used to display the avatar
     */
    public URI getAvatarUrlNoPermCheck(final ApplicationUser avatarUser, final Avatar avatar, @Nonnull Size size)
    {
        final Long id = avatar.getId();
        final String sizeName = size.getParam();
        if (avatar.getAvatarType() == Avatar.Type.USER)
        {
            return URI.create(jiraAvatarSupport.get().getAvatarById(id, sizeName).getUrl());
        }
        else
        {
            return getProjectAvatarUrl(avatar, size);
        }
    }

    @Override
    public URI getAvatarAbsoluteURL(User remoteUser, String username, Avatar.Size size)
            throws AvatarsDisabledException
    {
        return getAvatarURLImpl(fromStaleUser(remoteUser), false, userManager.getUserByName(username), size);
    }

    @Override
    public URI getAvatarAbsoluteURL(ApplicationUser remoteUser, ApplicationUser avatarUser, Avatar.Size size)
            throws AvatarsDisabledException
    {
        return getAvatarURLImpl(remoteUser, false, avatarUser, size);
    }

    @Override
    public boolean hasCustomUserAvatar(User remoteUser, String username)
    {
        ApplicationUser user = userManager.getUserByName(username);
        if (user == null)
        {
            throw new IllegalArgumentException(String.format("User '%s' does not exist", username));
        }
        return hasCustomUserAvatar(fromStaleUser(remoteUser), user);
    }

    @Override
    public boolean hasCustomUserAvatar(ApplicationUser remoteUser, ApplicationUser user)
    {
        // In addition to no configured avatar, if the user's avatar is external, it is considered "custom".
        // This should be finessed into the real question which is "can I have the edit link for this?" which should
        // be encapsulated by the avatar support plugin moduletype. Bonus is that question would work for admin use case
        // As of end of 2014 this belongs to the Identity team's backlog and the Atlassian ID project's ongoing work on
        // cross-product avatar APIs. Delete this comment by the end of 2016 if everything has changed again - christo
        return remoteUser != null && (configuredAvatarIdFor(user) != null || isUsingExternalAvatar(remoteUser, user));
    }

    @Override
    public void setCustomUserAvatar(User remoteUser, String username, Long avatarId)
            throws AvatarsDisabledException, NoPermissionException
    {
        ApplicationUser user = userManager.getUserByName(username);
        if (user == null)
        {
            throw new IllegalArgumentException(String.format("User '%s' does not exist", username));
        }
        setCustomUserAvatar(fromStaleUser(remoteUser), user, avatarId);
    }

    @Override
    public void setCustomUserAvatar(ApplicationUser remoteUser, ApplicationUser user, Long avatarId)
            throws AvatarsDisabledException, NoPermissionException
    {
        if (!canSetCustomUserAvatar(remoteUser, user))
        {
            throw new NoPermissionException();
        }

        setConfiguredAvatarIdFor(user, avatarId);
    }

    @Override
    public boolean canSetCustomUserAvatar(User remoteUser, String username)
    {
        ApplicationUser user = userManager.getUserByName(username);
        if (user == null)
        {
            throw new IllegalArgumentException(String.format("User '%s' does not exist", username));
        }
        return canSetCustomUserAvatar(fromStaleUser(remoteUser), user);
    }

    @Override
    public boolean canSetCustomUserAvatar(ApplicationUser remoteUser, ApplicationUser user)
    {
        return avatarManager.hasPermissionToEdit(remoteUser, user);
    }

    @Override
    public URI getProjectAvatarURL(final Project project, final Avatar.Size size)
    {
        final String baseUrl = velocityRequestContextFactory.getJiraVelocityRequestContext().getBaseUrl();
        return getProjectAvatarURLImpl(project, size, baseUrl);
    }

    @Override
    public URI getProjectAvatarAbsoluteURL(final Project project, final Avatar.Size size)
    {
        final String baseUrl = velocityRequestContextFactory.getJiraVelocityRequestContext().getCanonicalBaseUrl();
        return getProjectAvatarURLImpl(project, size, baseUrl);
    }

    private URI getProjectAvatarUrl(Avatar avatar, Avatar.Size size)
    {
        final String baseUrl = velocityRequestContextFactory.getJiraVelocityRequestContext().getCanonicalBaseUrl();
        UrlBuilder urlBuilder = new UrlBuilder(baseUrl + "/secure/projectavatar", applicationProperties.getEncoding(), false);

        if (size != null && !size.isDefault)
        {
            urlBuilder.addParameter("size", size.param);
        }

        // optional avatarId
        Long avatarId = avatar != null ? avatar.getId() : null;
        if (avatarId != null)
        {
            urlBuilder.addParameter("avatarId", avatarId.toString());
        }

        return urlBuilder.asURI();
    }

    private URI getProjectAvatarURLImpl(final Project project, final Avatar.Size size, final String baseUrl)
    {
        final Avatar avatar = project.getAvatar();
        final Long avatarId = avatar == null ? null : avatar.getId();

        if (avatarManager.getDefaultAvatarId(Avatar.Type.PROJECT).equals(avatarId))
        {
            return getProjectDefaultAvatarURLImpl(size, baseUrl);
        }

        UrlBuilder urlBuilder = new UrlBuilder(baseUrl + "/secure/projectavatar", applicationProperties.getEncoding(), false);

        if (size != null && !size.isDefault)
        {
            urlBuilder.addParameter("size", size.param);
        }

        urlBuilder.addParameter("pid", project.getId());

        if (avatarId != null)
        {
            urlBuilder.addParameter("avatarId", avatarId.toString());
        }

        return urlBuilder.asURI();
    }

    @Override
    public URI getProjectDefaultAvatarURL(final Avatar.Size size)
    {
        final String baseUrl = velocityRequestContextFactory.getJiraVelocityRequestContext().getBaseUrl();
        return getProjectDefaultAvatarURLImpl(size, baseUrl);
    }

    @Override
    public URI getProjectDefaultAvatarAbsoluteURL(final Avatar.Size size)
    {
        final String baseUrl = velocityRequestContextFactory.getJiraVelocityRequestContext().getCanonicalBaseUrl();
        return getProjectDefaultAvatarURLImpl(size, baseUrl);
    }

    private URI getProjectDefaultAvatarURLImpl(final Avatar.Size size, final String baseUrl)
    {
        UrlBuilder urlBuilder = new UrlBuilder(baseUrl + "/secure/projectavatar", applicationProperties.getEncoding(), false);

        if (size != null && !size.isDefault)
        {
            urlBuilder.addParameter("size", size.param);
        }

        urlBuilder.addParameter("avatarId", avatarManager.getDefaultAvatarId(Avatar.Type.PROJECT).toString());

        return urlBuilder.asURI();
    }

    /**
     * Returns the avatar id that is configured for the given User. If the user has not configured an avatar, this
     * method returns null.
     *
     * @param user the user whose avatar we want
     * @return an avatar id, or null
     */
    protected Long configuredAvatarIdFor(ApplicationUser user)
    {
        PropertySet userProperties = userPropertyManager.getPropertySet(user);
        if (userProperties.exists(AvatarManager.USER_AVATAR_ID_KEY))
        {
            long avatarId = userProperties.getLong(AvatarManager.USER_AVATAR_ID_KEY);
            LOGGER.debug("Avatar configured for user '{}' is {}", user.getUsername(), avatarId);

            return avatarId;
        }

        return null;
    }

    /**
     * Returns true if Gravatar support is enabled.
     *
     * @return a boolean indicating whether Gravatar support is on
     */
    @Override
    public boolean isGravatarEnabled()
    {
        return gravatarSettings.isAllowGravatars();
    }

    @Override
    public boolean isUsingExternalAvatar(final ApplicationUser remoteUser, final ApplicationUser avatarUser)
    {
        return jiraAvatarSupport.get().getAvatar(avatarUser, "small").isExternal();
    }

    /**
     * Sets the given avatar id as the configured avatar id for a user.
     *
     * @param user the User whose avatar is being configured
     * @param avatarId the avatar id to configure
     */
    protected void setConfiguredAvatarIdFor(ApplicationUser user, Long avatarId)
    {
        PropertySet userProperties = userPropertyManager.getPropertySet(user);
        userProperties.setLong(AvatarManager.USER_AVATAR_ID_KEY, avatarId);
        LOGGER.debug("Set configured avatar id for user '{}' to {}", user.getUsername(), avatarId);
    }

    /**
     * Returns true if the passed in user has permission to view the passed in avatar. By definition, any user can view
     * the system avatars (e.g. avatars with no owner).
     *
     * @param user a User
     * @param avatar an Avatar
     * @return a boolean indicating whether the passed in user has permission to view the passed in avatar
     */
    protected boolean canViewAvatar(ApplicationUser user, Avatar avatar)
    {
        boolean hasPermission = avatarManager.hasPermissionToView(user != null ? user.getDirectoryUser() : null, avatar.getAvatarType(), avatar.getOwner());
        if (!hasPermission)
        {
            LOGGER.debug("User '{}' is not allowed to view avatar {}", user, avatar.getId());
        }

        return hasPermission;
    }

    /**
     * Returns the default avatar, if configured. Otherwise returns null.
     *
     * @return the default Avatar, or null
     */
    protected Avatar getDefaultAvatar()
    {
        Long defaultAvatarId = avatarManager.getDefaultAvatarId(Avatar.Type.USER);

        return defaultAvatarId != null ? avatarManager.getById(defaultAvatarId) : null;
    }

    /**
     * Returns the anonymous avatar, if configured. Otherwise returns null.
     *
     * @return the anonymous avatar, or null
     */
    protected Avatar getAnonymousAvatar()
    {
        Long anonAvatarId = avatarManager.getAnonymousAvatarId();

        return anonAvatarId != null ? avatarManager.getById(anonAvatarId) : null;
    }

}
