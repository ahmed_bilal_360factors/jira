package com.atlassian.jira.service.services.analytics.stop;

import com.atlassian.analytics.api.annotations.EventName;

import java.util.Map;

/**
 * Event that will inform analytics system
 * about jira shutdown
 */
@EventName("jira.stop")
public class JiraStopAnalyticEvent
{
    /**
     * We are going to use a dynamic properties
     * so we can extend it later.
     */
    private final Map<String,Object> properties;

    public JiraStopAnalyticEvent(final Map<String, Object> properties)
    {
        this.properties = properties;
    }

    public Map<String, Object> getProperties()
    {
        return properties;
    }
}
