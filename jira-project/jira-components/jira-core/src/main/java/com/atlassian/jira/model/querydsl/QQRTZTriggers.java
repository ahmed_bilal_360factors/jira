package com.atlassian.jira.model.querydsl;

import com.mysema.query.sql.ColumnMetadata;
import com.mysema.query.types.path.*;

import java.sql.Types;
import javax.annotation.Generated;

/**
 * QQRTZTriggers is a Querydsl query object.
 *
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QQRTZTriggers extends JiraRelationalPathBase<QRTZTriggersDTO>
{
    public static final QQRTZTriggers Q_R_T_Z_TRIGGERS = new QQRTZTriggers("Q_R_T_Z_TRIGGERS");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final StringPath triggerName = createString("triggerName");
    public final StringPath triggerGroup = createString("triggerGroup");
    public final NumberPath<Long> job = createNumber("job", Long.class);
    public final DateTimePath<java.sql.Timestamp> nextFire = createDateTime("nextFire", java.sql.Timestamp.class);
    public final StringPath triggerState = createString("triggerState");
    public final StringPath triggerType = createString("triggerType");
    public final DateTimePath<java.sql.Timestamp> startTime = createDateTime("startTime", java.sql.Timestamp.class);
    public final DateTimePath<java.sql.Timestamp> endTime = createDateTime("endTime", java.sql.Timestamp.class);
    public final StringPath calendarName = createString("calendarName");
    public final NumberPath<Integer> misfireInstr = createNumber("misfireInstr", Integer.class);

    public QQRTZTriggers(String alias)
    {
        super(QRTZTriggersDTO.class, alias, "qrtz_triggers");
        addMetadata();
    }

    private void addMetadata()
    {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(triggerName, ColumnMetadata.named("trigger_name").withIndex(2).ofType(Types.VARCHAR).withSize(255));
        addMetadata(triggerGroup, ColumnMetadata.named("trigger_group").withIndex(3).ofType(Types.VARCHAR).withSize(255));
        addMetadata(job, ColumnMetadata.named("job").withIndex(4).ofType(Types.NUMERIC).withSize(18));
        addMetadata(nextFire, ColumnMetadata.named("next_fire").withIndex(5).ofType(Types.TIMESTAMP).withSize(35));
        addMetadata(triggerState, ColumnMetadata.named("trigger_state").withIndex(6).ofType(Types.VARCHAR).withSize(255));
        addMetadata(triggerType, ColumnMetadata.named("trigger_type").withIndex(7).ofType(Types.VARCHAR).withSize(60));
        addMetadata(startTime, ColumnMetadata.named("start_time").withIndex(8).ofType(Types.TIMESTAMP).withSize(35));
        addMetadata(endTime, ColumnMetadata.named("end_time").withIndex(9).ofType(Types.TIMESTAMP).withSize(35));
        addMetadata(calendarName, ColumnMetadata.named("calendar_name").withIndex(10).ofType(Types.VARCHAR).withSize(255));
        addMetadata(misfireInstr, ColumnMetadata.named("misfire_instr").withIndex(11).ofType(Types.NUMERIC).withSize(9));
    }
}

