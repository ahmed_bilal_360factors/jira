package com.atlassian.jira.issue.attachment.store;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.issue.attachment.AttachmentDirectoryAccessor;
import com.atlassian.jira.util.BoundedExecutorServiceWrapper;

/**
 * Factory responsible for creating instances of {@link com.atlassian.jira.issue.attachment.store.DefaultFileSystemAttachmentStore}
 * with provided {@link com.atlassian.jira.issue.attachment.AttachmentDirectoryAccessor}.
 *
 * @since v6.4
 */
public class DefaultFileSystemAttachmentStoreFactory
{
    private final LocalTemporaryFileStore localTemporaryFileStore;
    private final BoundedExecutorServiceWrapper managedExecutor;
    private final EventPublisher eventPublisher;

    public DefaultFileSystemAttachmentStoreFactory(
            final LocalTemporaryFileStore localTemporaryFileStore,
            final BoundedExecutorServiceWrapper managedExecutor, final EventPublisher eventPublisher)
    {
        this.localTemporaryFileStore = localTemporaryFileStore;
        this.managedExecutor = managedExecutor;
        this.eventPublisher = eventPublisher;
    }

    public DefaultFileSystemAttachmentStore createFileSystemStore(final AttachmentDirectoryAccessor newDirectoryAccessor)
    {
        return new DefaultFileSystemAttachmentStore(newDirectoryAccessor, localTemporaryFileStore, managedExecutor, eventPublisher);
    }
}
