package com.atlassian.jira.ajsmeta;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.opensymphony.util.TextUtils;

import java.io.IOException;
import java.io.Writer;

import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Utility class for including the Google site verification key in the JIRA header if it's set.
 *
 * @since v6.0
 */
public class GoogleSiteVerification
{
    public static final String GOOGLE_SITE_VERIFICATION_KEY = "google.site.verification.key";

    private final ApplicationProperties applicationProperties;

    public GoogleSiteVerification(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = notNull("applicationProperties", applicationProperties);
    }

    /**
     * Returns the Google site verification key or an empty string if it is not defined.
     *
     * @return a String containing the Google site verification key (may be empty)
     */
    private String getKey()
    {
        String propertyValue = applicationProperties.getString(GOOGLE_SITE_VERIFICATION_KEY);
        return isBlank(propertyValue) ? "" : propertyValue;
    }

    public void writeHtml(final Writer writer) throws IOException
    {
        writer.write("<meta name=\"google-site-verification\"");
        writer.write(" content=\"");
        writer.write(TextUtils.htmlEncode(getKey()));
        writer.write("\">\n");
    }

    /**
     * Returns whether Google site verification is available, i.e. has the verification key been defined.
     * @return true if Google site verification is available.
     */
    public boolean isAvailable()
    {
        return !isBlank(getKey());
    }
}
