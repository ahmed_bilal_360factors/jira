package com.atlassian.jira.web.action.issue;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import com.atlassian.core.util.FileSize;
import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.bc.issue.attachment.AttachmentService;
import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.event.issue.IssueEventSource;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.exception.IssueNotFoundException;
import com.atlassian.jira.exception.IssuePermissionException;
import com.atlassian.jira.issue.AttachmentError;
import com.atlassian.jira.issue.AttachmentsBulkOperationResult;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.util.IssueUpdateBean;
import com.atlassian.jira.issue.util.IssueUpdater;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.action.issue.util.BackwardCompatibleTemporaryAttachmentUtil;
import com.atlassian.jira.web.action.message.ClosingPolicy;
import com.atlassian.jira.web.action.message.MessageResponder;
import com.atlassian.jira.web.action.message.PopUpMessage;
import com.atlassian.jira.web.action.message.PopUpMessageFactory;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;

import org.apache.commons.lang.math.NumberUtils;
import org.ofbiz.core.entity.GenericValue;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;


/**
 * Responsible for rendering the attach file form and converting temporary attachments to real attachments linked to an
 * issue.  This action does not actually handle uploading the attachments themselves.  See {@link
 * com.atlassian.jira.web.action.issue.AttachTemporaryFile} for more details
 *
 * @since v4.2
 */
public class AttachFile extends AbstractCommentableIssue implements OperationContext
{
    private static final String ATTACHMENTS_VIEW = "ManageAttachments.jspa";

    private final Function<AttachmentError, PopUpMessage> ERROR_TO_MESSAGE = new Function<AttachmentError, PopUpMessage>()
    {
        @Override
        public PopUpMessage apply(@Nullable final AttachmentError error)
        {
            Preconditions.checkNotNull(error, "error");
            final String fileName = error.getFilename();
            final String plainMessage = error.getLocalizedMessage();
            return messageFactory.createPlainMessage(MessageType.ERROR, fileName, plainMessage, ClosingPolicy.MANUAL);
        }
    };
    private final AttachmentService attachmentService;
    private final IssueUpdater issueUpdater;
    private final BackwardCompatibleTemporaryAttachmentUtil temporaryAttachmentUtil;
    private final MessageResponder responder;
    private final PopUpMessageFactory messageFactory;
    private long maxSize = Long.MIN_VALUE;
    private String[] filetoconvert;

    public AttachFile(
            final SubTaskManager subTaskManager,
            final FieldScreenRendererFactory fieldScreenRendererFactory,
            final FieldManager fieldManager,
            final ProjectRoleManager projectRoleManager,
            final CommentService commentService,
            final AttachmentService attachmentService,
            final IssueUpdater issueUpdater,
            final UserUtil userUtil,
            final BackwardCompatibleTemporaryAttachmentUtil temporaryAttachmentUtil,
            final MessageResponder responder,
            final PopUpMessageFactory messageFactory)
    {
        super(subTaskManager, fieldScreenRendererFactory, fieldManager, projectRoleManager, commentService, userUtil);
        this.attachmentService = attachmentService;
        this.issueUpdater = issueUpdater;
        this.temporaryAttachmentUtil = temporaryAttachmentUtil;
        this.responder = responder;
        this.messageFactory = messageFactory;
    }

    @Override
    public String doDefault() throws Exception
    {
        try
        {
            return attachmentService.canCreateAttachments(getJiraServiceContext(), getIssueObject()) ? INPUT : ERROR;
        }
        catch (final IssueNotFoundException e)
        {
            // Error is added above
            return ERROR;
        }
        catch (final IssuePermissionException e)
        {
            // Error is added above
            return ERROR;
        }
    }

    @Override
    protected void doValidation()
    {
        try
        {
            attachmentService.canCreateAttachments(getJiraServiceContext(), getIssueObject());
            super.doValidation(); // validate comment
        }
        catch (final IssueNotFoundException ex)
        {
            // Do nothing as error is added above
            return;
        }
        catch (final IssuePermissionException ex)
        {
            // Do nothing as error is added above
            return;
        }

        final List<String> temporaryAttachmentsIds = getTemporaryAttachmentsIds();
        if (temporaryAttachmentsIds.isEmpty())
        {
            addError(AttachTemporaryFile.TEMP_FILENAME, getText("attachfile.error.filerequired"));
        }
        else
        {
            if (!temporaryAttachmentUtil.allAttachmentsExists(temporaryAttachmentsIds))
            {
                // Display these errors under the upload box as the checkbox will have gone away to were bits go
                // when they die.
                addError(AttachTemporaryFile.TEMP_FILENAME, getText("attachment.temporary.id.session.time.out"));
            }
        }
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception
    {
        final List<String> temporaryAttachmentsIds = getTemporaryAttachmentsIds();
        final AttachmentsBulkOperationResult<ChangeItemBean> convertResult =
                temporaryAttachmentUtil.convertTemporaryAttachments(
                        getLoggedInApplicationUser(),
                        getIssueObject(),
                        temporaryAttachmentsIds
                );
        final List<AttachmentError> errors = convertResult.getErrors();
        final List<ChangeItemBean> results = convertResult.getResults();

        final IssueUpdateBean issueUpdateBean = createIssueUpdateBeanWithComment(results, createComment());
        issueUpdater.doUpdate(issueUpdateBean, true);
        temporaryAttachmentUtil.clearTemporaryAttachmentsByFormToken(getFormToken());

        final List<PopUpMessage> messages = copyOf(transform(errors, ERROR_TO_MESSAGE));
        final URI redirect = getRedirect();
        final String title = getI18nHelper().getText("attachfile.title");
        return responder.respond(messages, redirect, this, title);
    }

    private URI getRedirect()
    {
        final Map<String, String> redirectParameters =
                MapBuilder.<String, String>newBuilder()
                .add("id", getIssue().getString("id"))
                .add("atl_token", getXsrfToken())
                .toMap();
        final UrlBuilder urlBuilder = new UrlBuilder(ATTACHMENTS_VIEW);
        urlBuilder.addParametersFromMap(redirectParameters);
        return urlBuilder.asURI();
    }

    private IssueUpdateBean createIssueUpdateBeanWithComment(final List<ChangeItemBean> changeItems, @Nullable final Comment comment)
    {
        final GenericValue issueGV = getIssue();
        final IssueUpdateBean issueUpdateBean = new IssueUpdateBean(
                issueGV, issueGV, EventType.ISSUE_UPDATED_ID, getLoggedInApplicationUser());

        issueUpdateBean.setComment(comment);
        issueUpdateBean.setChangeItems(changeItems);
        issueUpdateBean.setDispatchEvent(true);
        issueUpdateBean.setParams(EasyMap.build("eventsource", IssueEventSource.ACTION));
        return issueUpdateBean;
    }

    public String getTargetUrl()
    {
        return isInlineDialogMode() ? redirectToIssue() : redirectToAttachments();
    }

    private String redirectToAttachments()
    {
        return ATTACHMENTS_VIEW + "?id=" + getIssue().getLong("id");
    }

    private String redirectToIssue()
    {
        return getViewUrl();
    }


    public String[] getFiletoconvert()
    {
        return filetoconvert;
    }

    public void setFiletoconvert(final String[] filetoconvert)
    {
        this.filetoconvert = filetoconvert;
    }

    public long getMaxSize()
    {
        if (maxSize != Long.MIN_VALUE)
        {
            return maxSize;
        }

        final String maxSizeProperty = getApplicationProperties().getDefaultBackedString(APKeys.JIRA_ATTACHMENT_SIZE);
        maxSize = NumberUtils.toLong(maxSizeProperty, -1);
        return maxSize;
    }

    // Used in JSP
    @SuppressWarnings ("UnusedDeclaration")
    public String getMaxSizePretty()
    {
        final long maxSize = getMaxSize();
        if (maxSize > 0)
        {
            return FileSize.format(maxSize);
        }
        else
        {
            return "Unknown?";
        }
    }

    private List<String> getTemporaryAttachmentsIds()
    {
        return asList(getFiletoconvert());
    }

    private <T> List<T> asList(@Nullable final T[] strings)
    {
        if (strings == null)
        {
            return Collections.emptyList();
        }
        else
        {
            return Arrays.asList(strings);
        }
    }

    @Override
    public Map<String, Object> getDisplayParams()
    {
        final Map<String, Object> displayParams = new HashMap<String, Object>(super.getDisplayParams());
        displayParams.put("theme", "aui");
        return displayParams;
    }
}
