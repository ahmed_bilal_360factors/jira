package com.atlassian.jira.web.filters;

import com.atlassian.core.filters.AbstractHttpFilter;
import com.atlassian.jira.setup.AsynchronousJiraSetup;
import com.atlassian.jira.setup.AsynchronousJiraSetupFactory;
import com.atlassian.jira.setup.InstantSetupStrategy;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This filter is used to report on progress of a last step of instant setup.  It has to live in a filter first in the
 * filterchain since otherwhise we run the risk of having another filter call through to the ComponentManager.  This
 * could cause deadlocks in pico while the ComponentManager is being restarted during the data import!
 *
 * @since v6.4
 */
public class SetupProgressFilter extends AbstractHttpFilter
{
    @Override
    protected void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException
    {
        try
        {
            final JSONObject json = new JSONObject();
            response.setContentType("application/json");

            final String sessionId = (String) request.getSession().getAttribute("setup-session-id");
            final InstantSetupStrategy.Step step = InstantSetupStrategy.Step.valueOf(request.getParameter("askingAboutStep").toUpperCase());

            populateJsonWithSetupStatus(json, AsynchronousJiraSetupFactory.getInstance().getStatusOnceStepIsDone(sessionId, step));
            final String result = json.toString();
            response.getWriter().write(result);
            response.getWriter().flush();
        }
        catch (final JSONException e)
        {
            throw new ServletException(e);
        }
    }

    private void populateJsonWithSetupStatus(final JSONObject json, final AsynchronousJiraSetup.SetupStatus<InstantSetupStrategy.Step> setupStatus)
            throws JSONException
    {
        final JSONObject stepsJson = new JSONObject();
        json.put("steps", stepsJson);
        for (final InstantSetupStrategy.Step step : setupStatus.getSteps().keySet())
        {
            stepsJson.put(step.name().toLowerCase(), setupStatus.getSteps().get(step).name().toLowerCase());
        }

        final String errorMessage = setupStatus.getError();
        if (errorMessage != null)
        {
            json.put("errorMessage", errorMessage);
        }
    }

}
