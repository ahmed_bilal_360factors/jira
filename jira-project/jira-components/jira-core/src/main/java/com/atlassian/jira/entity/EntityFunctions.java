package com.atlassian.jira.entity;

import com.google.common.base.Function;

/**
 * Functions that act on JIRA data objects (entities).
 *
 * @since v6.4
 */
public class EntityFunctions
{
    public static final Function<WithId, Long> TO_ID = new Function<WithId, Long>()
    {
        @Override
        public Long apply(final WithId input)
        {
            return input == null ? null : input.getId();
        }
    };
}
