package com.atlassian.jira.web.filters.pagebuilder;

import com.atlassian.jira.web.pagebuilder.DecoratorListener;
import com.opensymphony.module.sitemesh.filter.Buffer;
import com.opensymphony.module.sitemesh.filter.HttpContentType;
import com.opensymphony.module.sitemesh.parser.HTMLPageParser;
import com.opensymphony.module.sitemesh.scalability.NoopScalabilitySupport;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Response wrapper that is aware of decoration. If decorating is turned on, the response is buffered so that it can
 * be parsed later.
 * @since 6.1
 */
class PageBuilderResponseWrapper extends HttpServletResponseWrapper implements DecoratorListener
{
    private Buffer buffer;
    private boolean writerHasBeenExposed;
    private boolean outputStreamHasBeenExposed;
    private boolean decoratorHasBeenSet;

    PageBuilderResponseWrapper(HttpServletResponse response)
    {
        super(response);
    }

    @Override
    public void onDecoratorSet()
    {
        if (writerHasBeenExposed)
        {
            throw new IllegalStateException("Cannot set decorator after the response writer has been accessed.");
        }
        decoratorHasBeenSet = true;
    }

    @Override
    public PrintWriter getWriter() throws IOException
    {
        writerHasBeenExposed = true;
        if (outputStreamHasBeenExposed)
        {
            throw new IllegalStateException("getWriter() called after getOutputStream()");
        }

        String contentType = getContentType();
        // If a content type hasn't been set, we have no confidence that the response can be parsed in order to decorate
        // it, so don't bother buffering. Instead just proxy onto to the response object directly.
        if (decoratorHasBeenSet && contentType != null)
        {
            if (buffer == null)
            {
                HttpContentType httpContentType = new HttpContentType(contentType);
                buffer = new Buffer(new HTMLPageParser(), httpContentType.getEncoding(), new NoopScalabilitySupport());
            }
            return buffer.getWriter();
        }
        else
        {
            return getResponse().getWriter();
        }
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException
    {
        outputStreamHasBeenExposed = true;
        if (writerHasBeenExposed)
        {
            throw new IllegalStateException("getOutputStream() called after getWriter()");
        }
        if (decoratorHasBeenSet)
        {
            throw new IllegalStateException("getOutputStream() does not support decorators");
        }
        return super.getOutputStream();
    }

    Buffer getBuffer()
    {
        return buffer;
    }

    boolean isBuffering()
    {
        return null != buffer;
    }
}
