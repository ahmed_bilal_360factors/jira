package com.atlassian.jira.jql.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;

/**
 * A component that can add project role and group permission checks to lucene queries.
 *
 * @since v6.4
 */
public class QueryProjectRoleAndGroupPermissionsDecorator
{
    private static final Logger log = LoggerFactory.getLogger(CommentClauseQueryFactory.class);
    public static final BooleanQuery matchNoDocsQuery = new BooleanQuery();

    private final PermissionManager permissionManager;
    private final ProjectRoleManager projectRoleManager;

    public QueryProjectRoleAndGroupPermissionsDecorator(final PermissionManager permissionManager, final ProjectRoleManager projectRoleManager)
    {
        this.permissionManager = permissionManager;
        this.projectRoleManager = projectRoleManager;
    }

    /**
     * Append the permission query to the passed query.
     *
     * @param queryCreationContext the context of query creation
     * @param groupLevelField the name of the lucene document field which represents the group level visibility
     * @param roleLevelField the name of the lucene document field which represents the role level visibility
     * @return the lucene query combined from passed query and permission query
     */
    public Query appendPermissionFilterQuery(final Query query, final QueryCreationContext queryCreationContext, final String groupLevelField, final String roleLevelField)
    {
        final Query permissionFilterQuery = createPermissionQuery(queryCreationContext, groupLevelField, roleLevelField);
        if (permissionFilterQuery != matchNoDocsQuery)
        {
            BooleanQuery queryWithFilter = new BooleanQuery();
            queryWithFilter.add(query, BooleanClause.Occur.MUST);
            queryWithFilter.add(permissionFilterQuery, BooleanClause.Occur.MUST);
            return queryWithFilter;
        }
        else
        {
            return new BooleanQuery();
        }
    }

    /**
     * Takes a worklog index query and adds some necessary permission checks to it.
     *
     * @param query worklog query
     * @param queryCreationContext query context
     * @return a new query with permission checks added
     */
    public Query decorateWorklogQueryWithPermissionChecks(final Query query, final QueryCreationContext queryCreationContext)
    {
        return appendPermissionFilterQuery(query, queryCreationContext, DocumentConstants.WORKLOG_LEVEL, DocumentConstants.WORKLOG_LEVEL_ROLE);
    }

    /**
     * Creates the lucene query that will restrict the results to only elements that are visible to the user
     * based on the query creation context and elements' group and role visibility levels.
     *
     * @param queryCreationContext the context of query creation
     * @param groupLevelField the name of the lucene document field which represents the group level visibility
     * @param roleLevelField the name of the lucene document field which represents the role level visibility
     * @return the query with all conditions to filter restricted items. Return empty BooleanQuery if user has no rights to see any project. Return MatchAllDocsQuery query if security is overriden
     */
    public Query createPermissionQuery(final QueryCreationContext queryCreationContext, final String groupLevelField, final String roleLevelField)
    {
        if (queryCreationContext.isSecurityOverriden())
        {
            return new MatchAllDocsQuery();
        }
        final List<Long> projectIds = getVisibleProjectIds(queryCreationContext.getApplicationUser());

        // if we cannot see any projects, then we might as well quit now
        if (projectIds.isEmpty())
        {
            return matchNoDocsQuery;
        }

        final BooleanQuery levelQuery = createLevelRestrictionQuery(projectIds, queryCreationContext.getApplicationUser(), groupLevelField, roleLevelField);
        final BooleanQuery projectVisibilityQuery = createProjectVisibilityQuery(projectIds);

        final BooleanQuery indexQuery = new BooleanQuery();
        indexQuery.add(levelQuery, BooleanClause.Occur.MUST);
        indexQuery.add(projectVisibilityQuery, BooleanClause.Occur.MUST);
        return indexQuery;
    }

    TermQuery getTermQuery(final String documentConstant, final String value)
    {
        return new TermQuery(new Term(documentConstant, value));
    }

    BooleanQuery createProjectVisibilityQuery(final List<Long> projectIds)
    {
        final BooleanQuery visibilityQuery = new BooleanQuery();
        final String projectIndexField = SystemSearchConstants.forProject().getIndexField();
        for (final Long projectId : projectIds)
        {
            visibilityQuery.add(getTermQuery(projectIndexField, projectId.toString()), BooleanClause.Occur.SHOULD);
        }
        return visibilityQuery;
    }

    /**
     * Creates a lucene query that will restrict the results to only the fields that are visible to the user based on
     * fields' group and role levels for the passed projects.
     *
     * @param projectIds the ids of the projects that we are interested in.
     * @param searcher user running this search
     * @param groupLevelField the name of the lucene document field which represents the group level visibility
     * @param roleLevelField the name of the lucene document field which represents the role level visibility
     * @return lucene query, never null
     */
    BooleanQuery createLevelRestrictionQuery(final List<Long> projectIds, final ApplicationUser searcher, final String groupLevelField, final String roleLevelField)
    {
        final BooleanQuery levelQuery = new BooleanQuery();

        // always add the no group groupLevelField and no project role groupLevelField
        final Query noGroupOrLevelConstraints = createNoGroupOrProjectRoleLevelQuery(groupLevelField, roleLevelField);
        levelQuery.add(noGroupOrLevelConstraints, BooleanClause.Occur.SHOULD);

        // Only create groupLevelField restriction queries if the user is not null (i.e. not Anonymous/not-logged-in user)
        if (searcher != null)
        {
            // add group groupLevelField restriction
            final Set<String> groups = getGroups(searcher);
            if (!groups.isEmpty())
            {
                levelQuery.add(createGroupLevelQuery(groups, groupLevelField), BooleanClause.Occur.SHOULD);
            }

            // add project role groupLevelField restriction
            final ProjectRoleManager.ProjectIdToProjectRoleIdsMap projectIdToProjectRolesMap = projectRoleManager.createProjectIdToProjectRolesMap(
                    searcher, projectIds);
            if (!projectIdToProjectRolesMap.isEmpty())
            {
                final Query query = createProjectRoleLevelQuery(projectIdToProjectRolesMap, roleLevelField);
                levelQuery.add(query, BooleanClause.Occur.SHOULD);
            }
        }
        return levelQuery;
    }

    Set<String> getGroups(final ApplicationUser searcher)
    {
        UserUtil userUtil = ComponentAccessor.getComponent(UserUtil.class);
        return userUtil.getGroupNamesForUser(searcher.getName());
    }

    /**
     * Creates new query with the restriction of field group groupLevelField set to -1 (no group groupLevelField) AND field project role
     * groupLevelField set to -1 (no project role groupLevelField).
     *
     * @param groupLevelField the name of the lucene document field which represents the group level visibility
     * @param roleLevelField the name of the lucene document field which represents the role level visibility
     * @return query
     */
    Query createNoGroupOrProjectRoleLevelQuery(final String groupLevelField, final String roleLevelField)
    {
        final BooleanQuery query = new BooleanQuery();
        query.add(getTermQuery(groupLevelField, "-1"), BooleanClause.Occur.MUST);
        query.add(getTermQuery(roleLevelField, "-1"), BooleanClause.Occur.MUST);
        return query;
    }

    /**
     * Creates a project role level based restriction query.
     * <p/>
     * NOTE: This method should not be called with null or empty map (i.e if the user is not a member of any roles). If
     * the user is not a member of any roles, the right thing to do is not to call this method.
     *
     * @param projectIdToProjectRolesMap Map[Long,Collection[ProjectRole]]
     * @param roleLevelField the name of the lucene document field which represents the role level visibility
     * @return query
     */
    Query createProjectRoleLevelQuery(final ProjectRoleManager.ProjectIdToProjectRoleIdsMap projectIdToProjectRolesMap, final String roleLevelField)
    {
        final BooleanQuery query = new BooleanQuery();
        if ((projectIdToProjectRolesMap == null) || projectIdToProjectRolesMap.isEmpty())
        {
            log.debug("Groups must be specified!");
            return query;
        }

        for (final ProjectRoleManager.ProjectIdToProjectRoleIdsMap.Entry entry : projectIdToProjectRolesMap)
        {
            final Long projectId = entry.getProjectId();
            final Collection<Long> projectRoles = entry.getProjectRoleIds();
            for (final Long projectRoleId : projectRoles)
            {
                query.add(createFieldInProjectAndUserInRoleQuery(projectId, projectRoleId, roleLevelField), BooleanClause.Occur.SHOULD);
            }
        }
        return query;
    }

    /**
     * Creates a new query that sets the project id must be equal to the given value and the project role visibility of
     * the field must be equal to the given value.
     *
     * @param projectId project ID
     * @param projectRoleId project role ID
     * @param roleLevelField the name of the lucene document field which represents the role level visibility
     * @return new query
     */
    Query createFieldInProjectAndUserInRoleQuery(final Long projectId, final Long projectRoleId, final String roleLevelField)
    {

        final BooleanQuery query = new BooleanQuery();
        if (projectId == null)
        {
            log.debug("projectId must be specified!");
            return query;
        }
        if (projectRoleId == null)
        {
            log.debug("projectRoleId must be specified!");
            return query;
        }

        query.add(getTermQuery(SystemSearchConstants.forProject().getIndexField(), projectId.toString()), BooleanClause.Occur.MUST);
        query.add(getTermQuery(roleLevelField, projectRoleId.toString()), BooleanClause.Occur.MUST);
        return query;
    }

    Query createGroupLevelQuery(final Set<String> groups, final String groupLevelField)
    {
        final BooleanQuery query = new BooleanQuery();
        if ((groups == null) || groups.isEmpty())
        {
            log.debug("Groups must be specified!");
            return query;
        }

        for (final String group : groups)
        {
            query.add(getTermQuery(groupLevelField, group), BooleanClause.Occur.SHOULD);
        }
        return query;
    }


    List<Long> getVisibleProjectIds(final ApplicationUser searcher)
    {
        final Collection<Project> projects = permissionManager.getProjects(ProjectPermissions.BROWSE_PROJECTS, searcher);
        final List<Long> projectIds = new ArrayList<Long>();
        for (final Project project : projects)
        {
            if (project != null)
            {
                projectIds.add(project.getId());
            }
        }
        return projectIds;
    }

}
