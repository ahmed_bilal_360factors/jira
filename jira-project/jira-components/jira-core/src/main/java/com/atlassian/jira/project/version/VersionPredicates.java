package com.atlassian.jira.project.version;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;

public class VersionPredicates
{
    public static Predicate<Version> isArchived()
    {
        return new Predicate<Version>()
        {
            @Override
            public boolean apply(@Nullable final Version version)
            {
                return version != null && version.isArchived();
            }
        };
    }

    public static Predicate<Version> isReleased()
    {
        return new Predicate<Version>()
        {
            @Override
            public boolean apply(@Nullable final Version version)
            {
                return version != null && version.isReleased();
            }
        };
    }
}
