package com.atlassian.jira.model.querydsl;

import javax.annotation.Generated;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;

import org.ofbiz.core.entity.GenericValue;

/**
 * Data Transfer Object for the IssueTypeScreenSchemeEntity entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 *
 * @see QIssueTypeScreenSchemeEntity
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class IssueTypeScreenSchemeEntityDTO
{
    private Long id;
    private String issuetype;
    private Long scheme;
    private Long fieldscreenscheme;

    public Long getId()
    {
        return id;
    }

    public String getIssuetype()
    {
        return issuetype;
    }

    public Long getScheme()
    {
        return scheme;
    }

    public Long getFieldscreenscheme()
    {
        return fieldscreenscheme;
    }

    public IssueTypeScreenSchemeEntityDTO(Long id, String issuetype, Long scheme, Long fieldscreenscheme)
    {
        this.id = id;
        this.issuetype = issuetype;
        this.scheme = scheme;
        this.fieldscreenscheme = fieldscreenscheme;
    }
    /**
    * Creates a GenericValue object from the values in this Data Transfer Object.
    * <p>
    *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
    * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
    * @return a GenericValue object constructed from the values in this Data Transfer Object.
    */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator)
    {
        return ofBizDelegator.makeValue("IssueTypeScreenSchemeEntity", new FieldMap()
                        .add("id", id)
                        .add("issuetype", issuetype)
                        .add("scheme", scheme)
                        .add("fieldscreenscheme", fieldscreenscheme)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */

    public static IssueTypeScreenSchemeEntityDTO fromGenericValue(GenericValue gv)
    {
        return new IssueTypeScreenSchemeEntityDTO(
            gv.getLong("id"),
            gv.getString("issuetype"),
            gv.getLong("scheme"),
            gv.getLong("fieldscreenscheme")
        );
    }
}

