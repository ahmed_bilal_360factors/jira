package com.atlassian.jira.upgrade.tasks;

import java.util.EnumSet;

import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;

/**
 * Indexes issues because of the changes to how Number Custom Fields are indexed and searched.
 * It would be nice to only re-index instances/projects/issues where we know such custom fields exist,
 * but as the NumberCustomFieldIndexer and ExactNumberSearcher are api they can be used by plugin types about which
 * we know nothing, so we don't actually know when they are used. We do know however these fields are used by
 * JIRA-Agile and ServiceDesk so are pretty common.
 */
public class UpgradeTask_Build64009 extends AbstractImmediateUpgradeTask
{
    public UpgradeTask_Build64009()
    {
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception
    {
        getReindexRequestService().requestReindex(ReindexRequestType.DELAYED, EnumSet.of(AffectedIndex.ISSUE), EnumSet.noneOf(SharedEntityType.class));
    }

    @Override
    public String getBuildNumber()
    {
        return "64009";
    }

    @Override
    public String getShortDescription()
    {
        return "Indexes issues for changes to indexing of Number Custom Field Types.";
    }

}
