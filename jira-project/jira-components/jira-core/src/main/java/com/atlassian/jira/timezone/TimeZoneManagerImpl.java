package com.atlassian.jira.timezone;

import java.util.TimeZone;

import com.atlassian.core.user.preferences.Preferences;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.preferences.PreferenceKeys;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.user.util.UserManager;

import org.apache.commons.lang.StringUtils;

/**
 * @since v4.4
 */
public class TimeZoneManagerImpl implements TimeZoneManager
{
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final UserPreferencesManager userPreferencesManager;
    private final ApplicationProperties applicationProperties;
    private final UserManager userManager;

    public TimeZoneManagerImpl(JiraAuthenticationContext jiraAuthenticationContext, UserPreferencesManager userPreferencesManager, ApplicationProperties applicationProperties, final UserManager userManager)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.userPreferencesManager = userPreferencesManager;
        this.applicationProperties = applicationProperties;
        this.userManager = userManager;
    }

    @Override
    public TimeZone getLoggedInUserTimeZone()
    {
        return getTimeZoneforUser(jiraAuthenticationContext.getLoggedInUser());
    }

    @Override
    public TimeZone getTimeZoneforUser(User user)
    {
        if (userManager.isUserExisting(ApplicationUsers.from(user)))
        {
            Preferences preferences = userPreferencesManager.getPreferences(user);
            String timezoneId = (preferences != null) ? preferences.getString(PreferenceKeys.USER_TIMEZONE) : null;
            if (StringUtils.isNotEmpty(timezoneId))
            {
                return TimeZone.getTimeZone(timezoneId);
            }
        }
        return getDefaultTimezone();
    }

    @Override
    public TimeZone getDefaultTimezone()
    {
        String systemDefaultTimeZoneId = applicationProperties.getString(APKeys.JIRA_DEFAULT_TIMEZONE);
        if (StringUtils.isNotEmpty(systemDefaultTimeZoneId))
        {
            return TimeZone.getTimeZone(systemDefaultTimeZoneId);

        }
        return TimeZone.getDefault();
    }
}
