package com.atlassian.jira.issue;

import com.atlassian.fugue.Option;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.customfields.TextCustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.util.Predicate;
import com.atlassian.jira.util.Predicates;
import com.atlassian.util.concurrent.LazyReference;

import com.google.common.collect.ImmutableSet;

import java.util.List;
import java.util.Set;
import javax.annotation.Nonnull;

public class DefaultIssueTextFieldCharacterLengthValidator implements IssueTextFieldCharacterLengthValidator
{
    private final TextFieldCharacterLengthValidator textFieldCharacterLengthValidator;
    private final LazyReference<CustomFieldManager> customFieldManagerLazyReference = new LazyReference<CustomFieldManager>()
    {
        @Override
        protected CustomFieldManager create() throws Exception
        {
            return ComponentAccessor.getCustomFieldManager();
        }
    };

    public DefaultIssueTextFieldCharacterLengthValidator(final TextFieldCharacterLengthValidator textFieldCharacterLengthValidator)
    {
        this.textFieldCharacterLengthValidator = textFieldCharacterLengthValidator;
    }

    @Nonnull
    @Override
    public ValidationResult validateAllFields(@Nonnull final Issue issue)
    {
        return validateFields(issue, Option.<Set<String>>none());
    }

    @Nonnull
    @Override
    public ValidationResult validateModifiedFields(@Nonnull final MutableIssue issue)
    {
        return validateFields(issue, Option.some(issue.getModifiedFields().keySet()));
    }

    /**
     * Validates issue text fields.
     *
     * @param issue the issue
     * @param fieldsToValidate if present it holds ids of fields to be validated, otherwise all fields are to be
     * validated
     * @return validation results
     */
    private ValidationResult validateFields(@Nonnull final Issue issue, @Nonnull Option<Set<String>> fieldsToValidate)
    {
        final ImmutableSet.Builder<String> builder = new ImmutableSet.Builder<String>();

        validateSystemFields(issue, builder, (fieldsToValidate.isDefined() ? modifiedOnly(fieldsToValidate.get()) : Predicates.<String>truePredicate()));
        if (fieldsToValidate.isDefined())
        {
            // iterates over modified text cf fields
            validateModifiedCustomFields(issue, builder, fieldsToValidate.get());
        }
        else
        {
            // iterates over all text cf fields for this issue
            validateAllCustomFields(issue, builder);
        }

        return new ValidationResultImpl(builder.build(), textFieldCharacterLengthValidator.getMaximumNumberOfCharacters());
    }

    private Predicate<String> modifiedOnly(final Set<String> fieldsToValidate)
    {
        return new Predicate<String>()
        {
            @Override
            public boolean evaluate(final String fieldId)
            {
                return fieldsToValidate.contains(fieldId);
            }
        };
    }

    private void validateSystemFields(final Issue issue, final ImmutableSet.Builder<String> builder, final Predicate<String> shouldValidate)
    {
        if (shouldValidate.evaluate(IssueFieldConstants.DESCRIPTION))
        {
            if (textFieldCharacterLengthValidator.isTextTooLong(issue.getDescription()))
            {
                builder.add(IssueFieldConstants.DESCRIPTION);
            }
        }
        if (shouldValidate.evaluate(IssueFieldConstants.ENVIRONMENT))
        {
            if (textFieldCharacterLengthValidator.isTextTooLong(issue.getEnvironment()))
            {
                builder.add(IssueFieldConstants.ENVIRONMENT);
            }
        }
    }

    private void validateAllCustomFields(final Issue issue, final ImmutableSet.Builder<String> builder)
    {
        // due to issue object being passed to create method as a plain Issue
        // we can't get the list of modified fields and have to iterate over all fields
        // but this is for create method so each fields has to be checked
        final List<CustomField> customFieldObjects = customFieldManagerLazyReference.get().getCustomFieldObjects(issue);
        for (final CustomField cf : customFieldObjects)
        {
            validateCustomField(issue, builder, cf);
        }
    }

    private void validateCustomField(final Issue issue, final ImmutableSet.Builder<String> builder, final CustomField cf)
    {
        if (cf.getCustomFieldType() instanceof TextCustomFieldType)
        {
            final Object customFieldValue = issue.getCustomFieldValue(cf);
            if (customFieldValue instanceof String)
            {
                if (textFieldCharacterLengthValidator.isTextTooLong((String) customFieldValue))
                {
                    builder.add(cf.getId());
                }
            }
        }
    }

    private void validateModifiedCustomFields(final Issue issue, final ImmutableSet.Builder<String> builder, final Set<String> fieldsToValidate)
    {
        // in case there are many custom fields (25k) but only a single fields has been edited (inline issue edit)
        // iterate over modified fields only
        for (final String fieldId : fieldsToValidate)
        {
            if (IssueFieldConstants.DESCRIPTION.equals(fieldId) || IssueFieldConstants.ENVIRONMENT.equals(fieldId))
            {
                // these are validates in validateSystemFields (if modified)
                continue;
            }
            final CustomField cf = customFieldManagerLazyReference.get().getCustomFieldObject(fieldId);
            if (cf == null)
            {
                // this is not a custom field - for example summary
                continue;
            }
            validateCustomField(issue, builder, cf);
        }
    }


    class ValidationResultImpl implements ValidationResult
    {
        private final ImmutableSet<String> invalidFieldIds;
        private final long maximumNumberOfCharacters;

        ValidationResultImpl(@Nonnull final ImmutableSet<String> invalidFieldIds, final long maximumNumberOfCharacters)
        {
            this.invalidFieldIds = invalidFieldIds;
            this.maximumNumberOfCharacters = maximumNumberOfCharacters;
        }

        @Override
        public boolean isValid()
        {
            return invalidFieldIds.isEmpty();
        }

        @Override
        public long getMaximumNumberOfCharacters()
        {
            return maximumNumberOfCharacters;
        }

        @Nonnull
        @Override
        public Set<String> getInvalidFieldIds()
        {
            return invalidFieldIds;
        }
    }

}
