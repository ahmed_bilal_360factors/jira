package com.atlassian.jira.web.pagebuilder;

import java.io.Writer;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.web.action.util.FieldsResourceIncluder;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;

/**
 * Decorator representing general-*.jsp
 * @since v6.1
 */
public class GeneralJspDecorator extends AbstractJspDecorator implements Decorator
{
    public GeneralJspDecorator(WebResourceAssembler webResourceAssembler)
    {
        super(webResourceAssembler,
                "/decorators/general-head-pre.jsp",
                "/decorators/general-head-post.jsp",
                "/decorators/general-body-pre.jsp",
                "/decorators/general-body-post.jsp");
    }

    @Override
    public void writePreHead(final Writer writer)
    {
        webResourceAssembler.resources()
                .requireContext("atl.general")
                .requireContext("jira.general");

        final FieldsResourceIncluder headFieldResourceIncluder = ComponentAccessor.getComponent(FieldsResourceIncluder.class);
        headFieldResourceIncluder.includeFieldResourcesForCurrentUser();

        super.writePreHead(writer);
    }
}
