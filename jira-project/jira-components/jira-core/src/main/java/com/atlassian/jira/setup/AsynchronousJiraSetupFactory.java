package com.atlassian.jira.setup;

/**
 * This factory is responsible for creating a singleton of AsynchronousJiraSetup.
 *
 * @since v6.4
 */
public class AsynchronousJiraSetupFactory
{
    private static AsynchronousJiraSetup<InstantSetupStrategy.SetupParameters, InstantSetupStrategy.Step> instance;

    public static AsynchronousJiraSetup<InstantSetupStrategy.SetupParameters, InstantSetupStrategy.Step> getInstance()
    {
        synchronized (AsynchronousJiraSetupFactory.class)
        {
            if (instance == null)
            {
                instance = new AsynchronousJiraSetup<InstantSetupStrategy.SetupParameters, InstantSetupStrategy.Step>(new InstantSetupStrategy());
            }
        }
        return instance;
    }
}
