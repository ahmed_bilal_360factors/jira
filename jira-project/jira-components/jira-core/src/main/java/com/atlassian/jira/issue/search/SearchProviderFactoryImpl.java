package com.atlassian.jira.issue.search;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.index.IndexDirectoryFactory;
import com.atlassian.jira.issue.index.IssueSearcherFactory;

import org.apache.lucene.search.IndexSearcher;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

public class SearchProviderFactoryImpl implements SearchProviderFactory
{
    private final IssueSearcherFactory issueSearcherFactory;

    public SearchProviderFactoryImpl(final IssueSearcherFactory issueSearcherFactory)
    {
        this.issueSearcherFactory = notNull("jiraIndexSearchers", issueSearcherFactory);
    }

    public SearchProviderFactoryImpl()
    {
        this(ComponentAccessor.getComponentOfType(IssueSearcherFactory.class));
    }

    public IndexSearcher getSearcher(final String searcherName)
    {
        if (ISSUE_INDEX.equals(searcherName))
        {
            return issueSearcherFactory.getEntitySearcher(IndexDirectoryFactory.Name.ISSUE);
        }
        else if (COMMENT_INDEX.equals(searcherName))
        {
            return issueSearcherFactory.getEntitySearcher(IndexDirectoryFactory.Name.COMMENT);
        }
         else if (CHANGE_HISTORY_INDEX.equals(searcherName))
        {
            return issueSearcherFactory.getEntitySearcher(IndexDirectoryFactory.Name.CHANGE_HISTORY);
        }
        else if (WORKLOG_INDEX.equals(searcherName))
        {
            return issueSearcherFactory.getEntitySearcher(IndexDirectoryFactory.Name.WORKLOG);
        }
        throw new UnsupportedOperationException("Only issue, comment and change history indexes are catered for currently");
    }
}
