package com.atlassian.jira.issue.context.persistence;

import com.atlassian.bandana.BandanaContext;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.jira.entity.EntityUtils;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.context.ProjectContext;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.Function;
import com.atlassian.jira.util.collect.CollectionUtil;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.base.Predicate;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;

import static com.atlassian.jira.util.collect.MapBuilder.build;

/**
 * Used by CachingFieldConfigContextPersister to do real DB work.
 */
public class FieldConfigContextPersisterWorker
{
    public static final String ENTITY_TABLE_NAME = "ConfigurationContext";

    public static final String ENTITY_PROJECT_CATEGORY = JiraContextNode.FIELD_PROJECT_CATEGORY;
    public static final String ENTITY_PROJECT = JiraContextNode.FIELD_PROJECT;

    public static final String ENTITY_KEY = "key";
    public static final String ENTITY_SCHEME_ID = "fieldconfigscheme";

    private final OfBizDelegator delegator;
    private final ProjectManager projectManager;

    private static final Logger log = LoggerFactory.getLogger(FieldConfigContextPersisterWorker.class);
    private final CachedReference<Multimap<Long,GenericValue>> configContextsBySchemeId;

    public FieldConfigContextPersisterWorker(final OfBizDelegator delegator, final ProjectManager projectManager, final CacheManager cacheManager)
    {
        this.delegator = delegator;
        this.projectManager = projectManager;
        this.configContextsBySchemeId = cacheManager.getCachedReference(FieldConfigContextPersisterWorker.class, "configContextsBySchemeId", new ConfigContextCacheLoader());
    }

    public List<JiraContextNode> getAllContextsForCustomField(final String key)
    {
        final Iterable<GenericValue> contextNodeGvs =
                Iterables.filter
                        (
                                delegator.findByAnd(ENTITY_TABLE_NAME, build(ENTITY_KEY, key)),
                                new ProjectInConfigurationContextGenericValueExists()
                        );

        return CollectionUtil.transform(contextNodeGvs, new Function<GenericValue, JiraContextNode>()
        {
            public JiraContextNode get(final GenericValue input)
            {
                return transformToDomainObject(input);
            }
        });
    }

    public List<JiraContextNode> getAllContextsForConfigScheme(final FieldConfigScheme fieldConfigScheme)
    {
        final Iterable<GenericValue> contextNodeGvs =
                Iterables.filter
                        (
                                configContextsBySchemeId.get().get(fieldConfigScheme.getId()),
                                new ProjectInConfigurationContextGenericValueExists()
                        );

        return CollectionUtil.transform(contextNodeGvs, new Function<GenericValue, JiraContextNode>()
        {
            public JiraContextNode get(final GenericValue input)
            {
                return transformToDomainObject(input);
            }
        });
    }

    public void removeContextsForConfigScheme(@Nonnull final FieldConfigScheme fieldConfigScheme)
    {
        removeContextsForConfigScheme(fieldConfigScheme.getId());
    }

    public void removeContextsForConfigScheme(final Long fieldConfigSchemeId)
    {
        final int result = delegator.removeByAnd(ENTITY_TABLE_NAME, build(ENTITY_SCHEME_ID, fieldConfigSchemeId));
        invalidateAll();
        if (log.isDebugEnabled())
        {
            log.debug(result + " contexts deleted for field config scheme with id '" + fieldConfigSchemeId + "'");
        }
    }

    public void removeContextsForProject(final GenericValue project)
    {
        final int result = delegator.removeByAnd(ENTITY_TABLE_NAME, build(ENTITY_PROJECT, project.getLong("id")));
        invalidateAll();
        if (log.isDebugEnabled())
        {
            log.debug(result + " contexts deleted for " + project);
        }
    }

    public void removeContextsForProject(Project project)
    {
        final int result = delegator.removeByAnd(ENTITY_TABLE_NAME, build(ENTITY_PROJECT, project.getId()));
        invalidateAll();
        if (log.isDebugEnabled())
        {
            log.debug(result + " contexts deleted for " + project);
        }
    }

    public void removeContextsForProjectCategory(final ProjectCategory projectCategory)
    {
        final int result = delegator.removeByAnd(ENTITY_TABLE_NAME, build(ENTITY_PROJECT_CATEGORY, projectCategory.getId()));
        invalidateAll();
        if (log.isDebugEnabled())
        {
            log.debug(result + " contexts deleted for " + projectCategory);
        }
    }

    /**
     * Returns a Long object representing the id of the FieldConfigScheme
     * @param context the bandana context
     * @param key the database key
     */
    public Object retrieve(final BandanaContext context, final String key)
    {
        if (context != null)
        {
            final List<GenericValue> result = delegator.findByAnd(ENTITY_TABLE_NAME, transformToFieldsMap((JiraContextNode) context).add(ENTITY_KEY,
                key).toMap());
            if ((result != null) && !result.isEmpty())
            {
                final Long schemeId = result.iterator().next().getLong(ENTITY_SCHEME_ID);
                if (result.size() > 1)
                {
                    log.warn("More than one FieldConfigScheme returned for a given context. Database may be corrupted." + "Returning first Long: " + schemeId + ". Context: " + context + " with key: " + key + " returned " + result + ".");
                }
                return schemeId;
            }
        }
        return null;
    }

    public void store(final String fieldId, final JiraContextNode contextNode, final FieldConfigScheme fieldConfigScheme)
    {
        if (retrieve(contextNode, fieldId) != null)
        {
            remove(contextNode, fieldId);
        }

        // if fieldConfigScheme==null just remove it
        if (fieldConfigScheme != null)
        {
            final MapBuilder<String, Object> props = transformToFieldsMap(contextNode);
            props.add(ENTITY_KEY, fieldId);
            props.add(ENTITY_SCHEME_ID, fieldConfigScheme.getId());
            EntityUtils.createValue(ENTITY_TABLE_NAME, props.toMap());
        }
        invalidateAll();
    }

    public void remove(final BandanaContext context)
    {
        if (context != null)
        {
            final JiraContextNode contextNode = (JiraContextNode) context;
            delegator.removeByAnd(ENTITY_TABLE_NAME, transformToFieldsMap(contextNode).toMap());
            invalidateAll();
        }
        else
        {
            log.warn("Context was null. Nothing was removed");
        }
    }

    public void remove(final BandanaContext context, final String key)
    {
        if ((context != null) && (key != null))
        {
            delegator.removeByAnd(ENTITY_TABLE_NAME, transformToFieldsMap((JiraContextNode) context).add(ENTITY_KEY, key).toMap());
            invalidateAll();
        }
        else
        {
            log.warn("Context or key was null. Nothing was removed");
        }
    }

    void invalidateAll()
    {
        configContextsBySchemeId.reset();
    }

    private MapBuilder<String, Object> transformToFieldsMap(final JiraContextNode contextNode)
    {
        return MapBuilder.newBuilder(contextNode.appendToParamsMap(Collections.<String, Object> emptyMap()));
    }

    private JiraContextNode transformToDomainObject(final GenericValue contextAsGv)
    {
        return new ProjectContext(contextAsGv.getLong(ENTITY_PROJECT), projectManager);
    }

    private class ProjectInConfigurationContextGenericValueExists implements Predicate<GenericValue>
    {
        @Override
        public boolean apply(final GenericValue contextAsGv)
        {
            final Long projectId = contextAsGv.getLong(ENTITY_PROJECT);
            return projectId == null || projectManager.getProjectObj(projectId) != null;
        }
    }

    private class ConfigContextCacheLoader implements com.atlassian.cache.Supplier<Multimap<Long, GenericValue>>
    {
        @Override
        public Multimap<Long, GenericValue> get()
        {
            final List<GenericValue> genericValues = delegator.findAll(ENTITY_TABLE_NAME);
            final Multimap<Long, GenericValue> cacheValues = ArrayListMultimap.create();
            for (GenericValue genericValue : genericValues)
            {
                cacheValues.put(genericValue.getLong(ENTITY_SCHEME_ID), genericValue);
            }
            return cacheValues;
        }
    }
}
