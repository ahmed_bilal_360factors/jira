package com.atlassian.jira.index.property;

import java.io.IOException;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.jql.values.ClauseValuesGenerator;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermEnum;

import static org.apache.commons.compress.utils.IOUtils.closeQuietly;

/**
 * This value generator suggests auto-completion values for aliased issue properties.
 */
public class IssuePropertyClauseValueGenerator implements ClauseValuesGenerator
{
    private static final Logger log = LoggerFactory.getLogger(IssuePropertyClauseValueGenerator.class);

    private final String luceneFieldName;
    private final IssueIndexManager indexManager;

    public IssuePropertyClauseValueGenerator(final String luceneFieldName,
            final IssueIndexManager indexManager)
    {
        this.luceneFieldName = luceneFieldName;
        this.indexManager = indexManager;
    }

    @Override
    public Results getPossibleValues(final User searcher, final String jqlClauseName, final String valuePrefix, final int maxResults)
    {
        IndexReader indexReader = indexManager.getIssueSearcher().getIndexReader();
        ImmutableSet.Builder<Result> builder = ImmutableSet.builder();
        TermEnum termEnum = null;
        try
        {
            termEnum = indexReader.terms(new Term(luceneFieldName, valuePrefix));
            int i = 0;
            do
            {
                final Term term = termEnum.term();
                if (term.text().startsWith(valuePrefix) && term.field().equals(luceneFieldName))
                {
                    builder.add(new Result(term.text()));
                    i++;
                }
                else
                {
                    break;
                }
            } while (termEnum.next() && i < maxResults);

        }
        catch (IOException e)
        {
            log.error("IOException when getting possible values of the property on the following lucene field name" + luceneFieldName, e);
        }
        finally
        {
            closeQuietly(termEnum);
            closeQuietly(indexReader);
        }

        return new Results(ImmutableList.copyOf(builder.build()));
    }
}
