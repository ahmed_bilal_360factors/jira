package com.atlassian.jira.util;

import javax.annotation.Nonnull;

/**
 * You can never have enough StringUtils or TextUtils
 */
public class TextUtil
{
    /**
     * Pad the incoming String with spaces on the left to the given desired length.
     * <p>If the String is already of the minimum size or bigger, return it as is.</p>
     *
     * @param original the String to pad
     * @param minLength the desired length
     * @return A string padded with space to reach the desired minimum length
     */
    @Nonnull
    public static String lpad(@Nonnull String original, final int minLength)
    {
        final int length = original.length();
        if (length >= minLength)
        {
            // no padding required
            return original;
        }
        else
        {
            StringBuilder sb = new StringBuilder(minLength);
            for (int i = 0; i < minLength - length; i++)
            {
                sb.append(' ');
            }
            sb.append(original);
            return sb.toString();
        }
    }

    /**
     * Pad the incoming String with spaces on the right to the given desired length.
     * <p>If the String is already of the minimum size or bigger, return it as is.</p>
     *
     * @param original the String to pad
     * @param minLength the desired length
     * @return A string padded with space to reach the desired minimum length
     */
    @Nonnull
    public static String rpad(@Nonnull final String original, final int minLength)
    {
        final int length = original.length();
        if (length >= minLength)
        {
            // no padding required
            return original;
        }
        else
        {
            StringBuilder sb = new StringBuilder(minLength);
            sb.append(original);
            for (int i = 0; i < minLength - length; i++)
            {
                sb.append(' ');
            }
            return sb.toString();
        }
    }

}
