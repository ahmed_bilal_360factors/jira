package com.atlassian.jira.appconsistency.db;

import java.util.Collection;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.database.DatabaseCollationReader;
import com.atlassian.jira.database.SupportedCollations;
import com.atlassian.jira.startup.StartupCheck;

import com.google.common.annotations.VisibleForTesting;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A checker that will print a warning message if an unsupported database collation is being used. This checker should
 * never prevent JIRA from starting even if the database collation is unsupported.
 * <p/>
 * see JRA-39970.
 *
 * @since v6.4
 */
public class CollationCheck implements StartupCheck
{
    private static final Logger log = LoggerFactory.getLogger(CollationCheck.class);
    private final DatabaseConfig databaseConfig;
    private final DatabaseCollationReader databaseCollationReader;
    private final JiraProperties jiraProperties;
    private final WarningLogger warningLogger;

    public CollationCheck(
            DatabaseConfig databaseConfig,
            DatabaseCollationReader databaseCollationReader,
            JiraProperties jiraProperties
    )
    {
        this.databaseConfig = databaseConfig;
        this.databaseCollationReader = databaseCollationReader;
        this.jiraProperties = jiraProperties;
        this.warningLogger = new WarningLogger();
    }

    public String getName()
    {
        return "Database collation check.";
    }

    /**
     * Always returns true and will log a warning message if the database is using an unsupported collation.
     *
     * @return true
     */
    public boolean isOk()
    {

        try
        {
            String collation = databaseCollationReader.findCollation();

            if (!SupportedCollations.isSupported(databaseConfig, collation))
            {
                Collection<String> supportedCollationsList = SupportedCollations.forDatabase(databaseConfig);
                if (!supportedCollationsList.isEmpty())
                {
                    String supportedCollationsString = StringUtils.join(supportedCollationsList, " or ");
                    showBigWarning(String.format("You are using an unsupported %s collation: %s. This may cause some functionality to not work.\n", getDatabaseType(), collation)
                                    + String.format("Please use %s as the collation instead.", supportedCollationsString)
                    );
                }
                else
                {
                    showBigWarning("Your database is not supported. This may cause some functionality to not work");
                }
            }
        }
        catch (Exception e)
        {
            // Make sure all exceptions are caught. The collation check should never prevent JIRA from starting
            showBigWarning("The database collation could not be read. An unsupported collation could cause some functionality to not work");
            log.info("Exception when reading database collation", e);
        }

        return true;
    }

    private String getDatabaseType()
    {
        return databaseConfig.getDatabaseType();
    }

    // NOTE: Never called since this StartUp checker just produces a log message and does not lock up JIRA.
    public String getFaultDescription()
    {
        return null;
    }

    // NOTE: Never called since this StartUp checker just produces a log message and does not lock up JIRA.
    public String getHTMLFaultDescription()
    {
        return null;
    }

    @Override
    public void stop()
    {
    }

    @Override
    public String toString()
    {
        return getName();
    }

    private void showBigWarning(String message)
    {
        getWarningLogger().showWarning(message);
    }

    @VisibleForTesting
    WarningLogger getWarningLogger()
    {
        return warningLogger;
    }

    @VisibleForTesting
    class WarningLogger
    {

        /**
         * Logs a warning message surrounded by a line of *'s above and below
         */
        public void showWarning(String message)
        {
            final String lineOfStars = StringUtils.repeat("*", 100);
            final String newLine = jiraProperties.getProperty("line.separator");
            final String warning = newLine + newLine
                    + lineOfStars + newLine
                    + message + newLine
                    + lineOfStars + newLine;

            log.warn(warning);
        }

    }
}
