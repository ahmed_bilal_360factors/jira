package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Condition to determine whether an issue is Unresolved
 * <p/>
 * An issue must be in the JiraHelper context params.
 *
 * @since v4.1
 */
public class IsIssueUnresolvedCondition extends AbstractIssueWebCondition
{
    private static final Logger log = LoggerFactory.getLogger(IsIssueUnresolvedCondition.class);

    public boolean shouldDisplay(ApplicationUser user, Issue issue, JiraHelper jiraHelper)
    {
        return issue.getResolutionObject() == null;

    }

}
