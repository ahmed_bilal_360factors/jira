package com.atlassian.jira.issue.attachment;

import com.atlassian.fugue.Option;
import com.atlassian.jira.web.action.issue.TemporaryWebAttachmentsMonitor;

/**
 * Locator to abstract how we obtain the TemporaryWebAttachmentsMonitor.
 * Implementations should store one of these monitors per user session.
 * @since v6.4
 */
public interface TemporaryWebAttachmentsMonitorLocator
{
    /**
     *
     * @return TemporaryStreamAttachmentsMonitor if one already exists or none in other case.
     */
    Option<TemporaryWebAttachmentsMonitor> get();

    /**
     * Retrieves TemporaryStreamAttachmentsMonitor from store or creates new one if needed.
     * @return existing instance of TemporaryStreamAttachmentsMonitor or new one if it is not present in store.
     * @throws IllegalStateException if this method is invoked outside request scope
     */
    TemporaryWebAttachmentsMonitor getOrCreate();
}
