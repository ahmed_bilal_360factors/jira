package com.atlassian.jira.startup;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.concurrent.atomic.AtomicBoolean;

import com.atlassian.jira.web.monitor.dump.ThreadInfos;

import com.google.common.annotations.VisibleForTesting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * A class which takes a thread dump after a specified time. Thread dumping can be cancelled by calling the
 * cancelDump method, which will also cause the thread to finish next time it awakens from sleep. The thread this
 * runs on will sleep until the specified duration has elapsed or this object has been notified to stop waiting. If it
 * is interrupted, it will immediately attempt to dump the thread logs.
 *
 * @since 6.4
 */
final class ThreadDumper implements Runnable
{
    private static final Logger classLogger = LoggerFactory.getLogger(ThreadDumper.class);
    
    private final Logger log;
    private final AtomicBoolean shouldLog = new AtomicBoolean(true);
    private final long timeout;

    /**
     * @param timeout time to wait before logging - if we reach this time and logging has not been cancelled, then
     * threads will be dumped.
     */
    public ThreadDumper(final long timeout)
    {
        this(timeout, classLogger);
    }
    
    /**
     * @param timeout time to wait before logging - if we reach this time and logging has not been cancelled, then
     * threads will be dumped.
     * @param log the logger to log to
     */
    @VisibleForTesting
    ThreadDumper(final long timeout, Logger log)
    {
        if (timeout < 1)
        {
            this.timeout = 1;
        }
        else
        {
            this.timeout = timeout;
        }
        this.log = log;
    }

    /**
     * Cancels thread dumping if the threads have not yet been dumped and calls the notify() method on this object.
     */
    public synchronized void cancelDump()
    {
        shouldLog.set(false);
        this.notify();
    }

    @Override
    public synchronized void run()
    {
        if (!shouldLog.get())
        {
            log.debug("Thread logging set to do-not-log. Exiting early.");
            return;
        }

        try
        {
            long end = System.currentTimeMillis() + timeout;
            while (!timedOut(end) &&  !interrupted() && shouldLog.get())
            {
                this.wait(end - System.currentTimeMillis());
            }
        }
        catch (InterruptedException e)
        {
            log.error("Thread logging thread interrupted. Will log threads immediately.", e);
        }

        logThreads();
    }

    private boolean timedOut(long end)
    {
        return System.currentTimeMillis() >= end;
    }

    private boolean interrupted() {
        Thread thread = Thread.currentThread();
        return thread.isInterrupted();
    }

    private void logThreads()
    {
        if (!shouldLog.get())
        {
            log.info("Thread logging requested and JIRA shutdown completed within specified timeout " + timeout + "ms. Hooray!");
            return;
        }

        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        ThreadInfo[] threadInfos = threadMXBean.dumpAllThreads(threadMXBean.isObjectMonitorUsageSupported(), threadMXBean.isSynchronizerUsageSupported());

        StringWriter out = new StringWriter();
        PrintWriter printWriter = new PrintWriter(out);
        printWriter.printf("Logging all threads because time limit for shutdown was reached: %d.", this.timeout);
        printWriter.println();

        for (ThreadInfo thread : threadInfos)
        {
            printWriter.append(ThreadInfos.toString(thread));
        }
        log.error(out.toString());
    }

    public boolean isLogEnabled()
    {
        return this.shouldLog.get();
    }

    public long getTimeout()
    {
        return this.timeout;
    }
}
