package com.atlassian.jira.issue.attachment.store.strategy.move;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.atlassian.jira.issue.attachment.AttachmentFileGetData;
import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.util.concurrent.Effect;
import com.atlassian.util.concurrent.Effects;

import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @since v6.4
 */
public class ResendingAttachmentStreamCreator
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ResendingAttachmentStreamCreator.class);
    private final IOUtilsWrapper  ioUtilsWrapper;

    public ResendingAttachmentStreamCreator(final IOUtilsWrapper ioUtilsWrapper)
    {
        this.ioUtilsWrapper = ioUtilsWrapper;
    }

    public Pair<InputStream, Effect<Object>> getInputStreamWithCloseHandler(final AttachmentGetData attachmentGetData)
    {
        try
        {
            if (attachmentGetData instanceof AttachmentFileGetData)
            {
                final AttachmentFileGetData fileGetData = (AttachmentFileGetData) attachmentGetData;
                final File file = fileGetData.getFile();
                return getInputStreamForFile(file, Effects.noop());
            }
            else
            {
                return getStreamCopiedToTemporaryFile(attachmentGetData);
            }
        }
        catch (final Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    private Pair<InputStream, Effect<Object>> getStreamCopiedToTemporaryFile(final AttachmentGetData attachmentGetData)
            throws Exception
    {
        final File tempFile = copyStreamToTemporaryFile(attachmentGetData.getInputStream());
        try
        {
            return getInputStreamForFile(tempFile, new Effect<Object>()
            {
                @Override
                public void apply(final Object ignore)
                {
                    deleteTemporaryFile(tempFile);
                }
            });
        }
        catch (final Exception e)
        {
            deleteTemporaryFile(tempFile);
            throw e;
        }
    }

    private void deleteTemporaryFile(final File tempFile)
    {
        if (!tempFile.delete())
        {
            LOGGER.warn("Failed to delete temporary file: "+tempFile.getAbsolutePath());
        }
    }

    private File copyStreamToTemporaryFile(final InputStream inputStream) throws IOException
    {
        final File tempFile = ioUtilsWrapper.createTempFile("tempAttachment", "att");
        final FileOutputStream output = ioUtilsWrapper.openOutputStream(tempFile);
        try
        {
            ioUtilsWrapper.copy(inputStream, output);
            return tempFile;
        }
        finally
        {
            IOUtils.closeQuietly(output);
        }
    }

    private Pair<InputStream, Effect<Object>> getInputStreamForFile(final File file, final Effect<Object> effect)
            throws IOException
    {
        final InputStream fileInputStream = ioUtilsWrapper.openInputStream(file);
        return Pair.<InputStream, Effect<Object>>of(fileInputStream, new Effect<Object>()
        {
            @Override
            public void apply(final Object ignore)
            {
                try
                {
                    effect.apply(ignore);
                }
                finally
                {
                    IOUtils.closeQuietly(fileInputStream);
                }
            }
        });
    }
}

