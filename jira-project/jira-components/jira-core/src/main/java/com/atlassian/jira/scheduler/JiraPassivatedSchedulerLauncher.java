package com.atlassian.jira.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Will Launch the scheduler but leave it in standby mode.
 *
 * @since v6.1
 */
public class JiraPassivatedSchedulerLauncher extends JiraSchedulerLauncher
{
    private static final Logger log = LoggerFactory.getLogger(JiraPassivatedSchedulerLauncher.class);

    @Override
    protected void proceedIfAllClear()
    {
        log.info("JIRA Scheduler not started: This node is in the passivated state.");
    }
}
