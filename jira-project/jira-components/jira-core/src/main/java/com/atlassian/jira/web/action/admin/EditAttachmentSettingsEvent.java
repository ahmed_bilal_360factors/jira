package com.atlassian.jira.web.action.admin;

import javax.annotation.concurrent.Immutable;

import com.atlassian.jira.config.util.AttachmentPathManager;

/**
 * Event being thrown when attachment settings edit attempt was made. It does not necessarily means that some settings
 * has changed.
 *
 * @since v6.4
 */
@Immutable
public class EditAttachmentSettingsEvent
{
    private final String primaryStoreType;
    private final String secondaryStoreType;
    private final AttachmentPathManager.Mode attachmentMode;

    public EditAttachmentSettingsEvent(final String primaryStoreType, final String secondaryStoreType,
            final AttachmentPathManager.Mode attachmentMode)
    {
        this.primaryStoreType = primaryStoreType;
        this.secondaryStoreType = secondaryStoreType;
        this.attachmentMode = attachmentMode;
    }

    public String getPrimaryStoreType()
    {
        return primaryStoreType;
    }

    public String getSecondaryStoreType()
    {
        return secondaryStoreType;
    }

    public AttachmentPathManager.Mode getAttachmentMode()
    {
        return attachmentMode;
    }
}
