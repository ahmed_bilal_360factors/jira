package com.atlassian.jira.plugin.attachment;

/**
 * Builds an {@link com.atlassian.jira.plugin.attachment.AttachmentArchiveEntry}.
 *
 * @since v6.4
 */
public class AttachmentArchiveEntryBuilder
{

    private int entryIndex;
    private String name;
    private long size;
    private String mediaType;

    public AttachmentArchiveEntryBuilder entryIndex(final int entryIndex)
    {
        this.entryIndex = entryIndex;
        return this;
    }

    public AttachmentArchiveEntryBuilder name(final String name)
    {
        this.name = name;
        return this;
    }

    public AttachmentArchiveEntryBuilder size(final long size)
    {
        this.size = size;
        return this;
    }

    public AttachmentArchiveEntryBuilder mediaType(final String mediaType)
    {
        this.mediaType = mediaType;
        return this;
    }

    public AttachmentArchiveEntry build()
    {
        return new AttachmentArchiveEntryImpl(entryIndex, name, size, mediaType);
    }
}