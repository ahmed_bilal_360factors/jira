package com.atlassian.jira.plugin;

public interface OsgiServiceTrackerCache
{
    /**
     * Retrieves and returns a public component from OSGi land via its class.
     * @param clazz The interface class
     * @return The component, or null if not found
     * @since v6.4
     */
    public <T> T getOsgiComponentOfType(final Class<T> clazz);
}
