package com.atlassian.jira.setup;

import com.google.common.collect.ImmutableMap;

/**
 * This interface abstracts actual process of setting up JIRA from the way progressing through steps is handled.
 * Strategy delegates step switching through StepSwitcher callback. SetupStrategy also is responsible for defining
 * initial steps.
 *
 * @since v6.4
 */
public interface SetupStrategy<ParametersT, StepT>
{
    enum Status
    {
        AWAITING,
        PENDING,
        SUCCESS,
        FAILURE
    }

    void setup(final ParametersT parameters, StepSwitcher<StepT> switcher) throws Exception;

    /**
     * Define initial statuses of different steps.
     *
     * @return map of steps to their statuses. At least one step <b>must</b> be in pending status
     */
    ImmutableMap<StepT, SetupStrategy.Status> getInitialSteps();

    interface StepSwitcher<StepT>
    {
        void withStep(final StepT step, final StepTask task) throws Exception;

        void setError(String error);

        void setBundleHasLicenseError(boolean hasError);
    }

    interface StepTask
    {
        void run() throws Exception;
    }
}
