package com.atlassian.jira.jql.query;

import java.util.List;

import com.atlassian.fugue.Option;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.index.IndexDocumentConfiguration;
import com.atlassian.jira.index.property.PluginIndexConfiguration;
import com.atlassian.jira.index.property.PluginIndexConfigurationManager;
import com.atlassian.jira.issue.customfields.converters.DoubleConverter;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.util.JqlDateSupport;
import com.atlassian.jira.jql.validator.IssuePropertyClauseValidator;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.InjectableComponent;
import com.atlassian.query.clause.Property;
import com.atlassian.query.clause.TerminalClause;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import static com.atlassian.jira.entity.property.EntityPropertyType.ISSUE_PROPERTY;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;

/**
 * Factory for producing clauses for issue properties.
 *
 * @since v6.2
 */
public class IssuePropertyClauseQueryFactory extends AbstractPropertyClauseQueryFactory
{
    private final PluginIndexConfigurationManager pluginIndexConfigurationManager;

    public IssuePropertyClauseQueryFactory(final PluginIndexConfigurationManager pluginIndexConfigurationManager,
            final DoubleConverter doubleConverter,
            final JqlDateSupport jqlDateSupport,
            final JqlOperandResolver operandResolver,
            final JiraAuthenticationContext authenticationContext)
    {
        super(doubleConverter, jqlDateSupport, operandResolver, authenticationContext);
        this.pluginIndexConfigurationManager = pluginIndexConfigurationManager;
    }

    @Override
    protected Option<Property> getProperty(final TerminalClause terminalClause)
    {
        return terminalClause.getProperty();
    }

    @Override
    protected boolean isSupportedOperator(final TerminalClause terminalClause)
    {
        return IssuePropertyClauseValidator.isSupportedOperator(terminalClause.getOperator());
    }

    @Override
    protected Iterable<IndexDocumentConfiguration.Type> getPropertyTypes(final Property property)
    {
        {
            final Iterable<PluginIndexConfiguration> configurations =
                    pluginIndexConfigurationManager.getDocumentsForEntity(ISSUE_PROPERTY.getDbEntityName());

            final String propertyKey = property.getKeysAsString();
            final String objRef = property.getObjectReferencesAsString();

            final Iterable<IndexDocumentConfiguration.KeyConfiguration> keyConfigurations = concat(transform(configurations, new Function<PluginIndexConfiguration, List<IndexDocumentConfiguration.KeyConfiguration>>()
            {
                @Override
                public List<IndexDocumentConfiguration.KeyConfiguration> apply(final PluginIndexConfiguration indexConfiguration)
                {
                    return indexConfiguration.getIndexDocumentConfiguration().getKeyConfigurations();
                }
            }));

            final Iterable<IndexDocumentConfiguration.KeyConfiguration> filteredConfigurations = filter(keyConfigurations, new Predicate<IndexDocumentConfiguration.KeyConfiguration>()
            {
                @Override
                public boolean apply(final IndexDocumentConfiguration.KeyConfiguration keyConfiguration)
                {
                    return keyConfiguration.getPropertyKey().equals(propertyKey);
                }
            });

            final Iterable<IndexDocumentConfiguration.ExtractConfiguration> extractConfigurations = concat(transform(filteredConfigurations, new Function<IndexDocumentConfiguration.KeyConfiguration, List<IndexDocumentConfiguration.ExtractConfiguration>>()
            {
                @Override
                public List<IndexDocumentConfiguration.ExtractConfiguration> apply(final IndexDocumentConfiguration.KeyConfiguration keyConfiguration)
                {
                    return keyConfiguration.getExtractorConfigurations();
                }
            }));

            return transform(filter(extractConfigurations, new Predicate<IndexDocumentConfiguration.ExtractConfiguration>()
            {
                @Override
                public boolean apply(final IndexDocumentConfiguration.ExtractConfiguration extractConfig)
                {
                    return extractConfig.getPath().equals(objRef);
                }
            }), new Function<IndexDocumentConfiguration.ExtractConfiguration, IndexDocumentConfiguration.Type>()
            {
                @Override
                public IndexDocumentConfiguration.Type apply(final IndexDocumentConfiguration.ExtractConfiguration extractConfiguration)
                {
                    return extractConfiguration.getType();
                }
            });
        }
    }
}
