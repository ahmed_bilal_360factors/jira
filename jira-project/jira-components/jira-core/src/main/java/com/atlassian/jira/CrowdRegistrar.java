package com.atlassian.jira;

import java.util.concurrent.Executors;

import com.atlassian.crowd.cql.parser.CqlQueryParser;
import com.atlassian.crowd.cql.parser.CqlQueryParserImpl;
import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.dao.permission.InternalUserPermissionDAO;
import com.atlassian.crowd.dao.webhook.NoopWebhookDAOImpl;
import com.atlassian.crowd.dao.webhook.WebhookDAO;
import com.atlassian.crowd.directory.DirectoryCacheFactoryImpl;
import com.atlassian.crowd.directory.InternalDirectoryUtils;
import com.atlassian.crowd.directory.InternalDirectoryUtilsImpl;
import com.atlassian.crowd.directory.PasswordConstraintsLoader;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapperImpl;
import com.atlassian.crowd.directory.ldap.cache.DirectoryCacheFactory;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelper;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelperImpl;
import com.atlassian.crowd.directory.loader.DelegatedAuthenticationDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.DelegatedAuthenticationDirectoryInstanceLoaderImpl;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.InternalDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.InternalDirectoryInstanceLoaderImpl;
import com.atlassian.crowd.directory.loader.InternalHybridDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.LDAPDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.LDAPDirectoryInstanceLoaderImpl;
import com.atlassian.crowd.directory.loader.RemoteCrowdDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.RemoteCrowdDirectoryInstanceLoaderImpl;
import com.atlassian.crowd.embedded.api.ApplicationFactory;
import com.atlassian.crowd.embedded.api.CrowdDirectoryService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.PasswordScoreService;
import com.atlassian.crowd.embedded.api.UnfilteredCrowdService;
import com.atlassian.crowd.embedded.core.CrowdDirectoryServiceImpl;
import com.atlassian.crowd.embedded.core.CrowdEmbeddedApplicationFactory;
import com.atlassian.crowd.embedded.core.CrowdServiceImpl;
import com.atlassian.crowd.embedded.core.FilteredCrowdServiceImpl;
import com.atlassian.crowd.embedded.core.FilteredGroupsProvider;
import com.atlassian.crowd.embedded.core.NoOpPasswordScoreServiceImpl;
import com.atlassian.crowd.embedded.core.XmlFilteredGroupsProvider;
import com.atlassian.crowd.embedded.core.util.CrowdServiceFactory;
import com.atlassian.crowd.embedded.core.util.StaticCrowdServiceFactory;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.embedded.spi.GroupDao;
import com.atlassian.crowd.embedded.spi.MembershipDao;
import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.event.EventStore;
import com.atlassian.crowd.event.EventStoreGeneric;
import com.atlassian.crowd.event.StoringEventListener;
import com.atlassian.crowd.integration.rest.service.factory.RestCrowdClientFactory;
import com.atlassian.crowd.manager.application.AliasManager;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationManagerGeneric;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.application.ApplicationServiceGeneric;
import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.manager.directory.BeforeGroupRemoval;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.manager.directory.DirectoryManagerGeneric;
import com.atlassian.crowd.manager.directory.DirectorySynchronisationInformationStore;
import com.atlassian.crowd.manager.directory.DirectorySynchronisationInformationStoreImpl;
import com.atlassian.crowd.manager.directory.DirectorySynchroniser;
import com.atlassian.crowd.manager.directory.DirectorySynchroniserHelper;
import com.atlassian.crowd.manager.directory.DirectorySynchroniserHelperImpl;
import com.atlassian.crowd.manager.directory.DirectorySynchroniserImpl;
import com.atlassian.crowd.manager.directory.NoopBeforeGroupRemoval;
import com.atlassian.crowd.manager.directory.SynchronisationStatusManager;
import com.atlassian.crowd.manager.directory.SynchronisationStatusManagerImpl;
import com.atlassian.crowd.manager.directory.monitor.DirectoryMonitorManager;
import com.atlassian.crowd.manager.directory.monitor.DirectoryMonitorManagerImpl;
import com.atlassian.crowd.manager.directory.monitor.poller.DirectoryPollerManager;
import com.atlassian.crowd.manager.login.ForgottenLoginManager;
import com.atlassian.crowd.manager.permission.PermissionManager;
import com.atlassian.crowd.manager.permission.PermissionManagerImpl;
import com.atlassian.crowd.manager.permission.UserPermissionAdminService;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.proxy.TrustedProxyManager;
import com.atlassian.crowd.manager.validation.ClientValidationManager;
import com.atlassian.crowd.manager.validation.ClientValidationManagerImpl;
import com.atlassian.crowd.manager.webhook.KeyedExecutor;
import com.atlassian.crowd.manager.webhook.NoLongTermFailureStrategy;
import com.atlassian.crowd.manager.webhook.WebhookHealthStrategy;
import com.atlassian.crowd.manager.webhook.WebhookNotificationListener;
import com.atlassian.crowd.manager.webhook.WebhookNotificationListenerImpl;
import com.atlassian.crowd.manager.webhook.WebhookPinger;
import com.atlassian.crowd.manager.webhook.WebhookRegistry;
import com.atlassian.crowd.manager.webhook.WebhookRegistryImpl;
import com.atlassian.crowd.manager.webhook.WebhookService;
import com.atlassian.crowd.manager.webhook.WebhookServiceImpl;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.search.ldap.ActiveDirectoryQueryTranslaterImpl;
import com.atlassian.crowd.search.ldap.LDAPQueryTranslater;
import com.atlassian.crowd.search.ldap.LDAPQueryTranslaterImpl;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.factory.CrowdClientFactory;
import com.atlassian.crowd.util.InetAddressCacheUtil;
import com.atlassian.crowd.util.InstanceFactory;
import com.atlassian.crowd.util.PasswordHelper;
import com.atlassian.crowd.util.PasswordHelperImpl;
import com.atlassian.jira.crowd.embedded.CrowdDelegatingI18Helper;
import com.atlassian.jira.crowd.embedded.DefaultJaacsService;
import com.atlassian.jira.crowd.embedded.JaacsService;
import com.atlassian.jira.crowd.embedded.JiraDirectoryPollerManager;
import com.atlassian.jira.crowd.embedded.JiraInstanceFactory;
import com.atlassian.jira.crowd.embedded.JiraPasswordConstraintsLoader;
import com.atlassian.jira.crowd.embedded.JiraPasswordEncoderFactory;
import com.atlassian.jira.crowd.embedded.NoopAliasManager;
import com.atlassian.jira.crowd.embedded.NoopClientProperties;
import com.atlassian.jira.crowd.embedded.NoopForgottenLoginManager;
import com.atlassian.jira.crowd.embedded.NoopInternalUserPermissionDAO;
import com.atlassian.jira.crowd.embedded.NoopPropertyManager;
import com.atlassian.jira.crowd.embedded.NoopTokenAuthenticationManager;
import com.atlassian.jira.crowd.embedded.NoopTrustedProxyManager;
import com.atlassian.jira.crowd.embedded.NoopUserPermissionAdminService;
import com.atlassian.jira.crowd.embedded.ofbiz.InternalMembershipDao;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizApplicationDao;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizCacheFlushingManager;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizDelegatingMembershipDao;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizDirectoryDao;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizGroupDao;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizInternalMembershipDao;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizUserDao;
import com.atlassian.jira.user.AutoGroupAdder;
import com.atlassian.jira.user.AutoGroupAdderImpl;
import com.atlassian.jira.user.directory.loader.JiraDbCachingRemoteDirectoryInstanceLoader;
import com.atlassian.jira.user.directory.loader.JiraDelegatingDirectoryInstanceLoader;

import static com.atlassian.jira.ComponentContainer.Scope.INTERNAL;
import static com.atlassian.jira.ComponentContainer.Scope.PROVIDED;

/**
 * Registers embedded crowd's components as well as JIRA as a Crowd Server (JaaCS).
 *
 * @since v6.4
 */
class CrowdRegistrar
{
    // The layout here is meant to parallel the spring context XML files that crowd server uses
    // to make it easier to maintain.

    /**
     * Registers the components needed for embedded crowd and JIRA as a Crowd Server (JaaCS).
     *
     * @param register the component containter into which the components will be registered
     */
    static void registerEmbeddedCrowdAndJaacsComponents(ComponentContainer register)
    {
        registerCrowdEncryption(register);
        registerCrowdDao(register);
        registerCrowdPersistence(register);
        registerCrowdPasswordConstraints(register);
        registerCrowdManagers(register);
        registerCrowdLdap(register);
        registerCrowdUtils(register);
        registerCrowdPlugins(register);
        registerCrowdServer(register);

        registerEmbeddedCrowd(register);
        registerCrowdRestClient(register);
        registerJaaCS(register);
    }


    // components/crowd-password-encoders/src/main/resources/applicationContext-CrowdEncryption.xml
    private static void registerCrowdEncryption(ComponentContainer register)
    {
        register.implementation(PROVIDED, PasswordEncoderFactory.class, JiraPasswordEncoderFactory.class);
    }

    // components/crowd-persistence-hibernate4/src/main/resources/applicationContext-CrowdDAO.xml
    private static void registerCrowdDao(ComponentContainer register)
    {
        register.implementation(INTERNAL, UserDao.class, OfBizUserDao.class);
        register.implementation(INTERNAL, GroupDao.class, OfBizGroupDao.class);
        register.implementation(PROVIDED, InternalUserPermissionDAO.class, NoopInternalUserPermissionDAO.class);
        register.implementation(INTERNAL, InternalMembershipDao.class, OfBizInternalMembershipDao.class);
        register.implementation(INTERNAL, MembershipDao.class, OfBizDelegatingMembershipDao.class);
        register.implementation(INTERNAL, ApplicationDAO.class, OfBizApplicationDao.class);
        register.implementation(INTERNAL, WebhookDAO.class, NoopWebhookDAOImpl.class);
    }

    // components/crowd-persistence-hibernate4/src/main/resources/applicationContext-CrowdPersistence.xml
    private static void registerCrowdPersistence(final ComponentContainer register)
    {
        register.implementation(INTERNAL, PasswordHelper.class, PasswordHelperImpl.class);
    }

    // components/crowd-persistence-hibernate4/src/main/resources/applicationContext-CrowdPasswordConstraints.xml
    private static void registerCrowdPasswordConstraints(final ComponentContainer register)
    {
        register.implementation(INTERNAL, PasswordConstraintsLoader.class, JiraPasswordConstraintsLoader.class);
    }

    // components/crowd-core/src/main/resources/applicationContext-CrowdManagers.xml
    private static void registerCrowdManagers(ComponentContainer register)
    {
        // Most of the components in this context are not used or get registered elsewhere
        register.implementation(PROVIDED, ApplicationManager.class, ApplicationManagerGeneric.class);
        register.implementation(PROVIDED, ApplicationService.class, ApplicationServiceGeneric.class);
        register.implementation(INTERNAL, StoringEventListener.class);
        // Set max events to 10000 based on advice in https://studio.atlassian.com/wiki/display/EMBCWD/API+Upgrade+Guides
        register.instance(INTERNAL, EventStore.class, new EventStoreGeneric(10000));
        register.implementation(PROVIDED, PropertyManager.class, NoopPropertyManager.class);
        register.implementation(INTERNAL, DirectoryDao.class, OfBizDirectoryDao.class);
        register.implementation(PROVIDED, DirectoryManager.class, DirectoryManagerGeneric.class);
        register.implementation(INTERNAL, DirectoryPollerManager.class, JiraDirectoryPollerManager.class);
        register.implementation(INTERNAL, DirectoryMonitorManager.class, DirectoryMonitorManagerImpl.class);
        register.implementation(INTERNAL, SynchronisationStatusManager.class, SynchronisationStatusManagerImpl.class);
        register.implementation(INTERNAL, DirectorySynchronisationInformationStore.class, DirectorySynchronisationInformationStoreImpl.class);
        register.implementation(INTERNAL, DirectoryCacheFactory.class, DirectoryCacheFactoryImpl.class);
        register.implementation(INTERNAL, DirectorySynchroniser.class, DirectorySynchroniserImpl.class);
        register.implementation(INTERNAL, DirectorySynchroniserHelper.class, DirectorySynchroniserHelperImpl.class);
        register.implementation(INTERNAL, PermissionManager.class, PermissionManagerImpl.class);
        register.implementation(PROVIDED, AliasManager.class, NoopAliasManager.class);
        register.implementation(INTERNAL, WebhookRegistry.class, WebhookRegistryImpl.class);
        register.implementation(INTERNAL, WebhookService.class, WebhookServiceImpl.class);
        register.implementation(INTERNAL, WebhookNotificationListener.class, WebhookNotificationListenerImpl.class);
        register.implementation(INTERNAL, WebhookHealthStrategy.class, NoLongTermFailureStrategy.class);
        register.instance(INTERNAL, KeyedExecutor.class, new KeyedExecutor<Long>(Executors.newSingleThreadExecutor()));
        register.implementation(INTERNAL, WebhookPinger.class, WebhookPinger.class);
    }

    // components/crowd-ldap/src/main/resources/applicationContext-CrowdLDAP.xml
    private static void registerCrowdLdap(ComponentContainer register)
    {
        // connectorValidator not used
        register.implementation(PROVIDED, LDAPPropertiesMapper.class, LDAPPropertiesMapperImpl.class);
        register.implementation(INTERNAL, LDAPPropertiesHelper.class, LDAPPropertiesHelperImpl.class);
        register.implementation(INTERNAL, LDAPQueryTranslater.class, LDAPQueryTranslaterImpl.class);
        register.implementation(INTERNAL, ActiveDirectoryQueryTranslaterImpl.class, ActiveDirectoryQueryTranslaterImpl.class);
        register.implementation(INTERNAL, RemoteCrowdDirectoryInstanceLoader.class, RemoteCrowdDirectoryInstanceLoaderImpl.class);
    }

    // components/crowd-core/src/main/resources/applicationContext-CrowdUtils.xml
    private static void registerCrowdUtils(ComponentContainer register)
    {
        register.implementation(INTERNAL, InstanceFactory.class, JiraInstanceFactory.class);
        register.implementation(INTERNAL, InternalDirectoryUtils.class, InternalDirectoryUtilsImpl.class);
        register.implementation(INTERNAL, PasswordScoreService.class, NoOpPasswordScoreServiceImpl.class);
        // staticResourceBundleProvider not used
        // i18nHelperConfiguration not used
        register.implementation(PROVIDED, com.atlassian.crowd.util.I18nHelper.class, CrowdDelegatingI18Helper.class);
    }

    // components/crowd-server/src/main/resources/applicationContext-CrowdPlugins.xml
    private static void registerCrowdPlugins(ComponentContainer register)
    {
        // components/crowd-server/src/main/resources/system-listeners.xml
        register.implementation(INTERNAL, AutoGroupAdder.class, AutoGroupAdderImpl.class);

    }

    // components/crowd-server/src/main/resources/applicationContext-CrowdServer.xml
    private static void registerCrowdServer(ComponentContainer register)
    {
        // Most of the components in this context are not used or get registered elsewhere
        register.instance(INTERNAL, InetAddressCacheUtil.class, new InetAddressCacheUtil(null));
        register.implementation(PROVIDED, TokenAuthenticationManager.class, NoopTokenAuthenticationManager.class);
        register.implementation(PROVIDED, TrustedProxyManager.class, NoopTrustedProxyManager.class);

        register.implementation(INTERNAL, InternalDirectoryInstanceLoader.class, InternalDirectoryInstanceLoaderImpl.class);
        register.implementation(INTERNAL, LDAPDirectoryInstanceLoader.class, LDAPDirectoryInstanceLoaderImpl.class);
        // dbCachingDelegatingDirectoryInstanceLoader is handled internally by JiraDbCachingRemoteDirectoryInstanceLoader
        // dbCachingDirectoryInstanceLoader is registered here as InternalHybridDirectoryInstanceLoader (next line)
        register.implementation(INTERNAL, InternalHybridDirectoryInstanceLoader.class, JiraDbCachingRemoteDirectoryInstanceLoader.class);
        register.implementation(INTERNAL, DelegatedAuthenticationDirectoryInstanceLoader.class, DelegatedAuthenticationDirectoryInstanceLoaderImpl.class);
        register.implementation(PROVIDED, DirectoryInstanceLoader.class, JiraDelegatingDirectoryInstanceLoader.class);

        register.implementation(PROVIDED, CqlQueryParser.class, CqlQueryParserImpl.class);
        register.implementation(PROVIDED, ForgottenLoginManager.class, NoopForgottenLoginManager.class);
        register.implementation(PROVIDED, ClientValidationManager.class, ClientValidationManagerImpl.class);
        register.implementation(INTERNAL, ApplicationFactory.class, CrowdEmbeddedApplicationFactory.class);
        register.implementation(PROVIDED, UnfilteredCrowdService.class, CrowdServiceImpl.class);
        register.implementation(PROVIDED, CrowdService.class, FilteredCrowdServiceImpl.class, UnfilteredCrowdService.class, FilteredGroupsProvider.class);

        register.implementation(PROVIDED, UserPermissionAdminService.class, NoopUserPermissionAdminService.class);
        register.implementation(INTERNAL, BeforeGroupRemoval.class, NoopBeforeGroupRemoval.class);
    }

    // Things that crowd itself doesn't use because they are specific to the embedded crowd world
    private static void registerEmbeddedCrowd(ComponentContainer register)
    {
        register.implementation(PROVIDED, CrowdDirectoryService.class, CrowdDirectoryServiceImpl.class);
        register.implementation(INTERNAL, CrowdServiceFactory.class, StaticCrowdServiceFactory.class);
        register.implementation(INTERNAL, FilteredGroupsProvider.class, XmlFilteredGroupsProvider.class);
        register.implementation(INTERNAL, OfBizCacheFlushingManager.class);
    }

    // components/crowd-integration-client-rest/src/main/resources/applicationContext-CrowdRestClient.xml
    private static void registerCrowdRestClient(ComponentContainer register)
    {
        register.implementation(PROVIDED, ClientProperties.class, NoopClientProperties.class);
        register.implementation(INTERNAL, CrowdClientFactory.class, RestCrowdClientFactory.class);
    }

    // Things that are specific to "JIRA as a Crowd Server" (JaaCS)
    private static void registerJaaCS(ComponentContainer register)
    {
        register.implementation(INTERNAL, JaacsService.class, DefaultJaacsService.class);
    }
}
