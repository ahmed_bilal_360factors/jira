package com.atlassian.jira.avatar.types.issuetype;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.types.BasicTypedTypeAvatarService;

public class IssueTypeTypeAvatarService extends BasicTypedTypeAvatarService
{
    public IssueTypeTypeAvatarService(final AvatarManager avatarManager, final IssueTypeAvatarAccessPolicy policy)
    {
        super(Avatar.Type.ISSUETYPE, avatarManager, policy);
    }
}
