package com.atlassian.jira.avatar.types.issuetype;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.types.AvatarAccessPolicy;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;

import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;

public class IssueTypeAvatarAccessPolicy implements AvatarAccessPolicy
{
    final GlobalPermissionManager permissionManager;

    public IssueTypeAvatarAccessPolicy(final GlobalPermissionManager permissionManager) {

        this.permissionManager = permissionManager;
    }

    @Override
    public boolean userCanViewAvatar(final ApplicationUser user, final Avatar avatar)
    {
        return Avatar.Type.ISSUETYPE == avatar.getAvatarType();
    }

    @Override
    public boolean userCanCreateAvatarFor(@Nullable final ApplicationUser remoteUser, @Nonnull final String owningObjectId)
    {
        return permissionManager.hasPermission(ADMINISTER, remoteUser);
    }
}
