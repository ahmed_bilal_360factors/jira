package com.atlassian.jira.web.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nonnull;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.exception.AttachmentNotFoundException;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.zip.AttachmentZipEntryStreamConsumer;
import com.atlassian.jira.issue.attachment.zip.AttachmentZipFileCreator;
import com.atlassian.jira.issue.attachment.zip.ZipEntryNotFoundException;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.util.Consumer;
import com.atlassian.jira.util.IOUtil;
import com.atlassian.jira.util.http.JiraHttpUtils;
import com.atlassian.jira.util.mime.MimeManager;
import com.atlassian.jira.web.exception.WebExceptionChecker;
import com.atlassian.seraph.util.RedirectUtils;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This servlet can BUNDLE up all the attachments of a in issue into 1 ZIP file OR it can unzip a specific entry of a
 * named attachment
 *
 * @since 4.1
 */
public class AttachmentZipServlet extends HttpServlet
{
    private static final Logger log = LoggerFactory.getLogger(AttachmentZipServlet.class);

    private static final String SECURE_VIEWS_SECURITYBREACH_JSP = "/secure/views/securitybreach.jsp";
    private static final String APPLICATION_OCTET_STREAM = "application/octet-stream";

    private static final Pattern ISSUE_ID_ONLY = Pattern.compile(".+/([0-9]+)\\.zip");
    private static final Pattern ISSUE_ID_AND_ZIP = Pattern.compile(".+/unzip/([0-9]+)/([0-9]+)(\\[|%5B)([0-9]+)(\\]|%5D)/.*");

    @Override
    protected void doGet(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
            throws ServletException, IOException
    {
        // check that attachments and zip support is enabled
        if (!checkSupportEnabled())
        {
            httpServletResponse.sendError(404, "Attachments as ZIP support is disabled");
            return;
        }
        final Issue issue;
        final String uri = httpServletRequest.getRequestURI();
        try
        {
            // this does permission checks as well
            issue = getIssue(uri);
        }
        catch (final PermissionException e)
        {
            redirectForSecurityBreach(httpServletRequest, httpServletResponse);
            return;
        }
        if (issue == null)
        {
            httpServletResponse.sendError(404, "Could not find issue");
            return;
        }
        if (uri.contains("unzip"))
        {
            unzipSpecifiedAttachment(httpServletResponse, issue, uri);
        }
        else
        {
            zipAllAttachments(httpServletRequest, httpServletResponse, issue);
        }
    }

    private void zipAllAttachments(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse, final Issue issue)
            throws IOException, ServletException
    {
        final String issueKey = issue.getKey();
        File zipFile;
        try
        {
            zipFile = createAttachmentsZipFile(issue);
        }
        catch (final IOException e)
        {
            log.error("Can not create temporary zip file : " + httpServletRequest.getPathInfo() + ": " + e.getMessage(), e);
            httpServletResponse.sendError(404, "Could not create zip file for issue : " + issueKey);
            return;
        }

        try
        {
            setResponseHeaders(httpServletRequest, httpServletResponse, zipFile, issueKey);
            writeZipResponse(httpServletResponse, new FileInputStream(zipFile));
        }
        catch (final Exception e)
        {
            if (WebExceptionChecker.canBeSafelyIgnored(e))
            {
                return;
            }
            // now send a 404 only if we have not yet written any bytes to the out stream
            if (!httpServletResponse.isCommitted())
            {
                httpServletResponse.sendError(404, "Could not serve zip file of attachments for issue " + issueKey + " : " + e.getMessage());
            }
            else
            {
                throw new ServletException("Could not serve zip file of attachments for issue " + issueKey, e);
            }
        }
        finally
        {
            deleteFile(zipFile);
        }
    }

    private void unzipSpecifiedAttachment(final HttpServletResponse httpServletResponse, final Issue issue, final String uri)
            throws IOException
    {
        final String issueKey = issue.getKey();
        final Matcher matcher = ISSUE_ID_AND_ZIP.matcher(uri);
        if (!matcher.find() || matcher.groupCount() != 5)
        {
            httpServletResponse.sendError(HttpServletResponse.SC_NOT_FOUND, "Could not create zip file for issue : " + issueKey);
            return;
        }
        final long attachmentId = Long.parseLong(matcher.group(2));
        final int entryIndex = Integer.parseInt(matcher.group(4));

        final AttachmentZipEntryStreamConsumer zipEntryStreamConsumer = new AttachmentZipEntryStreamConsumer(
                httpServletResponse.getOutputStream(),
                createOnZipExistsFunction(httpServletResponse),
                entryIndex);

        try
        {
            final AttachmentManager attachmentManager = ComponentAccessor.getComponent(AttachmentManager.class);
            final Attachment attachment = attachmentManager.getAttachment(attachmentId);

            attachmentManager.streamAttachmentContent(attachment, zipEntryStreamConsumer);
        }
        catch (final AttachmentNotFoundException e)
        {
            httpServletResponse.sendError(HttpServletResponse.SC_NOT_FOUND,
                    String.format("Could not find attachment with id: %d", attachmentId));
        }
        catch (final ZipEntryNotFoundException e)
        {
            httpServletResponse.sendError(HttpServletResponse.SC_NOT_FOUND,
                    String.format("Could not find entry with index: %d  in attachment with id: %d",
                            entryIndex, attachmentId));
        }
        catch (final IOException e)
        {
            httpServletResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                    String.format("Could not load entry with index: %d in attachment with id: %d because of unexpected error",
                            entryIndex, attachmentId));
        }
    }

    private Consumer<ZipArchiveEntry> createOnZipExistsFunction(final HttpServletResponse httpServletResponse)
    {
        return new Consumer<ZipArchiveEntry>()
        {
            @Override
            public void consume(@Nonnull final ZipArchiveEntry element)
            {
                sniffContentAndSetZipEntryResponseHeaders(httpServletResponse, element);
            }
        };
    }


    private void sniffContentAndSetZipEntryResponseHeaders(final HttpServletResponse httpServletResponse, final ZipArchiveEntry zipEntry)
    {
        final MimeManager mimeManager = ComponentAccessor.getComponent(MimeManager.class);
        final String suggestedContentType = mimeManager.getSanitisedMimeType(APPLICATION_OCTET_STREAM, zipEntry.getName());

        try
        {
            setFileDownloadHeaders(httpServletResponse, zipEntry.getSize(), zipEntry.getName(), suggestedContentType);
        }
        catch (final IOException e)
        {
            throw new RuntimeException("Failed to set appropriate file download headers because of: "+e.getMessage());
        }
    }

    private void redirectForSecurityBreach(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
            throws ServletException, IOException
    {
        if (getLoggedInUser() != null)
        {
            RequestDispatcher rd = httpServletRequest.getRequestDispatcher(SECURE_VIEWS_SECURITYBREACH_JSP);
            JiraHttpUtils.setNoCacheHeaders(httpServletResponse);
            rd.forward(httpServletRequest, httpServletResponse);
        }
        else
        {
            httpServletResponse.sendRedirect(RedirectUtils.getLoginUrl(httpServletRequest));
        }
    }

    /**
     * The URL pattern is either /attachmentzip/1234 or attachmentzip/unzip/1234/4567[n]
     *
     * @param url the url in play
     * @return the parsed issue from it or PermissionException
     * @throws PermissionException if the current user cant see the issue
     */

    private Issue getIssue(final String url) throws PermissionException
    {
        Matcher matcher = ISSUE_ID_ONLY.matcher(url);
        String issueId = null;
        if (matcher.find())
        {
            issueId = matcher.group(1);
        }
        else
        {
            matcher = ISSUE_ID_AND_ZIP.matcher(url);
            // we must have BOTH bits considered good
            if (matcher.find() && matcher.groupCount() == 5)
            {
                issueId = matcher.group(1);
            }

        }
        return parseForIssue(issueId);
    }

    private Issue parseForIssue(final String issueId) throws PermissionException
    {
        if (issueId == null)
        {
            return null;
        }
        try
        {
            final Long id = Long.parseLong(issueId);
            final Issue issue = ComponentAccessor.getIssueManager().getIssueObject(id);
            if (issue != null)
            {
                // ok does the user have permission to see that issue
                if (!hasPermissionToViewAttachment(issue))
                {
                    throw new PermissionException("The user does not have permission to see this issue");
                }
            }
            return issue;
        }
        catch (NumberFormatException e)
        {
            return null;
        }
    }

    private File createAttachmentsZipFile(final Issue issue) throws IOException
    {
        final AttachmentZipFileCreator zipFileCreator = ComponentAccessor.getComponent(AttachmentZipFileCreator.class);
        return zipFileCreator.toZipFile(issue);
    }

    private boolean writeZipResponse(final HttpServletResponse httpServletResponse, final InputStream inputStream)
            throws IOException
    {
        boolean bytesWritten = false;
        OutputStream out = httpServletResponse.getOutputStream();

        byte[] buffer = new byte[4096];
        try
        {
            while (true)
            {
                int bytesRead = inputStream.read(buffer);
                if (bytesRead == -1)
                {
                    break;
                }
                out.write(buffer, 0, bytesRead);
                bytesWritten = true;
            }
        }
        finally
        {
            IOUtil.shutdownStream(inputStream);
            IOUtil.shutdownStream(out);
        }
        return bytesWritten;
    }

    /**
     * Sets the content type, content length and "Content-Disposition" header of the response
     *
     * @param request the HTTP request
     * @param response HTTP response
     * @param file the zip file created
     * @param issueKey the issue key in play
     * @throws java.io.IOException if the stream cant be written to
     */
    private void setResponseHeaders(final HttpServletRequest request, final HttpServletResponse response,
            final File file, final String issueKey) throws IOException
    {
        setFileDownloadHeaders(response,
                file.length(), issueKey + ".zip", "application/zip");
    }

    private void setFileDownloadHeaders(final HttpServletResponse httpServletResponse,
            final long fileSize, final String fileName, final String contentType)
            throws IOException
    {
        httpServletResponse.setContentType(contentType);

        if(fileSize >= 0)
        {
            httpServletResponse.setContentLength((int) fileSize);
        }

        final MimeSniffingKit sniffingKit = ComponentAccessor.getComponent(MimeSniffingKit.class);

        // sets the relevant headers
        sniffingKit.setAttachmentResponseHeaders(fileName, contentType, httpServletResponse);
    }

    /**
     * Checks if the given user had permission to see the attachments for an issue.
     *
     * @param issue the issue in play
     * @return true if user can see the attachments, false otherwise
     * @throws com.atlassian.jira.exception.DataAccessException because of bad JIRA design
     */
    private boolean hasPermissionToViewAttachment(final Issue issue) throws DataAccessException
    {
        return ComponentAccessor.getPermissionManager().hasPermission(Permissions.BROWSE, issue, getLoggedInUser());
    }

    @SuppressWarnings ("ResultOfMethodCallIgnored")
    private void deleteFile(final File zipFile)
    {
        if (zipFile != null)
        {
            zipFile.delete();
        }
    }

    private boolean checkSupportEnabled()
    {
        ApplicationProperties ap = getApplicationProperties();
        return ap.getOption(APKeys.JIRA_OPTION_ALLOWATTACHMENTS) && ap.getOption(APKeys.JIRA_OPTION_ALLOW_ZIP_SUPPORT);
    }

    ApplicationProperties getApplicationProperties()
    {
        return ComponentAccessor.getApplicationProperties();
    }

    protected User getLoggedInUser()
    {
        return ComponentAccessor.getComponent(JiraAuthenticationContext.class).getLoggedInUser();
    }
}
