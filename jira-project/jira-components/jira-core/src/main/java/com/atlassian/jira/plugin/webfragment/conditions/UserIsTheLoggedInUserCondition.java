package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.profile.AbstractUserProfileFragment;

import static com.atlassian.jira.user.ApplicationUsers.from;

/**
 * Used to determine if the logged in user is the same as the profile user in the jira helper.
 *
 * @since v3.12
 */
public class UserIsTheLoggedInUserCondition extends AbstractWebCondition
{
    public static final String PROFILE_USER = AbstractUserProfileFragment.PROFILE_USER;

    public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper)
    {
        User profileUser = (jiraHelper == null) ? null : (User) jiraHelper.getRequest().getAttribute(PROFILE_USER);
        return jiraHelper != null && jiraHelper.getRequest() != null && user != null && user.equals(from(profileUser));
    }
}
