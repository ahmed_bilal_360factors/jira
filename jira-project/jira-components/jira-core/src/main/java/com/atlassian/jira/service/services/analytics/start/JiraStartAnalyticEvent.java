package com.atlassian.jira.service.services.analytics.start;

import java.util.Map;

import com.atlassian.analytics.api.annotations.EventName;

/**
 * Event that will send info
 * to the analytics system about
 * jira configuration
 */
@EventName("jira.start")
public class JiraStartAnalyticEvent
{

    // Public field for all properties that are unknown
    public static final String UNKNOWN = "unknown";

    /**
     * We are going to use a dynamic properties
     * so we can extend it later.
     */
    private final Map<String,Object> properties;

    public JiraStartAnalyticEvent(final Map<String, Object> properties)
    {
        this.properties = properties;
    }

    public Map<String, Object> getProperties()
    {
        return properties;
    }
}
