package com.atlassian.jira.database;

import javax.annotation.Nonnull;

import com.mysema.query.sql.SQLTemplates;

/**
 * Provides access to database connections from the Database Connection Pool.
 *
 * @since 6.4
 */
public interface DbConnectionManager
{
    /**
     * Executes SQL statements as defined in the callback function and returns the results.
     * <p>
     *     This method is mostly useful for running SELECT statements and returning the given results in some form.
     * <p>
     *     Even if OfBiz is currently running in a ThreadLocal transaction, this will retrieve a fresh connection from
     *     the pool.
     *     Do not close the given connection - this manager will return it to the pool after the method is complete.
     *     If the Callback function throws a RuntimeException and the connection is not in auto-commit mode, then this
     *     method will perform a rollback on the connection.
     * <p>
     *     The connection will have the default auto-commit value as defined by the JIRA connection pool.
     *     As at JIRA 6.4 this means autocommit == true.
     *     See {@link org.apache.commons.dbcp.PoolableConnectionFactory#activateObject(Object)} for details.
     *
     * @param callback the callback function that runs the query
     * @param <T> type of results
     * @return results of the callback function
     *
     * @since 6.4
     * @see #execute(SqlCallback)
     */
    <T> T executeQuery(@Nonnull QueryCallback<T> callback);

    /**
     * Executes SQL statements as defined in the callback function.
     * <p>
     *     This method does not return results and is mostly useful for running INSERT, UPDATE, and DELETE operations.
     * <p>
     *     Example Usage:
     * <pre>
     *     dbConnectionManager.execute(new SqlCallback()
     *     {
     *         public void run(final DbConnection dbConnection)
     *         {
     *             dbConnection.update(QIssueLink.ISSUE_LINK)
     *                     .set(QIssueLink.ISSUE_LINK.linktype, issueLinkType.getId())
     *                     .where(QIssueLink.ISSUE_LINK.id.eq(issueLink.getId()))
     *                     .execute();
     *         }
     *     });
     * </pre>
     * <p>
     *     Even if OfBiz is currently running in a ThreadLocal transaction, this will retrieve a fresh connection from
     *     the pool.
     *     Do not close the given connection - this manager will return it to the pool after the method is complete.
     *     If the Callback function throws a RuntimeException and the connection is not in auto-commit mode, then this
     *     method will perform a rollback on the connection.
     * <p>
     *     The connection will have the default auto-commit value as defined by the JIRA connection pool.
     *     As at JIRA 6.4 this means autocommit == true.
     *     See {@link org.apache.commons.dbcp.PoolableConnectionFactory#activateObject(Object)} for details.
     *
     * @param callback the callback function that runs the query
     *
     * @since 6.4
     * @see #executeQuery(QueryCallback)
     */
    void execute(@Nonnull SqlCallback callback);

    /**
     * This will return a DB dialect as appropriate for usage with Querydsl.
     *
     * @return a DB dialect as appropriate for usage with Querydsl.
     *
     * @since 6.4
     */
    @Nonnull
    SQLTemplates getDialect();
}
