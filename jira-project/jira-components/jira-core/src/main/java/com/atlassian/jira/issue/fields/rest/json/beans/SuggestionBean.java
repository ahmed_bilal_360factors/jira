package com.atlassian.jira.issue.fields.rest.json.beans;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Represents possible value (suggestion) for single select field.
 *
 * @since 6.4
 */
@JsonIgnoreProperties (ignoreUnknown = true)
public class SuggestionBean
{
    @JsonProperty
    private String label;

    @JsonProperty
    private String value;

    @JsonProperty
    private String icon;

    @JsonProperty
    private boolean selected;

    public SuggestionBean(final String label, final String value, final String icon, final boolean selected)
    {
        this.label = label;
        this.value = value;
        this.icon = icon;
        this.selected = selected;
    }

    public String getLabel()
    {
        return label;
    }

    public String getValue()
    {
        return value;
    }

    public String getIcon()
    {
        return icon;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(final boolean selected)
    {
        this.selected = selected;
    }
}
