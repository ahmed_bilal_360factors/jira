package com.atlassian.jira.index.request;

import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * DAO for reading reindex requests.
 *
 * @since 6.4
 */
public interface ReindexRequestDao
{
    /**
     * Retrieves all reindex requests with the specified status, ordered by request time.
     *
     * @param status the status.
     *
     * @return a list of requests.
     */
    @Nonnull
    public List<ReindexRequestBase> getRequestsWithStatus(@Nonnull ReindexStatus status);

    /**
     * Retrieves components for a reindex request.
     *
     * @param requestId the ID of the request.
     *
     * @return components for the request.  Returns empty list if the request does not exist.
     */
    @Nonnull
    public List<ReindexComponent> getComponentsForRequest(long requestId);

    @Nonnull
    public ReindexRequestBase writeRequest(@Nonnull ReindexRequestBase request);

    @Nonnull
    public ReindexComponent writeComponent(@Nonnull ReindexComponent component);

    /**
     * Remove all components of a reindex request.
     *
     * @param requestId the reindex request ID.
     */
    public void removeComponents(long requestId);

    /**
     * Finds a request by ID.  Returns null if not found.
     *
     * @param requestId the ID of the request to find.
     *
     * @return the request, or null if not found.
     */
    @Nullable
    public ReindexRequestBase findRequestById(long requestId);

    /**
     * Clear any pending requests.
     */
    void removeAllPendingRequests();
}
