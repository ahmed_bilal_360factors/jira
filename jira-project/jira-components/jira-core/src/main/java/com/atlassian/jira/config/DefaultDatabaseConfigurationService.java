package com.atlassian.jira.config;

import com.atlassian.jira.config.database.DatabaseConfigurationManager;

import javax.annotation.Nullable;

/**
 * Default implementation of {@link DatabaseConfigurationService}.
 *
 * @since 6.4.4
 */
public class DefaultDatabaseConfigurationService implements DatabaseConfigurationService
{
    private final String schemaName;

    public DefaultDatabaseConfigurationService(final DatabaseConfigurationManager databaseConfigurationManager)
    {
        schemaName = databaseConfigurationManager.getDatabaseConfiguration().getSchemaName();
    }

    @Override
    @Nullable
    public String getSchemaName()
    {
        return schemaName;
    }
}
