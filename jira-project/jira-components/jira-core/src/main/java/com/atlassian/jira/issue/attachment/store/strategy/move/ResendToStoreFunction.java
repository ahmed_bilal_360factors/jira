package com.atlassian.jira.issue.attachment.store.strategy.move;

import java.io.InputStream;

import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.StoreAttachmentBean;
import com.atlassian.jira.issue.attachment.StoreAttachmentResult;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.util.concurrent.Effect;
import com.atlassian.util.concurrent.Function;

import com.google.common.util.concurrent.FutureCallback;

/**
 * @since v6.4
 */
@ParametersAreNonnullByDefault
public class ResendToStoreFunction implements Function<AttachmentGetData, Unit>
{
    private final AttachmentKey attachmentKey;
    private final StreamAttachmentStore store;
    private final ResendingAttachmentStreamCreator resendingAttachmentStreamCreator;

    public ResendToStoreFunction(
            final AttachmentKey attachmentKey,
            final StreamAttachmentStore streamAttachmentStore,
            final ResendingAttachmentStreamCreator resendingAttachmentStreamCreator)
    {

        this.attachmentKey = attachmentKey;
        this.store = streamAttachmentStore;
        this.resendingAttachmentStreamCreator = resendingAttachmentStreamCreator;
    }

    @Override
    public Unit get(final AttachmentGetData attachmentGetData)
    {
        final Pair<InputStream, Effect<Object>> streamWithCloseHandler = resendingAttachmentStreamCreator
                .getInputStreamWithCloseHandler(attachmentGetData);
        final Effect<Object> cleanup = streamWithCloseHandler.second();

        try
        {
            final StoreAttachmentBean storeAttachmentBean = new StoreAttachmentBean.Builder(streamWithCloseHandler.first())
                    .withSize(attachmentGetData.getSize())
                    .withKey(attachmentKey)
                    .build();

            store.putAttachment(storeAttachmentBean)
                    .then(getFutureCallbackFromEffect(cleanup));
            return Unit.VALUE;
        }
        catch (final RuntimeException e)
        {
            cleanup.apply(Unit.VALUE);
            throw e;
        }
    }

    private FutureCallback<? super StoreAttachmentResult> getFutureCallbackFromEffect(final Effect<Object> cleanEffect)
    {
        //noinspection NullableProblems
        return new FutureCallback<StoreAttachmentResult>()
        {
            @Override
            public void onSuccess(final StoreAttachmentResult result)
            {
                cleanEffect.apply(result);
            }

            @Override
            public void onFailure(final Throwable t)
            {
                cleanEffect.apply(t);
            }
        };
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final ResendToStoreFunction that = (ResendToStoreFunction) o;

        return attachmentKey.equals(that.attachmentKey)
                && resendingAttachmentStreamCreator.equals(that.resendingAttachmentStreamCreator)
                && store.equals(that.store);
    }

    @Override
    public int hashCode()
    {
        int result = attachmentKey.hashCode();
        result = 31 * result + store.hashCode();
        result = 31 * result + resendingAttachmentStreamCreator.hashCode();
        return result;
    }
}
