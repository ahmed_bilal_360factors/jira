package com.atlassian.jira.web.action.util;

import com.atlassian.jira.bc.dataimport.DataImportService;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.LicenseJohnsonEventRaiser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;

import javax.servlet.ServletContext;

/**
 * An error handler to be used both by {@link com.atlassian.jira.web.action.setup.SetupImport} and
 * {@link ImportAll} for some consistent error handling.
 *
 * @since v4.4
 */
public class DefaultImportResultHandler implements ImportResultHandler
{
    private final JiraLicenseService jiraLicenseService;
    private final LicenseJohnsonEventRaiser licenseJohnsonEventRaiser;

    public DefaultImportResultHandler(final JiraLicenseService jiraLicenseService,
            final LicenseJohnsonEventRaiser licenseJohnsonEventRaiser)
    {
        this.jiraLicenseService = jiraLicenseService;
        this.licenseJohnsonEventRaiser = licenseJohnsonEventRaiser;
    }

    public boolean handleErrorResult(ServletContext context, DataImportService.ImportResult lastResult, I18nHelper i18n, ErrorCollection errorCollection)
    {
        switch (lastResult.getImportError())
        {
            case UPGRADE_EXCEPTION:
                final JohnsonEventContainer eventCont = JohnsonEventContainer.get(context);
                final Event errorEvent = new Event(EventType.get("upgrade"), "An error occurred performing JIRA upgrade task", lastResult.getSpecificErrorMessage(), EventLevel.get(EventLevel.ERROR));
                if (eventCont != null)
                {
                    // Use licenseJohnsonEventRaiser to check for specific license errors; if it's not a Johnson problem report it normally.
                    final LicenseDetails licenseDetails = jiraLicenseService.getLicense();
                    if (!licenseJohnsonEventRaiser.checkLicenseIsTooOldForBuild(context, licenseDetails))
                    {
                        eventCont.addEvent(errorEvent);
                    }
                }
                return true;
            case CUSTOM_PATH_EXCEPTION:
                errorCollection.addErrorMessage(i18n.getText("admin.errors.custom.path", "<a id=\"reimport\" href=\"#\">", "</a>"));
                return false;
            case DOWNGRADE_FROM_ONDEMAND:
                errorCollection.addErrorMessage(i18n.getText("admin.errors.import.downgrade.error", lastResult.getSpecificErrorMessage(), "<a id='acknowledgeDowngradeError' href='#'>", "</a>"));
                return false;
            default:
                return !lastResult.getErrorCollection().hasAnyErrors() && checkLicenseIsInvalidOrTooOldForBuild(context);
        }
    }

    private boolean checkLicenseIsInvalidOrTooOldForBuild(final ServletContext context)
    {
        final LicenseDetails licenseDetails = jiraLicenseService.getLicense();
        return licenseJohnsonEventRaiser.checkLicenseIsTooOldForBuild(context, licenseDetails) ;
    }
}
