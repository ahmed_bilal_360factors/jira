package com.atlassian.jira.index.ha.events;

import javax.annotation.Nonnull;

import com.atlassian.analytics.api.annotations.EventName;

import static com.atlassian.jira.index.ha.DisasterRecoveryLauncher.RecoveryMode;

/**
 * Event fired when a server starts in a recovery disasterRecoveryMode other than PRIMARY
 */
@EventName ("ha.disaster.recovery.started")
public class DisasterRecoveryEvent
{
    /**
     * The {@link RecoveryMode} the server is started in.
     */
    private final String disasterRecoveryMode;

    public DisasterRecoveryEvent(@Nonnull final RecoveryMode mode)
    {
        //the event values pass through a whitelist defined here
        //https://bitbucket.org/atlassian/atlassian-analytics/src/HEAD/analytics-client-lib/src/main/resources/filters/allowedwords.dict?at=master
        //if more a more apt description can be found we should use it
        this.disasterRecoveryMode = (mode.equals(RecoveryMode.COLD)) ? "recovery-mode-standby" : "recovery-mode-second";
    }

    public String getDisasterRecoveryMode()
    {
        return disasterRecoveryMode;
    }
}
