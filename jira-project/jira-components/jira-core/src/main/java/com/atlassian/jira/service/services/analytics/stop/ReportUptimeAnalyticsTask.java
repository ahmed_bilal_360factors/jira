package com.atlassian.jira.service.services.analytics.stop;

import com.atlassian.jira.service.services.analytics.JiraAnalyticTask;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableMap;

import java.lang.management.ManagementFactory;
import java.util.Map;

/**
 * Task to report instance uptime on shutdown
 *
 * @since v6.4
 */
public class ReportUptimeAnalyticsTask implements JiraAnalyticTask
{
    @Override
    public void init()
    {

    }

    @Override
    public Map<String, Object> getAnalytics()
    {
        final ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
        //We can't use SystemInfoUtils here as we do not have ServletContext here
        builder.put("uptime", ManagementFactory.getRuntimeMXBean().getUptime());
        return builder.build();
    }

    @Override
    public boolean isReportingDataShape()
    {
        return false;
    }
}
