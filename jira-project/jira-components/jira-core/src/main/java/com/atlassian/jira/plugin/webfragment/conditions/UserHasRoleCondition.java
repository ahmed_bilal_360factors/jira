package com.atlassian.jira.plugin.webfragment.conditions;

import java.util.Collection;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.plugin.PluginParseException;

public class UserHasRoleCondition extends AbstractJiraCondition {

	public String roleName = "manager";
	
	public void init(Map params) throws PluginParseException {
		String value = (String) params.get("role");
		if (value == null) {
			throw new PluginParseException("Could not determine role for: " + params.get("role"));
		}
		roleName = value;
		super.init(params);
	}
	
	public boolean shouldDisplay(User user, JiraHelper jiraHelper) {
		ProjectRoleManager projectRoleManager = (ProjectRoleManager) ComponentAccessor.getComponentOfType(ProjectRoleManager.class);
		Collection<ProjectRole> userRoles = projectRoleManager.getProjectRoles(user, jiraHelper.getProjectObject());
		for (ProjectRole projectRole : userRoles) {
			if(projectRole.getName().equalsIgnoreCase(roleName)) {
				return true;
			}
		}
		return false;
	}
	
}
