package com.atlassian.jira.plugin.permission;

import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.permission.ProjectPermissionCategory;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.conditions.AlwaysDisplayCondition;

import javax.annotation.Nullable;

import static com.atlassian.fugue.Option.option;

/**
 * Since v6.3.
 */
class DefaultProjectPermission implements ProjectPermission
{
    private final String key;
    private final String nameKey;
    private final String descriptionKey;
    private final ProjectPermissionCategory category;
    private final Condition condition;
    private final ProjectPermissionKey projectPermissionKey;

    DefaultProjectPermission(String key, String nameKey, String descriptionKey,
                             ProjectPermissionCategory category, @Nullable Condition condition)
    {
        this.key = key;
        this.projectPermissionKey = new ProjectPermissionKey(key);
        this.nameKey = nameKey;
        this.descriptionKey = descriptionKey;
        this.category = category;
        this.condition = option(condition).getOrElse(new AlwaysDisplayCondition());
    }

    @Override
    public String getKey()
    {
        return key;
    }

    @Override
    public ProjectPermissionKey getProjectPermissionKey()
    {
        return projectPermissionKey;
    }

    @Override
    public String getNameI18nKey()
    {
        return nameKey;
    }

    @Override
    public String getDescriptionI18nKey()
    {
        return descriptionKey;
    }

    @Override
    public ProjectPermissionCategory getCategory()
    {
        return category;
    }

    @Override
    public Condition getCondition()
    {
        return condition;
    }
}