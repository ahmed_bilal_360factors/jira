package com.atlassian.jira.issue.attachment.store;

import java.io.IOException;

import javax.annotation.Nonnull;

import com.atlassian.jira.util.RuntimeIOException;

/**
 * @since v6.4
 */
public class NotEnoughDataIOException extends RuntimeIOException
{
    public NotEnoughDataIOException(@Nonnull final String message, @Nonnull final  IOException cause)
    {
        super(message, cause);
    }

    public NotEnoughDataIOException(@Nonnull final IOException cause)
    {
        super(cause);
    }

    public NotEnoughDataIOException(@Nonnull final String message)
    {
        super(message);
    }
}
