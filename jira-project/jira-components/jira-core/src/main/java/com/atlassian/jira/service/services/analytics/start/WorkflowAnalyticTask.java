package com.atlassian.jira.service.services.analytics.start;

import java.util.Map;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.service.services.analytics.JiraAnalyticTask;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.workflow.WorkflowManager;

import com.google.common.annotations.VisibleForTesting;

/**
 * Get all the information from workflows.
 */
public class WorkflowAnalyticTask implements JiraAnalyticTask
{

    private WorkflowManager worfklowManager;

    public WorkflowAnalyticTask()
    {
    }

    @VisibleForTesting
    WorkflowAnalyticTask(final WorkflowManager worfklowManager)
    {
        this.worfklowManager = worfklowManager;
    }

    @Override
    public void init()
    {
        worfklowManager = ComponentAccessor.getWorkflowManager();
    }

    @Override
    public Map<String, Object> getAnalytics()
    {
        final MapBuilder<String, Object> builder = MapBuilder.newBuilder();
        builder.add("workflows", worfklowManager.getActiveWorkflows().size());
        return builder.toMap();
    }

    @Override
    public boolean isReportingDataShape()
    {
        return true;
    }
}
