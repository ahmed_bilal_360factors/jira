package com.atlassian.jira.setup;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.fugue.Effect;
import com.atlassian.fugue.Iterables;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.LocaleManager;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.extension.JiraStartedEvent;
import com.atlassian.jira.issue.fields.layout.field.EditableDefaultFieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.renderer.RenderableField;
import com.atlassian.jira.issue.fields.renderer.wiki.AtlassianWikiRenderer;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.startup.DatabaseInitialImporter;
import com.atlassian.jira.upgrade.UpgradeManager;
import com.atlassian.jira.upgrade.UpgradeManagerParams;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.web.action.setup.IndexLanguageToLocaleMapper;
import com.atlassian.jira.web.action.setup.SetupSharedVariables;
import com.atlassian.johnson.event.Event;

import com.google.common.collect.ImmutableMap;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.HttpResponseException;
import org.apache.log4j.Logger;

/**
 * Default implementation of SetupStrategy. It is intended to be used during Instant Setup's SetupFinishing step.
 *
 * @since v6.4
 */
public class InstantSetupStrategy
        implements SetupStrategy<InstantSetupStrategy.SetupParameters, InstantSetupStrategy.Step>
{
    private static final Logger log = Logger.getLogger(InstantSetupStrategy.class);

    public enum Step
    {
        DATABASE,
        PLUGINS,
        ENVIRONMENT,
        FINISHING
    }

    private static final String DEFAULT_GROUP_ADMINS = "jira-administrators";
    private static final String DEFAULT_GROUP_DEVELOPERS = "jira-developers";
    private static final String DEFAULT_GROUP_USERS = "jira-users";

    public static void setupJiraBaseUrl(final String baseUrl)
    {
        final ApplicationProperties applicationProperties = ComponentAccessor.getComponent(ApplicationProperties.class);
        if (applicationProperties.getString(APKeys.JIRA_BASEURL) == null)
        {
            applicationProperties.setString(APKeys.JIRA_BASEURL, baseUrl);
        }
    }

    private void doStepTask(final StepSwitcher<Step> switcher, final InstantSetupStrategy.Step step, final StepTask stepTask) throws Exception
    {
        switcher.withStep(step, stepTask);

        final SetupJohnsonUtil johnsonUtil = ComponentAccessor.getComponent(SetupJohnsonUtil.class);
        final Collection johnsonEvents = johnsonUtil.getEvents();

        if (johnsonEvents.size() > 0)
        {
            throw new RuntimeException(( (Event) johnsonEvents.iterator().next()).getDesc());
        }
    }

    @Override
    public void setup(final SetupParameters setupParameters,
            final StepSwitcher<Step> switcher) throws Exception
    {
        doStepTask(switcher, Step.DATABASE, new StepTask()
        {
            @Override
            public void run() throws Exception
            {
                setupDatabase();
            }
        });
        doStepTask(switcher, Step.PLUGINS, new StepTask()
        {
            @Override
            public void run() throws Exception
            {
                startPlugins();
            }
        });
        doStepTask(switcher, Step.ENVIRONMENT, new StepTask()
        {
            @Override
            public void run() throws Exception
            {
                importInitialData(setupParameters.getJiraLicenseKey(), setupParameters.getServerId());
                setupJiraBaseUrl(setupParameters.getBaseUrl());
            }
        });
        doStepTask(switcher, Step.FINISHING, new StepTask()
        {
            @Override
            public void run() throws Exception
            {
                upgradeJiraAndFinishSetup(setupParameters, switcher);
            }
        });
    }

    @Override
    public ImmutableMap<Step, Status> getInitialSteps()
    {
        return ImmutableMap.of(
                Step.DATABASE, Status.PENDING,
                Step.PLUGINS, Status.AWAITING,
                Step.ENVIRONMENT, Status.AWAITING,
                Step.FINISHING, Status.AWAITING);
    }

    private void setupDatabase()
    {
        final DatabaseConfigurationManager databaseConfigurationManager = ComponentAccessor.getComponent(DatabaseConfigurationManager.class);
        if (!databaseConfigurationManager.isDatabaseSetup())
        {
            final DatabaseConfig hsqlDatabaseConfiguration = databaseConfigurationManager.getInternalDatabaseConfiguration();
            databaseConfigurationManager.setDatabaseConfiguration(hsqlDatabaseConfiguration);
            // now do the post db setup work
            databaseConfigurationManager.activateDatabaseWithoutRunningPostDbSetupRunnables();
        }
    }

    private void startPlugins()
    {
        final DatabaseConfigurationManager databaseConfigurationManager = ComponentAccessor.getComponent(DatabaseConfigurationManager.class);
        databaseConfigurationManager.runPostDbSetupRunnables();
    }

    private void importInitialData(final String jiraLicenseKey, final String serverId)
    {
        final DatabaseInitialImporter databaseInitialImporter = ComponentAccessor.getComponent(DatabaseInitialImporter.class);

        // Load initial data with instant evaluation bits injected
        databaseInitialImporter.importInitialData(null, serverId, jiraLicenseKey);
        // initialize information on instant setup after all data are re-created
        // variables set during setup phase are lost
        final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();
        applicationProperties.setOption(APKeys.JIRA_SETUP_IS_INSTANT, true);
    }


    private void upgradeJiraAndFinishSetup(final SetupParameters setupParameters,
            final StepSwitcher switcher) throws Exception
    {
        final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();
        applicationProperties.setString(APKeys.JIRA_SETUP, "true");

        // SetupComplete.initialiseSystemPropertiesBeforeSetupUpgradeTasks():
        applicationProperties.setOption(APKeys.JIRA_OPTION_USER_EXTERNALMGT, false);
        applicationProperties.setOption(APKeys.JIRA_OPTION_VOTING, true);
        applicationProperties.setOption(APKeys.JIRA_OPTION_WATCHING, true);
        applicationProperties.setOption(APKeys.JIRA_OPTION_ISSUELINKING, true);
        applicationProperties.setString(APKeys.JIRA_OPTION_EMAIL_VISIBLE, "show");

        final UpgradeManager.Status status = ComponentAccessor.getComponent(UpgradeManager.class).doUpgradeIfNeededAndAllowed(null, UpgradeManagerParams.builder().withSetupMode().build());

        if (!status.successful())
        {
            Iterables.first(status.getErrors()).foreach(new Effect<String>()
            {
                @Override
                public void apply(final String error)
                {
                    switcher.setError(error);
                }
            });
            throw new RuntimeException("Upgrade has failed");
        }
        else
        {
            // SetupComplete.initialiseSystemPropertiesAfterSetupUpgradeTasks():
            try
            {
                ComponentAccessor.getSubTaskManager().enableSubTasks();
            }
            catch (final CreateException e)
            {
                log.error("Error encountered when trying to enable sub tasks", e);
                throw new RuntimeException(e);
            }

            //setWikiRendererOnAllRenderableFields();
            final FieldLayoutManager fieldLayoutManager = ComponentAccessor.getFieldLayoutManager();
            final EditableDefaultFieldLayout editableDefaultFieldLayout = fieldLayoutManager.getEditableDefaultFieldLayout();
            final List<FieldLayoutItem> fieldLayoutItems = editableDefaultFieldLayout.getFieldLayoutItems();
            for (final FieldLayoutItem fieldLayoutItem : fieldLayoutItems)
            {
                if (fieldLayoutItem.getOrderableField() instanceof RenderableField)
                {
                    final RenderableField field = (RenderableField) fieldLayoutItem.getOrderableField();
                    if (field.isRenderable())
                    {
                        editableDefaultFieldLayout.setRendererType(fieldLayoutItem, AtlassianWikiRenderer.RENDERER_TYPE);
                    }
                }
            }
            fieldLayoutManager.storeEditableDefaultFieldLayout(editableDefaultFieldLayout);


            // This is here so that al SAL lifeCycleAware components get notified of JIRA
            // being started since this only happens when JIRA is setup.
            ComponentAccessor.getPluginEventManager().broadcast(new JiraStartedEvent());

            setCorrectLocale(setupParameters.getLocale());
            final UserService.CreateUserValidationResult result = createAdministrator(setupParameters.getCredentials());

            if (!result.isValid())
            {
                final String errorMessage;

                if (StringUtils.isEmpty(setupParameters.getCredentials().get("email")))
                {
                    errorMessage = getText("setup.finishing.error.create.administrator.empty.email");
                }
                else if (StringUtils.isEmpty(setupParameters.getCredentials().get("password")))
                {
                    errorMessage = getText("setup.finishing.error.create.administrator.empty.password");
                }
                else
                {
                    errorMessage = getText("setup.finishing.error.create.administrator.generic", setupParameters.getCredentials().get("email"));
                }
                switcher.setError(errorMessage);
                throw new RuntimeException(errorMessage);
            }
            else
            {
                applicationProperties.setString(APKeys.JIRA_SETUP_INSTANT_USER, result.getUsername());
            }

            if (setupParameters.isIsBundleSelected())
            {
                saveBundleLicense(switcher, setupParameters.getLocalBaseUrl(), setupParameters.getCredentials().get("email"), setupParameters.getCredentials().get("password"), setupParameters.getBundleLicenseKey(), setupParameters.getBundleLicenseUrl());
            }
        }
    }

    private void saveBundleLicense(final StepSwitcher switcher, final String baseUrl, final String username, final String password, final String bundleLicenseKey, final String bundleLicenseUrl)
    {

        if (bundleLicenseKey == null)
        {
            switcher.setBundleHasLicenseError(true);
            return;
        }


        if (bundleLicenseUrl == null)
        {
            switcher.setBundleHasLicenseError(true);
            return;
        }

        final HttpClient httpClient = new HttpClient();

        httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(10000);
        httpClient.getParams().setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);

        try
        {
            authenticateUser(httpClient, baseUrl, username, password);
            enableWebSudo(httpClient, baseUrl);
            installLicense(httpClient, bundleLicenseKey, bundleLicenseUrl);
        }
        catch (HttpResponseException x)
        {
            switcher.setBundleHasLicenseError(true);
            log.error("Cannot setup product license, http status code: " + x.getStatusCode(), x);
        }
        catch (IOException x)
        {
            switcher.setBundleHasLicenseError(true);
            log.error("Cannot setup product license", x);
        }
        finally
        {
            destroySession(httpClient, baseUrl);
        }
    }

    private static void authenticateUser(final HttpClient httpClient, final String baseUrl, final String username, final String password)
            throws IOException
    {
        final PostMethod method = new PostMethod(baseUrl + "/rest/auth/1/session");

        try
        {
            final StringRequestEntity requestEntity = new StringRequestEntity(
                    prepareJSON(ImmutableMap.of("username", username, "password", password)),
                    "application/json", "UTF-8");

            method.setRequestEntity(requestEntity);

            final int status = httpClient.executeMethod(method);
            if (status != 200)
            {
                throw new HttpResponseException(status, "Problem authenticating user during product bundle license installation");
            }
        }
        finally
        {
            method.releaseConnection();
        }
    }

    private static void enableWebSudo(final HttpClient httpClient, final String baseUrl) throws IOException
    {
        final SetupSharedVariables setupSharedVariables = ComponentAccessor.getComponent(SetupSharedVariables.class);
        final PostMethod method = new PostMethod(baseUrl + "/secure/SetupAdminAccount!enableWebSudo.jspa");

        final String token = DigestUtils.sha1Hex(String.valueOf(System.currentTimeMillis() + String.valueOf(Math.random())));
        setupSharedVariables.setWebSudoToken(token);
        try
        {
            method.setParameter("webSudoToken", token);

            final int status = httpClient.executeMethod(method);
            if (status != 200)
            {
                throw new HttpResponseException(status, "Problem establishing websudo session.");
            }
        }
        finally
        {
            method.releaseConnection();
            setupSharedVariables.setWebSudoToken(null);
        }
    }

    private void installLicense(final HttpClient httpClient, final String bundleLicenseKey, final String bundleLicenseUrl)
            throws IOException
    {
        final PutMethod method = new PutMethod(bundleLicenseUrl);

        try
        {
            final StringRequestEntity requestEntity = new StringRequestEntity(
                    prepareJSON(ImmutableMap.of("rawLicense", bundleLicenseKey)),
                    "application/vnd.atl.plugins+json", "UTF-8");

            method.setRequestEntity(requestEntity);
            final int status = httpClient.executeMethod(method);
            if (status != 200)
            {
                throw new HttpResponseException(status, "Problem installing license.");
            }
        }
        finally
        {
            method.releaseConnection();
        }
    }

    private static void destroySession(final HttpClient httpClient, final String baseUrl)
    {
        final DeleteMethod method = new DeleteMethod(baseUrl + "/rest/auth/1/session");
        method.setRequestHeader("Content-Type", "application/json");

        try
        {
            httpClient.executeMethod(method);
        }
        catch (final IOException e)
        {
            log.warn("Problem with destroying session during product bundle license installation", e);
        }
        finally
        {
            method.releaseConnection();
        }
    }

    private static String prepareJSON(final Map<String, String> params)
    {
        final JSONObject jsonObject = new JSONObject();

        for (final Map.Entry<String, String> entry : params.entrySet())
        {
            try
            {
                jsonObject.put(entry.getKey(), entry.getValue());
            }
            catch (final JSONException e)
            {
                log.warn("Problem preparing json", e);
            }
        }

        return jsonObject.toString();
    }

    private static UserService.CreateUserValidationResult createAdministrator(final Map<String, String> credentials)
    {
        final User administrator;

        final String email = credentials.get("email");
        final String username = credentials.get("email");
        final String fullname = credentials.get("email");
        final String password = credentials.get("password");
        final UserService userService = ComponentAccessor.getComponent(UserService.class);

        final UserService.CreateUserValidationResult result = userService.validateCreateUserForSetup(
                null, username, password, password, email, fullname);

        if (result.isValid())
        {
            try
            {
                administrator = userService.createUserNoNotification(result);
            }
            catch (final PermissionException e)
            {
                throw new RuntimeException(e);
            }
            catch (final CreateException e)
            {
                throw new RuntimeException(e);
            }

            final GroupManager groupManager = ComponentAccessor.getGroupManager();

            final Group groupAdmins = getOrCreateGroup(groupManager, DEFAULT_GROUP_ADMINS);
            final Group groupDevelopers = getOrCreateGroup(groupManager, DEFAULT_GROUP_DEVELOPERS);
            final Group groupUsers = getOrCreateGroup(groupManager, DEFAULT_GROUP_USERS);

            if ((administrator != null) && (groupAdmins != null) && (groupDevelopers != null) && (groupUsers != null))
            {
                try
                {
                    if (!groupManager.isUserInGroup(administrator.getName(), groupAdmins.getName()))
                    {
                        groupManager.addUserToGroup(administrator, groupAdmins);
                    }
                    if (!groupManager.isUserInGroup(administrator.getName(), groupDevelopers.getName()))
                    {
                        groupManager.addUserToGroup(administrator, groupDevelopers);
                    }
                    if (!groupManager.isUserInGroup(administrator.getName(), groupUsers.getName()))
                    {
                        groupManager.addUserToGroup(administrator, groupUsers);
                    }
                }
                catch (final GroupNotFoundException e)
                {
                    throw new RuntimeException(e);
                }
                catch (final UserNotFoundException e)
                {
                    throw new RuntimeException(e);
                }
                catch (final OperationNotPermittedException e)
                {
                    throw new RuntimeException(e);
                }
                catch (final OperationFailedException e)
                {
                    throw new RuntimeException(e);
                }

                // Make sure to enable admin users to change licenses during install (in case the license used is too
                // old for the JIRA version)
                final GlobalPermissionManager globalPermissionManager = ComponentAccessor.getGlobalPermissionManager();
                if (!globalPermissionManager.getGroupNames(Permissions.ADMINISTER).contains(DEFAULT_GROUP_ADMINS))
                {
                    globalPermissionManager.addPermission(Permissions.ADMINISTER, DEFAULT_GROUP_ADMINS);
                }
            }

        }

        return result;
    }

    /**
     * try and get or create a group, if we get a problem let the user know
     *
     * @param groupName the name of the group to get or create
     * @return a Group if one is found or can be created, null otherwise
     */
    private static Group getOrCreateGroup(final GroupManager groupManager, final String groupName)
    {
        final Group group = groupManager.getGroup(groupName);
        if (group != null)
        {
            return group;
        }
        try
        {
            return groupManager.createGroup(groupName);
        }
        catch (final OperationNotPermittedException e)
        {
            throw new RuntimeException(e);
        }
        catch (final InvalidGroupException e)
        {
            throw new RuntimeException(e);
        }
    }

    private static void setCorrectLocale(final String locale)
    {
        setJiraLocale(locale);
        setIndexingLanguageForDefaultServerLocale(locale);
    }

    private static void setJiraLocale(final String locale)
    {
        // Always get a fresh copy of the application properties because during the short life of some actions
        // (eg. SetupDatabase) the Pico container can be swapped out from underneath us.
        final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();

        if (isLocaleValid(locale))
        {
            applicationProperties.setString(APKeys.JIRA_I18N_DEFAULT_LOCALE, locale);
        }
    }

    private static void setIndexingLanguageForDefaultServerLocale(final String locale)
    {
        // Always get a fresh copy of the application properties because during the short life of some actions
        // (eg. SetupDatabase) the Pico container can be swapped out from underneath us.
        final ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();
        final IndexLanguageToLocaleMapper languageToLocaleMapper = ComponentAccessor.getComponent(IndexLanguageToLocaleMapper.class);

        applicationProperties.setString(APKeys.JIRA_I18N_LANGUAGE_INPUT, languageToLocaleMapper.getLanguageForLocale(locale));
    }

    private static boolean isLocaleValid(final String locale)
    {
        final LocaleManager localeManager = ComponentAccessor.getComponentOfType(LocaleManager.class);
        final Set<Locale> installedLocales = localeManager.getInstalledLocales();

        return installedLocales.contains(localeManager.getLocale(locale));
    }


    private static String getText(final String key)
    {
        return getI18nHelper().getText(key);
    }

    private static String getText(final String key, final String object)
    {
        return getI18nHelper().getText(key, object);
    }

    private static I18nHelper getI18nHelper()
    {
        return ComponentAccessor.getComponent(JiraAuthenticationContext.class).getI18nHelper();
    }

    public static final class SetupParameters
    {
        private final String jiraLicenseKey;
        private final String baseUrl;
        private final String localBaseUrl;
        private final String serverId;
        private final Map<String, String> credentials;
        private final String locale;
        private final boolean isBundleSelected;
        private final String bundleLicenseKey;
        private final String bundleLicenseUrl;

        private SetupParameters(final String jiraLicenseKey, final String baseUrl, final String localBaseUrl, final String serverId, final Map<String, String> credentials, final String locale, final boolean isBundleSelected, final String bundleLicenseKey, final String bundleLicenseUrl)
        {
            this.jiraLicenseKey = jiraLicenseKey;
            this.baseUrl = baseUrl;
            this.localBaseUrl = localBaseUrl;
            this.serverId = serverId;
            this.credentials = credentials;
            this.locale = locale;
            this.isBundleSelected = isBundleSelected;
            this.bundleLicenseKey = bundleLicenseKey;
            this.bundleLicenseUrl = bundleLicenseUrl;
        }

        public static SetupParametersBuilder builder()
        {
            return new SetupParametersBuilder();
        }

        public String getJiraLicenseKey()
        {
            return jiraLicenseKey;
        }

        public String getBaseUrl()
        {
            return baseUrl;
        }

        public String getLocalBaseUrl()
        {
            return localBaseUrl;
        }

        public String getServerId()
        {
            return serverId;
        }

        public Map<String, String> getCredentials()
        {
            return credentials;
        }

        public String getLocale()
        {
            return locale;
        }

        public boolean isIsBundleSelected()
        {
            return isBundleSelected;
        }

        public String getBundleLicenseKey()
        {
            return bundleLicenseKey;
        }

        public String getBundleLicenseUrl()
        {
            return bundleLicenseUrl;
        }

        public static class SetupParametersBuilder
        {
            private String jiraLicenseKey;
            private String baseUrl;
            private String localBaseUrl;
            private String serverId;
            private Map<String, String> credentials;
            private String locale;
            private boolean isBundleSelected;
            private String bundleLicenseKey;
            private String bundleLicenseUrl;

            private SetupParametersBuilder()
            {
            }

            public SetupParametersBuilder setJiraLicenseKey(final String jiraLicenseKey)
            {
                this.jiraLicenseKey = jiraLicenseKey;
                return this;
            }

            public SetupParametersBuilder setBaseUrl(final String baseUrl)
            {
                this.baseUrl = baseUrl;
                return this;
            }

            public SetupParametersBuilder setLocalBaseUrl(final String localBaseUrl)
            {
                this.localBaseUrl = localBaseUrl;
                return this;
            }

            public SetupParametersBuilder setServerId(final String serverId)
            {
                this.serverId = serverId;
                return this;
            }

            public SetupParametersBuilder setCredentials(final Map<String, String> credentials)
            {
                this.credentials = credentials;
                return this;
            }

            public SetupParametersBuilder setLocale(final String locale)
            {
                this.locale = locale;
                return this;
            }

            public SetupParametersBuilder setIsBundleSelected(final boolean isBundleSelected)
            {
                this.isBundleSelected = isBundleSelected;
                return this;
            }

            public SetupParametersBuilder setBundleLicenseKey(final String bundleLicenseKey)
            {
                this.bundleLicenseKey = bundleLicenseKey;
                return this;
            }

            public SetupParametersBuilder setBundleLicenseUrl(final String bundleLicenseUrl)
            {
                this.bundleLicenseUrl = bundleLicenseUrl;
                return this;
            }

            public SetupParameters build()
            {
                return new SetupParameters(jiraLicenseKey, baseUrl, localBaseUrl, serverId, credentials, locale, isBundleSelected, bundleLicenseKey, bundleLicenseUrl);
            }
        }
    }
}
