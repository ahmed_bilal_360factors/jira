package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.entity.Update;
import com.atlassian.jira.upgrade.AbstractDelayableUpgradeTask;

/**
 * Marks upgrade tasks that have entries in the UpgradeHistory with a (null) status as done.
 *
 * Any entry prior to this task running that has a null entry was done. The "Status" field did not exist prior to this version.
 *
 * @since v6.4
 */
public class UpgradeTask_Build64008 extends AbstractDelayableUpgradeTask
{
    static final String ENTITY = "UpgradeHistory";
    private static final String ID = "id";
    private static final String STATUS = "status";
    private static final String STATUS_COMPLETE = "complete";

    public UpgradeTask_Build64008()
    {
        super();
    }

    @Override
    public String getBuildNumber()
    {
        return "64008";
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception
    {
        if (setupMode)
        {
            // There is no need to do this on a clean install
            return;
        }

        Update.into(ENTITY)
                .set(STATUS, STATUS_COMPLETE)
                .whereEqual(STATUS, (String) null)
                .execute(getEntityEngine());
    }

    @Override
    public String getShortDescription()
    {
        return "Setting upgrade status on completed upgrades.";
    }
}
