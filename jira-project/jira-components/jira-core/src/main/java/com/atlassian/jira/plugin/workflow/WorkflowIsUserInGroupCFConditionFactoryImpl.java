package com.atlassian.jira.plugin.workflow;

import java.util.List;
import java.util.Map;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.security.util.GroupSelectorUtils;

import com.google.common.collect.Maps;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ConditionDescriptor;

/**
 * Factory for editing plugins that require a group selector custom field, eg.
 * {@link com.atlassian.jira.workflow.condition.InGroupCFCondition}
 */
public class WorkflowIsUserInGroupCFConditionFactoryImpl extends AbstractWorkflowPluginFactory implements WorkflowPluginConditionFactory
{
    private GroupSelectorUtils groupSelectorUtils;

    public WorkflowIsUserInGroupCFConditionFactoryImpl(GroupSelectorUtils groupSelectorUtils)
    {
        this.groupSelectorUtils = groupSelectorUtils;
    }
    protected void getVelocityParamsForInput(Map<String,Object> velocityParams)
    {
        List<Field> fields = groupSelectorUtils.getCustomFieldsSpecifyingGroups();
        Map<String,String> groupcf = Maps.newHashMapWithExpectedSize(fields.size());
        for (Field customField : fields)
        {
            groupcf.put(customField.getId(), customField.getName());
        }
        velocityParams.put("groupcfs", groupcf);
    }

    protected void getVelocityParamsForEdit(Map<String,Object> velocityParams, AbstractDescriptor descriptor)
    {
        getVelocityParamsForInput(velocityParams);
        getVelocityParamsForView(velocityParams, descriptor);
    }

    protected void getVelocityParamsForView(Map<String,Object> velocityParams, AbstractDescriptor descriptor)
    {
        if (!(descriptor instanceof ConditionDescriptor))
        {
            throw new IllegalArgumentException("Descriptor must be a ConditionDescriptor.");
        }

        ConditionDescriptor conditionDescriptor = (ConditionDescriptor) descriptor;

        FieldManager fieldManager = ComponentAccessor.getFieldManager();
        String fieldId = (String) conditionDescriptor.getArgs().get("groupcf");
        Field field = fieldManager.getField(fieldId);
        velocityParams.put("groupcf", (field == null ? fieldId : field.getName()));
    }

    public Map<String,Object> getDescriptorParams(Map<String,Object> conditionParams)
    {
        // Process The map
        final String value = extractSingleParam(conditionParams, "groupcf");
        return FieldMap.build("groupcf", value);
    }
}
