package com.atlassian.jira.onboarding;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserPropertyManager;

import com.opensymphony.module.propertyset.PropertySet;

public class OnboardingStoreImpl implements OnboardingStore
{
    private final UserPropertyManager userPropertyManager;

    public OnboardingStoreImpl(final UserPropertyManager userPropertyManager)
    {
        this.userPropertyManager = userPropertyManager;
    }

    @Override
    public boolean isSet(final ApplicationUser user, final String key)
    {
        final PropertySet propertySet = userPropertyManager.getPropertySet(user);
        return propertySet.exists(key);
    }

    @Override
    public void setBoolean(final ApplicationUser user, final String key, final boolean value)
    {
        final PropertySet propertySet = userPropertyManager.getPropertySet(user);
        propertySet.setBoolean(key, value);
    }

    @Override
    public boolean getBoolean(final ApplicationUser user, final String key)
    {
        if (isSet(user, key))
        {
            final PropertySet propertySet = userPropertyManager.getPropertySet(user);
            return propertySet.getBoolean(key);
        }
        return false;
    }

    @Override
    public void setString(final ApplicationUser user, final String key, final String value)
    {
        final PropertySet propertySet = userPropertyManager.getPropertySet(user);
        propertySet.setString(key, value);
    }

    @Override
    public String getString(final ApplicationUser user, final String key)
    {
        if (isSet(user, key))
        {
            final PropertySet propertySet = userPropertyManager.getPropertySet(user);
            return propertySet.getString(key);
        }
        return null;
    }
}
