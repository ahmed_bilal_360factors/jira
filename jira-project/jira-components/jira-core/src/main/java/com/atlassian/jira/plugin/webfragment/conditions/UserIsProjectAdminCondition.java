package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.base.Supplier;

import javax.annotation.Nullable;

import static com.atlassian.jira.plugin.webfragment.conditions.AbstractPermissionCondition.getHasProjectsKey;
import static com.atlassian.jira.plugin.webfragment.conditions.RequestCachingConditionHelper.cacheConditionResultInRequest;

/**
 * Checks that the current user is a project admin for at least one project.
 */
public class UserIsProjectAdminCondition extends AbstractWebCondition
{
    private final PermissionManager permissionManager;

    public UserIsProjectAdminCondition(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public boolean shouldDisplay(@Nullable final ApplicationUser user, @Nullable JiraHelper jiraHelper)
    {
        return cacheConditionResultInRequest(getHasProjectsKey(Permissions.PROJECT_ADMIN, user), new Supplier<Boolean>()
        {
            @Override
            public Boolean get()
            {
                return permissionManager.hasProjects(Permissions.PROJECT_ADMIN, user);
            }
        });
    }
}
