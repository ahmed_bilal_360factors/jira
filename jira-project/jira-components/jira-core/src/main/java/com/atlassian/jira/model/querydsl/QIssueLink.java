package com.atlassian.jira.model.querydsl;

import com.mysema.query.sql.ColumnMetadata;
import com.mysema.query.types.path.*;

import java.sql.Types;
import javax.annotation.Generated;

/**
 * QIssueLink is a Querydsl query object.
 *
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QIssueLink extends JiraRelationalPathBase<IssueLinkDTO>
{
    public static final QIssueLink ISSUE_LINK = new QIssueLink("ISSUE_LINK");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final NumberPath<Long> linktype = createNumber("linktype", Long.class);
    public final NumberPath<Long> source = createNumber("source", Long.class);
    public final NumberPath<Long> destination = createNumber("destination", Long.class);
    public final NumberPath<Long> sequence = createNumber("sequence", Long.class);

    public QIssueLink(String alias)
    {
        super(IssueLinkDTO.class, alias, "issuelink");
        addMetadata();
    }

    private void addMetadata()
    {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(linktype, ColumnMetadata.named("linktype").withIndex(2).ofType(Types.NUMERIC).withSize(18));
        addMetadata(source, ColumnMetadata.named("source").withIndex(3).ofType(Types.NUMERIC).withSize(18));
        addMetadata(destination, ColumnMetadata.named("destination").withIndex(4).ofType(Types.NUMERIC).withSize(18));
        addMetadata(sequence, ColumnMetadata.named("sequence").withIndex(5).ofType(Types.NUMERIC).withSize(18));
    }
}

