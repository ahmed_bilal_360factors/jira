/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

package com.atlassian.jira.project.version;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.NamedPredicates;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.ofbiz.core.entity.GenericValue;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

public class OfBizVersionStore implements VersionStore
{
    private final OfBizDelegator delegator;
    private final ProjectManager projectManager;

    public OfBizVersionStore(OfBizDelegator delegator, ProjectManager projectManager)
    {
        this.delegator = delegator;
        this.projectManager = projectManager;
    }

    @Nonnull
    @Override
    public Iterable<Version> getAllVersions()
    {
        final List<GenericValue> versionGvs = delegator.findAll("Version", ImmutableList.of("sequence"));
        return versionGvs != null ? Iterables.transform(versionGvs, toVersion()) : Collections.<Version>emptyList();
    }

    protected Function<GenericValue, Version> toVersion()
    {
        return new Function<GenericValue, Version>()
        {
            @Override
            public Version apply(@Nullable final GenericValue genericValue)
            {
                return new VersionImpl(projectManager, notNull(genericValue));
            }
        };
    }

    @Nonnull
    @Override
    public Iterable<Version> getVersionsByName(@Nonnull final String name)
    {
        return Iterables.filter(getAllVersions(), NamedPredicates.equalsIgnoreCase(name));
    }

    @Nonnull
    @Override
    public Iterable<Version> getVersionsByProject(@Nonnull Long projectId)
    {
        final List<GenericValue> versionGvs = delegator.findByAnd("Version", FieldMap.build("project", projectId), ImmutableList.of("sequence"));
        return versionGvs != null ? Iterables.transform(versionGvs, toVersion()) : Collections.<Version>emptyList();
    }

    @Nonnull
    @Override
    public Version createVersion(@Nonnull Map<String, Object> versionParams)
    {
        return new VersionImpl(projectManager, delegator.createValue(OfBizDelegator.VERSION, versionParams));
    }

    public void storeVersion(@Nonnull Version version)
    {
        delegator.store(version.getGenericValue());
    }

    public void storeVersions(@Nonnull final Collection<Version> versions)
    {
        for (Version version : versions)
        {
            if (version != null)
            {
                storeVersion(version);
            }
        }
    }

    public Version getVersion(@Nonnull Long id)
    {
        GenericValue versionGv = delegator.findById("Version", id);
        return versionGv != null ? new VersionImpl(projectManager, versionGv) : null;
    }

    public void deleteVersion(@Nonnull Version version)
    {
        delegator.removeValue(notNull(version).getGenericValue());
    }

}
