package com.atlassian.jira.license;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.cache.Supplier;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.event.ClearCacheEvent;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.Nonnull;

import static com.atlassian.jira.entity.Delete.from;
import static com.atlassian.jira.entity.Entity.PRODUCT_LICENSE;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.any;
import static org.apache.commons.lang.StringUtils.trimToNull;


/**
 * @since v6.3
 */
@SuppressWarnings("deprecation")
@EventComponent
public class MultiLicenseStoreImpl implements MultiLicenseStore
{
    public static final String ID_COLUMN = "id";
    private final EntityEngine entityEngine;
    private final JiraLicenseStore jiraLicenseStore;
    private final FeatureManager featureManager;
    private final CachedReference<ImmutableList<String>> cachedLicenses;

    public MultiLicenseStoreImpl(EntityEngine entityEngine, JiraLicenseStore jiraLicenseStore,
            FeatureManager featureManager, CacheManager cacheManager)
    {
        this.entityEngine = entityEngine;
        this.jiraLicenseStore = jiraLicenseStore;
        this.featureManager = featureManager;
        this.cachedLicenses = cacheManager.getCachedReference("MultiLicenseStoreImpl.cachedLicenses", new LicenseSupplier(entityEngine));
    }

    @Override
    @Nonnull
    public Iterable<String> retrieve()
    {
        if (licenseRolesAreEnabled())
        {
            List<String> multiStoredStrings = cachedLicenses.get();
            if (!multiStoredStrings.isEmpty())
            {
                return multiStoredStrings;
            }

            String fallback = jiraLicenseStore.retrieve();
            if (fallback != null)
            {
                return ImmutableList.of(fallback);
            }

        }
        else
        { // prefer jiraLicenseStore to multiStore because that is more likely to be populated.
            String fallback = jiraLicenseStore.retrieve();
            if (fallback != null)
            {
                return ImmutableList.of(fallback);
            }
            List<String> multiStoredStrings = cachedLicenses.get();
            if (!multiStoredStrings.isEmpty())
            {
                return multiStoredStrings;
            }
        }

        return ImmutableList.of();
    }

    private boolean licenseRolesAreEnabled()
    {
        return featureManager.isEnabled(CoreFeatures.LICENSE_ROLES_ENABLED);
    }

    @Override
    public void store(@Nonnull final Iterable<String> newLicenseKeys)
    {
        checkNotNull(newLicenseKeys, "newLicenseKeys");
        if (licenseRolesAreEnabled())
        {
            if (Iterables.isEmpty(newLicenseKeys))
            {
                throw new IllegalArgumentException("You must store at least one license.");
            }

            if (any(newLicenseKeys, Predicates.isNull()))
            {
                throw new IllegalArgumentException("You cannot store null licenses - no changes have been made to licenses.");
            }

            try
            {
                entityEngine.delete(from(PRODUCT_LICENSE).all());

                for (String licenseKey : newLicenseKeys)
                {
                    entityEngine.createValue(PRODUCT_LICENSE, new ProductLicense(licenseKey));
                }

                // this is where the old license is stored - clean it up if it's there.
                String singleLicense = trimToNull(jiraLicenseStore.retrieve());
                if (singleLicense != null)
                {
                    jiraLicenseStore.remove();
                }
            }
            finally
            {
                clearCache();
            }
        }
        else
        {
            Iterator<String> iterator = newLicenseKeys.iterator();
            if (iterator.hasNext())
            {
                jiraLicenseStore.store(iterator.next());
            }
            else
            {
                jiraLicenseStore.store(null); // this throws an exception
            }
        }
    }

    @EventListener
    public void onClearCacheEvent(ClearCacheEvent c)
    {
        clearCache();
    }

    private void clearCache()
    {
        cachedLicenses.reset();
    }

    @Override
    public String retrieveServerId()
    {
        return jiraLicenseStore.retrieveServerId();
    }

    @Override
    public void storeServerId(String serverId)
    {
        jiraLicenseStore.storeServerId(serverId);
    }

    @Override
    public void resetOldBuildConfirmation()
    {
        jiraLicenseStore.resetOldBuildConfirmation();
    }

    @Override
    public void confirmProceedUnderEvaluationTerms(String userName)
    {
        jiraLicenseStore.confirmProceedUnderEvaluationTerms(userName);
    }

    @VisibleForTesting
    static class LicenseSupplier implements Supplier<ImmutableList<String>>
    {
        final EntityEngine entityEngine;

        @VisibleForTesting
        LicenseSupplier(EntityEngine entityEngine)
        {
            this.entityEngine = entityEngine;
        }

        @Override
        public ImmutableList<String> get()
        {
            List<ProductLicense> list = entityEngine.selectFrom(PRODUCT_LICENSE).findAll().orderBy(ID_COLUMN);
            return copyOf(extractStrings(list));
        }

        private List<String> extractStrings(List<ProductLicense> productLicenses)
        {
            List<String> result = new ArrayList<String>(productLicenses.size());

            for (ProductLicense productLicense : productLicenses)
            {
                if (productLicense != null)
                {
                    result.add(productLicense.getLicenseKey());
                }
            }

            return result;
        }

    }
}
