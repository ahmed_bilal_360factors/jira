package com.atlassian.jira.web.action.admin.customfields;

import java.util.LinkedHashMap;
import java.util.Map;

import com.atlassian.jira.component.ComponentAccessor;

public class CustomFieldContextManagementUtil
{
    public static Map<String, String> getGlobalContextOption()
    {
        // Always recreate this since the locale may change
        Map<String, String> globalContextOptions = new LinkedHashMap<String, String>();
        globalContextOptions.put("true", ComponentAccessor.getJiraAuthenticationContext().getI18nHelper().getText("admin.global.context.option.global", "<strong>", "</strong>"));
        globalContextOptions.put("false", ComponentAccessor.getJiraAuthenticationContext().getI18nHelper().getText("admin.global.context.option.project"));
        return globalContextOptions;
    }
}
