package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Condition to determine whether an issue is a subtask
 * <p/>
 * An issue must be in the JiraHelper context params.
 *
 * @since v4.1
 */
public class IsSubTaskCondition extends AbstractIssueWebCondition
{
    private static final Logger log = LoggerFactory.getLogger(IsSubTaskCondition.class);

    public boolean shouldDisplay(ApplicationUser user, Issue issue, JiraHelper jiraHelper)
    {
        return issue.isSubTask();
    }

}
