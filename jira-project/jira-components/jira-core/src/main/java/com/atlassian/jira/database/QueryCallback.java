package com.atlassian.jira.database;

/**
 * A callback for DB operations that gets given a managed database connection and returns results.
 * This is useful for running SELECT statements and returning results.
 *
 * @see com.atlassian.jira.database.DbConnectionManager#executeQuery(QueryCallback)
 * @since v6.4
 */
public interface QueryCallback<T>
{
    T runQuery(DbConnection dbConnection);
}
