package com.atlassian.jira.index.request;

import java.util.Map;

import com.atlassian.jira.entity.AbstractEntityFactory;
import com.atlassian.jira.ofbiz.FieldMap;

import org.ofbiz.core.entity.GenericValue;

/**
 * @since 6.4
 */
public class ReindexComponentFactory extends AbstractEntityFactory<ReindexComponent>
{
    @Override
    public Map<String, Object> fieldMapFrom(ReindexComponent value)
    {
        return FieldMap.build(ReindexComponent.ID, value.getId())
                .add(ReindexComponent.REQUEST_ID, value.getRequestId())
                .add(ReindexComponent.AFFECTED_INDEX, value.getAffectedIndex().toString())
                .add(ReindexComponent.ENTITY_TYPE, value.getEntityType().toString());
    }

    @Override
    public String getEntityName()
    {
        return "ReindexComponent";
    }

    @Override
    public ReindexComponent build(GenericValue genericValue)
    {
        return new ReindexComponent(genericValue.getLong(ReindexComponent.ID),
                                    genericValue.getLong(ReindexComponent.REQUEST_ID),
                                    AffectedIndex.valueOf(genericValue.getString(ReindexComponent.AFFECTED_INDEX)),
                                    SharedEntityType.valueOf(genericValue.getString(ReindexComponent.ENTITY_TYPE)));
    }
}
