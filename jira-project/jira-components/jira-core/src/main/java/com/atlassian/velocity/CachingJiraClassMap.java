package com.atlassian.velocity;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.atlassian.fugue.Option;

import com.google.common.annotations.VisibleForTesting;

import org.apache.velocity.util.introspection.ClassMap;
import org.apache.velocity.util.introspection.MethodMap;

/**
 * Caches all calls to {@link org.apache.velocity.util.introspection.ClassMap#findMethod(String, Object[])}
 *
 * @since v6.4
 */
public class CachingJiraClassMap implements ClassMap
{
    private final ConcurrentMap<MethodCacheEntry, Option<Method>> methodCache = new ConcurrentHashMap<MethodCacheEntry, Option<Method>>();
    private final JiraClassMap delegate;

    public CachingJiraClassMap(final JiraClassMap delegate)
    {
        this.delegate = delegate;
    }

    @Override
    public Method findMethod(final String name, final Object[] params) throws MethodMap.AmbiguousException
    {
        final MethodCacheEntry cacheKey = getCacheKey(name, params);
        final Option<Method> cachedMethod = methodCache.get(cacheKey);
        if (cachedMethod != null)
        {
            return cachedMethod.getOrNull();
        }

        final Method resolvedMethod = delegate.findMethod(name, params);
        final Option<Method> prevValue = methodCache.putIfAbsent(cacheKey, Option.option(resolvedMethod));
        return prevValue != null ? prevValue.getOrNull() : resolvedMethod;
    }

    private MethodCacheEntry getCacheKey(final String name, final Object[] params)
    {
        return new MethodCacheEntry(name, params);
    }

    @VisibleForTesting
    static class MethodCacheEntry
    {
        private final String methodName;
        private final Class<?>[] paramsTypes;

        private MethodCacheEntry(final String methodName, final Object[] params)
        {
            this.methodName = methodName;
            this.paramsTypes = VelocityTypesUtil.getParametersTypes(params);
        }

        @Override
        public boolean equals(final Object o)
        {
            if (this == o) { return true; }
            if (o == null || getClass() != o.getClass()) { return false; }

            final MethodCacheEntry that = (MethodCacheEntry) o;

            if (methodName != null ? !methodName.equals(that.methodName) : that.methodName != null) { return false; }

            return Arrays.equals(paramsTypes, that.paramsTypes);
        }

        @Override
        public int hashCode()
        {
            int result = methodName != null ? methodName.hashCode() : 0;
            result = 31 * result + Arrays.hashCode(paramsTypes);
            return result;
        }
    }
}
