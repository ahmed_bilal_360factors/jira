package com.atlassian.jira.security.util;

import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeEntity;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: owenfellows
 * Date: 02-Aug-2004
 * Time: 14:46:41
 * To change this template use File | Settings | File Templates.
 */
public class GroupToNotificationSchemeMapper extends AbstractGroupMapper
{
    public static final String GROUP_DROPDOWN = "Group_Dropdown";
    private final NotificationSchemeManager notificationSchemeManager;

    public GroupToNotificationSchemeMapper(NotificationSchemeManager notificationSchemeManager)
    {
        this.notificationSchemeManager = notificationSchemeManager;
        setGroupMapping(init());
    }

    /**
     * Go through all the Notification Schemes and create a Map, where the key is the group name
     * and values are Sets of Schemes
     */
    private Map<String, Set<Scheme>> init()
    {
        Map<String, Set<Scheme>> mapping = new HashMap<String, Set<Scheme>>();

        // Get all Permission Schmes
        final List<Scheme> schemes = notificationSchemeManager.getSchemeObjects();
        for (Scheme notificationScheme : schemes)
        {
            final Collection<SchemeEntity> entities = notificationScheme.getEntities();
            for (SchemeEntity notificationRecord : entities)
            {
                if (GROUP_DROPDOWN.equals(notificationRecord.getType()))
                {
                    addEntry(mapping, notificationRecord.getParameter(), notificationScheme);
                }
            }
        }

        return mapping;
    }
}
