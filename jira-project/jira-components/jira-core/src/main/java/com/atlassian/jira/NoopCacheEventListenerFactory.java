package com.atlassian.jira;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.event.CacheEventListener;
import net.sf.ehcache.event.CacheEventListenerFactory;

/**
 * Work-around for CACHE-95 / EHC-1089.
 * <p>
 * Due to this bug, the first cache that registers a listener configuration pollutes the default configuration
 * with that listener.  As a result, every cache thereafter gets that listener factory, too.  To prevent this,
 * we explicitly give the default configuration a listener factory that will deliver a single no-op listener
 * on every cache.  This ensures that the default configuration has at least one listener, which in turn makes
 * the listener configuration clone itself properly.  The extra listener call on every cache notification is
 * unlikely to be at all significant.
 * </p>
 * <p>
 * This is only {@code public} because EhCache wants to instantiate a new one every time it creates a cache.
 * Awesome.
 * </p>
 *
 * @since v6.4
 */
public final class NoopCacheEventListenerFactory extends CacheEventListenerFactory
{
    private static final Logger LOG = LoggerFactory.getLogger(NoopCacheEventListenerFactory.class);

    static CacheManager workAroundCache95(CacheManager ehCacheManager)
    {
        final CacheConfiguration defaultConfig = ehCacheManager.getConfiguration().getDefaultCacheConfiguration();
        final CacheConfiguration.CacheEventListenerFactoryConfiguration listenerConfig =
                new CacheConfiguration.CacheEventListenerFactoryConfiguration()
                        .className(NoopCacheEventListenerFactory.class.getName());

        defaultConfig.clone().addCacheEventListenerFactory(listenerConfig);
        if (defaultConfig.getCacheEventListenerConfigurations().isEmpty())
        {
            LOG.warn("It looks like CACHE-95 has been fixed.  This workaround should be removed.");
        }
        else
        {
            LOG.debug("Working around CACHE-95");
        }
        return ehCacheManager;
    }

    @Override
    public CacheEventListener createCacheEventListener(Properties properties)
    {
        return NoopCacheEventListener.INSTANCE;
    }

    static final class NoopCacheEventListener implements CacheEventListener
    {
        static final NoopCacheEventListener INSTANCE = new NoopCacheEventListener();

        @Override
        public void notifyElementRemoved(Ehcache cache, Element element) {}

        @Override
        public void notifyElementPut(Ehcache cache, Element element) {}

        @Override
        public void notifyElementUpdated(Ehcache cache, Element element) {}

        @Override
        public void notifyElementExpired(Ehcache cache, Element element) {}

        @Override
        public void notifyElementEvicted(Ehcache cache, Element element) {}

        @Override
        public void notifyRemoveAll(Ehcache cache) {}

        @Override
        public void dispose() {}

        // What that, Skip?  You want to clone my stateless singleton?  W'evs.
        @SuppressWarnings({ "CloneDoesntDeclareCloneNotSupportedException", "CloneDoesntCallSuperClone" })
        public Object clone()
        {
            return this;
        }
    }
}
