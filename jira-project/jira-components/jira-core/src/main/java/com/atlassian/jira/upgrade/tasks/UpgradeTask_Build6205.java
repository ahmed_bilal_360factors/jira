package com.atlassian.jira.upgrade.tasks;

import java.util.EnumSet;

import javax.annotation.Nullable;

import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;

/**
 * Indexes attachments by name so they are JQL searchable.
 */
public class UpgradeTask_Build6205 extends AbstractReindexUpgradeTask
{
    @Override
    public String getBuildNumber()
    {
        return "6205";
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception
    {
        getReindexRequestService().requestReindex(ReindexRequestType.DELAYED, EnumSet.of(AffectedIndex.ISSUE), EnumSet.noneOf(SharedEntityType.class));
    }

    @Override
    public String getShortDescription()
    {
        return "Indexes if issue has attachments so they are JQL searchable.";
    }

    @Nullable
    @Override
    public String dependsUpon()
    {
        return "6200";
    }

}
