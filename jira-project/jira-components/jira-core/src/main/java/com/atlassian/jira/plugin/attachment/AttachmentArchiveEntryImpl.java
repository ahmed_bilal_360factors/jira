package com.atlassian.jira.plugin.attachment;

import javax.annotation.concurrent.Immutable;

import com.atlassian.jira.issue.attachment.Path;

import com.google.common.base.Objects;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;

@JsonAutoDetect
@Immutable
public class AttachmentArchiveEntryImpl implements AttachmentArchiveEntry
{
    private final int entryIndex;
    private final String name;
    private final long size;
    private final String mediaType;

    AttachmentArchiveEntryImpl(
            final int entryIndex,
            final String name,
            final long size,
            final String mediaType
    )
    {
        this.entryIndex = entryIndex;
        this.name = name;
        this.size = size;
        this.mediaType = mediaType;
    }

    public long getEntryIndex()
    {
        return entryIndex;
    }

    public String getName()
    {
        return name;
    }

    @Override
    @JsonIgnore
    public String getAbbreviatedName()
    {
        return new Path(name).abbreviate(40).toString();
    }

    public long getSize()
    {
        return size;
    }

    public String getMediaType()
    {
        return mediaType;
    }

    @Override
    public int hashCode() {return Objects.hashCode(entryIndex, name, size, mediaType);}

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()) {return false;}
        final AttachmentArchiveEntryImpl other = (AttachmentArchiveEntryImpl) obj;
        return Objects.equal(this.entryIndex, other.entryIndex)
                && Objects.equal(this.name, other.name)
                && Objects.equal(this.size, other.size)
                && Objects.equal(this.mediaType, other.mediaType);
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this)
                .add("entryIndex", entryIndex)
                .add("name", name)
                .add("size", size)
                .add("mediaType", mediaType)
                .toString();
    }
}
