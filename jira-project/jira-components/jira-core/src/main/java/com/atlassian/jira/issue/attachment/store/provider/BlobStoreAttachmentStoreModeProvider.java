package com.atlassian.jira.issue.attachment.store.provider;

import javax.annotation.Nonnull;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;

import com.google.common.annotations.VisibleForTesting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Reads BlobStore specific feature flags that set in which mode will the
 * {@link BackwardCompatibleStoreAdapterProvider} operate.
 */
public class BlobStoreAttachmentStoreModeProvider
{
    private static final Logger log = LoggerFactory.getLogger(BlobStoreAttachmentStoreModeProvider.class);

    @VisibleForTesting
    public static final String INITIAL_MODE_SYSPROP = "com.atlassian.jira.DualAttachmentStore.initialMode";
    @VisibleForTesting
    static final String FS_ONLY_FLAG = "com.atlassian.jira.FS_ATTACHMENTS_ONLY";
    @VisibleForTesting
    static final String REMOTE_ONLY_FLAG = "com.atlassian.jira.REMOTE_ATTACHMENTS_ONLY";
    @VisibleForTesting
    static final String FS_PRIMARY_FLAG = "com.atlassian.jira.FS_ATTACHMENTS_PRIMARY";
    @VisibleForTesting
    static final String REMOTE_PRIMARY_FLAG = "com.atlassian.jira.REMOTE_ATTACHMENTS_PRIMARY";

    public enum Mode
    {
        FS_ONLY, FS_PRIMARY, REMOTE_PRIMARY, REMOTE_ONLY
    }

    private final FeatureManager featureManager;
    private final JiraProperties jiraProperties;

    public BlobStoreAttachmentStoreModeProvider(
            @Nonnull final FeatureManager featureManager,
            @Nonnull final JiraProperties jiraProperties)
    {
        this.featureManager = featureManager;
        this.jiraProperties = jiraProperties;
    }


    private boolean enabled(final String featureKey)
    {
        return featureManager.isEnabled(featureKey);
    }

    @Nonnull public Mode mode()
    {
        final Mode mode;

        if (enabled(REMOTE_ONLY_FLAG))
        {
            mode = Mode.REMOTE_ONLY;
        }
        else if (enabled(REMOTE_PRIMARY_FLAG))
        {
            mode = Mode.REMOTE_PRIMARY;
        }
        else if (enabled(FS_PRIMARY_FLAG))
        {
            mode = Mode.FS_PRIMARY;
        }
        else if (enabled(FS_ONLY_FLAG))
        {
            mode = Mode.FS_ONLY;
        }
        else
        {
            // No feature flag specified to override the mode; use the initial mode.
            mode = getInitialMode();
        }

        log.debug("Using DualAttachmentStoreProvider mode '{}'", mode);
        return mode;
    }

    /**
     * @return initial mode as defined by sysprop, or FS_ONLY if sysprop value is invalid or unspecified.
     */
    @Nonnull private Mode getInitialMode()
    {
        Mode mode;
        final String initialMode = jiraProperties.getProperty(INITIAL_MODE_SYSPROP, Mode.FS_ONLY.name());
        try
        {
            mode = Mode.valueOf(initialMode);
        }
        catch (final IllegalArgumentException e)
        {
            log.warn("Invalid initial DualAttachmentStoreProvider mode specified in sysprop {}: '{}'. Falling back to FS_ONLY.", INITIAL_MODE_SYSPROP, initialMode);
            mode = Mode.FS_ONLY;
        }
        return mode;
    }

}
