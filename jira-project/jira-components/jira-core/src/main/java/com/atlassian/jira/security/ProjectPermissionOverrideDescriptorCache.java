package com.atlassian.jira.security;

import java.util.List;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.security.plugin.ProjectPermissionOverrideModuleDescriptor;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.util.concurrent.ResettableLazyReference;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;

/**
 * Simple cache for Project Permission Override Module Descriptors (@see ProjectPermissionOverrideModuleDescriptor)
 * Modules being enabled / disabled trigger cache reload
 *
 * @since v6.4
 */
public class ProjectPermissionOverrideDescriptorCache implements Startable
{
    private EventPublisher eventPublisher;
    private ResettableLazyReference<List<ProjectPermissionOverrideModuleDescriptor>> enabledModuleDescriptors;

    public ProjectPermissionOverrideDescriptorCache(final EventPublisher eventPublisher)
    {
        this.eventPublisher = eventPublisher;
        enabledModuleDescriptors = new ResettableLazyReference<List<ProjectPermissionOverrideModuleDescriptor>>()
        {
            @Override
            protected List<ProjectPermissionOverrideModuleDescriptor> create() throws Exception
            {
                return ImmutableList.copyOf(ComponentAccessor.getPluginAccessor().getEnabledModuleDescriptorsByClass(ProjectPermissionOverrideModuleDescriptor.class));
            }
        };
    }

    @EventListener
    public void onPluginModuleEnabledEvent(final PluginModuleEnabledEvent event)
    {
        if (event.getModule() instanceof ProjectPermissionOverrideModuleDescriptor)
        {
            reloadCache();
        }
    }

    @EventListener
    public void onPluginModuleDisabledEvent(final PluginModuleDisabledEvent event)
    {
        if (event.getModule() instanceof ProjectPermissionOverrideModuleDescriptor)
        {
            reloadCache();
        }
    }

    public List<ProjectPermissionOverrideModuleDescriptor> getProjectPermissionOverrideDescriptors()
    {
        return enabledModuleDescriptors.get();
    }

    @Override
    public void start() throws Exception
    {
        eventPublisher.register(this);
    }

    @VisibleForTesting
    protected synchronized void reloadCache()
    {
        enabledModuleDescriptors.reset();
    }
}
