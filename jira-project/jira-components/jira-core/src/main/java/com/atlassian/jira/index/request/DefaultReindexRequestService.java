package com.atlassian.jira.index.request;

import java.util.Collections;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

import com.google.common.collect.ImmutableSet;

import org.apache.log4j.Logger;

/**
 * @since 6.4
 */
public class DefaultReindexRequestService implements ReindexRequestService
{
    private static final Logger log = Logger.getLogger(DefaultReindexRequestService.class);

    private final ReindexRequestManager reindexRequestManager;
    private final PermissionManager permissionManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public DefaultReindexRequestService(ReindexRequestManager reindexRequestManager,
            PermissionManager permissionManager, JiraAuthenticationContext jiraAuthenticationContext)
    {
        this.reindexRequestManager = reindexRequestManager;
        this.permissionManager = permissionManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Nonnull
    @Override
    public Set<ReindexRequest> processRequests(Set<ReindexRequestType> reindexRequestTypes, final boolean runInBackground) throws PermissionException
    {
        return processRequests(false, reindexRequestTypes, runInBackground);
    }

    @Nonnull
    @Override
    public Set<ReindexRequest> processRequestsAndWait(Set<ReindexRequestType> reindexRequestTypes, final boolean runInBackground) throws PermissionException
    {
        return processRequests(true, reindexRequestTypes, runInBackground);
    }

    private Set<ReindexRequest> processRequests(boolean waitForCompletion, Set<ReindexRequestType> reindexRequestTypes, final boolean runInBackground) throws PermissionException
    {
        final ApplicationUser user = jiraAuthenticationContext.getUser();
        if (!permissionManager.hasPermission(Permissions.ADMINISTER, user))
        {
            throw new PermissionException();
        }

        if (reindexRequestManager.isReindexInProgress())
        {
            throw new IllegalStateException(i18n().getText("admin.indexing.alreadyinprogress"));
        }

        Set<ReindexRequest> requests = reindexRequestManager.processPendingRequests(waitForCompletion, reindexRequestTypes, runInBackground);

        return ImmutableSet.copyOf(requests);
    }

    @Nonnull
    @Override
    public ReindexRequest requestReindex(@Nonnull ReindexRequestType type, @Nonnull Set<AffectedIndex> affectedIndexes, @Nonnull Set<SharedEntityType> entityTypes)
    {
        return reindexRequestManager.requestReindex(type, affectedIndexes, entityTypes);
    }

    @Nullable
    @Override
    public ReindexRequest getReindexProgress(long requestId)
    {
        Set<ReindexRequest> results = getReindexProgress(Collections.singleton(requestId));
        if (results.isEmpty())
        {
            return null;
        }
        else
        {
            return results.iterator().next();
        }
    }

    @Nonnull
    @Override
    public Set<ReindexRequest> getReindexProgress(@Nonnull Set<Long> requestIds)
    {
        return reindexRequestManager.getReindexProgress(requestIds);
    }

    @Override
    public boolean isReindexRequested()
    {
        return reindexRequestManager.isReindexRequested();
    }

    private I18nHelper i18n()
    {
        return jiraAuthenticationContext.getI18nHelper();
    }

}
