/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

package com.atlassian.jira.web.action.admin.index;

import java.util.Collection;

import com.atlassian.jira.index.ha.IndexRecoveryService;
import com.atlassian.jira.issue.fields.option.TextOption;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.jira.web.component.cron.CronEditorBean;
import com.atlassian.jira.web.component.cron.CronEditorWebComponent;
import com.atlassian.jira.web.component.cron.generator.CronExpressionGenerator;
import com.atlassian.jira.web.component.cron.parser.CronExpressionParser;
import com.atlassian.sal.api.websudo.WebSudoRequired;

import webwork.action.ActionContext;

@WebSudoRequired
public class EditIndexRecoverySettings extends ProjectActionSupport
{
    private final IndexRecoveryService indexRecoveryService;

    // options
    private boolean recoveryEnabled;

    private IndexRecoveryUtil.Interval snapshotInterval;
    private CronEditorBean cronEditorBean;

    public EditIndexRecoverySettings(ProjectManager projectManager, PermissionManager permissionManager,
            final IndexRecoveryService indexRecoveryService)
    {
        super(projectManager, permissionManager);
        this.indexRecoveryService = indexRecoveryService;
    }

    public String doDefault() throws Exception
    {
        recoveryEnabled = indexRecoveryService.isRecoveryEnabled(getLoggedInApplicationUser());
        if (recoveryEnabled)
        {
            String cronExpression = indexRecoveryService.getSnapshotCronExpression(getLoggedInApplicationUser());
            cronEditorBean = new CronExpressionParser(cronExpression).getCronEditorBean();
        }
        else
        {
            recoveryEnabled = false;
            cronEditorBean = new CronExpressionParser().getCronEditorBean();
        }
        return INPUT;
    }

    public CronEditorBean getCronEditorBean()
    {
        return cronEditorBean;
    }

    protected void doValidation()
    {
        cronEditorBean = new CronEditorBean("service.schedule", ActionContext.getParameters());
        CronEditorWebComponent component = new CronEditorWebComponent();
        addErrorCollection(component.validateInput(cronEditorBean, "cron.editor.name"));
        super.doValidation();
    }

    @RequiresXsrfCheck
    protected String doExecute() throws Exception
    {
        String cronExpression = new CronExpressionGenerator().getCronExpressionFromInput(cronEditorBean);
        indexRecoveryService.updateRecoverySettings(getLoggedInApplicationUser(), recoveryEnabled, cronExpression);
        return returnComplete("IndexAdmin.jspa");
    }

    public boolean isRecoveryEnabled()
    {
        return recoveryEnabled;
    }

    public void setRecoveryEnabled(final boolean recoveryEnabled)
    {
        this.recoveryEnabled = recoveryEnabled;
    }

    public String getSnapshotInterval()
    {
        return snapshotInterval.name();
    }

    public void setSnapshotInterval(final String snapshotInterval)
    {
        this.snapshotInterval = IndexRecoveryUtil.Interval.valueOf(snapshotInterval);
    }

    public Collection<TextOption> getIntervalOptions()
    {
        return IndexRecoveryUtil.getIntervalOptions(getI18nHelper());
    }


}
