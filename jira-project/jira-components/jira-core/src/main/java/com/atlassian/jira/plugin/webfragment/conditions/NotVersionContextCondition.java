package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.VersionHelper;
import com.atlassian.jira.user.ApplicationUser;

/**
 * This condition specifies that the item should only be displayed if the supplied {@link
 * com.atlassian.jira.plugin.webfragment.model.JiraHelper} is not of type {@link com.atlassian.jira.plugin.webfragment.model.VersionHelper}
 * i.e. we are not in a Version context.
 *
 * @since v4.0
 */
public class NotVersionContextCondition extends AbstractWebCondition
{
    public boolean shouldDisplay(final ApplicationUser user, final JiraHelper jiraHelper)
    {
        return !(jiraHelper instanceof VersionHelper);
    }
}
