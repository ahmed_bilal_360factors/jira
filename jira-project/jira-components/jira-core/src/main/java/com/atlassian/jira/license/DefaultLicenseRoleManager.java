package com.atlassian.jira.license;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.event.DirectoryEvent;
import com.atlassian.crowd.event.directory.RemoteDirectorySynchronisedEvent;
import com.atlassian.crowd.event.group.GroupCreatedEvent;
import com.atlassian.crowd.event.group.GroupDeletedEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.fugue.Option;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.cluster.ClusterSafe;
import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.config.FeatureEvent;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.security.groups.GroupManager;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Default implementation of {@link com.atlassian.jira.license.LicenseRoleManager}.
 *
 * @since 6.3
 */
@EventComponent
public final class DefaultLicenseRoleManager implements LicenseRoleManager
{
    private final static LicenseRoleId PLATFORM_LICENSE_ROLE_ID = LicenseRoleId.valueOf("com.atlassian.jira.platform");
    private final LicenseRoleDefinitions definitions;
    private final GroupManager groupManager;
    private final LicenseRoleStore licenseRoleStore;

    @ClusterSafe
    private final Cache<String, Option<LicenseRole>> licenseRoleCache;
    @ClusterSafe
    private final Cache<String, Integer> activeUsersCountForLicense;

    public DefaultLicenseRoleManager(@Nonnull final CacheManager cacheManager, @Nonnull final LicenseRoleStore store,
            @Nonnull LicenseRoleDefinitions definitions, @Nonnull final GroupManager groupManager)
    {
        notNull("cacheManager", cacheManager);

        this.groupManager = notNull("groupManager", groupManager);
        this.licenseRoleStore = notNull("store", store);
        this.definitions = notNull("definitions", definitions);

        this.licenseRoleCache = cacheManager.getCache(DefaultLicenseRoleManager.class.getName() + ".licenseRoleGroups",
                new LicenseRoleLoader(store, groupManager, definitions),
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).maxEntries(100).build());
        this.activeUsersCountForLicense = cacheManager.getCache(
                DefaultLicenseRoleManager.class.getName() + ".activeUsersCountForLicense",
                new ActiveUserCountLoader(),
                new CacheSettingsBuilder().expireAfterWrite(2, TimeUnit.HOURS).build());

    }

    @Nonnull
    @Override
    public Option<LicenseRole> getLicenseRole(@Nonnull final LicenseRoleId licenseRoleId)
    {
        notNull("licenseRoleId", licenseRoleId);

        //We know this can't be null because we use options to represent null.
        //noinspection ConstantConditions
        return licenseRoleCache.get(licenseRoleId.getName());
    }

    @Nonnull
    @Override
    public Set<LicenseRole> getLicenseRoles()
    {
        Set<LicenseRole> roles = Sets.newHashSet();
        for (LicenseRoleDefinition definition : definitions.getInstalledRoleDefinitions())
        {
            Iterables.addAll(roles, getLicenseRole(definition.getLicenseRoleId()));
        }
        return Collections.unmodifiableSet(roles);
    }

    /**
     * Get groups that has been associated with all authenticated license roles
     *
     * @return groups that has been associated with all authenticated license roles
     */
    @Nonnull
    @Override
    public Set<String> getGroupsForInstalledLicenseRoles()
    {
        Set<String> groupsForAuthenticatedLicenseRoles = Sets.newHashSet();
        for (LicenseRole licenseRole : getLicenseRoles())
        {
            groupsForAuthenticatedLicenseRoles.addAll(licenseRole.getGroups());
        }
        return Collections.unmodifiableSet(groupsForAuthenticatedLicenseRoles);
    }

    @Override
    public void removeGroupFromLicenseRoles(@Nonnull String groupName)
    {
        licenseRoleStore.removeGroup(groupName);
        clearCaches();
    }

    @Override
    public boolean isLicenseRoleInstalled(@Nonnull final LicenseRoleId licenseRoleId)
    {
        notNull("licenseRoleId", licenseRoleId);

        return definitions.isLicenseRoleInstalled(licenseRoleId);
    }

    @Nonnull
    @Override
    public LicenseRole setLicenseRole(@Nonnull final LicenseRole role)
    {
        validateRole(notNull("role", role));

        final LicenseRoleId id = role.getId();
        try
        {
            licenseRoleStore.save(new LicenseRoleStore.LicenseRoleData(id, role.getGroups(), role.getPrimaryGroup()));
        }
        finally
        {
            clearCaches(id);
        }

        //noinspection ConstantConditions
        return licenseRoleCache.get(id.getName()).getOrElse(new Supplier<LicenseRole>()
        {
            @Override
            public LicenseRole get()
            {
                return IdLicenseRole.empty(id);
            }
        });
    }

    @Override
    public int getUserCount(@Nonnull final LicenseRoleId roleId)
    {
        notNull("roleId", roleId);

        return activeUsersCountForLicense.get(roleId.getName());
    }

    private void validateRole(LicenseRole role)
    {
        if (!isLicenseRoleInstalled(role.getId()))
        {
            throw new IllegalArgumentException(String.format("Role '%s' is not authenticated.", role.getId()));
        }
        for (String group : role.getGroups())
        {
            if (!groupManager.groupExists(group))
            {
                throw new IllegalArgumentException(String.format("Role '%s' contains group '%s' that does not exist.",
                        role.getId(), group));
            }
        }
    }

    @SuppressWarnings ({ "UnusedDeclaration" })
    @EventListener
    public void clearCache(ClearCacheEvent event)
    {
        clearCaches();
    }

    @SuppressWarnings ({ "UnusedDeclaration" })
    @EventListener
    public void onLicenseRoleDefined(LicenseRoleDefinedEvent event)
    {
        clearCaches(event.getLicenseRoleId());
    }

    @SuppressWarnings ({ "UnusedDeclaration" })
    @EventListener
    public void onLicenseRoleUndefined(LicenseRoleUndefinedEvent event)
    {
        clearCaches(event.getLicenseRoleId());
    }

    @SuppressWarnings ({ "UnusedDeclaration" })
    @EventListener
    public void onLicenseChanged(NewLicenseEvent event)
    {
        clearCaches();
    }

    @SuppressWarnings ({ "UnusedDeclaration" })
    @EventListener
    public void onFeatureChange(FeatureEvent event)
    {
        //We need this because the license might change as the feature changes. We can remove this code once
        //the dark feature becomes light.
        if (CoreFeatures.forFeatureKey(event.feature()) == CoreFeatures.LICENSE_ROLES_ENABLED)
        {
            clearCaches();
        }
    }

    @SuppressWarnings ({ "UnusedDeclaration" })
    @EventListener
    public void onGroupDeleted(GroupDeletedEvent deletedEvent)
    {
        clearCaches();
    }

    @SuppressWarnings ({ "UnusedDeclaration" })
    @EventListener
    public void onGroupCreated(GroupCreatedEvent createdEvent)
    {
        clearCaches();
    }

    @SuppressWarnings ({ "UnusedDeclaration" })
    @EventListener
    public void onDirectoryModified(final DirectoryEvent event)
    {
        clearCaches();
    }

    @SuppressWarnings ({ "UnusedDeclaration" })
    @EventListener
    public void onDirectorySynchronisation(final RemoteDirectorySynchronisedEvent event)
    {
        clearCaches();
    }

    private void clearCaches()
    {
        licenseRoleCache.removeAll();
        activeUsersCountForLicense.removeAll();
    }

    private void clearCaches(final LicenseRoleId id)
    {
        licenseRoleCache.remove(id.getName());
        activeUsersCountForLicense.remove(id.getName());
    }

    private static class LicenseRoleLoader implements CacheLoader<String, Option<LicenseRole>>
    {
        private final LicenseRoleStore store;
        private final GroupManager groupManager;
        private final LicenseRoleDefinitions definitions;

        @VisibleForTesting
        LicenseRoleLoader(final LicenseRoleStore store, final GroupManager groupManager, final LicenseRoleDefinitions definitions)
        {
            this.store = store;
            this.groupManager = groupManager;
            this.definitions = definitions;
        }

        @Nonnull
        @Override
        public Option<LicenseRole> load(@Nonnull final String licenseRole)
        {
            final LicenseRoleId id = LicenseRoleId.valueOf(licenseRole);
            return definitions.getInstalledRoleDefinition(id).map(new Function<LicenseRoleDefinition, LicenseRole>()
            {
                @Override
                public LicenseRole apply(@Nullable final LicenseRoleDefinition input)
                {
                    final LicenseRoleStore.LicenseRoleData licenseRoleData = store.get(id);
                    final Set<String> realGroups = Sets.filter(licenseRoleData.getGroups(), new Predicate<String>()
                    {
                        @Override
                        public boolean apply(@Nullable final String input)
                        {
                            return groupManager.groupExists(input);
                        }
                    });

                    final Option<String> primaryGroup = licenseRoleData.getPrimaryGroup();
                    if (primaryGroup.isEmpty() || !realGroups.contains(primaryGroup.get()))
                    {
                        return new DefinitionLicenseRole(input, realGroups, Option.none(String.class));
                    }
                    else
                    {
                        return new DefinitionLicenseRole(input, realGroups, primaryGroup);
                    }
                }
            });
        }
    }

    private class ActiveUserCountLoader implements CacheLoader<String, Integer>
    {
        @Override
        @Nonnull
        public Integer load(@Nonnull String roleIdName)
        {
            int activeUsers = 0;
            LicenseRoleId roleId= LicenseRoleId.valueOf(roleIdName);
            Option<LicenseRole> licenseRole = getLicenseRole(roleId);
            if (licenseRole.isDefined())
            {
                final Set<User> users = new HashSet<User>();
                for (String group : licenseRole.get().getGroups())
                {
                    Collection<User> usersInGroup = groupManager.getUsersInGroup(group);
                    for (User user : usersInGroup)
                    {
                        if (user.isActive())
                        {
                            users.add(user);
                        }
                    }
                }
                if (PLATFORM_LICENSE_ROLE_ID.equals(roleId))
                {
                    // remove all license role users from the users set hence platform doesn't
                    // double count application active users.
                    for (LicenseRole role : getLicenseRoles())
                    {
                        if (!role.getId().equals(PLATFORM_LICENSE_ROLE_ID))
                        {
                            for (String roleGroup : role.getGroups())
                            {
                                users.removeAll(groupManager.getUsersInGroup(roleGroup));
                            }
                        }
                    }
                }
                activeUsers = users.size();
            }
            return activeUsers;
        }
    }
}
