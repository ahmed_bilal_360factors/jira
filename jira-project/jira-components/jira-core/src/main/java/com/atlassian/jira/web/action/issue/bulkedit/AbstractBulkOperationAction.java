/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */
package com.atlassian.jira.web.action.issue.bulkedit;

import java.util.Collection;
import java.util.List;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.web.SessionKeys;
import com.atlassian.jira.web.action.IssueActionSupport;
import com.atlassian.jira.web.bean.BulkEditBean;
import com.atlassian.jira.web.bean.BulkEditBeanSessionHelper;
import com.atlassian.jira.web.component.ColumnLayoutItemProvider;
import com.atlassian.jira.web.component.IssueTableLayoutBean;

import com.google.common.annotations.VisibleForTesting;

import org.apache.commons.lang.StringUtils;

import webwork.action.ActionContext;

public class AbstractBulkOperationAction extends IssueActionSupport
{
    private final SearchService searchService;
    private final BulkEditBeanSessionHelper bulkEditBeanSessionHelper;

    public AbstractBulkOperationAction(final SearchService searchService,
                                       final BulkEditBeanSessionHelper bulkEditBeanSessionHelper)
    {
        this.searchService = searchService;
        this.bulkEditBeanSessionHelper = bulkEditBeanSessionHelper;
    }

    public BulkEditBean getBulkEditBean()
    {
        return getRootBulkEditBean();
    }

    public List getColumns() throws Exception
    {
        return getColumnsProvider().getColumns(getLoggedInUser(), getSearchRequest());
    }

    @VisibleForTesting
    ColumnLayoutItemProvider getColumnsProvider()
    {
        return new ColumnLayoutItemProvider();
    }

    public IssueTableLayoutBean getIssueTableLayoutBean() throws Exception
    {
        IssueTableLayoutBean layoutBean = new IssueTableLayoutBean(getColumns());
        layoutBean.setSortingEnabled(false);
        return layoutBean;
    }

    protected void clearBulkEditBean()
    {
        bulkEditBeanSessionHelper.removeFromSession();
    }

    protected String finishWizard() throws Exception
    {
        final String returnUrl = getRedirectUrl();
        clearBulkEditBean();
        return getRedirect(returnUrl);
    }

    protected String watchProgress(String progressUrl) throws Exception
    {
        getRootBulkEditBean().setRedirectUrl(getRedirectUrl());
        return getRedirect(progressUrl);
    }

    /**
     * A bulk operation-specific override to avoid the slightly insane
     * default behaviour of {@link com.atlassian.jira.web.action.JiraWebActionSupport#getRedirect(String)}, since it
     * will go wherever {@link #getReturnUrl()} tells it to before it goes
     * to the URL you pass as a parameter.
     *
     * @param urlToActuallyGoTo the URL to actually go to.
     * @return the result of the redirection.
     */
    @Override
    public String getRedirect(final String urlToActuallyGoTo)
    {
        final String eventuallyReturnToUrl = getReturnUrl();
        setReturnUrl(null);
        storeReturnUrl(eventuallyReturnToUrl);
        return super.getRedirect(urlToActuallyGoTo);
    }

    protected String getRedirectUrl() throws Exception
    {
        String finishedUrl = null;
        BulkEditBean editBean = getRootBulkEditBean();
        if (editBean != null)
        {
            finishedUrl = editBean.getRedirectUrl();
        }
        if (StringUtils.isEmpty(finishedUrl))
        {
            finishedUrl = "/secure/IssueNavigator.jspa";
            if (editBean != null && editBean.isSingleMode())
            {
                finishedUrl = "/browse/" + editBean.getSingleIssueKey();
            }
        }
        return finishedUrl;
    }

    protected void storeBulkEditBean(BulkEditBean bulkEditBean)
    {
        bulkEditBeanSessionHelper.storeToSession(bulkEditBean);
        storeReturnUrl(getReturnUrl());
    }

    protected void storeReturnUrl(String returnUrl)
    {
        BulkEditBean editBean = getRootBulkEditBean();
        if (editBean != null && !editBean.isRedirectUrlSet())
        {
            editBean.setRedirectUrl(returnUrl);
        }
    }

    public BulkEditBean getRootBulkEditBean()
    {
        final BulkEditBean bean = bulkEditBeanSessionHelper.getFromSession();
        if (bean == null)
        {
            log.warn(
                    "Bulk edit bean unexpectedly null. Perhaps session was lost (e.g. when URL used is different to base URL in General Configuration)?");
        }
        return bean;
    }

    /**
     * Determines if the current user can disable mail notifications for this bulk operation.
     * <p/>
     * Only global admins or a user who is a project admin of all projects associated with the selected issues
     * can disable bulk mail notifications.
     *
     * @return true     if the user is a global admin or a project admin of all projects associated with the selected issues.
     */
    public boolean isCanDisableMailNotifications()
    {
        // Check for global admin permission
        if (hasGlobalPermission(GlobalPermissionKey.ADMINISTER))
        {
            return true;
        }
        // Check for project admin permission on all projects from selected issue collection
        else
        {
            Collection<Project> projects = getBulkEditBean().getProjectObjects();

            for (Project project : projects)
            {
                if (!hasProjectPermission(ProjectPermissions.ADMINISTER_PROJECTS, project))
                {
                    return false;
                }
            }

            return true;
        }
    }

    public boolean isSendBulkNotification()
    {
        return getBulkEditBean().isSendBulkNotification();
    }

    public void setSendBulkNotification(boolean sendBulkNotification)
    {
        if (getBulkEditBean() != null)
        {
            getBulkEditBean().setSendBulkNotification(sendBulkNotification);
        }
    }

    protected String redirectToStart()
    {
        ActionContext.getSession()
                .put(SessionKeys.SESSION_TIMEOUT_MESSAGE, getText("bulk.operation.session.timeout.message"));
        return getRedirect("SessionTimeoutMessage.jspa");
    }
}
