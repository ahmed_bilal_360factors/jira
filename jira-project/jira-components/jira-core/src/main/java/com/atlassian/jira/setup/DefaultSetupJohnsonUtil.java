package com.atlassian.jira.setup;

import java.util.Collection;

import com.atlassian.jira.web.ServletContextProvider;
import com.atlassian.johnson.JohnsonEventContainer;


public class DefaultSetupJohnsonUtil implements SetupJohnsonUtil
{
    public Collection getEvents()
    {
        final JohnsonEventContainer johnsonContainer = JohnsonEventContainer.get(ServletContextProvider.getServletContext());

        return johnsonContainer.getEvents();
    }
}
