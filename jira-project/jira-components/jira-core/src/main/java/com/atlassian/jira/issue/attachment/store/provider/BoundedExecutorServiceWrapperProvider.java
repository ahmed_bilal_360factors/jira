package com.atlassian.jira.issue.attachment.store.provider;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Effect;
import com.atlassian.jira.concurrent.ResettableLazyReference;
import com.atlassian.jira.event.ComponentManagerShutdownEvent;
import com.atlassian.jira.util.BoundedExecutorServiceWrapper;
import com.atlassian.jira.util.Supplier;

import org.picocontainer.injectors.ProviderAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides single {@link com.atlassian.jira.util.BoundedExecutorServiceWrapper}. It is responsible for shutting
 * executor down when {@link com.atlassian.jira.event.ComponentManagerShutdownEvent} arrives.
 *
 * @since v6.4
 */
public class BoundedExecutorServiceWrapperProvider extends ProviderAdapter
{

    private static final Logger log = LoggerFactory.getLogger(BoundedExecutorServiceWrapperProvider.class);

    private final ResettableLazyReference<BoundedExecutorServiceWrapper> executorReference = new ResettableLazyReference<BoundedExecutorServiceWrapper>();

    private final Supplier<BoundedExecutorServiceWrapper> creator = new Supplier<BoundedExecutorServiceWrapper>()
    {
        @Override
        public BoundedExecutorServiceWrapper get()
        {
            return new BoundedExecutorServiceWrapper.Builder()
                    .withConcurrency(100)
                    .withThreadPoolName("file-attachment-store")
                    .build();
        }
    };

    private final AtomicBoolean registered = new AtomicBoolean(false);

    public BoundedExecutorServiceWrapper provide(final EventPublisher eventPublisher)
    {
        if (!registered.getAndSet(true))
        {
            eventPublisher.register(this);
        }
        return executorReference.getOrCreate(creator);
    }

    @EventListener
    public void stop(final ComponentManagerShutdownEvent event)
    {
            executorReference.reset().foreach(new Effect<BoundedExecutorServiceWrapper>()
            {
                @Override
                public void apply(final BoundedExecutorServiceWrapper boundedExecutorServiceWrapper)
                {
                    final boolean terminated = boundedExecutorServiceWrapper.awaitTermination();
                    if (!terminated)
                    {
                        log.warn("Failed to terminate " + BoundedExecutorServiceWrapper.class.getName());
                    }
                }
            });

    }
}
