package com.atlassian.jira.index.request;

import java.util.List;

import javax.annotation.Nonnull;

/**
 * A coalescer that doesn't do any coalescing.  All requests are left exactly as they are.
 *
 * @since 6.4
 */
public class ReindexRequestNonCoalescer implements ReindexRequestCoalescer
{
    @Nonnull
    @Override
    public List<ReindexRequest> coalesce(@Nonnull List<ReindexRequest> requests)
    {
        return requests;
    }
}
