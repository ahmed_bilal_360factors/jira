package com.atlassian.jira.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

import com.atlassian.jira.util.resourcebundle.DefaultResourceBundle;
import com.atlassian.jira.web.bean.i18n.TranslationStore;

/**
 * Simple TranslationStore to be used in the Bootstrap container, because it is needed to show start up errors.
 * <p>
 *     This is a very thin wrapper around a ResourceBundle for the given Locale.
 * </p>
 * See JRA-43308
 *
 * @since v6.4.5
 */
public class BootstrapTranslationStore implements TranslationStore
{
    private final ResourceBundle resourceBundle;

    public BootstrapTranslationStore(final Locale locale)
    {
        resourceBundle = DefaultResourceBundle.getDefaultResourceBundle(locale);
    }

    @Override
    public String get(final String key)
    {
        return resourceBundle.getString(key);
    }

    @Override
    public boolean containsKey(final String key)
    {
        return resourceBundle.containsKey(key);
    }

    @Override
    public Iterable<String> keys()
    {
        return resourceBundle.keySet();
    }
}
