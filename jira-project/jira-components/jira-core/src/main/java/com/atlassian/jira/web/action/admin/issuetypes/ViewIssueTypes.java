package com.atlassian.jira.web.action.admin.issuetypes;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.IssueTypeService;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.jira.web.action.admin.constants.AbstractViewConstants;
import com.atlassian.jira.web.action.admin.issuetypes.events.IssueTypeCreatedFromViewIssueTypesPageEvent;
import com.atlassian.jira.web.action.admin.translation.TranslationManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;

import com.google.common.collect.ImmutableList;

import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import static com.atlassian.jira.config.IssueTypeService.IssueTypeCreateInput;
import static com.atlassian.jira.config.IssueTypeService.IssueTypeCreateInput.Type.STANDARD;
import static com.atlassian.jira.config.IssueTypeService.IssueTypeCreateInput.Type.SUBTASK;

@WebSudoRequired
public class ViewIssueTypes extends AbstractViewConstants<IssueType> implements AddIssueTypeAction
{
    public static final String NEW_ISSUE_TYPE_DEFAULT_ICON = "/images/icons/issuetypes/genericissue.png";

    private String style;

    private final FieldManager fieldManager;
    private final FieldConfigSchemeManager configSchemeManager;
    private final IssueTypeSchemeManager issueTypeSchemeManager;
    private final IssueTypeManageableOption issueTypeManageableOption;
    private final EventPublisher eventPublisher;
    private final IssueTypeService issueTypeService;

    public ViewIssueTypes(
            final FieldManager fieldManager,
            final FieldConfigSchemeManager configSchemeManager,
            final IssueTypeSchemeManager issueTypeSchemeManager,
            final TranslationManager translationManager,
            final IssueTypeManageableOption issueTypeManageableOption,
            final EventPublisher eventPublisher,
            final IssueTypeService issueTypeService)
    {
        super(translationManager);
        this.fieldManager = fieldManager;
        this.configSchemeManager = configSchemeManager;
        this.issueTypeSchemeManager = issueTypeSchemeManager;
        this.issueTypeManageableOption = issueTypeManageableOption;
        this.eventPublisher = eventPublisher;
        this.issueTypeService = issueTypeService;
        setIconurl(NEW_ISSUE_TYPE_DEFAULT_ICON);
    }

    protected String getConstantEntityName()
    {
        return "IssueType";
    }

    protected String getNiceConstantName()
    {
        return getText("admin.issue.constant.issuetype.lowercase");
    }

    protected Collection<IssueType> getConstants()
    {
        return ImmutableList.copyOf(issueTypeService.getIssueTypes(getLoggedInApplicationUser()));
    }

    protected void clearCaches()
    {
        getConstantsManager().refreshIssueTypes();
        fieldManager.refresh();
    }

    protected String getIssueConstantField()
    {
        return "type";
    }

    protected IssueType getConstant(String id)
    {
        return issueTypeService.getIssueType(getLoggedInApplicationUser(), id).getOrNull();
    }

    protected String getRedirectPage()
    {
        return "ViewIssues.jspa";
    }

    @RequiresXsrfCheck
    public String doAddIssueType() throws Exception
    {
        final boolean isSubTask = SubTaskManager.SUB_TASK_ISSUE_TYPE_STYLE.equals(getStyle());

        final IssueTypeCreateInput typeInput = IssueTypeCreateInput.builder()
                .setDescription(getDescription())
                .setName(getName())
                .setType(isSubTask ? SUBTASK : STANDARD)
                .build();

        final IssueTypeService.CreateValidationResult createIssueTypeValidationResult =
                issueTypeService.validateCreateIssueType(getLoggedInApplicationUser(), typeInput);

        if (createIssueTypeValidationResult.isValid())
        {
            issueTypeService.createIssueType(getLoggedInApplicationUser(), createIssueTypeValidationResult);

            eventPublisher.publish(new IssueTypeCreatedFromViewIssueTypesPageEvent());
            return redirectToView();
        }
        else
        {
            addErrors(createIssueTypeValidationResult.getErrorCollection().getErrors());
            return ERROR;
        }
    }

    public String doAddNewIssueType()
    {
        return INPUT;
    }
    
    @Override
    protected GenericValue addConstant() throws GenericEntityException
    {
        throw new UnsupportedOperationException("Use doAddIssueType command instead!");
    }

    protected String redirectToView()
    {
        return returnCompleteWithInlineRedirect("ViewIssueTypes.jspa");
    }

    protected String getDefaultPropertyName()
    {
        return APKeys.JIRA_CONSTANT_DEFAULT_ISSUE_TYPE;
    }

    @Override
    public ManageableOptionType getManageableOption()
    {
        return issueTypeManageableOption;
    }

    @Override
    public List<Pair<String, Object>> getHiddenFields()
    {
        return Collections.emptyList();
    }

    public String getActionType()
    {
        return "view";
    }

    public Collection getAllRelatedSchemes(String id)
    {
        return issueTypeSchemeManager.getAllRelatedSchemes(id);
    }

    public List getSchemes()
    {
        return configSchemeManager.getConfigSchemesForField(fieldManager.getIssueTypeField());
    }

    public FieldConfigScheme getDefaultScheme()
    {
        return issueTypeSchemeManager.getDefaultIssueTypeScheme();
    }

    @Override
    public String getStyle()
    {
        return style;
    }

    @Override
    public void setStyle(String style)
    {
        this.style = style;
    }
    
    @Override
    public String getSubmitUrl()
    {
        return "AddIssueType.jspa";
    }
    
    @Override
    public String getCancelUrl()
    {
        return "ViewIssueTypes.jspa";
    }
}
