package com.atlassian.jira.issue.attachment.store.provider;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.attachment.FileSystemAttachmentStore;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;

/**
 * An interface representing objects able to provide instances of {@link com.atlassian.jira.issue.attachment.StreamAttachmentStore}
 *
 * @since v6.4
 */
public interface StreamAttachmentStoreProvider
{
    StreamAttachmentStore getStreamAttachmentStore();

    Option<FileSystemAttachmentStore> getFileSystemStore();
}
