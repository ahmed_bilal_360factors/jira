package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.base.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static com.atlassian.jira.plugin.webfragment.conditions.RequestCachingConditionHelper.cacheConditionResultInRequest;

/**
 * Condition to check a permission against a given issue for the current user.
 * <p/>
 * An issue must be in the JiraHelper context params.
 *
 * @since v4.1
 */
public class HasIssuePermissionCondition extends AbstractPermissionCondition
{
    private static final Logger log = LoggerFactory.getLogger(HasIssuePermissionCondition.class);

    public HasIssuePermissionCondition(PermissionManager permissionManager)
    {
        super(permissionManager);
    }

    public boolean shouldDisplay(final ApplicationUser user, JiraHelper jiraHelper)
    {
        final Map<String, Object> params = jiraHelper.getContextParams();
        final Issue issue = (Issue) params.get("issue");

        if (issue == null)
        {
            log.warn("Trying to run permission condition on an issue, but no issue exists");
            return false;
        }

        final String cacheKey = getHasPermissionKey(permission, user, issue.getKey());

        return cacheConditionResultInRequest(cacheKey, new Supplier<Boolean>()
        {
            @Override
            public Boolean get()
            {
                return permissionManager.hasPermission(permission, issue, user);
            }
        });
    }

}
