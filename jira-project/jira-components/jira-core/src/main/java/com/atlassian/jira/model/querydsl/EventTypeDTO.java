package com.atlassian.jira.model.querydsl;

import javax.annotation.Generated;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;

import org.ofbiz.core.entity.GenericValue;

/**
 * Data Transfer Object for the EventType entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 *
 * @see QEventType
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class EventTypeDTO
{
    private Long id;
    private Long templateId;
    private String name;
    private String description;
    private String type;

    public Long getId()
    {
        return id;
    }

    public Long getTemplateId()
    {
        return templateId;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public String getType()
    {
        return type;
    }

    public EventTypeDTO(Long id, Long templateId, String name, String description, String type)
    {
        this.id = id;
        this.templateId = templateId;
        this.name = name;
        this.description = description;
        this.type = type;
    }
    /**
    * Creates a GenericValue object from the values in this Data Transfer Object.
    * <p>
    *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
    * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
    * @return a GenericValue object constructed from the values in this Data Transfer Object.
    */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator)
    {
        return ofBizDelegator.makeValue("EventType", new FieldMap()
                        .add("id", id)
                        .add("templateId", templateId)
                        .add("name", name)
                        .add("description", description)
                        .add("type", type)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     *   This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */

    public static EventTypeDTO fromGenericValue(GenericValue gv)
    {
        return new EventTypeDTO(
            gv.getLong("id"),
            gv.getLong("templateId"),
            gv.getString("name"),
            gv.getString("description"),
            gv.getString("type")
        );
    }
}

