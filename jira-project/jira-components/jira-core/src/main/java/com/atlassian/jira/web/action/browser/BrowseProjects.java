package com.atlassian.jira.web.action.browser;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Nullable;

import com.atlassian.fugue.Option;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.plugin.userformat.ProfileLinkUserFormat;
import com.atlassian.jira.plugin.userformat.UserFormats;
import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.SimpleLink;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.jira.web.SessionKeys;
import com.atlassian.jira.web.action.ActionViewData;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.atlassian.seraph.util.RedirectUtils;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import com.google.common.base.Function;
import com.google.common.base.Predicates;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
import org.ofbiz.core.entity.GenericEntityException;

import webwork.action.ActionContext;
import webwork.action.ServletActionContext;


/**
 * Action for Browse Projects
 */
public class BrowseProjects extends JiraWebActionSupport
{

    private final UserProjectHistoryManager projectHistoryManager;
    private final ProjectManager projectManager;
    private final PermissionManager permissionManager;
    private final SimpleLinkManager simpleLinkManager;
    private final WebInterfaceManager webInterfaceManager;
    private final PageBuilderService pageBuilderService;
    private final UserFormats userFormats;

    private static final String ALL = "all";
    private static final String NONE = "none";
    private static final String RECENT = "recent";

    private final Long PROJECT_DEFAULT_AVATAR_ID;

    /**
     * List of available categories as {@link com.atlassian.jira.web.action.browser.BrowseProjects.ProjectCategoryBean} objects.
     * <p/>
     * We filter out categories with no visible projects.
     * <p/>
     * We add a pseudo category for "none" No Category.
     * We add a pseudo category for "Recent Projects" if there are 2 or more categories.
     *
     */
    private final Supplier<List<ProjectCategoryBean>> categories = Suppliers.memoize(new Supplier<List<ProjectCategoryBean>>()
    {
        @Override
        public List<ProjectCategoryBean> get()
        {
            List<ProjectCategoryBean> categories = Lists.newArrayList();

            final Collection<ProjectCategory> projectCategories = projectManager.getAllProjectCategories();
            for (ProjectCategory projectCategory : projectCategories)
            {
                if (projectCategory != null)
                {
                    final Collection<Project> projects = permissionManager.getProjects(ProjectPermissions.BROWSE_PROJECTS, getLoggedInApplicationUser(), projectCategory);
                    if (projects != null && !projects.isEmpty())
                    {
                        categories.add(new ProjectCategoryBean(projectCategory));
                    }
                }
            }

            if (!categories.isEmpty())
            {
                //We only add "no category" if there are other categories
                final Collection<Project> noCategoryProjects = permissionManager.getProjects(ProjectPermissions.BROWSE_PROJECTS, getLoggedInApplicationUser(), null);
                if (!noCategoryProjects.isEmpty())
                {
                    categories.add(new ProjectCategoryBean(getText("browse.projects.none"), getText("browse.projects.none.desc"), NONE));
                }
            }

            if (!recentProjects.get().isEmpty())
            {
                categories.add(0, new ProjectCategoryBean(getText("browse.projects.recent"), getText("browse.projects.recent.desc"), RECENT));
            }

            categories.add(new ProjectCategoryBean(getText("browse.projects.all"), getText("browse.projects.all.desc"), ALL));

            return ImmutableList.copyOf(categories);
        }
    });
    private Supplier<List<ProjectBean>> projects = Suppliers.memoize(new Supplier<List<ProjectBean>>()
    {
        @Override
        public List<ProjectBean> get()
        {
            final Collection<Project> allProjects = permissionManager.getProjects(ProjectPermissions.BROWSE_PROJECTS, getLoggedInApplicationUser());

            return Lists.newArrayList(
                Iterables.filter(
                    Iterables.transform(allProjects, new Function<Project, ProjectBean>()
                    {
                        @Override
                        public ProjectBean apply(@Nullable final Project project)
                        {
                            if (project == null)
                            {
                                return null;
                            }
                            else
                            {
                                return new ProjectBean(project, recentProjects.get().contains(project));
                            }
                        }
                    }),
                    Predicates.notNull()
            ));
        }
    });
    private final Supplier<List<Project>> recentProjects = Suppliers.memoize(new Supplier<List<Project>>()
    {
        @Override
        public List<Project> get()
        {
            return ImmutableList.copyOf(projectHistoryManager.getProjectHistoryWithPermissionChecks(Permissions.BROWSE, getLoggedInUser()));

        }
    });
    private String selectedCategory;

    public BrowseProjects(final UserProjectHistoryManager projectHistoryManager,
            final ProjectManager projectManager,
            final PermissionManager permissionManager,
            final SimpleLinkManager simpleLinkManager,
            final WebInterfaceManager webInterfaceManager,
            final AvatarManager avatarManager,
            final PageBuilderService pageBuilderService, final UserFormats userFormats)
    {
        this.projectHistoryManager = projectHistoryManager;
        this.projectManager = projectManager;
        this.permissionManager = permissionManager;
        this.simpleLinkManager = simpleLinkManager;
        this.webInterfaceManager = webInterfaceManager;
        this.pageBuilderService = pageBuilderService;
        this.userFormats = userFormats;

        PROJECT_DEFAULT_AVATAR_ID = avatarManager.getDefaultAvatarId(Avatar.Type.PROJECT);
    }

    @Override
    protected String doExecute() throws Exception
    {
        if (!projects.get().isEmpty()) {
            pageBuilderService.assembler().resources().requireWebResource("jira.webresources:browseprojects");
            pageBuilderService.assembler().resources().requireContext("jira.browse");
            pageBuilderService.assembler().resources().requireContext("jira.browse.projects");

            //prepare data for Backbone application
            pageBuilderService.assembler().data().requireData("com.atlassian.jira.project.browse:categories", getCategoriesJsonable());
            pageBuilderService.assembler().data().requireData("com.atlassian.jira.project.browse:projects", getProjectsJsonable());
            pageBuilderService.assembler().data().requireData("com.atlassian.jira.project.browse:selectedCategory", getSelectedCategory());

            return super.doExecute();
        }
        else {
            return ERROR;
        }
    }

    @ActionViewData("success")
    public Collection<SimpleLink> getOperationLinks()
    {
        return simpleLinkManager.getLinksForSection("system.browse.projects.operations", getLoggedInUser(), getJiraHelper());
    }

    private JiraHelper getJiraHelper()
    {
        final Map<String, Object> params = Maps.newHashMap();
        return new JiraHelper(ServletActionContext.getRequest(), null, params);
    }

    @ActionViewData("success")
    public String getInfoPanelHtml()
    {
        final StringBuilder sb = new StringBuilder();
        final List<WebPanelModuleDescriptor> webPanelDescriptors = webInterfaceManager.getDisplayableWebPanelDescriptors("webpanels.browse.projects.info-panels", Collections.<String, Object>emptyMap());
        for (final WebPanelModuleDescriptor webPanelDescriptor : webPanelDescriptors)
        {
            final Option<String> result = SafePluginPointAccess.call(new Callable<String>()
            {
                @Override
                public String call() throws Exception
                {
                    return webPanelDescriptor.getModule().getHtml(Collections.<String, Object>emptyMap());
                }
            });
            if (result.isDefined())
            {
                sb.append(result.get());
            }
        }
        return sb.toString();
    }
    @ActionViewData("error")
    public Collection<String> getErrorMessages()
    {
        final List<String> errors = Lists.newArrayList();
        if ("true".equals(getApplicationProperties().getString(APKeys.JIRA_SETUP)))
        {
            boolean projectsEmpty = projectManager.getProjectObjects().isEmpty();
            if (getLoggedInApplicationUser() == null && !projectsEmpty)
            {
                errors.add(getText("noprojects.notloggedin"));
                String loginLink = "<a href=\"" + RedirectUtils.getLinkLoginURL(getHttpRequest()) + "\">" + getText("common.words.login") + "</a>";
                String logInOrSignup = getText("noprojects.mustfirstlogin", loginLink);
                if (JiraUtils.isPublicMode())
                {
                    logInOrSignup += " " + getText("noprojects.signup",
                            "<a href=\"" + getHttpRequest().getContextPath() + "/secure/Signup!default.jspa\">", "</a>");
                }
                errors.add(logInOrSignup);
            }
            else if (!projectsEmpty)
            {
                errors.add(getText("noprojects.nopermissions"));
                if (hasGlobalPermission(GlobalPermissionKey.ADMINISTER))
                {
                    String linkToProjectsAdminPage = getHttpRequest().getContextPath() + "/secure/admin/default.jsp";
                    errors.add(getText("noprojects.viewallprojects.message", "<a href=\"" + linkToProjectsAdminPage + "\">", "</a>"));
                }
                else
                {
                    errors.add(getText("noprojects.contactadmin.permissions", getAdministratorContactLink()));
                }
            }
            else
            {
                errors.add(getText("noprojects"));
                if (hasGlobalPermission(GlobalPermissionKey.ADMINISTER))
                {
                    errors.add("<a class=\"add-project-trigger\" href=\"" + getHttpRequest().getContextPath() +
                            "/secure/admin/AddProject!default.jspa?nextAction=browseprojects&src=noprojectsmessage\">" +
                            getText("noprojects.createprojectnow.link") + "</a>");
                }
                else
                {
                    errors.add(getText("noprojects.contactadmin.permissions", getAdministratorContactLink()));
                }
            }
        }
        else
        {
            errors.add(getText("noprojects.mustsetupfirst"));
            errors.add(getText("noprojects.createadmintocreateotheradmins"));
            errors.add("<a href=\"" + getHttpRequest().getContextPath() + "/secure/Setup!default.jspa\">" +
                    getText("noprojects.setupjira.link") + "</a>");
        }
        return errors;
    }

    /**
     * Get the currently active Category.  Looks for it in the session, if it is "all" return it.
     * Else, check to see if the category exists and we can see projects in it.
     * Else, return the first category.
     *
     * @return the currently active category id, "all", "none" or "recent".
     * @throws GenericEntityException Sorry, yes we are using GVs.
     */
    public String getSelectedCategory() throws GenericEntityException
    {
        if (selectedCategory != null)
        {
            return selectedCategory;
        }

        String sessionCategory = getSession().get(SessionKeys.BROWSE_PROJECTS_CURRENT_TAB);

        if (ALL.equals(sessionCategory))
        {
            return sessionCategory;
        }

        String returnKey = null;
        for (ProjectCategoryBean category : categories.get())
        {
            if (returnKey == null || category.getId().equals(sessionCategory))
            {
                returnKey = category.getId();
            }
        }

        selectedCategory = returnKey == null ? ALL : returnKey;
        return selectedCategory;
    }

    private Map<String, String> getSession()
    {
        return ActionContext.getSession();
    }

    public Jsonable getCategoriesJsonable() throws GenericEntityException
    {
        return new Jsonable()
        {
            @Override
            public void write(final Writer writer) throws IOException
            {
                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
                mapper.writeValue(writer, categories.get());
            }
        };
    }

    public Jsonable getProjectsJsonable()
    {
        return new Jsonable()
        {
            @Override
            public void write(final Writer writer) throws IOException
            {
                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
                mapper.writeValue(writer, projects.get());
            }
        };
    }


    private static String convertCategoryToDescription(final ProjectCategory category)
    {
        final String name = category.getName();
        final String desc = category.getDescription();
        if (StringUtils.isBlank(desc))
        {
            return name;
        }
        else
        {
            return name + " - " + desc;
        }
    }

    public boolean hasDefaultAvatar(Project project)
    {
        final Long avatarId = project.getAvatar().getId();
        return avatarId == null || avatarId.equals(PROJECT_DEFAULT_AVATAR_ID);
    }

    public void setSelectedCategory(final String selectedCategory)
    {
        this.selectedCategory = selectedCategory;
    }

    /**
     * Simple bean that contains all needed Project information
     */

    public class ProjectBean
    {
        private final String key;
        private final String name;
        private final Long id;
        private final boolean hasDefaultAvatar;
        private final boolean recent;
        private final String lead;
        private final String leadProfileLink;
        private final Long projectCategoryId;
        private final String url;

        public ProjectBean(final Project project, final boolean recent)
        {
            this(project.getKey(), project.getName(), project.getId(), hasDefaultAvatar(project), project.getProjectLead(),
                    project.getProjectCategoryObject(), recent, project.getUrl());
        }

        public ProjectBean(final String key, final String name, final Long id, final boolean hasDefaultAvatar,
                final ApplicationUser lead, final ProjectCategory projectCategory, final boolean recent, final String url)
        {
            this.key = key;
            this.name = name;
            this.id = id;
            this.hasDefaultAvatar = hasDefaultAvatar;
            this.recent = recent;
            if (lead != null)
            {
                this.lead = lead.getKey();
                this.leadProfileLink = userFormats.formatter(ProfileLinkUserFormat.TYPE).formatUserkey(this.lead, "");
            }
            else {
                this.lead = null;
                this.leadProfileLink = null;
            }
            this.url = url;
            if (projectCategory != null)
            {
                this.projectCategoryId = projectCategory.getId();
            }
            else
            {
                this.projectCategoryId = null;
            }
        }

        public String getKey()
        {
            return key;
        }

        public String getName()
        {
            return name;
        }

        public Long getId()
        {
            return id;
        }

        public boolean isHasDefaultAvatar()
        {
            return hasDefaultAvatar;
        }

        public boolean isRecent()
        {
            return recent;
        }

        @Nullable
        public String getLead()
        {
            return lead;
        }

        public String getUrl()
        {
            return url;
        }

        @Nullable
        public String getLeadProfileLink()
        {
            return leadProfileLink;
        }

        @Nullable
        public Long getProjectCategoryId()
        {
            return projectCategoryId;
        }
    }

    /**
     * Simple bean that contains Project category information and its containing projects as GVS.
     */
    public class ProjectCategoryBean
    {
        private final String name;
        private final String description;
        private final String id;

        public ProjectCategoryBean(ProjectCategory category)
        {
            this(category.getName(), convertCategoryToDescription(category), String.valueOf(category.getId()));
        }

        public ProjectCategoryBean(final String name, final String description, final String id)
        {
            this.name = name;
            this.description = description;
            this.id = id;
        }

        public String getName()
        {
            return name;
        }

        public String getDescription()
        {
            return description;
        }

        public String getId()
        {
            return id;
        }
    }
}