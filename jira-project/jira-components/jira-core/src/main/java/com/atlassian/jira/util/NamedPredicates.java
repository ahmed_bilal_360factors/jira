package com.atlassian.jira.util;

import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

public class NamedPredicates
{
    public static com.google.common.base.Predicate<Named> equalsIgnoreCase(final String name)
    {
        return new com.google.common.base.Predicate<Named>()
        {
            @Override
            public boolean apply(@Nullable final Named version)
            {
                return StringUtils.equalsIgnoreCase(notNull(version).getName(), name);
            }
        };
    }
}

