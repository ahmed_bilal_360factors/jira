package com.atlassian.jira.issue.attachment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import com.atlassian.fugue.Either;

/**
 * Maps {@link com.atlassian.jira.issue.attachment.Attachment} to {@link com.atlassian.jira.issue.attachment.StoreAttachmentBean}
 *
 * @since v6.4
 */
public class StoreAttachmentBeanMapper
{

    public StoreAttachmentBean mapToBean(final Attachment metadata, final InputStream source)
    {
        final StoreAttachmentBean.Builder builder = new StoreAttachmentBean.Builder(source);
        return mapAttachmentToStoreBean(metadata, builder);
    }

    public Either<FileNotFoundException, StoreAttachmentBean> mapToBean(final Attachment metadata, final File source)
    {
        try
        {
            final FileInputStream inputStream = new FileInputStream(source);
            final StoreAttachmentBean.Builder builder = new StoreAttachmentBean.Builder(inputStream);
            return Either.right(mapAttachmentToStoreBean(metadata, builder));
        }
        catch (final FileNotFoundException exception)
        {
            return Either.left(exception);
        }
    }

    private StoreAttachmentBean mapAttachmentToStoreBean(final Attachment metadata, final StoreAttachmentBean.Builder builder)
    {
        return builder
                .withId(metadata.getId())
                .withFileName(metadata.getFilename())
                .withSize(metadata.getFilesize())
                .withIssueKey(metadata.getIssue().getKey())
                .withOriginalProjectKey(metadata.getIssue().getProjectObject().getOriginalKey())
                .build();
    }

}
