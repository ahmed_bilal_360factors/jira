package com.atlassian.jira.issue.attachment.store.strategy;

import com.atlassian.jira.issue.attachment.NoAttachmentDataException;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

/**
 * Operation strategy that will ignore any {@link com.atlassian.jira.issue.attachment.NoAttachmentDataException} that
 * is thrown from primary store, in this case the operation on secondary store will be treated as primary and the error
 * from primary will be silently ignored
 *
 * @since v6.4
 */
public class FailoverOnMissingOperationStrategy implements StoreOperationStrategy
{
    private final StreamAttachmentStore primaryStore;
    private final StreamAttachmentStore secondaryStore;

    public FailoverOnMissingOperationStrategy(final StreamAttachmentStore primaryAttachmentStore, final StreamAttachmentStore secondaryAttachmentStore)
    {
        this.primaryStore = primaryAttachmentStore;
        this.secondaryStore = secondaryAttachmentStore;
    }

    @Override
    public <V> Promise<V> perform(final Function<StreamAttachmentStore, Promise<V>> operation)
    {

        //The logic is as follows:
        //1. try to get attachment from primary store
        //1.1 on success perform operation in background from secondary store (this is used to trigger fake load on secondary store)
        //1.2 on error go to 2
        //2. try to perform from secondary store (this requires creating second level of Promises this is why there is flatMap in play
        //3. replace the result with operation on secondary store
        return operation.get(primaryStore)
                .fold(new com.google.common.base.Function<Throwable, Promise<V>>()
                {
                    @Override
                    public Promise<V> apply(final Throwable throwableFromPrimary)
                    {
                        if (throwableFromPrimary instanceof NoAttachmentDataException)
                        {
                            return operation.get(secondaryStore);
                        }
                        else
                        {
                            return Promises.rejected(throwableFromPrimary);
                        }
                    }
                }, new com.google.common.base.Function<V, Promise<V>>()
                {
                    @Override
                    public Promise<V> apply(final V input)
                    {
                        operation.get(secondaryStore);
                        return Promises.promise(input);
                    }
                }).flatMap(com.google.common.base.Functions.<Promise<V>>identity());
    }
}
