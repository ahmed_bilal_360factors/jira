package com.atlassian.jira.index.ha;

import com.atlassian.core.util.DateUtils;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.task.TaskProgressSink;
import com.atlassian.jira.util.collect.Sized;
import com.atlassian.jira.web.action.admin.index.IndexCommandResult;

import java.io.File;

/**
 * Manager to recover an index from a previous index backup
 *
 * @since v6.2
 */
public interface IndexRecoveryManager extends Sized
{
    /**
     * Recovers an index from an index backup.
     *
     * @param recoveryFile The backup file
     * @param taskProgressSink
     * @throws IndexException If we are unable to recover the index
     */
    IndexCommandResult recoverIndexFromBackup(File recoveryFile, TaskProgressSink taskProgressSink) throws IndexException;


    /**
     * Gets the updated date range for issues that need to be re-indexed because the latest updates are missing from the index.
     * The range is
     * <dl>
     *     <dt>from</dt><dd>Latest date in the index</dd>
     *     <dt>to</dt><dd>Latest date in the database</dd>
     * </dl>
     * The range can actually be negative, indicating there are indexed updates which do not belong and so may need to be removed.
     *
     * @return Date range of issues that need to be re-indexed.
     * @since v6.4
     */
    DateUtils.DateRange getDurationToRecover();

    /**
     * Reindex issues with last updated date within the following range
     * The range is
     * <dl>
     *     <dt>from</dt><dd>Latest date in the index</dd>
     *     <dt>to</dt><dd>Latest date in the database</dd>
     * </dl>
     * The range can actually be negative, indicating there are indexed updates which do not belong and so may need to be removed.
     * @param range Date range to recover
     * @param taskProgressSink A progress sink.
     * @throws IndexException
     * @throws SearchException
     * @since v6.4
     */
    void reindexIssuesIn(DateUtils.DateRange range, TaskProgressSink taskProgressSink)
            throws IndexException, SearchException;
}
