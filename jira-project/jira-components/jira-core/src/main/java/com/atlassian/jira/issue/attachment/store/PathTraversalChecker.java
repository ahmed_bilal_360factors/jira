package com.atlassian.jira.issue.attachment.store;

import java.io.File;

import com.atlassian.fugue.Option;
import com.atlassian.jira.util.PathTraversalRuntimeException;
import com.atlassian.jira.util.PathUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PathTraversalChecker
{
    private static final Logger LOGGER = LoggerFactory.getLogger(PathTraversalChecker.class);

    public Option<Exception> validateFileInSecureDirectory(final File untrustedFile, final File secureDirectory)
    {
        try
        {
            final boolean pathInSecureDir = PathUtils.isPathInSecureDir(secureDirectory, untrustedFile);
            if(pathInSecureDir)
            {
                return Option.none();
            }
            else
            {
                LOGGER.warn("Detected PathTraversal attack attempt! SecureDirectory: [ {} ]  UntrustedFile: [ {} ]",
                        secureDirectory.getAbsolutePath(), untrustedFile.getAbsolutePath());
                return Option.<Exception>some(new PathTraversalRuntimeException());
            }
        }
        catch (final Exception exception)
        {
            return Option.some(exception);
        }
    }
}
