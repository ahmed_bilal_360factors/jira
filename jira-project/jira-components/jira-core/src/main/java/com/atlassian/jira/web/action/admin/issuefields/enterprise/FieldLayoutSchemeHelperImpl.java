package com.atlassian.jira.web.action.admin.issuefields.enterprise;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.fields.layout.field.EditableFieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutScheme;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.Function;
import com.atlassian.jira.util.InjectableComponent;
import com.atlassian.jira.util.collect.CollectionUtil;
import com.atlassian.query.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since v4.0
 */
@InjectableComponent
public class FieldLayoutSchemeHelperImpl implements FieldLayoutSchemeHelper
{
    private static final Logger log = LoggerFactory.getLogger(FieldLayoutSchemeHelperImpl.class);
    private static final Function<Project, Long> PROJECT_TO_ID_FUNCTION = new Function<Project, Long>()
    {
        @Override
        public Long get(final Project input)
        {
            return input.getId();
        }
    };

    private final FieldLayoutManager fieldLayoutManager;
    private final SearchProvider searchProvider;

    public FieldLayoutSchemeHelperImpl(final FieldLayoutManager fieldLayoutManager, final SearchProvider searchProvider)
    {
        this.fieldLayoutManager = notNull("fieldLayoutManager", fieldLayoutManager);
        this.searchProvider = notNull("searchProvider", searchProvider);
    }

    public boolean doesChangingFieldLayoutAssociationRequireMessage(final User user, final FieldLayoutScheme fieldLayoutScheme, final Long oldFieldLayoutId, final Long newFieldLayoutId)
    {
        boolean messageRequired = false;
        if (!fieldLayoutManager.isFieldLayoutsVisiblyEquivalent(oldFieldLayoutId, newFieldLayoutId))
        {
            messageRequired = doProjectsHaveIssues(user, fieldLayoutScheme.getProjectsUsing());
        }

        return messageRequired;
    }

    public boolean doesChangingFieldLayoutRequireMessage(final User user, final EditableFieldLayout fieldLayout)
    {
        return doProjectsHaveIssues(user, fieldLayoutManager.getProjectsUsing(fieldLayout));
    }

    public boolean doesChangingFieldLayoutSchemeForProjectRequireMessage(final User user, final Long projectId, final Long oldFieldLayoutSchemeId, final Long newFieldLayoutSchemeId)
    {
        boolean messageRequired = false;
        if (!fieldLayoutManager.isFieldLayoutSchemesVisiblyEquivalent(oldFieldLayoutSchemeId, newFieldLayoutSchemeId))
        {
            messageRequired = doProjectIdsHaveIssues(user, Collections.singletonList(projectId));
        }
        return messageRequired;
    }

    /**
     * @param user the user
     * @param projects the projects to check; if empty the result will be false.
     * @return if there are any issues in the scheme's associated projects
     */
    private boolean doProjectsHaveIssues(final User user, final Collection<Project> projects)
    {
        final List<Long> projectIds = CollectionUtil.transform(projects, PROJECT_TO_ID_FUNCTION);
        return doProjectIdsHaveIssues(user, projectIds);
    }

    private boolean doProjectIdsHaveIssues(final User user, final Collection<Long> projectIds)
    {
        if (projectIds.isEmpty())
        {
            return false;
        }
        final JqlClauseBuilder builder = JqlQueryBuilder.newClauseBuilder().project().inNumbers(projectIds);
        return doesQueryHaveIssues(user, builder.buildQuery());
    }

    private boolean doesQueryHaveIssues(final User user, final Query query)
    {
        try
        {
            final long issueCount = searchProvider.searchCountOverrideSecurity(query, user);
            return (issueCount > 0);
        }
        catch (SearchException e)
        {
            log.warn(e.getMessage(), e);
            // can't determine whether or not there are issues - but let's just pretend there are
            return true;
        }
    }
}
