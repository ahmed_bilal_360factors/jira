package com.atlassian.jira.entity;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.permission.PermissionSchemeEntry;

import org.ofbiz.core.entity.GenericValue;

/**
 * EntityFactory for PermissionSchemeEntry
 *
 * @since v6.4
 */
public class PermissionSchemeEntryFactory extends AbstractEntityFactory<PermissionSchemeEntry>
{
    @Override
    public String getEntityName()
    {
        return "SchemePermissions";
    }

    @Override
    public PermissionSchemeEntry build(GenericValue gv)
    {
        if (gv == null)
        {
            return null;
        }

        //    <field name="id" type="numeric"/>
        //
        //    <field name="scheme" type="numeric"/>
        //    <field name="permission" type="numeric"/>
        //    <field name="type" col-name="perm_type" type="long-varchar"/>
        //    <field name="parameter" col-name="perm_parameter" type="long-varchar"/>
        //    <field name="permissionKey" type="long-varchar"/>

        return new PermissionSchemeEntry(
                gv.getLong("id"),
                gv.getLong("scheme"),
                gv.getString("permissionKey"),
                gv.getString("type"),
                gv.getString("parameter")
        );
    }

    @Override
    public FieldMap fieldMapFrom(PermissionSchemeEntry permissionSchemeEntry)
    {
        return new FieldMap("id", permissionSchemeEntry.getId())
                .add("scheme", permissionSchemeEntry.getSchemeId())
                .add("permissionKey", permissionSchemeEntry.getPermissionKey())
                .add("type", permissionSchemeEntry.getType())
                .add("parameter", permissionSchemeEntry.getParameter());
    }
}
