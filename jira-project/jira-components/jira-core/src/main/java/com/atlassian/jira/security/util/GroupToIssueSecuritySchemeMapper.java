package com.atlassian.jira.security.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeEntity;

public class GroupToIssueSecuritySchemeMapper extends AbstractGroupMapper
{
    private final IssueSecuritySchemeManager issueSecuritySchemeManager;

    public GroupToIssueSecuritySchemeMapper(IssueSecuritySchemeManager issueSecuritySchemeManager)
    {
        this.issueSecuritySchemeManager = issueSecuritySchemeManager;
        setGroupMapping(init());
    }

    /**
     * Go through all the Issue Security Schemes and create a Map, where the key is the group name
     * and values are Sets of Schemes
     */
    private Map<String, Set<Scheme>> init()
    {
        Map<String, Set<Scheme>> mapping = new HashMap<String, Set<Scheme>>();
        // Get all Permission Schmes
        final List<Scheme> schemes = issueSecuritySchemeManager.getSchemeObjects();
        for (Scheme issueSecurityScheme : schemes)
        {
            Collection<SchemeEntity> entities = issueSecurityScheme.getEntities();
            for (SchemeEntity entity : entities)
            {
                if ("group".equals(entity.getType()))
                {
                    addEntry(mapping, entity.getParameter(), issueSecurityScheme);
                }
            }
        }
        return mapping;
    }
}
