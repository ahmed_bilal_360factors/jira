package com.atlassian.jira;

import com.atlassian.jira.i18n.BootstrapJiraAuthenticationContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.login.BootstrapLoginManagerImpl;
import com.atlassian.jira.security.login.LoginManager;
import com.atlassian.jira.security.xsrf.BootstrapXsrfTokenGenerator;
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
import com.atlassian.jira.web.pagebuilder.BootstrapPageBuilderServiceSpi;
import com.atlassian.jira.web.pagebuilder.PageBuilderServiceSpi;

import static com.atlassian.jira.ComponentContainer.Scope.INTERNAL;
import static com.atlassian.jira.ComponentContainer.Scope.PROVIDED;

/**
 * If JIRA fails its Startup checks, then we use this to add just a few more components to the Pico Container.
 * <p>
 *     For example, if the database build number is higher than the application code build number, we fail startup.
 * </p>
 * <p>
 *     SetupContainerRegistrar does a similar trick of adding components to exising bootstrap container.
 *     But SetupContainerRegistrar requirements would clash with the requirements below, so we need to ensure we only
 *     extend Bootstrap with one or the other.
 * </p>
 * <p>
 *     JRA-43308: We need to add just enough components such that the Servlet Filters can work without throwing an NPE,
 *     and we need a minimal ability of localisation for the errors.jsp page.
 * </p>
 *
 * @see BootstrapContainerRegistrar
 * @see SetupContainerRegistrar
 */
class FailedStartupContainerRegistrar
{
    public void registerComponents(final ComponentContainer register)
    {
        // JRA-43308: The XsrfTokenAdditionRequestFilter Servlet Filter needs an XsrfTokenGenerator
        register.implementation(PROVIDED, XsrfTokenGenerator.class, BootstrapXsrfTokenGenerator.class);

        // JRA-43308: Seraph's SecurityFilter Servlet Filter will call JiraSeraphSecurityService.getRequiredRoles()
        // and this calls LoginManager.getRequiredRoles()
        register.implementation(INTERNAL, LoginManager.class, BootstrapLoginManagerImpl.class);

        // JRA-43308: PageBuilderFilter Servlet Filter will call PageBuilderServiceSpi
        register.implementation(INTERNAL, PageBuilderServiceSpi.class, BootstrapPageBuilderServiceSpi.class);

        // JRA-43308: JiraAuthenticationContext is used inside errors.jsp which we use to show Startup Check failures.
        // (eg "Failed to start JIRA due to a build number inconsistency".)
        register.implementation(PROVIDED, JiraAuthenticationContext.class, BootstrapJiraAuthenticationContext.class);
    }
}
