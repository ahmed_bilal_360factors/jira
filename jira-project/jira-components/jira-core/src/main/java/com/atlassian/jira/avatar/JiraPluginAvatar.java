package com.atlassian.jira.avatar;

import java.io.IOException;
import java.io.InputStream;

/**
 * Representation of an Avatar image for a user (or possibly some other application specific entity) which is defined
 * through a plugin. The URL may be hosted on an external system such that some metadata about the Avatar are not known
 * by the implementation, for example whether it is the default avatar, provided by the system or user-customised.
 */
public interface JiraPluginAvatar
{

    /**
     * String representation of the primary unique id of the user (or other entity) that the Avatar belongs to.
     *
     * @return the id.
     */
    public String getOwnerId();

    /**
     * Return an absolute URL that can be used to fetch the image data for this Avatar. The URL does not have to be
     * internet-accessible, but if it is not, then {@link #isExternal()} must return false.
     *
     * @return the URL.
     */
    public String getUrl();

    /**
     * The t-shirt size following AUI {@link com.atlassian.plugins.avatar.AuiSize size} definitions, e.g. xsmall,
     * medium, xxlarge. See .
     *
     * @return the size.
     */
    public int getSize();

    /**
     * The MIME type for the Avatar.
     *
     * @return image/png or whatever.
     */
    public String getContentType();

    /**
     * Whether the URL for the Avatar is hosted on an external and internet-accessible service like Gravatar, Atlassian
     * ID avatars or some other universally accessible service. If uncertain, return false. Note this also implies that
     * it would probably be silly to call {@link #getBytes()}.
     *
     * @return false unless the server behind this Avatar is guaranteed internet-accessible.
     */
    public boolean isExternal();

    /**
     * Provides an InputStream for reading the image data. Callers must close the stream. DO NOT call this method if the
     * URL is public unless you somehow know that the configuration is OK with this. External Avatars implies the bytes
     * could be read off the network which the caller can always do for themselves if they really need them.
     * Implementations may decide to do this but the caller should take responsibility. Alternately, implementations may
     * throw IOException to indicate the bytes are unavailable or contraband.
     *
     * @return a stream for reading the image data.
     * @throws java.io.IOException if called when returns true or if the bytes cannot be found.
     */
    public InputStream getBytes() throws IOException;
}
