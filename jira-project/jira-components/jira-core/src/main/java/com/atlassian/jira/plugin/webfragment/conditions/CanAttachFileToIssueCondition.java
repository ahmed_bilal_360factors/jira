package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.issue.attachment.AttachmentService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.SimpleErrorCollection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Condition that determines whether the current user can attach a file to the current issue.
 * <p/>
 * An issue must be in the JiraHelper context params.
 *
 * @since v4.1
 */
public class CanAttachFileToIssueCondition extends AbstractIssueWebCondition
{
    private static final Logger log = LoggerFactory.getLogger(CanAttachFileToIssueCondition.class);
    private final AttachmentService attachmentService;


    public CanAttachFileToIssueCondition(AttachmentService attachmentService)
    {
        this.attachmentService = attachmentService;
    }

    public boolean shouldDisplay(ApplicationUser user, Issue issue, JiraHelper jiraHelper)
    {
        JiraServiceContext context = new JiraServiceContextImpl(user, new SimpleErrorCollection());
        return attachmentService.canCreateAttachments(context, issue);
    }

}
