package com.atlassian.jira.issue.fields.rest.json;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.issue.fields.option.Option;
import com.atlassian.jira.issue.fields.rest.json.beans.SuggestionBean;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.BaseUrl;
import com.google.common.collect.Lists;
import edu.umd.cs.findbugs.annotations.Nullable;
import org.apache.commons.lang3.StringUtils;

import java.net.URI;
import java.util.Collection;

/**
 * Factory for converting {@link com.atlassian.jira.project.Project} and {@link com.atlassian.jira.issue.fields.option.Option}
 * into {@link com.atlassian.jira.issue.fields.rest.json.beans.SuggestionBean} suitable for use as suggestion for SingleSelect fields
 *
 * @since 6.4
 */
public class SuggestionBeanFactory
{
    private final AvatarService avatarService;
    private final BaseUrl baseUrl;

    public SuggestionBeanFactory(final AvatarService avatarService, final BaseUrl baseUrl)
    {
        this.avatarService = avatarService;
        this.baseUrl = baseUrl;
    }

    public Collection<SuggestionBean> projectSuggestions(Collection<Project> projects, Long selectedProjectId)
    {
        Collection<SuggestionBean> result = Lists.newArrayListWithCapacity(projects.size());
        for (Project project : projects)
        {
            result.add(this.projectSuggestion(project, project.getId().equals(selectedProjectId)));
        }

        return result;
    }

    public Collection<SuggestionBean> optionSuggestions(Collection<Option> options, String selectedOptionId)
    {
        Collection<SuggestionBean> result = Lists.newArrayListWithCapacity(options.size());
        for (Option option: options)
        {
            result.add(this.optionSuggestion(option, option.getId().equals(selectedOptionId)));
        }

        return result;
    }

    private SuggestionBean optionSuggestion(Option option, boolean isSelected)
    {
        final String iconUri = toIconURI(option.getImagePath());
        return new SuggestionBean(option.getName(), option.getId(), iconUri, isSelected);
    }

    private SuggestionBean projectSuggestion(Project project, boolean isSelected)
    {
        final String label = project.getName() + " (" + project.getKey() + ")";
        final String value = project.getId().toString();
        final String icon = avatarService.getProjectAvatarAbsoluteURL(project, Avatar.Size.SMALL).toString();
        return new SuggestionBean(label, value, icon, isSelected);
    }

    @Nullable
    private String toIconURI(@Nullable String imagePath)
    {
        if (imagePath == null)
        {
            return null;
        }
        try {
            final String relativeImagePath = StringUtils.stripStart(imagePath.trim(), "/");
            final URI imageURI = new URI(relativeImagePath);
            return baseUrl.getBaseUri().resolve(imageURI).toString();
        }
        catch (Exception e)
        {
            return null;
        }
    }

}
