package com.atlassian.jira.web.pagebuilder;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * No op PageBuilderServiceSpi to be used in the Bootstrap container, because it is called from Servlet Filters.
 * See JRA-43308
 *
 * @since v6.4.5
 */
public class BootstrapPageBuilderServiceSpi implements PageBuilderServiceSpi
{
    @Override
    public void initForRequest(final HttpServletRequest request, final HttpServletResponse response, final DecoratorListener decoratorListener, final ServletContext servletContext)
    {
        // no-op
    }

    @Override
    public void clearForRequest()
    {
        // no-op
    }

    @Override
    public PageBuilderSpi getSpi()
    {
        throw new IllegalStateException("Not implemented in the Bootstrap Container");
    }
}
