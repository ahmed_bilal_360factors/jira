package com.atlassian.jira.index.ha;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.Date;
import java.util.Properties;

import javax.annotation.Nullable;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Reindex metadata contained within an index snapshot. The metadata is stored in a properties file contributed to the
 * snapshot zip.
 */
public class ReindexMetadata
{
    private static final Logger log = LoggerFactory.getLogger(ReindexMetadata.class);

    /**
     * filename of the metadata file
     */
    private static final String METADATA_FILENAME = "reindex-metadata.properties";

    /**
     * Key containing the start time of the index (millis since epoch)
     */
    private static final String INDEX_START_TIME_KEY = "index.start.time";

    /**
     * The loaded properties
     */
    private Properties properties;

    /**
     * Create a new, empty, metadata object
     */
    public ReindexMetadata()
    {
        properties = new Properties();
    }

    /**
     * Load the metadata from an existing metadata file contained within this index directory. If the metadata file
     * doesn't exist, that's ok.
     *
     * @param indexDir the directory containing the snapshot files
     * @throws java.io.IOException if indexDir is not a directory, or cannot be read
     */
    public ReindexMetadata(File indexDir) throws IOException
    {
        if (!indexDir.isDirectory() || !indexDir.canRead())
        {
            throw new IOException(indexDir + " is not a readable directory");
        }

        File metadataFile = new File(indexDir, METADATA_FILENAME);
        if (metadataFile.exists())
        {
            FileReader in = new FileReader(metadataFile);
            try
            {
                loadProperties(in);
            }
            finally
            {
                IOUtils.closeQuietly(in);
            }
        }
    }

    /**
     * Load the properties from the input stream
     *
     * @param in the input stream
     */
    private void loadProperties(final Reader in) throws IOException
    {
        properties = new Properties();
        properties.load(in);
    }

    /**
     * @return the start time of the index, or null if the metadata does not define one
     */
    @Nullable
    public Date getIndexStartTime()
    {
        Date startDate;

        String startTimeValue = properties.getProperty(INDEX_START_TIME_KEY);
        if (startTimeValue != null && !startTimeValue.isEmpty())
        {
            try
            {
                startDate = new Date(Long.parseLong(startTimeValue));
            }
            catch (NumberFormatException e)
            {
                log.error("Invalid value for key " + INDEX_START_TIME_KEY + " in index snapshot metadata", e);
                startDate = null;
            }
        }
        else
        {
            startDate = null;
        }
        return startDate;
    }

    /**
     * @param indexStartTime the index start time, in milliseconds since epoch
     */
    public void setIndexStartTime(final long indexStartTime)
    {
        properties.setProperty(INDEX_START_TIME_KEY, Long.toString(indexStartTime));
    }

    /**
     * Persist the metadata
     *
     * @param targetDir the target directory
     * @throws IOException if the metadata cannot be persisted
     */
    public void storeTo(File targetDir) throws IOException
    {
        File metadataFile = new File(targetDir, METADATA_FILENAME);
        FileWriter writer = new FileWriter(metadataFile);
        try
        {
            properties.store(writer, null);
        }
        finally
        {
            IOUtils.closeQuietly(writer);
        }
    }
}
