package com.atlassian.jira.security.util;

import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.util.NameComparator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class AbstractGroupMapper implements GroupMapper
{
    private Map<String, Set<Scheme>> groupMapping;

    protected void addEntry(Map<String, Set<Scheme>> map, String key, Scheme value)
    {
        Set<Scheme> set = map.get(key);
        if (set == null)
        {
            set = new HashSet<Scheme>();
            map.put(key, set);
        }

        set.add(value);
    }

    protected Map<String, Set<Scheme>> getGroupMapping()
    {
        return groupMapping;
    }

    protected void setGroupMapping(Map<String, Set<Scheme>> groupToSchemeMapping)
    {
        this.groupMapping = groupToSchemeMapping;
    }

    @Override
    public Collection<Scheme> getMappedValues(String groupName)
    {
        final Set<Scheme> schemes = getGroupMapping().get(groupName);
        if (schemes != null)
        {
            List<Scheme> schemeList = new ArrayList<Scheme>(schemes);
            Collections.sort(schemeList, NameComparator.COMPARATOR);
            return schemeList;
        }
        else
        {
            return Collections.emptyList();
        }
    }
}
