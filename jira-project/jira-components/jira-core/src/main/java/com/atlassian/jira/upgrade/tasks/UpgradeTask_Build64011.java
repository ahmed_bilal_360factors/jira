package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;

import java.util.EnumSet;

/**
 * Indexes issues because of the changes to how Number Custom Fields are indexed and searched.
 * https://ops.atlassian.com/jira/browse/HOT-25809
 */
public class UpgradeTask_Build64011 extends AbstractImmediateUpgradeTask
{

    public UpgradeTask_Build64011()
    {
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception
    {
        getReindexRequestService().requestReindex(ReindexRequestType.DELAYED, EnumSet.of(AffectedIndex.ISSUE), EnumSet.noneOf(SharedEntityType.class));
    }

    @Override
    public String getBuildNumber()
    {
        return "64011";
    }

    @Override
    public String getShortDescription()
    {
        return "Indexes issues for changes to indexing of Number Custom Field Types.";
    }

}
