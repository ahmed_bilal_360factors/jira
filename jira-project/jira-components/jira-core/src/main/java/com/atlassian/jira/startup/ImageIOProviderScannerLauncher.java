package com.atlassian.jira.startup;

import javax.imageio.ImageIO;

/**
 * ImageIO launcher pre-loads image service providers making the classes available to plugins via VM global.
 *
 * @since v6.4.1
 */
public class ImageIOProviderScannerLauncher implements JiraLauncher
{
    @Override
    public void start()
    {
        //JDEV-32213: Eagerly load ImageIO classes - P2 plugins are not able to class load the twelve monkeys library that is used in atlassian-core-thumber.
        ImageIO.scanForPlugins();
    }

    @Override
    public void stop()
    {
    }
}
