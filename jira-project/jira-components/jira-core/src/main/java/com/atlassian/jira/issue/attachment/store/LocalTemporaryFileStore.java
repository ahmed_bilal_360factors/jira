package com.atlassian.jira.issue.attachment.store;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.atlassian.core.util.FileUtils;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.AttachmentWriteException;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.jira.util.LimitedOutputStream;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

import com.google.common.base.Function;
import com.google.common.base.Supplier;

import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LocalTemporaryFileStore
{
    private static final Logger log = LoggerFactory.getLogger(LocalTemporaryFileStore.class);
    private static final String PREFIX = "temp";
    private static final String SUFFIX = "";
    private final File temporaryDirectory;
    private final PathTraversalChecker pathTraversalChecker;

    public LocalTemporaryFileStore(final File rootTemporaryDirectory, final PathTraversalChecker pathTraversalChecker)
    {
        this.temporaryDirectory = rootTemporaryDirectory;
        this.pathTraversalChecker = pathTraversalChecker;
    }

    public TemporaryAttachmentId createTemporaryFile(final InputStream inputStream)
    {
        return copyStreamToFile(inputStream, Option.<Long>none());
    }

    public TemporaryAttachmentId createTemporaryFile(final InputStream inputStream, final long size)
    {
        return copyStreamToFile(inputStream, Option.some(size));
    }

    private TemporaryAttachmentId copyStreamToFile(final InputStream inputStream, final Option<Long> optionalSize)
    {
        try
        {
            final File tempFile = File.createTempFile(PREFIX, SUFFIX, temporaryDirectory);
            return copyStreamToFileWithCleanupOnFail(inputStream, tempFile, optionalSize);
        }
        catch (final Exception e)
        {
            throw new AttachmentWriteException("Could not save attachment data from stream.", e);
        }
        finally
        {
            IOUtils.closeQuietly(inputStream);
        }
    }

    private TemporaryAttachmentId copyStreamToFileWithCleanupOnFail(final InputStream inputStream,
            final File temporaryFile, final Option<Long> optionalSize) throws Exception
    {
        try
        {
            copyStreamToFile(inputStream, temporaryFile, optionalSize);
            final String temporaryFileId = temporaryFile.getName();
            return TemporaryAttachmentId.fromString(temporaryFileId);
        }
        catch (final Exception e)
        {
            deleteTemporaryFile(temporaryFile);
            throw e;
        }
    }

    private void copyStreamToFile(final InputStream inputStream, final File temporaryFile, final Option<Long> optionalSize)
            throws IOException
    {
        if (optionalSize.isDefined())
        {
            copyStreamToFileWithSizeCheck(inputStream, temporaryFile, optionalSize.get());
        }
        else
        {
            FileUtils.copyFile(inputStream, temporaryFile, true);
        }
    }

    private void copyStreamToFileWithSizeCheck(final InputStream inputStream, final File temporaryFile, final long size)
            throws IOException
    {
        final LimitedOutputStream limitedOutputStream = getLimitedOutputStreamToFile(temporaryFile, size);
        try
        {
            IOUtils.copy(inputStream, limitedOutputStream);
        }
        finally
        {
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(limitedOutputStream);
        }

        checkFileNotSmallerThanExpected(temporaryFile, size);
    }

    private LimitedOutputStream getLimitedOutputStreamToFile(final File temporaryFile, final long size)
            throws FileNotFoundException
    {
        final FileOutputStream fileOutputStream = new FileOutputStream(temporaryFile, true);
        try
        {
            final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            return new LimitedOutputStream(bufferedOutputStream, size);
        }
        catch (final RuntimeException e)
        {
            IOUtils.closeQuietly(fileOutputStream);
            throw e;
        }
    }

    private void checkFileNotSmallerThanExpected(final File temporaryFile, final long expectedSize) throws IOException
    {
        final long fileSize = temporaryFile.length();
        if (fileSize < expectedSize)
        {
            throw new NotEnoughDataIOException("Expected to get {" + expectedSize + "} of data, but got only {" + fileSize + "}");
        }
    }

    private File getTempFileById(final String temporaryAttachmentId)
    {
        return new File(temporaryDirectory, temporaryAttachmentId);
    }

    public Either<Exception, File> getTemporaryAttachmentFile(final TemporaryAttachmentId temporaryAttachmentId)
    {
        final String tempId = temporaryAttachmentId.toStringId();
        final File tempFileById = getTempFileById(tempId);

        return pathTraversalChecker.validateFileInSecureDirectory(tempFileById, temporaryDirectory)
                .fold(new Supplier<Either<Exception, File>>()
                {
                    @Override
                    public Either<Exception, File> get()
                    {
                        return Either.right(tempFileById);
                    }
                }, new Function<Exception, Either<Exception, File>>()
                {
                    @Override
                    public Either<Exception, File> apply(final Exception exception)
                    {
                        return Either.left(exception);
                    }
                });
    }

    public Promise<Unit> deleteTemporaryAttachment(final TemporaryAttachmentId temporaryAttachmentId)
    {
        final String tempId = temporaryAttachmentId.toStringId();
        final File temporaryAttachmentFile = new File(temporaryDirectory, tempId);

        final Option<Exception> exception = pathTraversalChecker.validateFileInSecureDirectory(temporaryAttachmentFile, temporaryDirectory);
        if(exception.isDefined())
        {
            return Promises.rejected(exception.get());
        }

        if(temporaryAttachmentFile.exists())
        {
            deleteTemporaryFile(temporaryAttachmentFile);
        }
        return Promises.promise(Unit.VALUE);
    }

    private void deleteTemporaryFile(final File temporaryAttachmentFile)
    {
        if(!temporaryAttachmentFile.delete())
        {
            log.info("Failed to delete temporary attachment: {}", temporaryAttachmentFile.getAbsolutePath());
        }
    }

}
