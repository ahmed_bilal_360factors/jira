package com.atlassian.jira.bc.dataimport;

import java.util.Set;

import com.atlassian.jira.entity.Entity.Name;

import com.google.common.collect.ImmutableSet;

public class EntityImportExportExclusions
{
    public static Set<String> ENTITIES_EXCLUDED_FROM_IMPORT_EXPORT = ImmutableSet.of(
            Name.ENTITY_PROPERTY_INDEX_DOCUMENT,
            Name.REPLICATED_INDEX_OPERATION,
            Name.NODE_INDEX_COUNTER,
            Name.CLUSTER_LOCK_STATUS,
            Name.CLUSTER_MESSAGE,
            Name.CLUSTER_NODE_HEARTBEAT);
}
