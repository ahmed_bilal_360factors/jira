package com.atlassian.jira.issue.managers;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.bc.issue.properties.IssuePropertyHelper;
import com.atlassian.jira.entity.EntityConstants;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.entity.property.JsonEntityPropertyManager;
import com.atlassian.jira.io.InputStreamFunctionFactory;
import com.atlassian.jira.issue.AttachmentIndexManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.AttachmentKeyMapper;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.store.strategy.move.IOUtilsWrapper;
import com.atlassian.jira.plugin.attachment.AttachmentArchive;
import com.atlassian.jira.plugin.attachment.AttachmentArchiveEntry;
import com.atlassian.jira.plugin.attachment.AttachmentArchiveImpl;
import com.atlassian.jira.plugin.attachment.AttachmentProcessorModuleDescriptor;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugin.PluginAccessor;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.collect.Iterables.filter;

public class DefaultAttachmentIndexManager implements AttachmentIndexManager
{
    private static final Logger log = LoggerFactory.getLogger(DefaultAttachmentIndexManager.class);
    private static final String NO_INDEX = "[]";

    private final PluginAccessor pluginAccessor;
    private final StreamAttachmentStore attachmentStore;
    private final JsonEntityPropertyManager jsonEntityPropertyManager;
    private final IssuePropertyHelper issuePropertyHelper;
    private final String entityDbName;
    private final IOUtilsWrapper fileFactory;
    private final InputStreamFunctionFactory inputStreamFunctionFactory;
    private final AttachmentKeyMapper attachmentKeyMapper;

    public DefaultAttachmentIndexManager(
            final PluginAccessor pluginAccessor,
            final StreamAttachmentStore attachmentStore,
            final JsonEntityPropertyManager jsonEntityPropertyManager,
            final IssuePropertyHelper entityPropertyHelper,
            final IOUtilsWrapper fileFactory,
            final InputStreamFunctionFactory inputStreamFunctionFactory,
            final AttachmentKeyMapper attachmentKeyMapper
    )
    {
        this.pluginAccessor = pluginAccessor;
        this.attachmentStore = attachmentStore;
        this.jsonEntityPropertyManager = jsonEntityPropertyManager;
        this.issuePropertyHelper = entityPropertyHelper;
        this.fileFactory = fileFactory;
        this.inputStreamFunctionFactory = inputStreamFunctionFactory;
        this.attachmentKeyMapper = attachmentKeyMapper;
        entityDbName = entityPropertyHelper.getEntityPropertyType().getDbEntityName();
    }

    @Override
    public void processAttachmentAndCreateIndex(final @Nonnull File file, final @Nonnull Attachment attachment,
            final @Nonnull Issue issue)
    {
        processAttachmentAndCreateIndex(file, attachment, issue, 0);
    }

    protected Option<AttachmentArchive> processAttachmentAndCreateIndex(final @Nonnull File file,
            final @Nonnull Attachment attachment, final @Nonnull Issue issue, final int maxEntries)
    {
        final Option<String> index = generateArchiveFileIndex(file, attachment);

        processAttachment(index.getOrElse(NO_INDEX), attachment, issue);

        return index.map(new Function<String, AttachmentArchive>()
        {
            @Override
            public AttachmentArchive apply(final String input)
            {
                return AttachmentArchiveImpl.fromJSONArrayQuiet(input, maxEntries);
            }
        });
    }

    private Option<String> generateArchiveFileIndex(final @Nonnull File file, final @Nonnull Attachment attachment)
    {
        final Iterable<AttachmentProcessorModuleDescriptor> filteredProcessors = filterProcessors(attachment);

        if (Iterables.isEmpty(filteredProcessors))
        {
            return Option.none();
        }

        final AttachmentProcessorModuleDescriptor firstDescriptor = filteredProcessors.iterator().next();

        final List<AttachmentArchiveEntry> entries = firstDescriptor.getModule().processAttachment(file);

        final Iterable<JSONObject> jsons = AttachmentArchiveImpl.serialize(entries);

        final JSONArray array = new JSONArray(Lists.newArrayList(
                Iterables.filter(jsons, new TotalLengthPredicate(EntityConstants.EXTREMELY_LONG_MAXIMUM_LENGTH))));

        return Option.some(array.toString());
    }

    private Iterable<AttachmentProcessorModuleDescriptor> filterProcessors(final @Nonnull Attachment attachment)
    {
        final String fileExtension = FilenameUtils.getExtension(attachment.getFilename());
        return filter(
                pluginAccessor.getEnabledModuleDescriptorsByClass(AttachmentProcessorModuleDescriptor.class),
                new Predicate<AttachmentProcessorModuleDescriptor>()
                {
                    @Override
                    public boolean apply(final AttachmentProcessorModuleDescriptor attachmentProcessorModuleDescriptor)
                    {
                        return Iterables.any(
                                attachmentProcessorModuleDescriptor.getFileExtensions(), new Predicate<String>()
                                {
                                    @Override
                                    public boolean apply(@Nullable final String s)
                                    {
                                        return fileExtension.equalsIgnoreCase(s);
                                    }
                                }
                        );
                    }
                }
        );
    }

    private void processAttachment(final @Nonnull String index, final @Nonnull Attachment attachment,
            final @Nonnull Issue issue)
    {
        jsonEntityPropertyManager.put(null, entityDbName, issue.getId(), attachment.getId().toString(), index,
                issuePropertyHelper.createSetPropertyEventFunction(), true);
    }

    @Override
    public Option<AttachmentArchive> getAttachmentContents(final @Nonnull Attachment attachment, final @Nonnull Issue issue, final int maxEntries)
    {
        final Option<EntityProperty> property =
                Option.option(jsonEntityPropertyManager.get(entityDbName, issue.getId(), attachment.getId().toString()));


        return property.map(new Function<EntityProperty, String>()
        {
            @Override
            public String apply(final EntityProperty property)
            {
                return property.getValue();
            }
        }).map(new Function<String, AttachmentArchive>()
        {
            @Override
            public AttachmentArchive apply(@Nullable final String input)
            {
                return AttachmentArchiveImpl.fromJSONArrayQuiet(input, maxEntries);
            }
        }).orElse(new Supplier<Option<AttachmentArchive>>()
        {
            @Override
            public Option<AttachmentArchive> get() {
                return fileFactory.processTemporaryFile(
                        "attachment-toc-",
                        ".tmp",
                        generateIndex(attachment, issue, maxEntries)
                );
            }
        });
    }

    private Function<File, Option<AttachmentArchive>> generateIndex(
            final Attachment attachment,
            final Issue issue,
            final int maxEntries
    )
    {
        return new Function<File, Option<AttachmentArchive>>()
        {
            @Override
            public Option<AttachmentArchive> apply(@Nullable final File temporaryFile)
            {
                Preconditions.checkNotNull(temporaryFile, "temporaryFile");
                final AttachmentKey attachmentKey = attachmentKeyMapper.fromAttachment(attachment);
                final com.atlassian.util.concurrent.Function<InputStream, Unit> saveStream =
                        inputStreamFunctionFactory.saveStream(temporaryFile);
                return attachmentStore.getAttachment(attachmentKey, saveStream).fold(new Function<Throwable, Option<AttachmentArchive>>() {
                    @Override
                    public Option<AttachmentArchive> apply(@Nullable Throwable throwable) {
                        log.debug(String.format("Failed to retrieve attachment with %s while generating attachment index.", attachmentKey), throwable);
                        return Option.none();
                    }
                }, new Function<Unit, Option<AttachmentArchive>>() {
                    @Override
                    public Option<AttachmentArchive> apply(@Nullable Unit unit) {
                        return processAttachmentAndCreateIndex(temporaryFile, attachment, issue, maxEntries);
                    }
                }).claim();
            }
        };
    }

    @Override
    public void removeAttachmentIndex(final @Nonnull Attachment attachment, final @Nonnull Issue issue)
    {
        jsonEntityPropertyManager.delete(entityDbName, issue.getId(), attachment.getId().toString());
    }

    @Override
    public boolean isExpandable(final Attachment attachment)
    {
        final Iterable<AttachmentProcessorModuleDescriptor> processors = filterProcessors(attachment);
        return processors.iterator().hasNext();
    }

    /**
     * JSONs are saved to the database by EntityProperty mechanism maximum length of whole index is
     * {@link com.atlassian.jira.entity.EntityConstants#EXTREMELY_LONG_MAXIMUM_LENGTH}.
     * In very rare cases the index could contain less than 100 entries but still longer content than allowed.
     *
     * This mutable predicate checks if current json can be added to the array.
     */
    @NotThreadSafe
    protected static class TotalLengthPredicate implements Predicate<JSONObject>
    {
        //'[' and ']' braces and some spaces that are needed for the array
        private static final int ADDITIONAL_CHARS_NEEDED = 6;
        //Comma and space characters that separate entries
        private static final int ADDITIONAL_CHARS_PER_ENTRY = 2;

        private final long maxLength;
        private long currentLength;

        protected TotalLengthPredicate(final long maxLength)
        {
            this.maxLength = maxLength;
            currentLength = ADDITIONAL_CHARS_NEEDED;
        }

        @Override
        public boolean apply(final JSONObject json)
        {
            final long jsonLength = json.toString().length();

            if((currentLength + jsonLength + ADDITIONAL_CHARS_PER_ENTRY < maxLength))
            {
                currentLength += jsonLength + ADDITIONAL_CHARS_PER_ENTRY;
                return true;
            }
            return false;
        }
    }
}
