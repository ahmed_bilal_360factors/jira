package com.atlassian.jira.jql.query;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.search.constants.SimpleFieldSearchConstants;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.query.clause.TerminalClause;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;

public final class DefaultWorklogQueryRegistry implements WorklogQueryRegistry
{
    private final List<SimpleFieldSearchConstants> worklogConstants = ImmutableList.of(
            SystemSearchConstants.forWorklogAuthor(),
            SystemSearchConstants.forWorklogDate());

    private final QueryRegistry queryRegistry;
    private final Set<String> worklogClauses;

    public DefaultWorklogQueryRegistry(final QueryRegistry queryRegistry)
    {
        this.queryRegistry = queryRegistry;
        worklogClauses = resolveClauseHandlers();
    }

    private Set<String> resolveClauseHandlers()
    {
        return ImmutableSet.copyOf(concat(transform(worklogConstants, new Function<SimpleFieldSearchConstants, Iterable<String>>()
        {
            @Override
            public Iterable<String> apply(final SimpleFieldSearchConstants input)
            {
                return input.getJqlClauseNames().getJqlFieldNames();
            }
        })));
    }

    @Override
    public Option<WorklogClauseQueryFactory> getClauseQueryFactory(final QueryCreationContext context, final TerminalClause clause)
    {
        Collection<ClauseQueryFactory> factories = queryRegistry.getClauseQueryFactory(context, clause);
        if (worklogClauses.contains(clause.getName()))
        {
            if (factories.size() == 1)
            {
                ClauseQueryFactory factory = factories.iterator().next();
                if (factory instanceof WorklogClauseQueryFactory)
                {
                    return some((WorklogClauseQueryFactory) factory);
                }
            }
        }
        return none();
    }
}