package com.atlassian.jira.upgrade.tasks;

import javax.annotation.Nullable;

import com.atlassian.jira.entity.Delete;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.upgrade.AbstractDelayableUpgradeTask;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;

import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Removes the JIRA News gadget. ACE will replace it.
 *
 * @since v6.4
 */
public class UpgradeTask_Build64002 extends AbstractDelayableUpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask_Build64002.class);

    private static final String NEWS_GADGET_URI = "http://www.atlassian.com/gadgets/news.xml";
    private static final String GADGET_TABLE = "PortletConfiguration";
    private static final String GADGET_URI_COLUMN = "gadgetXml";

    private final OfBizDelegator ofBizDelegator;

    public UpgradeTask_Build64002(final OfBizDelegator ofBizDelegator)
    {
        super();
        this.ofBizDelegator = ofBizDelegator;
    }

    @Override
    public String getBuildNumber()
    {
        return "64002";
    }

    @Override
    public String getShortDescription()
    {
        return "Removing the JIRA News gadget.";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception
    {
        final DateTime startedAt = new DateTime();
        int deletedGadgets = Delete.from(GADGET_TABLE).whereLike(GADGET_URI_COLUMN, NEWS_GADGET_URI).execute(ofBizDelegator);
        log.info(String.format("Upgrade task took %d seconds to remove %d news gadgets.", Seconds.secondsBetween(startedAt, new DateTime()).getSeconds(), deletedGadgets));
    }
}
