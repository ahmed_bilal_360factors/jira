package com.atlassian.jira.crowd.embedded;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.manager.login.ForgottenLoginManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.token.ExpirableUserToken;

/**
 * We don't provide any of this functionality.  Just need to provide this guy because
 * Crowd Rest Plugin needs an implementation.
 *
 * @since v4.3
 */
public class NoopForgottenLoginManager implements ForgottenLoginManager
{

    @Override
    public void sendResetLink(Application application, String username, int tokenExpirySeconds)
    {
    }

    @Override
    public boolean sendUsernames(Application application, String email)
    {
        return false;
    }

    @Override
    public void sendResetLink(long directoryId, String username, int tokenExpirySeconds)
    {
    }

    @Override
    public boolean isValidResetToken(long directoryId, String username, String token)
    {
        return false;
    }

    @Override
    public void resetUserCredential(long directoryId, String username, PasswordCredential credential, String token)
    {
    }

    @Override
    public ExpirableUserToken createAndStoreResetToken(long directoryId, String username, int tokenExpirySeconds)
    {
        throw new UnsupportedOperationException("Not implemented");
    }
}
