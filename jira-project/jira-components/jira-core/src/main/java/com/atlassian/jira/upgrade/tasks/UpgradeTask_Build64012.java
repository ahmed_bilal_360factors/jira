package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The browser-metrics plugin has become a mandatory requirement in JIRA 6.4, so it's critical that it's enabled. It
 * may have been disabled previously, which left unchanged would cause the issue navigator to be broken when JIRA
 * starts.
 *
 * @since v6.4
 */
public class UpgradeTask_Build64012 extends AbstractImmediateUpgradeTask {
    private final PluginAccessor pluginAccessor;
    private final PluginController pluginController;
    private static final Logger LOG = LoggerFactory.getLogger(UpgradeTask_Build64012.class);
    private static final String PLUGIN_KEY = "com.atlassian.plugins.browser.metrics.browser-metrics-plugin";

    public UpgradeTask_Build64012(final PluginController pluginController, final PluginAccessor pluginAccessor)
    {
        super();
        this.pluginAccessor = pluginAccessor;
        this.pluginController = pluginController;
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception
    {
        if (setupMode)
        {
            // There is no need to do this on a clean install
            return;
        }

        Plugin plugin = pluginAccessor.getPlugin(PLUGIN_KEY);
        if (plugin == null)
        {
            LOG.warn("The plugin '" + PLUGIN_KEY + "' does not exist.");
        }
        else
        {
            PluginState pluginState = plugin.getPluginState();
            if (pluginState != PluginState.ENABLING && pluginState != PluginState.ENABLED)
            {
                pluginController.enablePlugins(PLUGIN_KEY);
            }
        }
    }

    @Override
    public String getBuildNumber()
    {
        return "64012";
    }

    @Override
    public String getShortDescription()
    {
        return "Enables the browser metrics plugin if it's disabled.";
    }
}
