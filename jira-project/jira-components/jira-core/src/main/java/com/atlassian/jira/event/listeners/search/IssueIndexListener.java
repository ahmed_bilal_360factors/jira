package com.atlassian.jira.event.listeners.search;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.AbstractIssueEventListener;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.index.IssueIndexingParams;
import com.atlassian.jira.issue.worklog.Worklog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * This listener updates the search index within JIRA.
 * <p>
 * Do not delete ;)
 */
public class IssueIndexListener extends AbstractIssueEventListener
{
    private static final Logger log = LoggerFactory.getLogger(IssueIndexListener.class);

    public static final String NAME = "Issue Index Listener";

    @Override
    public void init(final Map params)
    {}

    @Override
    public String[] getAcceptedParams()
    {
        return new String[0];
    }

    /**
     * Should not be deleted - required for JIRA operation
     */
    @Override
    public boolean isInternal()
    {
        return true;
    }

    @Override
    public void issueCreated(final IssueEvent event)
    {
        reIndexModifiedIssue(event);
    }

    @Override
    public void issueUpdated(final IssueEvent event)
    {
        reIndexModifiedIssue(event);
    }

    @Override
    public void issueAssigned(final IssueEvent event)
    {
        reIndexModifiedIssue(event);
    }

    @Override
    public void issueResolved(final IssueEvent event)
    {
        reIndexModifiedIssue(event);
    }

    @Override
    public void issueClosed(final IssueEvent event)
    {
        reIndexModifiedIssue(event);
    }

    @Override
    public void issueCommented(final IssueEvent event)
    {
        // For a comment we need to update the issue because the last updated date is changed, but this does not effect the change history
        reIndex(event, IssueIndexingParams.INDEX_ISSUE_ONLY);
        if (event.getComment() != null)
        {
            reIndexComment(event.getComment());
        }
    }

    @Override
    public void issueWorkLogged(final IssueEvent event)
    {
        // For a worklog we need to update the issue because the last updated date is changed, but this does not effect the change history
        reIndex(event, IssueIndexingParams.INDEX_ISSUE_ONLY);
        if (event.getWorklog() != null)
        {
            reIndexWorklog(event.getWorklog());
        }
    }

    @Override
    public void issueReopened(final IssueEvent event)
    {
        reIndexModifiedIssue(event);
    }

    @Override
    public void issueGenericEvent(final IssueEvent event)
    {
        reIndexModifiedIssue(event);
    }

    @Override
    public void issueCommentEdited(final IssueEvent event)
    {
        // For a comment we need to update the issue because the last updated date is changed, but this does not effect the change history
        reIndex(event, IssueIndexingParams.INDEX_ISSUE_ONLY);
        if (event.getComment() != null)
        {
            reIndexComment(event.getComment());
        }
    }

    @Override
    public void issueCommentDeleted(final IssueEvent event)
    {
        // For a comment we need to update the issue because the last updated date is changed, but this does not effect the change history
        // We also need to reindex all the comments because the event does not contain the deleted comment
        final IssueIndexingParams issueIndexingParams = IssueIndexingParams.builder().withComments().build();
        reIndex(event, issueIndexingParams);
    }

    @Override
    public void issueWorklogUpdated(final IssueEvent event)
    {
        // For a worklog we need to update the issue because the last updated date is changed, but this does not effect the change history
        reIndex(event, IssueIndexingParams.INDEX_ISSUE_ONLY);
        if (event.getWorklog() != null)
        {
            reIndexWorklog(event.getWorklog());
        }
    }

    @Override
    public void issueWorklogDeleted(final IssueEvent event)
    {
        // For a worklog we need to update the issue because the last updated date is changed, but this does not effect the change history
        // We also need to reindex all the worklogs because the event does not contain the deleted worklog
        final IssueIndexingParams issueIndexingParams = IssueIndexingParams.builder().withWorklogs().build();
        reIndex(event, issueIndexingParams);
    }

    @Override
    public void issueDeleted(final IssueEvent event)
    {}

    @Override
    public void issueMoved(final IssueEvent event)
    {
        reIndex(event, IssueIndexingParams.INDEX_ALL);
    }

    @Override
    public void customEvent(final IssueEvent event)
    {}

    private static void reIndex(final IssueEvent issueEvent, IssueIndexingParams issueIndexingParams)
    {
        final Set<Issue> issuesToReindex = new HashSet<Issue>();
        final Issue issue = issueEvent.getIssue();
        issuesToReindex.add(issue);

        //if there are any subtasks that were modified as part of an issue operation (e.g. editing a parent issue's
        //security level) ensure that they are also re-indexed.
        if (issueEvent.isSubtasksUpdated())
        {
            issuesToReindex.addAll(issue.getSubTaskObjects());
        }
        try
        {
            ComponentAccessor.getIssueIndexManager().reIndexIssueObjects(issuesToReindex, issueIndexingParams);
        }
        catch (final Exception issueReindexException) //catch everything here to make sure it doesn't bring server down.
        {
            log.error("Error re-indexing changes for issue '" + issue.getKey() + "'", issueReindexException);
        }
    }

    private static void reIndexModifiedIssue(IssueEvent event)
    {
        final IssueIndexingParams issueIndexingParams = IssueIndexingParams.builder().withChangeHistory().build();
        reIndex(event, issueIndexingParams);
        if (event.getComment() != null)
        {
            reIndexComment(event.getComment());
        }
    }

    private static void reIndexComment(final Comment comment)
    {
        try
        {
            ComponentAccessor.getIssueIndexManager().reIndexComments(Collections.singletonList(comment));
        }
        catch (final Exception commentReindexException) //catch everything here to make sure it doesn't bring server down.
        {
            log.error("Error re-indexing comment '" + comment.getId() + "'", commentReindexException);
        }
    }

    private static void reIndexWorklog(final Worklog worklog)
    {
        try
        {
            ComponentAccessor.getIssueIndexManager().reIndexWorklogs(Collections.singletonList(worklog));
        }
        catch (final Exception worklogReindexException) //catch everything here to make sure it doesn't bring server down.
        {
            log.error("Error re-indexing worklog '" + worklog.getId() + "'", worklogReindexException);
        }
    }

    /**
     * As we wish to only have one IssueIndexListener at a time - enforce uniqueness
     */
    @Override
    public boolean isUnique()
    {
        return true;
    }

    @Override
    public String getDescription()
    {
        return getI18NBean().getText("admin.listener.issue.index.desc");
    }
}
