package com.atlassian.jira.startup;

import java.util.concurrent.TimeUnit;

import com.atlassian.core.util.DateUtils;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.index.ha.IndexRecoveryManager;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.task.TaskProgressSink;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class will check for a the index and perform minor repairs if required.
 * This does not operate in a cluster or in disaster recover mode.
 *
 * @since v6.4
 */
public class IndexRecoveryLauncher implements JiraLauncher
{
    private static final String DR_PROPERTY_KEY = "disaster.recovery";
    private static final Logger LOG = LoggerFactory.getLogger(IndexRecoveryLauncher.class);

    /**
     * Returns true if This instance is neither clustered or in disaster recovery mode.
     * @return true if standalone mode
     */
    private boolean isStandaloneMode()
    {
        return !ComponentAccessor.getComponent(ApplicationProperties.class).getOption(DR_PROPERTY_KEY)
                && !ComponentAccessor.getComponent(ClusterManager.class).isClustered();
    }

    @Override
    public void start()
    {
        ApplicationProperties applicationProperties = ComponentAccessor.getApplicationProperties();
        if ("true".equals(applicationProperties.getString(APKeys.JIRA_SETUP)))
        {
            if (isStandaloneMode())
            {
                final IndexRecoveryManager recoveryManager = ComponentAccessor.getComponent(IndexRecoveryManager.class);

                try
                {
                    DateUtils.DateRange dateRange = recoveryManager.getDurationToRecover();
                    if (dateRange == null)
                    {
                        return;
                    }
                    long difference = Math.abs(dateRange.endDate.getTime() - dateRange.startDate.getTime());

                    if (difference > TimeUnit.DAYS.toMillis(1))
                    {
                        LOG.info("Issue index is out of date with the database by more than 24 hours. Automatic recovery is not attempted");
                        return;
                    }

                    if (difference > 1000)
                    {
                        LOG.info("Recover Issue Index - start");
                    }

                    // Although the difference in time stamps is about zero, we still reindex the boundary case.
                    // but don't say anything, because it might unduly frighten admins into thinking something is wrong.
                    recoveryManager.reindexIssuesIn(dateRange, TaskProgressSink.NULL_SINK);

                    if (difference > 1000)
                    {
                        LOG.info("Recover Issue Index - end");
                    }
                }
                catch (IndexException e)
                {
                    LOG.error("Automatic index recovery failed", e);
                }
                catch (SearchException e)
                {
                    LOG.error("Automatic index recovery failed", e);
                }
                catch (RuntimeException e)
                {
                    LOG.error("Automatic index recovery failed", e);
                }
            }
        }
    }

    @Override
    public void stop()
    {
    }
}
