package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Condition to check if the issue is currently editable
 * <p/>
 * An issue must be in the JiraHelper context params.
 *
 * @since v4.1
 */
public class IsIssueEditableCondition extends AbstractIssueWebCondition
{
    private static final Logger log = LoggerFactory.getLogger(IsIssueEditableCondition.class);
    private final IssueManager issueManager;


    public IsIssueEditableCondition(IssueManager issueManager)
    {
        this.issueManager = issueManager;
    }

    public boolean shouldDisplay(ApplicationUser user, Issue issue, JiraHelper jiraHelper)
    {
        return issueManager.isEditable(issue);
    }

}
