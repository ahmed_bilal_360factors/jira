package com.atlassian.jira.license;

import javax.annotation.concurrent.Immutable;

/**
 * Fired when {@link com.atlassian.jira.license.LicenseRole} is undefined.
 *
 * @since v6.4
 */
@Immutable
public class LicenseRoleUndefinedEvent extends LicenseRoleEvent
{
    public LicenseRoleUndefinedEvent(final LicenseRoleId id)
    {
        super(id);
    }

    @Override
    public String toString()
    {
        return String.format("LicenseRole(%s) undefined.", getLicenseRoleId());
    }
}
