package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Condition to determine whether Subtasks are turned on or not
 *
 * @since v4.1
 */
public class SubTasksEnabledCondition extends AbstractWebCondition
{
    private static final Logger log = LoggerFactory.getLogger(SubTasksEnabledCondition.class);
    private final SubTaskManager subTaskManager;

    public SubTasksEnabledCondition(SubTaskManager subTaskManager)
    {
        this.subTaskManager = subTaskManager;
    }

    public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper)
    {

        return subTaskManager.isSubTasksEnabled();
    }
}
