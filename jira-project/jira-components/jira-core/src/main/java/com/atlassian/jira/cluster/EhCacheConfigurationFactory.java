package com.atlassian.jira.cluster;

import java.net.URL;

import javax.annotation.Nonnull;

import net.sf.ehcache.config.Configuration;

/**
 * Builder for our EhCacheConfiguration.
 *
 * @since v6.4
 */
public interface EhCacheConfigurationFactory
{
    Configuration newConfiguration(@Nonnull URL baseXmlConfiguration, @Nonnull ClusterNodeProperties clusterNodeProperties);
}
