package com.atlassian.jira.issue.attachment.store.strategy.get;

import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;

/**
 * Implementation of {@link com.atlassian.jira.issue.attachment.store.strategy.get.AttachmentGetStrategy} which just
 * delegates to single attachment store.
 *
 * @since v6.4
 */
@ParametersAreNonnullByDefault
public class SingleStoreAttachmentGetStrategy implements AttachmentGetStrategy
{
    private final StreamAttachmentStore streamAttachmentStore;

    public SingleStoreAttachmentGetStrategy(final StreamAttachmentStore streamAttachmentStore)
    {
        this.streamAttachmentStore = streamAttachmentStore;
    }

    @Override
    public <A> Promise<A> getAttachmentData(final AttachmentKey attachmentKey, final Function<AttachmentGetData, A> attachmentGetDataProcessor)
    {
        return streamAttachmentStore.getAttachmentData(attachmentKey, attachmentGetDataProcessor);
    }

    @Override
    public Promise<Boolean> exists(final AttachmentKey attachmentKey)
    {
        return streamAttachmentStore.exists(attachmentKey);
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final SingleStoreAttachmentGetStrategy that = (SingleStoreAttachmentGetStrategy) o;

        return streamAttachmentStore.equals(that.streamAttachmentStore);
    }

    @Override
    public int hashCode()
    {
        return streamAttachmentStore.hashCode();
    }
}
