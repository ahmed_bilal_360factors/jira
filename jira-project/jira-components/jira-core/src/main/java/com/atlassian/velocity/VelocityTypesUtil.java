package com.atlassian.velocity;

import java.lang.reflect.Method;

import com.google.common.primitives.Primitives;

/**
 * Utility methods for translating parameter types as understood by velocity
 */
public class VelocityTypesUtil
{
    public static final Class<Object> NULL_OBJECT_TYPE = Object.class;
    private static final Class<?>[] EMPTY_ARRAY = new Class[0];

    public static Class<?>[] getMethodArgsTypes(final Method method)
    {
        final Class<?>[] parameterTypes = method.getParameterTypes();
        if (parameterTypes.length == 0)
        {
            return EMPTY_ARRAY;
        }
        final Class<?>[] translatedTypes = new Class[parameterTypes.length];
        for (int i = 0; i < parameterTypes.length; i++)
        {
            final Class<?> parameterType = parameterTypes[i];
            translatedTypes[i] = translateTypes(parameterType);
        }
        return translatedTypes;
    }

    public static Class<?>[] getParametersTypes(final Object[] params)
    {
        if (params == null || params.length == 0)
        {
            return EMPTY_ARRAY;
        }
        final Class<?>[] types = new Class[params.length];
        for (int i = 0; i < params.length; i++)
        {
            final Object param = params[i];
            types[i] = param == null ? NULL_OBJECT_TYPE : param.getClass();
        }
        return types;
    }

    private static Class<?> translateTypes(final Class<?> parameterType)
    {
        if (parameterType.isPrimitive())
        {
            return Primitives.wrap(parameterType);
        }
        else
        {
            return parameterType;
        }
    }
}
