/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

package com.atlassian.jira.security.util;

import com.atlassian.jira.permission.PermissionSchemeEntry;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.security.PermissionManager;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GroupToPermissionSchemeMapper extends AbstractGroupMapper
{
    private final PermissionSchemeManager permissionSchemeManager;
    private final PermissionManager permissionManager;

    public GroupToPermissionSchemeMapper(PermissionSchemeManager permissionSchemeManager, PermissionManager permissionManager)
    {
        this.permissionSchemeManager = permissionSchemeManager;
        this.permissionManager = permissionManager;
        setGroupMapping(init());
    }

    /**
     * Go through all the Permission Schemes and create a Map, where the key is the group name
     * and values are Sets of Schemes
     */
    private Map<String, Set<Scheme>> init()
    {
        Map<String, Set<Scheme>> mapping = new HashMap<String, Set<Scheme>>();

        // Get all Permission Schmes
        final List<Scheme> schemes = permissionSchemeManager.getSchemeObjects();
        for (Scheme permissionScheme : schemes)
        {
            // For each scheme get all the permissions
            for (ProjectPermission permission : permissionManager.getAllProjectPermissions())
            {
                // Get all the groups for this permission
                final Collection<PermissionSchemeEntry> entities = permissionSchemeManager.getPermissionSchemeEntries(permissionScheme, permission.getProjectPermissionKey());
                for (PermissionSchemeEntry permissionRecord : entities)
                {
                    if ("group".equals(permissionRecord.getType()))
                    {
                        addEntry(mapping, permissionRecord.getParameter(), permissionScheme);
                    }
                }
            }
        }

        return mapping;
    }
}
