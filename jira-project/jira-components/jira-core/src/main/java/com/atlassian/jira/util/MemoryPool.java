package com.atlassian.jira.util;

import java.lang.management.MemoryType;

/**
 * Holds information about a particular MemoryPool in the JVM.
 *
 * This info is read once and then held for the lifetime of this object, because it could be expensive to calculate the values.
 */
public class MemoryPool
{
    private final String name;
    private final long used;
    private final long committed;
    private final long max;
    private final MemoryType type;

    public MemoryPool(final String name, final long used, final long committed, final long max, final MemoryType type)
    {
        this.name = name;
        this.used = used;
        this.committed = committed;
        this.max = max;
        this.type = type;
    }

    public String getName()
    {
        return name;
    }

    public long getUsed()
    {
        return used;
    }

    public long getCommitted()
    {
        return committed;
    }

    public long getMax()
    {
        return max;
    }

    public MemoryType getType()
    {
        return type;
    }
}
