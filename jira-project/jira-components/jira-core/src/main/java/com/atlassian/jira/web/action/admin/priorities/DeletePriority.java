package com.atlassian.jira.web.action.admin.priorities;

import java.util.Collection;

import com.atlassian.jira.config.PriorityManager;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.action.admin.constants.AbstractDeleteConstant;
import com.atlassian.sal.api.websudo.WebSudoRequired;

@WebSudoRequired
public class DeletePriority extends AbstractDeleteConstant<Priority>
{
    private final PriorityManager priorityManager;

    public DeletePriority(PriorityManager priorityManager)
    {
        this.priorityManager = priorityManager;
    }

    protected String getConstantEntityName()
    {
        return "Priority";
    }

    protected String getNiceConstantName()
    {
        return getText("admin.issue.constant.priority.lowercase");
    }

    protected String getIssueConstantField()
    {
        return "priority";
    }

    protected Priority getConstant(String id)
    {
        return getConstantsManager().getPriorityObject(id);
    }

    protected String getRedirectPage()
    {
        return "ViewPriorities.jspa";
    }

    protected Collection<Priority> getConstants()
    {
        return getConstantsManager().getPriorityObjects();
    }

    protected void clearCaches()
    {
        getConstantsManager().refreshPriorities();
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception
    {
        if (confirm)
        {
            priorityManager.removePriority(id, newId);
        }
        if (getHasErrorMessages())
            return ERROR;
        else
            return getRedirect(getRedirectPage());
    }
}
