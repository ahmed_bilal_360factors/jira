package com.atlassian.jira.bulkedit;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.fields.ComponentsSystemField;
import com.atlassian.jira.issue.fields.LongIdsValueHolder;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.util.VersionHelperBean;
import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Abstract class for BulkEditMultiSelectFieldOption to reuse methods implementations in specific options
 *
 * @since v6.4
 */
public abstract class AbstractBulkEditMultiSelectFieldOption implements BulkEditMultiSelectFieldOption
{
    public static final String VALUES_SEPARATOR = ", ";

    public Map<String,Object> getFieldValuesMap(Issue issue, OrderableField field, Map<String, Object> fieldValuesHolder)
    {
        return fieldValuesHolder;
    }

    public boolean validateOperation(OrderableField field, Map<String,Object> fieldValuesHolder)
    {
        if (fieldValuesHolder.get(field.getId()) instanceof LongIdsValueHolder)
        {
            return (!((LongIdsValueHolder) fieldValuesHolder.get(field.getId())).getValuesToAdd().isEmpty()
                    || ((LongIdsValueHolder) fieldValuesHolder.get(field.getId())).size() > 0);
        }
        else
        {
            return (((Collection) fieldValuesHolder.get(field.getId())).size() > 0);
        }
    }

    public String getFieldValuesToAdd(OrderableField field, Map<String,Object> fieldValuesHolder)
    {
        if (fieldValuesHolder.get(field.getId()) instanceof LongIdsValueHolder)
        {
            Set<String> valuesToAdd = LongIdsValueHolder.fromFieldValuesHolder(field.getId(),fieldValuesHolder).getValuesToAdd();
            if (!valuesToAdd.isEmpty())
            {
                StringBuilder resultString = new StringBuilder();
                for (String value : valuesToAdd)
                {
                    //we need to cut prefix for new Versions/Components to be added and add comma as separator
                    resultString.append(value).append(VALUES_SEPARATOR);
                }
                return resultString.toString().substring(0,resultString.length() - VALUES_SEPARATOR.length());
            }
        }
        return StringUtils.EMPTY;
    }

    private String getNewValuePrefixForField(OrderableField field)
    {
        if (field.getId().equals(IssueFieldConstants.COMPONENTS)
            || field.getId().equals(IssueFieldConstants.AFFECTED_VERSIONS)
            || field.getId().equals(IssueFieldConstants.FIX_FOR_VERSIONS))
        {
            return LongIdsValueHolder.NEW_VALUE_PREFIX;
        }
        return StringUtils.EMPTY;
    }
}
