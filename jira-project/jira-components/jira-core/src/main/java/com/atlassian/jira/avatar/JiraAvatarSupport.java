package com.atlassian.jira.avatar;

import com.atlassian.jira.user.ApplicationUser;

/**
 * JIRA SPI for externally defined Avatars. The purpose of this interface is to enable a plugin implementation to
 * define key avatar services without having JIRA core depend on an externally defined interface. This interface is
 * derived directly from the plugins AvatarProvider interface. Only expected implementation is
 * {@link com.atlassian.jira.avatar.PluginAvatarAdapter}
 */
public interface JiraAvatarSupport
{
    /**
     * Obtain an avatar by the email of the user.
     *
     * @param email the unique String id of the object which has an avatar.
     * @param size the AUI standard t-shirt size name
     * @return the avatar.
     */
    JiraPluginAvatar getAvatar(String email, String size);

    /**
     * Obtain an avatar for the user.
     *
     * @param user the user whose avatar you want.
     * @param size the AUI standard t-shirt size name
     * @return the avatar.
     */
    JiraPluginAvatar getAvatar(ApplicationUser user, String size);

    /**
     * Obtain an avatar by its own identifier.
     *
     * @param avatarId the id of the avatar.
     * @param size the AUI standard t-shirt size name.
     * @return the avatar.
     */
    JiraPluginAvatar getAvatarById(Long avatarId, String size);
}
