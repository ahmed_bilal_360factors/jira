package com.atlassian.jira.index.property;

import com.atlassian.jira.jql.ClauseHandler;

/**
 * Alias for indexed issue property in lucene. Alias is basically a shortcut for 'issue.property[a].b.c = xyz'.
 */
public class JqlAlias
{
    private final ClauseHandler clauseHandler;

    public JqlAlias(final ClauseHandler clauseHandler)
    {
        this.clauseHandler = clauseHandler;
    }

    public ClauseHandler getClauseHandler()
    {
        return clauseHandler;
    }

}
