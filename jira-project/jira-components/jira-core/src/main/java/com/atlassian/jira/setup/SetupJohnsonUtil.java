package com.atlassian.jira.setup;

import java.util.Collection;

/**
 * A thin wrapper over JohnsonEventContainer used to enable testing setup behaviour in case
 * of any Johnson events.
 *
 * @since v6.3
 */
public interface SetupJohnsonUtil
{
    /**
     * Gets a collection of the Event objects.
     *
     * @return an unmodifiable collection
     */
    Collection getEvents();
}
