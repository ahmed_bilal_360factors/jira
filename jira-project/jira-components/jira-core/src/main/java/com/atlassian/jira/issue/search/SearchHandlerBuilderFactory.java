package com.atlassian.jira.issue.search;

import com.atlassian.jira.issue.index.indexers.FieldIndexer;
import com.atlassian.jira.jql.ClauseHandler;
import com.atlassian.jira.jql.ClauseInformation;
import com.atlassian.jira.jql.DefaultClauseHandler;
import com.atlassian.jira.jql.DefaultValuesGeneratingClauseHandler;
import com.atlassian.jira.jql.context.ClauseContextFactory;
import com.atlassian.jira.jql.context.SimpleClauseContextFactory;
import com.atlassian.jira.jql.permission.ClausePermissionChecker;
import com.atlassian.jira.jql.permission.ClausePermissionHandler;
import com.atlassian.jira.jql.permission.DefaultClausePermissionHandler;
import com.atlassian.jira.jql.query.ClauseQueryFactory;
import com.atlassian.jira.jql.validator.ClauseValidator;
import com.atlassian.jira.jql.values.ClauseValuesGenerator;
import com.atlassian.jira.util.ComponentLocator;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Component that can produce {@link com.atlassian.jira.issue.search.SearchHandler} builders.
 */
public final class SearchHandlerBuilderFactory
{
    private final ComponentLocator locator;

    public SearchHandlerBuilderFactory(final ComponentLocator locator)
    {
        this.locator = locator;
    }

    public SearchHandlerBuilder builder(ClauseInformation clauseInfo)
    {
        return new SearchHandlerBuilder(clauseInfo);
    }

    /**
     * Builder that can produce {@link com.atlassian.jira.issue.search.SearchHandler} instances.
     *
     * <p>
     *     {@link com.atlassian.jira.jql.context.ClauseContextFactory} is by default set to {@link com.atlassian.jira.jql.context.SimpleClauseContextFactory},
     *     all the other things need to be provided, otherwise the build method will throw an illegal state exception.
     * </p>
     */
    public final class SearchHandlerBuilder
    {
        private final ClauseInformation clauseInformation;

        private ClauseContextFactory clauseContextFactory = locator.getComponentInstanceOfType(SimpleClauseContextFactory.class);
        private ClauseQueryFactory clauseQueryFactory;
        private ClauseValidator clauseValidator;

        private ClausePermissionHandler permissionHandler;

        private final List<FieldIndexer> fieldIndexers = Lists.newArrayList();

        public SearchHandlerBuilder(final ClauseInformation clauseInformation)
        {
            this.clauseInformation = checkNotNull(clauseInformation);
        }

        public SearchHandlerBuilder setClauseQueryFactoryType(final Class<? extends ClauseQueryFactory> clauseQueryFactoryType)
        {
            this.clauseQueryFactory = locator.getComponent(clauseQueryFactoryType);
            return this;
        }

        public SearchHandlerBuilder setClauseValidatorType(final Class<? extends ClauseValidator> clauseValidatorType)
        {
            this.clauseValidator = locator.getComponent(clauseValidatorType);
            return this;
        }

        public SearchHandlerBuilder setContextFactory(final ClauseContextFactory classContextFactoryType)
        {
            this.clauseContextFactory = classContextFactoryType;
            return this;
        }

        public SearchHandlerBuilder setPermissionHandler(ClausePermissionHandler permissionHandler)
        {
            this.permissionHandler = permissionHandler;
            return this;
        }

        public SearchHandlerBuilder setPermissionChecker(ClausePermissionChecker permissionChecker)
        {
            this.permissionHandler = new DefaultClausePermissionHandler(permissionChecker);
            return this;
        }

        public SearchHandlerBuilder addIndexer(FieldIndexer indexer)
        {
            fieldIndexers.add(indexer);
            return this;
        }

        public SearchHandler buildWithValuesGenerator(ClauseValuesGenerator generator)
        {
            return withClauseHandler(new DefaultValuesGeneratingClauseHandler(clauseInformation, clauseQueryFactory, clauseValidator,
                    permissionHandler, clauseContextFactory, generator));
        }

        public SearchHandler build()
        {
            return withClauseHandler(new DefaultClauseHandler(clauseInformation, clauseQueryFactory, clauseValidator,
                    permissionHandler, clauseContextFactory));
        }

        private SearchHandler withClauseHandler(ClauseHandler clauseHandler)
        {
            Preconditions.checkState(clauseContextFactory != null);
            Preconditions.checkState(clauseValidator != null);
            Preconditions.checkState(permissionHandler != null);
            Preconditions.checkState(clauseQueryFactory != null);

            final SearchHandler.ClauseRegistration clauseRegistration = new SearchHandler.ClauseRegistration(clauseHandler);
            return new SearchHandler(fieldIndexers, null, Collections.singletonList(clauseRegistration));
        }
    }
}
