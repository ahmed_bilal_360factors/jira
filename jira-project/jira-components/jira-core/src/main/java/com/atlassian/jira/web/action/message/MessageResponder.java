package com.atlassian.jira.web.action.message;

import java.io.PrintWriter;
import java.net.URI;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.atlassian.adapter.jackson.ObjectMapper;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import com.google.common.collect.Iterables;

import webwork.action.Action;

/**
 * Prepares a response to an action. The response will contain messages and redirection.
 *
 * @since v6.4
 */
public final class MessageResponder
{
    private final SoyTemplateRendererProvider rendererProvider;
    private final ObjectMapper objectMapper;

    public MessageResponder(final SoyTemplateRendererProvider rendererProvider, final ObjectMapper objectMapper)
    {
        this.rendererProvider = rendererProvider;
        this.objectMapper = objectMapper;
    }

    /**
     * Guarantees that given messages will be shown to the user and user will be be able to follow the redirect.
     * It might modify the response headers and start sending the response body.
     *
     * @param messages information for the user about the outcome of the action
     * @param redirect redirects to the next page the user should see; <b>SHOULD NOT contain personal information</b>
     * @param action performed action
     * @param title human-readable, short description of the action
     * @return one of the standard {@link webwork.action.Action} view names
     */
    public final String respond(
            final Iterable<PopUpMessage> messages,
            final URI redirect,
            final JiraWebActionSupport action,
            final String title)
    {
        if (action.isInlineDialogMode())
        {
            return respondToAjax(messages, action);
        }
        else
        {
            return respondToSubmit(title, messages, redirect, action);
        }
    }

    private String respondToAjax(final Iterable<PopUpMessage> messages, final JiraWebActionSupport action)
    {
        final String serializedMessages = objectMapper.writeValueAsString(messages);
        final HttpServletResponse response = action.getHttpResponse();
        response.addHeader("X-Atlassian-Messages", serializedMessages);
        response.setHeader("X-Atlassian-Dialog-Control", "DONE");
        response.setStatus(200);
        return Action.NONE;
    }

    private String respondToSubmit(
            final String title,
            final Iterable<PopUpMessage> messages,
            final URI redirect,
            final JiraWebActionSupport action)
    {
        if (Iterables.isEmpty(messages))
        {
            return action.getRedirect(redirect.toString());
        }
        else
        {
            renderAcknowledgeView(title, messages, redirect, action);
            return Action.NONE;
        }
    }

    private void renderAcknowledgeView(
            final String title,
            final Iterable<PopUpMessage> messages,
            final URI redirect,
            final JiraWebActionSupport action)
    {
        try
        {
            final HttpServletResponse response = action.getHttpResponse();
            final PrintWriter writer = response.getWriter();
            final Map<String, Object> data = MapBuilder.<String, Object>newBuilder()
                    .add("title", title)
                    .add("messages", messages)
                    .add("redirectUri", redirect.toString())
                    .toMap();
            final SoyTemplateRenderer renderer = rendererProvider.getRenderer();
            renderer.render(writer, "jira.webresources:action-soy-templates", "JIRA.Templates.errors.acknowledge", data);
        }
        catch (final Exception e)
        {
            throw new RuntimeException(e);
        }
    }
}
