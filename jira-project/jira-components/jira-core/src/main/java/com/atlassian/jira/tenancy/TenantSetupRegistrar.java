package com.atlassian.jira.tenancy;

/**
 * has registration methods for registering and unregistering TenantInitialDataLoader objects
 *
 * @since v6.4
 */
public interface TenantSetupRegistrar
{
    void registerTenantInitialDataLoader(TenantInitialDataLoader tenantInitialDataLoader);

    void unregisterTenantInitialDataLoader(TenantInitialDataLoader tenantInitialDataLoader);

    void unregisterAll();
}
