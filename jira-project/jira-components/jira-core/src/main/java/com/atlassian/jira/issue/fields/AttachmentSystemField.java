package com.atlassian.jira.issue.fields;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.atlassian.core.util.FileSize;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.AttachmentsBulkOperationResult;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.AttachmentFileNameCreationDateComparator;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.FieldJsonRepresentation;
import com.atlassian.jira.issue.fields.rest.FieldTypeInfo;
import com.atlassian.jira.issue.fields.rest.FieldTypeInfoContext;
import com.atlassian.jira.issue.fields.rest.RestAwareField;
import com.atlassian.jira.issue.fields.rest.json.JsonData;
import com.atlassian.jira.issue.fields.rest.json.JsonType;
import com.atlassian.jira.issue.fields.rest.json.JsonTypeBuilder;
import com.atlassian.jira.issue.fields.rest.json.beans.AttachmentJsonBean;
import com.atlassian.jira.issue.fields.rest.json.beans.AttachmentRenderedJsonBean;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderLayoutItem;
import com.atlassian.jira.issue.fields.util.MessagedResult;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.thumbnail.ThumbnailManager;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollection.Reason;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.action.issue.util.BackwardCompatibleTemporaryAttachmentUtil;
import com.atlassian.jira.web.bean.BulkEditBean;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import webwork.action.Action;
import webwork.config.Configuration;

import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ATTACHMENTS;
import static java.util.Collections.emptyList;

public class AttachmentSystemField extends AbstractOrderableField implements HideableField, RestAwareField
{
    private static final Logger log = LoggerFactory.getLogger(AttachmentSystemField.class);

    public static final String FILETOCONVERT = "filetoconvert";

    private final AttachmentManager attachmentManager;
    private final PermissionManager permissionManager;
    private final JiraBaseUrls jiraBaseUrls;
    private final ThumbnailManager thumbnailManager;
    private final DateTimeFormatterFactory dateTimeFormatterFactory;
    private final EmailFormatter emailFormatter;
    private final BackwardCompatibleTemporaryAttachmentUtil temporaryAttachmentUtil;

    public AttachmentSystemField(
            final VelocityTemplatingEngine templatingEngine,
            final ApplicationProperties applicationProperties,
            final AttachmentManager attachmentManager,
            final JiraAuthenticationContext authenticationContext,
            final PermissionManager permissionManager,
            final JiraBaseUrls jiraBaseUrls,
            final ThumbnailManager thumbnailManager,
            final DateTimeFormatterFactory dateTimeFormatterFactory,
            final EmailFormatter emailFormatter,
            final BackwardCompatibleTemporaryAttachmentUtil temporaryAttachmentUtil)
    {
        super(IssueFieldConstants.ATTACHMENT, "issue.field.attachment", templatingEngine, applicationProperties,
                authenticationContext, permissionManager, null);

        this.attachmentManager = attachmentManager;
        this.permissionManager = permissionManager;
        this.jiraBaseUrls = jiraBaseUrls;
        this.thumbnailManager = thumbnailManager;
        this.dateTimeFormatterFactory = dateTimeFormatterFactory;
        this.emailFormatter = emailFormatter;
        this.temporaryAttachmentUtil = temporaryAttachmentUtil;
    }

    @Override
    public String getCreateHtml(final FieldLayoutItem fieldLayoutItem, final OperationContext operationContext, final Action action,
            final Issue issue, final Map displayParameters)
    {
        return getEditHtml(fieldLayoutItem, operationContext, action, issue, displayParameters);
    }

    @Override
    public String getEditHtml(final FieldLayoutItem fieldLayoutItem, final OperationContext operationContext, final Action action,
            final Issue issue, final Map displayParameters)
    {
        final Map<String, Object> velocityParams = getVelocityParams(fieldLayoutItem, action, issue, displayParameters);
        final Long maxSizeRaw = Long.valueOf(Configuration.getString(APKeys.JIRA_ATTACHMENT_SIZE));

        velocityParams.put("maxSizeRaw", maxSizeRaw);
        velocityParams.put("maxSize", FileSize.format(maxSizeRaw));

        final String formToken = (String) operationContext.getFieldValuesHolder().get(IssueFieldConstants.FORM_TOKEN);

        final Collection<?> tempFiles = (formToken != null)
                ? temporaryAttachmentUtil.getTemporaryWebAttachmentsByFormToken(formToken)
                : emptyList();

        final Object checkedFiles = operationContext.getFieldValuesHolder().get(getId());
        velocityParams.put("tempFiles", tempFiles);
        if (checkedFiles != null)
        {
            velocityParams.put("checkedFiles", checkedFiles);
        }

        return renderTemplate("attachment-edit.vm", velocityParams);
    }

    public String getViewHtml(final FieldLayoutItem fieldLayoutItem, final Action action, final Issue issue, final Map displayParameters)
    {
        throw new UnsupportedOperationException("Not implemented.");
    }

    public String getViewHtml(final FieldLayoutItem fieldLayoutItem, final Action action, final Issue issue, final Object value,
            final Map displayParameters)
    {
        throw new UnsupportedOperationException("Not implemented.");
    }

    public String availableForBulkEdit(final BulkEditBean bulkEditBean)
    {
        return "bulk.edit.unavailable";
    }

    public boolean isShown(final Issue issue)
    {
        final boolean hasCreateAttachmentPermission;
        if (issue.isCreated())
        {
            // check issue permission
            hasCreateAttachmentPermission = permissionManager.hasPermission(
                    ProjectPermissions.CREATE_ATTACHMENTS, issue, getAuthenticationContext().getUser());
        }
        else
        {
            // issue not saved to the DB yet - check the project permission
            hasCreateAttachmentPermission = permissionManager.hasPermission(
                    CREATE_ATTACHMENTS, issue.getProjectObject(), getAuthenticationContext().getUser(), true);
        }

        return hasCreateAttachmentPermission && attachmentManager.attachmentsEnabled();
    }

    public void populateDefaults(final Map<String, Object> fieldValuesHolder, final Issue issue)
    {
        // Do nothing
    }

    public void populateFromIssue(final Map<String, Object> fieldValuesHolder, final Issue issue)
    {
        // Do nothing
    }

    public void validateParams(final OperationContext operationContext, final ErrorCollection errorCollectionToAddTo,
            final I18nHelper i18n, final Issue issue, final FieldScreenRenderLayoutItem fieldScreenRenderLayoutItem)
    {
        final Map<String, Object> fieldValuesHolder = operationContext.getFieldValuesHolder();
        //noinspection unchecked
        final List<String> tempAttachmentIds = (List<String>) fieldValuesHolder.get(getId());

        if (tempAttachmentIds == null || tempAttachmentIds.isEmpty())
        {
            //there is no attachments to add
            return;
        }

        if (!temporaryAttachmentUtil.allAttachmentsExists(tempAttachmentIds))
        {
            errorCollectionToAddTo.addError(
                    getId(), i18n.getText("attachment.temporary.id.session.time.out"), Reason.VALIDATION_FAILED);
        }
    }

    public Object getDefaultValue(final Issue issue)
    {
        return null;
    }

    public void createValue(final Issue issue, final Object value)
    {
        //noinspection unchecked
        addAttachmentIgnoreErrors(issue, (List<String>) value);
    }

    public void updateValue(final FieldLayoutItem fieldLayoutItem, final Issue issue, final ModifiedValue modifiedValue,
            final IssueChangeHolder issueChangeHolder)
    {
        //noinspection unchecked
        issueChangeHolder.addChangeItems(addAttachmentIgnoreErrors(issue, (List<String>) modifiedValue.getNewValue()));
    }

    private List<ChangeItemBean> addAttachmentIgnoreErrors(final Issue issue, final List<String> tempAttachmentIds)
    {
        if (tempAttachmentIds == null || tempAttachmentIds.isEmpty())
        {
            return emptyList();
        }

        final ApplicationUser currentUser = getAuthenticationContext().getUser();
        final AttachmentsBulkOperationResult<ChangeItemBean> convertResult =
                temporaryAttachmentUtil.convertTemporaryAttachments(currentUser, issue, tempAttachmentIds);

        if (!convertResult.getErrors().isEmpty())
        {
            final String errors = Joiner.on(", ").join(convertResult.getErrors());
            log.warn(String.format("There were errors while converting temporary attachments: %s", errors));
        }

        return convertResult.getResults();
    }

    public void updateIssue(final FieldLayoutItem fieldLayoutItem, final MutableIssue issue, final Map fieldValueHolder)
    {
        if (fieldValueHolder.containsKey(getId()))
        {
            issue.setExternalFieldValue(getId(), fieldValueHolder.get(getId()));
        }
    }

    public MessagedResult needsMove(final Collection originalIssues, final Issue targetIssue, final FieldLayoutItem targetFieldLayoutItem)
    {
        // Attachments are handled separately
        return new MessagedResult(false);
    }

    public void populateForMove(final Map<String, Object> fieldValuesHolder, final Issue originalIssue, final Issue targetIssue)
    {
        // Should never be called as needsMove() returns false
        throw new UnsupportedOperationException("Not implemented.");
    }

    public void removeValueFromIssueObject(final MutableIssue issue)
    {
        // Do nothing - remove can only be done through a separate means.
    }

    public boolean canRemoveValueFromIssueObject(final Issue issue)
    {
        return false;
    }

    public boolean hasValue(final Issue issue)
    {
        return false;
    }

    public Object getValueFromParams(final Map params) throws FieldValidationException
    {
        throw new UnsupportedOperationException("Not implemented.");
    }

    public void populateParamsFromString(final Map<String, Object> fieldValuesHolder, final String stringValue,
            final Issue issue) throws FieldValidationException
    {
        throw new UnsupportedOperationException("Not implemented.");
    }

    protected Object getRelevantParams(final Map<String, String[]> params)
    {
        if (params.containsKey(FILETOCONVERT))
        {
            return Arrays.asList(params.get(FILETOCONVERT));
        }
        else
        {
            return Lists.newArrayList();
        }
    }

    @Override
    public FieldTypeInfo getFieldTypeInfo(final FieldTypeInfoContext fieldTypeInfoContext)
    {
        return new FieldTypeInfo(null, null);
    }

    @Override
    public JsonType getJsonSchema()
    {
        return JsonTypeBuilder.systemArray(JsonType.ATTACHMENT_TYPE, getId());
    }

    @Override
    public FieldJsonRepresentation getJsonFromIssue(final Issue issue, final boolean renderedVersionRequired,
            final FieldLayoutItem fieldLayoutItem)
    {
        final List<Attachment> comments = attachmentManager.getAttachments(
                issue, new AttachmentFileNameCreationDateComparator(getAuthenticationContext().getLocale()));

        final FieldJsonRepresentation fieldJsonRepresentation = new FieldJsonRepresentation(
                new JsonData(AttachmentJsonBean.shortBeans(comments, jiraBaseUrls,
                        thumbnailManager, authenticationContext.getUser(), emailFormatter))
        );

        if (renderedVersionRequired)
        {
            fieldJsonRepresentation.setRenderedData(new JsonData(AttachmentRenderedJsonBean.shortBeans(comments, jiraBaseUrls,
                    thumbnailManager, dateTimeFormatterFactory, authenticationContext.getUser(), emailFormatter)));
        }
        return fieldJsonRepresentation;
    }
}
