package com.atlassian.jira.issue.search.optimizers;


import java.util.Collections;
import java.util.Set;

import javax.annotation.Nullable;

import com.atlassian.jira.util.NonInjectableComponent;
import com.atlassian.query.clause.Clause;

/**
 * @since v6.4
 */

/**
 * Util class for extracting determined projects from query.
 */
@NonInjectableComponent
public class DeterminedProjectsExtractor
{
    /**
     *
     * @param clause query clause to extract projects from
     * @return set of extracted project keys/names or empty list if no projects could be extracted or null clause was passed
     */
    public Set<String> extractDeterminedProjectsFromClause(@Nullable final Clause clause)
    {
        if (clause == null)
        {
            return Collections.emptySet();
        }

        final DeterminedProjectsInQueryVisitor projectsInQueryVisitor = new DeterminedProjectsInQueryVisitor();

        if (clause.accept(projectsInQueryVisitor))
        {
            return projectsInQueryVisitor.getDeterminedProjects();
        }
        else
        {
            return Collections.emptySet();
        }
    }
}
