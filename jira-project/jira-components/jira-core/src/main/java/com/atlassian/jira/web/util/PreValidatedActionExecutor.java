package com.atlassian.jira.web.util;

import java.util.List;

import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;

import com.google.common.base.Supplier;
import com.google.common.collect.Lists;

/**
 * Action executor with ability to run validation before executing the action.
 *
 * @param <E> Type representing validation or execution error.
 * @since v6.4
 */
@ParametersAreNonnullByDefault
public class PreValidatedActionExecutor<E>
{
    private final List<Supplier<Option<E>>> validators = Lists.newArrayList();

    public PreValidatedActionExecutor<E> withValidator(final Supplier<Option<E>> validator)
    {
        validators.add(validator);
        return this;
    }

    /**
     * Runs all validators and executes given supplier if all passed. Immediately returns validation error if any
     * validator fails, without continuing with other validation methods.
     *
     * @param supplier Action to perform when validation passes
     * @param <T> Result type
     */
    public <T> Either<E, T> executeFailOnFirstError(final Supplier<Either<E, T>> supplier)
    {
        for (final Supplier<Option<E>> validator : validators)
        {
            final Option<E> errorOption = validator.get();
            if (errorOption.isDefined())
            {
                return Either.left(errorOption.get());
            }
        }
        return supplier.get();
    }
}
