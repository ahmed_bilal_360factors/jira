package com.atlassian.jira.issue.attachment.store.strategy.move;

import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.util.concurrent.Function;

/**
 * @since v6.4
 */
public class SendToStoreFunctionFactory
{
    private final ResendingAttachmentStreamCreator resendingAttachmentStreamCreator;

    public SendToStoreFunctionFactory(final ResendingAttachmentStreamCreator resendingAttachmentStreamCreator)
    {
        this.resendingAttachmentStreamCreator = resendingAttachmentStreamCreator;
    }

    public Function<AttachmentGetData, Unit> createFunction(final AttachmentKey destinationKey, final StreamAttachmentStore secondaryStore)
    {
        return new ResendToStoreFunction(destinationKey, secondaryStore, resendingAttachmentStreamCreator);
    }
}
