package com.atlassian.jira.database;

import java.sql.Connection;
import java.sql.SQLException;

import com.atlassian.jira.exception.DataAccessException;

import com.mysema.query.sql.RelationalPath;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLTemplates;
import com.mysema.query.sql.dml.SQLUpdateClause;

/**
 * @since v6.4
 */
public class DbConnnectionImpl implements DbConnection
{
    private final Connection con;
    private final SQLTemplates dialect;

    public DbConnnectionImpl(final Connection con, final SQLTemplates dialect)
    {
        this.con = con;
        this.dialect = dialect;
    }

    @Override
    public Connection getJdbcConnection()
    {
        return con;
    }

    @Override
    public SQLQuery newSqlQuery()
    {
        return new SQLQuery(con, dialect);
    }

    @Override
    public SQLUpdateClause update(final RelationalPath<?> entity)
    {
        return new SQLUpdateClause(con, dialect, entity);
    }

    @Override
    public void setAutoCommit(final boolean autoCommit)
    {
        try
        {
            con.setAutoCommit(autoCommit);
        }
        catch (SQLException ex)
        {
            throw new DataAccessException(ex);
        }
    }

    @Override
    public void commit()
    {
        try
        {
            con.commit();
        }
        catch (SQLException ex)
        {
            throw new DataAccessException(ex);
        }
    }

    @Override
    public void rollback()
    {
        try
        {
            con.rollback();
        }
        catch (SQLException ex)
        {
            throw new DataAccessException(ex);
        }
    }
}
