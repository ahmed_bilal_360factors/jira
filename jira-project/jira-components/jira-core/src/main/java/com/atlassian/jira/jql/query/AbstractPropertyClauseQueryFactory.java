package com.atlassian.jira.jql.query;

import javax.annotation.Nonnull;

import com.atlassian.fugue.Option;
import com.atlassian.jira.index.IndexDocumentConfiguration;
import com.atlassian.jira.issue.customfields.converters.DoubleConverter;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.util.JqlDateSupport;
import com.atlassian.jira.jql.util.NumberIndexValueConverter;
import com.atlassian.jira.jql.util.SimpleIndexValueConverter;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.query.clause.Property;
import com.atlassian.query.clause.TerminalClause;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import static com.atlassian.jira.jql.query.QueryFactoryResult.createFalseResult;

/**
 * Base clause factory for issue property queries.
 */
public abstract class AbstractPropertyClauseQueryFactory implements ClauseQueryFactory
{
    private final DoubleConverter doubleConverter;
    private final JqlDateSupport jqlDateSupport;
    private final JqlOperandResolver operandResolver;
    private final JiraAuthenticationContext authenticationContext;

    protected AbstractPropertyClauseQueryFactory(final DoubleConverter doubleConverter,
            final JqlDateSupport jqlDateSupport,
            final JqlOperandResolver operandResolver,
            final JiraAuthenticationContext authenticationContext)
    {
        this.doubleConverter = doubleConverter;
        this.jqlDateSupport = jqlDateSupport;
        this.operandResolver = operandResolver;
        this.authenticationContext = authenticationContext;
    }

    @Nonnull
    @Override
    public QueryFactoryResult getQuery(@Nonnull final QueryCreationContext queryCreationContext,
            @Nonnull final TerminalClause terminalClause)
    {
        final Option<Property> property = getProperty(terminalClause);
        if (property.isDefined() && isSupportedOperator(terminalClause))
        {
            return getLuceneQuery(property.get(), terminalClause);
        }
        else
        {
            return createFalseResult();
        }
    }

    protected abstract boolean isSupportedOperator(final TerminalClause terminalClause);

    protected abstract Option<Property> getProperty(final TerminalClause terminalClause);

    protected abstract Iterable<IndexDocumentConfiguration.Type> getPropertyTypes(final Property property);

    private QueryFactoryResult getLuceneQuery(final Property property, final TerminalClause terminalClause)
    {
        final Iterable<IndexDocumentConfiguration.Type> propertyTypes = getPropertyTypes(property);
        final Iterable<OperatorSpecificQueryFactory> queryFactories = getQueryFactories(propertyTypes);

        final String fieldName = getLuceneFieldName(property);
        return new GenericClauseQueryFactory(fieldName, Lists.newArrayList(queryFactories), operandResolver)
                .getQuery(new QueryCreationContextImpl(authenticationContext.getUser()), terminalClause);
    }

    public static String getLuceneFieldName(final Property property)
    {
        return "ISSUEPROP_" + property.getAsPropertyString();
    }


    private Iterable<OperatorSpecificQueryFactory> getQueryFactories(Iterable<IndexDocumentConfiguration.Type> types)
    {
        return Iterables.concat(Iterables.transform(types, new Function<IndexDocumentConfiguration.Type, Iterable<OperatorSpecificQueryFactory>>()
        {
            @Override
            public Iterable<OperatorSpecificQueryFactory> apply(final IndexDocumentConfiguration.Type type)
            {
                final ImmutableList.Builder<OperatorSpecificQueryFactory> builder = ImmutableList.builder();
                switch (type)
                {
                    case NUMBER:
                        final NumberIndexValueConverter valueConverter = new NumberIndexValueConverter(doubleConverter);
                        builder.add(new ActualValueEqualityQueryFactory(valueConverter));
                        builder.add(new ActualValueRelationalQueryFactory(valueConverter));
                        break;
                    case TEXT:
                        builder.add(new LikeQueryFactory());
                        break;
                    case DATE:
                        builder.add(new DateEqualityQueryFactory(jqlDateSupport));
                        builder.add(new DateRelationalQueryFactory(jqlDateSupport));
                        break;
                    case STRING:
                        builder.add(new ActualValueEqualityQueryFactory(new SimpleIndexValueConverter(false)));
                        break;
                }
                return builder.build();
            }
        }));
    }

}
