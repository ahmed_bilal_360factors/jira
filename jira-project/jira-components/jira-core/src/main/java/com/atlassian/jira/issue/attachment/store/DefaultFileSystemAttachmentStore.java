package com.atlassian.jira.issue.attachment.store;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Callable;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.AttachmentCleanupException;
import com.atlassian.jira.issue.attachment.AttachmentDirectoryAccessor;
import com.atlassian.jira.issue.attachment.AttachmentFileGetData;
import com.atlassian.jira.issue.attachment.AttachmentGetData;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.AttachmentMoveException;
import com.atlassian.jira.issue.attachment.AttachmentReadException;
import com.atlassian.jira.issue.attachment.AttachmentStore.AttachmentAdapter;
import com.atlassian.jira.issue.attachment.FileAttachments;
import com.atlassian.jira.issue.attachment.FileSystemAttachmentStore;
import com.atlassian.jira.issue.attachment.NoAttachmentDataException;
import com.atlassian.jira.issue.attachment.StoreAttachmentBean;
import com.atlassian.jira.issue.attachment.StoreAttachmentResult;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ExecutorServiceWrapper;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Functions;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;

import com.google.common.base.Preconditions;
import com.google.common.io.Files;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @since v6.1
 */
@ParametersAreNonnullByDefault
public final class DefaultFileSystemAttachmentStore implements FileSystemAttachmentStore
{
    private static final Logger log = LoggerFactory.getLogger(DefaultFileSystemAttachmentStore.class);

    private final AttachmentDirectoryAccessor directoryAccessor;
    private final ExecutorServiceWrapper managedExecutor;
    private final LocalTemporaryFileStore localTemporaryFileStore;
    private final EventPublisher eventPublisher;

    public DefaultFileSystemAttachmentStore(
            final AttachmentDirectoryAccessor directoryAccessor,
            final LocalTemporaryFileStore localTemporaryFileStore,
            final ExecutorServiceWrapper managedExecutor,
            final EventPublisher eventPublisher)
    {
        this.directoryAccessor = directoryAccessor;
        this.localTemporaryFileStore = localTemporaryFileStore;
        this.managedExecutor = managedExecutor;
        this.eventPublisher = eventPublisher;
    }

    /**
     * Returns the physical File for the given attachment key.
     *
     * @param attachmentKey the key of attachment
     * @return the file.
     * @throws com.atlassian.jira.exception.DataAccessException on failure getting required attachment info.
     */
    @Override
    public File getAttachmentFile(final AttachmentKey attachmentKey) throws DataAccessException
    {
        return FileAttachments.getAttachmentFileHolder(attachmentKey, directoryAccessor.getAttachmentRootPath());
    }

    /**
     * This is intended for cases where you want more control over where the attachment actually lives and you just want
     * something to handle the look up logic for the various possible filenames an attachment can have.
     * <p/>
     * In practice, this is just used during Project Import
     *
     * @param attachment it's not an attachment but it acts like one for our purposes.
     * @param attachmentDir the directory the attachments live in. This is different that the system-wide attachment
     * directory. i.e. this would "attachments/MKY/MKY-1" and not just "attachments"
     * @return the actual attachment
     */
    @Override
    public File getAttachmentFile(final AttachmentAdapter attachment, final File attachmentDir)
    {
        return FileAttachments.getAttachmentFileHolder(attachment, attachmentDir);
    }

    @Nonnull
    @Override
    public Promise<StoreAttachmentResult> putAttachment(@Nonnull final StoreAttachmentBean storeAttachmentBean)
    {
        final InputStream dataStream = storeAttachmentBean.getStream();
        final AttachmentKey attachmentKey = storeAttachmentBean.getAttachmentKey();
        return putAttachment(dataStream, storeAttachmentBean.getSize(), attachmentKey);
    }

    // Creates temporaryAttachment first and moves it to real attachment directory afterwards to avoid situation when
    // someone is accessing half written attachment
    private Promise<StoreAttachmentResult> putAttachment(final InputStream inputStream, final Long size, final AttachmentKey attachmentKey)
    {
        return putTemporaryAttachment(inputStream, size).flatMap(Functions.toGoogleFunction(new Function<TemporaryAttachmentId, Promise<? extends StoreAttachmentResult>>()
        {
            @Override
            public Promise<? extends StoreAttachmentResult> get(final TemporaryAttachmentId temporaryAttachmentId)
            {
                return moveTemporaryToAttachment(temporaryAttachmentId, attachmentKey).flatMap(Functions.toGoogleFunction(new Function<Unit, Promise<? extends StoreAttachmentResult>>()
                {
                    @Override
                    public Promise<? extends StoreAttachmentResult> get(final Unit input)
                    {
                        return Promises.promise(StoreAttachmentResult.created());
                    }
                }));
            }
        }));
    }

    private File getAttachmentFileHolder(final AttachmentKey attachmentKey)
    {
        return FileAttachments.getAttachmentFileHolder(
                attachmentKey, directoryAccessor.getAttachmentRootPath());
    }

    @Override
    public <A> Promise<A> getAttachment(final AttachmentKey attachmentKey, final Function<InputStream, A> inputStreamProcessor)
    {
        return getAttachmentData(attachmentKey, new Function<AttachmentGetData, A>()
        {
            @Override
            public A get(final AttachmentGetData attachmentGetData)
            {
                return inputStreamProcessor.get(attachmentGetData.getInputStream());
            }
        });
    }

    @Override
    public <A> Promise<A> getAttachmentData(final AttachmentKey attachmentKey, final Function<AttachmentGetData, A> attachmentGetDataProcessor)
    {
        final File attachmentFile;
        try
        {
            attachmentFile = getAttachmentFile(attachmentKey);
        }
        catch (final DataAccessException e)
        {
            return Promises.rejected(new AttachmentReadException(e));
        }

        if (!(attachmentFile.exists() && attachmentFile.isFile()))
        {
            return Promises.rejected(new NoAttachmentDataException("Attachment does not exist in filesystem " + attachmentFile));
        }
        else
        {
            return managedExecutor.submit(new Callable<A>()
            {
                @Override
                public A call()
                {

                    final AttachmentFileGetData attachmentGetData = new AttachmentFileGetData(attachmentFile);
                    try
                    {
                        return attachmentGetDataProcessor.get(attachmentGetData);
                    }
                    catch (final RuntimeException e)
                    {
                        throw new AttachmentReadException(e);
                    }
                    finally
                    {
                        attachmentGetData.close();
                    }
                }
            });
        }
    }

    @Override
    public Promise<Boolean> exists(final AttachmentKey attachmentKey)
    {
        final File attachmentFile = getAttachmentFile(attachmentKey);
        return Promises.promise(attachmentFile.exists() && attachmentFile.isFile());
    }

    @Override
    public Promise<Unit> deleteAttachment(final AttachmentKey attachmentKey)
    {
        try
        {
            final File attachmentFile = getAttachmentFile(attachmentKey);

            if (attachmentFile.exists())
            {
                if (!attachmentFile.delete())
                {
                    log.warn("Failed to delete attachmentFile: " + attachmentFile.getAbsolutePath());
                }

                eventPublisher.publish(new JiraHomeChangeEvent(JiraHomeChangeEvent.Action.FILE_DELETED, JiraHomeChangeEvent.FileType.ATTACHMENT, attachmentFile));
            }
            else
            {
                log.warn("Trying to delete non-existent attachment: [" + attachmentFile.getAbsolutePath() + "] ..ignoring");
            }
        }
        catch (final DataAccessException e)
        {
            Promises.rejected(new AttachmentCleanupException(e));
        }
        return Promises.promise(Unit.VALUE);
    }

    @Override
    public Promise<Unit> deleteAttachmentContainerForIssue(final Issue issue)
    {
        Preconditions.checkNotNull(issue);
        final File attachmentDir = directoryAccessor.getAttachmentDirectory(issue);
        try
        {
            FileUtils.deleteDirectory(attachmentDir);

            eventPublisher.publish(new JiraHomeChangeEvent(JiraHomeChangeEvent.Action.FILE_DELETED, JiraHomeChangeEvent.FileType.ATTACHMENT, attachmentDir));

            return Promises.promise(Unit.VALUE);
        }
        catch (final IOException e)
        {
            return Promises.rejected(new AttachmentCleanupException(e));
        }
    }

    @Override
    public Promise<Unit> moveAttachment(final AttachmentKey oldAttachmentKey, final AttachmentKey newAttachmentKey)
    {
        final File oldAttachmentFile = getAttachmentFile(oldAttachmentKey);
        final File newAttachmentFile = getAttachmentFile(newAttachmentKey);

        return moveFile(oldAttachmentFile, newAttachmentFile, false);
    }

    @Override
    @Nonnull
    public Option<ErrorCollection> errors()
    {
        return directoryAccessor.errors();
    }

    @Override
    public Promise<Unit> copyAttachment(final AttachmentKey sourceAttachmentKey, final AttachmentKey newAttachmentKey)
    {
        return getAttachment(sourceAttachmentKey, new Function<InputStream, TemporaryAttachmentId>()
        {
            @Override
            public TemporaryAttachmentId get(final InputStream input)
            {
                return localTemporaryFileStore.createTemporaryFile(input);
            }
        }).flatMap(new com.google.common.base.Function<TemporaryAttachmentId, Promise<Unit>>()
        {
            @Override
            public Promise<Unit> apply(final TemporaryAttachmentId temporaryFile)
            {
                return moveTemporaryToAttachment(temporaryFile, newAttachmentKey);
            }
        });
    }

    @Override
    public Promise<TemporaryAttachmentId> putTemporaryAttachment(final InputStream inputStream, final long size)
    {
        return managedExecutor.submit(new Callable<TemporaryAttachmentId>()
        {
            @Override
            public TemporaryAttachmentId call() throws Exception
            {
                return localTemporaryFileStore.createTemporaryFile(inputStream, size);
            }
        });
    }

    @Override
    public Promise<Unit> moveTemporaryToAttachment(final TemporaryAttachmentId temporaryAttachmentId, final AttachmentKey destinationKey)
    {
        final Either<Exception, File> tempFileEitherException = localTemporaryFileStore.getTemporaryAttachmentFile(temporaryAttachmentId);

        if (tempFileEitherException.isLeft())
        {
            return Promises.rejected(tempFileEitherException.left().get());
        }

        final File tempFile = tempFileEitherException.right().get();
        final File destinationFile = getAttachmentFileHolder(destinationKey);

        return moveFile(tempFile, destinationFile, true);
    }

    private Promise<Unit> moveFile(final File sourceFile, final File destinationFile, boolean isTemporary)
    {
        final File parentFile = destinationFile.getParentFile();

        if (!ensureDirectoryExists(parentFile))
        {
            return Promises.rejected(new AttachmentMoveException("Unable to create target directory " + parentFile.getAbsolutePath()));
        }

        return renameOrCopyAndDeleteFile(sourceFile, destinationFile, isTemporary);
    }

    private boolean ensureDirectoryExists(final File directory)
    {
        return directory.mkdirs() || directory.exists();
    }

    /**
     * Attempt to rename the sourceFile to the destination file. If the sourceFile cannot be renamed then it is copied
     * to the destination file and the sourceFile is marked for deletion. The source file should exist and be
     * accessible and the destinationFile file should not exist.
     * @param sourceFile file to move or copy and delete
     * @param destinationFile destination file where the source file will be moved to
     * @return Promise with either a value or an AttachmentRuntimeException
     */
    private Promise<Unit> renameOrCopyAndDeleteFile(final File sourceFile, final File destinationFile, final boolean isTemporary)
    {
        if (!sourceFile.exists())
        {
            return Promises.rejected(new NoAttachmentDataException("Source file does not exist " + sourceFile.getAbsolutePath()));
        }
        else
        {
            try
            {
                Files.move(sourceFile, destinationFile);

                if(!isTemporary)
                {
                    eventPublisher.publish(new JiraHomeChangeEvent(JiraHomeChangeEvent.Action.FILE_DELETED, JiraHomeChangeEvent.FileType.ATTACHMENT, sourceFile));
                }

                eventPublisher.publish(new JiraHomeChangeEvent(JiraHomeChangeEvent.Action.FILE_ADD, JiraHomeChangeEvent.FileType.ATTACHMENT, destinationFile));

                return Promises.promise(Unit.VALUE);
            }
            catch (IOException e)
            {
                return Promises.rejected(new AttachmentMoveException(
                        "Unable to create target file " + destinationFile.getAbsolutePath()));
            }
        }
    }

    @Override
    public Promise<Unit> deleteTemporaryAttachment(final TemporaryAttachmentId temporaryAttachmentId)
    {
        return localTemporaryFileStore.deleteTemporaryAttachment(temporaryAttachmentId);
    }
}
