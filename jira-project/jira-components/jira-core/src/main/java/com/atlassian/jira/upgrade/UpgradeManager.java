package com.atlassian.jira.upgrade;

import com.atlassian.jira.bean.export.IllegalXMLCharactersException;
import com.atlassian.scheduler.status.RunDetails;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.CollectionUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public interface UpgradeManager
{
    /**
     * @return the number of pending delayed upgrades.
     */
    int getPendingDelayedUpgradeCount();

    /**
     * Schedules the execution of delayed upgrades.
     *
     * @param delayMins the amount of time in minutes to wait before executing the delayed upgrades.
     * @param isAllowReindex set this to true to allow any required reindex to be done.
     *
     * @return the status of the upgrade process.
     *
     * @since 6.4
     */
    @Nonnull
    Status scheduleDelayedUpgrades(int delayMins, final boolean isAllowReindex);

    /**
     * Returns true if a scheduled upgrade is in progress.
     *
     * @return if a scheduled upgrade is in progress.
     *
     * @since 6.4
     */
    @Nonnull
    boolean areDelayedUpgradesRunning();

    RunDetails getLastUpgradeResult();

    /**
     * Performs the upgrade if one is required and the license is not too old to proceed with the upgrade.
     *
     * @param backupPath - a path to the default location of the export, may be <code>null</code>, in which case no auto
     * export will be performed
     * @param upgradeManagerParams contain information needed to upgrade process
     * @return status of the upgrade process
     * @throws com.atlassian.jira.bean.export.IllegalXMLCharactersException if backup was impossible due to invalid XML
     * characters
     */
    Status doUpgradeIfNeededAndAllowed(@Nullable String backupPath, UpgradeManagerParams upgradeManagerParams) throws IllegalXMLCharactersException;

    /**
     * Export path of the last backup performed by this manager
     *
     * @return path to the last backup file
     */
    String getExportFilePath();

    /**
     * @return the history of upgrades performed on this instance of JIRA in reverse chronological order
     * @since v4.1
     */
    List<UpgradeHistoryItem> getUpgradeHistory();

    /**
     * Return true if the upgrade task implemented in the parameter class has completed successfully..
     * Note: We use the class as the parameter to help when tasks are refactored and reordered etc.
     *
     * @param clazz The Upgrade Task's implementing class.
     *
     * @since v6.4
     */
    boolean hasUpgradeTaskRun(Class<? extends UpgradeTask>  clazz);

    /**
     * Status of the upgrade process
     */
    public static class Status
    {
        public static final Status OK = new Status(Collections.<String>emptyList());

        private final Collection<String> errors;

        /**
         * Creates status with only one error message and indexing params set to none.
         * @param error the only error message
         */
        public Status(String error)
        {
            this(ImmutableList.of(error));
        }

        /**
         * Creates status with only one error message and indexing params set to none.
         * @param errors a list of errors that occurred during the upgrade
         */
        public Status(Collection<String> errors)
        {
            this.errors = errors;
        }

        /**
         * Checks whether this upgrade was successful.
         * @return true if upgrade tasks returned no errors
         */
        public boolean successful()
        {
            return CollectionUtils.isEmpty(errors);
        }

        /**
         * List of errors that occurred during the upgrade
         * @return list of errors
         */
        public Collection<String> getErrors()
        {
            return errors;
        }
    }
}
