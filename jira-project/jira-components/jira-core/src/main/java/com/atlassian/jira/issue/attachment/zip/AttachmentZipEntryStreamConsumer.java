package com.atlassian.jira.issue.attachment.zip;

import com.atlassian.fugue.Option;
import com.atlassian.jira.util.Consumer;
import com.atlassian.jira.util.IOUtil;
import com.atlassian.jira.util.io.InputStreamConsumer;

import io.atlassian.blobstore.client.api.Unit;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * This class implements {@link com.atlassian.jira.util.io.InputStreamConsumer}. It will consume inputStream, which have
 * to provide valid zipStream data and stream content of entry with specified index to provided outputStream.
 *
 * @since 6.4
 */
public class AttachmentZipEntryStreamConsumer implements InputStreamConsumer<Unit>
{
    private final OutputStream outputStream;
    private final Consumer<ZipArchiveEntry> onZipEntryExists;
    private final int entryIndex;

    /**
     * Constructing {@link com.atlassian.jira.issue.attachment.zip.AttachmentZipEntryStreamConsumer}
     *
     * @param outputStream outputStream where content of zipEntry will be streamed
     * @param onZipEntryExists consumer which will get called before any data will get streamed into outputStream
     * @param entryIndex index of zip entry which content will get streamed to outputStream, counting from 0
     */
    public AttachmentZipEntryStreamConsumer(final OutputStream outputStream,
            final Consumer<ZipArchiveEntry> onZipEntryExists, final int entryIndex)
    {
        this.outputStream = outputStream;
        this.onZipEntryExists = onZipEntryExists;
        this.entryIndex = entryIndex;
    }

    /**
     * This method will consume inputStream, which have to provide valid zipStream data and stream content of entry with
     * specified index to provided outputStream. Callback {@link Consumer} onZipEntryExists method will get called
     * before any bytes gets written to outputStream.
     *
     * @param inputStream has to provide valid zipStream bytes
     * @return {@link io.atlassian.blobstore.client.api.Unit}
     * @throws ZipEntryNotFoundException when entry with given index was not found
     */
    @Override
    public Unit withInputStream(final InputStream inputStream) throws IOException, ZipEntryNotFoundException
    {
        final ZipArchiveInputStream zipArchiveInputStream = new ZipArchiveInputStream(inputStream);
        final Option<ZipArchiveEntry> zipEntryOption = findZipEntryByIndex(zipArchiveInputStream, entryIndex);

        if (zipEntryOption.isEmpty())
        {
            throw new ZipEntryNotFoundException("Could not find entry with index: " + entryIndex);
        }

        final ZipArchiveEntry zipEntry = zipEntryOption.get();
        streamZipEntry(zipArchiveInputStream, zipEntry);

        return Unit.UNIT;
    }

    private void streamZipEntry(final ZipArchiveInputStream zipArchiveInputStream, final ZipArchiveEntry zipEntry)
            throws IOException
    {
        BufferedInputStream bufferedZipEntryInputStream = null;
        try
        {
            onZipEntryExists.consume(zipEntry);

            bufferedZipEntryInputStream = new BufferedInputStream(zipArchiveInputStream);
            IOUtil.copy(bufferedZipEntryInputStream, outputStream);
        }
        finally
        {
            IOUtil.shutdownStream(bufferedZipEntryInputStream);
        }
    }

    private Option<ZipArchiveEntry> findZipEntryByIndex(final ZipArchiveInputStream zipInputStream,
            final int entryIndex) throws IOException
    {
        ZipArchiveEntry entry = zipInputStream.getNextZipEntry();
        int i = 0;
        while (entry != null && i < entryIndex)
        {
            entry = zipInputStream.getNextZipEntry();
            i++;
        }
        return Option.option(entry);
    }
}
