package com.atlassian.jira.crowd.embedded;

import com.atlassian.crowd.directory.PasswordConstraintsLoaderImpl;
import com.atlassian.crowd.embedded.api.PasswordScoreService;
import com.atlassian.jira.config.FeatureManager;

/**
 * Because crowd shouldn't be telling us where SAL's DarkFeatureManager should live.
 */
public class JiraPasswordConstraintsLoader extends PasswordConstraintsLoaderImpl
{
    private final FeatureManager featureManager;

    public JiraPasswordConstraintsLoader(PasswordScoreService passwordScoreService, FeatureManager featureManager)
    {
        super(passwordScoreService, null);
        this.featureManager = featureManager;
    }

    @Override
    protected boolean isPasswordPolicyEnabled()
    {
        return featureManager.getDarkFeatures().getGlobalEnabledFeatureKeys().contains(PASSWORD_POLICY_FEATURE);
    }
}