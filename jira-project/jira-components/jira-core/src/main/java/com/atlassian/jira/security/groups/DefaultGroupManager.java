package com.atlassian.jira.security.groups;

import static com.atlassian.crowd.search.EntityDescriptor.group;
import static com.atlassian.crowd.search.query.entity.EntityQuery.ALL_RESULTS;

import java.util.ArrayList;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.GroupQuery;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Default implementation of GroupManager.
 *
 * @since v3.13
 */
public class DefaultGroupManager implements GroupManager
{
    private final CrowdService crowdService;
    private final DirectoryManager directoryManager;

    public DefaultGroupManager(final CrowdService crowdService, final DirectoryManager directoryManager)
    {
        this.crowdService = crowdService;
        this.directoryManager = directoryManager;
    }

    @Override
    public Collection<Group> getAllGroups()
    {
        SearchRestriction restriction = NullRestrictionImpl.INSTANCE;
        final com.atlassian.crowd.embedded.api.Query<Group> query = new GroupQuery<Group>(Group.class, GroupType.GROUP,restriction, 0, -1);
        return convertIterableToCollection(crowdService.search(query));
    }

    public boolean groupExists(final String groupName)
    {
        return getGroup(groupName) != null;
    }

    public Group createGroup(final String groupName) throws OperationNotPermittedException, InvalidGroupException
    {
        return crowdService.addGroup(new ImmutableGroup(groupName));
    }

/*    public Group getGroup(final String groupName)
    {
        return crowdService.getGroup(groupName);
    }*/
    
    public Group getGroup(final String groupName)
    {
    	String predictGroupName = groupName;
    	//Iterate jira Projects and get their keys and append with group Name
    	List<Project> projects =  ComponentAccessor.getProjectManager().getProjectObjects();
    	Group group;
    	for (Iterator<Project> iterator = projects.iterator(); iterator.hasNext();) {
			Project project = (Project) iterator.next();
			group = crowdService.getGroup(project.getKey() + "-" + groupName);
			if(group != null && !group.getName().isEmpty())
			{
				return group;
			}
		}
    	
/*    	String[] ids = (String[]) ActionContext.getParameters().get("pid");
    	if(ids !=null && ids.length >0)
    	{	
	    	String pid = ids[0];
	    	@SuppressWarnings("deprecation")
			Project predictProject = ComponentManager.getInstance().getProjectManager().getProjectObj(Long.parseLong(pid));
	    	if(predictProject!=null && !predictProject.getKey().isEmpty()){
	    		predictGroupName = predictProject.getKey() + "-" + groupName;
	    	}
    	}*/
        return crowdService.getGroup(predictGroupName);
    }

    @Override
    public Group getGroupEvenWhenUnknown(final String groupName)
    {
        if (groupExists(groupName))
        {
            return getGroup(groupName);
        }
        else
        {
            return new ImmutableGroup(groupName);
        }
    }

    @Override
    public Group getGroupObject(String groupName)
    {
        return getGroup(groupName);
    }

    @Override
    public boolean isUserInGroup(final String username, final String groupname)
    {
        if (username == null || groupname == null)
        {
            // This is done because this was the old behaviour in OSUser for group.containsUser() and User.inGroup()
            return false;
        }
        return crowdService.isUserMemberOfGroup(username, groupname);
    }

    @Override
    public boolean isUserInGroup(final User user, final Group group)
    {
        if (user == null || group == null)
        {
            // This is done because this was the old behaviour in OSUser for group.containsUser() and User.inGroup()
            return false;
        }
        return isUserInGroup(user, group.getName());
    }

    @Override
    public boolean isUserInGroup(final User user, final String groupname)
    {
        if (user == null || groupname == null)
        {
            // Historical behaviour for nulls
            return false;
        }
        // JDEV-30356: Performance improvement.
        // We can talk directly to the embedded crowd DirectoryManager in order to avoid looking up the user by username
        try
        {
            return directoryManager.isUserNestedGroupMember(user.getDirectoryId(), user.getName(), groupname);
        }
        catch (final OperationFailedException ex)
        {
            // rethrow as runtime version
            throw new com.atlassian.crowd.exception.runtime.OperationFailedException(ex);
        }
        catch (DirectoryNotFoundException e)
        {
            // directory not found - very unlucky
            throw new ConcurrentModificationException("Directory mapping was removed while determining if user is a nested group member: " + e.getMessage());
        }
    }

    public Collection<User> getUsersInGroup(final String groupName)
    {
        Iterable<User> usersIterable = crowdService.search(
                QueryBuilder.queryFor(User.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(groupName).returningAtMost(ALL_RESULTS));
        return convertIterableToCollection(usersIterable);
    }
    
	public Group getPredictGroup(final String groupName)
    {
        return crowdService.getGroup(groupName);
    }

    public Collection<User> getUsersInGroup(final Group group)
    {
        //return getUsersInGroup(group.getPredictName());
    	return getUsersInGroup(group.getName());
    }

    public Collection<String> getUserNamesInGroup(final Group group)
    {
        return getUserNamesInGroup(group.getName());
    }

    public Collection<String> getUserNamesInGroup(final String groupName)
    {
        Iterable<String> usersIterable = crowdService.search(
                QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(groupName).returningAtMost(ALL_RESULTS));
        return convertIterableToCollection(usersIterable);
    }

    public Collection<User> getDirectUsersInGroup(final Group group)
    {
        Collection<User> usersInGroup = getUsersInGroup(group.getName());
        // Remove any indirect members
        for (Iterator<User> iter = usersInGroup.iterator(); iter.hasNext(); )
        {
            if (!crowdService.isUserDirectGroupMember(iter.next(), group))
            {
                iter.remove();
            }
        }
        return usersInGroup;
    }

    public Collection<Group> getGroupsForUser(final String userName)
    {
        Iterable<Group> searchResults = crowdService.search(
                QueryBuilder.queryFor(Group.class, group()).parentsOf(EntityDescriptor.user()).withName(userName).returningAtMost(ALL_RESULTS));
        return convertIterableToCollection(searchResults);
    }

    public Collection<Group> getGroupsForUser(final User user)
    {
        return getGroupsForUser(user.getName());
    }

    public Collection<String> getGroupNamesForUser(final String userName)
    {
        Iterable<String> usersIterable = crowdService.search(
                QueryBuilder.queryFor(String.class, group()).parentsOf(EntityDescriptor.user()).withName(userName).returningAtMost(ALL_RESULTS));
        return convertIterableToCollection(usersIterable);
    }

    @Override
    public Collection<String> getGroupNamesForUser(final User user)
    {
        return getGroupNamesForUser(user.getName());
    }

    @Override
    public Collection<String> getGroupNamesForUser(final ApplicationUser user)
    {
        return getGroupNamesForUser(user.getName());
    }

    private <T> Collection<T> convertIterableToCollection(Iterable<T> iterable)
    {
        // try to just cast if possible (will work on current implementation of Crowd)
        if (iterable instanceof Collection)
        {
            return (Collection<T>) iterable;
        }
        // Cast didn't work - do the work to create a Collection.
        final Collection<T> collection = new ArrayList<T>();
        for (T member : iterable)
        {
            collection.add(member);
        }
        return collection;
    }

    public void addUserToGroup(User user, Group group) throws GroupNotFoundException, UserNotFoundException, OperationNotPermittedException, OperationFailedException
    {
        crowdService.addUserToGroup(user, group);
    }
}
