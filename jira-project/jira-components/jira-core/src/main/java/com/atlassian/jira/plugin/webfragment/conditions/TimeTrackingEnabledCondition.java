package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.bc.issue.worklog.WorklogService;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Condition to determine whether time tracking is turned on or not
 *
 * @since v4.1
 */
public class TimeTrackingEnabledCondition extends AbstractWebCondition
{
    private static final Logger log = LoggerFactory.getLogger(TimeTrackingEnabledCondition.class);
    private final WorklogService worklogService;

    public TimeTrackingEnabledCondition(WorklogService worklogService)
    {
        this.worklogService = worklogService;
    }

    public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper)
    {
        return worklogService.isTimeTrackingEnabled();
    }
}
