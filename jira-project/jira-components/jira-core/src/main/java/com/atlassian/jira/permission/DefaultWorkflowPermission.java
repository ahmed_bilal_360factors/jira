package com.atlassian.jira.permission;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.security.type.SecurityType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.workflow.WorkflowUtil;
import org.ofbiz.core.entity.GenericValue;

import java.util.Set;

/**
 * Represents a single permission granted in the JIRA workflow XML, eg:
 * <pre>
 &lt;meta name="jira.permission.edit.group">${pkey}-interest&lt;/meta>
 </pre>
 or
 <pre>
 &lt;meta name="jira.permission.delete.lead">&lt;/meta>
 </pre>
 */
public class DefaultWorkflowPermission implements WorkflowPermission
{
    public static final String PREFIX = "jira.permission.";
    public static final String PREFIX_PARENT = PREFIX + "subtasks.";

    private SecurityType grantType;
    private boolean parentPermission;
    private ProjectPermissionKey permission;
    private String value;

    protected DefaultWorkflowPermission(ProjectPermissionKey permission, SecurityType grantType, String value, boolean isParentPermission)
    {
        this.grantType = grantType;
        this.parentPermission = isParentPermission;
        this.permission = permission;
        this.value = value;
    }

    public Set getUsers(PermissionContext ctx)
    {
        return grantType.getUsers(ctx, getValueFor(ctx.getIssue()));
    }

    @Override
    public boolean allows(ProjectPermissionKey permission, Issue issue, ApplicationUser user)
    {
        return user == null
                ? grantType.hasPermission(issue, getValueFor(issue))
                : grantType.hasPermission(issue, getValueFor(issue), user.getDirectoryUser(), false);
    }

    public String toString()
    {
        return (parentPermission ? "parent " : "") + "workflow perm granting " + permission.permissionKey() + " to " + grantType.getDisplayName() + " '" + value + "'";
    }


    /** Get the value of the meta attribute, with '${pkey}' evaluated to the project key. */
    private String getValueFor(Issue issue)
    {
        GenericValue project = issue.getProject();
        return WorkflowUtil.interpolateProjectKey(project, value);
    }

}
