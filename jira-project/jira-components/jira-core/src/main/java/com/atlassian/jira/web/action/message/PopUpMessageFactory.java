package com.atlassian.jira.web.action.message;

import com.atlassian.jira.web.action.JiraWebActionSupport.MessageType;

import org.apache.commons.lang.StringEscapeUtils;

/**
 * Creates {@link com.atlassian.jira.web.action.message.PopUpMessage} instances.
 *
 * @since v6.4
 */
public final class PopUpMessageFactory
{
    /**
     * @param type type (severity) of the message
     * @param title plain text title
     * @param plainMessage plaintext body of the message
     * @param closingPolicy closing policy compatible with AUI flags
     */
    public final PopUpMessage createPlainMessage(
            final MessageType type,
            final String title,
            final String plainMessage,
            final ClosingPolicy closingPolicy)
    {
        final String htmlMessage = StringEscapeUtils.escapeHtml(plainMessage);
        return createHtmlMessage(type, title, htmlMessage, closingPolicy);
    }

    /**
     * @param type type (severity) of the message
     * @param title plain text title
     * @param htmlMessage HTML body of the message
     * @param closingPolicy closing policy compatible with AUI flags
     */
    public final PopUpMessage createHtmlMessage(
            final MessageType type,
            final String title,
            final String htmlMessage,
            final ClosingPolicy closingPolicy)
    {
        return new PopUpMessage(type, title, htmlMessage, closingPolicy);
    }

}
