package com.atlassian.jira.issue.fields.config;

import com.atlassian.jira.entity.AbstractEntityFactory;
import com.atlassian.jira.ofbiz.FieldMap;
import org.ofbiz.core.entity.GenericValue;

import java.util.Map;

import static com.atlassian.jira.issue.fields.config.ConfigurationContext.FIELD_CONFIG_SCHEME_ID;
import static com.atlassian.jira.issue.fields.config.ConfigurationContext.FIELD_ID;
import static com.atlassian.jira.issue.fields.config.ConfigurationContext.ID;
import static com.atlassian.jira.issue.fields.config.ConfigurationContext.PROJECT_ID;
import static com.atlassian.jira.issue.fields.config.ConfigurationContext.PROJECT_CATEGORY_ID;

public class ConfigurationContextEntityFactory extends AbstractEntityFactory<ConfigurationContext>
{
    @Override
    public Map<String, Object> fieldMapFrom(final ConfigurationContext value)
    {
        return FieldMap.build(ID, value.getId())
                .add(PROJECT_CATEGORY_ID, value.getProjectCategoryId())
                .add(PROJECT_ID, value.getProjectId())
                .add(FIELD_ID, value.getFieldId())
                .add(FIELD_CONFIG_SCHEME_ID, value.getFieldConfigSchemeId());
    }

    @Override
    public String getEntityName()
    {
        return "ConfigurationContext";
    }

    @Override
    public ConfigurationContext build(final GenericValue gv)
    {
        return new ConfigurationContext(gv.getLong(ID),
                gv.getLong(PROJECT_CATEGORY_ID),
                gv.getLong(PROJECT_ID),
                gv.getString(FIELD_ID),
                gv.getLong(FIELD_CONFIG_SCHEME_ID)
                );
    }
}
