package com.atlassian.jira.index.property;

/**
 * Manager for JQL aliases registered by add-ons.
 */
public interface JqlAliasManager
{
    /**
     * @return all JQL aliases registered by add-ons.
     */
    Iterable<JqlAlias> getJqlAliases();
}
