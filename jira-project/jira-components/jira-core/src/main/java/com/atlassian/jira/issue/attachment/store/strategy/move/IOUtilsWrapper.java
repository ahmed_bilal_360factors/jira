package com.atlassian.jira.issue.attachment.store.strategy.move;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.atlassian.jira.util.RuntimeIOException;

import com.google.common.base.Function;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @since v6.4
 */
public class IOUtilsWrapper
{
    private static final Logger log = LoggerFactory.getLogger(IOUtilsWrapper.class);

    public InputStream openInputStream(final File file)
    {
        try
        {
            return FileUtils.openInputStream(file);
        }
        catch (final IOException e)
        {
            throw new RuntimeIOException(e);
        }
    }

    public FileOutputStream openOutputStream(final File file)
    {
        try
        {
            return FileUtils.openOutputStream(file);
        }
        catch (final IOException e)
        {
            throw new RuntimeIOException(e);
        }
    }

    public int copy(final InputStream inputStream, final OutputStream output)
    {
        try
        {
            return IOUtils.copy(inputStream, output);
        }
        catch (final IOException e)
        {
            throw new RuntimeIOException(e);
        }
    }

    public File createTempFile(final String prefix, final String suffix)
    {
        try
        {
            return File.createTempFile(prefix, suffix);
        }
        catch (final IOException e)
        {
            throw new RuntimeIOException(e);
        }
    }

    public <A> A processTemporaryFile(final String prefix, final String suffix, final Function<File, A> operation)
    {
        final File temporaryFile = createTempFile(prefix, suffix);
        try
        {
            return operation.apply(temporaryFile);
        }
        finally
        {
            final boolean deleted = temporaryFile.delete();
            if (!deleted)
            {
                log.warn("Failed to delete temporary file: " + temporaryFile);
            }
        }
    }
}
