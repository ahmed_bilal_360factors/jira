package com.atlassian.jira.model.querydsl;

import com.mysema.query.sql.ColumnMetadata;
import com.mysema.query.types.path.*;

import java.sql.Types;
import javax.annotation.Generated;

/**
 * QTrackbackPing is a Querydsl query object.
 *
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QTrackbackPing extends JiraRelationalPathBase<TrackbackPingDTO>
{
    public static final QTrackbackPing TRACKBACK_PING = new QTrackbackPing("TRACKBACK_PING");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final NumberPath<Long> issue = createNumber("issue", Long.class);
    public final StringPath url = createString("url");
    public final StringPath title = createString("title");
    public final StringPath blogname = createString("blogname");
    public final StringPath excerpt = createString("excerpt");
    public final DateTimePath<java.sql.Timestamp> created = createDateTime("created", java.sql.Timestamp.class);

    public QTrackbackPing(String alias)
    {
        super(TrackbackPingDTO.class, alias, "trackback_ping");
        addMetadata();
    }

    private void addMetadata()
    {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(issue, ColumnMetadata.named("issue").withIndex(2).ofType(Types.NUMERIC).withSize(18));
        addMetadata(url, ColumnMetadata.named("url").withIndex(3).ofType(Types.VARCHAR).withSize(255));
        addMetadata(title, ColumnMetadata.named("title").withIndex(4).ofType(Types.VARCHAR).withSize(255));
        addMetadata(blogname, ColumnMetadata.named("blogname").withIndex(5).ofType(Types.VARCHAR).withSize(255));
        addMetadata(excerpt, ColumnMetadata.named("excerpt").withIndex(6).ofType(Types.VARCHAR).withSize(255));
        addMetadata(created, ColumnMetadata.named("created").withIndex(7).ofType(Types.TIMESTAMP).withSize(35));
    }
}

