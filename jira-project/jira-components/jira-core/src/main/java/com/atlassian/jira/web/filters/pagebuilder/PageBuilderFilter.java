package com.atlassian.jira.web.filters.pagebuilder;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.web.filters.CommittedResponseHtmlErrorRecoveryFilter;
import com.atlassian.jira.web.pagebuilder.DecoratablePage;
import com.atlassian.jira.web.pagebuilder.PageBuilderServiceSpi;

import com.opensymphony.module.sitemesh.HTMLPage;
import com.opensymphony.module.sitemesh.Page;
import com.opensymphony.sitemesh.Content;
import com.opensymphony.sitemesh.compatability.HTMLPage2Content;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A filter that runs decoration. Runs before Sitemesh decoration.
 * @since 6.4
 */
public class PageBuilderFilter implements Filter
{
    private static final Logger log = LoggerFactory.getLogger(PageBuilderFilter.class);
    private final String filterName;
    private FilterConfig filterConfig;

    public PageBuilderFilter()
    {
        filterName = this.getClass().getCanonicalName() + "_alreadyfiltered";
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
        this.filterConfig = filterConfig;
    }

    @Override
    public void destroy()
    {
        this.filterConfig = null;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException
    {
        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;

        // Disallow more than one active PageBuilderFilter on the stack at any point in time.
        // (Inherited from ChainedFilterStepRunner - probably not actually needed here. It guards against things like
        // the request dispatcher re-adding the filter to the stack during INCLUDE processing, while allowing the filter
        // to be reused after the stack has been unwound, as would happen during FORWARD / ERROR processing.)
        // PageBuilderFilter is only configured for the default dispatcher (REQUEST) in web.xml anyway.
        if (request.getAttribute(filterName) != null)
        {
            filterChain.doFilter(request, response);
            return;
        }
        request.setAttribute(filterName, Boolean.TRUE);

        if (log.isDebugEnabled())
        {
            log.debug(String.format("PageBuilderFilter decorated servlet path [%s] ", request.getServletPath()));
        }

        // The sitemesh code below assumes that we will always be rendering content-type text/html. This assumption
        // is valid as long as this filter is only mapped to to urls that will always return text/html.
        // If we weren't always rendering text/html we could use Sitemesh's PageParserSelector, however it's unclear
        // how to proceed if we encountered a content-type that's not supported by PageParserSelector.shouldParsePage()
        PageBuilderResponseWrapper responseWrapper = new PageBuilderResponseWrapper(response);

        try
        {
            getPageBuilderServiceSpi().initForRequest(request,
                    response, responseWrapper, filterConfig.getServletContext());

            filterChain.doFilter(request, responseWrapper);

            if (responseWrapper.isBuffering())
            {   // if data was written to our buffer: now would be a good time to decorate it!
                Page page = responseWrapper.getBuffer().parse();
                renderDecoratablePage(new HTMLPage2Content((HTMLPage) page));
            }   // else: do nothing - data was written directly through to the underlying response
        }
        finally
        {
            getPageBuilderServiceSpi().clearForRequest();
            request.removeAttribute(filterName);
        }
    }

    private void renderDecoratablePage(final Content content)
    {
        getPageBuilderServiceSpi().getSpi().finish(new DecoratablePage()
        {
            @Override
            public String getTitle()
            {
                return content.getTitle();
            }

            @Override
            public String getMetaProperty(final String key)
            {
                return content.getProperty("meta." + key);
            }

            @Override
            public String getBodyTagProperty(String key)
            {
                return content.getProperty("body." + key);
            }

            @Override
            public String getPageProperty(String key)
            {
                return content.getProperty("page." + key);
            }

            @Override
            public void writeHead(final Writer writer)
            {
                try
                {
                    content.writeHead(writer);
                }
                catch (IOException ex)
                {
                    throw new RuntimeException(ex);
                }
            }

            @Override
            public void writeBody(final Writer writer)
            {
                try
                {
                    content.writeBody(writer);
                }
                catch (IOException ex)
                {
                    throw new RuntimeException(ex);
                }
            }
        });
    }

    private PageBuilderServiceSpi getPageBuilderServiceSpi()
    {
        return ComponentAccessor.getComponentOfType(PageBuilderServiceSpi.class);
    }
}
