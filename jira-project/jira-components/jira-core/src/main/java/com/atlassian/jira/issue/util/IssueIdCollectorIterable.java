package com.atlassian.jira.issue.util;

import java.io.IOException;
import java.util.Collection;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.jql.query.IssueIdCollector;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;

/**
 * Turns issue IDs from an IssueIdCollector into an IssueIdsIssueIterable.
 *
 * @since 6.4
 */
public class IssueIdCollectorIterable extends IssueIdsIssueIterable
{
    public IssueIdCollectorIterable(@Nonnull final IssueIdCollector collector, @Nonnull final IssueManager issueManager)
    throws IOException
    {
        super(readIssueIds(collector), issueManager);
    }

    @Nonnull
    private static Collection<Long> readIssueIds(@Nonnull IssueIdCollector collector)
    throws IOException
    {
        return ImmutableList.copyOf(Collections2.transform(collector.getIssueIds(), new StringToLongFunction()));
    }

    private static class StringToLongFunction implements Function<String, Long>
    {
        @Override
        public Long apply(@Nullable String input)
        {
            return Long.valueOf(input);
        }
    }
}