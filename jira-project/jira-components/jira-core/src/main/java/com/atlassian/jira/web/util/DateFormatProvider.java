package com.atlassian.jira.web.util;

import java.io.IOException;
import java.io.Writer;
import java.text.DateFormatSymbols;
import java.util.Arrays;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import com.google.common.collect.ImmutableMap;

/**
 * Provides localized version of names of months, days, eras and meridiem.
 */
public class DateFormatProvider implements WebResourceDataProvider
{
    private final JiraAuthenticationContext authenticationContext;

    public DateFormatProvider(final JiraAuthenticationContext authenticationContext)
    {
        this.authenticationContext = authenticationContext;
    }

    @Override
    public Jsonable get()
    {
        final DateFormatSymbols dateFormatSymbols = DateFormatSymbols.getInstance(authenticationContext.getLocale());

            return new Jsonable()
            {
                @Override
                public void write(final Writer writer) throws IOException
                {
                    try
                    {
                        getDateFormatsJson(dateFormatSymbols).write(writer);
                    }
                    catch (JSONException e)
                    {
                        throw new JsonMappingException(e);
                    }
                }
            };
    }

    private JSONObject getDateFormatsJson(final DateFormatSymbols dateFormatSymbols)
    {
        final ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();

        builder.put("meridiem", Arrays.asList(dateFormatSymbols.getAmPmStrings()));
        builder.put("eras", Arrays.asList(dateFormatSymbols.getEras()));
        builder.put("months", Arrays.asList(dateFormatSymbols.getMonths()).subList(0, 12));
        builder.put("monthsShort", Arrays.asList(dateFormatSymbols.getShortMonths()).subList(0, 12));
        builder.put("weekdaysShort", Arrays.asList(dateFormatSymbols.getShortWeekdays()).subList(1, 8));
        builder.put("weekdays", Arrays.asList(dateFormatSymbols.getWeekdays()).subList(1, 8));

        return new JSONObject(builder.build());
    }
}
