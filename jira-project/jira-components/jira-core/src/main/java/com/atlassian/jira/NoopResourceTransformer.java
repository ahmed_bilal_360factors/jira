package com.atlassian.jira;

import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.transformer.TransformableResource;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;
import com.atlassian.plugin.webresource.url.UrlBuilder;

import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This is an implementation of resource transformer that does not transform resource. It was added here in order to
 * implement fake i18n transformer in atlassian-plugins-webresource-plugin-mock.xml
 *
 */
public class NoopResourceTransformer implements WebResourceTransformerFactory
{
    @Override
    public TransformerUrlBuilder makeUrlBuilder(final TransformerParameters parameters)
    {
        return new TransformerUrlBuilder()
        {
            @Override
            public void addToUrl(final UrlBuilder urlBuilder)
            {
                //noop
            }
        };
    }

    @Override
    public UrlReadingWebResourceTransformer makeResourceTransformer(final TransformerParameters parameters)
    {
       return new UrlReadingWebResourceTransformer()
       {
           @Override
           public DownloadableResource transform(final TransformableResource transformableResource, final QueryParams params)
           {
               return new DownloadableResource()
               {
                   @Override
                   public boolean isResourceModified(final HttpServletRequest request, final HttpServletResponse response)
                   {
                       return transformableResource.nextResource().isResourceModified(request, response);
                   }

                   @Override
                   public void serveResource(final HttpServletRequest request, final HttpServletResponse response)
                           throws DownloadException
                   {
                       transformableResource.nextResource().serveResource(request, response);
                   }

                   @Override
                   public void streamResource(final OutputStream out) throws DownloadException
                   {
                       transformableResource.nextResource().streamResource(out);
                   }

                   @Override
                   public String getContentType()
                   {
                       return transformableResource.nextResource().getContentType();
                   }
               };
           }
       };
    }
}
