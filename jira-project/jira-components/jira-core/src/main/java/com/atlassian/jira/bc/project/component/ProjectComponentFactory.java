package com.atlassian.jira.bc.project.component;

import java.util.Map;

import com.atlassian.jira.entity.AbstractEntityFactory;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.project.AssigneeTypes;

import org.ofbiz.core.entity.GenericValue;

public class ProjectComponentFactory extends AbstractEntityFactory<ProjectComponent>
{
    //  <entity entity-name="Component" table-name="component" package-name="">
    //    <field name="id" type="numeric"/>
    //    <field name="project" type="numeric"/>
    //    <field name="name" col-name="cname" type="long-varchar"/>
    //    <field name="description" col-name="description" type="very-long"/>
    //    <field name="url" type="long-varchar"/>
    //    <field name="lead" type="long-varchar"/>
    //    <field name="assigneetype" type="numeric"/>
    //  </entity>

    @Override
    public Map<String, Object> fieldMapFrom(final ProjectComponent projectComponent)
    {
        return new FieldMap("id", projectComponent.getId())
                .add("project", projectComponent.getProjectId())
                .add("name", projectComponent.getName())
                .add("description", projectComponent.getDescription())
                        // URL is currently not included in the object
//                .add("url", projectComponent.getXxx())
                .add("lead", projectComponent.getLead())
                .add("assigneetype", projectComponent.getAssigneeType());
    }

    @Override
    public String getEntityName()
    {
        return "Component";
    }

    @Override
    public ProjectComponent build(final GenericValue gv)
    {
        return new ProjectComponentImpl(
                gv.getLong("id"),
                gv.getString("name"),
                gv.getString("description"),
                gv.getString("lead"),
                interpretAssigneeType(gv.getLong("assigneetype")),
                gv.getLong("project"),
                gv
        );
    }

    /**
     * Converts Long to primitive, most importantly knowing how to interpret a NULL value.
     * @param assigneetype assigneetype as stored in the DB
     * @return primitive long for the given Long, converting NULL correctly
     */
    private long interpretAssigneeType(final Long assigneetype)
    {
        if (assigneetype == null)
        {
            // if the component has no assigneetype, let the project decide the assignee type
            return AssigneeTypes.PROJECT_DEFAULT;
        }
        else
        {
            return assigneetype.longValue();
        }
    }
}
