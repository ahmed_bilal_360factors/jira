package com.atlassian.jira.upgrade;

import com.atlassian.jira.issue.index.IssueIndexingParams;

public abstract class AbstractImmediateUpgradeTask extends AbstractUpgradeTask
{
    protected AbstractImmediateUpgradeTask()
    {
        super();
    }

    @Override
    public ScheduleOption getScheduleOption()
    {
        return ScheduleOption.BEFORE_JIRA_STARTED;
    }

}
