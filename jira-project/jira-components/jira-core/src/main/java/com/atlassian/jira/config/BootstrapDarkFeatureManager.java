package com.atlassian.jira.config;

import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.features.EnabledDarkFeatures;
import com.atlassian.sal.api.user.UserKey;

/**
 * Dark feature manager used during JIRA bootstrap. Effectively only dark features that are set in
 * jira-features.properties or in command line are taken into account.
 */
public class BootstrapDarkFeatureManager implements DarkFeatureManager
{

    public BootstrapDarkFeatureManager()
    {
    }

    @Override
    public boolean isFeatureEnabledForAllUsers(final String featureKey)
    {
        return false;
    }

    @Override
    public boolean isFeatureEnabledForCurrentUser(final String featureKey)
    {
        return false;
    }

    @Override
    public boolean isFeatureEnabledForUser(final UserKey userKey, final String featureKey)
    {
        return false;
    }

    @Override
    public boolean canManageFeaturesForAllUsers()
    {
        return false;
    }

    @Override
    public void enableFeatureForAllUsers(final String s)
    {
        throw new UnsupportedOperationException("This operation is not supported during setup");
    }

    @Override
    public void disableFeatureForAllUsers(final String s)
    {
        throw new UnsupportedOperationException("This operation is not supported during setup");
    }

    @Override
    public void enableFeatureForCurrentUser(final String s)
    {
        throw new UnsupportedOperationException("This operation is not supported during setup");
    }

    @Override
    public void enableFeatureForUser(final UserKey userKey, final String s)
    {
        throw new UnsupportedOperationException("This operation is not supported during setup");
    }

    @Override
    public void disableFeatureForCurrentUser(final String s)
    {
        throw new UnsupportedOperationException("This operation is not supported during setup");
    }

    @Override
    public void disableFeatureForUser(final UserKey userKey, final String s)
    {
        throw new UnsupportedOperationException("This operation is not supported during setup");
    }

    @Override
    public EnabledDarkFeatures getFeaturesEnabledForAllUsers()
    {
        return EnabledDarkFeatures.NONE;
    }

    @Override
    public EnabledDarkFeatures getFeaturesEnabledForCurrentUser()
    {
        return EnabledDarkFeatures.NONE;
    }

    @Override
    public EnabledDarkFeatures getFeaturesEnabledForUser(final UserKey userKey)
    {
        return EnabledDarkFeatures.NONE;
    }
}
