package com.atlassian.jira.license;

import javax.annotation.Nonnull;

import com.atlassian.annotations.ExperimentalApi;

import com.google.common.collect.ImmutableList;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Event fired when a JIRA administrator requested to continue with evaluation license when the license support
 * expired for the version of JIRA installed. It contains the administrator's user name and the expired licenses.
 *
 * @since v6.4
 */

@ExperimentalApi
public class ConfirmEvaluationLicenseEvent
{
    private final String userWhoConfirmed;
    private final Iterable<LicenseDetails> expiredLicenses;

    public ConfirmEvaluationLicenseEvent(@Nonnull String userWhoConfirmed, final @Nonnull Iterable<LicenseDetails> expiredLicenses)
    {
        checkNotNull(userWhoConfirmed, "UserWhoConfirmed");
        checkNotNull(expiredLicenses, "expiredLicenses");
        this.userWhoConfirmed = userWhoConfirmed;
        this.expiredLicenses = ImmutableList.copyOf(expiredLicenses);
    }

    @Nonnull
    public String getUserWhoConfirmed()
    {
        return userWhoConfirmed;
    }

    public Iterable<LicenseDetails> getExpiredLicenses()
    {
        return expiredLicenses;
    }
}
