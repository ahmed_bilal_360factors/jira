package com.atlassian.jira.bulkedit;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.LongIdsValueHolder;
import com.atlassian.jira.issue.fields.OrderableField;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
 * Represents the Bulk Edit multi select field option for which the field values provided in Bulk Edit will be added to the already set field values
 *
 * @since v6.4
 */
public class BulkEditMultiSelectFieldOptionAdd extends AbstractBulkEditMultiSelectFieldOption implements BulkEditMultiSelectFieldOption
{
    private static final String ID = "add";
    private static final String NAME_I18N_KEY = "bulkedit.actions.multiselect.field.option.ADD";
    private static final String DESCRIPTION_I18N_KEY = "bulkedit.actions.multiselect.field.option.ADD";

    public String getId()
    {
        return ID;
    }

    public String getNameI18nKey()
    {
        return NAME_I18N_KEY;
    }

    public String getDescriptionI18nKey()
    {
        return DESCRIPTION_I18N_KEY;
    }

    @Override
    public Map<String,Object> getFieldValuesMap(final Issue issue, final OrderableField field, final Map<String, Object> fieldValuesHolder)
    {

        final Map<String, Object> issueFieldValuesHolder = Maps.newHashMap();
        field.populateFromIssue(issueFieldValuesHolder, issue);
        final Collection<Object> fieldValuesFromIssue = (Collection<Object>) issueFieldValuesHolder.get(field.getId());
        final Collection<Object> fieldValuesBulkChange = (Collection<Object>) fieldValuesHolder.get(field.getId());
        Collection<Object> accumulatedFieldValues;
        if (fieldValuesHolder.get(field.getId()) instanceof LongIdsValueHolder)
        {
            accumulatedFieldValues = Lists.newArrayList(fieldValuesFromIssue);
            accumulatedFieldValues.addAll(fieldValuesBulkChange);
            LongIdsValueHolder valueHolder = new LongIdsValueHolder(new ArrayList<Long>((Collection) accumulatedFieldValues));

            Set<String> valuesToAdd = LongIdsValueHolder.fromFieldValuesHolder(field.getId(),fieldValuesHolder).getValuesToAdd();
            if (!valuesToAdd.equals(Collections.<String>emptySet()))
            {
                valueHolder.setValuesToAdd(valuesToAdd);
            }

            Map<String,Object> valuesMap = Maps.newHashMap();
            valuesMap.put(field.getId(), valueHolder);
            return valuesMap;
        }
        else if (fieldValuesHolder.get(field.getId()) instanceof Set)
        {
            accumulatedFieldValues = Sets.newHashSet(fieldValuesFromIssue);
            accumulatedFieldValues.addAll(fieldValuesBulkChange);

            Map<String,Object> valuesMap = Maps.newHashMap();
            valuesMap.put(field.getId(), accumulatedFieldValues);
            return valuesMap;
        }
        else
        {
            return fieldValuesHolder;
        }
    }
}
