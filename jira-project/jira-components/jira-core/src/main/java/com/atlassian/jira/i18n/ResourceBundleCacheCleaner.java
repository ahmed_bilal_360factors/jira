package com.atlassian.jira.i18n;

import java.util.List;
import java.util.ResourceBundle;

import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.osgi.util.BundleClassLoaderAccessor;

import com.google.common.base.Supplier;
import com.google.common.collect.Lists;

public class ResourceBundleCacheCleaner
{
    private final Supplier<PluginAccessor> pluginAccessor;
    private final JiraProperties jiraProperties;

    public ResourceBundleCacheCleaner(final ComponentLocator locator, JiraProperties jiraProperties)
    {
        this.jiraProperties = jiraProperties;
        this.pluginAccessor = locator.getComponentSupplier(PluginAccessor.class);
    }

    /**
     * Clear internal {@link java.util.ResourceBundle#cacheList} from plugin bundles. All plugin related bundles are
     * cached in I18nHelper so keeping a copy in {@link java.util.ResourceBundle#cacheList} will only be duplication.
     */
    public void cleanPluginBundlesFromResourceBundleCache()
    {
        if(!jiraProperties.isResourceBundleCacheCleaningDisabled())
        {
            final List<Plugin> enabledPlugins = Lists.newArrayList(pluginAccessor.get().getEnabledPlugins());
            for (final Plugin plugin : enabledPlugins)
            {
                final ClassLoader pluginClassLoader = plugin.getClassLoader();
                if (isPluginClassLoader(pluginClassLoader))
                {
                    ResourceBundle.clearCache(pluginClassLoader);
                }
            }
        }
    }

    private boolean isPluginClassLoader(ClassLoader pluginClassLoader)
    {
        return pluginClassLoader != null && pluginClassLoader.getClass().getName().startsWith
                (BundleClassLoaderAccessor.class.getName());
    }
}
