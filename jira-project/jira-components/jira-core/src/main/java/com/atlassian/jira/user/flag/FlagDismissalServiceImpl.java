package com.atlassian.jira.user.flag;

import java.util.Iterator;
import java.util.Set;

import com.atlassian.core.AtlassianCoreException;
import com.atlassian.core.util.Clock;
import com.atlassian.jira.propertyset.JiraPropertySetFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;

import com.opensymphony.module.propertyset.PropertySet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.emptySet;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class FlagDismissalServiceImpl implements FlagDismissalService
{
    private static final String DISMISSALS_KEY = "com.atlassian.jira.flag.dismissals";
    private static final String RESETS_KEY = "com.atlassian.jira.flag.resets";
    private final UserPreferencesManager userPreferencesManager;
    private final JiraPropertySetFactory jiraPropertySetFactory;
    private final Clock clock;
    private static final Logger log = LoggerFactory.getLogger(FlagDismissalServiceImpl.class);

    public FlagDismissalServiceImpl(
            final UserPreferencesManager userPreferencesManager,
            final JiraPropertySetFactory jiraPropertySetFactory,
            final Clock clock)
    {
        this.userPreferencesManager = userPreferencesManager;
        this.jiraPropertySetFactory = jiraPropertySetFactory;
        this.clock = clock;
    }

    @Override
    public void dismissFlagForUser(String flagKey, ApplicationUser user)
    {
        if (user == null || isBlank(flagKey))
        {
            return;
        }
        JSONObject dismissals = getDismissalsForUser(user);
        try
        {
            dismissals.put(flagKey, clock.getCurrentDate().getTime());
            setDismissalsForUser(user, dismissals);
        }
        catch (JSONException e)
        {
            log.debug("Exception occurred while trying to dismiss flag:", e);
        }
    }

    @Override
    public void resetFlagDismissals(String flagKey)
    {
        JSONObject resets = getDismissalResets();

        try
        {
            resets.put(flagKey, clock.getCurrentDate().getTime());
            getPropertySet().setText(RESETS_KEY, resets.toString());
        }
        catch (JSONException e)
        {
            log.debug("Exception occurred while trying reset flag dismissal:", e);
        }
    }

    @Override
    public Set<String> getDismissedFlagsForUser(ApplicationUser user)
    {
        JSONObject userDismissalData = getDismissalsForUser(user);
        try
        {
            Set<String> currentDismissals = newHashSet();

            Iterator<String> dismissedKeys = userDismissalData.keys();
            JSONObject resetTimes = getDismissalResets();
            while (dismissedKeys.hasNext())
            {
                String dismissedFlag = dismissedKeys.next();
                long lastDismissal = userDismissalData.getLong(dismissedFlag);
                if (!resetTimes.has(dismissedFlag) || resetTimes.getLong(dismissedFlag) < lastDismissal)
                {
                    currentDismissals.add(dismissedFlag);
                }
            }
            return currentDismissals;
        }
        catch (JSONException e)
        {
            log.debug("Exception occurred while trying to retrieve dismissed flags:", e);
            return emptySet();
        }
    }

    private PropertySet getPropertySet() {
        return jiraPropertySetFactory.buildCachingDefaultPropertySet(getClass().getCanonicalName());
    }

    private JSONObject getDismissalResets()
    {
        String dismissalResets = getPropertySet().getText(RESETS_KEY);
        try
        {
            return isBlank(dismissalResets) ? new JSONObject() : new JSONObject(dismissalResets);
        }
        catch (JSONException e)
        {
            return new JSONObject();
        }
    }

    private JSONObject getDismissalsForUser(ApplicationUser user)
    {
        String dismissals = userPreferencesManager.getExtendedPreferences(user).getText(DISMISSALS_KEY);
        try
        {
            return isBlank(dismissals) ? new JSONObject() : new JSONObject(dismissals);
        }
        catch (JSONException e)
        {
            return new JSONObject();
        }
    }

    private void setDismissalsForUser(ApplicationUser user, JSONObject dismissals)
    {
        try
        {
            userPreferencesManager.getExtendedPreferences(user).setText(DISMISSALS_KEY, dismissals.toString());
        }
        catch (AtlassianCoreException e)
        {
            log.debug("Exception occurred while trying to dismiss flag:", e);
        }
    }
}
