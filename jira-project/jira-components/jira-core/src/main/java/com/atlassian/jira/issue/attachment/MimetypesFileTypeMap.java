package com.atlassian.jira.issue.attachment;

import javax.servlet.ServletContext;

import com.atlassian.jira.web.ServletContextProvider;

public final class MimetypesFileTypeMap
{
    public static String getContentType(final String filename)
    {
        final ServletContext context = ServletContextProvider.getServletContext();
        final String nullableContentType = getNullableContentType(filename, context);
        if (nullableContentType == null)
        {
            return "application/octet-stream";
        }
        return nullableContentType;
    }

    private static String getNullableContentType(final String filename, final ServletContext context)
    {
        if (context != null)
        {
            return context.getMimeType(filename);
        }
        else
        {
            return javax.activation.MimetypesFileTypeMap.getDefaultFileTypeMap().getContentType(filename);
        }
    }

    private MimetypesFileTypeMap()
    {
    }
}
