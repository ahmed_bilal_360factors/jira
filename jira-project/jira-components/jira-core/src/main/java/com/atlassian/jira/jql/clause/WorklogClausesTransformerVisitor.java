package com.atlassian.jira.jql.clause;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.jql.query.WorklogClauseQueryFactory;
import com.atlassian.jira.jql.query.WorklogQueryExecutor;
import com.atlassian.jira.jql.query.WorklogQueryRegistry;
import com.atlassian.jira.util.InjectableComponent;
import com.atlassian.jira.util.NonInjectableComponent;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.ChangedClause;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.ClauseVisitor;
import com.atlassian.query.clause.MultiClause;
import com.atlassian.query.clause.NotClause;
import com.atlassian.query.clause.OrClause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.clause.WasClause;
import com.atlassian.query.operator.Operator;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;

import java.util.Collection;
import java.util.List;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.google.common.collect.Iterables.getOnlyElement;

/**
 * This visitor executes all worklog clauses and replaces them
 * with a clause of the form "issuekey in (issue id-s of found worklogs)".
 *
 * <p>
 *     The purpose is to apply all worklog clauses to single worklog
 *     instead of executing them separately. We need this because
 *     one issue can have many worklogs and worklogs are stored as
 *     documents in a separate index.
 * </p>
 */
@NonInjectableComponent
public class WorklogClausesTransformerVisitor implements ClauseVisitor<Clause>
{
    /**
     * A self-contradictory clause that always evaluates to false. Needed in case worklog query returns 0 results.
     */
    private static final class AlwaysFalseClause extends AndClause
    {
        public AlwaysFalseClause()
        {
            super(new TerminalClauseImpl(IssueFieldConstants.ISSUE_KEY, Operator.EQUALS, "FALSE"),
                    new TerminalClauseImpl(IssueFieldConstants.ISSUE_KEY, Operator.NOT_EQUALS, "FALSE"));
        }
    }

    private static final Clause FALSE = new AlwaysFalseClause();

    private final WorklogQueryRegistry queryRegistry;
    private final WorklogQueryExecutor worklogQueryExecutor;

    private final QueryCreationContext queryCreationContext;

    private WorklogClausesTransformerVisitor(final QueryCreationContext queryCreationContext,
            final WorklogQueryRegistry queryRegistry,
            final WorklogQueryExecutor worklogQueryExecutor)
    {
        this.queryCreationContext = queryCreationContext;
        this.queryRegistry = queryRegistry;
        this.worklogQueryExecutor = worklogQueryExecutor;
    }

    @Override
    public Clause visit(final AndClause andClause)
    {
        return simplify(new AndClause(transformMultiClauseExecutingWorklogConditions(andClause, BooleanClause.Occur.MUST)));
    }

    @Override
    public Clause visit(final OrClause orClause)
    {
        return simplify(new OrClause(transformMultiClauseExecutingWorklogConditions(orClause, BooleanClause.Occur.SHOULD)));
    }

    @Override
    public Clause visit(final TerminalClause clause)
    {
        return queryRegistry.getClauseQueryFactory(queryCreationContext, clause).map(new Function<WorklogClauseQueryFactory, Clause>()
        {
            @Override
            public Clause apply(final WorklogClauseQueryFactory input)
            {
                return toIssueSet(input.getWorklogQuery(queryCreationContext, clause));
            }
        }).getOrElse(clause);
    }

    private Clause simplify(MultiClause multiClause)
    {
        return multiClause.getClauses().size() == 1 ? getOnlyElement(multiClause.getClauses()) : multiClause;
    }

    private Collection<Clause> transformMultiClauseExecutingWorklogConditions(MultiClause multiClause, BooleanClause.Occur operator)
    {
        ImmutableList.Builder<Clause> newSubClauses = ImmutableList.builder();
        BooleanQuery luceneWorklogsQuery = new BooleanQuery();

        for (Clause clause : multiClause.getClauses())
        {
            Option<Query> maybeWorklogQuery = convertClauseToWorklogQuery(clause);
            if (maybeWorklogQuery.isDefined())
            {
                luceneWorklogsQuery.add(maybeWorklogQuery.get(), operator);
            }
            else
            {
                newSubClauses.add(clause.accept(this));
            }
        }

        if (luceneWorklogsQuery.getClauses().length > 0)
        {
            newSubClauses.add(toIssueSet(luceneWorklogsQuery));
        }

        return newSubClauses.build();
    }

    private Option<Query> convertClauseToWorklogQuery(Clause clause)
    {
        if (clause instanceof TerminalClause)
        {
            Option<WorklogClauseQueryFactory> worklogClauseFactory = queryRegistry.getClauseQueryFactory(queryCreationContext, (TerminalClause) clause);
            if (worklogClauseFactory.isDefined())
            {
                return some(worklogClauseFactory.get().getWorklogQuery(queryCreationContext, (TerminalClause) clause));
            }
        }
        return none();
    }

    private Clause toIssueSet(final Query worklogQuery)
    {
        List<Long> issueIds = Lists.newArrayList(worklogQueryExecutor.executeWorklogQuery(worklogQuery, queryCreationContext));
        return issueIds.isEmpty() ? FALSE
                : new TerminalClauseImpl(IssueFieldConstants.ISSUE_KEY, issueIds.toArray(new Long[issueIds.size()]));
    }

    @Override
    public Clause visit(final NotClause notClause)
    {
        return new NotClause(notClause.getSubClause().accept(this));
    }

    @Override
    public Clause visit(final WasClause clause)
    {
        return clause;
    }

    @Override
    public Clause visit(final ChangedClause clause)
    {
        return clause;
    }

    @InjectableComponent
    public static class Factory
    {
        private final WorklogQueryRegistry queryRegistry;
        private final WorklogQueryExecutor worklogQueryExecutor;

        public Factory(final WorklogQueryRegistry queryRegistry, final WorklogQueryExecutor worklogQueryExecutor)
        {
            this.queryRegistry = queryRegistry;
            this.worklogQueryExecutor = worklogQueryExecutor;
        }

        public WorklogClausesTransformerVisitor create(final QueryCreationContext queryCreationContext)
        {
            return new WorklogClausesTransformerVisitor(queryCreationContext, queryRegistry, worklogQueryExecutor);
        }
    }
}
