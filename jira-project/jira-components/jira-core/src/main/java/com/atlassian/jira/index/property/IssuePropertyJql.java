package com.atlassian.jira.index.property;

import javax.annotation.concurrent.Immutable;

import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.query.clause.Property;

import static com.atlassian.jira.entity.property.EntityPropertyType.ISSUE_PROPERTY;

/**
 * Wraps JQL query for an issue property.
 */
@Immutable
public final class IssuePropertyJql
{
    private final String jql;

    public static IssuePropertyJql getIssuePropertyJql(Property property)
    {
        return new IssuePropertyJql(ISSUE_PROPERTY.getJqlName() + property.toString());
    }

    private IssuePropertyJql(final String jql) {this.jql = jql;}

    public String getCanonicalJql()
    {
        return jql;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final IssuePropertyJql that = (IssuePropertyJql) o;

        if (jql != null ? !jql.equals(that.jql) : that.jql != null) { return false; }

        return true;
    }

    @Override
    public int hashCode()
    {
        return jql != null ? jql.hashCode() : 0;
    }
}
