package com.atlassian.jira.issue.attachment.store.strategy.move;

import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.AttachmentKey;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.util.concurrent.Promise;

/**
 * Strategy for moving temporary attachment to real attachment under specified destination key.
 *
 * @since v6.4
 */
@ParametersAreNonnullByDefault
public interface MoveTemporaryAttachmentStrategy
{

    Promise<Unit> moveTemporaryToAttachment(TemporaryAttachmentId temporaryAttachmentId, AttachmentKey destinationKey);

}
