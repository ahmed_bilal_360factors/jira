package com.atlassian.jira.bc.license;

import com.atlassian.jira.EventComponent;
import com.atlassian.jira.license.LicenseRole;
import com.atlassian.jira.license.LicenseRoleId;
import com.atlassian.jira.license.LicenseRoleManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;


public class LicenseRoleAuthorizationServiceImpl implements LicenseRoleAuthorizationService
{
    private final LicenseRoleManager licenseRoleManager;
    private final GroupManager groupManager;

    public LicenseRoleAuthorizationServiceImpl(LicenseRoleManager licenseRoleManager, GroupManager groupManager)
    {
        this.licenseRoleManager = licenseRoleManager;
        this.groupManager = groupManager;
    }

    @Override
    public boolean isLicenseRoleInstalled(@Nonnull LicenseRoleId role)
    {
        return licenseRoleManager.isLicenseRoleInstalled(role);
    }

    @Override
    public boolean canUseRole(@Nullable ApplicationUser user, @Nonnull LicenseRoleId role)
    {
        if (user == null)
        {
            return false;
        }

        LicenseRole licenseRole = licenseRoleManager.getLicenseRole(role).getOrNull();
        if (licenseRole != null)
        {
            for (String groupName : licenseRole.getGroups())
            {
                if (groupManager.isUserInGroup(user.getUsername(), groupName))
                {
                    return true;
                }
            }
        }

        return false;
    }

    @Deprecated
    @Override
    public boolean isAuthenticated(@Nonnull final LicenseRoleId role)
    {
        return isLicenseRoleInstalled(role);
    }

    @Override
    public int getUserCount(@Nonnull final LicenseRoleId roleId)
    {
        return licenseRoleManager.getUserCount(roleId);
    }

    @Deprecated
    @Override
    public boolean isAuthenticated(@Nullable ApplicationUser user, @Nonnull LicenseRoleId role)
    {
        return canUseRole(user, role);
    }
}
