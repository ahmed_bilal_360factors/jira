package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.issue.index.IssueIndexingParams;

/**
 * Indexes worklogs in their own index so they are JQL searchable by date or author.
 * Update is performed only if the time tracking option is enabled and there are any worklogs in the instance.
 */
public class UpgradeTask_Build64006 extends AbstractReindexUpgradeTask
{
    @Override
    public void doUpgrade(final boolean setupMode) throws Exception
    {
        getReindexRequestService().requestReindex(ReindexRequestType.DELAYED, WORKLOG_ONLY, NO_SHARED_ENTITIES);
    }

    @Override
    public String getBuildNumber()
    {
        return "64006";
    }

    @Override
    public String getShortDescription()
    {
        return "Indexes worklogs in their own index so they are JQL searchable.";
    }
}
