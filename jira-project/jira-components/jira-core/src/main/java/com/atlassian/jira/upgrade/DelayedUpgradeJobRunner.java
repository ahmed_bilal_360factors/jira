package com.atlassian.jira.upgrade;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.index.request.ReindexRequest;
import com.atlassian.jira.index.request.ReindexRequestManager;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.ReindexStatus;
import com.atlassian.jira.issue.index.IssueIndexingParams;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.jira.util.index.Contexts;
import com.atlassian.jira.util.index.IndexLifecycleManager;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;

import com.google.common.collect.ImmutableList;

import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

/**
 * Job to run any delayed upgrade tasks.
 *
 * @since v6.4
 */
public class DelayedUpgradeJobRunner implements JobRunner
{
    private static final Logger log = Logger.getLogger(DelayedUpgradeJobRunner.class);
    private static final Comparator<String> BUILD_NUMBER_COMPARATOR = new BuildNumComparator();
    private static final String REINDEX_ALLOWED_PROPERTY="upgrade.reindex.allowed";

    static final String ENTITY = "UpgradeHistory";
    private static final String ID = "id";
    private static final String FIELD_TARGETBUILD = "targetbuild";
    private static final String FIELD_UPGRADECLASS = "upgradeclass";
    private static final String FIELD_STATUS = "status";
    private static final String STATUS_PENDING = "pending";
    private static final String STATUS_COMPLETE = "complete";

    private final EntityEngine entityEngine;
    private final IndexLifecycleManager indexManager;
    private final ClusterLockService clusterLockService;
    private final ApplicationProperties applicationProperties;
    private final ReindexRequestManager reindexRequestManager;
    private final BuildUtilsInfo buildUtilsInfo;

    public DelayedUpgradeJobRunner(final EntityEngine entityEngine, final IndexLifecycleManager indexManager, final ClusterLockService clusterLockService,
            final ApplicationProperties applicationProperties, final ReindexRequestManager reindexRequestManager, final BuildUtilsInfo buildUtilsInfo)
    {
        this.entityEngine = entityEngine;
        this.indexManager = indexManager;
        this.clusterLockService = clusterLockService;
        this.applicationProperties = applicationProperties;
        this.reindexRequestManager = reindexRequestManager;
        this.buildUtilsInfo = buildUtilsInfo;
    }

    @Nullable
    @Override
    public JobRunnerResponse runJob(final JobRunnerRequest jobRunnerRequest)
    {
        ClusterLock lock = clusterLockService.getLockForName(DelayedUpgradeJobRunner.class.getName());
        lock.lock();
        try
        {
            final ImmutableList.Builder<String> errors = new ImmutableList.Builder<String>();

            List<GenericValue> tasks = Select.from(ENTITY).runWith(entityEngine).asList();

            // We now need to sort this into build number sequence using BUILD_NUMBER_COMPARATOR, because build numbers are
            // not simply sequential.
            ArrayList<GenericValue> sortedTasks = new ArrayList<GenericValue>(tasks);
            Collections.sort(sortedTasks, new Comparator<GenericValue>()
            {
                @Override
                public int compare(final GenericValue task1, final GenericValue task2)
                {
                    String buildNo1 = task1.getString(FIELD_TARGETBUILD);
                    buildNo1 = buildNo1 == null ? "0" : buildNo1;
                    String buildNo2 = task2.getString(FIELD_TARGETBUILD);
                    buildNo2 = buildNo2 == null ? "0" : buildNo2;
                    return BUILD_NUMBER_COMPARATOR.compare(buildNo1, buildNo2);
                }
            });

            for (GenericValue taskGv : sortedTasks)
            {
                try
                {
                    final UpgradeTask task;
                    try
                    {
                        task = JiraUtils.loadComponent(taskGv.getString(FIELD_UPGRADECLASS), this.getClass());
                    }
                    catch (ClassNotFoundException e)
                    {
                        // Although there is a history record for this class, it may well have been removed from the JIRA source.
                        log.debug(String.format("Historical task %s has history but no longer exists.", taskGv.getString(FIELD_UPGRADECLASS)));
                        continue;
                    }

                    // If task is complete bump the build number.
                    if (STATUS_COMPLETE.equals(taskGv.getString(FIELD_STATUS)))
                    {
                        if (task.getBuildNumber() != null && BUILD_NUMBER_COMPARATOR.compare(task.getBuildNumber(), getJiraBuildNumber()) > 0)
                        {
                            setJiraBuildNumber(task.getBuildNumber());
                        }
                    }
                    else if (STATUS_PENDING.equals(taskGv.getString(FIELD_STATUS)))
                    {
                        if (task.getScheduleOption().equals(UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED))
                        {
                            // This is a bad thing as this should be all done before startup
                            log.error(String.format("Foreground upgrade task %s did not run before JIRA started", task.getBuildNumber()));
                            log.error("No more upgrades will be run until this is resolved.");
                            errors.add(String.format("Foreground upgrade task %s did not run before JIRA started", task.getBuildNumber()));
                            break;
                        }

                        try
                        {
                            log.info("Performing Upgrade Task: " + task.getShortDescription());
                            task.doUpgrade(false);
                        }
                        catch (Exception e)
                        {
                            log.error(String.format("Upgrade task %s failed", task.getBuildNumber()), e);
                            errors.add(String.format("Upgrade task %s failed. Error : %s", task.getBuildNumber(), e.getMessage()));
                            break;
                        }
                        taskGv.set(FIELD_STATUS, STATUS_COMPLETE);
                        taskGv.store();
                        log.info("Upgrade Task: '" + task.getShortDescription() + "' succeeded");
                        setJiraBuildNumber(task.getBuildNumber());
                    }
                }
                catch (GenericEntityException e)
                {
                    log.error(String.format("Failed to set status for upgrade task %s", taskGv.getString(FIELD_TARGETBUILD)), e);
                    errors.add(String.format("Failed to set status for upgrade task %s. Error %s", taskGv.getString(FIELD_TARGETBUILD), e.getMessage()));
                }
            }


            if (isReindexAllowed(jobRunnerRequest))
            {
                try
                {
                    reindex(errors);
                }
                catch (Exception e)
                {
                    log.error("Failed to reindex JIRA following upgrades.", e);
                    errors.add(String.format("Failed to find class for upgrade task %s", e.getMessage()));
                }
            }

            ImmutableList<String> allErrors = errors.build();

            if (allErrors.isEmpty())
            {
                // there may not be any patches for this version, so increment to latest build number.
                setJiraBuildNumber(buildUtilsInfo.getCurrentBuildNumber());
                return JobRunnerResponse.success();
            }
            else
            {
                return JobRunnerResponse.failed(allErrors.toString());
            }
        }
        finally
        {
            lock.unlock();
        }
    }

    private boolean isReindexAllowed(final JobRunnerRequest jobRunnerRequest)
    {
        Boolean isReindexAllowed = (Boolean) jobRunnerRequest.getJobConfig().getParameters().get(REINDEX_ALLOWED_PROPERTY);
        // If no parameter is found we assume reindex is allowed.
        return isReindexAllowed == null ? true : isReindexAllowed;
    }

    public void reindex(IssueIndexingParams indexesToReindex) throws Exception
    {
        indexManager.reIndexIssuesInBackground(Contexts.percentageLogger(indexManager, log), indexesToReindex);
    }


    /**
     * Get the current build number from the database.  This represents the level that this application is patched to.
     * This may be different to the current version if there are patches waiting to be applied.
     *
     * @return The version information from the database
     */
    private String getJiraBuildNumber()
    {
        //if null in the database, we need to set to '0' so that it can be compared with
        //other versions.
        if (applicationProperties.getString(APKeys.JIRA_PATCHED_VERSION) == null)
        {
            setJiraBuildNumber("0");
        }
        return applicationProperties.getString(APKeys.JIRA_PATCHED_VERSION);
    }

    private void setJiraBuildNumber(final String version)
    {
        log.info("Setting current build number to " + version);
        applicationProperties.setString(APKeys.JIRA_PATCHED_VERSION, version);
    }

    private static ClusterLockService getClusterLockService()
    {
        return ComponentAccessor.getComponent(ClusterLockService.class);
    }

    public static boolean isUpgradeRunning()
    {
        ClusterLock lock = getClusterLockService().getLockForName(DelayedUpgradeJobRunner.class.getName());
        if (lock.tryLock())
        {
            lock.unlock();
            return false;
        }
        return true;
    }

    /**
     * Performs reindex.
     *
     * @return true if successful and there were no errors, false if there was an error performing reindex.
     *
     * @throws Exception if an error occurs.
     * @param errors
     */
    private void reindex(final ImmutableList.Builder<String> errors) throws Exception
    {
        log.debug("Reindex all data if indexing is turned on.");
        Set<ReindexRequest> reindexRequests = reindexRequestManager.processPendingRequests(true, EnumSet.allOf(ReindexRequestType.class), true);

        //Check if any requests failed
        for (ReindexRequest reindexRequest : reindexRequests)
        {
            if (reindexRequest.getStatus() == ReindexStatus.FAILED)
            {
                log.error(String.format("Reindex of %s failed", reindexRequest.getAffectedIndexes()));
                errors.add(String.format("Reindex of %s failed", reindexRequest.getAffectedIndexes()));
                return;
            }
        }
    }

}
