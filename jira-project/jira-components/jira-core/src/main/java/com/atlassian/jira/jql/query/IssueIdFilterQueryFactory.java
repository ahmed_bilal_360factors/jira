package com.atlassian.jira.jql.query;

import java.io.IOException;
import java.util.Set;

import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.filters.IssueIdFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.ConstantScoreQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;

/**
 * Creates a lucene query that will restrict result to issues which are satisfy query executed on other index.
 *
 * @since v6.4
 */
public class IssueIdFilterQueryFactory
{
    private static final Logger log = LoggerFactory.getLogger(IssueIdFilterQueryFactory.class);
    private final SearchProviderFactory searchProviderFactory;

    public IssueIdFilterQueryFactory(final SearchProviderFactory searchProviderFactory)
    {
        this.searchProviderFactory = searchProviderFactory;
    }

    /**
     * Creates a lucene query that will restrict result to issues which are satisfy query executed on other index.
     *
     * @param indexQuery the lucene query that will be executed on passed index
     * @param indexName  the name of the index used in searching. The index name should be provided by {@link com.atlassian.jira.issue.search.SearchProviderFactory}
     * Documents on this index should have the field which represents issue id.
     *
     * @return the constant score lucene query which will filters by issue ids. Return empty BooleanQuery when exception was caught.
     */
    public Query createIssueIdFilterQuery(final Query indexQuery, final String indexName)
    {
        try
        {
            if (log.isDebugEnabled())
            {
                log.debug("Searching the " + indexName + " index using the query: " + indexQuery.toString());
            }

            // Now we need to execute the search and build up a new query that enumerates the issue id's found by the index search
            final IndexSearcher searcher = searchProviderFactory.getSearcher(indexName);

            final IssueIdCollector collector = new IssueIdCollector(searcher.getIndexReader());

            searcher.search(indexQuery, collector);
            final Set<String> issueIds = collector.getIssueIds();

            return new ConstantScoreQuery(new IssueIdFilter(issueIds));
        }
        catch (final IOException e)
        {
            log.error("Unable to search the " + indexName + " index.", e);
            return new BooleanQuery();
        }
    }

}
