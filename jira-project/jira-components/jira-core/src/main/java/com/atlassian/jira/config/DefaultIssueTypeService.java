package com.atlassian.jira.config;

import com.atlassian.fugue.Effect;
import com.atlassian.fugue.Option;
import com.atlassian.fugue.Options;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.types.issuetype.IssueTypeTypeAvatarService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.lang.Pair;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import javax.annotation.Nonnull;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.util.ErrorCollection.Reason.CONFLICT;
import static com.atlassian.jira.util.ErrorCollection.Reason.FORBIDDEN;
import static com.atlassian.jira.util.ErrorCollection.Reason.NOT_FOUND;
import static com.atlassian.jira.util.ErrorCollection.Reason.NOT_LOGGED_IN;
import static com.atlassian.jira.util.ErrorCollection.Reason.VALIDATION_FAILED;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.transform;
import static java.util.Collections.emptyList;

public class DefaultIssueTypeService implements IssueTypeService
{
    private final ConstantsManager constantsManager;
    private final IssueTypeTypeAvatarService avatarService;
    private final I18nHelper i18n;
    private final GlobalPermissionManager globalPermissionManager;
    private final IssueTypeManager issueTypeManager;
    private final IssueTypeSchemeManager schemeManager;
    private final PermissionManager permissionManager;
    private final IssueTypeSchemeManager issueTypeSchemeManager;
    private final SubTaskManager subTaskManager;
    private final ApplicationProperties applicationProperties;

    public DefaultIssueTypeService(final ConstantsManager constantsManager,
            final IssueTypeTypeAvatarService avatarService,
            final I18nHelper i18n,
            final GlobalPermissionManager globalPermissionManager,
            final IssueTypeManager issueTypeManager,
            final IssueTypeSchemeManager schemeManager,
            final PermissionManager permissionManager,
            final IssueTypeSchemeManager issueTypeSchemeManager,
            final SubTaskManager subTaskManager,
            final ApplicationProperties applicationProperties)
    {
        this.constantsManager = constantsManager;
        this.avatarService = avatarService;
        this.i18n = i18n;
        this.globalPermissionManager = globalPermissionManager;
        this.issueTypeManager = issueTypeManager;
        this.schemeManager = schemeManager;
        this.permissionManager = permissionManager;
        this.issueTypeSchemeManager = issueTypeSchemeManager;
        this.subTaskManager = subTaskManager;
        this.applicationProperties = applicationProperties;
    }

    @Override
    public Option<IssueType> getIssueType(final ApplicationUser applicationUser, final String id)
    {
        return option(find(getIssueTypes(applicationUser), new Predicate<IssueType>()
        {
            @Override
            public boolean apply(final IssueType issueType)
            {
                return issueType.getId().equals(id);
            }
        }, null));
    }

    @Override
    public Iterable<IssueType> getIssueTypes(final ApplicationUser applicationUser)
    {
        final Collection<Project> projects = permissionManager.getProjects(ProjectPermissions.BROWSE_PROJECTS, applicationUser);
        final Iterable<Iterable<IssueType>> visibleIssueTypes = transform(projects, new Function<Project, Iterable<IssueType>>()
        {
            @Override
            public Iterable<IssueType> apply(final Project project)
            {
                return issueTypeSchemeManager.getIssueTypesForProject(project);
            }
        });
        final ImmutableSet.Builder<IssueType> allIssueTypesBuilder = ImmutableSet.<IssueType>builder()
                .addAll(concat(visibleIssueTypes));

        if (globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, applicationUser))
        {
            allIssueTypesBuilder.addAll(issueTypeSchemeManager.getIssueTypesForDefaultScheme());
        }
        return allIssueTypesBuilder.build();
    }

    @Override
    public Iterable<IssueType> getAvailableAlternativesForIssueType(final ApplicationUser applicationUser, final String id)
    {
        return getIssueType(applicationUser, id).fold(new Supplier<Iterable<IssueType>>()
        {
            @Override
            public Iterable<IssueType> get()
            {
                return emptyList();
            }
        }, new Function<IssueType, Iterable<IssueType>>()
        {
            @Override
            public Iterable<IssueType> apply(final IssueType issueType)
            {
                return issueTypeManager.getAvailableIssueTypes(issueType);
            }
        });
    }

    @Override
    public CreateValidationResult validateCreateIssueType(final ApplicationUser user, @Nonnull final IssueTypeCreateInput issueTypeCreateInput)
    {
        checkNotNull(issueTypeCreateInput);

        final Option<ErrorCollection> userValidation = validateUser(user);
        if (userValidation.isDefined())
        {
            return CreateValidationResult.error(userValidation.get());
        }

        final ErrorCollection errorCollection = validateCreate(issueTypeCreateInput);
        if (errorCollection.hasAnyErrors())
        {
            return CreateValidationResult.error(errorCollection);
        }
        else
        {
            return CreateValidationResult.ok(issueTypeCreateInput);
        }
    }

    @Override
    public IssueTypeResult createIssueType(final ApplicationUser user, @Nonnull final CreateValidationResult validationResult)
    {
        checkArgument(validationResult.isValid());
        checkArgument(validationResult.getIssueTypeInput().isDefined());

        final IssueTypeCreateInput issueTypeCreateInput = validationResult.getIssueTypeInput().get();
        final String name = issueTypeCreateInput.getName();
        final IssueTypeCreateInput.Type type = issueTypeCreateInput.getType();
        final String description = issueTypeCreateInput.getDescription().getOrElse(StringUtils.EMPTY);

        final IssueType issueType = createIssueTypeOrSubTaskType(name, type, description);

        return new IssueTypeResult(issueType);
    }

    @Override
    public UpdateValidationResult validateUpdateIssueType(final ApplicationUser user,
            @Nonnull final String issueTypeId,
            @Nonnull final IssueTypeUpdateInput issueTypeUpdateInput)
    {
        checkNotNull(issueTypeId);
        checkNotNull(issueTypeUpdateInput);

        final Option<ErrorCollection> userValidation = validateUser(user);
        if (userValidation.isDefined())
        {
            return UpdateValidationResult.error(userValidation.get());
        }

        return getIssueType(user, issueTypeId).fold(new Supplier<UpdateValidationResult>()
        {
            @Override
            public UpdateValidationResult get()
            {
                return UpdateValidationResult.error(new SimpleErrorCollection(i18n.getText("admin.error.issue.type.update.not.exist"), NOT_FOUND));
            }
        }, new Function<IssueType, UpdateValidationResult>()
        {
            @Override
            public UpdateValidationResult apply(final IssueType issueType)
            {
                final ErrorCollection errorCollection = validateUpdate(user, issueTypeUpdateInput, issueType);
                if (errorCollection.hasAnyErrors())
                {
                    return UpdateValidationResult.error(errorCollection);
                }
                else
                {
                    return UpdateValidationResult.ok(issueTypeUpdateInput, issueType);
                }
            }
        });
    }

    @Override
    public IssueTypeResult updateIssueType(final ApplicationUser user, @Nonnull final UpdateValidationResult validationResult)
    {
        checkArgument(validationResult.getIssueTypeInput().isDefined());
        checkArgument(validationResult.getIssueType().isDefined());

        final IssueType issueTypeToUpdate = validationResult.getIssueType().get();
        final IssueTypeUpdateInput issueTypeUpdateInput = validationResult.getIssueTypeInput().get();

        final String name = issueTypeUpdateInput.getName().getOrElse(issueTypeToUpdate.getName());
        final String description = issueTypeUpdateInput.getDescription().getOrElse(issueTypeToUpdate.getDescription());

        final Long avatarId = issueTypeUpdateInput.getAvatarId().getOrElse(new Supplier<Long>()
        {
            @Override
            public Long get()
            {
                return issueTypeToUpdate.getAvatar() != null ?
                        issueTypeToUpdate.getAvatar().getId() :
                        null;
            }
        });

        issueTypeManager.updateIssueType(issueTypeToUpdate, name, description, avatarId);

        return new IssueTypeResult(issueTypeManager.getIssueType(issueTypeToUpdate.getId()));
    }

    @Override
    public DeleteValidationResult validateDeleteIssueType(final ApplicationUser user,
            @Nonnull final IssueTypeDeleteInput issueTypeDeleteInput)
    {
        checkNotNull(issueTypeDeleteInput);
        checkNotNull(issueTypeDeleteInput.getIssueTypeToDeleteId());

        final Option<ErrorCollection> userValidationResult = validateUser(user);
        if (userValidationResult.isDefined())
        {
            return deleteValidationFailed(userValidationResult.get());
        }

        Option<IssueType> issueTypeToDelete = getIssueType(user, issueTypeDeleteInput.getIssueTypeToDeleteId());
        if (issueTypeToDelete.isEmpty())
        {
            return deleteValidationFailed(i18n.getText("admin.error.issue.type.delete.not.exist"), NOT_FOUND);
        }

        if (!alternativeProvided(issueTypeDeleteInput, issueTypeToDelete) || !alternativeExists(user, issueTypeDeleteInput))
        {
            return deleteValidationFailed(i18n.getText("admin.error.issue.type.delete.alternative.not.exist"), NOT_FOUND);
        }

        if (issueTypeManager.hasAssociatedIssues(issueTypeToDelete.get()))
        {
            final Iterable<IssueType> alternativesForIssueType = getAvailableAlternativesForIssueType(user, issueTypeDeleteInput.getIssueTypeToDeleteId());
            if (isEmpty(alternativesForIssueType))
            {
                return deleteValidationFailed(i18n.getText("admin.error.issue.type.delete.no.alternative.available"), NOT_FOUND);
            }
            else if (issueTypeDeleteInput.getAlternativeIssueTypeId().isDefined()
                    && !containsAlternative(alternativesForIssueType, issueTypeDeleteInput.getAlternativeIssueTypeId().get()))
            {
                return deleteValidationFailed(i18n.getText("admin.error.issue.type.delete.invalid.alternative"), CONFLICT);
            }
        }

        return new DeleteValidationResult(new SimpleErrorCollection(), some(issueTypeDeleteInput));
    }

    private boolean containsAlternative(final Iterable<IssueType> alternativesForIssueType, final String alternativeIssueTypeId)
    {
        return Iterables.any(alternativesForIssueType, new Predicate<IssueType>()
        {
            @Override
            public boolean apply(final IssueType issueType)
            {
                return issueType.getId().equals(alternativeIssueTypeId);
            }
        });
    }

    @Override
    public void deleteIssueType(final ApplicationUser user, @Nonnull final DeleteValidationResult validationResult)
    {
        checkNotNull(validationResult);
        checkArgument(validationResult.isValid());

        checkNotNull(validationResult.getDeleteIssueTypeInput());
        checkArgument(validationResult.getDeleteIssueTypeInput().isDefined());

        final String issueTypeToDeleteId = validationResult.getDeleteIssueTypeInput().get().getIssueTypeToDeleteId();
        issueTypeManager.removeIssueType(issueTypeToDeleteId,
                validationResult.getDeleteIssueTypeInput().get().getAlternativeIssueTypeId().getOrNull());
    }

    private ErrorCollection validateCreate(@Nonnull final IssueTypeCreateInput issueTypeCreateInput)
    {
        // noinspection unchecked
        final Iterable<ErrorCollection> errorCollections = Options.flatten(Lists.newArrayList(
                validateType(issueTypeCreateInput),
                validateName(issueTypeCreateInput.getName(), Option.<IssueType>none())
        ));

        return buildAllErrors(errorCollections);
    }

    private ErrorCollection validateUpdate(final ApplicationUser user,
            @Nonnull final IssueTypeUpdateInput issueTypeUpdateInput,
            @Nonnull final IssueType issueTypeToUpdate)
    {
        checkNotNull(issueTypeUpdateInput);
        checkNotNull(issueTypeToUpdate);

        final ImmutableList.Builder<ErrorCollection> builder = ImmutableList.builder();
        final Effect<ErrorCollection> errorCollectionEffect = new Effect<ErrorCollection>()
        {
            @Override
            public void apply(final ErrorCollection errorCollection)
            {
                builder.add(errorCollection);
            }
        };

        if (issueTypeUpdateInput.getAvatarId().isDefined())
        {
            validateAvatar(user, issueTypeUpdateInput).foreach(errorCollectionEffect);
        }
        if (issueTypeUpdateInput.getName().isDefined())
        {
            validateName(issueTypeUpdateInput.getName().get(), some(issueTypeToUpdate)).foreach(errorCollectionEffect);
        }

        return buildAllErrors(builder.build());
    }

    private IssueType createIssueTypeOrSubTaskType(final String name, final IssueTypeCreateInput.Type type, final String description)
    {
        if (type.equals(IssueTypeCreateInput.Type.STANDARD))
        {
            return issueTypeManager.createIssueType(name, description, Long.parseLong(applicationProperties.getString(APKeys.JIRA_DEFAULT_ISSUETYPE_AVATAR_ID)));
        }
        else
        {
            return issueTypeManager.createSubTaskIssueType(name, description, Long.parseLong(applicationProperties.getString(APKeys.JIRA_DEFAULT_ISSUETYPE_SUBTASK_AVATAR_ID)));
        }
    }

    private ErrorCollection buildAllErrors(final Iterable<ErrorCollection> errorCollections)
    {
        final SimpleErrorCollection allErrors = new SimpleErrorCollection();
        for (ErrorCollection errorCollection : errorCollections)
        {
            allErrors.addErrorMessages(errorCollection.getErrorMessages());
            allErrors.addErrors(errorCollection.getErrors());
            allErrors.addReasons(errorCollection.getReasons());
        }
        return allErrors;
    }

    private Option<ErrorCollection> validateType(final IssueTypeCreateInput issueTypeCreateInput)
    {
        if (!subTaskManager.isSubTasksEnabled() && IssueTypeCreateInput.Type.SUBTASK.equals(issueTypeCreateInput.getType()))
        {
            return errorCollection(i18n.getText("admin.errors.subtasks.disabled"), ErrorCollection.Reason.VALIDATION_FAILED);
        }
        return none();
    }

    private Option<ErrorCollection> validateName(final String name, final Option<IssueType> issueTypeToUpdate)
    {
        final Option<Pair<String, ErrorCollection.Reason>> nameValidation = constantsManager.validateName(name, issueTypeToUpdate);
        if (nameValidation.isDefined())
        {
            return errorCollection("name", nameValidation.get().first(), nameValidation.get().second());
        }
        return none();
    }

    private Option<ErrorCollection> validateUser(final ApplicationUser user)
    {
        if (user == null)
        {
            return errorCollection(i18n.getText("rest.authentication.no.user.logged.in"), NOT_LOGGED_IN);
        }
        if (!globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user))
        {
            return errorCollection(i18n.getText("rest.authorization.admin.required"), FORBIDDEN);
        }
        return none();
    }

    private Option<ErrorCollection> validateAvatar(final ApplicationUser user, final IssueTypeUpdateInput issueTypeCreateInput)
    {
        return issueTypeCreateInput.getAvatarId().flatMap(new Function<Long, Option<ErrorCollection>>()
        {
            @Override
            public Option<ErrorCollection> apply(final Long avatarId)
            {
                final Avatar avatar = avatarService.getAvatar(user, avatarId);
                if (avatar == null)
                {
                    return errorCollection("avatarId", i18n.getText("admin.errors.project.no.avatar.with.id"), VALIDATION_FAILED);
                }
                return none();
            }
        });
    }

    private boolean alternativeExists(final ApplicationUser user, final IssueTypeDeleteInput issueTypeDeleteInput)
    {
        return (issueTypeDeleteInput.getAlternativeIssueTypeId().isDefined()
                && getIssueType(user, issueTypeDeleteInput.getAlternativeIssueTypeId().get()).isDefined())
                || issueTypeDeleteInput.getAlternativeIssueTypeId().isEmpty();
    }

    private boolean alternativeProvided(final IssueTypeDeleteInput issueTypeDeleteInput, final Option<IssueType> issueTypeToDelete)
    {
        boolean hasAssociatedIssues = issueTypeManager.hasAssociatedIssues(issueTypeToDelete.get());
        return (hasAssociatedIssues && issueTypeDeleteInput.getAlternativeIssueTypeId().isDefined())
                || !hasAssociatedIssues;
    }

    private Option<ErrorCollection> errorCollection(final String field, final String text, final ErrorCollection.Reason reason)
    {
        final ErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addError(field, text, reason);
        return some(errorCollection);
    }

    private Option<ErrorCollection> errorCollection(final String text, final ErrorCollection.Reason reason)
    {
        return Option.<ErrorCollection>some(new SimpleErrorCollection(text, reason));
    }

    private DeleteValidationResult deleteValidationFailed(final ErrorCollection errorCollection)
    {
        return new DeleteValidationResult(errorCollection, Option.<IssueTypeDeleteInput>none());
    }

    private DeleteValidationResult deleteValidationFailed(final String text, final ErrorCollection.Reason reason)
    {
        return deleteValidationFailed(new SimpleErrorCollection(text, reason));
    }

}
