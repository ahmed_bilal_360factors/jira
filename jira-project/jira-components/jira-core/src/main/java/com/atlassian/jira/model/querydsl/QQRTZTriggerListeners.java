package com.atlassian.jira.model.querydsl;

import com.mysema.query.sql.ColumnMetadata;
import com.mysema.query.types.path.*;

import java.sql.Types;
import javax.annotation.Generated;

/**
 * QQRTZTriggerListeners is a Querydsl query object.
 *
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QQRTZTriggerListeners extends JiraRelationalPathBase<QRTZTriggerListenersDTO>
{
    public static final QQRTZTriggerListeners Q_R_T_Z_TRIGGER_LISTENERS = new QQRTZTriggerListeners("Q_R_T_Z_TRIGGER_LISTENERS");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final NumberPath<Long> trigger = createNumber("trigger", Long.class);
    public final StringPath triggerListener = createString("triggerListener");

    public QQRTZTriggerListeners(String alias)
    {
        super(QRTZTriggerListenersDTO.class, alias, "qrtz_trigger_listeners");
        addMetadata();
    }

    private void addMetadata()
    {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(trigger, ColumnMetadata.named("trigger_id").withIndex(2).ofType(Types.NUMERIC).withSize(18));
        addMetadata(triggerListener, ColumnMetadata.named("trigger_listener").withIndex(3).ofType(Types.VARCHAR).withSize(255));
    }
}

