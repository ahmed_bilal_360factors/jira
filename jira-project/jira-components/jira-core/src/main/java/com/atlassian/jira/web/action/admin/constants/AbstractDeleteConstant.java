package com.atlassian.jira.web.action.admin.constants;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.ofbiz.core.entity.GenericEntityException;

public abstract class AbstractDeleteConstant<T extends IssueConstant> extends AbstractConstantAction<T>
{
    protected String id;
    protected String newId;
    protected boolean confirm;
    private T constant;
    private List matchingIssues;

    protected void doValidation()
    {
        if (getConstant() == null)
        {
            addErrorMessage(getText("admin.errors.no.constant.found", getNiceConstantName(), id));
        }

        if (newId == null || getConstant(newId) == null)
        {
            addError("newId", getText("admin.errors.specify.valid.constant.name", getNiceConstantName()));
        }
    }

    @RequiresXsrfCheck
    protected abstract String doExecute() throws Exception;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNewId()
    {
        return newId;
    }

    public void setNewId(String newId)
    {
        this.newId = newId;
    }

    public T getConstant()
    {
        if (constant == null)
        {
            constant = getConstant(id);
        }
        return constant;
    }

    public List getMatchingIssues() throws GenericEntityException
    {

        if (matchingIssues == null)
        {
            matchingIssues = getConstant().getGenericValue().getRelated("ChildIssue");

            if (matchingIssues == null)
            {
                matchingIssues = Collections.EMPTY_LIST;
            }
        }
        return matchingIssues;
    }

    public Collection<T> getNewConstants()
    {
        return Lists.newArrayList(Iterables.filter(getConstants(), new Predicate<T>()
        {
            @Override
            public boolean apply(final T constant)
            {
                return !id.equals(constant.getId());
            }
        }));
    }

    public boolean isConfirm()
    {
        return confirm;
    }

    public void setConfirm(boolean confirm)
    {
        this.confirm = confirm;
    }
}
