package com.atlassian.jira.util;

import java.util.List;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Eithers;
import com.atlassian.fugue.Pair;

import com.google.common.collect.ImmutableList;

/**
 * Utility functions for Eithers.
 *
 * @since v6.4
 */
public class EitherUtils
{
    /**
     * Transforms list of Eithers into two lists, which contains left and right values.
     */
    public static <L, R> Pair<List<L>, List<R>> splitEithers(final Iterable<Either<L, R>> eithers)
    {
        return Pair.<List<L>, List<R>>pair(
                ImmutableList.copyOf(Eithers.filterLeft(eithers)),
                ImmutableList.copyOf(Eithers.filterRight(eithers))
        );
    }
}
