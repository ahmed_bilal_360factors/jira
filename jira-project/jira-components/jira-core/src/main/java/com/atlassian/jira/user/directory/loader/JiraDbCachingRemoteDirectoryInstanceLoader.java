package com.atlassian.jira.user.directory.loader;

import com.atlassian.crowd.cql.parser.CqlQueryParser;
import com.atlassian.crowd.directory.ldap.cache.DirectoryCacheFactory;
import com.atlassian.crowd.directory.loader.DbCachingRemoteDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.DelegatingDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.DelegatingDirectoryInstanceLoaderImpl;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.InternalDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.LDAPDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.RemoteCrowdDirectoryInstanceLoader;
import com.atlassian.crowd.manager.directory.monitor.DirectoryMonitorManager;

import com.google.common.collect.ImmutableList;

/**
 * A Pico-friendly wrapper around the Crowd's {@link DbCachingRemoteDirectoryInstanceLoader}.
 * <p>
 * This forces Pico to ignore the other Constructor in DbCachingRemoteDirectoryInstanceLoader.
 * We also deal with "dbCachingDelegatingDirectoryInstanceLoader" internally, as it is yet
 * another "DirectoryInstanceLoader" requiring yet another marker interface and yet another
 * Pico-friendly constructor if we let Pico deal with it.  This is much simpler.
 *
 * @since v4.3
 */
public class JiraDbCachingRemoteDirectoryInstanceLoader extends DbCachingRemoteDirectoryInstanceLoader
{
    public JiraDbCachingRemoteDirectoryInstanceLoader(
            final LDAPDirectoryInstanceLoader ldapDirectoryInstanceLoader,
            final RemoteCrowdDirectoryInstanceLoader remoteCrowdDirectoryInstanceLoader,
            final InternalDirectoryInstanceLoader internalDirectoryInstanceLoader,
            final DirectoryMonitorManager directoryMonitorManager,
            final DirectoryCacheFactory directoryCacheFactory,
            final CqlQueryParser cqlQueryParser)
    {
        super(dbCachingDelegatingDirectoryInstanceLoader(ldapDirectoryInstanceLoader, remoteCrowdDirectoryInstanceLoader),
                internalDirectoryInstanceLoader, directoryMonitorManager, directoryCacheFactory, cqlQueryParser);
    }

    static DelegatingDirectoryInstanceLoader dbCachingDelegatingDirectoryInstanceLoader(DirectoryInstanceLoader... loaders)
    {
        return new DelegatingDirectoryInstanceLoaderImpl(ImmutableList.copyOf(loaders));
    }
}
