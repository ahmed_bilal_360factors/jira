package com.atlassian.jira.license;

import javax.annotation.concurrent.Immutable;

/**
 * Fired when a {@link com.atlassian.jira.license.LicenseRole} is defined.
 *
 * @since v6.4
 */
@Immutable
public class LicenseRoleDefinedEvent extends LicenseRoleEvent
{
    public LicenseRoleDefinedEvent(final LicenseRoleId id)
    {
        super(id);
    }

    @Override
    public String toString()
    {
        return String.format("LicenseRole(%s) Defined", getLicenseRoleId());
    }
}
