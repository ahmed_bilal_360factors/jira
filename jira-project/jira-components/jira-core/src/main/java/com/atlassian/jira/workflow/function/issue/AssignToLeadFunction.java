package com.atlassian.jira.workflow.function.issue;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.user.ApplicationUser;
import com.opensymphony.module.propertyset.PropertySet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class AssignToLeadFunction extends AbstractJiraFunctionProvider
{
    private static final Logger log = LoggerFactory.getLogger(AssignToLeadFunction.class);

    public void execute(Map transientVars, Map args, PropertySet ps)
    {
        MutableIssue issue = getIssue(transientVars);
        String leadKey = null;
        ApplicationUser lead;
        if (issue.getComponentObjects() != null)
        {
            leadKey = findAComponentLead(issue);
        }
        if (leadKey == null)
        {
            // Use Project lead
            lead = issue.getProjectObject().getProjectLead();
            leadKey = lead == null ? null : lead.getKey();
        }
        else
        {
            // Find User object for component lead
            lead = getLead(leadKey);

            if (lead == null)
            {
                log.warn("Component lead '" + leadKey + "' in project " +
                        issue.getProjectObject().getName() + " does not exist");
                return;
            }
        }
        if (leadKey == null)
        {
            return;
        }

        log.debug("Automatically setting assignee to lead developer " + leadKey);
        issue.setAssignee(lead.getDirectoryUser());

        // JRA-14269: issue.store() should never have been called in this function, as it can cause the Issue object
        // to be persisted to the database prematurely. However, since it has been here for a while, removing it could
        // break existing functionality for lots of users. But, because an NPE is only thrown when this function is used
        // in the Create step, all we have to do to prevent this error from occuring is check if the issue has already
        // been stored before. If it has, we can call store() to update the issue, which maintains working (albeit
        // incorrect) behaviour. If it hasn't, we defer the store() call, as it should have been implemented initially.
        if (issue.isCreated())
        {
            issue.store();
        }
    }

    private String findAComponentLead(final Issue issue)
    {
        // Loop over the components until we find a non-null component lead
        for (ProjectComponent component : issue.getComponentObjects())
        {
            String leadKey = component.getLead();
            if (leadKey != null)
            {
                return leadKey;
            }
        }
        return null;
    }

    ApplicationUser getLead(String userKey)
    {
        return ComponentAccessor.getUserManager().getUserByKey(userKey);
    }
}
