package com.atlassian.jira.database;

import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Nonnull;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.ofbiz.DefaultOfBizConnectionFactory;
import com.atlassian.jira.ofbiz.OfBizConnectionFactory;

import com.mysema.query.sql.HSQLDBTemplates;
import com.mysema.query.sql.MySQLTemplates;
import com.mysema.query.sql.OracleTemplates;
import com.mysema.query.sql.PostgresTemplates;
import com.mysema.query.sql.SQLServerTemplates;
import com.mysema.query.sql.SQLTemplates;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DbConnectionManagerImpl implements DbConnectionManager
{
    private static final Logger log = LoggerFactory.getLogger(DbConnectionManagerImpl.class);

    private final OfBizConnectionFactory ofBizConnectionFactory;
    private final SQLTemplates dialect;

    public DbConnectionManagerImpl(DatabaseConfigurationManager databaseConfigurationManager)
    {
        this.ofBizConnectionFactory = DefaultOfBizConnectionFactory.getInstance();

        final DatabaseConfig databaseConfig = databaseConfigurationManager.getDatabaseConfiguration();
        // Find the Querydsl template for the configured database vendor
        final SQLTemplates.Builder dialectBuilder = findDialectBuilder(databaseConfig);

        if (!StringUtils.isEmpty(databaseConfig.getSchemaName()))
        {
            // tell Querydsl to include the schema name in SQL it generates
            dialectBuilder.printSchema();
        }
        this.dialect = dialectBuilder.build();
    }

    private static SQLTemplates.Builder findDialectBuilder(final DatabaseConfig databaseConfig)
    {
        if (databaseConfig.isMySql())
        {
            return MySQLTemplates.builder();
        }
        if (databaseConfig.isPostgres())
        {
            return PostgresTemplates.builder();
        }
        if (databaseConfig.isOracle())
        {
            return OracleTemplates.builder();
        }
        if (databaseConfig.isSqlServer())
        {
            return SQLServerTemplates.builder();
        }
        if (databaseConfig.isHSql())
        {
            return HSQLDBTemplates.builder();
        }
        throw new IllegalStateException("Unrecognised database dialect '" + databaseConfig.getDatabaseType() + "'.");
    }

    @Override
    public <T> T executeQuery(@Nonnull final QueryCallback<T> callback)
    {
        final Connection con = borrowConnection();
        try
        {
            return callback.runQuery(new DbConnnectionImpl(con, getDialect()));
        }
        finally
        {
            try
            {
                con.close();
            }
            catch (SQLException ex)
            {
                log.error("Unable to close database connection.", ex);
            }
        }
    }

    private Connection borrowConnection()
    {
        try
        {
            return ofBizConnectionFactory.getConnection();
        }
        catch (SQLException ex)
        {
            throw new DataAccessException(ex);
        }
    }

    @Override
    @Nonnull
    public SQLTemplates getDialect()
    {
        return dialect;
    }

    @Override
    public void execute(@Nonnull final SqlCallback callback)
    {
        final Connection con = borrowConnection();
        try
        {
            callback.run(new DbConnnectionImpl(con, getDialect()));
        }
        catch (RuntimeException ex)
        {
            try
            {
                con.rollback();
            }
            catch (SQLException sqlEx)
            {
                log.error("Unable to rollback SQL connection.", sqlEx);
            }
            // Rethrow the RuntimeException
            throw ex;
        }
        finally
        {
            try
            {
                con.close();
            }
            catch (SQLException ex)
            {
                // We don't want to hide any exceptions that may have already been raised during the actual work.
                log.error("Unable to close SQL connection.", ex);
            }
        }
    }
}
