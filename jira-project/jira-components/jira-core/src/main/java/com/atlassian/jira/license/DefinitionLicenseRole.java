package com.atlassian.jira.license;

import com.atlassian.fugue.Option;

import javax.annotation.Nonnull;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * A {@link com.atlassian.jira.license.LicenseRole} uses the data
 * in a {@link com.atlassian.jira.license.LicenseRoleDefinition} to implement {@link #getId()} and {@link #getName()}.
 *
 * @since 6.4
 */
final class DefinitionLicenseRole extends AbstractLicenseRole
{
    private final LicenseRoleDefinition def;

    DefinitionLicenseRole(final LicenseRoleDefinition def, final Iterable<String> groups, final Option<String> primaryGroup)
    {
        super(groups, primaryGroup);
        this.def = notNull("def", def);
    }

    @Nonnull
    @Override
    public LicenseRoleId getId()
    {
        return def.getLicenseRoleId();
    }

    @Nonnull
    @Override
    public String getName()
    {
        return def.getName();
    }

    @Nonnull
    @Override
    public LicenseRole withGroups(@Nonnull final Iterable<String> groups, @Nonnull final Option<String> primaryGroup)
    {
        return new DefinitionLicenseRole(def, groups, primaryGroup);
    }
}
