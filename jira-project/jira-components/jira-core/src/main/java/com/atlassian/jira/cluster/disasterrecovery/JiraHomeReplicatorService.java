package com.atlassian.jira.cluster.disasterrecovery;

import java.io.File;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.configurable.ObjectConfiguration;
import com.atlassian.configurable.ObjectConfigurationException;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.AttachmentPathManager;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.config.util.SecondaryJiraHome;
import com.atlassian.jira.event.ComponentManagerShutdownEvent;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.plugin.PluginPath;
import com.atlassian.jira.service.AbstractService;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.util.concurrent.ThreadFactories;

import com.google.common.annotations.VisibleForTesting;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent.Action;
import static com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent.FileType;

/**
 * Secondary JIRA Home that tracks changes to attachment, index, avatar and plugin files.
 *
 * @since v6.4
 */
@EventComponent
public class JiraHomeReplicatorService extends AbstractService implements Startable
{
    private static final Logger log = LoggerFactory.getLogger(JiraHomeReplicatorService.class);
    private static final String NAME = JiraHomeReplicatorService.class.getName();

    private static final JobRunnerKey DELAYED_REPLICATION_KEY = JobRunnerKey.of(NAME + ".delayedReplicationKey");
    private static final JobId DELAYED_REPLICATION_ID = JobId.of(NAME + ".delayedReplicationId");

    private static final JobRunnerKey FULL_REPLICATION_KEY = JobRunnerKey.of(NAME + ".fullReplicationKey");
    private static final JobId FULL_REPLICATION_ID = JobId.of(NAME + ".fullReplicationId");

    private static final String SCHEDULED_REPLICATION_KEY = "homereplicator";

    private static final String FULL_REPLICATION_LOCK = NAME + ".fullReplicationLock";
    private static final int REPLICATOR_THREADS = 4;

    private final JiraHome jiraHome;
    private final SecondaryJiraHome secondaryJiraHome;
    private final ApplicationProperties applicationProperties;

    private final ExecutorService executorService;
    private final AttachmentPathManager attachmentPathManager;
    private final AvatarManager avatarManager;
    private final PluginPath pluginPath;
    private final SchedulerService schedulerService;

    // Guards against multiple full replications running at once
    private final ClusterLock fullReplicationLock;

    // Delayed replication types are queued and will be processed when the queue has been idle for a period of time
    private static final EnumSet<FileType> DELAYED_TYPES = EnumSet.of(FileType.PLUGIN);
    private static final int IDLE_SECONDS = 60;
    private final BlockingQueue<JiraHomeChangeEvent> delayedFileReplicationQueue;

    private static final File[] EMPTY_DIR = {};

    public JiraHomeReplicatorService(final JiraHome jiraHome, final SecondaryJiraHome secondaryHome,
            final ApplicationProperties applicationProperties, final AttachmentPathManager attachmentPathManager,
            final AvatarManager avatarManager, final PluginPath pluginPath, final ClusterLockService clusterLockService,
            final SchedulerService schedulerService)
    {
        this(jiraHome, secondaryHome, applicationProperties, attachmentPathManager, avatarManager, pluginPath,
                clusterLockService, schedulerService,
                Executors.newFixedThreadPool(REPLICATOR_THREADS, ThreadFactories.namedThreadFactory("JiraHomeReplicatorService")));
    }

    @VisibleForTesting
    protected JiraHomeReplicatorService(final JiraHome jiraHome, final SecondaryJiraHome secondaryHome,
            final ApplicationProperties applicationProperties, final AttachmentPathManager attachmentPathManager,
            final AvatarManager avatarManager, final PluginPath pluginPath, final ClusterLockService clusterLockService,
            final SchedulerService schedulerService, final ExecutorService executorService)
    {
        this.jiraHome = jiraHome;
        this.secondaryJiraHome = secondaryHome;
        this.applicationProperties = applicationProperties;
        this.attachmentPathManager = attachmentPathManager;
        this.avatarManager = avatarManager;
        this.pluginPath = pluginPath;
        this.schedulerService = schedulerService;
        this.fullReplicationLock = clusterLockService.getLockForName(FULL_REPLICATION_LOCK);
        this.executorService = executorService;
        this.delayedFileReplicationQueue = new LinkedBlockingQueue<JiraHomeChangeEvent>();
    }

    @Override
    public void start() throws Exception
    {
        schedulerService.registerJobRunner(DELAYED_REPLICATION_KEY, new QueueDrainingJob());
        schedulerService.registerJobRunner(FULL_REPLICATION_KEY, new FullReplicationJob());
    }

    /**
     * For every file changed event we are going to perform
     * an async task to add, move or delete the file
     */
    @EventListener
    public void onFileChangeEvent(final JiraHomeChangeEvent event)
    {
        if (!secondaryJiraHome.isEnabled())
        {
            return;
        }

        final FileType fileType = event.getFileType();
        if (!isEnabled(fileType))
        {
            return;
        }

        if (DELAYED_TYPES.contains(fileType))
        {
            queueDelayedEvent(event);
        }
        else
        {
            submitEvent(event);
        }
    }


    private void queueDelayedEvent(final JiraHomeChangeEvent event)
    {
        delayedFileReplicationQueue.add(event);
        JobConfig jobConfig = JobConfig.forJobRunnerKey(DELAYED_REPLICATION_KEY)
                .withRunMode(RunMode.RUN_LOCALLY)
                .withSchedule(Schedule.runOnce(DateTime.now().plusSeconds(IDLE_SECONDS).toDate()));
        try
        {
            schedulerService.scheduleJob(DELAYED_REPLICATION_ID, jobConfig);
        }
        catch (SchedulerServiceException e)
        {
            log.error("Failed to schedule delayed replication", e);
        }
    }

    private void submitEvent(final JiraHomeChangeEvent event)
    {
        if (event.getAction() == Action.FILE_ADD)
        {
            for (File file : event.getFiles())
            {
                submitCopyTask(file);
            }
        }
        else
        {
            for (File file : event.getFiles())
            {
                submitDeleteTask(file);
            }
        }
    }

    private Future<?> submitDeleteTask(final File file)
    {
        log.debug("submitDeleteTask: file={}", file);
        return executorService.submit(new DeleteTask(file, jiraHome, secondaryJiraHome));
    }

    private Future<?> submitCopyTask(final File file)
    {
        log.debug("submitCopyTask: file={}", file);
        return executorService.submit(new CopyTask(file, jiraHome, secondaryJiraHome, executorService));
    }

    @EventListener
    public void shutdown(ComponentManagerShutdownEvent shutdownEvent)
    {
        shutdown();
    }

    private void shutdown()
    {
        log.debug("Shutting down");
        do
        {
            executorService.shutdown(); // Disable new tasks from being submitted
            try
            {
                // Wait a while for existing tasks to terminate
                if (!executorService.awaitTermination(5, TimeUnit.SECONDS))
                {
                    log.debug("Replication service has not shut down; cancelling replications in progress.");
                    executorService.shutdownNow(); // Cancel currently executing tasks

                    // Wait a while for tasks to respond to being cancelled
                    if (!executorService.awaitTermination(5, TimeUnit.SECONDS))
                    {
                        throw new RuntimeException("Replicate executor did not terminate");
                    }
                }
            }
            catch (InterruptedException ie)
            {
                // (Re-)Cancel if current thread also interrupted
                executorService.shutdownNow();
                // Preserve interrupt status
                Thread.currentThread().interrupt();
            }
        }
        while (!executorService.isTerminated());
    }

    /**
     * Checks if the following file type is enabled
     *
     * @param fileType the file type
     * @return true if the service is enabled, false if not
     */
    private boolean isEnabled(final FileType fileType)
    {
        return applicationProperties.getOption(fileType.getKey());
    }

    /**
     * Returns true if the replicator service is doing a full replication. That is scanning the enabled directories
     * for differences between JIRA home and the secondary store.
     *
     * @return true if a replication is currently in progress
     */
    public boolean isReplicating()
    {
        if (fullReplicationLock.tryLock())
        {
            fullReplicationLock.unlock();
            return false;
        }

        return true;
    }

    /**
     * Request a full replication of files in the home directories that are configured for replication.
     */
    public void replicateJiraHome() throws SchedulerServiceException
    {
        JobConfig jobConfig = JobConfig.forJobRunnerKey(FULL_REPLICATION_KEY)
                .withRunMode(RunMode.RUN_ONCE_PER_CLUSTER)
                .withSchedule(Schedule.runOnce(null));
        schedulerService.scheduleJob(FULL_REPLICATION_ID, jobConfig);
    }

    @Override
    public void run()
    {
        performReplication();
    }

    @Override
    public ObjectConfiguration getObjectConfiguration() throws ObjectConfigurationException
    {
        return getObjectConfiguration(SCHEDULED_REPLICATION_KEY, "services/com/atlassian/jira/service/services/homereplicator.xml", null);
    }

    public static class ReplicationResult
    {
        private int sourceFileCount;
        private int destinationFileCount;
        private int copiedFileCount;
        private int deletedFileCount;
        private Exception error;

        public void incrementSourceFileCount()
        {
            sourceFileCount++;
        }

        public void incrementDestinationFileCount()
        {
            destinationFileCount++;
        }

        public void incrementCopiedFileCount()
        {
            copiedFileCount++;
        }

        public void incrementDeletedFileCount()
        {
            deletedFileCount++;
        }

        public void setError(Exception error)
        {
            this.error = error;
        }

        public int getSourceFileCount()
        {
            return sourceFileCount;
        }

        public int getDestinationFileCount()
        {
            return destinationFileCount;
        }

        public int getCopiedFileCount()
        {
            return copiedFileCount;
        }

        public int getDeletedFileCount()
        {
            return deletedFileCount;
        }

        public Exception getError()
        {
            return error;
        }

        @Override
        public String toString()
        {
            return "ReplicationResult{" +
                    "sourceFileCount=" + sourceFileCount +
                    ", destinationFileCount=" + destinationFileCount +
                    ", copiedFileCount=" + copiedFileCount +
                    ", deletedFileCount=" + deletedFileCount +
                    '}';
        }
    }

    public class QueueDrainingJob implements JobRunner
    {
        @Nullable
        @Override
        public JobRunnerResponse runJob(final JobRunnerRequest request)
        {
            drainQueue();
            return JobRunnerResponse.success();
        }
    }

    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")  // Doesn't grok BlockingQueue.drainTo(Collection)
    @VisibleForTesting
    void drainQueue()
    {
        final List<JiraHomeChangeEvent> events = new ArrayList<JiraHomeChangeEvent>(delayedFileReplicationQueue.size());
        delayedFileReplicationQueue.drainTo(events);
        for (JiraHomeChangeEvent jiraHomeChangeEvent : events)
        {
            submitEvent(jiraHomeChangeEvent);
        }
    }

    public class FullReplicationJob implements JobRunner
    {
        @Nullable
        @Override
        public JobRunnerResponse runJob(final JobRunnerRequest request)
        {
            final Map<FileType, ReplicationResult> result = performReplication();
            if (result == null)
            {
                return JobRunnerResponse.aborted("Full replication is already in progress");
            }
            return JobRunnerResponse.success(result.toString());
        }
    }

    @VisibleForTesting
    @Nullable
    Map<FileType, ReplicationResult> performReplication()
    {
        try
        {
            // Wait a short time for the lock as the possibility exists that the lock is held by someone checking
            // isReplicating(). If this is the case we don't wan't the replication to abort.
            if (!fullReplicationLock.tryLock(500, TimeUnit.MILLISECONDS))
            {
                log.debug("Full replication is already in progress; aborting...");
                return null;
            }
        }
        catch (InterruptedException e)
        {
            log.debug("Full replication is already in progress; aborting...");
            return null;
        }
        try
        {
            log.debug("Replicating Jira Home with secondary home...");
            Map<FileType, ReplicationResult> results = new EnumMap<FileType, ReplicationResult>(FileType.class);
            for (FileType fileType : FileType.values())
            {
                replicateFileType(results, fileType);
            }
            log.debug("Jira Home replicated");
            return results;
        }
        finally
        {
            fullReplicationLock.unlock();
        }
    }

    private void replicateFileType(final Map<FileType, ReplicationResult> results, final FileType fileType)
    {
        if (isEnabled(fileType))
        {
            log.debug("Replicating: {}", fileType);
            final ReplicationResult result = fileType.replicate(this);
            results.put(fileType, result);
            log.debug("Replicated {}: {}", fileType, result);
        }
        else
        {
            log.debug("Not replicated: {}", fileType);
        }
    }

    ReplicationResult replicateAttachments()
    {
        return replicateDir(new File(attachmentPathManager.getAttachmentPath()));
    }

    ReplicationResult replicateAvatars()
    {
        return replicateDir(avatarManager.getAvatarBaseDirectory());
    }

    ReplicationResult replicateIndexSnapshots()
    {
        return replicateDir(new File(jiraHome.getExportDirectory().getAbsolutePath(), "indexsnapshots"));
    }

    ReplicationResult replicatePlugins()
    {
        return replicateDir(pluginPath.getInstalledPluginsDirectory());
    }

    private ReplicationResult replicateDir(File sourceDir)
    {
        String sourcePath = sourceDir.getAbsolutePath();
        File destinationDir = new File(StringUtils.replaceOnce(sourcePath,
                jiraHome.getHomePath(), secondaryJiraHome.getHomePath()));
        String destPath = destinationDir.getAbsolutePath();

        ReplicationResult result = new ReplicationResult();
        if (destPath.startsWith(sourcePath))
        {
            result.setError(new IllegalStateException("Destination [" + destPath + "] is a subdirectory of source [" + sourcePath + "]"));
        }
        else if (sourcePath.startsWith(destPath))
        {
            result.setError(new IllegalStateException("Source [" + sourcePath + "] is a subdirectory of destination [" + destPath + "]"));
        }
        else
        {
            replicateContents(sourceDir, destinationDir, result);
        }
        return result;
    }

    private void replicateContents(File sourceDir, File destinationDir, ReplicationResult result)
    {
        Map<String, File> filesToProcess = new HashMap<String, File>();
        Map<String, File> dirsToProcess = new HashMap<String, File>();

        processSourceDir(sourceDir, result, filesToProcess, dirsToProcess);

        processDestinationDir(destinationDir, result, filesToProcess, dirsToProcess);

        for (File file : filesToProcess.values())
        {
            result.incrementCopiedFileCount();
            submitCopyTask(file);
        }

        for (File file : dirsToProcess.values())
        {
            replicateContents(file, null, result);
        }
    }

    private void processDestinationDir(final File destinationDir, final ReplicationResult result,
            final Map<String, File> filesToProcess, final Map<String, File> dirsToProcess)
    {
        final File[] destinationFiles = getFiles(destinationDir);
        for (File file : destinationFiles)
        {
            if (file.isDirectory())
            {
                final File original = dirsToProcess.remove(file.getName());
                replicateContents(original, file, result);
            }
            else
            {
                result.incrementDestinationFileCount();
                final File original = filesToProcess.remove(file.getName());

                if (original == null)
                {
                    result.incrementDeletedFileCount();
                    File deletedFile = new File(StringUtils.replaceOnce(file.getAbsolutePath(),
                            secondaryJiraHome.getHomePath(), jiraHome.getHomePath()));
                    submitDeleteTask(deletedFile);
                }
                else if (original.length() != file.length())
                {
                    result.incrementCopiedFileCount();
                    submitCopyTask(original);
                }
            }
        }
    }

    private void processSourceDir(final File sourceDir, final ReplicationResult result,
            final Map<String, File> filesToProcess, final Map<String, File> dirsToProcess)
    {
        final File[] sourceFiles = getFiles(sourceDir);
        for (File file : sourceFiles)
        {
            if (file.isDirectory())
            {
                dirsToProcess.put(file.getName(), file);
            }
            else
            {
                result.incrementSourceFileCount();
                filesToProcess.put(file.getName(), file);
            }
        }
    }

    @Nonnull private File[] getFiles(@Nullable File dir)
    {
        if (dir == null)
        {
            return EMPTY_DIR;
        }
        File[] result = dir.listFiles();
        if (result == null)
        {
            return EMPTY_DIR;
        }
        return result;
    }
}
