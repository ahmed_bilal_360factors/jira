package com.atlassian.jira.web.component.cron.generator;

import java.util.concurrent.TimeUnit;

import com.atlassian.jira.web.component.cron.CronEditorBean;

import org.apache.commons.lang.StringUtils;

/**
 * Used to generate an approximate interval based on the state of a {@link com.atlassian.jira.web.component.cron.CronEditorBean}.
 *
 * This is a temporary helper to aid in downgrades from Cloud to Service at the v6.4 boundary
 */
public class CronIntervalGenerator
{
    /**
     * This is a utility method that will process the parameters that the view put into the form and calculate an
     * approx delay from the inputs.
     *
     * @param cronEditorBean the state of the editor form to use for input.
     * @return a delay that approximates the user inputs in cron format.
     */
    public long getDelayFromInput(CronEditorBean cronEditorBean)
    {
        long delay = TimeUnit.DAYS.toMillis(1);

        if (cronEditorBean.isDailyMode())
        {
            //generate a 'daily' spec
            delay = generateDailyDelay(cronEditorBean);
        }
        else if (cronEditorBean.isDayPerWeekMode())
        {
            //generate a 'days per week' spec
            delay = generateDaysOfWeekDelay();
        }
        else if (cronEditorBean.isDaysPerMonthMode())
        {
            //generate a 'days per month' spec
            delay = generateDaysOfMonthOptDelay();
        }
        else if (cronEditorBean.isAdvancedMode())
        {
            delay = TimeUnit.DAYS.toMillis(1);
        }

        return delay;
    }

    long generateDaysOfMonthOptDelay()
    {
        return TimeUnit.DAYS.toMillis(31);
    }

    long generateDaysOfWeekDelay()
    {
        // Assume weekly
        return TimeUnit.DAYS.toMillis(7);
    }

    long generateDailyDelay(CronEditorBean cronEditorBean)
    {
        int increment = getIntFromString(cronEditorBean.getIncrementInMinutes());

        //specify a precise time
        if (increment == 0 || cronEditorBean.isDaysPerMonthMode())
        {
            return TimeUnit.DAYS.toMillis(1);
        }
        else
        {
            return TimeUnit.MINUTES.toMillis(increment);
        }
    }

    int getIntFromString(String string)
    {
        if (string != null && !StringUtils.isEmpty(string))
        {
            return Integer.parseInt(string);
        }
        return 0;
    }
    
}
