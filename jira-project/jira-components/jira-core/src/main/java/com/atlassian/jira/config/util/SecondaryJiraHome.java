package com.atlassian.jira.config.util;


import java.io.File;

import javax.annotation.Nonnull;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.ReplicationSettingsConfiguredEvent;
import com.atlassian.jira.util.PathUtils;

import org.apache.commons.lang3.StringUtils;

/**
 * Secondary JIRA Home to allow replication
 * of everything to a different location for disaster recovery
 *
 * @since 6.4
 */
public class SecondaryJiraHome extends AbstractJiraHome
{
    private final ApplicationProperties applicationProperties;
    private final JiraHome primaryHome;
    private final ClusterManager clusterManager;
    private final EventPublisher eventPublisher;
    private volatile File location;

    public SecondaryJiraHome(final JiraHome jirahome, final ApplicationProperties applicationProperties, final ClusterManager clusterManager,
            final EventPublisher eventPublisher)
    {
        this.applicationProperties = applicationProperties;
        this.primaryHome = jirahome;
        this.clusterManager = clusterManager;
        this.eventPublisher = eventPublisher;

        final boolean isEnabled = applicationProperties.getOption(APKeys.JIRA_SECONDARY_STORAGE);
        refreshLocation(isEnabled);
    }

    @Nonnull
    @Override
    public File getLocalHome()
    {
        return location;
    }

    /**
     * We map the second default path inside the Jira Home because
     * if you are in a cluster this is the shared home and it is already mapped in all the instances
     * We also keep the same structure as the default Jira Home to be able to use this location as primary
     * in disaster recovery
     *
     * @return the default attachment path for the secondary
     */
    public String getDefaultPath()
    {
        return PathUtils.joinPaths(primaryHome.getHomePath(), "secondary");
    }

    public void setEnabled(boolean value)
    {
        applicationProperties.setOption(APKeys.JIRA_SECONDARY_STORAGE, value);
        refreshLocation(value);
    }

    public boolean isEnabled()
    {
        return applicationProperties.getOption(APKeys.JIRA_SECONDARY_STORAGE) && clusterManager.isClusterLicensed();
    }

    /**
     * Applies all the settings for the replication home
     *
     * @param attachments attachments enabled
     * @param plugins plugins enabled
     * @param indexSnapshots index snapshot enabled
     * @param avatars avatars enabled
     */
    public void applySettings(final boolean attachments, final boolean plugins, final boolean indexSnapshots, final boolean avatars)
    {
        final boolean replicationEnabled = attachments || plugins || indexSnapshots || avatars;

        applicationProperties.setOption(JiraHomeChangeEvent.FileType.ATTACHMENT.getKey(), attachments);
        applicationProperties.setOption(JiraHomeChangeEvent.FileType.PLUGIN.getKey(), plugins);
        applicationProperties.setOption(JiraHomeChangeEvent.FileType.INDEX_SNAPSHOT.getKey(), indexSnapshots);
        applicationProperties.setOption(JiraHomeChangeEvent.FileType.AVATAR.getKey(), avatars);

        setEnabled(replicationEnabled);

        eventPublisher.publish(
                new ReplicationSettingsConfiguredEvent(attachments, plugins, avatars, indexSnapshots));
    }

    /**
     * We refresh the location, we do this to only enable this if the feature is enabled.
     * If someone tries to use the secondary home the location will be null, if it is not enabled by this API
     */
    protected void refreshLocation(final boolean isEnabled)
    {
        if(isEnabled)
        {
            final String customPath = applicationProperties.getString(APKeys.JIRA_SECONDARY_HOME);
            this.location = new File(StringUtils.isNotBlank(customPath) ? customPath : getDefaultPath());
        }
        else
        {
            this.location = null;
        }

    }
}
