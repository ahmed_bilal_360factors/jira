package com.atlassian.jira.upgrade.tasks;

import javax.annotation.Nullable;

import com.atlassian.jira.entity.ClusterLockStatusEntity;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.Update;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @since v6.3
 */
public class UpgradeTask_Build6326 extends AbstractImmediateUpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask_Build6326.class);

    public UpgradeTask_Build6326()
    {
        super();
    }

    @Override
    public String getBuildNumber()
    {
        return "6326";
    }

    @Override
    public String getShortDescription()
    {
        return "Default value for the new upgrade_time field of database cluster lock.";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception
    {
        if (setupMode)
        {
            // nothing to do - the table will be empty
            return;
        }

        int rows = Update.into(Entity.CLUSTER_LOCK_STATUS)
                .set(ClusterLockStatusEntity.UPDATE_TIME, new Long(0))
                .whereEqual(ClusterLockStatusEntity.UPDATE_TIME, (Long) null)
                .execute(getEntityEngine());
        log.info("Updated " + rows + " rows in " + Entity.CLUSTER_LOCK_STATUS.getEntityName());
    }

    @Nullable
    @Override
    public String dependsUpon()
    {
        return "6325";
    }

}
