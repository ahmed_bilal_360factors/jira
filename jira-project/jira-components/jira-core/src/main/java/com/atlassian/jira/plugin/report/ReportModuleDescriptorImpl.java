/*
 * Created by IntelliJ IDEA.
 * User: Mike
 * Date: Jul 28, 2004
 * Time: 11:01:00 AM
 */
package com.atlassian.jira.plugin.report;

import java.text.MessageFormat;
import java.util.Map;
import java.util.concurrent.Callable;

import com.atlassian.fugue.Option;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.views.util.SearchRequestViewUtils;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.plugin.AbstractConfigurableModuleDescriptor;
import com.atlassian.jira.plugin.PluginInjector;
import com.atlassian.jira.plugin.webfragment.descriptors.ConditionDescriptorFactory;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.AutowireCapablePlugin;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.descriptors.ConditionalDescriptor;
import com.atlassian.query.Query;

import com.google.common.base.Functions;
import com.google.common.collect.ImmutableMap;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;

/**
 * The report plugin allows end users to write pluggable reports for JIRA.
 *
 * @see com.atlassian.jira.plugin.report.Report
 */
public class ReportModuleDescriptorImpl extends AbstractConfigurableModuleDescriptor<Report>
        implements ReportModuleDescriptor, ConditionalDescriptor
{
    public static final String PARAMS_PATTERN = "{0}?selectedProjectId={1,number,#}&projectOrFilterId=project-{1,number,#}&projectOrFilterName={2}&reportKey={3}";
    public static final int DEFAULT_WEIGHT = 1000;
    public static final String DEFAULT_THUMBNAIL_CSS_CLASS = "default-thumbnail";

    // only used if this report is a singleton!
    private Report report;
    private String label = "Unknown";
    private String url = "/secure/ConfigureReport!default.jspa";
    private String labelKey;
    private ReportCategory category = ReportCategoryImpl.OTHER;
    private String thumbnailCssClass = DEFAULT_THUMBNAIL_CSS_CLASS;
    private int weight = DEFAULT_WEIGHT;
    private String urlProviderClass;
    private ReportUrlProvider urlProvider;
    private Condition condition;

    private final ConditionDescriptorFactory conditionDescFactory;

    @Deprecated
    public ReportModuleDescriptorImpl(JiraAuthenticationContext authenticationContext, ModuleFactory moduleFactory)
    {
        this(authenticationContext, moduleFactory, ComponentAccessor.getComponent(ConditionDescriptorFactory.class));
    }

    public ReportModuleDescriptorImpl(JiraAuthenticationContext authenticationContext, ModuleFactory moduleFactory, ConditionDescriptorFactory conditionDescFactory)
    {
        super(authenticationContext, moduleFactory);
        this.conditionDescFactory = conditionDescFactory;
    }

    protected boolean isSingletonByDefault()
    {
        return false;
    }

    public void init(Plugin plugin, Element element) throws PluginParseException
    {
        super.init(plugin, element);

        final Element labelEl = element.element("label");
        if (labelEl != null)
        {
            if (labelEl.attribute("key") != null)
                labelKey = labelEl.attributeValue("key");
            else
                label = labelEl.getTextTrim();
        }

        final Element urlEl = element.element("url");
        if (urlEl != null)
        {
            String stringUrl = urlEl.getTextTrim();
            if (StringUtils.isNotBlank(stringUrl))
            {
                url = stringUrl;
            }
            urlProviderClass = StringUtils.trimToNull(urlEl.attributeValue("provider"));
        }

        final Element categoryEl = element.element("category");
        if (categoryEl != null)
        {
            String categoryKey = StringUtils.trimToNull(categoryEl.attributeValue("key"));
            final ReportCategory reportCategory = ReportCategoryImpl.byKey(categoryKey);
            if (reportCategory != null)
            {
                category = reportCategory;
            }
            else
            {
                throw new PluginParseException(String.format("The report module: %s specified a category key that is not a valid category: %s", getCompleteKey(), categoryKey));
            }
        }

        try
        {
            final String weightString = StringUtils.trimToNull(element.attributeValue("weight"));
            if (weightString != null)
            {
                weight = Integer.parseInt(weightString);
            }
        }
        catch (final NumberFormatException e)
        {
            throw new PluginParseException(String.format("The report module: %s specified a weight attribute that is not an integer", getCompleteKey()), e);
        }

        final Element thumbnail = element.element("thumbnail");
        if (thumbnail != null)
        {
            String clazz = StringUtils.trimToNull(thumbnail.attributeValue("cssClass"));
            if (clazz != null)
            {
                thumbnailCssClass = clazz;
            }
        }

        condition = conditionDescFactory.retrieveCondition(plugin, element);
    }

    public void enabled()
    {
        super.enabled();
        assertModuleClassImplements(Report.class);

        //load url provider if it is still not loaded
        if (urlProviderClass != null && urlProvider == null)
        {
            try
            {
                Class<Object> loadedClass = plugin.loadClass(urlProviderClass, getClass());
                if (!ReportUrlProvider.class.isAssignableFrom(loadedClass))
                {
                    throw new PluginParseException("Provided class " + urlProviderClass + " does not implement ReportUrlProvider interface");
                }
                try
                {
                    urlProvider = (ReportUrlProvider) PluginInjector.newInstance(loadedClass, plugin);
                }
                catch (Exception e)
                {
                    throw new PluginParseException("Cannot instantiate url provider class: " + urlProviderClass, e);
                }
            }
            catch (ClassNotFoundException e)
            {
                throw new PluginParseException("Cannot load url provider class: " + urlProviderClass, e);
            }
        }
    }

    public Report getModule()
    {
        if (!isSingleton())
        {
            return makeModule();
        }
        else
        {
            if (report == null)
            {
                report = makeModule();
            }

            return report;
        }
    }

    private Report makeModule()
    {
        final Plugin plugin = getPlugin();
        Report reportModule;
        if (plugin instanceof AutowireCapablePlugin)
        {
            reportModule = ((AutowireCapablePlugin) plugin).autowire(getModuleClass());
        }
        else
        {
            reportModule = JiraUtils.loadComponent(getModuleClass());
        }
        reportModule.init(this);
        return reportModule;
    }

    public String getLabel()
    {
        if (labelKey != null)
            return getI18nBean().getText(labelKey);

        return label;
    }

    public String getLabelKey()
    {
        return labelKey;
    }


    @Override
    public String getUrl(Project context)
    {
        Option<String> reportUrl = getUrl(ImmutableMap.<String, Object>of("project", context));
        if (reportUrl.isDefined())
        {
            return reportUrl.get();
        }
        return getProjectIssuesNavUrl(context);
    }

    private String getProjectIssuesNavUrl(Project project)
    {
        final Query projectQuery = JqlQueryBuilder.newBuilder().where()
                .project(project.getKey())
                .buildQuery();
        return SearchRequestViewUtils.getLink(new SearchRequest(projectQuery), "", getAuthenticationContext().getLoggedInUser());
    }

    @Override
    public Option<String> getUrl(final Map<String, Object> context)
    {
        if (urlProvider != null)
        {
            return SafePluginPointAccess.call(new Callable<Option<String>>()
            {
                @Override
                public Option<String> call() throws Exception
                {
                    return urlProvider.getUrl(ReportModuleDescriptorImpl.this, context);
                }
            }).flatMap(Functions.<Option<String>>identity());
        }

        Project project = (Project) context.get("project");
        return Option.option(MessageFormat.format(PARAMS_PATTERN, url, project.getId(), project.getName(), getCompleteKey()));
    }

    @Override
    public ReportCategory getCategory()
    {
        return category;
    }

    @Override
    public String getThumbnailCssClass()
    {
        return thumbnailCssClass;
    }

    @Override
    public int getWeight()
    {
        return weight;
    }

    @Override
    public Condition getCondition()
    {
        return condition;
    }
}
