package com.atlassian.jira.plugin.attachment;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.annotation.Nonnull;

import com.atlassian.jira.issue.attachment.MimetypesFileTypeMap;

import com.google.common.collect.ImmutableList;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extracts a ZIP archive.
 * @since v6.3
 */
public class ZipAttachmentProcessor implements AttachmentProcessor
{
    private static final Logger log = LoggerFactory.getLogger(ZipAttachmentProcessor.class);

    @Override
    public List<AttachmentArchiveEntry> processAttachment(@Nonnull final File file)
    {
        ZipFile zipFile = null;

        try
        {
            zipFile = new ZipFile(file);
            final Enumeration<? extends ZipArchiveEntry> enumeration = zipFile.getEntries();

            final ImmutableList.Builder<AttachmentArchiveEntry> builder = ImmutableList.builder();

            int index = 0;
            int countNoDirectory=0;
            while (enumeration.hasMoreElements() && countNoDirectory < MAX_ENTRIES)
            {
                final ZipArchiveEntry zipEntry = enumeration.nextElement();

                if (!zipEntry.isDirectory())
                {
                    final String name = zipEntry.getName();
                    final String mediaType = MimetypesFileTypeMap.getContentType(name);
                    final AttachmentArchiveEntry entry = new AttachmentArchiveEntryBuilder()
                            .entryIndex(index)
                            .name(name)
                            .size(zipEntry.getSize())
                            .mediaType(mediaType)
                            .build();

                    builder.add(entry);
                    countNoDirectory++;
                }
                index++;
            }

            return builder.build();
        }
        catch (final IOException e)
        {
            log.warn("Something went wrong while processing attachment's stream. Probably the attachment is not a zip file");
            return Collections.emptyList();
        }
        finally
        {
            try
            {
                if (zipFile != null)
                {
                    zipFile.close();
                }
            }
            catch (final IOException ignore)
            {
            }
        }
    }

}
