package com.atlassian.jira.issue.comparator;

import java.util.Comparator;
import java.util.Locale;

import com.atlassian.jira.util.Named;

/**
 * Comparator for Named objects that only compares the names, uses the constructed locale to correctly sort
 * constants names taking into account i18n strings.
 *
 * @since v4.0
 */
public class LocaleSensitiveNamedComparator implements Comparator<Named>
{
    private final LocaleSensitiveStringComparator stringComparator;

    public LocaleSensitiveNamedComparator(final Locale locale)
    {
        this.stringComparator = new LocaleSensitiveStringComparator(locale);
    }

    public int compare(final Named o1, final Named o2)
    {
        if (o1 == null && o2 == null)
        {
            return 0;
        }

        if (o1 == null)
        {
            return 1;
        }

        if (o2 == null)
        {
            return -1;
        }

        String projName1 = o1.getName();
        String projName2 = o2.getName();

        return this.stringComparator.compare(projName1, projName2);
    }
}
