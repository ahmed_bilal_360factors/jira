package com.atlassian.jira.web.action.setup;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.LocaleManager;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.FileFactory;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.web.action.ActionViewData;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SetupMode extends AbstractSetupAction
{
    private static final String SETUP_CLASSIC = "classic";
    private static final String SETUP_INSTANT = "instant";

    private String setupOption = SETUP_CLASSIC;
    private final DatabaseConfigurationManager databaseConfigurationManager;
    private final JiraWebResourceManager webResourceManager;
    private final BuildUtilsInfo buildUtilsInfo;
    private final SetupSharedVariables sharedVariables;

    private static final Logger log = LoggerFactory.getLogger(SetupMode.class);

    private String localeForTexts;
    private String jiraLanguage;

    public SetupMode(final FileFactory fileFactory, final JiraProperties jiraProperties,
            DatabaseConfigurationManager databaseConfigurationManager, final JiraWebResourceManager webResourceManager,
            final BuildUtilsInfo buildUtilsInfo, final SetupSharedVariables setupSharedVariables)
    {
        super(fileFactory, jiraProperties);
        this.databaseConfigurationManager = databaseConfigurationManager;
        this.webResourceManager = webResourceManager;
        this.buildUtilsInfo = buildUtilsInfo;
        this.sharedVariables = setupSharedVariables;
    }

    @Override
    public String doDefault() throws Exception
    {
        if (setupAlready())
        {
            return SETUP_ALREADY;
        }

        if (databaseConfigurationManager.isDatabaseSetup())
        {
            // database configuration already exists, but redirect to SetupDatabase anyway
            // so that the action can perform import of initial data
            return getRedirect("SetupDatabase!default.jspa");
        }

        webResourceManager.putMetadata("version-number", buildUtilsInfo.getVersion());

        return INPUT;
    }

    @Override
    public String doExecute() throws Exception
    {
        if (setupAlready())
        {
            return SETUP_ALREADY;
        }

        sharedVariables.setIsInstantSetup(SETUP_INSTANT.equals(setupOption));

        if (sharedVariables.getIsInstantSetup())
        {
            return getRedirect("SetupProductBundle!default.jspa");
        }

        return getRedirect("SetupDatabase!default.jspa");
    }

    public String getSetupOption()
    {
        return setupOption;
    }

    public void setSetupOption(final String setupOption)
    {
        this.setupOption = setupOption;
    }

    @ActionViewData
    public String getSelectedMode()
    {
        if (!sharedVariables.isSetupModeDecided())
        {
            return null;
        }

        return sharedVariables.getIsInstantSetup() ? SETUP_INSTANT : SETUP_CLASSIC;
    }

    private Map<String, String> getInstalledLocales()
    {
        Set<Locale> installedLocales = ComponentAccessor.getComponentOfType(LocaleManager.class).getInstalledLocales();
        Map<String, String> localeMap = new LinkedHashMap<String, String>();

        for (Locale installedLocale : installedLocales)
        {
            localeMap.put(installedLocale.toString(), installedLocale.getDisplayName(installedLocale));
        }

        return localeMap;
    }

    public String getDefaultServerLanguage()
    {
        String defaultLocale = ComponentAccessor.getApplicationProperties().getDefaultLocale().toString();
        if (getInstalledLocales().keySet().contains(defaultLocale))
        {
            return defaultLocale;
        }
        else
        {
            return "en_UK";
        }
    }

    public String doGetInstalledLocales() throws JSONException, IOException
    {
        final HttpServletResponse response = getHttpResponse();
        final LocaleManager localeManager = ComponentAccessor.getComponentOfType(LocaleManager.class);
        final Set<Locale> installedLocales = localeManager.getInstalledLocales();
        final JSONObject jsonObject = new JSONObject();
        final JSONObject locales = new JSONObject();

        for (Locale installedLocale : installedLocales)
        {
            locales.put(installedLocale.toString(), installedLocale.getDisplayName(installedLocale));
        }

        jsonObject.put("currentLocale", getDefaultServerLanguage());
        jsonObject.put("locales", locales);

        response.setContentType("application/json");
        response.getWriter().write(jsonObject.toString());
        response.getWriter().flush();

        return NONE;
    }

    @ActionViewData(key = "languageDialogDefaultTexts")
    public String getLanguageTextsJson() throws JSONException
    {
        return getLanguageTextsJsonForLocale(getLocale());
    }

    private String getLanguageTextsJsonForLocale(final Locale locale) throws JSONException
    {
        final I18nHelper i18nHelper = ComponentAccessor.getI18nHelperFactory().getInstance(locale);
        final JSONObject json = new JSONObject();

        json.put("button", i18nHelper.getText("common.words.save"));
        json.put("cancel", i18nHelper.getText("common.forms.cancel"));
        json.put("connectionWarningContent", i18nHelper.getText("setup.language.dialog.connection.warning.content"));
        json.put("connectionWarningTitle", i18nHelper.getText("setup.language.dialog.connection.warning.title"));
        json.put("langLabel", i18nHelper.getText("setup.choose.language"));
        json.put("langDesc", i18nHelper.getText("setupdb.server.language.description"));
        json.put("header", i18nHelper.getText("setup.language.dialog.header"));

        return json.toString();
    }

    public String doGetLanguageTexts() throws JSONException, IOException
    {
        final HttpServletResponse response = getHttpResponse();
        final LocaleManager localeManager = ComponentAccessor.getComponentOfType(LocaleManager.class);
        final Locale locale = localeManager.getLocale(localeForTexts);

        response.setContentType("application/json");
        response.getWriter().write(getLanguageTextsJsonForLocale(locale));
        response.getWriter().flush();

        return NONE;
    }

    public String doChangeLanguage() throws IOException
    {
        final HttpServletResponse response = getHttpResponse();
        final LocaleManager localeManager = ComponentAccessor.getComponentOfType(LocaleManager.class);
        final ErrorCollection errorCollection = new SimpleErrorCollection();

        localeManager.validateUserLocale(getLoggedInUser(), getJiraLanguage(), errorCollection);

        if (errorCollection.hasAnyErrors())
        {
            log.warn("Invalid locale submitted ({}), JIRA locale will not be changed", getJiraLanguage());
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
        else
        {
            setJiraLocale(getJiraLanguage());
            setIndexingLanguageForDefaultServerLocale();
        }

        response.setContentType("application/json");
        response.getWriter().write("{}");
        response.getWriter().flush();

        return NONE;
    }

    public void setLocaleForTexts(String localeForTexts)
    {
        this.localeForTexts = localeForTexts;
    }

    public String getJiraLanguage()
    {
        return jiraLanguage;
    }

    public void setJiraLanguage(String jiraLanguage)
    {
        this.jiraLanguage = jiraLanguage;
    }
}
