package com.atlassian.jira.upgrade.tasks;

import java.util.EnumSet;

import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;

import javax.annotation.Nullable;

import static java.lang.String.format;

/**
 * Ensures that a reindex is performed to support quoted phrase queries on text fields.
 *
 * @since v6.1
 */
public class UpgradeTask_Build6141 extends AbstractReindexUpgradeTask
{
    @Override
    public String getBuildNumber()
    {
        return "6141";
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception
    {
        getReindexRequestService().requestReindex(ReindexRequestType.DELAYED, EnumSet.of(AffectedIndex.ISSUE, AffectedIndex.COMMENT), EnumSet.noneOf(SharedEntityType.class));

    }

    @Override
    public String getShortDescription()
    {
        return format
                (
                        "%s This is necessary for the index to support quoted phrase queries on text fields.",
                        super.getShortDescription()
                );
    }

    @Nullable
    @Override
    public String dependsUpon()
    {
        return "6140";
    }

}
