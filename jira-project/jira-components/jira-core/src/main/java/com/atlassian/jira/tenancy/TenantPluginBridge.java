package com.atlassian.jira.tenancy;

import java.net.URL;

import com.atlassian.jira.InitializingComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.plugin.SplitStartupPluginSystemLifecycle;
import com.atlassian.plugins.landlord.spi.LandlordRequestException;
import com.atlassian.plugins.landlord.spi.LandlordRequests;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Acts as the bridge between the plugin system and the TenantAccessor - only allows phase 2 if a tenant has arrived
 * and the plugin system earlyStart is complete.
 *
 * @since v6.4
 */
public class TenantPluginBridge implements InitializingComponent
{
    private volatile boolean triggered;
    private volatile boolean started;
    private final SplitStartupPluginSystemLifecycle pluginManager;
    private final ApplicationProperties applicationProperties;
    private final TenancyCondition tenancyCondition;
    private final JiraTenantAccessor jiraTenantAccessor;

    private static final Logger log = LoggerFactory.getLogger(TenantPluginBridge.class);

    public TenantPluginBridge(final SplitStartupPluginSystemLifecycle pluginManager, final ApplicationProperties applicationProperties,
                 final TenancyCondition tenancyCondition, final JiraTenantAccessor jiraTenantAccessor)
    {
        this.pluginManager = pluginManager;
        this.applicationProperties = applicationProperties;
        this.tenancyCondition = tenancyCondition;
        this.jiraTenantAccessor = jiraTenantAccessor;
    }

    /**
     * Called when a tenant arrives
     */
    public void trigger()
    {
        triggered = true;
        startPhase2IfTenanted();
    }

    /**
     * Called to start phase 2
     */
    public void start()
    {
        started = true;
        startPhase2IfTenanted();
    }

    private void startPhase2IfTenanted()
    {
        if (triggered && started)
        {
            pluginManager.lateStartup();
        }
    }

    @Override
    public void afterInstantiation() throws Exception
    {

        String baseUrl =  applicationProperties.getDefaultBackedText(APKeys.JIRA_BASEURL);
        if (!tenancyCondition.isEnabled())
        {
            LandlordRequests landlordRequests = ComponentAccessor.getComponent(LandlordRequests.class);
            if (baseUrl != null)
            {
                try
                {
                    landlordRequests.acceptTenant(getBaseUrlWithoutProtocol(baseUrl));
                }
                catch (LandlordRequestException e)
                {
                    log.error("There was a problem tenanting JIRA", e);
                }
            }
        }
        else
        {
            if (baseUrl != null)
            {
                triggered = true;
                jiraTenantAccessor.addTenant(new JiraTenantImpl(getBaseUrlWithoutProtocol(baseUrl)));
            }
        }
    }

    private String getBaseUrlWithoutProtocol(String baseUrl)
    {
        try
        {
            URL url = new URL(baseUrl);
            return url.getAuthority() + url.getPath();
        }
        catch (Exception e)
        {
            return baseUrl;
        }
    }
}
