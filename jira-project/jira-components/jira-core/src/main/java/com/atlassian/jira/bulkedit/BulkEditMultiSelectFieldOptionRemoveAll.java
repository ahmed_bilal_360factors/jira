package com.atlassian.jira.bulkedit;


import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.OrderableField;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import java.util.Map;

/**
 * Represents Bulk Edit multi select field option for which the already set field values will be all removed
 *
 * @since v6.4
 */
public class BulkEditMultiSelectFieldOptionRemoveAll extends AbstractBulkEditMultiSelectFieldOption implements BulkEditMultiSelectFieldOption
{
    private static final String ID = "removeall";
    private static final String NAME_I18N_KEY = "bulkedit.actions.multiselect.field.option.REMOVEALL";
    private static final String DESCRIPTION_I18N_KEY = "bulkedit.actions.multiselect.field.option.REMOVEALL";

    public String getId()
    {
        return ID;
    }

    public String getNameI18nKey()
    {
        return NAME_I18N_KEY;
    }

    public String getDescriptionI18nKey()
    {
        return DESCRIPTION_I18N_KEY;
    }

    @Override
    public Map<String,Object> getFieldValuesMap(final Issue issue, final OrderableField field, final Map<String, Object> fieldValuesHolder)
    {
        final Collection valueHolder = (Collection) fieldValuesHolder.get(field.getId());
        valueHolder.clear();
        return ImmutableMap.of(field.getId(), (Object) valueHolder);
    }

    @Override
    public boolean validateOperation(final OrderableField field, final Map<String,Object> fieldValuesHolder)
    {
        return true;
    }

    @Override
    public String getFieldValuesToAdd(final OrderableField field, final Map<String,Object> fieldValuesHolder)
    {
        return StringUtils.EMPTY;
    }
}
