package com.atlassian.jira.plugin.userformat;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import com.atlassian.fugue.Option;
import com.atlassian.jira.plugin.profile.UserFormat;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import com.atlassian.jira.security.RequestCacheKeys;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.commons.lang.StringUtils;

/**
 * Wrapping implementation of UserFormat that caches the results in RequestCache.
 * 
 * Note: results of both format methods are cached in the same cache.
 * But for example format("1", "2") and format("1", "2", null) are still considered as different invocations because
 * there is no guarantee they will return the same results.
 *
 * @since v6.4
 */
public class CachingUserFormat implements UserFormat
{
    private static final long MAX_CACHE_SIZE = 500L;
    private final UserFormat delegate;
    private final Cache<Key, Option<String>> cache;
    /**
     * Giving EMPTY_PARAMS is for preventing NPE when combining extra params with initial velocity params.
     */
    private final static Map<String, Object> EMPTY_PARAMS = new HashMap<String, Object>(0);
    
    @VisibleForTesting
    final static String ID_HOLDER_SIGNATURE = "{CACHING_CONTEXT_ID}";

    public CachingUserFormat(final UserFormat delegate)
    {
        this.delegate = delegate;

        final Map<String, Object> requestCache = JiraAuthenticationContextImpl.getRequestCache();

        @SuppressWarnings("unchecked")
        Cache<Key, Option<String>> cache = (Cache<Key, Option<String>>) requestCache.get(RequestCacheKeys.USER_FORMAT_CACHE);

        if (cache == null)
        {
            cache = CacheBuilder.newBuilder().maximumSize(MAX_CACHE_SIZE).build();
            requestCache.put(RequestCacheKeys.USER_FORMAT_CACHE, cache);
        }
        this.cache = cache;
    }

    @Override
    public String format(final String userkey, final String id)
    {
        return format(userkey, id, EMPTY_PARAMS);
    }

    @Override
    public String format(final String userkey, final String id, final Map<String, Object> params)
    {
        final Key cacheKey = new Key(userkey, delegate, params);
        try
        {
            return StringUtils.replace(formattedUserProfileFromCacheIfAny(cacheKey, userkey, params), ID_HOLDER_SIGNATURE, id);
        }
        catch (ExecutionException e)
        {
            throw new RuntimeException(e.getCause());
        }
    }
    
    private String formattedUserProfileFromCacheIfAny(final Key cacheKey, final String userkey, final Map<String, Object> params) throws ExecutionException
    {
        return cache.get(cacheKey, doRenderFormattedUser(userkey, params)).getOrNull();
    }

    /**
     * It does render formatted user profile and then cache within request level. However, user profile will be cached without
     * the context ID, because it varies for each situation so that it might causes the cached value won't be returned at all.
     * Therefore, the context ID will be mapped later in returned value from cache.
     *
     * @param userkey
     * @param params
     * @return
     */
    private Callable<Option<String>> doRenderFormattedUser(final String userkey, final Map<String, Object> params)
    {
        return new Callable<Option<String>>() {
            @Override
            public Option<String> call() throws Exception {
                return Option.option(delegate.format(userkey, ID_HOLDER_SIGNATURE, params));
            }
        };
    }

    @VisibleForTesting
    UserFormat getDelegate()
    {
        return delegate;
    }

    private static class Key
    {
        private final String userkey;
        private final Map<String, Object> contextParams;
        private final boolean paramsProvided;
        private final UserFormat userFormat;
        private int hashCode;

        private Key(final String userkey, final UserFormat userFormat, final Map<String, Object> contextParams)
        {
            this.userkey = userkey;
            this.contextParams = contextParams;
            this.paramsProvided = contextParams != null;
            this.userFormat = userFormat;
        }

        @Override
        public boolean equals(final Object o)
        {
            if (this == o) { return true; }
            if (o == null || getClass() != o.getClass()) { return false; }

            final Key key = (Key) o;

            if (paramsProvided != key.paramsProvided) { return false; }
            if (userkey != null ? !userkey.equals(key.userkey) : key.userkey != null) { return false; }
            if (userFormat != null ? !userFormat.equals(key.userFormat) : key.userFormat != null) { return false; }
            if (contextParams != null ? !contextParams.equals(key.contextParams) : key.contextParams != null)
            {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode()
        {
            int result = hashCode;
            if (result == 0)
            {
                result = userkey != null ? userkey.hashCode() : 0;
                result = 31 * result + (contextParams != null ? contextParams.hashCode() : 0);
                result = 31 * result + (paramsProvided ? 1 : 0);
                result = 31 * result + (userFormat != null ? userFormat.hashCode() : 0);
                hashCode = result;
            }
            return result;
        }
    }
}
