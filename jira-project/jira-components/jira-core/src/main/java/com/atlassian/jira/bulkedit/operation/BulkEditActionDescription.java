package com.atlassian.jira.bulkedit.operation;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.IssueFieldConstants;

import java.util.Map;
import javax.annotation.Nullable;

/**
 * Descriptions for field actions used in Bulk Edit Wizard - Confirmation Page
 *
 * @since v6.4
 */
public enum BulkEditActionDescription
{
    ISSUE_TYPE(IssueFieldConstants.ISSUE_TYPE,"bulkedit.actions.description.CHANGE_TO"),
    SECURITY(IssueFieldConstants.SECURITY, "bulkedit.actions.description.CHANGE_TO"),
    PRIORITY(IssueFieldConstants.PRIORITY, "bulkedit.actions.description.CHANGE_TO"),
    FIX_FOR_VERIONS(IssueFieldConstants.FIX_FOR_VERSIONS, "bulkedit.actions.description.REPLACE_EXISTING"),
    AFFECTED_VERSIONS(IssueFieldConstants.AFFECTED_VERSIONS, "bulkedit.actions.description.REPLACE_EXISTING"),
    COMPONENTS(IssueFieldConstants.COMPONENTS, "bulkedit.actions.description.REPLACE_EXISTING"),
    ASSIGNEE(IssueFieldConstants.ASSIGNEE, "bulkedit.actions.description.CHANGE_TO"),
    REPORTER(IssueFieldConstants.REPORTER, "bulkedit.actions.description.CHANGE_TO"),
    ENVIRONMENT(IssueFieldConstants.ENVIRONMENT, "bulkedit.actions.description.CHANGE_TO"),
    DUE_DATE(IssueFieldConstants.DUE_DATE, "bulkedit.actions.description.CHANGE_TO"),
    COMMENT(IssueFieldConstants.COMMENT, "bulkedit.actions.description.ADD_NEW"),
    LABEL(IssueFieldConstants.LABELS, "bulkedit.actions.description.REPLACE_EXISTING");

    public static final String DEFAULT_REPLACE = "bulkedit.actions.description.CHANGE_TO";
    public static final String DEFAULT_REMOVEALL = "bulkedit.actions.description.CHANGE_TO";

    private final String id;
    private final String descriptionI18nKey;

    private BulkEditActionDescription(final String id, final String descriptionI18nKey)
    {
        this.id = id;
        this.descriptionI18nKey = descriptionI18nKey;
    }

    public String getId()
    {
        return id;
    }

    public String getDescriptionI18nKey()
    {
        return descriptionI18nKey;
    }

    public static Option<BulkEditActionDescription> getOptionById(String id)
    {
        for (BulkEditActionDescription description : values())
        {
            if (description.getId().equals(id))
            {
                return Option.some(description);
            }
        }
        return Option.none();
    }
}