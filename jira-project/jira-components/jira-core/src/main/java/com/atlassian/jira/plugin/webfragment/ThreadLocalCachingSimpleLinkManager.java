package com.atlassian.jira.plugin.webfragment;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.SimpleLink;
import com.atlassian.jira.plugin.webfragment.model.SimpleLinkSection;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

import com.google.common.base.Supplier;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import static com.atlassian.jira.security.JiraAuthenticationContextImpl.getRequestCache;

/**
 * {@link com.atlassian.jira.plugin.webfragment.SimpleLinkManager} implementation that caches results in {@link
 * com.atlassian.jira.security.JiraAuthenticationContextImpl#getRequestCache()}.
 *
 * @since v6.4
 */
@SuppressWarnings("deprecation")
public class ThreadLocalCachingSimpleLinkManager extends DefaultSimpleLinkManager
{
    private static final String LAZY_LOCATION_KEY = ThreadLocalCachingSimpleLinkManager.class.getName() + ".shouldLocationBeLazy";

    private static final String LAZY_SECTION_KEY = ThreadLocalCachingSimpleLinkManager.class.getName() + ".shouldSectionBeLazy";

    private static final String LINKS_FOR_SECTION_KEY = ThreadLocalCachingSimpleLinkManager.class.getName() + ".getLinksForSection";

    private static final String LINKS_FOR_SECTION_IC_KEY = ThreadLocalCachingSimpleLinkManager.class.getName() + ".getLinksForSectionIgnoreConditions";

    private static final String SECTIONS_FOR_LOCATION_KEY = ThreadLocalCachingSimpleLinkManager.class.getName() + ".getSectionsForLocation";

    private static final String NE_SECTIONS_FOR_LOCATION_KEY = ThreadLocalCachingSimpleLinkManager.class.getName() + ".getNonEmptySectionsForLocation";

    public ThreadLocalCachingSimpleLinkManager(final DynamicWebInterfaceManager webInterfaceManager,
            final SimpleLinkFactoryModuleDescriptors simpleLinkFactoryModuleDescriptors, final JiraAuthenticationContext authenticationContext,
            final WebResourceUrlProvider webResourceUrlProvider, final VelocityRequestContextFactory velocityRequestContextFactory)
    {
        super(webInterfaceManager, simpleLinkFactoryModuleDescriptors, authenticationContext, webResourceUrlProvider, velocityRequestContextFactory);
    }

    @Override
    public boolean shouldLocationBeLazy(@Nonnull final String location, final User remoteUser, @Nonnull final JiraHelper jiraHelper)
    {
        final LoadingCache<CacheKey, Boolean> cache = getRequestCache(LAZY_LOCATION_KEY, new Supplier<LoadingCache<CacheKey, Boolean>>()
        {
            @Override
            public LoadingCache<CacheKey, Boolean> get()
            {
                return CacheBuilder.newBuilder().build(new CacheLoader<CacheKey, Boolean>()
                {
                    @Override
                    public Boolean load(@Nonnull final CacheKey key) throws Exception
                    {
                        return ThreadLocalCachingSimpleLinkManager.super.shouldLocationBeLazy(key.location, key.remoteUser, key.jiraHelper);
                    }
                });
            }
        });

        return cache.getUnchecked(new CacheKey(location, remoteUser, jiraHelper));
    }

    protected static class CacheKey
    {
        @Nonnull
        private final String location;
        @Nullable
        private final User remoteUser;
        @Nonnull
        private final JiraHelper jiraHelper;
        @Nullable
        private final Boolean addIconPrefix;

        private final int hash;

        public CacheKey(@Nonnull final String location, @Nullable final User remoteUser, @Nonnull final JiraHelper jiraHelper)
        {
            this(location, remoteUser, jiraHelper, null);
        }

        public CacheKey(@Nonnull final String location, @Nullable final User remoteUser, @Nonnull final JiraHelper jiraHelper, @Nullable Boolean addIconPrefix)
        {
            this.location = location;
            this.remoteUser = remoteUser;
            this.jiraHelper = jiraHelper;
            this.addIconPrefix = addIconPrefix;
            this.hash = calculateHashCode();
        }

        @Override
        public boolean equals(final Object o)
        {
            if (this == o) { return true; }
            if (!(o instanceof CacheKey)) { return false; }

            final CacheKey cacheKey = (CacheKey) o;

            if (addIconPrefix != null ? !addIconPrefix.equals(cacheKey.addIconPrefix) : cacheKey.addIconPrefix != null)
            { return false; }
            if (!jiraHelper.equals(cacheKey.jiraHelper)) { return false; }
            if (!location.equals(cacheKey.location)) { return false; }
            if (remoteUser != null ? !remoteUser.equals(cacheKey.remoteUser) : cacheKey.remoteUser != null)
            {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode()
        {
            return hash;
        }

        protected int calculateHashCode()
        {
            int result = location.hashCode();
            result = 31 * result + (remoteUser != null ? remoteUser.hashCode() : 0);
            result = 31 * result + jiraHelper.hashCode();
            result = 31 * result + (addIconPrefix != null ? addIconPrefix.hashCode() : 0);
            return result;
        }

    }

    @Override
    public boolean shouldSectionBeLazy(final String section)
    {
        final LoadingCache<String, Boolean> cache = getRequestCache(LAZY_SECTION_KEY, new Supplier<LoadingCache<String, Boolean>>()
        {
            @Override
            public LoadingCache<String, Boolean> get()
            {
                return CacheBuilder.newBuilder().build(new CacheLoader<String, Boolean>()
                {
                    @Override
                    public Boolean load(@Nonnull final String key) throws Exception
                    {
                        return ThreadLocalCachingSimpleLinkManager.super.shouldSectionBeLazy(key);
                    }
                });
            }
        });

        return cache.getUnchecked(section);
    }

    @Override
    @Nonnull
    public List<SimpleLink> getLinksForSection(@Nonnull final String section, final User remoteUser, @Nonnull final JiraHelper jiraHelper)
    {
        return getLinksForSection(section, remoteUser, jiraHelper, false);
    }

    @Override
    @Nonnull
    public List<SimpleLink> getLinksForSection(@Nonnull final String section, final User remoteUser, @Nonnull final JiraHelper jiraHelper, final boolean addIconCachingPrefix)
    {
        final LoadingCache<CacheKey, List<SimpleLink>> cache = getRequestCache(LINKS_FOR_SECTION_KEY, new Supplier<LoadingCache<CacheKey, List<SimpleLink>>>()
        {
            @Override
            public LoadingCache<CacheKey, List<SimpleLink>> get()
            {
                return CacheBuilder.newBuilder().build(new CacheLoader<CacheKey, List<SimpleLink>>()
                {
                    @Override
                    public List<SimpleLink> load(@Nonnull final CacheKey key) throws Exception
                    {
                        return ThreadLocalCachingSimpleLinkManager.super.getLinksForSection(key.location, key.remoteUser, key.jiraHelper, key.addIconPrefix);
                    }
                });
            }
        });

        return cache.getUnchecked(new CacheKey(section, remoteUser, jiraHelper, addIconCachingPrefix));
    }

    @Override
    @Nonnull
    public List<SimpleLink> getLinksForSectionIgnoreConditions(@Nonnull final String section, final User remoteUser, @Nonnull final JiraHelper jiraHelper)
    {
        final LoadingCache<CacheKey, List<SimpleLink>> cache = getRequestCache(LINKS_FOR_SECTION_IC_KEY, new Supplier<LoadingCache<CacheKey, List<SimpleLink>>>()
        {
            @Override
            public LoadingCache<CacheKey, List<SimpleLink>> get()
            {
                return CacheBuilder.newBuilder().build(new CacheLoader<CacheKey, List<SimpleLink>>()
                {
                    @Override
                    public List<SimpleLink> load(@Nonnull final CacheKey key) throws Exception
                    {
                        return ThreadLocalCachingSimpleLinkManager.super.getLinksForSectionIgnoreConditions(key.location, key.remoteUser, key.jiraHelper);
                    }
                });
            }
        });

        return cache.getUnchecked(new CacheKey(section, remoteUser, jiraHelper));
    }

    @Override
    @Nonnull
    public List<SimpleLinkSection> getSectionsForLocation(@Nonnull final String location, final User remoteUser, @Nonnull final JiraHelper jiraHelper)
    {
        final LoadingCache<CacheKey, List<SimpleLinkSection>> cache = getRequestCache(SECTIONS_FOR_LOCATION_KEY, new Supplier<LoadingCache<CacheKey, List<SimpleLinkSection>>>()
        {
            @Override
            public LoadingCache<CacheKey, List<SimpleLinkSection>> get()
            {
                return CacheBuilder.newBuilder().build(new CacheLoader<CacheKey, List<SimpleLinkSection>>()
                {
                    @Override
                    public List<SimpleLinkSection> load(@Nonnull final CacheKey key) throws Exception
                    {
                        return ThreadLocalCachingSimpleLinkManager.super.getSectionsForLocation(key.location, key.remoteUser, key.jiraHelper);
                    }
                });
            }
        });

        return cache.getUnchecked(new CacheKey(location, remoteUser, jiraHelper));
    }

    @Override
    public SimpleLinkSection getSectionForURL(@Nonnull final String topLevelSection, @Nonnull final String URL, final User remoteUser, final JiraHelper jiraHelper)
    {
        return super.getSectionForURL(topLevelSection, URL, remoteUser, jiraHelper);
    }

    @Override
    @Nonnull
    public List<SimpleLinkSection> getNotEmptySectionsForLocation(@Nonnull final String location, final User remoteUser, @Nonnull final JiraHelper jiraHelper)
    {
        final LoadingCache<CacheKey, List<SimpleLinkSection>> cache = getRequestCache(NE_SECTIONS_FOR_LOCATION_KEY, new Supplier<LoadingCache<CacheKey, List<SimpleLinkSection>>>()
        {
            @Override
            public LoadingCache<CacheKey, List<SimpleLinkSection>> get()
            {
                return CacheBuilder.newBuilder().build(new CacheLoader<CacheKey, List<SimpleLinkSection>>()
                {
                    @Override
                    public List<SimpleLinkSection> load(@Nonnull final CacheKey key) throws Exception
                    {
                        return ThreadLocalCachingSimpleLinkManager.super.getNotEmptySectionsForLocation(key.location, key.remoteUser, key.jiraHelper);
                    }
                });
            }
        });

        return cache.getUnchecked(new CacheKey(location, remoteUser, jiraHelper));
    }
}
