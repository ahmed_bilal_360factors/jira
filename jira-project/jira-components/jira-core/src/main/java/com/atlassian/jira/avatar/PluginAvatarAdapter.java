package com.atlassian.jira.avatar;

import javax.annotation.Nonnull;

/**
 * Local adapter for implementing JIRA legacy Avatar class with the PluginAvatar using a wrapper approach.
 */
class PluginAvatarAdapter implements Avatar
{
    private JiraPluginAvatar pluginAvatar;
    private Type type;

    public PluginAvatarAdapter(JiraPluginAvatar pluginAvatar, Avatar.Type type)
    {
        this.pluginAvatar = pluginAvatar;
        this.type = type;
    }

    @Nonnull
    @Override
    public Type getAvatarType()
    {
        return type;
    }

    @Nonnull
    @Override
    public String getContentType()
    {
        return pluginAvatar.getContentType();
    }

    @Nonnull
    @Override
    public String getOwner()
    {
        return pluginAvatar.getOwnerId();
    }

    @Override
    public boolean isSystemAvatar()
    {
        return false;
    }

    @Nonnull
    @Override
    public String getFileName()
    {
        // this is a best effort at creating a useful string but it's almost certainly not the full file name, which
        // actually may not even exist. Nobody should call this method anyway, it's deprecated and not called from
        // any production Atlassian code, its meaning is bit ambiguous and it was always a BAD IDEA, my fault, Christo.
        return pluginAvatar.getUrl().replaceFirst("https?://[^/]+/]", "");

    }

    @Override
    public Long getId()
    {
        // TODO can we remove the multiple user avatar feature?
        return null;
    }
}
