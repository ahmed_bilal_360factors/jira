package com.atlassian.jira.bc.license;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.license.LicenseRole;
import com.atlassian.jira.license.LicenseRoleId;
import com.atlassian.jira.license.LicenseRoleManager;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;

import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since v6.3
 */
public class DefaultLicenseRoleService implements LicenseRoleService, LicenseRoleAdminService
{
    private final GroupManager groupManager;
    private final LicenseRoleManager licenseRoleManager;
    private final JiraAuthenticationContext ctx;
    private final GlobalPermissionManager permissionManager;

    public DefaultLicenseRoleService(@Nonnull final GroupManager groupManager,
            @Nonnull final LicenseRoleManager licenseRoleManager,
            @Nonnull final JiraAuthenticationContext ctx,
            @Nonnull final GlobalPermissionManager permissionManager)
    {
        this.ctx = notNull("ctx", ctx);
        this.permissionManager = notNull("permissionManager", permissionManager);
        this.groupManager = notNull("groupManager", groupManager);
        this.licenseRoleManager = notNull("licenseRoleManager", licenseRoleManager);
    }

    @Override
    public boolean userHasRole(@Nullable ApplicationUser user, @Nonnull LicenseRoleId licenseRoleId)
    {
        notNull("licenseRoleId", licenseRoleId);

        if (user == null)
        {
            return false;
        }

        for (LicenseRole licenseRole : licenseRoleManager.getLicenseRole(licenseRoleId))
        {
            for (String groupName : licenseRole.getGroups())
            {
                if (groupManager.isUserInGroup(user.getUsername(), groupName))
                {
                    return true;
                }
            }
        }

        return false;
    }

    @Nonnull
    @Override
    public ServiceOutcome<Set<LicenseRole>> getRoles()
    {
        final ServiceOutcome<Set<LicenseRole>> outcome = validatePermission();
        if (!outcome.isValid())
        {
            return outcome;
        }

        return ServiceOutcomeImpl.ok(licenseRoleManager.getLicenseRoles());
    }

    @Nonnull
    @Override
    public ServiceOutcome<LicenseRole> getRole(@Nonnull final LicenseRoleId id)
    {
        notNull("licenseRoleId", id);

        final ServiceOutcome<LicenseRole> outcome = validatePermission();
        if (!outcome.isValid())
        {
            return outcome;
        }

        final Option<LicenseRole> roleOption = licenseRoleManager.getLicenseRole(id);
        if (roleOption.isDefined())
        {
            return ServiceOutcomeImpl.ok(roleOption.get());
        }
        else
        {
            return generateNotFoundOutcomeFor(id);
        }
    }

    @Nonnull
    @Override
    public ServiceOutcome<LicenseRole> setRole(@Nonnull final LicenseRole role)
    {
        notNull("role", role);

        ServiceOutcome<LicenseRole> outcome = validatePermission();
        if (!outcome.isValid())
        {
            return outcome;
        }

        if (!licenseRoleManager.isLicenseRoleInstalled(role.getId()))
        {
            return generateNotFoundOutcomeFor(role.getId());
        }

        outcome = validateGroups(role);
        if (!outcome.isValid())
        {
            return outcome;
        }

        return ServiceOutcomeImpl.ok(licenseRoleManager.setLicenseRole(role));
    }

    @Nonnull
    @Override
    public ServiceOutcome<LicenseRole> setGroups(@Nonnull final LicenseRoleId licenseRoleId, @Nonnull final Iterable<String> groups)
    {
        notNull("licenseRoleId", licenseRoleId);
        notNull("groups", groups);

        ServiceOutcome<LicenseRole> outcome = getRole(licenseRoleId);
        if (!outcome.isValid())
        {
            return outcome;
        }

        final LicenseRole licenseRole = outcome.get().withGroups(groups, Option.none(String.class));

        outcome = validateGroups(licenseRole);
        if (!outcome.isValid())
        {
            return outcome;
        }

        return ServiceOutcomeImpl.ok(licenseRoleManager.setLicenseRole(licenseRole));
    }

    private <T> ServiceOutcome<T> validatePermission()
    {
        if (!permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, ctx.getUser()))
        {
            return ServiceOutcomeImpl.error(ctx.getI18nHelper().getText("licenserole.service.permission.denied"),
                    ErrorCollection.Reason.FORBIDDEN);
        }
        else
        {
            return ServiceOutcomeImpl.ok(null);
        }
    }

    private <T> ServiceOutcome<T> validateGroups(LicenseRole role)
    {
        for (String group : role.getGroups())
        {
            if (group == null || !groupManager.groupExists(group))
            {
                //Making sure null is "null".
                group = String.valueOf(group);
                return generateErrorOutcomeFor(LicenseRoleAdminService.ERROR_GROUPS,
                        ctx.getI18nHelper().getText("licenserole.service.group.does.not.exist", group));
            }
        }

        return ServiceOutcomeImpl.ok(null);
    }

    private <T> ServiceOutcome<T> generateErrorOutcomeFor(final String key, final String message)
    {
        final SimpleErrorCollection collection = new SimpleErrorCollection();
        collection.addReason(ErrorCollection.Reason.VALIDATION_FAILED);
        collection.addError(key, message);

        return new ServiceOutcomeImpl<T>(collection);
    }

    private <T> ServiceOutcome<T> generateNotFoundOutcomeFor(LicenseRoleId id)
    {
        final String message = ctx.getI18nHelper().getText("licenserole.service.role.does.not.exist", id.getName());
        return ServiceOutcomeImpl.error(message, ErrorCollection.Reason.NOT_FOUND);
    }
}
