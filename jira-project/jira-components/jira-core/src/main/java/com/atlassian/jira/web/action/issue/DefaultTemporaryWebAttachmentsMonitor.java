package com.atlassian.jira.web.action.issue;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachment;
import com.atlassian.jira.util.ExceptionUtil;
import com.atlassian.util.concurrent.Effect;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.text.MessageFormat.format;

/**
 * This class maintains a list of temporary attachments uploaded by a user.  When the user's session expires or the user
 * logs out, it is responsible of deleting any temporary files that were not converted to proper attachments.
 *
 * @since 6.4
 */
@ParametersAreNonnullByDefault
public class DefaultTemporaryWebAttachmentsMonitor
        implements HttpSessionBindingListener, TemporaryWebAttachmentsMonitor
{

    private static final Logger log = LoggerFactory.getLogger(DefaultTemporaryWebAttachmentsMonitor.class);

    private final ConcurrentMap<String, TemporaryWebAttachment> temporaryAttachments = new ConcurrentHashMap<String, TemporaryWebAttachment>();

    private final StreamAttachmentStore attachmentStore;

    public DefaultTemporaryWebAttachmentsMonitor(final StreamAttachmentStore attachmentStore)
    {
        this.attachmentStore = attachmentStore;
    }

    @Override
    public Option<TemporaryWebAttachment> getById(final String temporaryAttachmentId)
    {
        return Option.option(temporaryAttachments.get(temporaryAttachmentId));
    }

    @Override
    public Option<TemporaryWebAttachment> removeById(final String temporaryAttachmentId)
    {
        return Option.option(temporaryAttachments.remove(temporaryAttachmentId));
    }

    @Override
    public void add(final TemporaryWebAttachment temporaryAttachment)
    {
        checkNotNull(temporaryAttachment, "temporaryAttachment");
        final String stringId = checkNotNull(temporaryAttachment.getStringId(), "attachmentStringId");

        if (temporaryAttachments.putIfAbsent(stringId, temporaryAttachment) != null)
        {
            throw new IllegalArgumentException(String.format("Temporary attachment with id='%s' already in monitor", stringId));
        }

    }

    @Override
    public Collection<TemporaryWebAttachment> getByFormToken(final String formToken)
    {
        checkNotNull(formToken);
        return ImmutableList.copyOf(Iterables.filter(temporaryAttachments.values(), new Predicate<TemporaryWebAttachment>()
        {
            @Override
            public boolean apply(@SuppressWarnings ("NullableProblems") final TemporaryWebAttachment attachment)
            {
                return formToken.equals(attachment.getFormToken());
            }
        }));
    }

    @Override
    public void cleanByFormToken(final String formToken)
    {
        for (final TemporaryWebAttachment temporaryWebAttachment : getByFormToken(formToken))
        {
            safelyRemoveTemporaryAttachmentFromStore(temporaryWebAttachment);
            temporaryAttachments.remove(temporaryWebAttachment.getStringId());
        }
    }

    private void safelyRemoveTemporaryAttachmentFromStore(final TemporaryWebAttachment temporaryWebAttachment)
    {

        attachmentStore.deleteTemporaryAttachment(temporaryWebAttachment.getTemporaryAttachmentId()).fail(new Effect<Throwable>()
        {
            @Override
            public void apply(final Throwable throwable)
            {
                ExceptionUtil.logExceptionWithWarn(log, "Got exception while removing temporary attachment.", throwable);
            }
        });
    }

    @Override
    public void valueBound(final HttpSessionBindingEvent event)
    {
        // nothing to do here
    }

    @Override
    public void valueUnbound(final HttpSessionBindingEvent event)
    {
        // clear temporary attachments
        for (final Map.Entry<String, TemporaryWebAttachment> entry : temporaryAttachments.entrySet())
        {
            safelyRemoveTemporaryAttachmentFromStore(entry.getValue());
        }
        temporaryAttachments.clear();
    }
}

