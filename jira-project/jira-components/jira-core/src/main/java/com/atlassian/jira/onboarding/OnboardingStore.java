package com.atlassian.jira.onboarding;

import com.atlassian.jira.user.ApplicationUser;

import com.google.common.annotations.VisibleForTesting;


public interface OnboardingStore
{
    // All variables have package-level scope for OnboardingService and testing.
    @VisibleForTesting
    static final String STARTED_FLOW_KEY = "jira.onboarding.first.use.flow.started";

    /**
     * Set as true whenever the onboarding flow was evaluated and skipped for the current user (therefore resolved). It
     * is semantically different from completed and can be used in the future to identify users that never saw the
     * onboarding experience from those that effectively completed it.
     */
    @VisibleForTesting
    static final String FIRST_USE_FLOW_RESOLVED = "jira.onboarding.first.use.flow.resolved";

    @VisibleForTesting
    static final String FIRST_USE_FLOW_COMPLETED = "jira.onboarding.first.use.flow.completed";

    @VisibleForTesting
    static final String FIRST_USE_FLOW_CURRENT_SEQUENCE = "jira.onboarding.first.use.flow.current.sequence";

    public boolean isSet(ApplicationUser user, String key);

    public void setBoolean(ApplicationUser user, String key, boolean value);

    public boolean getBoolean(ApplicationUser user, String key);

    public void setString(ApplicationUser user, String key, String value);

    public String getString(ApplicationUser user, String key);
}
