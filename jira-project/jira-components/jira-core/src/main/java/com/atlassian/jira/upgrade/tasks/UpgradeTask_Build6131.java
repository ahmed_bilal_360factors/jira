package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.annotation.Nullable;

/**
 * Removes issue keys from jiraissue table.
 *
 * @since v6.1
 */
public class UpgradeTask_Build6131 extends AbstractImmediateUpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask_Build6131.class);

    public UpgradeTask_Build6131()
    {
        super();
    }


    @Override
    public String getBuildNumber()
    {
        return "6131";
    }

    @Override
    public String getShortDescription()
    {
        return "Removes issue keys from jiraissue table.";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception
    {
        final Connection connection = getDatabaseConnection();
        try
        {
            final String updateSql = "UPDATE " + convertToSchemaTableName("jiraissue") + " SET pkey = NULL WHERE pkey IS NOT NULL";

            PreparedStatement updateStmt = connection.prepareStatement(updateSql);
            try
            {
                int updatedCount = updateStmt.executeUpdate();
                log.info(String.format("Updated %d issues.", updatedCount));
            }
            finally
            {
                updateStmt.close();
            }
        }
        finally
        {
            connection.close();
        }
    }

    @Nullable
    @Override
    public String dependsUpon()
    {
        return "6130";
    }

}
