package com.atlassian.jira.web.action.message;

import com.atlassian.jira.web.action.JiraWebActionSupport.MessageType;

import com.google.common.base.Objects;

/**
 * A message for the user. Defines desired closing behaviour.
 *
 * @since v6.4
 */
public final class PopUpMessage
{
    private final MessageType type;
    private final String title;
    private final String htmlMessage;
    private final ClosingPolicy closingPolicy;

    /**
     * @param type type (severity) of the message
     * @param title plain text title
     * @param htmlMessage HTML body of the message
     * @param closingPolicy closing policy compatible with AUI flags
     */
    PopUpMessage(final MessageType type, final String title, final String htmlMessage, final ClosingPolicy closingPolicy)
    {
        this.type = type;
        this.title = title;
        this.htmlMessage = htmlMessage;
        this.closingPolicy = closingPolicy;
    }

    /**
     * @return type (severity) of the message
     */
    public MessageType getType()
    {
        return type;
    }

    /**
     * @return message type compatible with AUI messages and flags
     */
    public String getAuiMessageType()
    {
        return type.asWebParameter();
    }

    /**
     * @return plain text title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * @return HTML body of the message
     */
    public String getHtmlMessage()
    {
        return htmlMessage;
    }

    /**
     * @return closing policy compatible with AUI flags
     */
    public ClosingPolicy getClosingPolicy()
    {
        return closingPolicy;
    }

    /**
     * @return closing policy compatible with AUI flags
     */
    public String getAuiClosingPolicy()
    {
        return closingPolicy.getAuiValue();
    }

    @Override
    public int hashCode() {return Objects.hashCode(type, title, htmlMessage, closingPolicy);}

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()) {return false;}
        final PopUpMessage other = (PopUpMessage) obj;
        return Objects.equal(this.type, other.type)
                && Objects.equal(this.title, other.title)
                && Objects.equal(this.htmlMessage, other.htmlMessage)
                && Objects.equal(this.closingPolicy, other.closingPolicy);
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this)
                .add("type", type)
                .add("title", title)
                .add("htmlMessage", htmlMessage)
                .add("closingPolicy", closingPolicy)
                .toString();
    }
}
