package com.atlassian.jira.model.querydsl;

import com.mysema.query.sql.ColumnMetadata;
import com.mysema.query.types.path.*;

import java.sql.Types;
import javax.annotation.Generated;

/**
 * QFieldLayoutItem is a Querydsl query object.
 *
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QFieldLayoutItem extends JiraRelationalPathBase<FieldLayoutItemDTO>
{
    public static final QFieldLayoutItem FIELD_LAYOUT_ITEM = new QFieldLayoutItem("FIELD_LAYOUT_ITEM");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final NumberPath<Long> fieldlayout = createNumber("fieldlayout", Long.class);
    public final StringPath fieldidentifier = createString("fieldidentifier");
    public final StringPath description = createString("description");
    public final NumberPath<Long> verticalposition = createNumber("verticalposition", Long.class);
    public final StringPath ishidden = createString("ishidden");
    public final StringPath isrequired = createString("isrequired");
    public final StringPath renderertype = createString("renderertype");

    public QFieldLayoutItem(String alias)
    {
        super(FieldLayoutItemDTO.class, alias, "fieldlayoutitem");
        addMetadata();
    }

    private void addMetadata()
    {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(fieldlayout, ColumnMetadata.named("fieldlayout").withIndex(2).ofType(Types.NUMERIC).withSize(18));
        addMetadata(fieldidentifier, ColumnMetadata.named("fieldidentifier").withIndex(3).ofType(Types.VARCHAR).withSize(255));
        addMetadata(description, ColumnMetadata.named("description").withIndex(4).ofType(Types.VARCHAR).withSize(4000));
        addMetadata(verticalposition, ColumnMetadata.named("verticalposition").withIndex(5).ofType(Types.NUMERIC).withSize(18));
        addMetadata(ishidden, ColumnMetadata.named("ishidden").withIndex(6).ofType(Types.VARCHAR).withSize(60));
        addMetadata(isrequired, ColumnMetadata.named("isrequired").withIndex(7).ofType(Types.VARCHAR).withSize(60));
        addMetadata(renderertype, ColumnMetadata.named("renderertype").withIndex(8).ofType(Types.VARCHAR).withSize(255));
    }
}

