package com.atlassian.velocity;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.apache.velocity.util.introspection.MethodMap;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;

/**
 * Overrides Velocity method map to utilize its find function but not populate methods cache.
 */
public class JiraMethodMap extends MethodMap
{
    private static final Predicate<Class<?>> IS_PUBLIC_CLASS = new Predicate<Class<?>>()
    {
        @Override
        public boolean apply(final Class<?> clazz)
        {
            return Modifier.isPublic(clazz.getModifiers());
        }
    };

    private static final Function<Class<?>, Iterable<Method>> GET_ALL_DECLARED_METHODS = new Function<Class<?>, Iterable<Method>>()
    {
        @Override
        public Iterable<Method> apply(final Class<?> clazz)
        {
            return Arrays.asList(clazz.getDeclaredMethods());
        }
    };

    private static final Predicate<? super Method> PUBLIC_METHOD = new Predicate<Method>()
    {
        @Override
        public boolean apply(final Method method)
        {
            return Modifier.isPublic(method.getModifiers());
        }
    };

    private static final Function<? super Class<?>, Iterable<Class<?>>> GET_CLASS_AND_ALL_INTERFACES = new Function<Class<?>, Iterable<Class<?>>>()
    {
        @Override
        public Iterable<Class<?>> apply(final Class<?> clazz)
        {
            return new ClassAndAllInterfacesIterator(clazz);
        }
    };

    private static final Function<? super MethodComparator, Method> TO_METHOD = new Function<MethodComparator, Method>()
    {
        @Override
        public Method apply(final MethodComparator methodComparator)
        {
            return methodComparator.method;
        }
    };

    private static final Function<Method, MethodComparator> TO_METHOD_COMPARATOR = new Function<Method, MethodComparator>()
    {
        @Override
        public MethodComparator apply(@Nullable final Method method)
        {
            return new MethodComparator(method);
        }
    };

    private final Class<?> clazz;
    private final LoadingCache<String, List<Method>> methodsCache = CacheBuilder.newBuilder()
            .softValues()
            .weakKeys()
                    //this cache is only to initialize cache in JiraIntrospectorCache so we do not need to keep it longer than
                    //longest "hypothetical" single request, there is no real harm if this get unloaded
            .expireAfterAccess(1, TimeUnit.MINUTES)
            .build(new CacheLoader<String, List<Method>>()
            {
                @Override
                public List<Method> load(@Nonnull final String s) throws Exception
                {
                    return copyOf(loadMethodsForName(s));
                }
            });

    public JiraMethodMap(final Class<?> clazz)
    {
        this.clazz = clazz;
    }

    @Override
    public List<Method> get(final String methodName)
    {
        if (methodName == null)
        {
            return Collections.emptyList();
        }
        return methodsCache.getUnchecked(methodName);
    }

    @Override
    public void add(final Method method)
    {
        throw new UnsupportedOperationException("Unsupported operation");
    }

    @Override
    public Method find(final String methodName, final Object[] args) throws AmbiguousException
    {
        Preconditions.checkNotNull(args, "params cannot not be null");

        //first try direct matches
        final Class<?>[] parametersTypes = VelocityTypesUtil.getParametersTypes(args);
        final Collection<Method> methods = copyOf(Iterables.filter(get(methodName), new Predicate<Method>()
        {
            @Override
            public boolean apply(final Method method)
            {
                return Arrays.equals(VelocityTypesUtil.getMethodArgsTypes(method), parametersTypes);
            }
        }));
        if (methods.size() == 1)
        {
            return Iterables.getOnlyElement(methods);
        }
        else if (methods.isEmpty())
        {
            return super.find(methodName, args);
        }
        else
        {
            throw new MethodMap.AmbiguousException();
        }
    }

    private Iterable<Method> loadMethodsForName(final String methodName)
    {
        return deduplicateByNameAndArgumentTypes(
                publicMethodsWithName(allClassesAndInterfaces(), methodName));
    }

    private Iterable<Method> deduplicateByNameAndArgumentTypes(final Iterable<Method> methods)
    {
        return Iterables.transform(
                ImmutableSet.copyOf(transform(methods, TO_METHOD_COMPARATOR)).asList(),
                TO_METHOD);
    }

    private Iterable<Method> publicMethodsWithName(final Iterable<Class<?>> classes, final String methodName)
    {
        final Iterable<Method> allMethods = concat(transform(classes, GET_ALL_DECLARED_METHODS));
        final Iterable<Method> publicMethods = filter(allMethods, PUBLIC_METHOD);
        return filter(publicMethods, new MethodByName(methodName));
    }

    private Iterable<Class<?>> allClassesAndInterfaces()
    {
        return filter(concat(transform(getSuperClasses(clazz), GET_CLASS_AND_ALL_INTERFACES)),
                IS_PUBLIC_CLASS);
    }

    private Iterable<Class<?>> getSuperClasses(final Class<?> clazz)
    {
        final ImmutableList.Builder<Class<?>> retBuilder = ImmutableList.builder();
        for (Class<?> current = clazz; current != null; current = current.getSuperclass())
        {
            retBuilder.add(current);
        }
        return retBuilder.build();
    }

    private static class ClassAndAllInterfacesIterator implements Iterable<Class<?>>
    {

        private final Class<?> clazz;

        public ClassAndAllInterfacesIterator(final Class<?> clazz)
        {
            this.clazz = clazz;
        }

        @Override
        public Iterator<Class<?>> iterator()
        {
            return new Iterator<Class<?>>()
            {
                private final Queue<Class<?>> interfacesToVisit = Lists.<Class<?>>newLinkedList(Collections.singleton(clazz));
                @Override
                public boolean hasNext()
                {
                    return !interfacesToVisit.isEmpty();
                }

                @Override
                public Class<?> next()
                {
                    final Class<?> currentInterface = interfacesToVisit.remove();
                    interfacesToVisit.addAll(Arrays.asList(currentInterface.getInterfaces()));
                    return currentInterface;
                }

                @Override
                public void remove()
                {
                    throw new UnsupportedOperationException("Not supported");
                }
            };
        }
    }

    private static class MethodComparator
    {
        private final Method method;

        private MethodComparator(final Method method)
        {
            this.method = method;
        }

        @Override
        public boolean equals(final Object o)
        {
            if (this == o) { return true; }
            if (o == null || getClass() != o.getClass()) { return false; }

            final MethodComparator that = (MethodComparator) o;

            if (!method.getName().equals(that.method.getName())) { return false; }
            if (!Arrays.equals(method.getParameterTypes(), that.method.getParameterTypes())) { return false; }
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = method.getName().hashCode();
            result = 31 * result + Arrays.hashCode(method.getParameterTypes());
            return result;
        }
    }

    private static class MethodByName implements Predicate<Method>
    {
        private final String methodName;

        public MethodByName(final String methodName) {this.methodName = methodName;}

        @Override
        public boolean apply(final Method method)
        {
            return method.getName().equals(methodName);
        }
    }
}
