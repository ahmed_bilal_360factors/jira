package com.atlassian.jira.model.querydsl;

import com.mysema.query.sql.ColumnMetadata;
import com.mysema.query.types.path.*;

import java.sql.Types;
import javax.annotation.Generated;

/**
 * QMovedIssueKey is a Querydsl query object.
 *
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QMovedIssueKey extends JiraRelationalPathBase<MovedIssueKeyDTO>
{
    public static final QMovedIssueKey MOVED_ISSUE_KEY = new QMovedIssueKey("MOVED_ISSUE_KEY");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final StringPath oldIssueKey = createString("oldIssueKey");
    public final NumberPath<Long> issueId = createNumber("issueId", Long.class);

    public QMovedIssueKey(String alias)
    {
        super(MovedIssueKeyDTO.class, alias, "moved_issue_key");
        addMetadata();
    }

    private void addMetadata()
    {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(oldIssueKey, ColumnMetadata.named("old_issue_key").withIndex(2).ofType(Types.VARCHAR).withSize(255));
        addMetadata(issueId, ColumnMetadata.named("issue_id").withIndex(3).ofType(Types.NUMERIC).withSize(18));
    }
}

