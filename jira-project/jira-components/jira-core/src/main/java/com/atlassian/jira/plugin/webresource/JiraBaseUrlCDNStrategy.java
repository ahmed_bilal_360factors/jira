package com.atlassian.jira.plugin.webresource;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import org.apache.commons.lang.StringUtils;

import java.net.MalformedURLException;
import java.net.URL;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * CDN Strategy for JIRA that produces a dynamic base URL. This will transform CDN-able resources to be served from
 * a single host, with the app's hostname as the first section of the URL path. For example, {@code /my/resource.js}
 * will be transformed to be served from {@code //my.cdnhost.com/my.jirahost.com/my/resource.js}
 *
 * <ul>
 *     <li>
 *         Set the dark feature {@code jira.baseurl.cdn.enabled} to true to enable
 *     </li>
 *     <li>
 *         (Set the dark feature {@code jira.baseurl.cdn.disabled} to true to override any enablement and disable the feature.)
 *     </li>
 *     <li>
 *         Set the system property {@code jira.baseurl.cdn.prefix} to the CDN prefix, eg {@code //my.cdnhost.com}. This
 *         must not end with a trailing slash. The JIRA hostname (determined by the {@code jira.baseurl} application
 *         property) is appended to the prefix as the first path component, i.e.
 *         {@code //my.cdnhost.com/my.jirahost.com}.
 *     </li>
 * </ul>
 *
 * The benefit of this strategy over {@link com.atlassian.jira.plugin.webresource.JiraPrefixCDNStrategy} is that it
 * allows the JIRA hostname to be changed at runtime and for that to be dynamically incorporated into the CDN base URL
 * to a single CDN proxy to service multiple JIRA instances.
 *
 * @since v6.4
 */
public class JiraBaseUrlCDNStrategy implements CDNStrategy
{
    static final String ENABLED_FEATURE_KEY = "jira.baseurl.cdn.enabled";
    static final String PREFIX_SYSTEM_PROPERTY = "jira.baseurl.cdn.prefix";

    private final JiraProperties jiraSystemProperties = JiraSystemProperties.getInstance();
    private final ApplicationProperties applicationProperties;

    public JiraBaseUrlCDNStrategy(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = notNull("applicationProperties", applicationProperties);;
    }

    @Override
    public boolean supportsCdn()
    {
        return !StringUtils.isBlank(getPrefix());
    }

    @Override
    public String transformRelativeUrl(String s)
    {
        return getPrefix() + s;
    }

    private String getPrefix()
    {
        String prefix = jiraSystemProperties.getProperty(PREFIX_SYSTEM_PROPERTY);
        if (null == prefix)
        {
            return null;
        }

        try
        {
            // JIRA's base url is pretty ghetto, it's a string that contains the URL scheme and trailing path eg
            // "http://dogeydoge.atlassian.net/the/path/to/jira". We need only the URL authority here (in particular,
            // we don't want the trailing path).
            String baseUrl = applicationProperties.getString(APKeys.JIRA_BASEURL);
            String authority = new URL(baseUrl).getAuthority();
            return prefix + "/" + authority;
        }
        catch (MalformedURLException ex)
        {
            return null;
        }
    }
}
