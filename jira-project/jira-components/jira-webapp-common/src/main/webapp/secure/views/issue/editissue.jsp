<%@ page import="com.atlassian.jira.component.ComponentAccessor" %>
<%@ page import="com.atlassian.jira.plugin.keyboardshortcut.KeyboardShortcutManager" %>
<%@ page import="com.atlassian.jira.util.JiraUtils" %>
<%@ taglib prefix="ww" uri="webwork" %>
<%@ taglib prefix="aui" uri="webwork" %>
<%@ taglib uri="jiratags" prefix="jira" %>
<%@ taglib prefix="page" uri="sitemesh-page" %>
<html>
<head>
    <meta content="issueaction" name="decorator" />
    <%
        KeyboardShortcutManager keyboardShortcutManager = ComponentAccessor.getComponent(KeyboardShortcutManager.class);
        keyboardShortcutManager.requireShortcutsForContext(KeyboardShortcutManager.Context.issuenavigation);
    %>
    <link rel="index" href="<ww:url value="/issuePath" atltoken="false"/>" />
    <title>
        <ww:text name="'editissue.title'"/>: <ww:property value="summary" /> [<ww:property value="issue/string('key')" />]
    </title>
</head>
<body>

<ww:if test="editable == true">
    <page:applyDecorator id="issue-edit" name="auiform">
        <page:param name="action"><ww:url value="'/secure/EditIssue.jspa'"/></page:param>
        <page:param name="submitButtonName">Update</page:param>
        <page:param name="submitButtonText"><ww:text name="'common.forms.update'" /></page:param>
        <page:param name="cancelLinkURI"><ww:url value="/issuePath" atltoken="false"/></page:param>
        <page:param name="isMultipart">true</page:param>

        <aui:component template="formHeading.jsp" theme="'aui'">
            <aui:param name="'text'"><ww:text name="'editissue.title'"/></aui:param>
        </aui:component>

        <ww:if test="/issueExists == true">

            <aui:component name="'id'" template="hidden.jsp"  theme="'aui'" />

            <ww:component template="issuefields.jsp" name="'createissue'">
                <ww:param name="'displayParams'" value="/displayParams"/>
                <ww:param name="'issue'" value="/issueObject"/>
                <ww:param name="'tabs'" value="/fieldScreenRenderTabs"/>
                <ww:param name="'errortabs'" value="/tabsWithErrors"/>
                <ww:param name="'selectedtab'" value="/selectedTab"/>
                <ww:param name="'ignorefields'" value="/ignoreFieldIds"/>
                <ww:param name="'create'" value="'false'"/>
            </ww:component>

            <jsp:include page="/includes/panels/updateissue_comment.jsp" />

        </ww:if>
    </page:applyDecorator>
</ww:if>
<ww:else>
    <div class="form-body">
        <header>
            <h1><ww:text name="'common.words.error'"/></h1>
        </header>
    <aui:component template="auimessage.jsp" theme="'aui'">
        <aui:param name="'messageType'">error</aui:param>
        <aui:param name="'messageHtml'">
            <ww:if test="hasErrorMessages == 'true'">
                <ul>
                    <ww:iterator value="flushedErrorMessages">
                        <li><ww:property value="." /></li>
                    </ww:iterator>
                </ul>
            </ww:if>
            <ww:if test="remoteUser == null">
                <p><ww:text name="'editissue.notloggedin'"/></p>
                <p>
                    <ww:text name="'editissue.mustfirstlogin'">
                        <ww:param name="'value0'"><jira:loginlink><ww:text name="'common.words.login'"/></jira:loginlink></ww:param>
                        <ww:param name="'value1'"></a></ww:param>
                    </ww:text>
                    <ww:if test="extUserManagement != true">
                        <% if (JiraUtils.isPublicMode()) { %>
                            <ww:text name="'noprojects.signup'">
                                <ww:param name="'value0'"><a href="<%= request.getContextPath() %>/secure/Signup!default.jspa"></ww:param>
                                <ww:param name="'value1'"></a></ww:param>
                            </ww:text>
                        <% } %>
                    </ww:if>
                </p>
            </ww:if>
            <ww:else>
                <ww:if test="hasEditIssuePermission(/issueObject) == 'true'">
                    <p><ww:text name="'editissue.error.no.edit.workflow'"/></p>
                </ww:if>
                <ww:else>
                    <p><ww:text name="'editissue.error.no.edit.permission'"/></p>
                </ww:else>
            </ww:else>
        </aui:param>
    </aui:component>
</ww:else>
<script type="text/javascript">


AJS.$(document).ready(function () {
/*if (cm_selectedProjectKey!="TSTRNG"){	
	if (AJS.$("textarea#description").length > 0){
		var desText = AJS.$("textarea#description").val();
		desText = desText.replace(/\+/g," ");
		AJS.$("textarea#description").val(desText);
	}
}	*/
	if (cm_selectedProjectKey == "HLF" ||cm_selectedProjectKey=='WHIPTAIL' || cm_selectedProjectKey == 'SNP'){
		subtaskType = AJS.$("#summary-val").find("a").text();
			//AJS.$(".aui-page-header-main").text("Incident Report");
			AJS.$("#customfield_10000 option[value='16707']").remove();
			AJS.$("#customfield_10000 option[value='16706']").remove();
			AJS.$("#customfield_10000 option[value='16710']").remove();
			AJS.$("#customfield_10000 option[value='16711']").remove();
			AJS.$("#customfield_10000 option[value='16712']").remove();
			AJS.$("#customfield_10000 option[value='16713']").remove();
	}
		
	if (cm_selectedProjectKey == "GRAVMID"){
		console.log("URL === "+window.location.href);
		
		
		
	}
		var href = window.location.href;
		var lastEnd = href.lastIndexOf("=");
		var type = href.substring(lastEnd+1,href.length);
	
		if (type=="Case"){
				console.log("Issue Type Case == "+type);
			addCaseValidation(0);
		}
	
	
	if(AJS.$("#customfield_12804").length > 0 && AJS.$("#customfield_12804").val().trim() != ""){
		antiSanitizeUsers("customfield_12804");
	}
	
	if(AJS.$("#customfield_12805").length > 0 && AJS.$("#customfield_12805").val().trim() != ""){
		antiSanitizeUsers("customfield_12805");
	}
	
	if(AJS.$("#customfield_13559").length > 0){
		sanitizeUsers("customfield_13559");
	}
	
	if(AJS.$("#customfield_12808").length > 0 && AJS.$("#customfield_12808").val().trim() != ""){
		antiSanitizeUsers("customfield_12808");
	}
	
	AJS.$(".icon-default.aui-icon.aui-icon-small.aui-iconfont-admin-roles").each(function( index ) {
		if(AJS.$( this ).parent().prev().length > 0){
			var customFieldId = AJS.$( this ).parent().prev().attr("id");
			if(customFieldId !="customfield_12804" && customFieldId !="customfield_12805" && customFieldId !="customfield_13559" && customFieldId !="customfield_12808"){
				antiSanitizeUsers(customFieldId);
			}												
		}
	});
	
	console.log("Recurrence Value === "+AJS.$('#customfield_10701').val());
	if (AJS.$('#customfield_10701').val()=='10601'){
		var checkedValues = AJS.$('input[name="customfield_10705"]:checked').map(function() {
			return this.value;
		}).get();
			console.log("selectedDays == " + checkedValues);
			selectedDays = checkedValues.length;
			console.log("selectedDays2 == " + selectedDays);
			console.log(AJS.$('input[name="customfield_10803"]:checked').val());
		        if (AJS.$('input[name="customfield_10803"]:checked').val()=='10732'){
			if (AJS.$("#customfield_10804").length>0 && AJS.$("#customfield_10804").val().trim() != ""){
				numberOfweeks = AJS.$("#customfield_10804").val();
				console.log("number of weeks == "+numberOfweeks);
			}
		}
		else {
			numberOfweeks = AJS.$("#customfield_10709").val();
			console.log("number of weeks == "+numberOfweeks);
		}
	}

	AJS.$('#customfield_10701').trigger("change");
	if (AJS.$('#customfield_10701').val()!='16702'){
		previousRecurrence = AJS.$('#customfield_10701').val();
		
	}
	
		if ((cm_selectedProjectKey=='HLF' || cm_selectedProjectKey=='WHIPTAIL' || cm_selectedProjectKey == 'SNP')&& type!='Case'){
			addHLFValidationOnFields(subtaskType);
		}
			
	
	
	//AJS.$('#customfield_10701').prop("disabled",true);
});

AJS.$(".localHelp").remove();
AJS.$(".help-lnk").remove();

AJS.$('#issue-edit').submit(function(ev) {
	if (cm_selectedProjectKey == 'HLF'){
		
		//return false;
		
	}
		
	var selectedIntervalVal = AJS.$('input[name="customfield_10803"]:checked').val();
	if (selectedIntervalVal == 10733){
		AJS.$("#customfield_10804").val("");
	}
	else if (selectedIntervalVal == 10732){
		AJS.$("#customfield_10709").val("");
	}
	
	if (AJS.$('#customfield_10701').val()=='10601'){
		AJS.$("#customfield_14901").val(numberOfweeks);
		AJS.$("#customfield_14900").val(selectedDays);
		//AJS.$('#issue-edit').append(AJS.$("#previousWeeklyNumber"));
	}
	AJS.$('#customfield_15000').val(previousRecurrence);
	
if(AJS.$("#assignee-field").length > 0){
		var cmAssigneeObj = AJS.$("#assignee option:selected");
		if(cmAssigneeObj.val() != "" && cmAssigneeObj.val() != "-1"){
		   if (cmAssigneeObj.val().lastIndexOf(cm_selectedProjectKey)>=0){

			}
			else
				{
					cmAssigneeObj.val(cmAssigneeObj.val());
				}

			
		}
	}
	
	if(AJS.$("#assignee").length > 0){
		var cmAssigneeObj = AJS.$("#assignee option:selected");
		if(cmAssigneeObj.val() != "" && cmAssigneeObj.val() != "-1"){
			if (cmAssigneeObj.val().lastIndexOf(cm_selectedProjectKey)>=0){

			}
			else
				{
					cmAssigneeObj.val(cmAssigneeObj.val() + "@" + cm_selectedProjectKey);
				}
		}
	}
	
	if (AJS.$("#customfield_12927").length>0){
                var userName = AJS.$("#customfield_12927").val()+ "@" +cm_selectedProjectKey;
                 AJS.$("#customfield_12927").val(userName);
                console.log(userName);
        }

	if (AJS.$("#customfield_13208").length>0){
                var userName = AJS.$("#customfield_13208").val()+ "@" +cm_selectedProjectKey;
                 AJS.$("#customfield_13208").val(userName);
                console.log(userName);
        }
	
	if (AJS.$("#customfield_12805").length>0){
		//alert(AJS.$("#customfield_12805").val());
                var userName = AJS.$("#customfield_12805").val()+ "@" +cm_selectedProjectKey;
			//	alert(userName);
                 AJS.$("#customfield_12805").val(userName);
                console.log("AssuserName== " +userName);
        }
	
	if(AJS.$("#customfield_12804").length > 0){
		sanitizeUsers("customfield_12804");
	}
	
	if(AJS.$("#customfield_12805").length > 0){
		if (cm_selectedProjectKey != "HLF"){
			sanitizeUsers("customfield_12805");
		}
		
	}
	
	if(AJS.$("#customfield_13559").length > 0){
		sanitizeUsers("customfield_13559");
	}
	
	if(AJS.$("#customfield_12808").length > 0){
		sanitizeUsers("customfield_12808");
	}
	
	AJS.$(".icon-default.aui-icon.aui-icon-small.aui-iconfont-admin-roles").each(function( index ) {
		if(AJS.$( this ).parent().prev().length > 0){
			var customFieldId = AJS.$( this ).parent().prev().attr("id");
			if(customFieldId !="customfield_12804" && customFieldId !="customfield_12805" && customFieldId !="customfield_13559" && customFieldId !="customfield_12808"){
				sanitizeUsers(customFieldId);
			}												
		}
	});
AJS.$("#issue-edit-submit").prop("disabled",true);
});

function doValidation(){
	console.log("in doValidation == " + subtaskType);
	var rtArray = [];
	var inputs = getInputArrayForValidation();
	console.log("Ar == " +inputs);
	var errorArr = [];
	var isValidationErr = false;
	var lastEle = "";
	for (var i=0;i<inputs.length;i++){
		//console.log("Val== " + AJS.$("#"+inputs[i]).val());
		var inputType = inputs[i][1];
		var inputVal = getValueByType(inputType,inputs[i][0]);	
		if (inputVal==""){
			var label = "";
			if (inputType =="0"){
				label = AJS.$("#"+inputs[i][0]).parent().find("label").text();
			}
			else if (inputType =="-1"){
				
				label = AJS.$("#"+inputs[i][0]).parent().prev().text();
			}
			else if (inputType =="1"){
				
				label = AJS.$("#cf-"+inputs[i][0]).parent().prev().text();
			}
			else if (inputType == "2"){
				label = AJS.$("#"+inputs[i][0]+"-1").parent().prev().text();
			}
			label = label.replace("Required","");
			addErrDiv("#"+inputs[i][0], label+ " is required.",inputType);
			errorArr.push(true);
			if (lastEle ==""){
				lastEle = inputs[i][0];
			}
			//isValidationErr = true;
		}
		else{
			errorArr.splice(i,1);
			removeErrDiv("#"+inputs[i][0],inputType);
		}
		
	}
	if (errorArr.length > 0){
		AJS.$(window).scrollTop(AJS.$("#divErr"+lastEle).offset().top-100);
		return false;
	}
		
	else{
		return true;
	}

	
}

function getValueByType(inputType,fieldName){
	console.log("inputType == "+inputType);
	  var returnVal = "";
	  switch(inputType){
	  case '0':
		console.log("TTTT == " +AJS.$('#'+fieldName).val());
		returnVal = AJS.$("#"+fieldName).val();
	  break;
		
		case '-1':
		console.log("TTTT == " +AJS.$('#'+fieldName).val());
		returnVal = AJS.$("#"+fieldName).val();
	  break;
	
	  case '1':
		 var returnVal = AJS.$("input[name='"+fieldName+"']:checked").val();
		console.log("ppp== "+returnVal);
		 if (returnVal=="-1"){
			  console.log("ppp== "+returnVal);
			 returnVal="";
		 }
	  break;
	   case '2':
	     var bodyParts = [];
	     AJS.$.each(AJS.$("input[name='"+fieldName+"']:checked"), function(){            
                bodyParts.push(AJS.$(this).val());
            });
			console.log("bodyParts == " +bodyParts);
			if (bodyParts.length > 0){
				returnVal="2";
			}
	   break;
	  
	  }
	return returnVal;
	
}


function addErrDiv(fieldName,errorMsg,inputType){
	removeErrDiv(fieldName,inputType);
			if (inputType=="1" ){
				fieldName = fieldName.replace("#","");
				console.log(AJS.$("#cf-"+fieldName).parent());
				AJS.$("#cf-"+fieldName).parent().parent().append("<div id='divErr"+fieldName+"' class='error'>"+errorMsg+"</div>");
				
			}
			else if (inputType =="2"){
				fieldName = fieldName.replace("#","");
				console.log(AJS.$("#"+fieldName+"-1").parent());
				AJS.$("#"+fieldName+"-1").parent().parent().append("<div id='divErr"+fieldName+"' class='error'>"+errorMsg+"</div>");
				//AJS.$("#divErr"+fieldName)[0].scrollIntoView(true);
				//AJS.$(window).scrollTop(AJS.$("#divErr"+fieldName).offset().top);
			}
			else if (inputType=="0" || inputType=="-1"){
				
				AJS.$(fieldName).parent().append("<div id='divErr"+fieldName.replace("#","")+"' class='error'>"+errorMsg+"</div>");
			//	AJS.$("#divErr"+fieldName)[0].scrollIntoView(true);
				//AJS.$(window).scrollTop(AJS.$("#divErr"+fieldName).offset().top);
			}
			
			//AJS.$("#divErr"+fieldName)[0].scrollIntoView(true);
				//AJS.$(window).scrollTop(AJS.$("#divErr"+fieldName).offset().top);
				
		}
	
	function removeErrDiv(fieldName,inputType){
		if (inputType == "0"){
			fieldName = fieldName.replace("#","");
			AJS.$("#divErr"+fieldName).remove();
		}
		else if (inputType =="-1"){
			AJS.$(fieldName).next().next().next().remove();
		}
		else if (inputType =="1"){
			fieldName = fieldName.replace("#","");
			AJS.$("#divErr"+fieldName).remove();
		}
		else if (inputType =="2"){
			fieldName = fieldName.replace("#","");
			AJS.$("#divErr"+fieldName).remove();
		}
		
	}
	
	function getInputArrayForValidation(){
		console.log("Input Type ==" +subtaskType);
		var inputs = [];
		if (subtaskType =="Injury / Illness" || subtaskType=="Near Miss"){
			var loopMaxIt = 0;
			if (subtaskType =="Near Miss"){
				loopMaxIt = 11;
			}
			else{
				loopMaxIt=12;
			}
			for (var i=0;i<loopMaxIt;i++){
				inputs[i] = new Array(2);
			}
			inputs[0][0] = "customfield_14301";
			inputs[0][1] = "0";
			inputs[1][0] = "customfield_13570";
			inputs[1][1] = "0";
			inputs[2][0] = "customfield_14308";
			inputs[2][1] = "0";
			inputs[3][0] = "customfield_12805";
			inputs[3][1] = "-1";
			inputs[4][0] = "customfield_14311";
			inputs[4][1] = "1";
			inputs[5][0] = "customfield_14312";
			inputs[5][1] = "1";
			inputs[6][0] = "customfield_14314";
			inputs[6][1] = "0";
			inputs[7][0] = "customfield_14315";
			inputs[7][1] = "0";
			if (subtaskType !="Near Miss"){
			inputs[8][0] = "customfield_14000";
			inputs[8][1] = "2";
			inputs[9][0] = "customfield_15100";
			inputs[9][1] = "2";
			inputs[10][0] = "customfield_15101";
			inputs[10][1] = "2";
			inputs[11][0] = "customfield_14307";
			inputs[11][1] = "0";
			}
			else{
			inputs[8][0] = "customfield_15100";
			inputs[8][1] = "2";
			inputs[9][0] = "customfield_15101";
			inputs[9][1] = "2";
			inputs[10][0] = "customfield_14307";
			inputs[10][1] = "0";
			}
			
		}
		else if (subtaskType == "Spill / Release"){
			for (var i=0;i<6;i++){
				inputs[i] = new Array(2);
			}
			inputs[0][0] = "customfield_15102";
			inputs[0][1] = "0";
			inputs[1][0] = "customfield_15105";
			inputs[1][1] = "0";
			inputs[2][0] = "customfield_15109";
			inputs[2][1] = "0";
			inputs[3][0] = "customfield_15110";
			inputs[3][1] = "0";
			inputs[4][0] = "customfield_15100";
			inputs[4][1] = "2";
			inputs[5][0] = "customfield_15101";
			inputs[5][1] = "2";
		}
		
		else if (subtaskType == "Property Damage"){
			for (var i=0;i<1;i++){
				inputs[i] = new Array(2);
			}
			inputs[0][0] = "customfield_15201";
			inputs[0][1] = "0";
		}
		//console.log("Input Array == "+inputs);
		return inputs;
	}

</script>
</body>
</html>
