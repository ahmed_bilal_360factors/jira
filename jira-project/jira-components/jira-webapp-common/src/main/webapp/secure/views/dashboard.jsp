<%@ taglib prefix="ww" uri="webwork" %>
<%@ taglib prefix="aui" uri="webwork" %>
<html>
<head>
    <title><ww:property value="/dashboardTitle"/></title>
    <content tag="section">home_link</content>
</head>
<body class="page-type-dashboard">
    <ww:render value="/dashboardRenderable"/>
<style>
	.separator { border-top : 1px solid #ddd !important }
</style>
	<script>
	/*Developed this for BFSI*/
		AJS.$(document).ready(function () {
			AJS.$(".vertical.tabs").prepend("<li style=\"background-color:#ddd;padding:8px;margin-left:-10px;margin-right:-10px;font-weight:bold\">Dashboards</li>");
			AJS.$(".vertical.tabs").append("<li style=\"background-color:#ddd;padding:8px;margin-left:-10px;margin-right:-10px;font-weight:bold\">Reports</li>");

		AJS.$.ajax({
				url: "/casemanagement/rest/predictapi/latest/filterservice",
				type: 'get',
				dataType: 'json',
				async: false,
				success: function(data) {
					AJS.$(".vertical.tabs").append("<li><a class=\"filter-link\" href='/casemanagement/issues/?filter=-1'><strong><span title='My Open Issues'>My Open Issues</span></strong></a></li>");
					AJS.$(".vertical.tabs").append("<li><a class=\"filter-link\" href='/casemanagement/issues/?filter=-2'><strong><span title='Reported by Me'>Reported by Me</span></strong></a></li>");
					AJS.$(".vertical.tabs").append("<li><a class=\"filter-link\" href='/casemanagement/issues/?filter=-3'><strong><span title='Recently Viewed'>Recently Viewed</span></strong></a></li>");
					AJS.$(".vertical.tabs").append("<li><a class=\"filter-link\" href='/casemanagement/issues/?filter=-4'><strong><span title='All Issues'>All Issues</span></strong></a></li>");
					for (var key in data.filters) {
						AJS.$(".vertical.tabs").append("<li><a class=\"filter-link\" href='/casemanagement/issues/?filter="+ key + "'><strong><span title='Filter : " + data.filters[key] + "'>"+ data.filters[key] +"</span></strong></a></li>");
					}
				},
				error: function(error){
					//paramObj.assignee = paramObj.assignee + "@" +  cm_selectedProjectKey;
				}
			 });
		})
		//alert("Test");
	</script>
</body>
</html>
