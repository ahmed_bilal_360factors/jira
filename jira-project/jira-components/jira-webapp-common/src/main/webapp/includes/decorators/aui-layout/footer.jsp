<%@ page import="com.atlassian.jira.plugin.navigation.HeaderFooterRendering" %>
<%@ page import="static com.atlassian.jira.component.ComponentAccessor.*" %>
<%@ page import="com.atlassian.jira.project.Project" %>
<%@ page import="com.atlassian.jira.user.UserProjectHistoryManager" %>
<%@ page import="com.atlassian.jira.security.Permissions" %>
<%@ page import="com.atlassian.jira.security.JiraAuthenticationContext" %>
<%@ page import="com.atlassian.jira.security.PermissionManager" %>
<%@ page import="java.util.List" %>
<section class="footer-body" style="display:none">
<%
    //
    // IDEA gives you a warning below because it cant resolve JspWriter.  I don't know why but its harmless
    //
    HeaderFooterRendering footerRendering = getComponent(HeaderFooterRendering.class);
    footerRendering.includeFooters(out, request);
    // include web panels
    footerRendering.includeWebPanels(out, "atl.footer");
    
    Project predictSelectedProject = getComponent(UserProjectHistoryManager.class).getCurrentProject(Permissions.BROWSE, getComponent(JiraAuthenticationContext.class).getLoggedInUser());
    //predictSelectedProject = null;
	if(predictSelectedProject == null){
		PermissionManager permissionManager = getComponent(PermissionManager.class);
    	//Get projects for current user with browse permission. There must be one project only
    	List<Project> userProjects =  (List<Project>) permissionManager.getProjectObjects(10, getComponent(JiraAuthenticationContext.class).getLoggedInUser());
		if(userProjects != null && userProjects.size() > 0){
			predictSelectedProject = userProjects.get(0);
		}	
	}
%>
    <div id="footer-logo"><a href="http://www.atlassian.com/">Atlassian</a></div>
</section>
<jsp:include page="/includes/decorators/global-translations.jsp" />
<script type="text/javascript">
AJS.$("#customfield_14901").prev().hide();
AJS.$("#customfield_14901").hide();
AJS.$("#customfield_14900").prev().hide();
AJS.$("#customfield_14900").hide();
AJS.$("#customfield_15000").prev().hide();
AJS.$("#customfield_15000").hide();
AJS.$("#customfield_15001").prev().hide();
AJS.$("#customfield_15001").hide();
var isPDFAdded=false;
var isFirst=0;
cm_selectedProjectId = 0;
cm_selectedProjectKey = "";
var subTaskType = "";
<% if( predictSelectedProject !=null) { %>
cm_selectedProjectId = "<%=predictSelectedProject.getId()%>";
cm_selectedProjectKey = "<%=predictSelectedProject.getKey()%>";
<% } %>


/*******For filter limit in issue search*******/
try{
if(AJS.$("#criteriaJson").length){
	var customFields = JSON.parse(AJS.$("#criteriaJson").html())
	var predictCriteriaFields = customFields.searchers.groups[0].searchers;
	var filteredCustomFields = new Array();
	for(var index=0;index<predictCriteriaFields.length;index++){
	  if(predictCriteriaFields[index].name.indexOf("_") == -1 && predictCriteriaFields[index].name.indexOf("% Limits") == -1 ){
		filteredCustomFields.push(predictCriteriaFields[index]);
	  }
	} 
	customFields.searchers.groups[0].searchers = filteredCustomFields;
	AJS.$("#criteriaJson").html(JSON.stringify(customFields));



	var predictCriteriaFields = JSON.parse(AJS.$("#jqlFieldz").html())
	var filteredCustomFields = new Array();
	for(var index=0;index<predictCriteriaFields.length;index++){
	  if(predictCriteriaFields[index].value.indexOf("_") == -1  && predictCriteriaFields[index].value.indexOf("% Limits") == -1 ){
		filteredCustomFields.push(predictCriteriaFields[index]);
	  }
	} 

	AJS.$("#jqlFieldz").html(JSON.stringify(filteredCustomFields));
}
}

catch(e){
	
}
/************/

AJS.$("#project-name-val").html("Compliance Management");
AJS.$("#project-name-val").attr("href","/casemanagement");
AJS.$(".aui-page-header-image").remove();

AJS.$("#heading-avatar").remove();
AJS.$(".breadcrumbs").css( "padding-left", "15px");
AJS.$("#summary-val").css( "padding-left", "15px");

var issueType = "<%=request.getParameter("issuetype")%>";
cm_openCreateCaseDialog = "<%=request.getParameter("predict_createcase")%>";
cm_openCreateCaseType = "<%=request.getParameter("predict_issue_type")%>";

cm_predict_createriskregistercase = "<%=request.getParameter("predict_createriskregistercase")%>";
cm_predefined_summary = "<%=request.getParameter("predict_riskcontrolitemname")%>";
cm_openCreateTaskDialog = "<%=request.getParameter("predict_createtask")%>";
cm_predefined_cost = "<%=request.getParameter("predict_riskcontrolitemcost")%>";
cm_predefined_efficacy = "<%=request.getParameter("predict_riskcontrolitemefficacy")%>";

cm_surveyId = "<%=request.getParameter("predict_survey_id")%>";
cm_surveyName = "<%=request.getParameter("predict_survey_name")%>";

cm_requirementId =  "<%=request.getParameter("requirementId")%>";
cm_standardId =  "<%=request.getParameter("standardId")%>";

cm_site_id = "<%=request.getParameter("predict_site_id")%>";
cm_asset_id = "<%=request.getParameter("predict_asset_id")%>";

cm_projectId =  "<%=request.getParameter("projectId")%>";
cm_projectTask =  "<%=request.getParameter("projectTask")%>";
cm_projectCase =  "<%=request.getParameter("projectCase")%>";
var isTaskReadOnly=false;
var isCaseReadOnly=false;
var isCapaReadOnly=false;
var isReadOnly=false;	
var numberOfweeks = 0;
var selectedDays = 0;
var previousRecurrence="";
var parentKey = "";
AJS.$(document).ready(function () {
	var isPredictUser="<%=session.getAttribute("predictUser")%>";
	var permissionsFlags="<%=session.getAttribute("permissionsFlags")%>";
	//alert(Boolean(isPredictUser));
	if (isPredictUser=='false'){
		//alert("2 " +isPredictUser);
		AJS.$("#dash-options").hide();
	
	}
	if (permissionsFlags!=null){
		var permissionArray = new Array();
		permissionArray = permissionsFlags.split("-");
		isReadOnly = permissionArray[0]; //
		isTaskReadOnly = permissionArray[1];
		isCapaReadOnly = permissionArray[2];
		isCaseReadOnly = permissionArray[3];
		isRequirement = permissionArray[4];
	}
	console.log(isReadOnly);
	console.log(isCapaReadOnly);
	console.log(isTaskReadOnly);
	console.log(isCaseReadOnly);
	
	AJS.$(".create-case").each(function( index ) {
		var href = AJS.$( this ).attr("href") ;
		if(href !="#"){
			AJS.$( this ).attr("href",href + encodeURIComponent("&pid="  + cm_selectedProjectId))
		}	
	});


	AJS.$("#project-name-val").html("Compliance Management");
	AJS.$("#project-name-val").attr("href","/casemanagement");
	AJS.$(".aui-page-header-image").remove();
	
	
	AJS.$("#share_type_selector").find('[value="project"]').remove();
	var tdObj = AJS.$("#searchOwnerUserName").parent().parent()
	tdObj.prev().parent(); 
	
	
	if(cm_openCreateCaseDialog == "true")
	{
		if(cm_openCreateCaseType && cm_openCreateCaseType == "Task"){
			AJS.$("#task_link").trigger("click");
		}else if(cm_openCreateCaseType && cm_openCreateCaseType == "CAPA"){
			//This is capa link
			AJS.$("#website_link").trigger("click");
		}else if(cm_openCreateCaseType && cm_openCreateCaseType == "Calibration Record"){
			//This is calibration record
			AJS.$("#calibration_record_linkid").trigger("click");
		}
		else{
			AJS.$("#case_link").trigger("click");
		}
	}
	if(cm_openCreateTaskDialog == "true")
	{
		AJS.$("#task_link").trigger("click");
	}
	
	if( (cm_projectTask=='true' || cm_projectCase=='true') && cm_projectId!='null') {
		if(cm_projectTask=='true') {
			AJS.$("#task_link").trigger("click");
		} else if(cm_projectCase=='true') {
			AJS.$("#case_link").trigger("click");
		}
	}
	
		if(cm_openCreateCaseType && cm_openCreateCaseType == "Calibration Record"){
			AJS.$("#customfield_11600").val(cm_site_id);
			AJS.$("#customfield_11600").trigger("change");
			AJS.$("#customfield_11600\\:1").val(cm_asset_id);
			AJS.$("#customfield_11600\\:1").trigger("change");
		}
		if(cm_projectId!='null') {
			//AJS.$("label:contains('__client_project_id')").parent().hide();
			AJS.$("label:contains('__client_project_id')").parent().find('input').val(cm_projectId);
		}
		PDFLink();
		var sText = AJS.$("#summary-val").text();
		sText = sText.replace(/\+/g," ");
		AJS.$("#summary-val").text(sText);

});




AJS.$(window).load(function() {
	if (cm_selectedProjectKey=='HLF'){
		var href = window.location.href;
		var lastParamEnd = href.lastIndexOf("&");
		var lastParam = href.substring(lastParamEnd+1,href.length);
		var lastParamSplitArr  = lastParam.split("=");
		
		var pKey = lastParamSplitArr[0];
		if (pKey!="" && pKey=="parentKey"){
			var cancelHref = AJS.$("#issue-edit-submit").next().prop("href");
			var lastDash = cancelHref.lastIndexOf("-");
			cancelHref = cancelHref.substring(0,lastDash);
			cancelHref = cancelHref+"-"+lastParamSplitArr[1];
			AJS.$("#issue-edit-submit").next().prop("href",cancelHref);
		}
	}


		/*parent.$('.page-container').animate({
        'marginTop' : "-=3000px" //moves down
        });*/

	if(typeof cm_surveyId !="undefined" && cm_surveyId !="null"){
		AJS.$("#customfield_10900").val(cm_surveyId);
	}	
	if(typeof cm_surveyName !="undefined" && cm_surveyName !="null"){
		AJS.$("#customfield_11400").val(cm_surveyName);
	}	
	
	if(cm_predict_createriskregistercase == "true"){
		//Set Summary
		if(cm_predefined_summary){
			AJS.$("#summary").val(cm_predefined_summary);
			AJS.$("#summary").attr('readonly','readonly');
			AJS.$("#customfield_10901").val(cm_predefined_cost);
			AJS.$("#customfield_10901").attr('readonly','readonly');
			AJS.$("#customfield_14400").val(cm_predefined_efficacy);
			AJS.$("#customfield_14400").attr('readonly','readonly');
		}
		AJS.$("label:contains(\"__caselinktype__\")").next().val("riskregister:"+cm_surveyId);
		AJS.$("#customfield_11400").val("");
		AJS.$("#customfield_10900").val("");
    }else if(typeof cm_surveyId != "undefined" && cm_surveyId!="" && cm_surveyId !="null" ){
		AJS.$("label:contains(\"__caselinktype__\")").next().val("survey:"+cm_surveyId);
	}
	AJS.$("#logo").remove();
if (issueType=="1"){
	//alert(1);
		AJS.$("#assetCustField").unbind('change');
		AJS.$("#assetCustField").hide();
		AJS.$("#customfield_11600").unbind('change');
		if (cm_selectedProjectKey=='HLF' || cm_selectedProjectKey=='WHIPTAIL' || cm_selectedProjectKey == 'ICUA'|| cm_selectedProjectKey == 'STHC' || cm_selectedProjectKey == 'KERN'){
			
			AJS.$(".aui-page-header-main").text("Incident Report");
			AJS.$("#customfield_10000 option[value='16707']").remove();
			AJS.$("#customfield_10000 option[value='16706']").remove();
			AJS.$("#customfield_10000 option[value='16710']").remove();
			AJS.$("#customfield_10000 option[value='16711']").remove();
			AJS.$("#customfield_10000 option[value='16712']").remove();
			AJS.$("#customfield_10000 option[value='16713']").remove();
		}
		else if (cm_selectedProjectKey=='GRAVMID'){
			//AJS.$("#description-preview_link").parent().parent().hide();
		}
		
		addCaseValidation(1);
	}
	
	if (cm_selectedProjectKey=='GRAVMID' || cm_selectedProjectKey=='GRTH' || cm_selectedProjectKey=='Ctest' || cm_selectedProjectKey == 'CTEST'){
		hideUnWantedMenuItems();
		
	}
	if (cm_selectedProjectKey=='HLF' || cm_selectedProjectKey=='WHIPTAIL' || cm_selectedProjectKey=='ICUA' || cm_selectedProjectKey=='STHC'|| cm_selectedProjectKey == 'KERN'){
		hideUnWantedMenuItems();
		AJS.$("#case_link").text("Incident");
	}
	if (cm_selectedProjectKey=='SMS' || cm_selectedProjectKey=="SMSLITE"){
		AJS.$("#sms_incident_link").parent().hide();
		
	}
	
	if (cm_selectedProjectKey=='BSG'){
		AJS.$("#osha_case_link").parent().hide();
		AJS.$("#facility_link").parent().hide();
		AJS.$("#facility_link").parent().next().hide();
		AJS.$("#facility_link").parent().next().next().hide();
		AJS.$("#facility_link").parent().next().next().next().hide();
		AJS.$("#ehs_training_link").parent().hide();
		AJS.$("#ehs_training_link").parent().prev().hide();
		AJS.$("#ehs_training_link").parent().prev().prev().hide();
	//	AJS.$("#bb_safety_link").parent().hide();
		AJS.$("#sustainability_link").parent().next().next().hide();
		AJS.$("#sustainability_link").parent().hide();
		AJS.$("#jsa_total").parent().hide();
		AJS.$("#create_incident").parent().hide();
		AJS.$("#create_change_control").parent().hide();
		AJS.$("#incident_report").parent().hide();
		AJS.$("#equipment-inspection").parent().hide();
		AJS.$("#forklift_inspection").parent().hide();
		AJS.$("#safety_meeting").parent().hide();
		AJS.$("#safety_meeting").parent().next().hide();
		AJS.$("#safety_meeting").parent().next().next().hide();
		AJS.$("#Quality-Audit-Checklist").parent().hide();
		AJS.$("#Quality-Audit-Checklist").parent().prev().hide();
		AJS.$("#CAPA-REPORT").parent().hide();
		AJS.$("#behavioral_based_safety_form_link").parent().hide();
		AJS.$("#pha_what_if_checklist").parent().hide();
		AJS.$("#pha_hazop").parent().hide();
		AJS.$("#pha_fmea").parent().hide();
		AJS.$("#calibration_record_linkid").parent().hide();
		AJS.$("#calibration_record_linkid").parent().next().hide();
		
		AJS.$("#fire_tube_link").parent().hide();
		AJS.$("#toxic_link").parent().hide();
		AJS.$("#toxic_link").parent().next().hide();
		AJS.$("#sand_link").parent().next().hide();
		AJS.$("#sand_link").parent().next().next().hide();
		
		AJS.$("#workplaqce_injury_link").parent().hide();
		AJS.$("#workplaqce_injury_link").parent().next().hide();
		AJS.$("#workplaqce_injury_link").parent().next().next().hide();
		
		AJS.$("#my_links_JSA").parent().hide();
		AJS.$("#kyc_links_link").parent().hide();
		AJS.$("#kyc_monitoring_link").parent().hide();
		
	}
	
	if(cm_requirementId > 0){
		AJS.$.getJSON( "/predict360/ws/restapi/requirementservice/sitesbyrequirement/"+cm_requirementId, function( data ) {
			var items = [];
			var optionVal = "";
			AJS.$.each( data.output, function( key, val ) {
				optionVal += "[value!=" + val.jiraSiteId + "]";
			});
			AJS.$('#customfield_11600 option[value!=""]' + optionVal).remove();	
		});
	}
	
	
	AJS.$.ajaxSetup({
	
	beforeSend: function( xhr,settings ) {
		
		//alert(window.location.href);
		
			//alert(1);
			
		
		
		isPDFAdded=true;
		if (this.url && this.url.indexOf("/secure/QuickCreateIssue.jspa?decorator=none")!=-1){
			//alert(2);
		}
		
		
		if(this.url && this.url.indexOf("/secure/AjaxIssueAction.jspa?decorator=none") >=0 ){
			var paramObj = parseParams(settings.data);
			if(paramObj.fieldsToForcePresent=="assignee"){
				if(paramObj.assignee.toLowerCase().lastIndexOf("@" + cm_selectedProjectKey.toLowerCase()) > -1){
					//check is the user name contain @ itself
					     AJS.$.ajax({
							url: "/casemanagement/rest/api/2/user?username=" + paramObj.assignee,
							type: 'get',
							dataType: 'json',
							async: false,
							success: function(data) {
								//user found do nothing
							},
							error: function(error){
								paramObj.assignee = paramObj.assignee + "@" +  cm_selectedProjectKey;
							}
						 });
				}else if(paramObj.assignee != "" && paramObj.assignee != "-1"){
					paramObj.assignee = paramObj.assignee + "@" +  cm_selectedProjectKey;
				}	
			}else if(paramObj.fieldsToForcePresent=="customfield_12804"){
				paramObj.customfield_12804 = ParseArrayForCodes(paramObj.customfield_12804.split(","));
			}else if(paramObj.fieldsToForcePresent=="customfield_13559"){
				paramObj.customfield_13559 = ParseArrayForCodes(paramObj.customfield_13559.split(","));
			}else if(paramObj.fieldsToForcePresent=="customfield_12805"){
				paramObj.customfield_12805 = ParseArrayForCodes(paramObj.customfield_12805.split(","));
			}else if(paramObj.fieldsToForcePresent=="customfield_12808"){
				paramObj.customfield_12808 = ParseArrayForCodes(paramObj.customfield_12808.split(","));
			}else if(paramObj.fieldsToForcePresent=="customfield_12802"){
				paramObj.customfield_12802 = ParseArrayForGroupCodes(paramObj.customfield_12802.split(","));
			}
			
			
			settings.data = AJS.$.param( paramObj );
			
		}else if(this.url && this.url.indexOf("/secure/CommentAssignIssue.jspa") >=0 ){
            var paramObj = parseParams(settings.data);
            if(paramObj.assignee != "" && paramObj.assignee != "-1"){
				paramObj.assignee = paramObj.assignee + "@" +  cm_selectedProjectKey;
			}	
            settings.data = AJS.$.param( paramObj );
     	}else if(this.url && this.url.indexOf("/secure/AssignIssue.jspa") >=0 ){
            var paramObj = parseParams(settings.data);
            if(paramObj.assignee != "" && paramObj.assignee != "-1"){
				paramObj.assignee = paramObj.assignee + "@" +  cm_selectedProjectKey;
			}	
            settings.data = AJS.$.param( paramObj );
     	}
		
	},
	
    dataFilter: function(data, type) {
		
//http://localhost:8080/casemanagement/rest/api/latest/user/assignable/multiProjectSearch?username=ab&projectKeys=DEVTEAM&maxResults=500&_=1456920013977
//http://localhost:8080/casemanagement/rest/api/1.0/users/picker?showAvatar=true&query=abi&_=1456920121033
//http://localhost:8080/casemanagement/rest/api/latest/user/assignable/search?username=ab&projectKeys=DEVTEAM&issueKey=DEVTEAM-545&maxResults=500&_=1456920208429
//alert("ok ok ");
	if(this.url.indexOf("/user/assignable/multiProjectSearch?username=") >=0 || this.url.indexOf("/rest/api/latest/user/assignable/search?username=") >=0 ){
	
	if(type == 'json' && typeof(data) == 'string' && data != "") {
				  var parsed_data = JSON.parse(data);
				  AJS.$.each(parsed_data, function(i, item)
				  {
					  var lastIndexOf = item.name.toLowerCase().lastIndexOf("@" + cm_selectedProjectKey.toLowerCase());
					  if( lastIndexOf > -1){
							console.log("Going to split");
							console.log(item.name);
							var orgininalName =  item.name;
							item.name =  orgininalName.substring(0,lastIndexOf);
							console.log(item.name);
					  }
					  else{
						  
						  var orgininalName =  item.name;
						  item.name =  orgininalName + "@" + cm_selectedProjectKey;
						  console.log(item.name);
					  }
					  
				  });
				  return JSON.stringify(parsed_data);
			}
		}else if(this.url.indexOf("/rest/api/1.0/users/picker?showAvatar=true&query=") >=0){
			if(type == 'json' && typeof(data) == 'string' && data != "") {
				  var parsed_data = JSON.parse(data);
					if(parsed_data.user){
					 AJS.$.each(parsed_data.users, function(i, item)
					  {
						  var lastIndexOf = item.name.lastIndexOf("@" + cm_selectedProjectKey);
						  if( lastIndexOf > -1){
								var orgininalName =  item.name;
								item.name =  orgininalName.substring(0,lastIndexOf);
								console.log(item.name);
						  }
						  
					  });
					}  
				  return JSON.stringify(parsed_data);
			}
		}		
		
		else if(this.url.indexOf("/casemanagement/plugins/servlet/user/picker") >=0) {
				  var parsed_data = JSON.parse(data);
				  AJS.$.each(parsed_data.users, function(i, item)
				  {
					  var lastIndexOf = item.name.toUpperCase().lastIndexOf("@" + cm_selectedProjectKey);
					  if( lastIndexOf > -1){
							var orgininalName =  item.name;
							item.name =  orgininalName.substring(0,lastIndexOf);
					  }
					  else{
						  
						  var orgininalName =  item.name;
						  item.name =  orgininalName + "@" + cm_selectedProjectKey;
						  console.log(item.name);
					  }
					  
				  });
				  return JSON.stringify(parsed_data);
		}
        return data;
    },
		
    ajaxComplete: function() {
		
	
		AJS.$("#project-name-val").html("Compliance Management");
		AJS.$("#project-name-val").attr("href","/casemanagement");
		AJS.$(".aui-page-header-image").remove(); 
    }
});
/*	AJS.$("#priority-field").trigger( "click" )
	AJS.$(AJS.$(".icon.aui-ss-icon.noloading.drop-menu")[0]).trigger("click");
	AJS.$("#priority-field").trigger( "click" )
	*/
	/*var obj = AJS.$(".icon.aui-ss-icon.noloading.drop-menu")[0]
	console.log(obj);
	AJS.$(obj).trigger("click");
*//*	
	setTimeout(function(){ 
							
		parent.$('.page-container').animate({
        'marginTop' : "+=200px" //moves down
        });
		parent.$("#somerandominput").trigger( "click" );
		parent.$("#somerandominput").focus();
						}, 2000);
						
	setTimeout(function(){ 
							
		parent.$('.page-container').animate({
        'marginTop' : "-=200px" //moves down
        });
						}, 2000);
	
*/

});
//parent.doCMLayout(AJS.$(document).height());
/*function onElementHeightChange(elm, callback){
	var lastHeight = elm.clientHeight, newHeight;
	(function run(){
		newHeight = elm.clientHeight;
		if( lastHeight != newHeight )
			callback();
		lastHeight = newHeight;

        if( elm.onElementHeightChangeTimer )
          clearTimeout(elm.onElementHeightChangeTimer);

		elm.onElementHeightChangeTimer = setTimeout(run, 200);
	})();
}


onElementHeightChange(document.body, function(){
  parent.doCMLayout(AJS.$(document).height());
});*/

AJS.$("#project-name-val").html("Compliance Management");
AJS.$("#project-name-val").attr("href","/casemanagement");
AJS.$(".aui-page-header-image").remove();

function removeProjectAvatar(){
	AJS.$("#project-name-val").html("Compliance Management");
	AJS.$("#project-name-val").attr("href","/casemanagement");
	AJS.$(".aui-page-header-image").remove(); 	
}

AJS.$( document ).ajaxComplete(function() {
	console.log(AJS.$("#type-val > img").attr("title"));
	var issueType= AJS.$("#type-val > img").attr("title");	
	if (cm_selectedProjectKey=="HLF"){
			if (issueType=="Near Miss"){
				var oldURL = document.referrer;
				console.log("Last URL == "+oldURL);
				var href = AJS.$("#edit-issue").prop("href");
				var lastEnd = oldURL.lastIndexOf("&");
				var lastDash =  oldURL.lastIndexOf("=");
				var lastP = oldURL.substring(lastEnd+1,lastDash);
				console.log("Last Parent Key == "+lastP);
				console.log("Location == "+window.location.href);
				if (lastP!="parentKey"){
					href = href+"&parentKey="+parentKey;
					console.log("gng to edit mode===  " +href);
					window.location.href = href;
				}
				else{
					//history.go(-1);
					//var lastSlash = oldURL.lastIndexOf("\");
				//	var lastUrl_LastSlash = oldURL(lastSlash+1,oldURL.length);
					//window.location.href = oldURL.substring(0,lastEnd-1);
				}
			}
			
		}
		
		
	/*if (cm_selectedProjectKey!="TSTRNG"){
		if (AJS.$("textarea#description").length > 0){
			var desText = AJS.$("textarea#description").val();
			desText = desText.replace(/\+/g," ");
			AJS.$("textarea#description").val(desText);
		}
	}*/
	removeProjectAvatar();
});

AJS.$( document ).ajaxError(function() {
	removeProjectAvatar();
});

AJS.$( document ).ajaxSend(function() {
	removeProjectAvatar();
});

AJS.$( document ).ajaxComplete(function() {
setTimeout(function(){
		var divs =[];
		 divs=AJS.$("#assignee-container>div");
		 console.log("TTTTTTTTTTTTT==== "+divs.length);
		
		console.log("TTTTTTTTTTTABBBBTT==== "+divs[0]);
		divs = AJS.$(divs[0]).find("div");
		console.log("RRRRRRRRRRRRRRRRR==== "+divs.length);
		AJS.$(divs[0]).show();
		AJS.$(divs[1]).show();
		AJS.$(divs[0]).click(function(e){
			AJS.$(divs[0]).hide();
		});
		AJS.$(divs[1]).click(function(e){
			AJS.$(divs[1]).hide();
		});
	},100);


AJS.$("#customfield_14901").prev().hide();
AJS.$("#customfield_14901").hide();
AJS.$("#customfield_14900").prev().hide();
AJS.$("#customfield_14900").hide();
AJS.$("#customfield_15000").prev().hide();
AJS.$("#customfield_15000").hide();
AJS.$("#customfield_15001").prev().hide();
AJS.$("#customfield_15001").hide();
if (cm_selectedProjectKey=='HLF' || cm_selectedProjectKey=="WHIPTAIL" || cm_selectedProjectKey=="ICUA" || cm_selectedProjectKey=="STHC"|| cm_selectedProjectKey == 'KERN'){
		hideUnWantedMenuItems();
		AJS.$("#case_link").text("Incident");
	}
	PDFLink();
	var issueType= AJS.$("#type-val > img").attr("title");	
	if (issueType!=undefined){
		console.log("PROJCT KEY  == "+ cm_selectedProjectKey);
			var issueType= AJS.$("#type-val > img").attr("title");	
		
		if (issueType.indexOf("Case -")>-1){
			issueType = issueType.substring(0,issueType.indexOf("-")-1);
		}
		 if (issueType.indexOf("Sub-task -")>-1){
			issueType = "SubTask";
		}
		console.log("TYPE====TYPE====" +issueType);
		subTaskType = issueType;
		//if (cm_selectedProjectKey == "GRAVMID" || cm_selectedProjectKey=="DEVTEAM"){
			console.log("GRAVMID111");
			if (issueType == "Case"){
				console.log("In Case111==");
				var href = AJS.$("#edit-issue").prop("href");
				var lastEnd = href.lastIndexOf("=");
				var lastP = href.substring(lastEnd+1,href.length);
				console.log("Last Param == "+lastP);
				if (lastP != issueType){
					console.log("HREF111 ==== "+href);
					href = href+"&type="+issueType;
					console.log("New Value== " +href);
					AJS.$("#edit-issue").prop("href",href);
					console.log("After Changed === "+AJS.$("#edit-issue").prop("href"));
				}
			}
			
		//}
		
		if (cm_selectedProjectKey == "HLF" || cm_selectedProjectKey=="WHIPTAIL" || cm_selectedProjectKey == "ICUA" || cm_selectedProjectKey == "STHC"|| cm_selectedProjectKey == 'KERN'){
			if (issueType =="Case"){
				parentKey = AJS.$("#key-val").prop("href");
			
			var lastDash = parentKey.lastIndexOf("-");
			parentKey = parentKey.substring(lastDash+1,parentKey.length);
				console.log("Parent Key === "+parentKey);
			}
		}
		
		
		
		
		
		if(issueType=='Task' || issueType=='CAPA' || issueType=='Case' || issueType=='SubTask' ){
			if (isTaskReadOnly=='false' && isCapaReadOnly=='false' && isCaseReadOnly=='false'){
				doTaskReadOnly();
			}
			if(issueType=='Task' || issueType=='SubTask'){
				if ((isTaskReadOnly=='false' && isCapaReadOnly=='true' && isCaseReadOnly=='true') || (isTaskReadOnly=='false' && isCapaReadOnly=='false' && isCaseReadOnly=='true')
					|| (isTaskReadOnly=='false' && isCapaReadOnly=='true' && isCaseReadOnly=='false')) 	{
		
						doTaskReadOnly();
				}	
			}
			else if (issueType=='Case'){
				if ((isTaskReadOnly=='true' && isCapaReadOnly=='true' && isCaseReadOnly=='false') || (isTaskReadOnly=='true' && isCapaReadOnly=='false' && isCaseReadOnly=='false')
					|| (isTaskReadOnly=='false' && isCapaReadOnly=='true' && isCaseReadOnly=='false')) 	{
		
						doTaskReadOnly();
				}	
				
			}
			else if (issueType=='CAPA'){
				if ((isTaskReadOnly=='true' && isCapaReadOnly=='false' && isCaseReadOnly=='false') || (isTaskReadOnly=='true' && isCapaReadOnly=='false' && isCaseReadOnly=='false')
					|| (isTaskReadOnly=='false' && isCapaReadOnly=='false' && isCaseReadOnly=='true')) 	{
		
						doTaskReadOnly();
				}	
				
			}
			
		}	
	
}
	removeProjectAvatar();
	AJS.$('#customfield_10701').trigger("change");
		if (AJS.$("#create-subtask-dialog").length>0 ){
			
		    AJS.$("#single_assignee").prop('checked', true).trigger("click");
			AJS.$('#single_assignee').trigger("change");
		
		
		}
});

function hideUnWantedMenuItems(){
	AJS.$("#jsa_total").parent().hide();
		AJS.$("#forklift_inspection").parent().hide();
		AJS.$("#safety_meeting").parent().hide();
		if (cm_selectedProjectKey =="WHIPTAIL"  || cm_selectedProjectKey=="ICUA" || cm_selectedProjectKey=="STHC"|| cm_selectedProjectKey == 'KERN') {
			console.log("Helloooo++++ " +cm_selectedProjectKey);
			AJS.$("#behavioral_based_safety_form_link").parent().show();
		}
		AJS.$("#sms_toolbox_link").hide();
}
function PDFLink(){
var loc=window.location.href;
		if (loc.indexOf("browse")>-1){ 
		var issueKey =loc.substring(loc.lastIndexOf("/")+1,loc.length);
var ul = document.getElementById("opsbar-jira.issue.tools");
console.log( document.getElementById("lastPDF"));
if (document.getElementById("lastPDF")==null){
	console.log("2");
var newLiAdd="<li id=lastPDF class=aui-list-item><a href=/casemanagement/si/jira.issueviews:issue-word/"+issueKey+"/"+issueKey+".PDF class=aui-list-item-link id=jira.issueviews:issue-html><span class=trigger-label>PDF</span></a></li>";
AJS.$(ul).find("ul").append(newLiAdd);
//console.log(ul.innerHTML);
		}
		}	

}

function makeReadOnly(element){
	AJS.$(element).find('*').unbind('click');
	AJS.$(element).find('*').removeAttr('href');
	
}

function doTaskReadOnly(){
		var divEle = AJS.$(".command-bar");
		console.log(divEle);
		makeReadOnly(divEle);
		AJS.$("#edit-issue").removeClass("toolbar-trigger issueaction-edit-issue");
		AJS.$("#edit-issue").addClass("toolbar-trigger");
		var spanItems = AJS.$("#edit-issue").find("span");
		AJS.$(spanItems[0]).hide();
		spanItems=AJS.$("#comment-issue").find("span");
		AJS.$(spanItems[0]).hide();
		AJS.$("#comment-issue").attr('id','');
		AJS.$("#watching-toggle").attr('id','');
		AJS.$("#assign-issue").removeClass("toolbar-trigger issueaction-assign-issue");
		AJS.$("#assign-issue").addClass("toolbar-trigger");
		document.getElementById('summary-val').className = '';
		divEle = AJS.$("#details-module");
		makeReadOnly(divEle);
		divEle=AJS.$("#edit-labels-"+JIRA.Issue.getIssueId()+"-labels");
		AJS.$(divEle).removeClass('icon icon-edit-sml edit-labels');
		console.log(JIRA.Issue.getIssueId());
		AJS.$(divEle).addClass("icon icon-edit-sml");
		divEle = AJS.$("#peoplemodule");
		makeReadOnly(divEle);
		document.getElementById('assignee-val').className = '';
		document.getElementById('reporter-val').className = '';
		divEle = AJS.$("#descriptionmodule");
		makeReadOnly(divEle);
		divEle = AJS.$("#linkingmodule");
		//makeReadOnly(divEle);
		AJS.$("#add-links-link").removeClass("issueaction-link-issue aui-icon aui-icon-small aui-iconfont-add issueaction-aui-icon");
		AJS.$("#add-links-link").addClass("issueaction-link-issue aui-icon aui-icon-small aui-iconfont");
		divEle=AJS.$(".delete-link");
	//AJS.$("delete-link_internal-43600_10003").hide();
		console.log(divEle)
		AJS.$(divEle).css('display','none');
		//activitymodule
		divEle = AJS.$("#activitymodule");
		//makeReadOnly(divEle);
		AJS.$("#footer-comment-button").hide();
		AJS.$(".action-links").hide();
		divEle=AJS.$(".action-details");
		makeReadOnly(divEle);
		AJS.$("#add-attachments-link").hide();
		AJS.$(".aui-dd-parent").hide();
		AJS.$(".attachment-delete").hide();
		AJS.$("#due-date").removeClass("editable-field inactive");
		AJS.$("#customfield_10700-val").removeClass("editable-field inactive");
		AJS.$("#customfield_10709-val").removeClass("editable-field inactive");
		
		AJS.$("#summary-val").children().hide();
		AJS.$("#edit-labels-"+JIRA.Issue.getIssueId()+"-labels").hide();
		AJS.$("#assignee-val").children().hide();
		AJS.$("#reporter-val").children().hide();
		AJS.$("#due-date").find("span").hide();
		
		
		spanItems = AJS.$("#customfield_10700-val").find("span");
		console.log(spanItems);
		AJS.$(spanItems[1]).hide();
		spanItems = AJS.$("#customfield_10709-val").find("span");
		AJS.$(spanItems[1]).hide();
	
}

AJS.$( document ).ajaxComplete(function() {
	
	removeProjectAvatar();
});

AJS.$( document ).ajaxStart(function() {
	removeProjectAvatar();
});

AJS.$( document ).ajaxStop(function() {
	removeProjectAvatar();
});

AJS.$( document ).ajaxSuccess(function() {
	removeProjectAvatar();
});

/*
AJS.$('#issue-content').bind("DOMSubtreeModified",function(){
  
});*/

JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function(e, context, reason) {
		if (reason == JIRA.CONTENT_ADDED_REASON.inlineEditStarted) {
		   var $context = AJS.$(context);
		   console.log("ABC=================== "+context);
			if($context.find("#assignee-field").length > 0) {
				//alert("started");
				AJS.$("#assignee-field").blur();
				//parent.$("#somerandominput").trigger( "click" );
				//parent.$("#somerandominput").focus();
				//console.log(parent.$("#somerandominput"));
				
			}else if($context.find("#customfield_12804").length > 0){
				//alert("Going right direction");
				antiSanitizeUsers("customfield_12804");
			}else if($context.find("#customfield_13559").length > 0){
				//alert("Going right direction");
				antiSanitizeUsers("customfield_13559");
			}else if($context.find("#customfield_12805").length > 0){
				//alert("Going right direction");
				antiSanitizeUsers("customfield_12805");
			}else if($context.find("#customfield_12808").length > 0){
				antiSanitizeUsers("customfield_12808");
			}else if($context.find("#customfield_12805").length > 0){
				antiSanitizeUsers("customfield_12805");
			}else if($context.find("#customfield_12106").length > 0){
				antiSanitizeUsers("customfield_12106");
			}else if($context.find("#customfield_12802").length > 0){
				var groupAssigneeVal = AJS.$("#customfield_12802").val();
				antiSanitizeGroup("customfield_12802");
			}
			else if ($context.find("textarea#description").length > 0 ){
				var desText = AJS.$("textarea#description").val();
				desText = desText.replace(/\+/g," ");
				AJS.$("textarea#description").val(desText);
			}
	/*if (cm_selectedProjectKey!="TSTRNG"){		
		if (AJS.$("textarea#description").length > 0){
			var desText = AJS.$("textarea#description").val();
			desText = desText.replace(/\+/g," ");
			AJS.$("textarea#description").val(desText);
		}
	}*/
	
		}		        
		if (reason == JIRA.CONTENT_ADDED_REASON.panelRefreshed) {

		}
});

        AJS.$(function() {
			//AJS.$(AJS.$(".icon.aui-ss-icon.noloading.drop-menu")[0]).trigger("click");
			//AJS.$(".icon.aui-ss-icon.noloading.drop-menu")
            //AJS.$("[onClick]").each(function(){
			
			AJS.$(AJS.$(".icon.aui-ss-icon.noloading.drop-menu")[0]).click(function() {
				//parent.$("#somerandominput").trigger( "click" );
				//parent.$("#somerandominput").focus();
				console.log(":: Test ::");
			});			
				
			/*AJS.$(".icon.aui-ss-icon.noloading.drop-menu").each(function(){
                //var x = AJS.$(this).attr("onClick");
                //AJS.$(this).removeAttr("onClick").unbind("click");
                AJS.$(this).click(function(e){
                    // do what you want here...
					//console.log(x);
                    //eval(x); // do original action here...
					//parent.$("#somerandominput").trigger( "click" );
					parent.$("#somerandominput").focus();
					console.log("Here");
					//AJS.$(this).focus();
					//AJS.$(this).trigger("click");
                });
            });*/
        });


/*AJS.$.fn.setCursorPosition = function(position){
	if(this.lengh == 0) return this;
	return AJS.$(this).setSelection(position, position);
}
AJS.$("#summary").setCursorPosition(100);
*/


  function getQueryVariable(url, variable) {
	  console.log(getQueryVariable);
  	 var query = url.substring(1);
     var vars = query.split('&');
     for (var i=0; i<vars.length; i++) {
          var pair = vars[i].split('=');
		  console.log(pair);
          if (pair[0] == variable) {
            return pair[1];
          }
     }

     return false;
  }
  
  function parseParams(str) {
    return str.split('&').reduce(function (params, param) {
        var paramSplit = param.split('=').map(function (value) {
            return decodeURIComponent(value.replace('+', ' '));
        });
        params[paramSplit[0]] = paramSplit[1];
        return params;
    }, {});
}
  
  function ParseArrayForCodes(attendees){
		console.log(attendees);
		var originalAttendees = new Array();
		for(userIndex = 0; userIndex < attendees.length ; userIndex++){
			if(attendees[userIndex].trim() != "" && attendees[userIndex].trim() != "+"){
				var lastIndexOf = attendees[userIndex].trim().toLowerCase().lastIndexOf("@" + cm_selectedProjectKey.toLowerCase());
				if( lastIndexOf == -1){
					console.log(attendees[userIndex]);
					attendees[userIndex] = (attendees[userIndex].trim() + "@" + cm_selectedProjectKey).replace(/\+/g, ' '); 
					attendees[userIndex] = attendees[userIndex].trim();
					//Special hard code check
					if(attendees[userIndex].trim() == "safety+department@SMS"){
						attendees[userIndex] = "safety department@SMS";
					}
					console.log(attendees[userIndex]);
					originalAttendees.push(attendees[userIndex]);
				}else{
					if(attendees[userIndex] == "jkaiser@hrs" || attendees[userIndex] == "admin@HRS" || attendees[userIndex] == "jmaselas@hrs"  || attendees[userIndex] == "kmcdaniel@hrs"   || attendees[userIndex] == "kladut@hrs"   || attendees[userIndex] == "laustin@hrs"   || attendees[userIndex] == "mmatran@hrs"   || attendees[userIndex] == "tmclin@hrs"){
						originalAttendees.push(attendees[userIndex].trim() +"@hrs");
					}else{
						originalAttendees.push(attendees[userIndex].trim());
					}
				}
			}
		}
		console.log(AJS.$("#multiple_assignee").val());
		for(userIndex = 0; userIndex<originalAttendees.length; userIndex++){
			var lastIndexOf = originalAttendees[userIndex].lastIndexOf("@" + cm_selectedProjectKey);
			if (AJS.$("#multiple_assignee").val()=='1'){
				return cm_selectedProjectKey + "-" +  originalAttendees[userIndex].substring(0,lastIndexOf);
			}else{
				originalAttendees[userIndex] = originalAttendees[userIndex].substring(0,lastIndexOf);
				}
				console.log("1KKKK===" +AJS.$("#cutsomfield_13208"));
			if (AJS.$("#cutsomfield_13208")){
			if (AJS.$("#cutsomfield_13208").length>0){
                           var userName = AJS.$("#customfield_13208").val()+"@"+cm_selectedProjectKey;
                           AJS.$("#customfield_13208").val(userName);
                         console.log(userName);
                       originalAttendees[userIndex] = userName;

                    }
			}

		}
    	return originalAttendees;
		
	}
  
	 function ParseArrayForGroupCodes(attendees){
			console.log(attendees);
			var originalAttendees = new Array();
			for(userIndex = 0; userIndex < attendees.length ; userIndex++){
				if(attendees[userIndex].trim() != "" && attendees[userIndex].trim() != "+"){
					var lastIndexOf = attendees[userIndex].trim().toLowerCase().lastIndexOf("@" + cm_selectedProjectKey.toLowerCase());
					if( lastIndexOf == -1){
						console.log(attendees[userIndex]);
						attendees[userIndex] = (cm_selectedProjectKey + "-" + attendees[userIndex].trim()).replace(/\+/g, ' '); 
						attendees[userIndex] = attendees[userIndex].trim();
						originalAttendees.push(attendees[userIndex]);
					}else{
						originalAttendees.push(attendees[userIndex].trim());
					}
				}
			}
			console.log(originalAttendees);
			return originalAttendees;
		}

	function antiSanitizeUsers(fieldId){
		var attendees = AJS.$("#" + fieldId).val().split(",");
		for(userIndex = 0; userIndex < attendees.length ; userIndex++){
			if(attendees[userIndex].trim() != ""){
				var lastIndexOf = attendees[userIndex].toLowerCase().lastIndexOf("@" + cm_selectedProjectKey.toLowerCase());
				if( lastIndexOf > -1){
					var orgininalName =  attendees[userIndex];
					attendees[userIndex] = orgininalName.substring(0,lastIndexOf); 
				}
				
			}
		}
		AJS.$("#" + fieldId).val(attendees);
	}

	function sanitizeUsers(fieldId){
		if (AJS.$("#assignee") && AJS.$("#assignee").val()=='undefined'){
			if (AJS.$("#assignee").length <= 0 ){
					var fieldObj = AJS.$("#" + fieldId);
					var attendees = ParseArrayForCodes(fieldObj.val().split(","));
					var cloned = fieldObj.clone();
					cloned.val(attendees);
					cloned.hide();
					fieldObj.attr("name","");
					fieldObj.parent().append(cloned);
			}
		
		}
	}
	
	function antiSanitizeGroup(fieldId){
		var attendees = AJS.$("#" + fieldId).val().split(",");
		for(userIndex = 0; userIndex < attendees.length ; userIndex++){
			if(attendees[userIndex].trim() != ""){
				var lastIndexOf = attendees[userIndex].toLowerCase().indexOf(cm_selectedProjectKey.toLowerCase()+ "-");
				if( lastIndexOf > -1){
					var orgininalName =  attendees[userIndex];
					attendees[userIndex] = orgininalName.substring(attendees[userIndex].indexOf("-")+1); 
					console.log(attendees[userIndex]);
				}
				
			}
		}
		AJS.$("#" + fieldId).val(attendees);
	}
	
	
	function doSubmit(){
		
		if (cm_selectedProjectKey == 'HLF' || cm_selectedProjectKey == 'WHIPTAIL' || cm_selectedProjectKey == 'ICUA' || cm_selectedProjectKey == 'STHC'|| cm_selectedProjectKey == 'KERN' ){
			//var issueType= AJS.$("#type-val").text();
			//console.log(subTaskType);
			if (doValidation()){
				AJS.$("#issue-edit").submit();
				
			}
		}
	
	}
	
	function doCaseSubmit(type){
		console.log("Form TYPE===== "+type);
	  if (doCaseFormValidation()){	
		if (type==1){
			AJS.$("#issue-create").submit();
		}
		else if (type==0){
				AJS.$("#issue-edit").submit();
		}
	  }
	}
	
	
	
	function addHLFValidationOnFields(issueType_Sub){
		
		console.log("Hello there is a mistake");
		
		if (issueType_Sub =="Injury / Illness" || issueType_Sub == "Near Miss"){
			AJS.$("#summary").prev().text("Type");
			AJS.$("#summary").prop("readonly","readonly");
			AJS.$("#customfield_12805").parent().prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#customfield_14301").prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#customfield_13570").prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#customfield_14308").prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#cf-customfield_14310").parent().prev().append('<span class="aui-icon icon-required">Required</span>');
			//AJS.$("#cf-customfield_14310").prop("checked", true);
			AJS.$("#cf-customfield_14311").parent().prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#cf-customfield_14312").parent().prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#cf-customfield_14313").parent().prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("textarea#customfield_14314").prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("textarea#customfield_14315").prev().append('<span class="aui-icon icon-required">Required</span>');
			if (issueType_Sub != "Near Miss"){
				AJS.$("#customfield_14000-1").parent().prev().append('<span class="aui-icon icon-required">Required</span>');
			}
			AJS.$("#customfield_15101-1").parent().prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#customfield_15100-1").parent().prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#customfield_10229").prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#customfield_10230").prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#customfield_10231").prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#customfield_10232").prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#customfield_10233").prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#customfield_10234").prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#customfield_14307").prev().append('<span class="aui-icon icon-required">Required</span>');
		}
		else if (issueType_Sub == "Spill / Release"){
			AJS.$("#summary").prev().text("Type");
			AJS.$("#summary").prop("readonly","readonly");
			AJS.$("#customfield_15102").prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#customfield_15105").prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#customfield_15109").prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#customfield_15110").prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#customfield_15101-1").parent().prev().append('<span class="aui-icon icon-required">Required</span>');
			AJS.$("#customfield_15100-1").parent().prev().append('<span class="aui-icon icon-required">Required</span>');
		}	
		else if (subtaskType == "Property Damage"){
			AJS.$("#summary").prev().text("Type");
			AJS.$("#summary").prop("readonly","readonly");
			AJS.$("#customfield_15201").prev().append('<span class="aui-icon icon-required">Required</span>');
		}
	if (cm_selectedProjectKey == "HLF" || cm_selectedProjectKey =="WHIPTAIL" || cm_selectedProjectKey == "ICUA" || cm_selectedProjectKey == "STHC"|| cm_selectedProjectKey == 'KERN'){		
		AJS.$("input[name='Update']").prop("type", "button");
			AJS.$("#issue-edit-submit").click(function(){
				doSubmit();
				
			});
			
	}
	}
		
		
	function addCaseValidation(formType){
			
			AJS.$("#customfield_14351").prev().append('<span class="aui-icon icon-required">Required</span>');
			if (cm_selectedProjectKey == 'HLF' || cm_selectedProjectKey=='WHIPTAIL' || cm_selectedProjectKey == 'ICUA' || cm_selectedProjectKey == 'STHC'|| cm_selectedProjectKey == 'KERN'){
				AJS.$("#customfield_11600").prev().append('<span class="aui-icon icon-required">Required</span>');
				AJS.$("#assignee").prev().prev().append('<span class="aui-icon icon-required">Required</span>');
				AJS.$("#single_assignee").parent().prev().append('<span class="aui-icon icon-required">Required</span>');
			}
			if (formType==1){
				AJS.$("input[name='Create']").prop("type", "button");
				AJS.$("#issue-create-submit").click(function(){
					doCaseSubmit(1);
					
				});
			}
			
		else if (formType==0){
					
			AJS.$("input[name='Update']").prop("type", "button");
				AJS.$("#issue-edit-submit").click(function(){
					doCaseSubmit(0);
					
				});		
			
		}		
				
	}	
	
	function doCaseFormValidation(){
		var errorArr = [];
		console.log("In Case Vallidations");
		var inputs = setCaseValidation();
		for (var i = 0;i < inputs.length;i++){
			var fieldName = inputs[i];
			console.log("Val == "+AJS.$("#"+fieldName).val());
			if (AJS.$("#"+fieldName).val()==""){
				//Don't put error if it is group assigne
				if(fieldName == 'assignee' && !AJS.$("#single_assignee").is(':checked')){
					continue;
				}
				
				var label = "";
				errorArr.push(fieldName);
				label = AJS.$("#"+fieldName).parent().find("label").text();
				label = label.replace("Required","");
				AJS.$("#divErr"+fieldName).remove();
				AJS.$("#"+fieldName).parent().append("<div id='divErr"+fieldName+"' class='error'>'"+label+"' is required. </div>");
			}
			else{
				errorArr.splice(i,1);
				AJS.$("#divErr"+fieldName).remove();
			}
			
			if (fieldName == 'assignee'){
				if (AJS.$("#"+fieldName).val()=="-1" && AJS.$("#single_assignee").is(':checked')){
					var label = "";
					errorArr.push(fieldName);
					label = AJS.$("#"+fieldName).parent().find("label").text();
					label = label.replace("Required","");
					AJS.$("#divErr"+fieldName).remove();
					AJS.$("#"+fieldName).parent().append("<div id='divErr"+fieldName+"' class='error'>'"+label+"' is required. </div>");
				}
				else{
					errorArr.splice(i,1);
					AJS.$("#divErr"+fieldName).remove();
				}
			}
		}
		
		if (errorArr.length > 0){
			console.log("Case Va;idation Errorr... ");
		//AJS.$(window).scrollTop(AJS.$("#divErr"+lastEle).offset().top-100);
		return false;
	}
		
	else{
		return true;
	}
		
		
	}
	
	function setCaseValidation(){
		var inputs = [];
		inputs.push("summary");
		inputs.push("customfield_14351");
		if (cm_selectedProjectKey == 'HLF' || cm_selectedProjectKey=='WHIPTAIL' || cm_selectedProjectKey == 'ICUA' || cm_selectedProjectKey == 'STHC'|| cm_selectedProjectKey == 'KERN'){
			inputs.push("customfield_11600");
			inputs.push("assignee");
			inputs.push("customfield_12800");
		}
		
		return inputs;
	}
	
</script>
