package com.atlassian.jira.issue.attachment;

public class StoreAttachmentResult
{
    private final ResultType resultType;

    public StoreAttachmentResult(final ResultType resultType)
    {
        this.resultType = resultType;
    }

    public static StoreAttachmentResult created()
    {
        return new StoreAttachmentResult(ResultType.CREATED);
    }

    public static StoreAttachmentResult updated()
    {
        return new StoreAttachmentResult(ResultType.UPDATED);
    }

    public ResultType getResultType()
    {
        return resultType;
    }

    public enum ResultType
    {
        CREATED, UPDATED
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        final StoreAttachmentResult that = (StoreAttachmentResult) o;

        if (resultType != that.resultType) { return false; }

        return true;
    }

    @Override
    public int hashCode()
    {
        return resultType.hashCode();
    }
}
