package com.atlassian.jira.util;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import com.atlassian.util.concurrent.Promise;

/**
 * @since v6.4
 */
public interface ExecutorServiceWrapper
{
    <O> Promise<O> submit(Callable<O> job);

    boolean awaitTermination();

    boolean awaitTermination(long timeout, TimeUnit unit);

    boolean isTerminated();
}
