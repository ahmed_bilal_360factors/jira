package com.atlassian.jira.issue.attachment;

import java.io.InputStream;

/**
 * @since v6.4
 */
public interface AttachmentGetData
{
    long getSize();

    InputStream getInputStream();
}
