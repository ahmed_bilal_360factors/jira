package com.atlassian.jira.issue.attachment;

import java.io.InputStream;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Bean used to store attachment in attachment stores.
 *
 * @since v6.4
 */
public class StoreAttachmentBean
{

    private final long id;
    private final Long size;
    private final String originalProjectKey;
    private final String issueKey;
    private final String fileName;
    private final InputStream stream;

    /**
     * Constructs bean.
     * @param id id of attachment to store
     * @param size size of content to store
     * @param originalProjectKey originalProjectKey of issue which this attachment should be linked to
     * @param issueKey issueKey of issue which this attachment should be linked to
     * @param fileName fileName of attachment
     * @param inputStream inputStream with attachment content
     */
    @ParametersAreNonnullByDefault
    private StoreAttachmentBean(final long id, final Long size, final String originalProjectKey, final String issueKey,
            final String fileName, final InputStream inputStream)
    {
        this.id = id;
        this.size = size;
        this.originalProjectKey = originalProjectKey;
        this.issueKey = issueKey;
        this.fileName = fileName;
        this.stream = inputStream;
    }

    public Long getSize()
    {
        return size;
    }

    public String getOriginalProjectKey()
    {
        return originalProjectKey;
    }

    public String getIssueKey()
    {
        return issueKey;
    }

    public long getId()
    {
        return id;
    }

    public String getFileName()
    {
        return fileName;
    }

    public AttachmentKey getAttachmentKey()
    {
        return new AttachmentKey(originalProjectKey, issueKey, id, fileName);
    }

    public InputStream getStream()
    {
        return stream;
    }

    @ParametersAreNonnullByDefault
    public static class Builder
    {
        private long id;
        private Long size;
        private String originalProjectKey;
        private String issueKey;
        private String fileName;
        private final InputStream inputStream;

        public Builder(final InputStream inputStream)
        {
            this.inputStream = inputStream;
        }

        public Builder withId(final long id)
        {
            this.id = id;
            return this;
        }

        public Builder withIssueKey(final String issueKey)
        {
            this.issueKey = issueKey;
            return this;
        }

        public Builder withOriginalProjectKey(final String originalProjectKey)
        {
            this.originalProjectKey = originalProjectKey;
            return this;
        }

        public Builder withSize(final long size)
        {
            this.size = size;
            return this;
        }

        public Builder withFileName(final String fileName)
        {
            this.fileName = fileName;
            return this;
        }

        public Builder withKey(final AttachmentKey destinationKey)
        {
            this.originalProjectKey = destinationKey.getProjectKey();
            this.issueKey = destinationKey.getIssueKey();
            this.id = destinationKey.getAttachmentId();
            this.fileName = destinationKey.getAttachmentFilename();
            return this;
        }

        public StoreAttachmentBean build()
        {
            return new StoreAttachmentBean(id, size, originalProjectKey, issueKey, fileName, inputStream);
        }

    }
}
