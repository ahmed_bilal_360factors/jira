package com.atlassian.jira.issue.attachment;

import java.io.InputStream;

import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.fugue.Unit;
import com.atlassian.util.concurrent.Function;
import com.atlassian.util.concurrent.Promise;

/**
 * Stream based operations on attachments This interface contains subset of methods from
 * {@link com.atlassian.jira.issue.attachment.AttachmentStore} and is dedicated to replace it.
 *
 * @since v6.4
 */
@ParametersAreNonnullByDefault
public interface StreamAttachmentStore extends AttachmentHealth
{
    /**
     * Store attachment data for a given attachment.
     *
     * @param storeAttachmentBean attachment metadata, used to determine the logical key under which to store the
     * attachment data
     * @return A promise of an attachment that performs the 'put' operation once the promise is claimed. The promise
     * will contain an {@link com.atlassian.jira.issue.attachment.AttachmentRuntimeException} in case of error.
     */
    Promise<StoreAttachmentResult> putAttachment(StoreAttachmentBean storeAttachmentBean);

    /**
     * Retrieve data for a given attachment.
     *
     * @param attachmentKey The key of attachment used to identify attachment data
     * @param inputStreamProcessor Function that processes the attachment data.
     * @param <A> The class that the inputStreamProcessor returns when run.
     * @return A promise of an object that represented the processed attachment data (i.e. from running the
     * inputStreamProcessor over the attachment data).  The promise will contain an {@link
     * com.atlassian.jira.issue.attachment.AttachmentRuntimeException} in case of error.
     */
    <A> Promise<A> getAttachment(AttachmentKey attachmentKey, Function<InputStream, A> inputStreamProcessor);

    /**
     * Retrieve data for a given attachment. Provides more information to consumer than (@link StreamAttachmentStore#getAttachment)
     *
     * @param attachmentKey The key of attachment used to identify attachment data
     * @param attachmentGetDataProcessor Function that processes the attachment data.
     * @param <A> The class that the attachmentGetDataProcessor returns when run.
     * @return A promise of an object that represented the processed attachment data (i.e. from running the
     * inputStreamProcessor over the attachment data).  The promise will contain an {@link
     * com.atlassian.jira.issue.attachment.AttachmentRuntimeException} in case of error.
     */
    <A> Promise<A> getAttachmentData(AttachmentKey attachmentKey, Function<AttachmentGetData, A> attachmentGetDataProcessor);



    /**
     * Move attachment identified by {@code oldAttachmentKey} to be identified by {@code newAttachmentKey} and will be not
     * reachable under {@code oldAttachmentKey} anymore.
     * @param oldAttachmentKey the old AttachmentKey
     * @param newAttachmentKey the new AttachmentKey
     */
    Promise<Unit> moveAttachment(AttachmentKey oldAttachmentKey, AttachmentKey newAttachmentKey);

    /**
     * Copies attachment identified by {@code oldAttachmentKey} to be identified also by {@code newAttachmentKey}.
     * @param sourceAttachmentKey attachmentKey of existing attachment which should be copied
     * @param newAttachmentKey attachmentKey under which copied
     */
    Promise<Unit> copyAttachment(AttachmentKey sourceAttachmentKey, AttachmentKey newAttachmentKey);

    /**
     * Delete the attachment identified by provided attachmentKey.
     *
     * @return a promise that will contain an AttachmentCleanupException in case of error.
     */
    Promise<Unit> deleteAttachment(AttachmentKey attachmentKey);

    /**
     * Creates temporaryAttachment in store which can be later moved to attachment via {@link #moveTemporaryToAttachment(TemporaryAttachmentId, AttachmentKey)}
     *
     * @param inputStream stream to temporary attachment data
     * @param size the size of provided stream
     * @return returns promise to temporaryAttachmentId
     */
    Promise<TemporaryAttachmentId> putTemporaryAttachment(InputStream inputStream, long size);

    /**
     * Moving temporary attachment created by {@link #putTemporaryAttachment(java.io.InputStream, long)} to real attachment.
     * @param temporaryAttachmentId id of temporary attachment, returned by {@link #putTemporaryAttachment(java.io.InputStream, long)}
     * @param destinationKey destination key of under which attachment will be identified
     */
    Promise<Unit> moveTemporaryToAttachment(TemporaryAttachmentId temporaryAttachmentId, AttachmentKey destinationKey);

    /**
     * Deletes temporary attachment created previously by {@link #putTemporaryAttachment(java.io.InputStream, long)}.
     * @param temporaryAttachmentId id of temporary attachment
     */
    Promise<Unit> deleteTemporaryAttachment(TemporaryAttachmentId temporaryAttachmentId);

    /**
     * Checks if attachment exists.
     *
     * @param attachmentKey key of attachment to check for existence
     * @return a {@link com.atlassian.util.concurrent.Promise} to boolean result, true if attachment exists and false
     * otherwise
     */
    Promise<Boolean> exists(AttachmentKey attachmentKey);

    interface Factory
    {

        enum StoreType
        {
            PRIMARY, SECONDARY
        }

        /**
         * Creates the store for this factory
         * @param storeType we pass if we are requesting a PRIMARY or SECONDARY for the plugin to configure it safely
         * @return a new attachment store for this new configuration
         */
        StreamAttachmentStore createStoreFor(final StoreType storeType);

        /**
         * We ask the factory if the store is ready for this configuration
         * We use this to enable/disable the configuration in the EditAttachmentsSettings page
         * @param storeType the store type
         * @return true if the user configured properly the plugin or not
         */
        boolean isConfiguredFor(final StoreType storeType);
    }
}
