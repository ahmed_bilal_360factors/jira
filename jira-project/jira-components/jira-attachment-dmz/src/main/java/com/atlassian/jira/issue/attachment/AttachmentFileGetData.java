package com.atlassian.jira.issue.attachment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.concurrent.locks.ReentrantLock;

import javax.annotation.concurrent.ThreadSafe;

import com.atlassian.jira.util.RuntimeIOException;

import com.google.common.base.Preconditions;

import org.apache.commons.io.IOUtils;

/**
 * Represents attachment data based on files system.
 * The method {@link #getInputStream()} can be called only once for each instance of this object.
 * Method {@link #close()} can be called multiple times.
 * This class is thread safe, that means that getInputStream and close can be called from two threads, and when multiple
 * threads try to call getInputStream only one will succeed and other will receive {@link java.lang.IllegalStateException}
 * @since v6.4
 *
 */
@ThreadSafe
public class AttachmentFileGetData implements AttachmentGetData
{
    final File file;
    InputStream inputStream;
    boolean closed = false;
    final ReentrantLock lock = new ReentrantLock();

    public AttachmentFileGetData(final File file)
    {
        this.file = Preconditions.checkNotNull(file, "file");
    }

    /**
     * Size of file represented by this object, or 0 if the file does not exist or is directory
     * @return file object represented by this object.
     */
    public File getFile()
    {
        return file;
    }

    /**
     * close the underlying input stream if this was and is open. NOP in other cases.
     */
    public void close()
    {

        InputStream prevValue;

        lock.lock();
        try
        {
            prevValue = inputStream;
            inputStream = null;
            closed = true;
        }
        finally
        {
            lock.unlock();
        }
        if (prevValue != null)
        {
            IOUtils.closeQuietly(prevValue);
        }
    }

    @Override
    public long getSize()
    {
        return file.length();
    }

    /**
     * Opens input stream for the file handled by this object. One can call this method only once for instance of this object.
     * To close the stream use method {@link #close()} of this class. Closing the stream directly in not an error in this case method close
     * will be NOP.
     * @throws java.7lang.IllegalStateException if this method is called twice or close was called before this method.
     * @throws com.atlassian.jira.util.RuntimeIOException if this object points to not existing file
     */
    @Override
    public InputStream getInputStream()
    {
        lock.lock();
        try
        {
            if (inputStream != null ){
                throw new IllegalStateException("Input stream already obtained");
            }
            if (closed) {
                throw new IllegalStateException("This object was already closed");
            }
            try
            {
                inputStream = new FileInputStream(file);
            }
            catch (FileNotFoundException e)
            {
                throw new RuntimeIOException(e);
            }
            return inputStream;
        }
        finally
        {
            lock.unlock();
        }
    }
}
