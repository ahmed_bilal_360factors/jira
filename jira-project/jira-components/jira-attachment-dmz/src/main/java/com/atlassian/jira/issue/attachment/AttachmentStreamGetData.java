package com.atlassian.jira.issue.attachment;

import java.io.InputStream;

/**
 * @since v6.4
 */
public class AttachmentStreamGetData implements AttachmentGetData
{
    private final InputStream inputStream;
    private final long size;

    public AttachmentStreamGetData(final InputStream inputStream, final long size)
    {
        this.inputStream = inputStream;
        this.size = size;
    }

    @Override
    public InputStream getInputStream()
    {
        return inputStream;
    }

    @Override
    public long getSize()
    {
        return size;
    }
}
