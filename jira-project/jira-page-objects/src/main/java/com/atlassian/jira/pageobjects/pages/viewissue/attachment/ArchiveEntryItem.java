package com.atlassian.jira.pageobjects.pages.viewissue.attachment;

import com.atlassian.jira.pageobjects.elements.ImageElement;
import com.atlassian.pageobjects.elements.PageElement;

import org.openqa.selenium.By;

/**
 * Archive entry in Attachment row in "Attachment" section of "View Issue" page
 *
 * @since v6.4
 */
public final class ArchiveEntryItem
{
    private final PageElement item;

    ArchiveEntryItem(final PageElement item)
    {
        this.item = item;
    }

    public String getTitle()
    {
        return getFileLink().getLabel();
    }

    public ImageElement getIcon()
    {
        final PageElement imageElement = item.find(By.cssSelector(".attachment-thumb img"));
        return new ImageElement(imageElement);
    }

    public String getSize()
    {
        return item.find(By.className("attachment-size")).getText();
    }

    public FileLinkElement getFileLink()
    {
        final PageElement element = item.find(By.className("file-path"));
        return FileLinkElement.createFromTheSameElement(element);
    }

    public int getIndex()
    {
        return Integer.valueOf(item.getAttribute("data-entry-index"));
    }
}
