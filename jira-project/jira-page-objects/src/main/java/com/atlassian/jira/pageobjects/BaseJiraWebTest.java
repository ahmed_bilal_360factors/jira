package com.atlassian.jira.pageobjects;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * <p/>
 * Lightweight base class mainly containing annotations common for all tests.
 * <p/>
 * <p/>
 * DO NOT put any utility methods here. Use page objects framework for that. In fact, do not put anything here without
 * permission :P
 * <p/>
 * <p/>
 *
 * @since v4.4
 */
public abstract class BaseJiraWebTest extends JiraWebDriverTest
{
    @ClassRule
    public static final TestRule jiraWebTestClassRules = new TestRule()
    {
        @Override
        public Statement apply(final Statement base, final Description description)
        {
            return getBaseClassRule().apply(base, description);
        }
    };

    @Rule
    public final TestRule webTestRule = getBaseRule();
}
