package com.atlassian.jira.pageobjects.pages.viewissue.attachment;

import java.util.List;

import javax.annotation.Nullable;

import com.atlassian.jira.pageobjects.dialogs.DeleteAttachmentDialog;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementActions;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;

import com.google.common.base.Function;
import com.google.common.base.Supplier;

import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.transform;

/**
 * An attachment row in "Attachment" section of "View Issue" page.
 *
 * @since v6.4
 */
public final class AttachmentRow
{
    private final PageElement row;
    private final PageBinder pageBinder;
    private final PageElementActions actions;
    private final Timeouts timeouts;

    AttachmentRow(final PageElement row, final PageBinder pageBinder, final PageElementActions actions, Timeouts timeouts)
    {
        this.row = row;
        this.pageBinder = pageBinder;
        this.actions = actions;
        this.timeouts = timeouts;
    }

    public long getAttachmentId()
    {
        return Long.valueOf(row.getAttribute("data-attachment-id"));
    }

    public String getTitle()
    {
        return getFileLink().getLabel();
    }

    public FileLinkElement getFileLink()
    {
        final PageElement parent = isZipFile() ? findVisiblePart() : row;
        final PageElement element = parent.find(By.cssSelector(".attachment-title a"));
        return FileLinkElement.createFromTheSameElement(element);
    }

    public DeleteAttachmentDialog delete()
    {
        Poller.waitUntilTrue(Conditions.forSupplier(timeouts.timeoutFor(TimeoutType.UI_ACTION), new Supplier<Boolean>()
        {
            @Override
            public Boolean get()
            {
                actions.moveToElement(row).perform();
                return row.find(By.className("attachment-delete")).isVisible();
            }
        }));

        row.find(By.className("attachment-delete")).find(By.tagName("a")).click();

        return pageBinder.bind(DeleteAttachmentDialog.class);
    }

    private PageElement findVisiblePart()
    {
        return row.find(By.className(isExpanded() ? "verbose" : "concise"));
    }

    private boolean isExpanded()
    {
        return row.hasClass("expanded");
    }

    private boolean isZipFile()
    {
        return row.find(By.className("zip-contents")).isPresent();
    }

    public List<ArchiveEntryItem> expandEntries()
    {
        clickExpanderIfNecessary();
        final PageElement entriesContainer = row.find(By.className("zip-contents"));
        final PageElement loadedEntriesContainer = entriesContainer.find(By.className("archive-expanded"));
        waitUntilVisible(loadedEntriesContainer);
        final List<PageElement> entries = entriesContainer.findAll(By.className("archive-entry"));
        return transform(
                entries,
                new Function<PageElement, ArchiveEntryItem>()
                {
                    @Override
                    public ArchiveEntryItem apply(@Nullable final PageElement input)
                    {
                        checkNotNull(input, "input");
                        return new ArchiveEntryItem(input);
                    }
                }
        );
    }

    private void clickExpanderIfNecessary()
    {
        final PageElement expander = row.find(By.className("aui-iconfont-collapsed"));
        if (expander.isVisible())
        {
            expander.click();
        }
    }

    public boolean containsExpansionWarning()
    {
        return row
                .find(By.className("aui-message-warning"))
                .getText()
                .contains("This archive is either corrupt, empty or not an archive at all");
    }

    private void waitUntilVisible(final PageElement element)
    {
        waitUntilTrue(element.timed().isVisible());
    }


    public ArchiveFooter getFooter()
    {
        final PageElement footer = row.find(By.className("zip-contents-trailer"));
        return new ArchiveFooter(footer);
    }
}
