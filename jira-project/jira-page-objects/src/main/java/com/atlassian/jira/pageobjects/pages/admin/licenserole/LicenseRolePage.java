package com.atlassian.jira.pageobjects.pages.admin.licenserole;

import com.atlassian.jira.pageobjects.components.fields.SingleSelect;
import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.TimedElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;

import java.util.Collections;
import java.util.List;
import javax.annotation.Nullable;
import javax.inject.Inject;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;
import static org.junit.Assert.assertTrue;

public class LicenseRolePage extends AbstractJiraPage
{
    public static final String TRACE_KEY = "role.put.finished";
    @Inject
    private TraceContext context;

    private final Function<PageElement, Role> TOROLE = new Function<PageElement, Role>()
    {
        @Override
        public Role apply(@Nullable final PageElement input)
        {
            return new Role(input);
        }
    };

    private final Function<PageElement, RoleGroup> TOGROUP = new Function<PageElement, RoleGroup>()
    {
        @Override
        public RoleGroup apply(@Nullable final PageElement input)
        {
            return new RoleGroup(input);
        }
    };

    private static final Function<PageElement, String> TOSTRING = new Function<PageElement, String>()
    {
        @Override
        public String apply(final PageElement input)
        {
            return StringUtils.stripToNull(input.getText());
        }
    };

    @ElementBy (cssSelector = "#license-roles")
    private PageElement root;

    @Override
    public TimedCondition isAt()
    {
        //Wait until the table finishes loading.
        final TimedElement element = root.timed();
        return Conditions.and(element.isPresent(), Conditions.not(element.hasClass("loading")));
    }

    @Override
    public String getUrl()
    {
        return "/secure/admin/LicenseRoles.jspa";
    }

    public List<Role> roles()
    {
        return copyOf(transform(root.findAll(By.cssSelector("form")), TOROLE));
    }

    public Role role(String id)
    {
        final PageElement element = root.find(By.cssSelector(String.format("form[data-role='%s']", id)));
        if (!element.isPresent())
        {
            throw new IllegalArgumentException(String.format("Can't find role with id '%s'.", id));
        }
        return new Role(element);
    }

    private void executeAndAwait(Runnable function)
    {
        final Tracer checkpoint = context.checkpoint();
        function.run();
        context.condition(checkpoint, TRACE_KEY);
    }

    public class Role
    {
        private final PageElement form;
        private final PageElement name;

        private Role(PageElement form)
        {
            if (!form.isPresent())
            {
                throw new IllegalArgumentException("row is not on the page.");
            }

            this.form = form;
            this.name = form.find(By.tagName("h2"));
        }

        public List<RoleGroup> getGroups()
        {
            final List<PageElement> all = form.findAll(By.cssSelector("tbody tr"));
            if (all.isEmpty())
            {
                return Collections.emptyList();
            }
            else if (all.size() == 1)
            {
                final PageElement row = all.get(0);
                if (row.find(By.className("license-role-empty")).isPresent())
                {
                    return Collections.emptyList();
                }
                return copyOf(transform(all, TOGROUP));
            }
            else
            {
                return copyOf(transform(all, TOGROUP));
            }
        }

        public String getName()
        {
            return TOSTRING.apply(name);
        }

        public Role addGroup(final String group)
        {
            executeAndAwait(new Runnable()
            {
                @Override
                public void run()
                {
                    SingleSelect select = pageBinder.bind(SingleSelect.class, form.find(By.className("license-role-selector-container")));
                    select.select(group);
                }
            });
            return this;
        }

        public RoleGroup getGroup(final String group)
        {
            return Iterables.find(getGroups(), new Predicate<RoleGroup>()
            {
                @Override
                public boolean apply(final RoleGroup input)
                {
                    return group.equals(input.getName());
                }
            }, null);
        }
    }

    public class RoleGroup
    {
        private final String name;
        private final PageElement row;

        private RoleGroup(PageElement row)
        {
            this.row = row;
            this.name = TOSTRING.apply(row.find(By.className("license-role-name")));
        }

        public String getName()
        {
            return name;
        }

        public boolean isPrimary()
        {
            return getPrimaryInput().hasAttribute("checked", Boolean.TRUE.toString());
        }

        public void setPrimary()
        {
            final PageElement input = getPrimaryInput();
            executeAndAwait(new Runnable()
            {
                @Override
                public void run()
                {
                    input.click();
                }
            });
        }

        private PageElement getPrimaryInput()
        {
            final PageElement element = this.row.find(By.cssSelector(".license-role-primary input"));
            assertTrue("Primary option should be present.", element.isPresent());
            return element;
        }

        public boolean canRemove()
        {
            //You can remove if its a link.
            return getRemove().getTagName().equals("a");
        }

        public void remove()
        {
            assertTrue("Remove group is disabled.", canRemove());
            final PageElement remove = getRemove();

            executeAndAwait(new Runnable()
            {
                @Override
                public void run()
                {
                    remove.click();
                }
            });
        }

        private PageElement getRemove()
        {
            final PageElement element = this.row.find(By.className("license-role-remove"));
            assertTrue("Remove option should be present.", element.isPresent());
            return element;
        }
    }
}
