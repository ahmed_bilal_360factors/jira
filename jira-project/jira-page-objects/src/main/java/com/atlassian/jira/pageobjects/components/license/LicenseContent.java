package com.atlassian.jira.pageobjects.components.license;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

import org.openqa.selenium.By;

/**
 * @since v6.4
 */
public abstract class LicenseContent
{
    @Inject
    private TraceContext context;

    @Nonnull
    protected abstract PageElement getContent();

    protected abstract PageElement getRemindMeLaterLink();

    protected abstract PageElement getRemindMeNeverLink();

    protected abstract PageElement getMacLink();

    public boolean isPresent()
    {
       return isPresentAndVisible(getContent());
    }

    public String getMacUrl()
    {
        final PageElement pageElement = getMacLink();
        if (isPresentAndVisible(pageElement))
        {
            return pageElement.getAttribute("href");
        }
        return null;
    }

    public boolean canRemindLater()
    {
        return isPresent(getRemindMeLaterLink());
    }


    public void remindLater()
    {
        if (!canRemindLater())
        {
            throw new IllegalStateException("Unable to remind later.");
        }

        clickElementAndWaitForBanner(getRemindMeLaterLink(), "license-later-done");
    }

    public boolean canRemindNever()
    {
        return isPresent(getRemindMeNeverLink());
    }

    public void remindNever()
    {
        if (!canRemindNever())
        {
            throw new IllegalStateException("Unable to remind never.");
        }

        clickElementAndWaitForBanner(getRemindMeNeverLink(), "license-never-done");
    }

    public int days()
    {
        if (getContent().isPresent())
        {
            return Integer.parseInt(getContent().getAttribute("data-days"));
        }
        else
        {
            return Integer.MIN_VALUE;
        }
    }

    public boolean isSubscription()
    {
        return getContent().isPresent() && Boolean.parseBoolean(getContent().getAttribute("data-subscription"));
    }

    protected void clickElementAndWaitForBanner(final PageElement element, final String traceKey)
    {
        final Tracer checkpoint = context.checkpoint();

        element.click();

        //Wait until the REST call and animation is done.
        Poller.waitUntilTrue(context.condition(checkpoint, traceKey));
        //Wait until the animation is done.
        Poller.waitUntilFalse(getContent().timed().isVisible());
    }



    protected static boolean isPresentAndVisible(final PageElement element)
    {
        return element.isPresent() && element.isVisible();
    }

    protected static boolean isPresent(final PageElement element)
    {
        return element.isPresent();
    }
}
