package com.atlassian.jira.pageobjects.model;

import javax.ws.rs.core.MediaType;

import com.atlassian.uri.Uri;

/**
 * @since v6.4
 */
public class FileDownloadInfoDeserializer
{
    private static final int COLON_NOTATION_PART_COUNT = 3;
    private static final String SEPARATOR = ":";

    public FileDownloadInfo deserializeColonNotation(final String colonNotation)
    {
        final String[] parts = colonNotation.split(SEPARATOR, COLON_NOTATION_PART_COUNT);
        if (parts.length != COLON_NOTATION_PART_COUNT)
        {
            final String message = String.format(
                    "%s should contain %d parts separated by '%s'",
                    colonNotation,
                    COLON_NOTATION_PART_COUNT,
                    SEPARATOR
            );
            throw new IllegalArgumentException(message);
        }
        final MediaType mediaType = MediaType.valueOf(parts[0]);
        final String fileName = parts[1];
        final Uri downloadUri = Uri.parse(parts[2]);
        return new FileDownloadInfo(mediaType, fileName, downloadUri);
    }
}
