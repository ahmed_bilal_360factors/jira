package com.atlassian.jira.pageobjects.pages.viewissue.attachment;

import com.atlassian.pageobjects.elements.PageElement;

import com.google.common.base.Optional;

import org.openqa.selenium.By;

/**
 * Archive footer at the bottom of the attachment row in "Attachment" section of "View Issue" page
 *
 * @since v6.4
 */
public class ArchiveFooter
{
    private final PageElement element;

    ArchiveFooter(final PageElement element) {this.element = element;}

    public Optional<String> getLimitInfo()
    {
        final PageElement limitInfo = element.find(By.className("limit-info"));
        return limitInfo.isPresent() ? Optional.of(limitInfo.getText()) : Optional.<String>absent();
    }

    public FileLinkElement getDownloadArchiveLink()
    {
        final PageElement link = element.find(By.tagName("a"));
        return FileLinkElement.createFromTheSameElement(link);
    }
}
