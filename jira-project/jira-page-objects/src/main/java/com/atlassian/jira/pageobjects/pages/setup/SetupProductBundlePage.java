package com.atlassian.jira.pageobjects.pages.setup;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import org.openqa.selenium.By;

/**
 * @since v6.4
 */
public class SetupProductBundlePage extends AbstractJiraPage
{
    private static final String URI = "/secure/SetupProductBundle!default.jspa";

    @ElementBy (id = "jira-setup-product-bundle-submit")
    private PageElement submitButton;

    @ElementBy (id = "jira-setup-product-bundle")
    private PageElement form;

    @Override
    public String getUrl()
    {
        return URI;
    }

    @Override
    public TimedCondition isAt()
    {
        return form.timed().isPresent();
    }

    public LicenseSetupPage submit()
    {
        submitButton.click();
        return pageBinder.bind(LicenseSetupPage.class);
    }

    public SetupAccountPage submitToInstantPath()
    {
        submitButton.click();
        return pageBinder.bind(SetupAccountPage.class);
    }

    public enum BundleSelection
    {
        TRACKING,
        DEVELOPMENT,
        SERVICEDESK
    }

    public SetupProductBundlePage selectBundle(BundleSelection bundle)
    {
        for(PageElement choice : form.findAll(By.cssSelector("div.jira-setup-choice-box")))
        {
            if (choice.getAttribute("data-choice-value").equals(bundle.name()))
            {
                choice.click();
                break;
            }
        }
        return this;
    }
}
