package com.atlassian.jira.pageobjects.elements;

import com.atlassian.jira.pageobjects.model.FileDownloadInfo;
import com.atlassian.jira.pageobjects.model.FileDownloadInfoDeserializer;
import com.atlassian.pageobjects.elements.PageElement;

/**
 * A downloadable element.
 *
 * @since v6.4
 */
public class DownloadableElement
{
    private final FileDownloadInfoDeserializer downloadInfoDeserializer = new FileDownloadInfoDeserializer();
    private final PageElement element;

    public DownloadableElement(final PageElement element)
    {
        this.element = element;
    }

    public FileDownloadInfo getDownloadInfo()
    {
        final String colonNotation = element.getAttribute("data-downloadurl");
        return downloadInfoDeserializer.deserializeColonNotation(colonNotation);
    }

    public boolean isDraggable()
    {
        return element.getAttribute("draggable").equals("true");
    }
}
