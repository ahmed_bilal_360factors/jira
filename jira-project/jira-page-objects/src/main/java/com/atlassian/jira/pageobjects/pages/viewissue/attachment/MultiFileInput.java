package com.atlassian.jira.pageobjects.pages.viewissue.attachment;


import java.io.File;
import java.util.List;

import com.atlassian.pageobjects.elements.PageElement;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Multiple files input.
 *
 * @since v6.4
 */
public final class MultiFileInput
{
    private final PageElement fileInput;
    private final List<File> uploadedFiles = Lists.newArrayList();

    public MultiFileInput(final PageElement fileInput)
    {
        this.fileInput = fileInput;
    }

    public void putFile(final File file)
    {
        waitUntilTrue(fileInput.timed().isPresent());
        hackVisibility();
        uploadedFiles.add(file);
        fileInput.type(file.getAbsolutePath());
    }

    /**
     * @see <a href="https://code.google.com/p/selenium/wiki/FrequentlyAskedQuestions#Q:_Does_WebDriver_support_file_uploads?">
     * Visibility hack</a>
     */
    private void hackVisibility()
    {
        if (!fileInput.isVisible())
        {
            final String visibility = "arguments[0].style.display = 'block'; "
                    + "arguments[0].style.visibility = 'visible'; "
                    + "arguments[0].style.width = '1px'; "
                    + "arguments[0].style.height = '1px'; "
                    + "arguments[0].style.opacity = 0.0001; ";
            final String nonIntrusiveness = "arguments[0].style.position = 'fixed'; "
                    + "arguments[0].style.padding = 0; "
                    + "arguments[0].style.border = 0; "
                    + "arguments[0].style.margin = 0; ";
            fileInput.javascript().execute(visibility + nonIntrusiveness);
        }
    }

    public List<File> getFiles()
    {
        return ImmutableList.copyOf(uploadedFiles);
    }
}
