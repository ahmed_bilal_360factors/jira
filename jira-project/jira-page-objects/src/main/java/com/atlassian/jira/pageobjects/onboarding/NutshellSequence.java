package com.atlassian.jira.pageobjects.onboarding;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import javax.inject.Inject;

public class NutshellSequence extends Sequence
{
    @ElementBy(className = "onboarding-nutshell")
    private PageElement content;

    @ElementBy(className = "next")
    private PageElement nextButton;

    @Inject
    private PageElementFinder pageElementFinder;

    private enum NutshellStep
    {
        PROJECTS,
        PROJECTS_ISSUES,
        ISSUE,
        ISSUE_SUMMARY,
        ISSUE_KEY,
        ISSUE_ASSIGNEE,
        ISSUE_STATUS,
        WORKFLOW,
        WORKFLOW_BUTTONS,
        WORKFLOW_CLICK,
        WORKFLOW_DONE,
        ALL_DONE
    }

    private NutshellStep currentStep = NutshellStep.PROJECTS;

    @Override
    protected TimedCondition isAt()
    {
        return content.timed().isVisible();
    }

    public void nextStep()
    {
        // 1. wait until dialog appears
        // 2. click "Next"
        // 3. increment counter OR mark sequence as done
        waitForInlineDialog();
        nextButton.click();
        if (currentStep.ordinal() < NutshellStep.ALL_DONE.ordinal())
        {
            currentStep = NutshellStep.values()[currentStep.ordinal() + 1];
        }
        else
        {
            currentStep = null; // Done
        }
    }

    public boolean isDone()
    {
        return currentStep == null;
    }

    private void waitForInlineDialog()
    {
        PageElement inlineDialog = pageElementFinder.find(By.id("inline-dialog-nutshellDialog"));
        Poller.waitUntilTrue("Inline dialog did not appear", inlineDialog.timed().isVisible());
    }
}
