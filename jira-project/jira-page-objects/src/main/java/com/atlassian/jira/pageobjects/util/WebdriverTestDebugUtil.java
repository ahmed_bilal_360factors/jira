package com.atlassian.jira.pageobjects.util;

import java.io.File;
import java.util.Date;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.TestEnvironment;
import com.atlassian.webdriver.debug.WebDriverDebug;
import org.junit.runner.Description;

/**
 * @since v6.4
 */
public class WebdriverTestDebugUtil
{
    private final JiraTestedProduct jira;
    private TestEnvironment testEnvironment;
    private Class<?> testClass;

    public WebdriverTestDebugUtil(final JiraTestedProduct jira, TestEnvironment testEnvironment, Class<?> testClass)
    {
        this.jira = jira;
        this.testEnvironment = testEnvironment;
        this.testClass = testClass;
    }

    public File takeScreenshot(final String name)
    {
        final WebDriverDebug debug = getWebDriverDebug();
        final String fileName = new Date().getTime() + "-" + name + ".png";
        final File destDirectory = new File(testEnvironment.artifactDirectory(), testClass.getName());
        final File destFile = new File(destDirectory, fileName);
        debug.takeScreenshotTo(destFile);
        System.out.println("Saved screenshot to: " + destFile.getAbsolutePath());
        return destFile;
    }

    private WebDriverDebug getWebDriverDebug()
    {
        return new WebDriverDebug(jira.getTester().getDriver());
    }
}
