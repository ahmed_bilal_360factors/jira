package com.atlassian.jira.pageobjects.onboarding;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public class WelcomeToJiraPage extends AbstractJiraPage
{
    private static final String URI = "/secure/WelcomeToJIRA.jspa";

    @ElementBy(id = "onboarding")
    private PageElement container;

    @Override
    public TimedCondition isAt()
    {
        return container.timed().isPresent();
    }

    @Override
    public String getUrl()
    {
        return URI;
    }
}
