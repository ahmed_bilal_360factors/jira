package com.atlassian.jira.pageobjects.components.license;


import javax.annotation.Nonnull;

import com.atlassian.jira.pageobjects.elements.AuiFlag;
import com.atlassian.pageobjects.elements.PageElement;

import org.openqa.selenium.By;

public class LicenseFlag extends LicenseContent
{
    private final AuiFlag flag;

    public LicenseFlag(AuiFlag auiFlag)
    {
        this.flag = auiFlag;
    }

    @Nonnull
    @Override
    protected PageElement getContent()
    {
        return flag.getFlagElement().find(By.id("license-flag-content"));
    }

    public void dismiss()
    {
        if (!flag.isCloseable())
        {
            throw new IllegalStateException("Unable to close flag.");
        }
        clickElementAndWaitForBanner(getCloseIcon(), "license-later-done");
    }

    private PageElement getCloseIcon()
    {
        return flag.getFlagElement().find(By.className("icon-close"));
    }

    @Override
    protected PageElement getRemindMeNeverLink()
    {
        return getContent().find(By.id("license-flag-never"));
    }

    @Override
    protected PageElement getRemindMeLaterLink()
    {
        return getContent().find(By.id("license-flag-later"));
    }

    @Override
    protected PageElement getMacLink()
    {
        return getContent().find(By.id("license-flag-my-link"));
    }
}
