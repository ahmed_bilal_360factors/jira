package com.atlassian.jira.webtest.webdriver.util;

import com.atlassian.jira.pageobjects.JiraTestedProduct;

/**
 * @since v6.4
 */
public class AUIDialogs
{
    public static final String CLOSE_ALL_JS = "AJS.$('.aui-inline-dialog').hide()";
    private final JiraTestedProduct jira;

    public AUIDialogs(final JiraTestedProduct jira)
    {
        this.jira = jira;
    }

    public void closeAll()
    {
        jira.getTester().getDriver().executeScript(CLOSE_ALL_JS);
    }
}
