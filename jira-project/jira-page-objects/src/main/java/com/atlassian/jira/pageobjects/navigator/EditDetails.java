package com.atlassian.jira.pageobjects.navigator;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.SelectElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

/**
 * Edit details for Bulk Edit.
 *
 * @since v5.1
 */
public class EditDetails extends AbstractJiraPage
{
    @ElementBy (id = "availableActionsTable")
    protected PageElement availableActionTable;

    @ElementBy (id = "cblabels")
    protected PageElement cblabels;

    @ElementBy (name = "changelabels")
    protected SelectElement changelabels;

    @ElementBy (id = "labels-textarea")
    protected PageElement labels;

    @ElementBy (id = "cbfixVersions")
    protected PageElement cbfixVersions;

    @ElementBy (name = "changefixVersions")
    protected SelectElement changefixVersions;

    @ElementBy (id = "fixVersions-textarea")
    protected PageElement versions;

    @ElementBy (id = "cbversions")
    protected PageElement cbaffectsVersions;

    @ElementBy (name = "changeversions")
    protected SelectElement changeversions;

    @ElementBy (id = "versions-textarea")
    protected PageElement affectsVersions;

    @ElementBy (id = "cbcomponents")
    protected PageElement cbcomponents;

    @ElementBy (name = "changecomponents")
    protected SelectElement changecomponents;

    @ElementBy (id = "components-textarea")
    protected PageElement components;

    @ElementBy (id = "next")
    protected PageElement next;

    @ElementBy (className = "error")
    protected PageElement errormessage;

    @ElementBy (id = "unavailableActionsTable")
    protected PageElement unavailableActions;

    @Override
    public TimedCondition isAt()
    {
        return availableActionTable.timed().isVisible();
    }

    @Override
    public String getUrl()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    public EditDetails typeLabels(String value)
    {
        if (!value.isEmpty())
        {
            labels.type(value).type(Keys.SPACE);
        }
        return this;
    }

    public EditDetails selectLabelsOption(String option)
    {
        changelabels.select(Options.value(option));
        return this;
    }

    public EditDetails markLabelsCheckbox()
    {
        cblabels.select();
        return this;
    }

    public EditDetails addLabels(String option, String label)
    {
        markLabelsCheckbox();
        selectLabelsOption(option);
        typeLabels(label);
        return this;
    }

    public EditDetails selectFixVersionsOption(String option)
    {
        changefixVersions.select(Options.value(option));
        return this;
    }

    public EditDetails addFixVersions(String option, String fixVersion)
    {
        cbfixVersions.select();
        selectFixVersionsOption(option);
        if (!fixVersion.isEmpty())
        {
            versions.type(fixVersion).type(Keys.RETURN);
        }
        return this;
    }

    public EditDetails selectComponentsOption(String option)
    {
        changecomponents.select(Options.value(option));
        return this;
    }

    public EditDetails addComponents(String option, String component)
    {
        cbcomponents.select();
        selectComponentsOption(option);
        if (!component.isEmpty())
        {
            components.type(component).type(Keys.RETURN);
        }
        return this;
    }

    public ConfirmationPage next()
    {
        next.click();
        return pageBinder.bind(ConfirmationPage.class);
    }

    public EditDetails expandUnavailableActions()
    {
        unavailableActions.click();
        return this;
    }

    public EditDetails nextWhenFailedValidation()
    {
        next.click();
        return this;
    }

    public String getValidationError()
    {
        if (errormessage.timed().isVisible().byDefaultTimeout())
        {
            return errormessage.find(By.tagName("p")).getText();
        }
        else return null;
    }

}
