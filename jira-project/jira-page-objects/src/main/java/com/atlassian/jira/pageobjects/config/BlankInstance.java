package com.atlassian.jira.pageobjects.config;

/**
 * @see com.atlassian.jira.pageobjects.config.ResetDataOnce
 * @see com.atlassian.jira.pageobjects.config.ResetData
 * @since v6.4
 */
public class BlankInstance
{
    public static final String PROJECT_KEY = "HSP";
}
