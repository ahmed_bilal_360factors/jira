package com.atlassian.jira.pageobjects.dialogs;

import javax.inject.Inject;

import com.atlassian.jira.pageobjects.elements.FormMessages;
import com.atlassian.jira.pageobjects.elements.LinkElement;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

/**
 * Dialog which presents messages to the user and allows him to proceed by acknowledging them.
 *
 * @since v6.4
 */
public final class AcknowledgeDialog
{
    @Inject
    private PageBinder binder;
    @ElementBy (className = "acknowledge-dialog")
    private PageElement dialog;
    @ElementBy (className = "aui-button", within = "dialog")
    private PageElement acknowledgeLink;

    @WaitUntil
    public void waitForLoad()
    {
        Poller.waitUntilTrue(dialog.timed().isPresent());
    }

    public LinkElement getAcknowledgeLink()
    {
        return new LinkElement(acknowledgeLink);
    }

    public FormMessages getMessages()
    {
        return binder.bind(FormMessages.class, dialog);
    }
}
