package com.atlassian.jira.pageobjects.pages.setup;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import com.google.inject.Inject;

public class SetupAccountPage extends AbstractJiraPage
{
    private static final String URI = "/secure/SetupAccount!default.jspa";

    @ElementBy (id = "jira-setup-account")
    private PageElement form;

    @Override
    public TimedCondition isAt()
    {
        return form.timed().isPresent();
    }

    @Override
    public String getUrl()
    {
        return URI;
    }

    public CheckAccount checkAccountView()
    {
        return pageBinder.bind(CheckAccount.class);
    }

    public static class CheckAccount
    {
        @ElementBy(className = "jira-setup-account-form-basic")
        private PageElement basicForm;

        @ElementBy(name = "jira-setup-account-field-email")
        private PageElement emailField;

        @ElementBy(className = "jira-setup-account-form-button")
        private PageElement nextButton;

        @Inject private PageBinder binder;

        @Init
        public void init()
        {
            basicForm.timed().isVisible().byDefaultTimeout();
        }

        public CheckAccount setEmail(String email)
        {
            emailField.type(email);
            return this;
        }

        public CreateAccount clickNextWithNonExistingUserEmail()
        {
            if(nextButton.timed().isEnabled().byDefaultTimeout()) {
                nextButton.click();
            }
            return binder.bind(CreateAccount.class);
        }
    }

    public static class CreateAccount
    {
        @ElementBy(className = "jira-setup-account-form-registration")
        private PageElement registrationForm;

        @ElementBy(name = "jira-setup-account-field-email")
        private PageElement emailField;

        @ElementBy(name = "jira-setup-account-field-fullname")
        private PageElement fullNameField;

        @ElementBy(name = "jira-setup-account-field-password")
        private PageElement passwordField;

        @ElementBy(id = "jira-setup-account-field-agreement")
        private PageElement licenseAgreement;

        @ElementBy(className = "jira-setup-account-form-button")
        private PageElement createAccountButton;

        @Inject private PageBinder binder;

        @Init
        public void init()
        {
            registrationForm.timed().isVisible().byDefaultTimeout();
        }

        public String getEmail()
        {
            return emailField.getValue();
        }

        public CreateAccount setFullName(String name)
        {
            fullNameField.type(name);
            return this;
        }

        public CreateAccount setPassword(String password)
        {
            passwordField.type(password);
            return this;
        }

        public CreateAccount acceptLicenseAgreement(boolean whether)
        {
            if (whether ^ licenseAgreement.isSelected())
            {
                licenseAgreement.click();
            }
            return this;
        }

        public SetupAccountSummary clickCreate()
        {
            if(createAccountButton.timed().isEnabled().byDefaultTimeout()) {
                createAccountButton.click();
            }
            return binder.bind(SetupAccountSummary.class);
        }
    }

    public static class SetupAccountSummary
    {
        @ElementBy(className = "jira-setup-account-summary")
        private PageElement summaryContainer;

        @ElementBy(id = "jira-setup-account-button-submit")
        private PageElement submitButton;

        @Inject private PageBinder binder;

        @Init
        public void init()
        {
            summaryContainer.timed().isVisible().byDefaultTimeout();
            submitButton.timed().isVisible().byDefaultTimeout();
            submitButton.timed().isEnabled().byDefaultTimeout();
        }

        public SetupFinishingPage clickNext()
        {
            if(submitButton.timed().isEnabled().byDefaultTimeout()) {
                submitButton.click();
            }
            return binder.bind(SetupFinishingPage.class);
        }
    }
}
