package com.atlassian.jira.pageobjects.elements;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.uri.Uri;

/**
 * Any HTML image.
 *
 * @since v6.4
 */
public class ImageElement
{
    private final PageElement element;

    public ImageElement(final PageElement element)
    {
        this.element = element;
    }

    public Uri getUri()
    {
        return Uri.parse(element.getAttribute("src"));
    }

    public String getAlternativeText()
    {
        return element.getAttribute("alt");
    }
}
