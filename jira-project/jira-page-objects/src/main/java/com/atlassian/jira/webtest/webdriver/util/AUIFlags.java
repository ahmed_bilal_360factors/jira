package com.atlassian.jira.webtest.webdriver.util;

import com.atlassian.jira.pageobjects.JiraTestedProduct;

/**
 * @since v6.4
 */
public class AUIFlags
{
    public static final String CLOSE_FLAGS_JS = "AJS.$('.aui-flag[aria-hidden=false]').each( function(it) { this.close() } )";
    private final JiraTestedProduct jira;

    public AUIFlags(final JiraTestedProduct jira)
    {
        this.jira = jira;
    }

    public void closeAllFlags()
    {
        jira.getTester().getDriver().executeScript(CLOSE_FLAGS_JS);
    }
}
