package com.atlassian.jira.pageobjects.model;

import javax.annotation.concurrent.Immutable;
import javax.ws.rs.core.MediaType;

import com.atlassian.uri.Uri;

import com.google.common.base.Objects;

/**
 * @since v6.4
 */
@Immutable
public class FileDownloadInfo
{
    private final MediaType mediaType;
    private final String fileName;
    private final Uri downloadUri;

    public FileDownloadInfo(final MediaType mediaType, final String fileName, final Uri downloadUri)
    {
        this.mediaType = mediaType;
        this.fileName = fileName;
        this.downloadUri = downloadUri;
    }

    public MediaType getMediaType()
    {
        return mediaType;
    }

    public String getFileName()
    {
        return fileName;
    }

    public Uri getDownloadUri()
    {
        return downloadUri;
    }

    @Override
    public int hashCode() {return Objects.hashCode(mediaType, fileName, downloadUri);}

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()) {return false;}
        final FileDownloadInfo other = (FileDownloadInfo) obj;
        return Objects.equal(this.mediaType, other.mediaType)
                && Objects.equal(this.fileName, other.fileName)
                && Objects.equal(this.downloadUri, other.downloadUri);
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this)
                .add("mediaType", mediaType)
                .add("fileName", fileName)
                .add("downloadUri", downloadUri)
                .toString();
    }
}
