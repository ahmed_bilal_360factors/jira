package com.atlassian.jira.pageobjects.dialogs;

import org.openqa.selenium.By;

public class DeleteAttachmentDialog extends FormDialog
{
    public static final String DELETE_ATTACHMENT_DIALOG = "delete-attachment-dialog";

    public DeleteAttachmentDialog()
    {
        super(DELETE_ATTACHMENT_DIALOG);
    }

    public boolean submit()
    {
        return super.submit(By.id("delete-attachment-submit"));
    }
}