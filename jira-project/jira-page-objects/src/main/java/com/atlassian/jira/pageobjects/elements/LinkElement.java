package com.atlassian.jira.pageobjects.elements;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.uri.Uri;

/**
 * A link element: &lt;a&gt;.
 *
 * @since v6.4
 */
public final class LinkElement
{
    private final PageElement element;

    public LinkElement(final PageElement element)
    {
        this.element = element;
    }

    public Uri getDestination()
    {
        return Uri.parse(element.getAttribute("href"));
    }

    public String getText()
    {
        return element.getText();
    }

    public void click()
    {
        element.click();
    }
}
