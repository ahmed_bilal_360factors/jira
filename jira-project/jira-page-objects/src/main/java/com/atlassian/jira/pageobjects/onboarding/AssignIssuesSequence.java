package com.atlassian.jira.pageobjects.onboarding;

import com.atlassian.jira.pageobjects.components.fields.SingleSelect;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import javax.inject.Inject;

public class AssignIssuesSequence extends Sequence
{
    @ElementBy(className = "onboarding-sequence-assign-issues")
    private PageElement content;

    @ElementBy(className = "issue-table")
    private PageElement issueTable;

    @ElementBy(id = "next")
    private PageElement nextButton;

    @Inject
    private PageBinder pageBinder;

    @Override
    protected TimedCondition isAt()
    {
        return content.timed().isVisible();
    }

    public void assignIssue(int index, String assignee)
    {
        SingleSelect singleSelect = pageBinder.bind(SingleSelect.class, getRow(index));
        singleSelect.select(assignee);
    }

    public void done()
    {
        nextButton.click();
    }

    private PageElement getRow(int index)
    {
        return issueTable.findAll(By.cssSelector("tbody > tr")).get(index);
    }
}
