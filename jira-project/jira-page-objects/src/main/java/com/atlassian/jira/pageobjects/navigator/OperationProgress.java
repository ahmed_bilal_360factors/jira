package com.atlassian.jira.pageobjects.navigator;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import org.openqa.selenium.By;

import java.util.List;

/**
 * Changes confirmation page for Bulk Edit
 *
 * @since v6.4
 */
public class OperationProgress extends AbstractJiraPage
{
    @ElementBy(className = "formtitle")
    protected PageElement title;

    @ElementBy(name = "Acknowledge")
    protected PageElement acknowledgeButton;


    @Override
    public TimedCondition isAt()
    {
        return title.timed().hasText("Bulk Operation Progress");
    }

    @Override
    public String getUrl()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    public AdvancedSearch acknowledge()
    {
        acknowledgeButton.withTimeout(TimeoutType.SLOW_PAGE_LOAD).click();
        return pageBinder.bind(AdvancedSearch.class);
    }

}
