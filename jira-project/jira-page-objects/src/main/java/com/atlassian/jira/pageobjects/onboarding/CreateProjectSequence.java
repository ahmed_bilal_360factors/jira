package com.atlassian.jira.pageobjects.onboarding;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;

public class CreateProjectSequence extends Sequence
{
    @ElementBy(className = "onboarding-sequence-create-project")
    private PageElement content;

    @ElementBy(id = "submit")
    private PageElement submitButton;

    @ElementBy(id = "skip")
    private PageElement skipButton;

    @ElementBy(id = "project-name")
    private PageElement projectName;

    @ElementBy(id = "project-key")
    private PageElement projectKey;

    @Override
    protected TimedCondition isAt()
    {
        return content.timed().isVisible();
    }

    public CreateProjectSequence setProjectName(String value)
    {
        projectName.click().type(value);
        return this;
    }

    public CreateProjectSequence setProjectKey(String value)
    {
        projectKey.click().type(value);
        return this;
    }

    public TimedQuery<String> getProjectKey()
    {
        return projectKey.timed().getValue();
    }

    public CreateProjectSequence submit()
    {
        submitButton.click();
        return this;
    }
}
