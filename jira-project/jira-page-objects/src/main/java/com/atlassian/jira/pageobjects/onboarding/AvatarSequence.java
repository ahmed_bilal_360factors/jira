package com.atlassian.jira.pageobjects.onboarding;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public class AvatarSequence extends Sequence
{
    @ElementBy(className = "onboarding-sequence-avatar-picker")
    private PageElement content;

    @ElementBy(className = "avatar-picker-done")
    private PageElement doneButton;

    @Override
    public TimedCondition isAt()
    {
        return content.timed().isVisible();
    }

    public void done()
    {
        doneButton.click();
    }
}
