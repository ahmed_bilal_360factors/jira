package com.atlassian.jira.pageobjects.onboarding;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class CreateIssuesSequence extends Sequence
{
    @ElementBy(className = "onboarding-sequence-create-issues")
    private PageElement content;

    @ElementBy(className = "issue-table")
    private PageElement issueTable;

    @ElementBy(id = "create-issues-form")
    private PageElement createIssuesForm;

    @ElementBy(className = "iic-trigger")
    private PageElement createIssueButton;

    @ElementBy(cssSelector = ".iic-widget .aui-list-truncate li")
    private PageElement issueTypeIcons;

    @ElementBy(className = "iic-widget__summary")
    private PageElement summaryField;

    @ElementBy(id = "next")
    private PageElement nextButton;

    @Override
    protected TimedCondition isAt()
    {
        return Conditions.and(
                content.timed().isVisible(),
                createIssuesForm.timed().isVisible()
        );
    }

    public void done()
    {
        nextButton.click();
    }

    public int getNumberOfIssuesCreated()
    {
        return issueTable.findAll(By.cssSelector("tbody > tr")).size();
    }

    public void openCreateIssue()
    {
        if (!summaryField.isVisible())
        {
            createIssueButton.click();
        }
        Poller.waitUntilTrue(Conditions.and(
                issueTypeIcons.timed().isPresent(), // Wait for issue types to load
                summaryField.timed().isVisible()
        ));
    }

    public void submitCreateIssue(String summary)
    {
        summaryField.type(summary).type(Keys.RETURN);
        Poller.waitUntilEquals("", summaryField.timed().getValue());
    }

    public void closeCreateIssue()
    {
        summaryField.type(Keys.ESCAPE);
        Poller.waitUntilFalse(summaryField.timed().isVisible());
    }

    public void createIssue(String summary)
    {
        openCreateIssue();
        submitCreateIssue(summary);
        closeCreateIssue();
    }
}
