package com.atlassian.jira.pageobjects.pages;

import java.net.URI;
import java.net.URISyntaxException;

import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import com.google.common.base.Supplier;

public class ViewAttachmentPage extends AbstractJiraPage
{
    private final long attachmentId;
    private final String attachmentName;

    public ViewAttachmentPage(long attachmentId, String attachmentName)
    {
        this.attachmentId = attachmentId;
        this.attachmentName = attachmentName;
    }

    @Override
    public TimedCondition isAt()
    {
        return Conditions.forSupplier(new Supplier<Boolean>()
        {
            @Override
            public Boolean get()
            {
                return "complete".equals(driver.executeScript("return document.readyState;"));
            }
        });
    }

    @Override
    public String getUrl()
    {
        String path = String.format("/secure/attachment/%d/%s", attachmentId, attachmentName);
        try
        {
            URI uri = new URI(null, null, path, null);
            return uri.getRawPath();
        }
        catch (URISyntaxException e)
        {
            return null;
        }
    }
}