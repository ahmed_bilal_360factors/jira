package com.atlassian.jira.pageobjects.pages.admin.cluster;

import com.atlassian.jira.pageobjects.pages.AbstractJiraAdminPage;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import org.openqa.selenium.By;

/**
 * Replication Settings Page
 *
 * @since v6.4
 */
public class ReplicationSettingsPage extends AbstractJiraAdminPage
{

    private static final String URI = "/secure/admin/cluster/ReplicationSettings.jspa";

    @Override
    public String linkId()
    {
        return "replication";
    }

    @Override
    public TimedCondition isAt()
    {
        return elementFinder.find(By.id("body")).timed().isPresent();
    }

    @Override
    public String getUrl()
    {
        return URI;
    }
}
