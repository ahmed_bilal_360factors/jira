package com.atlassian.jira.pageobjects.pages.setup;

import java.util.concurrent.TimeUnit;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import static org.hamcrest.Matchers.is;

/**
 * @since v6.4
 */
public class SetupFinishingPage extends AbstractJiraPage
{
    private static final String URI = "/secure/SetupFinishing.jspa";

    @ElementBy (id = "jira-setup-finishing-submit")
    private PageElement submitButton;

    @ElementBy (id = "jira-setup-finishing")
    private PageElement form;

    @Override
    public String getUrl()
    {
        return URI;
    }

    @Override
    public TimedCondition isAt()
    {
        return form.timed().isPresent();
    }

    public void submit()
    {
        submitButton.click();
    }

    public void waitToFinish()
    {
        Poller.waitUntil("Setup hasn't finished.", submitButton.timed().isVisible(), is(true), Poller.by(5, TimeUnit.MINUTES));
    }
}
