package com.atlassian.jira.pageobjects.pages.setup;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import org.openqa.selenium.By;

/**
 * @since v6.4
 */
public class SetupModePage extends AbstractJiraPage
{
    private static final String URI = "/secure/SetupMode!default.jspa";

    @ElementBy (id = "jira-setup-mode-submit")
    private PageElement submitButton;

    @ElementBy (id = "jira-setup-mode")
    private PageElement form;

    @ElementBy(id = "jira-setup-language-dialog-trigger")
    private PageElement languageChangeDialogTrigger;

    @Override
    public String getUrl()
    {
        return URI;
    }

    @Override
    public TimedCondition isAt()
    {
        return form.timed().isPresent();
    }

    public DatabaseSetupPage submit()
    {
        submitButton.click();
        return pageBinder.bind(DatabaseSetupPage.class);
    }

    public SetupProductBundlePage submitToInstantPath()
    {
        submitButton.click();
        return pageBinder.bind(SetupProductBundlePage.class);
    }

    public enum ModeSelection
    {
        INSTANT,
        CLASSIC
    }

    public SetupModePage selectMode(ModeSelection mode)
    {
        for(PageElement choice : form.findAll(By.cssSelector("div.jira-setup-choice-box")))
        {
            if (choice.getAttribute("data-choice-value").equals(mode.name().toLowerCase()))
            {
                choice.click();
                break;
            }
        }
        return this;
    }

    public SetupLanguageDialog openLanguageDialog()
    {
        languageChangeDialogTrigger.click();
        return pageBinder.bind(SetupLanguageDialog.class);
    }

    public String getLanguageChangeDialogTriggerText()
    {
        return languageChangeDialogTrigger.getText();
    }
}
