package com.atlassian.jira.pageobjects.pages.viewissue.attachment;


import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import com.google.inject.Inject;

/**
 * The page for uploading attachments.
 *
 * @since v6.4
 */
public final class AttachFilePage extends AbstractJiraPage
{
    private static final String URI_TEMPLATE = "/secure/AttachFile!default.jspa?id=%d";

    private final int issueId;
    private MultiFileInput fileInput;

    @Inject
    private PageBinder pageBinder;
    @ElementBy (id = "attach-file-submit")
    private PageElement submit;
    @ElementBy (tagName = "input", className = "upfile")
    private PageElement fileInputElement;

    public AttachFilePage(final int issueId)
    {
        this.issueId = issueId;
    }

    @Init
    public void init()
    {
        this.fileInput = new MultiFileInput(fileInputElement);
    }

    public MultiFileInput getFileInput()
    {
        return fileInput;
    }

    public final <T> T submit(final Class<T> resultPageClass)
    {
        submit.click();
        return pageBinder.bind(resultPageClass);
    }

    @Override
    public final TimedCondition isAt()
    {
        return submit.timed().isPresent();
    }

    @Override
    public final String getUrl()
    {
        return String.format(URI_TEMPLATE, issueId);
    }

}
