package com.atlassian.jira.pageobjects.onboarding;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 * Abstract class to aid in implementing page objects for onboarding sequences.
 * @see http://extranet.atlassian.com/pages/viewpage.action?pageId=2295240868
 */
public abstract class Sequence
{
    protected abstract TimedCondition isAt();

    @WaitUntil
    protected void waitUntil()
    {
        Poller.waitUntilTrue("Sequence content did not appear", isAt());
    }
}

