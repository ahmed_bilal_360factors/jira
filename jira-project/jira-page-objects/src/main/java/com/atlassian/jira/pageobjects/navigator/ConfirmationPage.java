package com.atlassian.jira.pageobjects.navigator;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

import java.util.List;

/**
 * Changes confirmation page for Bulk Edit
 *
 * @since v6.4
 */
public class ConfirmationPage extends AbstractJiraPage
{
    @ElementBy(id = "confirm")
    protected PageElement confirm;

    @ElementBy(id = "updatedfields")
    protected PageElement updatedfields;


    @Override
    public TimedCondition isAt()
    {
        return updatedfields.timed().isVisible();
    }

    @Override
    public String getUrl()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    public String getUpdatedField()
    {
        return updatedfields.find(By.tagName("td")).getText();
    }

    public String getUpdateOption()
    {
        List<PageElement> updatedFieldsRows = updatedfields.findAll(By.tagName("td"));
        return updatedFieldsRows.get(1).getText();
    }

    public OperationProgress confirm()
    {
        confirm.click();
        return pageBinder.bind(OperationProgress.class);
    }

}
