package com.atlassian.jira.pageobjects.components.license;


import javax.annotation.Nonnull;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import org.openqa.selenium.By;

/**
 * @since v6.3
 */
public class LicenseBanner extends LicenseContent
{
    @ElementBy (id = "license-banner-content")
    private PageElement content;

    @Nonnull
    @Override
    protected PageElement getContent()
    {
        return content;
    }

    @Override
    protected PageElement getRemindMeNeverLink()
    {
        return getContent().find(By.id("license-banner-never"));
    }

    @Override
    protected PageElement getRemindMeLaterLink()
    {
        return getContent().find(By.id("license-banner-later"));
    }

    @Override
    protected PageElement getMacLink()
    {
        return getContent().find(By.id("license-banner-my-link"));
    }

    public String getSalesUrl()
    {
        final PageElement pageElement = getSalesLink();
        if (isPresentAndVisible(pageElement))
        {
            return pageElement.getAttribute("href");
        }
        return null;
    }

    private PageElement getSalesLink()
    {
        return getContent().find(By.id("license-banner-sales-link"));
    }
}
