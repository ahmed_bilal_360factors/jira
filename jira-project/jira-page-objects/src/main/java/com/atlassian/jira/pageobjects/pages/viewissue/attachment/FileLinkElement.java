package com.atlassian.jira.pageobjects.pages.viewissue.attachment;

import com.atlassian.jira.pageobjects.elements.DownloadableElement;
import com.atlassian.jira.pageobjects.elements.LinkElement;
import com.atlassian.jira.pageobjects.model.FileDownloadInfo;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.uri.Uri;

/**
 * A downloadable link to a file.
 *
 * @since v6.4
 */
public final class FileLinkElement
{
    public static FileLinkElement createFromTheSameElement(final PageElement element)
    {
        final LinkElement link = new LinkElement(element);
        final DownloadableElement downloadable = new DownloadableElement(element);
        return new FileLinkElement(link, downloadable);
    }

    private final LinkElement link;
    private final DownloadableElement downloadable;

    private FileLinkElement(final LinkElement link, final DownloadableElement downloadable)
    {
        this.link = link;
        this.downloadable = downloadable;
    }

    public String getLabel()
    {
        return link.getText();
    }

    public Uri getFileUri()
    {
        return link.getDestination();
    }

    public boolean isDraggable()
    {
        return downloadable.isDraggable();
    }

    public FileDownloadInfo getDownloadInfo()
    {
        return downloadable.getDownloadInfo();
    }

    public void click() { link.click(); }
}
