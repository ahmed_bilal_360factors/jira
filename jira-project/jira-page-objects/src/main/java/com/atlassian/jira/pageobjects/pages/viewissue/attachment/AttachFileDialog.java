package com.atlassian.jira.pageobjects.pages.viewissue.attachment;


import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

import org.openqa.selenium.By;

/**
 * The dialog for uploading attachments.
 *
 * @since v6.4
 */
public final class AttachFileDialog
{
    @ElementBy (id = "attach-file-dialog")
    private PageElement dialogElement;
    private MultiFileInput fileInput;
    private PageElement submit;

    @Init
    public void init()
    {
        submit = dialogElement.find(By.id("attach-file-submit"));
        final PageElement fileInputElement = dialogElement.find(By.cssSelector("input.upfile"));
        fileInput = new MultiFileInput(fileInputElement);
    }

    @WaitUntil
    public void waitForLoad()
    {
        Poller.waitUntilTrue(dialogElement.timed().isPresent());
    }

    /**
     * @return input for attachments
     */
    public MultiFileInput getFileInput()
    {
        return fileInput;
    }

    /**
     * Clicks the submit button and waits for the dialog to disappear.
     */
    public void submit()
    {
        submit.click();
        Poller.waitUntilFalse(dialogElement.timed().isVisible());
    }
}
