package com.atlassian.jira.pageobjects;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.pageobjects.setup.JiraWebTestClassRules;
import com.atlassian.jira.pageobjects.setup.JiraWebTestLogger;
import com.atlassian.jira.pageobjects.setup.JiraWebTestRules;
import com.atlassian.jira.pageobjects.setup.SingleJiraWebTestRunner;
import com.atlassian.pageobjects.PageBinder;

import com.google.inject.Inject;

import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;

/**
 * Base test class, which exposes objects necessary for black-box tests. Allows subclasses to customize the order of
 * {@link org.junit.Rule}s and {@link org.junit.ClassRule}s.
 *
 * @since v6.4
 */
@RunWith (SingleJiraWebTestRunner.class)
public abstract class JiraWebDriverTest
{
    protected static final Logger logger = JiraWebTestLogger.LOGGER;
    @Inject
    protected static JiraTestedProduct jira;
    @Inject
    protected static PageBinder pageBinder;
    @Inject
    protected static Backdoor backdoor;
    @Inject
    private static JiraWebTestClassRules baseClassRule;

    public static TestRule getBaseClassRule()
    {
        return baseClassRule;
    }

    private final TestRule baseRule = JiraWebTestRules.forJira(jira);

    public final TestRule getBaseRule()
    {
        return baseRule;
    }
}
