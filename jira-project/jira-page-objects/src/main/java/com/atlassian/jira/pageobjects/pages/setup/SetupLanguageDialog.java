package com.atlassian.jira.pageobjects.pages.setup;

import com.atlassian.jira.pageobjects.components.fields.SingleSelect;
import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 * @since v6.4.1
 */
public class SetupLanguageDialog extends AbstractJiraPage
{
    @ElementBy (id = "jira-setup-language-form")
    protected PageElement form;

    @ElementBy (cssSelector = ".jira-setup-language-save-button")
    protected PageElement submit;

    protected SingleSelect language;

    public void selectLanguage(final String language)
    {
        this.language.select(language);

        Poller.waitUntilTrue(submit.timed().isEnabled());
    }

    public SetupModePage submitLanguage()
    {
        submit.click();

        Poller.waitUntilFalse(isAt());

        return pageBinder.bind(SetupModePage.class);
    }

    @Init
    public void init()
    {
        language = pageBinder.bind(SingleSelect.class, form);
    }

    @Override
    public TimedCondition isAt()
    {
        return form.timed().isPresent();
    }

    @Override
    public String getUrl()
    {
        return null;
    }
}
