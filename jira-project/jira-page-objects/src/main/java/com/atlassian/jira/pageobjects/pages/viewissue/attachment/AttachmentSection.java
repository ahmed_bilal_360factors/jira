package com.atlassian.jira.pageobjects.pages.viewissue.attachment;

import java.util.List;
import java.util.Map;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementActions;
import com.atlassian.pageobjects.elements.timeout.Timeouts;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;

import org.openqa.selenium.By;

/**
 * Attachment section of "View Issue" page
 *
 * @since v6.4
 */
public class AttachmentSection
{
    @Inject
    private PageBinder pageBinder;

    @Inject
    private PageElementActions actions;

    @Inject
    protected Timeouts timeouts;

    @ElementBy (id = "attachmentmodule")
    private PageElement section;

    public Map<String, AttachmentRow> getAttachmentRowsByTitle()
    {
        final ImmutableMap.Builder<String, AttachmentRow> rowsByTitle = ImmutableMap.builder();
        final List<PageElement> rows = section.findAll(By.className("attachment-content"));
        for (final PageElement row : rows)
        {
            final AttachmentRow attachmentRow = new AttachmentRow(row, pageBinder, actions, timeouts);
            rowsByTitle.put(attachmentRow.getTitle(), attachmentRow);
        }
        return rowsByTitle.build();
    }
}