package com.atlassian.jira.pageobjects.pages.project;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import org.openqa.selenium.By;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.google.common.collect.ImmutableList.copyOf;

/**
 * Browse projects page implementation.
 *
 * @since v6.3
 */
public class BrowseProjectsPage extends AbstractJiraPage
{
    public static final String ALL = "all";
    public static final String RECENT = "recent";
    public static final String NO_CATEGORY = "none";
    private final String selectedCategory;
    private final String containsFilter;
    private final int page;

    @ElementBy(cssSelector = "tbody.projects-list")
    private PageElement projectsList;

    @ElementBy(id = "filter-projects")
    private PageElement filterContainer;

    @ElementBy(id = "pagination")
    private PageElement paginationContainer;

    @ElementBy(cssSelector = ".aui-page-panel-nav")
    private PageElement navigationContainer;

    public BrowseProjectsPage()
    {
        this(null, null, 0);
    }

    public BrowseProjectsPage(final String selectedCategory, final String containsFilter, final int page)
    {
        this.selectedCategory = selectedCategory;
        this.containsFilter = containsFilter;
        this.page = page;
    }

    @Override
    public String getUrl()
    {
        StringBuilder params = new StringBuilder();

        if (selectedCategory != null && !selectedCategory.isEmpty())
        {
            params.append("&selectedCategory="+selectedCategory);
        }
        if (containsFilter != null && !containsFilter.isEmpty())
        {
            params.append("&contains="+containsFilter);
        }
        if (page > 0)
        {
            params.append("&page="+page);
        }

        String url = "/secure/BrowseProjects.jspa" + (params.length() > 0 ? "?" + params.substring(1): "");

        return url;
    }

    @Override
    public TimedCondition isAt()
    {
        return projectsList.timed().isPresent();
    }

    public boolean isAllowed()
    {
        return !isForbidden();
    }

    public boolean isForbidden()
    {
        final PageElement errorMsgParagraphs = elementFinder.find(By.cssSelector("div.aui-message.error p"));
        // the error message is: You are not logged in and do not have the permissions required to browse projects as a guest.
        return errorMsgParagraphs.isPresent() && errorMsgParagraphs.getText().contains("do not have the permissions");
    }

    public List<String> getProjectKeys()
    {
        final List<PageElement> projectLinkElements = elementFinder.findAll(By.cssSelector("tbody.projects-list tr td:nth-child(2) a"));
        return copyOf(Iterables.transform(projectLinkElements, new Function<PageElement, String>()
        {
            @Override
            public String apply(final PageElement from)
            {
                final String href = from.getAttribute("href");
                return href.substring(href.lastIndexOf("/") + 1);
            }
        }));
    }

    public TimedCondition isEmpty()
    {
        return projectsList.find(By.cssSelector(".no-results")).timed().isPresent();
    }

    public BrowseProjectsPage filterByText(final String text)
    {
        filterContainer.find(By.id("project-filter-text")).type(text);
        try
        {
            final String parameter = "contains=" + URLEncoder.encode(text, "UTF-8");
            waitUntilTrue("Failed to filter by text.", Conditions.forSupplier(timeouts.timeoutFor(TimeoutType.PAGE_LOAD), new Supplier<Boolean>()
            {
                @Override
                public Boolean get()
                {
                    return driver.getCurrentUrl().contains(parameter);
                }
            }));
            return pageBinder.bind(BrowseProjectsPage.class, selectedCategory, text, 1);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e);
        }
    }

    public BrowseProjectsPage filterByCategory(String category)
    {
        String id = category + "-panel-tab-lnk";
        navigationContainer.find(By.id(id)).click();

        return pageBinder.bind(BrowseProjectsPage.class, category, containsFilter, 1);
    }

    public BrowseProjectsPage goToPage(int number)
    {
        String selector = "li:nth-child(%d) a";
        paginationContainer.find(By.cssSelector(String.format(selector, number + 1))).click();
        return pageBinder.bind(BrowseProjectsPage.class, selectedCategory, containsFilter, number);
    }

    public BrowseProjectsPage goToNextPage()
    {
        paginationContainer.find(By.cssSelector(".aui-nav-next a")).click();
        return pageBinder.bind(BrowseProjectsPage.class, selectedCategory, containsFilter, (this.page < 2) ? 2 : this.page + 1);
    }

    public BrowseProjectsPage goToPreviousPage()
    {
        paginationContainer.find(By.cssSelector(".aui-nav-previous a")).click();
        return pageBinder.bind(BrowseProjectsPage.class, selectedCategory, containsFilter, this.page - 1);
    }

    public TimedQuery<String> getCurrentPageNumber()
    {
        return paginationContainer.find(By.cssSelector(".aui-nav-selected")).timed().getText();
    }

    public TimedQuery<String> getNumberOfPages()
    {
        return paginationContainer.find(By.cssSelector("ol")).timed().getAttribute("data-total");
    }

    public TimedCondition hasPagination()
    {
        // we assume that pagination is rendered if previous link is
        return paginationContainer.find(By.cssSelector(".aui-nav-previous")).timed().isPresent();
    }

    public static Map<String, String> getFilterParamsFromUrl(String url)
    {
        Map<String, String> params = Maps.newHashMap();
        if (url.contains("?"))
        {
            String[] hashParams = url.split("[?]")[1].split("&");
            for (String param : hashParams)
            {
                String[] paramParts = param.split("=");
                if (paramParts.length == 2)
                {
                    params.put(paramParts[0], paramParts[1]);
                }
            }
        }
        return params;
    }

    /**
     * Return project at given position in table
     *
     * @param index 1-based index of element in the table
     * @return Project
     */
    public Project getProjectAt(int index)
    {
        String selector = String.format("tr:nth-child(%d)", index);
        PageElement row = projectsList.find(By.cssSelector(selector));
        return pageBinder.bind(Project.class, row);
    }

    public static class Project
    {
        @Inject
        private PageBinder pageBinder;

        private final PageElement rowElement;
        private PageElement name;
        private PageElement category;

        public Project(PageElement rowElement)
        {
            this.rowElement = rowElement;
        }

        @Init
        public void initContent()
        {
            name = rowElement.find(By.cssSelector("td:nth-child(2) a"));
            category = rowElement.find(By.cssSelector("td:nth-child(4) a"));
        }

        public TimedQuery<String> getName()
        {
            return name.timed().getText();
        }

        public TimedQuery<String> getCategory()
        {
            if (category.timed().isPresent().byDefaultTimeout())
                return category.timed().getText();
            else
                return null;
        }
    }
}
