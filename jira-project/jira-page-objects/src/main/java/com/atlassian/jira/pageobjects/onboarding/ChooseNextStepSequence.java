package com.atlassian.jira.pageobjects.onboarding;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public class ChooseNextStepSequence extends Sequence
{
    @ElementBy(className = "onboarding-sequence-next-step")
    private PageElement content;

    @ElementBy(cssSelector = ".next-step[data-step-key='browseprojects']")
    private PageElement browseProjectsLink;

    @Override
    protected TimedCondition isAt()
    {
        return content.timed().isVisible();
    }

    public void clickBrowseProjects()
    {
        browseProjectsLink.click();
    }
}
