# JIRA Source Code
 
## Getting Started
[Development Handbook](https://extranet.atlassian.com/display/JIRADEV/JIRA+Development+Handbook)

[Rules of Engagement](https://extranet.atlassian.com/display/JIRADEV/The+JIRA+development+process)

## Running from Source

```
> ./jmake run
```

or

```
> ./jmake debug
```

Run Unit tests:

```
> ./jmake unit-tests
```

See also

```
> ./jmake --help
> ./jmake <command> -h
```

## Working on stable

First create an issue branch from atlassian_jira_6_4_branch named like **stable-issue/JDEV-1234-my-change** (Stash UI will help you)

## Builds

All relevant builds can be found here: https://jira-bamboo.internal.atlassian.com/browse/J64SPT

If you followed the naming pattern described above, Bamboo will trigger the builds for you. :)
