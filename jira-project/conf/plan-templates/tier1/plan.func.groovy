// Depends on:
//     tier1/shortcuts.func.groovy
//     tier1/shortcuts.tier1.groovy
//     shortcuts.common.groovy

plan(key: 'CIFUNCHAL', name: 'CI Functional Tests', description: '') {
    def jdk7 = 'JDK 1.7'
    def repoName = 'JIRA 6.4.x branch'

    tier1Project()
    tier1Labels()

    repositoryPolling(repoName: repoName)
    branches(pattern: 'stable-issue/.*', removeAfterDays: '7')
    planDependencies()
    notifications()
    globalVariables()

    stage(name: 'Compile', manual: 'false') {
        produceFuncArtifacts(repoName: repoName, jdk: jdk7)
    }

    stage(name: 'Unit and Functional Tests', manual: 'false') {
        findbugs(repoName: repoName, jdk: jdk7)
        funcTestsJobs(jdk: jdk7)
        funcTestsHallelujahServer(jdk: jdk7)
        onDemandPluginUnitTests(repoName: repoName, jdk: jdk7)
        platformCompatibilityTest(repoName: repoName, jdk: jdk7)
        qUnitTests(repoName: repoName, jdk: jdk7)
        unitTestsAndFunctionalUnitTests(repoName: repoName, jdk: jdk7)
    }
}
