// --------------------------------------------------------------------------------------------------
// This file contains shortcuts common to all release plans
// --------------------------------------------------------------------------------------------------
releaseProject() {
    project(key: 'J64SPT', name: 'JIRA 6.4 Stable - Plan templates', description: '')
}

releaseLabels() {
    label(name: 'plan-templates')
    label(name: 'release')
}

releaseGlobalVariables() {
    variable(key: 'jira.6.4.release.version', value: '6.4.5')
    variable(key: 'jira.6.4.release.branch', value: 'atlassian_jira_6_4_5_branch')
    variable(key: 'jira.6.4.next.version', value: '6.4.6-SNAPSHOT')
    variable(key: 'jira.6.4.tag', value: 'atlassian_jira_6_4_5')
    variable(key: 'jira.version.increment', value: '1')
    variable(key: 'jira.6.4.branch.raw', value: 'atlassian_jira_6_4_branch')
}

generateBomAndDownloadLicenses() {
    task(
            type: 'script',
            description: 'Generate BOM and download license files',
            script: './bin/ci-third-party-licensing.sh',
            argument: 'generate',
            environmentVariables: 'MAVEN_OPTS="-Xmx1024m -XX:MaxPermSize=256m" M2_HOME="${bamboo.capability.system.builder.mvn3.Maven 3.0}'
    )
}
