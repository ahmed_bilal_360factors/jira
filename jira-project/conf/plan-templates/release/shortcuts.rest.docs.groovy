globalVariables() {
    releaseGlobalVariables()
}

publishRestDocs(['repoName']) {
    job(name: 'Default Job', key: 'JOB1', description: '') {
        requireLocalLinux()

        checkout(repoName: '#repoName', forceClean: 'false')
        executeRestDocsScript()
    }
}

executeRestDocsScript() {
    task(
            type: 'script',
            description: 'Executes Rest Docs Script',
            script: './bin/jira-rest-docs.sh',
            argument: '${bamboo.jira.6.4.release.version}',
            environmentVariables: 'HOME=${system.bamboo.agent.home} MAVEN_OPTS="${bamboo.maven.opts}" M3_HOME="${bamboo.capability.system.builder.mvn3.Maven 3.0}" JAVA_HOME="${bamboo.capability.system.jdk.JDK 1.6}"'
    )
}