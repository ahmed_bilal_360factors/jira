singleInstantSetupFuncTest(['name', 'testClass', 'index', 'jdk']) {
    job(name: '#name', key: 'FUNC#index', description: 'Setup test on pristine JIRA') {
        requireLinux()
        requireMaven30()
        requireJdk(jdk: '#jdk')

        artifactDefinition(name: 'Jira Logs', location: 'jira-distribution/jira-func-tests-runner/target/jirahome/log', pattern: '*', shared: 'false')
        artifactDefinition(name: 'Logs Screenshots and Movies', location: 'jira-distribution/jira-func-tests-runner/target/test-reports', pattern: '*', shared: 'false')
        artifactDefinition(name: 'SeleniumTestHarness', location: 'jira-distribution/jira-func-tests-runner/target/surefire-reports', pattern: '*', shared: 'false')

        artifactSubscription(name: 'hallelujah-dist', destination: '')
        artifactSubscription(name: 'prepare hallelujah', destination: '')

        miscellaneousConfiguration(cleanupWorkdirAfterBuild: 'true')
        prepareRunner()

        runSingleInstantSetupFuncTest(testClass: '#testClass', jdk: '#jdk')
    }
}

singleInstantSetupWebDriverTest(['name', 'index', 'testMethod', 'jdk']) {
    job(name: '#name', key: 'WEBDRIVER#index', description: 'Setup test on pristine JIRA') {
        requireLinux()
        requireMaven30()
        requireJdk(jdk: '#jdk')

        artifactDefinition(name: 'Jira Logs', location: 'jira-distribution/jira-webdriver-tests-runner/target/jirahome/log', pattern: '*', shared: 'false')
        artifactDefinition(name: 'Logs, Screenshots and Movies', location: 'jira-distribution/jira-webdriver-tests-runner/target/test-reports', pattern: '*', shared: 'false')
        artifactDefinition(name: 'Test reports', location: 'jira-distribution/jira-webdriver-tests-runner/target/surefire-reports', pattern: '*', shared: 'false')

        artifactSubscription(name: 'prepare-webdriver-tests', destination: '')
        artifactSubscription(name: 'jira-webdriver-tests-runner', destination: '')
        artifactSubscription(name: 'jira-webdriver-tests', destination: 'jira-distribution/jira-webdriver-tests-runner/PassedArtifacts')
        artifactSubscription(name: 'jira-webapp-dist', destination: 'jira-distribution/jira-webdriver-tests-runner/PassedArtifacts')

        miscellaneousConfiguration(cleanupWorkdirAfterBuild: 'true')

        brokerUrlDiscovery()
        prepareWebDriverRunner(jdk: '#jdk')
        webDriverVncServerSetup(jdk: '#jdk')
        createTestListFile(testMethod: '#testMethod', fileName: 'testsList.txt')
        runWebDriverInstantSetupTests(jdk: '#jdk', testsFileName: 'testsList.txt')
        webDriverTestsJUnitParser()
        webDriverVncServerTeardown(jdk: '#jdk')
    }
}

runSingleInstantSetupFuncTest(['testClass', 'jdk']) {
    task(
            type: 'maven3',
            description: 'Runs single functional test of setup',
            goal: 'clean verify -PdefaultBuild,maven3-statistics -Dmaven.test.unit.skip=true -Ddev.mode.plugins -Djira.security.disabled=true  -Drmi.port=${bamboo.capability.rmi.port} -Djira.functest.single.testclass=#testClass -Dfunc.mode.plugins -Dsystem.bamboo.agent.home=${bamboo.agentWorkingDirectory} -B',
            buildJdk: '#jdk',
            mavenExecutable: 'Maven 3.0',
            environmentVariables: 'MAVEN_OPTS="-Xmx768m -Xms128m -XX:MaxPermSize=256m"',
            hasTests: 'true'
    )
}

runWebDriverInstantSetupTests(['jdk', 'testsFileName']) {
    task(
            type: 'maven3',
            description: 'Runs single webdriver test of setup',
            goal: '${bamboo.run.webdriver.test.1} ${bamboo.run.webdriver.test.2} -Djira.hallelujah.queueId=${bamboo.planKey}-${bamboo.buildNumber}-webdriver -Dhallelujah.local.test.list=#testsFileName -Dtest.jira.setup.skip=true -Djira.seleniumtest.dev.mode=true',
            buildJdk: '#jdk',
            mavenExecutable: 'Maven 3.0',
            environmentVariables: 'DISPLAY=":20" MAVEN_OPTS="-Xmx512m -XX:MaxPermSize=256m" M2_HOME="${bamboo.capability.system.builder.mvn3.Maven 3.0}" JAVA_HOME="${bamboo.capability.system.jdk.#jdk}"',
            workingSubDirectory: 'jira-distribution',
            hasTests: 'false'
    )
}

produceFuncArtifactsForInstantSetup(['repoName', 'jdk']) {
    job(name: 'Produce func tests artifacts', key: 'FUNCARTIFACTS', description: '') {
        requireLinux()
        requireMaven30()
        requireJdk(jdk: '#jdk')

        artifactDefinition(name: 'hallelujah-dist', location: 'jira-distribution/jira-func-tests-runner/target', pattern: 'jira-*-dist.zip', shared: 'true')
        artifactDefinition(name: 'prepare hallelujah', location: 'jira-distribution/jira-func-tests-runner/src/main/dist', pattern: '*.sh', shared: 'true')

        miscellaneousConfiguration(cleanupWorkdirAfterBuild: 'true')

        cleanCheckout(repoName: '#repoName')
        prepareFuncArtifacts(jdk: '#jdk')
    }
}

produceWebDriverArtifactsForInstantSetup(['repoName', 'jdk']) {
    job(name: 'Produce webdriver artifacts', key: 'WDARTIFACTS', description: '') {
        requireLinux()
        requireMaven30()
        requireJdk(jdk: '#jdk')

        artifactDefinition(name: 'jira-distribution POM', location: 'jira-distribution/target', pattern: 'pom-transformed.xml', shared: 'true')
        artifactDefinition(name: 'jira-webapp-dist', location: 'jira-components/jira-webapp/target', pattern: 'jira.war', shared: 'true')
        artifactDefinition(name: 'jira-webdriver-tests-runner', location: '', pattern: 'jira-webdriver-tests-runner.zip', shared: 'true')
        artifactDefinition(name: 'jira-webdriver-tests', location: 'jira-webdriver-tests/target', pattern: 'jira-webdriver-tests-*-jar-with-dependencies.jar', shared: 'true')
        artifactDefinition(name: 'prepare-webdriver-tests', location: 'jira-distribution/jira-webdriver-tests-runner', pattern: 'prepare*.sh', shared: 'true')

        miscellaneousConfiguration(cleanupWorkdirAfterBuild: 'true')

        cleanCheckout(repoName: '#repoName')
        prepareWebDriverArtifactsForInstantSetup(jdk: '#jdk')
        excludeIntegrationTests()
    }
}

prepareWebDriverArtifactsForInstantSetup(['jdk']) {
    // enable no-skip-test-compile and pseudo-loc profiles since we need Antarctica lang bundle generated
    task(
            type: 'maven3',
            description: 'Prepares the webdriver test artifacts',
            goal: '${bamboo.produce.artifacts}  -Denforcer.skip=true -Ddestruction.plugin.version=${bamboo.destruction.plugin.version} -Pno-skip-test-compile,pseudo-loc',
            buildJdk: '#jdk',
            mavenExecutable: 'Maven 3.0',
            environmentVariables: 'MAVEN_OPTS="-Xmx1024m -Xms128m -XX:MaxPermSize=256m"',
            hasTests: 'false'
    )
}
