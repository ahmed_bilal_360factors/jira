notifications() {
    notification(type: 'Comment Added', recipient: 'group', group: 'jira-developers')
    notification(type: 'Failed Builds and First Successful', recipient: 'watchers')
}

activeDirectory(['repoName', 'jdk']) {
    job(name: 'TPM Active Directory', key: 'JOB1', description: '') {
        requireWindowsSnowflake()
        requireActiveDirectory()
        requireMaven30()
        requireJdk(jdk: '#jdk')

        artifactDefinition(name: 'All logs', location: '.', pattern: '**/*.log*', shared: 'false')
        artifactDefinition(name: 'Jira Version', location: '', pattern: '**/serverInfo.txt', shared: 'false')

        miscellaneousConfiguration(cleanupWorkdirAfterBuild: 'true')

        cleanCheckout(repoName: '#repoName')
        runActiveDirectoryTests(jdk: '#jdk')
    }
}

runActiveDirectoryTests(['jdk']) {
    task(
            type: 'maven3',
            description: 'Runs TPM tests',
            goal: '-f jira-distribution/tpm-standalone/spm/pom.xml clean install -DJIRA=Trunk -DStandalone -DIntegrationTest=JIRA_ActiveDirectory -Dsnapshot=${bamboo.plugin.builds.jira.6.4.stable.version} -B -e',
            environmentVariables: 'MAVEN_OPTS=${bamboo.maven.opts}',
            buildJdk: '#jdk',
            mavenExecutable: 'Maven 3.0',
            hasTests: 'true',
            testDirectory: '**/target/failsafe-reports/*.xml'
    )
}

openLdap(['repoName', 'jdk']) {
    job(name: 'TPM OpenLDAP', key: 'OPENLDAP', description: '') {
        requireLinux()
        requireMaven30()
        requireJdk(jdk: '#jdk')

        artifactDefinition(name: 'All logs', location: '.', pattern: '**/*.log*', shared: 'false')
        artifactDefinition(name: 'Jira Version', location: '', pattern: '**/serverInfo.txt', shared: 'false')

        miscellaneousConfiguration(cleanupWorkdirAfterBuild: 'true')

        cleanCheckout(repoName: '#repoName')
        runOpenLdapTests(jdk: '#jdk')
    }
}

runOpenLdapTests(['jdk']) {
    task(
            type: 'maven3',
            description: 'Runs TPM tests',
            goal: '-f jira-distribution/tpm-standalone/spm/pom.xml clean install -DJIRA=Trunk -DStandalone -DIntegrationTest=JIRA_OpenLdap -Dsnapshot=${bamboo.plugin.builds.jira.6.4.stable.version} -B -e',
            buildJdk: '#jdk',
            mavenExecutable: 'Maven 3.0',
            hasTests: 'true',
            testDirectory: '**/target/failsafe-reports/*.xml'
    )
}
