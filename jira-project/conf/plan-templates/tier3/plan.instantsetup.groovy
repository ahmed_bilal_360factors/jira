// Depends on:
//     tier3/shortcuts.tier3.groovy
//     ../tier1/shortcuts.func.groovy
//     ../tier1/shortcuts.webdriver.groovy
//     shortcuts.common.groovy

plan(key: 'IS', name: 'Instant Setup', description: 'Test Instant Setup') {
    def jdk = 'JDK 1.7'
    def repoName = 'JIRA 6.4.x branch'

    tier3Project()
    tier3Labels()

    repository(name: repoName)
    notification(type: 'Failed Builds and First Successful', recipient: 'responsible')
    onceADay(time: "00:00")
    globalVariables()

    stage(name: 'Prepare distributions') {
        produceFuncArtifactsForInstantSetup(repoName: repoName, jdk: jdk);
        produceWebDriverArtifactsForInstantSetup(repoName: repoName, jdk: jdk);
    }

    stage(name: 'Test Instant Setup', manual: 'false') {
        singleInstantSetupFuncTest(name: 'Test Setup - Greenhopper', index: '01', testClass: 'com.atlassian.jira.webtests.ztests.setup.TestSetupPreinstalledBundlesAgile', jdk: jdk)
        singleInstantSetupFuncTest(name: 'Test Setup - ServiceDesk', index: '02', testClass: 'com.atlassian.jira.webtests.ztests.setup.TestSetupPreinstalledBundleServiceDesk', jdk: jdk)
        singleInstantSetupFuncTest(name: 'Test Setup - Default', index: '03', testClass: 'com.atlassian.jira.webtests.ztests.setup.TestSetupPreinstalledBundleDefault', jdk: jdk)

        singleInstantSetupWebDriverTest(name: 'TestInstantSetup', index: '01', testMethod: 'com.atlassian.jira.webtest.webdriver.tests.setup.TestInstantSetup.testInstantSetupSucceeds', jdk: jdk)
        singleInstantSetupWebDriverTest(name: 'TestLanguageChangeDuringSetup', index: '02', testMethod: 'com.atlassian.jira.webtest.webdriver.tests.setup.TestLanguageChangeDuringSetup.testLanguageDialogChangesLanguage', jdk: jdk)
    }
}
