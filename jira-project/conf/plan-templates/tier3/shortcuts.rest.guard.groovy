notifications() {
    notification(type: 'Comment Added', recipient: 'hipchat', apiKey: '${bamboo.atlassian.hipchat.apikey.password}', room: 'JIRA Dev', notify: 'true')
    notification(type: 'Failed Jobs and First Successful', recipient: 'responsible')
    notification(type: 'Failed Jobs and First Successful', recipient: 'hipchat', apiKey: '${bamboo.atlassian.hipchat.apikey.password}', room: 'JBAC Notifications', notify: 'true')
    notification(type: 'All Builds Completed', recipient: 'stash')
}

restApiGuard(['jiraRepo', 'restClientRepo', 'jdk']) {
    job(name: 'Default Job', key: 'JOB1', description: '') {
        requireLinux()
        requireMaven30()
        requireJdk(jdk: '#jdk')

        checkoutInDir(repoName: '#jiraRepo', dir: 'jira-master')
        checkoutInDir(repoName: '#restClientRepo', dir: 'jrjc')
        installJira(jdk: '#jdk')
        runIntegrationTests(jdk: '#jdk')
    }
}

installJira(['jdk']) {
    task(
            type: 'maven3',
            description: 'install JIRA to .m2',
            goal: 'install -DskipTests=true',
            buildJdk: '#jdk',
            mavenExecutable: 'Maven 3.0',
            environmentVariables: 'MAVEN_OPTS="-Xmx1536m -Xms256m -XX:MaxPermSize=256m"',
            workingSubDirectory: 'jira-master'
    )
}

runIntegrationTests(['jdk']) {
    task(
            type: 'maven3',
            description: 'Run Integration Tests',
            goal: 'clean integration-test -Djira.version=${bamboo.plugin.builds.jira.6.4.stable.version}',
            buildJdk: '#jdk',
            mavenExecutable: 'Maven 3.0',
            environmentVariables: 'MAVEN_OPTS="-Xmx256m -Xms128m"',
            workingSubDirectory: 'jrjc',
            hasTests: 'true',
            testDirectory: '**/target/surefire-reports/*.xml, **/target/group-__no_test_group__/tomcat6x/surefire-reports/*.xml'
    )
}
