package tier3

import com.atlassian.bamboo.plugin.plantemplates.PlanBuilder
import com.atlassian.bamboo.plugin.plantemplates.ShortcutExpander
import com.atlassian.bamboo.plugin.plantemplates.TemplatePrinter

abstract class TpmPlan
{
    def builder = new PlanBuilder(new ShortcutExpander([]))

    protected TpmPlan() {
    }

    protected def name
    protected def key
    protected def description = ''
    protected def requireDatabase
    protected def beforeRunTests = {}
    protected def afterRunTests = {}
    protected def databaseProperty
    protected def batchName
    protected def batchKey
    protected def directory = 'generated/tier3'
    protected def String filename
    protected def environmentVariable = ''
    protected def numberOfBatches = 10
    protected def buildJdk = 'JDK 1.7'
    protected def runJdk = 'JDK 1.7_patched'
    protected def annotatePlan = {}

    def createPlan()
    {
        builder.plan(key: key, name: name, description: description) {

            def repoName = 'JIRA ci-master-1 stash'

            builder.tier3Project()
            builder.tier3Labels()

            dailyBuild(repoName, '18', '0')
            noBranchMonitoring()
            notifications()
            miscellaneousSettings()

            builder.stage(name: 'Build JIRA', manual: 'false') {
                buildJiraFromSources(repoName, buildJdk)
            }

            builder.stage(name: 'Test JIRA', manual: 'false') {
                testsJobs(repoName, runJdk)
            }

            builder.stage(name: 'Cleanup') {
              purgeMavenSandboxJob()
            }
            annotatePlan(builder)
        }
        def File directory = new File(this.directory)
        directory.mkdirs()
        new File(directory, filename)
            .withWriter() { it << new TemplatePrinter().buildDslFromMap(builder.result as Map) }
    }

    def dailyBuild(repoName, scheduledHour, scheduledMin) {
        builder.repository(name: repoName)
        builder.trigger(description: 'daily execution trigger', type: 'cron', cronExpression: "0 $scheduledMin $scheduledHour 1/1 * ? *".toString(), onlyBranchesWithChanges: 'true')
    }

    def noBranchMonitoring() {
        builder.branchMonitoring(
                enabled: 'false',
                notificationStrategy: 'NOTIFY_COMMITTERS',
                remoteJiraBranchLinkingEnabled: 'true'
        )
    }

    def notifications() {
    }

    def miscellaneousSettings() {
        builder.planMiscellaneous() {
            builder.sandbox(enabled:"true", automaticPromotion:"false")
        }
    }

    def buildJiraFromSources(repoName, jdk) {
        builder.job(name: 'Build JIRA from sources', key: 'BJFS', description: '') {
            builder.requireLinux()

            builder.checkout(repoName: repoName, forceClean: 'false')
            deployJira(jdk)
        }
    }

    def deployJira(jdk) {
        builder.task(
                type: 'maven3',
                description: 'Deploys Standalone Distribution',
                goal: 'deploy -B -pl jira-distribution/jira-standalone-distribution,jira-distribution/jira-func-tests-runner,jira-components/jira-plugins/jira-plugin-test-resources,jira-components/jira-plugins/jira-func-test-plugin,jira-components/jira-plugins/jira-reference-plugin,jira-components/jira-plugins/jira-reference-language-pack,jira-components/jira-plugins/jira-reference-dependent-plugin -am -Pdistribution -Dmaven.test.skip -Djira.minify.skip=true -Djira.func.tests.runner.create -DobrRepository=NONE -DskipSources',
                buildJdk: jdk,
                mavenExecutable: 'Maven 3.0',
                environmentVariables: 'MAVEN_OPTS="-Xmx512m -Xms256m -XX:MaxPermSize=256m"',
                hasTests: 'false'
        )
    }

    def abstract testsJobs(repoName, jdk)

    def testsJob(name, key, repoName, jdk, testTarget, batching, require) {
        builder.job(name: name , key: key, description: '') {
            require(builder)

            builder.artifactDefinition(name: 'All Logs', location: '.', pattern: '**/*.log*', shared: 'false')
            builder.artifactDefinition(name: 'Cargo Logs', location: '.', pattern: '**/test-reports/**', shared: 'false')
            builder.artifactDefinition(name: 'Catalina out', location: '.', pattern: '**/*.out', shared: 'false')
            builder.artifactDefinition(name: 'Jira Version', location: '', pattern: '**/serverInfo.txt', shared: 'false')

            builder.miscellaneousConfiguration(cleanupWorkdirAfterBuild: 'true')

            builder.cleanCheckout(repoName: repoName)
            beforeRunTests(this)
            runTests(jdk, testTarget, batching)
            afterRunTests(this)
        }
    }

    def clearLocalMavenRepo() {
        builder.task(
                type: 'script',
                description: 'Clear Cache',
                scriptBody: '''
echo "=== Cleaning JIRA local .m2 cache ==="
if [ -d ~/.m2/repository/com/atlassian/jira ]; then
   rm -rf ~/.m2/repository/com/atlassian/jira
fi'''
        )
    }

    def purgeMavenSandboxJob() {
      builder.job(name: 'Purge sandbox', key: 'PURGESANDBOX', description: 'Purge the Maven sandbox created by this build.') {
        builder.requireLinux()
        builder.requireMaven3()
        builder.requireJdk(jdk: 'JDK 1.7')

        builder.task(
                type: 'script',
                description: 'Purge the Maven sandbox created by this build',
                environmentVariables: 'PATH=${bamboo.capability.system.builder.mvn3.Maven 3.0}/bin/:$PATH JAVA_HOME=${bamboo.capability.system.jdk.JDK 1.7}',
                scriptBody: '''
#!/bin/bash

# Not using a Maven task so Bamboo won't misconfigure the repository url
# ("unknown protocol: dav" exception)
mvn -B -Dsandbox.key=${bamboo.sandbox.key} com.atlassian.maven.plugins:sandbox-maven-plugin:2.1-beta19:delete'''
        )
      }
    }

    def runTests(jdk, testTarget, batching) {
        builder.task(
                type: 'maven3',
                description: 'Runs tests',
                goal: '-f jira-distribution/tpm-standalone/spm/pom.xml clean install -DJIRA=Trunk -DStandalone ' + testTarget + ' ' + batching + ' -Dsnapshot=${bamboo.jira.version} -B -e',
                buildJdk: jdk,
                mavenExecutable: 'Maven 3.0',
                environmentVariables: environmentVariable + 'MAVEN_OPTS="-Xmx1024m -Xms256m -XX:MaxPermSize=256m"',
                hasTests: 'true',
                testDirectory: '**/target/failsafe-reports/*.xml'
        )
    }

    static class TpmPlanBuilder {
        def TpmPlan tpm

        TpmPlanBuilder(TpmPlan tpm) {
            this.tpm = tpm
        }

        def TpmPlanBuilder name(name) {
            tpm.name = name
            return this
        }

        def TpmPlanBuilder key(key) {
            tpm.key = key
            return this
        }

        def TpmPlanBuilder description(description) {
            tpm.description = description
            return this
        }

        def TpmPlanBuilder requireDatabase(requireDatabase) {
            tpm.requireDatabase = requireDatabase
            return this
        }

        def TpmPlanBuilder beforeRunTests(beforeRunTests) {
            tpm.beforeRunTests = beforeRunTests
            return this
        }

        def TpmPlanBuilder afterRunTests(afterRunTests) {
            tpm.afterRunTests = afterRunTests
            return this
        }

        def TpmPlanBuilder databaseProperty(databaseProperty) {
            tpm.databaseProperty = databaseProperty
            return this
        }

        def TpmPlanBuilder batchName(batchName) {
            tpm.batchName = batchName
            return this
        }

        def TpmPlanBuilder batchKey(batchKey) {
            tpm.batchKey = batchKey
            return this
        }

        def TpmPlanBuilder environmentVariable(environmentVariable) {
            tpm.environmentVariable = environmentVariable + ' '
            return this
        }

        def TpmPlanBuilder directory(directory) {
            tpm.directory = directory
            return this
        }

        def TpmPlanBuilder filename(filename) {
            tpm.filename = filename
            return this
        }

        def TpmPlanBuilder runJdk(jdk) {
            tpm.runJdk = jdk
            return this
        }

        def TpmPlanBuilder annotatePlan(annotatePlan) {
            tpm.annotatePlan = annotatePlan
            return this
        }

        def build() {
            tpm.createPlan()
        }
    }
}
