package tier3

/**
 * To install this plan run
 *  mvn compile
 *  node load.js generated/tier3/plan.oracle11g.groovy tier3/shortcuts.tier3.groovy shortcuts.common.groovy
 */
public class Oracle11gPlan
{
    static def build() {
        DatabaseTpmPlan.builder().key('ORACLE11G')
                .name('TPM Oracle11g')
                .batchName('Oracle11g')
                .batchKey('B')
                .requireDatabase({builder ->
                        builder.requireOracle11g()
                })
                .beforeRunTests() {
                        tpm -> tpm.builder.task(
                                type: 'script',
                                description: 'Oracle Workaround',
                                scriptBody: '''
sqlplus -s /nolog <<EOF
connect oracle/oracle
alter system set local_listener = XE;
alter system register;
quit
EOF
rm mem.log
screen -mdS mem_monitor watch -n 30 'echo "--------">>mem.log;date >>mem.log;free -m >>mem.log;ps axo pid,pcpu,rss,size,args | grep -E "java|oracle|RSS" >>mem.log'
''')
                 }
                 .afterRunTests() {
                        tpm -> tpm.builder.task(
                                type: 'script',
                                description: 'Stop memory monitoring',
                                scriptBody: '''
screen -S mem_monitor -X quit
''')
                 }
                .databaseProperty('-DOracle10g')
                .filename('plan.oracle11g.groovy')
                .environmentVariable('TZ="America/New_York"')
                .build()
    }

    public static void main(String[] args)
    {
        build()
    }
}


