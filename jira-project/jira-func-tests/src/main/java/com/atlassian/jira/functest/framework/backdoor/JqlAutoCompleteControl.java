package com.atlassian.jira.functest.framework.backdoor;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

import com.sun.jersey.api.client.WebResource;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Control for /rest/api/2/jql/autocompletedata/suggestions?fieldName=x+fieldValue=z
 */
public class JqlAutoCompleteControl extends BackdoorControl<JqlAutoCompleteControl>
{
    public JqlAutoCompleteControl(final JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public Results getAutoCompleteResults(final String fieldName, final String fieldValue)
    {
        return autoCompleteResource(fieldName, fieldValue).get(Results.class);
    }

    protected WebResource autoCompleteResource(final String fieldName, final String fieldValue)
    {
        return resourceRoot(rootPath).path("rest")
                .path("api")
                .path("2")
                .path("jql")
                .path("autocompletedata")
                .path("suggestions")
                .queryParam("fieldName", fieldName)
                .queryParam("fieldValue", fieldValue);
    }

    public static class Results
    {
        public Results()
        {
        }

        @JsonProperty
        private List<AutoCompleteResult> results = new ArrayList<AutoCompleteResult>();

        public List<AutoCompleteResult> getResults()
        {
            return results;
        }

        @Override
        public String toString()
        {
            return "Results{" +
                    "results=" + results +
                    '}';
        }
    }

    public static class AutoCompleteResult
    {
        @JsonProperty
        private String value;
        @JsonProperty
        private String displayName;

        public AutoCompleteResult()
        {
        }

        public String getValue()
        {
            return value;
        }

        public String getDisplayName()
        {
            return displayName;
        }

        @Override
        public String toString()
        {
            return "AutoCompleteResult{" +
                    "value='" + value + '\'' +
                    ", displayName='" + displayName + '\'' +
                    '}';
        }
    }
}
