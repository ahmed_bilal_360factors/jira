package com.atlassian.jira.functest.framework.assertions;

import com.atlassian.jira.functest.framework.AbstractFuncTestUtil;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.common.base.Function;
import junit.framework.Assert;
import net.sourceforge.jwebunit.WebTester;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.w3c.dom.Element;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nullable;

import static com.atlassian.jira.functest.framework.util.RegexMatchers.regexMatches;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;
import static org.junit.Assert.assertThat;

/**
 * Default implementation of {@link com.atlassian.jira.functest.framework.assertions.ProjectFieldsAssertions}
 *
 * @since 6.4
 */
public class ProjectFieldsAssertionsImpl extends AbstractFuncTestUtil implements ProjectFieldsAssertions
{
    public ProjectFieldsAssertionsImpl(final WebTester tester, final JIRAEnvironmentData environmentData)
    {
        super(tester, environmentData, 2);
    }

    @Override
    public void assertProjectsEquals(final String[] projects)
    {
        try {
            final List<String> projectNamesAvailable = getProjectNames();
            final List<Matcher<? super String>> expectedOptionsMatchers = copyOf(
                    transform(copyOf(projects), new Function<String, Matcher<? super String>>()
                    {
                        @Override
                        public Matcher<String> apply(@Nullable final String input)
                        {
                            return regexMatches(input + " \\(.+\\)");
                        }
                    }));
            assertThat(copyOf(projectNamesAvailable), Matchers.contains(expectedOptionsMatchers));
        }
        catch (JSONException e) {
            Assert.fail("Exception while parsing project field options");
        }
    }

    @Override
    public void assertIssueTypesEquals(final String[] issueTypes)
    {
        try
        {
            List<String> issueTypeNames = getIssueTypeNames();
            Assert.assertEquals(copyOf(issueTypes), copyOf(issueTypeNames));
        }
        catch (JSONException e)
        {
            Assert.fail("Exception while parsing issue type field options");
        }
    }

    @Override
    public void assertIssueTypeIsAvailable(final String issueType)
    {
        try
        {
            List<String> issueTypeNames = getIssueTypeNames();
            Assert.assertTrue(issueTypeNames.contains(issueType));
        }
        catch (JSONException e)
        {
            Assert.fail("Could not check if "+issueType+" is available. Exception occured during parsing of issue type field options.");
        }
    }

    @Override
    public void assertIssueTypeIsSelected(final String issueType)
    {
        try
        {
            String issueTypeId = getIssueTypeId(issueType);
            tester.assertFormElementEquals("issuetype", issueTypeId);
        }
        catch (JSONException e)
        {
            Assert.fail("Could not check if "+issueType+" is selected. Exception occured during parsing of issue type field options.");
        }
    }

    private List<String> getProjectNames() throws JSONException {
        List<String> projectNames = new ArrayList<String>();

        Element options = tester.getDialog().getElement("project-options");
        String jsonProjects = options.getAttribute("data-suggestions");
        JSONArray projects = new JSONArray(jsonProjects);
        JSONObject recentProjects = projects.getJSONObject(0);

        for (int i = 0; i < recentProjects.getJSONArray("items").length(); i++)
        {
            projectNames.add(recentProjects.getJSONArray("items").getJSONObject(i).getString("label"));
        }
        JSONObject allProjects = projects.getJSONObject(1);
        for (int i = 0; i < allProjects.getJSONArray("items").length(); i++)
        {
            projectNames.add(allProjects.getJSONArray("items").getJSONObject(i).getString("label"));
        }

        return projectNames;
    }

    private List<String> getIssueTypeNames() throws JSONException {
        List<String> issueTypeNames = new ArrayList<String>();

        Element options = tester.getDialog().getElement("issuetype-options");
        String jsonIssueTypes = options.getAttribute("data-suggestions");
        JSONArray issueTypes = new JSONArray(jsonIssueTypes);
        for (int i = 0; i < issueTypes.length(); i++)
        {
            JSONObject item = issueTypes.getJSONObject(i);
            if (item.has("items"))
            {
                for (int j = 0; j < item.getJSONArray("items").length(); j++)
                {
                    issueTypeNames.add(item.getJSONArray("items").getJSONObject(j).getString("label"));
                }
            }
            else
            {
                issueTypeNames.add(item.getString("label"));
            }
        }

        return issueTypeNames;
    }

    private String getIssueTypeId(final String issueTypeName) throws JSONException
    {
        Element options = tester.getDialog().getElement("issuetype-options");
        String jsonIssueTypes = options.getAttribute("data-suggestions");
        JSONArray issueTypes = new JSONArray(jsonIssueTypes);

        for (int i = 0; i < issueTypes.length(); i++)
        {
            JSONObject item = issueTypes.getJSONObject(i);
            if (item.has("items"))
            {
                for (int j = 0; j < item.getJSONArray("items").length(); j++)
                {
                    if (issueTypeName.equals(item.getJSONArray("items").getJSONObject(j).getString("label"))) {
                        return item.getString("value");
                    }
                }
            }
            else
            {
                if (issueTypeName.equals(item.getString("label"))) {
                    return item.getString("value");
                }
            }
        }
        return null;
    }
}
