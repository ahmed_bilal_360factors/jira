package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.testkit.client.BackdoorControl;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class DeprecatedWebResourceControl extends BackdoorControl<DeprecatedWebResourceControl>
{
    public DeprecatedWebResourceControl(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public List<String> getTransform1s()
    {
        return getResource().get(DeprecatedDescriptors.class).transform1s;
    }

    public List<String> getCondition1s()
    {
        return getResource().get(DeprecatedDescriptors.class).condition1s;
    }

    private WebResource getResource()
    {
        return createResourceForPath("webResources", "1.0").path("deprecatedDescriptors");
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class DeprecatedDescriptors
    {
        @JsonProperty
        private List<String> condition1s;
        @JsonProperty
        private List<String> transform1s;
    }
}
