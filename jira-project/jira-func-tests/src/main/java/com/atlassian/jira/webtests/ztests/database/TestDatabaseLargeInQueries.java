package com.atlassian.jira.webtests.ztests.database;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.functest.framework.backdoor.EntityEngineControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.framework.util.env.EnvironmentUtils;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.RestFuncTest;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

/**
 * Test that OfBiz handles queries with large 'IN' portions.  e.g. ... where project.id in (1, 2, 3...).
 * Certain databases don't cope well with these sorts of queries and require workarounds.
 *
 * @since 6.4
 */
@WebTest ({ Category.FUNC_TEST, Category.REST, Category.DATABASE })
public class TestDatabaseLargeInQueries extends RestFuncTest
{
    private EnvironmentUtils environmentUtils;

    @Override
    protected void setUpTest()
    {
        environmentUtils = new EnvironmentUtils(getTester(), getEnvironmentData());
        administration.restoreBlankInstance();
    }

    /**
     * Test with a large number of 'IN' values of numeric type.
     */
    public void testLargeInQueryNumeric()
    {
        if (environmentUtils.isOracle())
        {
            System.err.println("Warning: this test is currently ignored in Oracle, awaiting environment fixes.");
            return;
        }

        List<Long> values = new ArrayList<Long>();
        for (long value = 8000L; value < 10001L; value++)
        {
            values.add(value);
        }

        List<Map<String, String>> results = backdoor.entityEngine().findByValueList("Project", "id", values,
                                                ImmutableList.of("id", "name"), EntityEngineControl.ValueType.NUMBER);
        assertEquals(ImmutableList.of(ImmutableMap.of("id", 10000, "name", "homosapien")), results);
    }

    /**
     * Test with a large number of 'IN' values of string type.
     */
    public void testLargeInQueryString()
    {
        List<String> values = new ArrayList<String>();
        values.add("homosapien");
        for (int i = 0; i < 2050; i++)
        {
            values.add(String.valueOf(i));
        }

        List<Map<String, String>> results = backdoor.entityEngine().findByValueList("Project", "name", values,
                                                ImmutableList.of("id", "name"), EntityEngineControl.ValueType.STRING);
        assertEquals(ImmutableList.of(ImmutableMap.of("id", 10000, "name", "homosapien")), results);
    }

    /**
     * The IN(...) fragments each have less than 2000 parameters but in total there are more than 2000 parameters.
     */
    public void testLargeNumberOfParametersWithSmallerInFragments()
    {
        List<String> values = new ArrayList<String>();
        values.add("homosapien");
        for (int i = 0; i < 1200; i++)
        {
            values.add(String.valueOf(i));
        }

        List<Map<String, String>> results = backdoor.entityEngine().findByValueList2Fields("Project", "lead", "name", values,
                                                ImmutableList.of("id", "name"), EntityEngineControl.ValueType.STRING);
        assertEquals(ImmutableList.of(ImmutableMap.of("id", 10000, "name", "homosapien")), results);
    }
}
