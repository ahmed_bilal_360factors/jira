package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import java.io.IOException;
import java.util.List;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response;

import com.atlassian.httpclient.api.HttpStatus;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.ProjectRole;
import com.atlassian.jira.testkit.client.restclient.RoleClient;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;

import com.meterware.httpunit.WebResponse;
import com.sun.jersey.api.client.UniformInterfaceException;

import org.hamcrest.BaseMatcher;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.xml.sax.SAXException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * @since 6.4
 */
@WebTest ({ Category.FUNC_TEST, Category.REST })
public class TestRoleResource extends RestFuncTest
{
    private RoleClient roleClient;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        roleClient = new RoleClient(getEnvironmentData());
        backdoor.restoreBlankInstance();
    }

    public void testGetAllRoles()
    {
        List<ProjectRole> projectRoles = roleClient.get();

        Matcher[] expectedProjectRoles =
                {
                        projectRole(10000L, "Users", "A project role that represents users in a project", "jira-users"),
                        projectRole(10001L, "Developers", "A project role that represents developers in a project", "jira-developers"),
                        projectRole(10002L, "Administrators", "A project role that represents administrators in a project", "jira-administrators")
                };

        Matcher<Iterable<? super ProjectRole>> values = CoreMatchers.hasItems(expectedProjectRoles);
        assertThat(projectRoles, values);
    }

    public void testAnonymousGet()
    {
        try
        {
            roleClient.anonymous().get();
            fail("Anonymous users should not be able to get here");
        }
        catch (UniformInterfaceException e)
        {
            assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), e.getResponse().getStatus());
            e.printStackTrace();
        }
    }

    public void testNormalUserGet()
    {
        try
        {
            backdoor.usersAndGroups().addUser("user");
            roleClient.loginAs("user").get();
            fail("Login users with no admin credentials should not be able to get here");
        }
        catch (UniformInterfaceException e)
        {
            assertEquals(Response.Status.FORBIDDEN.getStatusCode(), e.getResponse().getStatus());
        }
        finally
        {
            backdoor.usersAndGroups().deleteUser("user");
        }

    }

    public void testGetSelfForEachRole() throws IOException, SAXException, JSONException
    {
        List<ProjectRole> projectRoles = roleClient.get();

        for (ProjectRole projectRole : projectRoles)
        {
            //We validate the webresource
            ProjectRole projectRole1 = roleClient.get(projectRole.id.toString());
            assertNotNull(projectRole1);
            assertEquals(projectRole.description, projectRole1.description);
            assertEquals(projectRole.name, projectRole1.name);
            assertEquals(projectRole.self, projectRole1.self);
            assertEquals(projectRole.id, projectRole1.id);

            //We validate the self to see that it returns the same info
            final String url = projectRole.self.replace(getBaseUrl(), ""); //We need to remove the base url from the self
            WebResponse response = GET(url, MapBuilder.<String, String>emptyMap());
            assertThat(response.getResponseCode(), equalTo(200));
            final JSONObject jsonResponse = new JSONObject(response.getText());
            assertEquals(projectRole.description, jsonResponse.get("description"));
            assertEquals(projectRole.name, jsonResponse.get("name"));
            assertEquals(projectRole.self, jsonResponse.get("self"));
            assertEquals(projectRole.id.longValue(), jsonResponse.getLong("id"));
        }
    }

    public void testGetNonExistingRole() throws IOException, SAXException
    {
        //Testing the 404 in the rest api
        WebResponse response = GET(getBaseUrl() + "/role/12312321312", MapBuilder.<String, String>emptyMap());
        assertEquals(HttpStatus.NOT_FOUND.code, response.getResponseCode());
    }

    private BaseMatcher<ProjectRole> projectRole(
            @Nonnull final Long id, @Nonnull final String name, @Nonnull final String description, @Nonnull final String actorName)
    {
        return new BaseMatcher<ProjectRole>()
        {
            @Override
            public boolean matches(final Object o)
            {
                if (!(o instanceof ProjectRole))
                {
                    return false;
                }

                final ProjectRole changedValue = (ProjectRole) o;
                boolean result = name.equals(changedValue.name)
                        && id.equals(changedValue.id)
                        && description.equals(changedValue.description)
                        && containsActor(changedValue.actors, actorName);
                return result;
            }

            @Override
            public void describeTo(final Description descr)
            {
                descr.appendText(new ProjectRole().name(name).id(id).description(description).toString());
            }

            private boolean containsActor(final List<ProjectRole.Actor> actors, final String actor)
            {
                for (ProjectRole.Actor roleActor : actors)
                {
                    if (actor.equalsIgnoreCase(roleActor.name))
                    {
                        return true;
                    }
                }
                return false;
            }
        };
    }
}
