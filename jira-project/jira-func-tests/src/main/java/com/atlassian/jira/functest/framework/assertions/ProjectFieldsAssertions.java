package com.atlassian.jira.functest.framework.assertions;

/**
 * Assertions for project and issue type fields
 *
 * @since v6.4
 */
public interface ProjectFieldsAssertions
{
    /**
     * Checks whether provided projects are the only ones available for selection in project picker field
     *
     * @param projects Array of project names
     */
    void assertProjectsEquals(String[] projects);

    /**
     * Checks if provided issue types are the only ones available for selection
     *
     * @param issueTypes Array of issue types names
     */
    void assertIssueTypesEquals(String[] issueTypes);

    /**
     * Checks if provided issue type is available for selection in issue type select
     *
     * @param issueType Issue type name
     */
    void assertIssueTypeIsAvailable(String issueType);

    /**
     * Checks whether provided issue type is selected
     *
     * @param issueType Issue type name
     */
    void assertIssueTypeIsSelected(String issueType);
}
