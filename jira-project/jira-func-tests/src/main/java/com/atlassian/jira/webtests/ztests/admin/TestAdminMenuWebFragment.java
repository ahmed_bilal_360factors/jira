package com.atlassian.jira.webtests.ztests.admin;

import java.util.Arrays;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;

import com.google.common.collect.ImmutableList;

/**
 * This test asserts that the links in the admin menu is available/unavailable under various conditions.
 *
 * To test just the admin menu, this test case only uses users with permission to goto the administration section
 *
 * To keep it simple, the test xml file only changed the group settings (and not any roles)
 */
@WebTest ({Category.FUNC_TEST, Category.ADMINISTRATION, Category.BROWSING })
public class TestAdminMenuWebFragment extends FuncTestCase
{
    public void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestWebFragment.xml");
    }

    public void tearDownTest()
    {
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        administration.restoreBlankInstance();
        super.tearDownTest();
    }

    public void testAdminMenuWebFragment()
    {
        _testSystemAdminCanSeeAllAdminSections();
        _testProjectAdminCanSeeProjectSectionOnly();
        _testOtherUsersCannotSeeAdminSections();
    }

    /**
     * Test that all administrative sections are AVAILABLE when the user IS a system administrator
     */
    public void _testSystemAdminCanSeeAllAdminSections()
    {
        //assert system administrators can see all the sections
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
        navigation.gotoAdmin();
        assertAdminLinksAreVisible();
    }

    /**
     * Test that the system administrator can only see the project section
     */
    public void _testProjectAdminCanSeeProjectSectionOnly()
    {
        //assert project administrator can only see the project section
        navigation.login("project_admin", "project_admin");
        navigation.gotoAdmin();
        assertAdminLinksAreNotVisible();
    }

    /**
     * Test that all other users (including not logged in) can only see project section with restriction message
     */
    public void _testOtherUsersCannotSeeAdminSections()
    {
        //users should not be able to view the Admin link - so go there directly
        navigation.login("user", "user");
        tester.gotoPage("/secure/project/ViewProjects.jspa");
        tester.assertTextPresent("You do not have the permissions to administer any projects, or there are none created.");
        assertAdminLinksAreNotVisible();

        //non-logged-in users should not be able to view the Admin link - so go there directly
        navigation.logout();
        tester.gotoPage("/secure/project/ViewProjects.jspa");
        assertions.assertNodeHasText(new CssLocator(tester, ".aui-message.warning"),"If you log in or sign up for an account, you might be able to see more here.");
        assertions.assertNodeByIdDoesNotExist("adminMenu"); // Users that are not logged in shouldn't see any admin menu
    }

    public void assertAdminLinksAreVisible()
    {
        tester.assertElementPresent("admin-nav-heading");
    }

    public void assertAdminLinksAreNotVisible()
    {
        tester.assertButtonNotPresent("admin-nav-heading");
    }
}
