package com.atlassian.jira.functest.rule;

import java.io.File;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.testkit.client.AttachmentsControl;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;

import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;

/**
 * Creates {@link org.junit.rules.TestRule} instances.
 *
 * @since v6.4
 */
public class Rules
{
    /**
     * Prepares attachment files, but not database entries.
     *
     * @param environmentData Environment, which specifies attachment source and target directory
     * @param sourceSubPath Path relative to {@code environmentData.getXMLDataLocation()} (typically
     * ".../jira-webdriver-tests/src/main/xml/")
     */
    public static TestRule prepareAttachments(final JIRAEnvironmentData environmentData, final String sourceSubPath)
    {
        final Backdoor backdoor = new Backdoor(environmentData);
        final AttachmentsControl attachmentsControl = backdoor.attachments();
        return chain()
                .around(enableAttachments(attachmentsControl))
                .around(cleanAttachments(attachmentsControl))
                .around(copyAttachments(environmentData, sourceSubPath, attachmentsControl));
    }

    public static RuleChain chain()
    {
        return RuleChain.emptyRuleChain();
    }

    public static TestRule enableAttachments(final AttachmentsControl attachmentsControl)
    {
        return new EnableAttachmentsRule(attachmentsControl);
    }

    public static TestRule cleanAttachments(final AttachmentsControl attachmentsControl)
    {
        return new CleanAttachmentsRule(attachmentsControl);
    }

    public static TestRule copyAttachments(
            final JIRAEnvironmentData environmentData,
            final String sourceSubPath,
            final AttachmentsControl attachmentsControl
    )
    {
        return new CopyAttachmentsRule(environmentData, sourceSubPath, attachmentsControl);
    }

    public static TestRule cleanDirectory(final File directory)
    {
        return new CleanDirectoryRule(directory);
    }

    public static TestRule copyDirectory(final File sourceDirectory, final File targetDirectory)
    {
        return new CopyDirectoryRule(sourceDirectory, targetDirectory);
    }

}
