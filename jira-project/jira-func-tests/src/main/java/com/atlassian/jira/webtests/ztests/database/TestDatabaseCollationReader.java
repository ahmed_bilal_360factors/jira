package com.atlassian.jira.webtests.ztests.database;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.parser.SystemInfoParser;
import com.atlassian.jira.functest.framework.parser.SystemInfoParserImpl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isOneOf;
import static org.hamcrest.Matchers.not;

/**
 * A func test which checks if the database collation was read successfully
 *
 * @since v6.4
 */
@WebTest ({ Category.FUNC_TEST, Category.DATABASE })
public class TestDatabaseCollationReader extends FuncTestCase
{
    private SystemInfoParser.SystemInfo systemInfo;

    protected void setUpTest()
    {
        administration.restoreBlankInstance();
        systemInfo = new SystemInfoParserImpl(getTester(), navigation).getSystemInfo();
    }

    public void testDatabaseCollationReadSuccessfully()
    {
        if (getDatabaseType().startsWith("hsql"))
        {
            // Reading HSQL collations is not possible yet
            return;
        }

        String databaseCollation = systemInfo.getProperty("Database collation");

        assertThat("Could not read collation for database type " + getDatabaseType(), databaseCollation, not(isOneOf(null, "", "null", "Unknown")));
    }

    private String getDatabaseType() {
        return systemInfo.getDatabaseType();
    }
}
