package com.atlassian.jira.functest.framework.backdoor;

import java.util.Arrays;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

import com.google.common.collect.Lists;
import com.sun.jersey.api.client.WebResource;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Control for rest/api/2/jql/autocompletedata.
 */
public class SearchAutoCompleteControl extends BackdoorControl<SearchAutoCompleteControl>
{
    public SearchAutoCompleteControl(final JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public AutoCompleteData getAutoCompleteData()
    {
        return createResource().path("jql").path("autocompletedata").get(AutoCompleteData.class);
    }

    @Override
    protected WebResource createResource()
    {
        return resourceRoot(rootPath).path("rest").path("api").path("2");
    }

    public static class AutoCompleteData
    {
        public AutoCompleteData()
        {
        }

        @JsonProperty
        private AutoCompleteField[] visibleFieldNames;

        public AutoCompleteData(final AutoCompleteField[] visibleFieldNames)
        {
            this.visibleFieldNames = visibleFieldNames;
        }

        public Iterable<AutoCompleteField> getVisibleFieldNames()
        {
            return Lists.newArrayList(visibleFieldNames);
        }
    }

    public static class AutoCompleteField
    {
        @JsonProperty
        private String value;
        @JsonProperty
        private String displayName;
        @JsonProperty
        private String auto = "false";
        @JsonProperty
        private String searchable = "false";
        @JsonProperty
        private String[] operators;
        @JsonProperty
        private String[] types;

        public AutoCompleteField()
        {
        }


        public AutoCompleteField(final String value, final String displayName, final String auto, final String searchable, final String[] operators, final String[] types)
        {
            this.value = value;
            this.displayName = displayName;
            this.auto = auto;
            this.searchable = searchable;
            this.operators = operators;
            this.types = types;
        }

        public String getValue()
        {
            return value;
        }

        public String getDisplayName()
        {
            return displayName;
        }

        public String getAuto()
        {
            return auto;
        }

        public String getSearchable()
        {
            return searchable;
        }

        public String[] getOperators()
        {
            return operators;
        }

        public String[] getTypes()
        {
            return types;
        }

        @Override
        public boolean equals(final Object o)
        {
            if (this == o) { return true; }
            if (o == null || getClass() != o.getClass()) { return false; }

            final AutoCompleteField that = (AutoCompleteField) o;

            if (auto != null ? !auto.equals(that.auto) : that.auto != null) { return false; }
            if (displayName != null ? !displayName.equals(that.displayName) : that.displayName != null)
            {
                return false;
            }
            if (!Arrays.equals(operators, that.operators)) { return false; }
            if (searchable != null ? !searchable.equals(that.searchable) : that.searchable != null) { return false; }
            if (!Arrays.equals(types, that.types)) { return false; }
            if (value != null ? !value.equals(that.value) : that.value != null) { return false; }

            return true;
        }

        @Override
        public int hashCode()
        {
            int result = value != null ? value.hashCode() : 0;
            result = 31 * result + (displayName != null ? displayName.hashCode() : 0);
            result = 31 * result + (auto != null ? auto.hashCode() : 0);
            result = 31 * result + (searchable != null ? searchable.hashCode() : 0);
            result = 31 * result + (operators != null ? Arrays.hashCode(operators) : 0);
            result = 31 * result + (types != null ? Arrays.hashCode(types) : 0);
            return result;
        }

        @Override
        public String toString()
        {
            return "AutoCompleteField{" +
                    "value='" + value + '\'' +
                    ", displayName='" + displayName + '\'' +
                    ", auto='" + auto + '\'' +
                    ", searchable='" + searchable + '\'' +
                    ", operators=" + Arrays.toString(operators) +
                    ", types=" + Arrays.toString(types) +
                    '}';
        }
    }
}
