package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Some simple tests to make sure the services admin pages do not explode.
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.BROWSING })
public class TestServices extends FuncTestCase
{

    private static final String SERVICE_CLASS = "com.atlassian.jira.service.services.DebugService";
    private static final String SERVICE_NAME = "Test Debug Service";
    private static final String SERVICE_PARAM_DESC_1 = "How long to run";
    private static final String SERVICE_PARAM_DESC_2 = "Debug param one";
    private static final String HELLO_WORLD_1 = "hello world 1";
    private static final String HELLO_WORLD_2 = "hello world 2";
    private static final String GOODBYE_WORLD_1 = "goodbye world 1";

    public void setUpTest()
    {
        administration.restoreBlankInstance();
    }

    public void testServiceRunner()
    {
        administration.utilities().runServiceNow(10000);
    }

    public void testAddServiceWithParams()
    {
        navigation.gotoAdminSection(Navigation.AdminSection.SERVICES);

        tester.setFormElement("name", SERVICE_NAME);
        tester.setFormElement("clazz", SERVICE_CLASS);
        tester.submit("Add Service");

        tester.assertTextPresent(SERVICE_PARAM_DESC_1);
        tester.assertTextPresent(SERVICE_PARAM_DESC_2);

        final String urlString = tester.getDialog().getResponse().getURL().toExternalForm();
        final Matcher matcher = Pattern.compile("EditService!default.jspa?.*id=(\\d+)").matcher(urlString);

        if (!matcher.find())
        {
            fail("Did not get redirected to EditService!default.jspa");
        }

        final long id = Long.parseLong(matcher.group(1));        

        tester.setFormElement("debug param one", HELLO_WORLD_1);
        tester.setFormElement("debug param two", HELLO_WORLD_2);
        tester.selectOption("service.schedule.interval", "every 2 hours");
        tester.selectOption("service.schedule.runToHours", "5");
        tester.selectOption("service.schedule.runToMeridian", "pm");
        tester.submit("Update");

        tester.assertTextPresent(HELLO_WORLD_2);
        tester.assertTextPresent(HELLO_WORLD_1);
        tester.assertTextPresent("Daily every 2 hours from 1:00 am to 5:00 pm");


        // Now test the edit
        tester.clickLink("edit_" + id);
        tester.setFormElement("debug param one", GOODBYE_WORLD_1);
        tester.checkRadioOption("service.schedule.dailyWeeklyMonthly", "advanced");
        tester.setFormElement("service.schedule.cronString", "0 * * * * ?");
        tester.submit("Update");

        tester.assertTextPresent(GOODBYE_WORLD_1);
        tester.assertTextPresent("0 * * * * ?");
        tester.assertTextNotPresent(HELLO_WORLD_1);

        // Now delete the service we just added.
        tester.clickLink("del_" + id);

        // Now make sure the service is not there anymore
        tester.assertTextNotPresent(HELLO_WORLD_2);
        tester.assertTextNotPresent(HELLO_WORLD_1);
    }
}
