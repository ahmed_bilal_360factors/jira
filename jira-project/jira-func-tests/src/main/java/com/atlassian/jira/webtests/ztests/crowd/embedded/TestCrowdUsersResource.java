package com.atlassian.jira.webtests.ztests.crowd.embedded;

import com.atlassian.crowd.acceptance.tests.rest.service.UsersResourceTest;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.crowd.plugin.rest.entity.MultiValuedAttributeEntity;
import com.atlassian.crowd.plugin.rest.entity.MultiValuedAttributeEntityList;
import com.atlassian.crowd.plugin.rest.entity.UserEntity;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.util.EnvironmentAware;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.common.collect.Sets;
import com.sun.jersey.api.client.WebResource;

import java.net.URI;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;

/**
 * Acceptance tests for the Crowd users resource.
 *
 * @since v4.3
 */
@WebTest ({ Category.FUNC_TEST, Category.USERS_AND_GROUPS })
public class TestCrowdUsersResource extends UsersResourceTest implements EnvironmentAware
{
    private static final String USERNAME_PARAM = "username";
    private static final String KEY_PARAM = "key";
    private static final String EXTERNAL_ID = "externalId"; // external id of the 'admin' user
    private static final String EXTERNAL_ID_WITH_DIRECTORY = "2:" + EXTERNAL_ID; // with the directory prefix


    /**
     * Constructs a test case with the given name.
     *
     * @param name the test name
     */
    public TestCrowdUsersResource(String name)
    {
        super(name, new CrowdEmbeddedServer().usingXmlBackup(CrowdEmbeddedServer.XML_BACKUP));
    }

    @Override
    public void setEnvironmentData(JIRAEnvironmentData environmentData)
    {
        setRestServer(new CrowdEmbeddedServer(environmentData).usingXmlBackup(CrowdEmbeddedServer.XML_BACKUP));
    }

    @Override
    public void testGetUserWithAttributes()
    {
        // DISABLED because the functionality is not supported in JIRA
    }

    @Override
    public void testGetNestedGroups()
    {
        // DISABLED because the functionality is not supported in JIRA
    }

    @Override
    public void testGetDirectGroups()
    {
        // DISABLED because the functionality is not supported in JIRA
    }

    /**
     * Below code is copied from base class and adapted to run against JIRA. Base class contains hardcoded values
     * in assertions which are not same in JIRA and Crowd. This test overrides corresponding test from base
     * class and is asserting data present in {@link CrowdEmbeddedServer#xmlBackup}.
     */
    public void testGetUserWithAttributesByKey()
    {
        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE)
                .queryParam(KEY_PARAM, "{key}").queryParam("expand", "attributes")
                .build(EXTERNAL_ID_WITH_DIRECTORY);
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        assertValidUserWithAttributes(webResource);
        assertEquals(userResourceByNameUri("admin"), webResource.get(UserEntity.class).getLink().getHref());
    }

    private URI userResourceByNameUri(String username)
    {
        return getBaseUriBuilder().path(USERS_RESOURCE).queryParam(USERNAME_PARAM, "{username}").build(username);
    }

    private static void assertValidUserWithAttributes(WebResource webResource)
    {
        UserEntity user = webResource.get(UserEntity.class);

        assertEquals("admin", user.getName());
        assertEquals("bob", user.getFirstName());
        assertEquals("the builder", user.getLastName());
        assertEquals("bob the builder", user.getDisplayName());
        assertEquals("bob@example.net", user.getEmail());
        assertEquals(EXTERNAL_ID_WITH_DIRECTORY, user.getKey());
        assertTrue(user.isActive().booleanValue());

        final MultiValuedAttributeEntityList attributeList = user.getAttributes();
        assertNotNull(attributeList);

        final Set<String> attributeNames = Sets.newHashSet();
        for (MultiValuedAttributeEntity attribute : attributeList)
        {
            attributeNames.add(attribute.getName());
        }

        assertThat(attributeNames, hasItems(
                UserConstants.INVALID_PASSWORD_ATTEMPTS,
                UserConstants.LAST_AUTHENTICATED,
                UserConstants.REQUIRES_PASSWORD_CHANGE));
    }
}
