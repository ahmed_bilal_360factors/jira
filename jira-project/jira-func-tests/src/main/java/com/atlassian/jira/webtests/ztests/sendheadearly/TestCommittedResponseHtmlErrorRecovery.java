package com.atlassian.jira.webtests.ztests.sendheadearly;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;

import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;

/**
 *
 * @since 6.4
 */
@WebTest ({ Category.FUNC_TEST, Category.HTTP })
public class TestCommittedResponseHtmlErrorRecovery extends FuncTestCase
{
    private static final String OPENING_HTML_TAG = "<html";   // Should be unique enough. Full tag probably looks something like: <html lang="en">
    private static final String CLOSING_HTML_TAG = "</html>";
    private static final String MAGIC_JAVASCRIPT_MARKER = "\'\"-->>]]>";
    private static final String POST_FLUSH_CONTENT = "POST_FLUSH_CONTENT";

    @Override
    protected void setUpTest()
    {
        administration.restoreBlankInstance();
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Override
    protected void tearDownTest()
    {
        navigation.logout();
    }

    public void testWithFlushAndContent()
    {
        tester.gotoPage("/CommittedResponseExceptionThrowingAction.jspa");
        String response = tester.getDialog().getResponseText();

        assertNotNull("Expected a response", response);
        assertThat("Expected to see an opening HTML tag in the response", response, containsString(OPENING_HTML_TAG));
        assertThat("Expected to NOT see a closing HTML tag in the response", response, not(containsString(CLOSING_HTML_TAG)));
        assertThat("Expected to see magic javascript injected in the response", response, containsString(MAGIC_JAVASCRIPT_MARKER));
        assertThat("Expected to NOT to see any post-flush content in the response", response, not(containsString(POST_FLUSH_CONTENT)));
    }
}
