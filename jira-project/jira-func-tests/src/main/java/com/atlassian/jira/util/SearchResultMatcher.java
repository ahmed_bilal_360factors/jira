package com.atlassian.jira.util;

import java.util.List;
import java.util.Set;

import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.SearchResult;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 * Matcher for {@link SearchResult} from testkit.
 */
public class SearchResultMatcher
{
    public static TypeSafeMatcher<SearchResult> issues(final String... issueKeys)
    {
        return issues(Lists.newArrayList(issueKeys));
    }

    public static TypeSafeMatcher<SearchResult> issues(final List<String> issueKeys)
    {
        return issues(Sets.newHashSet(issueKeys));
    }

    private static TypeSafeMatcher<SearchResult> issues(final Set<String> issueKeys)
    {
        return new TypeSafeMatcher<SearchResult>()
        {
            @Override
            protected boolean matchesSafely(final SearchResult searchResult)
            {
                return searchResult.total == issueKeys.size() && Iterables.all(searchResult.issues, new Predicate<Issue>()
                {
                    @Override
                    public boolean apply(final Issue issue)
                    {
                        return issueKeys.contains(issue.key);
                    }
                });
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("Search result does not contain issues").appendValue(issueKeys);
            }
        };
    }

}
