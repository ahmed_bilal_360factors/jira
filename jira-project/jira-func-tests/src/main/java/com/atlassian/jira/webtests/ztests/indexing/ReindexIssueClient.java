package com.atlassian.jira.webtests.ztests.indexing;

import javax.ws.rs.core.MediaType;

import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class ReindexIssueClient extends RestApiClient
{
    private final JIRAEnvironmentData environmentData;

    public ReindexIssueClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
        this.environmentData = environmentData;
    }

    public Response<?> reindexIssue(final String... issueIds)
    {
        return toResponse(new Method()
        {
            @Override
            public ClientResponse call()
            {
                WebResource request = getReindexResource();
                for (String issueId : issueIds)
                {
                    request = request.queryParam("issueId", issueId);
                }
                return request.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);
            }
        });
    }

    private WebResource getReindexResource()
    {
        return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("rest").path("api").path("2").path("reindex").path("issue");
    }
}
