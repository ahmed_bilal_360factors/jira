package com.atlassian.jira.webtests.ztests.timetracking.modern;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import com.atlassian.jira.testkit.client.restclient.Visibility;
import com.atlassian.jira.testkit.client.restclient.Worklog;
import com.atlassian.jira.testkit.client.restclient.WorklogClient;
import com.atlassian.jira.webtests.Groups;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Func test for searching worklogs by date.
 *
 * @since v6.4
 */
@WebTest ({ Category.FUNC_TEST, Category.REST, Category.WORKLOGS })
public class TestWorklogDateSearching extends AbstractWorklogSearchingTest
{
    public void testEqualOperators() throws Exception
    {
        final String secondStartDate = "2014-08-11";
        final String otherStartDate = "2014-08-14";
        final String issueKey1 = createDefaultIssue();
        final String issueKey2 = createDefaultIssue();
        createWorklog(issueKey1, defaultStartDate);
        createWorklog(issueKey2, secondStartDate);

        SearchResult result = executeJqlSearch("worklogDate = " + defaultStartDate);
        assertThat(result, sizeEquals(1));

        String query = "worklogDate in (" + defaultStartDate + ", " + secondStartDate + ", " + otherStartDate + ")";
        result = executeJqlSearch(query);
        assertThat(result, sizeEquals(2));
    }

    public void testGreaterThanOperator() throws Exception
    {
        final String startDate = "2014-08-10";
        final String lowerStartDate = "2014-08-09";
        final String issueKey = createDefaultIssue();
        createWorklog(issueKey, startDate);

        SearchResult result = executeJqlSearch("worklogDate > " + startDate);

        assertThat(result, sizeEquals(0));

        result = executeJqlSearch("worklogDate > " + lowerStartDate);
        assertThat(result, sizeEquals(1));
    }

    public void testGreaterThenOrEqualsOperator()
    {
        final String startDate = "2014-08-10";
        final String upperStartDate = "2014-08-11";
        final String issueKey = createDefaultIssue();
        createWorklog(issueKey, startDate);

        SearchResult result = executeJqlSearch("worklogDate >= " + startDate);
        assertThat(result, sizeEquals(1));

        result = executeJqlSearch("worklogDate >= " + upperStartDate);
        assertThat(result, sizeEquals(0));
    }

    public void testLessThenOperator()
    {
        final String startDate = "2014-08-10";
        final String upperStartDate = "2014-08-11";
        final String issueKey = createDefaultIssue();
        createWorklog(issueKey, startDate);

        SearchResult result = executeJqlSearch("worklogDate < " + startDate);
        assertThat(result, sizeEquals(0));

        result = executeJqlSearch("worklogDate < " + upperStartDate);
        assertThat(result, sizeEquals(1));
        assertThat(result, containsIssues(issueKey));
    }

    public void testLessThenOrEquals()
    {
        final String startDate = "2014-08-10";
        final String lowerStartDate = "2014-08-09";
        final String issueKey = createDefaultIssue();
        createWorklog(issueKey, startDate);

        SearchResult result = executeJqlSearch("worklogDate <= " + startDate);
        assertThat(result, sizeEquals(1));

        result = executeJqlSearch("worklogDate <= " + lowerStartDate);
        assertThat(result, sizeEquals(0));
    }

    public void testWhetherConditionsAreSatisfiedForSingleWorklog() throws Exception
    {
        final String issueWithWorklogOn10th = createDefaultIssue();
        createWorklog(issueWithWorklogOn10th, "2014-08-10");

        final String issueWithWorklogOn13th = createDefaultIssue();
        createWorklog(issueWithWorklogOn13th, "2014-08-13");

        SearchResult result = executeJqlSearch("worklogDate > 2014-08-11 AND worklogDate < 2014-08-12");
        assertThat(result, sizeEquals(0));

        result = executeJqlSearch("worklogDate > 2014-08-14 AND worklogDate < 2014-08-09");
        assertThat(result, sizeEquals(0));

        result = executeJqlSearch("worklogDate > 2014-08-09 AND worklogDate < 2014-08-11");
        assertThat(result, sizeEquals(1));
        assertThat(result, containsIssues(issueWithWorklogOn10th));

        result = executeJqlSearch("worklogDate > 2014-08-12 AND worklogDate < 2014-08-14");
        assertThat(result, sizeEquals(1));
        assertThat(result, containsIssues(issueWithWorklogOn13th));

        result = executeJqlSearch("worklogDate > 2014-08-09 AND worklogDate < 2014-08-14");
        assertThat(result, sizeEquals(2));
        assertThat(result, containsIssues(issueWithWorklogOn10th, issueWithWorklogOn13th));
    }

    public void testWorklogServedAsString() throws Exception
    {
        final String startDate = "2014-08-10";
        final String issueKey = createDefaultIssue();
        createWorklog(issueKey, startDate);

        SearchResult result = executeJqlSearch("worklogDate = " + startDate);

        assertThat(result, sizeEquals(1));
        assertThat(result, containsIssues(issueKey));
    }

    public void testWorklogServedAsJql() throws Exception
    {
        final String issueKey = createDefaultIssue();
        final Worklog worklog = new Worklog();
        worklog.started = new SimpleDateFormat(TIME_FORMAT).format(new Date());
        worklog.timeSpent = "1h";
        worklogClient.post(issueKey, worklog);

        SearchResult result = executeJqlSearch("worklogDate = now()");

        assertThat(result, sizeEquals(1));
        assertThat(result, containsIssues(issueKey));
    }

    public void testDuplicatedRelationalOperators() throws Exception
    {
        final String issueCreatedOn11th = createDefaultIssue();
        createWorklog(issueCreatedOn11th, "2014-08-11");

        final String issueCreatedOn10th = createDefaultIssue();
        createWorklog(issueCreatedOn10th, "2014-08-10");

        final String issueCreatedOn13th = createDefaultIssue();
        createWorklog(issueCreatedOn13th, "2014-08-13");

        SearchResult result = executeJqlSearch("worklogDate > 2014-08-09 AND worklogDate > 2014-08-10");
        assertThat(result, containsIssues(issueCreatedOn11th, issueCreatedOn13th));
        assertThat(result, sizeEquals(2));

        result = executeJqlSearch("worklogDate > 2014-08-10 AND worklogDate > 2014-08-12");
        assertThat(result, sizeEquals(1));
        assertThat(result, containsIssues(issueCreatedOn13th));

        result = executeJqlSearch("worklogDate < 2014-08-13 AND worklogDate < 2014-08-12");
        assertThat(result, sizeEquals(2));
        assertThat(result, containsIssues(issueCreatedOn10th, issueCreatedOn11th));

        result = executeJqlSearch("worklogDate > 2014-08-09 AND worklogDate > 2014-08-10\n"
                + " AND worklogDate < 2014-08-13 and worklogDate < 2014-08-12");
        assertThat(result, sizeEquals(1));
        assertThat(result, containsIssues(issueCreatedOn11th));
    }

    public void testComplexQuery() throws Exception
    {
        final String reporter = "admin";
        final String projectKey1 = "HSP";
        final String projectKey2 = "MKY";
        final String startDate = "2014-08-10";
        final String upperStartDate = "2014-08-11";
        final String lowerStartDate = "2014-08-09";
        String issueKey = backdoor.issues().createIssue(projectKey1, "test issue!", reporter).key;
        backdoor.issues().createIssue(projectKey2, "test issue!", reporter);
        createWorklog(issueKey, startDate);

        final String rangeQuery = "(worklogDate > " + lowerStartDate + " AND worklogDate < " + upperStartDate + ")";
        final String falseQuery = "worklogDate > 2222-08-10";
        final String compoundQuery = "(reporter = " + reporter + " and " + rangeQuery + ")";
        final String project2Query = "project = " + projectKey2;

        SearchResult result;
        result = executeJqlSearch(rangeQuery + " and " + falseQuery + " and " + compoundQuery);
        assertThat(result.total, equalTo(0));

        result = executeJqlSearch(rangeQuery + " or " + falseQuery + " and " + compoundQuery);
        assertThat(result.total, equalTo(1));

        result = executeJqlSearch(rangeQuery + " or " + falseQuery + " and " + compoundQuery + " or " + project2Query);
        assertThat(result.total, equalTo(2));
    }

    public void testWhenTimeTrackingIsOff() throws Exception
    {
        final String issueKey = createDefaultIssue();
        createWorklog(issueKey, defaultStartDate);

        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_TIMETRACKING, false);
        int responseCode = getResponseCode("worklogDate=" + defaultStartDate);
        assertThat(responseCode, equalTo(400));

        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_TIMETRACKING, true);
        responseCode = getResponseCode("worklogDate=" + defaultStartDate);
        assertThat(responseCode, equalTo(200));
    }

    public void testWhetherPermissionsToProjectAreRespected() throws Exception
    {
        String issueKey = createDefaultIssue();
        createWorklog(issueKey, defaultStartDate);

        backdoor.permissionSchemes().removeGroupPermission(DEFAULT_PERMISSION_SCHEME_ID, ProjectPermissions.BROWSE_PROJECTS, Groups.USERS);
        int responseCode = getResponseCode("worklogDate=" + defaultStartDate);
        assertThat(responseCode, equalTo(400));

        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERMISSION_SCHEME_ID, ProjectPermissions.BROWSE_PROJECTS, Groups.USERS);
        responseCode = getResponseCode("worklogDate=" + defaultStartDate);
        assertThat(responseCode, equalTo(200));
    }

    public void testWhetherPermissionsToVisibleForGroupAreRespected() throws Exception
    {
        final String currentUser = "admin";
        final String groupName = "Test-Group";
        backdoor.usersAndGroups().addGroup(groupName);
        backdoor.usersAndGroups().addUserToGroup(currentUser, groupName);

        String issueKey2 = backdoor.issues().createIssue("HSP", "test issue!", currentUser).key;
        final Worklog worklog = new Worklog();
        worklog.started = getDateFromString(defaultStartDate);
        worklog.visibility = new Visibility("group", groupName);
        worklog.timeSpent = "1h";
        worklogClient.post(issueKey2, worklog);

        SearchResult result = executeEqualWorklogDateSearchAsUser(defaultStartDate, currentUser);
        assertThat(result.total, equalTo(1));

        backdoor.usersAndGroups().removeUserFromGroup(currentUser, groupName);
        result = executeEqualWorklogDateSearchAsUser(defaultStartDate, currentUser);
        assertThat(result.total, equalTo(0));
    }

    public void testWhetherPermissionsToVisibleForRoleAreRespected() throws Exception
    {
        final String currentUser = "admin";
        final String projectKey = "HSP";
        backdoor.projectRole().addActors(projectKey, JIRA_USERS_ROLE, null, new String[] { currentUser });
        String issueKey = backdoor.issues().createIssue(projectKey, "test issue!", currentUser).key;

        final Worklog worklog = new Worklog();
        worklog.started = getDateFromString(defaultStartDate);
        worklog.visibility = new Visibility("role", JIRA_USERS_ROLE);
        worklog.timeSpent = "1h";
        worklogClient.post(issueKey, worklog);

        SearchResult result = executeEqualWorklogDateSearchAsUser(defaultStartDate, currentUser);
        assertThat(result.total, equalTo(1));

        backdoor.projectRole().deleteUser(projectKey, JIRA_USERS_ROLE, currentUser);
        result = executeEqualWorklogDateSearchAsUser(defaultStartDate, currentUser);
        assertThat(result.total, equalTo(0));
    }

    public void testSearchingCreatedWorklog() throws Exception
    {
        final String issueKey = createDefaultIssue();
        createWorklog(issueKey, defaultStartDate);

        SearchResult result = executeEqualWorklogDateSearch(defaultStartDate);
        assertThat(result.total, equalTo(1));
    }

    public void testSearchingEditedWorklog() throws Exception
    {
        final String newStartDate = "2014-08-11";
        final String issueKey = createDefaultIssue();
        final Worklog worklog = createWorklog(issueKey, defaultStartDate);

        SearchResult result = executeEqualWorklogDateSearch(newStartDate);
        assertThat(result, sizeEquals(0));

        final Worklog worklogToEdit = new Worklog();
        worklogToEdit.id = worklog.id;
        worklogToEdit.started = getDateFromString(newStartDate);
        worklogClient.put(issueKey, worklogToEdit);

        result = executeEqualWorklogDateSearch(newStartDate);
        assertThat(result, sizeEquals(1));
    }

    public void testSearchingDeletedWorklog() throws Exception
    {
        final String issueKey = createDefaultIssue();
        final Worklog worklog = createWorklog(issueKey, defaultStartDate);

        SearchResult result = executeEqualWorklogDateSearch(defaultStartDate);
        assertThat(result, sizeEquals(1));

        worklogClient.delete(issueKey, worklog);

        result = executeEqualWorklogDateSearch(defaultStartDate);
        assertThat(result, sizeEquals(0));
    }

    public void testSearchingWorklogInDeletedIssue() throws Exception
    {
        final String issueKey = createDefaultIssue();
        createWorklog(issueKey, defaultStartDate);

        SearchResult result = executeEqualWorklogDateSearch(defaultStartDate);
        assertThat(result, sizeEquals(1));
        assertThat(result, containsIssues(issueKey));

        issueClient.delete(issueKey, null);

        result = executeEqualWorklogDateSearch(defaultStartDate);
        assertThat(result, sizeEquals(0));
    }

    public void testIssueContainsMultipleWorklogs()
    {
        final String issueWithWorklogsOn10thAnd13th = createDefaultIssue();
        createWorklog(issueWithWorklogsOn10thAnd13th, "2014-08-10");
        createWorklog(issueWithWorklogsOn10thAnd13th, "2014-08-13");
        final String issueWithWorklogOn9thAnd14th = createDefaultIssue();
        createWorklog(issueWithWorklogOn9thAnd14th, "2014-08-09");
        createWorklog(issueWithWorklogOn9thAnd14th, "2014-08-14");

        SearchResult searchResult = executeJqlSearch("worklogDate < 2014-08-10");
        assertThat(searchResult, sizeEquals(1));
        assertThat(searchResult, containsIssues(issueWithWorklogOn9thAnd14th));

        searchResult = executeJqlSearch("worklogDate > 2014-08-09 AND worklogDate < 2014-08-14");
        assertThat(searchResult, sizeEquals(1));
        assertThat(searchResult, containsIssues(issueWithWorklogsOn10thAnd13th));

        searchResult = executeJqlSearch("worklogDate >= 2014-08-10 AND worklogDate <= 2014-08-13");
        assertThat(searchResult, sizeEquals(1));
        assertThat(searchResult, containsIssues(issueWithWorklogsOn10thAnd13th));

        searchResult = executeJqlSearch("worklogDate = 2014-08-09 OR worklogDate = 2014-08-13");
        assertThat(searchResult, sizeEquals(2));
        assertThat(searchResult, containsIssues(issueWithWorklogOn9thAnd14th, issueWithWorklogsOn10thAnd13th));

        searchResult = executeJqlSearch("worklogDate in (\"2014-08-09\", \"2014-08-13\")");
        assertThat(searchResult, sizeEquals(2));
        assertThat(searchResult, containsIssues(issueWithWorklogOn9thAnd14th, issueWithWorklogsOn10thAnd13th));

        searchResult = executeJqlSearch("worklogDate > 2014-08-13");
        assertThat(searchResult, sizeEquals(1));
        assertThat(searchResult, containsIssues(issueWithWorklogOn9thAnd14th));

        searchResult = executeJqlSearch("worklogDate >= 2014-08-13");
        assertThat(searchResult, sizeEquals(2));
        assertThat(searchResult, containsIssues(issueWithWorklogOn9thAnd14th, issueWithWorklogsOn10thAnd13th));
    }
}
