package com.atlassian.jira.webtests.ztests.attachment;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.ws.rs.core.UriBuilder;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rules.CopyAttachmentsRule;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.IOUtils;

import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.core.IsEqual.equalTo;

@WebTest ({ Category.FUNC_TEST, Category.ATTACHMENTS })
public class TestAttachmentZipServlet extends FuncTestCase
{

    public static final int ID_OF_ISSUE_WITH_MANY_ATTACHMENTS = 10001;
    public static final int ID_OF_ISSUE_WITH_ZIP_ARCHIVE_ATTACHMENT = 10000;
    public static final int XML_ATTACHMENT_ID = 10000;
    public static final int ZIP_ATTACHMENT_ID = 10001;
    public static final int PNG_ATTACHMENT_ID = 10002;
    public static final int JPEG_ATTACHMENT_ID = 10100;
    private final HttpClient client = new HttpClient();
    private boolean restored = false;

    protected CopyAttachmentsRule copyAttachmentsRule;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();

        backdoor.attachments().enable();
        restoreDataOnce();
        copyAttachmentsRule = new CopyAttachmentsRule(this);
        copyAttachmentsRule.before();
        copyAttachmentsRule.copyAttachmentsFrom("TestAttachmentZipServlet/attachments");

        client.getState().setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("admin", "admin"));
        client.getParams().setAuthenticationPreemptive(true);
    }

    private void restoreDataOnce()
    {
        if(!restored)
        {
            backdoor.restoreDataFromResource("xml/TestAttachmentZipServlet/TestAttachmentZipServlet.xml");
            restored = true;
        }
    }

    @Override
    protected void tearDownTest()
    {
        copyAttachmentsRule.after();
        super.tearDownTest();
    }

    public void testDownloadAllAttachmentsOfIssueAsZip() throws Exception
    {
        final String uri = zipAllAttachmentsUri().toString();
        final GetMethod request = makeRequestToAttachmentServlet(uri);

        final InputStream is = request.getResponseBodyAsStream();
        try
        {
            assertResponseZipContainsEntries(is, "test1.xml", "test3.png", "test1.xml.zip", "test.jpeg");
        }
        finally
        {
            IOUtils.closeQuietly(is);
        }
    }

    public void testCanDownloadOneEntryFromZip() throws Exception
    {
        final GetMethod request = makeRequestToAttachmentServlet(unzipFileUri(0).toString());

        final InputStream is = request.getResponseBodyAsStream();
        try
        {
            assertResponseBodyEqualsTo(is, " <someXmlContent>here</someXmlContent>");
        }
        finally
        {
            IOUtils.closeQuietly(is);
        }
    }

    public void testShouldHaveAppropriateHeaderAccordingToFileType() throws Exception
    {
        verifyAttachmentWithIdProvidedWithAppropriateContentType("text/xml", XML_ATTACHMENT_ID);
        verifyAttachmentWithIdProvidedWithAppropriateContentType("application/zip", ZIP_ATTACHMENT_ID);
        verifyAttachmentWithIdProvidedWithAppropriateContentType("image/png", PNG_ATTACHMENT_ID);
        verifyAttachmentWithIdProvidedWithAppropriateContentType("image/jpeg", JPEG_ATTACHMENT_ID);
    }

    private void verifyAttachmentWithIdProvidedWithAppropriateContentType(final String expectedContentType, final int attachmentId)
            throws IOException, URISyntaxException
    {
        final GetMethod request = makeRequestToAttachmentServlet(getAttachmentURI(attachmentId).toString());
        final Header responseHeader = request.getResponseHeader("Content-Type");
        assertThat(responseHeader.getValue(), startsWith(expectedContentType));
    }

    private void assertResponseBodyEqualsTo(final InputStream resultStream, final String expectedStreamContent) throws IOException
    {
        final String resultStreamContent = IOUtils.toString(resultStream, "UTF-8");
        assertThat(resultStreamContent, equalTo(expectedStreamContent));
    }

    private void assertResponseZipContainsEntries(final InputStream is, final String ... expectedEntries) throws Exception
    {
        final ZipInputStream zipInputStream = new ZipInputStream(is);
        final Set<String> entries = new HashSet<String>();
        ZipEntry entry;
        while( (entry = zipInputStream.getNextEntry()) != null )
        {
            entries.add(entry.getName());
        }

        assertThat(entries, containsInAnyOrder(expectedEntries));
    }

    private GetMethod makeRequestToAttachmentServlet(final String uri) throws IOException
    {
        final GetMethod request = new GetMethod(uri);
        request.setRequestHeader("User-Agent", "Mozilla/5.0 (compatible; MSIE 7.01; Windows NT 5.0)");
        final int status = client.executeMethod(request);
        assertEquals(HttpStatus.SC_OK, status);
        return request;
    }

    private URI getAttachmentURI(final int attachmentId) throws URISyntaxException
    {
        return getUriBuilder()
                .path("secure/attachment/" + attachmentId + "/")
                .build();
    }

    private URI unzipFileUri(final int entryId) throws URISyntaxException
    {
        return getUriBuilder()
                .path("secure/attachmentzip/unzip/" + ID_OF_ISSUE_WITH_ZIP_ARCHIVE_ATTACHMENT + "/10003%5B" + entryId + "%5D/")
                .build();
    }

    private URI zipAllAttachmentsUri() throws URISyntaxException
    {
        return getUriBuilder()
                .path("secure/attachmentzip/" + ID_OF_ISSUE_WITH_MANY_ATTACHMENTS + ".zip")
                .build();
    }

    private UriBuilder getUriBuilder() throws URISyntaxException
    {
        return UriBuilder.fromUri(environmentData.getBaseUrl().toURI());
    }
}
