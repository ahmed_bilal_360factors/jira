package com.atlassian.jira.functest.rule;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.rules.ExternalResource;

/**
 * Cleans up a directory before and after a test.
 *
 * @since v6.4
 */
class CleanDirectoryRule extends ExternalResource
{
    private final File directory;

    CleanDirectoryRule(final File directory)
    {
        this.directory = directory;
    }

    @Override
    protected void before() throws Throwable
    {
        super.before();
        cleanDirectory();
    }

    private void cleanDirectory()
    {
        try
        {
            FileUtils.cleanDirectory(directory);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void after()
    {
        super.after();
        cleanDirectory();
    }

}
