package com.atlassian.jira.webtests.ztests.workflow;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.SearchRestClient;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.SearchResult;
import com.atlassian.jira.rest.client.api.domain.Transition;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

@WebTest ({ Category.FUNC_TEST, Category.WORKFLOW })
public class TestWorkflowTransitionReindexing extends FuncTestCase
{
    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        backdoor.restoreBlankInstance();
    }

    public void testReIndexingFieldsSetDuringTransition() throws Exception
    {
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_TIMETRACKING, true);
        backdoor.screens().addFieldToScreen("Resolve Issue Screen", "Log Work");
        backdoor.screens().addFieldToScreen("Resolve Issue Screen", "Summary");
        backdoor.screens().addFieldToScreen("Resolve Issue Screen", "Comment");

        final JiraRestClient restClient = createRestClient();
        IssueRestClient issueClient = restClient.getIssueClient();

        IssueInput issueInput = new IssueInputBuilder("HSP", 1l)
                .setSummary("new issue")
                .setDescription("issue description")
                .build();

        Issue issue = issueClient.getIssue(issueClient.createIssue(issueInput).claim().getKey()).claim();
        Iterable<Transition> transitions = issueClient.getTransitions(issue).claim();
        Transition resolveIssue = Iterables.find(transitions, new Predicate<Transition>()
        {
            @Override
            public boolean apply(final Transition transition)
            {
                return transition.getName().contains("Resolve");
            }
        });

        navigation.issue().gotoIssue(issue.getKey());
        tester.clickLink("action_id_" + resolveIssue.getId());
        tester.setWorkingForm("issue-workflow-transition");

        String changedSummaryValue = "changed summary";
        tester.setFormElement("comment", "some random comment");
        tester.setFormElement("worklog_timeLogged", "2h");
        tester.setFormElement("summary", changedSummaryValue);

        tester.submit();

        SearchRestClient searchClient = restClient.getSearchClient();

        SearchResult searchResult = searchClient.searchJql("timespent > 1h").claim();
        assertThat(searchResult.getIssues(), contains(issue(issue.getKey())));

        searchResult = searchClient.searchJql("summary ~ \"" + changedSummaryValue + "\"").claim();
        assertThat(searchResult.getIssues(), contains(issue(issue.getKey())));

        searchResult = searchClient.searchJql("summary ~ issue").claim();
        assertThat(searchResult.getIssues(), Matchers.<Issue>emptyIterable());

        searchResult = searchClient.searchJql("comment ~ random").claim();
        assertThat(searchResult.getIssues(), contains(issue(issue.getKey())));
    }

    private Matcher<Issue> issue(final String issueKey)
    {
        return new TypeSafeMatcher<Issue>()
        {
            @Override
            protected boolean matchesSafely(final Issue issue)
            {
                return issue.getKey().equals(issueKey);
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("Issue").appendValue(issueKey).appendText("not on the list");
            }
        };
    }
}
