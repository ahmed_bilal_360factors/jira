package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import java.util.Date;
import java.util.Set;

import com.atlassian.jira.functest.framework.admin.plugins.ReferencePlugin;
import com.atlassian.jira.functest.framework.backdoor.PluginIndexConfigurationControl;
import com.atlassian.jira.functest.framework.backdoor.SearchAutoCompleteControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.jql.operator.OperatorClasses;
import com.atlassian.query.operator.Operator;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import static com.atlassian.jira.functest.framework.backdoor.SearchAutoCompleteControl.AutoCompleteData;
import static com.atlassian.jira.functest.framework.backdoor.SearchAutoCompleteControl.AutoCompleteField;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.assertThat;

@WebTest ({ Category.FUNC_TEST, Category.REST, Category.JQL, Category.REFERENCE_PLUGIN })
public class TestSearchAutoCompleteResource extends RestFuncTest
{
    private static final String CUSTOM_FIELD_TYPE = String.format("%s:%s", BUILT_IN_CUSTOM_FIELD_KEY, CUSTOM_FIELD_TYPE_TEXTFIELD);

    private SearchAutoCompleteControl client;
    private PluginIndexConfigurationControl configurationControlClient;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        backdoor.restoreBlankInstance();
        this.client = new SearchAutoCompleteControl(getEnvironmentData());
        this.configurationControlClient = new PluginIndexConfigurationControl(getEnvironmentData());
    }

    public void testAutoCompleteControlContainsJqlAlias() throws Exception
    {
        updatePluginIndexConfiguration(ReferencePlugin.KEY, "pluginindexconfiguration1.xml", configurationControlClient);

        AutoCompleteData autoCompleteData = client.getAutoCompleteData();
        AutoCompleteField labelField = createAutoCompleteField("label",
                "label",
                true,
                OperatorClasses.EQUALITY_OPERATORS_WITH_EMPTY, String.class, true);
        AutoCompleteField labelDate = createAutoCompleteField("labelDate",
                "labelDate",
                false,
                OperatorClasses.EQUALITY_AND_RELATIONAL_WITH_EMPTY, Date.class, true);
        AutoCompleteField labelDescription = createAutoCompleteField("labelDescription",
                "labelDescription",
                false,
                OperatorClasses.TEXT_OPERATORS, String.class, true);
        AutoCompleteField  labelId = createAutoCompleteField("labelId",
                "labelId",
                false,
                OperatorClasses.EQUALITY_AND_RELATIONAL_WITH_EMPTY, Number.class, true);

        assertThat(autoCompleteData.getVisibleFieldNames(), hasItems(labelField, labelDate, labelDescription, labelId));
    }

    public void testAutoCompleteWithConflictingAliases() throws Exception
    {
        updatePluginIndexConfiguration("plugin 1", "conflict_indexconf1.xml", configurationControlClient);
        updatePluginIndexConfiguration("plugin 2", "conflict_indexconf2.xml", configurationControlClient);

        AutoCompleteData autoCompleteData = client.getAutoCompleteData();

        AutoCompleteField firstLabelField = createAutoCompleteField("\"issue.property[label].value\"",
                "conflict - plugin 1",
                true,
                OperatorClasses.EQUALITY_OPERATORS_WITH_EMPTY, String.class, true);

        AutoCompleteField secondLabelField = createAutoCompleteField("\"issue.property[conflicting_alias].value\"",
                "conflict - plugin 2",
                true,
                OperatorClasses.EQUALITY_OPERATORS_WITH_EMPTY, String.class, true);

        assertThat(autoCompleteData.getVisibleFieldNames(), hasItems(firstLabelField, secondLabelField));
    }

    public void testAutoCompleteWithConflictingAliasesAndCustomFields()
    {
        updatePluginIndexConfiguration("plugin 1", "conflict_indexconf1.xml", configurationControlClient);
        backdoor.customFields().createCustomField("conflict", "some description", CUSTOM_FIELD_TYPE, null);

        AutoCompleteData autoCompleteData = client.getAutoCompleteData();

        AutoCompleteField firstLabelField = createAutoCompleteField("\"issue.property[label].value\"",
                "conflict - plugin 1",
                true,
                OperatorClasses.EQUALITY_OPERATORS_WITH_EMPTY, String.class, true);


        AutoCompleteField customField = createAutoCompleteField("cf[10000]", "conflict - cf[10000]",
                false,
                Sets.<Operator>newHashSet(),
                Object.class,
                false);

        assertThat(autoCompleteData.getVisibleFieldNames(), hasItems(firstLabelField, customField));
    }

    public void testAutoCompleteWithSingleCustomField()
    {
        backdoor.customFields().createCustomField("conflict", "some description", CUSTOM_FIELD_TYPE, null);

        AutoCompleteData autoCompleteData = client.getAutoCompleteData();

        AutoCompleteField customField = createAutoCompleteField("conflict", "conflict - cf[10000]",
                false,
                Sets.<Operator>newHashSet(),
                Object.class,
                false);

        assertThat(autoCompleteData.getVisibleFieldNames(), hasItem(customField));
    }

    public void testAutoCompleteWithTwoConflictingCustomFields()
    {
        backdoor.customFields().createCustomField("conflict", "some description", CUSTOM_FIELD_TYPE, null);
        backdoor.customFields().createCustomField("conflict", "some description", CUSTOM_FIELD_TYPE, null);

        AutoCompleteData autoCompleteData = client.getAutoCompleteData();

        AutoCompleteField customField1 = createAutoCompleteField("cf[10000]", "conflict - cf[10000]",
                false,
                Sets.<Operator>newHashSet(),
                Object.class,
                false);
        AutoCompleteField customField2 = createAutoCompleteField("cf[10001]", "conflict - cf[10001]",
                false,
                Sets.<Operator>newHashSet(),
                Object.class,
                false);

        assertThat(autoCompleteData.getVisibleFieldNames(), hasItems(customField1, customField2));
    }

    private AutoCompleteField createAutoCompleteField(final String name,
            final String displayName,
            final Boolean auto,
            final Set<Operator> operators,
            final Class<?> type,
            final Boolean searchable)
    {
        final String[] operatorsArray = Iterables.toArray(Iterables.transform(operators, new Function<Operator, String>()
        {
            @Override
            public String apply(final Operator operator)
            {
                return operator.getDisplayString();
            }
        }), String.class);

        return new AutoCompleteField(name, displayName, auto.toString(), searchable.toString(),
                operatorsArray, new String[] {type.getCanonicalName()});
    }
}
