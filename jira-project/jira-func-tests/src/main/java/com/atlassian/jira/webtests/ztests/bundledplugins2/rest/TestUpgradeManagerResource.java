package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import java.util.Collections;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.SystemPropertiesControl;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import com.atlassian.jira.webtests.ztests.upgrade.UpgradeManagerClient;

import com.sun.jersey.api.client.UniformInterfaceException;

/**
 * Tests upgrade manager logic in a running system.
 *
 * @since 6.4
 */
@WebTest ( { Category.FUNC_TEST, Category.REST })
public class TestUpgradeManagerResource extends RestFuncTest
{
    private static final String UPGRADE_PROPERTY_NAME = "UpgradeTaskForTesting.invocationCount";
    private static final String UPGRADE_REINDEX_ALLOWED_PROPERTY = "upgrade.reindex.allowed";

    private UpgradeManagerClient upgradeManagerClient;
    private SystemPropertiesControl systemPropertiesClient;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        upgradeManagerClient = new UpgradeManagerClient(getEnvironmentData());
        systemPropertiesClient = new SystemPropertiesControl(getEnvironmentData());

        systemPropertiesClient.unsetProperty(UPGRADE_PROPERTY_NAME);
        systemPropertiesClient.unsetProperty(UPGRADE_REINDEX_ALLOWED_PROPERTY);

        administration.restoreData("TestEditIssue.xml");
    }

    @Override
    protected void tearDownTest()
    {
        //For convenience so that in case the next tests don't restore data we don't leave some god-awful
        //upgrade version in the database
        administration.restoreBlankInstance();

        super.tearDownTest();
    }

    /**
     * Tests that upgrade manager runs an upgrade task.
     */
    public void testRunUpgradeTask()
    {
        upgradeManagerClient.addUpgradeTask("com.atlassian.jira.dev.backdoor.UpgradeTaskForTesting");
        upgradeManagerClient.doUpgrade();

        //Test that upgrade manager has run
        String ic = systemPropertiesClient.getProperty(UPGRADE_PROPERTY_NAME);
        assertEquals("Expected upgrade task to run once.", "1", ic);
    }

    /**
     * Checks that reindex runs when an upgrade task asks for it.
     */
    public void testUpgradeTaskWithReindex()
    {
        //Intentionally de-index an issue
        backdoor.indexing().deindex("MKY-1");
        SearchResult result = backdoor.search().getSearch(new SearchRequest().jql("issuekey = 'MKY-1'"));
        assertEquals("Should not find issue.", Collections.emptyList(), result.issues);

        upgradeManagerClient.addUpgradeTask("com.atlassian.jira.dev.backdoor.UpgradeTaskForTesting");
        upgradeManagerClient.doUpgrade();

        //Test that upgrade manager has run
        String ic = systemPropertiesClient.getProperty(UPGRADE_PROPERTY_NAME);
        assertEquals("Expected upgrade task to run once.", "1", ic);

        //Test that reindex has occurred
        //Should be able to find the issue now
        result = backdoor.search().getSearch(new SearchRequest().jql("issuekey = 'MKY-1'"));
        assertEquals("Issue not found.", 1, result.issues.size());
    }

    /**
     * Tests that reindex does not run when an upgrade task doesn't ask for it.
     */
    public void testUpgradeTaskWithoutReindex()
    {
        //Intentionally de-index an issue
        backdoor.indexing().deindex("MKY-1");
        SearchResult result = backdoor.search().getSearch(new SearchRequest().jql("issuekey = 'MKY-1'"));
        assertEquals("Should not find issue.", Collections.emptyList(), result.issues);

        upgradeManagerClient.addUpgradeTask("com.atlassian.jira.dev.backdoor.UpgradeTaskForTestingNoReindex");
        upgradeManagerClient.doUpgrade();

        //Test that upgrade manager has run
        String ic = systemPropertiesClient.getProperty(UPGRADE_PROPERTY_NAME);
        assertEquals("Expected upgrade task to run once.", "1", ic);

        //Reindex shuold not occur
        //Still should not be able to find the de-indexed issue
        result = backdoor.search().getSearch(new SearchRequest().jql("issuekey = 'MKY-1'"));
        assertEquals("Should not find issue.", Collections.emptyList(), result.issues);
    }

    /**
     * Tests when reindexingAllowed flag is set to false.
     */
    public void testReindexNotAllowed()
    {
        //Force reindexNotAllowed mode
        systemPropertiesClient.setProperty(UPGRADE_REINDEX_ALLOWED_PROPERTY, Boolean.FALSE.toString());

        //Intentionally de-index an issue
        backdoor.indexing().deindex("MKY-1");
        SearchResult result = backdoor.search().getSearch(new SearchRequest().jql("issuekey = 'MKY-1'"));
        assertEquals("Should not find issue.", Collections.emptyList(), result.issues);

        upgradeManagerClient.addUpgradeTask("com.atlassian.jira.dev.backdoor.UpgradeTaskForTesting");
        upgradeManagerClient.doUpgrade();

        //Test that upgrade manager has run
        String ic = systemPropertiesClient.getProperty(UPGRADE_PROPERTY_NAME);
        assertEquals("Expected upgrade task to run once.", "1", ic);

        //Reindex should not have occurred because system property was set
        //Issue should still not be indexed
        result = backdoor.search().getSearch(new SearchRequest().jql("issuekey = 'MKY-1'"));
        assertEquals("Should not find issue.", Collections.emptyList(), result.issues);
    }

    /**
     * A failing upgrade task will prevent subsequent tasks from running.
     */
    public void testFailedUpgradePreventsSubsequentTasksFromRunning()
    {
        upgradeManagerClient.addUpgradeTask("com.atlassian.jira.dev.backdoor.UpgradeTaskForTesting");
        upgradeManagerClient.addUpgradeTask("com.atlassian.jira.dev.backdoor.FailingUpgradeTaskForTesting");
        upgradeManagerClient.addUpgradeTask("com.atlassian.jira.dev.backdoor.UpgradeTaskForTestingNoReindex");

        try
        {
            upgradeManagerClient.doUpgrade();
            fail("Expected 500 error.");
        }
        catch (UniformInterfaceException e)
        {
            //Expected 500 error
        }

        //Should have executed tasks 1 and 2, 3 should not have run due to failure in 2
        String ic = systemPropertiesClient.getProperty(UPGRADE_PROPERTY_NAME);
        assertEquals("Wrong upgrade task execution count.", "2", ic);
    }
}
