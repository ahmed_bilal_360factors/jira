package com.atlassian.jira.webtests.ztests.indexing;

import com.atlassian.jira.rest.v2.index.ReindexRequestBean;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

public class ReindexRequestClient extends RestApiClient
{
    private final JIRAEnvironmentData environmentData;

    public ReindexRequestClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
        this.environmentData = environmentData;
    }

    public long[] performReindex()
    {
        Response<long[]> response = toResponse(new Method()
        {
            @Override
            public ClientResponse call()
            {
                WebResource reindexResource = getReindexResource();
                return reindexResource.type(APPLICATION_JSON_TYPE).post(ClientResponse.class);
            }
        }, long[].class);

        return response.body;
    }

    public ReindexRequestBean getReindexRequest(long id)
    {
        return getReindexResource().path(String.valueOf(id)).get(ReindexRequestBean.class);
    }

    private WebResource getReindexResource()
    {
        return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("rest").path("api").path("2").path("reindex").path("request");
    }
}
