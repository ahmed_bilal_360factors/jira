package com.atlassian.jira.functest.rule;

import java.io.File;

import com.atlassian.jira.testkit.client.AttachmentsControl;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * Cleans up attachment files before and after a test.
 *
 * @since v6.4
 */
class CleanAttachmentsRule implements TestRule
{
    private final AttachmentsControl attachmentsControl;

    CleanAttachmentsRule(final AttachmentsControl attachmentsControl)
    {
        this.attachmentsControl = attachmentsControl;
    }

    @Override
    public Statement apply(final Statement base, final Description description)
    {
        return new Statement()
        {
            @Override
            public void evaluate() throws Throwable
            {
                final String attachmentPath = attachmentsControl.getAttachmentPath();
                final File attachmentDirectory = new File(attachmentPath);
                final TestRule cleanDirectoryRule = Rules.cleanDirectory(attachmentDirectory);
                cleanDirectoryRule.apply(base, description).evaluate();
            }
        };
    }
}
