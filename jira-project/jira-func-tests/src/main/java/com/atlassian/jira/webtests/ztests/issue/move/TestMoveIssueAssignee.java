package com.atlassian.jira.webtests.ztests.issue.move;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;

/**
 * Tests that the assignee field is set appropriately on Move Issue.
 * In particular, if the assignee is not valid in the new Project.
 */
@WebTest({Category.FUNC_TEST, Category.MOVE_ISSUE })
public class TestMoveIssueAssignee extends FuncTestCase
{
    protected void setUpTest()
    {
        this.administration.restoreData("TestMoveIssueAssignee.xml");
    }

    public void testMoveSingleIssueSameWorkflow() throws Exception
    {
        // Simple case - assignee is valid in new Project, so leave alone
        navigation.issue().viewIssue("RAT-24");
        tester.clickLink("move-issue");
        navigation.issue().selectProject("Porcine");
        assertions.getTextAssertions().assertTextPresent("Step 1 of 4");
        tester.submit("Next >>");
        assertions.getTextAssertions().assertTextPresent("Step 3 of 4");
        tester.submit("Next >>");
        assertions.getTextAssertions().assertTextPresent("Step 4 of 4");
        tester.submit("Move");
        assertions.getViewIssueAssertions().assertAssignee("Mahatma Gandhi");

        // Interesting case - assignee is INACTIVE and NOT valid in new Project, but we still keep it
        navigation.issue().viewIssue("RAT-23");
        tester.clickLink("move-issue");
        navigation.issue().selectProject("Canine");
        assertions.getTextAssertions().assertTextPresent("Step 1 of 4");
        tester.submit("Next >>");
        assertions.getTextAssertions().assertTextPresent("Step 3 of 4");
        tester.submit("Next >>");
        assertions.getTextAssertions().assertTextPresent("Step 4 of 4");

        tester.submit("Move");
        assertions.getViewIssueAssertions().assertAssignee("pleb");

    }
}
