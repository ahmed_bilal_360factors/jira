package com.atlassian.jira.functest.framework;

import com.atlassian.jira.functest.framework.admin.SendBulkMail;

/**
 *
 * @since v4.4
 */
public class DefaultSendBulkMail implements SendBulkMail
{
    private final Navigation navigation;

    public DefaultSendBulkMail(final Navigation navigation)
    {
        this.navigation = navigation;
    }

    @Override
    public SendBulkMail goTo()
    {
        navigation.gotoAdminSection(Navigation.AdminSection.SEND_EMAIL);
        return this;
    }
}
