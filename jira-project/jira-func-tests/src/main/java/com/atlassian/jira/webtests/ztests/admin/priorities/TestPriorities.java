package com.atlassian.jira.webtests.ztests.admin.priorities;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.beans.Priority;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

/**
 * Tests for the administration of priorities.
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION })
public class TestPriorities extends FuncTestCase
{
    private void addNewPriority(String name)
    {
        tester.setFormElement("name", name);
        tester.setFormElement("iconurl", "/images/icons/priorities/blocker.png");
        tester.setFormElement("statusColor", "#efefef");

        tester.submit();
    }

    private void assertDuplicatePriorityError()
    {
        assertions.getJiraFormAssertions().assertFieldErrMsg("A priority with that name already exists.");
    }

    private Matcher<Priority> hasPriority(final String id, final String name)
    {
        return new TypeSafeMatcher<Priority>()
        {
            @Override
            public void describeTo(Description description)
            {
                description.appendText(String.format("Priority(id=%s, name=%s)", id, name));
            }

            @Override
            protected boolean matchesSafely(Priority priority)
            {
                return id.equals(priority.getId()) && name.equals(priority.getName());
            }
        };
    }

    public void testDefaultPriorities()
    {
        administration.restoreData("TestPriorities.xml");

        List<Priority> priorities = backdoor.getTestkit().priorities().getPriorities();
        assertThat(priorities, hasSize(5));

        /**
         * PriorityControl from testkit internally calls out to PriorityManager#getPriorities();
         * which guarantees ordering of priorities from highest to lowest.
         *
         * This is to make sure ordering is asserted as well.
         */
        assertThat(priorities, Matchers.<Priority>contains(
                hasPriority("1", "Highest"),
                hasPriority("2", "High"),
                hasPriority("3", "Medium"),
                hasPriority("4", "Low"),
                hasPriority("5", "Lowest")));
    }

    //JRA-18985: It is possible to make a priority a duplicate.
    public void testSameName() throws Exception
    {
        administration.restoreBlankInstance();
        navigation.gotoAdminSection(Navigation.AdminSection.PRIORITIES);

        //Check to see that we can't add a priority of the same name.
        addNewPriority("Blocker");
        assertDuplicatePriorityError();

        addNewPriority("BlocKER");
        assertDuplicatePriorityError();

        //Check to see that we can't change a priority name to an already existing name.
        tester.gotoPage("secure/admin/EditPriority!default.jspa?id=2");

        //Check to see that we can't edit a priority such that becomes a duplicate.
        tester.setFormElement("name", "Blocker");
        tester.submit();
        assertDuplicatePriorityError();

        tester.setFormElement("name", "blocker");
        tester.submit();
        assertDuplicatePriorityError();
    }
}
