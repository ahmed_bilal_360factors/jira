package com.atlassian.jira.webtests.ztests.bulk;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.navigation.IssueNavigatorNavigation;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;

import org.apache.commons.lang3.StringUtils;

/**
 * Tests Bulk Edit of custom field type that does not validate itself properly against jira character limit.
 */
@WebTest ({ Category.FUNC_TEST, Category.BULK_OPERATIONS, Category.REFERENCE_PLUGIN })
public class TestBulkEditCustomFieldWithNoValidation extends FuncTestCase
{

    private static final int LIMIT = 255;
    private static final String SOME_LONG_TEXT = StringUtils.repeat("yaddi yaddi yadda ", 15);
    private static final String CUSTOM_FIELD_WITHOUT_LENGTH_VALIDATION_ID = "customfield_10100";

    @Override
    protected void setUpTest()
    {
        administration.restoreData("TestCreateIssueWithDegeneratedCustomFieldType.xml");
        backdoor.advancedSettings().setTextFieldCharacterLengthLimit(LIMIT);
    }

    public void testBulkEditReportsAnErrorWhenFieldIsTooLong() throws Exception
    {
        navigation.issueNavigator().displayAllIssues();
        navigation.issueNavigator().bulkChange(IssueNavigatorNavigation.BulkChangeOption.ALL_PAGES);
        tester.assertTextPresent("Step 1 of 4: Choose Issues");
        tester.checkCheckbox("bulkedit_10300", "on");
        tester.checkCheckbox("bulkedit_10200", "on");
        tester.checkCheckbox("bulkedit_10100", "on");
        tester.submit("Next");
        tester.checkCheckbox("operation", "bulk.edit.operation.name");
        tester.submit("Next");
        // We should be able to Bulk Edit the Environment
        tester.checkCheckbox("actions", CUSTOM_FIELD_WITHOUT_LENGTH_VALIDATION_ID);
        tester.setFormElement(CUSTOM_FIELD_WITHOUT_LENGTH_VALIDATION_ID, SOME_LONG_TEXT);
        tester.submit("Next");
        tester.submit("Confirm");

        waitForOperationProgressPage(tester);

        // make sure jira does not blow up with 500 error message as a result of RuntimeException thrown by IssueManager
        assertions.getJiraFormAssertions().assertFormErrMsg("Error while performing bulk operation on issues");
        tester.submit("Acknowledge");
    }

}
