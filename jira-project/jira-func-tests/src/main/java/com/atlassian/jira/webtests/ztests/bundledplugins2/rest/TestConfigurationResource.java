package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.ConfigurationBean;
import com.atlassian.jira.testkit.client.restclient.ConfigurationClient;

import static com.atlassian.jira.testkit.client.restclient.ConfigurationBean.TimeTrackingConfigurationBean.TimeFormat;
import static com.atlassian.jira.testkit.client.restclient.ConfigurationBean.TimeTrackingConfigurationBean.TimeTrackingUnit;

/**
 * Test for the resource which returns JIRA configuration.
 * @since v6.4
 */
@WebTest ({ Category.FUNC_TEST, Category.REST })
public class TestConfigurationResource extends RestFuncTest
{
    private ConfigurationClient configurationClient;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        backdoor.restoreBlankInstance();
        this.configurationClient = new ConfigurationClient(getEnvironmentData());
    }

    public void testGettingJIRAConfiguration()
    {
        backdoor.attachments().disable();
        backdoor.issueLinking().disable();

        backdoor.subtask().enable();
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_TIMETRACKING, true);
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED, true);

        final ConfigurationBean configuration = configurationClient.getConfiguration();

        assertFalse(configuration.attachmentsEnabled);
        assertFalse(configuration.issueLinkingEnabled);

        assertTrue(configuration.subTasksEnabled);
        assertTrue(configuration.timeTrackingEnabled);
        assertTrue(configuration.unassignedIssuesAllowed);
        assertTrue(configuration.votingEnabled);
        assertTrue(configuration.watchingEnabled);

        assertEquals(configuration.timeTrackingConfiguration.defaultUnit, TimeTrackingUnit.minute);
        assertEquals(configuration.timeTrackingConfiguration.timeFormat, TimeFormat.pretty);
        assertEquals(configuration.timeTrackingConfiguration.workingDaysPerWeek, 7.0d, Math.pow(10, -5));
        assertEquals(configuration.timeTrackingConfiguration.workingHoursPerDay, 24.0d, Math.pow(10, -5)); // :)
    }

    public void testTimeTrackingDisabled()
    {
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_TIMETRACKING, false);

        final ConfigurationBean configuration = configurationClient.getConfiguration();

        assertFalse(configuration.timeTrackingEnabled);
        assertNull(configuration.timeTrackingConfiguration);
    }
}
