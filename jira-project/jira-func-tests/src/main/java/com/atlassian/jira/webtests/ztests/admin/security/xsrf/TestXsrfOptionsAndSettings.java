package com.atlassian.jira.webtests.ztests.admin.security.xsrf;

import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfCheck;
import com.atlassian.jira.functest.framework.security.xsrf.XsrfTestSuite;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.EmailFuncTestCase;

/**
 * @since v4.1
 */
@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.SECURITY , Category.SLOW_IMPORT})
public class TestXsrfOptionsAndSettings extends EmailFuncTestCase
{
    public void testSendEmail() throws Exception
    {
        administration.restoreBlankInstance();
        configureAndStartSmtpServer();

        new XsrfTestSuite(
            new XsrfCheck("SendBulkMail", new XsrfCheck.Setup()
            {
                public void setup()
                {
                    navigation.gotoAdminSection(Navigation.AdminSection.SEND_EMAIL);
                    tester.checkCheckbox("sendToRoles", "false");
                    tester.selectOption("groups", "jira-users");
                    tester.setFormElement("subject", "I'm sending an email");
                    tester.setFormElement("message", "This is the message.");
                 }
            }, new XsrfCheck.FormSubmission("Send"))
        ).run(funcTestHelperFactory);

        // check that the emails came through
        flushMailQueueAndWait(2);
    }
}
