package com.atlassian.jira.webtests.ztests.upgrade.tasks;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rules.RestRule;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.meterware.httpunit.WebResponse;
import org.xml.sax.SAXException;

import java.io.IOException;

import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static com.atlassian.jira.functest.framework.suite.Category.UPGRADE_TASKS;

/**
 * Ensure the JIRA News gadgets is deleted correctly
 *
 * @since v6.4
 */
@WebTest ({ FUNC_TEST, UPGRADE_TASKS })
public class TestUpgradeTask64002 extends FuncTestCase
{
    private RestRule restRule;

    @Override
    public void setUpTest()
    {
        restRule = new RestRule(this);
        restRule.before();
    }

    @Override
    public void tearDownTest()
    {
        restRule.after();
    }

    public void testNewsGadgetDeleted() throws IOException, SAXException, JSONException
    {
        administration.restoreData("TestUpgradeTask64002.xml");

        //retrieve a dashboard that contained 2 news gadgets.
        WebResponse response = restRule.GET("/rest/dashboards/1.0/10100.json");
        assertEquals("application/json", response.getContentType());

        final JSONObject dashboard = new JSONObject(response.getText());
        assertNoNewsGadgetPresent(dashboard, 4);
    }

    private void assertNoNewsGadgetPresent(final JSONObject contents, final int expectedNumberOfGadgets)
            throws JSONException
    {
        final JSONArray gadgets = contents.getJSONArray("gadgets");
        assertEquals(expectedNumberOfGadgets, gadgets.length());
        for (int i = 0; i < gadgets.length(); i++)
        {
            JSONObject gadget = gadgets.getJSONObject(i);
            assertFalse(gadget.getString("gadgetUrl").contains("news"));
        }
    }
}
