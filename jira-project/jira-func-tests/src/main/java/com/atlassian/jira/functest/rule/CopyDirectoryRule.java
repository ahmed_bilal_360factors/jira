package com.atlassian.jira.functest.rule;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.rules.ExternalResource;

/**
 * Copies specified directory contents before a test.
 *
 * @since v6.4
 */
class CopyDirectoryRule extends ExternalResource
{
    private final File sourceDirectory;
    private final File targetDirectory;

    CopyDirectoryRule(final File sourceDirectory, final File targetDirectory)
    {
        this.sourceDirectory = sourceDirectory;
        this.targetDirectory = targetDirectory;
    }

    @Override
    protected void before() throws Throwable
    {
        super.before();
        try
        {
            FileUtils.copyDirectory(sourceDirectory, targetDirectory);
        }
        catch (final IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}
