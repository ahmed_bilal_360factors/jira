package com.atlassian.jira.functest.framework.backdoor;

import java.util.List;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.sun.jersey.api.client.WebResource;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonProperty;

public class IssuePickerControl extends BackdoorControl<IssuePickerControl>
{
    private final Function<WebResource, WebResource> apiFunction;

    public static IssuePickerControl version1(final JIRAEnvironmentData environmentData)
    {
        return new IssuePickerControl(environmentData, new Function<WebResource, WebResource>()
        {
            @Override
            public WebResource apply(final WebResource webResource)
            {
                return webResource.path("rest")
                    .path("api")
                    .path("1.0")
                    .path("issues")
                    .path("picker");
            }
        });
    }

    public static IssuePickerControl version2(final JIRAEnvironmentData environmentData)
    {
        return new IssuePickerControl(environmentData, new Function<WebResource, WebResource>()
        {
            @Override
            public WebResource apply(final WebResource webResource)
            {
                return webResource.path("rest")
                    .path("api")
                    .path("2")
                    .path("issue")
                    .path("picker");
            }
        });
    }

    private IssuePickerControl(final JIRAEnvironmentData environmentData, final Function<WebResource, WebResource> apiFunction)
    {
        super(environmentData);
        this.apiFunction = apiFunction;
    }

    public IssuePickerResult getIssuePickerResult(final String query,
            final String currentJQL,
            final String currentIssueKey,
            final String currentProjectId,
            final Boolean showSubTasks,
            final Boolean showSubTaskParent)
    {
        final WebResource webResource = apiFunction.apply(resourceRoot(rootPath))
                .queryParam("query", emptyStringIfNull(query))
                .queryParam("currentJQL", emptyStringIfNull(currentJQL))
                .queryParam("currentIssueKey", emptyStringIfNull(currentIssueKey))
                .queryParam("currentProjectId", emptyStringIfNull(currentProjectId))
                .queryParam("showSubTasks", showSubTasks.toString())
                .queryParam("showSubTaskParent", showSubTaskParent.toString());

        return webResource.get(IssuePickerResult.class);
    }

    private String emptyStringIfNull(final String currentJQL) {return Objects.firstNonNull(currentJQL, StringUtils.EMPTY);}

    public static class IssuePickerResult
    {
        @JsonProperty
        private List<IssueSection> sections;

        public List<IssueSection> getSections()
        {
            return sections;
        }

        @Override
        public String toString()
        {
            return "IssuePickerResult{" +
                    "sections=" + sections +
                    '}';
        }
    }

    public static class IssueSection
    {
        @JsonProperty
        private String label;
        @JsonProperty
        private String sub;
        @JsonProperty
        private String id;
        @JsonProperty
        private String msg;
        @JsonProperty
        private List<IssuePickerIssue> issues;

        public String getLabel()
        {
            return label;
        }

        public String getSub()
        {
            return sub;
        }

        public String getId()
        {
            return id;
        }

        public String getMsg()
        {
            return msg;
        }

        public List<IssuePickerIssue> getIssues()
        {
            return issues;
        }

        @Override
        public String toString()
        {
            return "IssueSection{" +
                    "label='" + label + '\'' +
                    ", sub='" + sub + '\'' +
                    ", id='" + id + '\'' +
                    ", msg='" + msg + '\'' +
                    ", issues=" + issues +
                    '}';
        }
    }

    public static class IssuePickerIssue
    {
        @JsonProperty
        private String key;
        @JsonProperty
        private String keyHtml;
        @JsonProperty
        private String img;
        @JsonProperty
        private String summary;
        @JsonProperty
        private String summaryText;

        public String getKey()
        {
            return key;
        }

        public String getKeyHtml()
        {
            return keyHtml;
        }

        public String getImg()
        {
            return img;
        }

        public String getSummary()
        {
            return summary;
        }

        public String getSummaryText()
        {
            return summaryText;
        }

        @Override
        public String toString()
        {
            return "IssuePickerIssue{" +
                    "key='" + key + '\'' +
                    ", keyHtml='" + keyHtml + '\'' +
                    ", img='" + img + '\'' +
                    ", summary='" + summary + '\'' +
                    ", summaryText='" + summaryText + '\'' +
                    '}';
        }
    }
}
