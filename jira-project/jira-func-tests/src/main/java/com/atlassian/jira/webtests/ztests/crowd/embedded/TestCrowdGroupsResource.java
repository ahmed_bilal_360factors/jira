package com.atlassian.jira.webtests.ztests.crowd.embedded;

import com.atlassian.crowd.acceptance.tests.rest.service.GroupsResourceTest;
import com.atlassian.crowd.integration.rest.entity.MembershipsEntity;
import com.atlassian.crowd.model.group.Membership;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.webtests.util.EnvironmentAware;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.sun.jersey.api.client.WebResource;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Tests the Crowd REST API for the running in JIRA.
 *
 * @since v4.3
 */
@WebTest ({ Category.FUNC_TEST, Category.USERS_AND_GROUPS })
public class TestCrowdGroupsResource extends GroupsResourceTest implements EnvironmentAware
{
    public TestCrowdGroupsResource(String name)
    {
        super(name, new CrowdEmbeddedServer().usingXmlBackup(CrowdEmbeddedServer.XML_BACKUP));
    }

    @Override
    public void setEnvironmentData(JIRAEnvironmentData environmentData)
    {
        setRestServer(new CrowdEmbeddedServer(environmentData).usingXmlBackup(CrowdEmbeddedServer.XML_BACKUP));
    }

    @Override
    public void testStoreGroupAttributes()
    {
        // DISABLED because the functionality is not supported in JIRA
    }

    @Override
    public void testDeleteGroupAttribute()
    {
        // DISABLED because the functionality is not supported in JIRA
    }

    @Override
    public void testGetGroup_Expanded()
    {
        // DISABLED because the functionality is not supported in JIRA
    }

    @Override
    public void testAddDirectChildGroup_NoNestedGroups()
    {
        // DISABLED because the functionality is not supported in JIRA
    }

    @Override
    /**
     * Copy and paste from superclass because we still have the 3 standard JIRA groups and the assertion compares all.
     */
    public void testGetMembershipsReturnsExactlyExpectedMemberships()
    {
        WebResource webResource = getRootWebResource("crowd", APPLICATION_PASSWORD)
                .path(GROUPS_RESOURCE)
                .path("membership");

        MembershipsEntity mems = webResource.get(MembershipsEntity.class);

        assertNotNull(mems.getList());

        Map<String, Set<String>> users = new HashMap<String, Set<String>>(64);
        Map<String, Set<String>> childGroups = new HashMap<String, Set<String>>(64);

        for (Membership e : mems.getList())
        {
            users.put(e.getGroupName(), e.getUserNames());
            childGroups.put(e.getGroupName(), e.getChildGroupNames());
        }

        final Set<String> empty = Collections.emptySet();
        Map<String, Set<String>> expectedUsers = ImmutableMap.<String,Set<String>>builder()
                .put("animals", empty)
                .put("badgers", users("admin", "eeeep"))
                .put("birds", empty)
                .put("cats", empty)
                .put("crowd-administrators", users("admin", "secondadmin"))
                .put("crowd-testers", users("penny"))
                .put("crowd-users", empty)
                .put("dogs", empty)
                // The jira groups
                .put("jira-users", users("admin", "regularuser"))
                .put("jira-developers", users("admin"))
                .put("jira-administrators", users("admin"))
                .put("UpperCaseGroupName", users("regularuser"))
                .build();

        Map<String, Set<String>> expectedChildGroups = ImmutableMap.<String,Set<String>>builder()
                .put("animals", empty)
                .put("badgers", empty)
                .put("birds", empty)
                .put("cats", empty)
                .put("crowd-administrators", users("crowd-testers"))
                .put("crowd-testers", users("badgers"))
                .put("crowd-users", users("badgers"))
                .put("dogs", empty)
                .put("jira-users", empty)
                .put("jira-developers", empty)
                .put("jira-administrators", empty)
                .put("UpperCaseGroupName", empty)
                .build();

        assertEquals(expectedUsers, users);
        assertEquals(expectedChildGroups, childGroups);
    }

    private static Set<String> users(String... users)
    {
        return ImmutableSet.copyOf(users);
    }
}
