package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import java.util.List;

import com.atlassian.httpclient.api.HttpStatus;
import com.atlassian.jira.functest.framework.backdoor.IndexingControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.ReindexStatus;
import com.atlassian.jira.index.request.SharedEntityType;
import com.atlassian.jira.issue.index.IssueIndexingParams;
import com.atlassian.jira.rest.v2.index.ReindexRequestBean;
import com.atlassian.jira.webtests.ztests.indexing.ReindexRequestClient;

import com.google.common.primitives.Longs;
import com.sun.jersey.api.client.UniformInterfaceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThat;

@WebTest ( { Category.FUNC_TEST, Category.REST })
public class TestReindexRequestResource extends RestFuncTest
{
    private static final Logger log = LoggerFactory.getLogger(TestReindexRequestResource.class);

    private ReindexRequestClient reindexRequestClient;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        reindexRequestClient = new ReindexRequestClient(getEnvironmentData());

        clearReindexRequests();
    }

    private void clearReindexRequests()
    {
       backdoor.indexing().clearPendingReindexRequests();
    }

    /**
     * Read status of an existing request.
     */
    public void testReadReindexRequestStatus()
    {
        //Make a request
        ReindexRequestType type = ReindexRequestType.IMMEDIATE;
        String query = "";
        long requestId = makeReindexRequest(type, query);

        ReindexRequestBean request = reindexRequestClient.getReindexRequest(requestId);
        assertEquals("Wrong ID.", requestId, request.getId());
        assertEquals("Wrong type.", ReindexRequestType.IMMEDIATE, request.getType());
        assertEquals("Wrong status.", ReindexStatus.PENDING, request.getStatus());
    }

    /**
     * Should get 404 when reading status of non-existing request.
     */
    public void testReadReindexRequestStatusNotFound()
    {
        try
        {
            reindexRequestClient.getReindexRequest(Integer.MAX_VALUE);
            fail("Should get not found error.");
        }
        catch (UniformInterfaceException e)
        {
            assertEquals("Wrong response status.", HttpStatus.NOT_FOUND.code, e.getResponse().getStatus());
        }
    }

    /**
     * Adds a request, perform reindex and validate state has updated.
     */
    public void testPerformReindex()
    throws InterruptedException
    {
        //Make a request
        ReindexRequestType type = ReindexRequestType.IMMEDIATE;
        String query = "";
        long requestId = makeReindexRequest(type, query);

        List<Long> requests = Longs.asList(reindexRequestClient.performReindex());
        assertThat(requests, hasItem(requestId));
        assertReindexRequestStatusAfterCompletion(requestId, ReindexStatus.COMPLETE);
    }

    /**
     * Adds a request, perform reindex and validate state has updated;
     */
    public void testPerformReindexWithSharedEntities()
    throws InterruptedException
    {
        //Make a request
        ReindexRequestType type = ReindexRequestType.DELAYED;
        String query = "";
        long requestId = makeReindexRequest(type, query, SharedEntityType.SEARCH_REQUEST);

        List<Long> requests = Longs.asList(reindexRequestClient.performReindex());
        assertThat(requests, hasItem(requestId));
        assertReindexRequestStatusAfterCompletion(requestId, ReindexStatus.COMPLETE);
    }

    /**
     * Just reindex comments without issues and tests whether the reindex was successful.
     */
    public void testReindexJustAComment()
    {
        IssueIndexingParams indexingParams = IssueIndexingParams.builder().withComments().withoutIssues().build();
        IndexingControl.IndexingProgress progress = backdoor.indexing().startInBackground(indexingParams);
        progress.waitForCompletion();
        String taskId = progress.getTaskId();
        assertEquals("Reindex was not successful.", Boolean.TRUE, backdoor.indexing().getIndexingTaskProgress(taskId));
    }

    private long makeReindexRequest(ReindexRequestType type, String query, SharedEntityType... sharedEntityTypes)
    {
        return backdoor.indexing().makeReindexRequest(type, query, sharedEntityTypes);
    }

    private void assertReindexRequestStatusAfterCompletion(long requestId, ReindexStatus expectedStatus)
    {
        ReindexRequestBean request;
        int numChecks = 0;
        try
        {
            do
            {
                request = reindexRequestClient.getReindexRequest(requestId);
                assertNotNull("Request not found.", request);
                log.info("Current request status: " + request.getStatus());
                numChecks++;
                Thread.sleep(500L);
            }
            while (request.getStatus() != expectedStatus && numChecks < 20);

            assertEquals("Reindex did not complete with expected status.", expectedStatus, request.getStatus());
        }
        catch (InterruptedException e)
        {
            throw new RuntimeException(e);
        }
    }
}
