package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import java.util.Collections;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import com.atlassian.jira.webtests.ztests.indexing.ReindexIssueClient;

/**
 * Test reindex issue REST API.
 *
 * @since 6.4
 */
@WebTest({Category.FUNC_TEST, Category.REST })
public class TestReindexIssue extends RestFuncTest
{
    private ReindexIssueClient reindexIssueClient;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        reindexIssueClient = new ReindexIssueClient(environmentData);
        administration.restoreData("TestEditIssue.xml");
    }

    public void testReindexIssueByKey()
    {
        //De-index issue and verify it can't be found
        backdoor.indexing().deindex("MKY-1");
        SearchResult result = backdoor.search().getSearch(new SearchRequest().jql("issuekey = 'MKY-1'"));
        assertEquals("Should not find issue.", Collections.emptyList(), result.issues);

        //Use reindex issue REST call to reindex the issue
        Response response = reindexIssueClient.reindexIssue("MKY-1");
        assertEquals("Wrong status code.", 200, response.statusCode);

        //Verify search now works
        result = backdoor.search().getSearch(new SearchRequest().jql("issuekey = 'MKY-1'"));
        assertEquals("Issue not found.", 1, result.issues.size());
    }

    public void testReindexIssueById()
    {
        //De-index issue and verify it can't be found
        backdoor.indexing().deindex("MKY-1");
        SearchResult result = backdoor.search().getSearch(new SearchRequest().jql("issuekey = 'MKY-1'"));
        assertEquals("Should not find issue.", Collections.emptyList(), result.issues);

        //Use reindex issue REST call to reindex the issue
        Response response = reindexIssueClient.reindexIssue("10001");
        assertEquals("Wrong status code.", 200, response.statusCode);

        //Verify search now works
        result = backdoor.search().getSearch(new SearchRequest().jql("issuekey = 'MKY-1'"));
        assertEquals("Issue not found.", 1, result.issues.size());
    }

    public void testReindexMultipleIssues()
    {
        //De-index issue and verify it can't be found
        backdoor.indexing().deindex("MKY-1");
        backdoor.indexing().deindex("MKY-2");
        SearchResult result = backdoor.search().getSearch(new SearchRequest().jql("issuekey = 'MKY-1'"));
        assertEquals("Should not find issue.", Collections.emptyList(), result.issues);
        result = backdoor.search().getSearch(new SearchRequest().jql("issuekey = 'MKY-2'"));
        assertEquals("Should not find issue.", Collections.emptyList(), result.issues);

        //Use reindex issue REST call to reindex the issue
        Response response = reindexIssueClient.reindexIssue("MKY-1", "MKY-2");
        assertEquals("Wrong status code.", 200, response.statusCode);

        //Verify search now works
        result = backdoor.search().getSearch(new SearchRequest().jql("issuekey = 'MKY-1'"));
        assertEquals("Issue not found.", 1, result.issues.size());
        result = backdoor.search().getSearch(new SearchRequest().jql("issuekey = 'MKY-2'"));
        assertEquals("Issue not found.", 1, result.issues.size());
    }

    public void testReindexIssueNotFound()
    {
        Response response = reindexIssueClient.reindexIssue("GALAH");
        assertEquals("Wrong status code.", 404, response.statusCode);
    }

}
