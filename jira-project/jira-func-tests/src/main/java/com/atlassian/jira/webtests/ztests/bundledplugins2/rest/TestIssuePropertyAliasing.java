package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import java.io.IOException;
import java.util.List;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.admin.plugins.ReferencePlugin;
import com.atlassian.jira.functest.framework.backdoor.JqlAutoCompleteControl;
import com.atlassian.jira.functest.framework.backdoor.PluginIndexConfigurationControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.internal.jql.FieldAndPredicateAutoCompleteResultGenerator;
import com.atlassian.jira.testkit.client.restclient.EntityPropertyClient;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import com.atlassian.jira.util.IndexDocumentConfigurationControlUtil;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.query.operator.Operator;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import org.hamcrest.Description;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsCollectionContaining;
import org.joda.time.format.ISODateTimeFormat;

import static com.atlassian.jira.functest.framework.backdoor.JqlAutoCompleteControl.AutoCompleteResult;
import static com.atlassian.jira.util.SearchResultMatcher.issues;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;

@WebTest ({ Category.FUNC_TEST, Category.ENTITY_PROPERTIES, Category.REST, Category.REFERENCE_PLUGIN })
public class TestIssuePropertyAliasing extends FuncTestCase
{
    private static final Label TEST_LABEL = new Label("propertyAliasing", "description of the label", "2014-10-30", 10l);

    private EntityPropertyClient client;
    private PluginIndexConfigurationControl configurationControl;
    private JqlAutoCompleteControl autoCompleteControl;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        this.client = new EntityPropertyClient(environmentData, "issue");
        this.configurationControl = new PluginIndexConfigurationControl(environmentData);
        this.autoCompleteControl = new JqlAutoCompleteControl(environmentData);
        backdoor.restoreBlankInstance();
    }

    public void testJqlAliasSearchingForStrings() throws IOException
    {
        setLabelDocumentConfiguration();

        IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "Issue with aliases issue property");

        setLabelProperty(issue, TEST_LABEL);

        assertSearchResultsForAlias("label", Operator.EQUALS, TEST_LABEL.getValue(), issue);
        assertSearchResultsForAlias("label", Operator.IN, "('" + TEST_LABEL.getValue() + "')", issue);
    }

    public void testJqlAliasingForRelativeDateQueries() throws Exception
    {
        setLabelDocumentConfiguration();

        IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "Issue with aliases issue property");

        setLabelProperty(issue, TEST_LABEL);

        assertSearchResultsForAlias("labelDate", Operator.GREATER_THAN_EQUALS, "'2014-10-29'", issue);
        assertSearchResultsForAlias("labelDate", Operator.LESS_THAN, "'2014-11-01'", issue);
        assertSearchResultsForAlias("labelDate", Operator.LESS_THAN, "'2014-11-01'", issue);
    }


    public void testJqlAliasingForJqlFunctionDateQueries() throws Exception
    {
        setLabelDocumentConfiguration();

        IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "Issue with aliases issue property");

        String date = ISODateTimeFormat.dateTimeNoMillis().print(System.currentTimeMillis());
        Label label = new Label("val", "description", date, 1l);

        setLabelProperty(issue, label);

        assertSearchResultsForAlias("labelDate", Operator.GREATER_THAN_EQUALS, "'-2d'", issue);
        assertSearchResultsForAlias("labelDate", Operator.GREATER_THAN, "startOfDay()", issue);
        assertSearchResultsForAlias("labelDate", Operator.LESS_THAN, "endOfDay()", issue);
        assertSearchResultsForAlias("labelDate", Operator.LESS_THAN_EQUALS, "'2d'", issue);
    }

    public void testJqlAliasingForTextValues() throws Exception
    {
        setLabelDocumentConfiguration();

        IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "Issue with aliases issue property");

        setLabelProperty(issue, TEST_LABEL);

        assertSearchResultsForAlias("labelDescription", Operator.LIKE, "description", issue);
        assertSearchResultsForAlias("labelDescription", Operator.LIKE, "label", issue);
    }

    public void testJqlAliasSearchingForMultipleIssues() throws Exception
    {
        setLabelDocumentConfiguration();

        IssueCreateResponse firstIssue = backdoor.issues().createIssue("HSP", "First issue");
        IssueCreateResponse secondIssue = backdoor.issues().createIssue("HSP", "Second issue");
        IssueCreateResponse thirdIssue = backdoor.issues().createIssue("HSP", "Third issue");

        String date = ISODateTimeFormat.dateTimeNoMillis().print(System.currentTimeMillis());

        Label firstLabel = new Label("first value", "first description", date, 1l);
        Label secondLabel = new Label("second value", "second description", date, 1l);
        Label thirdLabel = new Label("third value", "third description", date, 1l);

        setLabelProperty(firstIssue, firstLabel);
        setLabelProperty(secondIssue, secondLabel);
        setLabelProperty(thirdIssue, thirdLabel);

        assertSearchResultsForAlias("label", Operator.EQUALS, "'" + firstLabel.value + "'", firstIssue);
        assertSearchResultsForAlias("labelDescription", Operator.LIKE, "description", firstIssue, secondIssue, thirdIssue);

        assertSearchResult(new SearchRequest().jql("label = 'second value' AND labelDescription ~ 'description'"), secondIssue);
        assertSearchResult(new SearchRequest().jql("labelDate > '-1d' AND labelDescription ~ 'description'"),firstIssue, secondIssue, thirdIssue);
        assertSearchResult(new SearchRequest().jql("labelId = 1"),firstIssue, secondIssue, thirdIssue);
    }

    public void testConflictingJqlAliases() throws Exception
    {
        updatePluginIndexConfiguration("first plugin", "conflict_indexconf1.xml", configurationControl);
        updatePluginIndexConfiguration("second plugin", "conflict_indexconf2.xml", configurationControl);

        IssueCreateResponse firstIssue = backdoor.issues().createIssue("HSP", "Some issue");
        IssueCreateResponse secondIssue = backdoor.issues().createIssue("HSP", "Some issue");
        String date = ISODateTimeFormat.dateTimeNoMillis().print(System.currentTimeMillis());

        client.put(firstIssue.key, "label", new Label("label", "description", date, 1l).toJson());
        client.put(secondIssue.key, "conflicting_alias", new Label("conflict", "description", date, 1l).toJson());

        assertSearchResultsForAlias("conflict", Operator.EQUALS, "'label'", firstIssue);
        assertSearchResultsForAlias("conflict", Operator.EQUALS, "'conflict'", secondIssue);

        assertSearchResultsForAlias("issue.property[conflicting_alias].value", Operator.EQUALS, "'conflict'", secondIssue);
        assertSearchResultsForAlias("issue.property[label].value", Operator.EQUALS, "'label'", firstIssue);
    }

    public void testAliasAutoCompletionWithoutConflict() throws Exception
    {
        setLabelDocumentConfiguration();

        String date = ISODateTimeFormat.dateTimeNoMillis().print(System.currentTimeMillis());

        String[] values = new String[] {"value1", "value2", "value3", "value4", "value5", "value6"};

        for (String value : values)
        {
            IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "new issue with label value " + value);
            setLabelProperty(issue, new Label(value, "some description", date, 1l));
        }

        assertAutoCompletionResult("label", "val", values);
        assertAutoCompletionResult("label", "value", values);
        assertAutoCompletionResult("label", "value1", "value1");
        assertAutoCompletionResult("label", "value2", "value2");
    }

    public void testAliasWithMoreThen15Matches() throws Exception
    {
        setLabelDocumentConfiguration();

        String date = ISODateTimeFormat.dateTimeNoMillis().print(System.currentTimeMillis());

        for (int i = 0; i < 20; i++)
        {
            IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "new issue " + i);
            setLabelProperty(issue, new Label("value" + i, "some description", date, 1l));
        }

        JqlAutoCompleteControl.Results autoCompleteResults = autoCompleteControl.getAutoCompleteResults("label", "val");
        assertThat(autoCompleteResults.getResults(), hasSize(lessThanOrEqualTo(FieldAndPredicateAutoCompleteResultGenerator.MAX_RESULTS)));
    }

    public void testAutoCompletionWorksForConflictingProperties() throws Exception
    {
        updatePluginIndexConfiguration("first plugin", "conflict_indexconf1.xml", configurationControl);
        updatePluginIndexConfiguration("second plugin", "conflict_indexconf2.xml", configurationControl);

        IssueCreateResponse firstIssue = backdoor.issues().createIssue("HSP", "Some issue");
        IssueCreateResponse secondIssue = backdoor.issues().createIssue("HSP", "Some issue");
        String date = ISODateTimeFormat.dateTimeNoMillis().print(System.currentTimeMillis());

        client.put(firstIssue.key, "label", new Label("some label value", "description", date, 1l).toJson());
        client.put(secondIssue.key, "conflicting_alias", new Label("conflict", "description", date, 1l).toJson());

        assertAutoCompletionResult("issue.property[label].value", "som", "\"some label value\"");
        assertAutoCompletionResult("issue.property[conflicting_alias].value", "conf", "conflict");

        assertThat(autoCompleteControl.getAutoCompleteResults("label", "lab").getResults(), Matchers.<AutoCompleteResult>emptyIterable());
    }

    public void testAutoCompletionReturnsOnlyDistrinctResults()
    {
        setLabelDocumentConfiguration();

        String date = ISODateTimeFormat.dateTimeNoMillis().print(System.currentTimeMillis());

        // sets the same property 20 times
        for (int i = 0; i < 20; i++)
        {
            IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "new issue " + i);
            setLabelProperty(issue, new Label("value", "some description", date, 1l));
        }

        // assert only one result
        assertThat(autoCompleteControl.getAutoCompleteResults("label", "val").getResults(), hasSize(equalTo(1)));
    }

    private void assertAutoCompletionResult(final String fieldName, final String fieldValue, final String... values)
    {
        final JqlAutoCompleteControl.Results results = autoCompleteControl.getAutoCompleteResults(fieldName, fieldValue);
        assertThat(results.getResults(), new TypeSafeMatcher<List<AutoCompleteResult>>()
        {
            @Override
            protected boolean matchesSafely(final List<AutoCompleteResult> autoCompleteResults)
            {
                return IsCollectionContaining.hasItems(values).matches(Lists.transform(autoCompleteResults, new Function<AutoCompleteResult, String>()
                {
                    @Override
                    public String apply(final AutoCompleteResult result)
                    {
                        return result.getValue();
                    }
                }));
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("Auto complete result does not contain").appendValue(values);
            }
        });
    }

    private void assertSearchResultsForAlias(final String alias, final Operator operator, final String value,
            final IssueCreateResponse... issues)
    {
        final SearchRequest searchRequest = new SearchRequest().jql(String.format("%s %s %s", alias, operator.getDisplayString(), value));
        assertSearchResult(searchRequest, issues);
    }

    private void assertSearchResult(final SearchRequest searchRequest, final IssueCreateResponse... issues)
    {
        SearchResult searchResult = backdoor.search().getSearch(searchRequest);
        assertThat(searchResult, issues(Lists.transform(Lists.newArrayList(issues), new Function<IssueCreateResponse, String>()
        {
            @Override
            public String apply(final IssueCreateResponse issueCreateResponse)
            {
                return issueCreateResponse.key();
            }
        })));
    }

    private void setLabelDocumentConfiguration()
    {
        updatePluginIndexConfiguration(ReferencePlugin.KEY, "pluginindexconfiguration1.xml", configurationControl);
    }

    private void setLabelProperty(final IssueCreateResponse issue, final Label label)
    {
        client.put(issue.key, "label", label.toJson());
    }

    private static class Label
    {
        private final String value;
        private final String description;
        private final String date;
        private final Long id;

        private Label(final String value, final String description, final String date, final Long id)
        {
            this.value = value;
            this.description = description;
            this.date = date;
            this.id = id;
        }

        public JSONObject toJson()
        {
            return new JSONObject(ImmutableMap.<String, Object>of("value", value,
                    "description", description,
                    "date", date,
                    "id", id));
        }

        public String getValue()
        {
            return value;
        }

        public String getDescription()
        {
            return description;
        }

        public String getDate()
        {
            return date;
        }

        public Long getId()
        {
            return id;
        }
    }
}
