package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import org.codehaus.jackson.annotate.JsonProperty;

import static com.atlassian.jira.permission.ProjectPermissions.ADD_COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.DELETE_ALL_COMMENTS;
import static java.lang.Boolean.TRUE;

@WebTest ({ Category.FUNC_TEST, Category.REST })
public class TestPermissionsResource extends RestFuncTest
{
    private PermissionsClient permissionsClient;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        permissionsClient = new PermissionsClient(getEnvironmentData());
        administration.restoreData("TestPermissionsRest.xml");
        // The restore didn't restart the plugin system, but we need this module restarted
        backdoor.plugins().disablePluginModule("com.atlassian.jira.dev.func-test-plugin:func.test.global.permission");
        backdoor.plugins().enablePluginModule("com.atlassian.jira.dev.func-test-plugin:func.test.global.permission");
    }

    public void testGlobalPermissions()
    {
        Response<PermissionsOuter> outer = permissionsClient.getPermissions();

        PermissionJsonBean admin = outer.body.permissions.get("ADMINISTER");
        assertNotNull(admin);
        assertEquals("Ability to perform most administration functions (excluding Import & Export, SMTP Configuration, etc.).", admin.description);
        assertEquals("0", admin.id);
        assertEquals("ADMINISTER", admin.key);
        assertEquals("JIRA Administrators", admin.name);
        assertEquals("GLOBAL", admin.type);
        assertNull(admin.deprecatedKey);
        assertEquals(true, admin.havePermission);

        //try a user without these permissions
        outer = permissionsClient.loginAs("user", "user").getPermissions();

        admin = outer.body.permissions.get("ADMINISTER");
        assertNotNull(admin);
        assertEquals(false, admin.havePermission);

        // test a plugged global permission
        PermissionJsonBean plugGlobalPermission = outer.body.permissions.get("func.test.global.permission");
        assertEquals("func.test.global.permission.description", plugGlobalPermission.description);
        assertEquals("func.test.global.permission", plugGlobalPermission.key);
        assertEquals("func.test.global.permission.name", plugGlobalPermission.name);
        assertEquals("GLOBAL", plugGlobalPermission.type);
        assertNull(plugGlobalPermission.deprecatedKey);
        assertEquals(false, plugGlobalPermission.havePermission);
    }

    public void testGetLegacyProjectPermissions()
    {
        Response<PermissionsOuter> outer = permissionsClient.getPermissions();

        // test a project permission
        PermissionJsonBean browse = outer.body.permissions.get("BROWSE");
        assertNotNull(browse);
        assertEquals("Ability to browse projects and the issues within them.", browse.description);
        assertEquals("10", browse.id);
        assertEquals("BROWSE", browse.key);
        assertEquals("Browse Projects", browse.name);
        assertEquals("PROJECT", browse.type);
        assertEquals(TRUE, browse.deprecatedKey);
        assertEquals(true, browse.havePermission);

        // test an issue permission
        PermissionJsonBean comment = outer.body.permissions.get("COMMENT_ISSUE");
        assertNotNull(comment);
        assertEquals("Ability to comment on issues.", comment.description);
        assertEquals("15", comment.id);
        assertEquals("COMMENT_ISSUE", comment.key);
        assertEquals("Add Comments", comment.name);
        assertEquals("PROJECT", browse.type);
        assertEquals(TRUE, browse.deprecatedKey);
        assertEquals(true, comment.havePermission);

        //try a user without these permissions
        outer = permissionsClient.loginAs("user", "user").getPermissions();

        // test a project permission
        browse = outer.body.permissions.get("BROWSE");
        assertNotNull(browse);
        assertEquals(false, browse.havePermission);

        // test an issue permission
        comment = outer.body.permissions.get("COMMENT_ISSUE");
        assertNotNull(comment);
        assertEquals(false, comment.havePermission);
    }

    public void testGetProjectPermissions()
    {
        Response<PermissionsOuter> outer = permissionsClient.getPermissions();

        // test a project permission
        PermissionJsonBean browse = outer.body.permissions.get(BROWSE_PROJECTS.permissionKey());
        assertNotNull(browse);
        assertEquals("Ability to browse projects and the issues within them.", browse.description);
        assertEquals("10", browse.id);
        assertEquals(BROWSE_PROJECTS.permissionKey(), browse.key);
        assertEquals("Browse Projects", browse.name);
        assertEquals("PROJECT", browse.type);
        assertNull(browse.deprecatedKey);
        assertEquals(true, browse.havePermission);

        // test an issue permission
        PermissionJsonBean comment = outer.body.permissions.get(ADD_COMMENTS.permissionKey());
        assertNotNull(comment);
        assertEquals("Ability to comment on issues.", comment.description);
        assertEquals("15", comment.id);
        assertEquals(ADD_COMMENTS.permissionKey(), comment.key);
        assertEquals("Add Comments", comment.name);
        assertEquals("PROJECT", comment.type);
        assertNull(comment.deprecatedKey);
        assertEquals(true, comment.havePermission);

        //try a user without these permissions
        outer = permissionsClient.loginAs("user", "user").getPermissions();

        // test a project permission
        browse = outer.body.permissions.get(BROWSE_PROJECTS.permissionKey());
        assertNotNull(browse);
        assertEquals(false, browse.havePermission);

        // test an issue permission
        comment = outer.body.permissions.get(ADD_COMMENTS.permissionKey());
        assertNotNull(comment);
        assertEquals(false, comment.havePermission);

        // test a plugged project permission
        PermissionJsonBean plugProjectPermission = outer.body.permissions.get("func.test.project.permission");
        assertEquals("func.test.project.permission.description", plugProjectPermission.description);
        assertEquals("func.test.project.permission", plugProjectPermission.key);
        assertEquals("func.test.project.permission.name", plugProjectPermission.name);
        assertEquals("PROJECT", plugProjectPermission.type);
        assertNull(plugProjectPermission.deprecatedKey);
        assertEquals(false, plugProjectPermission.havePermission);
    }

    public void testLegacyProjectFilter()
    {
        Response<PermissionsOuter> outer = permissionsClient.getPermissions();

        // test a project permission
        PermissionJsonBean browse = outer.body.permissions.get("BROWSE");
        assertNotNull(browse);
        assertEquals("Ability to browse projects and the issues within them.", browse.description);
        assertEquals("10", browse.id);
        assertEquals("BROWSE", browse.key);
        assertEquals("Browse Projects", browse.name);
        assertEquals("PROJECT", browse.type);
        assertEquals(TRUE, browse.deprecatedKey);
        assertEquals(true, browse.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("projectKey", "PROJECTA").toMap());
        browse = outer.body.permissions.get("BROWSE");
        assertNotNull(browse);
        assertEquals("10", browse.id);
        assertEquals("Browse Projects", browse.name);
        assertEquals(true, browse.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("projectKey", "PROJECTB").toMap());
        browse = outer.body.permissions.get("BROWSE");
        assertNotNull(browse);
        assertEquals("10", browse.id);
        assertEquals("Browse Projects", browse.name);
        assertEquals(false, browse.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("projectId", "10000").toMap());
        browse = outer.body.permissions.get("BROWSE");
        assertNotNull(browse);
        assertEquals("10", browse.id);
        assertEquals("Browse Projects", browse.name);
        assertEquals(true, browse.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("projectId", "10001").toMap());
        browse = outer.body.permissions.get("BROWSE");
        assertNotNull(browse);
        assertEquals("10", browse.id);
        assertEquals("Browse Projects", browse.name);
        assertEquals(false, browse.havePermission);
    }

    public void testProjectFilter()
    {
        Response<PermissionsOuter> outer = permissionsClient.getPermissions();

        // test a project permission
        PermissionJsonBean browse = outer.body.permissions.get(BROWSE_PROJECTS.permissionKey());
        assertNotNull(browse);
        assertEquals("Ability to browse projects and the issues within them.", browse.description);
        assertEquals("10", browse.id);
        assertEquals(BROWSE_PROJECTS.permissionKey(), browse.key);
        assertEquals("Browse Projects", browse.name);
        assertEquals("PROJECT", browse.type);
        assertNull(browse.deprecatedKey);
        assertEquals(true, browse.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("projectKey", "PROJECTA").toMap());
        browse = outer.body.permissions.get(BROWSE_PROJECTS.permissionKey());
        assertNotNull(browse);
        assertEquals("10", browse.id);
        assertEquals("Browse Projects", browse.name);
        assertEquals(true, browse.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("projectKey", "PROJECTB").toMap());
        browse = outer.body.permissions.get(BROWSE_PROJECTS.permissionKey());
        assertNotNull(browse);
        assertEquals("10", browse.id);
        assertEquals("Browse Projects", browse.name);
        assertEquals(false, browse.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("projectId", "10000").toMap());
        browse = outer.body.permissions.get(BROWSE_PROJECTS.permissionKey());
        assertNotNull(browse);
        assertEquals("10", browse.id);
        assertEquals("Browse Projects", browse.name);
        assertEquals(true, browse.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("projectId", "10001").toMap());
        browse = outer.body.permissions.get(BROWSE_PROJECTS.permissionKey());
        assertNotNull(browse);
        assertEquals("10", browse.id);
        assertEquals("Browse Projects", browse.name);
        assertEquals(false, browse.havePermission);
    }

    public void testLegacyIssueFiltering()
    {
        Response<PermissionsOuter> outer = permissionsClient.getPermissions();
        PermissionJsonBean commentDeleteAll = outer.body.permissions.get("COMMENT_DELETE_ALL");
        assertNotNull(commentDeleteAll);
        assertEquals("Ability to delete all comments made on issues.", commentDeleteAll.description);
        assertEquals("36", commentDeleteAll.id);
        assertEquals("COMMENT_DELETE_ALL", commentDeleteAll.key);
        assertEquals("Delete All Comments", commentDeleteAll.name);
        assertEquals("PROJECT", commentDeleteAll.type);
        assertEquals(TRUE, commentDeleteAll.deprecatedKey);
        assertEquals(true, commentDeleteAll.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("projectKey", "PROJECTA").toMap());
        commentDeleteAll = outer.body.permissions.get("COMMENT_DELETE_ALL");
        assertNotNull(commentDeleteAll);
        assertEquals("36", commentDeleteAll.id);
        assertEquals("COMMENT_DELETE_ALL", commentDeleteAll.key);
        assertEquals(true, commentDeleteAll.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("issueKey", "PROJECTA-3").toMap());
        commentDeleteAll = outer.body.permissions.get("COMMENT_DELETE_ALL");
        assertNotNull(commentDeleteAll);
        assertEquals("36", commentDeleteAll.id);
        assertEquals("COMMENT_DELETE_ALL", commentDeleteAll.key);
        assertEquals(true, commentDeleteAll.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("issueKey", "PROJECTA-4").toMap());
        commentDeleteAll = outer.body.permissions.get("COMMENT_DELETE_ALL");
        assertNotNull(commentDeleteAll);
        assertEquals("36", commentDeleteAll.id);
        assertEquals("COMMENT_DELETE_ALL", commentDeleteAll.key);
        assertEquals(false, commentDeleteAll.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("issueId", "10200").toMap());
        commentDeleteAll = outer.body.permissions.get("COMMENT_DELETE_ALL");
        assertNotNull(commentDeleteAll);
        assertEquals("36", commentDeleteAll.id);
        assertEquals("COMMENT_DELETE_ALL", commentDeleteAll.key);
        assertEquals(true, commentDeleteAll.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("issueId", "10201").toMap());
        commentDeleteAll = outer.body.permissions.get("COMMENT_DELETE_ALL");
        assertNotNull(commentDeleteAll);
        assertEquals("36", commentDeleteAll.id);
        assertEquals("COMMENT_DELETE_ALL", commentDeleteAll.key);
        assertEquals(false, commentDeleteAll.havePermission);
    }

    public void testIssueFiltering()
    {
        Response<PermissionsOuter> outer = permissionsClient.getPermissions();
        PermissionJsonBean commentDeleteAll = outer.body.permissions.get(DELETE_ALL_COMMENTS.permissionKey());
        assertNotNull(commentDeleteAll);
        assertEquals("Ability to delete all comments made on issues.", commentDeleteAll.description);
        assertEquals("36", commentDeleteAll.id);
        assertEquals(DELETE_ALL_COMMENTS.permissionKey(), commentDeleteAll.key);
        assertEquals("Delete All Comments", commentDeleteAll.name);
        assertEquals("PROJECT", commentDeleteAll.type);
        assertNull(commentDeleteAll.deprecatedKey);
        assertEquals(true, commentDeleteAll.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("projectKey", "PROJECTA").toMap());
        commentDeleteAll = outer.body.permissions.get(DELETE_ALL_COMMENTS.permissionKey());
        assertNotNull(commentDeleteAll);
        assertEquals("36", commentDeleteAll.id);
        assertEquals(DELETE_ALL_COMMENTS.permissionKey(), commentDeleteAll.key);
        assertEquals(true, commentDeleteAll.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("issueKey", "PROJECTA-3").toMap());
        commentDeleteAll = outer.body.permissions.get(DELETE_ALL_COMMENTS.permissionKey());
        assertNotNull(commentDeleteAll);
        assertEquals("36", commentDeleteAll.id);
        assertEquals(DELETE_ALL_COMMENTS.permissionKey(), commentDeleteAll.key);
        assertEquals(true, commentDeleteAll.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("issueKey", "PROJECTA-4").toMap());
        commentDeleteAll = outer.body.permissions.get(DELETE_ALL_COMMENTS.permissionKey());
        assertNotNull(commentDeleteAll);
        assertEquals("36", commentDeleteAll.id);
        assertEquals(DELETE_ALL_COMMENTS.permissionKey(), commentDeleteAll.key);
        assertEquals(false, commentDeleteAll.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("issueId", "10200").toMap());
        commentDeleteAll = outer.body.permissions.get(DELETE_ALL_COMMENTS.permissionKey());
        assertNotNull(commentDeleteAll);
        assertEquals("36", commentDeleteAll.id);
        assertEquals(DELETE_ALL_COMMENTS.permissionKey(), commentDeleteAll.key);
        assertEquals(true, commentDeleteAll.havePermission);

        outer = permissionsClient.getPermissions(MapBuilder.<String, String>newBuilder().add("issueId", "10201").toMap());
        commentDeleteAll = outer.body.permissions.get(DELETE_ALL_COMMENTS.permissionKey());
        assertNotNull(commentDeleteAll);
        assertEquals("36", commentDeleteAll.id);
        assertEquals(DELETE_ALL_COMMENTS.permissionKey(), commentDeleteAll.key);
        assertEquals(false, commentDeleteAll.havePermission);
    }
    
    // test error reporting


    private class PermissionsClient extends RestApiClient<PermissionsClient>
    {
        protected PermissionsClient(JIRAEnvironmentData environmentData)
        {
            super(environmentData);
        }

        private Response<PermissionsOuter> getPermissions()
        {
            return toResponse(new Method()
            {
                @Override
                public ClientResponse call()
                {
                    return createResource().path("mypermissions").get(ClientResponse.class);
                }
            }, PermissionsOuter.class);
        }

        private Response<PermissionsOuter> getPermissions(final Map<String, String> params)
        {
            return toResponse(new Method()
            {
                @Override
                public ClientResponse call()
                {
                    WebResource path = createResource().path("mypermissions");
                    for (Map.Entry<String, String> entry : params.entrySet())
                    {
                        path = path.queryParam(entry.getKey(), entry.getValue());
                    }
                    return path.get(ClientResponse.class);
                }
            }, PermissionsOuter.class);
        }
    }

    private static class PermissionsOuter
    {
        @JsonProperty
        HashMap<String, PermissionJsonBean> permissions;
    }

    private static class PermissionJsonBean
    {
        @JsonProperty
        String id;
        @JsonProperty
        String key;
        @JsonProperty
        String name;
        @JsonProperty
        Boolean deprecatedKey;
        @JsonProperty
        String description;
        @JsonProperty
        boolean havePermission;
        @JsonProperty
        private String type;
    }
}
