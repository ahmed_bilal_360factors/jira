package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.jira.functest.framework.backdoor.IssuePickerControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.IssueUpdateRequest;
import com.atlassian.jira.testkit.client.restclient.IssueClient;

import com.google.common.base.Function;
import com.google.inject.internal.Lists;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import static com.atlassian.jira.functest.framework.backdoor.IssuePickerControl.IssuePickerIssue;
import static com.atlassian.jira.functest.framework.backdoor.IssuePickerControl.IssuePickerResult;
import static com.atlassian.jira.functest.framework.backdoor.IssuePickerControl.IssueSection;
import static com.atlassian.jira.rest.api.issue.ResourceRef.withId;
import static com.atlassian.jira.rest.api.issue.ResourceRef.withKey;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

@WebTest ({ Category.FUNC_TEST, Category.REST, Category.ISSUES })
public class TestIssuePickerResource extends RestFuncTest
{
    private ArrayList<IssuePickerControl> issuePickerControls;
    private IssueClient issueClient;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        backdoor.restoreBlankInstance();
        this.issuePickerControls = Lists.newArrayList(IssuePickerControl.version1(environmentData), IssuePickerControl.version2(environmentData));
        this.issueClient = new IssueClient(environmentData);
    }

    public void testCurrentIssueNotInIssuePickerResultList()
    {
        long demoProjectId = backdoor.project().addProject("Demonstration project", "DEMO", "admin");
        IssueCreateResponse issue1 = backdoor.issues().createIssue(demoProjectId, "first issue, which will not appear in autocomplete");
        IssueCreateResponse issue2 = backdoor.issues().createIssue(demoProjectId, "second issue which will appear in autocomplete");
        for (IssuePickerControl pickerControl : issuePickerControls)
        {
            IssuePickerResult pickerResult = pickerControl.getIssuePickerResult("DE", null, "DEMO-1", String.valueOf(demoProjectId), true, true);

            assertThat(flattenSections(pickerResult), not(Matchers.<IssuePickerIssue>hasItem(pickerIssueMatcher(issue1.key))));
            assertThat(flattenSections(pickerResult), Matchers.<IssuePickerIssue>hasItem(pickerIssueMatcher(issue2.key)));
        }
    }

    public void testOnlyIssuesFromSelectedProjectInAutoCompleteResult()
    {
        long projectInAutoCompleteResult = backdoor.project().addProject("one project", "DEN", "admin");
        long notInAutoCompleteResults = backdoor.project().addProject("second project", "DEM", "admin");

        IssueCreateResponse issue1 = backdoor.issues().createIssue(notInAutoCompleteResults, "issue which will appear in autocomplete");
        IssueCreateResponse issue2 = backdoor.issues().createIssue(projectInAutoCompleteResult, "issue, which will not appear in autocomplete");

        for (IssuePickerControl pickerControl : issuePickerControls)
        {
            IssuePickerResult pickerResult = pickerControl.getIssuePickerResult("DE", null, null, String.valueOf(projectInAutoCompleteResult), true, true);

            assertThat(flattenSections(pickerResult), not(Matchers.<IssuePickerIssue>hasItem(pickerIssueMatcher(issue1.key))));
            assertThat(flattenSections(pickerResult), Matchers.<IssuePickerIssue>hasItem(pickerIssueMatcher(issue2.key)));
        }
    }

    public void testOnlyIssuesMatchingJqlQueryIncludedInResults()
    {
        long projectInAutoCompleteResult = backdoor.project().addProject("one project", "DEN", "admin");
        long notInAutoCompleteResults = backdoor.project().addProject("second project", "DEM", "admin");

        IssueCreateResponse issue1 = backdoor.issues().createIssue(notInAutoCompleteResults, "issue which will appear in autocomplete");
        IssueCreateResponse issue2 = backdoor.issues().createIssue(projectInAutoCompleteResult, "issue, which will not appear in autocomplete");

        for (IssuePickerControl pickerControl : issuePickerControls)
        {
            IssuePickerResult pickerResult = pickerControl.getIssuePickerResult("DE", "project = DEN", null, null, true, true);

            assertThat(flattenSections(pickerResult), not(Matchers.<IssuePickerIssue>hasItem(pickerIssueMatcher(issue1.key))));
            assertThat(flattenSections(pickerResult), Matchers.<IssuePickerIssue>hasItem(pickerIssueMatcher(issue2.key)));
        }
    }

    public void testSubTasksDisplayedInAutoCompletionResult()
    {
        backdoor.subtask().enable();
        long project = backdoor.project().addProject("one project", "DEN", "admin");
        IssueCreateResponse issue1 = backdoor.issues().createIssue(project, "Issue 1");
        IssueCreateResponse issue2 = backdoor.issues().createIssue(project, "Issue 2");
        IssueCreateResponse subtask = createSubtask(project, issue1.key);

        for (IssuePickerControl pickerControl : issuePickerControls)
        {
            IssuePickerResult pickerResult = pickerControl.getIssuePickerResult("DE", "project = DEN", null, null, true, true);

            assertThat(flattenSections(pickerResult), Matchers.<IssuePickerIssue>hasItems(pickerIssueMatcher(issue2.key), pickerIssueMatcher(issue1.key), pickerIssueMatcher(subtask.key)));

            IssuePickerResult pickerResultsWithoutSubTasks = pickerControl.getIssuePickerResult("DE", "project = DEN", null, null, false, true);

            assertThat(flattenSections(pickerResultsWithoutSubTasks), not(Matchers.<IssuePickerIssue>hasItem(pickerIssueMatcher(subtask.key))));
            assertThat(flattenSections(pickerResultsWithoutSubTasks), Matchers.<IssuePickerIssue>hasItems(pickerIssueMatcher(issue1.key), pickerIssueMatcher(issue2.key)));
        }
    }

    public void testParentIssueIncludedInAutoCompletionResult()
    {
        backdoor.subtask().enable();
        long project = backdoor.project().addProject("one project", "DEN", "admin");
        IssueCreateResponse issue1 = backdoor.issues().createIssue(project, "Issue 1");
        IssueCreateResponse issue2 = backdoor.issues().createIssue(project, "Issue 2");
        IssueCreateResponse subtask = createSubtask(project, issue1.key);

        for (IssuePickerControl pickerControl : issuePickerControls)
        {
            IssuePickerResult pickerResult = pickerControl.getIssuePickerResult("DE", null, subtask.key, null, true, true);

            assertThat(flattenSections(pickerResult), Matchers.<IssuePickerIssue>hasItems(pickerIssueMatcher(issue2.key), pickerIssueMatcher(issue1.key)));

            IssuePickerResult pickerResultWithoutParent = pickerControl.getIssuePickerResult("DE", null, subtask.key, null, true, false);

            assertThat(flattenSections(pickerResultWithoutParent), Matchers.<IssuePickerIssue>hasItems(pickerIssueMatcher(issue2.key)));
            assertThat(flattenSections(pickerResultWithoutParent), not(Matchers.<IssuePickerIssue>hasItems(pickerIssueMatcher(issue1.key))));
        }
    }

    private IssueCreateResponse createSubtask(final long project, final String parentKey)
    {
        IssueFields fields = new IssueFields();
        fields.project(withId(String.valueOf(project)));
        fields.parent(withKey(parentKey));
        fields.issueType(withId("10000"));
        fields.priority(withId("1"));
        fields.summary("subtask");

        return issueClient.create(new IssueUpdateRequest().fields(fields));
    }

    private Iterable<IssuePickerIssue> flattenSections(IssuePickerResult result)
    {
        return concat(transform(result.getSections(), new Function<IssueSection, List<IssuePickerIssue>>()
        {
            @Override
            public List<IssuePickerIssue> apply(final IssueSection issueSection)
            {
                return issueSection.getIssues();
            }
        }));
    }

    private Matcher<IssuePickerIssue> pickerIssueMatcher(final String issueKey)
    {
        return hasProperty("key", equalTo(issueKey));
    }

}
