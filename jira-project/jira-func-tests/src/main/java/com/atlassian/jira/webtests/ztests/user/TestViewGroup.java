package com.atlassian.jira.webtests.ztests.user;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.admin.IssueSecurityLevel;
import com.atlassian.jira.functest.framework.admin.NotificationType;
import com.atlassian.jira.functest.framework.locator.CssLocator;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;

import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;

/**
 * Test the view group page.
 *
 * @since v3.13
 */
@WebTest ({ Category.FUNC_TEST, Category.USERS_AND_GROUPS, Category.FILTERS })
public class TestViewGroup extends FuncTestCase
{
    public static final int CREATE_SHARED_FILTER = 22;
    private static final String TEST_GROUP = "test_group";

    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreBlankInstance();
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    /**
     * Ensure that shared group filters are reported.
     */
    public void testGroupSharedFiltersReported()
    {
        gotoViewGroup("jira-users");

        //there should be no saved filters in this group now.
        assertions.assertNodeHasText(new CssLocator(tester, "table.aui td"), "There are no Saved Filters associated with this Group.");

        administration.addGlobalPermission(CREATE_SHARED_FILTER, "jira-users");
        long adminId = Long.parseLong(backdoor.filters().createFilter("", "AdministratorFilter", ADMIN_USERNAME, "jira-users"));
        long fredId = Long.parseLong(backdoor.filters().createFilter("", "FredFilter", FRED_USERNAME, "jira-users"));

        gotoViewGroup("jira-users");
        assertions.assertNodeHasText(new CssLocator(tester, "table.aui td"), "AdministratorFilter (Owner: " + ADMIN_FULLNAME + ")");
        assertions.assertNodeHasText(new CssLocator(tester, "table.aui td"), "FredFilter (Owner: " + FRED_FULLNAME + ")");

        navigation.manageFilters().deleteFilter((int) adminId);

        gotoViewGroup("jira-users");
        assertions.assertNodeDoesNotHaveText(new CssLocator(tester, "table.aui td"), "AdministratorFilter (Owner: " + ADMIN_FULLNAME + ")");
        assertions.assertNodeHasText(new CssLocator(tester, "table.aui td"), "FredFilter (Owner: " + FRED_FULLNAME + ")");

        navigation.logout();
        navigation.login(FRED_USERNAME);

        navigation.manageFilters().deleteFilter((int) fredId);

        navigation.logout();
        navigation.login(ADMIN_USERNAME);

        gotoViewGroup("jira-users");
        assertions.assertNodeDoesNotHaveText(new CssLocator(tester, "table.aui td"), "AdministratorFilter (Owner: " + ADMIN_FULLNAME + ")");
        assertions.assertNodeDoesNotHaveText(new CssLocator(tester, "table.aui td"), "FredFilter (Owner: " + FRED_FULLNAME + ")");

        assertions.assertNodeHasText(new CssLocator(tester, "table.aui td"), "There are no Saved Filters associated with this Group.");
    }

    /**
     * Test for JRA-15837
     */
    public void testViewSchemes()
    {
        // add a test_group
        navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
        tester.setFormElement("addName", TEST_GROUP);
        tester.submit("add_group");
        tester.clickLinkWithText(TEST_GROUP);
        text.assertTextPresent(locator.page(), "There are no Permission Schemes associated with this Group.");
        text.assertTextPresent(locator.page(), "There are no Notification Schemes associated with this Group.");
        text.assertTextPresent(locator.page(), "There are no Saved Filters associated with this Group.");
        text.assertTextPresent(locator.page(), "There are no Issue Security Schemes associated with this Group.");

        // now add this group to various schemes and then check that its there

        // do permission schemes first
        administration.permissionSchemes().defaultScheme().grantPermissionToGroup(ADMINISTER_PROJECTS.permissionKey(), TEST_GROUP);

        // do notification schemes next
        administration.notificationSchemes().goTo().editNotifications(10000).addNotificationsForEvent(1, NotificationType.GROUP, TEST_GROUP);

        // do issue security next
        navigation.gotoAdminSection(Navigation.AdminSection.SECURITY_SCHEMES);
        // Click Link 'Add Issue Security Scheme' (id='add_securityscheme').
        administration.issueSecuritySchemes().newScheme("Test Issue Security Scheme","").newLevel("Code Red","").addIssueSecurity(IssueSecurityLevel.IssueSecurity.GROUP, TEST_GROUP);

        // now assert they are present
        gotoViewGroup(TEST_GROUP);
        text.assertTextPresent(locator.page(), "Default Permission Scheme");
        text.assertTextPresent(locator.page(), "Default Notification Scheme");
        text.assertTextPresent(locator.page(), "Test Issue Security Scheme");

        // now make sure its doesn't cache

        // Click Link 'Notification Schemes' (id='notification_schemes').
        navigation.gotoAdminSection(Navigation.AdminSection.NOTIFICATION_SCHEMES);
        // Click Link 'Notifications' (id='10000_edit').
        tester.clickLink("10000_edit");
        // Click Link 'Delete' (id='del_10060').
        tester.clickLink("del_10160");
        tester.submit("Delete");

        // Click Link 'Permission Schemes' (id='permission_schemes').
        navigation.gotoAdminSection(Navigation.AdminSection.PERMISSION_SCHEMES);
        // Click Link 'Permissions' (id='0_edit').
        tester.clickLink("0_edit");
        tester.clickLink("del_perm_" + ADMINISTER_PROJECTS.permissionKey() + "_test_group");
        tester.submit("Delete");

        // issue security schemes next
        // Click Link 'Issue Security Schemes' (id='security_schemes').
        navigation.gotoAdminSection(Navigation.AdminSection.SECURITY_SCHEMES);
        // Click Link 'Delete' (id='del_Test Issue Security Scheme').
        tester.clickLink("del_Test Issue Security Scheme");
        tester.submit("Delete");

        // should now be removed and not cached
        gotoViewGroup(TEST_GROUP);
        text.assertTextPresent(locator.page(), "There are no Permission Schemes associated with this Group.");
        text.assertTextPresent(locator.page(), "There are no Notification Schemes associated with this Group.");
        text.assertTextPresent(locator.page(), "There are no Saved Filters associated with this Group.");
        text.assertTextPresent(locator.page(), "There are no Issue Security Schemes associated with this Group.");
    }

    private void gotoViewGroup(final String group)
    {
        tester.gotoPage("secure/admin/user/ViewGroup.jspa?name=" + group);
        assertions.assertNodeHasText(new CssLocator(tester, ".aui-page-header-main .aui-nav-breadcrumbs a"), "Groups");
        assertions.assertNodeHasText(new CssLocator(tester, ".aui-page-header-main h2"), group);
    }
}

