package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.restclient.EntityPropertyClient;
import com.atlassian.jira.testkit.client.restclient.EntityPropertyKeys;
import com.atlassian.jira.util.json.JSONObject;
import com.google.common.collect.ImmutableMap;
import com.sun.jersey.api.client.WebResource;
import org.hamcrest.Matcher;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import javax.ws.rs.core.Response;

import static com.atlassian.jira.testkit.client.RestApiClient.REST_VERSION;
import static com.atlassian.jira.webtests.ztests.bundledplugins2.rest.util.PropertyAssertions.assertUniformInterfaceException;
import static com.atlassian.jira.webtests.ztests.bundledplugins2.rest.util.PropertyAssertions.propertyKey;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

@WebTest ({ Category.FUNC_TEST, Category.ENTITY_PROPERTIES, Category.REST })
public final class TestDashboardItemPropertyResource extends FuncTestCase
{
    private final static String DASHBOARD_ID = "10010";
    private final static String DASHBOARD_ID_SHARED = "10110";
    private final static String DASHBOARD_ID_NOT_EXITING = "20010";
    private final static String DASHBOARD_ID_SYSTEM = "10000";
    private final static String ITEM_ID_1 = "10010";
    private final static String ITEM_ID_2 = "10011";
    private final static String ITEM_ID_OF_SHARED_DASHBOARD = "10110";
    private final static String ITEM_ID_THAT_DOES_NOT_EXIST = "12345";

    private DashboardItemPropertyClient client;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        this.client = new DashboardItemPropertyClient(environmentData).dashboardId(DASHBOARD_ID);
        restoreInstanceData();
        administration.usersAndGroups().addUser(BOB_USERNAME, BOB_PASSWORD, BOB_FULLNAME, BOB_EMAIL);
    }

    public void testAddingDashboardItemProperty()
    {
        client.put(ITEM_ID_1, "prop1", value(1));
        client.put(ITEM_ID_1, "prop2", value(2));
        client.put(ITEM_ID_2, "prop3", value(3));

        List<EntityPropertyKeys.EntityPropertyKey> item1Keys = client.getKeys(ITEM_ID_1).keys;
        assertThat(item1Keys, hasSize(2));
        assertThat(item1Keys, (Matcher) hasItems(propertyKey("prop1"), propertyKey("prop2")));

        List<EntityPropertyKeys.EntityPropertyKey> item2Keys = client.getKeys(ITEM_ID_2).keys;
        assertThat(item2Keys, hasSize(1));
        assertThat(item2Keys, (Matcher) hasItem(propertyKey("prop3")));
    }

    public void testAccessingDashboardItemInWrongDashboardReturns404()
    {
        assertNotFoundResponse(DASHBOARD_ID_NOT_EXITING, ITEM_ID_1);
        assertNotFoundResponse(DASHBOARD_ID_SYSTEM, ITEM_ID_1);
    }

    public void testAccessingNonExistingDashboardItemReturns404()
    {
        assertNotFoundResponse(DASHBOARD_ID, ITEM_ID_THAT_DOES_NOT_EXIST);
    }

    public void testAccessingDashboardAsLoggedInUserWithoutReadPermissionsReturns404()
    {
        client.loginAs("bob");
        assertNotFoundResponseOnAccess(DASHBOARD_ID, ITEM_ID_1);
    }

    public void testAccessingDashboardAsAnonymousWithoutReadPermissionsReturns404()
    {
        client.anonymous();
        assertNotFoundResponseOnAccess(DASHBOARD_ID, ITEM_ID_1);
    }

    public void testAccessingPropertiesOfADashboardItemSharedWithEveryoneAsAnonymousWorks()
    {
        client.anonymous();
        EntityPropertyKeys keys = client.dashboardId(DASHBOARD_ID_SHARED).getKeys(DASHBOARD_ID_SHARED);
        assertThat(keys.keys, hasSize(0));
    }

    public void testUserMustHaveWriteAccessToTheDashboardToSetProperties()
    {
        client.dashboardId(DASHBOARD_ID_SHARED);
        client.loginAs(BOB_USERNAME);
        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                client.put(ITEM_ID_OF_SHARED_DASHBOARD, "prop1", value(1));
                return null;
            }
        }, Response.Status.FORBIDDEN);
    }

    public void testUserMustHaveReadAccessToTheDashboardToAccessProperties()
    {
        client.dashboardId(DASHBOARD_ID).put(ITEM_ID_1, "prop1", value(1));

        client.loginAs(BOB_USERNAME);


        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                client.dashboardId(DASHBOARD_ID).get(ITEM_ID_1, "prop1");
                return null;
            }
        }, Response.Status.NOT_FOUND);
    }

    public void testUserCanReadPropertiesOfSharedDashboard()
    {
        client.dashboardId(DASHBOARD_ID_SHARED).put(ITEM_ID_OF_SHARED_DASHBOARD, "prop1", value(1));
        client.loginAs(BOB_USERNAME);
        assertThat(client.dashboardId(DASHBOARD_ID_SHARED).get(ITEM_ID_OF_SHARED_DASHBOARD, "prop1").value, equalTo(valueAsMap(1)));
    }

    public void testPropertyDelete()
    {
        client.put(ITEM_ID_1, "prop1", value(1));
        client.put(ITEM_ID_2, "prop2", value(1));
        client.delete(ITEM_ID_1, "prop1");
        assertThat(client.getKeys(ITEM_ID_1).keys, hasSize(0));
        assertThat(client.getKeys(ITEM_ID_2).keys, hasSize(1)); // assert that properties of the other item are unaffected
    }

    public void testUpdatingProperty()
    {
        client.put(ITEM_ID_1, "prop1", value(1));
        assertThat(client.get(ITEM_ID_1, "prop1").value, equalTo(valueAsMap(1)));
        client.put(ITEM_ID_1, "prop1", value(2));
        assertThat(client.get(ITEM_ID_1, "prop1").value, equalTo(valueAsMap(2)));
    }

    public void testSelfLink()
    {
        client.put(ITEM_ID_1, "prop1", value(1));
        EntityPropertyKeys.EntityPropertyKey key = client.getKeys(ITEM_ID_1).keys.get(0);
        assertThat(key.self, equalTo(String.format(environmentData.getBaseUrl().toString() + "/rest/api/%s/dashboard/%s/items/%s/properties/prop1", REST_VERSION, DASHBOARD_ID, ITEM_ID_1)));
    }

    private void assertNotFoundResponse(String dashboardId, final String itemId)
    {
        client.dashboardId(dashboardId);
        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                client.put(itemId, "prop1", value(1));
                return null;
            }
        }, Response.Status.NOT_FOUND, String.format("dashboard %s does not contain item %s or you do not have permissions to view the dashboard", dashboardId, itemId));
    }

    private void assertNotFoundResponseOnAccess(String dashboardId, final String itemId)
    {
        client.dashboardId(dashboardId);
        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                client.getKeys(itemId);
                return null;
            }
        }, Response.Status.NOT_FOUND, String.format("dashboard %s does not contain item %s or you do not have permissions to view the dashboard", dashboardId, itemId));
    }

    private JSONObject value(final int value)
    {
        return new JSONObject(valueAsMap(value));
    }

    private Map<String, Object> valueAsMap(final int value)
    {
        return ImmutableMap.<String, Object>of("value", value);
    }

    /**
     * This method imports data with dashboard items configured for the admin user.
     *
     * <p>Dashboard with id 10010 has two gadgets of ids 10010 and 10011. This dashboard is not shared</p>
     * <p>Dashboard with id 10110 has one gadget of id 10110. This dashboard is shared with everyone</p>
     *
     */
    private void restoreInstanceData()
    {
        administration.restoreData("DashboardItems.xml");
    }

    private static class DashboardItemPropertyClient extends EntityPropertyClient
    {
        private String dashboardId;

        public DashboardItemPropertyClient(final JIRAEnvironmentData environmentData)
        {
            super(environmentData, "items");
        }

        public DashboardItemPropertyClient dashboardId(String id)
        {
            this.dashboardId = id;
            return this;
        }

        @Override
        protected WebResource createResource()
        {
            if (dashboardId == null)
            {
                throw new IllegalArgumentException("dashboardId not specified, use the dashboardId(id) method first");
            }
            return super.createResource().path("dashboard").path(dashboardId);
        }
    }
}
