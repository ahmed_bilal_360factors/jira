package com.atlassian.jira.webtests.ztests.navigator.jql;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;

import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;

/**
 * @since v6.4
 */
@WebTest ({ Category.FUNC_TEST, Category.JQL })
public class TestJqlNumberFields extends AbstractJqlFuncTest
{
    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("TestJqlNumberFields.xml");
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);
    }

    public void testNumberField()
    {
        // Test sequence
        assertOrderedSearchWithResults("order by testnumberfield DESC", "HSP-7", "HSP-3", "HSP-2", "HSP-1", "HSP-8", "HSP-6", "HSP-9", "HSP-4", "HSP-5");
        assertOrderedSearchWithResults("order by testnumberfield ASC", "HSP-5", "HSP-4", "HSP-9", "HSP-6", "HSP-8", "HSP-1", "HSP-2", "HSP-3", "HSP-7");
        // Test equality
        assertSearchWithResults("testnumberfield = 40", "HSP-3");
        assertSearchWithResults("testnumberfield = -50", "HSP-5");
        assertSearchWithResults("testnumberfield = -12.1", "HSP-4");
        assertSearchWithResults("testnumberfield = 0", "HSP-6");
        assertSearchWithResults("testnumberfield = 102");
        assertSearchWithResults("testnumberfield = -102");
        assertSearchWithResults("testnumberfield IN (40, -1)", "HSP-9", "HSP-3");

        // Test inequality
        assertSearchWithResults("testnumberfield != 40", "HSP-9", "HSP-8", "HSP-6", "HSP-5", "HSP-4", "HSP-2", "HSP-1");
        assertSearchWithResults("testnumberfield != -50", "HSP-9", "HSP-8", "HSP-6", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
        assertSearchWithResults("testnumberfield != -12.1", "HSP-9", "HSP-8", "HSP-6", "HSP-5", "HSP-3", "HSP-2", "HSP-1");

        // Test range queries
        assertSearchWithResults("testnumberfield > 0", "HSP-8", "HSP-3", "HSP-2", "HSP-1");
        assertSearchWithResults("testnumberfield < 0", "HSP-9", "HSP-5", "HSP-4");
        assertSearchWithResults("testnumberfield > 20", "HSP-3", "HSP-2");
        assertSearchWithResults("testnumberfield > 30", "HSP-3");
        assertSearchWithResults("testnumberfield >=30", "HSP-3", "HSP-2");
        assertSearchWithResults("testnumberfield < -10", "HSP-5", "HSP-4");
        assertSearchWithResults("testnumberfield <= -12", "HSP-5", "HSP-4");
        assertSearchWithResults("testnumberfield <= -12.5", "HSP-5");
        assertSearchWithResults("testnumberfield > -15", "HSP-9", "HSP-8", "HSP-6", "HSP-4", "HSP-3", "HSP-2", "HSP-1");
        assertSearchWithResults("testnumberfield <= -1", "HSP-9", "HSP-5", "HSP-4");
        assertSearchWithResults("testnumberfield > -1", "HSP-8", "HSP-6", "HSP-3", "HSP-2", "HSP-1");
    }

}