package com.atlassian.jira.functest.matcher;

import com.atlassian.jira.functest.framework.backdoor.LicenseRoleControl;
import static com.atlassian.jira.functest.framework.backdoor.LicenseRoleControl.BUSINESS_USER;
import com.google.common.base.Objects;
import com.google.common.collect.Sets;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.util.Locale;
import java.util.Set;

public class LicenseRoleBeanMatcher extends TypeSafeDiagnosingMatcher<LicenseRoleControl.LicenseRoleBean>
{
    private final String id;
    private final String name;
    private Set<String> groups = Sets.newHashSet();
    private String primaryGroup;

    public static LicenseRoleBeanMatcher forBusinessUser()
    {
        return new LicenseRoleBeanMatcher(BUSINESS_USER, "Business User");
    }

    public LicenseRoleBeanMatcher(String id, String name)
    {
        this.id = id.toLowerCase(Locale.ENGLISH);
        this.name = name;
    }

    @Override
    protected boolean matchesSafely(final LicenseRoleControl.LicenseRoleBean item, final Description mismatchDescription)
    {
        if (Objects.equal(name, item.getName())
                && groups.containsAll(item.getGroups())
                && Objects.equal(id, item.getId())
                && Objects.equal(primaryGroup, item.getPrimaryGroup()))
        {
            return true;
        }
        else
        {
            mismatchDescription.appendValue(String.format("[name: %s, groups: %s, id: %s, primary: %s]",
                    item.getName(), item.getGroups(), item.getId(), item.getPrimaryGroup()));

            return false;
        }
    }

    @Override
    public void describeTo(final Description description)
    {
        description.appendText(String.format("[name: %s, groups: %s, id: %s, primary: %s]",
                name, groups, id, primaryGroup));
    }

    public LicenseRoleBeanMatcher setGroups(final String...groups)
    {
        this.groups = Sets.newHashSet(groups);
        return this;
    }

    public LicenseRoleBeanMatcher setPrimaryGroup(final String primaryGroup)
    {
        this.primaryGroup = primaryGroup;
        return this;
    }
}
