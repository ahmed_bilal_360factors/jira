package com.atlassian.jira.functest.framework.backdoor;

import java.util.List;
import java.util.Locale;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

import com.google.common.base.Objects;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since v6.4
 */
public class ResourceBundleCacheControl extends BackdoorControl<ResourceBundleCacheControl>
{

    public ResourceBundleCacheControl(final JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public List<CachedPluginBundle> getCachedPluginResourceBundles()
    {
        final WebResource resource = createResource().path("resourcebundle/cachedpluginbundles");
        return resource.get(new GenericType<List<CachedPluginBundle>>()
        {
        });
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CachedPluginBundle
    {
        @JsonProperty
        public String bundleName;
        @JsonProperty
        public Locale locale;
        @JsonProperty
        public String classLoaderName;

        public CachedPluginBundle(final String bundleName, final Locale locale, final String classLoaderName)
        {
            this.bundleName = bundleName;
            this.locale = locale;
            this.classLoaderName = classLoaderName;
        }

        public CachedPluginBundle()
        {
        }

        @Override
        public String toString()
        {
            return Objects.toStringHelper(this)
                    .add("bundleName", bundleName)
                    .add("locale", locale)
                    .add("classLoaderName", classLoaderName)
                    .toString();
        }
    }
}
