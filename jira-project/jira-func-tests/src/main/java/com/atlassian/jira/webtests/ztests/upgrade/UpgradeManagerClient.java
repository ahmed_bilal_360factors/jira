package com.atlassian.jira.webtests.ztests.upgrade;

import javax.xml.ws.Response;

import com.atlassian.jira.rest.v2.upgrade.UpgradeResultBean;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

/**
 * @since 6.4
 */
public class UpgradeManagerClient extends RestApiClient
{
    private final JIRAEnvironmentData environmentData;

    public UpgradeManagerClient(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
        this.environmentData = environmentData;
    }

    public void addUpgradeTask(String task)
    {
        getUpgradeBackDoor().path("addUpgrade").queryParam("task", task).post();
    }

    public void doUpgrade()
    {
        getUpgradeBackDoor().path("doUpgrade").post();
    }

    public void doDelayedUpgradesAndWaitForCompletion()
    {
        getUpgradeResource().post();

        boolean upgradeComplete = false;
        do
        {
            try
            {
                UpgradeResultBean result = getUpgradeResource().get(UpgradeResultBean.class);
                upgradeComplete = true;
            }
            catch (UniformInterfaceException e)
            {
                upgradeComplete = e.getResponse().getStatus() != 303;
                try
                {
                    Thread.sleep(100);
                }
                catch (InterruptedException e1)
                {
                    throw new RuntimeException(e1);
                }
            }
        } while (!upgradeComplete);
    }

    private WebResource getUpgradeResource()
    {
        return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("rest").path("api").path("2").path("upgrade");
    }

    private WebResource getUpgradeBackDoor()
    {
        return resourceRoot(environmentData.getBaseUrl().toExternalForm()).path("rest").path("func-test").path("1.0").path("upgradeManager");
    }
}
