package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.fugue.Iterables;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.jira.testkit.client.restclient.IssueType;
import com.atlassian.jira.testkit.client.restclient.IssueTypeClient;
import com.atlassian.jira.testkit.client.restclient.IssueTypeCreateBean;
import com.atlassian.jira.testkit.client.restclient.IssueTypeUpdateBean;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;

import java.util.List;
import java.util.concurrent.Callable;

import static com.atlassian.jira.webtests.ztests.bundledplugins2.rest.util.PropertyAssertions.assertUniformInterfaceException;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

/**
 * Func tests for IssueTypeResource.
 *
 * @since v4.2
 */
@WebTest ({ Category.FUNC_TEST, Category.REST })
public class TestIssueTypeResource extends RestFuncTest
{
    private static final String BUG_ISSUE_TYPE_ID = "1";
    private static final String TASK_ISSUE_TYPE_ID = "3";
    private IssueTypeClient issueTypeClient;
    private IssueClient issueClient;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        backdoor.restoreBlankInstance();
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_ALLOWSUBTASKS, true);

        this.issueTypeClient = new IssueTypeClient(getEnvironmentData());
        this.issueClient = new IssueClient(getEnvironmentData());
    }

    public void testIssueTypeVisible() throws Exception
    {
        backdoor.restoreDataFromResource("TestIssueTypeResource.xml");

        IssueType issueType = issueTypeClient.get(TASK_ISSUE_TYPE_ID);

        assertEquals(getBaseUrl() + "/rest/api/2/issuetype/" + TASK_ISSUE_TYPE_ID, issueType.self);
        assertEquals("A task that needs to be done.", issueType.description);
        assertEquals(getBaseUrl() + "/images/icons/issuetypes/task.png", issueType.iconUrl);
        assertEquals("Task", issueType.name);
        assertEquals(false, issueType.subtask);
        assertEquals(TASK_ISSUE_TYPE_ID, issueType.id);
    }

    public void testIssueTypeNotVisible() throws Exception
    {
        backdoor.restoreDataFromResource("TestIssueTypeResource.xml");

        Response response = issueTypeClient.loginAs(FRED_USERNAME).getResponse(TASK_ISSUE_TYPE_ID);

        assertEquals(NOT_FOUND.getStatusCode(), response.statusCode);
    }

    public void testIssueTypeDoesNotExist()
    {
        Response responseZzz = issueTypeClient.loginAs(FRED_USERNAME).getResponse("zzz");

        assertEquals(NOT_FOUND.getStatusCode(), responseZzz.statusCode);
    }

    public void testIssueTypeResourceSupportsAbsoluteUrlInIconUrl() throws Exception
    {
        backdoor.restoreDataFromResource("TestIssueTypeResource.xml");

        IssueType response = issueTypeClient.loginAs(FRED_USERNAME).get(BUG_ISSUE_TYPE_ID);
        
        assertThat(response.iconUrl, equalTo("https://jira.atlassian.com/images/icons/bug.gif"));
    }

    public void testGetAllIssueTypes() throws Exception
    {
        List<IssueType> issueTypes = issueTypeClient.get();

        assertEquals(4, issueTypes.size());
        assertIssueTypesContain(issueTypes, "1");
        assertIssueTypesContain(issueTypes, "2");
        assertIssueTypesContain(issueTypes, "3");
        assertIssueTypesContain(issueTypes, "4");
    }

    public void testGetIssueTypesForUserWithRestrictedPermissionsToProjects()
    {
        backdoor.restoreDataFromResource("TestIssueTypeResource.xml");

        List<IssueType> issueTypes = issueTypeClient.loginAs(FRED_USERNAME).get();

        assertEquals(2, issueTypes.size());
        assertIssueTypesContain(issueTypes, "1");
        assertIssueTypesContain(issueTypes, "2");
    }

    public void testCreateIssueTypeSuccessful()
    {
        final IssueTypeCreateBean issueTypeCreateBean = getIssueTypeCreateBean();
        final IssueType issueType = issueTypeClient.post(issueTypeCreateBean);

        assertThat(issueType, eq(issueTypeCreateBean));

        assertThat(issueTypeClient.get(), Matchers.<IssueType>hasItem(eq(issueTypeCreateBean)));
    }

    public void testCreateIssueTypeWithAnonymousUser()
    {
        assertUniformInterfaceException(new Callable<Void>()
        {

            @Override
            public Void call() throws Exception
            {
                final IssueTypeCreateBean issueTypeCreateBean = getIssueTypeCreateBean();
                issueTypeClient.anonymous().post(issueTypeCreateBean);
                return null;
            }
        }, UNAUTHORIZED);
    }

    public void testCreateIssueTypeWithUserWithoutAdminPermissions()
    {
        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                final IssueTypeCreateBean issueTypeCreateBean = getIssueTypeCreateBean();
                issueTypeClient.loginAs(FRED_USERNAME).post(issueTypeCreateBean);
                return null;
            }
        }, FORBIDDEN);
    }

    public void testCreateIssueTypeWithExistingUserName()
    {
        final Option<IssueType> typeOption = Iterables.first(issueTypeClient.get());

        assertTrue(typeOption.isDefined());

        final IssueType issueType = typeOption.get();

        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                issueTypeClient.post(new IssueTypeCreateBean(issueType.name, "description", IssueTypeCreateBean.Type.standard));
                return null;
            }
        }, CONFLICT);
    }

    public void testCreateSubTaskSuccessful()
    {
        final IssueTypeCreateBean subTask = new IssueTypeCreateBean("name", "description", IssueTypeCreateBean.Type.subtask);
        final IssueType issueType = issueTypeClient.post(subTask);

        assertThat(issueType, eq(subTask));

        assertThat(issueTypeClient.get(), Matchers.<IssueType>hasItem(eq(subTask)));
    }

    public void testCreateSubTaskWhenSubTasksDisabled()
    {
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_ALLOWSUBTASKS, false);

        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                issueTypeClient.post(new IssueTypeCreateBean("name", "description", IssueTypeCreateBean.Type.subtask));
                return null;
            }
        }, BAD_REQUEST);
    }

    public void testCreateIssueTypeWithTooLongName()
    {
        final StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 100; i++)
        {
            stringBuilder.append("n");
        }
        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                issueTypeClient.post(new IssueTypeCreateBean(stringBuilder.toString(), "description", IssueTypeCreateBean.Type.standard));
                return null;
            }
        }, BAD_REQUEST);
    }

    public void testCreateIssueTypeWithoutDescription()
    {
        final IssueTypeCreateBean issueTypeCreateBean = new IssueTypeCreateBean("name", null, IssueTypeCreateBean.Type.standard);

        final IssueType issueType = issueTypeClient.post(issueTypeCreateBean);

        assertThat(issueType, eq(issueTypeCreateBean));

        assertThat(issueTypeClient.get(), Matchers.<IssueType>hasItem(eq(issueTypeCreateBean)));
    }

    public void testIssueTypeIsStandardByDefault()
    {
        final IssueTypeCreateBean issueTypeCreateBean = new IssueTypeCreateBean("name", "description", null);

        final IssueType issueType = issueTypeClient.post(issueTypeCreateBean);

        assertFalse(issueType.subtask);
    }

    public void testIssueTypeHasDefaultAvatarAfterCreation()
    {
        final IssueType issueType = issueTypeClient.post(getIssueTypeCreateBean());
        final String defaultIssueTypeAvatarId = backdoor.applicationProperties().getString(APKeys.JIRA_DEFAULT_ISSUETYPE_AVATAR_ID);

        assertThat(issueType.iconUrl, containsString(defaultIssueTypeAvatarId));
    }

    public void testSubTaskHasDefaultAvatarAfterCreation()
    {
        final IssueType issueType = issueTypeClient.post(new IssueTypeCreateBean("name", "description", IssueTypeCreateBean.Type.subtask));
        final String subTaskAvatarId = backdoor.applicationProperties().getString(APKeys.JIRA_DEFAULT_ISSUETYPE_SUBTASK_AVATAR_ID);

        assertThat(issueType.iconUrl, containsString(subTaskAvatarId));
    }

    public void testDeleteIssueTypeWithoutProvidingAlternative()
    {
        final IssueType issueType = issueTypeClient.get("1");
        assertEquals("Bug", issueType.name);

        final SearchResult searchResult = backdoor.search().getSearch(new SearchRequest().jql("issuetype = Bug"));
        assertThat("There are issues with issuetype = Bug which requires passing alternative id",
                searchResult.issues, Matchers.<Issue>emptyIterable());

        issueTypeClient.delete("1", Option.none(String.class));

        assertThat(issueTypeClient.get(), not(Matchers.<IssueType>hasItem(issueTypeNameEq("Bug"))));
    }

    public void testDeleteIssueTypeWithAlternative()
    {
        final IssueType issueType = issueTypeClient.get("1");
        assertEquals("Bug", issueType.name);

        final IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "new bug");
        assertThat(issueClient.get(issue.key).fields.issuetype, is(issueTypeNameEq("Bug")));

        final List<IssueType> alternatives = issueTypeClient.getAlternatives("1");
        assertThat(alternatives, not(Matchers.<IssueType>emptyIterable()));

        final IssueType alternative = Iterables.first(alternatives).get();
        issueTypeClient.delete("1", Option.some(alternative.id));

        assertThat(issueTypeClient.get(), not(Matchers.<IssueType>hasItem(issueTypeNameEq("Bug"))));
        assertThat(issueClient.get(issue.key).fields.issuetype, is(issueTypeNameEq(alternative.name)));
    }

    public void testDeleteIssueWithInvalidAlternative()
    {
        final IssueType issueType = issueTypeClient.get("1");
        assertEquals("Bug", issueType.name);

        final IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "new bug");
        assertThat(issueClient.get(issue.key).fields.issuetype, is(issueTypeNameEq("Bug")));

        final IssueType subtask = issueTypeClient.post(new IssueTypeCreateBean("subtask", "description", IssueTypeCreateBean.Type.subtask));

        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                // subtask is not a valid alternative for issue type
                issueTypeClient.delete(issueType.id, Option.some(subtask.id));
                return null;
            }
        }, CONFLICT);
    }

    public void testDeleteIssueWithSelfAsAlternative()
    {
        final IssueType issueType = issueTypeClient.get("1");
        assertEquals("Bug", issueType.name);

        final IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "new bug");
        assertThat(issueClient.get(issue.key).fields.issuetype, is(issueTypeNameEq("Bug")));

        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                issueTypeClient.delete(issueType.id, Option.some(issueType.id));
                return null;
            }
        }, CONFLICT);
    }

    public void testDeleteIssueTypeWithInvalidUser()
    {
        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                issueTypeClient.loginAs(FRED_USERNAME).delete("1", Option.none(String.class));
                return null;
            }
        }, FORBIDDEN);
    }

    public void testDeleteIssueTypeWithAnonymousUser()
    {
        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                issueTypeClient.anonymous().delete("1", Option.none(String.class));
                return null;
            }
        }, UNAUTHORIZED);
    }

    public void testDeleteIssueTypeWithAssociatedIssuesWithoutAlternative()
    {
        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                final IssueCreateResponse issue = backdoor.issues().createIssue("HSP", "some bug");
                assertThat(issueClient.get(issue.key).fields.issuetype, is(issueTypeNameEq("Bug")));

                final List<IssueType> alternatives = issueTypeClient.getAlternatives("1");

                // remove all alternatives
                for (final IssueType alternative : alternatives)
                {
                    issueTypeClient.delete(alternative.id, Option.<String>none());
                }

                issueTypeClient.delete("1", Option.<String>none());
                return null;
            }
        }, NOT_FOUND);
    }

    public void testDeleteNotExistingIssueType()
    {
        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                issueTypeClient.delete("150", Option.<String>none());
                return null;
            }
        }, NOT_FOUND);
    }

    public void testUpdatingIssueTypeName()
    {
        final IssueTypeUpdateBean bug2 = new IssueTypeUpdateBean("Bug2", "new bug description", null);
        final IssueType updatedIssueType = issueTypeClient.update("1", bug2);

        assertThat(updatedIssueType, eq(bug2));
        assertThat(issueTypeClient.get(), Matchers.<IssueType>hasItem(eq(bug2)));
    }

    public void testUpdateIssueTypeDescriptionWithKeepingOldName()
    {
        final IssueTypeUpdateBean bug = new IssueTypeUpdateBean("Bug", "new bug description", null);
        final IssueType issueType = issueTypeClient.update("1", bug);

        assertThat(issueType, eq(bug));
        assertThat(issueTypeClient.get(), Matchers.<IssueType>hasItem(eq(bug)));
    }

    public void testUpdateAvatarId()
    {
        final IssueType newIssueType = issueTypeClient.post(new IssueTypeCreateBean("new issue type", "description", IssueTypeCreateBean.Type.standard));
        final IssueType update = issueTypeClient.update("1", new IssueTypeUpdateBean(null, null, newIssueType.avatarId));

        assertEquals(newIssueType.avatarId, update.avatarId);
        assertEquals(update.name, "Bug");
    }

    public void testCantChangeNameToTheExistingOne()
    {
        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                issueTypeClient.update("2", new IssueTypeUpdateBean("Bug", "bug", null));
                return null;
            }
        }, CONFLICT);
    }

    public void testCantUpdateWithAnonymousUser()
    {
        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                issueTypeClient.anonymous().update("1", new IssueTypeUpdateBean("name", "name", null));
                return null;
            }
        }, UNAUTHORIZED);
    }

    public void testCantUpdateWithUserWithoutAdminRight()
    {
        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                issueTypeClient.loginAs(FRED_USERNAME).update("1", new IssueTypeUpdateBean("name", "name", null));
                return null;
            }
        }, FORBIDDEN);
    }

    public void testCantSetAvatarToNonExistingOne()
    {
        assertUniformInterfaceException(new Callable<Void>()
        {
            @Override
            public Void call() throws Exception
            {
                issueTypeClient.update("1", new IssueTypeUpdateBean(null, null, 100l));
                return null;
            }
        }, BAD_REQUEST);
    }

    private IssueTypeCreateBean getIssueTypeCreateBean() {return new IssueTypeCreateBean("name", "description", IssueTypeCreateBean.Type.standard);}

    private Matcher<IssueType> issueTypeNameEq(final String name)
    {
        return new TypeSafeMatcher<IssueType>()
        {
            @Override
            protected boolean matchesSafely(final IssueType issueType)
            {
                return name.equals(issueType.name);
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendValue("Issue type with name").appendValue("name").appendText("does not match");
            }
        };
    }

    private Matcher<IssueType> eq(final IssueTypeUpdateBean issueTypeUpdateBean)
    {
        return new TypeSafeMatcher<IssueType>()
        {
            @Override
            protected boolean matchesSafely(final IssueType issueType)
            {
                return issueType.name.equals(issueTypeUpdateBean.getName())
                        && issueType.description.equals(issueTypeUpdateBean.getDescription());
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("Issue type does not match").appendValue(issueTypeUpdateBean);
            }
        };
    }

    private Matcher<IssueType> eq(final IssueTypeCreateBean issueTypeCreateBean)
    {
        return new TypeSafeMatcher<IssueType>()
        {
            @Override
            protected boolean matchesSafely(final IssueType issueType)
            {
                return issueType.name.equals(issueTypeCreateBean.getName())
                        && issueType.description.equals(issueTypeCreateBean.getDescription())
                        && issueType.subtask ? issueTypeCreateBean.getType().equals(IssueTypeCreateBean.Type.subtask) : issueTypeCreateBean.getType().equals(IssueTypeCreateBean.Type.standard);
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("Issue type does not match").appendValue(issueTypeCreateBean);
            }
        };
    }

    private void assertIssueTypesContain(final List<IssueType> issueTypes, final String id)
    {
        assertThat(issueTypes, Matchers.<IssueType>hasItem(new TypeSafeMatcher<IssueType>()
        {
            @Override
            protected boolean matchesSafely(final IssueType issueType)
            {
                return issueType.id.equals(id);
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("THe list does not contain property with id").appendValue(id);
            }
        }));
    }
}
