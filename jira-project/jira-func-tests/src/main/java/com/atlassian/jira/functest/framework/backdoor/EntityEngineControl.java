package com.atlassian.jira.functest.framework.backdoor;

import java.util.List;
import java.util.Map;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;

import com.sun.jersey.api.client.WebResource;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * Put here anything you need in Backdoor/TestKit. Later it will be promoted to the official package.
 * @since v5.2
 */
public class EntityEngineControl extends BackdoorControl<EntityEngineControl>
{
    public EntityEngineControl(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    public List<Map<String, String>> findByAnd(final String entityName, final Map restriction)
    {
        WebResource.Builder resource = createResource().path("entityEngine").path("findByAnd").queryParam("entity", entityName).type(APPLICATION_JSON_TYPE).accept(APPLICATION_JSON_TYPE);
        return resource.post(List.class, restriction);
    }

    public List<Map<String, String>> findByValueList(final String entityName, String field, List<?> values, List<String> returnFields, ValueType type)
    {
        WebResource resource = createResource().path("entityEngine").path("findByValueList")
                                                .queryParam("entity", entityName)
                                                .queryParam("field", field)
                                                .queryParam("valueType", type.name());

        for (String returnField : returnFields)
        {
            resource = resource.queryParam("returnField", returnField);
        }

        WebResource.Builder res = resource.type(APPLICATION_JSON_TYPE).accept(APPLICATION_JSON_TYPE);

        return res.post(List.class, values);
    }

    public List<Map<String, String>> findByValueList2Fields(final String entityName, String field, String field2, List<?> values, List<String> returnFields, ValueType type)
    {
        WebResource resource = createResource().path("entityEngine").path("findByValueList2Fields")
                .queryParam("entity", entityName)
                .queryParam("field", field)
                .queryParam("field2", field2)
                .queryParam("valueType", type.name());

        for (String returnField : returnFields)
        {
            resource = resource.queryParam("returnField", returnField);
        }

        WebResource.Builder res = resource.type(APPLICATION_JSON_TYPE).accept(APPLICATION_JSON_TYPE);

        return res.post(List.class, values);
    }

    public static enum ValueType
    {
        STRING, NUMBER;
    }
}
