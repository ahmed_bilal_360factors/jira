package com.atlassian.jira.functest.framework.admin;

import com.atlassian.jira.functest.framework.AbstractFuncTestUtil;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.util.form.FormParameterUtil;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import net.sourceforge.jwebunit.WebTester;

import static com.atlassian.jira.webtests.LegacyProjectPermissionKeyMapping.getKey;

/**
 * Implements the {@link com.atlassian.jira.functest.framework.admin.PermissionSchemes} and
 * {@link com.atlassian.jira.functest.framework.admin.PermissionSchemes.PermissionScheme} interfaces.
 *
 * @since v4.0
 */
public class PermissionSchemesImpl extends AbstractFuncTestUtil implements PermissionSchemes,
        PermissionSchemes.PermissionScheme
{
    public PermissionSchemesImpl(WebTester tester, JIRAEnvironmentData environmentData)
    {
        super(tester, environmentData, 2);
    }

    public PermissionScheme defaultScheme()
    {
        getNavigation().gotoAdminSection(Navigation.AdminSection.PERMISSION_SCHEMES);
        tester.clickLinkWithText("Default Permission Scheme");
        return this;
    }

    public PermissionScheme scheme(String schemeName)
    {
        getNavigation().gotoAdminSection(Navigation.AdminSection.PERMISSION_SCHEMES);
        tester.clickLinkWithText(schemeName);
        return this;
    }

    @Override
    public void grantPermissionToGroup(int permission, String groupName)
    {
        grantPermissionToGroup(getKey(permission), groupName);
    }

    @Override
    public void grantPermissionToGroup(String permission, String groupName)
    {
        grantPermissionToGroup(new ProjectPermissionKey(permission), groupName);
    }

    @Override
    public void grantPermissionToGroup(ProjectPermissionKey permission, String groupName)
    {
        tester.clickLink("add_perm_" + permission.permissionKey());
        tester.checkCheckbox("type", "group");
        final FormParameterUtil formParameterUtil = new FormParameterUtil(tester, "jiraform", " Add ");
        formParameterUtil.addOptionToHtmlSelect("group", new String[] { groupName });
        formParameterUtil.setFormElement("group", groupName);
        formParameterUtil.submitForm();
    }

    @Override
    public void grantPermissionToReporter(int permission)
    {
        grantPermissionToReporter(getKey(permission));
    }

    @Override
    public void grantPermissionToReporter(String permission)
    {
        grantPermissionToReporter(new ProjectPermissionKey(permission));
    }

    @Override
    public void grantPermissionToReporter(ProjectPermissionKey permission)
    {
        tester.clickLink("add_perm_" + permission.permissionKey());
        tester.checkCheckbox("type", "reporter");
        tester.submit(" Add ");
    }

    @Override
    public void grantPermissionToProjectLead(int permission)
    {
        grantPermissionToProjectLead(getKey(permission));
    }

    @Override
    public void grantPermissionToProjectLead(String permission)
    {
        grantPermissionToProjectLead(new ProjectPermissionKey(permission));
    }

    @Override
    public void grantPermissionToProjectLead(ProjectPermissionKey permission)
    {
        tester.clickLink("add_perm_" + permission.permissionKey());
        tester.checkCheckbox("type", "lead");
        tester.submit(" Add ");
    }

    @Override
    public void grantPermissionToCurrentAssignee(int permission)
    {
        grantPermissionToCurrentAssignee(getKey(permission));
    }

    @Override
    public void grantPermissionToCurrentAssignee(String permission)
    {
        grantPermissionToCurrentAssignee(new ProjectPermissionKey(permission));
    }

    @Override
    public void grantPermissionToCurrentAssignee(ProjectPermissionKey permission)
    {
        tester.clickLink("add_perm_" + permission.permissionKey());
        tester.checkCheckbox("type", "assignee");
        tester.submit(" Add ");
    }

    @Override
    public void grantPermissionToUserCustomFieldValue(int permission, String customFieldId)
    {
        grantPermissionToUserCustomFieldValue(getKey(permission), customFieldId);
    }

    @Override
    public void grantPermissionToUserCustomFieldValue(String permission, String customFieldId)
    {
        grantPermissionToUserCustomFieldValue(new ProjectPermissionKey(permission), customFieldId);
    }

    @Override
    public void grantPermissionToUserCustomFieldValue(ProjectPermissionKey permission, String customFieldId)
    {
        tester.clickLink("add_perm_" + permission.permissionKey());
        tester.checkCheckbox("type", "userCF");
        tester.setFormElement("userCF", customFieldId);
        tester.submit(" Add ");
    }

    @Override
    public void grantPermissionToGroupCustomFieldValue(int permission, String customFieldId)
    {
        grantPermissionToGroupCustomFieldValue(getKey(permission), customFieldId);
    }

    @Override
    public void grantPermissionToGroupCustomFieldValue(String permission, String customFieldId)
    {
        grantPermissionToGroupCustomFieldValue(new ProjectPermissionKey(permission), customFieldId);
    }

    @Override
    public void grantPermissionToGroupCustomFieldValue(ProjectPermissionKey permission, String customFieldId)
    {
        tester.clickLink("add_perm_" + permission.permissionKey());
        tester.checkCheckbox("type", "groupCF");
        tester.setFormElement("groupCF", customFieldId);
        tester.submit(" Add ");
    }

    @Override
    public void grantPermissionToProjectRole(int permission, String projectRoleId)
    {
        grantPermissionToProjectRole(getKey(permission), projectRoleId);
    }

    @Override
    public void grantPermissionToProjectRole(String permission, String projectRoleId)
    {
        grantPermissionToProjectRole(new ProjectPermissionKey(permission), projectRoleId);
    }

    @Override
    public void grantPermissionToProjectRole(ProjectPermissionKey permission, String projectRoleId)
    {
        tester.clickLink("add_perm_" + permission.permissionKey());
        tester.checkCheckbox("type", "projectrole");
        tester.setFormElement("projectrole", projectRoleId);
        tester.submit(" Add ");
    }

    @Override
    public void grantPermissionToSingleUser(int permission, String username)
    {
        grantPermissionToSingleUser(getKey(permission), username);
    }

    @Override
    public void grantPermissionToSingleUser(final String permission, final String username)
    {
        grantPermissionToSingleUser(new ProjectPermissionKey(permission), username);
    }

    @Override
    public void grantPermissionToSingleUser(ProjectPermissionKey permission, String username)
    {
        tester.clickLink("add_perm_" + permission.permissionKey());
        tester.checkCheckbox("type", "user");
        tester.setFormElement("user", username);
        tester.submit(" Add ");
    }

    @Override
    public void removePermission(int permissionType, String permissionParam)
    {
        removePermission(getKey(permissionType), permissionParam);
    }

    @Override
    public void removePermission(final String permission, final String groupName)
    {
        if (tester.getDialog().isLinkPresent("del_perm_" + permission + "_" + groupName))
        {
            tester.clickLink("del_perm_" + permission + "_" + groupName);
            tester.submit("Delete");
        }
    }

    @Override
    public void removePermission(ProjectPermissionKey permissionType, String permissionParam)
    {
        removePermission(permissionType.permissionKey(), permissionParam);
    }

    @Override
    public void removePermission(GlobalPermissionKey permissionType, String permissionParam)
    {
        removePermission(permissionType.getKey(), permissionParam);
    }

    protected Navigation getNavigation()
    {
        return getFuncTestHelperFactory().getNavigation();
    }
}
