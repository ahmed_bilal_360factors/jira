package com.atlassian.jira.webtests.ztests.bundledplugins2.rest;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.CurrentUserPreferencesClient;

import com.sun.jersey.api.client.ClientResponse;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

@WebTest ({ Category.FUNC_TEST, Category.REST })
public class TestCurrentUserPreferencesResource extends RestFuncTest
{

    private static final String SOME_KEY = "someKey";
    private static final String SOME_VALUE = "someValue";

    private CurrentUserPreferencesClient preferencesClient;


    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        preferencesClient = new CurrentUserPreferencesClient(getEnvironmentData());
        backdoor.restoreBlankInstance();
    }

    @Override
    protected void tearDownTest()
    {
        preferencesClient.close();
    }


    public void testHappyPathUseCase()
    {
        //reading preference - shouldn't be there
        ClientResponse responseGet1 = preferencesClient.getPreference(SOME_KEY);
        assertThat(responseGet1.getStatus(), equalTo(ClientResponse.Status.NOT_FOUND.getStatusCode()));

        //writing preference
        ClientResponse responsePut = preferencesClient.setPreference(SOME_KEY, SOME_VALUE);
        assertNotNull(responsePut);
        assertThat(responsePut.getStatus(), equalTo(ClientResponse.Status.NO_CONTENT.getStatusCode()));

        //reading preference
        ClientResponse responseGet2 = preferencesClient.getPreference(SOME_KEY);
        assertThat(responseGet2.getStatus(), equalTo(ClientResponse.Status.OK.getStatusCode()));
        String message = responseGet2.getEntity(String.class);
        assertThat(message, equalTo(SOME_VALUE));

        //removing preference
        ClientResponse responseDelete1 = preferencesClient.removePreference(SOME_KEY);
        assertThat(responseDelete1.getStatus(), equalTo(ClientResponse.Status.NO_CONTENT.getStatusCode()));

        //reading preference
        ClientResponse responseGet3 = preferencesClient.getPreference(SOME_KEY);
        assertThat(responseGet3.getStatus(), equalTo(ClientResponse.Status.NOT_FOUND.getStatusCode()));
    }
}
