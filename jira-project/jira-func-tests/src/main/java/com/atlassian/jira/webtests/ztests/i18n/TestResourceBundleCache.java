package com.atlassian.jira.webtests.ztests.i18n;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.backdoor.ResourceBundleCacheControl;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;

import org.hamcrest.Matchers;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @since v6.4
 */
@WebTest({Category.FUNC_TEST, Category.I18N})
public class TestResourceBundleCache extends FuncTestCase
{

    protected void setUpTest()
    {
        super.setUpTest();
    }

    public void testResourceBundleCacheIsClearedFromPluginBundles()
    {
        administration.generalConfiguration().setJiraLocale("Deutsch (Deutschland)");
        navigation.dashboard();

        final ResourceBundleCacheControl resourceBundleCacheControl = backdoor.resourceBundleCacheControl();
        final Iterable<ResourceBundleCacheControl.CachedPluginBundle> cachedPluginResourceBundles =
                resourceBundleCacheControl.getCachedPluginResourceBundles();

        assertThat("ResourceBundle internal cache can not contain any plugin bundles to avoid duplication",
                cachedPluginResourceBundles, Matchers.<ResourceBundleCacheControl.CachedPluginBundle>emptyIterable());
    }

}
