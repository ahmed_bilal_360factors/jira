package com.atlassian.jira.webtests.zsuites;

import com.atlassian.jira.functest.framework.FuncTestSuite;
import com.atlassian.jira.webtests.ztests.bundledplugins2.streams.TestActivityStreams;
import junit.framework.Test;

/**
 * Streams related tests.
 *
 * @since v6.4
 */
public class FuncTestSuiteStreams extends FuncTestSuite
{
    /**
     * The suite instance.
     */
    public static final FuncTestSuiteStreams SUITE = new FuncTestSuiteStreams();

    /**
     * The pattern in JUnit/IDEA JUnit runner is that if a class has a static suite() method that returns a Test, then
     * this is the entry point for running your tests.  So make sure you declare one of these in the FuncTestSuite
     * implementation.
     *
     * @return a Test that can be run by as JUnit TestRunner
     */
    public static Test suite()
    {
        return SUITE.createTest();
    }

    public FuncTestSuiteStreams()
    {
        addTestsInPackage(TestActivityStreams.class.getPackage().getName(), true);
    }
}
