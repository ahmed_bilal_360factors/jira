package com.atlassian.jira.webtests.ztests.misc;

import com.atlassian.jira.functest.rules.RestRule;
import com.atlassian.jira.testkit.client.log.FuncTestOut;
import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.setup.JiraSetupInstanceHelper;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.meterware.httpunit.WebResponse;
import junit.framework.Assert;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.collect.Iterables.transform;
import static org.apache.commons.lang3.StringUtils.join;

/**
 * @since v4.3
 */
@WebTest ( { Category.FUNC_TEST, Category.SETUP })
public class TestDatabaseSetup extends FuncTestCase
{

    private RestRule restRule;

    @Override
    protected void setUpTest() {
        super.setUpTest();
        restRule = new RestRule(this);
        restRule.before();
    }

    @Override
    protected void tearDownTest()
    {
        restRule.after();
        super.tearDownTest();
    }

    /**
     * Because we cannot guarantee the order of test executions and at the end we need to have the db configure
     */
    public void testAll() throws IOException, SAXException, JSONException
    {
        if (getEnvironmentData().getProperty("databaseType") != null)
        {
            tester.getDialog().getWebClient().getClientProperties().setHiddenFieldsEditable(true);
            // Direct JDBC tests
            _testDirectJdbcSuccessful();
            _testDirectJdbcMissingHostname();
            _testDirectJdbcMissingPortNumber();
            _testDirectJdbcInvalidPort();
            _testDirectJdbcIncorrectPort();
            _testDirectJdbcMissingUsername();
            _testDirectJdbcInvalidCredential();

            // Set up database properly
            configureDirectJdbc();
        }
        else
        {
            FuncTestOut.log("Skipping TestDatabaseSetup: Internal DB configured.");
        }
    }

    protected boolean shouldSkipSetup()
    {
        return true;
    }

    private void configureDirectJdbc()
    {
        navigation.gotoPage("/secure/SetupDatabase!default.jspa");
        JiraSetupInstanceHelper.setupDirectJDBCConnection(tester, getEnvironmentData());
        text.assertTextPresent(locator.page(), "Set Up Application Properties");
    }

    private void _testDirectJdbcSuccessful() throws IOException, SAXException, JSONException
    {
        final Map<String, String> query = fillValidDirectJdbcValues();
        assertTestConnectionSuccessful(query);
    }

    private void _testDirectJdbcMissingHostname() throws IOException, SAXException
    {
        final Map<String, String> query = fillValidDirectJdbcValues();
        // Make username field empty (most likely missing field since it is not auto-populated)
        query.put("jdbcHostname", "");
        text.assertTextPresent(testConnection(query), "Hostname required");
    }

    private void _testDirectJdbcMissingPortNumber() throws IOException, SAXException
    {
        final Map<String, String> query = fillValidDirectJdbcValues();
        // Make username field empty (most likely missing field since it is not auto-populated)
        query.put("jdbcPort", "");
        text.assertTextPresent(testConnection(query), "Port required");
    }

    private void _testDirectJdbcInvalidPort() throws IOException, SAXException, JSONException
    {
        final Map<String, String> query = fillValidDirectJdbcValues();
        // Make URL invalid
        query.put("jdbcPort", "not-a-number");
        assertTestConnectionFailed(query);
    }

    private void _testDirectJdbcIncorrectPort() throws IOException, SAXException, JSONException
    {
        final Map<String, String> query = fillValidDirectJdbcValues();
        // Make URL invalid
        query.put("jdbcPort", "999");
        assertTestConnectionFailed(query);
    }

    private void _testDirectJdbcInvalidCredential() throws IOException, SAXException, JSONException
    {
        final Map<String, String> query = fillValidDirectJdbcValues();
        // Modify password to invalidate credentials
        query.put("jdbcPassword", getEnvironmentData().getProperty("password") + "extra-text-to-invalidate-password");
        assertTestConnectionFailed(query);
    }

    private void _testDirectJdbcMissingUsername() throws IOException, SAXException
    {
        final Map<String, String> query = fillValidDirectJdbcValues();
        // Make username field empty (most likely missing field since it is not auto-populated)
        query.put("jdbcUsername", "");
        text.assertTextPresent(testConnection(query), "Username required");
    }

    private Map<String, String> fillValidDirectJdbcValues() throws IOException, SAXException
    {
        final Map<String, String> query = new HashMap<String, String>();
        query.put("databaseOption", "external");
        query.put("databaseType", getEnvironmentData().getProperty("databaseType"));
        query.put("jdbcHostname", environmentData.getProperty("db.host"));
        query.put("jdbcPort", environmentData.getProperty("db.port"));
        // SID is only used for Oracle
        query.put("jdbcSid", environmentData.getProperty("db.instance"));
        // Database is used for all DBs except Oracle
        query.put("jdbcDatabase", environmentData.getProperty("db.instance"));

        query.put("jdbcUsername", getEnvironmentData().getProperty("username"));
        query.put("jdbcPassword", getEnvironmentData().getProperty("password"));
        query.put("schemaName", getEnvironmentData().getProperty("schema-name"));
        return query;
    }

    private void assertTestConnectionSuccessful(final Map<String, String> query)
            throws IOException, SAXException, JSONException
    {
        Assert.assertTrue(new JSONObject(testConnection(query)).getJSONObject("data").getBoolean("dbTestSuccessful"));
    }

    private void assertTestConnectionFailed(final Map<String, String> query)
            throws IOException, SAXException, JSONException
    {
        Assert.assertFalse(new JSONObject(testConnection(query)).getJSONObject("data").getBoolean("dbTestSuccessful"));
    }

    private String testConnection(final Map<String, String> query) throws IOException, SAXException
    {
        final WebResponse response = restRule.GET("/secure/SetupDatabase!connectionCheck.jspa?" + toQueryString(query));
        return response.getText();
    }

    private String toQueryString(final Map<String, String> query)
    {
        return join(transform(query.entrySet(), new Function<Map.Entry<String, String>, String>()
        {
            @Override
            public String apply(final Map.Entry<String, String> e)
            {
                return e.getKey() + "=" + e.getValue();
            }
        }), '&');
    }
}
