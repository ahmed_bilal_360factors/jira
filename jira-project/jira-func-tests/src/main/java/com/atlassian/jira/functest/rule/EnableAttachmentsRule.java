package com.atlassian.jira.functest.rule;

import com.atlassian.jira.testkit.client.AttachmentsControl;

import org.junit.rules.ExternalResource;

/**
 * Enables attachments feature before a test.
 *
 * @since v6.4
 */
class EnableAttachmentsRule extends ExternalResource
{
    private final AttachmentsControl attachmentsControl;

    EnableAttachmentsRule(final AttachmentsControl attachmentsControl)
    {
        this.attachmentsControl = attachmentsControl;
    }

    @Override
    protected void before() throws Throwable
    {
        super.before();
        attachmentsControl.enable();
    }
}
