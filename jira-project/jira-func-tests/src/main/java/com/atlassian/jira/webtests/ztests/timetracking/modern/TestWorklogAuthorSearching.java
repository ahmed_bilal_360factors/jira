package com.atlassian.jira.webtests.ztests.timetracking.modern;

import com.atlassian.fugue.Effect;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import org.hamcrest.MatcherAssert;

import static org.junit.Assert.assertThat;

/**
 * Func test for searching worklogs by author.
 *
 * @since v6.4
 */
@WebTest ({ Category.FUNC_TEST, Category.REST, Category.WORKLOGS })
public final class TestWorklogAuthorSearching extends AbstractWorklogSearchingTest
{
    @Override
    public void setUpTest()
    {
        super.setUpTest();
        backdoor.usersAndGroups().addUserToGroup("fred", "jira-developers");
    }

    public void testEqualsOperator() throws Exception
    {
        final String issueKey1 = createDefaultIssue();
        final String issueKey2 = createDefaultIssue();
        createWorklogAsUser(issueKey1, "admin");
        createWorklogAsUser(issueKey2, "fred");
        SearchResult searchResult = executeJqlSearch("worklogAuthor = fred");
        assertThat(searchResult, sizeEquals(1));
        assertThat(searchResult, containsIssues(issueKey2));
    }

    public void testNotEqualsOperator() throws Exception
    {
        final String issueKey1 = createDefaultIssue();
        final String issueKey2 = createDefaultIssue();
        createWorklogAsUser(issueKey1, "admin");
        createWorklogAsUser(issueKey2, "fred");
        SearchResult searchResult = executeJqlSearch("worklogAuthor != fred");
        assertThat(searchResult, sizeEquals(1));
        assertThat(searchResult, containsIssues(issueKey1));
    }

    public void testInOperator() throws Exception
    {
        final String issueKey1 = createDefaultIssue();
        final String issueKey2 = createDefaultIssue();
        createWorklogAsUser(issueKey1, "admin");
        createWorklogAsUser(issueKey2, "fred");
        SearchResult searchResult = executeJqlSearch("worklogAuthor in (fred, admin)");
        assertThat(searchResult, sizeEquals(2));
        assertThat(searchResult, containsIssues(issueKey1, issueKey2));
    }

    public void testNotInOperator() throws Exception
    {
        final String issueKey1 = createDefaultIssue();
        final String issueKey2 = createDefaultIssue();
        createWorklogAsUser(issueKey1, "admin");
        createWorklogAsUser(issueKey2, "fred");
        SearchResult searchResult = executeJqlSearch("worklogAuthor not in (fred)");
        assertThat(searchResult, sizeEquals(1));
        assertThat(searchResult, containsIssues(issueKey1));
    }

    public void testSearchingWorklogInDeletedIssue() throws Exception
    {
        final String issueKey = createDefaultIssue();
        createWorklogAsUser(issueKey, "admin");

        MatcherAssert.assertThat(executeJqlSearch("worklogAuthor = admin"), sizeEquals(1));

        issueClient.delete(issueKey, null);

        MatcherAssert.assertThat(executeJqlSearch("worklogAuthor = admin"), sizeEquals(0));
    }

    public void testAuthorAndDateConditionSimple() throws Exception
    {
        final String issue = createDefaultIssue();
        createWorklog(issue, "2014-08-10", "admin");
        createWorklog(issue, "2014-08-13", "fred");


        SearchResult searchResult = executeJqlSearch("worklogDate = 2014-08-10 AND worklogAuthor = admin");
        MatcherAssert.assertThat(searchResult, sizeEquals(1));
        MatcherAssert.assertThat(searchResult, containsIssues(issue));

        searchResult = executeJqlSearch("worklogDate = 2014-08-10 AND worklogAuthor = fred");
        MatcherAssert.assertThat(searchResult, sizeEquals(0));

        searchResult = executeJqlSearch("worklogDate = 2014-08-13 AND worklogAuthor = admin");
        MatcherAssert.assertThat(searchResult, sizeEquals(0));
    }

    public void testAuthorAndDateCondition() throws Exception
    {
        final String issueWithWorklogsOn10thAnd13th = createDefaultIssue();
        createWorklog(issueWithWorklogsOn10thAnd13th, "2014-08-10", "admin");
        createWorklog(issueWithWorklogsOn10thAnd13th, "2014-08-13", "fred");
        final String issueWithWorklogOn9thAnd14th = createDefaultIssue();
        createWorklog(issueWithWorklogOn9thAnd14th, "2014-08-09", "fred");
        createWorklog(issueWithWorklogOn9thAnd14th, "2014-08-14", "admin");

        final Effect<String> testFirstIssueReturnedForQuery = new Effect<String>() {
            @Override
            public void apply(final String query)
            {
                SearchResult searchResult = executeJqlSearchAsUser(query, "admin");
                MatcherAssert.assertThat(searchResult, sizeEquals(1));
                MatcherAssert.assertThat(searchResult, containsIssues(issueWithWorklogsOn10thAnd13th));
            }
        };

        testFirstIssueReturnedForQuery.apply("worklogDate < 2014-08-11 AND worklogAuthor = admin");
        testFirstIssueReturnedForQuery.apply("worklogDate < 2014-08-11 AND worklogAuthor = currentUser()");
        testFirstIssueReturnedForQuery.apply("worklogDate < 2014-08-11 AND worklogAuthor = echo('admin')");
        testFirstIssueReturnedForQuery.apply("worklogDate = 2014-08-10 AND worklogAuthor = admin");
        testFirstIssueReturnedForQuery.apply("worklogDate > 2014-08-09 AND worklogDate < 2014-08-11 AND worklogAuthor = admin");
        testFirstIssueReturnedForQuery.apply("worklogDate < 2014-08-11 AND project = HSP AND worklogAuthor = admin");
        testFirstIssueReturnedForQuery.apply("project = HSP AND (worklogDate < 2014-08-11 AND worklogAuthor = admin)");
        testFirstIssueReturnedForQuery.apply("worklogDate = 2014-08-10 AND worklogAuthor = admin OR (project = HSP AND (worklogDate < 2014-08-11 AND worklogAuthor = admin))");
    }

    public void testOrConditions() throws Exception
    {
        final String issueWithWorklogsOn10thAnd13th = createDefaultIssue();
        createWorklog(issueWithWorklogsOn10thAnd13th, "2014-08-10", "admin");
        createWorklog(issueWithWorklogsOn10thAnd13th, "2014-08-13", "fred");
        final String issueWithWorklogOn9thAnd14th = createDefaultIssue();
        createWorklog(issueWithWorklogOn9thAnd14th, "2014-08-09", "fred");
        createWorklog(issueWithWorklogOn9thAnd14th, "2014-08-14", "admin");

        final Effect<String> testBothIssuesReturnedForQuery = new Effect<String>() {
            @Override
            public void apply(final String query)
            {
                SearchResult searchResult = executeJqlSearchAsUser(query, "admin");
                MatcherAssert.assertThat(searchResult, sizeEquals(2));
                MatcherAssert.assertThat(searchResult, containsIssues(issueWithWorklogsOn10thAnd13th, issueWithWorklogOn9thAnd14th));
            }
        };

        testBothIssuesReturnedForQuery.apply("worklogDate < 2014-08-11 OR worklogAuthor = fred");
        testBothIssuesReturnedForQuery.apply("worklogAuthor = admin OR worklogAuthor = fred");
        testBothIssuesReturnedForQuery.apply("worklogAuthor = admin AND worklogDate = 2014-08-10 OR worklogAuthor = fred AND worklogDate = 2014-08-09");
    }
}
