package com.atlassian.jira.webtests.ztests.timetracking.modern;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.jira.testkit.client.restclient.SearchResult;
import com.atlassian.jira.testkit.client.restclient.Worklog;
import com.atlassian.jira.testkit.client.restclient.WorklogClient;
import com.atlassian.jira.webtests.Groups;
import com.atlassian.jira.webtests.ztests.bundledplugins2.rest.RestFuncTest;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.meterware.httpunit.WebResponse;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.Arrays;

abstract class AbstractWorklogSearchingTest extends RestFuncTest
{
    protected static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    protected static final long DEFAULT_PERMISSION_SCHEME_ID = 0;

    protected final String defaultStartDate = "2014-08-06";
    protected WorklogClient worklogClient;
    protected IssueClient issueClient;
    protected final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");

    @Override
    public void setUpTest()
    {
        super.setUpTest();

        backdoor.restoreBlankInstance();
        backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_TIMETRACKING, true);
        backdoor.applicationProperties().setOption(APKeys.JIRA_DEFAULT_TIMEZONE, true);
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERMISSION_SCHEME_ID, ProjectPermissions.EDIT_OWN_WORKLOGS, Groups.USERS);
        backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERMISSION_SCHEME_ID, ProjectPermissions.DELETE_OWN_WORKLOGS, Groups.USERS);

        worklogClient = new WorklogClient(environmentData);
        issueClient = new IssueClient(environmentData);
    }

    protected Worklog createWorklog(String issueKey, String date)
    {
        return createWorklog(issueKey, date, "admin");
    }

    protected Worklog createWorklog(String issueKey, String date, String userName)
    {
        return createWorklog(issueKey, dateTimeFormatter.parseDateTime(date), userName);
    }

    protected Worklog createWorklogAsUser(String issueKey, String userName)
    {
        return createWorklog(issueKey, DateTime.now(), userName);
    }

    private Worklog createWorklog(String issueKey, DateTime date, String userName)
    {
        final Worklog worklog = new Worklog();
        worklog.started = new SimpleDateFormat(TIME_FORMAT).format(date.toDate());
        worklog.timeSpent = "1h";
        return worklogClient.loginAs(userName).post(issueKey, worklog).body;
    }

    protected String getDateFromString(final String date)
    {
        final DateTime dateTime = dateTimeFormatter.parseDateTime(date);
        return new SimpleDateFormat(TIME_FORMAT).format(dateTime.toDate());
    }

    protected String createDefaultIssue()
    {
        return backdoor.issues().createIssue("HSP", "test issue!", "admin").key;
    }

    protected SearchResult executeEqualWorklogDateSearch(String date)
    {
        return backdoor.search().loginAs("fred", "fred").getSearch(new SearchRequest().jql("worklogDate=" + date));
    }

    protected SearchResult executeEqualWorklogDateSearchAsUser(String date, String user)
    {
        return backdoor.search().loginAs(user, user).getSearch(new SearchRequest().jql("worklogDate=" + date));
    }

    protected SearchResult executeJqlSearch(String query)
    {
        return executeJqlSearchAsUser(query, "fred");
    }

    protected SearchResult executeJqlSearchAsUser(String query, String user)
    {
        return backdoor.search().loginAs(user).getSearch(new SearchRequest().jql(query));
    }

    protected int getResponseCode(final String jql) throws Exception
    {
        final WebResponse webResponse2 = GET("/rest/api/2/search?jql=" + jql);
        return webResponse2.getResponseCode();
    }

    protected static Matcher<SearchResult> containsIssues(final String... issueKeys)
    {
        return new TypeSafeMatcher<SearchResult>()
        {
            @Override
            protected boolean matchesSafely(final SearchResult searchResult)
            {
                return Iterables.any(searchResult.issues, new Predicate<Issue>()
                {
                    @Override
                    public boolean apply(final Issue issue)
                    {
                        return Lists.newArrayList(issueKeys).contains(issue.key);
                    }
                });
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("Search result containing issue keys ").appendValue(Arrays.toString(issueKeys));
            }
        };
    }

    protected static Matcher<SearchResult> sizeEquals(final int size)
    {
        return new TypeSafeMatcher<SearchResult>()
        {
            @Override
            protected boolean matchesSafely(final SearchResult searchResult)
            {
                return searchResult.total == size;
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText("Search results of size ").appendValue(size);
            }
        };
    }
}
