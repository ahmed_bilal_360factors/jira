package com.atlassian.jira.functest.rule;

import java.io.File;

import com.atlassian.jira.testkit.client.AttachmentsControl;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * Copies attachments before a test.
 *
 * @since v6.4
 */
class CopyAttachmentsRule implements TestRule
{
    private final JIRAEnvironmentData environmentData;
    private final String sourceSubPath;
    private final AttachmentsControl attachmentsControl;

    CopyAttachmentsRule(
            final JIRAEnvironmentData environmentData,
            final String sourceSubPath,
            final AttachmentsControl attachmentsControl
    )
    {
        this.environmentData = environmentData;
        this.sourceSubPath = sourceSubPath;
        this.attachmentsControl = attachmentsControl;
    }

    @Override
    public Statement apply(final Statement base, final Description description)
    {
        return new Statement()
        {
            @Override
            public void evaluate() throws Throwable
            {
                final File dataDirectory = environmentData.getXMLDataLocation();
                final File sourceDirectory = new File(dataDirectory, sourceSubPath);
                final String attachmentPath = attachmentsControl.getAttachmentPath();
                final File targetDirectory = new File(attachmentPath);
                final TestRule copyDirectoryRule = Rules.copyDirectory(sourceDirectory, targetDirectory);
                copyDirectoryRule.apply(base, description).evaluate();
            }
        };
    }
}
