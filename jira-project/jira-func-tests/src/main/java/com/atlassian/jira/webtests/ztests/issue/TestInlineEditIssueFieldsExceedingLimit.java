package com.atlassian.jira.webtests.ztests.issue;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.functest.rules.RestRule;
import com.atlassian.jira.webtests.util.issue.IssueInlineEdit;

@WebTest ({ Category.FUNC_TEST, Category.ISSUES })
public class TestInlineEditIssueFieldsExceedingLimit extends FuncTestCase
{

    private RestRule restRule;

    @Override
    protected void setUpTest()
    {
        super.setUpTest();
        restRule = new RestRule(this);
        restRule.before();
        administration.restoreData("TestInlineEditIssueFieldsExceedingLimit.xml");
        navigation.login(ADMIN_USERNAME, ADMIN_PASSWORD);
    }

    @Override
    public void tearDownTest()
    {
        restRule.after();
    }

    public void testInlineEditTextArea() throws Exception
    {
        testInlineEditField("customfield_10000", "customfield_10000-val");
    }

    public void testInlineEditTextField() throws Exception
    {
        testInlineEditField("customfield_10001", "customfield_10001-val");
    }

    public void testInlineEditUrlField() throws Exception
    {
        testInlineEditField("customfield_10002", "environment-val");
    }

    public void testInlineEditEnvironment() throws Exception
    {
        testInlineEditField("environment", "environment-val");
    }

    public void testInlineEditDescription() throws Exception
    {
        testInlineEditField("description", "description-val");
    }

    public void testInlineEditSummary() throws Exception
    {
        // summary is not validated with jira character limit, edit succeeds
        testInlineEditField("summary", "summary-val", false);
    }

    private void testInlineEditField(String fieldName, String fieldId)
            throws Exception
    {
        testInlineEditField(fieldName, fieldId, true);
    }

    private void testInlineEditField(String fieldName, String fieldId, boolean shouldFail)
            throws Exception
    {
        navigation.issue().gotoIssue("HSP-2");
        final String oldValue = locator.id(fieldId).getText();
        final String newValue = oldValue + "s";

        // make sure the field is just at its limit
        backdoor.advancedSettings().setTextFieldCharacterLengthLimit(oldValue.length());

        IssueInlineEdit inlineEdit = new IssueInlineEdit(locator, tester, new RestRule(this));
        inlineEdit.inlineEditField("10100", fieldName, newValue);

        // Refresh the page
        navigation.issue().gotoIssue("HSP-2");

        if (shouldFail)
        {
            // Make sure the field has the value unchanged
            assertions.assertNodeByIdEquals(fieldId, oldValue);
        }
        else
        {
            assertions.assertNodeByIdEquals(fieldId, newValue);
        }
    }
}
