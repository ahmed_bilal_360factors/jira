package com.atlassian.jira.functest.framework.admin;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;

/**
 * Actions to be performed on the permission schemes in JIRA's administration.
 *
 * @since v4.0
 */
public interface PermissionSchemes
{
    /**
     * Navigates to the Default Permission Scheme.
     * @return the Default Permission Scheme to operate on.
     */
    PermissionScheme defaultScheme();

    /**
     * Navigates to the scheme with the specified name.
     * @param schemeName the permission scheme name.
     * @return the Permission Scheme with the given name.
     */
    PermissionScheme scheme(String schemeName);

    /**
     * @deprecated Use {@link ProjectPermissions#CREATE_ISSUES} or {@link ProjectPermissions#EDIT_ISSUES} instead.
     */
    class Type
    {
        public static final int CREATE_ISSUES = 11;
        public static final int EDIT_ISSUES = 12;
    }

    /**
     * Represents a permission scheme that actions can be carried out on
     */
    interface PermissionScheme
    {
        /**
         * @deprecated Use {@link #grantPermissionToGroup(ProjectPermissionKey, String)}.
         */
        @Deprecated
        void grantPermissionToGroup(int permission, String groupName);

        void grantPermissionToGroup(String permission, String groupName);

        void grantPermissionToGroup(ProjectPermissionKey permission, String groupName);

        /**
         * @deprecated Use {@link #grantPermissionToSingleUser(ProjectPermissionKey, String)}.
         */
        @Deprecated
        void grantPermissionToSingleUser(int permission, String username);

        void grantPermissionToSingleUser(String permission, String username);

        void grantPermissionToSingleUser(ProjectPermissionKey permission, String username);

        /**
         * @deprecated Use {@link #grantPermissionToReporter(ProjectPermissionKey)}.
         */
        @Deprecated
        void grantPermissionToReporter(int permission);

        void grantPermissionToReporter(String permission);

        void grantPermissionToReporter(ProjectPermissionKey permission);

        /**
         * @deprecated Use {@link #grantPermissionToProjectLead(ProjectPermissionKey)}.
         */
        @Deprecated
        void grantPermissionToProjectLead(int permission);

        void grantPermissionToProjectLead(String permission);

        void grantPermissionToProjectLead(ProjectPermissionKey permission);

        /**
         * @deprecated Use {@link #grantPermissionToCurrentAssignee(ProjectPermissionKey)}.
         */
        @Deprecated
        void grantPermissionToCurrentAssignee(int permission);

        void grantPermissionToCurrentAssignee(String permission);

        void grantPermissionToCurrentAssignee(ProjectPermissionKey permission);

        /**
         * @deprecated Use {@link #grantPermissionToUserCustomFieldValue(ProjectPermissionKey, String)}.
         */
        @Deprecated
        void grantPermissionToUserCustomFieldValue(int permission, String customFieldId);

        void grantPermissionToUserCustomFieldValue(String permission, String customFieldId);

        void grantPermissionToUserCustomFieldValue(ProjectPermissionKey permission, String customFieldId);

        /**
         * @deprecated Use {@link #grantPermissionToGroupCustomFieldValue(ProjectPermissionKey, String)}.
         */
        @Deprecated
        void grantPermissionToGroupCustomFieldValue(int permission, String customFieldId);

        void grantPermissionToGroupCustomFieldValue(String permission, String customFieldId);

        void grantPermissionToGroupCustomFieldValue(ProjectPermissionKey permission, String customFieldId);

        /**
         * @deprecated Use {@link #grantPermissionToProjectRole(ProjectPermissionKey, String)}.
         */
        @Deprecated
        void grantPermissionToProjectRole(int permission, String projectRoleId);

        void grantPermissionToProjectRole(String permission, String projectRoleId);

        void grantPermissionToProjectRole(ProjectPermissionKey permission, String projectRoleId);

        /**
         * Remove the given permission setting.
         *
         * @param permissionType the permission type. See {@link Type} for constants.
         * @param permissionParam the permission parameter. eg group-name for group based permissions, ID for project roles.
         * @deprecated Use {@link #removePermission(ProjectPermissionKey, String)} or {@link #removePermission(GlobalPermissionKey, String)}.
         */
        @Deprecated
        void removePermission(int permissionType, String permissionParam);

        void removePermission(String permissionType, String permissionParam);

        void removePermission(ProjectPermissionKey permissionType, String permissionParam);

        void removePermission(GlobalPermissionKey permissionType, String permissionParam);
    }
}

