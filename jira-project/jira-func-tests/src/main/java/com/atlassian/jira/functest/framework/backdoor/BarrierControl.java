package com.atlassian.jira.functest.framework.backdoor;

import com.atlassian.jira.webtests.util.JIRAEnvironmentData;

/**
 * Control for manipulating server-side barriers.
 *
 * @since v5.2
 */
public class BarrierControl extends BackdoorControl<BarrierControl>
{
    public BarrierControl(JIRAEnvironmentData environmentData)
    {
        super(environmentData);
    }

    /**
     * Raises the barrier called {@code barrierName} and calls the given {@code Runnable} before lowering the barrier
     * again. This is useful to test for race conditions in production code.
     *
     * @param barrierName a String containing the barrier name
     * @param r a Runnable
     */
    public void raiseBarrierAndRun(String barrierName, Runnable r)
    {
        raise(barrierName);
        try
        {
            r.run();
        }
        finally
        {
            lower(barrierName);
        }
    }

    /**
     * Raises the barrier called {@code barrierName} and calls the given {@code BarrierFunction} before lowering the
     * barrier again. The {@code BarrierFunction} is provided with the {@code Barrier} for fine-grained control.
     *
     * @param barrierName a String containing the barrier name
     * @param function a BarrierFunction
     */
    public void raiseBarrierAndRun(String barrierName, BarrierFunction function)
    {
        raise(barrierName);
        try
        {
            function.run(new Barrier(barrierName));
        }
        finally
        {
            lower(barrierName);
        }
    }

    private void raise(String barrierName)
    {
        createResource().path("barrier").path("raise").queryParam("barrierName", barrierName).post();
    }

    private void lower(String barrierName)
    {
        createResource().path("barrier").path("lower").queryParam("barrierName", barrierName).post();
    }

    /**
     * An instance of the barrier, provided to the BarrierFunction
     */
    public class Barrier
    {
        private String barrierName;

        public Barrier(String barrierName)
        {
            this.barrierName = barrierName;
        }

        /**
         * Lower the barrier to allow all waiting threads through, then immediately raise the barrier.
         * Each thread will only be allowed through the barrier once before it is raised again.
         */
        public void lowerThenRaise()
        {
            createResource().path("barrier").path("lowerThenRaise").queryParam("barrierName", barrierName).post();
        }
    }

    /**
     * A task to be run while the barrier is raised.
     */
    public interface BarrierFunction
    {
        /**
         * @param barrier the barrier
         */
        public void run(final Barrier barrier);
    }
}
