package com.atlassian.jira.webtests.ztests.admin;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.webtests.JIRAWebTest;

import com.sun.jersey.api.client.UniformInterfaceException;

import static com.atlassian.jira.functest.framework.backdoor.IssuesControl.LIST_VIEW_LAYOUT;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.MOVE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.SCHEDULE_ISSUES;
import static com.atlassian.jira.webtests.Groups.DEVELOPERS;
import static com.atlassian.jira.webtests.Groups.USERS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

@WebTest({Category.FUNC_TEST, Category.ADMINISTRATION, Category.PERMISSIONS, Category.SCHEMES })
public class TestPermissionSchemes extends JIRAWebTest
{
    public TestPermissionSchemes(String name)
    {
        super(name);
    }

    private static final String MOVE_TABLE_ID = "move_confirm_table";
    private static final int MOVE_TABLE_FIELD_NAME_COLUMN_INDEX = 0;
    private static final int MOVE_TABLE_OLD_VALUE_COLUMN_INDEX = 1;
    private static final int MOVE_TABLE_NEW_VALUE_COLUMN_INDEX = 2;

    public void testPermissionSchemes()
    {
        administration.restoreBlankInstance();
        backdoor.issueNavControl().setPreferredSearchLayout(LIST_VIEW_LAYOUT, ADMIN_USERNAME);

        administration.project().addProject(PROJECT_NEO, PROJECT_NEO_KEY, ADMIN_USERNAME);
        String issueKeyNormal = addIssue(PROJECT_HOMOSAP, PROJECT_HOMOSAP_KEY, "Bug", "test 1", "Minor", null, null, null, ADMIN_FULLNAME, "test environment 1", "test description for permission schemes", null, null, null);

        permissionSchemesCreateScheme();
        permissionSchemeAssociateScheme();
        permissionSchemeAddDuplicateScheme();
        permissionSchemeAddInvalidScheme();
        permissionSchemesMoveIssueToProjectWithAssignablePermission(issueKeyNormal);
        permissionSchemesMoveIssueWithSchedulePermission(issueKeyNormal);
        permissionSchemesMoveIssueToProjectWithCreatePermission(issueKeyNormal);

        permissionSchemeDeleteScheme();

        navigation.issue().deleteIssue(issueKeyNormal);
    }

    public void testProjectRolePermissionScheme()
    {
        logSection("Test to check that project role permission scheme works");
        administration.restoreData("TestSchemesProjectRoles.xml");
        gotoPermissionSchemes();
        tester.clickLinkWithText(DEFAULT_PERM_SCHEME);
        tester.assertTextPresent("Edit Permissions &mdash; " + DEFAULT_PERM_SCHEME);

        tester.clickLink("add_perm_" + MOVE_ISSUES.permissionKey());

        tester.assertTextPresent("Choose a project role");

        tester.checkCheckbox("type", "projectrole");
        tester.selectOption("projectrole", "test role");
        tester.submit();
        tester.assertTextPresent("(test role)");
    }

    public void permissionSchemesCreateScheme()
    {
        log("Permission Schemes: Create a new permission scheme");
        createPermissionScheme(PERM_SCHEME_NAME, PERM_SCHEME_DESC);
        tester.assertLinkPresentWithText(PERM_SCHEME_NAME);
        tester.assertTextPresent(PERM_SCHEME_DESC);
    }

    public void permissionSchemeDeleteScheme()
    {
        log("Permission Schemes:Delete a permission scheme");
        deletePermissionScheme(PERM_SCHEME_NAME);
        tester.assertLinkNotPresentWithText(PERM_SCHEME_NAME);
    }

    public void permissionSchemeAssociateScheme()
    {
        log("Permission Schemes: Associate a permission scheme with a project");
        associatePermSchemeToProject(PROJECT_NEO, PERM_SCHEME_NAME);

        assertThat(backdoor.project().getSchemes(PROJECT_NEO_KEY).permissionScheme.name, equalTo(PERM_SCHEME_NAME));

        associatePermSchemeToProject(PROJECT_NEO, DEFAULT_PERM_SCHEME);
    }

    /**
     * Create a scheme with a duplicate name
     */
    public void permissionSchemeAddDuplicateScheme()
    {
        log("Permission Schemes: Attempt to create a scheme with a duplicate name");
        createPermissionScheme(PERM_SCHEME_NAME, "");
        tester.assertTextPresent("Add Permission Scheme");
        tester.assertTextPresent("A Scheme with this name already exists.");
    }

    /**
     * Create a scheme with an invalid name
     */
    public void permissionSchemeAddInvalidScheme()
    {
        log("Permission Schemes: Attempt to create a scheme with an invalid name");
        createPermissionScheme("", "");
        tester.assertTextPresent("Add Permission Scheme");
        tester.assertTextPresent("Please specify a name for this Scheme.");
    }

    /**
     * Tests the ability to move an issue to a project WITHOUT the 'Assignable User' Permission
     */
    private void permissionSchemesMoveIssueToProjectWithAssignablePermission(String issueKey)
    {
        log("Move Operation: Moving issue to a project with 'Assign Issue' Permission.");
        associatePermSchemeToProject(PROJECT_NEO, PERM_SCHEME_NAME);
        setUnassignedIssuesOption(true);
        // Give jira-users 'Create' Permission
        backdoor.permissionSchemes().addGroupPermission(PERM_SCHEME_ID, CREATE_ISSUES, USERS);
        // give jira-developers 'Assignable Users' Permission
        backdoor.permissionSchemes().addGroupPermission(PERM_SCHEME_ID, ProjectPermissions.ASSIGNABLE_USER, DEVELOPERS);

        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("move-issue");
        navigation.issue().selectProject(PROJECT_NEO);
        tester.submit();
        tester.assertTextPresent("Step 3 of 4");

        tester.assertTextNotPresent(DEFAULT_ASSIGNEE_ERROR_MESSAGE);

        setUnassignedIssuesOption(false);
        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("move-issue");
        navigation.issue().selectProject(PROJECT_NEO);
        tester.submit();
        tester.assertTextPresent("Step 3 of 4");
        tester.assertTextNotPresent(DEFAULT_ASSIGNEE_ERROR_MESSAGE);

        backdoor.permissionSchemes().removeGroupPermission(PERM_SCHEME_ID, CREATE_ISSUES, USERS);
        associatePermSchemeToProject(PROJECT_NEO, DEFAULT_PERM_SCHEME);
    }

    /**
     * Test the abilty to move an issue with 'Schedule Issues' Permission and 'Due Date' Required
     */
    public void permissionSchemesMoveIssueWithSchedulePermission(String issueKey)
    {
        log("Move Operation: Moving issue to a project with 'Schedule Issue' Permission.");
        associatePermSchemeToProject(PROJECT_NEO, PERM_SCHEME_NAME);
        try
        {
            //Try to remove this permission.
            backdoor.permissionSchemes().removeGroupPermission(PERM_SCHEME_ID, SCHEDULE_ISSUES, DEVELOPERS);
        }
        catch (UniformInterfaceException e)
        {
            //It's fine if it fails, because the permission entity may not have existed in the first place.
        }
        backdoor.permissionSchemes().addGroupPermission(PERM_SCHEME_ID, CREATE_ISSUES, USERS);
        setDueDateToRequried();

        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("move-issue");
        navigation.issue().selectProject(PROJECT_NEO);
        tester.submit();

        tester.assertTextPresent("Step 3 of 4");

        tester.setWorkingForm("jiraform");
        tester.submit();
        tester.assertTextPresent("&quot;Due Date&quot; field is required and you do not have permission to Schedule Issues for project &quot;" + PROJECT_NEO + "&quot;.");

        // restore settings
        resetFields();
        backdoor.permissionSchemes().addGroupPermission(PERM_SCHEME_ID, SCHEDULE_ISSUES, DEVELOPERS);
        backdoor.permissionSchemes().removeGroupPermission(PERM_SCHEME_ID, CREATE_ISSUES, USERS);
        associatePermSchemeToProject(PROJECT_NEO, DEFAULT_PERM_SCHEME);
    }

    /**
     * Tests the ability to move an issue to a project WITHOUT the 'Create Issue' Permission
     */
    public void permissionSchemesMoveIssueToProjectWithCreatePermission(String issueKey)
    {
        log("Move Operation: Moving issue to a project with 'Create Issue' Permission.");
        associatePermSchemeToProject(PROJECT_NEO, PERM_SCHEME_NAME);

        backdoor.permissionSchemes().addGroupPermission(PERM_SCHEME_ID, CREATE_ISSUES, USERS);
        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("move-issue");
        assertions.getProjectFieldsAssertions().assertProjectsEquals(new String[] {PROJECT_HOMOSAP, PROJECT_NEO, PROJECT_HOMOSAP, PROJECT_MONKEY, PROJECT_NEO});
        backdoor.permissionSchemes().removeGroupPermission(PERM_SCHEME_ID, CREATE_ISSUES, USERS);
        navigation.issue().gotoIssue(issueKey);
        tester.clickLink("move-issue");
        assertions.getProjectFieldsAssertions().assertProjectsEquals(new String[] {PROJECT_HOMOSAP, PROJECT_HOMOSAP, PROJECT_MONKEY});

        associatePermSchemeToProject(PROJECT_NEO, DEFAULT_PERM_SCHEME);
    }
}

