package com.atlassian.jira.hallelujah;

import com.atlassian.buildeng.hallelujah.HallelujahClient;
import com.atlassian.buildeng.hallelujah.api.client.AbstractClientListener;
import com.atlassian.buildeng.hallelujah.api.client.ClientTestCaseProvider;
import com.atlassian.buildeng.hallelujah.api.client.ClientTestCaseResultCollector;
import com.atlassian.buildeng.hallelujah.api.client.ClientTestCaseRunner;
import com.atlassian.buildeng.hallelujah.api.model.TestCaseResult;
import com.atlassian.buildeng.hallelujah.core.JUnitReport;
import com.atlassian.buildeng.hallelujah.core.JUnitReportBuilder;
import com.atlassian.buildeng.hallelujah.api.model.TestCaseName;
import com.atlassian.buildeng.hallelujah.jms.JMSConnectionFactory.DeliveryMode;
import com.atlassian.buildeng.hallelujah.jms.JMSHallelujahClient;
import com.atlassian.buildeng.hallelujah.listener.TestsRunListener;
import com.atlassian.jira.hallelujah.local.LocalHallelujahClient;
import com.atlassian.jira.webtests.cargo.CargoTestHarness;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import junit.framework.Test;
import junit.framework.TestResult;
import org.apache.commons.lang.StringUtils;

import javax.jms.JMSException;
import java.io.File;
import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * We extend the CargoTestHarness in order to bring JIRA online the same way as the JIRA func tests
 *
 * But we don't actually want to run those tests the normal way, so we return a special suite that
 * runs a Halleujah Client instead
 *
 * It suite looks even weirder because it contains another suite - this is what the CargoTestHarness expects
 */
public class JIRAHallelujahClientTest extends CargoTestHarness
{

    public static Test suite() throws IOException
    {
        return suite(JIRAHallelujahClientTest.TestSuiteImpersonator.class);
    }

    private static HallelujahClient createClient()
    {
        final String localTestList = System.getProperty(LocalHallelujahClient.TEST_LIST_PROPERTY);
        if (StringUtils.isNotEmpty(localTestList))
        {
            try
            {
                System.out.println("Creating local client using file " + localTestList + "...");
                return new LocalHallelujahClient(new File(localTestList))
                        .registerListeners(new JIRAHallelujahClientTest.JUnitReportTestsRunListener("target/TEST-Hallelujah.xml", "AcceptanceTestHarness"));
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        } else {
            try
            {
                System.out.println("Creating jms client...");
                return new JMSHallelujahClient.Builder()
                        .setJmsConfig(JIRAHallelujahConfig.getConfiguration())
                        .setDeliveryMode(DeliveryMode.PERSISTENT)
                        .build();
            }
            catch (JMSException e)
            {
                throw new RuntimeException(e);
            }
        }
    }

    public static class TestSuiteImpersonator implements Test
    {
        private final HallelujahClient client;

        public TestSuiteImpersonator()
        {
            client = JIRAHallelujahClientTest.createClient();
        }

        @Override
        public int countTestCases()
        {
            return 1;
        }

        @Override
        public void run(TestResult result)
        {
            System.out.println("JIRA Hallelujah Client starting...");

            client.registerListeners(new TestsRunListener(new File("target/hallelujah-executed-tests.txt"))).run();

            System.out.println("JIRA Hallelujah Client finished.");
        }

        public static Test suite()
        {
            return new JIRAHallelujahClientTest.TestSuiteImpersonator();
        }
    }
    
    public static class JUnitReportTestsRunListener extends AbstractClientListener
    {
        private Queue<TestCaseResult> testResults = new ConcurrentLinkedQueue<TestCaseResult>();
        private final String filename;
        private final String suiteName;

        public JUnitReportTestsRunListener(final String filename, final String suiteName)
        {
            this.filename = filename;
            this.suiteName = suiteName;
        }

        @Override
        public boolean onPass(ClientTestCaseRunner clientTestCaseRunner, ClientTestCaseProvider clientTestCaseProvider, ClientTestCaseResultCollector clientTestCaseResultCollector, TestCaseResult result)
        {
            testResults.offer(result);
            return true;
        }

        @Override
        public boolean onFailure(ClientTestCaseRunner clientTestCaseRunner, ClientTestCaseProvider clientTestCaseProvider, ClientTestCaseResultCollector clientTestCaseResultCollector, TestCaseResult result)
        {
            testResults.offer(result);
            return true;
        }

        @Override
        public boolean onError(ClientTestCaseRunner clientTestCaseRunner, ClientTestCaseProvider clientTestCaseProvider, ClientTestCaseResultCollector clientTestCaseResultCollector, TestCaseResult result)
        {
            testResults.offer(result);
            return true;
        }

        @Override
        public void onFinish(ClientTestCaseRunner clientTestCaseRunner, ClientTestCaseProvider clientTestCaseProvider, ClientTestCaseResultCollector clientTestCaseResultCollector)
        {
            System.out.println("Generating jUnit XML report...");

            final JUnitReport jUnitReport = new JUnitReportBuilder()
                    .setFilename(filename)
                    .setSuiteName(suiteName)
                    .setResults(testResults)
                    .createJUnitReport();

            jUnitReport.writeReport();
        }
    }
}
