<%@ page import="com.atlassian.jira.plugin.navigation.HeaderFooterRendering" %>
<%@ page import="static com.atlassian.jira.component.ComponentAccessor.*" %>
<%@ page import="com.atlassian.jira.project.Project" %>
<%@ page import="com.atlassian.jira.user.UserProjectHistoryManager" %>
<%@ page import="com.atlassian.jira.security.Permissions" %>
<%@ page import="com.atlassian.jira.security.JiraAuthenticationContext" %>
<section class="footer-body" style="display:none">
<%
    //
    // IDEA gives you a warning below because it cant resolve JspWriter.  I don't know why but its harmless
    //
    HeaderFooterRendering footerRendering = getComponent(HeaderFooterRendering.class);
    footerRendering.includeFooters(out, request);
    // include web panels
    footerRendering.includeWebPanels(out, "atl.footer");
    
    Project predictSelectedProject = getComponent(UserProjectHistoryManager.class).getCurrentProject(Permissions.BROWSE, getComponent(JiraAuthenticationContext.class).getLoggedInUser());
    
%>
    <div id="footer-logo"><a href="http://www.atlassian.com/">Atlassian</a></div>
</section>
<jsp:include page="/includes/decorators/global-translations.jsp" />
<script type="text/javascript">

cm_selectedProjectId = 0;
cm_selectedProjectKey = "";

<% if( predictSelectedProject !=null) { %>
	cm_selectedProjectId = "<%=predictSelectedProject.getId()%>";
	cm_selectedProjectKey = "<%=predictSelectedProject.getKey()%>";
<% } %>



AJS.$("#heading-avatar").remove();
AJS.$(".breadcrumbs").css( "padding-left", "15px");
AJS.$("#summary-val").css( "padding-left", "15px");


cm_openCreateCaseDialog = "<%=request.getParameter("predict_createcase")%>";
cm_openCreateCaseType = "<%=request.getParameter("predict_issue_type")%>";

cm_predict_createriskregistercase = "<%=request.getParameter("predict_createriskregistercase")%>";
cm_predefined_summary = "<%=request.getParameter("predict_riskcontrolitemname")%>";
cm_openCreateTaskDialog = "<%=request.getParameter("predict_createtask")%>";

cm_surveyId = "<%=request.getParameter("predict_survey_id")%>";
cm_surveyName = "<%=request.getParameter("predict_survey_name")%>";

cm_requirementId =  "<%=request.getParameter("requirementId")%>";
cm_standardId =  "<%=request.getParameter("standardId")%>";

cm_site_id = "<%=request.getParameter("predict_site_id")%>";
cm_asset_id = "<%=request.getParameter("predict_asset_id")%>";

cm_projectId =  "<%=request.getParameter("projectId")%>";
cm_projectTask =  "<%=request.getParameter("projectTask")%>";
cm_projectCase =  "<%=request.getParameter("projectCase")%>";
	
AJS.$(document).ready(function () {
	AJS.$(".create-case").each(function( index ) {
		var href = AJS.$( this ).attr("href") ;
		if(href !="#"){
			AJS.$( this ).attr("href",href + encodeURIComponent("&pid="  + cm_selectedProjectId))
		}	
	});

	if(cm_openCreateCaseDialog == "true")
	{
		if(cm_openCreateCaseType && cm_openCreateCaseType == "Task"){
			AJS.$("#task_link").trigger("click");
		}else if(cm_openCreateCaseType && cm_openCreateCaseType == "CAPA"){
			//This is capa link
			AJS.$("#website_link").trigger("click");
		}else if(cm_openCreateCaseType && cm_openCreateCaseType == "Calibration Record"){
			//This is calibration record
			AJS.$("#calibration_record_linkid").trigger("click");
		}
		else{
			AJS.$("#case_link").trigger("click");
		}
	}
	/*if(cm_openCreateTaskDialog == "true")
	{
		AJS.$("#task_link").trigger("click");
	}*/
	
	if( (cm_projectTask=='true' || cm_projectCase=='true') && cm_projectId!='null') {
		if(cm_projectTask=='true') {
			AJS.$("#task_link").trigger("click");
		} else if(cm_projectCase=='true') {
			AJS.$("#case_link").trigger("click");
		}
	}
	
	if(cm_openCreateCaseType && cm_openCreateCaseType == "Calibration Record"){
		AJS.$("#customfield_11600").val(cm_site_id);
		AJS.$("#customfield_11600").trigger("change");
		AJS.$("#customfield_11600\\:1").val(cm_asset_id);
		AJS.$("#customfield_11600\\:1").trigger("change");
	}
	if(cm_projectId!='null') {
		//AJS.$("label:contains('__client_project_id')").parent().hide();
		AJS.$("label:contains('__client_project_id')").parent().find('input').val(cm_projectId);
	}
	
	
});

AJS.$(window).load(function() {
	
	if(typeof cm_surveyId !="undefined" && cm_surveyId !="null"){
		AJS.$("#customfield_10900").val(cm_surveyId);
	}	
	if(typeof cm_surveyName !="undefined" && cm_surveyName !="null"){
		AJS.$("#customfield_11400").val(cm_surveyName);
	}
	
	if(cm_predict_createriskregistercase == "true"){
		//Set Summary
		if(cm_predefined_summary){
			AJS.$("#summary").val(cm_predefined_summary);
			AJS.$("#summary").attr('readonly','readonly');
		}
		AJS.$("label:contains(\"__caselinktype__\")").next().val("riskregister:"+cm_surveyId);
		AJS.$("#customfield_11400").val("");
		AJS.$("#customfield_10900").val("");
    }else if(typeof cm_surveyId != "undefined" && cm_surveyId!="" && cm_surveyId !="null" ){
		AJS.$("label:contains(\"__caselinktype__\")").next().val("survey:"+cm_surveyId);
	}
	AJS.$("#logo").remove();

	
	if(cm_requirementId > 0){
		AJS.$.getJSON( "/predict360/ws/restapi/requirementservice/sitesbyrequirement/"+cm_requirementId, function( data ) {
			var items = [];
			var optionVal = "";
			$.each( data.output, function( key, val ) {
				optionVal += "[value!=" + val.jiraSiteId + "]";
			});
			$('#customfield_11600 option[value!=""]' + optionVal).remove();	
		});
	}
	        
});
</script>