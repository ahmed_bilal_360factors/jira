<%@ page import="com.atlassian.jira.security.xsrf.XsrfTokenGenerator"%>
<%@ page import="com.atlassian.jira.ComponentManager"%>
<%@ page import="java.net.URLEncoder"%>;
<%@ taglib prefix="ww" uri="webwork" %>
<%@ taglib prefix="aui" uri="webwork" %>
<%@ taglib prefix="page" uri="sitemesh-page" %>
<html>
<head>
    <title><ww:text name="'logout.confirm.title'"/></title>
    <meta name="decorator" content="message" />
</head>
<body>
    <div class="form-body">
        <header><h1><ww:text name="'logout.confirm.title'"/></h1></header>
        <aui:component template="auimessage.jsp" theme="'aui'">
            <aui:param name="'messageType'">warning</aui:param>
            <aui:param name="'messageHtml'">
                <p><ww:text name="'logout.confirm.desc'"/></p>
<%
XsrfTokenGenerator xsrfTokenGenerator = ComponentManager.getComponentInstanceOfType(XsrfTokenGenerator.class);
String logoutUrl = "/jira/secure/Logout!default.jspa?atl_token=" +  URLEncoder.encode(xsrfTokenGenerator.generateToken(request));
%>			
  <jsp:forward page="<%=logoutUrl%>"/>                
            </aui:param>
        </aui:component>
    </div>
</body>
</html>