<%@ taglib prefix="ww" uri="webwork" %>
<%@ taglib prefix="ui" uri="webwork" %>
<%@ taglib prefix="aui" uri="webwork" %>
<%@ taglib prefix="page" uri="sitemesh-page" %>
<%@ page import="com.atlassian.jira.component.ComponentAccessor" %>
<%@ page import="com.atlassian.jira.web.action.util.FieldsResourceIncluder" %>
<%@ page import="com.atlassian.plugin.webresource.WebResourceManager" %>
<html>
<head>
    <ww:if test="/ableToCreateIssueInSelectedProject == true">
        <title><ww:text name="'createissue.title'"/></title>
        <content tag="section">find_link</content>
        <%
            final FieldsResourceIncluder fieldResourceIncluder = ComponentAccessor.getComponent(FieldsResourceIncluder.class);
            fieldResourceIncluder.includeFieldResourcesForCurrentUser();

            final WebResourceManager wrm = ComponentAccessor.getWebResourceManager();
            wrm.requireResourcesForContext("jira.create.issue");
        %>
    </ww:if>
    <ww:else>
        <title><ww:text name="'common.words.error'"/></title>
        <meta name="decorator" content="message" />
    </ww:else>
</head>
<body class="aui-page-focused aui-page-focused-large">
<ww:if test="/ableToCreateIssueInSelectedProject == true">
    <ui:soy moduleKey="'jira.webresources:soy-templates'" template="'JIRA.Templates.Headers.pageHeader'">
        <ui:param name="'mainContent'">
            <h1><ww:text name="'createissue.title'"/></h1>
        </ui:param>
    </ui:soy>
    <ui:soy moduleKey="'com.atlassian.auiplugin:aui-experimental-soy-templates'" template="'aui.page.pagePanel'">
        <ui:param name="'content'">
            <ui:soy moduleKey="'com.atlassian.auiplugin:aui-experimental-soy-templates'" template="'aui.page.pagePanelContent'">
                <ui:param name="'content'">

            <page:applyDecorator id="issue-create" name="auiform">
                <page:param name="action">CreateIssueDetails.jspa</page:param>
                <page:param name="submitButtonName">Create</page:param>
                <page:param name="submitButtonText"><ww:property value="submitButtonName" escape="false" /></page:param>
                <page:param name="cancelLinkURI"><ww:url value="'default.jsp'" atltoken="false"/></page:param>
                <page:param name="isMultipart">true</page:param>

                <aui:component name="'pid'" template="hidden.jsp" theme="'aui'" />
                <aui:component name="'issuetype'" template="hidden.jsp" theme="'aui'" />
                <aui:component name="'formToken'" template="hidden.jsp" theme="'aui'" />

                <page:applyDecorator name="auifieldgroup">
                    <aui:component id="'project-name'" label="text('issue.field.project')" name="'project/string('name')'" template="formFieldValue.jsp" theme="'aui'" />
                </page:applyDecorator>

                <page:applyDecorator name="auifieldgroup">
                    <aui:component id="'issue-type'" label="text('issue.field.issuetype')" name="'issueType'" template="formIssueType.jsp" theme="'aui'">
                        <aui:param name="'issueType'" value="/constantsManager/issueType(issuetype)" />
                    </aui:component>
                </page:applyDecorator>

                <ww:component template="issuefields.jsp" name="'createissue'">
                    <ww:param name="'displayParams'" value="/displayParams"/>
                    <ww:param name="'issue'" value="/issueObject"/>
                    <ww:param name="'tabs'" value="/fieldScreenRenderTabs"/>
                    <ww:param name="'errortabs'" value="/tabsWithErrors"/>
                    <ww:param name="'selectedtab'" value="/selectedTab"/>
                    <ww:param name="'ignorefields'" value="/ignoreFieldIds"/>
                    <ww:param name="'create'" value="'true'"/>
                </ww:component>

                <jsp:include page="/includes/panels/updateissue_comment.jsp" />

            </page:applyDecorator>

                </ui:param>
            </ui:soy>
        </ui:param>
    </ui:soy>
</ww:if>
<ww:else>
    <div class="form-body">
        <header>
            <h1><ww:text name="'common.words.error'"/></h1>
        </header>
        <%@ include file="/includes/createissue-notloggedin.jsp" %>
    </div>
</ww:else>
<script type="text/javascript">
AJS.$(".localHelp").remove();
AJS.$("#issue-create-project-name").parent().hide();
AJS.$("#issue-create-issue-type").parent().hide();
AJS.$(".aui-page-header-main").find("h1").html("Create " + AJS.$("#issue-create-issue-type").html());
AJS.$(".help-lnk").remove();

AJS.$('#issue-create').submit(function(ev) {
    //ev.preventDefault(); // to stop the form from submitting

    	//if this case is linked to Survey than do this to make link
    	if(cm_openCreateCaseDialog == "true")
    	{
    		if(cm_predict_createriskregistercase == "true"){
    			//Set Summary
    			if(cm_predefined_summary){
    				AJS.$("#summary").val(cm_predefined_summary);
    				AJS.$("#summary").enable(false);
    			}	
    		}else{
    			AJS.$("#customfield_10900").val(cm_surveyId);
    			AJS.$("#customfield_11400").val(cm_surveyName);
    		}
    	}
    	
    	//If this is the task linked to the RKB task accordian
    	if(cm_openCreateTaskDialog == "true")
    	{
    		if(isRequirementFound)
    			AJS.$("#customfield_11701").val(cm_requirementId);
    		else{
    			if(AJS.$("#assetRequiredErrorDiv").length){
    				if(AJS.$("#customfield_11600").val()=="")
    					AJS.$("#assetRequiredErrorDiv").html('Please select site.');
    				else if(AJS.$(".select.cascadingselect-child").val()=="")
    					AJS.$("#assetRequiredErrorDiv").html('Please select asset.');
    			}else{
    				AJS.$(".aui-field-cascadingselect").append('<div id="assetRequiredErrorDiv" class="error" data-field="customfield_11600">Please select site.</div>');
    			}
    			AJS.$(".icon.throbber.loading").hide();
    			AJS.$("#create-issue-submit").removeAttr("disabled");
    			ev.preventDefault();
				//return;
    		}
    	}	
	
	
	
    /* Validations go here */
    //this.submit(); // If all the validations succeeded
});


</script>

</body>
</html>
