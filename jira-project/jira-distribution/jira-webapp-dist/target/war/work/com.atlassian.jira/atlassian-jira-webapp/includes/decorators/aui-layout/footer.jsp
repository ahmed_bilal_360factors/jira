<%@ page import="com.atlassian.jira.plugin.navigation.HeaderFooterRendering" %>
<%@ page import="static com.atlassian.jira.component.ComponentAccessor.*" %>
<%@ page import="com.atlassian.jira.project.Project" %>
<%@ page import="com.atlassian.jira.user.UserProjectHistoryManager" %>
<%@ page import="com.atlassian.jira.security.Permissions" %>
<%@ page import="com.atlassian.jira.security.JiraAuthenticationContext" %>
<%@ page import="com.atlassian.jira.security.PermissionManager" %>
<%@ page import="java.util.List" %>
<section class="footer-body" style="display:none">
<%
    //
    // IDEA gives you a warning below because it cant resolve JspWriter.  I don't know why but its harmless
    //
    HeaderFooterRendering footerRendering = getComponent(HeaderFooterRendering.class);
    footerRendering.includeFooters(out, request);
    // include web panels
    footerRendering.includeWebPanels(out, "atl.footer");
    
    Project predictSelectedProject = getComponent(UserProjectHistoryManager.class).getCurrentProject(Permissions.BROWSE, getComponent(JiraAuthenticationContext.class).getLoggedInUser());
    //predictSelectedProject = null;
	if(predictSelectedProject == null){
		PermissionManager permissionManager = getComponent(PermissionManager.class);
    	//Get projects for current user with browse permission. There must be one project only
    	List<Project> userProjects =  (List<Project>) permissionManager.getProjectObjects(10, getComponent(JiraAuthenticationContext.class).getLoggedInUser());
		if(userProjects != null && userProjects.size() > 0){
			predictSelectedProject = userProjects.get(0);
		}	
	}
%>
    <div id="footer-logo"><a href="http://www.atlassian.com/">Atlassian</a></div>
</section>
<jsp:include page="/includes/decorators/global-translations.jsp" />
<script type="text/javascript">
cm_selectedProjectId = 0;
cm_selectedProjectKey = "";
<% if( predictSelectedProject !=null) { %>
cm_selectedProjectId = "<%=predictSelectedProject.getId()%>";
cm_selectedProjectKey = "<%=predictSelectedProject.getKey()%>";
<% } %>


/*******For filter limit in issue search*******/
if(AJS.$("#criteriaJson").length){
	var customFields = JSON.parse(AJS.$("#criteriaJson").html())
	var predictCriteriaFields = customFields.searchers.groups[0].searchers;
	var filteredCustomFields = new Array();
	for(var index=0;index<predictCriteriaFields.length;index++){
	  if(predictCriteriaFields[index].name.indexOf("_") == -1 && predictCriteriaFields[index].name.indexOf("% Limits") == -1 ){
		filteredCustomFields.push(predictCriteriaFields[index]);
	  }
	} 
	customFields.searchers.groups[0].searchers = filteredCustomFields;
	AJS.$("#criteriaJson").html(JSON.stringify(customFields));



	var predictCriteriaFields = JSON.parse(AJS.$("#jqlFieldz").html())
	var filteredCustomFields = new Array();
	for(var index=0;index<predictCriteriaFields.length;index++){
	  if(predictCriteriaFields[index].value.indexOf("_") == -1  && predictCriteriaFields[index].value.indexOf("% Limits") == -1 ){
		filteredCustomFields.push(predictCriteriaFields[index]);
	  }
	} 

	AJS.$("#jqlFieldz").html(JSON.stringify(filteredCustomFields));
}

/************/

AJS.$("#project-name-val").html("Compliance Management");
AJS.$("#project-name-val").attr("href","/casemanagement");
AJS.$(".aui-page-header-image").remove();

AJS.$("#heading-avatar").remove();
AJS.$(".breadcrumbs").css( "padding-left", "15px");
AJS.$("#summary-val").css( "padding-left", "15px");


cm_openCreateCaseDialog = "<%=request.getParameter("predict_createcase")%>";
cm_openCreateCaseType = "<%=request.getParameter("predict_issue_type")%>";

cm_predict_createriskregistercase = "<%=request.getParameter("predict_createriskregistercase")%>";
cm_predefined_summary = "<%=request.getParameter("predict_riskcontrolitemname")%>";
cm_openCreateTaskDialog = "<%=request.getParameter("predict_createtask")%>";
cm_predefined_cost = "<%=request.getParameter("predict_riskcontrolitemcost")%>";
cm_predefined_efficacy = "<%=request.getParameter("predict_riskcontrolitemefficacy")%>";

cm_surveyId = "<%=request.getParameter("predict_survey_id")%>";
cm_surveyName = "<%=request.getParameter("predict_survey_name")%>";

cm_requirementId =  "<%=request.getParameter("requirementId")%>";
cm_standardId =  "<%=request.getParameter("standardId")%>";

cm_site_id = "<%=request.getParameter("predict_site_id")%>";
cm_asset_id = "<%=request.getParameter("predict_asset_id")%>";

cm_projectId =  "<%=request.getParameter("projectId")%>";
cm_projectTask =  "<%=request.getParameter("projectTask")%>";
cm_projectCase =  "<%=request.getParameter("projectCase")%>";
	
AJS.$(document).ready(function () {
	var isPredictUser="<%=session.getAttribute("predictUser")%>";
	//alert(Boolean(isPredictUser));
	if (isPredictUser=='false'){
		//alert("2 " +isPredictUser);
		AJS.$("#dash-options").hide();
	
	}
	AJS.$(".create-case").each(function( index ) {
		var href = AJS.$( this ).attr("href") ;
		if(href !="#"){
			AJS.$( this ).attr("href",href + encodeURIComponent("&pid="  + cm_selectedProjectId))
		}	
	});


	AJS.$("#project-name-val").html("Compliance Management");
	AJS.$("#project-name-val").attr("href","/casemanagement");
	AJS.$(".aui-page-header-image").remove();
	
	
	AJS.$("#share_type_selector").find('[value="project"]').remove();
	var tdObj = AJS.$("#searchOwnerUserName").parent().parent()
	tdObj.prev().parent(); 
	
	
	if(cm_openCreateCaseDialog == "true")
	{
		if(cm_openCreateCaseType && cm_openCreateCaseType == "Task"){
			AJS.$("#task_link").trigger("click");
		}else if(cm_openCreateCaseType && cm_openCreateCaseType == "CAPA"){
			//This is capa link
			AJS.$("#website_link").trigger("click");
		}else if(cm_openCreateCaseType && cm_openCreateCaseType == "Calibration Record"){
			//This is calibration record
			AJS.$("#calibration_record_linkid").trigger("click");
		}
		else{
			AJS.$("#case_link").trigger("click");
		}
	}
	if(cm_openCreateTaskDialog == "true")
	{
		AJS.$("#task_link").trigger("click");
	}
	
	if( (cm_projectTask=='true' || cm_projectCase=='true') && cm_projectId!='null') {
		if(cm_projectTask=='true') {
			AJS.$("#task_link").trigger("click");
		} else if(cm_projectCase=='true') {
			AJS.$("#case_link").trigger("click");
		}
	}
	
		if(cm_openCreateCaseType && cm_openCreateCaseType == "Calibration Record"){
			AJS.$("#customfield_11600").val(cm_site_id);
			AJS.$("#customfield_11600").trigger("change");
			AJS.$("#customfield_11600\\:1").val(cm_asset_id);
			AJS.$("#customfield_11600\\:1").trigger("change");
		}
		if(cm_projectId!='null') {
			//AJS.$("label:contains('__client_project_id')").parent().hide();
			AJS.$("label:contains('__client_project_id')").parent().find('input').val(cm_projectId);
		}
	
});

AJS.$(window).load(function() {


		/*parent.$('.page-container').animate({
        'marginTop' : "-=3000px" //moves down
        });*/

	if(typeof cm_surveyId !="undefined" && cm_surveyId !="null"){
		AJS.$("#customfield_10900").val(cm_surveyId);
	}	
	if(typeof cm_surveyName !="undefined" && cm_surveyName !="null"){
		AJS.$("#customfield_11400").val(cm_surveyName);
	}	
	
	if(cm_predict_createriskregistercase == "true"){
		//Set Summary
		if(cm_predefined_summary){
			AJS.$("#summary").val(cm_predefined_summary);
			AJS.$("#summary").attr('readonly','readonly');
			AJS.$("#customfield_10901").val(cm_predefined_cost);
			AJS.$("#customfield_10901").attr('readonly','readonly');
			AJS.$("#customfield_14400").val(cm_predefined_efficacy);
			AJS.$("#customfield_14400").attr('readonly','readonly');
		}
		AJS.$("label:contains(\"__caselinktype__\")").next().val("riskregister:"+cm_surveyId);
		AJS.$("#customfield_11400").val("");
		AJS.$("#customfield_10900").val("");
    }else if(typeof cm_surveyId != "undefined" && cm_surveyId!="" && cm_surveyId !="null" ){
		AJS.$("label:contains(\"__caselinktype__\")").next().val("survey:"+cm_surveyId);
	}
	AJS.$("#logo").remove();

	
	if(cm_requirementId > 0){
		AJS.$.getJSON( "/predict360/ws/restapi/requirementservice/sitesbyrequirement/"+cm_requirementId, function( data ) {
			var items = [];
			var optionVal = "";
			AJS.$.each( data.output, function( key, val ) {
				optionVal += "[value!=" + val.jiraSiteId + "]";
			});
			AJS.$('#customfield_11600 option[value!=""]' + optionVal).remove();	
		});
	}
	
	
	AJS.$.ajaxSetup({
	
	beforeSend: function( xhr,settings ) {
		if(this.url && this.url.indexOf("/secure/AjaxIssueAction.jspa?decorator=none") >=0 ){
			var paramObj = parseParams(settings.data);
			if(paramObj.fieldsToForcePresent=="assignee"){
				if(paramObj.assignee.toLowerCase().lastIndexOf("@" + cm_selectedProjectKey.toLowerCase()) > -1){
					//check is the user name contain @ itself
					     AJS.$.ajax({
							url: "/casemanagement/rest/api/2/user?username=" + paramObj.assignee,
							type: 'get',
							dataType: 'json',
							async: false,
							success: function(data) {
								//user found do nothing
							},
							error: function(error){
								paramObj.assignee = paramObj.assignee + "@" +  cm_selectedProjectKey;
							}
						 });
				}else if(paramObj.assignee != "" && paramObj.assignee != "-1"){
					paramObj.assignee = paramObj.assignee + "@" +  cm_selectedProjectKey;
				}	
			}else if(paramObj.fieldsToForcePresent=="customfield_12804"){
				paramObj.customfield_12804 = ParseArrayForCodes(paramObj.customfield_12804.split(","));
			}else if(paramObj.fieldsToForcePresent=="customfield_13559"){
				paramObj.customfield_13559 = ParseArrayForCodes(paramObj.customfield_13559.split(","));
			}else if(paramObj.fieldsToForcePresent=="customfield_12805"){
				paramObj.customfield_12805 = ParseArrayForCodes(paramObj.customfield_12805.split(","));
			}else if(paramObj.fieldsToForcePresent=="customfield_12808"){
				paramObj.customfield_12808 = ParseArrayForCodes(paramObj.customfield_12808.split(","));
			}else if(paramObj.fieldsToForcePresent=="customfield_12802"){
				paramObj.customfield_12802 = ParseArrayForGroupCodes(paramObj.customfield_12802.split(","));
			}
			
			
			settings.data = AJS.$.param( paramObj );
			
		}else if(this.url && this.url.indexOf("/secure/CommentAssignIssue.jspa") >=0 ){
            var paramObj = parseParams(settings.data);
            if(paramObj.assignee != "" && paramObj.assignee != "-1"){
				paramObj.assignee = paramObj.assignee + "@" +  cm_selectedProjectKey;
			}	
            settings.data = AJS.$.param( paramObj );
     	}else if(this.url && this.url.indexOf("/secure/AssignIssue.jspa") >=0 ){
            var paramObj = parseParams(settings.data);
            if(paramObj.assignee != "" && paramObj.assignee != "-1"){
				paramObj.assignee = paramObj.assignee + "@" +  cm_selectedProjectKey;
			}	
            settings.data = AJS.$.param( paramObj );
     	}
		
	},
	
    dataFilter: function(data, type) {
		
//http://localhost:8080/casemanagement/rest/api/latest/user/assignable/multiProjectSearch?username=ab&projectKeys=DEVTEAM&maxResults=500&_=1456920013977
//http://localhost:8080/casemanagement/rest/api/1.0/users/picker?showAvatar=true&query=abi&_=1456920121033
//http://localhost:8080/casemanagement/rest/api/latest/user/assignable/search?username=ab&projectKeys=DEVTEAM&issueKey=DEVTEAM-545&maxResults=500&_=1456920208429
//alert("ok ok ");
	if(this.url.indexOf("/user/assignable/multiProjectSearch?username=") >=0 || this.url.indexOf("/rest/api/latest/user/assignable/search?username=") >=0 ){
	
	if(type == 'json' && typeof(data) == 'string' && data != "") {
				  var parsed_data = JSON.parse(data);
				  AJS.$.each(parsed_data, function(i, item)
				  {
					  var lastIndexOf = item.name.toLowerCase().lastIndexOf("@" + cm_selectedProjectKey.toLowerCase());
					  if( lastIndexOf > -1){
							console.log("Going to split");
							console.log(item.name);
							var orgininalName =  item.name;
							item.name =  orgininalName.substring(0,lastIndexOf);
							console.log(item.name);
					  }
					  
				  });
				  return JSON.stringify(parsed_data);
			}
		}else if(this.url.indexOf("/rest/api/1.0/users/picker?showAvatar=true&query=") >=0){
			if(type == 'json' && typeof(data) == 'string' && data != "") {
				  var parsed_data = JSON.parse(data);
					if(parsed_data.user){
					 AJS.$.each(parsed_data.users, function(i, item)
					  {
						  var lastIndexOf = item.name.lastIndexOf("@" + cm_selectedProjectKey);
						  if( lastIndexOf > -1){
								var orgininalName =  item.name;
								item.name =  orgininalName.substring(0,lastIndexOf);
								console.log(item.name);
						  }
						  
					  });
					}  
				  return JSON.stringify(parsed_data);
			}
		}		
		
		else if(this.url.indexOf("/casemanagement/plugins/servlet/user/picker") >=0) {
				  var parsed_data = JSON.parse(data);
				  AJS.$.each(parsed_data.users, function(i, item)
				  {
					  var lastIndexOf = item.name.lastIndexOf("@" + cm_selectedProjectKey);
					  if( lastIndexOf > -1){
							var orgininalName =  item.name;
							item.name =  orgininalName.substring(0,lastIndexOf);
					  }
					  
				  });
				  return JSON.stringify(parsed_data);
		}
        return data;
    },
		
    ajaxComplete: function() {
		AJS.$("#project-name-val").html("Compliance Management");
		AJS.$("#project-name-val").attr("href","/casemanagement");
		AJS.$(".aui-page-header-image").remove(); 
    }
});
/*	AJS.$("#priority-field").trigger( "click" )
	AJS.$(AJS.$(".icon.aui-ss-icon.noloading.drop-menu")[0]).trigger("click");
	AJS.$("#priority-field").trigger( "click" )
	*/
	/*var obj = AJS.$(".icon.aui-ss-icon.noloading.drop-menu")[0]
	console.log(obj);
	AJS.$(obj).trigger("click");
*//*	
	setTimeout(function(){ 
							
		parent.$('.page-container').animate({
        'marginTop' : "+=200px" //moves down
        });
		parent.$("#somerandominput").trigger( "click" );
		parent.$("#somerandominput").focus();
						}, 2000);
						
	setTimeout(function(){ 
							
		parent.$('.page-container').animate({
        'marginTop' : "-=200px" //moves down
        });
						}, 2000);
	
*/

});
//parent.doCMLayout(AJS.$(document).height());
/*function onElementHeightChange(elm, callback){
	var lastHeight = elm.clientHeight, newHeight;
	(function run(){
		newHeight = elm.clientHeight;
		if( lastHeight != newHeight )
			callback();
		lastHeight = newHeight;

        if( elm.onElementHeightChangeTimer )
          clearTimeout(elm.onElementHeightChangeTimer);

		elm.onElementHeightChangeTimer = setTimeout(run, 200);
	})();
}


onElementHeightChange(document.body, function(){
  parent.doCMLayout(AJS.$(document).height());
});*/

AJS.$("#project-name-val").html("Compliance Management");
AJS.$("#project-name-val").attr("href","/casemanagement");
AJS.$(".aui-page-header-image").remove();

function removeProjectAvatar(){
	AJS.$("#project-name-val").html("Compliance Management");
	AJS.$("#project-name-val").attr("href","/casemanagement");
	AJS.$(".aui-page-header-image").remove(); 	
}

AJS.$( document ).ajaxComplete(function() {
	removeProjectAvatar();
});

AJS.$( document ).ajaxError(function() {
	removeProjectAvatar();
});

AJS.$( document ).ajaxSend(function() {
	removeProjectAvatar();
});

AJS.$( document ).ajaxComplete(function() {
	var loc=window.location.href;
		if (loc.includes("browse")){ 
		var issueKey =loc.substring(loc.lastIndexOf("/")+1,loc.length);
var ul = document.getElementById("opsbar-jira.issue.tools");
console.log( document.getElementById("lastPDF"));
if (document.getElementById("lastPDF")==null){
	console.log("2");
var newLiAdd="<li id=lastPDF class=aui-list-item><a href=/casemanagement/si/jira.issueviews:issue-word/"+issueKey+"/"+issueKey+".PDF class=aui-list-item-link id=jira.issueviews:issue-html><span class=trigger-label>PDF</span></a></li>";
AJS.$(ul).find("ul").append(newLiAdd);
//console.log(ul.innerHTML);
		}
		}	
	
	removeProjectAvatar();
});

AJS.$( document ).ajaxComplete(function() {
	removeProjectAvatar();
});

AJS.$( document ).ajaxStart(function() {
	removeProjectAvatar();
});

AJS.$( document ).ajaxStop(function() {
	removeProjectAvatar();
});

AJS.$( document ).ajaxSuccess(function() {
	removeProjectAvatar();
});

/*
AJS.$('#issue-content').bind("DOMSubtreeModified",function(){
  
});*/

JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function(e, context, reason) {
		if (reason == JIRA.CONTENT_ADDED_REASON.inlineEditStarted) {
		   var $context = AJS.$(context);
			if($context.find("#assignee-field").length > 0) {
				//alert("started");
				AJS.$("#assignee-field").blur();
				//parent.$("#somerandominput").trigger( "click" );
				//parent.$("#somerandominput").focus();
				//console.log(parent.$("#somerandominput"));
				
			}else if($context.find("#customfield_12804").length > 0){
				//alert("Going right direction");
				antiSanitizeUsers("customfield_12804");
			}else if($context.find("#customfield_13559").length > 0){
				//alert("Going right direction");
				antiSanitizeUsers("customfield_13559");
			}else if($context.find("#customfield_12805").length > 0){
				//alert("Going right direction");
				antiSanitizeUsers("customfield_12805");
			}else if($context.find("#customfield_12808").length > 0){
				antiSanitizeUsers("customfield_12808");
			}else if($context.find("#customfield_12805").length > 0){
				antiSanitizeUsers("customfield_12805");
			}else if($context.find("#customfield_12106").length > 0){
				antiSanitizeUsers("customfield_12106");
			}else if($context.find("#customfield_12802").length > 0){
				var groupAssigneeVal = AJS.$("#customfield_12802").val();
				antiSanitizeGroup("customfield_12802");
			}
		//console.log($context[0].prop("id"));
		}		        
		if (reason == JIRA.CONTENT_ADDED_REASON.panelRefreshed) {

		}
});

        AJS.$(function() {
			//AJS.$(AJS.$(".icon.aui-ss-icon.noloading.drop-menu")[0]).trigger("click");
			//AJS.$(".icon.aui-ss-icon.noloading.drop-menu")
            //AJS.$("[onClick]").each(function(){
			
			AJS.$(AJS.$(".icon.aui-ss-icon.noloading.drop-menu")[0]).click(function() {
				//parent.$("#somerandominput").trigger( "click" );
				//parent.$("#somerandominput").focus();
				console.log(":: Test ::");
			});			
				
			/*AJS.$(".icon.aui-ss-icon.noloading.drop-menu").each(function(){
                //var x = AJS.$(this).attr("onClick");
                //AJS.$(this).removeAttr("onClick").unbind("click");
                AJS.$(this).click(function(e){
                    // do what you want here...
					//console.log(x);
                    //eval(x); // do original action here...
					//parent.$("#somerandominput").trigger( "click" );
					parent.$("#somerandominput").focus();
					console.log("Here");
					//AJS.$(this).focus();
					//AJS.$(this).trigger("click");
                });
            });*/
        });


/*AJS.$.fn.setCursorPosition = function(position){
	if(this.lengh == 0) return this;
	return AJS.$(this).setSelection(position, position);
}
AJS.$("#summary").setCursorPosition(100);
*/


  function getQueryVariable(url, variable) {
	  console.log(getQueryVariable);
  	 var query = url.substring(1);
     var vars = query.split('&');
     for (var i=0; i<vars.length; i++) {
          var pair = vars[i].split('=');
		  console.log(pair);
          if (pair[0] == variable) {
            return pair[1];
          }
     }

     return false;
  }
  
  function parseParams(str) {
    return str.split('&').reduce(function (params, param) {
        var paramSplit = param.split('=').map(function (value) {
            return decodeURIComponent(value.replace('+', ' '));
        });
        params[paramSplit[0]] = paramSplit[1];
        return params;
    }, {});
}
  
  function ParseArrayForCodes(attendees){
		console.log(attendees);
		var originalAttendees = new Array();
		for(userIndex = 0; userIndex < attendees.length ; userIndex++){
			if(attendees[userIndex].trim() != "" && attendees[userIndex].trim() != "+"){
				var lastIndexOf = attendees[userIndex].trim().toLowerCase().lastIndexOf("@" + cm_selectedProjectKey.toLowerCase());
				if( lastIndexOf == -1){
					console.log(attendees[userIndex]);
					attendees[userIndex] = (attendees[userIndex].trim() + "@" + cm_selectedProjectKey).replace(/\+/g, ' '); 
					attendees[userIndex] = attendees[userIndex].trim();
					//Special hard code check
					if(attendees[userIndex].trim() == "safety+department@SMS"){
						attendees[userIndex] = "safety department@SMS";
					}
					console.log(attendees[userIndex]);
					originalAttendees.push(attendees[userIndex]);
				}else{
					if(attendees[userIndex] == "jkaiser@hrs" || attendees[userIndex] == "admin@HRS" || attendees[userIndex] == "jmaselas@hrs"  || attendees[userIndex] == "kmcdaniel@hrs"   || attendees[userIndex] == "kladut@hrs"   || attendees[userIndex] == "laustin@hrs"   || attendees[userIndex] == "mmatran@hrs"   || attendees[userIndex] == "tmclin@hrs"){
						originalAttendees.push(attendees[userIndex].trim() +"@hrs");
					}else{
						originalAttendees.push(attendees[userIndex].trim());
					}
				}
			}
		}
		console.log(originalAttendees);
		var lastIndexOf = originalAttendees.lastIndexOf("@" + cm_selectedProjectKey);
		originalAttendees=originalAttendees.substring(0,lastIndexOf);
  
		//alert(originalAttendees);
		return originalAttendees;
		
	}
  
	 function ParseArrayForGroupCodes(attendees){
			console.log(attendees);
			var originalAttendees = new Array();
			for(userIndex = 0; userIndex < attendees.length ; userIndex++){
				if(attendees[userIndex].trim() != "" && attendees[userIndex].trim() != "+"){
					var lastIndexOf = attendees[userIndex].trim().toLowerCase().lastIndexOf("@" + cm_selectedProjectKey.toLowerCase());
					if( lastIndexOf == -1){
						console.log(attendees[userIndex]);
						attendees[userIndex] = (cm_selectedProjectKey + "-" + attendees[userIndex].trim()).replace(/\+/g, ' '); 
						attendees[userIndex] = attendees[userIndex].trim();
						originalAttendees.push(attendees[userIndex]);
					}else{
						originalAttendees.push(attendees[userIndex].trim());
					}
				}
			}
			console.log(originalAttendees);
			return originalAttendees;
		}

	function antiSanitizeUsers(fieldId){
		var attendees = AJS.$("#" + fieldId).val().split(",");
		for(userIndex = 0; userIndex < attendees.length ; userIndex++){
			if(attendees[userIndex].trim() != ""){
				var lastIndexOf = attendees[userIndex].toLowerCase().lastIndexOf("@" + cm_selectedProjectKey.toLowerCase());
				if( lastIndexOf > -1){
					var orgininalName =  attendees[userIndex];
					attendees[userIndex] = orgininalName.substring(0,lastIndexOf); 
				}
				
			}
		}
		AJS.$("#" + fieldId).val(attendees);
	}

	function sanitizeUsers(fieldId){
				var fieldObj = AJS.$("#" + fieldId);
				var attendees = ParseArrayForCodes(fieldObj.val().split(","));
				var cloned = fieldObj.clone();
				cloned.val(attendees);
				cloned.hide();
				fieldObj.attr("name","");
				fieldObj.parent().append(cloned);
	}
	
	function antiSanitizeGroup(fieldId){
		var attendees = AJS.$("#" + fieldId).val().split(",");
		for(userIndex = 0; userIndex < attendees.length ; userIndex++){
			if(attendees[userIndex].trim() != ""){
				var lastIndexOf = attendees[userIndex].toLowerCase().indexOf(cm_selectedProjectKey.toLowerCase()+ "-");
				if( lastIndexOf > -1){
					var orgininalName =  attendees[userIndex];
					attendees[userIndex] = orgininalName.substring(attendees[userIndex].indexOf("-")+1); 
					console.log(attendees[userIndex]);
				}
				
			}
		}
		AJS.$("#" + fieldId).val(attendees);
	}
	
	
	
	
</script>
