<%@ page import="com.atlassian.jira.component.ComponentAccessor" %>
<%@ page import="com.atlassian.jira.plugin.navigation.HeaderFooterRendering" %>

<!--[if IE]><![endif]--><%-- Leave this here - it stops IE blocking resource downloads - see http://www.phpied.com/conditional-comments-block-downloads/ --%>
<script type="text/javascript">var contextPath = '<%=request.getContextPath()%>';
	var linkParser = document.createElement("a");
	linkParser.href = location.href;
	var path = linkParser.pathname.split("/");
	var actualPath = "";
	for(var i=2;i<path.length;i++){
	        if(i>2){
	                actualPath = actualPath + "/" +  path[i];
	        }else if(i==2){
	                actualPath = path[i];
	        }
	}
	var parentURI = linkParser.protocol + "//"+ linkParser.hostname + ":" + linkParser.port +  "/predict360/casemanagement.do?multi_param=true&cm_url=" + actualPath ;
	
	if(linkParser.search != ""){
	        parentURI = parentURI + encodeURIComponent(linkParser.search);
	}
	
	if (top.location.href == self.location.href){
	        top.location.href = parentURI;
	}
</script>
<%
    //
    // IDEA gives you a warning below because it cant resolve JspWriter.  I don't know why but its harmless
    //
    HeaderFooterRendering headerAndFooter = ComponentAccessor.getComponent(HeaderFooterRendering.class);

    headerAndFooter.requireCommonResources();
    headerAndFooter.includeResources(out);
%>
<script type="text/javascript" src="<%=headerAndFooter.getKeyboardShortCutScript(request) %>"></script>
<%
    headerAndFooter.includeWebPanels(out, "atl.header.after.scripts");
%>
